﻿Imports System.Reflection
Imports LeCroy.Library.Domain.Common.DTOs

Public Class ExtendedMenu
    Inherits Menu

    Public Sub New(ByVal menu As Menu)
        Dim propertyInfos As PropertyInfo() = GetType(Menu).GetProperties(BindingFlags.Public Or BindingFlags.Instance)
        For Each propertyInfo As PropertyInfo In propertyInfos
            propertyInfo.SetValue(Me, propertyInfo.GetValue(menu, Nothing), Nothing)
        Next
    End Sub

    Public SubMenu As IList(Of SubMenu)
End Class