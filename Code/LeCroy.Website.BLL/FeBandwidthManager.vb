﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeBandwidthManager
    Public Shared Function GetActiveBandwidthRanges(ByVal connectionString As String) As IList(Of BandwidthRange)
        Dim sqlString As String = "SELECT * FROM [BANDWIDTH_RANGE] WHERE [ACTIVE_IND] = 'Y'"
        Return BandwidthRangeRepository.FetchBandwidthRanges(connectionString, sqlString, New List(Of SqlParameter)().ToArray()).ToList()
    End Function
End Class