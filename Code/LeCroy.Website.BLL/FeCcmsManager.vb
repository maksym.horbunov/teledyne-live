﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class FeCcmsManager
    Public Shared Function GetDomain(ByVal connectionString As String, ByVal mainDomain As String, ByVal state As String) As IList(Of CCMSExceptionDomain)
        Dim sqlString As String = "SELECT * FROM [CCMS_EXCEPTION_DOMAINS] WHERE [STATE] = @STATE AND [MAINDOMAIN] = @MAINDOMAIN AND [SECODE] IN (SELECT [SECODE] FROM [CCMS_SECSR])"
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        Dim sqlParameters As List(Of SqlParameter) = dalHelper.BuildSqlParameterList(New KeyValuePair(Of String, String)("@STATE", state), New KeyValuePair(Of String, String)("@MAINDOMAIN", mainDomain))
        Return FetchCCMSExceptionDomain(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetDomainWithPersonnelJoin(ByVal connectionString As String, ByVal mainDomain As String, ByVal state As String) As IList(Of CCMSExceptionDomain)
        Dim sqlString As String = "SELECT a.* FROM [CCMS_PERSONNEL_DATA] AS b INNER JOIN [CCMS_EXCEPTION_DOMAINS] AS a ON b.ID = a.SPCODE WHERE [STATE] = @STATE AND [MAINDOMAIN] = @MAINDOMAIN AND b.RECTYPE = 'sp' AND b.Active = 'y'"
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        Dim sqlParameters As List(Of SqlParameter) = dalHelper.BuildSqlParameterList(New KeyValuePair(Of String, String)("@STATE", state), New KeyValuePair(Of String, String)("@MAINDOMAIN", mainDomain))
        Return FetchCCMSExceptionDomain(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Private Shared Function FetchCCMSExceptionDomain(ByVal connectionString As String, ByVal sqlString As String, ByVal sqlParameters As SqlParameter()) As IList(Of CCMSExceptionDomain)
        Dim ds As DataSet = DbHelperSQL.Query(connectionString, sqlString, sqlParameters)
        Dim retVal As List(Of CCMSExceptionDomain) = New List(Of CCMSExceptionDomain)
        If (ds.Tables.Count > 0) Then
            For Each dr As DataRow In ds.Tables(0).Rows
                retVal.Add(ConvertDataRowToCCMSExceptionDomain(dr))
            Next
        End If
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToCCMSExceptionDomain(ByVal dr As DataRow) As CCMSExceptionDomain
        Dim retVal As CCMSExceptionDomain = New CCMSExceptionDomain()
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        retVal.MainDomain = IIf(dalHelper.DoesDbColumnExist(dr, "MAINDOMAIN"), dr("MAINDOMAIN").ToString(), String.Empty)
        retVal.City = IIf(dalHelper.DoesDbColumnExist(dr, "CITY"), dr("CITY").ToString(), String.Empty)
        retVal.State = IIf(dalHelper.DoesDbColumnExist(dr, "STATE"), dr("STATE").ToString(), String.Empty)
        retVal.SeCode = IIf(dalHelper.DoesDbColumnExist(dr, "SECODE"), dr("SECODE").ToString(), String.Empty)
        retVal.SpCode = IIf(dalHelper.DoesDbColumnExist(dr, "SPCODE"), dr("SPCODE").ToString(), String.Empty)
        retVal.RowGuid = IIf(dalHelper.DoesDbColumnExist(dr, "ROWGUID"), Guid.Parse(dr("ROWGUID").ToString()), Guid.Empty)
        Return retVal
    End Function

    Public Shared Function IsEmailDomainInTeledyneFamily(ByVal connectionString As String, ByVal emailAddress As String) As Boolean
        If (String.IsNullOrEmpty(emailAddress)) Then Return False
        If Not (emailAddress.Contains("@")) Then Return False

        Dim splitString As String() = emailAddress.Split("@")
        If (String.IsNullOrEmpty(splitString(1))) Then Return False

        Dim sqlString As String = "SELECT * FROM [CCMS_TELEDYNE_DOMAINS] WHERE [DomainName] = @DOMAINNAME"
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        Dim sqlParameters As List(Of SqlParameter) = dalHelper.BuildSqlParameterList(New KeyValuePair(Of String, String)("@DOMAINNAME", splitString(1)))
        Return FetchCCMSTeledyneDomains(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0
    End Function

    Private Shared Function FetchCCMSTeledyneDomains(ByVal connectionString As String, ByVal sqlString As String, ByVal sqlParameters As SqlParameter()) As IList(Of CCMSExceptionDomain)
        Dim ds As DataSet = DbHelperSQL.Query(connectionString, sqlString, sqlParameters)
        Dim retVal As List(Of CCMSTeledyneDomains) = New List(Of CCMSTeledyneDomains)
        If (ds.Tables.Count > 0) Then
            For Each dr As DataRow In ds.Tables(0).Rows
                retVal.Add(ConvertDataRowToCCMSTeledyneDomains(dr))
            Next
        End If
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToCCMSTeledyneDomains(ByVal dr As DataRow) As CCMSTeledyneDomains
        Dim retVal As CCMSTeledyneDomains = New CCMSTeledyneDomains()
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        retVal.DomainName = IIf(dalHelper.DoesDbColumnExist(dr, "DomainName"), dr("DomainName").ToString(), String.Empty)
        retVal.RowGuid = IIf(dalHelper.DoesDbColumnExist(dr, "rowquid"), Guid.Parse(dr("rowquid").ToString()), Guid.Empty)
        Return retVal
    End Function

    Public Shared Function GetDistinctActiveSeAndDmEmails(ByVal connectionString As String) As List(Of String)
        Return FetchCCMSPersonnelData(connectionString, "SELECT DISTINCT [EMAIL] FROM [CCMS_PERSONNEL_DATA] WHERE [ACTIVE] = 'Y' AND [RecType] IN ('DM','SE') AND [EMAIL] <> 'N/A' ORDER BY [EMAIL]", New List(Of SqlParameter)().ToArray()).Select(Function(x) x.Email).ToList()
    End Function

    Private Shared Function FetchCCMSPersonnelData(ByVal connectionString As String, ByVal sqlString As String, ByVal sqlParameters As SqlParameter()) As IList(Of CCMSPersonnelData)
        Dim ds As DataSet = DbHelperSQL.Query(connectionString, sqlString, sqlParameters)
        Dim retVal As List(Of CCMSPersonnelData) = New List(Of CCMSPersonnelData)
        If (ds.Tables.Count > 0) Then
            For Each dr As DataRow In ds.Tables(0).Rows
                retVal.Add(ConvertDataRowToCCMSPersonnelData(dr))
            Next
        End If
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToCCMSPersonnelData(ByVal dr As DataRow) As CCMSPersonnelData
        Dim retVal As CCMSPersonnelData = New CCMSPersonnelData()
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        retVal.Email = IIf(dalHelper.DoesDbColumnExist(dr, "Email"), dr("Email").ToString(), String.Empty)
        Return retVal
    End Function
End Class