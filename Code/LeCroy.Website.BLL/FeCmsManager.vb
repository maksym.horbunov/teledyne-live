﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.DAL.Common.BaseClasses
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.Domain.Common.DTOs.Specialized

Public Class FeCmsManager
    Public Shared Function GetHomePageRotators(ByVal localeId As Int32, ByVal fallBackLocaleId As Int32) As IList(Of HomePageRotator)
        Dim sqlString As String = "SELECT DISTINCT * FROM [HOMEPAGE_ROTATORS] WHERE [DISPLAY_YN] = 'Y' AND [LOCALEID] IN (@LOCALEID, @FALLBACKLOCALEID)"
        Dim sqlParameters As List(Of SqlParameter) = New DALHelperUtils().BuildSqlParameterList(New KeyValuePair(Of String, String)("@LOCALEID", localeId.ToString()), New KeyValuePair(Of String, String)("@FALLBACKLOCALEID", fallBackLocaleId.ToString()))
        Return HomePageRotatorRepository.FetchHomePageRotators(sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetHomePageRotatorMediaTabs(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32) As IList(Of HomePageRotator)
        Return (From c In content
                Where c.RotatorTypeId = New Guid("827ce1bf-17b0-44ec-9c7b-b6702e87896f") And c.SortId >= 0 And c.LocaleId = localeId
                Order By c.SortId
                Select c).ToList()
    End Function

    Public Shared Function GetHomePageRotatorMediaContent(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32) As IList(Of HomePageRotator)
        Return (From c In content
                Where c.RotatorTypeId = New Guid("827ce1bf-17b0-44ec-9c7b-b6702e87896f") And c.SortId >= 0 And c.LocaleId = localeId
                Order By c.SortId
                Select c).ToList()
    End Function

    Public Shared Function GetHomePageRotatorCarouselItems(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32) As IList(Of HomePageRotator)
        Return GetHomePageRotatorCarouselItems(content, localeId, New Guid("eee0961d-8917-40d0-8e16-554bf2a747fa"))
    End Function

    Public Shared Function GetHomePageRotatorCarouselItems(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32, ByVal rotatorTypeId As Guid) As IList(Of HomePageRotator)
        Return (From c In content
                Where c.RotatorTypeId = rotatorTypeId And c.SortId >= 0 And c.LocaleId = localeId
                Order By c.SortId
                Select c).ToList()
    End Function

    Public Shared Function GetHomePageRotatorLearnMore(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32) As HomePageRotator
        Return (From c In content Where c.RotatorTypeId = New Guid("f05aaac7-6cfc-4021-be5c-22cdc1db011f") And c.LocaleId = localeId Select c).FirstOrDefault()
    End Function

    Public Shared Function GetHomePageRotatorHighlight(ByVal content As List(Of HomePageRotator), ByVal localeId As Int32) As HomePageRotatorHighlight
        Dim retVal As HomePageRotatorHighlight = New HomePageRotatorHighlight()
        retVal.Title = (From c In content Where c.RotatorTypeId = New Guid("7a2b600d-06fc-4749-987a-5ce2aba0b9e0") And c.LocaleId = localeId Select c.Title).FirstOrDefault()
        retVal.Description = (From c In content Where c.RotatorTypeId = New Guid("7a2b600d-06fc-4749-987a-5ce2aba0b9e0") And c.LocaleId = localeId Select c.Description).FirstOrDefault()
        retVal.Url = (From c In content Where c.RotatorTypeId = New Guid("7a2b600d-06fc-4749-987a-5ce2aba0b9e0") And c.LocaleId = localeId Select c.Url).FirstOrDefault()
        Return retVal
    End Function
End Class