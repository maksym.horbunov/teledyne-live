﻿Imports System.Text.RegularExpressions
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeContactManager
#Region "Contact"
    Public Shared Function ValidatePasswordIntegrity(ByVal password As String, ByVal confirmPassword As String) As InsertReturnValue(Of Boolean)
        Dim retVal As InsertReturnValue(Of Boolean) = New InsertReturnValue(Of Boolean) With {.Success = True, .ErrorMessages = New List(Of ErrorMessages)()}
        If (String.IsNullOrEmpty(confirmPassword)) Then 'Ensure confirm is filled in
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Confirm password is required"))
            Return retVal
        End If
        If Not (String.Compare(password, confirmPassword, False) = 0) Then  'Ensure password and confirm match
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Password and confirm password do not match"))
            Return retVal
        End If
        If (confirmPassword.Trim().Length < 5 Or confirmPassword.Trim().Length > 21) Then   'Ensure password meets our size qualifications
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Password must be at least 5 characters and no more than 20"))
            Return retVal
        End If

        Dim upperRegexCheck As Regex = New Regex("[A-Z]")
        Dim lowerRegexCheck As Regex = New Regex("[a-z]")
        Dim numberRegexCheck As Regex = New Regex("[0-9]")
        'Dim specialCharsCheck As List(Of String) = New List(Of String)(New String() {"<", ">", "`", "+", """", ":", ";", ",", "."})
        If (upperRegexCheck.Matches(confirmPassword).Count < 1) Then    'Ensure we have X upper characters in the password
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Password needs 1 uppercase character"))
            Return retVal
        End If
        If (lowerRegexCheck.Matches(confirmPassword).Count < 1) Then    'Ensure we have X lower characters in the password
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Password needs 1 lowercase character"))
            Return retVal
        End If
        If (numberRegexCheck.Matches(confirmPassword).Count < 1) Then   'Ensure we have X numbers in the password
            retVal.Success = False
            retVal.ErrorMessages.Add(New ErrorMessages("Password needs 1 number"))
            Return retVal
        End If
        'If (specialCharsCheck.Any(Function(x) confirmPassword.Contains(x))) Then   'Ensure we don't have bad characters in the password
        '    retVal.Success = False
        '    retVal.ErrorMessages.Add(New ErrorMessages(Utilities.LoadI18N("SLBCA0837", localeId)))
        '    Return retVal
        'End If
        Return retVal
    End Function

    Public Shared Sub GenerateUserValidationRequestEmail(ByVal connectionString As String, ByVal userValidationRequestId As Guid, ByVal emailAddress As String, ByVal serverName As String, ByVal localeId As Int32)
        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#BaseUrl#>", serverName)
        replacements.Add("<#ValidationUrl#>", String.Format("{0}/support/user/validateregistration.aspx?ticketid={1}", serverName, userValidationRequestId.ToString()))
        FeMailManager.Send_EmailWithReplacementParams(connectionString, 4, localeId, emailAddress, replacements)
    End Sub

    Public Shared Sub GenerateUserValidationRequestEmailAsPlainText(ByVal connectionString As String, ByVal userValidationRequestId As Guid, ByVal emailAddress As String, ByVal serverName As String, ByVal localeId As Int32)
        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#BaseUrl#>", serverName)
        replacements.Add("<#ValidationUrl#>", String.Format("{0}/support/user/validateregistration.aspx?ticketid={1}", serverName, userValidationRequestId.ToString()))
        replacements.Add("<#NewLine#>", Environment.NewLine)
        FeMailManager.Send_EmailWithReplacementParamsForPlainText(connectionString, 4, localeId, emailAddress, replacements)
    End Sub
#End Region

#Region "ContactOffice"
    Public Shared Function GetContactOfficesForProductSeries(ByVal connectionString As String, ByVal productSeriesId As Int32, ByVal officeType As String) As IList(Of ContactOffice)
        Dim contactOfficeIdsForSeries As List(Of Int32) = ContactOfficeXRefProductRepository.GetContactOfficeIdsForProductSeries(connectionString, productSeriesId)
        If (contactOfficeIdsForSeries.Count > 0) Then
            Return ContactOfficeRepository.GetContactOfficesForType(connectionString, contactOfficeIdsForSeries, officeType)
        End If
        Return New List(Of ContactOffice)
    End Function

    Public Shared Function FetchExtendedCountries(ByVal countries As List(Of Country), ByVal contactOffices As List(Of ContactOffice), ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct), ByVal regionCountryDictionary As Dictionary(Of String, ExtendedContactOffice)) As IList(Of ExtendedCountry)
        Dim retVal As List(Of ExtendedCountry) = New List(Of ExtendedCountry)
        For Each country As Country In countries
            retVal.Add(ConvertDataRowToExtendedCountry(country, contactOffices, contactOfficeXRefProducts, regionCountryDictionary))
        Next
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToExtendedCountry(ByVal country As Country, ByVal contactOffices As List(Of ContactOffice), ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct), ByVal regionCountryDictionary As Dictionary(Of String, ExtendedContactOffice)) As ExtendedCountry
        Dim retVal As ExtendedCountry = New ExtendedCountry
        retVal.CountryId = country.CountryId
        retVal.Name = country.Name
        retVal.RegionId = country.RegionId
        If (country.SortId.HasValue) Then
            retVal.SortId = country.SortId.Value
        Else
            retVal.SortId = 9999
        End If

        Dim contactOfficesTemp As List(Of ExtendedContactOffice) = FetchContactOffices(country.CountryId, contactOffices, contactOfficeXRefProducts)
        Dim countAdjustment As Int32 = contactOfficesTemp.Where(Function(x) x.Type.ToUpper() = "S").Count() 'S-Offices are added first, inject after them but before the real D-Offices start
        Dim d2 As Dictionary(Of String, ExtendedContactOffice) = regionCountryDictionary.Where(Function(k) k.Key.StartsWith(String.Format("{0}|", retVal.RegionId))).ToDictionary(Function(x) x.Key, Function(x) x.Value)
        Dim ide0 As IDictionaryEnumerator = d2.GetEnumerator()
        While (ide0.MoveNext())
            Dim hasThisGlobalDistributor As Boolean = contactOfficesTemp.Where(Function(x) x.Company.ToUpper() = CType(ide0.Value, ExtendedContactOffice).Company.ToUpper()).Count() > 0
            If Not (hasThisGlobalDistributor) Then
                Dim splitter As String() = ide0.Key.ToString().Split("|")
                Dim officeId As Int32 = Int32.Parse(splitter(1))
                Dim extendedContactOffices As List(Of ExtendedContactOffice) = New List(Of ExtendedContactOffice)({regionCountryDictionary(String.Format("{0}|{1}", retVal.RegionId, officeId))})
                If (contactOfficesTemp.Count <= 0) Then
                    contactOfficesTemp.Add(extendedContactOffices.FirstOrDefault())
                Else
                    contactOfficesTemp.Add(extendedContactOffices.FirstOrDefault())
                    'contactOfficesTemp.Insert(countAdjustment, extendedContactOffices.FirstOrDefault())
                End If
                countAdjustment += 1
            End If
        End While
        retVal.ContactOffices = contactOfficesTemp
        Return retVal
    End Function

    Private Shared Function FetchContactOffices(ByVal countryId As Int32, ByVal contactOffices As List(Of ContactOffice), ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct)) As IList(Of ExtendedContactOffice)
        Dim retVal As List(Of ExtendedContactOffice) = New List(Of ExtendedContactOffice)
        For Each contactOffice As ContactOffice In contactOffices.Where(Function(x) x.CountryIdWeb = countryId And x.ActiveYN.ToUpper() = "Y").OrderByDescending(Function(x) x.Type).ThenBy(Function(x) x.Company)  '.ThenBy(Function(x) x.SortId.Value)
            retVal.Add(ConvertDataRowToExtendedContactOffice(contactOffice, contactOfficeXRefProducts))
        Next
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToExtendedContactOffice(ByVal contactOffice As ContactOffice, ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct)) As ExtendedContactOffice
        Dim retVal As ExtendedContactOffice = New ExtendedContactOffice
        retVal.OfficeId = contactOffice.OfficeId
        retVal.Region = contactOffice.Region
        retVal.Type = contactOffice.Type
        retVal.Company = contactOffice.Company
        retVal.CompanySubHead = contactOffice.CompanySubHead
        retVal.Address1 = contactOffice.Address1
        retVal.Address2 = contactOffice.Address2
        retVal.Address3 = contactOffice.Address3
        retVal.Address4 = contactOffice.Address4
        retVal.Address5 = contactOffice.Address5
        retVal.Phone = contactOffice.Phone
        retVal.PhoneService = contactOffice.PhoneService
        retVal.Fax = contactOffice.Fax
        retVal.SalesFax = contactOffice.SalesFax
        retVal.FaxService = contactOffice.FaxService
        retVal.Email = contactOffice.Email
        retVal.EmailService = contactOffice.EmailService
        retVal.EmailSupport = contactOffice.EmailSupport
        retVal.Web = contactOffice.Web
        retVal.PhoneSupport = contactOffice.PhoneSupport
        If (contactOffice.SortId.HasValue) Then
            retVal.SortId = contactOffice.SortId.Value
        Else
            retVal.SortId = 9999
        End If
        retVal.IsFeatured = contactOffice.IsFeatured
        retVal.Products = contactOfficeXRefProducts.Where(Function(x) x.ContactOfficeFkId = contactOffice.OfficeId).ToList()
        Return retVal
    End Function

    Public Shared Function FetchContactOfficeXRefProducts(ByVal connectionString As String, ByVal contactOfficeXRefProducts As List(Of ContactOfficeXRefProduct)) As IList(Of ExtendedContactOfficeXRefProduct)
        Dim categoryData As List(Of Category) = CategoryRepository.GetAllCategories(connectionString)
        Dim productSeriesData As List(Of ProductSeries) = CleanProductSeriesDataSortIds(ProductSeriesRepository.GetAllActiveProductSeries(connectionString)).ToList()
        Dim protocolStandardData As List(Of ProtocolStandard) = ProtocolStandardRepository.GetAllProtocolStandards(connectionString).ToList()
        Dim protocolStandardSeriesData As List(Of ProtocolStandardSeries) = ProtocolStandardSeriesRepository.GetAllProtocolStandardSeries(connectionString).ToList()
        Dim retVal As List(Of ExtendedContactOfficeXRefProduct) = New List(Of ExtendedContactOfficeXRefProduct)
        For Each contactOfficeXRefProduct As ContactOfficeXRefProduct In contactOfficeXRefProducts
            If (productSeriesData.Where(Function(x) x.ProductSeriesId = contactOfficeXRefProduct.ProductSeriesFkId.Value).Count() > 0) Then
                If (contactOfficeXRefProduct.CategoryFkId.HasValue) Then
                    If (contactOfficeXRefProduct.CategoryFkId.Value = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS) Then
                        Dim protocolStandardSeries As List(Of ProtocolStandardSeries) = protocolStandardSeriesData.Where(Function(x) x.ProductSeriesId = contactOfficeXRefProduct.ProductSeriesFkId.Value).ToList()
                        If (protocolStandardSeries.Count > 1) Then
                            For Each pss In protocolStandardSeries
                                Dim hack As List(Of ProtocolStandardSeries) = New List(Of ProtocolStandardSeries)
                                hack.Add(pss)
                                retVal.Add(ConvertDataRowToExtendedContactOfficeXRefProduct(contactOfficeXRefProduct, categoryData, productSeriesData, hack, protocolStandardData))
                            Next
                        Else
                            retVal.Add(ConvertDataRowToExtendedContactOfficeXRefProduct(contactOfficeXRefProduct, categoryData, productSeriesData, protocolStandardSeriesData, protocolStandardData))
                        End If
                    Else
                        retVal.Add(ConvertDataRowToExtendedContactOfficeXRefProduct(contactOfficeXRefProduct, categoryData, productSeriesData, protocolStandardSeriesData, protocolStandardData))
                    End If
                Else
                    retVal.Add(ConvertDataRowToExtendedContactOfficeXRefProduct(contactOfficeXRefProduct, categoryData, productSeriesData, protocolStandardSeriesData, protocolStandardData))
                End If
            End If
        Next
        Return retVal.ToList().OrderBy(Function(x) x.SortId).ThenBy(Function(x) x.ProductSeriesName).ToList()
    End Function

    Private Shared Function ConvertDataRowToExtendedContactOfficeXRefProduct(ByVal contactOfficeXRefProduct As ContactOfficeXRefProduct, ByVal categoryData As List(Of Category), ByVal productSeriesData As List(Of ProductSeries), ByVal protocolStandardSeriesData As List(Of ProtocolStandardSeries), ByVal protocolStandardData As List(Of ProtocolStandard)) As ExtendedContactOfficeXRefProduct
        Dim retVal As ExtendedContactOfficeXRefProduct = New ExtendedContactOfficeXRefProduct
        retVal.ContactOfficeXRefProductId = contactOfficeXRefProduct.ContactOfficeXRefProductId
        retVal.ContactOfficeFkId = contactOfficeXRefProduct.ContactOfficeFkId
        If (contactOfficeXRefProduct.CategoryFkId.HasValue) Then
            retVal.CategoryFkId = contactOfficeXRefProduct.CategoryFkId.Value
            ' HACK: Monumentally large hack...
            Dim categoryName As String = categoryData.Where(Function(x) x.CategoryId = contactOfficeXRefProduct.CategoryFkId.Value).Select(Function(x) x.CategoryName).FirstOrDefault()
            If (String.Compare(categoryName, "Protocol Analyzers", True) = 0) Then
                retVal.CategoryName = categoryName
            Else
                retVal.CategoryName = "Oscilloscopes"
            End If
            retVal.SortId = 9999
        End If
        If (contactOfficeXRefProduct.ProductGroupFkId.HasValue) Then
            retVal.ProductGroupFkId = contactOfficeXRefProduct.ProductGroupFkId.Value
            retVal.SortId = 9999
        End If
        If (contactOfficeXRefProduct.ProductSeriesFkId.HasValue) Then
            retVal.ProductSeriesFkId = contactOfficeXRefProduct.ProductSeriesFkId.Value
            retVal.ProductSeriesName = productSeriesData.Where(Function(x) x.ProductSeriesId = contactOfficeXRefProduct.ProductSeriesFkId.Value).Select(Function(x) x.Name).FirstOrDefault()
            retVal.SortId = productSeriesData.Where(Function(x) x.ProductSeriesId = contactOfficeXRefProduct.ProductSeriesFkId.Value).Select(Function(x) x.SortId.Value).FirstOrDefault()
        End If
        If (contactOfficeXRefProduct.ProductFkId.HasValue) Then
            retVal.ProductFkId = contactOfficeXRefProduct.ProductFkId.Value
            retVal.SortId = 9999
        End If
        If (contactOfficeXRefProduct.ProtocolStandardFkId.HasValue) Then
            retVal.ProtocolStandardFkId = contactOfficeXRefProduct.ProtocolStandardFkId.Value
            retVal.ProtocolStandardName = protocolStandardData.Where(Function(x) x.ProtocolStandardId = contactOfficeXRefProduct.ProtocolStandardFkId.Value).Select(Function(x) x.Name).FirstOrDefault()
            retVal.SortId = 9999
        ElseIf (contactOfficeXRefProduct.CategoryFkId.HasValue) Then
            If (contactOfficeXRefProduct.CategoryFkId.Value = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS) Then
                Dim protocolStandardSeries As ProtocolStandardSeries = protocolStandardSeriesData.Where(Function(x) x.ProductSeriesId = contactOfficeXRefProduct.ProductSeriesFkId.Value).FirstOrDefault()
                If Not (protocolStandardSeries Is Nothing) Then
                    retVal.ProtocolStandardFkId = protocolStandardSeries.ProtocolStandardId
                    retVal.ProtocolStandardName = protocolStandardData.Where(Function(x) x.ProtocolStandardId = protocolStandardSeries.ProtocolStandardId).Select(Function(x) x.Name).FirstOrDefault()
                    retVal.SortId = 9999
                End If
            End If
        End If
        retVal.RowGuid = contactOfficeXRefProduct.RowGuid
        Return retVal
    End Function

    Public Shared Function GetDefaultContactOffice(ByVal contactOfficeId As Int32, ByVal contactOffices As List(Of ContactOffice), ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct)) As ExtendedContactOffice
        Dim retVal As List(Of ExtendedContactOffice) = New List(Of ExtendedContactOffice)
        For Each contactOffice As ContactOffice In contactOffices.Where(Function(x) x.OfficeId = contactOfficeId And x.ActiveYN.ToUpper() = "Y").ToList()
            retVal.Add(ConvertDataRowToExtendedContactOffice(contactOffice, contactOfficeXRefProducts))
        Next
        Return retVal.FirstOrDefault()
    End Function

    Public Shared Function IsProductSeriesIdInSuppliedString(ByVal lookForThisSeriesId As Int32, ByVal seriesIdString As String) As Boolean
        Dim retVal As Boolean = False
        Dim split As List(Of String) = seriesIdString.Split(",").ToList()
        If (split.Count > 0) Then
            Return split.Contains(lookForThisSeriesId)
        End If
        Return retVal
    End Function

    Private Shared Function CleanProductSeriesDataSortIds(ByVal sourceProductSeries) As List(Of ProductSeries)
        Dim retVal As List(Of ProductSeries) = New List(Of ProductSeries)
        For Each ps As ProductSeries In sourceProductSeries
            Dim newPs As ProductSeries = ps
            If Not (newPs.SortId.HasValue) Then
                newPs.SortId = 9999
            End If
            retVal.Add(newPs)
        Next
        Return retVal
    End Function
#End Region
End Class

Public Class ExtendedCountry
    Inherits Country
    Public ContactOffices As List(Of ExtendedContactOffice)
End Class

Public Class ExtendedContactOffice
    Inherits ContactOffice
    Public Products As List(Of ExtendedContactOfficeXRefProduct)
End Class

Public Class ExtendedContactOfficeXRefProduct
    Inherits ContactOfficeXRefProduct
    Public CategoryName As String
    Public ProductSeriesName As String
    Public ProtocolStandardName As String
    Public SortId As Int32
End Class