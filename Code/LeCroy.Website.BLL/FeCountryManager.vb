﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeCountryManager
    Public Shared Function GetRegionForCountryId(ByVal connectionString As String, ByVal countryId As Int32) As Int32
        Dim country As Country = CountryRepository.GetCountry(connectionString, countryId)
        If (country Is Nothing) Then
            Return 0
        End If
        Return country.RegionId
    End Function
End Class