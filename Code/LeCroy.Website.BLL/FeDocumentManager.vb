﻿Imports System.Data.SqlClient
Imports System.Text
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeDocumentManager
    Public Shared Function GetDocumentRedirectUrl(ByVal documentId As Int32, ByVal documentTypeId As Int32) As String
        Select Case documentTypeId
            Case 1, 2, 3, 4, 5, 6, 7, 17, 19, 21, 23, 26, 27, 29 'App, Manuals, Seminars, RN, WP, SIF, DiscoManuals, DataSheets, Accessories, Conference Proceedings, Tutorials, Specs, Comparisons
                Return String.Format("~/doc/docview.aspx?id={0}", documentId)
            Case 8  'Programming Examples
                Return String.Format("~/support/techlib/programmingexamples.aspx?type={0}&docid={1}", documentTypeId, documentId)
            Case 9, 10, 11, 12, 13, 14, 15, 16, 20, 24, 25 'SW, API, Firmware, Utilities, Pert, Arb, Logic, Sparq, SI, FlexNet, WaveStation
                Return String.Format("~/support/softwaredownload/download.aspx?did={0}", documentId)
            Case 18 'Videos
                Return String.Format("~/support/techlib/videos.aspx?capid=106&mid=528&smid=662&docid={0}", documentId)
            Case 22 'Corporate Presentations
                Return String.Empty
        End Select
        Return String.Empty
    End Function

#Region "Document"
    Public Shared Function GetDocumentsForGroup(ByVal connectionString As String, ByVal docGroupId As Int32) As IList(Of Document)
        Dim sqlString As String = "SELECT [DOCUMENT].DOCUMENT_ID, [DOCUMENT].DOC_TYPE_ID, [DOCUMENT].TITLE, [DOCUMENT].DESCRIPTION, [DOCUMENT].FILE_FULL_PATH, [DOCUMENT].DOC_SIZE, [DOCUMENT].IMAGE FROM DOCUMENT_GROUP_XREF INNER JOIN [DOCUMENT] ON DOCUMENT_GROUP_XREF.DOCUMENT_ID = [DOCUMENT].DOCUMENT_ID WHERE DOCUMENT_GROUP_XREF.DOC_GROUP_ID = @DOCGROUPID AND [DOCUMENT].POST = 'Y' ORDER BY [DOCUMENT].SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@DOCGROUPID", docGroupId.ToString())})
        Return PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, sqlString, sqlParameters.ToArray()))
    End Function

    Public Shared Function GetDefaultDocumentForDocumentGroup(ByVal connectionString As String, ByVal docGroupId As Int32) As Document
        Dim sqlString As String = "SELECT [DOCUMENT].DOCUMENT_ID, [DOCUMENT].DOC_TYPE_ID, [DOCUMENT].TITLE, [DOCUMENT].DESCRIPTION, [DOCUMENT].FILE_FULL_PATH, [DOCUMENT].DOC_SIZE, [DOCUMENT].IMAGE FROM DOCUMENT_GROUP_XREF INNER JOIN [DOCUMENT] ON DOCUMENT_GROUP_XREF.DOCUMENT_ID = [DOCUMENT].DOCUMENT_ID WHERE DOCUMENT_GROUP_XREF.DOC_GROUP_ID = @DOCGROUPID AND DOCUMENT_GROUP_XREF.DEFAULT_YN = 'Y' ORDER BY [DOCUMENT].SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@DOCGROUPID", docGroupId.ToString())})
        Dim documents As List(Of Document) = PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, sqlString, sqlParameters.ToArray()))
        If (documents.Count > 0) Then
            Return documents(0)
        End If
        Return New Document()
    End Function

    Public Shared Function GetDefaultDocumentForDocumentType(ByVal connectionString As String, ByVal defaultDocumentId As Int32, ByVal docTypeId As Int32) As Document
        ' Try to get requested document via docid
        Dim sqlString As String = "SELECT [DOCUMENT].DOCUMENT_ID, [DOCUMENT].DOC_TYPE_ID, [DOCUMENT].TITLE, [DOCUMENT].DESCRIPTION, [DOCUMENT].FILE_FULL_PATH, [DOCUMENT].DOC_SIZE, [DOCUMENT].IMAGE FROM [DOCUMENT] WHERE [DOCUMENT].DOC_TYPE_ID = @DOCTYPEID AND [DOCUMENT].DOCUMENT_ID = @DEFAULTDOCUMENTID AND [DOCUMENT].POST = 'Y'"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", docTypeId.ToString()))
        sqlParameters.Add(New SqlParameter("@DEFAULTDOCUMENTID", defaultDocumentId.ToString()))
        Dim documents As List(Of Document) = PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, sqlString, sqlParameters.ToArray()))
        If (documents.Count > 0) Then
            Return documents(0)
        End If

        ' Try to get default document of this type
        Dim fetchedDefaultDocumentId As Nullable(Of Int32) = GetDefaultDocumentIdForDocumentType(docTypeId)
        If (fetchedDefaultDocumentId.HasValue) Then
            sqlString = "SELECT [DOCUMENT].DOCUMENT_ID, [DOCUMENT].DOC_TYPE_ID, [DOCUMENT].TITLE, [DOCUMENT].DESCRIPTION, [DOCUMENT].FILE_FULL_PATH, [DOCUMENT].DOC_SIZE, [DOCUMENT].IMAGE FROM [DOCUMENT] WHERE [DOCUMENT].DOC_TYPE_ID = @DOCTYPEID AND [DOCUMENT].DOCUMENT_ID = @DEFAULTDOCUMENTID AND [DOCUMENT].POST = 'Y'"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCTYPEID", docTypeId.ToString()))
            sqlParameters.Add(New SqlParameter("@DEFAULTDOCUMENTID", defaultDocumentId.ToString()))
            documents = PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, sqlString, sqlParameters.ToArray()))
            If (documents.Count > 0) Then
                Return documents(0)
            End If
        End If

        ' Try to get something...
        sqlString = "SELECT TOP 1 [DOCUMENT].DOCUMENT_ID, [DOCUMENT].DOC_TYPE_ID, [DOCUMENT].TITLE, [DOCUMENT].DESCRIPTION, [DOCUMENT].FILE_FULL_PATH, [DOCUMENT].DOC_SIZE, [DOCUMENT].IMAGE FROM [DOCUMENT] WHERE [DOCUMENT].DOC_TYPE_ID = @DOCTYPEID AND [DOCUMENT].POST = 'Y'"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", docTypeId.ToString()))
        documents = PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, sqlString, sqlParameters.ToArray()))
        If (documents.Count > 0) Then
            Return documents(0)
        End If
        Return New Document
    End Function

    Public Shared Function GetDocuments(ByVal connectionString As String, ByVal docTypeId As Int32, ByVal categoryId As Nullable(Of Int32), ByVal productSeriesId As Nullable(Of Int32), ByVal productGroupId As Nullable(Of Int32), ByVal protocolStandardId As Nullable(Of Int32), ByVal productId As Nullable(Of Int32)) As IList(Of Document)
        Dim protocolAlreadyAdded As Boolean = False
        Dim sqlString As String = "SELECT DISTINCT d.* " &
                                        "FROM DOCUMENT_CATEGORY AS dc INNER JOIN [DOCUMENT] AS d ON dc.DOCUMENT_ID = d.DOCUMENT_ID " &
                                        "WHERE d.POST = 'Y' AND d.DOC_TYPE_ID = @DOCTYPEID {0} ORDER BY d.SORT_ID"
        Dim paramsSet As Boolean = False
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", docTypeId.ToString()))
        Dim sb As StringBuilder = New StringBuilder
        If (productSeriesId.HasValue) Then
            sb.Append("AND dc.PRODUCT_SERIES_ID = @PRODUCTSERIESID ")
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
            If (protocolStandardId.HasValue) Then
                sb.Append("AND dc.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID ")
                sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.Value.ToString()))
                protocolAlreadyAdded = True
            End If
            paramsSet = True
        End If
        If (productGroupId.HasValue) Then
            If Not (productGroupId.Value = 88) Then
                sb.Append("AND dc.GROUP_ID = @PRODUCTGROUPID ")
                sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", productGroupId.Value.ToString()))
                paramsSet = True
            Else
                Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(connectionString, productGroupId.Value)
                If Not (productGroup Is Nothing) Then
                    categoryId = productGroup.CategoryId
                End If
            End If
        End If
        If (protocolStandardId.HasValue And Not protocolAlreadyAdded) Then
            sb.Append("AND dc.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID ")
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.Value.ToString()))
            paramsSet = True
        End If
        If (categoryId.HasValue) Then
            If (categoryId.Value = 12 And productGroupId.HasValue) Then
                sqlParameters = New List(Of SqlParameter)
                If (productSeriesId.HasValue) Then
                    sqlString = "SELECT DISTINCT d.* FROM DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt " &
                            " WHERE dc.document_id = d.document_id AND d.doc_type_id = dt.DOC_TYPE_ID AND d.doc_type_id = @DOCTYPEID AND dc.CATEGORY_ID = 12 AND " &
                            " d.post = 'y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx " &
                            " WHERE psc.CATEGORY_ID = 12 AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID) ORDER BY d.SORT_ID"
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
                Else
                    sqlString = "SELECT DISTINCT d.* FROM DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt " &
                            " WHERE dc.document_id = d.document_id AND d.doc_type_id = dt.DOC_TYPE_ID AND d.doc_type_id = @DOCTYPEID AND dc.CATEGORY_ID = 12 AND " &
                            " d.post = 'y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx " &
                            " WHERE psc.CATEGORY_ID = 12 AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPID) ORDER BY d.SORT_ID"
                End If
                sqlParameters.Add(New SqlParameter("@DOCTYPEID", docTypeId.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", productGroupId.Value.ToString()))
            Else
                sb.Append("AND dc.CATEGORY_ID = @CATEGORYID ")
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
            End If
            paramsSet = True
        End If
        If (productId.HasValue) Then
            sb.Append("AND dc.PRODUCT_ID = @PRODUCTID ")
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.Value.ToString()))
            paramsSet = True
        End If
        If Not (paramsSet) Then
            Return New List(Of Document)
        End If
        Return PrepareDocumentDTOs(DocumentRepository.FetchDocuments(connectionString, String.Format(sqlString, sb.ToString()), sqlParameters.ToArray()).ToList())
    End Function

    Public Shared Function GetDocumentCount(ByVal connectionString As String, ByVal documentId As Int32, ByVal categoryId As Int32, ByVal subCategoryId As Int32, ByVal productSeriesId As Int32) As Int32
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentId.ToString()))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", subCategoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Dim documentCategories As List(Of DocumentCategory) = DocumentCategoryRepository.FetchDocumentCategories(connectionString, "SELECT DISTINCT d.PRODUCT_ID FROM DOCUMENT a INNER JOIN DOCUMENT_CATEGORY AS d ON a.DOCUMENT_ID = d.DOCUMENT_ID INNER JOIN PRODUCT AS f ON d.PRODUCT_ID = f.PRODUCTID INNER JOIN PRODUCT_SERIES_CATEGORY e ON f.PRODUCTID = e.PRODUCT_ID INNER JOIN PRODUCT_SERIES b ON e.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN SUBCATEGORY c ON b.SUBCAT_ID = c.SUBCAT_ID WHERE a.POST='y' AND d.DOCUMENT_ID = @DOCUMENTID AND b.DISABLED = 'n' AND e.CATEGORY_ID = @CATEGORYID AND c.SUBCAT_ID = @SUBCATEGORYID AND e.PRODUCT_SERIES_ID = @PRODUCTSERIESID", sqlParameters.ToArray())
        Return documentCategories.Count()
    End Function

    Public Shared Function GetDocumentsForSubCategory(ByVal connectionString As String, ByVal subCategoryId As Int32, ByVal productSeriesId As Nullable(Of Int32)) As List(Of Document)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", subCategoryId.ToString()))
        If (productSeriesId.HasValue) Then
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
            Return DocumentRepository.FetchDocuments(connectionString, "SELECT distinct DOCUMENT.TITLE ,DOCUMENT.DOC_TYPE_ID, DOCUMENT.DOCUMENT_ID, DOCUMENT.DESCRIPTION, DOCUMENT.FILE_FULL_PATH, DOCUMENT.VERSION, DOCUMENT.DOC_SIZE, DOCUMENT.COMMENTS, DOCUMENT.OBTAINKEY_YN, DOCUMENT.AGREEMENT_YN, '' as flgPassword, DOCUMENT.SORT_ID, DOCUMENT.DATE_ENTERED FROM PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID INNER JOIN PRODUCT ON DOCUMENT_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID WHERE (DOCUMENT.POST ='y') AND (DOCUMENT.DOC_TYPE_ID = 11) AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 1) and SUBCATEGORY.SUBCAT_ID =@SUBCATEGORYID AND PRODUCT_SERIES.PRODUCT_SERIES_ID = @PRODUCTSERIESID ORDER BY [DOCUMENT].SORT_ID", sqlParameters.ToArray()) 'AND (PRODUCT_SERIES.DISABLED = 'n') 
        Else
            Dim subCategory As Subcategory = SubcategoryRepository.GetSubcategory(connectionString, subCategoryId)
            If Not (subCategory Is Nothing) Then
                If (subCategory.CategoryId = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
                    If (subCategoryId = 23) Then ' HACK: No other way to do this...
                        Return DocumentRepository.FetchDocuments(connectionString, "SELECT DISTINCT d.* FROM [DOCUMENT_CATEGORY] dc, [DOCUMENT] d WHERE dc.CATEGORY_ID = 2 AND dc.PRODUCT_SERIES_ID = 276 AND dc.DOCUMENT_ID = d.DOCUMENT_ID AND d.DOC_TYPE_ID = 25 AND d.POST IN ('Y','D')", sqlParameters.ToArray())
                    Else    ' NOTE: Not sure why 441 is included in the select list, leaving as is as to not break anything
                        Return DocumentRepository.FetchDocuments(connectionString, "SELECT DISTINCT d.* FROM [DOCUMENT_CATEGORY] dc, [DOCUMENT] d WHERE dc.CATEGORY_ID = 2 AND dc.PRODUCT_SERIES_ID IN (399,441) AND dc.DOCUMENT_ID = d.DOCUMENT_ID AND d.DOC_TYPE_ID = 25 AND d.POST IN ('Y','D')", sqlParameters.ToArray())
                    End If
                End If
            End If
            Return DocumentRepository.FetchDocuments(connectionString, "SELECT distinct DOCUMENT.TITLE ,DOCUMENT.DOC_TYPE_ID, DOCUMENT.DOCUMENT_ID, DOCUMENT.DESCRIPTION, DOCUMENT.FILE_FULL_PATH, DOCUMENT.VERSION, DOCUMENT.DOC_SIZE, DOCUMENT.COMMENTS, DOCUMENT.OBTAINKEY_YN, DOCUMENT.AGREEMENT_YN, '' as flgPassword, DOCUMENT.SORT_ID, DOCUMENT.DATE_ENTERED FROM PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID INNER JOIN PRODUCT ON DOCUMENT_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID WHERE (DOCUMENT.POST ='y') AND (DOCUMENT.DOC_TYPE_ID = 11) AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 1) and SUBCATEGORY.SUBCAT_ID =@SUBCATEGORYID ORDER BY [DOCUMENT].SORT_ID", sqlParameters.ToArray()) 'AND (PRODUCT_SERIES.DISABLED = 'n') 
        End If
    End Function

    ' Just to force values for sorting
    Private Shared Function PrepareDocumentDTOs(ByVal documents As List(Of Document)) As List(Of Document)
        Dim retVal As List(Of Document) = New List(Of Document)
        For Each d As Document In documents
            retVal.Add(PrepareDocumentDTO(d))
        Next
        Return retVal
    End Function

    ' Just to force values for sorting
    Private Shared Function PrepareDocumentDTO(ByVal document As Document) As Document
        Dim retVal As Document = document
        If Not (retVal.SortId.HasValue) Then
            retVal.SortId = 9999
        End If
        If Not (retVal.CountryId.HasValue) Then
            retVal.CountryId = 208
        End If
        Return retVal
    End Function
#End Region

#Region "DocumentFeatured"
    Public Shared Function GetDocumentFeaturedForProductGroup(ByVal connectionString As String, ByVal productGroupId As Int32) As IList(Of ExtendedDocumentFeatured)
        Dim sqlString As String = "SELECT * FROM [DOCUMENT_FEATURED] INNER JOIN [DOCUMENT] ON DOCUMENT_FEATURED.DOCUMENTFKID = DOCUMENT.DOCUMENT_ID WHERE DOCUMENT_FEATURED.PRODUCTGROUPFKID = @PRODUCTGROUPID AND DOCUMENT.POST = 'Y' ORDER BY DOCUMENT_FEATURED.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", productGroupId.ToString()))
        Return DocumentFeaturedRepository.FetchExtendedDocumentFeatured(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetDocumentFeaturedForProtocolStandard(ByVal connectionString As String, ByVal protocolStandardId As Int32) As IList(Of ExtendedDocumentFeatured)
        Dim sqlString As String = "SELECT * FROM [DOCUMENT_FEATURED] INNER JOIN [DOCUMENT] ON DOCUMENT_FEATURED.DOCUMENTFKID = DOCUMENT.DOCUMENT_ID WHERE DOCUMENT_FEATURED.PROTOCOLSTANDARDFKID = @PROTOCOLSTANDARDID AND DOCUMENT.POST = 'Y' ORDER BY DOCUMENT_FEATURED.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.ToString()))
        Return DocumentFeaturedRepository.FetchExtendedDocumentFeatured(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetDocumentFeaturedForProductSeries(ByVal connectionString As String, ByVal productSeriesId As Int32) As IList(Of ExtendedDocumentFeatured)
        Dim sqlString As String = "SELECT * FROM [DOCUMENT_FEATURED] INNER JOIN [DOCUMENT] ON DOCUMENT_FEATURED.DOCUMENTFKID = DOCUMENT.DOCUMENT_ID WHERE DOCUMENT_FEATURED.PRODUCTSERIESFKID = @PRODUCTSERIESID AND DOCUMENT.POST = 'Y' ORDER BY DOCUMENT_FEATURED.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Return DocumentFeaturedRepository.FetchExtendedDocumentFeatured(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetDocumentFeaturedForCategory(ByVal connectionString As String, ByVal categoryId As Int32) As IList(Of ExtendedDocumentFeatured)
        Dim sqlString As String = "SELECT * FROM [DOCUMENT_FEATURED] INNER JOIN [DOCUMENT] ON DOCUMENT_FEATURED.DOCUMENTFKID = DOCUMENT.DOCUMENT_ID WHERE DOCUMENT_FEATURED.CATEGORYFKID = @CATEGORYID AND DOCUMENT.POST = 'Y' ORDER BY DOCUMENT_FEATURED.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        Return DocumentFeaturedRepository.FetchExtendedDocumentFeatured(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetDocumentFeaturedForProduct(ByVal connectionString As String, ByVal productId As Int32) As IList(Of ExtendedDocumentFeatured)
        Dim sqlString As String = "SELECT * FROM [DOCUMENT_FEATURED] INNER JOIN [DOCUMENT] ON DOCUMENT_FEATURED.DOCUMENTFKID = DOCUMENT.DOCUMENT_ID WHERE DOCUMENT_FEATURED.PRODUCTFKID = @PRODUCTID AND DOCUMENT.POST = 'Y' ORDER BY DOCUMENT_FEATURED.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
        Return DocumentFeaturedRepository.FetchExtendedDocumentFeatured(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function
#End Region

#Region "DocumentGroupXRefCount"
    Public Shared Function GetDocumentGroupVideoCount(ByVal connectionString As String) As Dictionary(Of Int32, Int32)
        Dim retVal As Dictionary(Of Int32, Int32) = New Dictionary(Of Int32, Int32)
        Dim sqlString As String = "SELECT [DOCUMENT_GROUP_XREF].DOC_GROUP_ID, COUNT([DOCUMENT].DOCUMENT_ID) AS [COUNT] FROM DOCUMENT_GROUP_XREF INNER JOIN [DOCUMENT] ON DOCUMENT_GROUP_XREF.DOCUMENT_ID = [DOCUMENT].DOCUMENT_ID WHERE DOCUMENT_GROUP_XREF.DOC_GROUP_ID IN (SELECT DOC_GROUP_ID FROM DOCUMENT_GROUP WHERE ACTIVE_YN = 'Y') AND [DOCUMENT].POST = 'Y' GROUP BY [DOCUMENT_GROUP_XREF].DOC_GROUP_ID"
        Dim ds As DataSet = AnonymousRepository.FetchAnonymousObject(connectionString, sqlString, New List(Of SqlParameter)().ToArray())
        If (ds.Tables.Count > 0) Then
            Dim dataRowsAsObject As List(Of DocumentGroupXRefCount) = New List(Of DocumentGroupXRefCount)
            For Each dr As DataRow In ds.Tables(0).Rows
                dataRowsAsObject.Add(ConvertDataRowToDocumentGroupXRefCount(dr))
            Next

            For Each documentGroup As DocumentGroupXRefCount In dataRowsAsObject
                If Not (retVal.ContainsKey(documentGroup.DocGroupId)) Then
                    retVal.Add(documentGroup.DocGroupId, documentGroup.Count)
                End If
            Next
        End If
        Return retVal
    End Function

    Private Shared Function ConvertDataRowToDocumentGroupXRefCount(ByVal dr As DataRow) As DocumentGroupXRefCount
        Dim retVal As DocumentGroupXRefCount = New DocumentGroupXRefCount
        retVal.DocGroupId = Int32.Parse(dr("DOC_GROUP_ID").ToString())
        retVal.Count = Int32.Parse(dr("COUNT").ToString())
        Return retVal
    End Function
#End Region

#Region "DocumentType"
    Public Shared Function GetDefaultDocumentIdForDocumentType(ByVal connectionString As String) As Nullable(Of Int32)
        Return GetDefaultDocumentIdForDocumentType(connectionString, DocumentTypeIds.VIDEOS)
    End Function

    Public Shared Function GetDefaultDocumentIdForDocumentType(ByVal connectionString As String, ByVal docTypeId As Int32) As Nullable(Of Int32)
        Dim sqlString As String = "SELECT [DOC_TYPE_ID], [DEFAULT_DOC_ID] FROM [DOCUMENT_TYPE] WHERE [DOC_TYPE_ID] = @DOCTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@DOCTYPEID", docTypeId.ToString())})
        Dim documentTypes As List(Of DocumentType) = DocumentTypeRepository.FetchDocumentTypes(connectionString, sqlString, sqlParameters.ToArray())
        If (documentTypes.Count > 0) Then
            Return documentTypes.FirstOrDefault().DefaultDocId
        End If
        Return Nothing
    End Function

    Public Shared Function GetDocumentTypesForDocumentCategoryProperties(ByVal connectionString As String, ByVal categoryId As Nullable(Of Int32), ByVal productSeriesId As Nullable(Of Int32), ByVal productGroupId As Nullable(Of Int32), ByVal protocolStandardId As Nullable(Of Int32), ByVal productId As Nullable(Of Int32)) As IList(Of DocumentType)
        Dim addedProtocolJoin As Boolean = False
        Dim sqlString As String = "SELECT DISTINCT dt.* " &
                                  "FROM DOCUMENT_CATEGORY AS dc INNER JOIN [DOCUMENT] AS d ON dc.DOCUMENT_ID = d.DOCUMENT_ID " &
                                  "INNER JOIN DOCUMENT_TYPE AS dt ON d.DOC_TYPE_ID = dt.DOC_TYPE_ID " &
                                  "WHERE d.POST = 'Y' {0} ORDER BY dt.SORTID"
        Dim paramsSet As Boolean = False
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim sb As StringBuilder = New StringBuilder
        If (productSeriesId.HasValue) Then
            sb.Append("AND dc.PRODUCT_SERIES_ID = @PRODUCTSERIESID ")
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
            If (protocolStandardId.HasValue) Then
                sb.Append("AND dc.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID ")
                sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.Value.ToString()))
                addedProtocolJoin = True
            End If
            paramsSet = True
        End If
        If (productGroupId.HasValue) Then
            sb.Append("AND dc.GROUP_ID = @PRODUCTGROUPID ")
            sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", productGroupId.Value.ToString()))
            paramsSet = True
        End If
        If (protocolStandardId.HasValue And Not addedProtocolJoin) Then
            sb.Append("AND dc.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID ")
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.Value.ToString()))
            paramsSet = True
        End If
        If (categoryId.HasValue) Then
            If (categoryId.Value = 12 And productGroupId.HasValue) Then
                sqlParameters = New List(Of SqlParameter)
                If (productSeriesId.HasValue) Then
                    sqlString = "SELECT DISTINCT dt.* FROM DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt " &
                            " WHERE dc.document_id = d.document_id AND d.doc_type_id = dt.DOC_TYPE_ID AND dc.CATEGORY_ID = 12 AND " &
                            " d.post = 'y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx " &
                            " WHERE psc.CATEGORY_ID = 12 AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID)"
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
                Else
                    sqlString = "SELECT DISTINCT dt.* FROM DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt " &
                            " WHERE dc.document_id = d.document_id AND d.doc_type_id = dt.DOC_TYPE_ID AND dc.CATEGORY_ID = 12 AND " &
                            " d.post = 'y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx " &
                            " WHERE psc.CATEGORY_ID = 12 AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPID)"
                End If
                sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", productGroupId.Value.ToString()))
            Else
                sb.Append("AND dc.CATEGORY_ID = @CATEGORYID ")
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
            End If
            paramsSet = True
        End If
        If (productId.HasValue) Then
            sb.Append("AND dc.PRODUCT_ID = @PRODUCTID ")
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.Value.ToString()))
            paramsSet = True
        End If

        If Not (paramsSet) Then
            Return New List(Of DocumentType)
        End If
        Return DocumentTypeRepository.FetchDocumentTypes(connectionString, String.Format(sqlString, sb.ToString()), sqlParameters.ToArray())
    End Function
#End Region
End Class