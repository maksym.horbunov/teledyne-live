﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeMailManager
    Public Shared Function Send_EmailWithReplacementParams(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressRecipient As String, ByVal replacements As Dictionary(Of String, String)) As Boolean
        Return Send_EmailWithReplacementParams(connectionString, emailTemplateId, localeId, String.Empty, New List(Of String)({emailAddressRecipient}), New List(Of String), New List(Of String), replacements)
    End Function

    Public Shared Function Send_EmailWithReplacementParams(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressFrom As String, ByVal emailAddressRecipients As List(Of String), ByVal emailAddressCarbonCopy As List(Of String), ByVal emailAddressBlindCarbonCopy As List(Of String), ByVal replacements As Dictionary(Of String, String)) As Boolean
        Return Send_EmailWithReplacementParams(connectionString, emailTemplateId, localeId, 2, emailAddressFrom, emailAddressRecipients, emailAddressCarbonCopy, emailAddressBlindCarbonCopy, String.Empty, replacements)
    End Function

    Public Shared Function Send_EmailWithReplacementParams(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailTypeId As Int32, ByVal emailAddressFrom As String, ByVal emailAddressRecipients As List(Of String), ByVal emailAddressCarbonCopy As List(Of String), ByVal emailAddressBlindCarbonCopy As List(Of String), ByVal headerInjection As String, ByVal replacements As Dictionary(Of String, String)) As Boolean
        Dim emailTemplateDetail As EmailTemplateDetail = GetEmailTemplateDetail(connectionString, emailTemplateId, localeId, emailTypeId)
        If (emailTemplateDetail Is Nothing) Then
            Return False
        End If

        Dim emailSubject As String = emailTemplateDetail.SubjectLine
        If (emailTemplateDetail.SubjectLine.Contains("<#") And emailTemplateDetail.SubjectLine.Contains("#>")) Then
            emailSubject = EmailTemplateDetailRepository.BuildEmailSubject(connectionString, emailTemplateId, localeId, replacements)
            If (String.IsNullOrEmpty(emailSubject)) Then
                emailSubject = emailTemplateDetail.SubjectLine
            End If
        End If

        Dim fromAddress As String = emailTemplateDetail.FromAddress
        If Not (String.IsNullOrEmpty(emailAddressFrom)) Then fromAddress = emailAddressFrom

        AppendLayoutElementsToDictionary(emailSubject, replacements)
        Dim emailContent As String = EmailTemplateDetailRepository.BuildEmailContent(connectionString, emailTemplateId, localeId, False, replacements)
        If Not (String.IsNullOrEmpty(emailContent)) Then
            If Not (String.IsNullOrEmpty(headerInjection)) Then
                emailContent = String.Format("{0}{1}{1}{2}", headerInjection, Environment.NewLine, emailContent)
            End If
            Return ProcessEmailRequest(emailAddressRecipients, fromAddress, emailSubject, emailContent, emailAddressCarbonCopy, GetListOfBCCAddresses(emailAddressBlindCarbonCopy), (emailTemplateDetail.EmailType = SubscriptionFormat.HTML))
        End If
        Return False
    End Function

    Public Shared Function Send_EmailWithReplacementParamsForPlainText(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressRecipient As String, ByVal replacements As Dictionary(Of String, String)) As Boolean
        Return Send_EmailWithReplacementParamsForPlainText(connectionString, emailTemplateId, localeId, String.Empty, New List(Of String)({emailAddressRecipient}), New List(Of String), New List(Of String), replacements)
    End Function

    Public Shared Function Send_EmailWithReplacementParamsForPlainText(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressFrom As String, ByVal emailAddressRecipients As List(Of String), ByVal emailAddressCarbonCopy As List(Of String), ByVal emailAddressBlindCarbonCopy As List(Of String), ByVal replacements As Dictionary(Of String, String)) As Boolean
        Dim emailTemplateDetail As EmailTemplateDetail = GetEmailTemplateDetail(connectionString, emailTemplateId, localeId, 1)
        If (emailTemplateDetail Is Nothing) Then
            Return False
        End If

        Dim emailSubject As String = emailTemplateDetail.SubjectLine
        If (emailTemplateDetail.SubjectLine.Contains("<#") And emailTemplateDetail.SubjectLine.Contains("#>")) Then
            emailSubject = EmailTemplateDetailRepository.BuildEmailSubject(connectionString, emailTemplateId, localeId, 1, replacements)
            If (String.IsNullOrEmpty(emailSubject)) Then
                emailSubject = emailTemplateDetail.SubjectLine
            End If
        End If

        Dim fromAddress As String = emailTemplateDetail.FromAddress
        If Not (String.IsNullOrEmpty(emailAddressFrom)) Then fromAddress = emailAddressFrom

        AppendLayoutElementsToDictionary(emailSubject, replacements)
        Dim emailContent As String = EmailTemplateDetailRepository.BuildEmailContentForPlainText(connectionString, emailTemplateId, localeId, False, replacements)
        If Not (String.IsNullOrEmpty(emailContent)) Then
            Return ProcessEmailRequest(emailAddressRecipients, fromAddress, emailSubject, emailContent, emailAddressCarbonCopy, GetListOfBCCAddresses(emailAddressBlindCarbonCopy), False)
        End If
        Return False
    End Function

    Public Shared Function Send_EmailWithNoReplacementParams(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressRecipient As String) As Boolean
        Return Send_EmailWithNoReplacementParams(connectionString, emailTemplateId, localeId, String.Empty, New List(Of String)({emailAddressRecipient}), New List(Of String), New List(Of String))
    End Function

    Public Shared Function Send_EmailWithNoReplacementParams(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailAddressFrom As String, ByVal emailAddressRecipients As List(Of String), ByVal emailAddressCarbonCopy As List(Of String), ByVal emailAddressBlindCarbonCopy As List(Of String)) As Boolean
        Dim emailTemplateDetail As EmailTemplateDetail = GetEmailTemplateDetail(connectionString, emailTemplateId, localeId)
        If (emailTemplateDetail Is Nothing) Then
            Return False
        End If
        Dim fromAddress As String = emailTemplateDetail.FromAddress
        If Not (String.IsNullOrEmpty(emailAddressFrom)) Then fromAddress = emailAddressFrom

        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        AppendLayoutElementsToDictionary(emailTemplateDetail.SubjectLine, replacements)
        Dim emailContent As String = EmailTemplateDetailRepository.BuildEmailContent(connectionString, emailTemplateId, localeId, False, replacements)
        If Not (String.IsNullOrEmpty(emailContent)) Then
            Return ProcessEmailRequest(emailAddressRecipients, fromAddress, emailTemplateDetail.SubjectLine, emailContent, emailAddressCarbonCopy, GetListOfBCCAddresses(emailAddressBlindCarbonCopy), (emailTemplateDetail.EmailType = SubscriptionFormat.HTML))
        End If
        Return False
    End Function

    Private Shared Function ProcessEmailRequest(ByVal emailAddressRecipients As List(Of String), ByVal fromAddress As String, ByVal subjectLine As String, ByVal body As String, ByVal emailAddressCarbonCopy As List(Of String), ByVal emailAddressBlindCarbonCopy As List(Of String), ByVal isHtmlEmail As Boolean) As Boolean
        Try
            fromAddress = Utilities.ValidateFromAddressIsLegal(fromAddress)
            Utilities.SendEmail(emailAddressRecipients, fromAddress, subjectLine, body, emailAddressCarbonCopy, emailAddressBlindCarbonCopy, isHtmlEmail)
        Catch
            Return False
        End Try
        Return True
    End Function

    Private Shared Sub AppendLayoutElementsToDictionary(ByVal subjectLine As String, ByVal replacements As Dictionary(Of String, String))
        replacements.Add("<#HeaderSubject#>", subjectLine)
        replacements.Add("<#FooterCopyrightYear#>", DateTime.Now.Year.ToString())
    End Sub

    Private Shared Function GetEmailTemplateDetail(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32) As EmailTemplateDetail
        Return GetEmailTemplateDetail(connectionString, emailTemplateId, localeId, 2)
    End Function

    Private Shared Function GetEmailTemplateDetail(ByVal connectionString As String, ByVal emailTemplateId As Int32, ByVal localeId As Int32, ByVal emailTypeId As Int32) As EmailTemplateDetail
        Dim emailTemplateDetail As EmailTemplateDetail = EmailTemplateDetailRepository.GetEmailTemplateDetailForEmailTemplateAndLocale(connectionString, emailTemplateId, localeId, emailTypeId)
        If (emailTemplateDetail Is Nothing) Then
            If Not (localeId = LocaleIds.ENGLISH_US) Then
                emailTemplateDetail = New EmailTemplateDetail()
                emailTemplateDetail = EmailTemplateDetailRepository.GetEmailTemplateDetailForEmailTemplateAndLocale(connectionString, emailTemplateId, LocaleIds.ENGLISH_US, emailTypeId)
                If (emailTemplateDetail Is Nothing) Then
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        End If
        Return emailTemplateDetail
    End Function

    Private Shared Function GetListOfBCCAddresses(ByVal emailAddressBlindCarbonCopy As List(Of String)) As List(Of String)
        Dim retVal As List(Of String) = emailAddressBlindCarbonCopy
        If Not (retVal.Contains("kate.kaplan@teledynelecroy.com")) Then
            retVal.Add("kate.kaplan@teledynelecroy.com")
        End If
        If Not (retVal.Contains("james.chan@teledynelecroy.com")) Then
            retVal.Add("james.chan@teledynelecroy.com")
        End If
        Return retVal
    End Function
End Class