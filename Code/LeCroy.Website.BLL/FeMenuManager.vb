﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeMenuManager
    Public Shared Function BuildMenusWithPrefetchedSubMenus(ByVal connectionString As String) As IList(Of ExtendedMenu)
        Dim retVal As List(Of ExtendedMenu) = New List(Of ExtendedMenu)
        Dim menus As List(Of Menu) = MenuRepository.GetMenus(connectionString, True, Nothing).ToList()

        For Each menu As Menu In menus
            Dim em As ExtendedMenu = New ExtendedMenu(menu)
            em.SubMenu = SubMenuRepository.GetSubMenusWithMenuId(connectionString, menu.MenuId, True).ToList()
            retVal.Add(em)
        Next
        Return retVal
    End Function
End Class