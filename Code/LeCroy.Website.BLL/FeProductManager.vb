﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeProductManager
    Public Shared Function CanProductShow(ByVal connectionString As String, ByVal productId As Int32, ByVal categoryId As Nullable(Of Int32), ByVal productSeriesId As Nullable(Of Int32)) As Boolean
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If (categoryId.HasValue) Then
            If (categoryId.Value = 26) Then Return False
            If (productSeriesId.HasValue) Then
                If (productSeriesId.Value = 484 Or productSeriesId.Value = 332) Then   ' Special case for LM9/10 since the products are not in the psc table (only the master modules are)
                    sqlString = "SELECT DISTINCT d.* FROM [PRODUCT] a, [CONFIG] b, [PRODUCT_SERIES_CATEGORY] c, [PRODUCT] d WHERE a.PRODUCTID = c.PRODUCT_ID AND b.OPTIONID = d.PRODUCTID AND a.PRODUCTID = b.PRODUCTID AND c.CATEGORY_ID = 1 AND c.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND d.GROUP_ID = 179 AND d.DISABLED = 'n' AND a.DISABLED = 'n' AND d.PRODUCTID = @PRODUCTID ORDER BY d.SORT_ID"
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
                Else
                    If (productSeriesId.Value = 511) Then 'HACK:
                        sqlString = "SELECT p.* FROM [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE p.PRODUCTID = psc.PRODUCT_ID AND psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND p.DISABLED = 'N' AND p.EOL_YN = 'N' AND p.PRODUCTID = @PRODUCTID"
                        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
                    Else
                        sqlString = "SELECT p.* FROM [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE p.PRODUCTID = psc.PRODUCT_ID AND psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND p.DISABLED = 'N' AND p.EOL_YN = 'N' AND p.PRODUCTID = @PRODUCTID"
                        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
                    End If
                End If
            Else
                sqlString = "SELECT p.* FROM [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc WHERE p.PRODUCTID = psc.PRODUCT_ID AND psc.CATEGORY_ID = @CATEGORYID AND p.DISABLED = 'N' AND p.EOL_YN = 'N' AND p.PRODUCTID = @PRODUCTID"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
            End If
        ElseIf (productSeriesId.HasValue) Then
            sqlString = "SELECT p.* FROM [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE p.PRODUCTID = psc.PRODUCT_ID AND psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND p.DISABLED = 'N' AND p.EOL_YN = 'N' AND p.PRODUCTID = @PRODUCTID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
        Else
            sqlString = "SELECT * FROM [PRODUCT] p WHERE p.DISABLED = 'N' AND p.EOL_YN = 'N' AND p.PRODUCTID = @PRODUCTID"
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
        End If

        If Not (String.IsNullOrEmpty(sqlString)) Then
            If (ProductRepository.FetchProducts(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0) Then
                Return True
            Else
                If (productSeriesId.HasValue) Then
                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(connectionString, productSeriesId.Value)
                    If Not (productSeries Is Nothing) Then
                        Return String.Compare(productSeries.ValidateYN, "N", True) = 0 And String.Compare(productSeries.Disabled, "N", True) = 0
                    End If
                End If
            End If
        End If
        Return False
    End Function

    Public Shared Function CanOptionShow(ByVal connectionString As String, ByVal productId As Int32, ByVal categoryId As Int32) As Boolean
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If (categoryId = 26) Then Return False
        Select Case categoryId
            Case CategoryIds.HARDWARE_OPTIONS, CategoryIds.SOFTWARE_OPTIONS, CategoryIds.ACCESSORIES, CategoryIds.MIXED_SIGNAL_TEST_SOLUTIONS, CategoryIds.PROBES, CategoryIds.ARBITRARY_WAVEFORM_GENERATORS
                Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(connectionString, "SELECT * FROM [PRODUCT_SERIES_CATEGORY] WHERE [PRODUCT_ID] = @PRODUCTID AND [CATEGORY_ID] <> 26 AND [CATEGORY_ID] = @CATEGORYID", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", productId.ToString()), New SqlParameter("@CATEGORYID", categoryId.ToString())}).ToArray())
                If (productSeriesCategories.Where(Function(x) x.CategoryId = categoryId).ToList().Count() > 0) Then
                    Dim recordsFound As Boolean = False
                    For Each psc As ProductSeriesCategory In productSeriesCategories
                        Dim ps As ProductSeries = ProductSeriesRepository.GetProductSeries(connectionString, psc.ProductSeriesId)
                        If (ps Is Nothing) Then Continue For
                        recordsFound = True
                        If (String.Compare(ps.Disabled, "Y", True) = 0) Then
                            Return False
                        End If
                    Next
                    If (recordsFound) Then Return True ' Has legitimate product_series_category records for productid and categoryid
                End If
                If (productSeriesCategories.Count() > 0) Then
                    Return False    ' Found records in product_series_category, but categoryid doesn't match, it can't be good
                Else
                    sqlString = "SELECT * FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID AND DISABLED = 'N' AND EOL_YN = 'N'"
                End If
            Case Else
                sqlString = "SELECT * FROM [PRODUCT] WHERE [PRODUCTID] IN (SELECT psc.PRODUCT_ID FROM [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_ID = @PRODUCTID) AND DISABLED = 'N' AND EOL_YN = 'N'"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        End Select
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
        Return ProductRepository.FetchProducts(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0
    End Function

    Public Shared Function CanProductShowByProductSeriesId(ByVal connectionString As String, ByVal productSeriesId As Int32, ByVal categoryId As Nullable(Of Int32)) As Boolean
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If (categoryId.HasValue) Then
            If (categoryId.Value = 26) Then Return False
            sqlString = "SELECT * FROM [PRODUCT] WHERE [PRODUCTID] IN (SELECT psc.PRODUCT_ID FROM [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID) AND DISABLED = 'N' AND EOL_YN = 'N'"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.Value.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Else
            sqlString = "SELECT * FROM [PRODUCT] WHERE [PRODUCTID] IN (SELECT psc.PRODUCT_ID FROM [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT_SERIES] ps WHERE psc.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND psc.CATEGORY_ID <> 26) AND DISABLED = 'N' AND EOL_YN = 'N'"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        End If

        If Not (String.IsNullOrEmpty(sqlString)) Then
            If (ProductRepository.FetchProducts(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0) Then
                Return True
            Else
                Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(connectionString, productSeriesId)
                If Not (productSeries Is Nothing) Then
                    Return String.Compare(productSeries.ValidateYN, "N", True) = 0 And String.Compare(productSeries.Disabled, "N", True) = 0
                End If
            End If
        End If
        Return False
    End Function

    Public Shared Function CanProtocolShow(ByVal connectionString As String, ByVal protocolStandardId As Int32, ByVal productSeriesId As Nullable(Of Int32)) As Boolean
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim productSeries As List(Of ProductSeries) = New List(Of ProductSeries)

        If (productSeriesId.HasValue) Then
            sqlString = "SELECT * FROM [PRODUCT_SERIES] WHERE [DISABLED] = 'N' AND [PRODUCT_SERIES_ID] = @PRODUCTSERIESID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.Value.ToString()))
            productSeries = ProductSeriesRepository.FetchProductSeries(connectionString, sqlString, sqlParameters.ToArray()).ToList()
            For Each ps In productSeries
                If (String.Compare(ps.Disabled, "N", True) = 0) Then
                    If (String.Compare(ps.ValidateYN, "Y", True) = 0) Then
                        sqlString = "SELECT * FROM [PRODUCT] WHERE [DISABLED] = 'N' AND [EOL_YN] = 'N' AND [PRODUCTID] IN (" & vbCrLf &
                                    "     SELECT [PRODUCT_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [CATEGORY_ID] = 19 AND [PRODUCT_SERIES_ID] = @PRODUCTSERIESID)"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", ps.ProductSeriesId.ToString()))
                        If (ProductRepository.FetchProducts(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0) Then
                            Return True
                        End If
                    Else
                        Return True
                    End If
                End If
            Next
        Else
            sqlString = "SELECT * FROM [PRODUCT_SERIES] WHERE [DISABLED] = 'N' AND [PRODUCT_SERIES_ID] IN (" & vbCrLf &
                        "             SELECT DISTINCT [PRODUCT_SERIES_ID] FROM [PROTOCOL_STANDARD_SERIES] WHERE [PROTOCOL_STANDARD_ID] = @PROTOCOLSTANDARDID)"
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.ToString()))
            productSeries = ProductSeriesRepository.FetchProductSeries(connectionString, sqlString, sqlParameters.ToArray()).ToList()
            For Each ps In productSeries
                If (String.Compare(ps.Disabled, "N", True) = 0) Then
                    If (String.Compare(ps.ValidateYN, "Y", True) = 0) Then
                        sqlString = "SELECT * FROM [PRODUCT] WHERE [DISABLED] = 'N' AND [EOL_YN] = 'N' AND [PRODUCTID] IN (" & vbCrLf &
                                    "     SELECT [PRODUCT_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [CATEGORY_ID] = 19 AND [PRODUCT_SERIES_ID] = @PRODUCTSERIESID)"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", ps.ProductSeriesId.ToString()))
                        If (ProductRepository.FetchProducts(connectionString, sqlString, sqlParameters.ToArray()).ToList().Count > 0) Then
                            Return True
                        End If
                    Else
                        Return True
                    End If
                End If
            Next
        End If
        Return False
    End Function

    Public Shared Function GetProductSeriesForCategory(ByVal connectionString As String, ByVal categoryId As Int32, ByVal isFilteredBind As Boolean, ByVal rangeId As Nullable(Of Int32), ByVal excludeProductIds As String) As List(Of ProductSeries)
        Dim sqlString As String = "SELECT DISTINCT b.* FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID = @CATEGORYID AND a.DISABLED = 'N' ORDER BY b.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        If (isFilteredBind) Then
            Dim rangeSqlString As String = String.Empty
            If (rangeId.HasValue) Then
                rangeSqlString = " AND a.RANGE_ID = @RANGEID "
                sqlParameters.Add(New SqlParameter("@RANGEID", rangeId.Value.ToString()))
            End If
            Dim excludeSqlString As String = String.Empty
            If Not (String.IsNullOrEmpty(excludeProductIds)) Then
                excludeSqlString = String.Format("AND a.PRODUCTID NOT IN ({0}) ", excludeProductIds)
            End If
            sqlString = String.Format("SELECT DISTINCT b.* FROM PRODUCT a, PRODUCT_SERIES b, BANDWIDTH_RANGE c, PRODUCT_SERIES_CATEGORY d WHERE a.PRODUCTID = d.PRODUCT_ID AND b.PRODUCT_SERIES_ID = d.PRODUCT_SERIES_ID AND d.CATEGORY_ID = @CATEGORYID {0} {1} AND a.DISABLED = 'N' ORDER BY b.SORTID", rangeSqlString, excludeSqlString)
        End If
        Return ProductSeriesRepository.FetchProductSeries(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Public Shared Function GetProductSeriesCategoryCount(ByVal connectionString As String, ByVal categoryId As Int32, ByVal productSeriesId As Int32) As Int32
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(connectionString, "SELECT distinct product_id FROM PRODUCT_SERIES_CATEGORY where CATEGORY_ID = @CATEGORYID and PRODUCT_SERIES_ID = @PRODUCTSERIESID", sqlParameters.ToArray())
        Return productSeriesCategories.Count()
    End Function

    Public Shared Function GetProductSeriesForDocumentAndSubCategory(ByVal connectionString As String, ByVal documentId As Int32, ByVal categoryId As Int32, ByVal subCategoryId As Int32) As List(Of ProductSeries)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentId.ToString()))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", subCategoryId.ToString()))
        Return ProductSeriesRepository.FetchProductSeries(connectionString, "SELECT DISTINCT b.PRODUCT_SERIES_ID,b.NAME as NAME, b.FW_SORT_ID FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN SUBCATEGORY c ON b.SUBCAT_ID = c.SUBCAT_ID INNER JOIN DOCUMENT_CATEGORY d ON b.PRODUCT_SERIES_ID = d.PRODUCT_SERIES_ID INNER JOIN DOCUMENT e ON e.DOCUMENT_ID = d.DOCUMENT_ID WHERE e.POST='y' AND d.DOCUMENT_ID = @DOCUMENTID AND a.CATEGORY_ID = @CATEGORYID AND c.SUBCAT_ID = @SUBCATEGORYID ORDER BY b.FW_SORT_ID", sqlParameters.ToArray())  'AND b.DISABLED = 'n' ORDER BY b.SORTID
    End Function

    Public Shared Function GetProducts(ByVal connectionString As String, ByVal categoryId As Int32, ByVal productSeriesId As Int32, ByVal documentId As Int32, ByVal subCategoryId As Int32) As List(Of Product)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentId.ToString()))
        sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", subCategoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Return ProductRepository.FetchProducts(connectionString, "SELECT DISTINCT f.PRODUCTID, f.PARTNUMBER, f.SORT_ID FROM DOCUMENT a INNER JOIN DOCUMENT_CATEGORY AS d ON a.DOCUMENT_ID = d.DOCUMENT_ID INNER JOIN PRODUCT AS f ON d.PRODUCT_ID = f.PRODUCTID INNER JOIN PRODUCT_SERIES_CATEGORY e ON f.PRODUCTID = e.PRODUCT_ID INNER JOIN PRODUCT_SERIES b ON e.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN SUBCATEGORY c ON b.SUBCAT_ID = c.SUBCAT_ID WHERE a.POST='y' AND d.DOCUMENT_ID = @DOCUMENTID AND b.DISABLED = 'n' AND e.CATEGORY_ID = @CATEGORYID AND c.SUBCAT_ID = @SUBCATEGORYID AND e.PRODUCT_SERIES_ID= @PRODUCTSERIESID ORDER BY f.SORT_ID", sqlParameters.ToArray())
    End Function
End Class