﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FePropertyManager
    Public Shared Function GetProperties(ByVal connectionString As String) As IList(Of [Property])
        Dim sqlString As String = "Select Distinct p.PROPERTYID, p.PROPERTYNAME, p.SORTID, p.CATEGORY_DISPLAYNAME, p.CATEGORY_SORTID From SERIES_PROPERTY sp, [PROPERTY] p " +
                          "Where sp.PROPERTY_ID = p.PROPERTYID And sp.PRODUCT_SERIES_ID In " +
                          "   (Select Distinct ps.PRODUCT_SERIES_ID " +
                          "    From PRODUCT_SERIES ps, PRODUCT_SERIES_CATEGORY psc, PRODUCT p " +
                          "    Where ps.PRODUCT_SERIES_ID = psc.PRODUCT_SERIES_ID AND psc.PRODUCT_ID = p.PRODUCTID AND psc.CATEGORY_ID = 1 AND ps.[DISABLED] = 'N' AND p.[DISABLED] = 'N') " +
                          "Order By p.SORTID"
        Return PropertyRepository.FetchProperties(connectionString, sqlString, New List(Of SqlParameter)().ToArray()).ToList()
    End Function

    Public Shared Function GetSeriesProperties(ByVal connectionString As String, ByVal productSeriesIds As List(Of Int32)) As IList(Of SeriesProperty)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim productSeriesIdKeys As String = String.Empty
        For i As Int32 = 0 To productSeriesIds.Count - 1
            productSeriesIdKeys += String.Format("@PRODUCTSERIESID{0},", i)
            sqlParameters.Add(New SqlParameter(String.Format("@PRODUCTSERIESID{0}", i), productSeriesIds(i).ToString()))
        Next
        If Not (String.IsNullOrEmpty(productSeriesIdKeys)) Then productSeriesIdKeys = productSeriesIdKeys.Substring(0, productSeriesIdKeys.Length - 1)
        Dim sqlString As String = String.Format("Select * From SERIES_PROPERTY Where PRODUCT_SERIES_ID IN ({0})", productSeriesIdKeys)
        Return SeriesPropertyRepository.FetchSeriesProperties(connectionString, sqlString, sqlParameters.ToArray()).ToList()
    End Function
End Class