﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class FeProtocolStandardManager
    Public Shared Function GetArchivedDocumentsForProtocolStandard(ByVal connectionString As String, ByVal protocolStandardId As Int32) As List(Of Document)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.ToString()))
        Return DocumentRepository.FetchDocuments(connectionString, "SELECT DISTINCT '' as flgNew,DOCUMENT.DOCUMENT_ID,DOCUMENT.DOC_TYPE_ID,DOCUMENT.TITLE, DOCUMENT.VERSION, DOCUMENT.SORT_ID,'' as flgPassword, DOCUMENT.DATE_ENTERED FROM DOCUMENT INNER JOIN    DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'd') and  DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID ORDER BY DOCUMENT.SORT_ID", sqlParameters.ToArray())
    End Function

    Public Shared Function GetDocumentsForProtocolStandard(ByVal connectionString As String, ByVal protocolStandardId As Int32) As List(Of Document)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", protocolStandardId.ToString()))
        Return DocumentRepository.FetchDocuments(connectionString, "SELECT DISTINCT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOC_TYPE_ID,DOCUMENT.TITLE,DOCUMENT.DESCRIPTION, DOCUMENT.VERSION, DOCUMENT.SORT_ID, CASE WHEN DOCUMENT.PASSWORD_YN ='y' then '<img src=""/images/icons/lock.png"" width=""16"" height=""16""><br>requires password' else '' end as PASSWORD_YN, DOCUMENT.DOC_SIZE, DOCUMENT.COMMENTS, DOCUMENT.DATE_ENTERED FROM DOCUMENT INNER JOIN    DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'y') and  DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID ORDER BY DOCUMENT.SORT_ID", sqlParameters.ToArray())
    End Function
End Class