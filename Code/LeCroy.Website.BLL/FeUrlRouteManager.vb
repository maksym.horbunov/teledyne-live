﻿Public Class FeUrlRouteManager
    Public Shared Function IsValidParameter(ByVal parameter As Int32, ByVal configParameters As String) As Boolean
        Dim validParameters As List(Of Int32) = GetAllowedParametersFromConfiguration(configParameters)
        Return validParameters.Contains(parameter)
    End Function

    Private Shared Function GetAllowedParametersFromConfiguration(ByVal configKey As String) As List(Of Int32)
        Dim retVal As List(Of Int32) = New List(Of Int32)
        Try
            Dim configurationValue As String = configKey
            If (String.IsNullOrEmpty(configurationValue)) Then
                Return retVal
            End If
            Dim keys As String() = configurationValue.Split(",")
            For Each key As String In keys
                Dim testVal As Int32 = 0
                If (Int32.TryParse(key, testVal)) Then
                    If Not (retVal.Contains(testVal)) Then
                        retVal.Add(testVal)
                    End If
                End If
            Next
        Catch
        End Try
        Return retVal
    End Function
End Class