﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Utilities
    Public Shared Sub SendEmail(ByVal toEmailAddress As List(Of String), ByVal fromEmailAddress As String, ByVal messageSubject As String, ByVal messageBody As String, ByVal ccEmailAddress As List(Of String), ByVal bccEmailAddress As List(Of String))
        SendEmail(toEmailAddress, fromEmailAddress, messageSubject, messageBody, ccEmailAddress, bccEmailAddress, False)
    End Sub

    Public Shared Sub SendEmail(ByVal toEmailAddress As List(Of String), ByVal fromEmailAddress As String, ByVal messageSubject As String, ByVal messageBody As String, ByVal ccEmailAddress As List(Of String), ByVal bccEmailAddress As List(Of String), ByVal isHtmlEmail As Boolean)
        Try
            Dim fromEmail As MailAddress = New MailAddress(ValidateFromAddressIsLegal(fromEmailAddress))
            Dim mm As MailMessage = New MailMessage()
            mm.From = fromEmail
            mm.To.Add(String.Join(",", toEmailAddress))
            mm.Subject = messageSubject
            mm.Body = messageBody
            mm.IsBodyHtml = isHtmlEmail
            If (ccEmailAddress.Count > 0) Then
                Dim emailAddresses As String = String.Empty
                For Each emailAddress In ccEmailAddress
                    If Not (String.IsNullOrEmpty(emailAddress)) Then
                        emailAddresses += String.Format("{0},", emailAddress)
                    End If
                Next
                If Not (String.IsNullOrEmpty(emailAddresses)) Then
                    emailAddresses = emailAddresses.Substring(0, emailAddresses.Length - 1)
                    mm.CC.Add(emailAddresses)
                End If
            End If
            If (bccEmailAddress.Count > 0) Then
                Dim emailAddresses As String = String.Empty
                For Each emailAddress In bccEmailAddress
                    If Not (String.IsNullOrEmpty(emailAddress)) Then
                        emailAddresses += String.Format("{0},", emailAddress)
                    End If
                Next
                If Not (String.IsNullOrEmpty(emailAddresses)) Then
                    emailAddresses = emailAddresses.Substring(0, emailAddresses.Length - 1)
                    mm.Bcc.Add(emailAddresses)
                End If
            End If
            Dim smtpClient As SmtpClient = New SmtpClient(ConfigurationManager.AppSettings("EmailHost"), Int32.Parse(ConfigurationManager.AppSettings("Port")))
            smtpClient.Send(mm)
        Catch
        End Try
    End Sub

    Public Shared Function checkValidEmail(ByVal EmailAddress As String) As Boolean
        Dim pattern As String = "^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$"
        Dim check As New Regex(pattern, RegexOptions.IgnorePatternWhitespace)
        Dim valid As Boolean = False
        'make sure an email address was provided
        If String.IsNullOrEmpty(EmailAddress) Then
            valid = False
        Else
            'use IsMatch to validate the address
            valid = check.IsMatch(EmailAddress.ToLower())
        End If

        If (valid) Then
            If (EmailDomainBlackListRepository.IsEmailDomainBlackListed(ConfigurationManager.AppSettings("ConnectionString"), EmailAddress)) Then
                valid = False
            End If
        End If
        Return valid
    End Function

    Public Shared Function GenerateUserID(ByVal connectionString As String) As String
        Dim bl As Boolean = False
        Dim strUID1 As String = ""
        Dim strUIDTemp1 As String = ""
        Dim strUID2 As String = ""
        Dim strUIDTemp2 As String = ""

        Do While Not bl
            Do Until Len(strUID1) > 9 ' number of the strlen -1
                Randomize()
                ' Get a character
                strUIDTemp1 = Chr(Int(44 * Rnd()) + 48)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strUIDTemp1) < 58 Or (Asc(strUIDTemp1) > 64 And Asc(strUIDTemp1) < 91) Then
                    strUID1 += strUIDTemp1
                End If
            Loop

            Do Until Len(strUID2) > 9 ' number of the strlen -1
                Randomize()
                ' Get a character
                strUIDTemp2 = Chr(Int(44 * Rnd()) + 48)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strUIDTemp2) < 58 Or (Asc(strUIDTemp2) > 64 And Asc(strUIDTemp2) < 91) Then
                    strUID2 += strUIDTemp2
                End If
            Loop

            If CheckUserIDNotExist(connectionString, strUID1, strUID2) Then
                bl = True
            Else
                strUID1 = ""
                strUID2 = ""
                bl = False
            End If
        Loop
        GenerateUserID = strUID1 & strUID2
    End Function

    Public Shared Function CheckUserIDNotExist(ByVal connectionString As String, ByVal str1 As String, ByVal str2 As String) As Boolean
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@SUBID1", str1), New SqlParameter("@SUBID2", str2)})
        Dim subscription As Subscription = SubscriptionRepository.FetchSubscriptions(connectionString, "SELECT * FROM SUBSCRIPTION WHERE SUBSTRING(SUB_ID,1,10)=@SUBID1 and SUBSTRING(SUB_ID,11,10)=@SUBID2", sqlParameters.ToArray())
        If Not (subscription Is Nothing) Then
            If Not (String.IsNullOrEmpty(subscription.SubId)) Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Shared Function checksubscriptionexist(ByVal connectionString As String, ByVal strDocumentID As String, ByVal strEmail As String) As Boolean
        If (String.IsNullOrEmpty(strDocumentID) Or String.IsNullOrEmpty(strEmail)) Then
            Return False
        End If
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@DOCUMENTID", strDocumentID), New SqlParameter("@EMAIL", strEmail)})
        Dim subscription As Subscription = SubscriptionRepository.FetchSubscriptions(connectionString, "SELECT DISTINCT * FROM SUBSCRIPTION WHERE ENABLE='y' and EMAIL=@EMAIL and DOCUMENT_ID=@DOCUMENTID", sqlParameters.ToArray())
        If Not (subscription Is Nothing) Then
            If (String.Compare(strDocumentID, subscription.DocumentId.ToString(), True) = 0 And String.Compare(strEmail, subscription.Email, True) = 0) Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Shared Function SQLStringWithOutSingleQuotes(ByVal strinput As String) As String
        Dim invalidWords As List(Of String) = New List(Of String)({"SELECT", "DROP", "--", "INSERT", "DELETE", "_XP", "xp_", "1=1", "/* ... */", "Char("})
        If Not (String.IsNullOrEmpty(strinput)) Then
            strinput = strinput.Trim()
            strinput = strinput.Replace("'", "''")
            For Each word As String In invalidWords
                If (String.Compare(strinput, word, True) = 0) Then
                    strinput = strinput.Replace(word, String.Empty)
                End If
            Next
        Else
            strinput = String.Empty
        End If
        Return strinput
    End Function

    Public Shared Function ValidateFromAddressIsLegal(ByVal existingFromAddress As String) As String
        If (String.IsNullOrEmpty(existingFromAddress)) Then
            Return "webmaster@teledynelecroy.com"
        End If
        If Not (existingFromAddress.Contains(",")) Then
            Return existingFromAddress
        End If
        Return existingFromAddress.Split(",")(0)
    End Function
End Class