﻿Public Class MasterPageBase
    Inherits MasterPage

    Protected Sub InjectCssIntoHeadElement(ByVal resourceUrl As String)
        Dim control As HtmlLink = New HtmlLink()
        control.Href = ResolveUrl(resourceUrl)
        control.Attributes.Add("rel", "stylesheet")
        control.Attributes.Add("type", "text/css")
        CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(control)
    End Sub

    Protected Sub InjectJsIntoHeadElement(ByVal resourceUrl As String)
        Dim control As HtmlGenericControl = New HtmlGenericControl("script")
        control.Attributes.Add("type", "text/javascript")
        control.Attributes.Add("language", "javascript")
        control.Attributes.Add("src", ResolveUrl(resourceUrl))
        CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(control)
    End Sub
End Class