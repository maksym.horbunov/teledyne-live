﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Public Class PageBase
    Inherits Page

    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        ValidateIntegerForCountryCodeFromSession(Session("CountryCode"))
        MyBase.OnLoad(e)
    End Sub

#Region "Taken from Web.UI.BaseClasses"
    Private Shared Function ValidateIntegerFromSession(ByVal val As Object) As Int32
        If (val Is Nothing) Then
            Return -1
        End If
        If (String.IsNullOrEmpty(val)) Then
            Return -1
        End If
        Dim testVal As Int32 = 0
        If (Int32.TryParse(val.ToString(), testVal)) Then
            Return testVal
        End If
        Return -1
    End Function

    Public Shared Function ValidateIntegerForCountryCodeFromSession(ByVal val As Object) As Int32
        Dim retVal As Int32 = ValidateIntegerFromSession(val)
        If (retVal = -1) Then
            retVal = 208
        End If
        Return retVal
    End Function

    Public Shared Function ValidateIntegerForLocaleIdFromSession(ByVal val As Object) As Int32
        Dim retVal As Int32 = ValidateIntegerFromSession(val)
        If (retVal < 0) Then
            Return 1033
        End If
        Return retVal
    End Function

    Public Shared Function GenerateSubscriptionToOptedIn(ByVal subscriptionId As String, ByVal preference As Int32, ByVal countryId As Int32, ByVal emailAddress As String) As String
        Dim subscriptions As List(Of Subscription) = SubscriptionRepository.GetSubscriptions(ConfigurationManager.AppSettings("ConnectionString").ToString(), emailAddress).ToList()
        If (subscriptions.Count <= 0) Then  ' No Subscription records; insert it
            If (String.IsNullOrEmpty(subscriptionId)) Then subscriptionId = Utilities.GenerateUserID(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToString()
            SubscriptionRepository.InsertSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), CreateSubscription(subscriptionId, preference, countryId, emailAddress, "Y"))
            Return subscriptionId
        End If

        ' Subscription record(s) exist; check against doc id
        subscriptions = subscriptions.Where(Function(x) x.DocumentId = preference).ToList() ' Should only be one unique record if it exists, otherwise the update will bulk update all based on email+preference
        If (subscriptions.Count > 0) Then   ' Subscription records for email+docid exist; update
            SubscriptionRepository.UpdateSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), New List(Of String)({emailAddress}), New List(Of Int32)({preference}))
            Return subscriptionId   ' Unchanged
        Else    ' No email+docid records exist, insert
            If (String.IsNullOrEmpty(subscriptionId)) Then subscriptionId = Utilities.GenerateUserID(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToString()
            SubscriptionRepository.InsertSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), CreateSubscription(subscriptionId, preference, countryId, emailAddress, "Y"))
            Return subscriptionId
        End If
    End Function

    Public Shared Function CreateSubscription(ByVal subscriptionId As String, ByVal preference As Int32, ByVal countryId As Int32, ByVal emailAddress As String, ByVal enable As String) As Subscription
        Dim subscription As Subscription = New Subscription()
        subscription.SubId = subscriptionId
        subscription.Email = emailAddress
        subscription.DocumentId = preference
        subscription.ContactId = 0
        subscription.Enable = enable
        subscription.CountryId = countryId
        subscription.DocumentRead = "N"
        subscription.FormatId = 2
        subscription.ConfirmYn = "N"
        Return subscription
    End Function

    Protected Function SQLStringWithOutSingleQuotes(ByVal strinput As String) As String
        ' This method can be used to validate the data entered
        ' by the user on FORM for SQL Injection.  It removes all the malacious 
        ' characters and commands.  It also appends and pre-appends the variable
        ' with single quote (').
        Dim badChars As String() = New String() {"SELECT", "DROP", "select", "drop", "--",
                                                "INSERT", "insert", "DELETE", "delete", "_XP", "xp_", "1=1", "/* ... */", "Char("}
        If Not strinput Is Nothing Then
            If strinput.Length > 0 Then
                strinput = strinput.Trim()
                strinput = strinput.Replace("'", "''")
                Dim i As Integer
                For i = 0 To badChars.Length - 1 Step i + 1
                    strinput = strinput.Replace(badChars(i), "")
                Next
            Else
                strinput = ""
            End If
        Else
            strinput = ""
        End If
        Return strinput
    End Function
#End Region
End Class