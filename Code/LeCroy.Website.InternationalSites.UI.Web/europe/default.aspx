<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_default" %>
<html>
<head>
<title>Teledyne LeCroy Europe &#150; Select a Language</title>
</head>
<body bgcolor=black leftmargin=0 marginheight=0 marginwidth=0 topmargin=0>
<table width=100% border=0 cellspacing=0 cellpadding=0>
  <tr height=90>
    <td valign=top width=143 height=90><a href="https://teledynelecroy.com/"><img src="/images/tl_weblogo_white_254x60.png" alt="Teledyne LeCroy" height=60 width=254 border=0></a></td>
    <td height=90></td>
    <td height=90></td>
  </tr>
  <tr height=240>
    <td colspan=2 bgcolor=#0e58d5 height=240><img src="/images/image_europe2.jpg" alt="" height=240 width=900 border=0></td>
    <td bgcolor=#0e58d5 height=240></td>
  </tr>
  <tr height=20>
    <td rowspan=2 width=143 height=40></td>
    <td height=20></td>
    <td height=20></td>
  </tr>
  <tr height=20>
    <td height=20>
	<a onMouseOver="changeImages('languages_03','images/languages_aktive_03.gif');return true" onMouseOut="changeImages('languages_03','images/languages_03.gif');return true" href=/europe/english.aspx><img id=languages_03 src=images/languages_03.gif alt="" name=languages_03 height=32 width=73 border=0></a>
	<img src=images/line_10.jpg alt="" height=32 width=16 border=0> 
	<a onMouseOver="changeImages('languages_4','images/languages_aktive_04.gif');return true" onMouseOut="changeImages('languages_4','images/languages_04.gif');return true" href=/germany/><img id=languages_4 src=images/languages_04.gif alt="" name=languages_4 height=32 width=78 border=0></a>
	<img src=images/line_10.jpg alt="" height=32 width=16 border=0> 
	<a onMouseOver="changeImages('languages_5','images/languages_aktive_05.gif');return true" onMouseOut="changeImages('languages_5','images/languages_05.gif');return true" href=/france/><img id=languages_5 src=images/languages_05.gif alt="" name=languages_5 height=32 width=70 border=0></a>
	<img src=images/line_10.jpg alt="" height=32 width=16 border=0> 
	<a onMouseOver="changeImages('languages_6','images/languages_aktive_06.gif');return true" onMouseOut="changeImages('languages_6','images/languages_06.gif');return true" href=/IT/><img id=languages_6 src=images/languages_06.gif alt="" name=languages_6 height=32 width=64 border=0></a>
    </td>
    <td height=20></td>
  </tr>
  <tr height=32>
    <td width=143 height=32></td>
    <td height=32><img src=images/languages_14.gif alt="" height=21 width=230 border=0></td>
    <td height=32></td>
  </tr>
</table>
<script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>