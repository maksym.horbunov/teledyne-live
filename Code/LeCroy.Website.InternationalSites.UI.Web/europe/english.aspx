﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="english.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_english" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="SlideshowContainer">
        <div id="Slideshow">
            <!-- ws4000hd --><div id="card11" class="slideTop" onclick="window.location='/ws4000hd/'" style="cursor: pointer;"></div>
            <!-- wr8000hd --><div id="card12" class="slideTop" onclick="window.location='/wr8000hd/'" style="cursor: pointer;"></div>
            <!-- 12bits --><div id="card13" class="slideTop" onclick="window.location='/hdo/'" style="cursor: pointer;"></div>
            <!-- mda8000hd --><div id="card14" class="slideTop" onclick="window.location='/static-dynamic-complete/'" style="cursor: pointer;"></div>
            <!-- wavepulser --><div id="card15" class="slideTop" onclick="window.location='/wavepulser/'" style="cursor: pointer;"></div>
            <!-- waveprodhd --><div id="card16" class="slideTop" onclick="window.location='/waveprohd/'" style="cursor: pointer;"></div>
            <!-- wr9000 --><div id="card17" class="slideTop" onclick="window.location='/wr9000/'" style="cursor: pointer;"></div>
            <!-- HDA125 --><div id="card18" class="slideTop" onclick="window.location='/options/productdetails.aspx?modelid=9703&categoryid=18&groupid=54'" style="cursor: pointer;"></div>           
            <!-- testtools --><div id="card9e" class="slideTop" onclick="window.location='/testtools/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; position: relative; width: 616px; padding: 55px 0 0 50px; text-align:left;"></div>
                </div>
            </div>
            
            
        </div>

        <div id="SlideshowPrev"></div>
        <div id="SlideshowNext"></div>
    </div>
    <div class="container">
        <div id="content">
            <div id="subfeature">
                <ul>
                    <li class="left"><a href="/events/" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/events_en.jpg" alt="events" class="align-left" width="290" height="150" border="0" /></a></li>
                    <li class="">
                        <a href="/europe/promotions/promotions.aspx" style="text-decoration: none">
                            <img src="https://assets.lcry.net/images/promotions_en.jpg" class="align-left" alt="promotions" width="290" height="150" border="0" /></a>
                    </li>
                    <li class="right2"><a href="/europe/rental/" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/rental_en.jpg" alt="rental" width="290" height="150" class="align-right" border="0" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>