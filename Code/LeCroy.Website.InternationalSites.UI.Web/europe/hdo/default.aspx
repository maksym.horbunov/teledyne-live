﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_hdo_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td height="20" colspan="2">&nbsp;</td></tr>
        <tr>
            <td colspan="2" style="padding-left: 375px;">
		<span style="float: right;"><a href="/europe/hdo-demo-promotion.html"><img src="https://assets.lcry.net/images/landing_hdo_promo_eng.png" alt="" border="0" height="150" /></a></span>
                <font color="#0076C0" style="font-size: 24pt;">High Definition 12-bit Oscilloscopes</font><br />
                <span style="font-size: 20pt;">200 MHz – 1 GHz with True 12-bit Hardware, up to 8 Channels</span><br />
                <span style="font-size: 16pt;"><b style="color: #ff0000;">NEW</b> - Complete Power Analysis on 3-Phase Motor Drives - MDA800!</span>
            </td>
        </tr>
        <tr><td height="10" colspan="2">&nbsp;</td></tr>
        <tr>
            <td width="800" align="left" valign="top">
                <img src="../content/hdo-familyshot.png" alt="hdo family" width="800" height="282" border="0" usemap="#familyshot" />
                <map id="familyshot" name="familyshot">
                    <area shape="rect" coords="167,210,204,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8822" alt="pdf" />
                    <area shape="rect" coords="359,210,396,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8522" alt="pdf" />
                    <area shape="rect" coords="545,210,582,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8831" alt="pdf" />
                    <area shape="rect" coords="730,210,767,261" href="https://teledynelecroy.com/doc/docview.aspx?id=9214" alt="pdf" />
                </map>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <p class="text-small"><strong>Clean, Crisp Waveforms<br /></strong>Waveforms captured and displayed with HD4096 technology are cleaner and crisper.</p>
                            <p class="text-small"><strong>More Signal Details<br /></strong>Signal details often lost in the noise are clearly visible and easy to distinguish.</p>
                            <p class="text-small"><strong>Unmatched Measurement Precision<br /></strong>Unmatched measurement precision for improved debug and analysis.</p>
                        </td>
                        <td width="20">&nbsp;</td>
                        <td><img src="../content/hdo_quantization_xsm.png" width="400" height="184" alt="waveform" /></td>
                        <td width="25">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Key Features</font></strong></p>
                <ul>
                    <li class="text">200 MHz, 350 MHz, 500 MHz, and 1 GHz</li>
                    <li class="text">Up to 8 channels</li>
                    <li class="text">12-bit ADC resolution, up to 15-bit</li>
                    <li class="text">Long memory – up tp 250 Mpts/Ch</li>
                    <li class="text">12.1&quot; touch screen</li>
                    <li class="text">16 digital channel MSO option with 1.25&nbsp;GS/s</li>
                    <li class="text">Advanced analysis and reporting toolsets</li>
                    <li class="text">Advanced triggering with TriggerScan and measurement trigger </li>
                    <li class="text">Serial data trigger &amp; decode and debug toolkit options</li>
                    <li class="text">Advanced tools</li>
                    <ul>
                        <li>Power Analysis Option</li>
                        <li>Spectrum Analyzer Option </li>
                        <li>LabNotebook documentation and report generation </li>
                        <li>History Mode – waveform playback </li>
                    </ul>
                    <li class="text">
                        <p><strong>Motor Drive Analyzer MDA800</strong> for complete power analysis of 3-phase motor drives</p>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="2" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text">
                    <strong><font color="#0076C0">More Information</font></strong><br />
                    Please use the form below to request more information</p>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Company:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Country:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Action:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Documentation" Value="Documentation" />
                                <asp:ListItem Text="Quotatation" Value="Quotatation" />
                                <asp:ListItem Text="Demonstration" Value="Demonstration" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Remark:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Yes, I want to subscribe to<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the Teledyne LeCroy newsletter" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Models:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>HDO4022</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 2 Ch." Value="HDO4022" />
                                <asp:ListItem Text="<b>HDO4024</b>, 200 MHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO4024" />
                                <asp:ListItem Text="<b>HDO4034</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO4034" />
                                <asp:ListItem Text="<b>HDO4054</b>, 500 MHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO4054" />
                                <asp:ListItem Text="<b>HDO4104</b>, 1 GHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO4104" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 Digital Channels<br /><br />" Value="MS Option HDO4000" />
                                <asp:ListItem Text="<b>HDO6034</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO6034" />
                                <asp:ListItem Text="<b>HDO6054</b>, 500 MHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO6054" />
                                <asp:ListItem Text="<b>HDO6104</b>, 1 GHz, 12-bit, 50 Mpts/Ch, 4 Ch." Value="HDO6104" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 Digital Channels<br /><br />" Value="MS Option HDO6000" />
                                <asp:ListItem Text="<b>HDO8038</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="HDO8038" />
                                <asp:ListItem Text="<b>HDO8058</b>, 500 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="HDO8058" />
                                <asp:ListItem Text="<b>HDO8108</b>, 1 GHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="HDO8108" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 Digital Channels<br /><br />" Value="MS Option HDO8000" />
                                <asp:ListItem Text="<b>MDA803</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA803" />
                                <asp:ListItem Text="<b>MDA805</b>, 500 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA805" />
                                <asp:ListItem Text="<b>MDA810</b>, 1 GHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA810" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>