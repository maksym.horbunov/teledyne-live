﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_mda_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td height="30" colspan="2"></td></tr>
        <tr>
            <td height="30" colspan="2">
                <font color="#0076C0" style="font-size: 24pt;">Motor Drive Analyzer - MDA800 Series</font><br />
                <span style="font-size: 20pt;">
                    <strong><u>Static</u></strong> and <strong><u>Dynamic</u></strong>
                    3-phase Power Analysis with Motor Integration. <strong><u>Complete</u></strong>
                    debug capability, from embedded control to inverter subsystem.
                </span>
            </td>
        </tr>
        <tr>
            <td width="710" height="30"></td>
            <td></td>
        </tr>
        <tr>
            <td width="710" align="left" valign="top">
                <p><img src="../content/mda-2015-brushless.jpg" width="601" height="475" alt="MDA800" /></p>
            </td>
            <td align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Key Features</font></strong></p>
                <ul>
                    <li class="text"><strong>&quot;Static&quot; (steady-state) power analysis</strong></li>
                    <li class="text"><strong>&quot;Dynamic&quot; power analysis over many seconds</strong></li>
                    <li class="text"><strong>High bandwidth (up to 1 GHz) supports control debug and correlation to power events</strong><br /></li>
                    <li class="text">8 channels at 12-bit ADC resolution with optional 16 channel MSO</li>
                    <li class="text">Long memory – up tp 250 Mpts/Ch</li>
                    <li class="text">Numerics table for voltage, current, power, and mechanical parameters</li>
                    <li class="text">Per-cycle &quot;synthesized&quot; Waveforms for understanding dynamic activities</li>
                    <li class="text">Zoom+Gate - isolate power analysis in long acquisitions</li>
                    <li class="text">BLDC Hall sensor, QEI, and Resolver support for speed, direction, and rotor angle</li>
                    <li class="text">Intuitive, graphical user interface</li>
                    <li class="text">19 different serial trigger, decode, and measure/graph options</li>
                    <li class="text">12.1&quot; display with extended desktop support for UHD monitors (3840 x 2160 pixels)</li>
                </ul>
                <p class="text-small">
                    <br />
                    <img src="../content/icon-pdf-color.png" width="16" height="16" alt="pdf" />&nbsp;<a href="https://teledynelecroy.com/doc/docview.aspx?id=8822" target="_blank">&nbsp;MDA800 Datasheet</a><br />
                    <%--<img src="../content/icon-pdf-color.png" width="16" height="16" alt="pdf" />&nbsp;Product Overview</p>--%>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#c8c8c8"></td>
            <td height="1" align="left" valign="top" bgcolor="#c8c8c8"></td>
        </tr>
        <tr>
            <td colspan="2" align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" colspan="5" valign="middle" bgcolor="#94C020">
                            <span class="text-inline">THE MOTOR DRIVE ANALYZER – A NEW CLASS OF INSTRUMENT</span>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" colspan="5" align="left" valign="top" class="text-small"></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="text-small">
                            <p>
                                <strong>Instrument Evolution </strong>
                                <br />
                                The Motor Drive Analyzer has the bandwidth (up to 1 GHz), inputs (8 analog channels
                                + 16 optional digital channels), acquisition memory (up to 250 Mpts/ch) to acquire
                                any signal in the embedded control system, motor mechanical interface, inverter
                                subsystem, or anything in between. It performs three-phase power analysis on short
                                or long acquisitions, provides dynamic power analysis with per-cycle &quot;synthesized&quot;
                                Waveforms and Zoom+Gate functionality, and permits cross-correlation and debug of
                                the complete drive system. One acquisition system means one result on one display,
                                and faster validation and understanding of design problems.</p>
                        </td>
                        <td width="30"></td>
                        <td align="left" valign="top"><img src="../content/480v-drive-io.jpg" width="250" height="156" alt="screen1" /></td>
                        <td width="15" align="left" valign="top"></td>
                        <td align="left" valign="top"><img src="../content/milwaukee-drill3.jpg" width="250" height="156" alt="screen2" /></td>
                    </tr>
                    <tr>
                        <td height="15" colspan="5" align="left" valign="top" class="text-small">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="2" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text"><strong><font color="#0076C0">More Information</font></strong><br />Please use the form below to request more information</p>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Company:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Country:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Action:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Documentation" Value="Documentation" />
                                <asp:ListItem Text="Quotatation" Value="Quotatation" />
                                <asp:ListItem Text="Demonstration" Value="Demonstration" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Remark:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Yes, I want to subscribe to<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the Teledyne LeCroy newsletter" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Models:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>MDA803</b>, 350 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA803" />
                                <asp:ListItem Text="<b>MDA805</b>, 500 MHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA805" />
                                <asp:ListItem Text="<b>MDA810</b>, 1 GHz, 12-bit, 50 Mpts/Ch, 8 Ch." Value="MDA810" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Options:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblOptions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>MSO</b>, 16 Digital Ch. Mixed-Signal Option." Value="MSO" />
                                <asp:ListItem Text="<b>L</b>, 100 Mpt/Ch Memory Option" Value="L" />
                                <asp:ListItem Text="<b>XL</b>, 250 Mpt/Ch Memory Option" Value="XL" />
                                <asp:ListItem Text="<b>PWR</b>, Semiconductor Device Analysis" Value="PWR" />
                                <asp:ListItem Text="<b>EMB</b>, I2C, SPI, UART and RS-232 Trigger and Decode Package" Value="EMB" />
                                <asp:ListItem Text="<b>VBA</b>, Vehicle Bus Analyzer Bundle – Includes CAN TDM, CAN Symbolic, FlexRay TDP, LIN TD and Protobus MAG" Value="VBA" />
                                <asp:ListItem Text="<b>ProtoBus</b>, MAG	Serial Debug Toolkit - Measure Analyze Graph" Value="ProtoBus" />
                                <asp:ListItem Text="<b>XDEV</b>, Developer's Tool Kit Option" Value="XDEV" />
                                <asp:ListItem Text="<b>JITKIT</b>, Clock, clock-data Jitter Analysis and views of time, statistical, spectral, and jitter overlay" Value="JITKIT" />
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>