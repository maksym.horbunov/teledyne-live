﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="promotions_roe.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_promotions_promotions_roe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="content" style="background-color: #ffffff;">
        <%--Start Editing Here--%>
        <div style="margin-top: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><span style="text-transform: uppercase"><font face="Arial" size="5">TELEDYNE LECROY EUROPE</font></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><span style="text-transform: uppercase"><font face="Arial" size="5">PROMOTIONS OCT - DEC 2014</font></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <span style="font-size: 20pt; font-weight: bold;">Demo Unit Sell-out Autumn 2014</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <th>Qty</th>
                                <th align="left">Demo Oscilloscope</th>
                                <th>Specifications</th>
                                <th>Discount</th>
                            </tr>
                            <tr>
                                <td align="center" width="50">3</td>
                                <td align="left" width="200"><b>SDA 760ZI-A</b></td>
                                <td>6 GHz, 20 GS/s, 4 Ch, 32 Mpts/Ch Serial Data Analyzer with 15.4" Color Display.</td>
                                <td align="center" width="100"><b>40%</b></td>
                            </tr>
                            <tr>
                                <td align="center">1</td>
                                <td align="left"><b>SDA 830ZI-A</b></td>
                                <td>30 GHz, 80 GS/s, 4 Ch, 64 Mpts/Ch Serial Data Analyzer with 15.3" Color Display.</td>
                                <td align="center"><b>45%</b></td>
                            </tr>
                            <tr>
                                <td align="center">2</td>
                                <td align="left"><b>WaveRunner HRO 64ZI</b></td>
                                <td>12-bit 400 MHz, 2 GS/s, 4 Ch, 64 Mpts/Ch with 12.1" Color Display.</td>
                                <td align="center"><b>32%</b></td>
                            </tr>
                            <tr>
                                <td align="center">3</td>
                                <td align="left"><b>WaveRunner HRO 66ZI</b></td>
                                <td>12-bit 600 MHz, 2 GS/s, 4 Ch, 64 Mpts/Ch with 12.1" Color Display.</td>
                                <td align="center"><b>32%</b></td>
                            </tr>
                            <tr>
                                <td align="center">2</td>
                                <td align="left"><b>WaveRunner 625ZI</b></td>
                                <td>2.5 GHz, 20 GS/s, 4 Ch, 16 Mpts/Ch DSO with 12.1" Color Display.</td>
                                <td align="center"><b>30%</b></td>
                            </tr>
                            <tr>
                                <td align="center">15</td>
                                <td align="left"><b>HDO 6054</b></td>
                                <td>12-bit 500 MHz, 4 Ch, 12-bit, 2.5 GS/s, 50 Mpts/Ch High Definition Oscilloscope with 12.1" Color Display</td>
                                <td align="center"><b>35%</b></td>
                            </tr>
                            <tr>
                                <td align="center">10</td>
                                <td align="left"><b>HDO 6104</b></td>
                                <td>12-bit 1 GHz, 4 Ch, 12-bit, 2.5 GS/s, 50 Mpts/Ch High Definition Oscilloscope with 12.1" Color Display</td>
                                <td align="center"><b>25%</b></td>
                            </tr>
                            <tr>
                                <td align="center">1</td>
                                <td align="left"><b>WaveExpert 100H</b></td>
                                <td>Sampling Scope Mainframe, incl. Full Calibrations (SOLT), TDR with S-Parameter Measurement Digital Filters and Customization</td>
                                <td align="center"><b>25%</b></td>
                            </tr>
                        </table><br /><br />
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <th>Qty</th>
                                <th align="left">Demo Network Analyzer</th>
                                <th>Specifications</th>
                                <th>Discount</th>
                            </tr>
                            <tr>
                                <td align="center" width="50">1</td>
                                <td align="left" width="200"><b>SPARQ-4004E</b></td>
                                <td>Signal Integrity Network Analyzer, 40 GHz, 4-port, Internal Calibration</td>
                                <td align="center" width="100"><b>35%</b></td>
                            </tr>
                        </table><br /><br />
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <th>Qty</th>
                                <th align="left">Used Oscilloscopes</th>
                                <th>Specifications</th>
                                <th>Discount</th>
                            </tr>
                            <tr>
                                <td align="center" width="50">8</td>
                                <td align="left" width="200"><b>WavePro 7300</b></td>
                                <td>3 GHz, 20 GS/s, 4 Ch, 48 Mpts/Ch, 10.4" Color Display<br />One year warranty, Options upon request</td>
                                <td align="center" width="100">&nbsp;</td>
                            </tr>
                        </table><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align="left" style="color: #333333; font-family: Arial; font-size: 13px;">
                            List prices or discounts are subject to change without notice. Actual net prices may vary due to delivery terms, sales tax, VAT, duty and exchange rate variations as well as 
                            local (non-Teledyne LeCroy third party) service and support. Prices excl. VAT. Special offers limited to Teledyne LeCroy direct sales channel only until otherwise mentioned.<br />
                            Promotions not combinable with other promotions or discounts. Please contact local Teledyne LeCroy office for details and conditions.
                        </p>
                        <p align="left" style="color: #333333; font-family: Arial; font-size: 13px;">More information is available at our local Teledyne LeCroy office or via the following <a href="/europe/promotions/requests.asp">Link</a>.</p>
                        <font color="#333333" face="Arial">
                            <p align="left">
                                <font face="Arial" size="2">
                                    Contact us now to get more information:<br />
                                    <b>United Kingdom:</b><br />
                                    Tel. 0162 882 7611 or <a href="mailto:contact.uk@teledynelecroy.com">contact.uk@teledynelecroy.com</a><br /><br />
                                    <b>Other regions:</b><br />Tel. +41-22 719 2170 or <a href="mailto:contact.gmbh@teledynelecroy.com">contact.gmbh@teledynelecroy.com</a><br /><br />
                                    <b>Service &amp; Product Upgrades:</b><br /><a href="mailto:christophe.chappuis@teledynelecroy.com">christophe.chappuis@teledynelecroy.com</a>
                                </font>
                            </p>
                        </font>
                    </td>
                </tr>
            </table>
        </div>
        <%--End Editing Here--%>
    </div>
</asp:Content>