<%@ Language=VBScript %>
<HTML>
	<HEAD>
		<title>Teledyne LeCroy Product Request Page</title> 
		<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
		<!--#include virtual="/cgi-bin/profile/newfunctions.asp" -->
		<!--#include virtual="/include/dbconnstr.asp" -->
		<!--#include virtual="/include/global.asp" -->
		<!--#include virtual="/shopper/functions.asp" -->
		<%Session("localeid")=2057 
Session("RedirectTo") = "/europe/promotions/requests.asp" 'here is the name of the file itself
Session("UserFrom")="er"
If Not ValidateUser Then
	Response.redirect "/cgi-bin/profile/senttoreg.asp?er=y"
End If
ValidateUserNoRedir()

%>
		<meta http-equiv="Content-Type" content="text/html;">
		<style type="text/css"> body { font:Verdana, Arial, Helvetica, sans-serif; font-size:12px; text-align:center; }
	.container { width:700px; margin: 0 auto; text-align:left;}
	h1{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 24px; text-align: left;}
	h2{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size:16px; text-align: center;  }
	h3 { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; }
	.contact{ font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:bold; }
	.print{ font-family:Verdana, Arial, Helvetica, sans-serif; font-size:9px; }
	.wrimage{ margin: 0 0 0 378px; position: absolute; top: 465px; }
	.table{ width: 350px; }
	.promo {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; text-decoration:none;}
	ul{ font-family:Verdana, Arial, Helvetica, sans-serif; }
	li{font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif;}
	p{ font-family:Verdana, Arial, Helvetica, sans-serif; }
	        .style1
            {
                width: 33px;
            }
	        --> 
	</style>
		<script language="javascript">
		    function Validate() {
		        counter = 0;
		        for (var i = 0; i < document.frm.elements.length; i++) {
		            //alert(document.frm.elements[i].checked);
		            if (document.frm.elements[i].checked == false) {
		            }
		            else {
		                counter++;
		            }
		        }
		        if (counter == 0) {
		            alert("Please make a selection");
		            return;
		        }
		        document.frm.submit();
		    }
		</script>
		<!--#include virtual="/include/meta_info.asp" -->
	</HEAD>
	<body topmargin="0" leftmargin="0">
		<div class="container"><br>

			<form action="/goto/europepromo/requests/confirm.asp" method="post" name="frm" id="frm">
			<table width="100%" border="0" cellspacing="2" cellpadding="0">
				
				
						<tr>
							<td valign="top" width="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td valign="top" align="left">
								<table border="0" cellspacing="2" cellpadding="1" ID="Table1" width="532">
									<tr>
										<td colspan="2"><img src="/images/tl_weblogo_blkblue_189x30.png" border="0" width="189" height="30"></td>
										<td >&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3"><h1>Product Request Page</h1></td>
									</tr>
									<tr>
									
										<td colspan="3"><hr align="left" width="600">
										</td>
									</tr>
									<tr>
										<td valign="top" colspan="3"><b>
										<font face="Verdana" size="2">To request 
										further information about Teledyne LeCroy 
										products or an onsite product 
										demonstration, simply click on the boxes 
										and submit the request below.</font></b><br />
										    <br />
										</td>
									</tr>
									<tr>
										<td valign="top" width="102" bgcolor="#808080"><h3>
										<font color="#FFFFFF">
										Product-Demonstration</font></h3></td>
										<td valign="top" class="style1" bgcolor="#808080" width="106"><h3>
										<font color="#FFFFFF">Product-Literature</font></h3></td>
										<td class="promo" valign="top" width="427" bgcolor="#808080">&nbsp;</td>
									</tr>
									
									<tr>
										<td valign="top" >
										    &nbsp;</td>
										<td valign="top" class="style1" width="106">
										    &nbsp;</td>
										<td class="promo" valign="middle" width="427"><h3>
										News</h3></td>
									</tr>
									
									<tr>
										<td valign="top" >
										    &nbsp;</td>
										<td valign="top" class="style1" width="106">
										<input id="lit01" type="checkbox" name="lit01" /></td>
										<td class="promo" valign="middle" width="427">
										<b><font color="red">NEW!</font> 
										Probes &amp; Accessories Catalog</b></td>
									</tr>
									<tr>
										<td valign="top" >
										    &nbsp;</td>
										<td valign="top" class="style1" width="106">
										<input id="lit02" type="checkbox" name="lit02" /></td>
										<td class="promo" valign="middle" width="427">
										<b>Latest Promotions</b></td>
									</tr>
									<tr>
										<td valign="top" >
										    &nbsp;</td>
										<td valign="top" class="style1" width="106">
										<input id="lit03" type="checkbox" name="lit03" /></td>
										<td class="promo" valign="middle" width="427">
										<b>Teledyne LeCroy Product Overview</b></td>
									</tr>
									<tr>
										<td valign="top" >
										    &nbsp;</td>
										<td valign="top" class="style1" width="106">
										&nbsp;</td>
										<td class="promo" valign="middle" width="427">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top"><input id="demo1" name="demo1" type="checkbox" /></td>
										<td valign="top" class="style1" width="106"><input id="lit1" type="checkbox" name="lit1" /></td>
										<td class="promo" valign="middle" width="427"><b><font color="red">NEW</font><font color="#FF0000">!</font></b> High Definition Oscilloscopes: HDO 6000 350 MHz - 1 GHz</td>
									</tr>
                                    <tr>
										<td valign="top"><input id="demo2" name="demo2" type="checkbox" /></td>
										<td valign="top" class="style1" width="106"><input id="lit2" type="checkbox" name="lit2" /></td>
										<td class="promo" valign="middle" width="427"><b><font color="red">NEW</font><font color="#FF0000">!</font></b> High Definition Oscilloscopes: HDO 4000 200 MHz - 1 GHz</td>
									</tr>
									<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
										<td valign="middle" class="promo" width="427">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
									<td class="promo" valign="middle" width="427">
									<h3>Product Series</h3>
										</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo7" name="demo7" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit7" type="checkbox" name="lit7"></td>
										<td class="promo" valign="middle" width="427">
										WaveAce 40 MHz � 300 MHz Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo8" name="demo8" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit8" type="checkbox" name="lit8"></td>
										<td class="promo" valign="middle" width="427">
										WaveJet 100 MHz � 500 MHz Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo9" name="demo9" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit9" type="checkbox" name="lit9"></td>
										<td class="promo" valign="middle" width="427">
										WaveSurfer MXs-B 200 MHz � 1 GHz 
										Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo10" name="demo10" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit10" type="checkbox" name="lit10"></td>
										<td class="promo" valign="middle" width="427">
										WaveRunner 6 Zi 400 MHz � 4 GHz Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top">
										    <input id="demo11" name="demo11" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit11" type="checkbox" name="lit11"></td>
										<td class="promo" valign="middle" width="427">HDO 4000 200 MHz - 1 GHz High Definition Oscilloscopes</td>
									</tr>
                                    <tr>
										<td valign="top">
										    <input id="demo11a" name="demo11a" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit11a" type="checkbox" name="lit11a"></td>
										<td class="promo" valign="middle" width="427">HDO 6000 350 MHz - 1 GHz High Definition Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo12" name="demo12" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit12" type="checkbox" name="lit12"></td>
										<td class="promo" valign="middle" width="427">
										WavePro / SDA 7 Zi-A 1.5 GHz � 6 GHz 
										Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo13" name="demo13" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit13" type="checkbox" name="lit13"></td>
										<td class="promo" valign="middle" width="427">
										WaveMaster / SDA 8 Zi-A 4 GHz � 45 GHz 
										Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo14" name="demo14" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit14" type="checkbox" name="lit14"></td>
										<td class="promo" valign="middle" width="427">
										LabMaster Zi 13 GHz - 65 GHz 
										Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo31" name="demo31" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit31" type="checkbox" name="lit31"></td>
										<td class="promo" valign="middle" width="427">
										SPARQ Network Analyzer</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo15" name="demo15" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit15" type="checkbox" name="lit15"></td>
										<td class="promo" valign="middle" width="427">
										Arbitrary Signal Generators</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo16" name="demo16" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit16" type="checkbox" name="lit16"></td>
										<td class="promo" valign="middle" width="427">Protocol Analyzors</td>
									</tr>
									<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
									<td class="promo" valign="middle" width="427">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
									<td class="promo" valign="middle" width="427"><h3>
									Serial Data and Serial Bus
									Analysis</h3></td>
									</tr>
                                                                       <tr>
										<td valign="top" >
										    <input id="demo17" name="demo17" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit17" type="checkbox" name="lit17"></td>
										<td class="promo" valign="middle" width="427">I2C / SPI /UART /RS232 Bus</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo18" name="demo18" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit18" type="checkbox" name="lit18"></td>
										<td class="promo" valign="middle" width="427">CAN-Bus / LIN-BUS / FlexRay</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo19" name="demo19" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit19" type="checkbox" name="lit19"></td>
										<td class="promo" valign="middle" width="427">
										PSI5 / SENT</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo20" name="demo20" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit20" type="checkbox" name="lit20"></td>
										<td class="promo" valign="middle" width="427">
										MIL-1553-STD</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo21" name="demo21" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit21" type="checkbox" name="lit21"></td>
										<td class="promo" valign="middle" width="427">I2S /Audio Bus</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo22" name="demo22" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit22" type="checkbox" name="lit22"></td>
										<td class="promo" valign="middle" width="427">USB-Bus /Ethernet</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo23" name="demo23" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit23" type="checkbox" name="lit23"></td>
										<td class="promo" valign="middle" width="427">PCIe /SAS /SATA-Bus</td>
									</tr> 										
                                                                        	

<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
										<td valign="middle" class="promo" width="427">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
									<td class="promo" valign="middle" width="427"><h3>
									Signal Analysis Options</h3></td>
									</tr>
                                                                       <tr>
										<td valign="top" >
										    <input id="demo24" name="demo24" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit24" type="checkbox" name="lit24"></td>
										<td class="promo" valign="middle" width="427">Jitter & Timing Analyse / 
										Eye Diagram Software SDAII</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo25" name="demo25" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit25" type="checkbox" name="lit25"></td>
										<td class="promo" valign="middle" width="427">
										QualiPHY Compliance Test Options</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo26" name="demo26" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit26" type="checkbox" name="lit26"></td>
										<td class="promo" valign="middle" width="427">
										EyeDoctor II Signal Integrity Options</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo27" name="demo27" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit27" type="checkbox" name="lit27"></td>
										<td class="promo" valign="middle" width="427">
										Mixed Signal Oscilloscope Options</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo28" name="demo28" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit28" type="checkbox" name="lit28"></td>
										<td class="promo" valign="middle" width="427">
										Spektralanalysis and Advanced FFT Options</td>
									</tr>
									<tr>
										<td valign="top" >
										    <input id="demo29" name="demo29" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit29" type="checkbox" name="lit29"></td>
										<td class="promo" valign="middle" width="427">
										Power Signal Analyse Software PMA2 Options</td>
									</tr>
                                                                        <tr>
										<td valign="top" >
										    <input id="demo30" name="demo30" type="checkbox" /></td>
										<td valign="top" class="style1" width="106">
										<input id="lit30" type="checkbox" name="lit30"></td>
										<td class="promo" valign="middle" width="427">
										EMC Puls Parameter Software Options</td>
									</tr> 										

<tr>
										<td valign="top" >
                                            &nbsp;</td>
										<td valign="top" class="style1" width="106">&nbsp;</td>
										<td valign="middle" class="promo" width="427">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
										<td ><br>
											<input type="button" value="Submit Request" onclick="javascript:Validate();"></td>
									</tr>
									<tr>
										
										<td colspan="3"><hr align="left" width="600">
										</td>
									</tr>
									<tr>
										
										<td valign="top" colspan="3"><b>
										<font face="Verdana" size="2">To request 
										pricing information please click on the 
										following link:</font></b></td>
									</tr>
									<tr>
										
										<td valign="top" colspan="3"><h3>
												<ul>
													<li>
														<a href="/Shopper/RequestQuote/">Price 
														Request</a></li></ul>
											</h3>
										</td>
									</tr>
								</table>
								<br>
								<br>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td valign="top">&nbsp;</td>
						</tr>
				
			</table></form>
			<br>
			<br>
			<br>
		</div>
    <script>
        (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</HTML>