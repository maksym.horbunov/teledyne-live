﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_rental_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="Oscilloscope, Rental, Leasing, Oszilloskop, Vermietung, Oscilloscope, Service de Location, Protocol Analyzer, Oscilloscopi a noleggio" />
    <meta name="description" content="LeCroy is a leading provider of oscilloscopes, protocol analyzers and related test and measurement solutions that enable companies across a wide range of industries to design and test electronic devices of all types." />
    <link rel="stylesheet" href="../content/euro-landing-common.css" />
</head>
<body>
    <div class="header">
        <a href="https://teledynelecroy.com/"><img src="/images/tl_weblogo_blkblue_276x60.png" alt="Teledyne LeCroy" class="logo" /></a>
    </div>
    <div class="container">
        <form id="lecroyform" runat="server">
            <div align="center">
                <img src="./promotion.png" alt="" border="0" />
            </div><br />
            <div class="contact">
                <a name="contact"></a>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="100" rowspan="4" valign="top">
                            <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
                        </td>
                        <td colspan="2">
                            <p class="text"><strong><font color="#0076C0">More Information</font></strong><br />Please use the form below to request more information</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Company:</span></td>
                                    <td width="10">&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                            <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                                    </td>
                                    <td width="10">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Name:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Zip / City:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtZipCity" runat="server" MaxLength="15" Width="260" />
                                            <asp:RequiredFieldValidator ID="rfvZipCity" runat="server" ControlToValidate="txtZipCity" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Country:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:DropDownList ID="ddlCountry" runat="server" />
                                            <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Email:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                            <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Phone:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top"><asp:TextBox ID="txtPhone" runat="server" MaxLength="30" Width="260" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td colspan="4" height="15">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Rental Need:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top">
                                        <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                            <asp:ListItem Text="1-3 months" Value="1-3 months" />
                                            <asp:ListItem Text="3-6 months" Value="3-6 months" />
                                            <asp:ListItem Text="&gt;6 months" Value="More than 6 months" />
                                        </asp:CheckBoxList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td colspan="4" height="15">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small">Remarks:</span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class="text-small" valign="top" width="107"><b>Oscilloscopes:</b></td>
                                    <td class="text-small" width="3">&nbsp;</td>
                                    <td align="left" class="text-small" valign="top">
                                        <asp:CheckBoxList ID="cblScopes" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                            <asp:ListItem Text="<b>100 GHz,</b> 1 Ch, 240 GS/s Oscilloscope" Value="SDA MCM + LM10100" />
                                            <asp:ListItem Text="<b>65 GHz,</b> 4 Ch, 160 GS/s Oscilloscope" Value="SDA MCM + LM1065" />
                                            <asp:ListItem Text="<b>65 GHz,</b> 2 Ch, 160 GS/s Oscilloscope" Value="SDA MCM + LM1065" />
                                            <asp:ListItem Text="<b>36 GHz,</b> 4 Ch, 80 GS/s, Oscilloscope" Value="SDA + LM1036Zi" />
                                            <asp:ListItem Text="<b>20 GHz,</b> 4 Ch, 80 GS/s, Serial Data Analyzer Oscilloscope" Value="SDA820Zi B" />
                                            <asp:ListItem Text="<b>13 GHz,</b> 4 Ch, 40 GS/s, Serial Data Analyzer Oscilloscope" Value="SDA813Zi B" />
                                            <asp:ListItem Text="<b>4 GHz,</b> 10-bit, 4+16 Ch, 40 GS/s, HD Mixed Signal Oscilloscope" Value="HDO9404-MS" />
                                            <asp:ListItem Text="<b>2.5 GHz,</b> 4+16 Ch, 40 GS/s, Mixed Signal Oscilloscope" Value="WR8254M-MS" />
                                            <asp:ListItem Text="<b>1 GHz,</b> 8 Ch, 12-bit, 10 GS/s Motor Drive Analyzer" Value="MDA810-MS" />
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr><td colspan="3" height="15">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small"><b>Probes:</b></span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" class="text-small" valign="top">
                                        <asp:CheckBoxList ID="cblProbes" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                            <asp:ListItem Text="13 GHz Probe System with Solder-In Tip (13 GHz) and Positioner Tip Browser (13 GHz)" Value="D1305-PS" />
                                            <asp:ListItem Text="25 GHz Probe System with Solder-In Tip (25 GHz) and Positioner Tip Browser (22 GHz)" Value="D2505-PS" />
                                            <asp:ListItem Text="30A, 50 MHz High Sensitivity Current Probe - AC/DC, 30 A rms, 50 A Peak Pulse" Value="CP030A" />
                                            <asp:ListItem Text="1kV, 120 MHz High Voltage Differential Probe" Value="HVD3106" />
                                            <asp:ListItem Text="1 Ch, 100 MHz Differential Amplifier with Precision Voltage Source" Value="DA1855" />
                                            <asp:ListItem Text="4.0 GHz, 0.6 pF, 1 MOhm Active Voltage Probe" Value="ZS4000" />
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr><td colspan="3" height="15">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top"><span class="text-small"><b>Protocol Analyzers:</b></span></td>
                                    <td>&nbsp;</td>
                                    <td align="left" class="text-small" valign="top">
                                        <asp:CheckBoxList ID="cblProtocol" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                            <asp:ListItem Text="USB 2.0 / USB 3.1 (GEN1 & GEN 2) Protocol Analyser & Exerciser" Value="Voyager" />
                                            <asp:ListItem Text="GEN1/2/3/4 up to 8 lanes PCI Express Analyser Test system" Value="Summit" />
                                            <asp:ListItem Text="4 port SAS/SATA (6G), SAS (12G/24G) Analyser Test system" Value="Sierra" />
                                            <asp:ListItem Text="10G/40G Ethernet Analyser System incl. Jammer function" Value="SierraNet" />
                                            <asp:ListItem Text="Bluetooth 5 Classic and Low Energy Protocol Analyser" Value="Sodera" />
                                            <asp:ListItem Text="Near Field Communication Analyser" Value="NFC Analyser" />
                                            <asp:ListItem Text="HDMI & DisplayPort Test system" Value="QD780E" />
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr><td colspan="3" height="15">&nbsp;</td></tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" style="background-color:#0076C0; color:#ffffff;" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>