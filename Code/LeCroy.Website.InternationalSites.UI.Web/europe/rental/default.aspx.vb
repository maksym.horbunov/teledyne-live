﻿Imports LeCroy.Website.BLL
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common

Public Class europe_rental_default
    Inherits EUPageBase

#Region "Variables/Keys"
    Private Const EMAIL_ADDRESS_FROM As String = "webmaster@teledynelecroy.com"
    Private Const EMAIL_ADDRESS_TO As String = "marketing.europe@teledynelecroy.com"
    Private Const EMAIL_SUBJECT As String = "Euro Landing Page Request - Europe|Rental|{0}|{1}|{2}"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then DropDownListManager.BindDropDown(Of Country)(ddlCountry, CountryRepository.GetAllCountries(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToList(), "Name", "Name", True, "Select Country", String.Empty)
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvEmail_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmail.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtEmail.Text)) Then
            Return
        End If
        If Not (Utilities.checkValidEmail(txtEmail.Text)) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvCountry_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvCountry.ServerValidate
        args.IsValid = False
        If (ddlCountry.SelectedIndex <= 0) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            Dim actions As List(Of CheckBoxList) = New List(Of CheckBoxList)
            actions.Add(cblActions)
            Dim selections As List(Of CheckBoxList) = New List(Of CheckBoxList)
            selections.Add(cblScopes)
            selections.Add(cblProbes)
            selections.Add(cblProtocol)
            BuildAndSendEmail(txtCompany.Text, txtName.Text, txtZipCity.Text, ddlCountry.SelectedValue, txtEmail.Text, txtPhone.Text, GetStringFromCheckBoxList(actions), GetStringFromCheckBoxList(selections), txtRemark.Text)
            ClearForm()
            lblSuccess.Visible = True
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BuildAndSendEmail(ByVal company As String, ByVal name As String, ByVal zipCity As String, ByVal country As String, ByVal email As String, ByVal phone As String, ByVal action As String, ByVal selections As String, ByVal remarks As String)
        Dim sb As StringBuilder = New StringBuilder()
        sb.AppendFormat("Company: {0}{8}Name: {1}{8}Zip/City: {2}{8}Country: {3}{8}Email: {4}{8}Phone: {5}{8}{8}Rental Need(s): {6}{8}{8}Selected:{9}{8}{8}Remarks: {7}", company, name, zipCity, country, email, phone, action, remarks, Environment.NewLine, selections)
        Utilities.SendEmail(New List(Of String)({EMAIL_ADDRESS_TO}), EMAIL_ADDRESS_FROM, String.Format(EMAIL_SUBJECT, name, country, action), sb.ToString(), New List(Of String), New List(Of String)({"james.chan@teledyne.com"}))
    End Sub

    Private Function GetStringFromCheckBoxList(ByVal ctrls As List(Of CheckBoxList)) As String
        Dim sb As StringBuilder = New StringBuilder
        For Each ctrl In ctrls
            For Each li As ListItem In ctrl.Items
                If (li.Selected) Then
                    sb.Append(String.Format("{0},", li.Value))
                End If
            Next
        Next
        Dim retVal As String = sb.ToString()
        If Not (String.IsNullOrEmpty(retVal)) Then
            retVal = retVal.Substring(0, retVal.Length - 1)
        End If
        Return retVal
    End Function

    Private Sub ClearForm()
        Dim txtBoxes As List(Of TextBox) = New List(Of TextBox)({txtCompany, txtName, txtZipCity, txtEmail, txtPhone, txtRemark})
        For Each txtBox As TextBox In txtBoxes
            txtBox.Text = String.Empty
        Next
        Dim cbls As List(Of CheckBoxList) = New List(Of CheckBoxList)({cblActions, cblScopes, cblProbes, cblProtocol})
        For Each cbl As CheckBoxList In cbls
            If (cbl.Items.Count > 0) Then
                For Each li As ListItem In cbl.Items
                    If (li.Selected) Then li.Selected = False
                Next
            End If
        Next
        ddlCountry.SelectedIndex = 0
    End Sub
#End Region

End Class