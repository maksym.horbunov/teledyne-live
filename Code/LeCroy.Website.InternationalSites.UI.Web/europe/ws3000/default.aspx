﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.europe_ws3000_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left"><a href="https://teledynelecroy.com/europe/promotions/promotions.aspx" target="_blank"><img src="https://assets.lcry.net/images/home_ws3000_promo_eng.png" alt="" border="0" height="200" style="transform: rotate(-20deg); -webkit-transform: rotate(-20deg); -moz-transform: rotate(-20deg);" /></a></td>
            <td class="headline" colspan="2">
                200 MHz – 750 MHz<br /><font color="#0076C0" class="headline2">WaveSurfer 3000</font>
            </td>
        </tr>
        <tr>
            <td width="375">&nbsp;</td>
            <td width="295" align="left" valign="top">
                <p><img src="https://assets.lcry.net/images/home_ws3000_price_eng.jpg" alt="price" /></p>
                <div style="float: right; padding-right: 15px; padding-top: 140px;"><a href="https://teledynelecroy.com/doc/docview.aspx?id=9094" target="_blank"><img src="../content/icon-pdf-color.jpg" alt="pdf" /></a></div>
            </td>
            <td height="375" align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Key Features</font></strong></p>
                <ul>
                    <li class="text">200 MHz, 350 MHz, 500, and 750 MHz</li>
                    <li class="text">Up to 4 GS/s Sample Rate</li>
                    <li class="text">Long Memory – 10 Mpts/Ch</li>
                    <li class="text">10.1" Touch Screen</li>
                    <li class="text">Superior Toolset
                        <ul>
                            <li>WaveScan - Advanced Search and Find</li>
                            <li>History Mode - Waveform Playback</li>
                            <li>Segmented Memory</li>
                            <li>LabNotebook Documentation</li>
                            <li>Advanced Math and Measure</li>
                        </ul>
                    </li>
                    <li class="text">Multi-instrument capabilities
                        <ul>
                            <li>Protocol Analysis – Serial Trigger and Decode</li>
                            <li>Waveform Generation – Built-in Arbitrary Generator</li>
                            <li>Logic Analysis – 16 Channel MSO</li>
                            <li>Digital Voltmeter</li>
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="4" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text"><strong><font color="#0076C0">More Information</font></strong><br />Please use the form below to request more information</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Company:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Country:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Action:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Documentation" Value="Documentation" />
                                <asp:ListItem Text="Quotation" Value="Quotation" />
                                <asp:ListItem Text="Product Demonstration" Value="Product Demonstration" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Remarks:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Yes, I want to subscribe to<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the Teledyne LeCroy newsletter" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Models:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>WS3022</b>, 200 MHz, 2 Ch., 4 GS/s, 10 Mpts/Ch" Value="WS3022" />
                                <asp:ListItem Text="<b>WS3024</b>, 200 MHz, 4 Ch., 4 GS/s, 10 Mpts/Ch" Value="WS3024" />
                                <asp:ListItem Text="<b>WS3034</b>, 350 MHz, 4 Ch., 4 GS/s, 10 Mpts/Ch" Value="WS3034" />
                                <asp:ListItem Text="<b>WS3054</b>, 500 MHz, 4 Ch., 4 GS/s, 10 Mpts/Ch" Value="WS3054" />
                                <asp:ListItem Text="<b>WS3074</b>, 750 MHz, 4 Ch., 4 GS/s, 10 Mpts/Ch" Value="WS3074" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Options:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblOptions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>WS3K-MSO</b>, 16 Channel Mixed Signal Option" Value="WS3K-MSO" />
                                <asp:ListItem Text="<b>WS3K-FG</b>, Built-in Function Generator" Value="WS3K-FG" />
                                <asp:ListItem Text="<b>WS3K-EMB</b>, I2C, SPI, UART and RS-232 Trigger and Decode Package" Value="WS3K-EMB" />
                                <asp:ListItem Text="<b>WS3K-AUTO</b>, CAN and LIN Trigger and Decode Option" Value="WS3K-AUTO" />
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" style="background-color:#0076C0; color:#ffffff;" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>