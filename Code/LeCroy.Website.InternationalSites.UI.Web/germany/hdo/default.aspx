﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.germany_hdo_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td height="20" colspan="2">&nbsp;</td></tr>
        <tr>
            <td colspan="2" style="padding-left: 375px;">
		<span style="float: right;"><a href="/germany/hdo-demo-promotion.html"><img src="https://assets.lcry.net/images/landing_hdo_promo_ger.png" alt="" border="0" height="150" /></a></span>
                <font color="#0076C0" style="font-size: 24pt;">High Definition Oszilloskope</font><br />
                <span style="font-size: 20pt;">200 MHz – 1 GHz mit True 12 Bit Hardware-<br />Auflösung und bis zu 8 Analog-Kanälen</span><br />
                <span style="font-size: 16pt;"><b style="color: #ff0000;">NEU</b> - Komplete Power Analyse an 3-Phasen Antrieben - MDA800!</span>
            </td>
        </tr>
        <tr><td height="10" colspan="2">&nbsp;</td></tr>
        <tr>
            <td width="800" align="left" valign="top">
                <img src="../content/hdo-familyshot.png" alt="hdo family" width="800" height="282" usemap="#familyshot" />
                <map id="familyshot" name="familyshot">
                    <area shape="rect" coords="167,210,204,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8822" alt="pdf" />
                    <area shape="rect" coords="359,210,396,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8522" alt="pdf" />
                    <area shape="rect" coords="545,210,582,261" href="https://teledynelecroy.com/doc/docview.aspx?id=8831" alt="pdf" />
                    <area shape="rect" coords="730,210,767,261" href="https://teledynelecroy.com/doc/docview.aspx?id=9214" alt="pdf" />
                </map>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <p class="text-small"><strong>Gestochen scharfe Signaldarstellung<br /></strong>Signale werden dank HD4096 Technologie und 12-bit Hardware extrem detailliert angezeigt.</p>
                            <p class="text-small"><strong>Mehr Signaldetails<br /></strong>Signaldetails, die sonst im Rauschen untergehen, werden klar dargestellt und können nun analysiert werden.</p>
                            <p class="text-small"><strong>Hochpräzise Messungen<br /></strong>Die extrem genaue Signalerfassung der 12 Bit HD4096 Technologie erlaubt hochpräzise Messungen für eine noch schnellere Fehlersuche und -behebung.</p>
                        </td>
                        <td width="20">&nbsp;</td>
                        <td><img src="../content/hdo_quantization_xsm.png" width="400" height="184" alt="waveform" /></td>
                        <td width="25">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Produkt-Highlights:</font></strong></p>
                <ul>
                    <li class="text">200 MHz, 350 MHz, 500 MHz und 1 GHz</li>
                    <li class="text">Bis zu 8 analoge Kanäle</li>
                    <li class="text">12-bit AD-Wandler Auflösung, bis 15 Bit</li>
                    <li class="text">Großer Speicher – bis 250 Mkpt/K.</li>
                    <li class="text">12,1" touchscreen</li>
                    <li class="text">16 interne digitale Kanäle mit der MSO Option</li>
                    <li class="text">Umfangreiche Analyse- und Dokumentations-Tools</li>
                    <li class="text">Erweiterte Trigger mit TriggerScan und Parameter-Trigger</li>
                    <li class="text">Serielle Trigger & Decode und Debug Optionen</li>
                    <li class="text">Leistungsstarke Standardfunktionen</li>
                    <ul>
                        <li>Spektrum Analyzer Option</li>
                        <li>WaveScan Fehlersuche</li>
                        <li>LabNotebook Ergebnisdokumentation und Reporterstellung</li>
                        <li>History Mode Signal Abspielfunktion</li>
                    </ul>
                    <li class="text">
                        <p><strong>Motor Drive Analyzer MDA800</strong> für die komplette Power Analyse an 3-Phasen Antriebssystemen</p>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="2" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text">
                    <strong><font color="#0076C0">Mehr Information</font></strong><br />
                    Fordern Sie weitere Information mit dem folgenden Formular an:</p>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Firma:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Land:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Anfrage:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Produktunterlagen" Value="Produktunterlagen" />
                                <asp:ListItem Text="Angebot" Value="Angebot" />
                                <asp:ListItem Text="Produktvorführung" Value="Produktvorführung" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Nachricht:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Ja, ich möchte den<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teledyne LeCroy Newsletter erhalten" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Modelle:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>HDO4022</b>, 350 MHz, 2 K., 12 Bit, 50 Mpkt/K." Value="HDO4022" />
                                <asp:ListItem Text="<b>HDO4024</b>, 200 MHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO4024" />
                                <asp:ListItem Text="<b>HDO4034</b>, 350 MHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO4034" />
                                <asp:ListItem Text="<b>HDO4054</b>, 500 MHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO4054" />
                                <asp:ListItem Text="<b>HDO4104</b>, 1 GHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO4104" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 digitale Kanäle<br /><br />" Value="MS Option HDO4000" />
                                <asp:ListItem Text="<b>HDO6034</b>, 350 MHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO6034" />
                                <asp:ListItem Text="<b>HDO6054</b>, 500 MHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO6054" />
                                <asp:ListItem Text="<b>HDO6104</b>, 1 GHz, 4 K., 12 Bit, 50 Mpkt/K." Value="HDO6104" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 digitale Kanäle<br /><br />" Value="MS Option HDO6000" />
                                <asp:ListItem Text="<b>HDO8038</b>, 350 MHz, 8 K., 12 Bit, 50 Mpkt/K." Value="HDO8038" />
                                <asp:ListItem Text="<b>HDO8058</b>, 500 MHz, 8 K., 12 Bit, 50 Mpkt/K." Value="HDO8058" />
                                <asp:ListItem Text="<b>HDO8108</b>, 1 GHz, 8 K., 12 Bit, 50 Mpkt/K." Value="HDO8108" />
                                <asp:ListItem Text="<b>MS Option</b>, 16 digitale Kanäle<br /><br />" Value="MS Option HDO8000" />
                                <asp:ListItem Text="<b>MDA803</b>, 350 MHz, 8 K., 12 Bit, 50 Mpkt/K." Value="MDA803" />
                                <asp:ListItem Text="<b>MDA805</b>, 500 MHz, 8 K., 12 Bit, 50 Mpkt/K." Value="MDA805" />
                                <asp:ListItem Text="<b>MDA810</b>, 1 GHz, 8 K., 12 Bit, 50 Mpkt/K." Value="MDA810" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>