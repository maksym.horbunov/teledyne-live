﻿Imports LeCroy.Website.BLL
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common

Public Class germany_hdo_default
    Inherits DEPageBase

#Region "Variables/Keys"
    Private Const EMAIL_ADDRESS_FROM As String = "webmaster@teledynelecroy.com"
    Private Const EMAIL_ADDRESS_TO As String = "marketing.europe@teledynelecroy.com"
    Private Const EMAIL_SUBJECT As String = "German Landing Page Request - Europe|HDO|{0}|{1}|{2}"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim control As HtmlLink = New HtmlLink()
        control.Href = ResolveUrl("../content/euro-landing-hdo.css")
        control.Attributes.Add("rel", "stylesheet")
        control.Attributes.Add("type", "text/css")
        CType(Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
        If Not (IsPostBack) Then DropDownListManager.BindDropDown(Of Country)(ddlCountry, CountryRepository.GetAllCountries(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToList(), "Name", "Name", True, "Select Country", String.Empty)
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvEmail_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmail.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtEmail.Text)) Then
            Return
        End If
        If Not (Utilities.checkValidEmail(txtEmail.Text)) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvCountry_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvCountry.ServerValidate
        args.IsValid = False
        If (ddlCountry.SelectedIndex <= 0) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvModel_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvModel.ServerValidate
        args.IsValid = False
        If Not (cblModels.Items.Cast(Of ListItem)().Any(Function(item) item.Selected)) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            BuildAndSendEmail(txtCompany.Text, txtName.Text, txtEmail.Text, ddlCountry.SelectedValue, GetStringFromCheckBoxList(cblActions), txtRemark.Text, GetStringFromCheckBoxList(cblModels))
            If (cbxOptIn.Checked) Then GenerateSubscriptionToOptedIn(String.Empty, 11, 70, SQLStringWithOutSingleQuotes(txtEmail.Text))
            ClearForm()
            lblSuccess.Visible = True
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BuildAndSendEmail(ByVal company As String, ByVal name As String, ByVal email As String, ByVal country As String, ByVal action As String, ByVal remarks As String, ByVal models As String)
        Dim sb As StringBuilder = New StringBuilder()
        sb.AppendFormat("Company: {0}{7}Name: {1}{7}Email: {2}{7}Country: {8}{7}Action: {3}{7}Remarks: {4}{7}{7}Models: {5}{7}{7}Options: {6}", company, name, email, action, remarks, models, String.Empty, Environment.NewLine, country)
        Utilities.SendEmail(New List(Of String)({EMAIL_ADDRESS_TO}), EMAIL_ADDRESS_FROM, String.Format(EMAIL_SUBJECT, name, country, action), sb.ToString(), New List(Of String), New List(Of String)({"james.chan@teledyne.com"}))
    End Sub

    Private Function GetStringFromCheckBoxList(ByVal ctrl As CheckBoxList) As String
        Dim sb As StringBuilder = New StringBuilder
        For Each li As ListItem In ctrl.Items
            If (li.Selected) Then
                sb.Append(String.Format("{0},", li.Value))
            End If
        Next
        Dim retVal As String = sb.ToString()
        If Not (String.IsNullOrEmpty(retVal)) Then
            retVal = retVal.Substring(0, retVal.Length - 1)
        End If
        Return retVal
    End Function

    Private Sub ClearForm()
        Dim txtBoxes As List(Of TextBox) = New List(Of TextBox)({txtCompany, txtName, txtEmail, txtRemark})
        For Each txtBox As TextBox In txtBoxes
            txtBox.Text = String.Empty
        Next
        Dim cbls As List(Of CheckBoxList) = New List(Of CheckBoxList)({cblActions, cblModels})
        For Each cbl As CheckBoxList In cbls
            If (cbl.Items.Count > 0) Then
                For Each li As ListItem In cbl.Items
                    If (li.Selected) Then li.Selected = False
                Next
            End If
        Next
        ddlCountry.SelectedIndex = 0
        cbxOptIn.Checked = True
    End Sub
#End Region
End Class