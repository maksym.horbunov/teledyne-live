﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.germany_mda_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td height="30" colspan="2"></td></tr>
        <tr>
            <td height="30" colspan="2">
                <font color="#0076C0" style="font-size: 24pt;">Motor Drive Analyzer MDA800</font><br />
                <span style="font-size: 20pt;">
                    Komplette Power Analyse an<br />3-Phasen Antriebssystemen
                </span>
            </td>
        </tr>
        <tr>
            <td width="710" height="30"></td>
            <td></td>
        </tr>
        <tr>
            <td width="710" align="left" valign="top">
                <p><img src="../content/mda-2015-brushless.jpg" width="601" height="475" alt="MDA800" /></p>
            </td>
            <td align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Hauptmerkmale:</font></strong></p>
                <ul>
                    <li class="text"><strong><u>Statische</u> 3-Phasen Berechnungen wie ein Power Analyzer</strong></li>
                    <li class="text"><strong><u>Dynamische</u> 3-Phasen Power Analyse mit mechanischer Motor Analyse</strong></li>
                    <li class="text"><strong><u>Debug</u> an eingebetteten Steuerungssystemen mit hoher Bandbreite (1 GHz)</strong><br /></li>
                    <li class="text">350 MHz, 500 MHz und 1 GHz </li>
                    <li class="text">8 Kanäle</li>
                    <li class="text">12-bit AD-Wandler Auflösung, bis 15 Bit</li>
                    <li class="text">Großer Speicher – bis 250 Mkpt/K.</li>
                    <li class="text">12,1" Touchscreen</li>
                    <li class="text">16 interne digitale Kanäle mit MSO Option</li>
                    <li class="text">Umfangreiche Analyse- und Dokumentations-Tools</li>
                    <li class="text">Erweiterte Trigger mit TriggerScan und Mess-Trigger</li>
                    <li class="text">Serielle Trigger & Decode und Debug Optionen</li>
                    <li class="text">Weitere Ausstattungen<br />
                        <ul>
                            <li class="text">Power Analyse Option</li>
                            <li class="text">Spektrum Analyzer Option</li>
                            <li class="text">WaveScan Fehlersuche</li>
                            <li class="text">LabNotebook Ergebnisdokumentation und Reporterstellung</li>
                            <li class="text">History Mode Signal Abspielfunktion</li>
                        </ul>
                    </li>
                </ul>
                <p class="text-small">
                    <br />
                    <img src="../content/icon-pdf-color.png" width="16" height="16" alt="pdf" />&nbsp;<a href="https://teledynelecroy.com/doc/docview.aspx?id=8822" target="_blank">&nbsp;MDA800 Datenblatt</a><br />
                    <%--<img src="../content/icon-pdf-color.png" width="16" height="16" alt="pdf" />&nbsp;Product Overview</p>--%>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#c8c8c8"></td>
            <td height="1" align="left" valign="top" bgcolor="#c8c8c8"></td>
        </tr>
        <tr>
            <td colspan="2" align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" colspan="5" valign="middle" bgcolor="#94C020">
                            <span class="text-inline">THE MOTOR DRIVE ANALYZER - A NEW CLASS OF INSTRUMENT</span>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" colspan="5" align="left" valign="top" class="text-small"></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="text-small">
                            <p>
                                <strong>Messgeräte Evolution</strong>
                                <br />Der Motor Drive Analyzer verfügt über die Bandbreite (bis 1 GHz), Eingänge (8 analoge Kanäle + 16 optionale digitale Kanäle), den Erfassungsspeicher (bis 250 Mpkt/K.), um jedes Signal zu erfassen, egal ob ein schnelles Signal der eingbetteten Steuereinheit, Signale des Powersystems oder ein langsames mechanisches Signal. Im Anschluß werden umfassende 3-Phasen- oder mechanische Power Analysen durchgeführt, weit über die eines Power Analyzers hinaus. Ein Erfassungssystem bedeutet ein Ergebnis auf einem Display und ein schnelleres Verständnis des Problems.</p>
                        </td>
                        <td width="30"></td>
                        <td align="left" valign="top"><img src="../content/480v-drive-io.jpg" width="250" height="156" alt="screen1" /></td>
                        <td width="15" align="left" valign="top"></td>
                        <td align="left" valign="top"><img src="../content/milwaukee-drill3.jpg" width="250" height="156" alt="screen2" /></td>
                    </tr>
                    <tr>
                        <td height="15" colspan="5" align="left" valign="top" class="text-small">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="2" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text"><strong><font color="#0076C0">Mehr Information</font></strong><br />Fordern Sie weitere Information mit dem folgenden Formular an:</p>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Firma:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Land:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Anfrage:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Produktunterlagen" Value="Produktunterlagen" />
                                <asp:ListItem Text="Angebot" Value="Angebot" />
                                <asp:ListItem Text="Produktvorführung" Value="Produktvorführung" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Nachricht:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Ja, ich möchte den<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teledyne LeCroy Newsletter erhalten" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Modelle:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>MDA803</b>, 350 MHz, 8 K., 12-bit, 50 Mpkt/K." Value="MDA803" />
                                <asp:ListItem Text="<b>MDA805</b>, 500 MHz, 8 K., 12-bit, 50 Mpkt/K." Value="MDA805" />
                                <asp:ListItem Text="<b>MDA810</b>, 1 GHz, 8 K., 12-bit, 50 Mpkt/K." Value="MDA810" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Optionen:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblOptions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>MSO</b>, digitale Kanäle Mixed-Signal Option" Value="MSO" />
                                <asp:ListItem Text="<b>L</b>, 100 Mpkt/K. SpeicherOption" Value="L" />
                                <asp:ListItem Text="<b>XL</b>, 250 Mpkt/K. SpeicherOption" Value="XL" />
                                <asp:ListItem Text="<b>PWR</b>, Power Analyse Option" Value="PWR" />
                                <asp:ListItem Text="<b>EMB</b>, I2C, SPI, UART, RS-232 Trigger/Decode" Value="EMB" />
                                <asp:ListItem Text="<b>VBA</b>, Vehicle Bus Analyzer Paket: CAN TDM, CAN Symbolic, FlexRay TDP, LIN TD und Protobus MAG" Value="VBA" />
                                <asp:ListItem Text="<b>ProtoBus</b>, Serial Debug Toolkit – Messen. Analyzieren, Darstellen" Value="ProtoBus" />
                                <asp:ListItem Text="<b>XDEV</b>, Developer's Tool Kit Option" Value="XDEV" />
                                <asp:ListItem Text="<b>JITKIT</b>, Clock, Clock-Data Jitter Analyse und Darstellung von Überlagerungen für Zeit, Statistik, Spektrum und Jitter Serielle Trigger/Decode Optionen für nahezu alle Standards sind erhältlich." Value="JITKIT" />
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Request" ValidationGroup="vgSubmit" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>