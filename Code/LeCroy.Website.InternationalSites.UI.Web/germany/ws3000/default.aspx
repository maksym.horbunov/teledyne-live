﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/simple_site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.germany_ws3000_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left"><a href="https://teledynelecroy.com/germany/promotions/promotions.aspx" target="_blank"><img src="https://assets.lcry.net/images/home_ws3000_promo_ger.png" alt="" border="0" height="200" style="transform: rotate(-20deg); -webkit-transform: rotate(-20deg); -moz-transform: rotate(-20deg);" /></a></td>
            <td class="headline" colspan="2">
                200 MHz – 750 MHz<br /><font color="#0076C0" class="headline2">WaveSurfer 3000</font>
            </td>
        </tr>
        <tr>
            <td width="375">&nbsp;</td>
            <td width="295" align="left" valign="top">
                <p><img src="https://assets.lcry.net/images/home_ws3000_price_ger.jpg" alt="price" /></p>
                <div style="float: right; padding-right: 15px; padding-top: 140px;"><a href="https://teledynelecroy.com/doc/docview.aspx?id=9094" target="_blank"><img src="../content/icon-pdf-color.jpg" alt="pdf" /></a></div>
            </td>
            <td height="375" align="left" valign="top">
                <p class="text"><strong><font color="#0076C0">Produkt-Highlights</font></strong></p>
                <ul>
                    <li class="text">200 MHz, 350 MHz, 500 und 750 MHz</li>
                    <li class="text">4 GS/s Abtastrate</li>
                    <li class="text">Großer Speicher - 10 Mpkt/K.</li>
                    <li class="text">10,1" Touchscreen</li>
                    <li class="text">Serienmässige Tools für die Signalanalyse
                        <ul>
                            <li>WaveScan – Fehlersuche</li>
                            <li>History Mode – Signalaufzeichnung</li>
                            <li>Segmentierter Speicher</li>
                            <li>LabNotebook Dokumentation</li>
                            <li>Umfangreiche Messungen und Mathematik</li>
                        </ul>
                    </li>
                    <li class="text">Multifunktionalität 
                        <ul>
                            <li>Protokoll Analyse - serielle Trigger und Decoder</li>
                            <li>Signalgenerierung - integrierter arbiträtrer Signalgenerator</li>
                            <li>Logik Analyse - 16 Kanal MSO</li>
                            <li>Digitales Voltmeter (serienmäßig)</li>
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContactContent" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="100" rowspan="4" valign="top">
                <p><img src="../content/button-info.png" width="76" height="77" alt="" /></p>
            </td>
            <td colspan="2">
                <p class="text"><strong><font color="#0076C0">Mehr Information</font></strong><br />Fordern Sie weitere Information mit dem folgenden Formular an:</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Firma:</span></td>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Name:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="75" Width="260" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Email:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="260" />
                                <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Land:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                <asp:CustomValidator ID="cvCountry" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="vgSubmit" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Anfrage:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                            <asp:CheckBoxList ID="cblActions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="Produktunterlagen" Value="Produktunterlagen" />
                                <asp:ListItem Text="Angebot" Value="Angebot" />
                                <asp:ListItem Text="Produktvorführung" Value="Produktvorführung" />
                            </asp:CheckBoxList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Nachricht:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:TextBox ID="txtRemark" runat="server" Columns="34" MaxLength="500" Rows="3" TextMode="MultiLine" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr><td colspan="4" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top"><asp:CheckBox ID="cbxOptIn" runat="server" Checked="True" Text="Ja, ich möchte den<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teledyne LeCroy Newsletter erhalten" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="text-small" valign="top">Modelle:</td>
                        <td class="text-small" width="10">&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblModels" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>WS3022</b>, 200 MHz, 2 Kanäle, 4 GS/s, 10 Mpkt/K." Value="WS3022" />
                                <asp:ListItem Text="<b>WS3024</b>, 200 MHz, 4 Kanäle, 4 GS/s, 10 Mpkt/K." Value="WS3024" />
                                <asp:ListItem Text="<b>WS3034</b>, 350 MHz, 4 Kanäle, 4 GS/s, 10 Mpkt/K." Value="WS3034" />
                                <asp:ListItem Text="<b>WS3054</b>, 500 MHz, 4 Kanäle, 4 GS/s, 10 Mpkt/K." Value="WS3054" />
                                <asp:ListItem Text="<b>WS3074</b>, 750 MHz, 4 Kanäle, 4 GS/s, 10 Mpkt/K." Value="WS3074" />
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*Please select a model" ValidationGroup="vgSubmit" />
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td align="left" valign="top"><span class="text-small">Optionen:</span></td>
                        <td>&nbsp;</td>
                        <td align="left" class="text-small" valign="top">
                            <asp:CheckBoxList ID="cblOptions" runat="server" CausesValidation="false" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                                <asp:ListItem Text="<b>WS3K-MSO</b>, 16 digitale Kanäle" Value="WS3K-MSO" />
                                <asp:ListItem Text="<b>WS3K-FG</b>, Integrierter arbiträrer Signalgenerator" Value="WS3K-FG" />
                                <asp:ListItem Text="<b>WS3K-EMB</b>, I2C, SPI, UART, RS-232 Trigger/Decoder" Value="WS3K-EMB" />
                                <asp:ListItem Text="<b>WS3K-AUTO</b>, CAN und LIN Trigger/Decoder" Value="WS3K-AUTO" />
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr><td colspan="3" height="15">&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Anfrage abschicken" ValidationGroup="vgSubmit" style="background-color:#0076C0; color:#ffffff;" />&nbsp;&nbsp;<asp:Label ID="lblSuccess" runat="server" CssClass="success" Text="Your request has been submitted" Visible="false" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>