﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="promotions.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.it_promotions_promotions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="content" style="background-color: #ffffff;">
        <%--Start Editing Here--%>
        <div style="margin-top: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><span style="text-transform: uppercase"><font face="Arial" size="5">TELEDYNE LECROY ITALY</font></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><span style="text-transform: uppercase"><font face="Arial" size="5">Special Offers</font></span><br />All Offers are valid until 18 December 2020</td>
                </tr>
                <tr><td>&nbsp;</td></tr>
              <tr>
                    <td><table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr style="background-color: #c5d9f1; color: #000000;">
                                <td><b>Product</b></td>
                                <td><b>Performance Characteristics</b></td>
                                <td>&nbsp;</td>
                            </tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">8-Bit to 12-Bit HD Oscilloscope Trade-In</td>
                                <td>Trade Up Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>Any LeCroy 8-Bit Oscilloscope</b></td>
                                <td valign="top" width="608">Trade in your 8-Bit Oscilloscope to a 12-Bit HD Oscilloscope incl. EMB-Option<br />(12-Bit Oscilloscope from HDO6000A and >500 MHz)</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">30%</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">Trade in your Oscilloscope to an 8 Channel 12-Bit HDO8000A - limited quantity, new units</td>
                                <td>Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>Any Oscilloscope</b></td>
                                <td valign="top" width="608">Trade in your Oscilloscope to an 8-Channel 12-Bit HD Oscilloscope incl. EMB Option: I2C, SPI, UART, RS-232 Trigger &amp; Decode. Already purchased options will be included for FREE</td>
                                <td align="center" style="color: red; font-weight: bold;" valign="top" width="104">55%</td>
                            </tr><tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">  HDO8000A 8 Channel 12-Bit Oscilloscope - limited quantity, new units</td>
                                <td>Demo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>HDO8000A/MDA800A</b></td>
                                <td valign="top" width="608"><strong>8-Channel 12-Bit HD Oscilloscope, 350 MHz - 1 GHz, 10 GS/S, 50 Mpts/Ch, 12.1" Touch Screen<br />incl. EMB Option: I2C, SPI, UART, RS-232 Trigger & Decode.</strong></td>
                                <td align="center" style="color: red; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                             <tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">2.5 GHz - 4 GHz WaveRunner 8000/9000 Oscilloscopes</td>
                                <td>Demo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveRunner 8404M-MS</b></td>
                                <td valign="top" width="608"><strong> 4 GHz Oscilloscope, 4 Ch + 16 Digital Ch., 40 GS/s, 128 Mpts/Ch, 12.1" Touch Screen</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                           <tr>
                                <td valign="top" width="210"><b>WaveRunner 8404M-R</b></td>
                                <td valign="top" width="608"><strong>4 GHz Oscilloscope, 2U form factor without display, 4 Ch + 16 Digital Ch., 40 GS/s, 128 Mpts/Ch</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                           <tr>
                                <td valign="top" width="210"><b>WaveRunner 9254M-MS</b></td>
                                <td valign="top" width="608"><strong>2.5 GHz Oscilloscope, 4 Ch. + 16 Digital Ch., 40 GS/s, 128 Mpts/Ch, 15.4&quot; Touch Screen</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">1 GHz HD 12-Bit HDO6000A Oscilloscopes</td>
                                <td>Demo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>HDO6104A-MS</b></td>
                                <td valign="top" width="608"><strong>1 GHz</strong> 12-bit HD Oscilloscope, 4 Ch + 16 Digital Ch., 10 GS/s, 50 Mpts/Ch, 12.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">45%</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2" style="height: 26px">High Voltage Fiber Optic Probe </td>
                                <td style="height: 26px">Demo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>HVFO103</b></td>
                                <td valign="top" width="608"><strong>60 MHz</strong> High Voltage Fiber Optically-isolated Probes</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">45%</td>
                            </tr>
                           <tr>
                             <td colspan="3">&nbsp;</td></tr>
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">6 GHz - 65 GHz WaveMaster 8 Zi-B &amp; LabMaster 10 Zi-A Oscilloscopes</td>
                                <td>Promo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>LabMaster 10 Zi-A</b></td>
                                <td valign="top" width="608"><b>36 GHz / 65 GHz Oscilloscope, 4 Ch, 160 GS/s, 80 Mpts/Ch, 15.3" Touch Screen</b></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveMaster 8 Zi-B</b></td>
                                <td valign="top" width="608"><b>6 GHz - 30 GHz Oscilloscopes, 4 Ch., 80 GS/s, 128 Mpts/Ch, 50 &#8486; and 1 M&#8486; Input, 15.3" Touch Screen</b></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included Options:</div></td>
                                <td valign="top"><b>JITKIT Option</b>: Clock, Clock-Data Jitter Analysis and Views<br />
                                    <b>COMPLETELINQ</b>: Bundle - Multi-Lane SDA LinQ Framework<br />
                                    <b>SPECTRUM Analyzer Option</b>
                                </td><td>&nbsp;</td>
                            </tr>
                             <tr><td colspan="3">&nbsp;</td></tr>
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">Bandwidth Upgrade WavePro HD 12-Bit Oscilloscopes incl. 1 Gpts Mem.</td><td>Promo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WavePro HD Series</b></td>
                                <td valign="top" width="608"><strong>2.5 GHz - 6 GHz</strong> 12-bit HD Oscilloscope, 4 Ch, 20 GS/s, <strong>1 Gpts/Ch</strong>,15.6" Touch Screen<br /><strong>FREE bandwidth upgrade to next higher bandwidth</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">FREE BW UPGRADE</td>
                            </tr>
                           <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included FREE Option:</div></td>
                                <td valign="top"><b>WPHD-1000MPT: 1 Gpt Memory Option incl. 32GB RAM upgrade</b>
                                </td><td>&nbsp;</td>
                            </tr>
                         <tr><td colspan="3">&nbsp;</td></tr>
                        <!--    <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">  8 CH 350 MHz - 2 GHz WR &amp; MDA HD 12-Bit Oscilloscopes</td><td>Promo Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveRunner & MDA 8000HD</b></td>
                                <td valign="top" width="608"><strong>350 MHz - 2 GHz</strong> 12-bit HD Oscilloscope, 8 - 16 Ch Solution, 10 GS/s, <strong>1 Gpts/Ch</strong>,15.6" Touch Screen<br /><strong>Any Option</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">20%<br />20%
                                </td>
                            </tr>
                           <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included FREE Option:</div></td>
                                <td valign="top"><b>WR8KHD-1000MPT: 1 Gpt Memory Option incl. 32GB RAM upgrade</b>
                                </td><td>&nbsp;</td>
                            </tr>
                                  <tr><td colspan="3">&nbsp;</td></tr>
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">8 CH HD 12-Bit Oscilloscopes and Motor Drive Analyzers</td><td>Promo Discount</td>
                            </tr>
                           <tr>
                                <td valign="top" width="210"><b>HDO8000A Series</b></td>
                                <td valign="top" width="608"><b>350 MHz, 500 MHz, 1 GHz</b> 12-bit HD Oscilloscope, 8 Ch, 10 GS/s, 50 Mpts/Ch, 12.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>MDA800A Series</b></td>
                                <td valign="top" width="608"><b>350 MHz, 500 MHz, 1 GHz</b> 12-bit HD Motor Drive Analyzer, 8 Ch, 10 GS/s, 50 Mpts/Ch, 12.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                           <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Add Probes & Options:</div></td>
                                <td valign="top"><b>Power Management Software Package</b>
                                </td><td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                              <tr>
                                <td align="right" valign="top">&nbsp;</td>
                                <td valign="top"><b>HVFO Fibre-Optic Probes, HVD High Voltage Differential Probes,</b>
                                </td><td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                              <tr>
                                <td align="right" valign="top">&nbsp;</td>
                                <td valign="top"><b>RP Rail Probes or CP Current Probes</b>
                                </td><td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">50%</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>-->
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">500 MHz &amp; 1 GHz HD 12-Bit Oscilloscope POWER BUNDLE</td><td>Package Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>HDO6054A / HDO6104A</b></td>
                                <td valign="top" width="608"><b>500 MHz/1 GHz</b> 12-bit HD Oscilloscope, 4 Ch, 10 GS/s, 50 Mpts/Ch, 12.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">42%</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included  Options: </div></td>
                                <td valign="top"><b>Power Analysis Option<br />CP030</b> - 30A, 50 MHz <b>Current Probe</b> - AC/DC, 30 A rms, 50 A Peak Pulse,<br />
                                    <b>HVD3106A</b> - 1kV, 120 MHz <b>High Voltage Differential Probe</b> with Auto Zero Disconnect</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2">  200 MHz - 1 GHz WaveSurfer HD 12-Bit Oscilloscopes - FULLY LOADED</td><td>Package Discount</td>
                            </tr>
                           <tr>
                                <td valign="top" width="210"><b>WaveSurfer 4024HD</b></td>
                                <td valign="top" width="608"><b>200 MHz</b>, 5 GS/s, 4 Ch, 25 Mpts/Ch DSO with 12.1&quot; Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">52%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveSurfer 4034HD</b></td>
                                <td valign="top" width="608"><b>350 MHz</b>, 5 GS/s, 4 Ch, 25 Mpts/Ch DSO with 12.1&quot; Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">56%</td>
                            </tr>
                                 <tr>
                                <td valign="top" width="210"><b>WaveSurfer 4054HD</b></td>
                                <td valign="top" width="608"><b>500 MHz</b>, 5 GS/s, 4 Ch, 25 Mpts/Ch DSO with 12.1&quot; Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104"> 59%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveSurfer 4104HD</b></td>
                                <td valign="top" width="608"><b>1 GHz</b>, 5 GS/s, 4 Ch, 25 Mpts/Ch DSO with 12.1&quot; Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">60%</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included Options: </div></td>
                                <td valign="top"><strong>EMB Option:</strong> I2C, SPI, UART, RS-232 Trigger & Decode<br>
<strong>AUDIO Option:</strong> AudioBus trigger and decode<br>
<strong>AUTO Option: CAN, CAN FD, LIN & FlexRa Trigger & Decode</strong><br>
<strong>PWR Option:</strong> Power Analysis Option<br>
<strong>FG Option:</strong> Integrated Arbitrary Function Generator</td>
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>
                                 <tr><td colspan="3">&nbsp;</td></tr>
                           <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="2"> 100 MHz - 1 GHz WaveSurfer 3000z Oscilloscopes - FULLY LOADED</td><td>Package Discount</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveSurfer 3104z </b></td>
                                <td valign="top" width="608"><b>100 MHz</b>, 2 GS/s, 4 Ch, 20 Mpts/Ch, 10.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">73%</td>
                            </tr>
                           <tr>
                                <td valign="top" width="210"><b>WaveSurfer 3024z</b></td>
                                <td valign="top" width="608"><b>200 MHz</b>, 4 GS/s, 4 Ch, 20 Mpts/Ch, 10.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">70%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveSurfer 3034z</b></td>
                                <td valign="top" width="608"><b>350 MHz</b>, 4 GS/s, 4 Ch, 20 Mpts/Ch, 10.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">57%</td>
                            </tr>
                                 <tr>
                                <td valign="top" width="210"><b>WaveSurfer 3054z</b></td>
                                <td valign="top" width="608"><b>500 MHz</b>, 4 GS/s, 4 Ch, 20 Mpts/Ch, 10.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">61%</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210"><b>WaveSurfer 3104z</b></td>
                                <td valign="top" width="608"><b>1 GHz</b>, 4 GS/s, 4 Ch, 20 Mpts/Ch, 10.1" Touch Screen</td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">59%</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><div style="color: #0077c0; font-weight: bold;">Included Options: </div></td>
                                <td valign="top"><strong>EMB Option:</strong> I2C, SPI, UART, RS-232 Trigger & Decode<br>
<strong>AUDIO Option:</strong> AudioBus trigger and decode<br>
<strong>AUTO Option: CAN, CAN FD, LIN & FlexRa Trigger & Decode</strong><br>
<strong>PWR Option:</strong> Power Analysis Option<br>
<strong>FG Option:</strong> Integrated Arbitrary Function Generator</td>
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr><tr><td colspan="3">&nbsp;</td></tr>

                            
                           
                            <tr style="background-color: #0076c0; color: #ffffff;">
                                <td align="left" colspan="3">Bluetooth Analyzer Trade-up Promotions</td>
                            </tr>
                            <tr>
                                <td valign="top" width="210" style="font-weight: 700">BPA600</td>
                                <td valign="top" width="608"><b>Chip-set based Dual Mode Bluetooth Analyzer covering Bluetooth Classic (BR/EDR)  and Low Energy (LE) up to Version 4.2.</b> </td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">&gt;73%</td>
                            </tr>

                            <tr>
                                <td valign="top" width="210" style="font-weight: 700">BPA600/Sodera LE Trade Up for <span style="color: #0076c0;">Frontline X240</span></td>
                                <td valign="top" width="608"><strong>Are you already using a BPA600 or Sodera LE and need to develop Bluetooth 5.0 and beyond including Bluetooth Mesh? Trade up your analyzer for the new Frontline X240 Bluetooth Analyzer using Software Defined Radio Technology and get a 3000.-€ discount</strong></td>
                                <td align="center" style="color: #0076c0; font-weight: bold;" valign="top" width="104">&euro; 3,000</td>
                            </tr>

 <tr><td colspan="3">&nbsp;</td></tr>
                        </table><br /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align="left" style="color: #333333; font-family: Arial; font-size: 13px;">
                            List prices or discounts are subject to change without notice. Actual net prices may vary due to delivery terms, sales tax, VAT, duty and exchange rate variations as well as
                            local (non-Teledyne LeCroy third party) service and support. Prices excl. VAT. Special offers limited to Teledyne LeCroy direct sales channel only until otherwise mentioned.<br />
                            Promotions not combinable with other promotions or discounts. Please contact local Teledyne LeCroy office for details and conditions.<br /><br />
                        </p>
                        <p align="left" style="color: #333333; font-family: Arial; font-size: 13px;">More information is available at our local Teledyne LeCroy office or via the following <a href="/europe/promotions/requests.asp">Link</a>.</p>
                        <font color="#333333" face="Arial">
                            <p align="left">
                                <font face="Arial" size="2">
                                    Contact us now to get more information:<br />
                                    <b>Italy:</b><br />
                                    Tel. + 39 041 5997011 or <a href="mailto:lecroy.contact.italy@teledyne.com">lecroy.contact.italy@teledyne.com</a><br /><br />
                                    <b>Other regions:</b><br />Tel. +41 22 719 2170 or <a href="mailto:lecroy.contact.germany@teledyne.com">lecroy.contact.germany@teledyne.com</a><br /><br />
                                    <b>Service &amp; Product Upgrades:</b><br /><a href="mailto:christophe.chappuis@teledyne.com">christophe.chappuis@teledyne.com</a>
                                </font>
                            </p>
                        </font>
                    </td>
                </tr>
            </table>
        </div>
        <%--End Editing Here--%>
    </div>
</asp:Content>