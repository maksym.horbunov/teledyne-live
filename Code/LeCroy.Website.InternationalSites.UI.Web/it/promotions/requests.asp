<%@ Language=VBScript %>
<HTML>
	<HEAD>
		<title>Oscilloscope Promotions Request Page</title> 
		<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
		<!--#include virtual="/cgi-bin/profile/newfunctions.asp" -->
		<!--#include virtual="/include/dbconnstr.asp" -->
		<!--#include virtual="/include/global.asp" -->
		<!--#include virtual="/shopper/functions.asp" -->
		<%Session("localeid")=1040
Session("RedirectTo") = "/IT/Promotions/requests.asp" 'here is the name of the file itself

If Not ValidateUser Then
	Response.redirect "/cgi-bin/profile/SentToReg.asp"
End If
ValidateUserNoRedir()

%>
		<meta http-equiv="Content-Type" content="text/html;">
		<style type="text/css"> body { font:Verdana, Arial, Helvetica, sans-serif; font-size:12px; text-align:center; }
	.container { width:700px; margin: 0 auto; text-align:left;}
	h1{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 22px; text-align: left;color:"#0000cc";}
	h2{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size:16px; text-align: center; color:"#0000cc"; }
	h3 { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; }
	.contact{ font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:bold; }
	.print{ font-family:Verdana, Arial, Helvetica, sans-serif; font-size:9px; }
	.wrimage{ margin: 0 0 0 378px; position: absolute; top: 465px; }
	.table{ width: 350px; }
	.promo {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; text-decoration:none;}
	ul{ font-family:Verdana, Arial, Helvetica, sans-serif; }
	li{font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif;}
	p{ font-family:Verdana, Arial, Helvetica, sans-serif; }
	--> 
	</style>
		<script language="javascript">
		    function Validate() {
		        counter = 0;
		        for (var i = 0; i < document.frm.elements.length; i++) {
		            //alert(document.frm.elements[i].checked);
		            if (document.frm.elements[i].checked == false) {
		            }
		            else {
		                counter++;
		            }
		        }
		        if (counter == 0) {
		            alert("Please make a selection");
		            return;
		        }
		        document.frm.submit();
		    }
		</script>
		<!--#include virtual="/include/meta_info.asp" -->
	</HEAD>
	<body topmargin="0" leftmargin="0">
		<div class="container"><br>
            <img src="/images/tl_weblogo_blkblue_189x30.png" border="0" width=189 height=30>
			<h1><span style="font-weight: 400"><font size="5">Oscilloscope Request Page</font></span></h1>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<form action="/goto/EuropePromo/Requests/confirm.asp" method="post" name="frm" id="frm">
					<TBODY>
						<tr>
							<td valign="top" width="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td valign="top" align="left">
								<table border="0" cellspacing="1" cellpadding="1" ID="Table1">
									<tr>
										<td colspan="2"><hr align="left" width="500">
										</td>
									</tr>
									<tr>
										<td valign="top" colspan="2"><h3>Please send me more information about the following 
												promotions:</h3>
										</td>
									</tr>

									<tr>
										<TD vAlign="top">
										<INPUT id="46" type="checkbox" name="promo3"></TD>
										<TD class="promo" vAlign="middle">
										<span lang="IT" style="font-size: 9pt; font-family: Verdana; font-weight: 700">
										Programma di �Trade-Up�</span><font color=black face="Verdana" style="font-size: 9pt"><b>&ndash; 
										<br>
										</b></font>
										<span lang="IT" style="font-size: 9pt; font-family: Verdana">
										Programma di aggiornamento delle 
										prestazioni dell�oscilloscopio</span></TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="47" type="checkbox" name="promo4"></TD>
										<TD class="promo" vAlign="middle">
										WaveRunner MXi-A 400 MHz - 2 GHz Promotion</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										&nbsp;</TD>
										<TD class="promo" vAlign="middle">&nbsp;</TD>
									</tr>

									<tr>
										<TD vAlign="top">
										<INPUT id="44" type="checkbox" name="promo1"></TD>
										<TD class="promo" vAlign="middle">100 MHz - 2 GHz 
										Oscilloscope Demo Sell out</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="45" type="checkbox" name="promo2"></TD>
										<TD class="promo" vAlign="middle">Serial Data Oscilloscopes 
										6 GHz - 18 GHz Demo Sell out</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="72" type="checkbox" name="promo5"></TD>
										<TD class="promo" vAlign="middle">Sampling Oscilloscope Package with TDR/Signal Integrity Solution</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="73" type="checkbox" name="promo6"></TD>
										<TD class="promo" vAlign="middle">Customization &amp; Software Promotions</TD>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="80" name="promo7"></td>
										<td valign="middle" class="promo">Mixed-Signal Promotions</td>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="90" name="promo8"></td>
										<td valign="middle" class="promo">Serial Bus Trigger &amp; Decode Promotions</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" colspan="2"><h3>
												Product Test: Please contact me to arrange a demo for:</h3>
										</td>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="114" type="checkbox" name="demo1"></TD>
										<TD class="promo" vAlign="middle">WaveAce 
										60 MHz � 300 MHz Oscilloscopes</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="110" type="checkbox" name="demo2"></TD>
										<TD class="promo" vAlign="middle">WaveJet 100 MHz � 500 MHz Oscilloscopes</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="119" type="checkbox" name="demo3"></TD>
										<TD class="promo" vAlign="middle">WaveSurfer (M)Xs-A 200 MHz � 1 GHz Oscilloscopes</TD>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="115" type="checkbox" name="demo4"></TD>
										<TD class="promo" vAlign="middle">Mixed 
										Signal Oscilloscopes Xs-A 400 MHz � 1 
										GHz</TD>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="116" name="demo5"></td>
										<td valign="middle" class="promo">WaveRunner 
										(M)Xi-A 400 MHz � 2 GHz Oscilloscopes</td>
									</tr>
									<tr>
										<TD vAlign="top">
										<INPUT id="117" type="checkbox" name="demo7"></TD>
										<TD class="promo" vAlign="middle">Vehicle Bus Analyzer 400 MHz � 2 GHz Oscilloscopes</TD>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="118" name="demo8"></td>
										<td valign="middle" class="promo">WavePro 
										/ SDA 7 Zi 1.5 GHz � 6 GHz Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="14" name="demo9"></td>
										<td valign="middle" class="promo">
										WaveMaster / SDA 8 Zi 4 GHz - 30 GHz 
										Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="15" name="demo10"></td>
										<td valign="middle" class="promo">WaveExpert Sampling 20 GHz � 100 GHz 
											Oscilloscopes</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="16" name="demo11"></td>
										<td valign="middle" class="promo">Serial Bus Trigger &amp; Decode Options</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="17" name="demo12"></td>
										<td valign="middle" class="promo">Protocol Analyzer
										</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<TR>
										<td valign="top" colspan="2"><h3>Serial Bus Trigger &amp; Decoding Solutions � Send me 
												more information about:</h3>
										</td>
									</TR>
									<tr>
										<td valign="top"><input type="checkbox" id="18" name="call1"></td>
										<td valign="middle" class="promo">FlexRay Bus</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="19" name="call3"></td>
										<td valign="middle" class="promo">I2C / SPI Bus</td>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="23" name="call7"></td>
										<td valign="middle" class="promo">I2S Bus</td>
									</tr>
									<tr>
										<td valign="top">
										<input type="checkbox" id="24" name="call8"></td>
										<td valign="middle" class="promo">Audio Bus</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="20" name="call4"></td>
										<td valign="middle" class="promo">CAN Bus</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="21" name="call5"></td>
										<td valign="middle" class="promo">LIN Bus</td>
									</tr>
									<tr>
										<td valign="top"><input type="checkbox" id="22" name="call6"></td>
										<td valign="middle" class="promo">UART-RS-232 Bus</td>
									</tr>
									<tr>
										<td colspan="2"><br>
											<input type="button" value="Submit Request" onclick="javascript:Validate();"></td>
									</tr>
									<tr>
										<td colspan="2"><hr align="left" width="500">
										</td>
									</tr>
									<tr>
										<td valign="top" colspan="2"><h3><b>For further requests about our products please use the 
													following links:</b></h3>
										</td>
									</tr>
									<tr>
										<td valign="top" colspan="2"><h3>
												<ul>
													<li>
														<a href="/Shopper/RequestQuote/">Product Quotations</a>
													<li>
														<a href="/Support/RequestInfo/">Product Demonstrations</a>
													<li>
														<a href="/Support/RequestInfo/">Product Literature</a>
													<li>
														<a href="/Support/TechHelp/">Technical Request</a></li></ul>											</h3>
										</td>
									</tr>
								</table>
								<br>
								<br>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td valign="top">&nbsp;</td>
						</tr></form>
				</TBODY>
			</table>
			<br>
			<br>
			<br>
		</div>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-13095488-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script src="https://teledynelecroy.com/js/pardot.js"></script>
	</body>
</HTML>