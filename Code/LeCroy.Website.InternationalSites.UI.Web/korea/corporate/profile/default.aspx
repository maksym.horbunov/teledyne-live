﻿<%@ Page Title="Teledyne LeCroy Corporate Profile" Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.korea_corporate_profile_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <%--Start Editing Here--%>
    <table border="0" width="100%" cellspacing="0" cellpadding="0" height="600">
	<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="0" height="100%" width="100%">
			<tr>
				<td width="175" valign="top"></td>
				<td height="100%" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%">
              <tr> 
                <td align="center" colspan=2>&#12288;</td>
              </tr>
              <tr> 
                <td align="left" colspan=2> <table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td align="left" class="gana05"><strong>Teledyne LeCroy Korea</strong><br /><img src="/images/lecroymap_v4.jpg" /></td>
                    </tr>
                    <tr> 
                      <td align="center" class="gana05">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td height="2" BACKGROUND="board-icon/line_01.gif" class="bgbottom"></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td class="lecroy04"> 
                      <p align="left"> <font face="Verdana"> <strong><font color="#006699">&#9654; 서울 본사</font></strong> <br />
                          <font size="2">&nbsp;
                          서울특별시 강남구 영동대로 333 동원빌딩 10층<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        TEL. (82) 2 3452-0400 <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Fax. (82) 2 3452-0490</font></font></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td height="2" BACKGROUND="board-icon/line_01.gif" class="bgbottom"></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td class="lecroy04">
                      <p align="left"><font face="Verdana"><font color="#006699"><strong>&#9654; 대전 사무소</strong></font><br />
                        <font size="2">&nbsp;
                        대전광역시 유성구 온천로 59 동아벤처타워 1501호<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        TEL. (82)42 828-7020<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Fax. (82)42 828-7021</font></font></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td height="2" BACKGROUND="board-icon/line_01.gif" class="bgbottom"></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td class="lecroy04">
                      <p align="left"><font face="Verdana"><font color="#006699"><strong>&#9654; 
                      대구 사무소</strong></font><br>
                        <font size="2">&nbsp;
                        대구광역시 달서구 호산동로 90-16 유일빌딩 204호<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        TEL. (82)53 585-6683<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Fax. (82)53 585-6693</font></font></td>
                    </tr>
                    <tr> 
                      <td height="5" class="lecroy"></td>
                    </tr>
                    <tr> 
                      <td height="2" BACKGROUND="board-icon/line_01.gif" class="bgbottom"></td>
                    </tr>
                  </table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
    <%--End Editing Here--%>
</asp:Content>