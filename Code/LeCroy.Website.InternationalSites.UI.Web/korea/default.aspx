﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.korea_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="SlideshowContainer">
        <div id="Slideshow">
           <!-- ws4000hd --><div id="card11" class="slideTop" onclick="window.location='/ws4000hd/'" style="cursor: pointer;"></div>
            <!-- wr8000hd --><div id="card12" class="slideTop" onclick="window.location='/wr8000hd/'" style="cursor: pointer;"></div>
            <!-- 12bits --><div id="card13" class="slideTop" onclick="window.location='/hdo/'" style="cursor: pointer;"></div>
            <!-- mda8000hd --><div id="card14" class="slideTop" onclick="window.location='/static-dynamic-complete/'" style="cursor: pointer;"></div>
            <!-- wavepulser --><div id="card15" class="slideTop" onclick="window.location='/wavepulser/'" style="cursor: pointer;"></div>
            <!-- waveprodhd --><div id="card16" class="slideTop" onclick="window.location='/waveprohd/'" style="cursor: pointer;"></div>
            <!-- wr9000 --><div id="card17" class="slideTop" onclick="window.location='/wr9000/'" style="cursor: pointer;"></div>
            <!-- HDA125 --><div id="card18" class="slideTop" onclick="window.location='/options/productdetails.aspx?modelid=9703&categoryid=18&groupid=54'" style="cursor: pointer;"></div>           
        </div>
        <div id="SlideshowPrev"></div>
        <div id="SlideshowNext"></div>
    </div>
    <div class="container">
        <div id="content">
            <div id="subfeature">
                <ul>
                    <li class="left"><a href="/hdo/" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/hdo_sm_home.png" alt="HDO" class="align-left" width="146" height="150" border="0" /></a>
                        <div id="subfeatureheadleft"><a href="/hdo/" style="text-decoration: none">HDO</a></div>
                        <div id="subfeaturesubheadleft"><a href="/hdo/" style="text-decoration: none">200 MHz to 1 GHz</a></div>
                        <div id="subfeaturetextleft"><a href="/hdo/" style="text-decoration: none">HD4096<br />12-bit<br />기술</a></div>
                    </li>
                    <li class="right2">
                        <a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none"><img src="https://assets.lcry.net/images/wavesurfer-3000z_home.png" alt="WaveSurfer 3000z" width="158" height="150" class="align-right" border="0" /></a>
                        <div id="subfeatureheadright"><a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none">WaveSurfer<br />3000z</a></div>
                        <div id="subfeaturetextright"><a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none">100 MHz to 1 GHz<br /><br />with MAUI 고급 <br />사용자 인터페이스</a></div>
                    </li>
                    <li class="right2"><a href="/protocolanalyzer/protocoloverview.aspx?seriesid=445" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/summit_t24_home.png" alt="Summit T24 PCI Express Protocol Analyzer" width="158" height="150" class="align-right" border="0" /></a>
                        <div id="subfeatureheadright"><a href="/protocolanalyzer/protocoloverview.aspx?seriesid=445" style="text-decoration: none">Summit T24</a></div>
                        <div id="subfeaturetextright"><a href="/protocolanalyzer/protocoloverview.aspx?seriesid=445" style="text-decoration: none">경제적인<br />PCI Express<br />분석기</a></div>
                    </li>
                </ul>
            </div>
        </div>
        <asp:Literal ID="UC_ISSUE" runat="server" Visible="false" />
    </div>
<!-- promotion --><div id="overlay">
        <div style="width: 440px;">
            <a href="#" class="close">X</a>
            <a href="./promotions/promotions.aspx"><img src="https://assets.lcry.net/images/wavesurfer4000hd-korea-promo.jpg"></a>
        </div>
    </div>

    <script type="text/javascript">
	jQuery(document).ready(function($) {
	  $('#overlay').show();
  
	  $(".close").click(function() {
		  $("#overlay").hide();
	  });
	});
    </script>
</asp:Content>