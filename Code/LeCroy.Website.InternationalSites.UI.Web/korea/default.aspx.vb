﻿Imports LeCroy.Library.Domain.Common.DTOs

Public Class korea_default
    Inherits KRPageBase

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Dim ucFooter As usercontrols_footer = CType(Master.FindControl("ucFooter"), usercontrols_footer)
        Try
            Dim ucFooter As PlaceHolder = CType(Master.FindControl("ucFooter"), PlaceHolder)
            If Not (ucFooter Is Nothing) Then
                Dim koFooter As footer_KO = (CType(Master.FindControl("FooterKO"), footer_KO))
                If Not (koFooter Is Nothing) Then
                    Dim mediaLinks As OrderedDictionary = New OrderedDictionary()
                    mediaLinks.Add("https://www.facebook.com/TeledyneLeCroyKorea", "facebook-korea.jpg")
                    mediaLinks.Add("https://www.youtube.com/TeledyneLeCroyKorea", "youtube-korea.jpg")
                    mediaLinks.Add("http://blog.teledynelecroy.co.kr/", "naver-blog-korea.jpg")
                    koFooter.MediaLinks = mediaLinks
                Else
                    ' AF-TODO: currently not able to locate the control for updating. Need to resolve...
                    UC_ISSUE.Text = "Unable to locate Korea Media Links container"
                    UC_ISSUE.Visible = True
                End If
            End If

            Dim control As HtmlLink = New HtmlLink()
            control.Href = ResolveUrl(ConfigurationManager.AppSettings("KrPopup"))
            control.Attributes.Add("rel", "stylesheet")
            control.Attributes.Add("type", "text/css")
            CType(Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
        Catch ex As Exception
            ' AF-TODO: currently not able to locate the control for updating. Need to resolve...
            UC_ISSUE.Text = "Unable to process Request - " + ex.Message
            UC_ISSUE.Visible = True
        End Try
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class