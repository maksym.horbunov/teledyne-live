﻿<%@ Page Title="텔레다인르크로이 9월 뉴스레터" Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="promotions.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.korea_promotions_promotions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="content">
        <%--Start Editing Here--%>
<TABLE 
style="BORDER-TOP: #d9d9d9 8px solid; BORDER-RIGHT: #d9d9d9 8px solid; BORDER-BOTTOM: #d9d9d9 8px solid; BORDER-LEFT: #d9d9d9 8px solid" 
cellSpacing=0 cellPadding=0 width=700 border=0><!-- header -->
  <THEAD style="WIDTH: 684px">
  <TR><!-- &#47196;&#44256;&#50689;&#50669; -->
    <TH 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px">
      <TABLE cellSpacing=0 cellPadding=0 border=0>
        <TBODY>
        <TR>

          <TD 
          style="BORDER-LEFT-WIDTH: 0px; FONT-SIZE: 15px; HEIGHT: 15px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; BORDER-RIGHT-WIDTH: 0px; WIDTH: 427px; VERTICAL-ALIGN: bottom; BORDER-BOTTOM-WIDTH: 0px; FONT-WEIGHT: normal; COLOR: #006dbb; TEXT-ALIGN: right; PADDING-TOP: 17px; PADDING-RIGHT: 20px; BORDER-TOP-WIDTH: 0px">
			<IMG 
            style="VERTICAL-ALIGN: middle" alt=화살표 
            src="http://img.ozmailer.com/userFile/14754/736e4zb.png" width=7 
            height=5>&nbsp;프로모션 뉴스 2020년 </TD></TR><!-- &#44277;&#48177; -->
        <TR>
          <TD 
          style="BORDER-LEFT-WIDTH: 0px; HEIGHT: 4px; BORDER-RIGHT-WIDTH: 0px; WIDTH: 427px; BORDER-BOTTOM-WIDTH: 0px; LINE-HEIGHT: 0; BORDER-TOP-WIDTH: 0px">
			　</TD></TR><!-- &#47700;&#45684; -->
          </TBODY></TABLE></TH></TR><!-- &#45684;&#49828;&#47112;&#53552; &#49548;&#44060; -->
  <TR>
    <TH 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BACKGROUND: #f4f4f4; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px">
      <TABLE cellSpacing=0 cellPadding=0 border=0 width="684">
        <TBODY>
        <TR>
          <TD 
          style="border-width:0px; FONT-SIZE: 12px; HEIGHT: 82px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; WIDTH: 664px; FONT-WEIGHT: normal; COLOR: #333; TEXT-ALIGN: left; PADDING-TOP: 10px; PADDING-LEFT: 20px; LETTER-SPACING: -0.02em; LINE-HEIGHT: 1.3em; ">
			<p style="text-align: center">
			<b>
			<span style="letter-spacing: 0.05em"><font face="맑은 고딕">
			<span style="vertical-align: center"><font size="5" color="#006DBB">
			12bit 오실로스코프 35% 가격 인하* 프로모션&nbsp; </font></span>
			</font></span><font size="3" color="#006DBB"><br>
&nbsp;</font></b></p>
			<P><font size="2">안녕하세요. 고객님</font></P>
			<P>
			<span lang="EN-US" style="font-size: 10.0pt; line-height: 107%; font-family: 맑은 고딕">
			텔레다인르크로이 <b>
			<a href="https://teledynelecroy.com/oscilloscope/wavesurfer-4000hd-oscilloscopes">
			WaveSurfer 4000HD</a></b> 시리즈는 12.1” </span>
			<span style="font-size: 10.0pt; line-height: 107%; font-family: 맑은 고딕">
			터치 스크린 디스플레이에서 깨끗하고 선명한 파형을 보여주며 <b> <span lang="EN-US">12</span>비트 수직 
			분해능</b>을 제공합니다. 고해상도 오실로스코프 WaveSurfer 4000HD 시리즈를 지금<span lang="EN-US"> 
			35% 할인 된 가격으로 </span></span><span lang="en-us"><font size="2">
			<span style="font-family: 맑은 고딕">만나보세요!</span></font></span></P>
			<P>
			&nbsp;</P>
			<P><b>
			<font size="2" color="#FF0000">프로모션 기간: 2020년 12월 18일 까지 </font></b>
			<span lang="en-us"><font size="2"><span style="font-family: 맑은 고딕">
			(*소비자 가격 기준)</span></font></span><font size="2"><BR>
			</font><font size="2" style="line-height: 3em">텔레다인르크로이코리아 마케팅팀 드림</font></P></TD></TR></TBODY></TABLE></TH></TR></THEAD><!-- contents -->
  <TBODY style="WIDTH: 684px"><!-- &#48660;&#47196;&#44536; &#52968;&#53584;&#52768; &#44536;&#47353; -->
  <TR>
    <TD 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px"><!-- &#45824;&#51228;&#47785;-->
      <!-- &#52395;&#48264;&#51704; &#52968;&#53584;&#52768; -->
      <!-- &#46160;&#48264;&#51704; &#52968;&#53584;&#52768; -->
      </TD></TR><!-- &#49888;&#51228;&#54408; &#52968;&#53584;&#52768; &#44536;&#47353; -->
  <TR>
    <TD 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BACKGROUND: #f4f4f4; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px"><!-- &#45824;&#51228;&#47785;--></TD></TR><!-- &#44053;&#51340; &#52968;&#53584;&#52768; &#44536;&#47353; -->
  <TR>
    <TD 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px"><!-- &#45824;&#51228;&#47785;-->
      <TABLE cellSpacing=0 cellPadding=0 border=0 width="684">
        <TBODY>
        <TR>
          <TD 
          style="border-width:0px; FONT-SIZE: 20px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; FONT-WEIGHT: bold; COLOR: #333; PADDING-BOTTOM: 11px; PADDING-TOP: 15px; PADDING-LEFT: 20px; PADDING-RIGHT: 0px" height="242" colspan="2">
			<p align="center">
			<img border="0" src="https://img.ozmailer.com/userFile/14754/pxggm153r.jpg" width="595" height="400" align="right"></TD>
          </TR>
        <TR>
          <TD 
          style="border-style:solid; border-width:1px; FONT-SIZE: 20px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; FONT-WEIGHT: bold; COLOR: #333; PADDING-BOTTOM: 11px; PADDING-TOP: 15px; PADDING-LEFT: 20px; PADDING-RIGHT: 0px" bordercolor="#D2D2D2" bordercolorlight="#000000" width="203" bgcolor="#D2D2D2" align="center">
			<p align="left">프로모션 제품</TD>
          <TD 
          style="border-style:solid; border-width:1px; FONT-SIZE: 20px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; FONT-WEIGHT: bold; COLOR: #333; PADDING-BOTTOM: 11px; PADDING-TOP: 15px; PADDING-LEFT: 20px; PADDING-RIGHT: 0px" bordercolor="#D2D2D2" bordercolorlight="#000000" width="437" bgcolor="#D2D2D2" align="center">
			<p align="left">기본 사양</TD>
          </TR>
        <TR>
          <TD 
          style="border-style:solid; border-width:1px; FONT-SIZE: 20px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; FONT-WEIGHT: bold; COLOR: #333; PADDING-BOTTOM: 11px; PADDING-TOP: 15px; PADDING-LEFT: 20px; PADDING-RIGHT: 0px" bordercolor="#D2D2D2" bordercolorlight="#000000" width="203">
			<font size="3">
			<a href="https://teledynelecroy.com/oscilloscope/oscilloscopemodel.aspx?modelid=11383&capid=102&mid=504">
			WAVESURFER 4024HD</a>&nbsp; <p>
			<a href="https://teledynelecroy.com/oscilloscope/oscilloscopemodel.aspx?modelid=11384&capid=102&mid=504">
			WAVESURFER 4034HD</a></p>
			<p>
			<a href="https://teledynelecroy.com/oscilloscope/oscilloscopemodel.aspx?modelid=11385&capid=102&mid=504">
			WAVESURFER 4054HD</a></p>
			<p>
			<a href="https://teledynelecroy.com/oscilloscope/oscilloscopemodel.aspx?modelid=11386&capid=102&mid=504">
			WAVESURFER 4104HD</a></p>
			</font></TD>
          <TD 
          style="border-style:solid; border-width:1px; FONT-SIZE: 20px; FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif; FONT-WEIGHT: bold; COLOR: #333; PADDING-BOTTOM: 11px; PADDING-TOP: 15px; PADDING-LEFT: 20px; PADDING-RIGHT: 0px" bordercolor="#D2D2D2" bordercolorlight="#000000" width="437">
			<font style="font-size: 11pt"><span style="font-weight: 400">200 MHz 
			Bandwidth, 4 Input Channels, 12.5 Mpts/ch, 5 GS/s</span><p>
			<span style="font-weight: 400">350 MHz Bandwidth, 4 Input Channels, 
			12.5 Mpts/ch, 5 GS/s</span></p>
			<p><span style="font-weight: 400">500 MHz Bandwidth, 4 Input 
			Channels, 12.5 Mpts/ch, 5 GS/s</span></p>
			<p><span style="font-weight: 400">
			<span id="SiteContent_CenterColumn_lbQuickSpecs">&nbsp;&nbsp;&nbsp; 
			1 GHz Bandwidth, 4 Input Channels, 12.5 Mpts/ch, 5 GS/s</span></span></p>
			</font></TD>
          </TR>
        <!-- &#44053;&#51340; &#49548;&#44060; -->
        </TBODY></TABLE><!-- &#54364; -->
      </TD></TR><!-- &#49352;&#49548;&#49885; &#52968;&#53584;&#52768; &#44536;&#47353; -->
  <TR>
    <TD 
    style="BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px; BACKGROUND: #f4f4f4; BORDER-BOTTOM-WIDTH: 0px; BORDER-TOP-WIDTH: 0px"><!-- &#45824;&#51228;&#47785;-->
      </TD></TR></TBODY><!-- footer -->
<tfoot style="WIDTH: 684px">
 <tr>
          <td style="FONT-SIZE: 12px;HEIGHT: 65px;FONT-FAMILY: '맑은 고딕', '돋움', dotum, sans-serif;BORDER-BOTTOM: 1px solid #d2d2d2;COLOR: #333;PADDING-LEFT: 20px;text-align: center;">
			<p style="text-align: center;">
			<a href="https://teledynelecroy.com/oscilloscope/configure/configure_step2.aspx?modelid=11386">
			<img border="0" src="http://img.ozmailer.com/userFile/14754/a2c10gyry.jpg" width="142" height="47"></a></p></td>
          </tr></tfoot></TABLE>
        <%--End Editing Here--%>
    </div>
</asp:Content>