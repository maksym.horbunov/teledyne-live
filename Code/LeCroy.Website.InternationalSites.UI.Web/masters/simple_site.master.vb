﻿Public Class masters_simple_site
    Inherits MasterPageBase

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Page.Header.DataBind()  ' NOTE: Needed for inline ResolveUrl's
        If (Session("localeid") Is Nothing Or Len(Session("localeid")) = 0) Then
            Session("localeid") = 1033
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class