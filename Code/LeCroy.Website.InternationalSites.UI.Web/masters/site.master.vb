﻿Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Website.BLL
Imports IXFSDK
Imports IXFSDK.Util

Public Class masters_site
    Inherits MasterPageBase

#Region "Variables/Keys"
    Private _ixf_sdk_client As IXFSDKClient
    Public ReadOnly Property ixf_sdk_client As IXFSDKClient
        Get
            If (_ixf_sdk_client Is Nothing) Then
                Dim ixf_config As IXFConfiguration = New IXFSDKConfiguration()
                ixf_config.SetProperty(IXFConfigKeys.CAPSULE_MODE, IXFConfigKeys.REMOTE_PROD_CAPSULE_MODE)
                ixf_config.SetProperty(IXFConfigKeys.ACCOUNT_ID, "f00000000073308")
                ixf_config.SetProperty(IXFConfigKeys.CHARSET, "UTF-8")
                ixf_config.SetProperty(IXFConfigKeys.PAGE_ALIAS_URL, Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.PathAndQuery)
                ixf_config.SetProperty(IXFConfigKeys.WHITELIST_PARAMETER_LIST, "ixf")
                _ixf_sdk_client = New IXFSDKClient(ixf_config, HttpContext.Current)
            End If
            Return _ixf_sdk_client
        End Get
    End Property
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpModernizer"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpCommon"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpCycle"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHoverIntent"))
        ' removed, file does not exist and not included in main site master -- InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsLeCroyCommon"))
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssHomepage"))
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssBootstrap"))
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssHeaderFooterMaster"))
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssSerialData"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsBootstrapBundle"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsBootstrap"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsJQueryHeader"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsJQueryMain"))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Page.Header.DataBind()  ' NOTE: Needed for inline ResolveUrl's
        If (Session("localeid") Is Nothing Or Len(Session("localeid")) = 0) Then
            Session("localeid") = 1033
        End If
        Dim localeId As Int32 = Session("localeid")

        If (PageBase.ValidateIntegerForLocaleIdFromSession(Session("localeid")) = 1049) Then
            Dim control As Literal = New Literal()
            control.Text = "<style>header ul li {font-size: 12pt;} footer .h3, footer h3 a:link, footer h3 a:visited {font-size: 14px;}</style>"
            CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(control)
        End If

        If (localeId = LocaleIds.KOREAN) Then
            'AF-TODO needs review : ucFooter.CompanyStringOverride = "Korea"
        End If
        BindPage(localeId)
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindPage(ByVal localeId As Int32)
        BindHeader(localeId)
        BindFooter(localeId)
    End Sub

    Private Sub BindHeader(ByVal localeId As Int32)
        Dim controlFile As String

        'controlFile = "~/usercontrols/header_FR.ascx"
        Select Case localeId
            Case LocaleIds.ENGLISH_US ' load US/EN english
                controlFile = "~/usercontrols/header_EN.ascx"
            Case LocaleIds.ENGLISH_UK ' load UK/EU english
                controlFile = "~/usercontrols/header_EU.ascx"
            Case LocaleIds.FRENCH ' load French
                controlFile = "~/usercontrols/header_FR.ascx"
            Case LocaleIds.GERMAN ' load German
                controlFile = "~/usercontrols/header_DE.ascx"
            Case LocaleIds.ITALIAN ' load Italian
                controlFile = "~/usercontrols/header_IT.ascx"
            Case LocaleIds.JAPANESE ' load Japanese
                controlFile = "~/usercontrols/header_JP.ascx"
            Case LocaleIds.KOREAN ' load Korean
                controlFile = "~/usercontrols/header_KO.ascx"
            Case LocaleIds.CHINESE_PRC ' load Chinese
                controlFile = "~/usercontrols/header_ZH.ascx"
            Case Else ' load US english for all others
                controlFile = "~/usercontrols/header_EN.ascx" ' US used as default for unrecognized locales
        End Select

        Dim vw As Control
        vw = CType(LoadControl(controlFile), UserControl)
        vw.ID = "View_Dyn_Header"
        ucHeader.Controls.Clear()
        ucHeader.Controls.Add(vw)
    End Sub

    Private Sub BindFooter(ByVal localeId As Int32)
        Dim controlFile As String

        'controlFile = "~/usercontrols/footer_FR.ascx"
        Select Case localeId
            Case LocaleIds.ENGLISH_US ' load US/EN english
                controlFile = "~/usercontrols/footer_EN.ascx"
            Case LocaleIds.ENGLISH_UK ' load UK/EU english
                controlFile = "~/usercontrols/footer_EU.ascx"
            Case LocaleIds.FRENCH ' load French
                controlFile = "~/usercontrols/footer_FR.ascx"
            Case LocaleIds.GERMAN ' load German
                controlFile = "~/usercontrols/footer_DE.ascx"
            Case LocaleIds.ITALIAN ' load Italian
                controlFile = "~/usercontrols/footer_IT.ascx"
            Case LocaleIds.JAPANESE ' load Japanese
                controlFile = "~/usercontrols/footer_JP.ascx"
            Case LocaleIds.KOREAN ' load Korean
                controlFile = "~/usercontrols/footer_KO.ascx"
            Case LocaleIds.CHINESE_PRC ' load Chinese
                controlFile = "~/usercontrols/footer_ZH.ascx"
            Case Else ' load US english for all others
                controlFile = "~/usercontrols/footer_EN.ascx"  ' US used as default for unrecognized locales
        End Select

        Dim vw As Control
        vw = CType(LoadControl(controlFile), UserControl)
        vw.ID = "View_Dyn_Footer"
        ucFooter.Controls.Clear()
        ucFooter.Controls.Add(vw)
    End Sub

#End Region
End Class