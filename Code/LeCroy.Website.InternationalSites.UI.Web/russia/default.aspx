﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masters/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.russia_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="SlideshowContainer">
        <div id="Slideshow">
            <div id="card1" class="slide" onclick="window.location='/motor-drive-analyzer/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; position: relative; width: 616px; padding: 55px 0 0 50px; text-align:left;">
                        <h1><a href="/motor-drive-analyzer/" style="font-size: 38px; color: #5dc151; font-weight: 300; line-height: 32px; text-align:left;">8-канальный<br>осциллограф<br>анализатор<br>мощности<br>электроприводов<br>и асинхронных<br>двигателей</a></h1>
                    </div>
                    <div style="float: left; position: relative; width: 400px; padding: 15px 0 0 50px; text-align:left;"><h2><a href="/motor-drive-analyzer/" style="font-size: 18px; color: #000; font-weight: 300; line-height: 18px; text-align:left;">Полная системная отладка<br>электроприводов, проверка и контроль<br>работоспособности электроприводов</a></h2></div>
                </div>
            </div>
            <div id="card4" class="slide" onclick="window.location='/hdo/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; position: relative; width: 616px; padding: 55px 0 0 50px;">
                        <h1><a href="/hdo/" style="font-size: 36px; color: #000; font-weight: 300; line-height: 50px; text-align:right;">12-битные осциллографы высокого разрешения</a></h1>
                        <h2><a href="/hdo/">&nbsp;</a></h2>
                    </div>
                </div>
            </div>
            <div id="card5" class="slide" onclick="window.location='/100ghz/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; width: 600px; padding: 40px 0 0 600px;">
                        <h1><a style="font-family:Roboto; font-size:80px; font-weight:700; color:#fff;" href="/100ghz/">100 ГГц</a></h1>
                        <h2><a  href="/100ghz/" style="font-family:Roboto; font-size:32px; font-weight:200; padding-top:0; color:#fff;">Осциллограф реального времени</a></h2>
                    </div>
                </div>
            </div>
            <div id="card3" class="slide" onclick="window.location='/pam4/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; width: 600px; padding: 300px 0 0 150px;">
                        <h1><a style="font-family:Roboto; font-size:80px; font-weight:700; color:#fff;" href="/pam4/">PAM4</a></h1>
                        <h2><a href="/pam4/" style="font-family:Roboto; font-size:40px; font-weight:500; padding-top:0; color:#fff;">Signal Analysis</a></h2>
                    </div>
                </div>
            </div>
            <div id="card2" class="slide" onclick="window.location='/usb3.1/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; position: relative; width: 616px; padding: 300px 0 0 300px;">
                        <h1><a href="/usb3.1/" style="font-size: 92px; color: #fff; font-weight: 500; line-height: 75px; text-align:right;">USB 3.1</a></h1>
                        <h2><a href="/usb3.1/" style="font-size: 45px; color: #fff; font-weight: 300; line-height: 50px; text-align:right;">End-to-End Test Suite</a></h2>
                    </div>
                </div>
            </div>
            <div id="eu-card6" class="slide" onclick="window.location='/europe/ws3000/'" style="cursor: pointer;">
                <div class="centeredContent">
                    <div style="float: left; position: relative; width: 616px; padding: 85px 0 0 450px;">
                        <h1><a href="/europe/ws3000/" style="font-size: 60px; color: #000; font-weight: 300; line-height: 80px; text-align:right;">WaveSurfer 3000R</a></h1>
                        <h2><a href="/europe/ws3000/" style="font-size: 30px; color: #000; font-weight: 100; line-height: 40px; text-align:right;">200 МГц-750 МГц<br />Сенсорный дисплей 10.1"</a></h2>
                        <%--<h2><a href="/europe/ws3000/" style="line-height: 40px; padding: 30px 50px 0 0; text-align: right;"><img src="https://assets.lcry.net/images/home_ws3000_price_eng.jpg" alt="" border="0" /></a></h2>--%>
                        <div style="position: absolute; top: 250px; left: 525px;"><a href="/europe/ws3000/"><img src="https://assets.lcry.net/images/home_ws3000_promo_eng.png" alt="" border="0" height="200" /></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SlideshowPrev"></div>
        <div id="SlideshowNext"></div>
    </div>
    <div class="container">
        <div id="content">
            <div id="subfeature">
                <ul>
                    <li class="left"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=458" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/home-hdo-8000.png" alt="HDO8000" class="align-left" width="146" height="150" border="0" /></a>
                        <div id="subfeatureheadleft"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=458" style="text-decoration: none">HDO8000R</a></div>
                        <div id="subfeaturetextleft"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=458" style="text-decoration: none">8 каналов<br />12-битный<br>осциллограф<br />350 МГц -1ГГц</a></div>
                    </li>
                    <li class="middle">
                        <div id="subfeatureheadmiddle"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=480" style="text-decoration: none">WaveMaster 8 Zi-B-R</a></div>
                        <div id="subfeaturetextmiddle"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=480" style="text-decoration: none">Полоса пропускания 4 ГГц-30 ГГц,<br>дискретизация 80 ГГц,<br>превосходная точность отображения сигнала</a></div>
                        <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=480" style="text-decoration: none"><img src="https://assets.lcry.net/images/home-wavemaster-8zi-b.png" alt="WaveMaster Oscilloscope" width="247" height="76" border="0" /></a>
                    </li>
                    <li class="right2"><a href="/probes/probeseries.aspx?mseries=426" style="text-decoration: none">
                        <img src="https://assets.lcry.net/images/home-cp03x.png" alt="CP03xA" width="158" height="150" class="align-right" border="0" /></a>
                        <div id="subfeatureheadright"><a href="/probes/probeseries.aspx?mseries=426" style="text-decoration: none">CP03xA</a></div>
                        <div id="subfeaturetextright"><a href="/probes/probeseries.aspx?mseries=426" style="text-decoration: none">Токовые пробники<br>высокого<br>разрешения<br>(1 мА/дел)</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>