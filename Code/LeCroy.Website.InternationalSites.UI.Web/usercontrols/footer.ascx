﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="footer.ascx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.usercontrols_footer" %>
<ul id="ulHeaderQuickLinks" runat="server" visible="false">
    <asp:Repeater ID="rptHeaders" runat="server">
        <ItemTemplate>
            <li>
                <h3 class="h3"><asp:Literal ID="litHeader" runat="server" /></h3>
                <ul>
                    <asp:Repeater ID="rptElements" runat="server">
                        <ItemTemplate>
                            <li><asp:HyperLink ID="hlkElement" runat="server" /></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<div class="copyright">
    <p><asp:PlaceHolder ID="phSocialMediaLinks" runat="server" /></p>
    <div id="divSupplemental" runat="server" visible="false" style="padding-bottom: 1px;">
        <asp:Literal ID="litSupplemental" runat="server" />
    </div>
    <p>&#169; <asp:Literal ID="litCopyrightYear" runat="server" /> Teledyne <asp:Literal ID="litCompany" runat="server" Text="LeCroy" /></p>
    <asp:Repeater ID="rptLinks" runat="server">
        <ItemTemplate>
            <asp:HyperLink ID="hlkLink" runat="server" /><asp:Literal ID="litSplitter" runat="server" Text=" | " Visible="false" />
        </ItemTemplate>
    </asp:Repeater>
    <p><a href="http://teledyne.com/" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a></p>
</div>