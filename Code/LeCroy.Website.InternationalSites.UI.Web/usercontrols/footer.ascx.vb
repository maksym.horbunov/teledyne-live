﻿Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class usercontrols_footer
    Inherits UserControl

#Region "Variables/Keys"
    Public ShowQuickLinks As Boolean = False
    Public MediaLinks As OrderedDictionary = New OrderedDictionary()
    Public CompanyStringOverride As String = String.Empty
    Private _copyrightLinkCount As Int32 = 0
    Private _copyrightLinksCounted As Int32 = 0
#End Region

#Region "Page Events"
#End Region

#Region "Control Events"
    Private Sub rptHeaders_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptHeaders.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedMenu = CType(e.Item.DataItem, ExtendedMenu)
                Dim litHeader As Literal = CType(e.Item.FindControl("litHeader"), Literal)
                Dim rptElements As Repeater = CType(e.Item.FindControl("rptElements"), Repeater)

                litHeader.Text = row.MenuName
                If (row.SubMenu.Count() > 0) Then
                    AddHandler rptElements.ItemDataBound, AddressOf rptElements_ItemDataBound
                    rptElements.DataSource = row.SubMenu
                    rptElements.DataBind()
                End If
        End Select
    End Sub

    Private Sub rptElements_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As SubMenu = CType(e.Item.DataItem, SubMenu)
                Dim hlkElement As HyperLink = CType(e.Item.FindControl("hlkElement"), HyperLink)

                hlkElement.NavigateUrl = row.Url
                hlkElement.Text = row.SubMenuName
        End Select
    End Sub

    Private Sub rptLinks_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptLinks.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedMenu = CType(e.Item.DataItem, ExtendedMenu)
                Dim hlkLink As HyperLink = CType(e.Item.FindControl("hlkLink"), HyperLink)
                Dim litSplitter As Literal = CType(e.Item.FindControl("litSplitter"), Literal)

                _copyrightLinksCounted += 1
                hlkLink.NavigateUrl = row.Url
                hlkLink.Text = row.MenuName
                If (_copyrightLinksCounted < _copyrightLinkCount) Then
                    litSplitter.Visible = True
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Public Sub BindControl(ByVal extendedMenu As List(Of ExtendedMenu))
        If (ShowQuickLinks) Then
            ulHeaderQuickLinks.Visible = True
            BindLinks(extendedMenu.Where(Function(x) x.IsExpandable).OrderBy(Function(x) x.SortId).ToList())
        End If
        BindSocialMediaLinks()
        BindCopyright(extendedMenu.Where(Function(x) Not (x.IsExpandable) And Not (x.IsSupplemental)).OrderBy(Function(x) x.SortId).ToList())
        BindSupplemental(extendedMenu)
    End Sub

    Private Sub BindSocialMediaLinks()
        BindMediaLinkList()
        If (MediaLinks.Count > 0) Then
            Dim i As Int32 = 0
            Dim ide As IDictionaryEnumerator = MediaLinks.GetEnumerator
            While ide.MoveNext()
                i += 1
                phSocialMediaLinks.Controls.Add(GetHtmlAnchor(ide.Key.ToString(), ide.Value.ToString(), (i = MediaLinks.Count)))
            End While
        End If
    End Sub

    Private Sub BindMediaLinkList()
        If (MediaLinks.Count <= 0) Then
            MediaLinks.Add("https://facebook.com/teledynelecroy", "facebook.jpg")
            MediaLinks.Add("https://twitter.com/teledynelecroy", "twitter.jpg")
            MediaLinks.Add("https://youtube.com/lecroycorp", "youtube.jpg")
        End If
    End Sub

    Private Function GetHtmlAnchor(ByVal href As String, ByVal imageName As String, ByVal isLastElement As Boolean) As HtmlAnchor
        Dim retVal As MyHtmlAnchor = New MyHtmlAnchor()
        retVal.HRef = href
        retVal.AddThisText = String.Format("GaEventPush('OutboundLink', '{0}');", href)
        retVal.Controls.Add(GetHtmlImage(imageName, isLastElement))
        Return retVal
    End Function

    Private Function GetHtmlImage(ByVal imageName As String, ByVal isLastElement As Boolean) As HtmlImage
        Dim retVal As HtmlImage = New HtmlImage()
        retVal.Src = String.Format("/images/{0}", imageName)
        If Not (isLastElement) Then retVal.Attributes.Add("style", "padding-right: 6px;")
        Return retVal
    End Function

    Private Sub BindLinks(ByVal extendedMenu As List(Of ExtendedMenu))
        If (extendedMenu.Count() > 0) Then
            rptHeaders.DataSource = extendedMenu
            rptHeaders.DataBind()
        End If
    End Sub

    Private Sub BindCopyright(ByVal extendedMenu As List(Of ExtendedMenu))
        _copyrightLinkCount = extendedMenu.Count()
        litCopyrightYear.Text = DateTime.Now.Year.ToString()
        If Not (String.IsNullOrEmpty(CompanyStringOverride)) Then
            litCompany.Text = CompanyStringOverride
        End If
        If (extendedMenu.Count() > 0) Then
            rptLinks.DataSource = extendedMenu
            rptLinks.DataBind()
        End If
    End Sub

    Private Sub BindSupplemental(ByVal extendedMenu As List(Of ExtendedMenu))
        Dim supplementalData As List(Of ExtendedMenu) = extendedMenu.Where(Function(x) Not (x.IsExpandable) And x.IsSupplemental).OrderBy(Function(x) x.SortId).ToList()
        If (supplementalData.Count > 0) Then
            divSupplemental.Visible = True
            litSupplemental.Text = supplementalData.FirstOrDefault().MenuName
        End If
    End Sub
#End Region
End Class

<AspNetHostingPermission(System.Security.Permissions.SecurityAction.Demand, Level:=AspNetHostingPermissionLevel.Minimal)> _
Public NotInheritable Class MyHtmlAnchor
    Inherits HtmlAnchor

    Public AddThisText As String

    Protected Overrides Sub RenderAttributes(writer As HtmlTextWriter)
        MyBase.RenderAttributes(writer)
        writer.Write(String.Format(" onclick=""{0}"" ", AddThisText))
    End Sub
End Class