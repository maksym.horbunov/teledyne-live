﻿Imports LeCroy.Website.BLL

Public Class footer_KO
    Inherits System.Web.UI.UserControl

#Region "Variables/Keys"
    ' Support for Media Links copied from the original footer.ascx and footer.ascx.vb files
    Public MediaLinks As OrderedDictionary = New OrderedDictionary()
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#Region "Page Methods"
    Public Sub BindControl(ByVal extendedMenu As List(Of ExtendedMenu))
        'If (ShowQuickLinks) Then
        '    ulHeaderQuickLinks.Visible = True
        '    BindLinks(extendedMenu.Where(Function(x) x.IsExpandable).OrderBy(Function(x) x.SortId).ToList())
        'End If
        BindSocialMediaLinks()
        'BindCopyright(extendedMenu.Where(Function(x) Not (x.IsExpandable) And Not (x.IsSupplemental)).OrderBy(Function(x) x.SortId).ToList())
        'BindSupplemental(extendedMenu)
    End Sub

    Private Sub BindSocialMediaLinks()
        BindMediaLinkList()
        If (MediaLinks.Count > 0) Then
            Dim i As Int32 = 0
            Dim ide As IDictionaryEnumerator = MediaLinks.GetEnumerator
            While ide.MoveNext()
                i += 1
                phSocialMediaLinks.Controls.Add(GetHtmlAnchor(ide.Key.ToString(), ide.Value.ToString(), (i = MediaLinks.Count)))
            End While
        End If
    End Sub

    Private Sub BindMediaLinkList()
        If (MediaLinks.Count <= 0) Then
            MediaLinks.Add("https://facebook.com/teledynelecroy", "facebook.jpg")
            MediaLinks.Add("https://twitter.com/teledynelecroy", "twitter.jpg")
            MediaLinks.Add("https://youtube.com/lecroycorp", "youtube.jpg")
        End If
    End Sub

    Private Function GetHtmlAnchor(ByVal href As String, ByVal imageName As String, ByVal isLastElement As Boolean) As HtmlAnchor
        Dim retVal As MyHtmlAnchor = New MyHtmlAnchor()
        retVal.HRef = href
        retVal.AddThisText = String.Format("GaEventPush('OutboundLink', '{0}');", href)
        retVal.Controls.Add(GetHtmlImage(imageName, isLastElement))
        Return retVal
    End Function

    Private Function GetHtmlImage(ByVal imageName As String, ByVal isLastElement As Boolean) As HtmlImage
        Dim retVal As HtmlImage = New HtmlImage()
        retVal.Src = String.Format("/images/{0}", imageName)
        If Not (isLastElement) Then retVal.Attributes.Add("style", "padding-right: 6px;")
        Return retVal
    End Function
#End Region
End Class