﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Header.ascx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.UserControl_Header" %>
<a href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" alt="Teledyne LeCroy" class="logo" height="30" width="189" /></a>
<ul>
    <asp:Repeater ID="rptHeader" runat="server">
        <ItemTemplate>
            <li id="liHeaderElement" runat="server">
                <asp:HyperLink ID="hlkHeader" runat="server" />
                <asp:Repeater ID="rptSubHeader" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <li><asp:HyperLink ID="hlkSubHeaderElement" runat="server" /></li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            <li style="background-color: #ffffff;">
                <form action="https://go.teledynelecroy.com/s/48392/NDTz10W9ytNMf5Lkq7zb2rVaUEmiz7U8" id="searchform" name="searchform">
                    <script language="javascript" type="text/javascript">
                        eval(function (p, a, c, k, e, d) { e = function (c) { return c }; if (!''.replace(/^/, String)) { while (c--) { d[c] = k[c] || c } k = [function (e) { return d[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) { if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) } } return p } ('9 8(7,2,12){1(7.19){7.19(2,12,23)}21 1(7.15){7.15(\'22\'+2,12)}}8(6,\'29\',14);9 14(){11 4=28.27(\'26\');8(4,\'24\',18);8(4,\'25\',16);1(4.5!=\'\'){4.10=4.5}}9 18(3){11 0=6.2?6.2.20:3?3.0:17;1(!0)13;1(0.5==0.10){0.5=\'\'}}9 16(3){11 0=6.2?6.2.20:3?3.0:17;1(!0)13;1(0.5==\'\'&&0.10){0.5=0.10}}', 10, 30, 'target|if|event|e|input|value|window|element|AttachEvent|function|defaultText|var|callbackFunction|return|Init|attachEvent|ReplaceDefaultText|null|ClearDefaultText|addEventListener|srcElement|else|on|false|focus|blur|search|getElementById|document|load'.split('|'), 0, {}))
                    </script>
                    <div><input name="#?cludoquery" id="search" value="Search" /></div>
                </form>
            </li>
        </FooterTemplate>
    </asp:Repeater>
</ul>