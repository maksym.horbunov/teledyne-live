﻿Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class UserControl_Header
    Inherits UserControl

#Region "Variables/Keys"
#End Region

#Region "Page Events"
#End Region

#Region "Control Events"
    Private Sub rptHeader_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptHeader.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedMenu = CType(e.Item.DataItem, ExtendedMenu)
                Dim liHeaderElement As HtmlGenericControl = CType(e.Item.FindControl("liHeaderElement"), HtmlGenericControl)
                Dim hlkHeader As HyperLink = CType(e.Item.FindControl("hlkHeader"), HyperLink)
                Dim rptSubHeader As Repeater = CType(e.Item.FindControl("rptSubHeader"), Repeater)

                If Not (String.IsNullOrEmpty(row.CssClassOverride)) Then
                    liHeaderElement.Attributes.Add("class", row.CssClassOverride)
                End If
                If Not (String.IsNullOrEmpty(row.ImageUrl)) Then
                    'Dim imgUrl As Image = New Image()
                    'imgUrl.AlternateText = row.MenuName
                    'imgUrl.ImageUrl = row.ImageUrl
                    'imgUrl.Attributes.Add("style", "padding-top: 10px;")
                    'hlkHeader.Controls.Add(imgUrl)
                Else
                    If Not (String.IsNullOrEmpty(row.Url)) Then
                        If (String.Compare("#", row.Url, True) = 0) Then
                            If (String.Compare(ConfigurationManager.AppSettings("LocaleId"), "2057", True) = 0) Then  ' HACK: Big, big, big one...
                                hlkHeader.NavigateUrl = String.Format("{0}/{1}/english.aspx", ConfigurationManager.AppSettings("BaseUrl"), ConfigurationManager.AppSettings("CountrySubDomain"))
                                hlkHeader.Text = row.MenuName
                            Else
                                hlkHeader.NavigateUrl = String.Format("{0}/{1}/#", ConfigurationManager.AppSettings("BaseUrl"), ConfigurationManager.AppSettings("CountrySubDomain"))
                                hlkHeader.Text = row.MenuName
                            End If
                        Else
                            hlkHeader.NavigateUrl = row.Url
                            hlkHeader.Text = row.MenuName
                        End If
                    Else
                        hlkHeader.NavigateUrl = String.Format("{0}/{1}/#", ConfigurationManager.AppSettings("BaseUrl"), ConfigurationManager.AppSettings("CountrySubDomain"))
                        hlkHeader.Text = row.MenuName
                    End If
                End If

                If (row.SubMenu.Count() > 0) Then
                    AddHandler rptSubHeader.ItemDataBound, AddressOf rptSubHeader_ItemDataBound
                    rptSubHeader.DataSource = row.SubMenu
                    rptSubHeader.DataBind()
                End If
        End Select
    End Sub

    Private Sub rptSubHeader_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As SubMenu = CType(e.Item.DataItem, SubMenu)
                Dim hlkSubHeaderElement As HyperLink = CType(e.Item.FindControl("hlkSubHeaderElement"), HyperLink)

                hlkSubHeaderElement.NavigateUrl = row.Url
                hlkSubHeaderElement.Text = row.SubMenuName
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Public Sub BindControl(ByVal extendedMenu As List(Of ExtendedMenu))
        If (extendedMenu.Count() > 0) Then
            rptHeader.DataSource = extendedMenu
            rptHeader.DataBind()
        End If
    End Sub
#End Region
End Class