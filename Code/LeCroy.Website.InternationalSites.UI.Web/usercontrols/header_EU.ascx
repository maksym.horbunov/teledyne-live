﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="header_EU.ascx.vb" Inherits="LeCroy.Website.InternationalSites.UI.Web.header_EU" %>
<div class="container">
    <div class="inner-header popup-holder">
        <a class="navbar-brand" href="/">
            <img src="/css/images/logo.svg" width="224" height="38" alt="Teledyne LeCroy"></a>
        <div class="holder-content">
            <div class="holder-nav">
                <!-- main navigation of the page-->
                <nav id="nav" role="navigation">
                    <div class="header-nav"><a class="nav-opener" href="#"><i class="icon-close"></i></a><span class="title">Menu</span></div>
                    <ul class="main-menu">
                        <li class="nav-item dropdown-item"><a class="nav-link opener" href="#"><span>Products &amp; Services</span><i class="icon-arrow-down"></i></a>
                            <div class="drop">
                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>Products &amp; Services</span></a>
                                <div class="main-drop-content tabs-area">
                                    <ul class="tabs-menu container">
                                        <li class="active"><a class="opener" href="#" data-href="#tab-menu-1"><span>By Category</span><i class="icon-arrow-down d-lg-none"></i></a>
                                            <div class="drop d-lg-none">
                                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>By Category</span></a>
                                                <ul>
                                                    <li class="column-content">
                                                        <h6><a href="/scopes/">Oscilloscopes</a></h6>
                                                        <ul>
                                                            <li><a href="/hdo/"><span>High Definition Oscilloscopes (HDO)</span></a></li>
                                                            <li><a href="/oscilloscope/"><span>Oscilloscopes</span></a></li>
                                                            <li><a href="/probes/"><span>Oscilloscope Probes</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6><a href="/testtools/">Electronic Test Equipment</a></h6>
                                                        <ul>
                                                            <li><a href="/tdr-and-s-parameters/default.aspx"><span>TDR and S-parameters</span></a></li>
                                                            <li><a href="/digital-multimeters/default.aspx"><span>Digital Multimeters</span></a></li>
                                                            <li><a href="/power-supplies/default.aspx"><span>Power Supplies</span></a></li>
                                                            <li><a href="/waveform-function-generators/"><span>Arbitrary Waveform Generators</span></a></li>
                                                            <li><a href="/electronic-loads/default.aspx"><span>Electronic Loads</span></a></li>
                                                            <li><a href="/logicanalyzers/"><span>Logic Analyzers</span></a></li>
                                                            <li><a href="/spectrum-analyzers/default.aspx"><span>Spectrum Analyzers</span></a></li>
                                                            <li><a href="/vector-network-analyzer/default.aspx"><span>Vector Network Analyzer</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6><a href="/protocolanalyzer/">Protocol Analysis Solutions</a></h6>
                                                        <ul>
                                                            <li><a href="/protocolanalyzer/CXL"><span>Compute Express Link (CXL)</span></a></li>
                                                            <li><a href="/protocolanalyzer/ethernet"><span>Ethernet</span></a></li>
                                                            <li><a href="/protocolanalyzer/fibre-channel"><span>Fibre Channel</span></a></li>
                                                            <li><a href="/protocolanalyzer/gen-z"><span>Gen-Z</span></a></li>
                                                            <li><a href="/protocolanalyzer/mipi"><span>MIPI (CSI, DSI and UFS)</span></a></li>
                                                            <li><a href="/protocolanalyzer/nvm-express"><span>NVM Express</span></a></li>
                                                            <li><a href="/protocolanalyzer/pci-express"><span>PCI Express</span></a></li>
                                                            <li><a href="/protocolanalyzer/serial-ata"><span>Serial ATA (SATA)</span></a></li>
                                                            <li><a href="/protocolanalyzer/serial-attached-scsi"><span>Serial Attached SCSI (SAS)</span></a></li>
                                                            <li><a href="/protocolanalyzer/usb"><span>USB</span></a></li>
                                                            <li><a href="https://www.quantumdata.com/"><span>Video (HDMI, DisplayPort)</span></a></li>
                                                            <li><a href="http://fte.com/bluetooth"><span>Wireless (Bluetooth, 802.11, 802.15.4)</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6><a href="https://www.spdevices.com/">Modular Data Acquisition</a></h6>
                                                        <ul>
                                                            <li><a href="https://www.spdevices.com/products/hardware#14-bit-digitizers"><span>Digitizers</span></a></li>
                                                            <li><a href="https://www.spdevices.com/products/hardware#waveform-generators"><span>Waveform Generators</span></a></li>
                                                            <li><a href="https://www.spdevices.com/products/hardware#extension-modules"><span>Extension Modules</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6><a href="https://teledyne-ts.com/">Sensors and Calibrators</a></h6>
                                                        <ul>
                                                            <li><a href="https://teledyne-ts.com/straingage.html"><span>Strain Gage Services</span></a></li>
                                                            <li><a href="https://teledyne-ts.com/valvetest.html"><span>Valve Testing Products &amp; Services</span></a></li>
                                                            <li><a href="https://teledyne-ts.com/tqsensor.html"><span>Torque Sensors &amp; Load Cells</span></a></li>
                                                            <li><a href="https://teledyne-ts.com/autotqtest.html"><span>Automotive Torque Testing</span></a></li>
                                                            <li><a href="https://teledyne-ts.com/tqwrench.html"><span>Torque Wrench Products &amp; Calibrators</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6>Services</h6>
                                                        <ul>
                                                            <li><a href="/services/austinlabs.aspx"><span>Protocol Technology Testing and Training</span></a></li>
                                                            <li><a href="http://fte.com/services/default.aspx"><span>Automotive and Device Interoperability Testing</span></a></li>
                                                            <li><a href="https://teledyne-ts.com/valvetest.html"><span>Diagnostic Valve Testing / Device Validation / Strain Gage Calibration</span></a></li>
                                                            <li><a href="https://bethesignal.com/bogatin/"><span>Signal Integrity Academy</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="column-content">
                                                        <h6><a href="https://www.oakgatetech.com/">SSD Device Testing</a></h6>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a class="opener" href="#" data-href="#tab-menu-2"><span>By Serial Data Standard</span><i class="icon-arrow-down d-lg-none"></i></a>
                                            <div class="drop d-lg-none">
                                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>By Serial Data Standard</span></a>
                                                <div class="content">
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/memory.aspx">Memory</a></h6>
                                                        <ul>
                                                            <li><span>CCIX</span></li>
                                                            <li><span>DDR/LPDDR</span></li>
                                                            <li><span>Gen-Z</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/storage.aspx">Storage</a></h6>
                                                        <ul>
                                                            <li><span>NVMe</span></li>
                                                            <li><span>SAS</span></li>
                                                            <li><span>SATA</span></li>
                                                            <li><span>UFS / M-Phy</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/peripherals.aspx">Peripherals</a></h6>
                                                        <ul>
                                                            <li><span>Thunderbolt</span></li>
                                                            <li><span>USB &amp; USB-C</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/video.aspx">Video</a></h6>
                                                        <ul>
                                                            <li><span>DisplayPort</span></li>
                                                            <li><span>HDMI</span></li>
                                                            <li><span>HDCP</span></li>
                                                            <li><span>HDBaseT</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/wireless.aspx">Wireless</a></h6>
                                                        <ul>
                                                            <li><span>Bluetooth</span></li>
                                                            <li><span>Thread (802.15.4)</span></li>
                                                            <li><span>Wi-Fi</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/networks.aspx">Networks</a></h6>
                                                        <ul>
                                                            <li><span>Fibre Channel</span></li>
                                                            <li><span>Ethernet</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/pci_express.aspx">PCI Express</a></h6>
                                                        <ul>
                                                            <li><span>PCIe</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/encoding_schemes.aspx">Encoding Schemes</a></h6>
                                                        <ul>
                                                            <li><span>8b/10b</span></li>
                                                            <li><span>64b/66b</span></li>
                                                            <li><span>Manchester</span></li>
                                                            <li><span>NRZ</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/automotive.aspx">Automotive</a></h6>
                                                        <ul>
                                                            <li><span>Automotive Ethernet</span></li>
                                                            <li><span>CAN/CAN FD/J1939</span></li>
                                                            <li><span>FlexRay</span></li>
                                                            <li><span>LIN</span></li>
                                                            <li><span>Manchester</span></li>
                                                            <li><span>MOST50/150</span></li>
                                                            <li><span>SENT</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/embedded.aspx">Embedded</a></h6>
                                                        <ul>
                                                            <li><span>10/100/1000Base-T</span></li>
                                                            <li><span>Audio (I2S, LJ, RJ, TDM)</span></li>
                                                            <li><span>DDR/LPDDR</span></li>
                                                            <li><span>I2C</span></li>
                                                            <li><span>I3C</span></li>
                                                            <li><span>SPI</span></li>
                                                            <li><span>UART-RS232</span></li>
                                                            <li><span>USB2-HSIC</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/mil_aero_avionics.aspx">Mil/Aero/Avionics</a></h6>
                                                        <ul>
                                                            <li><span>ARINC 429</span></li>
                                                            <li><span>MIL-STD-1553</span></li>
                                                            <li><span>SPACEWIRE</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/mipi.aspx">MIPI</a></h6>
                                                        <ul>
                                                            <li><span>C-PHY</span></li>
                                                            <li><span>D-PHY</span></li>
                                                            <li><span>M-PHY</span></li>
                                                            <li><span>DigRF3G</span></li>
                                                            <li><span>DigRFv4</span></li>
                                                            <li><span>I3C</span></li>
                                                            <li><span>UniPro</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="sd-list">
                                                        <h6><a href="/SerialData/power_management.aspx">Power Management</a></h6>
                                                        <ul>
                                                            <li><span>PMBus</span></li>
                                                            <li><span>SMBus</span></li>
                                                            <li><span>SPMI</span></li>
                                                            <li><span>USB PD</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="tab-holder d-none d-lg-block">
                                        <div id="tab-menu-1">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col col-lg-3">
                                                        <div class="column-content pb-0">
                                                            <h6><a href="/scopes/">Oscilloscopes</a></h6>
                                                            <ul>
                                                                <li><a href="/hdo/"><span>High Definition Oscilloscopes (HDO)</span></a></li>
                                                                <li><a href="/oscilloscope/"><span>Oscilloscopes</span></a></li>
                                                                <li><a href="/probes/"><span>Oscilloscope Probes</span></a></li>
                                                            </ul>
                                                            <h6><a href="/testtools/">Electronic Test Equipment</a></h6>
                                                            <ul>
                                                                <li><a href="/tdr-and-s-parameters/default.aspx"><span>TDR and S-parameters</span></a></li>
                                                                <li><a href="/digital-multimeters/default.aspx"><span>Digital Multimeters</span></a></li>
                                                                <li><a href="/power-supplies/default.aspx"><span>Power Supplies</span></a></li>
                                                                <li><a href="/waveform-function-generators/"><span>Arbitrary Waveform Generators</span></a></li>
                                                                <li><a href="/electronic-loads/default.aspx"><span>Electronic Loads</span></a></li>
                                                                <li><a href="/logicanalyzers/"><span>Logic Analyzers</span></a></li>
                                                                <li><a href="/spectrum-analyzers/default.aspx"><span>Spectrum Analyzers</span></a></li>
                                                                <li><a href="/vector-network-analyzer/default.aspx"><span>Vector Network Analyzer</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col col-lg-3">
                                                        <div class="column-content pb-0">
                                                            <h6><a href="/protocolanalyzer/">Protocol Analysis Solutions</a></h6>
                                                            <ul>
                                                                <li><a href="/protocolanalyzer/CXL"><span>Compute Express Link (CXL)</span></a></li>
                                                                <li><a href="/protocolanalyzer/ethernet"><span>Ethernet</span></a></li>
                                                                <li><a href="/protocolanalyzer/fibre-channel"><span>Fibre Channel</span></a></li>
                                                                <li><a href="/protocolanalyzer/gen-z"><span>Gen-Z</span></a></li>
                                                                <li><a href="/protocolanalyzer/mipi"><span>MIPI (CSI, DSI and UFS)</span></a></li>
                                                                <li><a href="/protocolanalyzer/nvm-express"><span>NVM Express</span></a></li>
                                                                <li><a href="/protocolanalyzer/pci-express"><span>PCI Express</span></a></li>
                                                                <li><a href="/protocolanalyzer/serial-ata"><span>Serial ATA (SATA)</span></a></li>
                                                                <li><a href="/protocolanalyzer/serial-attached-scsi"><span>Serial Attached SCSI (SAS)</span></a></li>
                                                                <li><a href="/protocolanalyzer/usb"><span>USB</span></a></li>
                                                                <li><a href="https://www.quantumdata.com/"><span>Video (HDMI, DisplayPort)</span></a></li>
                                                                <li><a href="http://fte.com/bluetooth"><span>Wireless (Bluetooth, 802.11, 802.15.4)</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="coln col-lg-3">
                                                        <div class="column-content pb-0">
                                                            <h6><a href="https://www.spdevices.com/products/hardware">Modular Data Acquisition</a></h6>
                                                            <ul>
                                                                <li><a href="https://www.spdevices.com/products/hardware#14-bit-digitizers"><span>Digitizers</span></a></li>
                                                                <li><a href="https://www.spdevices.com/products/hardware#waveform-generators"><span>Waveform Generators</span></a></li>
                                                                <li><a href="https://www.spdevices.com/products/hardware#extension-modules"><span>Extension Modules</span></a></li>
                                                            </ul>
                                                            <h6><a href="https://teledyne-ts.com/">Sensors and Calibrators</a></h6>
                                                            <ul>
                                                                <li><a href="https://teledyne-ts.com/straingage.html"><span>Strain Gage Services</span></a></li>
                                                                <li><a href="https://teledyne-ts.com/valvetest.html"><span>Valve Testing Products &amp; Services</span></a></li>
                                                                <li><a href="https://teledyne-ts.com/tqsensor.html"><span>Torque Sensors &amp; Load Cells</span></a></li>
                                                                <li><a href="https://teledyne-ts.com/autotqtest.html"><span>Automotive Torque Testing</span></a></li>
                                                                <li><a href="https://teledyne-ts.com/tqwrench.html"><span>Torque Wrench Products &amp; Calibrators</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col col-lg-3">
                                                        <div class="column-content pb-0">
                                                            <h6>Services</h6>
                                                            <ul>
                                                                <li><a href="/services/austinlabs.aspx"><span>Protocol Technology Testing and Training</span></a></li>
                                                                <li><a href="http://fte.com/services/default.aspx"><span>Automotive and Device Interoperability Testing</span></a></li>
                                                                <li><a href="http://teledyne-ts.com/valvetest.html"><span>Diagnostic Valve Testing / Device Validation / Strain Gage Calibration</span></a></li>
                                                                <li><a href="https://bethesignal.com/bogatin/"><span>Signal Integrity Academy</span></a></li>
                                                            </ul>
                                                            <h6><a href="https://www.oakgatetech.com/">SSD Device Testing</a></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-menu-2">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/memory.aspx">Memory</a></h6>
                                                            <ul>
                                                                <li><span>CCIX</span></li>
                                                                <li><span>DDR/LPDDR</span></li>
                                                                <li><span>Gen-Z</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/storage.aspx">Storage</a></h6>
                                                            <ul>
                                                                <li><span>NVMe</span></li>
                                                                <li><span>SAS</span></li>
                                                                <li><span>SATA</span></li>
                                                                <li><span>UFS / M-Phy</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/peripherals.aspx">Peripherals</a></h6>
                                                            <ul>
                                                                <li><span>Thunderbolt</span></li>
                                                                <li><span>USB &amp; USB-C</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/video.aspx">Video</a></h6>
                                                            <ul>
                                                                <li><span>DisplayPort</span></li>
                                                                <li><span>HDMI</span></li>
                                                            <li><span>HDCP</span></li>
                                                            <li><span>HDBaseT</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/wireless.aspx">Wireless</a></h6>
                                                            <ul>
                                                                <li><span>Bluetooth</span></li>
                                                                <li><span>Thread (802.15.4)</span></li>
                                                                <li><span>Wi-Fi</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content">
                                                            <h6><a href="/SerialData/networks.aspx">Networks</a></h6>
                                                            <ul>
                                                                <li><span>Fibre Channel</span></li>
                                                                <li><span>Ethernet</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/pci_express.aspx">PCI Express</a></h6>
                                                            <ul>
                                                                <li><span>PCIe</span></li>
                                                            </ul>
                                                            <h6><a href="/SerialData/encoding_schemes.aspx">Encoding Schemes</a></h6>
                                                            <ul>
                                                                <li><span>8b/10b</span></li>
                                                                <li><span>64b/66b</span></li>
                                                                <li><span>Manchester</span></li>
                                                                <li><span>NRZ</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/automotive.aspx">Automotive</a></h6>
                                                            <ul>
                                                                <li><span>Automotive Ethernet</span></li>
                                                                <li><span>CAN/CAN FD/J1939</span></li>
                                                                <li><span>FlexRay</span></li>
                                                                <li><span>LIN</span></li>
                                                                <li><span>Manchester</span></li>
                                                                <li><span>MOST50/150</span></li>
                                                                <li><span>SENT</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/embedded.aspx">Embedded</a></h6>
                                                            <ul>
                                                                <li><span>10/100/1000Base-T</span></li>
                                                                <li><span>Audio (I2S, LJ, RJ, TDM)</span></li>
                                                                <li><span>DDR/LPDDR</span></li>
                                                                <li><span>I2C</span></li>
                                                                <li><span>I3C</span></li>
                                                                <li><span>SPI</span></li>
                                                                <li><span>UART-RS232</span></li>
                                                                <li><span>USB2-HSIC</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/mil_aero_avionics.aspx">Mil/Aero/Avionics</a></h6>
                                                            <ul>
                                                                <li><span>ARINC 429</span></li>
                                                                <li><span>MIL-STD-1553</span></li>
                                                                <li><span>SPACEWIRE</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/mipi.aspx">MIPI</a></h6>
                                                            <ul>
                                                                <li><span>C-PHY</span></li>
                                                                <li><span>D-PHY</span></li>
                                                                <li><span>M-PHY</span></li>
                                                                <li><span>DigRF3G</span></li>
                                                                <li><span>DigRFv4</span></li>
                                                                <li><span>I3C</span></li>
                                                                <li><span>UniPro</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="column-content pb-lg-0">
                                                            <h6><a href="/SerialData/power_management.aspx">Power Management</a></h6>
                                                            <ul>
                                                                <li><span>PMBus</span></li>
                                                                <li><span>SMBus</span></li>
                                                                <li><span>SPMI</span></li>
                                                                <li><span>USB PD</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown-item"><a class="nav-link opener" href="#"><span>Resources</span><i class="icon-arrow-down"></i></a>
                            <div class="drop">
                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>Resources</span></a>
                                <div class="main-drop-content">
                                    <ul>
                                        <li><a href="/support/softwaredownload/"><span>Software Downloads</span></a></li>
                                        <li><a href="/support/techlib/"><span>Technical Library</span></a></li>
                                        <li><a href="/support/techlib/videos.aspx"><span>Video Library</span></a></li>
                                        <li><a href="/events/"><span>Events &amp; Training</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown-item"><a class="nav-link opener" href="#"><span>Support</span><i class="icon-arrow-down"></i></a>
                            <div class="drop">
                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>Support</span></a>
                                <div class="main-drop-content">
                                    <ul>
                                        <li><a href="/support/service.aspx"><span>Instrument Service / Calibration</span></a></li>
                                        <li><a href="/support/register/"><span>Product Registration</span></a></li>
                                        <li><a href="/support/training.aspx"><span>Training</span></a></li>
                                        <li><a href="/support/user/"><span>Technical Support</span></a></li>
                                        <li><a href="/support/rohs.aspx"><span>RoHS &amp; WEEE</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown-item"><a class="nav-link opener" href="#"><span>Buy</span><i class="icon-arrow-down"></i></a>
                            <div class="drop">
                                <a class="nav-back" href="#"><i class="icon-arrow-down"></i><span>Buy</span></a>
                                <div class="main-drop-content">
                                    <ul>
                                        <li><a href="/shopper/requestquote/"><span>Request a Quote</span></a></li>
                                        <li><a href="/support/contact/"><span>Contact Us</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="block-signin">
                        <p>Sign in to your<br>
                            Teledyne Lecroy account</p>
                        <a class="btn btn-default" href="#">Sign In</a>
                    </div>
                </nav>
            </div>
            <ul class="holder-icons">
                <li><a class="open-search" id="open-search" href="#"><i class="icon-search"></i></a>
                    <div class="popup">
                        <form class="form-inline w-100" id="searchform" action="https://go.teledynelecroy.com/s/48392/NDTz10W9ytNMf5Lkq7zb2rVaUEmiz7U8" name="searchform">
                            <script>eval(function (p, a, c, k, e, d) { e = function (c) { return c }; if (!''.replace(/^/, String)) { while (c--) { d[c] = k[c] || c } k = [function (e) { return d[e] }]; e = function () { return '\\\w+' }; c = 1 }; while (c--) { if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) } } return p }('9 8(7,2,12){1(7.19){7.19(2,12,23)}21 1(7.15){7.15(\'22\'+2,12)}}8(6,\'29\',14);9 14(){11 4=28.27(\'26\');8(4,\'24\',18);8(4,\'25\',16);1(4.5!=\'\'){4.10=4.5}}9 18(3){11 0=6.2?6.2.20:3?3.0:17;1(!0)13;1(0.5==0.10){0.5=\'\'}}9 16(3){11 0=6.2?6.2.20:3?3.0:17;1(!0)13;1(0.5==\'\'&&0.10){0.5=0.10}}', 10, 30, 'target|if|event|e|input|value|window|element|AttachEvent|function|defaultText|var|callbackFunction|return|Init|attachEvent|ReplaceDefaultText|null|ClearDefaultText|addEventListener|srcElement|else|on|false|focus|blur|search|getElementById|document|load'.split('|'), 0, {}))</script>
                            <input class="form-control py-0" id="search" type="search" tabindex name="#?cludoquery" placeholder="Search TeledyneLeCroy.com" aria-label="Search">
                            <button class="form-control close" type="button"><i class="icon-close"></i></button>
                        </form>
                    </div>
                </li>
                <li class="dropdown-item dropdown-languages"><a href="#"><i class="icon-world"></i></a>
                    <div class="drop">
                        <ul class="list-languages">
                            <li><a href="/">English (United States)</a></li>
                            <li class="active"><a href="/europe/english.aspx">English (Europe)</a></li>
                            <li><a href="/france/">Français</a></li>
                            <li><a href="/germany/">Deutsch</a></li>
                            <li><a href="/it/">Italiano</a></li>
                            <li><a href="/japan/">日本語</a></li>
                            <li><a href="/korea/">한국어</a></li>
                            <li><a href="http://www.teledynelecroy.com.cn/">中文</a></li>
                        </ul>
                    </div>
                </li>
                <li class="link-signin"><a href="/support/user/"><i class="icon-profile"></i></a></li>
                <li class="hold-burger"><a class="nav-opener" href="#"><i></i></a></li>
            </ul>
        </div>
    </div>
</div>
