﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class OneHundredghz_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - 100 GHz Real-Time Oscilloscope"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class