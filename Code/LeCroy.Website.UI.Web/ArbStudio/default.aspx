<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.AWG_ArbStudio" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="370" valign="top">
                    <img src="<%=rootDir %>/images/arbstudio-text.png" alt="LogicStudio Logic Analyzers" />
                    <p>ArbStudio combines high performance hardware and a variety of waveform generation modes to address a wide range of signal generator requirements.<p>
                    <a href="<%=rootDir %>/support/softwaredownload/arbstudio.aspx"><img src="/images/buttons/try_it_free.png" border="0" /></a>&nbsp;</p>
                </td>
                <td width="248" valign="top"><img src="<%=rootDir %>/images/arbstudio.png" width="242"  border="0" /></td>
                <td width="203" valign="top">
                    <div class="subNav">
                        <ul>
                            <li><a href="<%=rootDir %>/arbstudio/arbstudio.aspx">Explore ArbStudio Features</a></li>
                            <li><a href="<%=rootDir %>/support/softwaredownload/documents.aspx?sc=23">Download Software</a></li>
                            <li><a href="<%=rootDir %>/support/techlib/productmanuals.aspx?type=2&cat=2">Manual</a></li>
                            <li><a href="/arbstudio/arbstudio.aspx#spec">Specifications</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <h3>Unmatched Performance</h3>
                    <p>Generate waveforms with 125 MHz analog bandwidth, 2 Mpts/Ch memory, 1 GS/s sample rate and 16-bit resolution.</p>
                    <h3>Intuitive User Interface</h3>
                    <p>Quickly create, edit and sequence waveforms on up to 4 channels simultaneously with easy access to all channels, settings and controls.</p>
                    <h3>Digital Pattern Generator</h3>
                    <p>Create digital waveforms, patterns and busses of up to 36 channels and output analog and digital waveforms simultaneously.</p>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top"><img src="<%=rootDir %>/images/arbstudio/arbstudio_main.png" height="250px" /></td>
            </tr>
        </table>
        <div class="rounded2">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left" valign="top"><img src="/images/arbstudio/arbstudio_laptop.png" align="left" /></td>
                    <td width="50px">&nbsp;</td>
                    <td align="center" class="right" valign="middle">
                        <br /><br /><h1>ArbStudio Arbitrary Waveform Generator</h1>
                        <h4>Meet the needs of today's engineers and technicians with uncompromised performance</h4><br />
                        <a href="<%=rootDir %>/arbstudio/arbstudio.aspx?<%=menuURL %>"><img src="/images/buttons/explore_arbstudio.png" border="0" /></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>