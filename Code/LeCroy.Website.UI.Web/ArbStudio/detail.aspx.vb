﻿'Imports System.Data.SqlClient
'Imports LeCroy.Library.DAL.Common
'Imports LeCroy.Library.Domain.Common.Constants
'Imports LeCroy.Library.Domain.Common.DTOs
'Imports LeCroy.Library.VBUtilities
'Imports LeCroy.Website.BLL

Public Class ArbStudio_detail
    Inherits BasePage

    '#Region "Variables/Keys"
    '    Private Const CATEGORY_ID As Int32 = 2
    '    Private Const PRODUCT_SERIES_ID As Int32 = 276
    '    Private Const CAPTION_ID As Int32 = 131
    '    Private Const MENU_ID As Int32 = 1043

    '    Dim tabsToShow As StringBuilder = New StringBuilder()
    '    Dim hasOverview As Boolean = False
    '    Dim hasDetail As Boolean = False
    '#End Region

    '#Region "Page Events"
    '    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    '        Me.Master.PageContentHeader.TopText = "Products"
    '        Me.Master.PageContentHeader.BottomText = "Arbitrary Waveform Generators"
    '        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - ArbStudio - Arbitrary Waveform Generator"
    '    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Redirect("/support/techlib/")
        '        Dim productId As Int32 = IsValidProductIdQueryString()

        '        If Not (FeProductManager.CanOptionShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId, CategoryIds.ARBITRARY_WAVEFORM_GENERATORS)) Then
        '            Response.Redirect("~/")
        '        End If

        '        If Not (IsPostBack) Then
        '            Session("cataid") = CATEGORY_ID
        '            Session("menuSelected") = CAPTION_ID
        '            'menuURL = String.Format("&capid={0}&mid={1}", captionId, menuId)
        '            BindPage(productId)
        '            ucResources.CategoryId = CATEGORY_ID
        '            ucResources.ProductSeriesId = PRODUCT_SERIES_ID
        '            ucResources.ProductId = productId
        '        End If
    End Sub
    '#End Region

    '#Region "Control Events"
    '#End Region

    '#Region "Page Methods"
    '    Private Function IsValidProductIdQueryString() As Int32
    '        Dim retVal As Int32 = 0
    '        If Len(Request.QueryString("modelid")) = 0 Then
    '            Response.Redirect("/arbstudio/")
    '        Else
    '            If IsNumeric(Request.QueryString("modelid")) Then
    '                retVal = Request.QueryString("modelid")
    '            Else
    '                Response.Redirect("/arbstudio/")
    '            End If
    '        End If
    '        Return retVal
    '    End Function

    '    Private Sub BindPage(ByVal productId As Int32)
    '        Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId)
    '        If Not (product Is Nothing) Then
    '            Me.lbModelImage.Text = "<img src='" + rootDir + product.IntroImagePath + "'  width='540' height='211'>"
    '            Me.landTitle.Text = product.Name
    '            Me.landContent.Text = product.Introduction
    '            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + product.Name
    '        End If

    '        ucResources.ShowRequestQuote = True
    '        If Not Request.Cookies("LCCOUNTRY") Is Nothing Then
    '            If Len(Request.Cookies("LCCOUNTRY").Value.ToString) > 0 Then
    '                If Request.Cookies("LCCOUNTRY").Value.ToString = "208" Then
    '                    If Functions.ModelHasEStoreLink(productId, Session("contactid")).ToString.Length > 0 Then
    '                        ucResources.ShowBuy = True
    '                    End If
    '                End If
    '            End If
    '        End If
    '        Dim additionalResources As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId, Int32.Parse(AppConstants.ADDITIONAL_RESOURCES_PRODUCT))
    '        If Not (additionalResources Is Nothing) Then
    '            lblAdditionalResources.Text = additionalResources.OverviewDetails
    '        End If
    '        getProductLine()
    '        getOverview()
    '        getSpecs()
    '        getOptions()
    '        getProductDetail(productId)
    '        If Not Request.Cookies("LCCOUNTRY") Is Nothing Then
    '            If Len(Request.Cookies("LCCOUNTRY").Value.ToString) > 0 Then
    '                If Request.Cookies("LCCOUNTRY").Value.ToString = "208" Then
    '                    getListPrice()
    '                End If
    '            End If
    '        End If
    '        lblTabsToShow.Text = tabsToShow.ToString()
    '        If hasDetail Then
    '            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
    '        ElseIf hasOverview Then
    '            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
    '        Else
    '            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
    '        End If
    '        Me.lb_leftmenu.Text = Functions.LeftMenu(CAPTION_ID, rootDir, pn_leftmenu, MENU_ID)
    '        'Me.menulabel.Visible = False
    '    End Sub

    '    Private Sub getProductLine()
    '        Dim productLine As String = String.Empty
    '        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
    '        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", PRODUCT_SERIES_ID.ToString()))
    '        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT distinct b.PRODUCTID, b.PARTNUMBER, B.PRODUCTDESCRIPTION, b.IMAGEFILENAME, b.INTRODUCTION, b.sort_id from product_series a INNER JOIN product_series_category psc on a.product_series_id = psc.product_series_id inner join product b ON psc.PRODUCT_ID = b.productid where b.disabled='n' and a.PRODUCT_SERIES_ID = @PRODUCTSERIESID order by b.sort_id", sqlParameters.ToArray())
    '        If (products.Count > 0) Then
    '            productLine += " <div class='searchResults'> "
    '            productLine += "   <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
    '            For i As Int32 = 0 To products.Count - 1
    '                productLine = productLine + "<tr><td class='cell'>"
    '                If (Len(products(i).Introduction) > 0) Then
    '                    productLine += "<strong><u><a href='/arbstudio/detail.aspx?modelid=" + products(i).ProductId.ToString() + "'>" + products(i).PartNumber + "</a></u></strong><br />" + Functions.GetProdDescriptionOnProdID(products(i).ProductId).ToString
    '                Else
    '                    productLine += "<strong><u>" + products(i).PartNumber + "</u></strong><br />" + Functions.GetProdDescriptionOnProdID(products(i).ProductId).ToString
    '                End If
    '                productLine = productLine + "</td></tr>"
    '            Next
    '            productLine = productLine + "</table>"
    '            productLine = productLine + "</div>"
    '            lb_productLine.Text = productLine
    '            tabsToShow.Append("<li><a href=""#product_line""><img src='")
    '            tabsToShow.Append(rootDir)
    '            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
    '        End If
    '    End Sub

    '    Private Sub getOverview()
    '        If (PRODUCT_SERIES_ID <= 0) Then
    '            Return
    '        End If
    '        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), PRODUCT_SERIES_ID, AppConstants.SERIES_OVERVIEW_TYPE)
    '        If Not (cmsOverview Is Nothing) Then
    '            Me.lb_overview.Text = cmsOverview.OverviewDetails
    '            tabsToShow.Append("<li><a href='#overview'><img src='")
    '            tabsToShow.Append(rootDir)
    '            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
    '        End If
    '    End Sub

    '    Private Sub getProductDetail(ByVal productId As Int32)
    '        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId, AppConstants.PRODUCT_OVERVIEW_TYPE)
    '        If Not (cmsOverview Is Nothing) Then
    '            If Not (String.IsNullOrEmpty(cmsOverview.OverviewDetails)) Then
    '                Me.lblProductDetail.Text = cmsOverview.OverviewDetails
    '                hasDetail = True
    '                tabsToShow.Append("<li><a href='#product_details'><img src='")
    '                tabsToShow.Append(rootDir)
    '                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
    '            End If
    '        End If
    '    End Sub

    '    Private Sub getSpecs()
    '        If (PRODUCT_SERIES_ID <= 0) Then
    '            Return
    '        End If
    '        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), PRODUCT_SERIES_ID, AppConstants.PROTOCOL_SPECS)
    '        If Not (cmsOverview Is Nothing) Then
    '            Me.lblSpecs.Text = cmsOverview.OverviewDetails
    '            tabsToShow.Append("<li><a href='#spec'><img src='")
    '            tabsToShow.Append(rootDir)
    '            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
    '        End If
    '    End Sub

    '    Private Sub getOptions()
    '        Dim sb As StringBuilder = New StringBuilder
    '        Dim sqlString As String = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND b.CATEGORY_ID = @CATEGORYID AND c.disabled='n' AND e.disabled='n' ORDER BY b.SORTID"
    '        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
    '        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", PRODUCT_SERIES_ID.ToString()))
    '        sqlParameters.Add(New SqlParameter("@CATEGORYID", CategoryIds.ARBITRARY_WAVEFORM_GENERATORS.ToString()))
    '        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    '        If (ds.Tables.Count > 0) Then
    '            For Each dr As DataRow In ds.Tables(0).Rows
    '                sb.AppendFormat("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='cell'><h3>{0}</h3></td></tr>", dr("GROUP_NAME").ToString())
    '                Dim groupDetailsSqlString As String = "SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, f.PRODUCT_SERIES_ID FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS f ON b.CATEGORY_ID = f.CATEGORY_ID AND c.PRODUCTID = f.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID and b.group_id = @GROUPID AND c.disabled='n' Order by c.sort_id"
    '                sqlParameters = New List(Of SqlParameter)
    '                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", PRODUCT_SERIES_ID.ToString()))
    '                sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))
    '                Dim groupDetails As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), groupDetailsSqlString, sqlParameters.ToArray())
    '                If (groupDetails.Tables.Count > 0) Then
    '                    For Each groupDr As DataRow In groupDetails.Tables(0).Rows
    '                        sb.AppendFormat("<tr><td class='cell'><strong><u><a href='{0}'>{1}</a></u></strong><br />{2}</td></tr>", String.Format("detail.aspx?modelid={0}", groupDr("PRODUCTID").ToString()), groupDr("PARTNUMBER").ToString(), Functions.GetProdDescriptionOnProdID(groupDr("PRODUCTID").ToString()).ToString())
    '                    Next
    '                End If
    '            Next
    '            sb.Append("</table>")
    '        End If
    '        tabsToShow.AppendFormat("<li><a href=""#options""><img src='{0}/images/tabs/tabs_options_off.gif' id='options' border='0' /></a></li>", rootDir)
    '        lblOptions.Text = sb.ToString()
    '    End Sub

    '    Private Sub getListPrice()
    '        Dim pNum As Integer
    '        Dim j As Integer
    '        Dim DsList As DataSet
    '        Dim strListPrice As String = ""

    '        If Functions.hasListPrice(PRODUCT_SERIES_ID) Then
    '            Dim strSQL As String = " SELECT distinct PRODUCT.PRODUCTID, PRODUCT.PARTNUMBER,PRODUCT.SORT_ID FROM  PRODUCT_SERIES_CATEGORY INNER JOIN  PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
    '                                   " WHERE PRODUCT.DISABLED = 'n' AND PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = @PRODUCTSERIESID ORDER BY PRODUCT.SORT_ID"
    '            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
    '            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", PRODUCT_SERIES_ID.ToString()))
    '            DsList = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
    '            pNum = DsList.Tables(0).Rows.Count()
    '            If pNum > 0 Then

    '                strListPrice = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>"
    '                strListPrice += "<tr>"
    '                strListPrice += "<td width='96' align='left' valign='top' class='title2'>Product</td>"
    '                strListPrice += "<td width='61' align='left' valign='top' class='title2'>Price*</td>"
    '                strListPrice += "<td width='176' align='left' valign='top' class='title2'>Description</td>"
    '                strListPrice += "<td width='115' align='left' valign='top'></td>"
    '                strListPrice += "</tr>"
    '                For j = 0 To pNum - 1
    '                    Dim dr As DataRow = DsList.Tables(0).Rows(j)
    '                    strListPrice += "<tr>"
    '                    strListPrice += "<td width='96' align='left' valign='top' class='title2'>" + dr("PARTNUMBER").ToString + "</td>"
    '                    strListPrice += "<td width='61' align='left' valign='top' class='cell'>" + FormatCurrency(Functions.GetProductListPriceonProductID(dr("PRODUCTID")).ToString(), 2) + "</td>"
    '                    strListPrice += "<td width='176' align='left' valign='top' class='cell'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</td>"
    '                    strListPrice += "<td width='115' align='left' valign='top'><a href='/oscilloscope/configure/configure_step2.aspx?seriesid=" + PRODUCT_SERIES_ID.ToString + "&modelid=" + dr("PRODUCTID").ToString() + "'><img src='/images/icons/icons_link.gif' align='absmiddle' border='0' />Request Quote</a></td>"
    '                    strListPrice += "</tr>"
    '                Next
    '                strListPrice += "</table>"
    '                strListPrice += "<p class='smaller'>* Prices Shown Are US Domestic Prices, MSRP, and are exclusive of <strong>local and state taxes, shipping, levies, fees, duties, exportation/importation costs, service and warranties outside the United States</strong>, and assume standard 30 day payment terms.</p>"
    '                strListPrice += "<p class='smaller'>Information and/or pricing may be changed or updated without notice. Teledyne LeCroy may also make improvements and/or changes in the products and/or the programs, as well as associated pricing, described in this information at any time without notice.</p>"
    '                strListPrice += "<p class='smaller'>Because international information is provided at this Web Site, not all products, programs or pricing mentioned will be available in your country. Please contact your local sales representative for information as to products and services available in your country.</p>"

    '                tabsToShow.Append("<li><a href='#listprice'><img src='")
    '                tabsToShow.Append(rootDir)
    '                tabsToShow.Append("/images/tabs/tabs_list_off.gif' id='listprice' border='0'></a></li>")
    '                Me.lblListPrice.Text = strListPrice
    '            End If
    '        End If
    '    End Sub
    '#End Region
End Class