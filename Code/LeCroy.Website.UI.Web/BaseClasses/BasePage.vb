Imports System
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions
Imports Newtonsoft.Json

Public Class BasePage
    Inherits System.Web.UI.Page
    Public Shared rootDir As String = ConfigurationManager.AppSettings("RootDir").ToString()

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        'If Session("localeid") Is Nothing Then
        '    Session("localeid") = "1033"
        'End If
        'localeid = Session("localeid").ToString()
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Fix for Safari and Chrome browsers
        'If Request.UserAgent.IndexOf("AppleWebKit") > 0 Then
        ' Request.Browser.Adapters.Clear()
        'End If

        ' Set the default root directory if it is defined in web.config file
        'If Not AppSettings("RootDir") Is Nothing Or Len(AppSettings("RootDir")) > 0 Then
        '    rootDir = AppSettings("RootDir").ToString
        'End If

        ' Set the localeid  in Session
        'Set localeid

        'If Session("localeid") Is Nothing Then
        '    Session("localeid") = "1033"
        'End If

        If Session("RedirectTo") Is Nothing Then
            Session("RedirectTo") = rootDir + "/default.aspx"
        End If

        HACK_DocumentBackButtonWorkaround()

        'localeid = Session("localeid").ToString()
        ValidateUserNoRedir()

        ' Check and set the Subscription user data.
        SetSubscriptionVariables()

        'GeoIP
        SetGeoIP()

        'If Not (String.IsNullOrEmpty(Request.QueryString("cc"))) Then
        '    Session("CountryCode") = Int32.Parse(Request.QueryString("cc"))
        '    Response.Cookies("LCCOUNTRY").Value = Request.QueryString("cc")
        'End If
        'If Not (String.IsNullOrEmpty(Request.QueryString("lig"))) Then
        '    Session("LocaleIdFromGeoIp") = Int32.Parse(Request.QueryString("lig"))
        'End If
        If Not (String.IsNullOrEmpty(Request.QueryString("loc"))) Then
            Session("localeid") = Int32.Parse(Request.QueryString("loc"))
        End If
    End Sub

    Protected Function SQLStringWithSingleQuotes(ByVal strinput As String) As String
        ' This method can be used to validate the data entered
        ' by the user on FORM for SQL Injection.  It removes all the malacious 
        ' characters and commands.  It also appends and pre-appends the variable
        ' with single quote (').
        Dim badChars As String() = New String() {"SELECT", "DROP", "select", "drop", "--",
                                                "INSERT", "insert", "DELETE", "delete", "_XP", "xp_", "1=1", "/* ... */", "Char("}

        If Not strinput Is Nothing Then
            If strinput.Length > 0 Then
                strinput = strinput.Trim()
                strinput = strinput.Replace("'", "''")
                strinput = "'" + strinput + "'"
                Dim i As Integer
                For i = 0 To badChars.Length - 1 Step i + 1
                    strinput = strinput.Replace(badChars(i), "")
                Next
            Else
                strinput = "''"
            End If
        Else
            strinput = "''"
        End If
        Return strinput
    End Function

    Protected Function SQLStringWithOutSingleQuotes(ByVal strinput As String) As String
        ' This method can be used to validate the data entered
        ' by the user on FORM for SQL Injection.  It removes all the malacious 
        ' characters and commands.  It also appends and pre-appends the variable
        ' with single quote (').
        Dim badChars As String() = New String() {"SELECT", "DROP", "select", "drop", "--",
                                                "INSERT", "insert", "DELETE", "delete", "_XP", "xp_", "1=1", "/* ... */", "Char("}

        If Not strinput Is Nothing Then
            If strinput.Length > 0 Then
                strinput = strinput.Trim()
                strinput = strinput.Replace("'", "''")
                Dim i As Integer
                For i = 0 To badChars.Length - 1 Step i + 1
                    strinput = strinput.Replace(badChars(i), "")
                Next
            Else
                strinput = ""
            End If
        Else
            strinput = ""
        End If
        Return strinput
    End Function

    Protected Function convertToString(ByVal inputObj As Object) As String
        If [Object].Equals(inputObj, Nothing) Or [Object].Equals(inputObj, System.DBNull.Value) Then
            Return ""
        Else
            Return ((inputObj.ToString()).Replace("NULL", "")).Replace("null", "")
        End If
    End Function

    Public Function ValidateUserNoRedir()
        ' This function validates if the user accessing the site
        ' is a valid user (Registered) or not.
        ' If user is not valid user it does NOT re-direct user to login page

        Dim GUIDFromUSER As String = ""
        Dim ContactIdTemp As String = ""


        If Not Session("ContactId") Is Nothing And Len(Session("ContactId")) > 0 Then
            If (ContactRepository.IsUserValidated(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId")))) Then
                ExpireExistingCookieAndIssueNewOne(Int32.Parse(Session("ContactId")))
                Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId")))
                If Not (contact Is Nothing) Then
                    SetUserDataIfValid(contact.ContactId, contact.ContactWebId)
                End If
                ValidateUserNoRedir = True
                Exit Function
            End If
        End If
        'Else
        ' Check the cookie
        If Not Request.Cookies("LCDATA") Is Nothing Then
            GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
        End If
        ' If Cookie is there
        If Len(GUIDFromUSER) > 0 Then
            ContactIdTemp = GetContactIDOnWebId(GUIDFromUSER)
            ' If we found the id in the database
            If Len(ContactIdTemp) > 0 Then
                If (ContactRepository.IsUserValidated(ConfigurationManager.AppSettings("ConnectionString").ToString(), ContactIdTemp)) Then
                    SetUserDataIfValid(Int32.Parse(ContactIdTemp), GUIDFromUSER)
                    ValidateUserNoRedir = True
                    Exit Function
                End If
                ValidateUserNoRedir = False
                Exit Function
                ' If not found
            Else
                'There is a problem:
                'We have the GUID but somewhat we don't have the
                ' contactID
                ValidateUserNoRedir = False
                Exit Function
            End If
        Else
            ' If we do not have the GUID
            ' There are 2 possibilities
            ' He refused the GUID but he is with username and password
            ' or he is first time here
            ' so:
            ValidateUserNoRedir = False
            Exit Function
        End If
        'End If
    End Function

    Public Sub SetUserDataIfValid(ByVal contactId As Int32, ByVal contactWebId As String)
        Session("ContactId") = contactId.ToString()
        Session("GUID") = contactWebId
        ExpireExistingCookieAndIssueNewOne(contactWebId)
        GetUserData(contactWebId)
    End Sub

    Private Sub ExpireExistingCookieAndIssueNewOne(ByVal contactId As Int32)
        Dim contact As Library.Domain.Common.DTOs.Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
        If (contact Is Nothing) Then Return
        If Not (contact.ContactId = contactId) Then Return
        ExpireExistingCookieAndIssueNewOne(contact.ContactWebId)
    End Sub

    Private Sub ExpireExistingCookieAndIssueNewOne(ByVal contactWebId As String)
        If Not (Request.Cookies("LCDATA") Is Nothing) Then
            Response.Cookies("LCDATA").Expires = DateTime.Now.AddDays(-1)
        End If
        Response.Cookies("LCDATA")("GUID") = contactWebId
        Response.Cookies("LCDATA").Expires = DateTime.Now.AddDays(365 * 5)
        Response.Cookies("LCDATA").Domain = ConfigurationManager.AppSettings("CookieDomain")
        Response.Cookies("LCDATA").HttpOnly = True
    End Sub

    Public Sub DumpUserDataFromSession()
        Session.Remove("ContactId")
        Session("ContactId") = Nothing
        Session.Remove("GUID")
        Session("GUID") = Nothing
        Session.Remove("Honorific")
        Session("Honorific") = Nothing
        Session.Remove("FirstName")
        Session("FirstName") = Nothing
        Session.Remove("LastName")
        Session("LastName") = Nothing
        Session.Remove("UserName")
        Session("UserName") = Nothing
        Session.Remove("Password")
        Session("Password") = Nothing
        Session.Remove("Title")
        Session("Title") = Nothing
        Session.Remove("Company")
        Session("Company") = Nothing
        Session.Remove("Address")
        Session("Address") = Nothing
        Session.Remove("Address2")
        Session("Address2") = Nothing
        Session.Remove("City")
        Session("City") = Nothing
        Session.Remove("State")
        Session("State") = Nothing
        Session.Remove("Zip")
        Session("Zip") = Nothing
        Session.Remove("Country")
        Session("Country") = Nothing
        Session.Remove("Phone")
        Session("Phone") = Nothing
        Session.Remove("Fax")
        Session("Fax") = Nothing
        Session.Remove("Email")
        Session("Email") = Nothing
        Session.Remove("URL")
        Session("URL") = Nothing
        Session.Remove("Applications")
        Session("Applications") = Nothing
        Session.Remove("JobFunction")
        Session("JobFunction") = Nothing
        Session.Remove("Department")
        Session("Department") = Nothing
    End Sub

    Sub GetUserData(ByVal strGUIDFromUser As String)
        Dim sqlString As String = "SELECT * FROM V_CONTACT WHERE CONTACT_WEB_ID = @CONTACTWEBID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACTWEBID", Trim(strGUIDFromUser.ToString())))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Session("Honorific") = dr("HONORIFIC").ToString()
            'Change Inital Character to Upper Case - DOES NOT WORK FOR JAPAN CHARACTERS
            Session("FirstName") = Left(dr("FIRST_NAME").ToString(), 1) & Mid(dr("FIRST_NAME").ToString(), 2)
            Session("LastName") = Trim(dr("LAST_NAME").ToString())
            Session("UserName") = Trim(dr("USERNAME").ToString())
            Session("Password") = Trim(dr("PASSWORD").ToString())
            Session("Title") = Trim(dr("TITLE").ToString())
            Session("Company") = Trim(dr("COMPANY").ToString())
            Session("Address") = Trim(dr("ADDRESS").ToString())
            Session("Address2") = Trim(dr("ADDRESS2").ToString())
            Session("City") = Trim(dr("CITY").ToString())
            Session("State") = Trim(dr("STATE_PROVINCE").ToString())
            Session("Zip") = Trim(dr("POSTALCODE").ToString())
            Session("Country") = Trim(dr("COUNTRY").ToString())
            Session("Phone") = Trim(dr("PHONE").ToString())
            Session("Fax") = Trim(dr("FAX").ToString())
            Session("Email") = Trim(dr("EMAIL").ToString())
            Session("URL") = Trim(dr("URL").ToString())
            Session("Applications") = Trim(dr("APPLICATION").ToString())
            Session("JobFunction") = Trim(dr("JOB_FUNCTION").ToString())
            Session("Department") = Trim(dr("DEPARTMENT").ToString())
        End If
    End Sub

    Function GetContactIDOnWebId(ByVal strWebID As String) As Long
        Dim result As Long = 0
        Dim p As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(strWebID) > 0 Then
            p.Add(New SqlParameter("@CONTACT_WEB_ID", Trim(strWebID.ToUpper())))
            result = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_getcontactidonwebid", p.ToArray())
        Else
            result = 0
        End If
        GetContactIDOnWebId = result
    End Function

    Function CheckApprovalStatus()
        '*************************************
        'Check the approval status of customer
        'KK 2/07/01
        'Modified by KK 1/22/2004 (added store procedure)
        'Arguments - none
        'Returns boolean
        '*************************************	
        'Check if session is already set for this value.
        'If Not Session("CheckApprovalStatus") Is Nothing Then
        '    Return Session("CheckApprovalStatus")
        'End If

        'Get first and second cookies from user. They should exist.
        Dim p As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim ContID As Long = 0
        Dim RealContID As Long = 0
        Dim FirstCookie As String = ""
        Dim SecondCookie As String = ""
        If Not Request.Cookies("LCDATA") Is Nothing Then
            FirstCookie = Request.Cookies("LCDATA")("GUID")
        End If
        If Not Request.Cookies("VFDATA") Is Nothing Then
            SecondCookie = Request.Cookies("VFDATA")("GUID")
        End If
        If Not FirstCookie Is Nothing And Not SecondCookie Is Nothing And
            Len(FirstCookie) > 0 And Len(SecondCookie) > 0 Then

            p.Add(New SqlParameter("@CONTACT_WEB_ID", Trim(FirstCookie.ToUpper())))
            ContID = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_getcontactidonwebid", p.ToArray())
            'Check contact_id for second cookie and compare it with this customer's Contact_id 
            ' in table CONTACT_SECOND
            p.Add(New SqlParameter("@SECOND_CONTACT_WEB_ID", Trim(SecondCookie.ToUpper())))
            RealContID = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_getcontactidonsecondwebid", p.ToArray())
            If StrComp(RealContID, ContID) = 0 Then
                Session("ResponseFlag") = True
                CheckApprovalStatus = True
            Else
                CheckApprovalStatus = False
            End If
        Else
            CheckApprovalStatus = False
        End If
        Session("CheckApprovalStatus") = CheckApprovalStatus
    End Function

    Protected Function GetCountofProdInBasket()
        Dim count As Integer = 0
        Dim pNum As Integer = 0

        Dim sqlString As String = "SELECT QTY FROM SHOPPER WHERE SESSIONID = @SESSIONID and add_to_cart='y'"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID))
        Dim shopper As List(Of Shopper) = ShopperRepository.FetchShopper(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()

        count = shopper.Count
        Dim i As Integer = 0
        If count > 0 Then
            For i = 0 To count - 1
                pNum = pNum + shopper(i).Qty
            Next
        Else
            pNum = 0
        End If

        Return pNum
    End Function

    Protected Function LoadI18N(ByVal lbl As String, ByVal localid As String) As String
        Return Functions.LoadI18N(lbl, localid)
    End Function

    'Protected Function GetTranslation(ByVal tablename As String, ByVal columnname As String, ByVal columnid As String, ByVal id As Integer, ByVal localeID As String, ByVal flgGetEnglish As Boolean) As String
    '    Return Functions.GetTranslation(tablename, columnname, columnid, id, localeID, flgGetEnglish)
    'End Function

    Function ValidateUser() As Boolean
        ' Author:           Nick Benes
        ' Date:             June 23, 1998
        ' Description:      Validate a user
        ' Returns:          TRUE or FALSE
        ' Revisions:        2
        '1-modified by Kate Kaplan 5/17/2004
        '2-modified by Shraddha Shah 5/18/2004
        ' Get the user contact id
        If Len(Session("ContactId")) > 0 Then
            If CLng(Session("ContactId")) > 0 Then
                If (ContactRepository.IsUserValidated(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId")))) Then
                    ExpireExistingCookieAndIssueNewOne(Int32.Parse(Session("ContactId")))
                    Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId")))
                    If Not (contact Is Nothing) Then
                        SetUserDataIfValid(contact.ContactId, contact.ContactWebId)
                    End If
                    ValidateUser = True
                    Exit Function
                End If
            End If
            'Else
            ' Check the cookie
            Dim GUIDFromUSER As String = ""
            If Not Request.Cookies("LCDATA") Is Nothing Then
                GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
            End If
            ' If Cookie is there
            If Len(GUIDFromUSER) > 0 Then
                Dim ContactIdTemp As String = GetContactIDOnWebId(GUIDFromUSER)
                ' If we found the id in the database
                If IsNumeric(ContactIdTemp) Then
                    If CLng(ContactIdTemp) > 0 Then
                        If (ContactRepository.IsUserValidated(ConfigurationManager.AppSettings("ConnectionString").ToString(), ContactIdTemp)) Then
                            SetUserDataIfValid(Int32.Parse(ContactIdTemp), GUIDFromUSER)
                            ValidateUser = True
                            Exit Function
                        End If
                        ValidateUser = False
                        Response.Redirect("~/Support/User/")
                        Exit Function
                    Else
                        'There is a problem:
                        'We have the GUID but somewhat we don't have the
                        ' contactID
                        ValidateUser = False
                        Response.Redirect("~/Support/User/")
                        Exit Function
                    End If
                    ' If not found
                Else
                    'There is a problem:
                    'We have the GUID but somewhat we don't have the
                    ' contactID
                    ValidateUser = False
                    Response.Redirect("~/Support/User/")
                    Exit Function
                End If
            Else
                ' If we do not have the GUID
                ' There are 2 possibilities
                ' He refused the GUID but he is with username and password
                ' or he is first time here
                ' so:
                If Session("UserFrom") = "Japan" Then
                    Response.Redirect("~/Support/User/")
                ElseIf Session("UserFrom") = "Korea" Then
                    Response.Redirect("~/Support/User/")
                Else
                    Response.Redirect("~/Support/User/")
                End If
                Exit Function
            End If
            'End If
        Else
            'There is a problem:
            'We have the GUID but somewhat we don't have the
            ' contactID
            ' Check the cookie
            Dim GUIDFromUSER As String = ""
            If Not Request.Cookies("LCDATA") Is Nothing Then
                GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
            End If
            ' If Cookie is there
            If Len(GUIDFromUSER) > 0 Then
                Dim ContactIdTemp As String = GetContactIDOnWebId(GUIDFromUSER)
                ' If we found the id in the database
                If IsNumeric(ContactIdTemp) Then
                    If CLng(ContactIdTemp) > 0 Then
                        If (ContactRepository.IsUserValidated(ConfigurationManager.AppSettings("ConnectionString").ToString(), ContactIdTemp)) Then
                            SetUserDataIfValid(Int32.Parse(ContactIdTemp), GUIDFromUSER)
                            ValidateUser = True
                            Exit Function
                        End If
                        ValidateUser = False
                        Response.Redirect("~/Support/User/")
                        Exit Function
                    Else
                        'There is a problem:
                        'We have the GUID but somewhat we don't have the
                        ' contactID
                        ValidateUser = False
                        Response.Redirect("~/Support/User/")
                        Exit Function
                    End If
                    ' If not found
                Else
                    'There is a problem:
                    'We have the GUID but somewhat we don't have the
                    ' contactID
                    ValidateUser = False
                    Response.Redirect("~/Support/User/")
                    Exit Function
                End If
            Else
                ValidateUser = False
                Response.Redirect("~/Support/User/")
                Exit Function
            End If
        End If
    End Function

    Function getProductPriceIfApproved(ByVal productId As Int32, ByVal masterProductId As Nullable(Of Int32)) As String
        Dim result As String = ""

        If CheckApprovalStatus() Then
            Dim ProductPrice As Decimal = GetProductPriceonProductID(productId, masterProductId)
            If ProductPrice > 0 Then
                result = " <font color='#007ac3'><strong>" + FormatCurrency(ProductPrice, 2) + "</strong></font> "
            Else
                result = "<font color='#007ac3'><strong>Call For Price</strong></font> "
            End If
        Else
            result = " "
        End If
        getProductPriceIfApproved = result
    End Function

    Function getProductPriceNoHTMLIfApproved(ByVal productId As Int32, ByVal masterProductId As Nullable(Of Int32)) As String
        '*************************************
        'Product Pricing
        'Arguments - prodid
        'Returns string with Price if User is approved to see prices
        ' Otherwise returns empty string
        '*************************************
        Dim result As String = ""

        If CheckApprovalStatus() Then
            Dim ProductPrice As Decimal = GetProductPriceonProductID(productId, masterProductId)
            If ProductPrice > 0 Then
                result = " " + FormatCurrency(ProductPrice, 2) + " "
            Else
                result = " (Call For Price) "
            End If
        Else
            result = " "
        End If
        getProductPriceNoHTMLIfApproved = result
    End Function

    Function ClearObjList(ByRef objList As ArrayList, ByVal del1 As String)
        Dim ddl As DropDownList
        Dim tb As TextBox
        Dim cbl As CheckBoxList
        Dim lbx As ListBox
        If Not objList Is Nothing Then
            For Each ot As OsciUC In objList

                If ot.ControlType = "d" Then
                    ddl = ot.Obj
                    ddl = CType(ddl, DropDownList)
                    For Each l As ListItem In ddl.Items
                        If l.Value = del1 Then
                            l.Selected = False
                        End If
                    Next

                End If

                If ot.ControlType = "t" Then
                    tb = ot.Obj
                    tb = CType(tb, TextBox)
                    If tb.ID = del1 Then
                        tb.Text = ""
                    End If
                End If

                If ot.ControlType = "c" Then
                    cbl = ot.Obj
                    cbl = CType(cbl, CheckBoxList)
                    For Each l As ListItem In cbl.Items
                        If l.Value = del1 Then
                            l.Selected = False
                        End If
                    Next
                End If

                If ot.ControlType = "s" Then
                    lbx = ot.Obj
                    lbx = CType(lbx, ListBox)
                    For Each l As ListItem In lbx.Items
                        If l.Value = del1 Then
                            l.Selected = False
                        End If
                    Next
                End If
            Next
        End If
    End Function

    Sub UpdateOptionsForMaster(ByVal masterProdID As String)
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If Len(masterProdID) > 0 Then
            sqlString = "Select productid FROM shopper WHERE SESSIONID = @SESSIONID and masterproductid=0 and product_series_id=0 and add_to_cart='y' and productid in (select productid from shopper WHERE SESSIONID = @SESSIONID and masterproductid=@MASTERPRODUCTID)"
            sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProdID.ToString()))
            Dim shopper As List(Of Shopper) = ShopperRepository.FetchShopper(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
            If (shopper.Count > 0) Then
                sqlString = " update shopper set qty=(qty+1) WHERE SESSIONID = @SESSIONID and masterproductid=0 and product_series_id=0 and add_to_cart='y' and productid in (select productid from shopper WHERE SESSIONID = @SESSIONID and masterproductid=@MASTERPRODUCTID)"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProdID.ToString()))
                DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())

                sqlString = " delete from shopper WHERE SESSIONID = @SESSIONID and masterproductid=@MASTERPRODUCTID and add_to_cart='y' and productid in (select productid from shopper WHERE SESSIONID = @SESSIONID and masterproductid=0 and product_series_id=0)"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProdID.ToString()))
                DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            End If

            sqlString = "UPDATE SHOPPER set MASTERPRODUCTID = 0, PRODUCT_SERIES_ID=0 WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND add_to_cart='y' AND PRODUCTID <> @PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProdID.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", masterProdID.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        End If
    End Sub

    Protected Function getDefaultMenu(ByVal captionID As String) As String
        Dim sqlString As String = String.Empty
        If IsNumeric(captionID) Then
            If InStr(SQLStringWithOutSingleQuotes(captionID), ",") Then
                captionID = Mid(SQLStringWithOutSingleQuotes(captionID), InStr(SQLStringWithOutSingleQuotes(captionID), ",") + 1)
            Else
                captionID = SQLStringWithOutSingleQuotes(captionID)
            End If
            sqlString = "SELECT MENU_ID FROM MENU_MAIN WHERE CAPTION_ID = @CAPTIONID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CAPTIONID", captionID.ToUpper()))
            Dim menuMain As List(Of MenuMain) = MenuMainRepository.FetchMenuMains(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
            If (menuMain.Count > 0) Then
                Return menuMain.FirstOrDefault().MenuId.ToString()
            End If
        End If
        Return AppConstants.PROTOCOL_ANALYZER_MENU
    End Function

    'Function setMenuSession()
    '    If Session("localeid") Is Nothing Or Len(Session("localeid")) = 0 Then
    '        Session("localeid") = 1033
    '    End If
    '    Dim sql As String = "SELECT CAPTION_ID, LOCALEID, CAPTION_NAME, URL,PROTOCOL_YN, CAPTION_TYPE " + _
    '    " FROM MENU_CAPTION where ENABLED='y' " + _
    '    " and ( expand_yn='y' or caption_type='L') " + _
    '    " and LOCALEID = " + Session("localeid").ToString() + _
    '    " order by sort_id "
    '    Dim menuStr As String = ""
    '    Dim menuURL As String = ""
    '    Dim dr As SqlDataReader = DbHelperSQL.ExecuteReader(sql)
    '    While dr.Read()

    '        Select Case Me.convertToString(dr("CAPTION_TYPE"))
    '            Case "O"
    '                AppConstants.SCOPE_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '            Case "P"
    '                AppConstants.PROTOCOL_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '            Case "A"
    '                AppConstants.APPLICATION_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '            Case "D"
    '                AppConstants.SERIAL_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '            Case "S"
    '                AppConstants.SUPPORT_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '            Case "L"
    '                AppConstants.ABOUT_LECROY_CAPTION_ID = Me.convertToString(dr("CAPTION_ID"))
    '        End Select

    '        sql = " SELECT MENU_ID, MENU_NAME, URL, DISPLAY_NEW, MENU_TYPE FROM MENU_MAIN " & _
    '            " where CAPTION_ID=" & Me.convertToString(dr("CAPTION_ID")) & _
    '            " and ENABLED='y' order by SORT_ID"
    '        Dim drr As SqlDataReader = DbHelperSQL.ExecuteReader(sql)
    '        While drr.Read()
    '            Select Case (Me.convertToString(drr("MENU_TYPE")).Trim())
    '                Case "OSM"
    '                    AppConstants.OSCILLOSCOPE_MENU = drr("MENU_ID")
    '                Case "PAM"
    '                    AppConstants.PROTOCOL_ANALYZER_MENU = drr("MENU_ID")
    '                Case "SDM"
    '                    AppConstants.SERIAL_DATA_MENU = drr("MENU_ID")
    '                Case "APM"
    '                    AppConstants.APPLICATIONS_MENU = drr("MENU_ID")
    '                Case "SUPM"
    '                    AppConstants.SUPPORT_MENU = drr("MENU_ID")
    '                Case "HOM"
    '                    AppConstants.OPTION_H_MENU = drr("MENU_ID")
    '                Case "SOM"
    '                    AppConstants.OPTION_S_MENU = drr("MENU_ID")
    '                Case "PRBM"
    '                    AppConstants.OPTION_P_MENU = drr("MENU_ID")
    '                Case "ACCM"
    '                    AppConstants.OPTION_A_MENU = drr("MENU_ID")
    '                Case "PROM"
    '                    AppConstants.PROTOCOL_OPTION_MENU = drr("MENU_ID")
    '                Case "PRSM"
    '                    AppConstants.PRESS_MENU = drr("MENU_ID")
    '                Case "NEWSM"
    '                    AppConstants.NEWSLETTER_MENU = drr("MENU_ID")
    '                Case "COMM"
    '                    AppConstants.COMPLIANCE_MENU = drr("MENU_ID")
    '                Case "TDM"
    '                    AppConstants.TRIG_DECODE_MENU = drr("MENU_ID")
    '                Case "CDM"
    '                    AppConstants.CLOCK_DATA_MENU = drr("MENU_ID")
    '                Case "JM"
    '                    AppConstants.JITTER_MENU = drr("MENU_ID")
    '                Case "BM"
    '                    AppConstants.BETA_MENU = drr("MENU_ID")
    '                Case "TLM"
    '                    AppConstants.TECH_LIB_MENU = drr("MENU_ID")
    '                Case "INSTM"
    '                    AppConstants.INSTRU_SERV_MENU = drr("MENU_ID")
    '                Case "FAQM"
    '                    AppConstants.FAQ_MENU = drr("MENU_ID")
    '                Case "SECUM"
    '                    AppConstants.OSC_SECURITY_MENU = drr("MENU_ID")
    '                Case "PRM"
    '                    AppConstants.PROD_REG_MENU = drr("MENU_ID")
    '                Case "SDM"
    '                    AppConstants.SOFTWARE_DOWNLOAD_MENU = drr("MENU_ID")
    '                Case "THM"
    '                    AppConstants.TECH_HELP_MENU = drr("MENU_ID")
    '                Case "ROHSM"
    '                    AppConstants.ROHS_MENU = drr("MENU_ID")
    '                Case "TRM"
    '                    AppConstants.TRAINING_MENU = drr("MENU_ID")
    '                Case "PROFM"
    '                    AppConstants.PROFILE_MENU = drr("MENU_ID")
    '                Case "CONTM"
    '                    AppConstants.CONTACT_US_MENU = drr("MENU_ID")
    '                Case "CARM"
    '                    AppConstants.CAREER_MENU = drr("MENU_ID")
    '                Case "INVM"
    '                    AppConstants.INVESTOR_MENU = drr("MENU_ID")
    '                Case "PRM"
    '                    AppConstants.PRESS_REL_MENU = drr("MENU_ID")
    '                Case "EVTM"
    '                    AppConstants.EVENTS_MENU = drr("MENU_ID")
    '                Case "FEAM"
    '                    AppConstants.FEATURES_MENU = drr("MENU_ID")
    '            End Select
    '        End While
    '        drr.Close()
    '        drr = Nothing
    '    End While
    '    dr.Close()
    '    dr = Nothing
    'End Function

    Sub SetSubscriptionVariables()

        Dim SubID As String = ""
        Dim CampaignID As Integer = 0
        If Not Session("CONTACTID") Is Nothing Then
            If Len(Session("CONTACTID")) = 0 Then
                Session("CONTACTID") = 0
            End If
        Else
            Session("CONTACTID") = 0
        End If
        If Not Request.QueryString("subid") Is Nothing And Len(Request.QueryString("subid")) > 0 Then
            SubID = SQLStringWithOutSingleQuotes(Request.QueryString("subid").ToString)
            Session("SubID") = SubID
        Else
            If Not Session("SubID") Is Nothing And Len(Session("SubID")) > 0 Then
                SubID = Session("SubID")
            Else
                Session("SubID") = ""
            End If
        End If
        If Not Request.QueryString("cid") Is Nothing And Len(Request.QueryString("cid")) > 0 Then
            If IsNumeric(Request.QueryString("cid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), ",") Then
                    CampaignID = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), ",") + 1)
                Else
                    CampaignID = SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString)
                End If
                Session("CampaignID") = CampaignID
            Else
                Session("CampaignID") = 0
            End If
        Else
            If Not Session("CampaignID") Is Nothing And Len(Session("CampaignID")) > 0 Then
                CampaignID = Session("CampaignID")
            Else
                Session("CampaignID") = 0
            End If
        End If
        If Len(SubID) > 0 And CampaignID > 0 Then
            If Len(Session("ContactID").ToString) > 0 Then
                Session("SubContactID") = Session("ContactID").ToString
            Else
                Session("SubContactID") = 0
            End If
            'check if Session("intProdID") exist
            Dim productId As Int32 = 0
            If Len(Session("intProdID")) > 0 And Mid(Request.ServerVariables("PATH_INFO"), 1, 8) = "/shopper" Then
                productId = CInt(Session("intProdID"))
            End If

            'Get the current page name
            If Len(Session("SubID")) > 0 And Len(Session("SubID")) <= 20 And Len(Session("CampaignID")) > 0 And Len(Session("SubContactID")) > 0 And Len(Session("CampaignID")) > 0 Then
                If Session("CampaignID") > 0 Then
                    Try
                        ' Run the procedure
                        Dim InsertSubParam As SqlParameter() = {New SqlParameter("@SessionID ", Data.SqlDbType.VarChar, 25), New SqlParameter("@DateEntered", Data.SqlDbType.DateTime),
                                                                New SqlParameter("@SubID", SqlDbType.Char, 20), New SqlParameter("@ContactID", SqlDbType.Int), New SqlParameter("@PageURL", SqlDbType.VarChar, 255),
                                                                New SqlParameter("@CampaignID", SqlDbType.Int), New SqlParameter("@ProductID", SqlDbType.Int)}
                        ' Set the values
                        InsertSubParam(0).Value = Session.SessionID.ToString        '@SessionID
                        InsertSubParam(1).Value = Now()                             '@DateEntered
                        InsertSubParam(2).Value = Session("SubID").ToString         '@SubID
                        InsertSubParam(3).Value = CLng(Session("SubContactID")) '@ContactID
                        InsertSubParam(4).Value = Request.ServerVariables("PATH_INFO").ToString '@PageURL
                        InsertSubParam(5).Value = CLng(CampaignID)    '@CampaignID
                        InsertSubParam(6).Value = CLng(productId)           '@ProductID
                        Dim rowsAffected As Int32 = 0
                        DbHelperSQL.RunProcedure(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SUBS_USERS_INS", InsertSubParam, rowsAffected)

                        Dim parameters3 As SqlParameter() = {New SqlParameter("@ContactID", Data.SqlDbType.Int), New SqlParameter("@SessionID", Data.SqlDbType.VarChar, 25)}
                        ' Set the values
                        parameters3(0).Value = CLng(Session("SubContactID"))
                        parameters3(1).Value = Session.SessionID.ToString
                        DbHelperSQL.RunProcedure(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SUBS_USERS_UPD", parameters3, rowsAffected)
                    Catch e As Exception
                        Functions.ErrorHandlerLog(True, "Error: BasePage.SetSubscriptionVariables - " + e.Message)
                    End Try

                End If
            End If
        End If

    End Sub

    Private Sub HACK_DocumentBackButtonWorkaround()
        Dim referrer As Uri = Request.UrlReferrer
        If (referrer Is Nothing) Then
            Return
        End If
        If (String.IsNullOrEmpty(referrer.ToString())) Then
            Return
        End If

        If Not (Session("IEHackPreviousPage") Is Nothing) Then
            If (String.Compare(referrer.ToString(), Session("IEHackPreviousPage").ToString(), True) <> 0) Then
                Session.Remove("IEHackPreviousPage")
                Session.Remove("IEHack")
            End If
        End If
    End Sub

    Private Function IsInternalIpAddress(ByVal remoteHost As String) As Boolean
        Dim internalIps As List(Of String) = New List(Of String)(New String() {"172.16", "172.24", "172.25", "172.28", "172.29", "172.30", "10.", "140.165"})  ' Use a hashset if this grows too big
        For Each ip As String In internalIps
            If (remoteHost.StartsWith(ip)) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Sub SetGeoIP()
        If Not (String.IsNullOrEmpty(Session("CountryCode"))) Then
            If Not (String.IsNullOrEmpty(Session("LocaleIdFromGeoIp"))) Then
                Return
            End If
        End If
        '********************************************************************************************************
        Dim serverName As String = Request.ServerVariables("SERVER_NAME")
        Dim ipAddress As String = Request.ServerVariables("REMOTE_ADDR").ToString()
        Dim localeIdFromGeoIp As String = "1033"    'Default, otherwise use value from IPStack as lookup
        Dim countryCode As String = "208"           'Default, otherwise use value from IPStack as lookup
        Dim countryISOCode As String = "US"         'Default, otherwise use value from IPStack as lookup

        If (String.Compare(serverName, "localhost", True) = 0) Then
            'Fall through
        Else
            If Not (String.IsNullOrEmpty(Request.ServerVariables("REMOTE_HOST"))) AndAlso IsInternalIpAddress(Request.ServerVariables("REMOTE_HOST")) Then
                'Fall through
            Else
                Dim url As String = String.Format("http://api.ipstack.com/{0}?access_key={1}", ipAddress, ConfigurationManager.AppSettings("IpStackApiKey").ToString())
                Try
                    Dim request As System.Net.WebRequest = System.Net.WebRequest.Create(url)
                    Using response As System.Net.WebResponse = request.GetResponse()
                        Using stream As System.IO.Stream = response.GetResponseStream()
                            Using sr As System.IO.StreamReader = New System.IO.StreamReader(stream)
                                Dim json As String = sr.ReadToEnd()
                                countryISOCode = JsonConvert.DeserializeObject(Of IpStackResponse)(json).CountryCode
                            End Using
                        End Using
                    End Using
                Catch
                End Try
            End If
        End If

        Dim sqlString As String = "SELECT * FROM [COUNTRY] WHERE [ISO_CODE] = @ISOCODE"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ISOCODE", countryISOCode))
        Dim country As Country = CountryRepository.FetchCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).FirstOrDefault()
        If Not (country Is Nothing) Then
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Value = country.DisplayCountryId.ToString()
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Expires = DateTime.Now.AddDays(365)
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").HttpOnly = True
            Session("CountryCode") = country.DisplayCountryId.ToString()
            Session("LocaleIdFromGeoIp") = country.LocaleId.ToString()
        Else
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Value = "208"
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Expires = DateTime.Now.AddDays(365)
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").HttpOnly = True
            Session("CountryCode") = "208"
            Session("LocaleIdFromGeoIp") = "1033"
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Value = ""
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").Expires = DateTime.Now.AddDays(365)
            'HttpContext.Current.Response.Cookies("LCCOUNTRY").HttpOnly = True
            'Session("CountryCode") = countryCode
            'Session("LocaleIdFromGeoIp") = localeIdFromGeoIp
        End If
    End Sub
End Class

Public Class IpStackResponse
    <JsonProperty(PropertyName:="country_code")>
    Public Property CountryCode As String
End Class