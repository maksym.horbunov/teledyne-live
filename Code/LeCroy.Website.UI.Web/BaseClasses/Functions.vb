Imports System.Data.SqlClient
Imports System.IO
Imports System.Configuration.ConfigurationManager
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class Functions

    Public Shared Function ChopDisplayContent(ByVal strValue As String, ByVal strLength As Integer) As String

        Dim retValue As String = ""

        If strValue.Length > 0 And strValue <> " " Then
            If strValue.Length > strLength Then
                retValue = ReplaceChar(strValue.Substring(0, strLength)) & "..."
            ElseIf strValue.Length > 0 Then
                retValue = strValue.Trim().ToString()
            Else
                retValue = "Title not available"
            End If
        End If
        Return retValue

    End Function

    Public Shared Function ReplaceChar(ByVal strValue As String) As String
        Dim retValue As String

        retValue = strValue.Replace("<", "&lt;")
        retValue = retValue.Replace(">", "&gt;")

        Return retValue

    End Function

    Public Shared Function ReplaceEscapeJSQuote(ByVal strValue As String) As String
        Dim retValue As String

        retValue = strValue.Replace("'", "\'")

        Return retValue

    End Function

    Public Shared Function RemoveHTML(ByVal strHTML As String) As String
        'Filter HTML
        Dim objRegExp As Regex
        Dim match As Match

        objRegExp = New Regex("<.+?>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        match = objRegExp.Match(strHTML)

        While match.Success
            strHTML = Replace(strHTML, match.Value, "")
            match = match.NextMatch
        End While

        RemoveHTML = strHTML
        objRegExp = Nothing
    End Function 'Filter HTML

    Public Shared Function stripHTML(ByVal strHTML As String) As String
        Dim rtnString As String

        Dim pattern As String = "<(p.?)[ >]"
        strHTML = System.Text.RegularExpressions.Regex.Replace(strHTML, pattern, Chr(13) & Chr(13))

        pattern = "<(br|/br)[ >]"
        strHTML = System.Text.RegularExpressions.Regex.Replace(strHTML, pattern, Chr(13))

        pattern = "<(.|\n)*?>"
        rtnString = System.Text.RegularExpressions.Regex.Replace(strHTML, pattern, "")

        Return rtnString

    End Function

    Public Shared Function DateToString(ByVal d As String, ByVal usa As Boolean, ByVal fullMonth As Boolean) As String
        '+------------------------------------------------------------------------------
        ' Function:		DateToString
        ' Contents:		Converts date value to long string (with better format than
        '				FormatDateTime())
        ' Arguments:	Date
        ' Returns:		String with format "MonthName DayOfMonth, FourDigitYear"
        '-------------------------------------------------------------------------------
        Dim strMonth As String = ""
        If fullMonth Then
            strMonth = MonthName(Convert.ToDateTime(d).Month.ToString)
        Else
            strMonth = MonthName(DatePart("m", d), True)
        End If

        If usa Then
            DateToString = strMonth.ToString & " " & Convert.ToDateTime(d).Day.ToString() & ", " & Convert.ToDateTime(d).Year.ToString()
        Else
            DateToString = Day(d) & " " & strMonth.ToString & " " & Year(d)
        End If
    End Function

    Public Shared Sub ErrorHandlerLog(ByVal IsError As Boolean, ByVal errMessage As String)
        Dim objStreamWriter As StreamWriter
        Dim sFilePath As String
        Dim strMonth As String = ""
        Dim strDay As String = ""

        Dim _sFilePath As String = AppSettings("FilePath")
        If Right(_sFilePath.ToString, 1) <> "\" Then
            _sFilePath = _sFilePath & "\"
        End If

        If Not Directory.Exists(_sFilePath & "Logs\") Then
            Directory.CreateDirectory(_sFilePath & "Logs")
        End If
        If Month(Now()).ToString.Length > 1 Then
            strMonth = Month(Now())
        Else
            strMonth = "0" & Month(Now()).ToString
        End If
        If DatePart("d", Now()).ToString.Length > 1 Then
            strDay = DatePart("d", Now())
        Else
            strDay = "0" & DatePart("d", Now()).ToString
        End If
        'Pass the file path and the file name to the StreamWriter constructor.
        sFilePath = _sFilePath & "Logs\LeCroy_Log_" & Year(Now()) & strMonth & strDay & ".log"

        objStreamWriter = New StreamWriter(sFilePath, True, Encoding.Unicode)
        'Write a line of text.
        objStreamWriter.WriteLine(Now() & " - " & errMessage)

        'Close the file.
        objStreamWriter.Close()
        'If IsError Then
        ' SendErrorAlert = True
        ' End If
    End Sub

    Private Shared Function GetReferenceSeriesId(ByVal refSeriesId As Int32, ByVal productId As Int32) As Int32
        If (refSeriesId > 0) Then
            Return refSeriesId
        End If

        Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId)
        If (product Is Nothing) Then Return 0

        Dim sqlString As String = "SELECT DISTINCT PRODUCT_SERIES.NAME, PRODUCT_SERIES.PRODUCT_SERIES_ID FROM [CONFIG] INNER JOIN [PRODUCT] ON CONFIG.OPTIONID = PRODUCT.PRODUCTID INNER JOIN [PRODUCT_SERIES_CATEGORY] ON CONFIG.PRODUCTID = PRODUCT_SERIES_CATEGORY.PRODUCT_ID INNER JOIN [PRODUCT_SERIES] ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID WHERE PRODUCT.PARTNUMBER = @PARTNUMBER AND PRODUCT_SERIES_CATEGORY.CATEGORY_ID IN (1, 2)"   ' removed PRODUCT_SERIES_CATEGORY.CATEGORY_ID, from select
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)()
        sqlParameters.Add(New SqlParameter("@PARTNUMBER", product.PartNumber))
        Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
        If (productSeries.Count <= 0) Then Return 0
        Return productSeries.FirstOrDefault().ProductSeriesId
    End Function

    Public Shared Sub ShopperSave(ByVal sessionid As String, ByVal seriesid As String, ByVal customerid As String, ByVal masterproductid As String, ByVal productid As String, ByVal refseriesid As String)
        ' Author:           Kate Kaplan
        ' Date:             December 18,2009
        ' Description:      saves temp cart
        ' Returns:          String
        ' Revisions:        2 - added refseriesid

        refseriesid = GetReferenceSeriesId(Int32.Parse(refseriesid), Int32.Parse(productid))
        Dim sqlstr As String = ""
        Dim PROPERTYGROUPID As String = ""
        Dim GROUPID As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlstr = "select GROUP_ID from PRODUCT where PRODUCTID=@PRODUCTID"
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
        GROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
        PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If String.IsNullOrEmpty(seriesid) Then
            sqlstr = "select top 1 psc.PRODUCT_SERIES_ID from PRODUCT_SERIES_CATEGORY psc inner join PRODUCT_SERIES ps on ps.PRODUCT_SERIES_ID=psc.PRODUCT_SERIES_ID " +
                    " where category_id = (SELECT  pg.CATEGORY_ID FROM  PRODUCT AS p INNER JOIN  PRODUCT_GROUP AS pg ON p.GROUP_ID = pg.GROUP_ID WHERE p.PRODUCTID = @PRODUCTID)   and ps.DISABLED='n' and psc.category_id in (2, 31) and  product_id=@PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
            seriesid = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        End If
        If String.IsNullOrEmpty(seriesid) Then seriesid = 0
        Dim countNum As Integer = 0
        sqlstr = "SELECT COUNT('1') FROM shopper WHERE masterproductid=@MASTERPRODUCTID and productid=@PRODUCTID and sessionid=@SESSIONID and REF_SERIES_ID=@REFSERIESID"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        sqlParameters.Add(New SqlParameter("@REFSERIESID", refseriesid))
        countNum = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        sqlParameters = New List(Of SqlParameter)
        If countNum = 0 Then
            If String.Compare(refseriesid, "0", True) = 0 And Not String.IsNullOrEmpty(masterproductid) Then
                refseriesid = seriesid
            End If
            sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,Date_entered,PROPERTYGROUPID,CUSTOMERID,REF_SERIES_ID) values (" +
                    "@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@DATEENTERED,@PROPERTYGROUPID,@CUSTOMERID,@REFSERIESID)"
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@QTY", "1"))
            sqlParameters.Add(New SqlParameter("@PRICE", GetProductPriceonProductID(Int32.Parse(productid), Int32.Parse(masterproductid)).ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", GROUPID))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
            sqlParameters.Add(New SqlParameter("@REFSERIESID", refseriesid))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        Else
            sqlstr = "update shopper set QTY=@QTY WHERE masterproductid=@MASTERPRODUCTID and productid=@PRODUCTID and sessionid=@SESSIONID and REF_SERIES_ID=@REFSERIESID"
            sqlParameters.Add(New SqlParameter("@QTY", (countNum + 1).ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@REFSERIESID", refseriesid))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        End If
    End Sub
    Public Shared Sub OsciSave(ByVal hc As HttpContext, ByVal sessionid As String, ByVal seriesid As String,
                                ByVal customerid As String, ByVal masterproductid As String,
                                ByVal objList As ArrayList, ByVal tbList As ArrayList)
        Dim cbl As CheckBoxList
        Dim ddl As DropDownList
        Dim rbl As RadioButtonList
        Dim tb As TextBox
        Dim lbx As ListBox
        Dim a As ArrayList
        Dim PROPERTYGROUPID As String = ""
        Dim sqlstr As String
        Dim qty As String
        qty = "1"
        a = New ArrayList()

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        'sqlstr = "delete from shopper where sessionid='" + sessionid + "' and masterproductid='" + masterproductid + "'"
        'DbHelperSQL.ExecuteSql(sqlstr)
        If Not masterproductid Is Nothing And Not "" = masterproductid Then
            sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count = 0 Then
                '*save master product
                Dim sql As String = ""
                Dim groupID As String = ""
                sql = "select * from PRODUCT where PRODUCTID=@PRODUCTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    groupID = dr("group_id").ToString()
                    PROPERTYGROUPID = dr("propertygroupid").ToString()
                End If

                sql = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
                sqlParameters.Add(New SqlParameter("@QTY", "1"))
                sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(masterproductid), Int32.Parse(masterproductid))).ToString()))
                sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            End If
        End If


        If Not objList Is Nothing Then
            Dim sqlStatementsAndParameters As List(Of Tuple(Of String, List(Of SqlParameter))) = New List(Of Tuple(Of String, List(Of SqlParameter)))   ' If lists stop keeping order of insert, then this is out the door
            For Each ou As OsciUC In objList
                Select Case ou.ControlType
                    Case "c"
                        cbl = ou.Obj
                        cbl = CType(cbl, CheckBoxList)
                        For Each l As ListItem In cbl.Items
                            If l.Selected Then
                                sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                If ds.Tables(0).Rows.Count = 0 Then
                                    sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
                                    sqlParameters = New List(Of SqlParameter)
                                    sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                    PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

                                    sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                                    sqlParameters = New List(Of SqlParameter)
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                    sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                    sqlParameters.Add(New SqlParameter("@QTY", qty))
                                    sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(l.Value), Int32.Parse(masterproductid))).ToString()))
                                    sqlParameters.Add(New SqlParameter("@GROUPID", ou.Group_ID))
                                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                    sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                                    sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                                    sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                                    sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                                End If
                            Else
                                sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                            End If
                        Next
                    Case "s"
                        lbx = ou.Obj
                        lbx = CType(lbx, ListBox)
                        For Each l As ListItem In lbx.Items
                            If l.Selected Then
                                sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                If ds.Tables(0).Rows.Count = 0 Then
                                    sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
                                    sqlParameters = New List(Of SqlParameter)
                                    sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                    PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                    sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                                    sqlParameters = New List(Of SqlParameter)
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                    sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                    sqlParameters.Add(New SqlParameter("@QTY", qty))
                                    sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(l.Value), Int32.Parse(masterproductid))).ToString()))
                                    sqlParameters.Add(New SqlParameter("@GROUPID", ou.Group_ID))
                                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                    sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                                    sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                                    sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                                    sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                                End If
                            Else
                                sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", l.Value))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                            End If
                        Next

                    Case "d"
                        ddl = ou.Obj
                        ddl = CType(ddl, DropDownList)
                        If ddl.SelectedValue <> "" Then
                            sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", ddl.SelectedValue))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                            If ds.Tables(0).Rows.Count = 0 Then
                                sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", ddl.SelectedValue))
                                PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", ddl.SelectedValue))
                                sqlParameters.Add(New SqlParameter("@QTY", qty))
                                sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(ddl.SelectedValue), Int32.Parse(masterproductid))).ToString()))
                                sqlParameters.Add(New SqlParameter("@GROUPID", ou.Group_ID))
                                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                                sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                            End If
                        Else
                            sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", ddl.SelectedValue))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                        End If
                    Case "r"
                        rbl = ou.Obj
                        rbl = CType(rbl, RadioButtonList)
                        If rbl.SelectedValue <> "" Then
                            sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", rbl.SelectedValue))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                            If ds.Tables(0).Rows.Count = 0 Then
                                sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", rbl.SelectedValue))
                                PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", rbl.SelectedValue))
                                sqlParameters.Add(New SqlParameter("@QTY", qty))
                                sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(rbl.SelectedValue), Int32.Parse(masterproductid))).ToString()))
                                sqlParameters.Add(New SqlParameter("@GROUPID", ou.Group_ID))
                                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                                sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                            End If
                        Else
                            sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", rbl.SelectedValue))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                        End If
                    Case "t"
                        tb = ou.Obj
                        tb = CType(tb, TextBox)
                        If Not tb.Text.Equals("") Then
                            sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", tb.ID))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                            If ds.Tables(0).Rows.Count = 0 Then
                                sqlstr = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", tb.ID))
                                PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                                sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, REF_SERIES_ID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@REFSERIESID)"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", tb.ID))
                                sqlParameters.Add(New SqlParameter("@QTY", qty))
                                sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(tb.ID), Int32.Parse(masterproductid))).ToString()))
                                sqlParameters.Add(New SqlParameter("@GROUPID", ou.Group_ID))
                                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                                sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerid))
                                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                                sqlParameters.Add(New SqlParameter("@REFSERIESID", seriesid))
                                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                            End If
                        Else
                            sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", tb.ID))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                            sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                        End If

                End Select
            Next
            DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)
        End If
    End Sub

    Public Shared Function GetDisabledProductSQL(ByVal localeId As String) As Tuple(Of String, SqlParameter)
        If Not (String.IsNullOrEmpty(localeId)) Then
            If (String.Compare(localeId, "1033", True) = 0) Then
                Return New Tuple(Of String, SqlParameter)("SELECT [PRODUCTID] FROM [PRODUCT] WHERE ([DISABLED]='y') OR [PRODUCTID] IN (SELECT DISTINCT [OPTIONID] FROM [CONFIG] WHERE ([STD]='y') AND ([OPTIONID] NOT IN (SELECT [OPTIONID] FROM [CONFIG] WHERE [STD]='n')))", New SqlParameter())
            Else
                Return New Tuple(Of String, SqlParameter)("SELECT [PRODUCTID] FROM [PRODUCT] WHERE ([DISABLED]='y' AND [PRODUCTID] NOT IN (SELECT [PRODUCTID] FROM [PRODUCTSENABLEDBYLOCALEID] WHERE [LOCALEID]=@LOCALEID)) OR [PRODUCTID] IN (SELECT [PRODUCT_ID] FROM [PRODUCTBYLOCALEID] WHERE [LOCALE_ID]=@LOCALEID) OR [PRODUCTID] IN (SELECT DISTINCT [OPTIONID] FROM [CONFIG] WHERE ([STD]='y') AND ([OPTIONID] NOT IN (SELECT [OPTIONID] FROM [CONFIG] WHERE [STD]='n')))", New SqlParameter("@LOCALEID", localeId))
            End If
        End If
        Return New Tuple(Of String, SqlParameter)("SELECT [PRODUCTID] FROM [PRODUCT] WHERE [DISABLED]='y' OR [PRODUCTID] IN (SELECT DISTINCT [OPTIONID] FROM [CONFIG] WHERE ([STD]='y') AND ([OPTIONID] NOT IN (SELECT [OPTIONID] FROM [CONFIG] WHERE [STD]='n')))", New SqlParameter())
    End Function

    '<Obsolete("Use Library.DAL.Common.ContactRepository.Login() from now on")>
    'Public Shared Function GetContactIDOnUserNameAndPwd(ByVal strUN As String, ByVal strPWD As String) As String
    '    Dim contactid As String
    '    contactid = ""
    '    Dim re As SqlDataReader
    '    Dim parameters As SqlParameter() = {New SqlParameter("@USERNAME", Data.SqlDbType.VarChar, 30), _
    '            New SqlParameter("@PASSWORD", Data.SqlDbType.VarChar, 30)}

    '    ' Set the values
    '    parameters(0).Value = (strUN.Replace("'", "''")).ToUpper()
    '    parameters(1).Value = (strPWD.Replace("'", "''")).ToUpper()

    '    ' Run the procedure
    '    Using db = New DbHelperSQL(ConfigurationManager.AppSettings("ConnectionString").ToString())
    '        re = DbHelperSQL.RunProcedure("sp_registration_signin", parameters)
    '    End Using
    '    re.Read()
    '    If re.HasRows Then
    '        contactid = re("CONTACT_ID").ToString()
    '    End If

    '    re.Close()
    '    re = Nothing
    '    Return contactid
    'End Function

    Public Shared Function GetContactIDOnWebId(ByVal strWebID As String) As Long
        If Len(strWebID) > 0 Then
            Dim p As List(Of SqlParameter) = New List(Of SqlParameter)
            p.Add(New SqlParameter("@CONTACT_WEB_ID", Trim(strWebID.ToUpper())))
            GetContactIDOnWebId = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_getcontactidonwebid", p.ToArray())
        Else
            GetContactIDOnWebId = 0
        End If
    End Function

    Public Shared Function LoadI18N(ByVal label As String, ByVal localeID As String) As String
        Dim lid As Integer
        lid = Integer.Parse(localeID)

        If lid = 1031 Or lid = 1040 Or lid = 1036 Then
            lid = 1033
        End If

        Dim sqlstr As String = ""
        Dim result As String = ""

        Dim parameters As SqlParameter() = {New SqlParameter("@LOCALEID", Data.SqlDbType.Int), _
                New SqlParameter("@Label", Data.SqlDbType.VarChar, 200)}

        ' Set the values
        parameters(0).Value = localeID
        parameters(1).Value = label

        ' Run the procedure
        result = DbHelperSQL.RunProcedure(Of String)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_LoadLabels", parameters)
        If (String.IsNullOrEmpty(result)) Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@LABEL", label))
            sqlstr = "Select TEXT from I18NTEXT where LOCALEID=1033 and LABEL=@LABEL"
            Dim i18 As List(Of I18NText) = I18NTextRepository.FetchI18NText(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If (i18.Count > 0) Then
                result = i18.FirstOrDefault().Text
            Else
                result = "Label" + label
            End If
        End If
        Return result
    End Function

    Public Shared Function GeneratedGUID() As String
        Dim bl As Boolean = False
        Dim strGUIDTemp As String = ""
        Dim strGIUDFinal As String = ""
        Do While Not bl
            Do Until Len(strGIUDFinal) > 14 ' number of the strlen -1
                Randomize()
                ' Get a character
                strGUIDTemp = Chr(Int(45 * Rnd()) + 47)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strGUIDTemp) < 58 Or (Asc(strGUIDTemp) > 64 And Asc(strGUIDTemp) < 91) Then
                    strGIUDFinal = strGIUDFinal & strGUIDTemp
                End If
            Loop
            If UCase(CheckWebIDNotExistance(strGIUDFinal)) = "TRUE" Then
                bl = True
            Else
                strGIUDFinal = ""
                bl = False
            End If
        Loop
        GeneratedGUID = strGIUDFinal
    End Function

    Shared Function CheckWebIDNotExistance(ByVal strWebID As String) As String
        ' Description:      Gets the ContactID based on Contact_web_Id
        ' Returns:          String "TRUE" if not found, or
        '                   String "FALSE" if found

        Dim sql As String = "SELECT CONTACT_ID FROM CONTACT WHERE UPPER(CONTACT_WEB_ID) = @CONTACTWEBID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACTWEBID", Trim(UCase(strWebID))))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            CheckWebIDNotExistance = "FALSE"
        Else
            CheckWebIDNotExistance = "TRUE"
        End If
    End Function

    Shared Function GetLongDescription(ByVal tablename As String, ByVal columnname As String, ByVal idintable As Integer) As String
        Dim strSQL As String = ""
        Dim TableID As String = ""
        Dim ColumnID As String = ""
        Dim LongDescription As String = ""
        If Len(tablename) > 0 And Len(columnname) > 0 And Len(idintable) > 0 Then
            'select tableid from table "TABLE_ID" (ecatalog)
            strSQL = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLENAME", tablename))
            Dim tableIds As List(Of TableId) = TableIdRepository.FetchTableIds(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (tableIds.Count > 0) Then TableID = tableIds.FirstOrDefault().Table_Id

            strSQL = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", columnname))
            Dim columnIds As List(Of ColumnId) = ColumnIdRepository.FetchColumnIds(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (columnIds.Count > 0) Then ColumnID = columnIds.FirstOrDefault().Column_Id

            strSQL = "Select TEXT from LONGDESCR where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@IDINTABLE Order by SEQ_ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
            sqlParameters.Add(New SqlParameter("@COLUMNID", ColumnID))
            sqlParameters.Add(New SqlParameter("@IDINTABLE", idintable.ToString()))
            Dim longDescrs As List(Of LongDescr) = LongDescrRepository.FetchLongDescr(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            For Each longDesc In longDescrs
                LongDescription = LongDescription & longDesc.Text
            Next
        End If
        Return LongDescription
    End Function

    Shared Function CheckDocExists(ByVal documentid As Integer) As Boolean
        CheckDocExists = False
        Dim strSQL As String
        If Len(documentid) > 0 Then
            strSQL = " Select *  FROM DOCUMENT  WHERE DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid.ToString()))
            CheckDocExists = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count > 0
        End If
    End Function

    Shared Function GetDOwnloadFilePath(ByVal documentid As Integer) As String
        Dim strSQL As String
        GetDOwnloadFilePath = ""
        If Len(documentid) > 0 Then
            strSQL = " Select *  FROM DOCUMENT  WHERE FILE_FULL_PATH is not null and DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid.ToString()))
            Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (documents.Count > 0) Then GetDOwnloadFilePath = documents.FirstOrDefault().FileFullPath.ToLower()
        End If
    End Function

    Shared Function getdownloaddescr(ByVal documentid As Integer) As String
        getdownloaddescr = ""
        If Len(documentid) > 0 Then
            Dim strSQL As String = " Select *  FROM DOCUMENT WHERE DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid.ToString()))
            Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (documents.Count > 0) Then getdownloaddescr = Replace(Replace(documents.FirstOrDefault().Title, "<i>", ""), "</i>", "") & " - Version " & documents.FirstOrDefault().Version & " - " & Mid(GetDOwnloadFilePath(documentid), InStrRev(GetDOwnloadFilePath(documentid), "/") + 1)
        End If
    End Function

    Shared Function InsertDownloadByProductRequest(ByVal lgContactId As Integer, ByVal lgRequestTypeId As Integer, ByVal lgObjectId As Integer, ByVal lgModelSeries As String, ByVal lgProductID As Integer, ByVal strSerialNum As String, ByVal strScopeID As String, ByVal strRemarks As String, ByVal strApplication As String, ByVal lgPageID As String, ByVal PresentID As String, ByVal DatePurchased As String, ByVal CampaignID As String) As Integer
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACT_ID", lgContactId.ToString()))
        sqlParameters.Add(New SqlParameter("@REQUEST_TYPE_ID", lgRequestTypeId.ToString()))
        sqlParameters.Add(New SqlParameter("@OBJECT_ID", lgObjectId.ToString()))
        If Not lgModelSeries Is Nothing And Len(lgModelSeries) > 0 Then
            sqlParameters.Add(New SqlParameter("@MODEL_SERIES", lgModelSeries.ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@MODEL_SERIES", "0"))
        End If
        sqlParameters.Add(New SqlParameter("@PRODUCT_ID", lgProductID.ToString()))
        sqlParameters.Add(New SqlParameter("@SERIAL_NUM", strSerialNum))
        sqlParameters.Add(New SqlParameter("@SCOPEID", strScopeID))
        sqlParameters.Add(New SqlParameter("@REMARKS", strRemarks))
        sqlParameters.Add(New SqlParameter("@APPLICATION", strApplication))
        sqlParameters.Add(New SqlParameter("@PAGE_ID", "0"))
        If Not CampaignID Is Nothing And Len(CampaignID) > 0 Then
            sqlParameters.Add(New SqlParameter("@CAMPAIGN_ID", CampaignID.ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@CAMPAIGN_ID", "0"))
        End If
        If Not PresentID Is Nothing And Len(PresentID) > 0 Then
            sqlParameters.Add(New SqlParameter("@PRESENT_ID", PresentID.ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@PRESENT_ID", "0"))
        End If
        If Not DatePurchased Is Nothing And Len(DatePurchased) > 0 Then
            sqlParameters.Add(New SqlParameter("@DATE_PURCH", CDate(DatePurchased).ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@DATE_PURCH", DBNull.Value.ToString()))
        End If
        Dim paramOut As SqlParameter = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
        paramOut.Direction = ParameterDirection.Output
        sqlParameters.Add(paramOut)
        InsertDownloadByProductRequest = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "ZP_InsertDownloadByProductRequest", sqlParameters.ToArray())
    End Function

    Shared Function InsertSWDownloadRequest(ByVal lgContactId As Integer, ByVal lgRequestTypeId As Integer, ByVal lgObjectId As Integer, ByVal strSerialNum As String, ByVal strScopeID As String, ByVal strRemarks As String, ByVal strApplication As String, ByVal lgPageID As String, ByVal PresentID As String, ByVal DatePurchased As String, ByVal CampaignID As String) As Integer
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACT_ID", lgContactId.ToString()))
        sqlParameters.Add(New SqlParameter("@REQUEST_TYPE_ID", lgRequestTypeId.ToString()))
        sqlParameters.Add(New SqlParameter("@OBJECT_ID", lgObjectId.ToString()))
        sqlParameters.Add(New SqlParameter("@SERIAL_NUM", strSerialNum))
        sqlParameters.Add(New SqlParameter("@SCOPEID", strScopeID))
        sqlParameters.Add(New SqlParameter("@REMARKS", strRemarks))
        sqlParameters.Add(New SqlParameter("@APPLICATION", strApplication))
        sqlParameters.Add(New SqlParameter("@PAGE_ID", "0"))
        If Not CampaignID Is Nothing And Len(CampaignID) > 0 Then
            sqlParameters.Add(New SqlParameter("@CAMPAIGN_ID", CampaignID.ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@CAMPAIGN_ID", "0"))
        End If
        If Not PresentID Is Nothing And Len(PresentID) > 0 Then
            sqlParameters.Add(New SqlParameter("@PRESENT_ID", PresentID.ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@PRESENT_ID", "0"))
        End If
        If Not DatePurchased Is Nothing And Len(DatePurchased) > 0 Then
            sqlParameters.Add(New SqlParameter("@DATE_PURCH", CDate(DatePurchased).ToString()))
        Else
            sqlParameters.Add(New SqlParameter("@DATE_PURCH", DBNull.Value))
        End If
        Dim paramOut As SqlParameter = New SqlParameter("@REQUEST_ID", SqlDbType.Int)
        paramOut.Direction = ParameterDirection.Output
        sqlParameters.Add(paramOut)
        InsertSWDownloadRequest = DbHelperSQL.RunProcedure(Of Int32)(ConfigurationManager.AppSettings("ConnectionString").ToString(), "ZP_INSERT_SWDOWNLOAD_REQUEST", sqlParameters.ToArray())
    End Function

    Shared Function checkSWisnew(ByVal DOWN_CAT_ID As Integer, ByVal localeID As Integer) As String
        Dim strSQL As String = ""
        checkSWisnew = ""
        'Select max Date_Entered from DOWNLOAD table
        'and check if this date is less than 60 days from now
        'if so show the word "NEW"
        If Len(DOWN_CAT_ID) > 0 Then

            strSQL = "Select max(DATE_ENTERED) as DATE_ENTERED from DOWNLOAD where DOWN_CAT_ID=@DOWNCATID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOWNCATID", DOWN_CAT_ID.ToString()))
            'Dim downloads As List(Of Download) = DownloadRep
            'response.Write strSQL
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                Dim temp As String = ds.Tables(0).Rows(0)("DATE_ENTERED")
                If DateDiff("d", Date.Parse(temp), Date.Now()) <= 60 Then
                    checkSWisnew = "<font color=""red"">" & LoadI18N("SLBCA0112", localeID) & "&nbsp;</font>"
                End If
            End If
        End If
    End Function

    Shared Function hasOptions(ByVal productID As String, ByVal catID As String, ByVal localeID As String) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             Aug 11,2009
        ' Description:      checkes if scope has options from categories - SW, Probes and e.g
        ' Returns:          Boolean
        ' Revisions:        1
        ' Revisions:        2 - 02/24/2010 - Added HW 
        Dim strSQL As String = ""
        hasOptions = False
        If Len(productID) > 0 And Len(catID) > 0 Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            Dim disabledProductContent As Tuple(Of String, SqlParameter) = GetDisabledProductSQL(localeID)
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)

            If catID = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                strSQL = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
                         " FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID " +
                         " INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) AND b.category_id = @CATEGORYID", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_HARDWARE_OPTIONS))
            ElseIf catID = AppConstants.GROUP_ID_WARRANTY Then
                strSQL = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
                         " FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID " +
                         " INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) AND b.category_id = @CATEGORYID", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.GROUP_ID_WARRANTY))
            ElseIf catID = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                strSQL = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
                         " FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID " +
                         " INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) and b.CATEGORY_ID in (@CATEGORYID1,@CATEGORYID2)", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_SOFTWARE_OPTIONS))
                sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_MIXEDSIGNAL))
            ElseIf catID = AppConstants.CAT_ID_ACCESSORIES Then
                strSQL = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
                         " FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID " +
                         " INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) AND b.GROUP_ID <> @CATEGORYID1 and b.CATEGORY_ID=@CATEGORYID2", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.GROUP_ID_WARRANTY))
                sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_ACCESSORIES))
            Else
                strSQL = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
                         " FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID " +
                         " INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) and b.CATEGORY_ID=@CATEGORYID", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", catID))
            End If
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productGroups.Count > 0) Then hasOptions = True
        End If
    End Function

    Shared Sub showCategory(ByRef ddl As DropDownList, ByVal compID As String, ByVal localeID As String)
        Dim sqlstr As String = ""
        Dim li As ListItem
        ddl.Items.Clear()
        li = New ListItem("", "0")
        ddl.Items.Add(li)
        'If compID = "1" Then
        '    sqlstr = "select subcat_id as category_id, subcat_name as category_name from SUBCATEGORY where subcat_id not in (4,5,8,18)  and category_id=19 order by SORT_ID "
        '    re = DbHelperSQL.ExecuteReader(sqlstr)
        '    While re.Read()
        '        li = New ListItem(re("category_name").ToString(), re("category_id").ToString())
        '        ddl.Items.Add(li)
        '    End While
        '    re.Close()
        'Else
        '    'If localeID <> "1041" Then
        '    'sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (15,16,9,19,20,21,27,28,29) order by SORT_ID"
        '    'Else
        '    sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (15,16,9,19,20,21,27,28,29) order by SORT_ID"
        '    'End If
        '    re = DbHelperSQL.ExecuteReader(sqlstr)
        '    While re.Read()
        '        li = New ListItem(re("category_name").ToString(), re("category_id").ToString())
        '        ddl.Items.Add(li)
        '    End While
        '    re.Close()
        'End If
        If (String.Compare(compID, "1", True) = 0) Then ' Scopes
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (2,9,15,16,19,20,21,24,27,28,29,30,31,32) order by SORT_ID"
        ElseIf (String.Compare(compID, "2", True) = 0) Then ' Analyzers
            sqlstr = "select subcat_id as category_id, subcat_name as category_name from SUBCATEGORY where subcat_id not in (4,5,8,18)  and category_id=19 order by SORT_ID "
        ElseIf (String.Compare(compID, "3", True) = 0) Then ' PERT
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (1,2,3,9,10,11,12,15,16,18,19,20,21,23,25,26,27,28,29,30,31,32) order by SORT_ID"
        ElseIf (String.Compare(compID, "4", True) = 0) Then ' Waveform
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (1,3,9,10,11,12,15,16,18,19,20,21,23,24,25,26,27,28,29,30,31,32) order by SORT_ID"
        ElseIf (String.Compare(compID, "5", True) = 0) Then ' Logic
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (1,2,3,9,10,11,12,15,16,18,19,20,21,23,24,25,26,27,28,29,30,32) order by SORT_ID"
        ElseIf (String.Compare(compID, "6", True) = 0) Then ' SPARQ
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (1,2,3,9,10,11,12,15,16,18,19,20,21,23,24,25,26,27,28,29,31,32) order by SORT_ID"
        ElseIf (String.Compare(compID, "7", True) = 0) Then ' SIStudio
            sqlstr = "select  category_id , category_name  from CATEGORY where category_id not in (1,2,3,9,10,11,12,15,16,18,19,20,21,23,24,25,26,27,28,29,30,31) order by SORT_ID"
        End If
        Dim categories As List(Of Category) = CategoryRepository.FetchCategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        For Each category In categories
            li = New ListItem(category.CategoryName, category.CategoryId.ToString())
            ddl.Items.Add(li)
        Next
    End Sub

    Shared Sub showProductModel(ByRef ddl As DropDownList, ByVal categoryID As String, ByVal compID As String, ByVal localeID As String)
        Dim sqlstr As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)()
        Dim li As ListItem
        ddl.Items.Clear()
        li = New ListItem("", "0")
        ddl.Items.Add(li)
        If categoryID <> "" Then
            If compID.Equals("2") Then
                sqlstr = " SELECT PRODUCT_SERIES_ID, NAME FROM PRODUCT_SERIES WHERE(PRODUCT_SERIES_ID IN (SELECT PRODUCT_SERIES_ID " + _
                        " FROM PRODUCT_SERIES_CATEGORY WHERE (CATEGORY_ID = 19))) AND SUBCAT_ID =@SUBCATEGORYID ORDER by NAME"
                sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", categoryID.ToString()))
                Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                For Each ps In productSeries
                    li = New ListItem(ps.Name, ps.ProductSeriesId.ToString())
                    ddl.Items.Add(li)
                Next
            Else
                If categoryID.Equals("1") Then
                    If localeID <> "1041" Then
                        sqlstr = " SELECT PRODUCT_SERIES_ID, NAME FROM PRODUCT_SERIES WHERE(PRODUCT_SERIES_ID IN (SELECT PRODUCT_SERIES_ID " +
                                " FROM PRODUCT_SERIES_CATEGORY WHERE (CATEGORY_ID = 1))) AND PRODUCT_SERIES_ID <> 7 AND (disabled='n' or PRODUCT_SERIES_ID =15) ORDER by NAME"
                    Else
                        sqlstr = " SELECT PRODUCT_SERIES_ID, NAME FROM PRODUCT_SERIES WHERE(PRODUCT_SERIES_ID IN (SELECT PRODUCT_SERIES_ID " +
                                " FROM PRODUCT_SERIES_CATEGORY WHERE (CATEGORY_ID = 1))) AND PRODUCT_SERIES_ID NOT IN (6,7) AND disabled='n' ORDER by NAME"
                    End If
                    Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    For Each ps In productSeries
                        li = New ListItem(ps.Name, ps.ProductSeriesId.ToString())
                        ddl.Items.Add(li)
                    Next
                Else
                    sqlstr = "SELECT GROUP_ID, GROUP_NAME FROM PRODUCT_GROUP  WHERE CATEGORY_ID = @CATEGORYID ORDER BY GROUP_NAME"
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryID.ToString()))
                    Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    For Each pg In productGroups
                        li = New ListItem(pg.GroupName, pg.GroupId.ToString())
                        ddl.Items.Add(li)
                    Next
                End If

            End If
        Else
            sqlstr = "SELECT GROUP_ID, GROUP_NAME FROM PRODUCT_GROUP  WHERE CATEGORY_ID = @CATEGORYID ORDER BY GROUP_NAME"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryID.ToString()))
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            For Each pg In productGroups
                li = New ListItem(pg.GroupName, pg.GroupId.ToString())
                ddl.Items.Add(li)
            Next
        End If
    End Sub

    Shared Function Techhelp_countProducts(ByRef hc As HttpContext, ByVal categoryID As String, ByVal PropertygroupID As String, ByVal disabled As String, ByVal localeid As String) As String
        Dim result As String = ""
        Dim sqlstr As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)()
        If PropertygroupID.Equals("") Then
            Return "0"
        End If

        If (String.IsNullOrEmpty(localeid)) Then
            localeid = "1033"
        Else
            Dim testVal As Int32 = 1033
            If Not (Int32.TryParse(localeid, testVal)) Then localeid = "1033"
        End If

        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(localeid)
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)

        If disabled.Equals("y") Then
            If categoryID.Equals("1") Then
                sqlstr = String.Format("Select PRODUCT.productid from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'y' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=1 and  PRODUCT.productid  in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            ElseIf categoryID.Equals("19") Then
                sqlstr = String.Format("Select PRODUCT.productid from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'y' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=19 and  PRODUCT.productid in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            Else
                sqlstr = String.Format("Select productid from PRODUCT where  GROUP_ID=@PROPERTYGROUPID and product.eol_yn = 'y' and productid  in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            End If
        Else
            If categoryID.Equals("1") Then
                sqlstr = String.Format("Select PRODUCT.productid from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'n' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=1 and  PRODUCT.productid not in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            ElseIf categoryID.Equals("19") Then
                sqlstr = String.Format("Select PRODUCT.productid from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'n' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=19 and  PRODUCT.productid not in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            Else
                sqlstr = String.Format("Select productid from PRODUCT where  GROUP_ID=@PROPERTYGROUPID and product.eol_yn = 'n' and productid not in ({0})", disabledProductContent.Item1)
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()))
            End If
        End If
        result = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray()).Count()
        Techhelp_countProducts = result
    End Function

    'Shared Function Techhelp_getPropertyIdonCategoryId(ByVal categoryID As Integer) As String
    '    Dim result As String = ""
    '    If categoryID > 0 Then
    '        Dim strSQL As String = "select propertygroupid  as propid from propertygroup where category_id = " & categoryID
    '        Dim re As SqlDataReader = DbHelperSQL.ExecuteReader(strSQL)
    '        If re.HasRows() Then
    '            re.Read()
    '            result = re("propid").ToString()
    '        End If
    '        re.Close()
    '    End If
    '    Techhelp_getPropertyIdonCategoryId = result
    'End Function

    Shared Function GetGroupIDonCATEGORYID(ByVal categoryID As String) As String
        Dim result As String = ""
        If Len(categoryID) > 0 Then
            If IsNumeric(categoryID) Then
                Dim strSQL As String = "SELECT GROUP_ID, GROUP_NAME FROM PRODUCT_GROUP  WHERE CATEGORY_ID = @CATEGORYID"
                Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@CATEGORYID", categoryID.ToString())}).ToArray())
                If (productGroups.Count > 0) Then result = productGroups.FirstOrDefault().GroupId.ToString()
            End If
        End If
        GetGroupIDonCATEGORYID = result
    End Function

    Shared Function GetDiscontinuedProductsbyLocaleID(ByVal localeid As String) As String
        Dim strSQL As String = ""
        Dim strDisabledProductList As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(localeid) > 0 Then
            strSQL = "Select PRODUCTID from PRODUCT where  (DISABLED='y' and productid not in (select PRODUCTID FROM PRODUCTSENABLEDBYLOCALEID where localeid=@LOCALEID)  and productid not in (select PRODUCT_ID FROM PRODUCTBYLOCALEID where locale_id=@LOCALEID) ) or ( disabled='n' and productid in (select PRODUCT_ID FROM PRODUCTBYLOCALEID where locale_id=@LOCALEID) and productid not in (select PRODUCTID FROM PRODUCTSENABLEDBYLOCALEID where localeid=@LOCALEID)  )" 'or productid not in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
            sqlParameters.Add(New SqlParameter("@LOCALEID", localeid))
        Else
            strSQL = "Select PRODUCTID from PRODUCT where  DISABLED='y' or productid not in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
        End If
        'Response.Write strSQL
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        For Each product In products
            If Len(strDisabledProductList) = 0 Then
                strDisabledProductList = product.ProductId.ToString()
            Else
                strDisabledProductList = strDisabledProductList & "," & product.ProductId.ToString()
            End If
        Next

        GetDiscontinuedProductsbyLocaleID = strDisabledProductList
    End Function

    Shared Function GetPSGCategoryNameonID(ByVal catid As Integer) As String
        Dim result As String = ""
        If catid > 0 Then
            Dim strSQL As String = "select subcat_name from SUBCATEGORY where subcat_id=@SUBCATEGORYID"
            Dim subcategories As List(Of Subcategory) = SubcategoryRepository.FetchSubcategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@SUBCATEGORYID", catid.ToString())}).ToArray())
            If (subcategories.Count > 0) Then result = subcategories.FirstOrDefault().SubCatName
        End If
        GetPSGCategoryNameonID = result
    End Function

    Shared Function GetCategoryNameonID(ByVal catid As Integer) As String
        Dim result As String = ""
        If catid > 0 Then
            Dim strSQL As String = "SELECT CATEGORY_NAME FROM CATEGORY WHERE CATEGORY_ID = @CATEGORYID"
            Dim categories As List(Of Category) = CategoryRepository.FetchCategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@CATEGORYID", catid.ToString())}).ToArray())
            If (categories.Count > 0) Then result = categories.FirstOrDefault().CategoryName
        End If
        GetCategoryNameonID = result
    End Function

    Shared Function Techhelp_getPropertygroupname(ByVal categoryID As Integer, ByVal propgroupid As Integer) As String
        Dim retVal As String = String.Empty
        Dim strsql As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)()
        If Len(categoryID) > 0 And Len(propgroupid) > 0 Then
            If CInt(categoryID) = 1 Or CInt(categoryID) = 19 Then
                strsql = " SELECT name FROM  PRODUCT_SERIES where PRODUCT_SERIES_ID = @PRODUCTSERIESID"
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", propgroupid.ToString()))
                Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strsql, sqlParameters.ToArray())
                If (productSeries.Count > 0) Then retVal = productSeries.FirstOrDefault().Name
            Else
                'PSG products
                strsql = "select  GROUP_NAME  from PRODUCT_GROUP where GROUP_ID=@GROUPID"
                sqlParameters.Add(New SqlParameter("@GROUPID", propgroupid.ToString()))
                Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strsql, sqlParameters.ToArray())
                If (productGroups.Count > 0) Then retVal = productGroups.FirstOrDefault().GroupName
            End If
        End If 'len(catid) > 0 
        Return retVal
    End Function

    Shared Function GetProductNameonID(ByVal prodid As Integer) As String
        Dim result As String = ""
        If prodid > 0 Then
            Dim strSQL As String = "SELECT partnumber FROM  product WHERE productID = @PRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", prodid.ToString()))
            Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (products.Count > 0) Then result = products.FirstOrDefault().PartNumber
        End If
        Return result
    End Function

    Shared Function InsertRequest(ByVal lgContactId As Long, ByVal lgRequestTypeId As Integer, ByVal lgObjectId As Integer, ByVal strRemarks As String, ByVal strApplication As String, ByVal lgPageID As Integer, ByVal CampaignID As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             Feb 26,2009
        ' Description:      Insert a new request
        ' Returns:          The Request_Id just inserted
        ' Revisions:        1
        If lgContactId > 0 And lgRequestTypeId > 0 And lgObjectId > 0 Then
            Dim strSQL As String = ""
            Dim strDate As String = Now()
            strSQL = " INSERT INTO REQUEST (CONTACT_ID, REQUEST_TYPE_ID, OBJECT_ID, REMARKS, ENTERED_BY, DATE_ENTERED, DATE_RESPONDED, RESPONDED_BY,PAGEID,CAMPAIGN_ID) VALUES(@CONTACTID,@REQUESTTYPEID,@OBJECTID,@REMARKS,@ENTEREDBY,@DATEENTERED,@DATERESPONDED,@RESPONDEDBY,@PAGEID,@CAMPAIGNID)"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@CONTACTID", lgContactId.ToString()),
                                                                                    New SqlParameter("@REQUESTTYPEID", lgRequestTypeId.ToString()),
                                                                                    New SqlParameter("@OBJECTID", lgObjectId.ToString()),
                                                                                    New SqlParameter("@ENTEREDBY", "webuser"),
                                                                                    New SqlParameter("@DATEENTERED", strDate.ToString()),
                                                                                    New SqlParameter("@DATERESPONDED", strDate.ToString()),
                                                                                    New SqlParameter("@RESPONDEDBY", "webuser"),
                                                                                    New SqlParameter("@PAGEID", lgPageID.ToString()),
                                                                                    New SqlParameter("@CAMPAIGNID", CampaignID.ToString())})
            Dim sqlParameter As SqlParameter = New SqlParameter("@REMARKS", SqlDbType.NVarChar)
            sqlParameter.Value = strRemarks.ToString()
            sqlParameters.Add(sqlParameter)
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            strSQL = "SELECT REQUEST_ID from REQUEST where CONTACT_ID=@CONTACTID and OBJECT_ID=@OBJECTID and DATE_ENTERED=@DATEENTERED and REQUEST_TYPE_ID=@REQUESTTYPEID"
            sqlParameters = New List(Of SqlParameter)({New SqlParameter("@CONTACTID", lgContactId.ToString()), New SqlParameter("@OBJECTID", lgObjectId.ToString()), New SqlParameter("@DATEENTERED", strDate.ToString()), New SqlParameter("@REQUESTTYPEID", lgRequestTypeId.ToString())})
            Dim requests As List(Of Request) = RequestRepository.FetchRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (requests.Count > 0) Then
                InsertRequest = requests.FirstOrDefault().RequestId.ToString()
            Else
                InsertRequest = ""
            End If
        Else
            InsertRequest = ""
        End If
    End Function

    Shared Function LeftMenu(ByVal CAPTION_ID As String, ByVal rootdir As String, ByVal pn As Panel, ByVal menu_id As String) As String

        Dim sqlstr As String = ""
        Dim sortID As String = ""
        Dim result As String = ""
        Dim menuURL As String = ""
        Dim chrv As String = Chr(13) & Chr(10)
        Dim se As StringWriter
        Dim hw As Html32TextWriter
        Dim len As Integer
        Dim numCaptionID As String = ""
        se = New StringWriter()
        hw = New Html32TextWriter(se)

        If IsNumeric(CAPTION_ID) Then
            If InStr(Replace(CAPTION_ID.ToString, "'", ""), ",") Then
                numCaptionID = Mid(Replace(CAPTION_ID.ToString, "'", ""), InStr(Replace(CAPTION_ID.ToString, "'", ""), ",") + 1)
            Else
                numCaptionID = Replace(CAPTION_ID.ToString, "'", "")
            End If


            sqlstr = "select * from MENU_MAIN where PROTOCOL_STANDARD_ID is NULL and ENABLED='y' and CAPTION_ID=@CAPTIONID order by sort_id "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CAPTIONID", numCaptionID))
            Dim menuMains As List(Of MenuMain) = MenuMainRepository.FetchMenuMains(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            For Each mm In menuMains
                menuURL = mm.Url
                If Not menuURL.Contains("http") Then
                    If menuURL.Contains("?") Then
                        menuURL += "&capid=" + CAPTION_ID + "&mid=" + mm.MenuId.ToString()
                    Else
                        'menuURL += "?capid=" + CAPTION_ID + "&mid=" + mm.MenuId.ToString()
                    End If
                    menuURL = rootdir + menuURL
                End If

                If mm.MenuId.ToString() = menu_id Then
                    result += chrv & "<li class='current'><a href='" & menuURL & "'>" & Trim(mm.MenuName.ToString()) & "</a>"
                    pn.RenderControl(hw)
                    len = se.ToString().Length
                    If (len > 0) Then
                        result += se.ToString()
                    End If
                    result += "</li>"
                Else
                    result += chrv & "<li><a href='" & menuURL & "'>" & Trim(mm.MenuName.ToString()) & "</a></li>"
                End If
            Next
        End If
        Return result
    End Function

    Shared Function LeftSubMenu(ByVal CAPTION_ID As String, ByVal rootdir As String, ByVal pn As Panel, ByVal menu_id As String, ByVal sub_menu_id As String) As String

        Dim sqlstr As String = ""
        Dim sortID As String = ""
        Dim result As String = ""
        Dim menuURL As String = ""
        Dim chrv As String = Chr(13) & Chr(10)
        Dim se As StringWriter
        Dim hw As Html32TextWriter
        Dim len As Integer
        se = New StringWriter()
        hw = New Html32TextWriter(se)

        sqlstr = "select * from MENU_SUBMENU1 where MENU_ID=@MENUID and enabled='y'  order by sort_id "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@MENUID", menu_id))
        Dim menuSubMenu As List(Of MenuSubMenu1) = MenuSubMenu1Repository.FetchMenuSubMenus(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        result += "<ul>"
        For Each msm In menuSubMenu
            menuURL = msm.Url
            If menuURL.Contains("?") Then
                menuURL += "&capid=" + CAPTION_ID +
                "&mid=" + msm.MenuId.ToString() +
                "&smid=" + msm.SubMenuId.ToString()
            Else
                menuURL += "?capid=" + CAPTION_ID +
                "&mid=" + msm.MenuId.ToString() +
                "&smid=" + msm.SubMenuId.ToString()
            End If
            If sub_menu_id = msm.SubMenuId.ToString() Then
                result += "<li class='current'><a href='" & rootdir & menuURL & "'>" & Trim(msm.SubMenuName) & "</a>"
                pn.RenderControl(hw)
                len = se.ToString().Length
                If (len > 0) Then
                    result += se.ToString()
                End If
                result += "</li>"
            Else
                result += "<li><a href='" & rootdir & menuURL & "'>" & Trim(msm.SubMenuName) & "</a></li>"
            End If
        Next
        result += "</ul>"
        Return result
    End Function

    Shared Function TopMenu(ByVal CAPTION_ID As String, ByVal rootdir As String) As String

        Dim sqlstr As String = ""
        Dim sortID As String = ""
        Dim menuURL As String = ""
        Dim chrv As String = Chr(13) & Chr(10)
        Dim se As StringWriter
        Dim hw As Html32TextWriter
        se = New StringWriter()
        hw = New Html32TextWriter(se)

        Dim result As String = "<ul>"
        sqlstr = "select * from MENU_MAIN where PROTOCOL_STANDARD_ID is NULL and CAPTION_ID=@CAPTIONID and enabled='y' order by sort_id "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CAPTIONID", CAPTION_ID))
        Dim menuMains As List(Of MenuMain) = MenuMainRepository.FetchMenuMains(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        For Each mm In menuMains
            menuURL = mm.Url
            If menuURL.Contains("?") Then
                menuURL += "&capid=" + CAPTION_ID + "&mid=" + mm.MenuId.ToString()
            Else
                menuURL += "?capid=" + CAPTION_ID + "&mid=" + mm.MenuId.ToString()
            End If

            result += "<li><a href='" & rootdir & menuURL & "'>" & mm.MenuName & "</a></li>"
        Next
        result += "</ul>"
        Return result
    End Function

    Public Shared Sub CreateMetaTags(ByRef h As HtmlHead, ByVal id As String, ByVal OVERVIEW_TYPE_ID As String)
        Dim hm As New HtmlMeta()
        Dim sqlstr As String = ""
        If InStr(id, ",") Then
            id = Mid(id, InStr(id, ",") + 1)
        Else
            id = id
        End If
        sqlstr = "SELECT * FROM CMS_OVERVIEW where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", id))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", OVERVIEW_TYPE_ID))
        Dim cmsOverviews As List(Of CmsOverview) = CmsOverviewRepository.FetchCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If (cmsOverviews.Count > 0) Then
            If cmsOverviews.FirstOrDefault().PageTitle <> "" Then
                hm = New HtmlMeta()
                hm.Name = "Title"
                hm.Content = cmsOverviews.FirstOrDefault().PageTitle
                h.Controls.Add(hm)
            End If
            If cmsOverviews.FirstOrDefault().PageDescr <> "" Then
                hm = New HtmlMeta()
                hm.Name = "Description"
                hm.Content = cmsOverviews.FirstOrDefault().PageDescr
                h.Controls.Add(hm)
            End If
            If cmsOverviews.FirstOrDefault().Keywords <> "" Then
                hm = New HtmlMeta()
                hm.Name = "keywords"
                hm.Content = cmsOverviews.FirstOrDefault().Keywords
                h.Controls.Add(hm)
            End If
        End If
    End Sub

    Public Shared Function checkdemoselected(ByVal demoid As String, ByRef demo() As String) As Boolean
        'check if array Session("demo") exists
        Dim i As Integer
        If UBound(demo) > 0 Then
            For i = 0 To (UBound(demo) - 1)

                If CInt(demoid) = CInt(demo(i)) Then
                    checkdemoselected = True
                    Exit Function
                Else
                    checkdemoselected = False
                End If
            Next
        Else
            checkdemoselected = False
        End If
    End Function
    Public Shared Function checkdemoselected(ByVal demoid As String, ByRef hc As HttpContext) As Boolean
        'check if array Session("demo") exists

        Dim demo As String
        Dim i As Integer
        demo = hc.Session("demo").ToString()

        If UBound(demo.Split(",")) > 0 Then
            For i = 0 To (UBound(demo.Split(",")) - 1)

                If CInt(demoid) = CInt(demo.Split(",")(i)) Then
                    checkdemoselected = True
                    Exit Function
                Else
                    checkdemoselected = False
                End If
            Next
        Else
            checkdemoselected = False
        End If
    End Function

    Public Shared Function GetRegionOnCountry(ByVal country As String) As Integer
        Dim strSQL As String = ""
        If Len(country) > 0 Then
            strSQL = "SELECT REGIONID FROM COUNTRY WHERE NAME = @COUNTRY"
            Dim regions As List(Of Region) = RegionRepository.FetchRegions(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@COUNTRY", country.ToString())}).ToArray())
            If (regions.Count > 0) Then
                If Len(regions.FirstOrDefault().RegionId) > 0 Then
                    GetRegionOnCountry = regions.FirstOrDefault().RegionId
                Else
                    GetRegionOnCountry = 0
                End If
            Else
                GetRegionOnCountry = 0
            End If
        Else
            GetRegionOnCountry = 0
        End If
    End Function

    Public Shared Function CheckcustomerfromLNA(ByVal country As String) As Boolean

        Dim RegionID As String = ""
        If Len(country) > 0 Then

            RegionID = GetRegionOnCountry(country)
            'response.Write RegionID
            'response.End 
            If CInt(RegionID) <> 0 Then
                If CInt(RegionID) = 3 Then
                    CheckcustomerfromLNA = True
                Else
                    CheckcustomerfromLNA = False
                End If
            Else
                CheckcustomerfromLNA = False
            End If
        Else
            CheckcustomerfromLNA = False
        End If
    End Function

    Public Shared Function CheckCustomerFromUS(ByVal country As String) As Boolean
        If Len(country) > 0 Then
            If LCase(Trim(country)) = "united states" Then
                CheckCustomerFromUS = True
            End If
        End If
    End Function

    Public Shared Function GetSalesRepEmail(ByVal countryName As String, ByVal requestTypeId As Int32, ByVal categoryId As Int32) As String
        If (String.IsNullOrEmpty(countryName)) Then Return String.Empty
        Dim sb As StringBuilder = New StringBuilder
        Dim xrefsAsProvided As List(Of SalesRepEmailAddressXRef) = GetSeparateListOfSalesRepEmailAddressXRefs(countryName, categoryId, requestTypeId)
        If (xrefsAsProvided.Count <= 0) Then Return String.Empty

        For Each salesRepEmailAddressXRef In xrefsAsProvided
            Dim salesRepEmailAddress As SalesRepEmailAddress = SalesRepEmailAddressRepository.GetSalesRepEmailAddress(ConfigurationManager.AppSettings("ConnectionString").ToString(), salesRepEmailAddressXRef.SalesRepEmailAddressFkId)
            If Not (salesRepEmailAddress Is Nothing) Then
                If (String.Compare(salesRepEmailAddress.IsActive, "Y", True) <> 0) Then Continue For
                sb.AppendFormat("{0},", salesRepEmailAddress.EmailAddress)
            End If
        Next

        Dim retVal As String = sb.ToString()
        If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 1)
        Return retVal
    End Function

    Public Shared Function GetSeparateListOfSalesRepEmailAddressXRefs(ByVal countryName As String, ByVal categoryId As Int32, ByVal requestTypeId As Int32) As IList(Of SalesRepEmailAddressXRef)
        Dim data As List(Of SalesRepEmailAddressXRef) = GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(countryName, categoryId, requestTypeId)
        If (data.Count <= 0) Then
            data = GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(countryName, categoryId, 1).ToList()
        End If
        If (data.Count <= 0) Then
            data = GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(countryName, 1, requestTypeId).ToList()
        End If
        If (data.Count <= 0) Then
            data = GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(countryName, 1, 1).ToList()
        End If
        If (data.Count <= 0) Then
            Functions.SendEmail("United States", String.Format("Could not locate an xref for country: {0}, category: {1}, request type: {2}.", countryName, categoryId, requestTypeId), "james.chan@teledynelecroy.com", "webmaster@teledynelecroy.com", "kate.kaplan@teledynelecroy.com", String.Empty, "Web.UI Error: Could not find sales rep email xref", String.Empty)
            Return New List(Of SalesRepEmailAddressXRef)
        End If
        Return data.ToList()
    End Function

    Public Shared Function GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(ByVal countryName As String, ByVal categoryId As Int32, ByVal requestTypeId As Int32) As IList(Of SalesRepEmailAddressXRef)
        Dim salesRepEmailAddressXRefs As List(Of SalesRepEmailAddressXRef) = SalesRepEmailAddressXRefRepository.GetSalesRepEmailAddressByCountryName(ConfigurationManager.AppSettings("ConnectionString").ToString(), countryName).ToList()
        Return salesRepEmailAddressXRefs.Where(Function(x) x.CategoryFkId = categoryId And x.RequestTypeFkId = requestTypeId).ToList()
    End Function

    Public Shared Function GetProductPriceonProductID(ByVal productId As Integer, ByVal masterProductId As Nullable(Of Int32)) As Decimal
        Dim retVal As Decimal = 0
        If (productId > 0) Then
            If (masterProductId.HasValue) Then
                Dim promotionXRef As PromotionXRef = PromotionXRefRepository.GetPromotionXRef(ConfigurationManager.AppSettings("ConnectionString").ToString(), masterProductId.Value, productId)
                If Not (promotionXRef Is Nothing) Then
                    retVal = promotionXRef.PromotionPrice
                Else
                    retVal = GetBaanSalePrice(productId)
                End If
            Else
                retVal = GetBaanSalePrice(productId)
            End If
        End If
        Return retVal
    End Function

    Private Shared Function GetBaanSalePrice(ByVal productId As Int32) As Decimal
        Dim retVal As Decimal = 0
        Dim baanPrice As BaanPrice = BaanPriceRepository.GetBaanPrice(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId)
        If Not (baanPrice Is Nothing) Then
            If (baanPrice.SalesPrice.HasValue) Then
                retVal = baanPrice.SalesPrice.Value
            Else
                retVal = baanPrice.ListPrice
            End If
        End If
        Return retVal
    End Function

    Public Shared Sub ClearSessionInShopper(ByRef hc As HttpContext)
        Dim s As String
        For i As Integer = 0 To hc.Session.Keys.Count - 1
            s = hc.Session.Keys(i)
            If s.Length > 9 Then
                If s.Substring(0, 9).Equals("productid") Then
                    hc.Session(s) = Nothing
                End If
                If s.Substring(0, 9).Equals("proListRQ") Then
                    hc.Session(s) = Nothing
                End If
            End If

            If s.Length > 10 Then
                If s.Substring(0, 10).Equals("mproductid") Then
                    hc.Session(s) = Nothing
                End If
            End If

            If s.Length > 7 Then
                If s.Substring(0, 7).Equals("objList") Then
                    hc.Session(s) = Nothing
                End If
            End If

            If s.Length > 8 Then
                If s.Substring(0, 8).Equals("seriesid") Then
                    hc.Session(s) = Nothing
                End If
            End If

            s = Nothing
        Next
    End Sub

    Public Shared Function GetPageTitle(ByVal id As String, ByVal OVERVIEW_TYPE_ID As String) As String
        ' Author:           Kate Kaplan
        ' Date:             1/23/2009
        ' Description:      GetPageTitle from CMS_OVERVIEW
        ' Returns:          String
        ' Revisions:        0
        Dim hm As New HtmlMeta()
        Dim sqlstr As String = ""
        Dim strTitle As String = ""
        sqlstr = "SELECT * FROM CMS_OVERVIEW where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", id))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", OVERVIEW_TYPE_ID))
        Dim cmsOverviews As List(Of CmsOverview) = CmsOverviewRepository.FetchCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If (cmsOverviews.Count > 0) Then
            If cmsOverviews.FirstOrDefault().PageTitle <> "" Then
                strTitle = cmsOverviews.FirstOrDefault().PageTitle
            End If
        End If
        GetPageTitle = strTitle
    End Function

    Public Shared Function GetSENumber(ByVal ContactID As Long) As String
        '*************************************
        'Gets SE_Code based on ContactID
        'Rev 1- KK - 9/6/2008 
        'rev 2 -KK -3/18/2011 (added CCMS_EXCEPTION_DOMAINS check)
        'Arguments - ContactID
        'Returns string
        '*************************************
        GetSENumber = ""
        Dim strSQL As String
        Dim dsCompany As DataSet
        Dim dsCompanyExist As DataSet
        Dim strSENumber As String = ""
        Dim dsZip As DataSet
        Dim strZip As String
        Dim dsSECode As DataSet
        If Len(ContactID) > 0 Then
            '****************************************************************
            'Check if the customer company is in the company exceptions list
            '****************************************************************
            'Select company name from contact table
            strSQL = "Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL,  SUBSTRING(LTRIM(RTRIM(CONTACT.EMAIL)), PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 1, { fn LENGTH(LTRIM(RTRIM(CONTACT.EMAIL))) } - PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 2)  AS DOMAIN  from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsCompany = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsCompany.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsCompany.Tables(0).Rows
                    'If the company is in exceptions list
                    'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES
                    strSQL = "Select SECode from CCMS_EXCEPTION_COMPANIES where STATE=@STATE and CompanyName = @COMPANYNAME and CITY=@CITY and SECode in (Select SECode from CCMS_SECSR)"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@STATE", dr("State").ToString()))
                    sqlParameters.Add(New SqlParameter("@COMPANYNAME", Replace(Trim(dr("Company")), "'", "").ToString()))
                    sqlParameters.Add(New SqlParameter("@CITY", Replace(Trim(dr("City")), "'", "").ToString()))
                    dsCompanyExist = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dsCompanyExist.Tables(0).Rows.Count > 0 Then
                        For Each drr As DataRow In dsCompanyExist.Tables(0).Rows
                            strSENumber = drr("SECode")
                        Next
                    End If
                    If Len(strSENumber) = 0 Then
                        Dim exceptionDomains As List(Of CCMSExceptionDomain) = BLL.FeCcmsManager.GetDomain(ConfigurationManager.AppSettings("ConnectionString").ToString(), Replace(Trim(dr("DOMAIN")), "'", ""), dr("State").ToString())
                        If (exceptionDomains.Count > 0) Then
                            Dim city As String = Replace(Trim(dr("CITY")), "'", "")
                            If (exceptionDomains.Where(Function(x) Not (String.IsNullOrEmpty(x.City)) AndAlso x.City.ToLower() = city.ToLower()).Count() > 0) Then
                                strSENumber = exceptionDomains.Where(Function(x) x.City.ToLower() = city.ToLower()).Select(Function(x) x.SeCode).FirstOrDefault()
                            Else
                                strSENumber = exceptionDomains.Where(Function(x) String.IsNullOrEmpty(x.City)).Select(Function(x) x.SeCode).FirstOrDefault()
                            End If
                        End If
                    End If
                Next
            End If
            '****************************************************************
            'If customer company is not in the company exceptions list
            'Get SENumber by Zip
            '****************************************************************
            If Len(strSENumber) = 0 Then
                'Select ZIP for current customer from table CONTACT
                strSQL = "Select POSTALCODE from CONTACT where CONTACT_ID=@CONTACTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
                dsZip = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If dsZip.Tables(0).Rows.Count > 0 Then
                    For Each drrr As DataRow In dsZip.Tables(0).Rows
                        strZip = Trim(drrr("POSTALCODE"))
                        If Len(strZip) >= 5 Then
                            'If Zip code have more then 5 digits we cut and use first five to find SENumber
                            If Len(strZip) > 5 Then
                                strZip = Mid(strZip, 1, 5)
                            End If
                            'Select SECode for this ZIP CODE from table CCMS_MAP_US
                            If Not IsNumeric(strZip) Then
                                GetSENumber = ""
                                Exit Function
                            End If
                            strSQL = "Select SECode from CCMS_MAP_US where ZipCode=@ZIPCODE"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@ZIPCODE", strZip))
                            dsSECode = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            If dsSECode.Tables(0).Rows.Count > 0 Then
                                For Each drrrr As DataRow In dsSECode.Tables(0).Rows
                                    strSENumber = Trim(drrrr("SECode"))
                                Next
                            End If
                        End If
                    Next
                End If
            End If
            GetSENumber = strSENumber
        End If
    End Function

    Public Shared Function GetQuoteBody(ByVal QuoteID As Long, ByVal ContactID As Long, ByVal FirstName As String) As String
        GetQuoteBody = ""
        If QuoteID > 0 Then
            Dim strSQL As String
            Dim strTab As String = "&nbsp;&nbsp;&nbsp;&nbsp;" 'Chr(9)
            Dim strCret As String = "<br>" 'Chr(13) & Chr(10)
            '   If Len(Session("emaillocaleid")) = 0 Then Session("emaillocaleid") = 1033
            Dim localeid As Integer = 1033
            'If this customer has scopes in the basket
            Dim content As StringBuilder = New StringBuilder()
            Dim count As Long = 0
            Dim TotalPrice As Decimal = 0
            Dim SubTotal As Decimal = 0
            Dim Total As Decimal = 0
            Dim ds As DataSet
            Dim dq As DataSet
            Dim dss As DataSet
            Dim dsss As DataSet
            Dim strQuoteTime As Date

            strSQL = "SELECT QUOTE_TIME  from QUOTE where CONTACTID=@CONTACTID and QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            dq = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            For Each dqr As DataRow In dq.Tables(0).Rows
                strQuoteTime = dqr("QUOTE_TIME")
            Next

            strSQL = " SELECT DISTINCT SHOPPERQUOTE.PROPERTYGROUPID , PROPERTYGROUP.SORTID " +
               " FROM SHOPPERQUOTE INNER JOIN " +
               " PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " +
               " PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = @QUOTEID ORDER BY PROPERTYGROUP.SORTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then

                content.Append(LoadI18N("SLBCA0098", localeid) & " " & FirstName & "," & strCret & strCret)
                content.Append(LoadI18N("SLBCA0099", localeid) & "!" & strCret)
                content.Append("<b>" & LoadI18N("SLBCA0100", localeid) & ":</b>" & strCret)
                content.Append("<b>" & strCret & LoadI18N("SLBCA0007", localeid) & "</b>: " & QuoteID & strCret)
                content.Append("<b>" & LoadI18N("SLBCA0069", localeid) & "</b>: " & FormatDateTime(strQuoteTime, 2) & strCret)
                content.Append("<b>" & LoadI18N("SLBCA0070", localeid) & "</b>: " & FormatDateTime((DateAdd(DateInterval.Day, 30, strQuoteTime)), 2) & strCret & strCret)
                content.Append(LoadI18N("SLBCA0595", localeid) & strCret & strCret)

                For Each dr As DataRow In ds.Tables(0).Rows
                    strSQL = "SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, PRODUCT.PRODUCTDESCRIPTION AS ProdDesc, " +
                       "SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " +
                       "FROM SHOPPERQUOTE INNER JOIN " +
                       "PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " +
                       "PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " +
                       "WHERE (SHOPPERQUOTE.QUOTEID = @QUOTEID)  AND (PRODUCT.PROPERTYGROUPID = @PROPERTYGROUPID) ORDER BY PROPERTYGROUP.SORTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                    sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", dr("PROPERTYGROUPID").ToString()))
                    dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        For Each drr As DataRow In dss.Tables(0).Rows
                            strSQL = "SELECT QTY AS Qty,PRICE FROM SHOPPERQUOTE WHERE PRODUCTID = @PRODUCTID AND QUOTEID = @QUOTEID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", drr("PRODUCTID").ToString()))
                            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                            dsss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            If dsss.Tables(0).Rows.Count > 0 Then
                                For Each drrr As DataRow In dsss.Tables(0).Rows
                                    content.Append("<b>" & drr("qty").ToString + strTab & "</b>")

                                    If Len(drr("PartNum").ToString) > 8 Then
                                        content.Append("<b>" & drr("PartNum") + strTab & "</b>")
                                    ElseIf Len(drr("PartNum").ToString) > 8 And Len(drr("PartNum").ToString) < 12 Then
                                        content.Append("<b>" & drr("PartNum").ToString + strTab + strTab & "</b>")
                                    Else
                                        content.Append("<b>" & drr("PartNum").ToString + strTab + strTab + strTab & "</b>")
                                    End If
                                    TotalPrice = (drr("QTY") * drrr("PRICE"))
                                    content.Append("<b>" & FormatCurrency(drrr("Price"), 2) + strTab & "</b>")
                                    'SubTotal = SubTotal + TotalPrice
                                    If CInt(drr("QTY")) > 1 And drrr("Price") > 0 Then
                                        content.Append("Extended Price: " + FormatCurrency(TotalPrice, 2) + strTab)
                                    End If
                                    If CInt(InStr(1, drr("ProdDesc"), "<font")) > 0 Then
                                        content.Append(strCret & "   " & Mid(drr("ProdDesc"), 1, (InStr(1, drr("ProdDesc"), "<font")) - 1) & strCret & strCret)
                                    Else
                                        content.Append(strCret & "   " & drr("ProdDesc") & strCret & strCret)
                                    End If
                                Next
                                'content.Append("Total for " + drr("PartNum") + FormatCurrency(SubTotal, 2) & strCret & strCret)
                                Total = Total + TotalPrice

                            End If
                        Next
                    End If
                Next
                If Total > 0 Then
                    content.Append("<b>" & LoadI18N("SLBCA0006", localeid) & ": " & FormatCurrency(Total, 2) & strCret & strCret & strCret & "</b>")
                Else
                    content.Append(LoadI18N("SLBCA0006", localeid) & ": " & LoadI18N("SLBCA0010", localeid) & strCret & strCret & strCret)
                End If
                content.Append("<hr size='1'>")
                content.Append("<b>Phone:</b> 1-800-553-2769 (1-800-45LECROY)" & strCret)
                content.Append("LeCroy Corporation" & strCret)
                content.Append("700 Chestnut Ridge Road" & strCret)
                content.Append("Chestnut Ridge. NY 10977" & strCret & strCret & strCret)
                content.Append("To review our Purchase Terms and Conditions for United States please follow the link below:" & strCret)
                content.Append("<a href=""https://teledynelecroy.com/Terms/"">https://teledynelecroy.com/Terms/</a>" & strCret & strCret & strCret)
                content.Append(strCret & LoadI18N("SLBCA0039", localeid) & ". ")
                content.Append(LoadI18N("SLBCA0041", localeid) & ". ")
                content.Append(LoadI18N("SLBCA0042", localeid) & "." & strCret & strCret)
                content.Append(LoadI18N("SLBCA0114", localeid) & " (<a href=""/cgi-bin/profile/modform.asp?redir=/ecommerce/quotehistory.aspx"">You can modify your profile here</a>)" & strCret & strCret)
                content.Append(CustomerData(ContactID))
            End If
            GetQuoteBody = content.ToString
        End If
    End Function


    Public Shared Sub SendEmail(ByVal strCountry As String, ByVal strBody As String, ByVal strTo As String, ByVal strFrom As String, ByVal strCC As String, ByVal strDisplayName As String, ByVal strSubject As String, ByVal AttachmentFile As String)
        If Len(strBody) > 0 And Len(strTo) > 0 Then
            Dim mm As New CDO.Message
            mm.To = strTo
            'send CC email
            If Len(Trim(strCC)) > 0 Then
                mm.CC = strCC
            End If
            If Len(strFrom) > 0 Then
                If (String.Compare("webmaster@teledynelecroy.com", strFrom, True) <> 0) Then
                    mm.From = "webmaster@teledynelecroy.com"
                    mm.ReplyTo = strFrom
                Else
                    mm.From = strDisplayName & "<" & strFrom & ">"
                    mm.ReplyTo = strFrom
                End If
            Else
                mm.From = strDisplayName & "<postmaster@teledynelecroy.com>"
            End If
            mm.BCC = "kate.kaplan@teledyne.com,james.chan@teledyne.com"
            mm.TextBody = strBody

            mm.Subject = strSubject
            If Len(strCountry) > 0 Then
                If CStr(strCountry) = "Japan" Then
                    mm.TextBodyPart.Charset = "shift-JIS"
                ElseIf CStr(strCountry) = "China" Then
                    mm.TextBodyPart.Charset = "GB2312"
                ElseIf Mid(CStr(strCountry), 1, 5) = "Korea" Then
                    mm.TextBodyPart.Charset = "korean"
                End If
            End If
            Try
                mm.Send()
            Catch ex As Exception
                Functions.ErrorHandlerLog(True, ex.Message + " " + ex.Source)
            End Try
        End If
    End Sub

    Public Shared Function CustomerData(ByVal ContactID As Long) As String
        CustomerData = ""
        Dim strInfo As String = ""
        Dim strSQL As String
        Dim dsCustomerData As DataSet
        Dim strCRet As String = "<br>"
        If ContactID > 0 Then
            strSQL = "Select * from CONTACT where CONTACT_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsCustomerData = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsCustomerData.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsCustomerData.Tables(0).Rows
                    ' Client data
                    strInfo = "Client: " & dr("Honorific") & " " & dr("First_Name") & " " & dr("Last_Name") & strCRet
                    If Len(Trim(dr("Title"))) > 0 Then
                        strInfo = strInfo & "Title: " & dr("Title") & strCRet
                    End If
                    If Len(Trim(dr("Company"))) > 0 Then
                        strInfo = strInfo & "Company: " & dr("Company") & strCRet
                    End If
                    If Len(Trim(dr("Address"))) > 0 Then
                        strInfo = strInfo & "Address: " & dr("Address") & " " & dr("Address2") & strCRet
                    End If
                    If Len(Trim(dr("City"))) > 0 Then
                        strInfo = strInfo & "City: " & dr("City") & strCRet
                    End If
                    If Len(Trim(dr("State_Province"))) > 0 Then
                        strInfo = strInfo & "State: " & dr("State_Province") & strCRet
                    End If
                    If Len(Trim(dr("PostalCode"))) > 0 Then
                        strInfo = strInfo & "Zip: " & dr("PostalCode") & strCRet
                    End If
                    If Len(Trim(dr("Country"))) > 0 Then
                        strInfo = strInfo & "Country: " & dr("Country") & strCRet
                    End If
                    If Len(Trim(dr("Phone"))) > 0 Then
                        strInfo = strInfo & "Phone: " & dr("Phone") & strCRet
                    End If
                    If Len(Trim(dr("Fax"))) > 0 Then
                        strInfo = strInfo & "Fax: " & dr("Fax") & strCRet
                    End If
                    If Len(Trim(dr("Email"))) > 0 Then
                        strInfo = strInfo & "Email: " & dr("Email") & strCRet
                    End If
                    If Len(Trim(dr("URL"))) > 0 Then
                        strInfo = strInfo & "URL: " & dr("URL") & strCRet
                    End If
                    If Len(Trim(dr("APPLICATION"))) > 0 Then
                        strInfo = strInfo & "Application: " & dr("APPLICATION") & strCRet
                    End If
                Next
                CustomerData = strInfo
            End If
        End If
    End Function

    Public Shared Function GetTechRepEmail(ByVal countryName As String, ByVal categoryId As Int32) As String
        GetTechRepEmail = ""
        If Len(countryName) > 0 Then
            Dim strSQL As String = ""
            strSQL = "select V_TECH_REP_EMAIL.EMAIL from V_COUNTRY, V_TECH_REP_EMAIL "
            strSQL = strSQL + "where UPPER(V_COUNTRY.NAME) = @COUNTRYNAME and "
            strSQL = strSQL + "V_TECH_REP_EMAIL.TECH_REP_ID = V_COUNTRY.TECH_REP_ID"
            'Response.Write strSQL
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@COUNTRYNAME", countryName.ToString())}).ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                GetTechRepEmail = ds.Tables(0).Rows(0)("EMAIL").ToString
            Else
                GetTechRepEmail = GetSalesRepEmail(countryName, 4, categoryId)
            End If
        End If
    End Function

    Public Shared Function EmailBodyClientData(ByVal ContactID As Long, ByVal localeid As String) As String
        EmailBodyClientData = ""
        Dim strInfo As String = ""
        Dim strSQL As String
        Dim dsEmailBodyClientData As DataSet
        Dim strCRet As String
        If Len(localeid) = 0 Then localeid = 1033
        strCRet = Chr(13) & Chr(10)

        If ContactID > 0 And Len(localeid) > 0 Then
            strSQL = "Select * from CONTACT where CONTACT_ID=@CONTACTID"
            dsEmailBodyClientData = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@CONTACTID", ContactID.ToString())}).ToArray())
            If dsEmailBodyClientData.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsEmailBodyClientData.Tables(0).Rows
                    ' Client data
                    If localeid <> 1041 Then

                        ' Client data
                        strInfo = strInfo & LoadI18N("SLBCA0076", localeid) & ": " & dr("Honorific").ToString & " " & dr("First_Name").ToString & " " & dr("Last_Name").ToString & strCRet
                        If Len((dr("Title").ToString)) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0077", localeid) & ": " & dr("Title").ToString & strCRet
                        End If
                        If Len(dr("Department").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0465", localeid) & ": " & dr("Department").ToString & strCRet
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0078", localeid) & ": " & dr("Company").ToString & strCRet
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & ": " & dr("Address").ToString & " " & dr("Address2").ToString & strCRet
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0080", localeid) & ": " & dr("City").ToString & strCRet
                        End If
                        If Len(dr("State_Province").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0081", localeid) & ": " & dr("State_Province").ToString & strCRet
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0082", localeid) & ": " & dr("PostalCode").ToString & strCRet
                        End If
                        If Len(dr("Country").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0083", localeid) & ": " & dr("Country").ToString & strCRet
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0084", localeid) & ": " & dr("Phone").ToString & strCRet
                        End If
                        If Len(dr("Fax").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0085", localeid) & ": " & dr("Fax").ToString & strCRet
                        End If
                        If Len(dr("Email").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0086", localeid) & ": mailto:" & dr("Email").ToString & strCRet
                        End If
                        If Len(dr("URL").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0087", localeid) & ": " & dr("URL").ToString & strCRet
                        End If
                        If Len(dr("APPLICATION").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0088", localeid) & ": " & dr("APPLICATION").ToString & strCRet
                        End If
                    Else
                        ' Client data for Japan
                        If Len(dr("Country").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0083", localeid) & ": " & dr("Country").ToString & strCRet
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0082", localeid) & ": " & dr("PostalCode").ToString & strCRet
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0080", localeid) & ": " & dr("City").ToString & strCRet
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & ": " & dr("Address").ToString & strCRet
                        End If
                        If Len(dr("Address2").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & "2: " & dr("Address2").ToString & strCRet
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0078", localeid) & ": " & dr("Company").ToString & strCRet
                        End If
                        If Len(dr("Title").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0077", localeid) & ": " & dr("Title").ToString & strCRet
                        End If
                        If Len(dr("First_Name").ToString) > 0 And Len(dr("Last_Name").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0076", localeid) & ": " & dr("Last_Name").ToString & " " & dr("First_name").ToString & " " & LoadI18N("SLBCA0160", localeid).ToString & strCRet
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0084", localeid) & ": " & dr("Phone").ToString & strCRet
                        End If
                        If Len(dr("Fax").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0085", localeid) & ": " & dr("Fax").ToString & strCRet
                        End If
                        If Len(dr("Email").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0086", localeid) & ": " & dr("Email").ToString & strCRet
                        End If
                        If Len(dr("URL").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0087", localeid) & ": " & dr("URL").ToString & strCRet
                        End If
                        If Len(dr("APPLICATION").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0088", localeid) & ": " & dr("APPLICATION").ToString & strCRet
                        End If
                    End If
                Next
                EmailBodyClientData = strInfo
            End If
        End If
    End Function

    Public Shared Function EmailBodyClientDataHtml(ByVal ContactID As Long, ByVal localeid As String) As String
        EmailBodyClientDataHtml = ""
        Dim strInfo As String = ""
        Dim strSQL As String
        Dim dsEmailBodyClientData As DataSet
        Dim strCRet As String
        If Len(localeid) = 0 Then localeid = 1033
        strCRet = "<br />"

        If ContactID > 0 And Len(localeid) > 0 Then
            strSQL = "Select * from CONTACT where CONTACT_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsEmailBodyClientData = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsEmailBodyClientData.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsEmailBodyClientData.Tables(0).Rows
                    ' Client data
                    If localeid <> 1041 Then

                        ' Client data
                        strInfo = strInfo & LoadI18N("SLBCA0076", localeid) & ": " & dr("Honorific").ToString & " " & dr("First_Name").ToString & " " & dr("Last_Name").ToString & strCRet
                        If Len((dr("Title").ToString)) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0077", localeid) & ": " & dr("Title").ToString & strCRet
                        End If
                        If Len(dr("Department").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0465", localeid) & ": " & dr("Department").ToString & strCRet
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0078", localeid) & ": " & dr("Company").ToString & strCRet
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & ": " & dr("Address").ToString & " " & dr("Address2").ToString & strCRet
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0080", localeid) & ": " & dr("City").ToString & strCRet
                        End If
                        If Len(dr("State_Province").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0081", localeid) & ": " & dr("State_Province").ToString & strCRet
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0082", localeid) & ": " & dr("PostalCode").ToString & strCRet
                        End If
                        If Len(dr("Country").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0083", localeid) & ": " & dr("Country").ToString & strCRet
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0084", localeid) & ": " & dr("Phone").ToString & strCRet
                        End If
                        If Len(dr("Fax").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0085", localeid) & ": " & dr("Fax").ToString & strCRet
                        End If
                        If Len(dr("Email").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0086", localeid) & ": mailto:" & dr("Email").ToString & strCRet
                        End If
                        If Len(dr("URL").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0087", localeid) & ": " & dr("URL").ToString & strCRet
                        End If
                        If Len(dr("APPLICATION").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0088", localeid) & ": " & dr("APPLICATION").ToString & strCRet
                        End If
                    Else
                        ' Client data for Japan
                        If Len(dr("Country").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0083", localeid) & ": " & dr("Country").ToString & strCRet
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0082", localeid) & ": " & dr("PostalCode").ToString & strCRet
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0080", localeid) & ": " & dr("City").ToString & strCRet
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & ": " & dr("Address").ToString & strCRet
                        End If
                        If Len(dr("Address2").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0079", localeid) & "2: " & dr("Address2").ToString & strCRet
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0078", localeid) & ": " & dr("Company").ToString & strCRet
                        End If
                        If Len(dr("Title").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0077", localeid) & ": " & dr("Title").ToString & strCRet
                        End If
                        If Len(dr("First_Name").ToString) > 0 And Len(dr("Last_Name").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0076", localeid) & ": " & dr("Last_Name").ToString & " " & dr("First_name").ToString & " " & LoadI18N("SLBCA0160", localeid).ToString & strCRet
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0084", localeid) & ": " & dr("Phone").ToString & strCRet
                        End If
                        If Len(dr("Fax").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0085", localeid) & ": " & dr("Fax").ToString & strCRet
                        End If
                        If Len(dr("Email").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0086", localeid) & ": " & dr("Email").ToString & strCRet
                        End If
                        If Len(dr("URL").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0087", localeid) & ": " & dr("URL").ToString & strCRet
                        End If
                        If Len(dr("APPLICATION").ToString) > 0 Then
                            strInfo = strInfo & LoadI18N("SLBCA0088", localeid) & ": " & dr("APPLICATION").ToString & strCRet
                        End If
                    End If
                Next
                EmailBodyClientDataHtml = strInfo
            End If
        End If
    End Function


    Public Shared Function GetOverviewHTML(ByVal OVERVIEW_TYPE_ID As String, ByVal ID As String) As String
        Dim retVal As String = String.Empty
        If Len(OVERVIEW_TYPE_ID) > 0 And Len(ID) > 0 Then
            Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(ID), Int32.Parse(OVERVIEW_TYPE_ID))
            If Not (cmsOverview Is Nothing) Then retVal = cmsOverview.OverviewDetails
        End If
        Return retVal
    End Function

    Public Shared Function getTitleForCat(ByVal catid As String) As String
        '*************************************
        'Gets CATEGORY_NAME from CATEGORY table
        'Rev 1- KK - 9/10/2009
        'Arguments - catid
        'Returns string
        '*************************************
        Dim strSQL As String = ""
        Dim title As String = ""
        If Len(catid) > 0 Then
            If IsNumeric(catid) Then
                strSQL = "SELECT category_name FROM category WHERE category_id = @CATEGORYID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
                title = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).ToString()
            End If
        End If
        getTitleForCat = title
    End Function

    Public Shared Function getTitleForType(ByVal docid As String) As String
        '*************************************
        'Gets DOCUMENT TYPE NAME
        'Rev 1- KK - 9/10/2009
        'Arguments - docid
        'Returns string
        '*************************************
        Dim strSQL As String = ""
        Dim title As String = ""
        If Len(docid) > 0 Then
            If IsNumeric(docid) Then
                strSQL = "SELECT name FROM document_type WHERE doc_type_id = @DOCTYPEID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@DOCTYPEID", docid))
                title = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).ToString()
            End If
        End If
        getTitleForType = title
    End Function

    Public Shared Function getTitleForGroup(ByVal groupid As String) As String
        '*************************************
        'Gets GROUP_NAME from PRODUCT_GROUP
        'Rev 1- KK - 9/10/2009
        'Arguments - groupid
        'Returns string
        '*************************************
        Dim strSQL As String = ""
        Dim title As String = ""
        Dim ds As DataSet
        If Len(groupid) > 0 Then
            If IsNumeric(groupid) Then
                strSQL = "SELECT group_name FROM product_group WHERE group_id = @GROUPID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    title = dr("group_name").ToString
                End If

            End If
        End If
        getTitleForGroup = title

    End Function
    Shared Function hasListPrice(ByVal mseries As String) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             Sept 16,2009
        ' Description:      checkes if scope series is flagged to display list price
        ' Returns:          Boolean
        ' Revisions:        1
        Dim strSQL As String = ""
        hasListPrice = False

        If Len(mseries) > 0 Then
            strSQL = "SELECT * FROM PRODUCT_SERIES where DisplayListPrice_YN='y' and PRODUCT_SERIES_ID=@PRODUCTSERIESID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            hasListPrice = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count > 0

        End If
    End Function

    Shared Function LeftMenuScope(ByVal CAPTION_ID As String, ByVal rootdir As String, ByVal pn As Panel, ByVal menu_id As String, ByVal mseries As String, ByVal modelid As String) As String
        ' Author:           Kate Kaplan
        ' Date:             Sept 16,2009
        ' Description:      creates HTML string for left hand side menu in oscilloscope section
        ' Returns:          String
        ' Revisions:        1
        Dim sqlstr As String = ""
        Dim sortID As String = ""
        Dim result As String = ""
        Dim menuURL As String = ""
        Dim chrv As String = Chr(13) & Chr(10)
        Dim se As StringWriter
        Dim hw As Html32TextWriter
        Dim len As Integer
        se = New StringWriter()
        hw = New Html32TextWriter(se)

        sqlstr = "select * from MENU_MAIN where PROTOCOL_STANDARD_ID is NULL and CAPTION_ID=@CAPTIONID AND [ENABLED] = 'Y' order by sort_id "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CAPTIONID", CAPTION_ID))
        Dim menuMain As List(Of MenuMain) = MenuMainRepository.FetchMenuMains(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        For Each mm In menuMain
            If mm.MenuId.ToString().Length > 0 And InStr(mm.Url, "/oscilloscope/configure") > 0 Then
                menuURL = "/oscilloscope/configure/configure_step2.aspx"
            Else
                menuURL = mm.Url
            End If
            If Not menuURL.Contains("http") Then
                If menuURL.Contains("?") Then
                    menuURL += "&capid=" + CAPTION_ID
                    menuURL += "&mid=" + mm.MenuId.ToString()
                    If Not mseries.ToString Is Nothing Then
                        menuURL += "&seriesid=" + mseries.ToString()
                    End If
                    If Not modelid.ToString Is Nothing Then
                        menuURL += "&modelid=" + modelid.ToString()
                    End If
                Else
                    'menuURL += "?capid=" + CAPTION_ID
                    'menuURL += "&mid=" + mm.MenuId.ToString()
                    'If mseries.ToString.Length > 0 Then
                    '    menuURL += "&seriesid=" + mseries.ToString()
                    'End If
                    'If modelid.ToString.Length > 0 Then
                    '    menuURL += "&modelid=" + modelid.ToString()
                    'End If
                End If
                menuURL = rootdir + menuURL
            End If

            If mm.MenuId.ToString() = menu_id Then
                result += chrv & "<li class='current'><a href='" & menuURL & "'>" & Trim(mm.MenuName) & "</a>"
                pn.RenderControl(hw)
                len = se.ToString().Length
                If (len > 0) Then
                    result += se.ToString()
                End If
                result += "</li>"
            Else
                result += chrv & "<li><a href='" & menuURL & "'>" & Trim(mm.MenuName) & "</a></li>"
            End If
        Next
        Return result
    End Function

    Shared Function GetCategoryIDonGroupID(ByVal groupid As String) As String
        ' Author:           Kate Kaplan
        ' Date:             Sept 16,2009
        ' Description:      gets Category_id on Group_id
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If Len(groupid) > 0 Then
            Dim sqlKeys As String = String.Empty
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(ConvertPotentialCommaSeparatedStringIntoList(groupid.ToString()), "GROUPID", sqlKeys, sqlParameters)
            If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count > 0) Then Return String.Empty
            Dim strSQL As String = String.Format("SELECT distinct CATEGORY_ID FROM PRODUCT_GROUP WHERE GROUP_ID in ({0})", sqlKeys)
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productGroups.Count > 0) Then
                If (productGroups.FirstOrDefault().CategoryId.HasValue) Then
                    result = productGroups.FirstOrDefault().CategoryId.Value.ToString()
                Else
                    result = ""
                End If
            Else
                result = ""
            End If
        End If
        GetCategoryIDonGroupID = result
    End Function

    Shared Function GetProdDescriptionOnProdID(ByVal productid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             October 1,2009
        ' Description:      Gets productdescription with LIM or SVC note
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If productid > 0 Then
            Dim strSQL As String = "SELECT PRODUCTDESCRIPTION,LIM_YN,SVC_YN FROM PRODUCT WHERE PRODUCTID=@PRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
            Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (products.Count > 0) Then
                If InStr(products.FirstOrDefault().ProductDescription, " <font") > 0 Then
                    result = Mid(products.FirstOrDefault().ProductDescription, 1, (InStr(products.FirstOrDefault().ProductDescription, " <font")))
                Else
                    result = products.FirstOrDefault().ProductDescription
                End If

                If LCase(products.FirstOrDefault().LimYN) = "y" Then
                    result += "&nbsp;<font color=#FF0000>Limited Availability</font>"
                End If
                If LCase(products.FirstOrDefault().SvcYN) = "y" Then
                    result += "&nbsp;<font color=red>Demonstration or Refurbished Units Only</font>"
                End If
            Else
                result = ""
            End If
        End If
        GetProdDescriptionOnProdID = result
    End Function

    Shared Function GetPromoImagesForSeries(ByVal mseries As Integer, ByVal rootDir As String) As String
        Return GetPromoImagesForSeries(mseries, rootDir, -1)
    End Function

    Public Shared Function GetPromoImagesForSeries(ByVal productSeriesId As Int32, ByVal rootDir As String, ByVal countryCode As Int32) As String
        ' Author:           Kate Kaplan
        ' Date:             October 15,2009
        ' Description:      Gets promo images and URLs for series
        ' Returns:          String
        ' Revisions:        1
        Dim sb As StringBuilder = New StringBuilder
        If (productSeriesId > 0) Then
            Dim sqlString As String = "SELECT * FROM [PRODUCT_SERIES_PROMO] WHERE [ACTIVE_YN] = 'y' AND [PRODUCT_SERIES_ID] = @PRODUCTSERIESID ORDER BY [SORT_ID]"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString())})
            Dim ds As DataSet = New DataSet()
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If ((dr("CountryFkId") Is DBNull.Value) OrElse (countryCode = Int32.Parse(dr("CountryFkId").ToString()))) Then
                        If Not (String.IsNullOrEmpty(dr("PROMO_IMAGE").ToString())) Then
                            If (sb.Length > 0) Then sb.Append("<br />")
                            If Not (String.IsNullOrEmpty(dr("PROMO_URL").ToString())) Then
                                sb.Append(String.Format("<a href=""{0}""><img src=""{1}"" alt=""{2}"" border=""0"" /></a>", String.Concat(rootDir, dr("PROMO_URL").ToString()), String.Concat(rootDir, dr("PROMO_IMAGE").ToString()), dr("PROMO_TITLE").ToString()))
                            Else
                                sb.Append(String.Format("<img src=""{0}"" alt=""{1}"" border=""0"" />", String.Concat(rootDir, dr("PROMO_IMAGE").ToString()), dr("PROMO_TITLE").ToString()))
                            End If
                        End If
                    End If
                Next
            End If
            ds = Nothing
        End If
        Return sb.ToString()
    End Function

    Shared Function GetPromoImagesForStandard(ByVal standardid As Integer, ByVal rootDir As String) As String
        ' Author:           Kate Kaplan
        ' Date:             October 16,2009
        ' Description:      Gets promo images and URLs for standard
        ' Returns:          String
        ' Revisions:        1
        Dim ds As DataSet
        Dim result As String = ""
        Dim strHtml As String = ""
        If standardid > 0 Then
            Dim strSQL As String = "SELECT * FROM  PROTOCOL_STANDARD_PROMO WHERE ACTIVE_YN = 'y'  AND  PROTOCOL_STANDARD_ID=@STANDARDID ORDER BY SORT_ID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@STANDARDID", standardid.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Len(dr("PROMO_IMAGE").ToString) > 0 Then
                        If Len(dr("PROMO_URL").ToString) > 0 Then
                            strHtml = "<a href='" & rootDir & dr("PROMO_URL").ToString & "'><img src='" & rootDir & dr("PROMO_IMAGE").ToString & "' alt='" & dr("PROMO_TITLE").ToString & "' border='0' /></a>"
                        Else
                            strHtml = "<img src='" & rootDir & dr("PROMO_IMAGE").ToString & "' alt='" & dr("PROMO_TITLE").ToString & "' border='0' />"
                        End If
                        If Len(result) > 0 Then
                            result += "<br />" & strHtml
                        Else
                            result = strHtml
                        End If
                    End If
                Next
            End If
            ds = Nothing
        End If
        GetPromoImagesForStandard = result
    End Function

    Shared Function GetScopeSeriesIDonProductID(ByVal productid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             October 16,2009
        ' Description:      gets Scope Series on Product Id
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If productid > 0 Then
            Dim strSQL As String = "SELECT PRODUCT_SERIES_ID FROM   PRODUCT_SERIES_CATEGORY WHERE CATEGORY_ID = 1 AND PRODUCT_ID = @PRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
            Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productSeries.Count > 0) Then
                result = productSeries.FirstOrDefault().ProductSeriesId.ToString()
            Else
                result = ""
            End If
        End If
        GetScopeSeriesIDonProductID = result
    End Function

    Shared Function SeriesHasEStoreLink(ByVal mseries As Integer, ByVal contactid As Long) As String
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries)
        If (productSeries Is Nothing) Then Return String.Empty
        If (String.IsNullOrEmpty(productSeries.ShopifyCollectionId)) Then Return String.Empty

        Dim categoryId As Int32 = ProductSeriesCategoryRepository.GetCategoryAssociatedToProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries)
        If (categoryId > 0) Then
            Return String.Format("<a href=""https://store.teledynelecroy.com{0}"">Buy Now</a>", productSeries.ShopifyCollectionId)
        End If
        Return String.Empty
    End Function

    Shared Function GroupHasEStoreLink(ByVal groupid As Integer, ByVal contactid As Long) As String
        ' Author:           Kate Kaplan
        ' Date:             July 26, 2010
        ' Description:      checks for estore enabled products by group
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If groupid > 0 Then

            Dim strSQL As String = "SELECT distinct b.CATEGORY_ID FROM  PRODUCT_GROUP b INNER JOIN PRODUCT c ON b.GROUP_ID = c.GROUP_ID WHERE b.GROUP_ID=@GROUPID AND  c.EStoreYN = 'y' "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupid.ToString()))
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productGroups.Count > 0) Then
                If contactid > 0 Then
                    'result = "<li class='link'><a href='https://store.teledynelecroy.com/cart_step2.aspx?seriesid=" + groupid.ToString + "'>Buy Now</a></li>"
                Else
                    If (productGroups.FirstOrDefault().CategoryId.HasValue) Then
                        result = "<li class='link'><a href='https://store.teledynelecroy.com'>Buy Now</a></li>"
                    Else
                        result = ""
                    End If
                End If
            Else
                result = ""
            End If
        End If
        GroupHasEStoreLink = result
    End Function
    Shared Function ModelHasEStoreLink(ByVal modelid As Integer, ByVal contactid As Long) As String
        ' Author:           Kate Kaplan
        ' Date:             July 26, 2010
        ' Description:      checks for estore enabled products by group
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If modelid > 0 Then
            Dim strSQL As String = "SELECT distinct b.CATEGORY_ID ,b.GROUP_ID FROM  PRODUCT_GROUP b INNER JOIN PRODUCT c ON b.GROUP_ID = c.GROUP_ID WHERE c.PRODUCTID=@PRODUCTID AND  c.EStoreYN = 'y' "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid.ToString()))
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productGroups.Count > 0) Then
                If contactid > 0 Then
                    'result = "<li class='link'><a href='https://store.teledynelecroy.com/cart_step2.aspx?seriesid=" + rs("GROUP_ID").ToString + "'>Buy Now</a></li>"
                Else
                    If (productGroups.FirstOrDefault().CategoryId.HasValue) Then
                        result = "<li class='link'><a href='https://store.teledynelecroy.com'>Buy Now</a></li>"
                    Else
                        result = ""
                    End If
                End If
            Else
                result = ""
            End If
        End If
        ModelHasEStoreLink = result
    End Function
    Shared Function CategoryHasEStoreLink(ByVal categoryid As Integer, ByVal contactid As Long) As String
        ' Author:           Kate Kaplan
        ' Date:             July 27, 2010
        ' Description:      checks for estore enabled products by group
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If categoryid > 0 Then
            Dim strSQL As String = "SELECT distinct b.CATEGORY_ID FROM  PRODUCT_GROUP b INNER JOIN PRODUCT c ON b.GROUP_ID = c.GROUP_ID WHERE b.CATEGORY_ID=@CATEGORYID AND  c.EStoreYN = 'y' "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid.ToString()))
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productGroups.Count > 0) Then
                If (productGroups.FirstOrDefault().CategoryId.HasValue) Then
                    result = "<li class='link'><a href='https://store.teledynelecroy.com'>Buy Now</a></li>"
                Else
                    result = ""
                End If
            Else
                result = ""
            End If
        End If
        CategoryHasEStoreLink = result
    End Function

    Shared Function GetSeriesNameonID(ByVal mseries As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             October 23,2009
        ' Description:      gets series name on product_series_id
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If mseries > 0 Then
            Dim strSQL As String = "SELECT NAME FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID = @PRODUCTSERIESID"
            Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@PRODUCTSERIESID", mseries.ToString())}).ToArray())
            If (productSeries.Count > 0) Then
                result = productSeries.FirstOrDefault().Name
            Else
                result = ""
            End If
        End If
        GetSeriesNameonID = result
    End Function

    Shared Function GetLiteratureNameonID(ByVal litid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             October 23,2009
        ' Description:      gets literature name on literature_id
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If litid > 0 Then
            Dim strSQL As String = "SELECT NAME FROM LITERATURE WHERE LITERATURE_ID = @LITERATUREID"
            Dim literatures As List(Of Literature) = LiteratureRepository.FetchLiterature(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@LITERATUREID", litid.ToString())}).ToArray())
            If (literatures.Count > 0) Then
                result = literatures.FirstOrDefault().Name
            Else
                result = ""
            End If
        End If
        GetLiteratureNameonID = result
    End Function

    Shared Function GetProductInterestonID(ByVal prodinterestid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             October 23,2009
        ' Description:      gets product interest on ID
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If prodinterestid > 0 Then
            Dim strSQL As String = "SELECT NAME FROM   PRODUCT_INTEREST WHERE KEY_ID = @KEYID"
            Dim productInterests As List(Of ProductInterest) = ProductInterestRepository.FetchProductInterests(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@KEYID", prodinterestid.ToString())}).ToArray())
            If (productInterests.Count > 0) Then
                result = productInterests.FirstOrDefault().Name
            End If
        End If
        GetProductInterestonID = result
    End Function
    Shared Function GetTacticIdOnCampaignID(ByVal campaignid As String) As String
        ' Author:           Kate Kaplan
        ' Date:             October 23,2009
        ' Description:      gets TACTIC_ID by CampaignID
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If Not String.IsNullOrEmpty(campaignid) Then
            If IsNumeric(campaignid) Then
                Dim strSQL As String = "Select TACTIC_ID from SUBSCRIPTION_CAMPAIGN where CAMPAIGN_ID=@CAMPAIGNID"
                Dim subscriptionCampaigns As List(Of SubscriptionCampaign) = SubscriptionCampaignRepository.FetchSubscriptionCampaigns(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)({New SqlParameter("@CAMPAIGNID", campaignid.ToString())}).ToArray())
                If (subscriptionCampaigns.Count > 0) Then
                    result = subscriptionCampaigns.FirstOrDefault().TacticId
                End If
            End If
        End If
        GetTacticIdOnCampaignID = result
    End Function

    ' NOTE: Replicated to BLL.Utilities
    Shared Function checksubscriptionexist(ByVal strDocumentID As String, ByVal strEmail As String) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             October 30,2009
        ' Description:      Checks if subscription exists by DOCUMENT_ID and EMAIL
        ' Returns:          Boolean
        ' Revisions:        1
        checksubscriptionexist = False
        If Len(strDocumentID) > 0 And Len(strEmail) > 0 Then
            Dim strSQL As String = "SELECT DISTINCT * FROM SUBSCRIPTION WHERE ENABLE='y' and EMAIL=@EMAIL and DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@EMAIL", strEmail))
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", strDocumentID))
            Dim subscriptions As List(Of Library.Domain.Common.DTOs.Subscription) = SubscriptionRepository.FetchSubscriptions(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (subscriptions.Count > 0) Then
                checksubscriptionexist = True
            End If
        End If

    End Function

    ' NOTE: Replicated to BLL.Utilities
    Shared Function GenerateUserID() As String
        ' Author:           Kate Kaplan
        ' Date:             October 30,2009
        ' Description:      Generates random string for SUB_ID in SUBSCRIPTION
        ' Returns:          String
        ' Revisions:        1

        Dim bl As Boolean = False
        Dim strUID1 As String = ""
        Dim strUIDTemp1 As String = ""
        Dim strUID2 As String = ""
        Dim strUIDTemp2 As String = ""

        Do While Not bl
            Do Until Len(strUID1) > 9 ' number of the strlen -1
                Randomize()
                ' Get a character
                strUIDTemp1 = Chr(Int(44 * Rnd()) + 48)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strUIDTemp1) < 58 Or (Asc(strUIDTemp1) > 64 And Asc(strUIDTemp1) < 91) Then
                    strUID1 += strUIDTemp1
                End If
            Loop

            Do Until Len(strUID2) > 9 ' number of the strlen -1
                Randomize()
                ' Get a character
                strUIDTemp2 = Chr(Int(44 * Rnd()) + 48)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strUIDTemp2) < 58 Or (Asc(strUIDTemp2) > 64 And Asc(strUIDTemp2) < 91) Then
                    strUID2 += strUIDTemp2
                End If
            Loop

            If CheckUserIDNotExist(strUID1, strUID2) Then
                bl = True
            Else
                strUID1 = ""
                strUID2 = ""
                bl = False
            End If
        Loop
        GenerateUserID = strUID1 & strUID2
    End Function

    ' NOTE: Replicated to BLL.Utilities
    Shared Function CheckUserIDNotExist(ByVal str1 As String, ByVal str2 As String) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             October 30,2009
        ' Description:     Checks if Generated parts of random string already exist in SUBSCRIPTION
        ' Returns:          String
        ' Revisions:        1
        Dim strSQL As String = ""
        CheckUserIDNotExist = True
        If Len(str1) > 0 And Len(str2) > 0 Then
            strSQL = "SELECT * FROM SUBSCRIPTION WHERE SUBSTRING(SUB_ID,1,10)=@USERID1 and SUBSTRING(SUB_ID,11,10)=@USERID2"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@USERID1", str1))
            sqlParameters.Add(New SqlParameter("@USERID2", str2))
            Dim subscriptions As List(Of Library.Domain.Common.DTOs.Subscription) = SubscriptionRepository.FetchSubscriptions(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (subscriptions.Count > 0) Then
                CheckUserIDNotExist = False
            End If
        End If
    End Function

    Shared Function GetPDFFilePath(ByVal documentid As String) As String
        ' Author:           Kate Kaplan
        ' Date:             November 12,2009
        ' Description:     Checks if Generated parts of random string already exist in SUBSCRIPTION
        ' Returns:          String
        ' Revisions:        1

        Dim strSQL As String
        GetPDFFilePath = ""
        If Len(documentid) > 0 Then
            If IsNumeric(documentid) Then
                strSQL = "select * from DOCUMENT where FILE_FULL_PATH is not null and DOCUMENT_ID=@DOCUMENTID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid))
                Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If (documents.Count > 0) Then
                    GetPDFFilePath = documents.FirstOrDefault().FileFullPath
                End If
            End If
        End If

    End Function

    Shared Function GetDownloadSubCategory(ByVal documentid As String) As String
        ' Author:           Kate Kaplan
        ' Date:             November 23,2009
        ' Description:      Gets subcat_id for Download document
        ' Returns:          String
        ' Revisions:        1
        Dim strSQL As String
        GetDownloadSubCategory = ""
        If Len(documentid) > 0 Then
            strSQL = "SELECT DISTINCT SUBCATEGORY.SUBCAT_ID, SUBCATEGORY.SUBCAT_NAME, SUBCATEGORY.SORT_ID " +
                " FROM PROTOCOL_STANDARD INNER JOIN  DOCUMENT INNER JOIN DOCUMENT_CATEGORY " +
                " ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID " +
                " ON PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID " +
                " INNER JOIN PROTOCOL_STANDARD_SERIES INNER JOIN PRODUCT_SERIES " +
                " ON PROTOCOL_STANDARD_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID ON " +
                " PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = PROTOCOL_STANDARD_SERIES.PROTOCOL_STANDARD_ID " +
                " INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID " +
                " WHERE     (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST <> 'n') AND (PROTOCOL_STANDARD.DISABLED = 'n') " +
                " AND (PRODUCT_SERIES.DISABLED = 'n') and document.document_id=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid))
            Dim subcategories As List(Of Subcategory) = SubcategoryRepository.FetchSubcategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (subcategories.Count > 0) Then
                GetDownloadSubCategory = subcategories.FirstOrDefault().SubCatId.ToString()
            End If
        End If
    End Function

    Shared Function GetSalesEmail(ByVal countryName As String, ByVal documentid As String, ByVal strState As String, ByVal contactid As String, ByVal categoryId As Int32, ByVal requestTypeId As Int32) As String
        ' Author:           Kate Kaplan
        ' Rev1 Date:        November 23,2009
        ' Rev2 Date:        March 18, 2011 (added US SE)
        ' Description:      Gets web download distribution list by Country or by SE for scopes downloads in United States
        ' Returns:          String
        ' Revisions:        2

        If Len(countryName) > 0 And Len(documentid) > 0 Then
            If Len(GetDownloadSubCategory(documentid).ToString) > 0 Then
                If countryName <> "United States" And countryName <> "Canada" Then
                    Return GetSalesRepEmail(countryName, requestTypeId, categoryId)
                End If
                If countryName = "Canada" Then
                    Dim strSQL As String = " SELECT SALES_REP_PSG.EMAIL FROM SALES_REP_PSG" +
                      " INNER JOIN  SALES_PSG ON SALES_REP_PSG.SALES_REP_ID = SALES_PSG.SALES_REP_PSG" +
                      " WHERE SALES_PSG.SUBCAT_ID = @SUBCATID AND SALES_PSG.COUNTRY_ID = 35"
                    Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SUBCATID", GetDownloadSubCategory(documentid).ToString()))
                    Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    Dim email As String = String.Empty
                    If (ds.Tables(0).Rows.Count > 0) Then
                        email = ds.Tables(0).Rows(0)("EMAIL")
                    End If
                    Return email
                Else
                    Dim ccmsEmail As String = GetCCMS_Email("SP", GetSPNumber(contactid))
                    If Not (String.IsNullOrEmpty(ccmsEmail)) Then
                        Return ccmsEmail
                    End If
                    Return GetSalesRepEmail(countryName, requestTypeId, categoryId)
                End If
            Else
                If countryName <> "United States" Then
                    Return GetSalesRepEmail(countryName, requestTypeId, categoryId)
                End If
                If Len(GetCCMS_Email("SE", GetSENumber(contactid))) > 0 Then
                    Return GetCCMS_Email("SE", GetSENumber(contactid)) + "," + GetSalesRepEmail(countryName, requestTypeId, categoryId)
                End If
                Return GetSalesRepEmail(countryName, requestTypeId, categoryId)
            End If
        End If
        Return String.Empty
    End Function
    Shared Function CheckSeriesExists(ByVal SeriesID As String, ByVal categoryID As String) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             November 24,2009
        ' Description:      Checks if  series exists and has live products assigned
        ' Returns:          Boolean
        ' Revisions:        1
        CheckSeriesExists = False
        Dim strSQL As String
        If Len(SeriesID) > 0 And Len(categoryID) > 0 Then
            If InStr(SeriesID, ",") Then
                SeriesID = Mid(SeriesID, InStr(SeriesID, ",") + 1)
            Else
                SeriesID = SeriesID
            End If
            If InStr(categoryID, ",") Then
                categoryID = Mid(categoryID, InStr(categoryID, ",") + 1)
            Else
                categoryID = categoryID
            End If
            strSQL = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID=@PRODUCTSERIESID and DISABLED='n' and VALIDATE_YN='n'"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", SeriesID))
            Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productSeries.Count > 0) Then
                CheckSeriesExists = True
            Else
                If categoryID = 1 Then
                    strSQL = "SELECT * FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCTID " +
                            " INNER JOIN PRODUCT_SERIES c ON a.PRODUCT_SERIES_ID=c.PRODUCT_SERIES_ID WHERE c.DISABLED='n' and " +
                            " a.CATEGORY_ID=@CATEGORYID and a.PRODUCT_SERIES_ID=@PRODUCTSERIESID and b.DISABLED='n'"
                Else
                    strSQL = "SELECT * FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCTID " +
                            " INNER JOIN PRODUCT_SERIES c ON a.PRODUCT_SERIES_ID=c.PRODUCT_SERIES_ID WHERE c.DISABLED='n' and " +
                            " a.CATEGORY_ID=@CATEGORYID and a.PRODUCT_SERIES_ID=@PRODUCTSERIESID"
                End If
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryID))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", SeriesID))
                Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If (productSeriesCategories.Count > 0) Then
                    CheckSeriesExists = True
                End If
            End If
        End If
    End Function
    Shared Function GetCategoryIDonSeriesID(ByVal seriesid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             December 2,2009
        ' Description:      gets Category_id on product_Series_id
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If seriesid > 0 Then
            Dim strSQL As String = "SELECT DISTINCT CATEGORY_ID FROM PRODUCT_SERIES_CATEGORY WHERE PRODUCT_SERIES_ID = @PRODUCTSERIESID AND [CATEGORY_ID] NOT IN (20,21,26)"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid.ToString()))
            Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productSeriesCategories.Count > 0) Then
                result = productSeriesCategories.FirstOrDefault().CategoryId.ToString()
            Else
                strSQL = "SELECT DISTINCT [PRODUCT_SERIES_ID] FROM [PROTOCOL_STANDARD_SERIES] WHERE [PRODUCT_SERIES_ID] = @PRODUCTSERIESID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid.ToString()))
                Dim protocolStandardSeries As List(Of ProtocolStandardSeries) = ProtocolStandardSeriesRepository.FetchProtocolStandardSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If (protocolStandardSeries.Count > 0) Then
                    result = AppConstants.CAT_ID_PROTOCOL
                Else
                    result = String.Empty
                End If
            End If
        End If
        GetCategoryIDonSeriesID = result
    End Function

    Shared Function GetGroupIDonProductID(ByVal productid As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             December 18,2009
        ' Description:      gets group_id on productid
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If productid > 0 Then
            Dim strSQL As String = "SELECT GROUP_ID FROM PRODUCT WHERE PRODUCTID = @PRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
            Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (products.Count > 0) Then
                result = products.FirstOrDefault().GroupId.ToString()
            End If
        End If
        GetGroupIDonProductID = result
    End Function


    Public Shared Function GeneratedGUID_ShortReg() As String
        ' Author:           Kate Kaplan
        ' Date:             Janualry 22, 2010
        ' Description:      gets group_id on productid
        ' Returns:          String
        ' Revisions:        1 
        Dim bl As Boolean = False
        Dim strGUIDTemp As String = ""
        Dim strGIUDFinal As String = ""
        Do While Not bl
            Do Until Len(strGIUDFinal) > 14 ' number of the strlen -1
                Randomize()
                ' Get a character
                strGUIDTemp = Chr(Int(44 * Rnd()) + 48)

                ' Check if character is in the 0-9 and A-Z range
                If Asc(strGUIDTemp) < 58 Or (Asc(strGUIDTemp) > 64 And Asc(strGUIDTemp) < 91) Then
                    strGIUDFinal = strGIUDFinal & strGUIDTemp
                End If
            Loop
            If UCase(CheckWebIDNotExistance_ShortReg(strGIUDFinal)) = "TRUE" Then
                bl = True
            Else
                strGIUDFinal = ""
                bl = False
            End If
        Loop
        GeneratedGUID_ShortReg = strGIUDFinal
    End Function

    Shared Function CheckWebIDNotExistance_ShortReg(ByVal strWebID As String) As String
        ' Author:           Kate Kaplan
        ' Date:             Janualry 22, 2010
        ' Description:      Gets the ContactID based on Contact_web_Id
        ' Returns:          String "TRUE" if not found, or
        '                   String "FALSE" if found

        Dim sql As String = "SELECT CONTACT_ID FROM  SHORT_REGISTRATION WHERE UPPER(SHORTREG_WEB_ID) = @WEBID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@WEBID", Trim(UCase(strWebID))))
        Dim ds As DataSet = New DataSet()
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            CheckWebIDNotExistance_ShortReg = "FALSE"
        Else
            CheckWebIDNotExistance_ShortReg = "TRUE"
        End If
    End Function

    Shared Function GetDocumentTypeIdOnDocumentID(ByVal documentid As Integer) As String
        Dim strSQL As String
        GetDocumentTypeIdOnDocumentID = ""
        If Len(documentid) > 0 Then
            strSQL = " Select DOC_TYPE_ID  FROM DOCUMENT WHERE DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid.ToString()))
            Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (documents.Count > 0) Then
                GetDocumentTypeIdOnDocumentID = documents.FirstOrDefault().DocTypeId.ToString()
            End If
        End If
    End Function

    Shared Function SeriesHasRequestQuoteLInk(ByVal mseries As Integer) As String
        ' Author:           Kate Kaplan
        ' Date:             February 18, 2010
        ' Description:      checks for estore enabled products (protocol analyzers)
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        If mseries > 0 Then
            Dim strSQL As String = "SELECT * FROM  PRODUCT_SERIES_CATEGORY b INNER JOIN PRODUCT c ON b.PRODUCT_ID = c.PRODUCTID WHERE b.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND  c.disabled = 'n' "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries.ToString()))
            Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (productSeriesCategories.Count > 0) Then
                result = "<li class=""link""><a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid=" + mseries.ToString + """>Request Quote</a></li>"
            End If
        End If
        SeriesHasRequestQuoteLInk = result
    End Function

    Public Shared Sub SendEmailHTML(ByVal strCountry As String, ByVal strBody As String, ByVal strTo As String, ByVal strFrom As String, ByVal strCC As String, ByVal strDisplayName As String, ByVal strSubject As String, ByVal AttachmentFile As String)
        If Len(strBody) > 0 And Len(strTo) > 0 Then
            Dim mm As New CDO.Message
            mm.To = strTo
            'send CC email
            If Len(Trim(strCC)) > 0 Then
                mm.CC = strCC
            End If
            If Len(strFrom) > 0 Then
                mm.From = strDisplayName & "<" & strFrom & ">"
                mm.ReplyTo = strFrom
            Else
                mm.From = strDisplayName & "<postmaster@teledynelecroy.com>"
            End If
            mm.BCC = "kate.kaplan@teledynelecroy.com"
            mm.HTMLBody = strBody

            mm.Subject = strSubject
            If Len(strCountry) > 0 Then
                If CStr(strCountry) = "Japan" Then
                    mm.HTMLBodyPart.Charset = "shift-JIS"
                ElseIf CStr(strCountry) = "China" Then
                    mm.HTMLBodyPart.Charset = "GB2312"
                ElseIf Mid(CStr(strCountry), 1, 5) = "Korea" Then
                    mm.HTMLBodyPart.Charset = "korean"
                End If
            End If
            Try
                mm.Send()
            Catch ex As Exception
                Functions.ErrorHandlerLog(True, ex.Message + " " + ex.Source)
            End Try
        End If
    End Sub
    Shared Function GetProtocolStandardNameOnID(ByVal standardid As Integer) As String
        Dim strSQL As String
        GetProtocolStandardNameOnID = ""
        If Len(standardid) > 0 Then
            strSQL = " Select NAME  FROM PROTOCOL_STANDARD WHERE PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", standardid.ToString()))
            Dim protocolStandards As List(Of ProtocolStandard) = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (protocolStandards.Count > 0) Then
                GetProtocolStandardNameOnID = protocolStandards.FirstOrDefault().Name
            End If
        End If
    End Function
    Shared Function CheckDownloadAgreement(ByVal documentid As String) As Boolean
        Dim strSQL As String = ""
        CheckDownloadAgreement = False
        If Len(documentid) > 0 Then
            strSQL = " Select *  FROM DOCUMENT  WHERE POST='y' and AGREEMENT_YN='y' and DOCUMENT_ID=@DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentid))
            Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (documents.Count > 0) Then
                CheckDownloadAgreement = True
            End If
        End If
    End Function
    Public Shared Function GetProductListPriceonProductID(ByVal ProdID As Integer) As Decimal
        '*************************************
        'Product List Pricing
        'Rev 1- KK - 8/3/2010
        'Arguments - prodid
        'Returns string
        '*************************************
        Dim result As String = ""
        Dim ProductPrice As Integer = 0
        Dim TotalPrice As Integer = 0
        Dim strSQL As String
        If Len(ProdID) > 0 Then
            strSQL = "SELECT LIST_PRICE AS PRICE FROM BAANPRICE  WHERE [PRODUCT ID] = @PRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", ProdID.ToString()))
            Dim ProdPrice As Object
            ProdPrice = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If Not ProdPrice Is Nothing Then
                If IsNumeric(ProdPrice) Then
                    If CSng(ProdPrice) > 0 Then
                        ProductPrice = CSng(ProdPrice)
                    Else
                        ProductPrice = 0
                    End If
                Else
                    ProductPrice = 0
                End If

            Else
                ProductPrice = 0
            End If
            '	end if
        Else
            ProductPrice = 0
        End If
        GetProductListPriceonProductID = ProductPrice
    End Function
    Shared Function CheckGroupExists(ByVal category_id As Integer, ByVal group_id As Integer) As Boolean
        ' Author:           Kate Kaplan
        ' Date:             October 13, 2010
        ' Description:      checks if group exists
        ' Returns:          String
        ' Revisions:        1
        Dim result As String = ""
        CheckGroupExists = False
        If category_id > 0 And group_id > 0 Then
            Dim strSQL As String = " SELECT * FROM PRODUCT INNER JOIN PRODUCT_GROUP ON PRODUCT.GROUP_ID = PRODUCT_GROUP.GROUP_ID WHERE PRODUCT_GROUP.CATEGORY_ID = @CATEGORYID And PRODUCT.GROUP_ID = @GROUPID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", category_id.ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", group_id.ToString()))
            Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (products.Count > 0) Then
                CheckGroupExists = True
            End If
        End If
    End Function

    Public Shared Function GetCCMS_Email(ByVal RecType As String, ByVal ID As String) As String
        ' Author:           Kate Kaplan
        ' Date:             March 18, 2011
        ' Description:      gets email from CCMS_PERSONNEL_DATA
        ' Returns:          String
        ' Revisions:        1 
        Dim results As String = ""
        If Len(RecType) > 0 And Len(ID) > 0 Then
            If IsNumeric(ID) Then
                Dim strSQL As String = ""
                strSQL = "Select EMAIL, EMAILCC from CCMS_PERSONNEL_DATA where RecType=@RECTYPE and ID=@ID and ACTIVE='Y'"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@RECTYPE", RecType))
                sqlParameters.Add(New SqlParameter("@ID", ID))
                Dim ccmsPersonnelData As List(Of CCMSPersonnelData) = CCMSPersonnelDataRepository.FetchCCMSPersonnelData(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If (ccmsPersonnelData.Count > 0) Then
                    If Not String.IsNullOrEmpty(ccmsPersonnelData.FirstOrDefault().Email) Then
                        results = Trim(ccmsPersonnelData.FirstOrDefault().Email)
                    End If
                    If Not String.IsNullOrEmpty(ccmsPersonnelData.FirstOrDefault().EmailCC) Then
                        If Len(results) = 0 Then
                            results = Trim(ccmsPersonnelData.FirstOrDefault().EmailCC)
                        Else
                            results = results & "," & Trim(ccmsPersonnelData.FirstOrDefault().EmailCC)
                        End If
                    End If
                End If
            End If
        End If
        GetCCMS_Email = results.ToString
    End Function

    Public Shared Function IsDocumentPasswordProtected(ByVal documentId As Int32) As Boolean
        If (documentId <= 0) Then
            Return False
        End If

        Dim sqlString As String = "SELECT PASSWORD_YN FROM DOCUMENT WHERE DOCUMENT_ID = @DOCUMENTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCUMENTID", documentId.ToString()))
        Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (documents.Count > 0) Then
            If (String.Compare(documents.FirstOrDefault().PasswordYN, "y", True) = 0) Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Shared Function IsValidInteger(ByVal inValue As String) As Boolean
        Dim retVal As Boolean = False
        Try
            Dim result As Int32 = 0
            retVal = Int32.TryParse(inValue, result)
        Catch
        End Try
        Return retVal
    End Function

    Public Shared Function InsertSubscription(ByVal subscriptionDocumentId As Int32, ByVal subscriptionCountryId As Int32, ByVal contactId As String, ByVal emailAddress As String) As Boolean
        Dim retVal As Boolean = False
        If (String.IsNullOrEmpty(emailAddress)) Then
            Return retVal
        End If

        Dim subscriptionId As String = GenerateUserID().ToString()
        If Not (Functions.checksubscriptionexist(subscriptionDocumentId.ToString(), emailAddress)) Then
            Dim sqlString As String = "INSERT INTO [SUBSCRIPTION] ([SUB_ID], [DATE_ENTERED], [EMAIL], [DOCUMENT_ID], [CONTACT_ID], [ENABLE], [COUNTRY_ID], [DOCUMENT_READ], [FORMAT_ID], [CONFIRM_YN]) " &
                                      "VALUES (@SUBID, @DATEENTERED, @EMAILADDRESS, @DOCUMENTID, @CONTACTID, 'n', @COUNTRYID, 'n', 2, 'n')"
            Dim returnedValue As Int32 = DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString,
                                                New SqlParameter("@SUBID", subscriptionId),
                                                New SqlParameter("@DATEENTERED", DateTime.Now.ToString()),
                                                New SqlParameter("@EMAILADDRESS", emailAddress),
                                                New SqlParameter("@DOCUMENTID", subscriptionDocumentId.ToString()),
                                                New SqlParameter("@CONTACTID", IIf(Not (String.IsNullOrEmpty(contactId)), contactId, "0")),
                                                New SqlParameter("@COUNTRYID", subscriptionCountryId.ToString()))
            If (returnedValue > 0) Then
                retVal = True
            End If
        End If
        Return retVal
    End Function

    Public Shared Sub SendRequestToGoogleAnalytics(ByVal urlEncodedPageTitle As String, ByVal currentPage As String, ByVal urlReferrer As Uri, ByVal userAgent As String, ByVal hostName As String, ByVal language As String, ByVal urchinData As String())
        Dim sb As StringBuilder = New StringBuilder()
        Dim referrer As String = "-"
        If Not (urlReferrer = Nothing) Then
            referrer = urlReferrer.ToString()
        End If
        Dim randomCookie As Int32 = GetRandomNumber(3, 10000000, 99999999)
        Dim baseTime As Long = ToUnixEpochTime(DateTime.Now)
        Dim cookie As String = String.Format("__utma%3D{0}.{1}.{2}.{3}.{4}.3%3B%2B__utmz%3D{0}.{2}.1.1.utmcsr%3D{6}%7Cutmccn%3D{5}%7Cutmcmd%3D{7}{8}%3B",
                                        randomCookie, GetRandomNumber(3, 1000000000, Int32.MaxValue), baseTime, ToUnixEpochTime(DateTime.Now), ToUnixEpochTime(DateTime.Now), IIf(String.IsNullOrEmpty(urchinData(0)), "(direct)", urchinData(0)), IIf(String.IsNullOrEmpty(urchinData(1)), "(direct)", urchinData(1)), IIf(String.IsNullOrEmpty(urchinData(2)), "(none)", urchinData(2)), IIf(String.IsNullOrEmpty(urchinData(3)), String.Empty, String.Format("%7Cutmctr%3D{0}", urchinData(3))))
        sb.Append(String.Format("{0}?utmwv={1}&utmn={2}&utmhn={9}&utmdt={3}&utmhid={4}&utmr={5}&utmp={6}&utmac={7}&utmcc={8}&utmcs={10}",
                                 ConfigurationManager.AppSettings("GoogleAnalyticsBaseUrl"), ConfigurationManager.AppSettings("GoogleAnalyticsVersionId"),
                                 GetRandomNumber(1, 0, Int32.MaxValue), urlEncodedPageTitle, GetRandomNumber(9, 0, Int32.MaxValue), referrer,
                                 currentPage.Replace("~", String.Empty), ConfigurationManager.AppSettings("GoogleAnalyticsAccountId"), cookie, hostName, language))
        Try
            Dim webRequest As System.Net.WebRequest = System.Net.WebRequest.Create(sb.ToString())
            CType(webRequest, System.Net.HttpWebRequest).UserAgent = userAgent
            Using webResponse As System.Net.WebResponse = webRequest.GetResponse()
            End Using
        Catch
        End Try
    End Sub

    Public Shared Function GetDnsName(ByVal ipAddress As String) As String
        Try
            Dim ipHost As System.Net.IPHostEntry = System.Net.Dns.GetHostByAddress(ipAddress)
            Return ipHost.HostName.ToString()
        Catch
        End Try
        Return String.Empty
    End Function

    Private Shared Function ToUnixEpochTime(ByVal convertThisDate As DateTime) As Long
        Dim unixEpoch As DateTime = New DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
        Return Convert.ToInt64((convertThisDate - unixEpoch).TotalSeconds)
    End Function

    Private Shared Function GetRandomNumber(ByVal seed As Int32, ByVal minSeed As Int32, ByVal maxSeed As Int32) As Int32
        Dim random As Random = New Random(seed) ' Without using a lock (syncLock); require a random arbitrary seed integer to truly randomize
        Return random.Next(minSeed, maxSeed)
    End Function

    Public Shared Function GetSPNumber(ByVal contactId As String) As String
        Return GetSPNumber(contactId, False)
    End Function

    ''' <summary>
    ''' Gets SPNumber on ContactID for US customer (Ported from Shopper/functions.asp)
    ''' </summary>
    ''' <param name="contactId"></param>
    ''' <returns>String</returns>
    ''' <remarks>rev1 - Kate Kaplan 12/15/2011</remarks>
    Public Shared Function GetSPNumber(ByVal contactId As String, ByVal retrieveSecondarySpCode As Boolean) As String
        Dim spOrSecondarySpCode As String = "SPCode"
        If (String.IsNullOrEmpty(contactId)) Then
            Return String.Empty
        End If

        'Check if the customer company is in the company exceptions list; Select company name from contact table
        Dim sqlString As String = "Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL,  SUBSTRING(LTRIM(RTRIM(CONTACT.EMAIL)), " &
                                  "PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 1, { fn LENGTH(LTRIM(RTRIM(CONTACT.EMAIL))) } - PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 2)  AS DOMAIN  " &
                                  "from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=@CONTACTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACTID", contactId))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count <= 0 Or ds.Tables(0).Rows.Count <= 0) Then
            Return GetSpNumberFromCCMSMapUs(contactId)
        End If

        Dim company As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("Company").ToString())), ds.Tables(0).Rows(0)("Company").ToString().Trim().Replace("'", String.Empty), String.Empty)
        Dim state As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("State").ToString())), ds.Tables(0).Rows(0)("State").ToString().Trim(), String.Empty)
        Dim city As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("City").ToString())), ds.Tables(0).Rows(0)("City").ToString().Trim().Replace("'", String.Empty), String.Empty)
        Dim domain As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("Domain").ToString())), ds.Tables(0).Rows(0)("Domain").ToString().Trim().Replace("'", String.Empty), String.Empty)

        'If the company is in exceptions list; We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES
        sqlString = "SELECT a.SPCode, a.SecondarySpCode FROM CCMS_PERSONNEL_DATA AS b INNER JOIN CCMS_EXCEPTION_COMPANIES AS a ON b.ID = a.SPCode  where a.STATE=@STATE and a.CompanyName = @COMPANYNAME and " &
                    "a.CITY=@CITY AND b.RecType = 'sp' and b.Active = 'y'"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@STATE", state))
        sqlParameters.Add(New SqlParameter("@COMPANYNAME", company))
        sqlParameters.Add(New SqlParameter("@CITY", city))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count <= 0 Or ds.Tables(0).Rows.Count <= 0) Then
            Dim exceptionDomains As List(Of CCMSExceptionDomain) = BLL.FeCcmsManager.GetDomainWithPersonnelJoin(ConfigurationManager.AppSettings("ConnectionString").ToString(), domain, state)
            If (exceptionDomains.Count > 0) Then
                If (exceptionDomains.Where(Function(x) Not (String.IsNullOrEmpty(x.City)) AndAlso x.City.ToLower() = city.ToLower()).Count() > 0) Then
                    Return exceptionDomains.Where(Function(x) x.City.ToLower() = city.ToLower()).Select(Function(x) x.SpCode).FirstOrDefault()
                Else
                    Return exceptionDomains.Select(Function(x) x.SpCode).FirstOrDefault()
                End If
            End If
            Return GetSpNumberFromCCMSMapUs(contactId)
        Else
            If (ds.Tables.Count <= 0) Then Return GetSpNumberFromCCMSMapUs(contactId)
            If (ds.Tables(0).Rows.Count <= 0) Then Return GetSpNumberFromCCMSMapUs(contactId)
            Return IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)(spOrSecondarySpCode).ToString())), ds.Tables(0).Rows(0)(spOrSecondarySpCode).ToString().Trim(), GetSpNumberFromCCMSMapUs(contactId))
        End If
    End Function

    Private Shared Function GetSpNumberFromCCMSMapUs(ByVal contactId As String) As String
        Dim retVal As String = String.Empty
        'If customer company is not in the company exceptions list or Get SENumber by Zip, Select ZIP for current customer from table CONTACT
        Dim sqlString As String = "Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID= @CONTACTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CONTACTID", contactId))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count <= 0 Or ds.Tables(0).Rows.Count <= 0) Then
            Return retVal
        End If

        Dim customerEmail As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("EMAIL").ToString())), ds.Tables(0).Rows(0)("EMAIL").ToString().Trim(), String.Empty)
        Dim postalCode As String = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("POSTALCODE").ToString())), ds.Tables(0).Rows(0)("POSTALCODE").ToString().Trim(), String.Empty)
        If (postalCode.Length >= 5) Then
            If (postalCode.Length > 5) Then
                postalCode = postalCode.Substring(0, 5) 'If Zip code have more then 5 digits we cut and use first five to find SPNumber
            End If
            'Select SECode for this ZIP CODE from table CCMS_MAP_US
            Dim tempPostalCode As Int32 = 0
            If Not (Int32.TryParse(postalCode, tempPostalCode)) Then
                HttpContext.Current.Response.Redirect("/support/softwaredownload/psg.aspx")
            End If
            sqlString = "Select SPCode from CCMS_MAP_US where ZipCode= @POSTALCODE"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@POSTALCODE", postalCode))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            If (ds.Tables.Count <= 0 Or ds.Tables(0).Rows.Count <= 0) Then
                Return retVal
            End If
            retVal = IIf(Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("SPCode").ToString())), ds.Tables(0).Rows(0)("SPCode").ToString().Trim(), String.Empty)
        End If
        Return retVal
    End Function

    Shared Function GetCountryIDOnName(ByVal country As String) As Integer
        Dim strSQL As String
        GetCountryIDOnName = 208
        If Len(country) > 0 Then
            strSQL = " Select COUNTRY_ID  FROM COUNTRY WHERE LOWER(NAME)=@NAME"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@NAME", country.ToString().ToLower()))
            Dim countries As List(Of Country) = CountryRepository.FetchCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (countries.Count > 0) Then
                GetCountryIDOnName = countries.FirstOrDefault().CountryId
            End If
        End If
    End Function

    Public Shared Function ConvertLocaleIdToCountry(ByVal localeId As Int32) As String
        Select Case localeId
            Case 1031
                Return "germany/"
            Case 1033
                Return String.Empty
            Case 1034
                Return "spain/"
            Case 1036
                Return "france/"
            Case 1040
                Return "it/"
            Case 1041
                Return "japan/"
            Case 1042
                Return "korea/"
            Case 2057
                Return "europe/"
            Case 2070
                Return "portugal/"
        End Select
        Return String.Empty
    End Function


    Public Shared Function GetContactidOnQuoteId(ByVal QuoteID As Long) As Long
        ' Author:           Kate Kaplan
        ' Date:             8/16/2013
        ' Description:      GetContactidOnQuoteId
        ' Returns:          Long
        ' Revisions:        0
        Dim strSQL As String = ""
        Dim ContactID As Long = 0
        If QuoteID > 0 Then
            strSQL = "SELECT CONTACTID from QUOTE where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            Dim quotes As List(Of Quote) = QuoteRepository.FetchQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (quotes.Count > 0) Then
                ContactID = quotes.FirstOrDefault().ContactId
            End If
        End If
        GetContactidOnQuoteId = ContactID
    End Function

    Public Shared Function GetQuoteTimeOnQuoteID(ByVal QuoteID As Long) As Date
        ' Author:           Kate Kaplan
        ' Date:             8/16/2013
        ' Description:      GetQuoteTimeOnQuoteID
        ' Returns:          date
        ' Revisions:        0
        Dim strSQL As String = ""
        Dim strResult As Date

        If QuoteID > 0 Then
            strSQL = "SELECT QUOTE_TIME from QUOTE where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            Dim quotes As List(Of Quote) = QuoteRepository.FetchQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (quotes.Count > 0) Then
                If (quotes.FirstOrDefault().QuoteTime.HasValue) Then
                    strResult = quotes.FirstOrDefault().QuoteTime.Value.ToShortDateString()
                End If
            End If
        End If
        GetQuoteTimeOnQuoteID = strResult
    End Function

    Public Shared Function DoesQuoteHaveActivePromotions(ByVal quoteId As Long) As Boolean
        Dim retVal As Boolean = False
        If (quoteId > 0) Then
            Dim sqlString As String = "SELECT sq.* FROM [SHOPPERQUOTE] sq INNER JOIN [PromotionXref] px ON sq.MASTERPRODUCTID = px.MasterProductFkId AND sq.PRODUCTID = px.ProductFkId WHERE px.IsActive = 1 AND px.StartDate <= GETDATE() AND px.EndDate > GETDATE() AND sq.QUOTEID = @QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId.ToString()))
            If (ShopperQuoteRepository.FetchShopperQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).Count > 0) Then
                Return True
            End If
            sqlString = "Select * FROM [SHOPPERQUOTE] INNER JOIN [BAANPRICE] On SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID] WHERE BAANPRICE.PROMO_YN = 'y' AND SHOPPERQUOTE.QUOTEID = @QUOTEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId.ToString()))
            retVal = ShopperQuoteRepository.FetchShopperQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).Count > 0
        End If
        Return retVal
    End Function

    Public Shared Function GetPromotionExpirationDate(ByVal quoteId As Long) As DateTime
        If (quoteId <= 0) Then Return DateTime.MinValue
        Dim sqlString As String = "SELECT px.* FROM [SHOPPERQUOTE] sq INNER JOIN [PromotionXref] px ON sq.MASTERPRODUCTID = px.MasterProductFkId AND sq.PRODUCTID = px.ProductFkId WHERE px.IsActive = 1 AND px.StartDate <= GETDATE() AND px.EndDate > GETDATE() AND sq.QUOTEID = @QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId.ToString()))
        Dim promotionXrefs As List(Of PromotionXRef) = PromotionXRefRepository.FetchPromotionXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
        If (promotionXrefs.Count > 0) Then
            Dim minDate As DateTime = promotionXrefs.Min(Function(x) x.EndDate.Value)
            If (DateDiff("d", GetQuoteTimeOnQuoteID(quoteId), minDate) < 30) Then
                Return minDate
            Else
                Return GetQuoteTimeOnQuoteID(quoteId).AddDays(30)
            End If
        Else
            sqlString = "SELECT MIN(BAANPRICE.PROMO_END_DATE) AS PROMO_DATE FROM [SHOPPERQUOTE] INNER JOIN [BAANPRICE] ON SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID] WHERE BAANPRICE.PROMO_YN = 'y' AND SHOPPERQUOTE.QUOTEID = @QUOTEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId.ToString()))
            Dim baanPrice As BaanPrice = BaanPriceRepository.FetchBaanPrices(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).FirstOrDefault()
            If (baanPrice Is Nothing) Then Return GetQuoteTimeOnQuoteID(quoteId).AddDays(30)

            If Not (baanPrice.PromoEndDate.HasValue) Then Return DateTime.MinValue
            If (DateDiff("d", GetQuoteTimeOnQuoteID(quoteId), baanPrice.PromoEndDate.Value) < 30) Then
                Return baanPrice.PromoEndDate.Value
            Else
                Return GetQuoteTimeOnQuoteID(quoteId).AddDays(30)
            End If
        End If
    End Function

    Public Shared Function GetQuoteBody(ByVal QuoteID As Long, ByVal flgdisplayPrice As Boolean, ByVal localeid As String) As String
        GetQuoteBody = ""
        Dim strCret As String = "<br />"
        If QuoteID > 0 Then
            Dim content As StringBuilder = New StringBuilder()
            Dim TotalPrice As Decimal = 0
            Dim SubTotal As Decimal = 0
            Dim Total As Decimal = 0
            Dim savingsTotal As Decimal = 0
            Dim strSQL As String = ""
            Dim count As Long = 0
            Dim dq, ds, dss, dsp, dca As DataSet
            content.Append("<table border=1 cellpadding=5 cellspacing= 0>")
            content.Append("<tr>")
            content.Append("<td width=145 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Product</font></strong></td>")
            content.Append("<td width=276 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Description</font></strong></td>")
            content.Append("<td width=60 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>QTY</font></strong></td>")
            If flgdisplayPrice Then
                content.Append("<td width=72 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Price</font></strong></td>")
                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Total</font></strong></td>")
            End If
            content.Append("</tr>")

            Dim quoteDate As DateTime = QuoteRepository.GetQuote(ConfigurationManager.AppSettings("ConnectionString"), QuoteID).QuoteTime
            '****get scopes
            strSQL = " Select distinct sq.PRODUCTID,p.partnumber,p.PRODUCTDESCRIPTION from SHOPPERQUOTE as sq " +
                     " INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID " +
                     " INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID " +
                     " where sq.QUOTEID=@QUOTEID and sq.PRODUCTID=sq.MASTERPRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            dq = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dq.Tables(0).Rows.Count > 0 Then
                For Each dqr As DataRow In dq.Tables(0).Rows
                    SubTotal = 0
                    savingsTotal = 0
                    content.Append("<tr>")
                    If flgdisplayPrice Then
                        content.Append("<td  colspan=5")
                    Else
                        content.Append("<td  colspan=3")
                    End If
                    content.Append(" valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dqr("partnumber").ToString + " system</font></strong></td>")
                    content.Append("</tr>")
                    strSQL = "SELECT DISTINCT pg.GROUP_NAME, pg.SORTID, pg.GROUP_ID FROM SHOPPERQUOTE AS sq " +
                             " INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID  " +
                             " where sq.QUOTEID=@QUOTEID and sq.MASTERPRODUCTID=@MASTERPRODUCTID ORDER BY pg.SORTID "
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", dqr("PRODUCTID").ToString()))
                    ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dsr As DataRow In ds.Tables(0).Rows
                            strSQL = "SELECT DISTINCT pg.GROUP_NAME, pg.SORTID, pg.GROUP_ID,sq.QTY, sq.PRICE, p.PARTNUMBER, p.PRODUCTDESCRIPTION, sq.PRODUCTID,sq.MASTERPRODUCTID " +
                                    " FROM SHOPPERQUOTE AS sq INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID " +
                                     " INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID where sq.QUOTEID=@QUOTEID and sq.group_id=@GROUPID and sq.MASTERPRODUCTID=@MASTERPRODUCTID ORDER BY pg.SORTID "
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                            sqlParameters.Add(New SqlParameter("@GROUPID", dsr("GROUP_ID").ToString()))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", dqr("PRODUCTID").ToString()))
                            dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            If dss.Tables(0).Rows.Count > 0 Then
                                For Each dssr As DataRow In dss.Tables(0).Rows
                                    content.Append("<tr>")
                                    content.Append("<td width=145 valign=top nowrap=nowrap><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dssr("partnumber").ToString + "</font></td>")
                                    content.Append("<td width=276 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dssr("PRODUCTDESCRIPTION").ToString + "</font></td>")
                                    content.Append("<td width=60 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dssr("QTY").ToString + "</font></td>")
                                    If flgdisplayPrice Then
                                        TotalPrice = dssr("PRICE") * dssr("QTY")
                                        SubTotal = SubTotal + TotalPrice
                                        If (Convert.ToInt32(dssr("PRICE").ToString()) <= 0) Then
                                            If (ShouldDisplayCallForPrice(Convert.ToInt32(dssr("MASTERPRODUCTID").ToString()), Convert.ToInt32(dssr("PRODUCTID").ToString()), quoteDate)) Then
                                                content.Append("<td align=center colspan=2 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></td>")
                                            Else
                                                content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", dssr("PRICE")).ToString())
                                                Try
                                                    Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(dssr("PRODUCTID").ToString()))
                                                    If (Convert.ToDecimal(dssr("PRICE").ToString()) <> priceInBaanTable) Then
                                                        content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(dssr("PRICE").ToString())) * Int32.Parse(dssr("QTY").ToString())).ToString("c"))
                                                        savingsTotal += (priceInBaanTable * Int32.Parse(dssr("QTY").ToString()))
                                                    End If
                                                Catch ex As Exception
                                                End Try
                                                content.Append("</font></strong></td>")
                                                content.Append("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", TotalPrice).ToString + "</font></td>")
                                            End If
                                        Else
                                            content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", dssr("PRICE")).ToString())
                                            Try
                                                Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(dssr("PRODUCTID").ToString()))
                                                If (Convert.ToDecimal(dssr("PRICE").ToString()) <> priceInBaanTable) Then
                                                    content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(dssr("PRICE").ToString())) * Int32.Parse(dssr("QTY").ToString())).ToString("c"))
                                                    savingsTotal += (priceInBaanTable * Int32.Parse(dssr("QTY").ToString()))
                                                End If
                                            Catch ex As Exception
                                            End Try
                                            content.Append("</font></strong></td>")
                                            content.Append("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", TotalPrice).ToString + "</font></td>")
                                        End If
                                    End If
                                    content.Append("</tr>")
                                Next
                            End If
                        Next
                        If flgdisplayPrice Then
                            If (savingsTotal > 0) Then
                                content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Price before savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dqr("partnumber").ToString(), (savingsTotal + SubTotal).ToString("c"))
                                content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Your total savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dqr("partnumber").ToString(), (savingsTotal * -1).ToString("c"))
                                content.Append("<tr>")
                                content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dqr("partnumber").ToString + " system:</font></strong></td>")
                                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString + "</font></strong></td>")
                                content.Append("</tr>")
                            Else
                                content.Append("<tr>")
                                content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dqr("partnumber").ToString + " system:</font></strong></td>")
                                If (SubTotal <= 0) Then
                                    content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></strong></td>")
                                Else
                                    content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString + "</font></strong></td>")
                                End If
                                content.Append("</tr>")
                            End If
                        End If
                    End If
                    If flgdisplayPrice Then
                        Total = Total + SubTotal
                    End If
                Next
            End If

            '** Handle other pushed categories here where masterproductid not in this quote as a productid
            strSQL = " Select distinct ca.CATEGORY_ID,ca.CATEGORY_NAME,sq.PRODUCTID,p.partnumber,p.PRODUCTDESCRIPTION,sq.REF_SERIES_ID,sq.PRICE,sq.QTY,sq.MASTERPRODUCTID from SHOPPERQUOTE as sq 
                        INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID 
                        INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID 
                        INNER JOIN CATEGORY ca on ca.CATEGORY_ID=pg.CATEGORY_ID 
                        where sq.QUOTEID=@QUOTEID and (sq.PRODUCTID<>sq.MASTERPRODUCTID and sq.MASTERPRODUCTID <> 0) 
                        and sq.MASTERPRODUCTID not in (Select distinct sq2.MASTERPRODUCTID from SHOPPERQUOTE as sq2
                        INNER JOIN  PRODUCT_GROUP AS pg ON sq2.GROUP_ID = pg.GROUP_ID 
                        INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID 
                        where sq2.QUOTEID=@QUOTEID and sq2.PRODUCTID=sq2.MASTERPRODUCTID)"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            dca = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dca.Tables(0).Rows.Count > 0 Then
                For Each dcr As DataRow In dca.Tables(0).Rows
                    SubTotal = 0
                    savingsTotal = 0
                    content.Append("<tr>")
                    If flgdisplayPrice Then
                        content.Append("<td  colspan=5")
                    Else
                        content.Append("<td  colspan=3")
                    End If
                    content.Append(" valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dcr("CATEGORY_NAME").ToString + "</font></strong></td>")
                    content.Append("</tr>")
                    'strSQL = "SELECT DISTINCT pg.GROUP_NAME, pg.SORTID, pg.GROUP_ID,sq.QTY, sq.PRICE, p.PARTNUMBER, p.PRODUCTDESCRIPTION, sq.PRODUCTID,sq.MASTERPRODUCTID " +
                    '                " FROM SHOPPERQUOTE AS sq INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID " +
                    '                 " INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID where sq.QUOTEID=@QUOTEID and sq.group_id=@GROUPID and sq.MASTERPRODUCTID=@MASTERPRODUCTID ORDER BY pg.SORTID "
                    'sqlParameters = New List(Of SqlParameter)
                    'sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                    'sqlParameters.Add(New SqlParameter("@GROUPID", QuoteID.ToString()))
                    'sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", dcr("CATEGORY_ID").ToString()))
                    'dsp = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    'If dsp.Tables(0).Rows.Count > 0 Then
                    '    For Each drp As DataRow In dsp.Tables(0).Rows
                    content.Append("<tr>")
                    content.Append("<td width=145 valign=top nowrap=nowrap><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dcr("partnumber").ToString + "</font></td>")
                    content.Append("<td width=276 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
                    content.Append(dcr("PRODUCTDESCRIPTION").ToString)
                    'If (dcr("REF_SERIES_ID") > 0 And Int32.Parse(dcr("CATEGORY_ID")) <> 3) Then
                    '    content.Append(strCret + " For " + GetSeriesNameonID(dcr("REF_SERIES_ID")).ToString)
                    'End If
                    content.Append("</font></td>")
                    content.Append("<td width=60 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dcr("QTY").ToString + "</font></td>")
                    If flgdisplayPrice Then
                        TotalPrice = dcr("PRICE") * dcr("QTY")
                        SubTotal = SubTotal + TotalPrice
                        If (Convert.ToDecimal(dcr("PRICE")) <= 0) Then
                            If (ShouldDisplayCallForPrice(Convert.ToInt32(dcr("MASTERPRODUCTID").ToString()), Convert.ToInt32(dcr("PRODUCTID").ToString()), quoteDate)) Then
                                content.Append("<td align=center colspan=2 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></td>")
                            Else
                                content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", dcr("PRICE")).ToString())
                                Try
                                    Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(dcr("PRODUCTID").ToString()))
                                    If (Convert.ToDecimal(dcr("PRICE").ToString()) < priceInBaanTable) Then
                                        content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(dcr("PRICE").ToString())) * Int32.Parse(dcr("QTY").ToString())).ToString("c"))
                                        savingsTotal += (priceInBaanTable * Int32.Parse(dcr("QTY").ToString()))
                                    End If
                                Catch ex As Exception
                                End Try
                                content.Append("</font></strong></td>")
                                Dim buyNowLink As String = String.Empty
                                If (Int32.Parse(dcr("CATEGORY_ID").ToString()) = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES) Then
                                    Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Convert.ToInt32(dcr("PRODUCTID").ToString()))
                                    If Not (product Is Nothing) Then
                                        If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                            buyNowLink = String.Format("<br />{0}", String.Format("<a href=""https://store.teledynelecroy.com{0}"" target=""_blank"">Buy it now</a>", product.ShopifyHandle))
                                        End If
                                    End If
                                End If
                                content.AppendFormat("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>{0}{1}</font></td>", String.Format("{0:c}", TotalPrice).ToString(), buyNowLink)
                            End If
                        Else
                            content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", dcr("PRICE")).ToString())
                            Try
                                Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(dcr("PRODUCTID").ToString()))
                                If (Convert.ToDecimal(dcr("PRICE").ToString()) < priceInBaanTable) Then
                                    content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(dcr("PRICE").ToString())) * Int32.Parse(dcr("QTY").ToString())).ToString("c"))
                                    savingsTotal += (priceInBaanTable * Int32.Parse(dcr("QTY").ToString()))
                                End If
                            Catch ex As Exception
                            End Try
                            content.Append("</font></strong></td>")
                            Dim buyNowLink As String = String.Empty
                            If (Int32.Parse(dcr("CATEGORY_ID").ToString()) = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES) Then
                                Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Convert.ToInt32(dcr("PRODUCTID").ToString()))
                                If Not (product Is Nothing) Then
                                    If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                        buyNowLink = String.Format("<br />{0}", String.Format("<a href=""https://store.teledynelecroy.com{0}"" target=""_blank"">Buy it now</a>", product.ShopifyHandle))
                                    End If
                                End If
                            End If
                            content.AppendFormat("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>{0}{1}</font></td>", String.Format("{0:c}", TotalPrice).ToString(), buyNowLink)
                        End If
                    End If
                    content.Append("</tr>")
                    '    Next
                    'End If
                    If flgdisplayPrice Then
                        If (savingsTotal > 0) Then
                            content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Price before savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dcr("CATEGORY_NAME").ToString(), (savingsTotal + SubTotal).ToString("c"))
                            content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Your total savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dcr("CATEGORY_NAME").ToString(), (savingsTotal * -1).ToString("c"))
                            content.Append("<tr>")
                            content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dcr("CATEGORY_NAME").ToString() + " system:</font></strong></td>")
                            content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString() + "</font></strong></td>")
                            content.Append("</tr>")
                        Else
                            content.Append("<tr>")
                            content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dcr("CATEGORY_NAME").ToString() + " system:</font></strong></td>")
                            If (SubTotal <= 0) Then
                                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></strong></td>")
                            Else
                                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString() + "</font></strong></td>")
                            End If
                            content.Append("</tr>")
                        End If
                    End If
                    If flgdisplayPrice Then
                        Total = Total + SubTotal
                    End If
                Next
            End If


            '****get other categories
            strSQL = "SELECT DISTINCT ca.CATEGORY_ID,ca.CATEGORY_NAME,ca.SORT_ID " +
                    " FROM SHOPPERQUOTE AS sq INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID " +
                     " INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID INNER JOIN CATEGORY ca on ca.CATEGORY_ID=pg.CATEGORY_ID" +
                     " where sq.QUOTEID=@QUOTEID and (sq.MASTERPRODUCTID=0 or sq.MASTERPRODUCTID='') ORDER by ca.SORT_ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            dca = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dca.Tables(0).Rows.Count > 0 Then
                For Each dcr As DataRow In dca.Tables(0).Rows
                    SubTotal = 0
                    savingsTotal = 0
                    content.Append("<tr>")
                    If flgdisplayPrice Then
                        content.Append("<td  colspan=5")
                    Else
                        content.Append("<td  colspan=3")
                    End If
                    content.Append(" valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dcr("CATEGORY_NAME").ToString + "</font></strong></td>")
                    content.Append("</tr>")
                    strSQL = "SELECT DISTINCT pg.GROUP_NAME, pg.SORTID, pg.GROUP_ID,sq.QTY, sq.PRICE, p.PARTNUMBER, p.PRODUCTDESCRIPTION, sq.PRODUCTID,sq.REF_SERIES_ID,sq.MASTERPRODUCTID,ca.CATEGORY_ID  " +
                            " FROM SHOPPERQUOTE AS sq INNER JOIN  PRODUCT_GROUP AS pg ON sq.GROUP_ID = pg.GROUP_ID " +
                             " INNER JOIN PRODUCT AS p ON sq.PRODUCTID = p.PRODUCTID INNER JOIN CATEGORY ca on ca.CATEGORY_ID=pg.CATEGORY_ID" +
                             " where sq.QUOTEID=@QUOTEID AND pg.CATEGORY_ID=@CATEGORYID and (sq.MASTERPRODUCTID=0 or sq.MASTERPRODUCTID='') ORDER by pg.SORTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", dcr("CATEGORY_ID").ToString()))
                    dsp = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dsp.Tables(0).Rows.Count > 0 Then
                        For Each drp As DataRow In dsp.Tables(0).Rows
                            content.Append("<tr>")
                            content.Append("<td width=145 valign=top nowrap=nowrap><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + drp("partnumber").ToString + "</font></td>")
                            content.Append("<td width=276 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
                            content.Append(drp("PRODUCTDESCRIPTION").ToString)
                            'If (drp("REF_SERIES_ID") > 0 And Int32.Parse(dcr("CATEGORY_ID")) <> 3) Then
                            '    content.Append(strCret + " For " + GetSeriesNameonID(drp("REF_SERIES_ID")).ToString)
                            'End If
                            content.Append("</font></td>")
                            content.Append("<td width=60 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + drp("QTY").ToString + "</font></td>")
                            If flgdisplayPrice Then
                                TotalPrice = drp("PRICE") * drp("QTY")
                                SubTotal = SubTotal + TotalPrice
                                If (Convert.ToDecimal(drp("PRICE")) <= 0) Then
                                    If (ShouldDisplayCallForPrice(Convert.ToInt32(drp("MASTERPRODUCTID").ToString()), Convert.ToInt32(drp("PRODUCTID").ToString()), quoteDate)) Then
                                        content.Append("<td align=center colspan=2 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></td>")
                                    Else
                                        content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", drp("PRICE")).ToString())
                                        Try
                                            Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(drp("PRODUCTID").ToString()))
                                            If (Convert.ToDecimal(drp("PRICE").ToString()) < priceInBaanTable) Then
                                                content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(drp("PRICE").ToString())) * Int32.Parse(drp("QTY").ToString())).ToString("c"))
                                                savingsTotal += (priceInBaanTable * Int32.Parse(drp("QTY").ToString()))
                                            End If
                                        Catch ex As Exception
                                        End Try
                                        content.Append("</font></strong></td>")
                                        Dim buyNowLink As String = String.Empty
                                        If (Int32.Parse(dcr("CATEGORY_ID").ToString()) = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES) Then
                                            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Convert.ToInt32(drp("PRODUCTID").ToString()))
                                            If Not (product Is Nothing) Then
                                                If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                                    buyNowLink = String.Format("<br />{0}", String.Format("<a href=""https://store.teledynelecroy.com{0}"" target=""_blank"">Buy it now</a>", product.ShopifyHandle))
                                                End If
                                            End If
                                        End If
                                        content.AppendFormat("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>{0}{1}</font></td>", String.Format("{0:c}", TotalPrice).ToString(), buyNowLink)
                                    End If
                                Else
                                    content.Append("<td width=72 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", drp("PRICE")).ToString())
                                    Try
                                        Dim priceInBaanTable As Decimal = GetBaanSalePrice(Convert.ToInt32(drp("PRODUCTID").ToString()))
                                        If (Convert.ToDecimal(drp("PRICE").ToString()) < priceInBaanTable) Then
                                            content.AppendFormat("<br /><span style=""font-face: Verdana, Geneva, sans-serif; font-size: 7pt; color: #ff0000;"">You saved {0}</span>", ((priceInBaanTable - Convert.ToDecimal(drp("PRICE").ToString())) * Int32.Parse(drp("QTY").ToString())).ToString("c"))
                                            savingsTotal += (priceInBaanTable * Int32.Parse(drp("QTY").ToString()))
                                        End If
                                    Catch ex As Exception
                                    End Try
                                    content.Append("</font></strong></td>")
                                    Dim buyNowLink As String = String.Empty
                                    If (Int32.Parse(dcr("CATEGORY_ID").ToString()) = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES) Then
                                        Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Convert.ToInt32(drp("PRODUCTID").ToString()))
                                        If Not (product Is Nothing) Then
                                            If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                                buyNowLink = String.Format("<br />{0}", String.Format("<a href=""https://store.teledynelecroy.com{0}"" target=""_blank"">Buy it now</a>", product.ShopifyHandle))
                                            End If
                                        End If
                                    End If
                                    content.AppendFormat("<td width=90 valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>{0}{1}</font></td>", String.Format("{0:c}", TotalPrice).ToString(), buyNowLink)
                                End If
                            End If
                            content.Append("</tr>")
                        Next
                    End If
                    If flgdisplayPrice Then
                        If (savingsTotal > 0) Then
                            content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Price before savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dcr("CATEGORY_NAME").ToString(), (savingsTotal + SubTotal).ToString("c"))
                            content.AppendFormat("<tr><td width=""553"" colspan=""4"" valign=""top""><strong><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000"">Your total savings for {0} system:</font></strong></td><td width=""90"" valign=""top""><font face=""Verdana, Geneva, sans-serif"" size=""2"" color=""#000000""><b>{1}</b></font></td></tr>", dcr("CATEGORY_NAME").ToString(), (savingsTotal * -1).ToString("c"))
                            content.Append("<tr>")
                            content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dcr("CATEGORY_NAME").ToString() + " system:</font></strong></td>")
                            content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString() + "</font></strong></td>")
                            content.Append("</tr>")
                        Else
                            content.Append("<tr>")
                            content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Sub total for " + dcr("CATEGORY_NAME").ToString() + " system:</font></strong></td>")
                            If (SubTotal <= 0) Then
                                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></strong></td>")
                            Else
                                content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", SubTotal).ToString() + "</font></strong></td>")
                            End If
                            content.Append("</tr>")
                        End If
                    End If
                    If flgdisplayPrice Then
                        Total = Total + SubTotal
                    End If
                Next
            End If
            '*****Total
            If flgdisplayPrice Then
                content.Append("<tr>")
                content.Append("<td width=553 colspan=4 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>Quote Total:</font></strong></td>")
                If (Total <= 0) Then
                    content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>CALL FOR PRICE</font></strong></td>")
                Else
                    content.Append("<td width=90 valign=top><strong><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + String.Format("{0:c}", Total).ToString + "</font></strong></td>")
                End If
                content.Append("</tr>")
            End If
            content.Append("</table>")
            GetQuoteBody = content.ToString
        End If
    End Function

    Private Shared Function ShouldDisplayCallForPrice(ByVal masterProductId As Int32, ByVal productId As Int32, ByVal quoteDate As DateTime) As Boolean
        Dim promotionXref As PromotionXRef = PromotionXRefRepository.GetPromotionXRef(ConfigurationManager.AppSettings("ConnectionString"), masterProductId, productId)
        If (promotionXref Is Nothing) Then Return True
        If (promotionXref.PromotionXRefPkId <= 0) Then Return True

        If (Not ((promotionXref.StartDate.HasValue) And (promotionXref.EndDate.HasValue))) Then
            If (promotionXref.IsActive) Then
                Return False
            Else
                Return True
            End If
        End If
        If (promotionXref.StartDate.HasValue) Then
            If (quoteDate < promotionXref.StartDate.Value) Then Return True
        End If
        If (promotionXref.EndDate.HasValue) Then
            If (quoteDate > promotionXref.EndDate.Value) Then Return True
        End If
        Return False
    End Function

    Public Shared Function GetSECSRQuoteFooter(ByVal Quotetype As String, ByVal QuoteID As Long, ByVal localeid As String) As String
        GetSECSRQuoteFooter = ""
        Dim strCret As String = "<br />"
        Dim content As StringBuilder = New StringBuilder()
        If String.IsNullOrEmpty(Quotetype) Then Quotetype = "scopes"
        If QuoteID > 0 Then
            content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
            If String.Compare(Quotetype, "psg") = 0 Then
                If Not String.IsNullOrEmpty(GetCCMS_Name("SP", GetSPNumber(GetContactidOnQuoteId(QuoteID)))) Then
                    content.Append("PSG Sales Operations Team" + strCret)
                    content.Append(GetCCMS_Name("SP", GetSPNumber(GetContactidOnQuoteId(QuoteID))).ToString + ",  PROTOCOL SALES SPECIALIST(S)" + strCret + strCret)
                End If
            Else
                If Not String.IsNullOrEmpty(GetCCMS_Name("SE", GetSENumber(GetContactidOnQuoteId(QuoteID)))) Then
                    If Len(GetCCMS_Name("CS", GetCSRIDonSE(GetSENumber(GetContactidOnQuoteId(QuoteID))))) > 0 Then
                        content.Append(GetCCMS_Name("CS", GetCSRIDonSE(GetSENumber(GetContactidOnQuoteId(QuoteID)))).ToString + ", " + LoadI18N("SLBCA0062", localeid) + strCret)
                    End If
                    content.Append(LoadI18N("SLBCA0063", localeid) + " " + GetCCMS_Name("SE", GetSENumber(GetContactidOnQuoteId(QuoteID))).ToString + ", " + LoadI18N("SLBCA0064", localeid))
                End If
                content.Append("</font>")
            End If
        End If
        GetSECSRQuoteFooter = content.ToString
    End Function

    Public Shared Function GetQuoteInfo(ByVal QuoteID As Long, ByVal localeid As String) As String
        GetQuoteInfo = ""
        If QuoteID > 0 Then
            Dim strQuoteTime As Date = GetQuoteTimeOnQuoteID(QuoteID).ToString
            Dim strInfo As String = ""
            Dim content As StringBuilder = New StringBuilder()
            content.Append("<table border=0 cellpadding=5 cellspacing= 0>")
            content.Append("<tr>")
            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000><strong>" + LoadI18N("SLBCA0007", localeid) + ":</strong></font></td>")
            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000><strong>" + QuoteID.ToString + "</strong></font></td>")
            content.Append("</tr>")
            content.Append("<tr>")
            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0069", localeid) + ":</font></td>")
            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + FormatDateTime(strQuoteTime.ToString, 2) + "</font></td>")
            content.Append("</tr>")
            content.Append("<tr>")
            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0070", localeid) + ":</font></td>")
            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>")

            If (DoesQuoteHaveActivePromotions(QuoteID)) Then
                Dim expirationDate As DateTime = GetPromotionExpirationDate(QuoteID)
                If Not (expirationDate = DateTime.MinValue) Then
                    content.Append(FormatDateTime(GetPromotionExpirationDate(QuoteID).ToShortDateString(), 2))
                Else
                    content.Append(FormatDateTime(strQuoteTime.AddDays(30), 2))
                End If
            Else
                content.Append(FormatDateTime(strQuoteTime.AddDays(30), 2))
            End If
            content.Append("</font></td>")
            content.Append("</tr>")
            content.Append(QuoteClientDataHtml(GetContactidOnQuoteId(QuoteID), localeid))
            content.Append("</table>")
            GetQuoteInfo = content.ToString
        End If
    End Function

    Public Shared Function QuoteClientDataHtml(ByVal ContactID As Long, ByVal localeid As String) As String
        QuoteClientDataHtml = ""
        Dim strSQL As String = ""
        Dim dsQuoteClientData As DataSet
        Dim content As StringBuilder = New StringBuilder()
        If Len(localeid) = 0 Then localeid = 1033
        If ContactID > 0 And Len(localeid) > 0 Then
            strSQL = "Select * from CONTACT where CONTACT_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsQuoteClientData = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsQuoteClientData.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsQuoteClientData.Tables(0).Rows
                    ' Client data
                    If localeid <> 1041 Then
                        ' Client data
                        content.Append("<tr>")
                        content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0076", localeid) + ":</font></td>")
                        content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Honorific").ToString + " " & dr("First_Name").ToString + " " + dr("Last_Name").ToString + "</font></td>")
                        content.Append("</tr>")
                        If Len((dr("Title").ToString)) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0077", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Title").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Department").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0465", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Department").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0078", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Company").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0079", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Address").ToString + " " + dr("Address2").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0080", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("City").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("State_Province").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0081", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("State_Province").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0082", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("PostalCode").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Country").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0083", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Country").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0084", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Phone").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If

                        If Len(dr("Email").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0086", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000><a href=""mailto:" + dr("Email").ToString + """>" + dr("Email").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If

                    Else
                        'Client data for Japan
                        If Len(dr("Country").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0083", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Country").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("PostalCode").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0082", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("PostalCode").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("City").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0080", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("City").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Address").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0079", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Address").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Address2").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0079", localeid) + "2:</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Address2").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Company").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0078", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Company").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Title").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0077", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Title").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("First_Name").ToString) > 0 And Len(dr("Last_Name").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0076", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Last_Name").ToString + " " + dr("First_name").ToString + " " + LoadI18N("SLBCA0160", localeid).ToString + "</font></td>")
                            content.Append("</tr>")
                        End If
                        If Len(dr("Phone").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0084", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + dr("Phone").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If

                        If Len(dr("Email").ToString) > 0 Then
                            content.Append("<tr>")
                            content.Append("<td valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000>" + LoadI18N("SLBCA0086", localeid) + ":</font></td>")
                            content.Append("<td  valign=top><font face=Verdana, Geneva, sans-serif size=2 color=#000000> " + dr("Email").ToString + "</font></td>")
                            content.Append("</tr>")
                        End If

                    End If
                Next
                dsQuoteClientData.Dispose()
                dsQuoteClientData = Nothing
                QuoteClientDataHtml = content.ToString
            End If
        End If
    End Function

    Public Shared Function QuoteTermsLink() As String
        QuoteTermsLink = ""
        Dim strCret As String = "<br />"
        Dim content As StringBuilder = New StringBuilder()
        content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
        content.Append("To review our Purchase Terms and Conditions for United States please follow the link below:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/terms/"">https://teledynelecroy.com/terms/</a>")
        content.Append("</font>")
        QuoteTermsLink = content.ToString
    End Function

    Public Shared Function GetPromoQuoteFooter(ByVal QuoteID As Long) As String
        GetPromoQuoteFooter = ""
        Dim strCret As String = "<br />"
        If QuoteID > 0 Then
            Dim strSQL As String
            Dim dsPromoQuoteFooter As DataSet
            Dim content As StringBuilder = New StringBuilder()
            strSQL = " SELECT   BAANPRICE.PARTNUMBER, BAANPRICE.DISCOUNT_PCT, BAANPRICE.PROMO_END_DATE  FROM SHOPPERQUOTE INNER JOIN BAANPRICE ON SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID]  INNER JOIN     CONTACT ON SHOPPERQUOTE.CUSTOMERID = CONTACT.CONTACT_ID WHERE   BAANPRICE.PROMO_YN = y AND CONTACT.COUNTRY = United States  and SHOPPERQUOTE.QUOTEID = @QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            dsPromoQuoteFooter = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsPromoQuoteFooter.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsPromoQuoteFooter.Tables(0).Rows
                    content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
                    content.Append("NOTE: This quote includes the following promotional prices:" + strCret)
                    content.Append(dr("Partnumber").ToString + " (" + dr("DISCOUNT_PCT").ToString + "% OFF) until " + FormatDateTime(dr("PROMO_END_DATE"), 2) + strCret)
                    content.Append("</font>")
                Next
            End If
            dsPromoQuoteFooter.Dispose()
            dsPromoQuoteFooter = Nothing
            GetPromoQuoteFooter = content.ToString
        End If
    End Function

    Public Shared Function GetQuoteProfileInfo(ByVal QuoteID As Long, ByVal localeid As String) As String
        GetQuoteProfileInfo = ""
        Dim strCret As String = "<br />"
        If QuoteID > 0 Then
            Dim strQuoteTime As Date = GetQuoteTimeOnQuoteID(QuoteID).ToString
            Dim strInfo As String = ""
            Dim content As StringBuilder = New StringBuilder()
            content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
            content.Append("<strong>Your contact information:</strong><br />(<a href=""https://teledynelecroy.com/support/user/userprofile.aspx"">Modify  your profile</a>)<br />")
            content.Append("<table border=0 cellpadding=5 cellspacing= 0>")
            content.Append(QuoteClientDataHtml(GetContactidOnQuoteId(QuoteID), localeid))
            content.Append("</table>")
            content.Append("</font>")
            GetQuoteProfileInfo = content.ToString
        End If
    End Function

    Public Shared Function GetCCMS_Name(ByVal RecType As String, ByVal ID As String) As String
        Dim results As String = ""
        If Len(RecType) > 0 And Len(ID) > 0 Then
            If IsNumeric(ID) Then
                Dim strSQL As String = "Select * from CCMS_PERSONNEL_DATA where RecType=@RECTYPE and ID=@ID and ACTIVE='Y'"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@RECTYPE", RecType))
                sqlParameters.Add(New SqlParameter("@ID", ID))
                Dim ccmsPersonnelData As List(Of CCMSPersonnelData) = CCMSPersonnelDataRepository.FetchCCMSPersonnelData(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If (ccmsPersonnelData.Count > 0) Then
                    results = String.Format("{0} {1}", ccmsPersonnelData.FirstOrDefault().FirstName.Trim(), ccmsPersonnelData.FirstOrDefault().LastName.Trim())
                End If
            End If
        End If
        GetCCMS_Name = results.ToString
    End Function

    Public Shared Function GetSELinks(ByVal QuoteID As Long, ByVal localeid As String) As String
        GetSELinks = ""
        Dim strCret As String = "<br />"
        Dim content As StringBuilder = New StringBuilder()
        content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
        content.Append("Click on the link below to approve quote request and send email to customer:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/shopper/mailtocustomer.aspx?cust=" + GetContactidOnQuoteId(QuoteID).ToString + "&qid=" + QuoteID.ToString + "&lid=" + localeid + """>Approve</a>" + strCret + strCret)
        content.Append("If this quote should not be approved, please click on the following link:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/shopper/ApprovalRefuse.asp?quoteid=" + QuoteID.ToString + """>Refuse</a>" + strCret + strCret)
        content.Append("To request this quote to be modified by CSRs and sent to the customer, please click on the following link:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/shopper/ModifyQuote.asp?cust=" + GetContactidOnQuoteId(QuoteID).ToString + "&qid=" + QuoteID.ToString + """>Modify</a>")
        content.Append("</font>")
        GetSELinks = content.ToString
    End Function

    Public Shared Function GetSendToDistLink(ByVal QuoteID As Long, ByVal localeid As String) As String
        GetSendToDistLink = ""
        Dim strCret As String = "<br />"
        Dim content As StringBuilder = New StringBuilder()
        content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
        content.Append("To forward the quote to the distributor, please click on the following link:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/shopper/sendtodistributor.aspx?cust=" + GetContactidOnQuoteId(QuoteID).ToString + "&qid=" + QuoteID.ToString + """>Send To Distributor</a>")
        content.Append("</font>")
        GetSendToDistLink = content.ToString
    End Function

    Public Shared Function GetCSRIDonSE(SENumber As String) As String
        Dim results As String = ""
        If Len(SENumber) > 0 Then
            Dim strSQL As String = "Select CSRCode from  CCMS_SECSR where SECode=@SECODE"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SECODE", SENumber.ToString()))
            Dim ccmsSeCsrs As List(Of CCMSSeCsr) = CCMSSeCsrRepository.FetchCCMSSeCsr(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (ccmsSeCsrs.Count > 0) Then
                If Not String.IsNullOrEmpty(ccmsSeCsrs.FirstOrDefault().CsrCode.ToString()) Then
                    results = Trim(ccmsSeCsrs.FirstOrDefault().CsrCode.ToString())
                End If
            End If
        End If
        GetCSRIDonSE = results.ToString
    End Function

    Public Shared Function GetQuoteDiscountNote(ByVal Quotetype As String, ByVal QuoteID As Long) As String
        GetQuoteDiscountNote = ""
        Dim content As StringBuilder = New StringBuilder()
        If String.IsNullOrEmpty(Quotetype) Then Quotetype = "scopes"
        If QuoteID > 0 Then
            content.Append("<font size=1 color=#54565b>Please note that your quotation will not reflect any educational, government or promotional pricing that may be available. Contact our Customer Support Center at ")
            If String.Compare(Quotetype, "psg") = 0 Then
                content.Append("(800) 909-7211")
            Else
                content.Append("(800) 553-2769")
            End If
            content.Append(" if you feel you may be eligible for these discounts.</font> ")

        End If
        GetQuoteDiscountNote = content.ToString
    End Function

    Public Shared Sub InsertQuoteReminder(ByVal Quotetype As String, ByVal QuoteID As Long, ByVal strTo As String, ByVal strBody As String)
        'Insert Quote Info Into EMAIL_CSR
        Dim strSQL As String = ""
        Dim psgyn As String = ""
        If String.Compare(Quotetype, "psg") = 0 Then
            psgyn = "y"
        Else
            psgyn = "n"
        End If

        strSQL = "INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION, PSG_YN) values(@QUOTEID,@SENDCOUNTER,@SENDEMAILDATE,@RESPONSEFLAG,@TO,@FROM,@EMAILBODY,@CUSTOMERINFO,@CSRNAME,@DESCRIPTION,@PSGYN)"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
        sqlParameters.Add(New SqlParameter("@SENDCOUNTER", "1"))
        sqlParameters.Add(New SqlParameter("@SENDEMAILDATE", DateTime.Now.ToString()))
        sqlParameters.Add(New SqlParameter("@RESPONSEFLAG", "N"))
        sqlParameters.Add(New SqlParameter("@TO", strTo))
        sqlParameters.Add(New SqlParameter("@FROM", "webquote@teledynelecroy.com"))
        Dim sqlParam As SqlParameter = New SqlParameter()
        sqlParam.ParameterName = "@EMAILBODY"
        sqlParam.SqlDbType = SqlDbType.NText
        sqlParam.Value = Replace(Replace(strBody, "'", "&#39;"), Chr(34), "")
        sqlParameters.Add(sqlParam)
        sqlParam = New SqlParameter()
        sqlParam.ParameterName = "@CUSTOMERINFO"
        sqlParam.SqlDbType = SqlDbType.NText
        sqlParam.Value = Replace(GetQuoteInfo(QuoteID.ToString, 1033), "'", "''")
        sqlParameters.Add(sqlParam)
        sqlParameters.Add(New SqlParameter("@CSRNAME", String.Empty))
        sqlParameters.Add(New SqlParameter("@DESCRIPTION", String.Empty))
        sqlParameters.Add(New SqlParameter("@PSGYN", psgyn))
        DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        SendEmail("", strSQL, "kate.kaplan@teledynelecroy.com", "kate.kaplan@teledynelecroy.com", "", "", "Insert Quote " & QuoteID.ToString, "")
    End Sub

    Public Shared Sub InsertQuoteCustomerEmail(ByVal QuoteID As Long, ByVal strTo As String, ByVal strFrom As String, ByVal strSEEmail As String, ByVal strBody As String)
        Dim strSQL As String = ""
        Dim strCret As String = "<br />"
        'Insert Quote Info Into EMAIL_CUSTOMER and EMAIL_SE
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        strSQL = "INSERT into EMAIL_CUSTOMER (QUOTEID, [TO], [FROM], EMAIL_BODY) values(@QUOTEID,@TO,@FROM,@EMAILBODY)"
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
        sqlParameters.Add(New SqlParameter("@TO", strTo.ToString()))
        sqlParameters.Add(New SqlParameter("@FROM", strFrom.ToString()))
        Dim sqlParam As SqlParameter = New SqlParameter()
        sqlParam.ParameterName = "@EMAILBODY"
        sqlParam.SqlDbType = SqlDbType.NText
        sqlParam.Value = Replace(Replace(strBody, "'", "&#39;"), Chr(34), "")
        sqlParameters.Add(sqlParam)
        DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        'SendEmail("", strSQL, "kate.kaplan@teledynelecroy.com", "kate.kaplan@teledynelecroy.com", "", "", "Insert Quote  - EMAIL_CUSTOMER - " & QuoteID.ToString, "")

        strSQL = "INSERT into EMAIL_QUOTE_SE (QUOTEID, [TO], [FROM], EMAIL_BODY) values(@QUOTEID,@TO,@FROM,@EMAILBODY)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
        sqlParameters.Add(New SqlParameter("@TO", strSEEmail.ToString()))
        sqlParameters.Add(New SqlParameter("@FROM", strFrom.ToString()))
        sqlParam = New SqlParameter()
        sqlParam.ParameterName = "@EMAILBODY"
        sqlParam.SqlDbType = SqlDbType.NText
        sqlParam.Value = "<font face=Verdana, Geneva, sans-serif size=2 color=#000000>The quote was requested 24 hours ago. The system approved the quote and sent it to the customer</font>" & strCret & Replace(Replace(strBody, "'", "&#39;"), Chr(34), "")
        sqlParameters.Add(sqlParam)
        DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        'SendEmail("", strSQL, "kate.kaplan@teledynelecroy.com", "kate.kaplan@teledynelecroy.com", "", "", "Insert Quote - EMAIL_QUOTE_SE -  " & QuoteID.ToString, "")
    End Sub

    Public Shared Function GetSEQuoteEmail(ByVal Quotetype As String, ByVal QuoteID As Long) As String
        GetSEQuoteEmail = "kate.kaplan@teledynelecroy.com"
        If String.Compare(Quotetype, "psg") = 0 Then
            GetSEQuoteEmail = GetCCMS_Email("SP", GetSPNumber(GetContactidOnQuoteId(QuoteID)).ToString).ToString
        Else
            GetSEQuoteEmail = GetCCMS_Email("SE", GetSENumber(GetContactidOnQuoteId(QuoteID)).ToString).ToString
        End If
    End Function

    Public Shared Function GetSpCsrEmail(ByVal spNumber As Int32) As String
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SECODE", spNumber.ToString()))
        Dim ccmsSeCsrs As List(Of CCMSSeCsr) = CCMSSeCsrRepository.FetchCCMSSeCsr(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [CCMS_SECSR] WHERE [SECODE] = @SECODE", sqlParameters.ToArray())
        Dim retVal As String = String.Empty
        For Each ccmsSeCsr In ccmsSeCsrs
            Dim ids As List(Of String) = New List(Of String)
            If Not (String.IsNullOrEmpty(ccmsSeCsr.CsrCode)) Then ids.Add(ccmsSeCsr.CsrCode)
            If Not (String.IsNullOrEmpty(ccmsSeCsr.CsrBackup1)) Then ids.Add(ccmsSeCsr.CsrBackup1)
            If Not (String.IsNullOrEmpty(ccmsSeCsr.CsrBackup2)) Then ids.Add(ccmsSeCsr.CsrBackup2)
            If (ids.Count > 0) Then
                Dim emailAddresses As String = GetCcmsPersonnelRecords(ids)
                If Not (String.IsNullOrEmpty(retVal)) Then retVal += ","
                retVal += emailAddresses
            End If
        Next
        Return retVal
    End Function

    Private Shared Function GetCcmsPersonnelRecords(ByVal ids As List(Of String)) As String
        Dim retVal As String = String.Empty
        For Each id In ids
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", id))
            Dim ccmsPersonnelData As CCMSPersonnelData = CCMSPersonnelDataRepository.FetchCCMSPersonnelData(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [CCMS_PERSONNEL_DATA] WHERE [ID] = @ID AND [RECTYPE] = 'CS' AND [ACTIVE] = 'Y'", sqlParameters.ToArray()).FirstOrDefault()
            If Not (ccmsPersonnelData Is Nothing) Then
                If Not (String.IsNullOrEmpty(retVal)) Then retVal += ","
                retVal += ccmsPersonnelData.Email
            End If
        Next
        Return retVal
    End Function

    Public Shared Function GetQuoteOrderConditionsStatement(ByVal Quotetype As String, ByVal QuoteID As Long) As String
        GetQuoteOrderConditionsStatement = ""
        Dim strCret As String = "<br />"
        Dim localeid As String = 1033
        Dim content As StringBuilder = New StringBuilder()
        If String.IsNullOrEmpty(Quotetype) Then Quotetype = "scopes"
        If QuoteID > 0 Then
            content.Append("<font face=Verdana, Geneva, sans - serif size=2 color=#000000>")
            If String.Compare(Quotetype, "psg") = 0 Then
                content.Append("For Order Placement please fax to 408-727-6622 or email to protocolsales@teledynelecroy.com. For Credit Card Orders or Technical Information please call 1-800-909-7211. Teledyne LeCroy standard terms of sale are Net30 days after invoice date. As with all credit items, satisfactory credit accommodations must be arranged. Minimum order is $50.00. ")
                content.Append(LoadI18N("SLBCA0041", localeid) + ". ")
                content.Append(LoadI18N("SLBCA0042", localeid) + "." + strCret + strCret)
            ElseIf String.Compare(Quotetype, "dist") = 0 Then
                content.Append("This product(s) is sold through our distribution partners. Your inquiry will be forwarded to one of our distributors.")
            Else
                content.Append(LoadI18N("SLBCA0039", localeid) + ". ")
                content.Append(LoadI18N("SLBCA0041", localeid) + ". ")
                content.Append(LoadI18N("SLBCA0042", localeid) + "." + strCret + strCret)
            End If
            content.Append("</font> ")
        End If
        GetQuoteOrderConditionsStatement = content.ToString
    End Function
    Public Shared Function GetDistEmailOnOfficeID(ByVal OfficeId As Integer) As String
        Dim results As String = ""
        If (OfficeId) > 0 Then
            Dim strSQL As String = "Select EMAIL_LEADS FROM CONTACT_OFFICE  WHERE OFFICE_ID = @OFFICEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@OFFICEID", OfficeId.ToString()))
            Dim contactOffices As List(Of ContactOffice) = ContactOfficeRepository.FetchContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (contactOffices.Count > 0) Then
                If Not String.IsNullOrEmpty(contactOffices.FirstOrDefault().EmailLeads.ToString()) Then
                    results = Trim(contactOffices.FirstOrDefault().EmailLeads.ToString())
                End If
            End If
        End If
        GetDistEmailOnOfficeID = results.ToString
    End Function
    Public Shared Function GetSendQuoteForReviewLink(ByVal QuoteID As Long, ByVal localeid As String) As String
        GetSendQuoteForReviewLink = ""
        Dim strCret As String = "<br />"
        Dim content As StringBuilder = New StringBuilder()
        content.Append("<font face=Verdana, Geneva, sans-serif size=2 color=#000000>")
        content.Append("To review the quote and send it to either Direct Sales Engineer or Distributor Managers, please click on the following link:" + strCret)
        content.Append("<a href=""https://teledynelecroy.com/reviewquote/default.aspx?cust=" + GetContactidOnQuoteId(QuoteID).ToString + "&qid=" + QuoteID.ToString + """>Review Quote</a>")
        content.Append("</font>")
        GetSendQuoteForReviewLink = content.ToString
    End Function
    Public Shared Sub UpdatedQuoteSECode(ByVal QuoteID As Long, ByVal SECode As String)
        Dim strSQL As String = ""
        If Not String.IsNullOrEmpty(SECode) And QuoteID > 0 Then
            strSQL = "Update QUOTE set SE_CODE=@SECODE where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SECODE", SECode))
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If
    End Sub

    Public Shared Function GetEmailContactid(ByVal ContactID As Long) As String
        Dim strSQL As String = ""
        Dim strEmail As String = ""
        If ContactID > 0 Then
            strSQL = "SELECT EMAIL from CONTACT where Contact_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            Dim contacts As List(Of Contact) = ContactRepository.FetchContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (contacts.Count > 0) Then
                strEmail = contacts.FirstOrDefault().Email
            End If
        End If
        GetEmailContactid = strEmail
    End Function

    Public Shared Function GetQuoteLogGUID(ByVal QuoteID As Long) As String
        Dim ds As DataSet
        Dim strSQL As String = ""
        Dim strGuID As String = ""
        strSQL = "SELECT rowguid FROM  QUOTE_LOG where QUOTEID=@QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                strGuID = dr("rowguid").ToString
            Next
        End If
        GetQuoteLogGUID = strGuID.ToString
    End Function
    Public Shared Function GetQuoteType(ByVal QuoteID As Long) As String
        Dim ds As DataSet
        Dim strSQL As String = ""
        GetQuoteType = "scopes"
        strSQL = "SELECT PSG_YN FROM  EMAIL_CSR  where QUOTEID=@QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not IsDBNull(dr("PSG_YN")) Then
                    If Not String.IsNullOrEmpty(dr("PSG_YN")) Then
                        If String.Compare(UCase(dr("PSG_YN")), "Y") = 0 Then
                            GetQuoteType = "psg"
                        End If
                    End If
                End If
            Next
        End If
        ds.Dispose()
        ds = Nothing
    End Function

    Public Shared Function GetDSMNumber(ByVal ContactID As Long) As String
        GetDSMNumber = ""
        Dim strSQL As String
        Dim dsDSMCode As DataSet
        If Len(ContactID) > 0 Then
            strSQL = "SELECT DISTINCT CCMS_MAP_US.DMCode FROM  CCMS_MAP_US INNER JOIN STATE ON CCMS_MAP_US.State = STATE.SHORT_NAME INNER JOIN CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE WHERE CONTACT.CONTACT_ID=@CONTACTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsDSMCode = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsDSMCode.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsDSMCode.Tables(0).Rows
                    If Not IsDBNull(dr("DMCode")) Then
                        GetDSMNumber = dr("DMCode").ToString
                    End If
                Next
            End If
            dsDSMCode.Dispose()
            dsDSMCode = Nothing
        End If
    End Function

    Public Shared Function ValidateIntegerFromSession(ByVal val As Object) As Int32
        If (val Is Nothing) Then
            Return -1
        End If
        If (String.IsNullOrEmpty(val)) Then
            Return -1
        End If
        Dim testVal As Int32 = 0
        If (Int32.TryParse(val.ToString(), testVal)) Then
            Return testVal
        End If
        Return -1
    End Function

    Public Shared Function ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(ByVal val As Object) As Int32
        Dim retVal As Int32 = ValidateIntegerFromSession(val)
        If (retVal = -1) Then
            retVal = 208
        End If
        Return retVal
    End Function

    Public Shared Function ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(ByVal val As Object) As Int32
        Dim retVal As Int32 = ValidateIntegerFromSession(val)
        If (retVal < 0) Then
            Return 1033
        End If
        Return retVal
    End Function

    Public Shared Function ConvertPotentialCommaSeparatedStringIntoList(ByVal sourceData As String) As List(Of String)
        Dim retVal As List(Of String) = New List(Of String)
        If (String.IsNullOrEmpty(sourceData)) Then Return retVal

        Dim split As String() = sourceData.Split(",")
        For Each s In split
            If Not (String.IsNullOrEmpty(s)) Then retVal.Add(s)
        Next
        Return retVal
    End Function

    Public Shared Function HACK_AddMiPersonnelToWebQuotes(ByVal quoteId As Int32) As Boolean
        Dim retVal As Boolean = False
        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(GetContactidOnQuoteId(quoteId)))
        If Not (contact Is Nothing) Then
            If (String.Compare(contact.StateProvince, "Michigan", True) = 0) Then
                retVal = True
            End If
        End If
        Return retVal
    End Function
End Class