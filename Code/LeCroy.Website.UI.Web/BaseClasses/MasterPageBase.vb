﻿Public Class MasterPageBase
    Inherits MasterPage

    Private _metaOgTitle As String = String.Empty
    Public Property MetaOgTitle As String
        Get
            Return _metaOgTitle
        End Get
        Set(value As String)
            _metaOgTitle = value
        End Set
    End Property

    Private _metaOgImage As String = String.Empty
    Public Property MetaOgImage As String
        Get
            Return _metaOgImage
        End Get
        Set(value As String)
            _metaOgImage = value
        End Set
    End Property

    Private _metaOgDescription As String = String.Empty
    Public Property MetaOgDescription As String
        Get
            Return _metaOgDescription
        End Get
        Set(value As String)
            _metaOgDescription = value
        End Set
    End Property

    Private _metaOgUrl As String = String.Empty
    Public Property MetaOgUrl As String
        Get
            Return _metaOgUrl
        End Get
        Set(value As String)
            _metaOgUrl = value
        End Set
    End Property

    Private _metaOgType As String = String.Empty
    Public Property MetaOgType As String
        Get
            Return _metaOgType
        End Get
        Set(value As String)
            _metaOgType = value
        End Set
    End Property

    Protected Sub InjectCssIntoHeadElement(ByVal resourceUrl As String, ByVal isTopMostMaster As Boolean)
        Dim control As HtmlLink = New HtmlLink()
        control.Href = ResolveUrl(resourceUrl)
        control.Attributes.Add("rel", "stylesheet")
        control.Attributes.Add("type", "text/css")
        If (isTopMostMaster) Then
            CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(control)
        Else
            CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
        End If
    End Sub

    Protected Sub InjectJsIntoHeadElement(ByVal resourceUrl As String, ByVal isTopMostMaster As Boolean)
        Dim control As HtmlGenericControl = New HtmlGenericControl("script")
        control.Attributes.Add("type", "text/javascript")
        control.Attributes.Add("language", "javascript")
        control.Attributes.Add("src", ResolveUrl(resourceUrl))
        If (isTopMostMaster) Then
            CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(control)
        Else
            CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
        End If
    End Sub

    Protected Sub InjectIEAlternateCss(ByVal isTopMostMaster As Boolean, ByVal isHack As Boolean)
        Dim literal As Literal = New Literal()
        Dim sb As StringBuilder = New StringBuilder
        Dim alternateIE7Header As String = String.Empty
        If (isHack) Then alternateIE7Header = String.Format("<link type=""text/css"" rel=""stylesheet"" href=""{0}"" />", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateHeaderIE7")))
        sb.AppendFormat("<!--[if IE 7]><link type=""text/css"" rel=""stylesheet"" href=""{0}"" />{1}<![endif]-->", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateIE7")), alternateIE7Header)
        sb.AppendFormat("<!--[if IE 6]><link type=""text/css"" rel=""stylesheet"" href=""{0}"" /><![endif]-->", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateIE6")))
        literal.Text = sb.ToString()
        If (isTopMostMaster) Then
            CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(literal)
        Else
            CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(literal)
        End If
    End Sub

    Public Sub BindMetaTags(ByVal isTopMostMaster As Boolean)
        Dim kvp As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
        Dim t As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)("title", _metaOgTitle)
        kvp.Add(t)
        Dim i As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)("image", _metaOgImage)
        kvp.Add(i)
        Dim d As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)("description", _metaOgDescription)
        kvp.Add(d)
        Dim u As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)("url", _metaOgUrl)
        kvp.Add(u)
        Dim ty As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)("type", _metaOgType)
        kvp.Add(ty)
        For Each item In kvp
            If Not (String.IsNullOrEmpty(item.Value)) Then
                Dim c As HtmlMeta = New HtmlMeta()
                c.Attributes.Add("property", String.Format("og:{0}", item.Key.ToString()))
                c.Content = item.Value.ToString()
                If (isTopMostMaster) Then
                    CType(Me.FindControl("Head1"), HtmlHead).Controls.Add(c)
                Else
                    CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(c)
                End If
            End If
        Next
    End Sub
End Class