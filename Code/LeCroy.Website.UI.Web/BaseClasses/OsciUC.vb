Imports Microsoft.VisualBasic

Public Class OsciUC
    Private gn As String
    Private gid As String
    Private co As Object
    Private tb As TextBox
    Private catid As String
    Private ty As String


    Public Sub New()
        gn = ""
        co = New Object()
        catid = ""
        ty = ""
        tb = New TextBox()
    End Sub

    Public Property Group_name() As String
        Get
            Return gn
        End Get
        Set(ByVal value As String)
            gn = value
        End Set
    End Property

    Public Property Group_ID() As String
        Get
            Return gid
        End Get
        Set(ByVal value As String)
            gid = value
        End Set
    End Property

    Public Property Obj() As Object
        Get
            Return co
        End Get
        Set(ByVal value As Object)
            co = value
        End Set
    End Property

    Public Property CataID() As String
        Get
            Return catid
        End Get
        Set(ByVal value As String)
            catid = value
        End Set
    End Property

    Public Property ControlType() As String
        Get
            Return ty
        End Get
        Set(ByVal value As String)
            ty = value
        End Set
    End Property

    Public Property Mount() As TextBox
        Get
            Return tb
        End Get
        Set(ByVal value As TextBox)
            tb = value
        End Set
    End Property

    Public ReadOnly Property ContentText() As String
        Get
            Dim result As String
            Dim ddl As DropDownList
            Dim lbx As ListBox
            Dim cbl As CheckBoxList
            Dim tb As TextBox
            result = gn
            result += "<br/>"

            Select Case ty
                Case "c"
                    cbl = co
                    cbl = CType(cbl, CheckBoxList)
                    For Each l As ListItem In cbl.Items
                        If l.Selected Then
                            result += l.Text + "<br/>"
                        End If
                    Next
                Case "s"
                    lbx = co
                    lbx = CType(lbx, ListBox)
                    For Each l As ListItem In lbx.Items
                        If l.Selected Then
                            result += l.Text + "<br/>"
                        End If
                    Next
                Case "d"
                    ddl = co
                    ddl = CType(ddl, DropDownList)
                    If ddl.SelectedValue <> "" Then
                        result += ddl.SelectedItem.Text + "<br/>"
                    End If
                Case "t"
                    tb = co
                    tb = CType(tb, TextBox)
                    result += tb.Text + "<br/>"
            End Select
            If result = gn + "<br/>" Then
                result = ""
            End If
            If result <> "" Then
                result += "<br/><br/>"
            End If

            Return result
        End Get
    End Property
End Class
