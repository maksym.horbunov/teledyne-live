﻿Public Class QuoteBasePage
    Inherits BasePage

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("flgSessionTransfer") Is Nothing Or Len(Session("flgSessionTransfer")) = 0 Then
            If Not (String.IsNullOrEmpty(Session("LocaleIdFromGeoIp"))) Then
                Session("localeid") = Session("LocaleIdFromGeoIp")
            Else
                Session("localeid") = 1033
            End If
        End If
    End Sub
End Class