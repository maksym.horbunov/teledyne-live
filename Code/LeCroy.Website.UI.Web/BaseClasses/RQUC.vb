Imports Microsoft.VisualBasic

Public Class RQUC
    Private co As CheckBox
    Private mo As TextBox
    Public Sub New()
        co = New CheckBox()
        mo = New TextBox()
        mo.Width = Unit.Pixel(40)
        mo.Text = "1"
    End Sub

    Public Property Obj() As CheckBox
        Get
            Return co
        End Get
        Set(ByVal value As CheckBox)
            co = value
        End Set
    End Property

    Public Property Mount() As TextBox
        Get
            Return mo
        End Get
        Set(ByVal value As TextBox)
            mo = value
        End Set
    End Property

End Class
