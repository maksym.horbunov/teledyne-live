﻿Public Class RegBasePage
    Inherits BasePage

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("flgSessionTransfer") Is Nothing Or Len(Session("flgSessionTransfer")) = 0 Then
            If Not (String.IsNullOrEmpty(Session("LocaleIdFromGeoIp"))) Then
                Session("localeid") = Session("LocaleIdFromGeoIp")
            End If
        End If

        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        If Session("RedirectTo") Is Nothing Then
            If (localeId = 1041) Then
                Session("RedirectTo") = "/Japan/"
            End If
        Else
            If (localeId = 1041 And InStr(LCase(Session("RedirectTo")), "japan") = 0) Then
                Session("RedirectTo") = "/Japan/"
            End If
        End If
    End Sub
End Class