﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="ApplicantDisclosure.aspx.vb" Inherits="LeCroy.Website.Corporate_Jobs_ApplicantDisclosure" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="pnlSubmit" runat="server" Visible="true">
        <div style="background-color: #ffffff; padding: 15px; width: auto;">
            <div style="color: #007AC3; font-size: 20px; font-weight: bold;">Voluntary Disclosure</div>
            <p><i><b>Teledyne is an Equal Opportunity/Affirmative Action employer.</b> All qualified applicants will receive consideration for employment without regard to race, color, religion, religious creed, gender, sexual orientation, gender identity, gender expression, transgender, pregnancy, marital status, national origin, ancestry, citizenship status, age, disability, protected Veteran Status, genetics or any other characteristic protected by applicable federal, state, or local law. To see our Policy on EEO <a href="./2018-reaffirmation-eeo.pdf" target="_blank">click here</a>. You may also view the EEO is the Law Poster by <a href="https://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank">clicking here</a> and its <a href="./eeo-supplement.pdf" target="_blank">supplement</a>.</i></p>
            <p>In compliance with the ADA Amendments Act (ADAAA), if you have a disability and would like to request an accommodation in order to apply for this position with Teledyne LeCroy, please email <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a> or call (845) 425-2000 and ask to speak with Human Resources. Determination on requests for reasonable accommodation will be made on a case-by-case basis. Please note that only those inquiries concerning a request for reasonable accommodation will receive a response.<br /><br />
            Teledyne LeCroy is a participant in E-Verify (<a href="./e-verify_participation_poster_english.pdf" target="_blank">English</a> | <a href="./e-verify_participation_poster_spanish.pdf" target="_blank">Spanish</a>) and <a href="./pay-transparency.pdf" target="_blank">Pay Transparency</a>. Details on Right To Work may be found in <a href="./e-verify_right_to_work_poster_english.pdf" target="_blank">English</a> and <a href="./e-verify_right_to_work_poster_spanish.pdf" target="_blank">Spanish</a>.</p>
            <p>Various government agencies request statistical information regarding our hiring practices. Your cooperation in completing this form is completely <u><b>voluntary</b></u>. Any information gathered is strictly confidential. Refusal to provide the information requested will not subject you to any retaliation or adverse treatment. Thank you for your cooperation.</p>
            <div id="divJobTitle" runat="server" visible="false">
                <b>Job applied for:</b>&nbsp;<asp:Literal ID="litJobTitle" runat="server" />
            </div><br />
            <div>
                <table border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="right" valign="middle"><b>First Name:</b>&nbsp;</td>
                        <td align="left" valign="top"><asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" Width="200" /></td>
                    </tr>
                    <tr>
                        <td align="right" valign="middle"><b>Last Name:</b>&nbsp;</td>
                        <td align="left" valign="top"><asp:TextBox ID="txtLastName" runat="server" MaxLength="50" Width="200" /></td>
                    </tr>
                </table>
            </div><br />
            <div>
                <span><b>Please indicate your gender:</b>&nbsp;</span>
                <asp:RadioButtonList ID="rblGender" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Text="Male" Value="M" />
                    <asp:ListItem Text="Female" Value="F" />
                </asp:RadioButtonList>
            </div><br />
            <div>
                <b>Please indicate your race/ethnic group (check all that apply):</b>&nbsp;
                <asp:CheckBoxList ID="cblEthnicity" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table">
                    <asp:ListItem Text="Hispanic or Latino" Value="32" />
                    <asp:ListItem Text="White (Not Hispanic or Latino)" Value="16" />
                    <asp:ListItem Text="Black or African American (Not Hispanic or Latino)" Value="8" />
                    <asp:ListItem Text="Native Hawaiian or Other Pacific Islander (Not Hispanic or Latino)" Value="4" />
                    <asp:ListItem Text="Asian (Not Hispanic or Latino)" Value="2" />
                    <asp:ListItem Text="American Indian or Alaska Native (Not Hispanic or Latino)" Value="1" />
                </asp:CheckBoxList>
            </div><br />
            <div>
                <asp:Button ID="btnDeclineStatistics" runat="server" CausesValidation="false" Text="I prefer not to disclose" />&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnSubmitStatistics" runat="server" CausesValidation="false" Text="Submit" Width="100" />
            </div>
            <div style="font-size: 9px;">
                Revised 7/25/2007 as per U.S. Equal Employment Opportunity Commission Guidelines
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">    
        <div align="center" style="background-color: #ffffff; height: 150px; width: 100%;">
            <br /><br /><h2>Thank You for participating!</h2>
            <asp:HyperLink ID="hlkHomePage" runat="server" Text="Back to the Teledyne LeCroy Homepage" />
        </div>
    </asp:Panel>
</asp:Content>