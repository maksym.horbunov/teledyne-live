﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Corporate_Jobs_ApplicantDisclosure
    Inherits BasePage

#Region "Variables/Keys"
    Private TICKETID_VIEWSTATEKEY As String = "TICKETID"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            AuthenticateRequest()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnDeclineStatistics_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeclineStatistics.Click
        HandleFailureRedirect(True)
    End Sub

    Private Sub btnSubmitStatistics_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitStatistics.Click
        TicketRepository.DeleteTicket(ConfigurationManager.AppSettings("ConnectionString").ToString(), Guid.Parse(ViewState(TICKETID_VIEWSTATEKEY).ToString()))
        InsertStatisticalDataRecord()
        pnlSubmit.Visible = False
        pnlSubmitted.Visible = True
        hlkHomePage.NavigateUrl = ConfigurationManager.AppSettings("DefaultDomain")
    End Sub
#End Region

#Region "Page Methods"
    Private Sub AuthenticateRequest()
        If (String.IsNullOrEmpty(Request.QueryString("ticketid"))) Then ' No querystring
            HandleFailureRedirect(False)
        End If

        Dim testVal As Guid = Guid.Empty
        If Not (Guid.TryParse(Request.QueryString("ticketid"), testVal)) Then   ' Invalid guid
            HandleFailureRedirect(False)
        End If

        ViewState(TICKETID_VIEWSTATEKEY) = testVal
        Dim ticket As Ticket = TicketRepository.GetTicket(ConfigurationManager.AppSettings("ConnectionString").ToString(), testVal)
        If (ticket Is Nothing) Then ' Does not exist
            HandleFailureRedirect(False)
        End If
        If Not (ticket.Id = testVal) Then ' Came back null
            HandleFailureRedirect(False)
        End If
        If (ticket.DateGenerated <= DateTime.Now.AddDays(-10)) Then  ' Has expired
            HandleFailureRedirect(True)
        End If
        If (ticket.TableFkId.HasValue And ticket.TableFkRowGuid.HasValue) Then
            If Not (ticket.TableFkId.Value = 31) Then   ' Not a supported tablefkid (JOB)
                HandleFailureRedirect(True)
            End If
            Dim jobRowGuidTestVal As Guid = Guid.Empty
            If (Guid.TryParse(ticket.TableFkRowGuid.Value.ToString(), jobRowGuidTestVal)) Then
                Dim job As Job = JobRepository.GetJob(ConfigurationManager.AppSettings("ConnectionString").ToString(), jobRowGuidTestVal)
                If Not (job Is Nothing) Then
                    divJobTitle.Visible = Not (String.IsNullOrEmpty(job.Title))
                    litJobTitle.Text = job.Title
                End If
            End If
        End If
    End Sub

    Private Sub InsertStatisticalDataRecord()
        Dim jobApplicantStatistic As JobApplicantStatistic = New JobApplicantStatistic()
        jobApplicantStatistic.ApplicantName = SQLStringWithOutSingleQuotes(String.Concat(txtFirstName.Text, " ", txtLastName.Text))
        jobApplicantStatistic.JobTitle = IIf(divJobTitle.Visible, litJobTitle.Text.Trim(), String.Empty)
        jobApplicantStatistic.Gender = rblGender.SelectedValue
        jobApplicantStatistic.Ethnicity = GetBitmaskSum()
        JobApplicantStatisticRepository.InsertJobStatistic(ConfigurationManager.AppSettings("ConnectionString").ToString(), jobApplicantStatistic)
    End Sub

    Private Function GetBitmaskSum() As Int32
        Dim retVal As Int32 = 0
        For Each li As ListItem In cblEthnicity.Items
            If (li.Selected) Then
                retVal += Int32.Parse(li.Value)
            End If
        Next
        Return retVal
    End Function

    Private Sub HandleFailureRedirect(ByVal deleteTicket As Boolean)
        If (deleteTicket) Then TicketRepository.DeleteTicket(ConfigurationManager.AppSettings("ConnectionString").ToString(), Guid.Parse(ViewState(TICKETID_VIEWSTATEKEY).ToString()))
        Response.Redirect(ConfigurationManager.AppSettings("DefaultDomain"))
    End Sub
#End Region
End Class