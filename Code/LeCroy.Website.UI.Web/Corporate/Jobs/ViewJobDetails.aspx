﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Corporate_Jobs_ViewJobDetails" Codebehind="ViewJobDetails.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="<%=rootDir %>/corporate/">Company Profile</a></li>
        <li class="current"><a href="default.aspx">Career Opportunities</a>
            <ul>
                <li class="current"><a href="default.aspx?flag=career">Careers</a></li>
                <li><a href="default.aspx?flag=culture">Our Culture</a></li>
                <li><a href="default.aspx?flag=history">History</a></li>
                <li><a href="default.aspx?flag=life">Life Style & Location</a></li>
                <li><a href="default.aspx?flag=benefit">Benefits</a></li>
                <li><a href="default.aspx?flag=college">Colleges</a></li>
            </ul>
        </li>
        <li><a href="#">Press Releases</a></li>
        <li><a href="<%=rootDir %>/Events/">World Wide Events</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
		<% showJobDetail() %><br />
        <p><i><b>Teledyne is an Equal Opportunity/Affirmative Action employer.</b> All qualified applicants will receive consideration for employment without regard to race, color, religion, religious creed, gender, sexual orientation, gender identity, gender expression, transgender, pregnancy, marital status, national origin, ancestry, citizenship status, age, disability, protected Veteran Status, genetics or any other characteristic protected by applicable federal, state, or local law. To see our Policy on EEO <a href="./2018-reaffirmation-eeo.pdf" target="_blank">click here</a>. You may also view the EEO is the Law Poster by <a href="https://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank">clicking here</a> and its <a href="./eeo-supplement.pdf" target="_blank">supplement</a>.</i></p>
        <p>In compliance with the ADA Amendments Act (ADAAA), if you have a disability and would like to request an accommodation in order to apply for this position with Teledyne LeCroy, please email <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a> or call (845) 425-2000 and ask to speak with Human Resources. Determination on requests for reasonable accommodation will be made on a case-by-case basis. Please note that only those inquiries concerning a request for reasonable accommodation will receive a response.<br /><br />
        Teledyne LeCroy is a participant in E-Verify (<a href="./e-verify_participation_poster_english.pdf" target="_blank">English</a> | <a href="./e-verify_participation_poster_spanish.pdf" target="_blank">Spanish</a>) and <a href="./pay-transparency.pdf" target="_blank">Pay Transparency</a>. Details on Right To Work may be found in <a href="./e-verify_right_to_work_poster_english.pdf" target="_blank">English</a> and <a href="./e-verify_right_to_work_poster_spanish.pdf" target="_blank">Spanish</a>.</p>
    </div>
	<div style="background-color: #ffffff;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Image ID="imgResumeMail" runat="server" BorderWidth="0" Height="8" ImageUrl="~/corporate/jobs/Images/mail.gif" Width="15" />&nbsp;
            <asp:LinkButton ID="btnApply" runat="server" Text="Apply for this position<br /><br />" />
        <noscript>
            <span style="color: #ff0000; font-weight: bold;">This page requires JavaScript be enabled. Please enable JavaScript on your browser and reload the page.</span><br /><br />
        </noscript>
        <span id="spanApply"></span>
	</div>

    <asp:Literal ID="litOpenDialogJavascript" runat="server" Text="<script language='javascript' type='text/javascript'>jQuery.noConflict();jQuery(document).ready(function (jQuery) { jQuery('#dialog-confirm').dialog('open'); });</script>" Visible="false" />
    <asp:Literal ID="litCloseDialogJavascript" runat="server" Text="<script language='javascript' type='text/javascript'>jQuery.noConflict();jQuery(document).ready(function (jQuery) { jQuery('#dialog-confirm').dialog('destroy'); });</script>" Visible="false" />
    <div class="demo" style="display:none;">
        <div id="dialog-confirm" title="Voluntary Disclosure" style="background-color: #ffffff; margin: 0 0 0 0;">
            <div>
                <p><i><b>Teledyne is an Equal Opportunity/Affirmative Action employer.</b> All qualified applicants will receive consideration for employment without regard to race, color, religion, religious creed, gender, sexual orientation, gender identity, gender expression, transgender, pregnancy, marital status, national origin, ancestry, citizenship status, age, disability, protected Veteran Status, genetics or any other characteristic protected by applicable federal, state, or local law. To see our Policy on EEO <a href="./2018-reaffirmation-eeo.pdf" target="_blank">click here</a>. You may also view the EEO is the Law Poster by <a href="https://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank">clicking here</a>.</i></p>
                <p>Various government agencies request statistical information regarding our hiring practices. Your cooperation in completing this form is completely <u><b>voluntary</b></u>. Any information gathered is strictly confidential. Refusal to provide the information requested will not subject you to any retaliation or adverse treatment. Thank you for your cooperation.</p>
                <div>
                    <span><b>Please indicate your gender:</b>&nbsp;</span>
                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Text="Male" Value="M" />
                        <asp:ListItem Text="Female" Value="F" />
                    </asp:RadioButtonList>
                </div><br />
                <div>
                    <b>Please indicate your race/ethnic group (check all that apply):</b>&nbsp;
                    <asp:CheckBoxList ID="cblEthnicity" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table">
                        <asp:ListItem Text="Hispanic or Latino" Value="32" />
                        <asp:ListItem Text="White (Not Hispanic or Latino)" Value="16" />
                        <asp:ListItem Text="Black or African American (Not Hispanic or Latino)" Value="8" />
                        <asp:ListItem Text="Native Hawaiian or Other Pacific Islander (Not Hispanic or Latino)" Value="4" />
                        <asp:ListItem Text="Asian (Not Hispanic or Latino)" Value="2" />
                        <asp:ListItem Text="American Indian or Alaska Native (Not Hispanic or Latino)" Value="1" />
                    </asp:CheckBoxList>
                </div><br />
                <div>
                    <input type="button" id="btnDeclineStatistics" name="btnDeclineStatistics" style="width: 200px;" title="I prefer not to disclose" value="I prefer not to disclose" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <noscript>
                        <span style="color: #ff0000; font-weight: bold;">This page requires JavaScript be enabled. Please enable JavaScript on your browser and reload the page.</span><br /><br />
                    </noscript>
                    <asp:Button ID="btnSubmitStatistics" runat="server" CausesValidation="false" Text="Submit" Width="100" />
                </div>
                <div style="font-size: 9px;">
                    Revised 7/25/2007 as per U.S. Equal Employment Opportunity Commission Guidelines
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources">
        <asp:LinkButton ID="btnApplyTop" runat="server" Text="-&nbsp;Apply for this Job" /><br /><br />
        <asp:LinkButton ID="lb_print" runat="server"> - Printer-friendly Page </asp:LinkButton>
    </div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>