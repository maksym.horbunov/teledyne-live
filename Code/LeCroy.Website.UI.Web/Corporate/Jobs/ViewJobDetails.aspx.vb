﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Corporate_Jobs_ViewJobDetails
    Inherits BasePage

    Dim sql As String = ""
    Dim ds As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Dim _mailToAddress As String = "teledynerecruitment@teledyne.com"    ' Default if fetch fails
    Dim _resetToZero As Boolean = True

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "About Us"
        Me.Master.PageContentHeader.BottomText = "Career Opportunities"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'If Not (IsPostBack) Then
        '    pnlSubmitted.Visible = False
        '    pnlSubmit.Visible = False
        'End If
        'litJavascript.Text = GetJavascriptPositionString(False)
        'InjectExternalResourcesIntoMaster()
    End Sub
#End Region

#Region "Control Events"
    'Protected Sub btnApplyTop_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApplyTop.Click
    '    btnApply_Click(Nothing, Nothing)
    '    HandleJavascript(False, False)
    'End Sub

    'Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApply.Click
    '    HandleExternalJobLocationRedirect()
    '    pnlSubmitted.Visible = False
    '    pnlSubmit.Visible = True
    '    btnApply.Visible = False
    '    imgResumeMail.Visible = False
    '    HandleJavascript(False, False)
    'End Sub

    'Protected Sub cvEmailAddress_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmailAddress.ServerValidate
    '    If (String.IsNullOrEmpty(txtEmail.Text.Trim())) Then
    '        args.IsValid = False
    '        cvEmailAddress.ErrorMessage = "Email address is required<br />"
    '        Return
    '    End If
    '    If Not (String.Compare(txtEmail.Text.Trim(), txtConfirmEmail.Text.Trim(), True) = 0) Then
    '        args.IsValid = False
    '        cvEmailAddress.ErrorMessage = "Emails do not match<br />"
    '        Return
    '    End If
    '    If Not (BLL.Utilities.checkValidEmail(txtEmail.Text.Trim())) Then
    '        args.IsValid = False
    '        cvEmailAddress.ErrorMessage = "Invalid email format<br />"
    '        Return
    '    End If
    '    args.IsValid = True
    'End Sub

    'Protected Sub cvResume_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvResume.ServerValidate
    '    cvResume.ErrorMessage = String.Empty
    '    If Not (fuResume.HasFile) Then
    '        args.IsValid = False
    '        cvResume.ErrorMessage = "Please enter your resume<br />"
    '        Return
    '    End If
    '    If (fuResume.PostedFile.ContentLength <= 0) Then
    '        args.IsValid = False
    '        cvResume.ErrorMessage = "Please enter your resume<br />"
    '        Return
    '    End If
    '    If Not (fuResume.PostedFile.ContentType.ToLower() = "application/pdf" Or
    '            fuResume.PostedFile.ContentType.ToLower() = "application/msword" Or
    '            fuResume.PostedFile.ContentType.ToLower() = "application/rtf" Or
    '            fuResume.PostedFile.ContentType.ToLower() = "text/plain" Or
    '            fuResume.PostedFile.ContentType.ToLower() = "application/vnd.openxmlformats-officedocument.wordprocessingml.document") Then
    '        args.IsValid = False
    '        cvResume.ErrorMessage = "Please provide a resume in either a PDF, MS Word, or text format<br />"
    '        Return
    '    End If
    '    args.IsValid = True
    'End Sub

    'Protected Sub cvDegree_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvDegree.ServerValidate
    '    If (ddlDegree.SelectedIndex <= 0) Then
    '        args.IsValid = False
    '        cvDegree.ErrorMessage = "Please select a degree<br />"
    '        Return
    '    End If
    '    args.IsValid = True
    'End Sub

    'Private Sub cvAuthorized_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvAuthorized.ServerValidate
    '    If (rblAuthorized.SelectedIndex < 0) Then
    '        args.IsValid = False
    '        cvAuthorized.ErrorMessage = "Please select an answer<br />"
    '        Return
    '    End If
    '    args.IsValid = True
    'End Sub

    'Private Sub cvSponsorship_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvSponsorship.ServerValidate
    '    If (rblSponsorship.SelectedIndex < 0) Then
    '        args.IsValid = False
    '        cvSponsorship.ErrorMessage = "Please select an answer<br />"
    '        Return
    '    End If
    '    args.IsValid = True
    'End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
    '    HandleExternalJobLocationRedirect()
    '    lblErrorMessage.Text = String.Empty
    '    cvResume.Validate()
    '    If (cvResume.IsValid) Then
    '        Page.Validate()
    '        If (Page.IsValid) Then
    '            SubmitApplication()
    '        Else
    '            cvResume.IsValid = False
    '            cvResume.ErrorMessage = "Resumes must be resubmitted on any error<br />"
    '            HandleJavascript(False, False)
    '        End If
    '    Else
    '        Page.Validate()
    '        HandleJavascript(False, False)
    '    End If
    'End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
    '    pnlSubmit.Visible = False
    '    imgResumeMail.Visible = True
    '    btnApply.Visible = True
    '    HandleJavascript(True, False)
    'End Sub

    'Private Sub btnSubmitStatistics_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitStatistics.Click
    '    If (String.IsNullOrEmpty(hdnName.Value)) Or (String.IsNullOrEmpty(hdnJobCode.Value)) Then
    '    Else
    '        Try
    '            Dim sqlString As String = "INSERT INTO [JOB_APPLICANT_STATISTIC] ([ApplicantName], [JobTitle], [Gender], [Ethnicity]) VALUES (@APPLICANTNAME, @JOBTITLE, @GENDER, @ETHNICITY)"
    '            Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
    '            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@APPLICANTNAME", SQLStringWithOutSingleQuotes(hdnName.Value)))
    '            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@JOBTITLE", SQLStringWithOutSingleQuotes(hdnJobCode.Value)))
    '            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@GENDER", rblGender.SelectedValue))
    '            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@ETHNICITY", GetBitmaskSum().ToString()))
    '            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    '        Catch
    '        End Try
    '    End If
    '    litCloseDialogJavascript.Visible = True
    '    HandleJavascript(False, False)
    'End Sub

    Protected Sub lb_print_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lb_print.Click
        If Not Request("id") Is Nothing Then
            Response.Redirect("printerfriendly.aspx?id=" & Request("id").ToString())
        Else
            Response.Redirect(rootDir & "/Default.aspx")
        End If
    End Sub
#End Region

#Region "Page Methods"
    'Private Sub InjectExternalResourcesIntoMaster()
    '    Dim css As HtmlLink = New HtmlLink()
    '    css.Href = ResolveUrl(ConfigurationManager.AppSettings("CssJQueryUiJob"))
    '    css.Attributes.Add("rel", "stylesheet")
    '    css.Attributes.Add("type", "text/css")
    '    CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(css)
    '    Dim jQueryUIMin As HtmlGenericControl = New HtmlGenericControl("script")
    '    jQueryUIMin.Attributes.Add("type", "text/javascript")
    '    jQueryUIMin.Attributes.Add("language", "javascript")
    '    jQueryUIMin.Attributes.Add("src", ConfigurationManager.AppSettings("JsJQueryUIJobDetails"))
    '    CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(jQueryUIMin)
    'End Sub

    'Private Sub HandleJavascript(ByVal resetValue As Boolean, ByVal trackSubmit As Boolean)
    '    Return
    '    _resetToZero = resetValue
    '    litJavascript.Text = GetJavascriptPositionString(trackSubmit)
    '    litJavascript.Visible = True
    'End Sub

    'Private Function GetJavascriptPositionString(ByVal trackSubmit As Boolean) As String
    '    Dim sb As StringBuilder = New StringBuilder()
    '    sb.Append(String.Format("<script type=""text/javascript"" language=""javascript"">{0}", Environment.NewLine))
    '    sb.Append(String.Format("var hdnPosition = document.getElementById('{0}');{1}", hdnPosition.ClientID, Environment.NewLine))
    '    sb.Append(String.Format("var txtFirstName = document.getElementById('{0}');{1}", txtFirstName.ClientID, Environment.NewLine))
    '    If (_resetToZero) Then
    '        sb.Append(String.Format("window.scrollTo(0, 0);{0}", Environment.NewLine))
    '    Else
    '        sb.Append(String.Format("window.scrollTo(0, hdnPosition.value);{0}", Environment.NewLine))
    '        sb.Append(String.Format("if (txtFirstName != null) txtFirstName.focus();{0}", Environment.NewLine))
    '    End If
    '    If (trackSubmit) Then
    '        If (IsIndeedCampaign()) Then
    '            Dim cpcString As String = String.Empty
    '            If (IsIndeedCPC()) Then
    '                cpcString = " (CPC)"
    '            End If
    '            sb.Append(String.Format("ga('send', 'event', 'Indeed{3}', 'Successful_Resume_Submit', '{0}|{1}', {{'nonInteraction': 1}});{2}", txtLastName.Text.Trim(), hdnJobCode.Value, Environment.NewLine, cpcString))
    '        Else
    '            sb.Append(String.Format("ga('send', 'event', 'Resumes', 'Successful_Resume_Submit', '{0}|{1}', {{'nonInteraction': 1}});{2}", txtLastName.Text.Trim(), hdnJobCode.Value, Environment.NewLine))
    '        End If
    '    End If
    '    sb.Append("</script>")
    '    Return sb.ToString()
    'End Function

    Protected Sub showJobDetail()
        Dim JobID As Integer
        If Len(Request.QueryString("id")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("id")) Then
                JobID = Request.QueryString("id")
            Else
                Response.Redirect("default.aspx")
            End If
        End If

        If Len(JobID) > 0 Then
            Dim strResponsibilities As String = ""
            Dim strExperience As String = ""
            sql = "SELECT * FROM JOB WHERE JOB_ID = @JOBID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@JOBID", JobID.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If Not (dr("ExternalUrl") Is DBNull.Value) Then
                    btnApply.Attributes.Add("onclick", String.Format("javascript:window.open('{0}'); return false;", dr("ExternalUrl").ToString()))
                    btnApplyTop.Attributes.Add("onclick", String.Format("javascript:window.open('{0}'); return false;", dr("ExternalUrl").ToString()))
                End If
                If Not (dr("MAILTO") Is DBNull.Value) Then
                    If Not (String.IsNullOrEmpty(dr("MAILTO"))) Then
                        _mailToAddress = dr("MAILTO").ToString()
                    End If
                End If
                If (dr("VIEWABLE") Is DBNull.Value) Then
                    Response.Redirect("~/corporate/jobs/?capid=107&mid=552")
                ElseIf Not (String.Compare(dr("VIEWABLE").ToString(), "on", True) = 0) Then
                    Response.Redirect("~/corporate/jobs/?capid=107&mid=552")
                End If
                Response.Write("<table border=""0"" width=""100%"" cellpadding=""2"" cellspacing=""5"">")
                Response.Write("<tr>")
                Response.Write("<td colspan=2 valign=top>")
                Response.Write("<a href=""default.aspx?flag=career"">")
                Response.Write("Back to Career Listings")
                Response.Write("</a><br><br>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<table cellpadding =0 cellspacing =0 border =0>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<nobr><b>Position Title : &nbsp;</b>" & dr("TITLE") & "</nobr>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Department :&nbsp;</b>" & dr("DEPT"))
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b><nobr>Position Type :&nbsp;</b>" & dr("POSITION_TYPE") & "</nobr>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Location : &nbsp;</b>" & dr("Location"))
                Response.Write("</td>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Job Code :&nbsp;</b>" & dr("CODE"))
                'hdnJobCode.Value = String.Format("{0} ({1}) - {2}", dr("TITLE").ToString(), dr("CODE").ToString(), dr("LOCATION").ToString())
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("</table>")
                Response.Write("</td>")
                Response.Write("<td valign = top align=right>&nbsp;")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td colspan=2 valign=top>")
                Response.Write(GetLongDescription("JOB", "DESCRIPTION", dr("JOB_ID")))
                Response.Write("</td>")
                Response.Write("</tr>")
                strResponsibilities = GetLongDescription("JOB", "RESPONSIBILITY", dr("JOB_ID"))
                If Len(strResponsibilities) > 0 Then
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write("<b>Responsibilities:</b>")
                    Response.Write("</td>")
                    Response.Write("</tr>")
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write(strResponsibilities)
                    Response.Write("</td>")
                    Response.Write("</tr>")
                End If
                strExperience = GetLongDescription("JOB", "EXPERIENCE", dr("JOB_ID"))
                If Len(strExperience) > 0 Then
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write("<b>Requirements:</b>")
                    Response.Write("</td>")
                    Response.Write("</tr>")
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write(strExperience)
                    Response.Write("</td>")
                    Response.Write("</tr>")
                End If
                'Response.Write("<tr>")
                'Response.Write("<td height=""30"" valign=""bottom"" colspan=""2""><nobr>")
                'Response.Write("<a href=""mailto:" & dr("MAILTO") & """ class=AWN>")
                'Response.Write("<img border=""0"" src=""/corporate/jobs/Images/mail.gif"" width=""15"" height=""8"">")
                'Response.Write("</a>")
                'Response.Write("Send your resume to <a href=""mailto:" & dr("MAILTO") & """ class=AWN>")
                'Response.Write(dr("MAILTO"))
                'Response.Write("</a>")
                'Response.Write("</nobr></td>")
                'Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td colspan=2>&nbsp;&nbsp;</td>")
                Response.Write("</tr>")
                Response.Write("</table>")
            Else
                Response.Write("Jobs are currently being updated, please check back soon.")
            End If
        End If
    End Sub

    Private Function GetLongDescription(ByVal tablename As String, ByVal columnname As String, ByVal idintable As String) As String
        Dim LongDescription As String = ""
        If Len(tablename) > 0 And Len(columnname) > 0 And Len(idintable) > 0 Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sql = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            sqlParameters.Add(New SqlParameter("@TABLENAME", tablename))
            Dim TableID As String = ""
            Dim ColumnID As String = ""
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                TableID = dr("Table_ID")
            End If

            sql = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", columnname))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                ColumnID = dr("Column_ID")
            End If

            sql = "Select TEXT from LONGDESCR where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@IDINTABLE Order by SEQ_ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
            sqlParameters.Add(New SqlParameter("@COLUMNID", ColumnID))
            sqlParameters.Add(New SqlParameter("@IDINTABLE", idintable))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    LongDescription = LongDescription & dr("TEXT")
                Next
            End If
        End If
        Return LongDescription
    End Function

    'Private Sub SubmitApplication()
    '    Dim sb As StringBuilder = New StringBuilder()
    '    hdnName.Value = String.Format("{0} {1}{2}", txtFirstName.Text.Trim(), IIf(Not (String.IsNullOrEmpty(txtMiddleName.Text.Trim())), String.Concat(txtMiddleName.Text.Trim(), " "), String.Empty), txtLastName.Text.Trim())
    '    sb.Append(String.Format("Name: {0} {1}{2}{3}", txtFirstName.Text.Trim(), IIf(Not (String.IsNullOrEmpty(txtMiddleName.Text.Trim())), String.Concat(txtMiddleName.Text.Trim(), " "), String.Empty), txtLastName.Text.Trim(), Environment.NewLine))
    '    sb.Append(String.Format("Email: {0}{1}", txtEmail.Text.Trim(), Environment.NewLine))
    '    sb.Append(String.Format("Phone: {0}{1}", txtPhoneNumber.Text.Trim(), Environment.NewLine))
    '    sb.Append(String.Format("Major: {0}{1}", txtMajor.Text.Trim(), Environment.NewLine))
    '    sb.Append(String.Format("Degree: {0}{1}", ddlDegree.SelectedItem.Text, Environment.NewLine))
    '    sb.Append(String.Format("Authorized to work in the U.S.: {0}{1}", rblAuthorized.SelectedValue, Environment.NewLine))
    '    sb.Append(String.Format("Sponsorhip required: {0}{1}{1}", rblSponsorship.SelectedValue, Environment.NewLine))
    '    If Not (String.IsNullOrEmpty(txtCoverLetter.Text.Trim())) Then
    '        sb.Append(txtCoverLetter.Text.Trim())
    '    End If

    '    Dim success As Boolean = False
    '    Try
    '        Dim fromAddress As MailAddress = New MailAddress("teledynerecruitment@teledyne.com", "Teledyne LeCroy Website - HR Web Application")
    '        Dim toAddress As MailAddress = New MailAddress(_mailToAddress, _mailToAddress)
    '        Dim mm As MailMessage = New MailMessage(fromAddress, toAddress)
    '        mm.Subject = String.Format("Job Application For {0}", hdnJobCode.Value)
    '        mm.Body = sb.ToString()
    '        Dim attachment As Attachment = New Attachment(fuResume.PostedFile.InputStream, fuResume.PostedFile.FileName) 'String.Format("Resume_{0}{1}.", txtFirstName.Text.Trim(), txtLastName.Text.Trim()))
    '        mm.Attachments.Add(attachment)
    '        Dim smtpClient As SmtpClient = New SmtpClient(ConfigurationManager.AppSettings("EmailHost"), Convert.ToInt32(ConfigurationManager.AppSettings("Port")))
    '        smtpClient.Send(mm)
    '        success = True
    '    Catch ex As Exception
    '        Functions.ErrorHandlerLog(True, String.Format("HR Web Application Submit Failure: Exception: {0} | StackTrace: {1} | Source: {2}", ex.Message, ex.StackTrace, ex.Source))
    '    End Try

    '    HandleJavascript(False, success)
    '    If (success) Then
    '        pnlSubmit.Visible = False
    '        pnlSubmitted.Visible = True
    '        txtFirstName.Text = String.Empty
    '        txtMiddleName.Text = String.Empty
    '        txtLastName.Text = String.Empty
    '        txtEmail.Text = String.Empty
    '        txtConfirmEmail.Text = String.Empty
    '        txtPhoneNumber.Text = String.Empty
    '        txtCoverLetter.Text = String.Empty
    '        txtMajor.Text = String.Empty
    '        ddlDegree.ClearSelection()
    '        litOpenDialogJavascript.Visible = True
    '        HandleJavascript(False, False)
    '    Else
    '        lblErrorMessage.Text = "We're sorry, but there was an unexpected error while processing your application.  Please try again."
    '    End If
    'End Sub

    Private Function IsIndeedCampaign() As Boolean
        Try
            If Not (String.IsNullOrEmpty(Request.QueryString("utm_source"))) Then
                Return String.Compare(Request.QueryString("utm_source"), "indeed", True) = 0
            End If
        Catch
        End Try
        Return False
    End Function

    Private Function IsIndeedCPC() As Boolean
        If Not (IsIndeedCampaign()) Then
            Return False
        End If
        Try
            If Not (String.IsNullOrEmpty(Request.QueryString("utc_medium"))) Then
                Return String.Compare(Request.QueryString("utc_medium"), "cpc", True) = 0
            End If
        Catch
        End Try
        Return False
    End Function

    Private Function GetBitmaskSum() As Int32
        Dim retVal As Int32 = 0
        For Each li As ListItem In cblEthnicity.Items
            If (li.Selected) Then
                retVal += Int32.Parse(li.Value)
            End If
        Next
        Return retVal
    End Function

    Private Sub HandleExternalJobLocationRedirect()
        Dim jobId As Int32 = ValidateQueryString()
        If (jobId <= 0) Then
            Response.Redirect("default.aspx")
        End If

        Dim job As Job = JobRepository.GetJob(ConfigurationManager.AppSettings("ConnectionString").ToString(), jobId)
        If (job Is Nothing) Then
            Response.Redirect("default.aspx")
        End If
        If Not (String.IsNullOrEmpty(job.ExternalUrl)) Then
            Response.Redirect(job.ExternalUrl)
        End If
    End Sub

    Private Function ValidateQueryString() As Int32
        If (String.IsNullOrEmpty(Request.QueryString("id"))) Then
            Response.Redirect("default.aspx")
        End If
        Dim testVal As Int32 = 0
        If Not (Int32.TryParse(Request.QueryString("id"), testVal)) Then
            Response.Redirect("default.aspx")
        End If
        Return testVal
    End Function
#End Region
End Class