<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Corporate_Jobs_Careers" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="<%= selectClass1 %>"><a href="default.aspx?flag=career<%=menuURL %>">Job Openings</a></li>
                <li class="<%= selectClass2 %>"><a href="default.aspx?flag=culture<%=menuURL %>">Our Culture</a></li>
                <li class="<%= selectClass3 %>"><a href="default.aspx?flag=history<%=menuURL %>">History</a></li>
                <li class="<%= selectClass4 %>"><a href="default.aspx?flag=life<%=menuURL %>">Life Style & Location</a></li>
                <li class="<%= selectClass6 %>"><a href="default.aspx?flag=college<%=menuURL %>">Colleges</a></li>
            </ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <asp:Panel ID="career" runat="server" Visible="false" EnableViewState="False">
            <p>With our <b>high-tech work environment and leading-edge products</b>, we resemble a recent start-up. Yet with our savvy leveraging of core technologies, significant R&amp;D investments, and increasing annual sales revenues, we've clearly demonstrated our financial stability and staying power. We've proven ourselves smart beyond our years and agile enough to respond quickly to marketplace conditions. This winning combination makes working at Teledyne LeCroy compelling, challenging, and most satisfying.</p>
            <p>We invite you to learn more about exciting career opportunities at Teledyne LeCroy and become part of our winning team.</p>
            <p>For <a href="default.aspx?flag=college">College Recruiting, please click on this link.</a></p><br /><br />
            <%showJobList()%><br />
            <p><i><b>Teledyne is an Equal Opportunity/Affirmative Action employer.</b> All qualified applicants will receive consideration for employment without regard to race, color, religion, religious creed, gender, sexual orientation, gender identity, gender expression, transgender, pregnancy, marital status, national origin, ancestry, citizenship status, age, disability, protected Veteran Status, genetics or any other characteristic protected by applicable federal, state, or local law. To see our Policy on EEO <a href="./2018-reaffirmation-eeo.pdf" target="_blank">click here</a>. You may also view the EEO is the Law Poster by <a href="https://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank">clicking here</a> and its <a href="./eeo-supplement.pdf" target="_blank">supplement</a>.</i></p>
            <p>In compliance with the ADA Amendments Act (ADAAA), if you have a disability and would like to request an accommodation in order to apply for this position with Teledyne LeCroy, please email <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a> or call (845) 425-2000 and ask to speak with Human Resources. Determination on requests for reasonable accommodation will be made on a case-by-case basis. Please note that only those inquiries concerning a request for reasonable accommodation will receive a response.<br /><br />
            Teledyne LeCroy is a participant in E-Verify (<a href="./e-verify_participation_poster_english.pdf" target="_blank">English</a> | <a href="./e-verify_participation_poster_spanish.pdf" target="_blank">Spanish</a>) and <a href="./pay-transparency.pdf" target="_blank">Pay Transparency</a>. Details on Right To Work may be found in <a href="./e-verify_right_to_work_poster_english.pdf" target="_blank">English</a> and <a href="./e-verify_right_to_work_poster_spanish.pdf" target="_blank">Spanish</a>.</p>
            <p><b>Stay Safe From Recruitment Fraud!</b> Apply for positions with Teledyne directly at <a href="https://globalcareers-teledyne.icims.com" target="_blank">https://globalcareers-teledyne.icims.com</a>.</p>
            <p><b>How To Identify Recruitment Fraud</b><br /> 
            It has been brought to our attention that there have been instances of fraudulent job offers purporting to be from Teledyne Technologies and/or its affiliates ("Teledyne"). These persons have been offering fraudulent employment opportunities to applicants and often asking for sensitive personal and financial information. Recruitment fraud is a sophisticated scam offering fake job opportunities to job seekers. This type of fraud is normally carried out through online services such as false websites, or through fake e-mails claiming to be from the company. The fraudsters often request recipients to provide personal information and to make payments as part of their fake recruiting process. <b>Teledyne does not collect any financial commitment from candidates as a pre-employment requirement.</b><br /><br />
            Fraudulent job offers often ask for financial or personal information early in the process. <b>Teledyne does not ask for any financial commitment or contribution from a candidate at any stage of the recruitment process.</b> A fraudulent offer may ask you to book travel through a certain travel agency with the promise to reimburse travel costs.<br /><br />
            Teledyne has no responsibility for fraudulent offers and advises candidates to follow the guidance provided above.</p>
        </asp:Panel>
        <asp:Panel ID="culture" runat="server" Visible="false" EnableViewState="False">
            <strong>Our Future, Your Future</strong>
            <p>We've made tremendous marketplace strides in the last couple of years - doubling our share in the digital oscilloscope market and positioning ourselves to leverage our technology to enter new markets. Our pace of innovation has increased. And the market for our key strength - high-end test and measurement instrumentation - has heated up. Teledyne LeCroy is in a strong growth mode now, as more and more electronic design companies throughout the world specify Teledyne LeCroy instruments as their enabling technology. It's a great time to join us - the 50-year-old start-up!</p>
            <p>Teledyne LeCroy has invested heavily in the right environment for people who like to get things done. Our work areas are state-of-the-art, providing the latest computers and best software, internet access, wireless conference centers and video conferencing to make communication with remote colleagues a snap. Educational opportunities such as paid tuition and "Lunch & Learn" programs are standard fare. When it's time to decompress a fast game of darts in the Engineering Lounge or a workout in our fully-equipped weight and cardio-exercise gym takes the edge off.</p>
            <table cellspacing="1" cellpadding="0" width="100%" border="0">
                <tbody>
                    <tr>
                        <td width="100%">
                            <strong>A Non-Corporate Corporate Culture</strong>
                            <p>Developing a highly productive, unbureaucratic culture in a technology-based industry is a subtle and complex process. One element that contributes to our can-do atmosphere is the high percentage of engineers in management. Another is the management style itself, which encourages personal growth and success right along with company success.</p>
                            <p>Teledyne LeCroy is a strong, talented technical community known for its creative, energized sales meetings, summer picnics, frequent barbecue & volleyball get-togethers, and pizza lunches. Our employees are the cr�me de la cr�me of the industry, and they lead full, interesting lives. Outside of work, their achievements - whether as Little League coaches, ice hockey players, rock climbers, or writers of technical articles - are often as spectacular as their successes on the job.</p>
                            <p><strong>A Passion For Technology</strong></p>
                            <p>Teledyne LeCroy has made a solid corporate commitment to investing substantially in Research &amp; Development. We work in an environment that offers the latest in education, equipment, and software, and supports extreme achievements.</p>
                            <p>Teledyne LeCroy engineers are real &quot;R&amp;D adventurers,&quot; a select group of the best and the brightest who tame and define the coming waves of technology. We define &quot;What's next?&quot; We keep a step ahead of the high-tech pack - right on the edge of the leading edge.</p>
                            <p>No one wants to feel like a cog in the machine. At Teledyne LeCroy, a true sense of job satisfaction comes from seeing a new project through from inception to manufacture. Our management style empowers individuals to attain their potential, whether pursuing research projects on their own or contributing to a team. Our emphasis on teamwork, mentoring, and delegation of responsibility results in a stimulating, collaborative atmosphere where we can all take pride in innovative jobs well done.</p>
                            <p><strong>Diversity</strong></p>
                            <p>Teledyne LeCroy is committed to actively seeking diversity in hiring and employee development. We firmly believe that the points of view brought by diverse groups, the physically challenged, and by alternative-lifestyle employees enrich our company.</p>
                            <p>Only by celebrating our differences can we build the strongest team, because a culture of diversity fosters greater productivity and creative solutions.</p>
                            <p>At Teledyne LeCroy, we believe that our policy of inclusion has contributed significantly to our continuing success in meeting the needs of the marketplace.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <asp:Panel ID="history" runat="server" Visible="false" EnableViewState="False">
            <p><strong>The LeCroy Story</strong></p>
            <p>In 1964, Walter LeCroy launched his small instrument design business in Irvington, New York. LeCroy Research Systems (LRS), as it was then known, was quickly recognized as an innovator in instrumentation. Eight years later, a second design and production facility was established in Geneva, Switzerland. By the early 1980s LRS had become LeCroy Corporation, which in 1995, completed its initial public offering (IPO) on NASDAQ.</p>
            <p>Today Teledyne LeCroy designs and manufactures high-end digital and analog oscilloscopes, high-performance PXI digitizers, differential amplifiers, probes and accessories. Electronic design companies throughout the world have selected Teledyne LeCroy instruments as their enabling technology to develop next-generation pagers, cellular phones, microprocessors, medical imaging systems, new car electronics and more. We currently employ more than 450 people worldwide and produce annual sales revenues in the area of $190 million.</p>
            <p>In November 2000 LeCroy Corporation was certified on the first official day of the newly mandated ISO9001:2000 level of quality certification. Because we have the advantages of a truly innovative spirit combined with proven staying power and financial stability, we consider ourselves a &quot;50-year-old Start-up.&quot;</p>
            <p align="center"><br /><img alt="Walter O. LeCroy, Jr. Photography" src="Images/WalterArt2.jpg" border="0" width="500" height="125" /></p>
        </asp:Panel>
        <asp:Panel ID="life" runat="server" Visible="false" EnableViewState="False">
            <p>Our Corporate Headquarters, based in Rockland County, New York, is where oscilloscopes, signal analysis systems, modular digitizers and digitizing components are designed and manufactured.</p>
            <p>We are nestled in the beautiful Hudson Valley. On one side, we are less than an hour from the fabulous urban amenities of New York City, and on the other, minutes from 600 lakes and ponds and over 34,000 acres of parkland including two large state recreational areas.</p>
            <p><strong><img alt="Rockland County" src="Images/Location_2.jpg" border="0" width="633" height="144" /><br /></strong>Rockland County is more diverse and sophisticated than almost any metropolitan area outside of New York City. Its restaurants produce markets, festivals, musical events and places of worship reflect Arab, Caribbean, Greek, Indian, Irish, Italian, Jewish as well as multiple Asian and Hispanic cultural communities.</p>
            <p>
                <strong><img alt="Hudson River Valley and the Northeast" src="Images/Location_3.jpg" border="0" width="633" height="168" /><br /></strong>
                In the Northeast, we enjoy four distinct seasons, and our fall foliage is not to be missed. Within easy driving distance lie an abundance of ski resorts, ocean beaches, climbable mountains and sailable rivers. Travel to anywhere in the world is facilitated by four major airports supporting all the main airlines.<br /><br />
                A short trip down the Hudson River from Rockland County brings you to New York City. Manhattan, the crown jewel in this cluster of five boroughs, is accessible from many points north and west - by car, train or ferry. Visit any of its world famous museums or take in a Broadway show. How about a fine dinner at one of Manhattan's many restaurants or a walk in Central Park, The Lincoln Center complex of performance spaces present the nation's finest in opera, ballet, classical music, and theater.<br /><br />
                For additional information, please contact the Rockland County Office of Tourism at 800-295-5723.
            </p>
        </asp:Panel>
        <asp:Panel ID="college" runat="server" Visible="false" EnableViewState="False">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" align="left" width="600">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td valign="top">
                                        <p>Teledyne LeCroy has a challenging, exciting environment where you can learn new skills and fine tune existing ones by working on cutting-edge projects with talented people.</p>
                                        <p>We are actively recruiting new college graduates. We recruit based on specific openings however we are always open to meeting fresh, talented Electrical Engineers for our Research &amp; Development and US Sales Organizations. Each year we hire college graduates for full-time positions. All positions at Teledyne LeCroy offer you a chance to have an immediate impact on our business and on your career. Interested?</p>
                                        <p>Check out our <a href="?flag=college2">Campus Schedule</a> to find out when we'll be at your school.</p>
                                        <p><br /></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td valign="top" width="20"></td>
                </tr>
                <tr>
                    <td valign="top"><p align="center"><a href="mailto:teledynerecruitment@teledyne.com"><img height="8" src="images/mail.gif" width="15" border="0" /></a> Send your resume to <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a></p></td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="college2" runat="server" Visible="false" EnableViewState="False">
            Teledyne LeCroy participates in several campus events during the school year. We continue to visit the following universities:<br />
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td>
                            <ul>
                                <li>Stevens Institute of Technology </li>
                                <li>Lehigh University </li>
                                <li>Cooper Union </li>
                                <li>Columbia University </li>
                                <li>Rensselaer Polytechnic Institute </li>
                                <li>Rochester Institute of Technology</li>
                                <li>Princeton University </li>
                                <li>Cornell University </li>
								<li>Stanford University</li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            Job postings and resume drop boxes have been set up with several of these schools. Please see your Career Services Department for more information on Teledyne LeCroy career opportunities.<br /><br />
            <a href="default.aspx">Click here for current openings</a><br /><br />
            If you would like to be considered for an entry level opportunity, please submit your resume to <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a>.<br />
        </asp:Panel>
    </div><br /><br />
</asp:Content>