﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Corporate_Jobs_Careers
    
    '''<summary>
    '''lb_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lb_leftmenu As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''pn_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftmenu As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''career control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents career As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''culture control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents culture As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''history control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents history As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''life control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents life As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''college control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents college As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''college2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents college2 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As LeCroy.Website.MasterPage_twocolumn_left
        Get
            Return CType(MyBase.Master,LeCroy.Website.MasterPage_twocolumn_left)
        End Get
    End Property
End Class
