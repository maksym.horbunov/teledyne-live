﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Corporate_Jobs_Careers
    Inherits BasePage
    Dim sql As String = ""
    Dim flag As String = ""
    Public Shared selectClass1
    Public Shared selectClass2
    Public Shared selectClass3
    Public Shared selectClass4
    Public Shared selectClass5
    Public Shared selectClass6
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "About Us"
        Me.Master.PageContentHeader.BottomText = "Career Opportunities"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.CAREER_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.CAREER_MENU
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID

        flag = SQLStringWithOutSingleQuotes(Request.QueryString("flag"))

        If flag = "career" Then
            Me.career.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Career Opportunities"
            selectClass1 = "current"
            selectClass2 = ""
            selectClass3 = ""
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = ""

        ElseIf flag = "culture" Then
            Me.culture.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Corporate Culture"
            selectClass1 = ""
            selectClass2 = "current"
            selectClass3 = ""
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = ""
        ElseIf flag = "history" Then
            Me.history.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Corporate History"
            selectClass1 = ""
            selectClass2 = ""
            selectClass3 = "current"
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = ""
        ElseIf flag = "life" Then
            Me.life.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Lifestyle and Locations"
            selectClass1 = ""
            selectClass2 = ""
            selectClass3 = ""
            selectClass4 = "current"
            selectClass5 = ""
            selectClass6 = ""
        ElseIf flag = "college" Then
            Me.college.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - College Recruiting"
            selectClass1 = ""
            selectClass2 = ""
            selectClass3 = ""
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = "current"
        ElseIf flag = "college2" Then
            Me.college2.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - College Recruiting"
            selectClass1 = ""
            selectClass2 = ""
            selectClass3 = ""
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = "current"
        Else
            Me.career.Visible = True
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Career Opportunities"
            selectClass1 = "current"
            selectClass2 = ""
            selectClass3 = ""
            selectClass4 = ""
            selectClass5 = ""
            selectClass6 = ""
        End If

        If Not Me.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Protected Sub showJobList()
        sql = "SELECT distinct(DEPT) FROM JOB WHERE VIEWABLE = 'ON' "
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of SqlParameter)().ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Response.Write("<table cellpadding =2 cellspacing =2 border =0>")
            For Each dr As DataRow In ds.Tables(0).Rows
                Response.Write("<tr><td valign = top colspan=5><b>" & dr("DEPT") & "</b> </td>")
                sql = "SELECT * FROM JOB WHERE VIEWABLE = 'ON' and DEPT = @DEPT ORDER BY DATE_ENTERED desc"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@DEPT", dr("DEPT").ToString()))
                Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                For Each drr As DataRow In dss.Tables(0).Rows
                    Response.Write("<tr>")
                    Response.Write("<td valign = top")
                    Response.Write("</td>")
                    Response.Write("<td colspan=""2"" valign = top>")
                    Response.Write("<a href=""ViewJobDetails.aspx?id=" & drr("JOB_ID") & """>")
                    If CStr(drr("POSITION_TYPE")) = "Full Time" Then
                        Response.Write(drr("TITLE"))
                    Else
                        Response.Write(drr("TITLE") & " (" & drr("POSITION_TYPE") & ")")
                    End If
                    Response.Write("</a>")
                    If DateDiff("d", drr("DATE_LAST_MODIFIED"), Now) < 10 Then
                        Response.Write("<b>&nbsp;&nbsp;New</b>")
                    End If
                    Response.Write("</td>")
                    Response.Write("<td valign=top >")
                    Response.Write("<nobr>" & drr("LOCATION") & "</nobr>")
                    Response.Write("</td>")
                    Response.Write("</tr>")
                Next
            Next
            Response.Write("</table><br/><br/><br/>")
        Else
            Response.Write("Jobs are currently being updated, please check back soon.")
        End If
    End Sub
End Class