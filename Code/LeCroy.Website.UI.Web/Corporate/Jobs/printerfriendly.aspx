﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="LeCroy.Website.Corporate_Jobs_printerfriendly" Codebehind="printerfriendly.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Teledyne LeCroy - Career Opportunities</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img src="<%=rootDir%>/images/tl_weblogo_white_276x60.png" border="0" /><br />
             <%  showJobDetail()%><br />
            <p><i><b>Teledyne is an Equal Opportunity/Affirmative Action employer.</b> All qualified applicants will receive consideration for employment without regard to race, color, religion, religious creed, gender, sexual orientation, gender identity, gender expression, transgender, pregnancy, marital status, national origin, ancestry, citizenship status, age, disability, protected Veteran Status, genetics or any other characteristic protected by applicable federal, state, or local law. To see our Policy on EEO <a href="./2018-reaffirmation-eeo.pdf" target="_blank">click here</a>. You may also view the EEO is the Law Poster by <a href="https://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank">clicking here</a> and its <a href="./eeo-supplement.pdf" target="_blank">supplement</a>.</i></p>
            <p>In compliance with the ADA Amendments Act (ADAAA), if you have a disability and would like to request an accommodation in order to apply for this position with Teledyne LeCroy, please email <a href="mailto:teledynerecruitment@teledyne.com">teledynerecruitment@teledyne.com</a> or call (845) 425-2000 and ask to speak with Human Resources. Determination on requests for reasonable accommodation will be made on a case-by-case basis. Please note that only those inquiries concerning a request for reasonable accommodation will receive a response.<br /><br />
            Teledyne LeCroy is a participant in E-Verify (<a href="./e-verify_participation_poster_english.pdf" target="_blank">English</a> | <a href="./e-verify_participation_poster_spanish.pdf" target="_blank">Spanish</a>) and <a href="./pay-transparency.pdf" target="_blank">Pay Transparency</a>. Details on Right To Work may be found in <a href="./e-verify_right_to_work_poster_english.pdf" target="_blank">English</a> and <a href="./e-verify_right_to_work_poster_spanish.pdf" target="_blank">Spanish</a>.</p>
        </div>
    </form>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>