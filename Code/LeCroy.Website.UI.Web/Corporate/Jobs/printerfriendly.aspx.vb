﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Corporate_Jobs_printerfriendly
    Inherits BasePage

    Dim sql As String = ""
    Dim ds As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack() Then

        End If
    End Sub

    Protected Sub showJobDetail()
        Dim JobID As Integer
        If Len(Request.QueryString("id")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("id")) Then
                JobID = Request.QueryString("id")
            Else
                Response.Redirect("default.aspx")
            End If
        End If

        If Len(JobID) > 0 Then

            Dim strResponsibilities As String = ""
            Dim strExperience As String = ""
            sql = "SELECT * FROM JOB WHERE JOB_ID = @JOBID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@JOBID", JobID.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                Response.Write("<table border=""0"" width=""100%"" cellpadding=""2"" cellspacing=""5"">")
                Response.Write("<tr>")
                Response.Write("<td colspan=2 valign=top>")
                Response.Write("<a href=""default.aspx?flag=career"">")
                Response.Write("Back to Career Listings")
                Response.Write("</a><br><br>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<table cellpadding =0 cellspacing =0 border =0>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<nobr><b>Position Title : &nbsp;</b>" & dr("TITLE") & "</nobr>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Department :&nbsp;</b>" & dr("DEPT"))
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b><nobr>Position Type :&nbsp;</b>" & dr("POSITION_TYPE") & "</nobr>")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Location : &nbsp;</b>" & dr("Location"))
                Response.Write("</td>")
                Response.Write("<tr>")
                Response.Write("<td valign=top>")
                Response.Write("<b>Job Code :&nbsp;</b>" & dr("CODE"))
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("</table>")
                Response.Write("</td>")
                Response.Write("<td valign = top align=right>&nbsp;")
                Response.Write("</td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td colspan=2 valign=top>")
                Response.Write(GetLongDescription("JOB", "DESCRIPTION", dr("JOB_ID")))
                Response.Write("</td>")
                Response.Write("</tr>")
                strResponsibilities = GetLongDescription("JOB", "RESPONSIBILITY", dr("JOB_ID"))
                If Len(strResponsibilities) > 0 Then
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write("<b>Responsibilities:</b>")
                    Response.Write("</td>")
                    Response.Write("</tr>")
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write(strResponsibilities)
                    Response.Write("</td>")
                    Response.Write("</tr>")
                End If
                strExperience = GetLongDescription("JOB", "EXPERIENCE", dr("JOB_ID"))
                If Len(strExperience) > 0 Then
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write("<b>Requirements:</b>")
                    Response.Write("</td>")
                    Response.Write("</tr>")
                    Response.Write("<tr>")
                    Response.Write("<td  colspan=""2"" valign=top>")
                    Response.Write(strExperience)
                    Response.Write("</td>")
                    Response.Write("</tr>")
                End If
                Response.Write("<tr>")
                Response.Write("<td height=""30"" valign=""bottom"" colspan=""2""><nobr>")
                Response.Write("<a href=""mailto:" & dr("MAILTO") & """ class=AWN>")
                Response.Write("<img border=""0"" src=""/Nav/Images/mail.gif"" width=""15"" height=""8"">")
                Response.Write("</a>")
                Response.Write("Send your resume to <a href=""mailto:" & dr("MAILTO") & """ class=AWN>")
                Response.Write(dr("MAILTO"))
                Response.Write("</a>")
                Response.Write("</nobr></td>")
                Response.Write("</tr>")
                Response.Write("<tr>")
                Response.Write("<td colspan=2>&nbsp;&nbsp;</td>")
                Response.Write("</tr>")
                Response.Write("</table><br><br>")
            Else
                Response.Write("Jobs are currently being updated, please check back soon.")
            End If
        End If
    End Sub

    Function GetLongDescription(ByVal tablename, ByVal columnname, ByVal idintable)

        If Len(tablename) > 0 And Len(columnname) > 0 And Len(idintable) > 0 Then
            sql = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            Dim TableID As String = String.Empty
            Dim ColumnID As String = String.Empty
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLENAME", tablename.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                TableID = dr("Table_ID")
            End If

            sql = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", columnname.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                ColumnID = dr("Column_ID")
            End If

            sql = "Select TEXT from LONGDESCR where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@ID Order by SEQ_ID"
            Dim LongDescription As String = ""
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
            sqlParameters.Add(New SqlParameter("@COLUMNID", ColumnID))
            sqlParameters.Add(New SqlParameter("@ID", idintable.ToString()))
            'ds = DbHelperSQL.Query(sql)
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    LongDescription = LongDescription & dr("TEXT")
                Next
            End If
            Return LongDescription
        End If
        Return String.Empty
    End Function
End Class