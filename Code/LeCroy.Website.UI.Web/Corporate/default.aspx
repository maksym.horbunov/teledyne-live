<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Corporate_Profile" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_company_profile.gif" alt="Company Profile"></p>
        <p>Teledyne LeCroy is a leading provider of oscilloscopes, protocol analyzers and related test and measurement solutions that enable companies across a wide range of industries to design and test electronic devices of all types. Since our founding in 1964, we have focused on creating products that improve productivity by helping engineers resolve design issues faster and more effectively.</p>
        Oscilloscopes are tools used by designers and engineers to measure and analyze complex electronic signals in order to develop high-performance systems and to validate electronic designs in order to improve time to market. Protocol analyzers are tools used by designers and engineers to generate and monitor traffic over high speed serial data interfaces, including DDR, PCI Express, Fibre Channel, Serial ATA, SAS, USB and others. Both products are critical in the development of a wide variety of demanding design applications.
		<p>We utilize both of these important product areas in combination to tackle the most demanding serial data test applications, offering tailored solutions to help our customers perfect their chips, interfaces, subsystems and products which utilize these important communications standards. With the explosion in the use of serial data communications technologies in semiconductors, between devices on circuit boards and between computers and peripherals, we are aimed at a growing and important market.</p>
		<p>Our oscilloscopes offer a powerful combination of large and informative displays combined with advanced waveshape analysis capabilities typically tailored to enhance the productivity of engineers in specific applications areas such as serial data test, disk drive test and automotive bus analysis.</p>
        <p><h1>Location and Facilities</h1></p>
        <p>Headquartered in Chestnut Ridge, New York, LeCroy has sales, service and development subsidiaries in the US and throughout Europe and Asia. LeCroy products are employed across a wide variety of industries, including semiconductor, computer, consumer electronics, military/aerospace, automotive/industrial, and telecommunications.</p>                        
    </div>
	<br /><br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources"><img src="../images/nav/Bullet_Sq-green.gif" width="13" height="13" /><a href="iso/iso-certificate-2021.pdf">ISO Certificate</a><br />
        <img src="../images/nav/Bullet_Sq-green.gif" width="13" height="13" /><a href="/files/pdf/teledyne-lecroy-iso-iec-17025-certificate-with-scope-accreditation.pdf">ISO/IEC 17025 Certificate with Scope of Accreditation</a>
    </div>
</asp:Content>