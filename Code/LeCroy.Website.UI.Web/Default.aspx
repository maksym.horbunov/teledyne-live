﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false" Inherits="LeCroy.Website._default" Codebehind="Default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<main id="main" role="main">
				<div class="banner-home section-padding bg-retina font-weight-medium"><span data-srcset="https://assets.lcry.net/images/banner-main.png, https://assets.lcry.net/images/banner-main@2x.png 2x"></span>
					<div class="container">
						<div class="row">
							<div class="title col-md-9 col-lg-10 col-xl">
								<h1 class="font-weight-bold">Test Solutions that Accelerate Design</h1>
								<p class="mb-0">Test Equipment and Tools for Every Need from Inception to Error-Free Data Flow</p>
							</div>
						</div>
					</div>
				</div>
				<section class="section-analysis section-padding section-solutions">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Extensive Range of Test Solutions</h2>
							<div class="sub-title justify-content-center">
								<div class="text-left">
									<p>Teledyne LeCroy offers the most extensive range of test solutions to help with design, development, and deployment of devices and systems. Whether you need to test the physical and electrical properties of your device or you need to see individual packets being transmitted between devices, we've got you covered. Our test equipment, training, and testing services help you make better, safer, and more reliable products.</p>
								</div>
							</div>
						</div>

						<div class="section-content section-white"> 
							<div class="row">

								<div class="col-sm-6 col-lg-4 col-xl-3">
									<div class="section-heading mb-4">
										<div class="text-padding">
											<h3>Let us help propel your team to test, validate and optimize your designs and increase productivity!</h3>
										</div>		
									</div>
								</div>

								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="/oscilloscope/"><span data-srcset="https://assets.lcry.net/images/data-01.jpg, https://assets.lcry.net/images/data-01@2x.jpg 2x"></span>
										<div class="text">
											<h5 class="title">Oscilloscopes</h5>
										</div>
										<div class="block-hover">
											<h5 class="title">Oscilloscopes</h5>
											<p>Our oscilloscopes offer a powerful combination of large and informative displays combined with advanced waveshape analysis capabilities - typically tailored to enhance the productivity of engineers.</p><span class="font-weight-medium">Learn More</span>
										</div></a></div>
								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="/protocolanalyzer/"><span data-srcset="https://assets.lcry.net/images/data-02.jpg, https://assets.lcry.net/images/data-02@2x.jpg 2x"></span>
										<div class="text">
											<h5 class="title">Protocol Solutions</h5>
										</div>
										<div class="block-hover">
											<h5 class="title">Protocol Solutions</h5>
											<p>Designed to generate, capture, and analyze high-speed communications traffic, Teledyne LeCroy’s tools help developers to discover and correct persistent and intermittent errors and flaws in their product design.</p><span class="font-weight-medium">Learn More</span>
										</div></a></div>

								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="/services/default.aspx"><span data-srcset="https://assets.lcry.net/images/data-06.jpg, https://assets.lcry.net/images/data-06@2x.jpg 2x"></span>
									<div class="text">
										<h5 class="title">Testing and Training Services</h5>
									</div>
									<div class="block-hover">
										<h5 class="title">Testing and Training Services</h5>
										<p>A wide range of testing and training services offerings tailored to help you reach your goals.</p><span class="font-weight-medium">Learn More</span>
									</div></a>
								</div>
								
								
								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="https://www.oakgatetech.com/"><span data-srcset="https://assets.lcry.net/images/ssddata-xx@2x.png, https://assets.lcry.net/images/ssddata-xx@2x.png 2x"></span>
									<div class="text">
										<h5 class="title">SSD Testing and Analytics</h5>
									</div>
									<div class="block-hover">
										<h5 class="title">SSD Testing and Analytics</h5>
										<p>Realize the full potential of Teledyne LeCroy test tools and verify SSD performance and functionality and reduce test complexity from development through production. Use our Analytics software to optimize storage infrastructure and applications in high-performance, hyperscale environments.</p><span class="font-weight-medium">Learn More</span>
									</div></a>
								</div>


								
								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="/testtools/"><span data-srcset="https://assets.lcry.net/images/data-04.jpg, https://assets.lcry.net/images/data-04@2x.jpg 2x"></span>
										<div class="text">
											<h5 class="title">Electronic Test Equipment</h5>
										</div>
										<div class="block-hover">
											<h5 class="title">Test Equipment and Tools</h5>
											<p>Helping you meet your test requirements with a wide selection of equipment covering all of your measurement needs.</p><span class="font-weight-medium">Learn More</span>
										</div></a></div>
								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="http://teledyne-ts.com"><span data-srcset="https://assets.lcry.net/images/data-05.jpg, https://assets.lcry.net/images/data-05@2x.jpg 2x"></span>
										<div class="text">
											<h5 class="title">Sensors and Calibrators</h5>
										</div>
										<div class="block-hover">
											<h5 class="title">Sensors and Calibrators</h5>
											<p>From critical valve testing products to automotive torque sensors, we make sensors and test equipment to help troubleshoot and optimize new designs as well as installed systems.</p><span class="font-weight-medium">Learn More</span>
										</div></a></div>
								
								<div class="col-sm-6 col-lg-4 col-xl-3"><a class="box-item mx-auto bg-retina" href="https://www.spdevices.com/products/hardware"><span data-srcset="https://assets.lcry.net/images/data-03.jpg, https://assets.lcry.net/images/data-03@2x.jpg 2x"></span>
									<div class="text">
										<h5 class="title">Modular Data Acquisition</h5>
									</div>
									<div class="block-hover">
										<h5 class="title">Modular Data Acquisition</h5>
										<p>Our products utilize patented calibration logic, the latest data converters, and FPGA technology resulting in an unrivalled combination of high sampling rate and resolution.</p><span class="font-weight-medium">Learn More</span>
									</div></a></div>
							</div>
						</div>		
					</div>
				</section>
				<div class="jumbotron jumbotron-fluid bg-retina text-center section-padding"><span data-srcset="https://assets.lcry.net/images/cross-sync-banner.png, https://assets.lcry.net/images/cross-sync-banner@2x.png 2x"></span>
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9 col-lg-10 col-xl-8">
								<h2>See the Whole Link</h2>
								<p class="mb-4">Debug and validate active link behavior across electrical and protocol layers.<br /><strong><em>Cross</em>Sync&trade; PHY</strong> – Cross-layer Analysis for PCI Express&reg;</p><a class="btn btn-white" href="/Options/cross-sync-phy.aspx">Learn More</a>
							</div>
						</div>
					</div>
				</div>
				<section class="section-events section-padding section-gray">
					<div class="container">
						<div class="row">
							<div class="col-lg-9 col-xl-8 mx-auto px-xl-5">
								<div class="section-heading text-center">
									<h2><a href="/events/">Events and Training</a></h2>
									<p>Our engineers helped develop some of the industry’s key technologies and continue to have a vigorous passion for improving products and sharing their knowledge. This experience and enthusiasm translates into the highest quality testing and training services possible.</p>
								</div>
								
							</div>
						</div>
                        <script type="text/javascript">
jQuery(document).ready(function() {
	var jsonURL = "/events/output.aspx";
	console.log('setting up events json: %s', jsonURL);
	console.log('site.master relinquished control of the $ variable, must use jQuery instead');
	var now;
	try { now = $.now(); } catch { now = 'undefined' }
	console.log('Now based on $ is %s', now);
	now = jQuery.now();
	console.log('Now based on jQuery is %s', now);
	jQuery.getJSON(jsonURL, { countryId: 208 }, processRequest);
	console.log('done setting up events');
});

							function processRequest(json) {
								console.log(json);
            jQuery('#dataTable').empty();
            for (i = 0; i < 6; i++) {
                var jsonStartDate = json[i].ExhibitionStartDate.replace("/Date(", "").replace("-0400)/", "").replace("-0500)/", "").replace("-0600)/", "");
                var date = new Date(jsonStartDate);
                const month = date.toLocaleString('default', { month: 'long' });
                var dateString = ("0" + date.getUTCDate()).slice(-2);
                dateString = month + "&nbsp;" + dateString;
                var jsonType = json[i].TypeId;
                var jsonTitle = json[i].Title;
                var jsonURL = json[i].Url;
                var jsonDescription = json[i].Description.replace(/(<([^>]+)>)/ig, "");
                var jsonAddress = json[i].Address.replace(/(<([^>]+)>)/ig, " ");
                var jsonCity = json[i].City;
                var jsonState = json[i].State;
                var jsonTimeZone = json[i].TimeZone;
                var location = "";

                jsonTitle = '<h5 class="title mb-sm-2 mb-lg-0">' + jsonTitle + '</h5>';
                jsonURL = '<a class="event-item mx-auto h-100 h-md-auto" href="' + jsonURL + '">';
                switch (jsonType) {
                    case 2: { jsonType = "<span class='type type-trade-show'>Trade Show</span>"; location = "Digital Event"; }; break;
                    case 3: { jsonType = "<span class='type type-seminar'>Seminar</span>"; location = "Online"; }; break;
                    case 4: { jsonType = "<span class='type type-webinar'>Webinar</span>"; location = "Online"; }; break;
                    case 5: { jsonType = "<span class='type type-training'>Training</span>"; location = "Online"; }; break;
                }

                jQuery('#dataTable').append(
                    '<div class="col-12">'+jsonURL
                    +'<div class="row"><div class="col d-flex">'+jsonType
                    +'<p class="location">'+location+'</p></div></div><div class="row"><div class="col d-lg-flex">'+jsonTitle
                    +'<time class="pt-lg-1 font-weight-medium flex-shrink-0 flex-grow-0" datetime="2020-06-20">'+dateString+'</time></div></div></a></div>');
            }
        }
                        </script> 
						<section class="content-events">
							<div class="row" id="dataTable">
							</div><div class="holder-btn-more"><a class="link-arrow" href="/events/"><span>Show More Events</span><i class="icon-btn-right-01"></i></a></div>
						</section>
					</div>
				</section>
			</main>
</asp:Content>