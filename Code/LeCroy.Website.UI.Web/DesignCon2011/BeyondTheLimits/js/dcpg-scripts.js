var dcpgVid;

function dcpgInit()
{
	 $('.jcarousel-skin-dcpg').jcarousel({scroll: 5});

	$('.jcarousel-skin-dcpg a').click(function(event){
		event.preventDefault();
		if ($('#dcpg-img-container').hasClass('dcpg-loading') || $(this).parent().hasClass('dcpg-active')) return;
		isLoading = true;
		$('.dcpg-active').removeClass('dcpg-active');
		$(this).parent().addClass('dcpg-active');
		var href = $(this).attr('href');
		
		if ($(this).hasClass('dcpg-video-link'))
		{
		
			$('#dcpg-img-container img').remove();
	
			if (!dcpgVid)
			{
				dcpgVid = $('<div id="dcpg-video-container"></div>');		
				$('#dcpg-img-container').append(dcpgVid).addClass("dcpg-loading");
			}
			else if (dcpgVid.show) dcpgVid.show();
			dcpgVid.flowplayer(
			{
				src: "flowplayer/flowplayer-3.2.6.swf",
				id: "myFlowPlayerId"
			},
			{
				onBeforeLoad: dcpgShowVideo,
				onLoad: dcpgIsVideoLoaded,
    			clip: 
    			{
       				url: href,
        			autoPlay: false,
        			scaling: 'fit'
   				}
   				
			});		
		}
		else
		{
			
			if (dcpgVid) 
			{
				if(dcpgVid.stop) dcpgVid.stop();
				if(dcpgVid.hide) dcpgVid.hide();
			}
					
			var img = $('<img>').hide().load(dcpgShowImage);
			$('#dcpg-img-container img').remove();
			$('#dcpg-img-container').append(img).addClass("dcpg-loading");
			img.attr('src', href);
		}
	});
	
	$('.jcarousel-skin-dcpg a').first().click();	
	
	$(document).pngFix();
}

function dcpgIsLoaded()
{
	$('.dcpg-loading').removeClass('dcpg-loading');
}

function dcpgIsVideoLoaded()
{
	$('.dcpg-loading').removeClass('dcpg-loading');
	flowplayer().play();
}

function dcpgShowImage()
{
	$('#dcpg-img-container img').fadeIn(null, dcpgIsLoaded);
}

function dcpgShowVideo()
{	
	$('#dcpg-video-container').fadeIn();
}



$(document).ready(dcpgInit);