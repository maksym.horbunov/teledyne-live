function init() {
	$('#products').addClass('jcarousel-skin-lecroyESC');
	$('#nav li').first().append('<div id="mixedSignalSlugs" class="slide-slug slide-slug-3 first"></div>').next().append('<div id="serialBusDebugSlugs" class="slide-slug slide-slug-1 first"></div>').next().append('<div id="protocolAnalyzersSlugs" class="slide-slug slide-slug-3"></div>').next().append('<div id="oscilloscopesSlugs" class="slide-slug slide-slug-2 first"></div>').next().append('<div id="decodedAppSlugs" class="slide-slug slide-slug-4 first"></div>');
	$('.slide-slug').hide();
	$('#products').jcarousel({visible:1, scroll:1, itemVisibleInCallback: doNavSlugs});
	$('#nav a').click(function(event){
		event.preventDefault();
		var elementId = $(this).attr('href');
		var index = $(elementId).parent().attr('jcarouselindex');
		jumpToSlide(index);
	});
//	$('input[type="text"]').focus(function(){
//		clearTextInputValue(this);
//	}).blur(function(){
//		setTextInputDefaultValue(this, true);
//	}).change(function(){
//		removeInvalidClass(this);
//	}).addClass('default').each(function(){
//		setTextInputDefaultValue(this);
//	});
//	$('input[name="btnSubmit"]').click(function(event){
//		if (!validateForm()) event.preventDefault();
//	});
	$('#sbdDescription li').each(function(){
		$(this).html($(this).html().replace(/,/g,"<br />"));	
	});
}

function doNavSlugs(carousel, li, index, state)
{
	var activeID = $(li).children().first().attr('id')
	$('#mixedSignalSlugs').hide();
	$('#serialBusDebugSlugs').hide();
	$('#oscilloscopesSlugs').hide();
	$('#protocolAnalyzersSlugs').hide();
	$('#decodedAppSlugs').hide();
	switch(activeID)
	{
		case "msAnalogDigitalDebug":
			$('#mixedSignalSlugs').attr('class', 'slide-slug slide-slug-3 first').show();
			break;
		case "msDebugAnalysis":
			$('#mixedSignalSlugs').attr('class', 'slide-slug slide-slug-3 second').show();
			break;
		case "msFasterSmarterDebug":
			$('#mixedSignalSlugs').attr('class', 'slide-slug slide-slug-3 third').show();
			break;
		case "sbdDescription":
			$('#serialBusDebugSlugs').attr('class', 'slide-slug slide-slug-1 first').show();
			break;
		case "paCompUSB":
			$('#protocolAnalyzersSlugs').attr('class', 'slide-slug slide-slug-3 first').show();
			break;
		case "paCompUSB2":
			$('#protocolAnalyzersSlugs').attr('class', 'slide-slug slide-slug-3 second').show();
			break;
		case "paKibra":
			$('#protocolAnalyzersSlugs').attr('class', 'slide-slug slide-slug-3 third').show();
			break;
		case "oUltimateDebug":
			$('#oscilloscopesSlugs').attr('class', 'slide-slug slide-slug-2 first').show();
			break;
		case "oDebugConfidence":
			$('#oscilloscopesSlugs').attr('class', 'slide-slug slide-slug-2 second').show();
			break;
		case "decodedApp":
			$('#decodedAppSlugs').attr('class', 'slide-slug slide-slug-1 first').show();
			break;
	}
}

function jumpToSlide(index)
{
	var carousel = jQuery('#products').data('jcarousel');
	carousel.scroll(jQuery.jcarousel.intval(index));
}

//function clearTextInputValue(element)
//{
//	element = $(element);
//	var clearEl = false;
//	switch(element.attr('name'))
//	{
//		case "txtFirstName":
//			if (element.attr('value') == "First Name") clearEl = true;
//			break;
//		case "txtLastName":
//			if (element.attr('value') == "Last Name") clearEl = true;
//			break;
//		case "txtEmail":
//			if (element.attr('value') == "Email Address") clearEl = true;
//			break;
//	}
//	if (clearEl)
//	{
//		element.attr('value', '');
//		element.removeClass('default').addClass('entered');
//	}
//}

//function setTextInputDefaultValue(element, onlyIfEmpty)
//{
//	element = $(element);
//	if (element.attr('value') == '' || !onlyIfEmpty)
//	{
//		switch(element.attr('name'))
//		{
//			case "txtFirstName":
//				element.attr('value', 'First Name');
//				break;
//			case "txtLastName":
//				element.attr('value', 'Last Name');
//				break;
//			case "txtEmail":
//				element.attr('value', 'Email Address');
//				break;
//		}
//		element.removeClass('entered').removeClass('invalid').addClass('default');
//	}
//}

//function validateForm()
//{
//	var firstNameEl = $('input[name="txtFirstName"]');
//	var lastNameEl = $('input[name="txtLastName"]');
//	var emailEl = $('input[name="txtEmail"]');

//	var firstNameVal = firstNameEl.attr('value');
//	var lastNameVal = lastNameEl.attr('value');
//	var emailVal = emailEl.attr('value');
//	
//	var firstName = false;
//	var lastName = false;
//	var email = false;
//	
//	if (/^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$/.test(firstNameVal)) firstName = true;
//	if (/^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$/.test(lastNameVal)) lastName = true;
//	if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(emailVal)) email = true;
//	
//	if (!firstName) firstNameEl.removeClass('default').removeClass('entered').addClass('invalid');
//	if (!lastName) lastNameEl.removeClass('default').removeClass('entered').addClass('invalid');
//	if (!email) emailEl.removeClass('default').removeClass('entered').addClass('invalid');
//	
//	if (firstName && lastName && email) 
//	{
//		alert("Form Validates");//remove me
//		return true;
//	}

//}

//function removeInvalidClass(element)
//{
//	element = $(element);
//	element.removeClass('invalid');
//}

//$(document).ready(init);