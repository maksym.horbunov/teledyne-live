﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="true"  Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <script type="text/javascript">
        function checkSubmit(e) {
            if (e && e.keyCode === 13) {
                e.preventDefault();
                document.getElementById('SiteContent_CenterColumn_btnEventsSearch').click();
            }
        }

    </script>
<main id="app" role="main" class="has-header-transparented skew-style v1 reset" >


    
            
				<section class="section-banner bg-retina small">
                    <span data-srcset="https://assets.lcry.net/images/banner-austinlabs-4.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Events & Training</h1>
								</div>
							</div>
						</div>
					</div>
				</section>
           


            

                <!--tabs-->

                <section class="section-event-solutions pb-0">
                    <div class="container">
                        <div class="section-content" v-if="filterDate == 'upcoming'">
                            <h2>Events Upcoming</h2>
                        </div> 
                        
                        <div class="section-content" v-if="filterDate == 'on-demand'">
                            <h2>Events On-demand</h2>
                        </div> 

                        <div class="section-content">
                            <div class="block-view view-cards">
                                <div class="acc-pci tabs-pci">
                                    <div class="lc-item mb-0" @click="refreshTab()" :class="[{ active: filterDate == 'upcoming' }]"><a v-on:click="filterDate = 'upcoming'">Upcoming</a></div>
                                    <div class="lc-item mb-0" @click="refreshTab()" :class="[{ active: filterDate == 'on-demand' }]"><a v-on:click="filterDate = 'on-demand'">On-demand</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                 <!--filters MY-->
                <section 
                :class="[{ demand: filterDate == 'on-demand' }]"
                class="section-training-filters section-gray" 
                >
                    <div class="container">
                        <div class="row justify-content-center form-row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3 mb-32 mb-md-0"><label>Region </label>
                                        <select required="required" name="reaching" v-model="filterRegionValue">
                                            <option v-for="(region, key) in filterRegion" :value=region :key="key">{{region}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-32 mb-md-0 line-none"><label>Event Type</label>
                                        <select required="required" name="reaching" v-model="filterTypeValue">
                                            <option v-for="type in filterType" :value=type :key="type">{{type}}</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 mb-32 mb-md-0"><label>Event Category</label>
                                        <select required="required" name="reaching" v-model="filterCategoryValue">
                                            <option v-for="category in filterCategory" :value=category :key="category">{{category}}</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3 search-wrap"><input type='text' v-model="filterSearch" class="form-control" placeholder="Search event or training"><i class="icon-search"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <!--filters K-->

                <!-- <section class="section-training-filters section-gray">
					<div class="container" id="divSelectors" runat="server" visible="true">
						<div class="row justify-content-center form-row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-3 mb-32 mb-md-0">
									    <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="true" />
									    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" />
									</div>

                                    <div class="col-md-3 mb-32 mb-md-0">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" />
                                    </div>

                                    <div class="col-md-3 mb-32 mb-md-0">
                                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" />
                                    </div>

									<div class="col-md-3 search-wrap">
									    <input ID="tbEventsSearch" onfocus="this.value=''" text="search for events" runat="server" Width="200px" onkeydown="return checkSubmit(event);" />
									    <i class="icon-search"><input type="submit" id="btnEventsSearch" runat="server" enabled="false" text="Search" onclick="return checkSubmit(event);" /></i>
									</div>
					            </div>
					        </div>
					    </div>
					</div>
				</section> -->

    
          

                <section class="section-event-solutions pt-40">
                    <div class="container">
   
                        <div class="section-content">
                            <div class="block-view view-list">
                                <div class="tab-holder pt-0" id="filters">
                                    <div class="events-non d-none">
                                        <p>No events match your selected criteria 
                                            <a class="link-arrow ml-2" href="/">
                                            <span>See All Events</span><i class="icon-btn-right-01"></i></a></p>
                                    </div>

                                    <div class="tabs-content" id="tab-pci-1">
                                        <section v-if="errored">
                                            <p>We're sorry, we're not able to retrieve this information at the moment, please try back later</p>
                                        </section>
    
                                        <section v-else>
                                            <div v-if="loading">Page loading...</div>
    
                                            <div class=".row.mb-24" v-for="product in computedFilter" :key = "product.ExhibitionId" v-if="computedFilter.length">
                                           
                                                <div class="mb-3 col-lg-12">
                                                    <div class="slide-event">
                                                        <div class="top-card"><span class="type type-trade-show">{{product.TypeName}}</span>
                                                            <h6 class="title-slide"> <a :href="product.Url">{{product.Title}}</a></h6>
                                                            <div class="footer-slide d-flex">
                                                                <div class="card-text mb-3">
                                                                    <p>{{product.Description}}</p>
                                                                </div>
                                                                <div class="hold-date d-flex align-items-center"> <span class="local">{{product.ExhibitionStartDate}}</span><span class="calendar"> <a href="#">Add to my calendar</a></span><span class="date">{{product.City}} {{product.State}}</span></div>
                                                            </div>
                                                        </div>
                                                        <div class="bottom-card"><a class="btn btn-default" :href="product.Url"> Explore </a></div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="bottom-card result text-center" v-if="computedFilter.length == 0" >
                                                <h4>There are no matching results</h4>
                                                <p>Try adjusting your search to find what you are looking for or <a href="/">contact our support</a> if you are interested in a specific event</p>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!--event-solutions К-->
                
                <!-- <section class="section-event-solutions pt-40">
                    <div class="container">
                        <div class="section-content">
                            <div class="block-view view-list">
                                <div class="tab-holder pt-0" id="filters">
                                    <asp:Panel ID="pnlNoResults" runat="server" Visible="false">
                                        <div class="events-non d-none">
                                            <p>No events match your selected criteria
                                                <a class="link-arrow ml-2" href="/"><span>See All Events</span><i class="icon-btn-right-01"></i></a>
                                            </p>

                                        </div>
                                    </asp:Panel>

                                    <div class="tabs-content" id="tab-pci-1">
                                        <asp:Panel ID="pnlResults" runat="server">
                                            <asp:Repeater ID="rptMonthEvents" runat="server">
                                                <ItemTemplate>

                                                    <div class="row justify-content-center">
                                                        <div class="col-sm-12">
                                                            <div class="section-heading line">
                                                                <h3 class="title px-0 mx-md-0"><asp:Literal ID="litEventDate" runat="server" /></h3>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-24">

                                                        <asp:Repeater ID="rptEvents" runat="server">
                                                            <ItemTemplate>

                                                                <div class="mb-3 col-lg-12">
                                                                    <div class="slide-event">
                                                                        <div class="top-card">
                                                                            <span class="type type-training"><asp:Literal ID="litEventName" runat="server" /></span>
                                                                            <h6 class="title-slide"><asp:HyperLink ID="hlkEventTitle" runat="server" /></h6>
                                                                            <div class="footer-slide d-flex">
                                                                                <div class="card-text mb-3">
                                                                                    <p><asp:Literal ID="litEventDescription" runat="server" /></p>
                                                                                </div>

                                                                                <div class="hold-date d-flex align-items-center">
                                                                                    <span class="local">
                                                                                        <asp:Literal ID="litEventDate" runat="server" />
                                                                                        <asp:Literal ID="litEventTime" runat="server" />
                                                                                    </span>



                                                                                    <span class="date">
                                                                                        <asp:Literal ID="litEventAddress" runat="server" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                       <div class="bottom-card"><a class="btn btn-default" href="#">Explore </a></div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater></div>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                    </div>

                                    <div class="on-demand-tab tabs-content" id="tab-pci-2">
                                        <asp:Repeater ID="rptWebinars" runat="server">
                                            <ItemTemplate>

                                                <div class="row mb-24">
                                                    <div class="mb-3 col-lg-12">
                                                        <div class="slide-event">
                                                            <div class="top-card">
                                                                <h6 class="title-slide"><asp:HyperLink ID="hlkWebinarTitle" runat="server"></asp:HyperLink></h6>
                                                                <div class="footer-slide d-flex">
                                                                    <div class="hold-date d-flex align-items-center"><span class="date"> <asp:Literal ID="litWebinarDate" runat="server" /></span></div>
                                                                </div>
                                                            </div>
                                                            <div class="bottom-card"><asp:HyperLink ID="hlkExplore" runat="server"></asp:HyperLink></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> -->

            </main>


</asp:Content>