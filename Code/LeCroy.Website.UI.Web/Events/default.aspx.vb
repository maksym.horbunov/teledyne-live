﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common
Imports LeCroy.Library.VBUtilities

Partial Class Events_Events
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Private _countryIds As List(Of Int32) = New List(Of Int32)
    Private _regionIds As List(Of Int32) = New List(Of Int32)


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        'Me.Master.PageContentHeader.TopText = "Events"
        'Me.Master.PageContentHeader.BottomText = String.Empty
        Me.Title = "Events at Teledyne LeCroy" 'ConfigurationManager.AppSettings("DefaultPageTitle") + " - Events"
        Me.MetaDescription = "Meta Description"

        Master.MetaOgTitle = "Events, Seminars, Webinars, and Training from Teledyne LeCroy"
        Master.MetaOgImage = "https://teledynelecroy.com/images/og-events.jpg"
        Master.MetaOgDescription = "Teledyne LeCroy hosts seminars, webinars, and attends tradeshows throughout the year. See our calendar of events for upcoming seminars and webinars in your area."
        Master.MetaOgUrl = "https://teledynelecroy.com/events/"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String

        'selected_tab.Value = Request.Form(selected_tab.UniqueID)

        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.EVENTS_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.EVENTS_MENU
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID

        'litCountryOrRegion.Text = "region"
        ddlCountry.Visible = False
        ddlRegion.Visible = True
        ViewState("CountryId") = Nothing



        'lbDetails.Text = getMonthEvents() 'getEventsType()
        If Not Me.IsPostBack Then
            ParseQueryStrings()
            BindEvents()
            BindWebinars()
        End If
    End Sub

    Private Sub ParseQueryStrings()
        If Not (String.IsNullOrEmpty(Request.QueryString("countryid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("countryid"), testVal)) Then
                ViewState("CountryId") = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("regionid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("regionid"), testVal)) Then
                ViewState("RegionId") = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("eventtypeid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("eventtypeid"), testVal)) Then
                ViewState("EventTypeId") = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("eventcategoryid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("eventcategoryid"), testVal)) Then
                ViewState("EventCategoryId") = testVal
            End If
        End If
    End Sub

    Protected Sub btnEventsSearch_Click(sender As Object, e As EventArgs) Handles btnEventsSearch.ServerClick
        BindEvents()
        BindWebinars()
    End Sub

    Private Sub BindEvents()
        Dim sqlparameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim sqlString As String = "SELECT DISTINCT DATENAME([month], e.exhibition_startdate) as MONTH, DATEPART([year], e.exhibition_startdate) as YEAR, DATEPART([month], e.exhibition_startdate) " +
                                  "FROM EXHIBITIONS e {0} WHERE (e.disabled = 'n') AND (e.exhibition_enddate >= (GETDATE()-1)) AND e.LOCALEID = 1033 {1}" +
                                  "ORDER BY DATEPART([year], e.exhibition_startdate), DATEPART([month], e.exhibition_startdate)"
        If (ViewState("CountryId") > 0 Or ViewState("RegionId") > 0 Or ViewState("EventTypeId") > 0 Or ViewState("EventCategoryId") > 0) Then
            Dim innerJoinString As String = String.Empty
            Dim sb As StringBuilder = New StringBuilder()

            If (ViewState("CountryId") > 0) Then
                sb.Append(" AND e.COUNTRY_ID = @COUNTRYID ")
                sqlparameters.Add(New SqlParameter("@COUNTRYID", ViewState("CountryId").ToString()))
            End If
            If (ViewState("RegionId") > 0) Then
                innerJoinString += "INNER JOIN COUNTRY as b on e.COUNTRY_ID = b.COUNTRY_ID INNER JOIN REGION as d ON d.REGION_ID = b.OFFICE_REGION_ID and d.REGION_ID = @REGIONID"
                sqlparameters.Add(New SqlParameter("@REGIONID", ViewState("RegionId").ToString()))
            End If
            If (ViewState("EventTypeId") > 0) Then
                sb.Append(" AND e.TYPE_ID = @TYPEID ")
                sqlparameters.Add(New SqlParameter("@TYPEID", ViewState("EventTypeId").ToString()))
            End If
            If (ViewState("EventCategoryId") > 0) Then
                innerJoinString += " , [ExhibitionCategoryXref] x, [ExhibitionCategory] ec "
                sb.Append(" AND e.Exhibition_Id = x.ExhibitionFkId AND x.ExhibitionCategoryFkId = ec.ExhibitionCategoryPkId AND ec.ExhibitionCategoryPkId = @EXHIBITIONCATEGORYPKID ")
                sqlparameters.Add(New SqlParameter("@EXHIBITIONCATEGORYPKID", ViewState("EventCategoryId").ToString()))
            End If
            sqlString = String.Format(sqlString, innerJoinString, sb.ToString())
        Else
            sqlString = String.Format(sqlString, String.Empty, String.Empty)
        End If

        Dim eventMonths As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlparameters.ToArray())
        If (eventMonths.Tables.Count <= 0) Then
            pnlResults.Visible = False
            pnlNoResults.Visible = True
            Return
        End If
        If (eventMonths.Tables(0).Rows.Count > 0) Then
            pnlNoResults.Visible = False
            pnlResults.Visible = True
            rptMonthEvents.DataSource = eventMonths
            rptMonthEvents.DataBind()
            PopulateRegionDropDown()
        Else
            pnlResults.Visible = False
            pnlNoResults.Visible = True
            If (ViewState("RegionId") Is Nothing Or ViewState("RegionId") <= 0) Then  ' Something really weird happened, don't show the dropdowns
                divSelectors.Visible = False
            End If
        End If
    End Sub

    Private Sub BindWebinars()
        Dim sqlparameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim sqlWebinar As String = "SELECT e.title, e.ARCHIVE_URL, e.exhibition_startdate from EXHIBITIONS e inner join exhibitioncategoryxref x on e.Exhibition_Id = x.ExhibitionFkId inner join exhibitioncategory c on x.ExhibitionCategoryFkId = c.ExhibitionCategoryPkId WHERE e.type_id =4 AND e.archive_yn='y' and e.archive_URL is not null"
        If (ViewState("EventCategoryId") > 0) Then
            sqlWebinar += " AND c.ExhibitionCategoryPkId = @EXHIBITIONCATEGORYPKID "
            sqlparameters.Add(New SqlParameter("@EXHIBITIONCATEGORYPKID", ViewState("EventCategoryId").ToString()))
        End If

        If Me.tbEventsSearch.Value <> "" Then
            Dim key As String = Me.tbEventsSearch.Value
            sqlWebinar += " and (title like @TITLELIKE or description like @DESCRIPTIONLIKE)"
            sqlparameters.Add(New SqlParameter("@TITLELIKE", String.Format("%{0}%", SQLStringWithOutSingleQuotes(key))))
            sqlparameters.Add(New SqlParameter("@DESCRIPTIONLIKE", String.Format("%{0}%", SQLStringWithOutSingleQuotes(key))))
        End If

        sqlWebinar += " ORDER BY exhibition_startdate desc"
        Dim webinars As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlWebinar, sqlparameters.ToArray())
        If (webinars.Tables(0).Rows.Count > 0) Then
            rptWebinars.DataSource = webinars
            rptWebinars.DataBind()
        Else
        End If
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim ddv As String
        If (ddlCountry.SelectedIndex > 0) Then
            ddv = Int32.Parse(ddlCountry.SelectedValue)
        Else
            ddv = Nothing
        End If
        ViewState("CountryId") = ddv
        BindEvents()
        BindWebinars()
    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegion.SelectedIndexChanged
        Dim ddv As String
        If (ddlRegion.SelectedIndex > 0) Then
            ddv = Int32.Parse(ddlRegion.SelectedValue)
        Else
            ddv = Nothing
        End If
        ViewState("RegionId") = ddv
        BindEvents()
        BindWebinars()
    End Sub

    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        If (ddlType.SelectedIndex > 0) Then
            ViewState("EventTypeId") = Int32.Parse(ddlType.SelectedValue)
        Else
            ViewState("EventTypeId") = Nothing
        End If
        BindEvents()
        BindWebinars()
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        If (ddlCategory.SelectedIndex > 0) Then
            ViewState("EventCategoryId") = Int32.Parse(ddlCategory.SelectedValue)
        Else
            ViewState("EventCategoryId") = Nothing
        End If
        BindEvents()
        BindWebinars()
    End Sub

    Private Sub rptMonthEvents_ItemDataBound_OLD(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptMonthEvents.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim litEventDate As Literal = CType(e.Item.FindControl("litEventDate"), Literal)
                Dim rptEvents As Repeater = CType(e.Item.FindControl("rptEvents"), Repeater)

                litEventDate.Text = String.Format("{0} {1}", dr("MONTH").ToString(), dr("YEAR").ToString())
                Dim sqlString As String = " SELECT a.rowguid, a.title, a.description, a.state, a.city, a.address, a.exhibition_startdate," +
                                          " a.exhibition_enddate, a.exhibition_time, a.URL, b.NAME, b.SALES_REP_PSG, b.NAME AS COUNTRY, a.Logo, c.homepage_name, a.ShowAddToCalendarYN, a.COUNTRY_ID, a.TYPE_ID " +
                                          " FROM EXHIBITIONS AS a INNER JOIN  COUNTRY AS b ON a.country_id = b.COUNTRY_ID INNER JOIN" +
                                          " EXHIBITION_TYPE AS c ON a.type_id = c.type_id   {0}" +
                                          " WHERE   (a.disabled = 'n') AND (a.exhibition_enddate >= GETDATE() - 1) AND a.LOCALEID = 1033 {1}" +
                                          " AND (DATENAME(month, a.exhibition_startdate) = @EVENTMONTH) AND (DATEPART([year], a.exhibition_startdate) = @EVENTYEAR) ORDER BY a.exhibition_startdate"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@EVENTMONTH", dr("MONTH").ToString()))
                sqlParameters.Add(New SqlParameter("@EVENTYEAR", dr("YEAR").ToString()))
                If (Not (ViewState("CountryId") Is Nothing) Or Not (ViewState("EventTypeId") Is Nothing) Or Not (ViewState("EventCategoryId") Is Nothing)) Then
                    Dim innerJoinString As String = String.Empty
                    Dim sb As StringBuilder = New StringBuilder()

                    If (ViewState("CountryId") > 0) Then
                        sb.Append(" AND a.COUNTRY_ID = @COUNTRYID ")
                        sqlParameters.Add(New SqlParameter("@COUNTRYID", ViewState("CountryId").ToString()))
                    End If
                    If (ViewState("EventTypeId") > 0) Then
                        sb.Append(" AND a.TYPE_ID = @TYPEID ")
                        sqlParameters.Add(New SqlParameter("@TYPEID", ViewState("EventTypeId").ToString()))
                    End If
                    If (ViewState("EventCategoryId") > 0) Then
                        innerJoinString = " INNER JOIN [ExhibitionCategoryXref] AS x ON a.Exhibition_Id = x.ExhibitionFkId INNER JOIN [ExhibitionCategory] ec ON x.ExhibitionCategoryFkId = ec.ExhibitionCategoryPkId "
                        sb.Append(" AND ec.ExhibitionCategoryPkId = @EXHIBITIONCATEGORYPKID ")
                        sqlParameters.Add(New SqlParameter("@EXHIBITIONCATEGORYPKID", ViewState("EventCategoryId").ToString()))
                    End If
                    sqlString = String.Format(sqlString, innerJoinString, sb.ToString())
                Else
                    sqlString = String.Format(sqlString, String.Empty, String.Empty)
                End If

                AddHandler rptEvents.ItemDataBound, AddressOf rptEvents_ItemDataBound
                Dim events As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
                If (events.Tables.Count > 0) Then
                    If (events.Tables(0).Rows.Count > 0) Then
                        For Each countryDataRow As DataRow In events.Tables(0).Rows
                            If Not (_countryIds.Contains(Int32.Parse(countryDataRow("COUNTRY_ID").ToString()))) Then
                                _countryIds.Add(Int32.Parse(countryDataRow("COUNTRY_ID").ToString()))
                            End If
                        Next
                        PopulateExhibitionTypeDropDown()
                        PopulateExhibitionCategoryDropDown()
                        rptEvents.DataSource = events
                        rptEvents.DataBind()
                    End If
                End If
        End Select
    End Sub

    Private Sub rptMonthEvents_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptMonthEvents.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim litEventDate As Literal = CType(e.Item.FindControl("litEventDate"), Literal)
                Dim rptEvents As Repeater = CType(e.Item.FindControl("rptEvents"), Repeater)


                litEventDate.Text = String.Format("{0} {1}", dr("MONTH").ToString(), dr("YEAR").ToString())
                Dim sqlString As String
                sqlString = " SELECT a.rowguid, a.title, a.description, a.state, a.city, a.address, a.exhibition_startdate," +
                            " a.exhibition_enddate, a.exhibition_time, a.URL, b.NAME, b.SALES_REP_PSG, b.NAME AS COUNTRY, a.Logo, c.homepage_name, a.ShowAddToCalendarYN, a.COUNTRY_ID, a.TYPE_ID, d.REGION_ID " +
                            " FROM EXHIBITIONS AS a INNER JOIN COUNTRY AS b ON a.country_id = b.COUNTRY_ID INNER JOIN" +
                            " REGION as d ON d.REGION_ID = b.OFFICE_REGION_ID {2} INNER JOIN" +
                            " EXHIBITION_TYPE AS c ON a.type_id = c.type_id {0}" +
                            " WHERE (a.disabled = 'n') AND (a.exhibition_enddate >= GETDATE() - 1) AND a.LOCALEID = 1033 {1}" +
                            " AND (DATENAME(month, a.exhibition_startdate) = @EVENTMONTH) AND (DATEPART([year], a.exhibition_startdate) = @EVENTYEAR) ORDER BY a.exhibition_startdate"

                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@EVENTMONTH", dr("MONTH").ToString()))
                sqlParameters.Add(New SqlParameter("@EVENTYEAR", dr("YEAR").ToString()))
                If (Not (ViewState("CountryId") Is Nothing) Or Not (ViewState("RegionId") Is Nothing) Or Not (ViewState("EventTypeId") Is Nothing) Or Not (ViewState("EventCategoryId") Is Nothing)) Then
                    Dim regionInnerJoinString As String = String.Empty
                    Dim innerJoinString As String = String.Empty
                    Dim sb As StringBuilder = New StringBuilder()
                    If (ViewState("CountryId") > 0) Then
                        sb.Append(" AND a.COUNTRY_ID = @COUNTRYID ")
                        sqlParameters.Add(New SqlParameter("@COUNTRYID", ViewState("CountryId").ToString()))
                    End If
                    If (ViewState("RegionId") > 0) Then
                        regionInnerJoinString += " AND d.REGION_ID = @REGIONID"
                        sqlParameters.Add(New SqlParameter("@REGIONID", ViewState("RegionId").ToString()))
                    End If
                    If (ViewState("EventTypeId") > 0) Then
                        sb.Append(" AND a.TYPE_ID = @TYPEID ")
                        sqlParameters.Add(New SqlParameter("@TYPEID", ViewState("EventTypeId").ToString()))
                    End If
                    If (ViewState("EventCategoryId") > 0) Then
                        innerJoinString += " INNER JOIN [ExhibitionCategoryXref] AS x ON a.Exhibition_Id = x.ExhibitionFkId INNER JOIN [ExhibitionCategory] ec ON x.ExhibitionCategoryFkId = ec.ExhibitionCategoryPkId "
                        sb.Append(" AND ec.ExhibitionCategoryPkId = @EXHIBITIONCATEGORYPKID ")
                        sqlParameters.Add(New SqlParameter("@EXHIBITIONCATEGORYPKID", ViewState("EventCategoryId").ToString()))
                    End If
                    sqlString = String.Format(sqlString, innerJoinString, sb.ToString(), regionInnerJoinString)
                Else
                    sqlString = String.Format(sqlString, String.Empty, String.Empty, String.Empty)
                End If

                AddHandler rptEvents.ItemDataBound, AddressOf rptEvents_ItemDataBound
                Dim events As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
                If (events.Tables.Count > 0) Then
                    If (events.Tables(0).Rows.Count > 0) Then
                        ' Determine list of countries with events
                        For Each countryDataRow As DataRow In events.Tables(0).Rows
                            If Not (_countryIds.Contains(Int32.Parse(countryDataRow("COUNTRY_ID").ToString()))) Then
                                _countryIds.Add(Int32.Parse(countryDataRow("COUNTRY_ID").ToString()))
                            End If
                        Next
                        PopulateExhibitionTypeDropDown()
                        PopulateExhibitionCategoryDropDown()
                        rptEvents.DataSource = events
                        rptEvents.DataBind()
                    End If
                End If

        End Select
    End Sub

    Private Sub PopulateCountryDropDown()
        If (String.Compare(ddlType.SelectedValue, "4", True) = 0) Then
            Dim countries As List(Of Country) = CountryRepository.GetCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _countryIds).ToList()
            ddlCountry.Items.Clear()
            For Each country In countries
                Dim region As String = String.Empty
                Select Case country.RegionId.Value
                    Case 2
                        region = "Europe"
                    Case 4
                        If (country.CountryId = 100) Then
                            region = "Japan"
                        ElseIf (country.CountryId = 105) Then
                            region = "Korea"
                        Else
                            region = "Asia/Pacific"
                        End If
                    Case 3
                        region = "North America/South America"
                End Select
                ddlCountry.Items.Add(New ListItem(region, country.CountryId))
            Next
            ddlCountry.Items.Insert(0, New ListItem("Show all events for all regions", String.Empty))
            'litCountryOrRegion.Text = "region"
            If (Not (ViewState("CountryId") Is Nothing) And ViewState("CountryId") > 0) Then
                If Not (ddlCountry.Items.FindByValue(ViewState("CountryId").ToString()) Is Nothing) Then
                    ddlCountry.SelectedValue = ViewState("CountryId").ToString()
                End If
            End If
        Else
            DropDownListManager.BindDropDown(ddlCountry, CountryRepository.GetCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _countryIds).ToList(), "Name", "CountryId", True, "Show all events for all countries", String.Empty)
            If (Not (ViewState("CountryId") Is Nothing) And ViewState("CountryId") > 0) Then
                If Not (ddlCountry.Items.FindByValue(ViewState("CountryId").ToString()) Is Nothing) Then
                    ddlCountry.SelectedValue = ViewState("CountryId").ToString()
                End If
            End If
            'litCountryOrRegion.Text = "country"
        End If
        If (ViewState("CountryId") > 0) Then
            If Not (ddlCountry.Items.FindByValue(ViewState("CountryId").ToString()) Is Nothing) Then
                ddlCountry.SelectedValue = ViewState("CountryId").ToString()
            End If
        End If
    End Sub

    Private Sub PopulateRegionDropDown()
        If (ddlRegion.Items.Count > 0) Then
            ddlRegion.ClearSelection()
            ddlRegion.Items.Clear()
            ddlRegion.SelectedIndex = -1
        End If
        Dim countriesWithEvents As List(Of Country) = CountryRepository.GetCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _countryIds).ToList()
        Dim allRegions As List(Of Region) = RegionRepository.GetAllRegions(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToList()
        Dim regionsWithEvents As List(Of Region) = New List(Of Region)
        For Each country In countriesWithEvents
            For Each region In allRegions
                Select Case country.RegionId.Value
                    Case 2 ' Europe
                        If region.RegionId = 2 And Not regionsWithEvents.Contains(region) Then
                            regionsWithEvents.Add(region)
                        End If
                    Case 3 ' North America
                        If region.RegionId = 5 And Not regionsWithEvents.Contains(region) Then
                            regionsWithEvents.Add(region)
                        End If
                    Case 4 ' Asia/Pacific
                        If (country.CountryId = 100) Then ' Japan
                            If region.RegionId = 3 And Not regionsWithEvents.Contains(region) Then
                                regionsWithEvents.Add(region)
                            End If
                        ElseIf (country.CountryId = 105) Then ' Korea
                            If region.RegionId = 4 And Not regionsWithEvents.Contains(region) Then
                                regionsWithEvents.Add(region)
                            End If
                        Else ' Remainder of Asia/Pacific
                            If region.RegionId = 1 And Not regionsWithEvents.Contains(region) Then
                                regionsWithEvents.Add(region)
                            End If
                        End If
                End Select
            Next
        Next
        ' Loop over the countries then for each country
        ' Check for Europe in ddl
        ' if not regionsWithEvents.Exists(Function(x) x.RegionId = 2 then
        '   regionsWithEvents.Add(allRegions.select...)
        ' end if
        ' Check for North America in ddl
        ' if not regionsWithEvents.Exists(Function(x) x.RegionId = 5 then
        '   regionsWithEvents.Add(allRegions.select...)
        ' end if
        ' Check for Japan in ddl
        ' if not regionsWithEvents.Exists(Function(x) x.RegionId = 3 then
        '   regionsWithEvents.Add(allRegions.select...)
        ' end if
        ' Check for Korea in ddl
        ' if not regionsWithEvents.Exists(Function(x) x.RegionId = 4 then
        '   regionsWithEvents.Add(allRegions.select...)
        ' end if
        ' Check for Asia/Pacific in ddl
        ' if not regionsWithEvents.Exists(Function(x) x.RegionId = 1 then
        '   regionsWithEvents.Add(allRegions.select...)
        ' end if
        ' next

        DropDownListManager.BindDropDown(ddlRegion, regionsWithEvents, "Name", "RegionId", True, "Show all events for all regions", String.Empty)

        If (Not (ViewState("RegionId") Is Nothing) And ViewState("RegionId") > 0) Then
            If Not (ddlRegion.Items.FindByValue(ViewState("RegionId").ToString()) Is Nothing) Then
                ddlRegion.SelectedValue = ViewState("RegionId").ToString()
            End If
        End If
        'litCountryOrRegion.Text = "region"
    End Sub

    Private Sub PopulateExhibitionTypeDropDown()
        ddlCountry.ClearSelection()
        ddlType.Items.Clear()
        ddlType.SelectedIndex = -1
        DropDownListManager.BindDropDown(ddlType, ExhibitionTypeRepository.GetExhibitionTypes(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToList(), "TypeName", "TypeId", True, "Show all event types", String.Empty)
        If (ViewState("EventTypeId") > 0) Then
            If Not (ddlType.Items.FindByValue(ViewState("EventTypeId").ToString()) Is Nothing) Then
                ddlType.SelectedValue = ViewState("EventTypeId").ToString()
            End If
        End If
    End Sub

    Private Sub PopulateExhibitionCategoryDropDown()
        ddlCategory.Items.Clear()
        ddlCategory.SelectedIndex = -1
        DropDownListManager.BindDropDown(Of ExhibitionCategory)(ddlCategory, ExhibitionCategoryRepository.GetCategories(ConfigurationManager.AppSettings("ConnectionString").ToString()), "Category", "ExhibitionCategoryPkId", True, "Show all event categories", String.Empty)
        If (ViewState("EventCategoryId") > 0) Then
            If Not (ddlCategory.Items.FindByValue(ViewState("EventCategoryId").ToString()) Is Nothing) Then
                ddlCategory.SelectedValue = ViewState("EventCategoryId").ToString()
            End If
        End If
    End Sub

    Private Sub rptEvents_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim litEventDate As Literal = CType(e.Item.FindControl("litEventDate"), Literal)
                Dim litEventTime As Literal = CType(e.Item.FindControl("litEventTime"), Literal)
                Dim litEventName As Literal = CType(e.Item.FindControl("litEventName"), Literal)
                Dim btnAddToCalendar As LinkButton = CType(e.Item.FindControl("btnAddToCalendar"), LinkButton)
                'Dim hlkEventLogo As HyperLink = CType(e.Item.FindControl("hlkEventLogo"), HyperLink)
                'Dim imgEventLogo As Image = CType(e.Item.FindControl("imgEventLogo"), Image)
                'Dim litEventType As Literal = CType(e.Item.FindControl("litEventType"), Literal)
                Dim hlkEventTitle As HyperLink = CType(e.Item.FindControl("hlkEventTitle"), HyperLink)
                Dim litEventDescription As Literal = CType(e.Item.FindControl("litEventDescription"), Literal)
                Dim litEventAddress As Literal = CType(e.Item.FindControl("litEventAddress"), Literal)

                Dim startDate As DateTime = Convert.ToDateTime(dr("exhibition_startdate").ToString())
                Dim endDate As DateTime = Convert.ToDateTime(dr("exhibition_enddate").ToString())

                If ((startDate.Day = endDate.Day) Or dr("exhibition_enddate").ToString().Length = 0) Then
                    litEventDate.Text = String.Format("{0} {1}", MonthName(Month(dr("exhibition_startdate").ToString()), True), startDate.Day.ToString())
                Else
                    litEventDate.Text = String.Format("{0} - {1}", String.Format("{0} {1}", MonthName(startDate.Month.ToString(), True), startDate.Day.ToString()), String.Format("{0} {1}", MonthName(endDate.Month.ToString(), True), endDate.Day.ToString()))
                End If
                If (dr("exhibition_time").ToString().Length > 0) Then
                    litEventTime.Text = String.Format("<br />{0}", dr("exhibition_time").ToString())
                End If
                litEventName.Text = dr("homepage_name").ToString()
                'litEventType.Text = dr("TYPE_ID").ToString()
                'btnAddToCalendar.CommandArgument = dr("rowguid").ToString()
                'If (String.Compare(dr("ShowAddToCalendarYN").ToString(), "N", True) = 0) Then
                'btnAddToCalendar.Visible = False
                'End If
                'If (dr("LOGO").ToString().Length > 0) Then
                'If (dr("URL").ToString().Length > 0) Then
                'hlkEventLogo.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dr("URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                'hlkEventLogo.NavigateUrl = dr("URL").ToString()
                'End If
                'imgEventLogo.ImageUrl = String.Format("{0}{1}", rootDir, dr("LOGO").ToString())
                'Else
                'imgEventLogo.ImageUrl = String.Format("{0}/images/spacer.gif", rootDir)
                'End If
                If (dr("URL").ToString().Length > 0) Then
                    hlkEventTitle.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dr("URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkEventTitle.NavigateUrl = dr("URL").ToString()
                    hlkEventTitle.Text = dr("TITLE").ToString()
                End If
                litEventDescription.Text = dr("description").ToString()
                If (String.Compare(dr("TYPE_ID").ToString(), "4", True) = 0) Then
                    Dim country As Country = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(dr("COUNTRY_ID").ToString()))
                    If Not (country Is Nothing) Then
                        If (country.RegionId.HasValue) Then
                            Select Case country.RegionId.Value
                                Case 2
                                    litEventAddress.Text = "Europe"
                                Case 4
                                    If (country.CountryId = 100) Then
                                        litEventAddress.Text = "Japan"
                                    ElseIf (country.CountryId = 105) Then
                                        litEventAddress.Text = "Korea"
                                    Else
                                        litEventAddress.Text = "Asia/Pacific"
                                    End If
                                Case 3
                                    litEventAddress.Text = dr("COUNTRY").ToString()
                            End Select
                        Else
                            litEventAddress.Text = ConstructExhibitionAddress(dr("ADDRESS").ToString(), dr("CITY").ToString(), dr("STATE").ToString(), dr("COUNTRY").ToString(), False)
                        End If
                    Else
                        litEventAddress.Text = ConstructExhibitionAddress(dr("ADDRESS").ToString(), dr("CITY").ToString(), dr("STATE").ToString(), dr("COUNTRY").ToString(), False)
                    End If
                Else
                    litEventAddress.Text = ConstructExhibitionAddress(dr("ADDRESS").ToString(), dr("CITY").ToString(), dr("STATE").ToString(), dr("COUNTRY").ToString(), False)
                End If
        End Select
    End Sub

    Private Sub rptWebinarEvents_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptWebinars.ItemDataBound
        Dim rptWebinars As Repeater = CType(e.Item.FindControl("rptWebinars"), Repeater)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dw As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim startDate As DateTime = Convert.ToDateTime(dw("exhibition_startdate").ToString())
                Dim litWebinarDate As Literal = CType(e.Item.FindControl("litWebinarDate"), Literal)
                Dim hlkWebinarTitle As HyperLink = CType(e.Item.FindControl("hlkWebinarTitle"), HyperLink)
                Dim hlkExplore As HyperLink = CType(e.Item.FindControl("hlkExplore"), HyperLink)

                litWebinarDate.Text = String.Format("{0} {1}, {2}", MonthName(Month(dw("exhibition_startdate").ToString()), True), startDate.Day.ToString(), startDate.Year.ToString())

                If (dw("ARCHIVE_URL").ToString().Length > 0) Then
                    hlkWebinarTitle.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dw("ARCHIVE_URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkWebinarTitle.NavigateUrl = dw("ARCHIVE_URL").ToString()
                    hlkWebinarTitle.Text = dw("title").ToString()
                    hlkExplore.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dw("ARCHIVE_URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkExplore.NavigateUrl = dw("ARCHIVE_URL").ToString()
                    hlkExplore.CssClass = "btn btn-default"
                    hlkExplore.Text = "Watch Now"

                End If

        End Select

    End Sub

    Private Function ConstructExhibitionAddress(ByVal address As String, ByVal city As String, ByVal state As String, ByVal country As String, ByVal isForCalendar As Boolean) As String
        Dim retVal As String = String.Empty
        Dim hadAddress As Boolean = False
        Dim hadCity As Boolean = False
        Dim hadState As Boolean = False
        If Not (String.IsNullOrEmpty(address)) Then
            Dim addressString As String = address
            If (isForCalendar) Then addressString = String.Format("{0}", address).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
            retVal += addressString
            hadAddress = True
        End If
        If Not (String.IsNullOrEmpty(city)) Then
            Dim cityString As String = String.Empty
            If (hadAddress) Then
                If (isForCalendar) Then
                    cityString = String.Format(", {0}", city).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    cityString = String.Format("<br />{0}", city)
                End If
            Else
                If (isForCalendar) Then
                    cityString = String.Format("{0}", city).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    cityString = String.Format("{0}", city)
                End If
            End If
            retVal += cityString
            hadCity = True
        End If
        If Not (String.IsNullOrEmpty(state)) Then
            Dim stateString As String = String.Empty
            If (hadAddress Or hadCity) Then
                If (isForCalendar) Then
                    stateString = String.Format(", {0}", state).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    stateString = String.Format("<br />{0}", state)
                End If
            Else
                If (isForCalendar) Then
                    stateString = String.Format("{0}", state).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    stateString = String.Format("{0}", state)
                End If
            End If
            retVal += stateString
            hadState = True
        End If
        If Not (String.IsNullOrEmpty(country)) Then
            Dim countryString As String = String.Empty
            If (hadAddress Or hadCity Or hadState) Then
                If (isForCalendar) Then
                    countryString = String.Format(", {0}", country).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    countryString = String.Format("<br />{0}", country)
                End If
            Else
                If (isForCalendar) Then
                    countryString = String.Format("{0}", country).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    countryString = String.Format("{0}", country)
                End If
            End If
            retVal += countryString
        End If
        Return retVal
    End Function

    Private Function UtcDateTime(ByVal dateValue As Object) As String
        Dim dateOf As DateTime = Convert.ToDateTime(dateValue.ToString())
        Return String.Format("{0}{1}{2}T{3}{4}{5}Z", dateOf.Year, AppendLeadingZero(dateOf.Month), AppendLeadingZero(dateOf.Day), AppendLeadingZero(dateOf.Hour), AppendLeadingZero(dateOf.Minute), AppendLeadingZero(dateOf.Second))
    End Function

    Private Function AppendLeadingZero(ByVal inValue As Int32) As String
        If (inValue < 10) Then
            Return String.Format("0{0}", inValue)
        End If
        Return inValue
    End Function

    Protected Sub btnAddToCalendar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim sqlString As String = "SELECT a.*, b.NAME AS COUNTRY, c.TYPE_NAME FROM [EXHIBITIONS] a INNER JOIN [COUNTRY] b ON a.COUNTRY_ID = b.COUNTRY_ID INNER JOIN [EXHIBITION_TYPE] c ON a.TYPE_ID = c.TYPE_ID WHERE a.ROWGUID = @ROWGUID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ROWGUID", New Guid(btn.CommandArgument.ToString()).ToString()))
        Dim exhibitions As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (exhibitions.Tables(0).Rows.Count > 0) Then
            Dim exhibition As DataRow = CType(exhibitions.Tables(0).Rows(0), DataRow)
            Dim sb As StringBuilder = New StringBuilder
            sb.AppendLine("BEGIN:VCALENDAR")
            sb.AppendLine("VERSION:2.0")
            sb.AppendLine("BEGIN:VEVENT")
            sb.AppendLine(String.Format("UID:RFCALITEM{0}", DateTime.Now.Ticks))
            sb.AppendLine(String.Format("DTSTAMP:{0}", UtcDateTime(DateTime.Now)))
            Dim startDate As DateTime = Convert.ToDateTime(exhibition("exhibition_startdate").ToString())
            Dim endDate As DateTime = Convert.ToDateTime(exhibition("exhibition_enddate").ToString())
            sb.AppendLine(String.Format("DTSTART:{0}", startDate))  'UtcDateTime(startDate.ToUniversalTime())))
            sb.AppendLine(String.Format("DTEND:{0}", endDate)) 'UtcDateTime(endDate.ToUniversalTime())))
            sb.AppendLine(String.Format("LOCATION:{0}", Server.HtmlDecode(ConstructExhibitionAddress(exhibition("ADDRESS").ToString(), exhibition("CITY").ToString(), exhibition("STATE").ToString(), exhibition("COUNTRY").ToString(), True))))
            sb.AppendLine(String.Format("CATEGORIES:{0}", exhibition("TYPE_NAME").ToString()))
            sb.AppendLine("TRANSP:OPAQUE")
            sb.AppendLine(String.Format("DESCRIPTION:{0}", exhibition("DESCRIPTION").ToString()))
            sb.AppendLine(String.Format("SUMMARY:{0}", exhibition("TITLE").ToString()))
            sb.AppendLine("CLASS:PUBLIC")
            sb.AppendLine("END:VEVENT")
            sb.AppendLine("END:VCALENDAR")
            Response.ContentType = "text/calendar"
            Response.AddHeader("content-disposition", String.Format("attachment; filename=LeCroy_{0}_{1}.ics", exhibition("TYPE_NAME").ToString().Replace(" ", String.Empty).ToLower(), exhibition("ROWGUID").ToString().Substring(0, 8).ToLower()))
            Response.Write(sb.ToString())
            Response.End()
        End If
    End Sub



End Class

