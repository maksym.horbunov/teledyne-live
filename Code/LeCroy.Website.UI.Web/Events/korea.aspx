﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/twocolumn-left.master" CodeBehind="korea.aspx.vb" Inherits="LeCroy.Website.events_korea" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul style="margin-top: 20px;">
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td valign="top"><div style="font-weight: bold; font-size: 24px; color: #007ac3;">이벤트 &amp; 교육</div></td>
                <td valign="top" align="right">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr><td><a href="/support/techlib/webcasts.aspx"><img src="<%=rootDir %>/images/Events/lecroy_gotowebinar.jpg" border="0" width="250" height="75" /></a></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="lbDetails" runat="server" />
        <asp:Panel ID="pnlNoResults" runat="server" Visible="false">No Data</asp:Panel>
        <asp:Panel ID="pnlResults" runat="server" Visible="false">
            <asp:Repeater ID="rptMonthEvents" runat="server">
                <HeaderTemplate>
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr><td colspan="8" valign="top"><div style="font-weight: bold; font-size: 18px; color: #000;"><asp:Literal ID="litEventDate" runat="server" /></div><br /></td></tr>
                        <asp:Repeater ID="rptEvents" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td width="15"><img src="../images/spacer.gif" alt="" border="0" /></td>
                                    <td align="left" valign="top" width="120">
                                        <b><nobr><asp:Literal ID="litEventDate" runat="server" /></nobr></b>
                                        <nobr><asp:Literal ID="litEventTime" runat="server" /></nobr><br /><br />
                                        <h3><asp:Literal ID="litEventName" runat="server" /></h3>
                                    </td>
                                    <td width="5"><img src="../images/spacer.gif" alt="" border="0" width="5" /></td>
                                    <td valign="top" width="90"><asp:HyperLink ID="hlkEventLogo" runat="server"><asp:Image ID="imgEventLogo" runat="server" AlternateText="" BorderStyle="None" BorderWidth="0" Height="50" Width="90" /></asp:HyperLink></td>
                                    <td width="5"><img src="../images/spacer.gif" alt="" border="0" width="5" /></td>
                                    <td valign="top" width="260">
                                        <table border="0" cellpadding="2" cellspacing="0" width="260">
                                            <tr><td valign="top"><b><asp:HyperLink ID="hlkEventTitle" runat="server" /></b></td></tr>
                                            <tr><td valign="top"><asp:Literal ID="litEventDescription" runat="server" /></td></tr>
                                        </table>
                                    </td>
                                    <td width="5"><img src="../images/spacer.gif" alt="" border="0" width="5" /></td>
                                    <td valign="top" width="160"><asp:Literal ID="litEventAddress" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td width="15"><img src="../images/spacer.gif" alt="" border="0" width="15" /></td>
                                    <td colspan="7"><hr /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                </ItemTemplate>
                <SeparatorTemplate>
                        <tr height="10"><td colspan="8"><br /></td></tr>
                </SeparatorTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
    </div>
</asp:Content>