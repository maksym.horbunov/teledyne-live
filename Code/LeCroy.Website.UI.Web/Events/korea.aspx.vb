﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class events_korea
    Inherits BasePage

#Region "Variables/Keys"
    Public menuURL As String = ""
    Public menuURL2 As String = ""
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim siteMaster As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (siteMaster Is Nothing) Then siteMaster.SetLocaleId = 1042
        Me.Master.PageContentHeader.TopText = "Events"
        Me.Master.PageContentHeader.BottomText = String.Empty
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Events"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.EVENTS_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.EVENTS_MENU
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID

        'lbDetails.Text = getMonthEvents() 'getEventsType()
        If Not Me.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            BindEvents()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptMonthEvents_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptMonthEvents.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim litEventDate As Literal = CType(e.Item.FindControl("litEventDate"), Literal)
                Dim rptEvents As Repeater = CType(e.Item.FindControl("rptEvents"), Repeater)

                litEventDate.Text = String.Format("{0} {1}", dr("MONTH").ToString(), dr("YEAR").ToString())
                Dim sqlString As String = " SELECT a.rowguid, a.title, a.description, a.state, a.city, a.address, a.exhibition_startdate," + _
                                          " a.exhibition_enddate, a.exhibition_time, a.URL, b.NAME, b.SALES_REP_PSG, b.NAME AS COUNTRY, a.Logo, c.TYPE_ID, c.homepage_name " + _
                                          " FROM EXHIBITIONS AS a INNER JOIN  COUNTRY AS b ON a.country_id = b.COUNTRY_ID INNER JOIN" + _
                                          " EXHIBITION_TYPE AS c ON a.type_id = c.type_id   " + _
                                          " WHERE   (a.disabled = 'n') AND (a.exhibition_enddate >= GETDATE() - 1) AND a.LOCALEID = 1042 " + _
                                          " AND (DATENAME(month, a.exhibition_startdate) = @EVENTMONTH) AND (DATEPART([year], a.exhibition_startdate) = @EVENTYEAR) ORDER BY a.exhibition_startdate"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@EVENTMONTH", dr("MONTH").ToString()))
                sqlParameters.Add(New SqlParameter("@EVENTYEAR", dr("YEAR").ToString()))
                AddHandler rptEvents.ItemDataBound, AddressOf rptEvents_ItemDataBound
                Dim events As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
                If (events.Tables(0).Rows.Count > 0) Then
                    rptEvents.DataSource = events
                    rptEvents.DataBind()
                End If
        End Select
    End Sub

    Private Sub rptEvents_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim litEventDate As Literal = CType(e.Item.FindControl("litEventDate"), Literal)
                Dim litEventTime As Literal = CType(e.Item.FindControl("litEventTime"), Literal)
                Dim litEventName As Literal = CType(e.Item.FindControl("litEventName"), Literal)
                'Dim btnAddToCalendar As LinkButton = CType(e.Item.FindControl("btnAddToCalendar"), LinkButton)
                Dim hlkEventLogo As HyperLink = CType(e.Item.FindControl("hlkEventLogo"), HyperLink)
                Dim imgEventLogo As Image = CType(e.Item.FindControl("imgEventLogo"), Image)
                Dim hlkEventTitle As HyperLink = CType(e.Item.FindControl("hlkEventTitle"), HyperLink)
                Dim litEventDescription As Literal = CType(e.Item.FindControl("litEventDescription"), Literal)
                Dim litEventAddress As Literal = CType(e.Item.FindControl("litEventAddress"), Literal)

                Dim startDate As DateTime = Convert.ToDateTime(dr("exhibition_startdate").ToString())
                Dim endDate As DateTime = Convert.ToDateTime(dr("exhibition_enddate").ToString())

                If ((startDate.Day = endDate.Day) Or dr("exhibition_enddate").ToString().Length = 0) Then
                    litEventDate.Text = String.Format("{0} {1}", MonthName(Month(dr("exhibition_startdate").ToString()), True), startDate.Day.ToString())
                Else
                    litEventDate.Text = String.Format("{0} - {1}", String.Format("{0} {1}", MonthName(startDate.Month.ToString(), True), startDate.Day.ToString()), String.Format("{0} {1}", MonthName(endDate.Month.ToString(), True), endDate.Day.ToString()))
                End If
                If (dr("exhibition_time").ToString().Length > 0) Then
                    litEventTime.Text = String.Format("<br />{0}", dr("exhibition_time").ToString())
                End If
                litEventName.Text = GetTranslation(Int32.Parse(dr("TYPE_ID").ToString()), "1042") ' dr("homepage_name").ToString()
                'btnAddToCalendar.CommandArgument = dr("rowguid").ToString()
                If (dr("LOGO").ToString().Length > 0) Then
                    If (dr("URL").ToString().Length > 0) Then
                        hlkEventLogo.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dr("URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                        hlkEventLogo.NavigateUrl = dr("URL").ToString()
                    End If
                    imgEventLogo.ImageUrl = String.Format("{0}{1}", rootDir, dr("LOGO").ToString())
                Else
                    imgEventLogo.ImageUrl = String.Format("{0}/images/spacer.gif", rootDir)
                End If
                If (dr("URL").ToString().Length > 0) Then
                    hlkEventTitle.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", dr("URL").ToString().Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkEventTitle.NavigateUrl = dr("URL").ToString()
                    hlkEventTitle.Text = dr("TITLE").ToString()
                End If
                litEventDescription.Text = dr("description").ToString()
                litEventAddress.Text = ConstructExhibitionAddress(dr("ADDRESS").ToString(), dr("CITY").ToString(), dr("STATE").ToString(), dr("COUNTRY").ToString(), False)
        End Select
    End Sub

    'Protected Sub btnAddToCalendar_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim btn As LinkButton = CType(sender, LinkButton)
    '    Dim sqlString As String = "SELECT a.*, b.NAME AS COUNTRY, c.TYPE_NAME FROM [EXHIBITIONS] a INNER JOIN [COUNTRY] b ON a.COUNTRY_ID = b.COUNTRY_ID INNER JOIN [EXHIBITION_TYPE] c ON a.TYPE_ID = c.TYPE_ID WHERE a.ROWGUID = @ROWGUID"
    '    Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
    '    sqlParameters.Add(New SqlParameter("@ROWGUID", New Guid(btn.CommandArgument.ToString()).ToString()))
    '    Dim exhibitions As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    '    If (exhibitions.Tables(0).Rows.Count > 0) Then
    '        Dim exhibition As DataRow = CType(exhibitions.Tables(0).Rows(0), DataRow)
    '        Dim sb As StringBuilder = New StringBuilder
    '        sb.AppendLine("BEGIN:VCALENDAR")
    '        sb.AppendLine("VERSION:2.0")
    '        sb.AppendLine("BEGIN:VEVENT")
    '        sb.AppendLine(String.Format("UID:RFCALITEM{0}", DateTime.Now.Ticks))
    '        sb.AppendLine(String.Format("DTSTAMP:{0}", UtcDateTime(DateTime.Now)))
    '        Dim startDate As DateTime = Convert.ToDateTime(exhibition("exhibition_startdate").ToString())
    '        Dim endDate As DateTime = Convert.ToDateTime(exhibition("exhibition_enddate").ToString())
    '        sb.AppendLine(String.Format("DTSTART:{0}", UtcDateTime(startDate.ToUniversalTime())))
    '        sb.AppendLine(String.Format("DTEND:{0}", UtcDateTime(endDate.ToUniversalTime())))
    '        sb.AppendLine(String.Format("LOCATION:{0}", Server.HtmlDecode(ConstructExhibitionAddress(exhibition("ADDRESS").ToString(), exhibition("CITY").ToString(), exhibition("STATE").ToString(), exhibition("COUNTRY").ToString(), True))))
    '        sb.AppendLine(String.Format("CATEGORIES:{0}", exhibition("TYPE_NAME").ToString()))
    '        sb.AppendLine("TRANSP:OPAQUE")
    '        sb.AppendLine(String.Format("DESCRIPTION:{0}", exhibition("DESCRIPTION").ToString()))
    '        sb.AppendLine(String.Format("SUMMARY:{0}", exhibition("TITLE").ToString()))
    '        sb.AppendLine("CLASS:PUBLIC")
    '        sb.AppendLine("END:VEVENT")
    '        sb.AppendLine("END:VCALENDAR")
    '        Response.ContentType = "text/calendar"
    '        Response.AddHeader("content-disposition", String.Format("attachment; filename=LeCroy_{0}_{1}.ics", exhibition("TYPE_NAME").ToString().Replace(" ", String.Empty).ToLower(), exhibition("ROWGUID").ToString().Substring(0, 8).ToLower()))
    '        Response.Write(sb.ToString())
    '        Response.End()
    '    End If
    'End Sub
#End Region

#Region "Page Methods"
    Private Sub BindEvents()
        Dim sqlString As String = "SELECT DISTINCT DATENAME([month], exhibition_startdate) as MONTH, " + _
                                  "DATEPART([year], exhibition_startdate) as YEAR, " + _
                                  "DATEPART([month], exhibition_startdate) " + _
                                  "FROM EXHIBITIONS WHERE (disabled = 'n')  " + _
                                  "AND (exhibition_enddate >= (GETDATE()-1)) AND [LOCALEID] = 1042 " + _
                                  "ORDER BY DATEPART([year], exhibition_startdate), DATEPART([month], exhibition_startdate)"
        Dim eventMonths As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray())
        If (eventMonths.Tables.Count <= 0) Then
            pnlResults.Visible = False
            pnlNoResults.Visible = True
            Return
        End If
        If (eventMonths.Tables(0).Rows.Count > 0) Then
            pnlNoResults.Visible = False
            pnlResults.Visible = True
            rptMonthEvents.DataSource = eventMonths
            rptMonthEvents.DataBind()
        Else
            pnlResults.Visible = False
            pnlNoResults.Visible = True
        End If
    End Sub

    Private Function ConstructExhibitionAddress(ByVal address As String, ByVal city As String, ByVal state As String, ByVal country As String, ByVal isForCalendar As Boolean) As String
        Dim retVal As String = String.Empty
        Dim hadAddress As Boolean = False
        Dim hadCity As Boolean = False
        Dim hadState As Boolean = False
        If Not (String.IsNullOrEmpty(address)) Then
            Dim addressString As String = address
            If (isForCalendar) Then addressString = String.Format("{0}", address).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
            retVal += addressString
            hadAddress = True
        End If
        If Not (String.IsNullOrEmpty(city)) Then
            Dim cityString As String = String.Empty
            If (hadAddress) Then
                If (isForCalendar) Then
                    cityString = String.Format(", {0}", city).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    cityString = String.Format("<br />{0}", city)
                End If
            Else
                If (isForCalendar) Then
                    cityString = String.Format("{0}", city).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    cityString = String.Format("{0}", city)
                End If
            End If
            retVal += cityString
            hadCity = True
        End If
        If Not (String.IsNullOrEmpty(state)) Then
            Dim stateString As String = String.Empty
            If (hadAddress Or hadCity) Then
                If (isForCalendar) Then
                    stateString = String.Format(", {0}", state).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    stateString = String.Format("<br />{0}", state)
                End If
            Else
                If (isForCalendar) Then
                    stateString = String.Format("{0}", state).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    stateString = String.Format("{0}", state)
                End If
            End If
            retVal += stateString
            hadState = True
        End If
        If Not (String.IsNullOrEmpty(country)) Then
            Dim countryString As String = String.Empty
            If (hadAddress Or hadCity Or hadState) Then
                If (isForCalendar) Then
                    countryString = String.Format(", {0}", country).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    countryString = String.Format("<br />{0}", country)
                End If
            Else
                If (isForCalendar) Then
                    countryString = String.Format("{0}", country).Replace("<br />", " ").Replace("<br/>", " ").Replace("<br>", " ").Trim()
                Else
                    countryString = String.Format("{0}", country)
                End If
            End If
            retVal += countryString
        End If
        Return retVal
    End Function

    Private Function UtcDateTime(ByVal dateValue As Object) As String
        Dim dateOf As DateTime = Convert.ToDateTime(dateValue.ToString())
        Return String.Format("{0}{1}{2}T{3}{4}{5}Z", dateOf.Year, AppendLeadingZero(dateOf.Month), AppendLeadingZero(dateOf.Day), AppendLeadingZero(dateOf.Hour), AppendLeadingZero(dateOf.Minute), AppendLeadingZero(dateOf.Second))
    End Function

    Private Function AppendLeadingZero(ByVal inValue As Int32) As String
        If (inValue < 10) Then
            Return String.Format("0{0}", inValue)
        End If
        Return inValue
    End Function


    Public Shared Function GetTranslation(ByVal id As Integer, ByVal localeID As String) As String
        Dim strSQL As String = ""
        Dim TableID As String = ""
        Dim ColID As String = ""
        Dim result As String = ""

        Dim table As TableId = TableIdRepository.GetTableId(ConfigurationManager.AppSettings("ConnectionString").ToString(), "EXHIBITION_TYPE")
        If Not (table Is Nothing) Then
            TableID = table.Table_Id.ToString()
        End If
        Dim column As ColumnId = ColumnIdRepository.GetColumnId(ConfigurationManager.AppSettings("ConnectionString").ToString(), "TYPE_ID")
        If Not (column Is Nothing) Then
            ColID = column.Column_Id.ToString()
        End If
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
        sqlParameters.Add(New SqlParameter("@COLUMNID", ColID))
        sqlParameters.Add(New SqlParameter("@ID", id.ToString()))
        sqlParameters.Add(New SqlParameter("@LOCALEID", localeID.ToString()))
        Dim tablesByLanguage As TablesByLanguage = TablesByLanguageRepository.FetchTablesByLanguage(ConfigurationManager.AppSettings("ConnectionString").ToString(), "Select VALUE from TABLESBYLANGUAGE where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@ID and LOCALEID=@LOCALEID", sqlParameters.ToArray()).FirstOrDefault()
        If Not (tablesByLanguage Is Nothing) Then
            result = tablesByLanguage.Value
        End If
        GetTranslation = result.ToString
    End Function
#End Region
End Class