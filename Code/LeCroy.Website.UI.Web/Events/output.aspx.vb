﻿Imports LeCroy.Library.DAL.Common
Imports Newtonsoft.Json

Public Class Events_output
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ParseQueryStrings()
    End Sub

    Private Sub ParseQueryStrings()
        Dim countryId As Nullable(Of Int32) = Nothing
        Dim typeId As Nullable(Of Int32) = Nothing
        Dim categoryId As Nullable(Of Int32) = Nothing
        Dim regionId As Nullable(Of Int32) = Nothing

        If Not (String.IsNullOrEmpty(Request.QueryString("countryid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("countryid"), testVal)) Then
                countryId = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("eventtypeid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("eventtypeid"), testVal)) Then
                typeId = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("eventcategoryid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("eventcategoryid"), testVal)) Then
                categoryId = testVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("regionid"))) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("regionid"), testVal)) Then
                regionId = testVal
            End If
        End If
        OutputJson(countryId, typeId, categoryId, regionId)
    End Sub

    Private Sub OutputJson(ByVal countryId As Nullable(Of Int32), ByVal typeId As Nullable(Of Int32), ByVal categoryid As Nullable(Of Int32), ByVal regionId As Nullable(Of Int32))
        Dim types As List(Of LeCroy.Library.Domain.Common.DTOs.ExhibitionType) = ExhibitionTypeRepository.GetExhibitionTypes(ConfigurationManager.AppSettings("ConnectionString")).ToList()
        Dim events As List(Of SimpleExhibition) = (From e In ExhibitionRepository.GetActiveExhibitions(ConfigurationManager.AppSettings("ConnectionString"), countryId, typeId, categoryid, regionId).ToList()
                                                   Where e.Disabled.ToLower() = "n" Select New SimpleExhibition With {.ExhibitionId = e.ExhibitionId, .Title = e.Title, .Description = e.Description, .TypeId = e.TypeId, .TypeName = types.Where(Function(x) x.TypeId = e.TypeId).FirstOrDefault().TypeName, .CountryId = e.CountryId, .State = e.State, .City = e.City, .Address = e.Address, .ExhibitionStartDate = e.ExhibitionStartDate, .ExhibitionEndDate = e.ExhibitionEndDate, .Url = e.Url, .Logo = e.Logo, .TimeZone = e.TimeZone}).Distinct().ToList()
        Response.Write(JsonConvert.SerializeObject(events))
        Response.End()
    End Sub
End Class

Public Class SimpleExhibition
    Public ExhibitionId As Int32
    Public Title As String
    Public Description As String
    Public TypeId As Int32
    Public TypeName As String
    Public CountryId As Int32
    Public State As String
    Public City As String
    Public Address As String
    Public ExhibitionStartDate As DateTime
    Public ExhibitionEndDate As DateTime
    Public Url As String
    Public Logo As String
    Public TimeZone As String
End Class