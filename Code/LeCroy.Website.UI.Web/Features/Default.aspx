<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Features_Default" Codebehind="Default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <asp:Literal ID="menulabel" runat="server" />
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <h1><asp:Label ID="lblCatName" runat="server" /></h1>
        <p><asp:Label ID="lblCatDesc" runat="server" /></p>
    </div>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <div class="tabs">
            <div class="bg">
                <ul class="tabNavigation"><asp:Label ID="lblTabsToShow" runat="server" /></ul>
                <div id="product_line"><br /><asp:Label ID="lb_productLine" runat="server" /></div>
                <div id="overview">
                    <div class="overviewLeft"><asp:Label ID="lb_overview" runat="server" /></div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <img src="<%=rootDir %>/images/sidebar/lft_hd_product_resources.gif" alt="Product Resources" width="203" height="23">
    <div class="resources">
        <ul>
            <asp:Literal ID="lblDataSheet" runat="server" />
            <asp:Literal ID="lblSpecifications" runat="server" />
            <li class="vid"><a href="<%= rootDir %>/Support/TechLib/Videos.aspx">Videos &amp; Demos</a></li>
            <li class="software"><a href="<%= rootDir %>/Support/SoftwareDownload/default.aspx?group=1">Software &amp; Utilities</a><br />Software Utilities and Firmware Downloads</li>
            <li class="link"><a href="<%= rootDir %>/Support/TechLib/ProductManuals.aspx?type=2&cat=1">Manual</a></li>
            <li class="link"><a href="<%= rootDir %>/Support/TechLib/library.aspx?type=1&cat=1">Application Notes</a></li>
        </ul>
    </div>
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>