Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Features_FeatureOverview
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, dss As DataSet
    Dim modelid As String = ""
    Dim categoryid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscopes"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Features"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim productMenu As String = ""
        Dim productLine As String = ""
        Dim overview As String = ""
        categoryid = AppConstants.CAT_ID_FEATURES

        ' if URL contains query string, validate all query string variables
        '** groupid
        If Len(Request.QueryString("modelid")) > 0 Then
            If IsNumeric(Request.QueryString("modelid")) Then
                modelid = Request.QueryString("modelid")
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not Page.IsPostBack Then
            Dim captionID As String = ""
            Dim menuID As String = ""

            '** captionID
            If Len(Request.QueryString("capid")) = 0 Then
                captionID = AppConstants.SCOPE_CAPTION_ID
            Else
                If IsNumeric(Request.QueryString("capid")) Then
                    captionID = Request.QueryString("capid")
                Else
                    captionID = AppConstants.SCOPE_CAPTION_ID
                End If
            End If

            '** menuID
            If Len(Request.QueryString("mid")) = 0 Then
                menuID = AppConstants.FEATURES_MENU
            Else
                If IsNumeric(Request.QueryString("mid")) Then
                    menuID = Request.QueryString("mid")
                Else
                    menuID = AppConstants.FEATURES_MENU
                End If
            End If
            Session("menuSelected") = captionID
            menuURL = "&capid=" + captionID
            menuURL += "&mid=" + menuID

            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            If Not modelid Is Nothing And Len(modelid) > 0 Then
                strSQL = "SELECT NAME,PRODUCTDESCRIPTION,INTRODUCTION,INTRO_IMAGE_PATH,URL_SPEC,URL_DATASHEET FROM PRODUCT where PRODUCTID=@PRODUCTID"
                sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    'modelName = dr("NAME").ToString()
                    Me.lbModelImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("INTRO_IMAGE_PATH").ToString() +
                                         "'  width='540' height='211'>"
                    Me.landTitle.Text = dr("NAME").ToString()
                    Me.landContent.Text = dr("INTRODUCTION").ToString()
                    'Me.landDesc.Text = Functions.GetProdDescriptionOnProdID(modelid.ToString).ToString
                    Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope - " + dr("NAME").ToString()
                    dataSheetURL = Me.convertToString(dr("URL_DATASHEET"))
                    specificationURL = Me.convertToString(dr("URL_SPEC"))
                    If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
                        lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a></li>"
                    End If
                    If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
                        lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
                    End If
                End If


                strSQL = "SELECT distinct b.PRODUCTID, b.PARTNUMBER, B.PRODUCTDESCRIPTION, b.IMAGEFILENAME,b.INTRODUCTION, " +
                " b.sort_id from product_group a INNER JOIN product b ON a.group_id = b.group_id INNER JOIN config c ON c.optionid = b.PRODUCTID  " +
                " WHERE b.disabled='n' and c.std='f' and a.CATEGORY_ID = @CATEGORYID order by b.sort_id "
                'Response.Write(strSQL)
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If dss.Tables(0).Rows.Count > 0 Then
                    productLine += " <div class='searchResults'> "
                    productLine += "   <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
                    'Me.Panel1.Visible = True
                    Me.menulabel.Text += "<ul>"
                    For Each drr As DataRow In dss.Tables(0).Rows
                        If modelid.ToString = drr("PRODUCTID").ToString Then
                            Me.menulabel.Text += "<li class='current'><a href='/Features/FeatureOverview.aspx?modelid=" + drr("PRODUCTID").ToString() + menuURL + "'>" + drr("PARTNUMBER").ToString() + "</a></li>"
                            Me.menulabel.Text += productMenu
                        Else
                            Me.menulabel.Text += "<li><a href='/Features/FeatureOverview.aspx?modelid=" + drr("PRODUCTID").ToString() + menuURL + "'>" + drr("PARTNUMBER").ToString() + "</a></li>"
                        End If
                        productLine = productLine + "<tr><td class='cell'>"
                        If (Len(convertToString(drr("INTRODUCTION"))) > 0) Then
                            productLine += "<strong><a href='/Features/FeatureOverview.aspx?modelid=" + drr("PRODUCTID").ToString() + menuURL + "'>" + drr("PARTNUMBER") + "</a></strong>" +
                            " - " + Functions.GetProdDescriptionOnProdID(drr("PRODUCTID").ToString).ToString
                        Else
                            productLine += "<strong>" + drr("PARTNUMBER") + "</strong>" +
                            " - " + Functions.GetProdDescriptionOnProdID(drr("PRODUCTID").ToString).ToString
                        End If
                        productLine = productLine + "</td></tr>"
                    Next
                    Me.menulabel.Text += "</ul>"


                    productLine = productLine + "</table>"
                    productLine = productLine + "</div>"
                    lb_productLine.Text = productLine
                    tabsToShow.Append("<li><a href=""#product_line""><img src='")
                    tabsToShow.Append(rootDir)
                    tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")

                End If
                strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
                ' Response.Write(strSQL)
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@ID", modelid))
                sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PRODUCT_OVERVIEW_TYPE))
                overview = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If Not overview = "" Then
                    Me.lb_overview.Text = overview
                    tabsToShow.Append("<li><a href='#overview'><img src='")
                    tabsToShow.Append(rootDir)
                    tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
                End If
            End If
            lblTabsToShow.Text = tabsToShow.ToString()
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            Me.lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            Me.menulabel.Visible = False

        End If
    End Sub

    Protected Function getTitle(ByVal partnum As String, ByVal introduction As String, ByVal pid As String) As String
        If introduction = "" Then
            Return partnum
        Else
            Return "<a href='/Features/default.aspx?modelid=" + modelid + menuURL + "'>" + partnum + "</a>"
        End If
    End Function
End Class