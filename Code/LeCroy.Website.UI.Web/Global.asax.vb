﻿Imports System.Web.Routing
Imports LeCroy.Website.BLL

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Application("SiteMenus") = FeMenuManager.BuildMenusWithPrefetchedSubMenus(ConfigurationManager.AppSettings("ConnectionString").ToString()).ToList()
        RouteTable.Routes.Ignore("{resource}.axd/{*pathinfo}") ' Need this to handle everything else...
        RouteTable.Routes.MapPageRoute("X", "doc/{documenttitle}", "~/doc/pageviewer.aspx")
        RouteTable.Routes.MapPageRoute("Resource1", "{category}/{seriesOrStandard}/resources", "~/resources/default.aspx")
        RouteTable.Routes.MapPageRoute("Resource2", "{category}/{standard}/{series}/resources", "~/resources/default.aspx")
        RouteTable.Routes.MapPageRoute("ResourceDetail1", "{category}/{seriesOrStandard}/resources/{doctype}", "~/resources/details.aspx")
        RouteTable.Routes.MapPageRoute("ResourceDetail2", "{category}/{seriesOrStandard}/{productidOrSeries}/resources/{doctype}", "~/resources/details.aspx")
        RouteTable.Routes.MapPageRoute("ScopeSeries", "oscilloscope/{series}", "~/oscilloscope/oscilloscopeseries.aspx")
        RouteTable.Routes.MapPageRoute("ScopeModel", "oscilloscope/{series}/{productid}", "~/oscilloscope/oscilloscopemodel.aspx")
        RouteTable.Routes.MapPageRoute("ProbeSeries", "probes/{series}", "~/probes/probeseries.aspx")
        RouteTable.Routes.MapPageRoute("ProbeModel", "probes/{series}/{productid}", "~/probes/probemodel.aspx")
        RouteTable.Routes.MapPageRoute("ProtocolStandard", "protocolanalyzer/{standard}", "~/protocolanalyzer/protocolstandard.aspx")
        RouteTable.Routes.MapPageRoute("ProtocolSeries", "protocolanalyzer/{standard}/{series}", "~/protocolanalyzer/protocoloverview.aspx")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
End Class