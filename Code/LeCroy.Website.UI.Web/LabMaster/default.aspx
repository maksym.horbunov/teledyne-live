<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.LabMaster_Overview" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop" style="background: url(/images/oscilloscope/labmaster_background.png) no-repeat; background-color: #cdd0d1;">
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="370" valign="top">
                    <img src="<%=rootDir %>/images/spacer.gif" width="100" height="115" alt="WaveRunner HRO - High Resolution Oscilloscope" />
                    <p style="width: 285px;">LabMaster 9 Zi-A Oscilloscopes have completely re-defined oscilloscope performance and capabilities.  Now, LeCroy extends technology leadership with a new <strong>36 GHz / 80 GS/s</strong> chipset and delivers <strong>65 GHz / 160 GS/s</strong> with patented Digital Bandwidth Interleave (DBI) technology in LabMaster 10 Zi-A.</p>
                </td>
                <td width="245" valign="top"></td>
                <td width="203">
                    <div class="subNav">
                        <ul>
                            <li><a href="<%=rootDir %>/resources/details.aspx?doctypeid=17&mseries=332" onClick="ga('send', 'event', 'LabMaster', 'Side Link', '9 Zi-A Datasheet', {'nonInteraction': 1});">LabMaster 9 Zi-A Datasheet</a></li>
                            <li><a href="<%=rootDir %>/resources/details.aspx?doctypeid=17&mseries=484" onClick="ga('send', 'event', 'LabMaster', 'Side Link', '10 Zi-A Datasheet', {'nonInteraction': 1});">LabMaster 10 Zi-A Datasheet</a></li>
                            <li><a href="<%=rootDir %>/oscilloscope/configure/configure_step2.aspx?seriesid=332" onClick="ga('send', 'event', 'LabMaster', 'Side Link', 'Request 9 Zi-A Quote', {'nonInteraction': 1});">Request 9 Zi-A Quote</a></li>
                            <li><a href="<%=rootDir %>/oscilloscope/configure/modular/default.aspx?seriesid=484" onClick="ga('send', 'event', 'LabMaster', 'Side Link', 'Request 10 Zi Quote', {'nonInteraction': 1});">Request 10 Zi-A Quote</a></li>
                        </ul>
					</div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <h2>Modular, Upgradeable, Flexible</h2>
                    <p class="pad15">The first fundamentally different oscilloscope design in 30 years, LabMaster is an entirely new class of oscilloscopes that is modular, inherently upgradeable, and infinitely flexible</p>
                    <h2>ChannelSync Precision</h2>
                    <p class="pad15">ChannelSync provides precise synchronization of multiple acquisition modules by using a single 10 GHz distributed clock and a single trigger circuit for all acquisition modules.</p>
                    <h2>Advancing Technology Innovation</h2>
                    <p class="pad15">LabMaster's high bandwidth and channel count is advancing technology boundaries.</p>
                </td>
                <td width="25px">&nbsp;</td>
                <td valign="top">
                    <table border="0" cellspacing="0" cellpadding="7" width="550">
                        <tr>
                            <td width="208" align="left" valign="top"></td>
                            <td width="208" align="left" valign="top"><strong>LabMaster 9 Zi-A</strong></td>
                            <td width="208" align="left" valign="top"><strong>LabMaster 10 Zi-A</strong></td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top"><strong>Bandwidth Density</strong></td>
                            <td width="208" align="left" valign="top">4 channels @ 20 GHz<br>per acquisition module</td>
                            <td width="208" align="left" valign="top">4 channels @ 36 GHz<br>per acquisition module</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC"><strong>Bandwidth Range</strong></td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">13 to 30 GHz</td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">20 to 65 GHz</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top"><strong>Maximum Number of Channels</strong></td>
                            <td width="208" align="left" valign="top">10 @ up to 30 GHz<br>20 @ up to 20 GHz</td>
                            <td width="208" align="left" valign="top">40 @ up to 65 GHz<br>80 @ up to 36 GHz</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC"><strong>Maximum Sample Rate</strong></td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">80 GS/s @ 25 to 30 GHz<br>40 GS/s @ 13 to 20 GHz</td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">160 GS/s @ 50 to 65 GHz<br>80 GS/s @ 20 to 36 GHz</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top"><strong>Maximum Memory</strong></td>
                            <td width="208" align="left" valign="top">512 Mpts/Ch @ 25 to 30 GHz<br>256 Mpts/Ch @ 13 to 20 GHz</td>
                            <td width="208" align="left" valign="top">1024 Mpts/Ch @ 50 to 65 GHz<br>512 Mpts/Ch @ 20 to 36 GHz</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC"><strong>Starting From</strong></td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">$146,030</td>
                            <td width="208" align="left" valign="top" bgcolor="#CCCCCC">$215,885</td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top"></td>
                            <td width="208" align="left" valign="top"><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=332" onclick="ga('send', 'event', 'LabMaster', 'Button', 'Explore 9 Zi-A', {'nonInteraction': 1});"><img src="/images/oscilloscope/explore_labmaster_9zi-a.png" alt="Explore LabMaster 9 Zi-A" border="0" /></a></td>
                            <td width="208" align="left" valign="top"><a href="/labmaster/labmaster10zi/" onclick="ga('send', 'event', 'LabMaster', 'Button', 'Explore 10 Zi', {'nonInteraction': 1});"><img src="/images/oscilloscope/explore_labmaster_10zi.png" alt="Explore LabMaster 10 Zi-A" border="0" /></a></td>
                        </tr>
                        <tr>
                            <td width="208" align="left" valign="top"></td>
                            <td colspan="2" align="center">
                                <input type="hidden" id="mseries1" name="mseries1" value="332" />
                                <input type="hidden" id="mseries2" name="mseries2" value="484" /><input type="hidden" id="hdnCallingPage" name="hdnCallingPage" value="LabMasterPage" />
                                <asp:ImageButton ID="btnCompare" runat="server" ImageUrl="~/images/compare_labmaster.png" PostBackUrl="~/resources/compare/compare.aspx" Text="Compare" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>