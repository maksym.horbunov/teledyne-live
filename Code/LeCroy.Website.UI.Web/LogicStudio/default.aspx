<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.LogicAnalyzer_LogicStudio" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="370" valign="top">
                    <img src="<%=rootDir %>/images/category/logicstudio.png" alt="LogicStudio Logic Analyzers">
                    <p>Logic analyzers are known to be slow, complicated and expensive but LogicStudio changes all this by delivering a powerful feature set, high performance hardware and an intuitive point and click user-interface.</p>
                    <h4>With I<sup>2</sup>C, SPI and UART Trigger and Decoding</h4>
                    <p><a href="<%=rootDir %>/Support/SoftwareDownload/LogicStudio.aspx"><img src="/images/buttons/try_it_free.png" border="0" /></a></p>
                </td>
                <td width="248" valign="top"><img src="<%=rootDir %>/images/logicstudio/logicstudio.png" width="242" height="278" border="0" /></td>
                <td width="203" valign="top">
                    <div class="subNav">
                        <ul><li><a href="<%=rootDir %>/logicstudio/logicstudio.aspx?mid=1051">Explore LogicStudio Features</a></li><li><a href="<%=rootDir %>/Support/SoftwareDownload/LogicStudio.aspx">Download Software</a></li><li><a href="<%=rootDir %>/doc/docview.aspx?id=7699">Manual</a></li><li><a href="/logicstudio/logicstudio.aspx?mid=1051#spec">Specifications</a></li></ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
				    <h3>Powerful Feature Set</h3>
                    <p>Timing cursors, history mode and serial data protocol decoding help debug the most complicated problems. <a href="<%=rootDir %>/logicstudio/logicstudio.aspx?mid=1051">More LogicStudio Features</a></p>
                    <h3>Easy to Use</h3>
                    <p>Simple mouse operations control every aspect of the user-interface from panning and zooming waveforms to configuring the trigger.</p>
                    <h3>Digital, Serial, Analog</h3>
                    <p>Turn your PC in to a MSO by connecting LogicStudio to any of ten popular oscilloscopes from Teledyne LeCroy, Tektronix and Agilent. <a href="/logicstudio/logicstudio.aspx?mid=1051#spec">See Compatible Oscilloscopes</a></p>
                    <h3>Try it for Free</h3>
                    <p>Using LogicStudio is the best way to experience the lively display, rich feature set, great debug capabilities and sleek user-interface.  <a href="<%=rootDir %>/Support/SoftwareDownload/LogicStudio.aspx">Download</a></p>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top"><img src="<%=rootDir %>/images/logicstudio/logicstudio_cat_01.png" height="250px" /></td>
            </tr>
        </table>
        <div class="rounded2">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left" valign="top"><img src="/images/logicstudio/logicstudio_wa.png" align="left" /></td>
                    <td width="20px">&nbsp;</td>
                    <td align="center" class="right" valign="middle">
                        <br /><br /><h1>LogicStudio 16 and WaveAce Oscilloscopes</h1><h4>Debug systems with analog, digital and serial data signals.</h4><br />
                        <table border="0">
                            <tr>
                                <td><a href="<%=rootDir %>/logicstudio/logicstudio.aspx?<%=menuURL %>"><img src="/images/buttons/explore_logicstudio.png" border="0" /></a></td>
                                <td width="50px">&nbsp;</td>
                                <td><a href="<%=rootDir %>/Oscilloscope/OscilloscopeSeries.aspx?mseries=402"><img src="/images/buttons/explore_waveace.png" border="0" /></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>