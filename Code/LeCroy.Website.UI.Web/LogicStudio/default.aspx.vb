Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class LogicAnalyzer_LogicStudio
    Inherits BasePage
    Public Shared menuURL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "LogicStudio"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - LogicStudio - Logic Analyzer"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String

        '** captionID
        captionID = AppConstants.PRODUCT_CAPTION_ID

        '** MenuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), 277, CategoryIds.LOGIC_ANALYZERS)) Then
            Response.Redirect("~/")
        End If

        If Not Me.IsPostBack Then
        End If
    End Sub
End Class