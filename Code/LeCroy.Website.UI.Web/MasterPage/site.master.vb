﻿Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Website.BLL
Imports IXFSDK
Imports IXFSDK.Util

Public Class MasterPage_site
    Inherits MasterPageBase

#Region "Variables/Keys"
    Private _setLocaleId As Nullable(Of Int32) = Nothing
    Public Property SetLocaleId() As Nullable(Of Int32)
        Get
            If (_setLocaleId.HasValue) Then
                Return _setLocaleId.Value
            End If
            If Not (String.IsNullOrEmpty(Session("localeid"))) Then
                Dim outVal As Int32 = LocaleIds.ENGLISH_US
                If (Int32.TryParse(Session("localeid"), outVal)) Then
                    Return outVal
                End If
            End If
            Return LocaleIds.ENGLISH_US
        End Get
        Set(ByVal value As Nullable(Of Int32))
            _setLocaleId = value
        End Set
    End Property

    Private _ixf_sdk_client As IXFSDKClient
    Public ReadOnly Property ixf_sdk_client As IXFSDKClient
        Get
            If (_ixf_sdk_client Is Nothing) Then
                Dim ixf_config As IXFConfiguration = New IXFSDKConfiguration()
                ixf_config.SetProperty(IXFConfigKeys.CAPSULE_MODE, IXFConfigKeys.REMOTE_PROD_CAPSULE_MODE)
                ixf_config.SetProperty(IXFConfigKeys.ACCOUNT_ID, "f00000000073308")
                ixf_config.SetProperty(IXFConfigKeys.CHARSET, "UTF-8")
                ixf_config.SetProperty(IXFConfigKeys.PAGE_ALIAS_URL, Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.PathAndQuery)
                ixf_config.SetProperty(IXFConfigKeys.WHITELIST_PARAMETER_LIST, "ixf")
                _ixf_sdk_client = New IXFSDKClient(ixf_config, HttpContext.Current)
            End If
            Return _ixf_sdk_client
        End Get
    End Property
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpModernizer"), True)
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsLeCroyCommon"), True)

        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssBootstrap"), True)
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssHeaderFooterMaster"), True)
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssSerialData"), True)

        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsBootstrap"), True)
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsBootstrapBundle"), True)
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsJQueryHeader"), True)
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsJQueryMain"), True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        BindPage(localeId)
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindPage(ByVal localeId As Int32)
        BindHeader(localeId)
        BindFooter(localeId)
    End Sub

    Private Sub BindHeader(ByVal localeId As Int32)
        Dim controlFile As String

        Select Case localeId
            Case LocaleIds.ENGLISH_US ' load US/EN english
                controlFile = "~/usercontrols/header_EN.ascx"
            Case LocaleIds.ENGLISH_UK ' load UK/EU english
                controlFile = "~/usercontrols/header_EU.ascx"
            Case LocaleIds.FRENCH ' load French
                controlFile = "~/usercontrols/header_FR.ascx"
            Case LocaleIds.GERMAN ' load German
                controlFile = "~/usercontrols/header_DE.ascx"
            Case LocaleIds.ITALIAN ' load Italian
                controlFile = "~/usercontrols/header_IT.ascx"
            Case LocaleIds.JAPANESE ' load Japanese
                controlFile = "~/usercontrols/header_JP.ascx"
            Case LocaleIds.KOREAN ' load Korean
                controlFile = "~/usercontrols/header_KO.ascx"
            Case LocaleIds.CHINESE_PRC ' load Chinese
                controlFile = "~/usercontrols/header_ZH.ascx"
            Case Else ' load US english for all others
                controlFile = "~/usercontrols/header_EN.ascx" ' US used as default for unrecognized locales
        End Select

        Dim vw As Control
        vw = CType(LoadControl(controlFile), UserControl)
        vw.ID = "View_Dyn_Header"
        ucHeader.Controls.Clear()
        ucHeader.Controls.Add(vw)
    End Sub

    Private Sub BindFooter(ByVal localeId As Int32)
        Dim controlFile As String

        Select Case localeId
            Case LocaleIds.ENGLISH_US ' load US/EN english
                controlFile = "~/usercontrols/footer_EN.ascx"
            Case LocaleIds.ENGLISH_UK ' load UK/EU english
                controlFile = "~/usercontrols/footer_EU.ascx"
            Case LocaleIds.FRENCH ' load French
                controlFile = "~/usercontrols/footer_FR.ascx"
            Case LocaleIds.GERMAN ' load German
                controlFile = "~/usercontrols/footer_DE.ascx"
            Case LocaleIds.ITALIAN ' load Italian
                controlFile = "~/usercontrols/footer_IT.ascx"
            Case LocaleIds.JAPANESE ' load Japanese
                controlFile = "~/usercontrols/footer_JP.ascx"
            Case LocaleIds.KOREAN ' load Korean
                controlFile = "~/usercontrols/footer_KO.ascx"
            Case 2052 ' load Chinese
                controlFile = "~/usercontrols/footer_ZH.ascx"
            Case Else ' load US english for all others
                controlFile = "~/usercontrols/footer_EN.ascx"  ' US used as default for unrecognized locales
        End Select

        Dim vw As Control
        vw = CType(LoadControl(controlFile), UserControl)
        vw.ID = "View_Dyn_Footer"
        ucFooter.Controls.Clear()
        ucFooter.Controls.Add(vw)
    End Sub
#End Region
End Class

