﻿Public Class MasterPage_twocolumn_left
    Inherits MasterPageBase

#Region "Variables/Keys"
    Public Property PageContentHeader As UserControls_PageContentHeader
        Get
            Return ucPageContentHeader
        End Get
        Private Set(value As UserControls_PageContentHeader)
        End Set
    End Property
#End Region

#Region "Page Events"
    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssSite"), False)
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssTwoColumnLeftMaster"), False)
        InjectIEAlternateCss(False, False)
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class