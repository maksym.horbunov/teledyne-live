function varargout = xdevdemo100(varargin);
%XDEVDEMO100 Simple math demos in MATLAB.
%
% Utility function for calling the demos related to math.
%
% See also DEMOS, XDEVDEMO.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes:
% $EndNotes

% $Description:    &lt;p&gt;The math demos show examples to get you started with the MATLAB language.&lt;/p&gt;
% $
if (nargin==0)
    help(mfilename)
    return;
end;
