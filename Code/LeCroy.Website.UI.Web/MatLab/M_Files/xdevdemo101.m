function varargout = xdevdemo101(WformIn1);
%XDEVDEMO101 Invert the waveform.
%
% WFORMOUT = XDEVDEMO101(WFORMIN1) inverts WFORMIN1.
%
% MATLAB Code that is executed: 
%
%   WformOut = -1*WformIn1;
%
% See also DEMOS, XDEVDEMO, XDEVDEMO100

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;
varargout{1} = -1*WformIn1;
