function varargout = xdevdemo102(WformIn1);
%XDEVDEMO102 Square the input waveform.
%
% WFORMOUT = XDEVDEMO102(WFORMIN1) outputs the WFORMIN2 
% squared (i.e. raised to the power of 2).
%
% MATLAB Code that is executed: 
%
%   WformOut = WformIn1.^2;
%
% See also DEMOS, XDEVDEMO, XDEVDEMO100.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;
varargout{1} = WformIn1.^2;
