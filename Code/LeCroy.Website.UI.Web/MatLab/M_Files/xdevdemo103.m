function varargout = xdevdemo103(WformIn1);
%XDEVDEMO103 Create a pulse from a sinewave.
%
% WFORMOUT = XDEVDEMO103(WFORMIN1) creates a pulse waveform from
% the sinewave input WFORMIN1.
%
%   MATLAB Code that is executed: 
%
%   idxPositive = find(WformIn1>0);
%   idxNegative = find(WformIn1<0);
%   WformOut = zeros(size(WformIn1));
%   WformOut(idxPositive) = 1;
%   WformOut(idxNegative) = -1;
%
% See also DEMOS, XDEVDEMO, XDEVDEMO100.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

WformIn1 = WformIn1-mean(WformIn1);
idxPositive = find(WformIn1>0);
idxNegative = find(WformIn1<0);
WformOut = zeros(size(WformIn1));
WformOut(idxPositive) = 1;
WformOut(idxNegative) = -1;
varargout{1}=WformOut;
