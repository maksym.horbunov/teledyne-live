function varargout = xdevdemo104(WformIn1);
%XDEVDEMO104 Create a pulse at zero crossings.
%
% WFORMOUT = XDEVDEMO104(WFORMIN1) creates a pulse waveform
% output based upon the zero crossing of the waveform input WFORMIN1.
%
%   MATLAB Code that is executed: 
%
%   sig = (WformIn1>guardband) - (WformIn1<-guardband);
%   idx  = find(sig);  
%   w = find(diff(sig(idx))); 
%   idx1 = idx(w);              
%   WformOut = zeros(size(WformIn1));
%   WformOut(idx1) = 1;
%
% See also DEMOS, XDEVDEMO, XDEVDEMO100.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

WformIn1 = WformIn1-mean(WformIn1);

% I use a guardband in case of noise.
guardband = max(WformIn1)*0.01;

sig = (WformIn1>guardband) - (WformIn1<-guardband);

% Find the index of all the points outside of the guardband
idx  = find(sig);  

% Generate indexes of when we transition from one state to the other.
%   Remember a derivative (diff) is zero for constant values and a number if there is a change.
w = find(diff(sig(idx))); 
idx1 = idx(w);              % Valid Point before Crossing state

% Generate the output signal with zeros except at just before crossing
WformOut = zeros(size(WformIn1));
WformOut(idx1) = 1;

varargout{1} = WformOut;