function varargout = xmapdemo106(WformIn1);
%XMAPDEMO106 Return times of zero crossings times with interpolation.
%
% WFORMOUT = XMAPDEMO106(WFORMIN1) uses interpolation to determine
% zero crossing times associated with WFORMIN1.
%
%   MATLAB Code that is executed: 
%
%   sig = (WformIn1>guardband) - (WformIn1<-guardband);
%   idx  = find(sig);  
%   w = find(diff(sig(idx))); 
%   idx1 = idx(w);              
%   idx2 = idx(w+1);            
%   y1 = waveform(idx1); 
%   y2 = waveform(idx2); 
%   WformOut = ((idx1 - y1.*(idx2-idx1)./(y2-y1))-1)/Fs; 
%
% See also DEMOS, XMAPDEMO, XMAPDEMO100.

%   $Author: Tgaudett $Revision: 1.0 $  $Date: 9/20/02 11:27a $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;
    
%Remove mean
WformIn1=WformIn1-mean(WformIn1);

% I use a guardband in case of noise.
guardband = max(WformIn1)*0.01;

sig = (WformIn1>guardband) - (WformIn1<-guardband);

% Find the index of all the points outside of the guardband
idx  = find(sig);  

% Generate indexes of when we transition from one state to the other.
%   Remember a derivative (diff) is zero for constant values and a number if there is a change.
w = find(diff(sig(idx))); 
idx1 = idx(w);              % Valid Point before Crossing state
idx2 = idx(w+1);            % Valid Point after Crossing

y1 = WformIn1(idx1); y2 = WformIn1(idx2); % Get y values for interp
Fs=1;
% Find Crossing using linear interpolation.  
%   index_value = current_index - current_y/slope
%   Times are the index_value/SampleRate.
varargout{1} = ((idx1 - y1.*(idx2-idx1)./(y2-y1))-1)/Fs; 

