function varargout = xdevdemo200(varargin);
%XDEVDEMO200 Filtering demos.
%
% Utility function for calling the demos related to filtering.
%
% See also DEMOS, XDEVDEMO.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:&lt;p&gt;The filtering demos show three basic filter types: low pass, band pass and high pass generated using Chebyshev filter design techneques.&lt;/p&gt;
% $
if nargin==0
    help(mfilename)
    return;
end;

