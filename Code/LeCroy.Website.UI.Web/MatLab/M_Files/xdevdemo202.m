function varargout = xdevdemo202(WformIn1,varargin)
%XDEVDEMO202 Band pass filter the data using Chebyshev filtering.
%
%   WFORMOUT = XDEVDEMO202(WFORMIN1)uses a cheby2 filter design to 
%   calculate the filter coeficents of a passband filter that are 
%   applied to the input waveform WFORMIN1.  The filtered waveform 
%   is returned as WFORMOUT.  A filter order of 10, a ripple of 20 
%   and a normalized cutoff frequency of [0.2 0.8] are assumed. 
%
%   This function may also be called as: 
%     WFORMOUT = XDEVDEMO202(WFORMIN1,FILTERORDER)
%     WFORMOUT = XDEVDEMO202(WFORMIN1,FILTERORDER,RIPPLE) 
%     WFORMOUT = XDEVDEMO202(WFORMIN1,FILTERORDER,RIPPLE,WN) 
%
%   FIlTERORDER is the order to the filter to calculate. RIPPLE is the 
%   amount of exceptable ripple in DB. WN is the normalized cutoff 
%   frequency.
%   
% Usage:
% WFORMOUT = XDEVDEMO202(WFORMIN1);
%
%   Relevant MATLAB Code that is executed: 
%   [B,A]=cheby2(filteroder,ripple,wn);
%   WformOut = filter(B,A,WformIn1);
%
% Example:
%   WformOut = xdevdemo202(WformIn1,5);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO200, FILTER, CHEBY2.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
switch(nargin)
    case 0
        help xdevdemo202
        return;
    case 1
       filterorder = 10;
       ripple = 20;
       Wn = [0.2,0.8];
    case 2
       filterorder = varargin{1};
       ripple = 20;
       Wn = [0.2,0.8];
    case 3
       filterorder = varargin{1};
       ripple = varargin{2};
       Wn = [0.2,0.8];
    case 4
       filterorder = varargin{1};
       ripple = varargin{2};
       Wn = varargin{3};
    otherwise
        error('Incorect number of inputs.');
end;
[B,A] = cheby2(filterorder,ripple,Wn);
varargout{1} = filter(B,A,WformIn1);

