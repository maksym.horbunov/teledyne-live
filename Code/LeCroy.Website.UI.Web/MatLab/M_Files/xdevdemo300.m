function varargout = xdevdemo300(varargin);
%XDEVDEMO300 Finding Tones demos.
%
% Utility function for calling the demos related to finding tones.
%
% See also DEMOS, XDEVDEMO.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:&lt;p&gt;The finding tones demos show how to extract sinewave information from the data.&lt;/p&gt;
% $
if nargin==0
    help(mfilename)
    return;
end;
