function varargout = xdevdemo400(varargin);
%XDEVDEMO400 Frequency domain demos.
%
% Utility function for calling the demos related to frequency
% domain algorithms.
%
% See also DEMOS, XDEVDEMO.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:&lt;p&gt;The frequency domain section calculates the power spectral density of the data with differnet windowing options.&lt;/p&gt;
% $
if nargin==0
    help(mfilename)
    return;
end;

