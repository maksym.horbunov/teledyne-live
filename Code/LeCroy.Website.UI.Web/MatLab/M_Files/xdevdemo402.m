function varargout = xdevdemo402(WformIn1,varargin);
%XDEVDEMO402 Power spectral density with zero padding.
%
%   WFORMOUT = XDEVDEMO402(WFORMIN1,[WINDOW]) calculates the power 
%   spectral density of the data, WFORMIN1, passed to it.  WINDOW 
%   is the window to use and takes the following values:
%       'RECTANGLE' - rectangular window
%       'HANNING'   - hanning window
%       'HAMMING'   - hamming window
%
%   Relevant MATLAB Code that is executed: 
%   WformOut = 20*log10(abs(fft(WformIn1.*window))/sqrt(N));
%
%
% Example:
%   WfromOut = xdevdemo402(WformIn1,'HANNING');
%
% See also DEMOS, XDEVDEMO, XDEVDEMO400, PSD, HANNING, HAMMING.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

% Calculate the power spectral density of the data.
% put data in dB units.

N=length(WformIn1);
switch(nargin)
    case 1
        window = ones(N,1);
    case 2
        windowName = varargin{1};
        if strcmp(upper(windowName(1:3)),'HAN')
            window = hanning(N);
        elseif strcmp(upper(windowName(1:3)),'HAM')
            window = hamming(N);           
        else
            window = ones(N,1);
        end;
end;
pad = N-2^(floor(log10(N)/log10(2))+1);
varargout{1} = 20*log10(abs(fft([WformIn1;zeros(pad,1)].*window))/sqrt(N));
