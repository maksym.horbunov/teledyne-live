function varargout = xdevdemo500(varargin);
%XDEVDEMO500 Parameters demos.
%
% Utility function for calling the demos related to parameters.
%
% See also DEMOS, XDEVDEMO.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:&lt;p&gt;The parameter section show examples of how to use MATLAB to calculate parameters and not full waveforms.&lt;/p&gt;
% $
if nargin==0
    help(mfilename)
    return;
end;

