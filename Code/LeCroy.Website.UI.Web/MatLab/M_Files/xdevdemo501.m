function varargout = xdevdemo501(WformIn1,varargin);
%XDEVDEMO501 Ratio of points above 0.5V.
%
% PARAMOUT = XDEVDEMO507(WFORMIN1) returns the ratio 
% of points in WFORMIN1 that are above 0.5V.
%
%   MATLAB Code that is executed: 
%   N = length(WformIn1);
%   k = length(find(WformIn1>=testlevel));
%   ParamOut = k/(n-k);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

% Pick a level to care about
if nargin==2
	testlevel = varargin{1};
else
	testlevel = 0.5;
end;

% Get length of waveform
N = length(WformIn1);

% Get length of vector with points above level
k = length(find(WformIn1>=testlevel));

% Return ratio of points above level versus total points
varargout{1} = k/(N-k);

