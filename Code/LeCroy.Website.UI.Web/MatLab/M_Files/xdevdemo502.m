function varargout = xdevdemo502(WformIn1);
%XDEVDEMO502  Standard deviation.
%
% PARAMOUT = XDEVDEMO507(WFORMIN1) returns the standard
% deviation from the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = std(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, STD.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = std(WformIn1);

