function varargout = xdevdemo503(WformIn1);
%XDEVDEMO503  Mean.
%
% PARAMOUT = XDEVDEMO507(WFORMIN1) returns the mean
% value from the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = mean(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, MEAN.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = mean(WformIn1);

