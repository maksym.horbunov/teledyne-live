function varargout = xdevdemo504(WformIn1);
%XDEVDEMO504 Variance.
%
% PARAMOUT = XDEVDEMO504(WFORMIN1) returns the variance
% for the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = var(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, VAR.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = var(WformIn1);

