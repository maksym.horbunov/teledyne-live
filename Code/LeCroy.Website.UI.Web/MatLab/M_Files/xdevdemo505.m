function varargout = xdevdemo505(WformIn1);
%XDEVDEMO505  Median value.
%
% PARAMOUT = XDEVDEMO505(WFORMIN1) returns the median
% value from the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = median(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, MEDIAN.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = median(WformIn1);

