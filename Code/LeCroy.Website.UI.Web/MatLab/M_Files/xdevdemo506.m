function varargout = xdevdemo506(WformIn1);
%XDEVDEMO506  Max value.
%
% PARAMOUT = XDEVDEMO506(WFORMIN1) returns the maximum
% value from the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = max(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, MAX.

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = max(WformIn1);

