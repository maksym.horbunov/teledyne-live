function varargout = xdevdemo507(WformIn1);
%XDEVDEMO507  Min value.
%
% PARAMOUT = XDEVDEMO507(WFORMIN1) returns the minimum
% value from the array WFORMIN1.
%
%   MATLAB Code that is executed: 
%   ParamOut = min(WformIn1);
%
% See also DEMOS, XDEVDEMO, XDEVDEMO500, MIN

%   $Author: Ldean $Revision: 1.0 $  $Date: 9/24/02 5:32p $

%   Testing:
%   This function was tested on the WAVEMASTER series scope.
%

% $Notes: 
% $EndNotes 

% $Description:
% $
if nargin==0
    help(mfilename)
    return;
end;

varargout{1} = min(WformIn1);

