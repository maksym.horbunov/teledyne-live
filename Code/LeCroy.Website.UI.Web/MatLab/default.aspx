﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.Matlab_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript" language="javascript">
        function openWinScreen(URL) {
            aWindow = window.open(URL, "thewindow", "alywaysRaised=yes,width=820,height=620,topmargin=0,leftmargin=0,bgcolor=white");
        }
    </script>
    <div style="background-color: #ffffff; padding: 10px;">
        <img border="0" src="/images/mathworks_logo.jpg"><br />
        <img src="/images/matlab-montage.jpg" width="358px" height="244px" align="right" />
        <h1>Overview – Using MATLAB software with Teledyne LeCroy oscilloscopes.</h1>
        <p>MATLAB&reg; is a software environment used by over 1,000,000 users to make measurements, analyze data, control instruments, and build test systems. MATLAB handles a range of computing tasks from data acquisition and analysis to application development. MATLAB integrates mathematical computing, visualization, and a powerful technical language.</p>
        <p>Read the summary brief called <a href="/doc/docview.aspx?id=906">Using MATLAB with Teledyne LeCroy Oscilloscopes</a> to learn the value of using MATLAB with digital oscilloscopes and how MATLAB enables custom measurements to be made directly on Teledyne LeCroy oscilloscopes. MATLAB includes tools for:<ul><li>Data acquisition</li><li>Data analysis and visualization</li><li>Instrument Control</li><li>Algorithm prototyping and development</li><li>Modeling and simulation</li><li>Programming and application development</li><li>Test system development</li></ul></p>
        <h1>XDEV Advanced Customization package for Teledyne LeCroy Oscilloscopes</h1>
        <hr width="80%" size="1" color="#0076c0" align="left">
        Only Teledyne LeCroy oscilloscopes completely integrate MATLAB into the scope's processing stream by allowing you to create and deploy a new measurement or math algorithm directly into the WaveShape Analysis Engine and display the result on the DSO in real-time! There is no need to establish remote communication between the scope and another program, create a new reference waveform or transfer large data files between the analyzer and another program. With the <a href="/options/productseries.aspx?mseries=289&groupid=144">XDEV Advanced Customization Package</a>, you extend Teledyne LeCroy oscilloscopes to include your most recent new technology algorithms the same day they are created. The XDEV Advanced Customization Package is available for these Teledyne LeCroy oscilloscopes:
        <ul><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=45">WaveMaster 8 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=46">SDA 8 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=47">DDA 8 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=39">WavePro 7 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=40">SDA 7 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=41">DDA 7 Zi</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=48">WaveRunner Xi-A</a></li><li><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=17">WaveRunner 6000A</a></li></ul>
        <p>The following presentation shows in four easy steps how to use MATLAB to make custom measurements on Teledyne LeCroy oscilloscopes such as the Teledyne LeCroy WaveMaster:</p>
        <p>
            <img border="0" src="../images/Icons/IconPDF.gif" /><a href="WM_XDEV-MATLAB_Specific_Site_Presentation.pdf" target="_blank">Presentation - WaveMaster Customization: Using XDEV Advanced Customization and MATLAB (3 MB)</a><br />
            <img border="0" src="/images/Icons/wmp.gif" width="22" height="22" /><a href="https://www.youtube.com/watch?v=e9dWaw_04kA" onclick="GaEventPush('OutboundLink', 'www.youtube.com/watch?v=e9dWaw_04kA');">XDEV Customization Video (6:02)</a>
        </p>
        <h1>Configuring and Controlling Teledyne LeCroy oscilloscopes in MATLAB</h1>
        <hr width="80%" size="1" color="#0076c0" align="left" />
        <p>MATLAB can configure and control Teledyne LeCroy oscilloscopes remotely over TCP/IP or directly on the oscilloscope using the <a href="http://www.mathworks.com/products/instrument/" onclick="GaEventPush('OutboundLink', 'www.mathworks.com/products/instrument/');">MATLAB Instrument Control Toolbox</a>. MATLAB provides a graphical tool, called Test & Measurement Tool, which allows you to interactively communicate with your instrument without writing code.</p>
        <p>Being able to configure and control your oscilloscope from MATLAB is essential if you need to develop interactive or automated test applications that consist of changing the setup of the oscilloscope multiple times. MATLAB can also be used to develop hybrid test systems consisting of a Teledyne LeCroy oscilloscope and other instruments from Teledyne LeCroy or other manufacturers.</p>
        <p>Once data is in MATLAB, you can analyze and visualize it using interactive tools and command-line functions for data analysis tasks such as signal processing, statistical analysis, digital filtering, curve fitting, and nonlinear optimization. Data acquisition and analysis work can also be incorporated into GUI-based applications.MATLAB can automatically generate reports in HTML or other formats so that you can share your results with others.</p>
        <p>To configure and control your oscilloscope, you will need MATLAB, Instrument Control Toolbox, and a MATLAB instrument driver built for your oscilloscope. Ask your Teledyne LeCroy account manager or visit the <a href="http://www.mathworks.com/products/instrument/supportedio12454.html" onclick="GaEventPush('OutboundLink', 'www.mathworks.com/products/instrument/supportedio12454.html');">Teledyne LeCroy MATLAB resource page</a> for obtaining MATLAB, Instrument Control Toolbox, and the appropriate MATLAB instrument driver for your instrument.</p><br />
        <h1>Teledyne LeCroy Application Briefs</h1><br />
        The following Teledyne LeCroy application briefs are available for using MATLAB with Teledyne LeCroy oscilloscopes for filtering signals and decoding NRZ data:<ul><li><a href="/doc/docview.aspx?id=7422">Filter Signals using MATLAB</a></li><li><a href="/doc/docview.aspx?id=583">Decoding NRZ Data using MATLAB</a></li></ul>
        <h1>Request a free trial of MATLAB Software</h1><br />
        You can request a free trial of MATLAB software for use with your Teledyne LeCroy oscilloscope. <a href="http://www.mathworks.com/products/connections/trials/testmeasure_partner.html?&s_cid=LeCroy_scopes" onclick="GaEventPush('OutboundLink', 'www.mathworks.com/products/connections/trials/testmeasure_partner.html?&s_cid=LeCroy_scopes');">Click here</a> if you would like to request a free trial of MATLAB software which will include MATLAB, Instrument Control Toolbox, and other toolboxes that you may want to use to acquire and analyze your oscilloscope data in MATLAB.
        <h1>Download MATLAB Example Files</h1>
        <hr width="80%" size="1" color="#0076c0" align="left">
        Below are example MATLAB script files you can download and use with your Teledyne LeCroy oscilloscope.<br /><br />To learn more about MATLAB, read the <a href="http://www.mathworks.com/products/matlab" onclick="GaEventPush('OutboundLink', 'www.mathworks.com/products/matlab');">MATLAB overview page</a>. The following presentation from The MathWorks provides some additional details on using MATLAB with Teledyne LeCroy oscilloscopes such as the Teledyne LeCroy WaveMaster to make custom measurements:<p><img border="0" src="../images/Icons/IconPDF.gif"><a href="MATLAB-WaveMaster_XDEV_Intro.pdf">MATLAB - WaveMaster XDEV Intro</a><br><br>&nbsp;</p>
        <p align="left">
            <b>Waveform Manipulation &amp; Analysis<br>&nbsp;</b>
            <img border="0" src="/images/icons/Icon_Zip.gif"><a href="M_Files/WveForm_Manipulation_Analysis.zip">Download all files</a>&nbsp;<br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo101.m">Invert the Waveform</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo102.m">Square the Waveform</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo103.m">Create a square wave pulse</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo104.m">Create a pulse at zero crossing</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo105.m">Find zero crossing times with interpolation</a>
        </p>
        <p align="left">
            <b>&nbsp;<br>Filtering<br>&nbsp;</b>
            <img border="0" src="/images/icons/Icon_Zip.gif"><a href="M_Files/Filtering.zip">Download all files</a>&nbsp;<br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo201.m">Low pass filter</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo202.m">Band pass filter</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo203.m">High pass filter</a>
        </p>
        <p>
            <b>&nbsp;<br>Finding Primary Sinewaves<br>&nbsp;</b>
            <img border="0" src="/images/icons/Icon_Zip.gif"><a href="M_Files/Finding_Primary_SineWaves.zip">Download all files</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo301.m">Recover primary sinewave</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif"><a href="M_Files/xdevdemo302.m">Remove primary sinewave</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif"><a href="M_Files/xdevdemo303.m">Recovers multiple primary sinewaves</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif"><a href="M_Files/xdevdemo304.m">Alternative method for same calculation as the previous demonstration</a><br />
        </p>
        <p>
            <b>Frequency Domain Analysis<br>&nbsp;</b>
            <img border="0" src="/images/icons/Icon_Zip.gif"><a href="M_Files/Frequency_Domain_Analysis.zip">Download all files</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo401.m">Power spectral density</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif"><a href="M_Files/xdevdemo402.m">Power spectral density with zero padding</a>
        </p>
        <p align="left">
            <b>&nbsp;<br>Parameter Calculation<br>&nbsp;</b>
            <img border="0" src="/images/icons/Icon_Zip.gif"><a href="M_Files/Parameter_Calculation.zip">Download all files</a>&nbsp;<br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo501.m">Ration of points above 0.5V</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo502.m">Standard deviation</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo503.m">Mean</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo504.m">Variance</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo505.m">Median</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo506.m">Max</a><br />
            <img border="0" src="/images/icons/icon_m_files.gif">&nbsp;<a href="M_Files/xdevdemo507.m">Min</a>
        </p>
    </div>
</asp:Content>