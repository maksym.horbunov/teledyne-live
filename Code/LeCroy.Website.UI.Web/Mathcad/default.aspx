﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.Mathcad_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript" language="javascript">
        function openWinScreen(URL) {
            aWindow = window.open(URL, "thewindow", "alwaysRaised=yes,width=820,height=620,topmargin=0,leftmargin=0,bgcolor=white");
        }
    </script>
    <div style="background-color: #ffffff; padding: 10px;">
        <b><font size="4" color="#800000">XDEV Customization&nbsp;</font><font size="4" color="#000000">&nbsp;<br>&nbsp;</font></b>
        <p align="left"><img border="0" src="/images/mathsoft_mathcad.gif"></p>
        <p>
            <font size="2">
                Founded in 1984, Mathsoft&#8482; has been helping engineers perform and document critical calculations in the familiar language of mathematics for two decades. Its flagship Mathcad<sup>®</sup> product line, the only engineering software package that supports true mathematical notation, is the personal choice of more than 1.5 million engineers and is sold in more than 50 countries.<br>
                <br>From the beginning, Mathcad has stood apart because of its ability to automatically document an engineer&#8217;s calculations in the language of applied mathematics. Rather than conceal calculations in unreadable code or hide formulas behind cells, Mathcad makes it easy for engineers to communicate the rationale that goes into their calculations, enabling collaboration and design quality assurance.<br>
                <br>Mathsoft&#8217;s presence and contributions toward technical innovation in engineering span a broad range of markets and industries, including electronics and electrical equipment, civil engineering services, education, defense and aerospace, telecommunications and government.
            </font>
        </p>
        <p>&nbsp;</p>
        <p><b><font size="3" color="#800000">XDEV Advanced Customization package for WaveMaster</font></b></p>
        <hr width="80%" size="1" color="#800000" align="left">
        <p align="center"><a href="Javascript:openWinScreen('/mathcad/mathcad_screen.gif');"><img border="0" src="mathcad_screen_small.gif"></a></p>
        <p><font color="#000000" size="2">Only WaveMaster completely integrates Mathcad into the scope&#8217;s processing stream by allowing you to create and deploy a new measurement or math algorithm directly into the WaveShape Analysis Engine and display the result on the DSO in real-time! There is no need to establish remote communication between the scope and another program, create a new reference waveform or transfer large data files between the analyzer and another program. With the XDEV Advanced Customization package, you extend WaveMaster to include your most recent new technology algorithms the same day they are created.</font></p>
        <p><font color="#000000" size="2">To view a brief presentation on the use of Mathcad in WaveMaster, please follow this </font><b><font color="#000000" size="1"><a href="Javascript:openWinScreen('wm_xdev-mathcad_specific_site_rev1a.htm');">LINK</a></font></b><font color="#000000" size="2">.</font></p>
        <font color="#000000" size="1"><a href="wavemaster_xdev-mathcad_specific_site_rev1a.pdf">This presentation is also available for download in PDF format.<br>File size 2.1 MBs</a></font><br>
        <font color="#000000" size="2">To learn more about Mathcad please follow this </font><font color="#000000" size="1"><b><a href="http://www.mathcad.com" onclick="GaEventPush('OutboundLink', 'www.mathcad.com');">LINK</a></b></font><br>&nbsp;<p><b><font size="3" color="#800000">Download Mathcad Example Files</font></b></p>
        <hr width="80%" size="1" color="#800000" align="left">
        <font size="2">View the following <a href="Javascript:openWinScreen('wavemaster_xdev-mathcad_rev1a.htm');">Mathcad Demonstration for your WaveMaster Oscilloscope</a> and review the .mcd files below.</font>
        <p><img border="0" src="../images/icons/iconpdf.gif"><font size="2"><a href="wavemaster_xdev-mathcad_rev1a.pdf">Mathcad - WaveMaster XDEV Intro</a><br></font><font size="2"><br>&nbsp;</font></p>
        <p align="left"><font size="2"><b>Waveform Manipulation &amp; Analysis<br>&nbsp;</b></font></p>
        <img border="0" src="/images/icons/icon_mathcad.gif"><font size="1">&nbsp;</font><font color="#000000" size="1"><a href="mcd_files/mathcad_ksmooth_function.mcd">KSmooth Function</a></font><br />
        <img border="0" src="/images/icons/icon_mathcad.gif"><font color="#000000" size="1">&nbsp;<a href="mcd_files/mathcad_medsmooth_function.mcd">Medium Smooth Function</a></font><br />
        <img border="0" src="/images/icons/icon_mathcad.gif"><font size="1">&nbsp;<font color="#000000"><a href="mcd_files/mathcad_invert_function.mcd">Invert Function</a></font></font><br />
        <img border="0" src="/images/icons/icon_mathcad.gif"><font size="1">&nbsp;<font color="#000000"><a href="mcd_files/mathcad_supersmooth_function.mcd">Super Smooth Function</a></font></font>
    </div>
</asp:Content>