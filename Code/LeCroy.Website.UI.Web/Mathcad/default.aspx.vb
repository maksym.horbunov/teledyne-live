﻿Public Class Mathcad_Default
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscopes - MATHCAD PC Oscilloscope Software"
    End Sub
End Class