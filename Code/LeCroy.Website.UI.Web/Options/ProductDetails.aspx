﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Options_ProductDetails" Codebehind="ProductDetails.aspx.vb" %>
<%@ Import Namespace="System.Configuration" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
<ul>
    <asp:Literal ID="lb_leftmenu" runat="server" />
    <asp:Panel ID="pn_leftmenu" runat="server">
        <asp:Literal ID="menulabel" runat="server" />
    </asp:Panel>
</ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Label ID="lbModelImage" runat="server" />
    <div class="content2">
        <h1><asp:Literal ID="landTitle" runat="server" /></h1>
        <p><asp:Literal ID="landContent" runat="server" /></p>
    </div>
    <br />
    <div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Literal ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line"><asp:Label ID="lb_productLine" runat="server" /></div>
            <div id="overview"><asp:Label ID="lb_overview" runat="server" /></div>
            <div id="product_details"><asp:Label ID="lblProductDetail" runat="server" /></div>
            <div id="spec"><asp:Label ID="lblSpecs" runat="server" /></div>
            <div id="accessories">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top" class="cell">
                            <h3><%= modelName%></h3>
                            <asp:DataList ID="dl_accessories" runat="server" RepeatColumns="1" RepeatDirection="Horizontal" Style="border: 1px solid #dedede;" GridLines="Both" BorderStyle="Solid">
                                <ItemTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="item" valign="top" align="left" width="174">
                                                <a href="<%= ConfigurationManager.AppSettings("AwsPublicBaseUrl") %><%#Eval("FILE_FULL_PATH").ToString().ToLower()%>" class="info" target="_blank"><img src="<%=rootDir %><%#Eval("IMAGE").ToString()%>" border="0" width="174" height="97"></a>
                                            </td>
                                            <td class="item" valign="top" align="left">
                                                <a href="<%= ConfigurationManager.AppSettings("AwsPublicBaseUrl") %><%#Eval("FILE_FULL_PATH").ToString().ToLower()%>" class="info" target="_blank"><%# Eval("TITLE")%></a><br /><%# Eval("DESCRIPTION")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="compatability">
                <table border="0" cellpadding="0" cellspacing="0" class="selectionGuide" width="450">
                    <tr>
                        <td valign="top" width="450">
                            <asp:DataList ID="dl_series" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Style="border: 1px solid #dedede;" GridLines="Both" BorderStyle="Solid">
                                <ItemTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="item" valign="top" id="1" align="center">
                                                <div id="info">
                                                    <%#Eval("name").ToString()%><br /><br />
                                                    <a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=<%#Eval("PRODUCT_SERIES_ID").ToString()%><%=menuURL %>" class="info" target="_blank"><img src="<%=rootDir %><%#Eval("PRODUCTIMAGE").ToString()%>" border="0"><span><%#Eval("PRODUCTIMAGEDESC")%></span></a><br />
                                                    &nbsp;&nbsp;<a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=<%#Eval("PRODUCT_SERIES_ID").ToString()%><%=menuURL %>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('ID_<%#Eval("PRODUCT_SERIES_ID").ToString()%>','','<%=rootDir %>/images/category/btn_learn_more_on.gif',0)" target="_blank"><img src="<%=rootDir %>/images/category/btn_learn_more_off.gif" alt="Learn More" name='ID_<%#Eval("PRODUCT_SERIES_ID").ToString()%>' width="74" height="15" border="0"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <LeCroy:Resources ID="ucResources" runat="server" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>