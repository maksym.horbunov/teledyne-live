﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class Options_ProductDetails
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, dss, dsSeries, dsProducts, dsAccessories As DataSet
    Public Shared modelName As String = ""
    Dim categoryid As String = ""
    Dim groupid As String = ""
    Protected modelid As String = ""
    Dim firstmenuid As Integer = 0
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Public menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imageIndex As Integer = 0
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Request.QueryString("capid") Is Nothing Or Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SCOPE_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SCOPE_CAPTION_ID
            End If
        End If

        '** menuID
        If Request.QueryString("mid") Is Nothing Or Len(Request.QueryString("mid")) = 0 Then
            If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                menuID = AppConstants.OPTION_H_MENU
            ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                menuID = AppConstants.OPTION_S_MENU
            ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                menuID = AppConstants.OPTION_A_MENU
            ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                'menuID = AppConstants.OPTION_P_MENU
                Response.Redirect("~/probes/")
            ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                menuID = AppConstants.OPTION_M_MENU
            End If
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_H_MENU
                ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_S_MENU
                ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    menuID = AppConstants.OPTION_A_MENU
                ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                    'menuID = AppConstants.OPTION_P_MENU
                    Response.Redirect("~/probes/")
                ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                    menuID = AppConstants.OPTION_M_MENU
                End If
            End If
        End If

        ' if URL contains query string, validate all query string variables
        '** modelid
        If Len(Request.QueryString("modelid")) = 0 Then
            Response.Redirect("/Options/default.aspx")
        Else
            If IsNumeric(Request.QueryString("modelid")) Then
                modelid = Request.QueryString("modelid")
            Else
                Response.Redirect("/Options/default.aspx")
            End If
        End If

        '** categoryid
        If Len(Request.QueryString("categoryid")) = 0 Then
            categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS
        Else
            If IsNumeric(Request.QueryString("categoryid")) Then
                categoryid = Request.QueryString("categoryid")
            Else
                categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS
            End If
        End If

        If (categoryid = AppConstants.CAT_ID_PROBES) Then
            If Not (String.IsNullOrEmpty(modelid)) Then
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid.ToString()))
                Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT [PRODUCT_SERIES_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [CATEGORY_ID] = @CATEGORYID AND [PRODUCT_ID] = @PRODUCTID", sqlParameters.ToArray()).ToList()
                If (productSeriesCategories.Count = 1) Then
                    Response.Redirect(String.Format("~/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}&capid=102&mid=508", modelid, AppConstants.CAT_ID_PROBES, productSeriesCategories.FirstOrDefault().ProductSeriesId))
                End If
            End If
            Response.Redirect("~/probes/")
        End If
        '** groupid
        ' Response.Write(IsNumeric(Request.QueryString("groupid")))
        If Len(Request.QueryString("groupid")) > 0 Then
            If IsNumeric(Request.QueryString("groupid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), ",") Then
                    groupid = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), ",") + 1)
                Else
                    groupid = SQLStringWithOutSingleQuotes(Request.QueryString("groupid"))
                End If
            Else
                Response.Redirect("/default.aspx")
            End If
        End If

        Session("cataid") = categoryid
        Session("SeriesID") = groupid
        Session("menuSelected") = captionID
        Dim tempVal As Int32 = 0
        If (Int32.TryParse(captionID, tempVal)) Then
            menuURL = "&capid=" + captionID
        Else
            menuURL = "&capid="
        End If
        menuURL += "&mid=" + menuID

        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SCOPE_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SCOPE_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                menuID = AppConstants.OPTION_H_MENU
            ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                menuID = AppConstants.OPTION_S_MENU
            ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                menuID = AppConstants.OPTION_A_MENU
            ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                'menuID = AppConstants.OPTION_P_MENU
                Response.Redirect("~/probes/")
            ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                menuID = AppConstants.OPTION_M_MENU
            End If
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_H_MENU
                ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_S_MENU
                ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    menuID = AppConstants.OPTION_A_MENU
                ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                    'menuID = AppConstants.OPTION_P_MENU
                    Response.Redirect("~/probes/")
                ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                    menuID = AppConstants.OPTION_M_MENU
                End If
            End If
        End If

        Session("menuSelected") = captionID
        ucResources.CategoryId = categoryid
        ucResources.ProductId = modelid
        If Not (String.IsNullOrEmpty(groupid)) Then ucResources.ProductGroupId = groupid

        If Not (FeProductManager.CanOptionShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), modelid, categoryid)) Then
            Response.Redirect("~/")
        End If

        Dim overview As String = ""
        If Not Page.IsPostBack Then
            strSQL = "select CATEGORY_ID,CATEGORY_NAME, DESCRIPTION from CATEGORY where category_id in (@HWOPTIONS,@SWOPTIONS,@ACCESSORIES,@MIXEDSIGNAL,@PROBES) order by sort_id"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@HWOPTIONS", AppConstants.CAT_ID_HARDWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@SWOPTIONS", AppConstants.CAT_ID_SOFTWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@ACCESSORIES", AppConstants.CAT_ID_ACCESSORIES))
            sqlParameters.Add(New SqlParameter("@MIXEDSIGNAL", AppConstants.CAT_ID_MIXEDSIGNAL))
            sqlParameters.Add(New SqlParameter("@PROBES", AppConstants.CAT_ID_PROBES))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each dr As DataRow In ds.Tables(0).Rows
                    'content
                    If Not categoryid Is Nothing And categoryid = dr("CATEGORY_ID").ToString() Then
                        Me.Master.PageContentHeader.BottomText = ds.Tables(0).Rows(i)("CATEGORY_NAME").ToString()
                        sqlParameters = New List(Of SqlParameter)
                        If categoryid = 3 Then
                            strSQL = "select distinct a.group_id, a.group_name, a.sortid " +
                            " from product_group a  INNER JOIN product b ON a.group_id = b.group_id " +
                            " WHERE  a.MENU_YN='y' and b.disabled='n' and a.category_id=@CATEGORYID order by a.sortid "
                            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                        Else
                            If categoryid = 18 Then
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                " where a.MENU_YN='y' and b.disabled='n' " +
                                " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                " aa.product_id=bb.productid and " +
                                " aa.category_id = @CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 UNION SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID AS group_id, " +
                                " PRODUCT_SERIES.NAME AS group_name, PRODUCT_SERIES.SORTID ,'2' as URLType  " +
                                " FROM PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY ON " +
                                " PRODUCT.PRODUCTID = PRODUCT_SERIES_CATEGORY.PRODUCT_ID INNER JOIN " +
                                " PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID " +
                                " WHERE(PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 18) order by sortid "
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            Else
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                        " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                        " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                        " where a.MENU_YN='y' and b.disabled='n' " +
                                        " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                        " aa.product_id=bb.productid and " +
                                        " aa.category_id =@CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 order by sortid "
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            End If
                        End If
                        'Response.Write(strSQL)
                        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        If dss.Tables(0).Rows.Count > 0 Then
                            Me.menulabel.Text += "<ul>"
                            For Each drr As DataRow In dss.Tables(0).Rows
                                If groupid.ToString = drr("group_id").ToString Then
                                    Me.menulabel.Text += "<li class='current'><a href='/Options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                Else
                                    Me.menulabel.Text += "<li><a href='/Options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                End If
                            Next
                            Me.menulabel.Text += "</ul>"
                        End If
                    End If
                Next
            End If

            strSQL = "SELECT NAME,PRODUCTDESCRIPTION,INTRODUCTION,INTRO_IMAGE_PATH,URL_SPEC,URL_DATASHEET FROM PRODUCT where PRODUCTID=@PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                modelName = dr("NAME").ToString()
                Me.lbModelImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("INTRO_IMAGE_PATH").ToString() +
                                     "'  width='540' height='211'>"
                Me.landTitle.Text = dr("NAME").ToString()
                Me.landContent.Text = dr("INTRODUCTION").ToString()
                'Me.landDesc.Text = Functions.GetProdDescriptionOnProdID(modelid.ToString).ToString
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + dr("NAME").ToString()
                dataSheetURL = Me.convertToString(dr("URL_DATASHEET"))
                specificationURL = Me.convertToString(dr("URL_SPEC"))
                'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
                '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a></li>"
                'End If
                'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
                '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
                'End If
            End If

            If Not groupid Is Nothing And Len(groupid) > 0 Then
                'Product Resources
                'If Len(Functions.ProductHasDatasheet(modelid, rootDir).ToString) > 0 Then
                '    lblDataSheet.Text = Functions.ProductHasDatasheet(modelid, rootDir).ToString
                'End If

                'If Len(Functions.ProductHasSpecs(modelid, rootDir).ToString) > 0 Then
                '    lblSpecifications.Text = Functions.ProductHasSpecs(modelid, rootDir).ToString
                'End If

                'If Len(Functions.CategoryHasManuals(categoryid, rootDir).ToString) > 0 Then
                '    lblManuals.Text = Functions.CategoryHasManuals(categoryid, rootDir).ToString
                'End If

                'If Len(Functions.CategoryHasAppNotes(categoryid, rootDir).ToString) > 0 Then
                '    lblAppNotes.Text = Functions.CategoryHasAppNotes(categoryid, rootDir).ToString
                'End If
                ucResources.ShowRequestQuote = True
                'lblRequestQuote.Text = "<li class='link'><a href='" & rootDir & "/Shopper/RequestQuote/Configure_Small_Step2.aspx?SeriesID=" & groupid.ToString & "'>Request Quote</a></li>"

                'Buy Now 
                If Not Session("CountryCode") Is Nothing Then
                    If Len(Session("CountryCode").ToString) > 0 Then
                        If Session("CountryCode").ToString = "208" Then
                            If Functions.ModelHasEStoreLink(modelid, Session("contactid")).ToString.Length > 0 Then
                                ucResources.ShowBuy = True
                                'lblEStore.Text = Functions.ModelHasEStoreLink(modelid, Session("contactid")).ToString
                            End If
                        End If
                    End If
                End If


                'Additional Resources
                If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_PRODUCT.ToString, modelid.ToString).ToString) > 0 Then
                    lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_PRODUCT.ToString, modelid.ToString)
                End If

                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    getProductLineByScopeSeries()
                Else
                    getProductLine()
                End If
                getOverview()
                getProductDetail()
                getSpecs()
                If categoryid = AppConstants.CAT_ID_PROBES Then
                    getAccessories()
                    getCompatability()
                End If
                lblTabsToShow.Text = tabsToShow.ToString()

                If hasDetail Then
                    lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
                ElseIf hasOverview Then
                    lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
                Else
                    lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
                End If

                Me.lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
                Me.menulabel.Visible = False
            End If
        End If
    End Sub

    Private Sub getProductDetail()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", modelid))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PRODUCT_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If Len(dr("OVER_VIEW_DETAILS").ToString()) > 0 Then
                Me.lblProductDetail.Text = dr("OVER_VIEW_DETAILS").ToString()
                firstmenuid = 2
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getSpecs()
        Dim propName As String = ""
        Dim propValue As String = ""
        Dim specString As String = ""
        Dim pNum As Integer
        Dim i As Integer
        Dim ii As Integer
        strSQL = "SELECT distinct b.prop_cat_id, a.prop_cat_name,a.sort_id FROM " +
                " property_category a INNER JOIN  PROPERTY b ON  a.prop_cat_id = b.prop_cat_id " +
                " INNER JOIN PRODUCTPROPVALUE c  ON c.PROPERTYID = b.PROPERTYID " +
                " WHERE c.PRODUCTID=@MODELID and c.VALUE>' ' ORDER by a.SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@MODELID", modelid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            specString = " " + _
                " <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                specString = specString + " <tr> "
                specString = specString + " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>"
                'specString = specString + " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id")) + " - " + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>"

                specString = specString + " </tr> "


                strSQL = "SELECT b.PROPERTYNAME,b.PROPERTYID, c.VALUE " +
                        "FROM  PROPERTY as b INNER JOIN PRODUCTPROPVALUE as c On b.PROPERTYID=c.PROPERTYID " +
                        "WHERE b.prop_cat_id = @PROPCATID and c.PRODUCTID=@PRODUCTID and c.VALUE>' ' ORDER by b.SORTID"
                Dim ds2 As DataSet
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id")).ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid))
                ds2 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                pNum = ds2.Tables(0).Rows.Count
                If pNum > 0 Then
                    For ii = 0 To pNum - 1
                        propName = ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()
                        'propName = "<font color=red>" + ds2.Tables(0).Rows(ii)("PROPERTYID").ToString() + "</font> - " + ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()

                        propValue = ds2.Tables(0).Rows(ii)("VALUE").ToString()
                        specString = specString + " <tr> " + _
                            " <td width='35%' class='cell'>" + propName + "</td>" + _
                            " <td width='65%' class='cell'>" + propValue + "</td> " + _
                           " </tr> "
                    Next
                    specString = specString + " <tr> " + _
                        " <td colspan='2' class='cell'>&nbsp;</td>" + _
                       " </tr> "
                End If
            Next
            specString = specString + " </table> "
            firstmenuid = 3
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
        Me.lblSpecs.Text = specString
    End Sub

    Private Sub getProductLine()

        '*left menu for products
        Dim sqlGetProducts As String
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String = ""
        Dim productLine As String = ""

        sqlGetProducts = "SELECT distinct b.PRODUCTID, b.PARTNUMBER, B.PRODUCTDESCRIPTION, " +
        " b.IMAGEFILENAME,b.INTRODUCTION, b.sort_id " +
        " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
        " where b.disabled='n' and a.group_id = @GROUPID order by b.sort_id "
        'Response.Write(sqlGetProducts)
        'Response.End()
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            productLine = "<p><span class='blue'>&nbsp;&nbsp;&nbsp;<strong> " + _
                        Functions.getTitleForGroup(groupid) + _
                        "</strong></span></p><br /> "

            productLine += " <div class='searchResults'> "
            productLine += "   <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                productLine = productLine + "<tr><td class='cell'>"
                If (Len(convertToString(dr("INTRODUCTION"))) > 0) Then
                    productLine += "<strong><a href='/Options/ProductDetails.aspx?modelid=" + dr("PRODUCTID").ToString() + _
                    "&categoryid=" + categoryid + "&groupid=" + groupid + "'>" + dr("PARTNUMBER") + "</a></strong>" + _
                    " - " + Functions.GetProdDescriptionOnProdID(dr("PRODUCTID").ToString).ToString
                Else
                    productLine += "<strong>" + dr("PARTNUMBER") + "</strong>" + _
                    " - " + Functions.GetProdDescriptionOnProdID(dr("PRODUCTID").ToString).ToString
                End If
                productLine = productLine + "</td></tr>"
            Next

            productLine = productLine + "</table>"
            productLine = productLine + "</div>"
            lb_productLine.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub

    Private Sub getOverview()
        If Len(groupid) > 0 Then
            strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", groupid))
            sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.GROUP_OVERVIEW_TYPE))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                Me.lb_overview.Text = Me.convertToString(dr("OVER_VIEW_DETAILS"))
                tabsToShow.Append("<li><a href='#overview'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getProductLineByScopeSeries()
        Dim pNum As Integer = 0
        Dim j As Integer = 0
        Dim oNum As Integer = 0
        Dim k As Integer = 0
        Dim strProductLineBySeries As String = ""
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
            strProductLineBySeries = "<p><span class='blue'>&nbsp;&nbsp;&nbsp;<strong> " + _
                        Functions.getTitleForGroup(groupid) + _
                        "</strong></span></p><br /> "
            strSQL = "SELECT DISTINCT e.PRODUCT_SERIES_ID,e.NAME, e.SORTID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                    " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                    " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                    " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                    " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                    " WHERE b.disabled='n' " +
                    " and c.std = 'n' " +
                    " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                    " ON aa.product_id=bb.productid WHERE aa.category_id = @CATEGORYID and bb.disabled='n' ) AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by e.SORTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            dsSeries = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            pNum = dsSeries.Tables(0).Rows.Count()
            If pNum > 0 Then
                strProductLineBySeries += "<div class='accordion'>"
                For j = 0 To pNum - 1
                    Dim dr As DataRow = dsSeries.Tables(0).Rows(j)
                    strProductLineBySeries += "<div class='list'>"

                    strProductLineBySeries += dr("NAME")
                    strProductLineBySeries += "</div>"

                    strProductLineBySeries += "<div class='faq'>"
                    strSQL = "SELECT DISTINCT b.PARTNUMBER, b.PRODUCTDESCRIPTION, b.INTRODUCTION, b.PRODUCTID, b.SORT_ID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                            " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                            " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                            " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                            " WHERE b.disabled='n' " +
                            " and c.std ='n' " +
                            " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                            " ON aa.product_id=bb.productid WHERE aa.category_id = @CATEGORYID and bb.disabled='n') AND e.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by b.SORT_ID"
                    ' Response.Write(strSQL)
                    'Response.End()
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("PRODUCT_SERIES_ID").ToString()))
                    dsProducts = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    oNum = dsProducts.Tables(0).Rows.Count()
                    If oNum > 0 Then
                        strProductLineBySeries += "<table width=450>"
                        For k = 0 To oNum - 1
                            Dim drr As DataRow = dsProducts.Tables(0).Rows(k)
                            strProductLineBySeries += "<td valign='top' class='cell'>"
                            strProductLineBySeries += getTitle(drr("PARTNUMBER").ToString, drr("INTRODUCTION").ToString, drr("PRODUCTID").ToString) + " - " + Functions.GetProdDescriptionOnProdID(drr("PRODUCTID").ToString).ToString
                            strProductLineBySeries += "</td>"
                            strProductLineBySeries += "</tr>"
                        Next
                        strProductLineBySeries += "</table>"
                    End If
                    strProductLineBySeries += "</div>"
                Next
                strProductLineBySeries += "</div>"
                lb_productLine.Text = strProductLineBySeries
            End If

        End If
    End Sub

    Protected Function getTitle(ByVal partnum As String, ByVal introduction As String, ByVal pid As String) As String
        If introduction = "" Then
            Return "<strong>" + partnum + "</strong>"
        Else
            Return "<a href='/Options/ProductDetails.aspx?modelid=" + pid + "&categoryid=" + categoryid + "&groupid=" + groupid + menuURL + "'><strong>" + partnum + "</strong></a>"
        End If
    End Function
    Private Sub getCompatability()
        Dim strResults As String = ""
        strSQL = " SELECT DISTINCT d.PRODUCT_SERIES_ID, d.NAME, d.PRODUCTIMAGE, d.PRODUCTIMAGEDESC, d.SORTID " +
                " FROM PRODUCT AS a INNER JOIN  CONFIG AS c " +
                " ON a.PRODUCTID = c.PRODUCTID INNER JOIN " +
                " PRODUCT_SERIES AS d INNER JOIN  PRODUCT_SERIES_CATEGORY AS e " +
                " ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID ON a.PRODUCTID = e.PRODUCT_ID " +
                " WHERE (e.CATEGORY_ID = 1) AND (a.DISABLED = 'n') AND (a.DISABLED = 'n') AND c.OPTIONID = @OPTIONID ORDER BY d.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@OPTIONID", modelid.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Me.dl_series.DataSource = ds
            Me.dl_series.DataBind()
            tabsToShow.Append("<li><a href='#compatability'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/compatability_off.gif' id='compatability' border='0'></a></li>")
        End If
    End Sub
    Private Sub getAccessories()
        Dim strResults As String = ""
        strSQL = " SELECT a.DOCUMENT_ID, a.TITLE, a.DESCRIPTION, a.FILE_FULL_PATH, a.VERSION, a.DOC_SIZE, a.COMMENTS, a.IMAGE " +
                 " FROM DOCUMENT_CATEGORY AS b INNER JOIN DOCUMENT AS a ON a.DOCUMENT_ID = b.DOCUMENT_ID " +
                 " WHERE a.DOC_TYPE_ID = 19 AND a.POST = 'y' AND a.COUNTRY_ID = 208 and b.PRODUCT_ID = @PRODUCTID ORDER BY a.SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", modelid.ToString()))
        dsAccessories = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If dsAccessories.Tables(0).Rows.Count > 0 Then
            Me.dl_accessories.DataSource = dsAccessories
            Me.dl_accessories.DataBind()
            tabsToShow.Append("<li><a href='#accessories'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/accessories_off.gif' id='accessories' border='0'></a></li>")
        End If
    End Sub
End Class