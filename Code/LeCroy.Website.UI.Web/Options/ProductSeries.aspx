﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Options_ProductSeries" Codebehind="ProductSeries.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <asp:Literal ID="menulabel" runat="server" />
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <div class="intro">
                    <h1><asp:Label ID="lbSeriesTileTop" runat="server" /></h1>
                    <p><asp:Label ID="lbSeriesDesc" runat="server" /></p>
                    <asp:Label ID="lbSeriesTileBelow" runat="server" CssClass="lecroyblue" />
                </div>
            </td>
            <td valign="top"><asp:Label ID="lbSeriesImage" runat="server" /></td>
        </tr>
    </table>
    <div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Literal ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line"><br /><asp:Label ID="lbQuickSpecs" runat="server" /></div>
            <div id="overview"><asp:Label ID="lblOverview" runat="server" /></div>
            <div id="product_details"><asp:Label ID="lblProductDetail" runat="server" /></div>
            <div id="compatability">
                <table width="450" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                    <tr>
                        <td valign="top" width="450">
                            <asp:DataList ID="dl_series" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Style="border: 1px solid #dedede;" GridLines="Both" BorderStyle="Solid">
                                <ItemTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="item" valign="top" id="1" align="center">
                                                <div id="info">
                                                    <%#Eval("name").ToString()%><br />
                                                    <br /><a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=<%#Eval("PRODUCT_SERIES_ID").ToString()%><%=menuURL %>" class="info" target="_blank"><img src="<%=rootDir %><%#Eval("PRODUCTIMAGE").ToString()%>" border="0"><span><%#Eval("PRODUCTIMAGEDESC")%></span></a>
                                                    <br />&nbsp;&nbsp;<a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=<%#Eval("PRODUCT_SERIES_ID").ToString()%><%=menuURL %>" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('ID_<%#Eval("PRODUCT_SERIES_ID").ToString()%>','','<%=rootDir %>/images/category/btn_learn_more_on.gif',0)" target="_blank"><img src="<%=rootDir %>/images/category/btn_learn_more_off.gif" alt="Learn More" name='ID_<%#Eval("PRODUCT_SERIES_ID").ToString()%>' width="74" height="15" border="0"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="spec"><asp:Literal ID="lblSpecs" runat="server" /></div>
            <div id="options"><asp:Literal ID="lblOptions" runat="server" /></div>
            <div id="rec_config"><asp:Label ID="lblRecConfig" runat="server" /></div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="lbPromotion" runat="server" />
    <div class="greyPadding"></div>
    <LeCroy:Resources ID="ucResources" runat="server" ShowBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>