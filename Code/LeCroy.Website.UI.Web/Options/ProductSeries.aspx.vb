﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class Options_ProductSeries
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, ds1, dss, ds2, dsSeries, dsProducts As DataSet
    Public mseries As String = ""
    Dim categoryid As String = ""
    Dim groupid As String = ""
    Dim standardid As String = ""
    Dim firstmenuid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim promoImages As String = ""
    Dim seriesName As String = ""
    Dim seriesExplorerText As String = ""
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim products As String = ""
    Dim productLine As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imageIndex As Integer = 0
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Request.QueryString("capid") Is Nothing Or Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SCOPE_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SCOPE_CAPTION_ID
            End If
        End If

        '** groupid
        If Len(Request.QueryString("groupid")) > 0 Then
            If IsNumeric(Request.QueryString("groupid")) Then
                groupid = Request.QueryString("groupid")
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        categoryid = Functions.GetCategoryIDonGroupID(groupid)
        If (categoryid = AppConstants.CAT_ID_PROBES) Then
            If Not (String.IsNullOrEmpty(groupid)) Then
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", groupid.ToString()))
                Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT [PRODUCT_SERIES_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [CATEGORY_ID] = @CATEGORYID AND [PRODUCT_ID] IN (SELECT [PRODUCTFKID] FROM [PRODUCT_GROUP_XREF] WHERE [PRODUCTGROUPFKID] = @PRODUCTGROUPID)", sqlParameters.ToArray()).ToList()
                If (productSeriesCategories.Count = 1) Then
                    Response.Redirect(String.Format("~/probes/probeseries.aspx?mseries={0}", productSeriesCategories.FirstOrDefault().ProductSeriesId))
                End If
            End If
            Response.Redirect("~/probes/")
        End If

        '** mseries
        If Len(Request.QueryString("mseries")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("mseries")) Then
                mseries = Request.QueryString("mseries")
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        If (String.IsNullOrEmpty(categoryid)) Then
            Response.Redirect("~/")
        End If

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries, categoryid)) Then
            Response.Redirect("~/")
        End If

        '** menuID
        If Request.QueryString("mid") Is Nothing Or Len(Request.QueryString("mid")) = 0 Then
            If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                menuID = AppConstants.OPTION_H_MENU
            ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                menuID = AppConstants.OPTION_S_MENU
            ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                menuID = AppConstants.OPTION_A_MENU
            ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                'menuID = AppConstants.OPTION_P_MENU
                Response.Redirect("~/probes/")
            ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                menuID = AppConstants.OPTION_M_MENU
            End If
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_H_MENU
                ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_S_MENU
                ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    menuID = AppConstants.OPTION_A_MENU
                ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                    'menuID = AppConstants.OPTION_P_MENU
                    Response.Redirect("~/probes/")
                ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                    menuID = AppConstants.OPTION_M_MENU
                End If
            End If
        End If

        'categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS
        '** standardid
        'If Len(Request.QueryString("standardid")) = 0 Then
        '    Response.Redirect("default.aspx")
        'Else
        '    If IsNumeric(Request.QueryString("standardid")) Then
        '        standardid = Request.QueryString("standardid")
        '        If Not CheckSeriesExists(standardid, 21) Then
        '            Response.Redirect("default.aspx")
        '        End If
        '    Else
        '        Response.Redirect("default.aspx")
        '    End If
        'End If
        ucResources.CategoryId = categoryid
        ucResources.ProductSeriesId = mseries
        ucResources.ProductGroupId = groupid

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        Session("cataid") = categoryid
        If Not groupid Is Nothing Then
            If IsNumeric(categoryid) Then
                ucResources.ShowRequestQuote = True
                'lblReqQuote.Text = "<li class='link'><a href=""/shopper/RequestQuote/select_scope.aspx?SeriesID=" + groupid + "&mseries=" + mseries + """>Request Quote</a></li>"
            End If
        End If
        If Not Me.IsPostBack Then
            '  Functions.CreateMetaTags(Me.Header, mseries, AppConstants.SERIES_OVERVIEW_TYPE)
            ' If categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
            'GetProductsByScopeSeries()
            GetSeries()
            'ElseIf categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_ACCESSORIES Then
            '    GetProductsByScopeSeries()
            'Else
            '    GetProducts()
            'End If
            getSeriesOverview()
            'getProductDetail()
            getSpecs()

            getCompatability()
            getRecommendedConfigurations()

            lblTabsToShow.Text = tabsToShow.ToString()

            If hasOverview Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            ElseIf hasDetail Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
            Else
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
            End If

            'Functions.CreateMetaTags(Me.Header, standardid, AppConstants.SERIES_OVERVIEW_TYPE)
            If Not categoryid Is Nothing And Len(categoryid) > 0 Then
                strSQL = "select CATEGORY_ID,CATEGORY_NAME, DESCRIPTION from CATEGORY where category_id=@CATEGORYID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count > 0 Then
                    Me.Master.PageContentHeader.BottomText = ds.Tables(0).Rows(0)("CATEGORY_NAME").ToString()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        sqlParameters = New List(Of SqlParameter)
                        If categoryid = 3 Then
                            strSQL = "select distinct a.group_id, a.group_name, a.sortid ,'1' as URLType" +
                            " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                            " where a.MENU_YN='y' and b.disabled='n' " +
                            " and a.category_id=@CATEGORYID order by a.sortid "
                            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                        Else
                            If categoryid = 18 Then
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                " where a.MENU_YN='y' and b.disabled='n' " +
                                " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                " aa.product_id=bb.productid and " +
                                " aa.category_id = @CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 UNION SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID AS group_id, " +
                                " PRODUCT_SERIES.NAME AS group_name, PRODUCT_SERIES.SORTID ,'2' as URLType  " +
                                " FROM PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY ON " +
                                " PRODUCT.PRODUCTID = PRODUCT_SERIES_CATEGORY.PRODUCT_ID INNER JOIN " +
                                " PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID " +
                                " WHERE(PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 18) order by sortid "
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            Else
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                        " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                        " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                        " where a.MENU_YN='y' and b.disabled='n' " +
                                        " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                        " aa.product_id=bb.productid and " +
                                        " aa.category_id = @CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 order by sortid "
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            End If

                        End If
                        ' Response.Write(strSQL)
                        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        If dss.Tables(0).Rows.Count > 0 Then
                            Me.menulabel.Text += "<ul>"
                            For Each drr As DataRow In dss.Tables(0).Rows
                                If groupid.ToString = drr("group_id").ToString Then
                                    If drr("URLType").ToString = "1" Then
                                        Me.menulabel.Text += "<li class='current'><a href='/Options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + menuURL + "'>" + drr("group_name").ToString() + "</a></li>"
                                    Else
                                        Me.menulabel.Text += "<li class='current'><a href='/Oscilloscope/OscilloscopeSeries.aspx?mseries=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                    End If

                                Else
                                    If drr("URLType").ToString = "1" Then
                                        Me.menulabel.Text += "<li><a href='/Options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + menuURL + "'>" + drr("group_name").ToString() + "</a></li>"
                                    Else
                                        Me.menulabel.Text += "<li><a href='/Oscilloscope/OscilloscopeSeries.aspx?mseries=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                    End If
                                End If
                            Next
                            Me.menulabel.Text += "</ul>"
                        End If
                    Next
                End If
            End If

            strSQL = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID=@PRODUCTSERIESID AND [DISABLED]='N'"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                seriesName = Me.convertToString(dr("NAME"))
                dataSheetURL = Me.convertToString(dr("URL_DATASHEET"))
                specificationURL = Me.convertToString(dr("URL_SPEC"))
                promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
                seriesExplorerText = Me.convertToString(ds.Tables(0).Rows(0)("SHORT_NAME"))
                lbSeriesTileTop.Text = seriesName
                lbSeriesTileBelow.Text = "Explore " + seriesExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + seriesExplorerText + "' >"
                lbSeriesImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString() + "' width='260' />"
                lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Serial Data - " + dr("NAME").ToString()
            End If
            'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
            '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a><br />Learn more about " & seriesName & "</li>"
            'End If
            'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
            '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
            'End If

            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
            'If Len(Functions.CategoryHasManuals(categoryid, rootDir).ToString) > 0 Then
            '    lblManuals.Text = Functions.CategoryHasManuals(categoryid, rootDir).ToString
            'End If

            'If Len(Functions.CategoryHasAppNotes(categoryid, rootDir).ToString) > 0 Then
            '    lblAppNotes.Text = Functions.CategoryHasAppNotes(categoryid, rootDir).ToString
            'End If


            'Buy Now 
            If Not Session("CountryCode") Is Nothing Then
                If Len(Session("CountryCode").ToString) > 0 Then
                    If Session("CountryCode").ToString = "208" Then
                        If Functions.SeriesHasEStoreLink(mseries, Session("contactid")).ToString.Length > 0 Then
                            ucResources.ShowBuy = True
                            'lblEStore.Text = Functions.SeriesHasEStoreLink(mseries, Session("contactid")).ToString
                        End If
                    End If
                End If
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString)
            End If

            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Private Sub getProductDetail()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If Len(dr("OVER_VIEW_DETAILS").ToString()) > 0 Then
                Me.lblProductDetail.Text = dr("OVER_VIEW_DETAILS").ToString()
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getSpecs()
        Dim propName As String = ""
        Dim propValue As String = ""
        Dim specString As String = ""
        Dim pNum As Integer
        Dim i As Integer
        Dim ii As Integer
        strSQL = "SELECT distinct b.prop_cat_id, a.prop_cat_name,a.sort_id FROM " +
                " property_category a, PROPERTY b, SERIES_PROPERTY c WHERE " +
                " a.prop_cat_id = b.prop_cat_id" +
                " and b.PROPERTYID = c.PROPERTY_ID and c.PRODUCT_SERIES_ID=@PRODUCTSERIESID ORDER by a.SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            specString = " " + _
                " <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                specString = specString + " <tr> " + _
                    " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>" + _
                   " </tr> "


                strSQL = "SELECT b.PROPERTYNAME, c.PROPERTY_VALUE FROM " +
                        " PROPERTY b, SERIES_PROPERTY c WHERE " +
                        " b.PROPERTYID = c.PROPERTY_ID and b.prop_cat_id = @PROPCATID and c.PRODUCT_SERIES_ID=@PRODUCTSERIESID ORDER by b.SORTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id")).ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
                Dim ds2 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                pNum = ds2.Tables(0).Rows.Count
                If pNum > 0 Then
                    For ii = 0 To pNum - 1
                        propName = ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()
                        propValue = ds2.Tables(0).Rows(ii)("PROPERTY_VALUE").ToString()
                        specString = specString + " <tr> " + _
                            " <td width='35%' class='cell'>" + propName + "</td>" + _
                            " <td width='65%' class='cell'>" + propValue + "</td> " + _
                           " </tr> "
                    Next
                    specString = specString + " <tr> " + _
                        " <td colspan='2' class='cell'>&nbsp;</td>" + _
                       " </tr> "
                End If
            Next
            specString = specString + " </table> "
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
        Me.lblSpecs.Text = specString

    End Sub

    Private Sub getSeriesOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            hasOverview = True
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If
    End Sub

    'Private Sub getProductLine()

    '    '*left menu for series
    '    Dim dr1 As SqlDataReader
    '    Dim countFromScope As String = ""
    '    Dim countFromSerialData As String = ""

    '    strSQL = "SELECT DISTINCT a. PRODUCT_SERIES_ID, a.NAME, a.DESCRIPTION FROM " + _
    '   " PRODUCT_SERIES a, PRODUCT_SERIES_CATEGORY b, PRODUCT c WHERE " + _
    '   " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " + _
    '   " AND b.product_id=c.productid " + _
    '   " AND c.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + ") " + _
    '   " AND b.CATEGORY_ID=" + AppConstants.CAT_ID_SERIAL_DATA & _
    '   " AND a.PRODUCT_SERIES_ID=" + standardid

    '    ds = DbHelperSQL.Query(strSQL)
    '    If (ds.Tables(0).Rows.Count > 0) Then
    '        lbSeries1.Text = "<a href='/SerialData/SerialDataStandard.aspx?standardid=" + standardid + menuURL + "'> " + _
    '                        ds.Tables(0).Rows(0)("NAME").ToString() + " </a> "
    '    End If
    '    '*left menu for products
    '    Dim productID As String
    '    Dim pNum As Integer
    '    Dim productID2 As String = ""
    '    Dim i As Integer

    '    strSQL = "select distinct product_series_id from PRODUCT_SERIES_CATEGORY " & _
    '     "where(category_id = " & AppConstants.CAT_ID_SCOPE & ") " & _
    '     "AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " & _
    '     "and product_id in (select product_id from PRODUCT_SERIES_CATEGORY  " & _
    '     "where(category_id = " & AppConstants.CAT_ID_SERIAL_DATA & ") " & _
    '     "and PRODUCT_SERIES_ID= " & standardid & ")"

    '    DbHelperSQL.Query(ds, "1", strSQL)

    '    For Each dr As DataRow In ds.Tables("1").Rows
    '        'Count # of scopes assigned to serial data stndard
    '        strSQL = "SELECT count(product_id) as amount FROM PRODUCT_SERIES_CATEGORY " & _
    '            "where(category_id = " & AppConstants.CAT_ID_SERIAL_DATA & ") " & _
    '          "and PRODUCT_SERIES_ID= " & standardid & " " & _
    '          "AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " & _
    '          "and PRODUCT_ID in ( " & _
    '          "select PRODUCT_ID from PRODUCT_SERIES_CATEGORY " & _
    '            "where(category_id =  " & AppConstants.CAT_ID_SCOPE & ") " & _
    '            "and product_series_id = " & dr("product_series_id").ToString() & ")"

    '        dr1 = DbHelperSQL.ExecuteReader(strSQL)
    '        dr1.Read()
    '        countFromSerialData = dr1("amount")
    '        dr1.Close()

    '        'Count # scopes in the scope series
    '        'if  countFromSerialData=countFromScope, display scope series name, if not - partnumbers
    '        strSQL = "SELECT count(product_id) as amount FROM PRODUCT_SERIES_CATEGORY " & _
    '            "where category_id =  " & AppConstants.CAT_ID_SCOPE & " " & _
    '          "AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " & _
    '          "and PRODUCT_SERIES_ID = " & dr("product_series_id").ToString()

    '        dr1 = DbHelperSQL.ExecuteReader(strSQL)
    '        dr1.Read()
    '        countFromScope = dr1("amount")
    '        dr1.Close()
    '        'Response.Write(countFromSerialData & "<br>")
    '        ' Response.Write(countFromScope & "<br>")
    '        If countFromSerialData = countFromScope Then
    '            strSQL = "SELECT NAME as partnumber,0 as PRODUCT_ID,1 as CATEGORY_ID," & dr("product_series_id").ToString() & " as PRODUCT_SERIES_ID , productimage as IMAGEFILENAME,'ScopeSeries.aspx' as target, " & _
    '            "'" & standardid & "' as standardid, PRODUCTIMAGEDESC as DESCRIPTION,  'quickBoxBlue' as css , 1 as sortid " & _
    '            " FROM PRODUCT_SERIES " & _
    '            " where PRODUCT_SERIES_ID=" & dr("product_series_id").ToString() & _
    '            " order by sortid"
    '        Else
    '            strSQL = "SELECT     b.PARTNUMBER, a.PRODUCT_ID,1 as CATEGORY_ID," & dr("product_series_id").ToString() & " as PRODUCT_SERIES_ID , b.SMALL_IMAGE_PATH AS IMAGEFILENAME, 'SDScopesOverview.aspx' AS target, " & _
    '            "'" & standardid & "' AS standardid, b.PARTNUMBER AS DESCRIPTION,'quickBoxBlue' AS css, b.sort_id as sortid " & _
    '            " From PRODUCT_SERIES_CATEGORY a INNER JOIN product b  On a.product_id=b.productid " & _
    '            "where category_id =  " & AppConstants.CAT_ID_SERIAL_DATA & " " & _
    '            "and PRODUCT_SERIES_ID=  " & standardid & " " & _
    '            "AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " )  " & _
    '            "and PRODUCT_ID in ( " & _
    '            "select PRODUCT_ID from PRODUCT_SERIES_CATEGORY " & _
    '            "where category_id =" & AppConstants.CAT_ID_SCOPE & "  " & _
    '              "and product_series_id = " & dr("product_series_id").ToString() & ") " & _
    '              "order by sort_id"
    '        End If
    '        DbHelperSQL.Query(ds, "3", strSQL)
    '    Next

    '    'Protocol analyzers
    '    strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME,'ProtocolSeries.aspx' as target, " & _
    '    "'" & standardid & "' as standardid, b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid " & _
    '    " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " & _
    '    " where(a.category_id = " & AppConstants.CAT_ID_PROTOCOL & ") " & _
    '    "AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " )  " & _
    '    "and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY  " & _
    '    "where(category_id = " & AppConstants.CAT_ID_SERIAL_DATA & ") " & _
    '    "and PRODUCT_SERIES_ID=" & standardid & ") " & _
    '    "order by b.sortid"
    '    DbHelperSQL.Query(ds, "3", strSQL)

    '    'Product Groups - series of groups of products assigned to serieal data standard
    '    strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME, " & _
    '            "'ProductSeries.aspx' as target,'" & standardid & "' as standardid, b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid " & _
    '            " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " & _
    '            " where(a.category_id NOT in ( 1,19,21,20) )" & _
    '            " AND PRODUCT_ID NOT IN (" + GetDisabledProductSQL(localeid) + " )  " & _
    '            " and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY  " & _
    '            " where(category_id = " & AppConstants.CAT_ID_SERIAL_DATA & ") " & _
    '            " and PRODUCT_SERIES_ID=" & standardid & ") " & _
    '            " order by b.sortid"
    '    DbHelperSQL.Query(ds, "3", strSQL)

    '    pNum = ds.Tables("3").Rows.Count
    '    products = ""
    '    productLine = ""

    '    If pNum > 0 Then
    '        For i = 0 To pNum - 1
    '            Dim modelID As String = ""
    '            If Not ds.Tables("3").Rows(i)("partnumber") Is Nothing Then
    '                modelID = ds.Tables("3").Rows(i)("partnumber").ToString()
    '            End If
    '            Dim modelSeries As String = ""
    '            If Not ds.Tables("3").Rows(i)("PRODUCT_SERIES_ID") Is Nothing Then
    '                modelSeries = ds.Tables("3").Rows(i)("PRODUCT_SERIES_ID").ToString()
    '            End If
    '            If Not ds.Tables("3").Rows(i)("product_id") Is Nothing Then
    '                productID = ds.Tables("3").Rows(i)("product_id").ToString()
    '            End If
    '            If modelSeries = mseries Then
    '                products = products + "<li  class=""current"">"
    '                products = products + "<a href='" & ds.Tables("3").Rows(i)("target").ToString() & "?modelid=" + productID + _
    '                "&mseries=" & modelSeries & "&standardid=" & standardid & menuURL + "'> " + modelID + "</a></li>"

    '                Dim categoryid As String = ds.Tables("3").Rows(i)("CATEGORY_ID").ToString()
    '                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_ACCESSORIES Then
    '                    GetProductsByScopeSeries()
    '                Else
    '                    GetProducts()
    '                End If
    '            Else
    '                products = products + "<li>"
    '                products = products + "<a href='" & ds.Tables("3").Rows(i)("target").ToString() & "?modelid=" + productID + _
    '                "&mseries=" & modelSeries & "&standardid=" & standardid & menuURL + "'> " + modelID + "</a></li>"
    '            End If

    '        Next
    '        tabsToShow.Append("<li><a href=""#product_line""><img src='")
    '        tabsToShow.Append(rootDir)
    '        tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
    '    End If
    '    lbProduct1.Text = products
    '    lbQuickSpecs.Text = productLine

    'End Sub

    'Protected Sub GetProducts()
    '    Dim pNum2 As Integer
    '    Dim j As Integer

    '    strSQL = "SELECT DISTINCT a.*" + _
    '                     " FROM PRODUCT a INNER JOIN PRODUCT_SERIES_CATEGORY c ON a.PRODUCTID = c.PRODUCT_ID " + _
    '                     " INNER JOIN  PRODUCT_SERIES b ON b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID " + _
    '                     " WHERE c.PRODUCT_SERIES_ID = " + mseries + _
    '                     " AND a.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) order by a.sort_id"
    '    ds2 = DbHelperSQL.Query(strSQL)
    '    pNum2 = ds2.Tables(0).Rows.Count
    '    If pNum2 > 0 Then
    '        For j = 0 To pNum2 - 1
    '            Dim modelID2 As String = Me.convertToString(ds2.Tables(0).Rows(j)("PARTNUMBER"))
    '            Dim intro As String = Me.convertToString(ds2.Tables(0).Rows(j)("INTRODUCTION"))
    '            productLine = productLine + "<div class='quickBox'>"
    '            If intro Is Nothing Or intro = "" Then
    '                productLine = productLine + "<strong>" + modelID2 + "</strong>" + _
    '                "&nbsp;&nbsp;" + Functions.GetProdDescriptionOnProdID(ds2.Tables(0).Rows(j)("PRODUCTID").ToString).ToString
    '            Else
    '                productLine = productLine + "<a href='/SerialData/ProductOverview.aspx?modelid=" + Me.convertToString(ds2.Tables(0).Rows(j)("PRODUCTID")) + _
    '                "&mseries=" & mseries & "&standardid=" & standardid & menuURL + "'><strong>" + modelID2 + "</strong></a> " + _
    '                "&nbsp;&nbsp;" + Functions.GetProdDescriptionOnProdID(ds2.Tables(0).Rows(j)("PRODUCTID").ToString).ToString
    '            End If
    '            productLine = productLine + "</div>"
    '        Next
    '    End If
    'End Sub

    'Protected Sub GetProductsByScopeSeries()
    '    Dim pNum As Integer = 0
    '    Dim j As Integer = 0
    '    Dim oNum As Integer = 0
    '    Dim k As Integer = 0

    '    strSQL = "SELECT DISTINCT f.PRODUCT_SERIES_ID, f.NAME, f.SORTID " + _
    '            " FROM   PRODUCT_SERIES AS f INNER JOIN    PRODUCT_SERIES_CATEGORY AS e ON f.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " + _
    '            " PRODUCT AS d ON e.PRODUCT_ID = d.PRODUCTID INNER JOIN   CONFIG AS c ON c.PRODUCTID = d.PRODUCTID INNER JOIN " + _
    '            " PRODUCT AS b ON b.PRODUCTID = c.OPTIONID INNER JOIN     PRODUCT_SERIES_CATEGORY AS a ON b.PRODUCTID = a.PRODUCT_ID " + _
    '            " WHERE b.disabled='n' and c.std ='n' " + _
    '            " and a.PRODUCT_SERIES_ID =" + standardid + " and d.disabled='n' " + _
    '            " and e.category_id =" + AppConstants.CAT_ID_SCOPE + " ORDER by f.SORTID"
    '    'Response.Write(strSQL)
    '    dsSeries = DbHelperSQL.Query(strSQL)
    '    pNum = dsSeries.Tables(0).Rows.Count()
    '    If pNum > 0 Then
    '        productLine += "<div class='accordion'>"
    '        For j = 0 To pNum - 1
    '            Dim dr As DataRow = dsSeries.Tables(0).Rows(j)
    '            productLine += "<div class='list'>"
    '            productLine += dr("NAME")
    '            productLine += "</div>"
    '            productLine += "<div class='faq'>"
    '            strSQL = "SELECT DISTINCT b.PARTNUMBER, b.PRODUCTDESCRIPTION, b.INTRODUCTION, b.PRODUCTID, b.SORT_ID " + _
    '            " FROM   PRODUCT_SERIES AS f INNER JOIN  PRODUCT_SERIES_CATEGORY AS e ON f.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " + _
    '            " PRODUCT AS d ON e.PRODUCT_ID = d.PRODUCTID INNER JOIN   CONFIG AS c ON c.PRODUCTID = d.PRODUCTID INNER JOIN " + _
    '            " PRODUCT AS b ON b.PRODUCTID = c.OPTIONID INNER JOIN     PRODUCT_SERIES_CATEGORY AS a ON b.PRODUCTID = a.PRODUCT_ID " + _
    '            " WHERE b.disabled='n' and c.std ='n' " + _
    '            " and a.PRODUCT_SERIES_ID =" + standardid + " and d.disabled='n' " + _
    '            " and e.category_id =" + AppConstants.CAT_ID_SCOPE + " and e.PRODUCT_SERIES_ID=" + dr("PRODUCT_SERIES_ID").ToString + " ORDER by b.SORT_ID"

    '            dsProducts = DbHelperSQL.Query(strSQL)
    '            oNum = dsProducts.Tables(0).Rows.Count()
    '            If oNum > 0 Then
    '                'Response.Write(oNum)
    '                productLine += "<table width=450>"
    '                For k = 0 To oNum - 1
    '                    Dim drr As DataRow = dsProducts.Tables(0).Rows(k)
    '                    productLine += "<td valign='top' class='cell'>"
    '                    Dim intro As String = Me.convertToString(dsProducts.Tables(0).Rows(k)("INTRODUCTION"))
    '                    If intro Is Nothing Or intro = "" Then
    '                        productLine = productLine + "<strong>" + Me.convertToString(dsProducts.Tables(0).Rows(k)("PARTNUMBER")) + "</strong>" + _
    '                        "&nbsp;&nbsp;" + Functions.GetProdDescriptionOnProdID(dsProducts.Tables(0).Rows(k)("PRODUCTID").ToString).ToString
    '                    Else
    '                        productLine = productLine + "<a href='/SerialData/ProductOverview.aspx?modelid=" + Me.convertToString(dsProducts.Tables(0).Rows(k)("PRODUCTID")) + _
    '                        "&mseries=" & mseries & "&standardid=" & standardid & menuURL + "'><strong>" + Me.convertToString(dsProducts.Tables(0).Rows(k)("PARTNUMBER")) + "</strong></a> " + _
    '                        "&nbsp;&nbsp;" + Functions.GetProdDescriptionOnProdID(dsProducts.Tables(0).Rows(k)("PRODUCTID").ToString).ToString
    '                    End If
    '                    productLine += "</td>"
    '                    productLine += "</tr>"
    '                Next
    '                productLine += "</table>"
    '            End If
    '            productLine += "</div>"
    '        Next
    '        productLine += "</div>"

    '    End If
    'End Sub
    Private Sub getRecommendedConfigurations()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.RECOMMENDED_CONFIGURATIONS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblRecConfig.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#rec_config'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_rec_config_off.gif' id='rec_config' border='0'></a></li>")
        End If
    End Sub


    Protected Sub GetSeries()
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Dim strResults As String = ""
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")

            ' Me.Panel1.Visible = True
            ' Me.lblGroupName.Text = Functions.getTitleForGroup(groupid)
            'strSQL = " SELECT DISTINCT b.PRODUCT_SERIES_ID, b.NAME " + _
            '        " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON " + _
            '        " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID  INNER JOIN PRODUCT c " + _
            '        " ON a.PRODUCT_ID = c.PRODUCTID  WHERE  c.GROUP_ID = " + groupid + _
            '        " AND c.DISABLED = 'n' AND a.CATEGORY_ID=" + categoryid + " ORDER BY b.NAME"
            strSQL = " SELECT DISTINCT b.PRODUCT_SERIES_ID, b.NAME " +
                    " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON " +
                    " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID  INNER JOIN PRODUCT c " +
                    " ON a.PRODUCT_ID = c.PRODUCTID INNER JOIN PRODUCT_GROUP_XREF d ON c.PRODUCTID = d.PRODUCTFKID  WHERE  d.PRODUCTGROUPFKID = @PRODUCTGROUPFKID AND c.DISABLED = 'n' AND a.CATEGORY_ID=@CATEGORYID ORDER BY b.NAME"
            ' Response.Write(strSQL)
            ' Response.End()
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
            sqlParameters.Add(New SqlParameter("@PRODUCTGROUPFKID", groupid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count() > 0 Then
                ' strSeries = "<table width=450>"

                For Each dr As DataRow In ds.Tables(0).Rows
                    strResults = strResults + "<div class='quickBox'>"
                    strResults = strResults + " <a href='/Options/ProductSeries.aspx?mseries=" + dr("PRODUCT_SERIES_ID").ToString() + "&groupid=" + groupid + "'>" + dr("NAME").ToString() + "</a> "
                    strResults = strResults + "</div >"
                Next
                '  strSeries += "</table>"
                lbQuickSpecs.Text = strResults
            End If
        End If
    End Sub
    Private Sub getCompatability()
        Dim strResults As String = ""
        strSQL = " SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME,PRODUCT_SERIES.PRODUCTIMAGE,PRODUCTIMAGEDESC, PRODUCT_SERIES.SORTID " +
                 " FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT " +
                 " ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN " +
                 " CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID " +
                 " INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 " +
                 " ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID " +
                 " ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID " +
                 " WHERE PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND  PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1  AND PRODUCT_1.DISABLED = 'n' " +
                 " AND PRODUCT_1.DISABLED = 'n' ORDER BY PRODUCT_SERIES.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        If ds.Tables(0).Rows.Count > 0 Then
            Me.dl_series.DataSource = ds
            Me.dl_series.DataBind()
            'hasOverview = True
            tabsToShow.Append("<li><a href='#compatability'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/compatability_off.gif' id='compatability' border='0'></a></li>")
        End If
    End Sub
End Class