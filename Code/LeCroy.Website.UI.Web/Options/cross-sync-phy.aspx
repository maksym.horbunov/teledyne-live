﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="cross-sync-phy.aspx.vb" Inherits="LeCroy.Website.Options_CrossSync" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style reset v1">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
		<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

		

				<section class="section-banner-slide section-white small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);">
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1><em>Cross</em>Sync&trade; PHY</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-6 mb-32 mb-lg-0">
								<div class="page-content">
									<h2>Cross-layer analysis</h2>
									<div class="page-text">
										<p>Interoperability issues can lead to finger-pointing exercises that cost money and time-to-market. Teledyne LeCroy CrossSync PHY software and interposers seamlessly merge the functions of your Teledyne LeCroy protocol analyzer and oscilloscope – giving insight into link behavior that no other instrument can provide.
										</p>
									</div>
									<div class="page-button">
										<button class="btn btn-default-outline mr-3 mb-3 mb-lg-0"><a href="/doc/cross-sync-phy-datasheet">Datasheet</a></button>
										<button class="btn btn-default-outline mr-3 mb-3 mb-lg-0"><a href="#webinars">Webinars</a></button>
									</div>
									<div class="page-video" style="margin-top:24px;"><span class="wistia_embed wistia_async_hk8qu9gxqp popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:183px;position:relative;width:365px">&nbsp;</span></div>
								</div></div>
							<div class="col-md-6"><img src="https://assets.lcry.net/images/cross-sync-phy01.png" alt="img-description"></div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="section-heading">
							<h2>The CrossSync PHY concept</h2>
						</div>
						<div class="row mb-4">
							<div class="col-md-6">
								<div class="page-text">
									<p>CrossSync PHY enables waveforms from Teledyne LeCroy oscilloscopes to be viewed alongside protocol analyzer traces, with complete time-correlation of electrical and protocol information for easy and powerful validation and root-cause analysis. </p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="page-text">
									<p>A growing range of CrossSync PHY capable interposers eliminates the complexity of cross-probing the same interface for capture by both the protocol analyzer and oscilloscope.</p>
								</div>
							</div>
						</div>
						<div class="row mb-4">
							<div class="col-md-12 text-center">            <img src="https://assets.lcry.net/images/cross-sync-phy02.png" alt="img-description"></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">   
										<div class="card-title">                                      
											<h3>Validate and debug active link operation</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>CrossSync PHY capable interposers enable observation of both electrical and protocol behavior without disturbing the link</li>
												<li>Sideband signals, reference clock and power rails are all easily accessible to oscilloscope probes</li>
												<li>Optional high-bandwidth oscilloscope probing points for PCI Express data lanes</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Quickly resolve interoperability issues by capturing the entire protocol stack</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>Trigger protocol analyzer and oscilloscope captures on the same high-level event</li>
												<li>Easily measure timing relationships between protocol and electrical domains</li>
												<li>Faster root-cause analysis means fewer costly finger-pointing exercises</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content"> 
										<div class="card-title">                                      
											<h3>Analyze link training with integrated physical and protocol views</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>Observe electrical-level results of protocol-level commands</li>
												<li>Combined navigation means always knowing which protocol and electrical behaviors happen at the same time</li>
												<li>No single instrument can deliver this level of cross-layer insight into link training </li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray section-visibility">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>More visibility</h2>
									</div>
									<div class="page-text mb-4">
										<p>CrossSync PHY gives deeper insight and more visibility into the most complex behaviors in a PCI Express link. Link training, power-on operations, and power management transitions all involve interactions between protocol and electrical layers.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">      
										<div class="card-title">                                      
											<h3>Dynamic Link Behavior</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>Characterize entire boot sequence with visibility into sideband signals, reference clock, data lanes and power rails.</li>
												<li>Observe speed changes in both electrical and protocol domains.</li>
												<li>Trigger on problematic link training behaviors and analyze their consequences through the entire protocol stack.</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a data-fancybox="gallery29" href="https://assets.lcry.net/images/cross-sync-v3.png">
											<img src="https://assets.lcry.net/images/cross-sync-visibility-1.jpg" alt="img-description">
										</a></div>
									
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">     
										<div class="card-title">                                      
											<h3>Quickly resolve interoperability issues</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>Trigger protocol analyzer and oscilloscope captures on the same high-level event</li>
												<li>Easily measure timing relationships between protocol and electrical domains</li>
												<li>Faster root-cause analysis means fewer costly finger-pointing exercises</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a data-fancybox="gallery30" href="https://assets.lcry.net/images/cross-sync-v4.png">
											<img src="https://assets.lcry.net/images/cross-sync-visibility-2.jpg" alt="img-description">
										</a></div>

								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">   
										<div class="card-title">                                      
											<h3>Analyze link training</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots">
												<li>Observe electrical-level results of protocol-level commands</li>
												<li>Combined navigation means always knowing which protocol and electrical behaviors happen at the same time</li>
												<li>No single instrument can deliver this level of cross-layer insight into link training behavior</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a data-fancybox="gallery31" href="https://assets.lcry.net/images/cross-sync-v5.png">
											<img src="https://assets.lcry.net/images/cross-sync-visibility-3.jpg" alt="img-description">
										</a></div>

								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>CrossSync PHY cross-analysis software</h2>
									</div>
									<div class="page-text mb-4">
										<p>The CrossSync PHY software option for your Teledyne LeCroy oscilloscope enables precise, intuitive navigation between time-correlated protocol analyzer and oscillscope traces.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<ul class="collection number white mb-0">
									<li><i>1</i>CrossSync PHY navigation bar makes clear the timing relationship between linked protocol analyzer and oscilloscope captures.</li>
									<li><i>2</i>One or many oscilloscope acquisition segments are represented in the navigation bar.</li>
									<li><i>3</i>Protocol analyzer trace represented in the navigation bar.</li>
									<li><i>4</i>Common trigger point ensures precise timing alignment between protocol trace, oscilloscope acquisitions, and navigation bar view.</li>
									<li><i>5</i>Oscilloscope timebase and protocol analyzer acquisition window remain synchronized while navigating through the combined acquisition, for total confidence in timing behavior.</li>
									<li><i>6</i>Selecting a packet on the protocol trace enables zoom traces on the oscilloscope to the same time window, enabling sub-packet-level measurement precision</li>
								</ul>
							</div>
							<div class="col-md-6">
								<div class="card-line d-f-center justify-content">
									<a data-fancybox="gallery25" href="https://assets.lcry.net/images/cross-sync-phy04.png">
										<img src="https://assets.lcry.net/images/cross-sync-phy04.png" alt="img-description">
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>CrossSync PHY interposers</h2>
									</div>
									<div class="page-text mb-4">
										<p>
											 CrossSync PHY capability enhances Teledyne LeCroy’s industry-leading set of protocol analysis interposers by adding high-fidelity oscilloscope probing points with simple and convenient signal access.		</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<ul class="collection number white mb-0">
									<li><i>1</i>Transparent signal path between host and endpoint, with convenient connections to both protocol analyzer and oscilloscope.</li>
									<li><i>2</i>Simple, low-loading direct connection for Teledyne LeCroy DH series oscilloscope probes up to 30 GHz bandwidth – no soldering required.</li>
									<li><i>3</i>Easy access to all sideband signals.</li>
									<li><i>4</i>High-bandwidth voltage and current connections for monitoring dynamic power rail behavior.</li>
									<li><i>5</i>High-fidelity buffered reference clock connection for observing clock behavior during power-up and low-power states.</li>
								</ul>
							</div>
							<div class="col-md-6">
								<div class="card-line d-f-center justify-content"><a data-fancybox="gallery16" href="https://assets.lcry.net/images/cross-sync-phy05.png"><img src="https://assets.lcry.net/images/cross-sync-phy05.png" alt="img-description"></a></div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-visibility">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>Compatible Protocol Analysis products</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mb-32 mb-lg-0">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">        
										<div class="card-title">                                      
											<h3>Summit T54 Protocol Analyzer</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Protocol Analyzer for PCI Express 5.0 and CXL (Compute Express Link)</li>
												<li>
														 Supports x1, x2 and x4 Links at data rates of 2.5GT/s, 5GT/s, 8GT/s, 16GT/s and 32GT/s</li>
												<li>
														 Trace buffer up to 64GB</li>
												<li>
														 Can be cascaded to support x8 Link Widths with up to 128GB of trace buffer</li>
												<li>
														 Support for Storage Protocol Decodes for NVMe, NVMe-MI, SATA Express and SCSI Express (SOP-PQI) </li>
												<li>
														 Support for sideband signals SMBus, CLKREQ#, WAKE#, PERST#</li>
												<li>
														 Uses CATC Trace software including Spreadsheet view, LTSSM State View and BitTracer        </li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer"><img src="https://assets.lcry.net/images/cross-sync-p1.jpg" alt="img-description"></a></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">           
										<div class="card-title">                                      
											<h3>PCI Express 4.0 x4 M.2 M-Key Interposer</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li> Supports PCI Express M.2 M-Key connectors up to x4 Link Width</li>
												<li>
														 Supports Data Rates up to 16GT/s</li>
												<li>
														 CrossSync PHY capable allowing direct connection of High Speed and Sideband signals to Oscilloscope     </li>
												<li>
														 Power rail probe points allowing Voltage and Current measurements on Oscilloscope</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a href="http://cdn.teledynelecroy.com/files/pdf/gen4_m.2_pci_express_interposer_datasheet.pdf"><img src="https://assets.lcry.net/images/cross-sync-p2.png" alt="img-description"></a></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray section-visibility ">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>Compatible Oscilloscope products</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 mb-32">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">        
										<div class="card-title">                                      
											<h3>LabMaster 10 Zi-A</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li> Up to 65 GHz real-time bandwidth</li>
												<li>
														 Supports PCI Express measurements to PCIe 5.0 and beyond</li>
												<li>
														 Powerful server-class PC with 20 processor cores and up to 192 GB system RAM for fast processing of long waveforms</li>
											</ul>
										</div>
										
									</div>
									<div class="inner-visible text-center"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes"><img src="https://assets.lcry.net/images/cross-sync-p3.png" alt="img-description"></a></div>
									
								</div>
							</div>
							<div class="col-md-4 mb-32">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">           
										<div class="card-title">                                      
											<h3>WaveMaster 8 Zi-B</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Up to 33 GHz real-time bandwidth</li>
												<li>
														 Supports PCI Express measurements to PCIe 4.0</li>
												<li>
														 Flexible inputs support 1 Mohm passive and current probes with no adapters necessary</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes"><img src="https://assets.lcry.net/images/cross-sync-p4.png" alt="img-description"></a></div>
									
								</div>
							</div>
							<div class="col-md-4 mb-32">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">     
										<div class="card-title">                                      
											<h3>RP4030 Active Voltage Rail Probe</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Connect directly to CrossSync PHY interposers with no soldering required</li>
												<li>
														 Up to 30 GHz bandwidth, low noise, low loading</li>
												<li>
														 Other tips available: solder-in (standard and high-sensitivity), high-temperature solder-in, handheld browser, QuickLink adapter</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a href="/probes/active-voltage-rail-probe"><img src="https://assets.lcry.net/images/cross-sync-p5.png" alt="img-description"></a></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 mb-32 mb-lg-0">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">          
										<div class="card-title">                                      
											<h3>DH Series high-bandwidth differential probes</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Connects directly to CrossSync PHY interposers with no soldering required</li>
												<li>
														 4 GHz bandwidth with low noise and ±30V Offset Capability</li>
												<li>
														 50 kΩ DC Input Impedance for low loading</li>
											</ul>
										</div>
										
									</div>
									<div class="inner-visible text-center"><a href="/probes/dh-series-differential-probes"><img src="https://assets.lcry.net/images/cross-sync-p6.png" alt="img-description"></a></div>
									
								</div>
							</div>
							<div class="col-md-4 mb-32 mb-lg-0">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">           
										<div class="card-title">                                      
											<h3>QualiPHY PCIe test automation software</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Automates LabMaster and WaveMaster oscilloscopes and Anritsu MP1900A BERT to perform PCI Express compliance and characterization testing</li>
												<li>
														 Available for PCI Express 3.0, 4.0 and 5.0</li>
											</ul>
										</div>
									</div>
									<div class="inner-visible text-center"><a href="/pcie-electrical-test/"><img src="https://assets.lcry.net/images/cross-sync-p7.png" alt="img-description"></a></div>
									
								</div>
							</div>
							<div class="col-md-4">
								<div class="card-line no-style">
									<div class="inner-content h-auto mb-24">          
										<div class="card-title">                                      
											<h3>PCI Express decode</h3>
										</div>
										<div class="card-content">
											<ul class="list-dots mb-2">
												<li>Decode PCI Express signals up to data link layer directly in the oscilloscope user interface</li>
												<li>Covers all rates up to PCIe 5.032 GT/s</li>
												<li>Decode information is annotated on the voltage waveform       </li>
											</ul>
										</div>
										
									</div>
									<div class="inner-visible text-center"><a href="/pcie-electrical-test/"><img src="https://assets.lcry.net/images/cross-sync-p8.png" alt="img-description"></a></div>
									
								</div>
							</div>
						</div>
					</div>
				</section>
		<section class="section-event-solutions pt-40" id="webinars">
					<div class="container">
						<div class="section-heading text-center">
							<h2>On-Demand Webinars</h2>
						</div>
						<div class="section-content">
							<div class="block-view view-list">
								<div class="tab-holder pt-0" id="filters">

									<div class="tabs-content">

										<div class="row mb-24">
											<div class="mb-3 col-lg-12">
												<div class="slide-event">
													<div class="top-card">
														<h6 class="title-slide"> <a href="https://go.teledynelecroy.com/l/48392/2021-02-23/8512dg">Debug PCIe&reg; Faster with Cross-layer Analysis Tools</a></h6>

													</div>
													<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2021-02-23/8512dg">Watch Now </a></div>
												</div>
											</div>
											</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</main>
</asp:Content>
