﻿Public Class Options_CrossSync
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "CrossSync PHY - Teledyne LeCroy"
        Me.MetaDescription = "Analyze link training, power management transitions and more across the entire protocol stack."

        Master.MetaOgTitle = "CrossSync PHY - Teledyne LeCroy"
        Master.MetaOgImage = "https://assets.lcry.net/images/cross-sync-phy01.png"
        Master.MetaOgDescription = "Analyze link training, power management transitions and more across the entire protocol stack."
        Master.MetaOgUrl = "https://teledynelecroy.com/options/cross-sync-phy.aspx"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub

End Class