<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Options_AdditionalOptions" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
	    <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <asp:Literal ID="menulabel" runat="server" />
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <h1><asp:Label ID="lblCatName" runat="server" /><asp:Literal ID="litSubCategoryName" runat="server" /></h1>
        <p><asp:Label ID="lblCatDesc" runat="server" /></p>
    </div>
    <asp:Panel ID="Panel1" runat="server" Visible="false"> 
        <div class="tabs">
            <div class="bg">
                <ul class="tabNavigation"><asp:Label ID="lblTabsToShow" runat="server" /></ul>
                <div id="product_line">
                    <asp:Label ID="lb_productLine" runat="server" />
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue"><strong><asp:Label ID="lblGroupName" runat="server" /></strong></span></p>
                    <p><asp:Label ID="lblSeries" runat="server" /></p>
                    <div class="searchResults">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <tr><td class="cell"><strong><%#getTitle(Eval("PARTNUMBER").ToString, Eval("INTRODUCTION").ToString, Eval("PRODUCTID").ToString)%></strong> - <%#Eval("PRODUCTDESCRIPTION")%></td></tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Label ID="lblSeriesList" runat="server" />
                        </table><br />
                    </div>
                    <div style="margin-top: -15px;"><asp:Literal ID="content" runat="server" /></div>
                </div>
                <div id="overview"><asp:Label ID="lb_overview" runat="server" /></div>
                <div id="spec"><asp:Label ID="lblSpecs" runat="server" /></div>
            </div>
        </div>
    </asp:Panel>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <LeCroy:Resources ID="ucResources" runat="server" IsOptionsSubPage="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>