﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Options_AdditionalOptions
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, dss, dsSeries, dsProducts As DataSet
    Dim categoryid As String = ""
    Dim groupid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Dim overview As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' if URL contains query string, validate all query string variables
        '** categoryid
        If Request.QueryString("categoryid") Is Nothing Or Len(Request.QueryString("categoryid")) = 0 Then
            categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS
        Else
            If IsNumeric(Request.QueryString("categoryid")) Then
                categoryid = Request.QueryString("categoryid")
            Else
                categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS
            End If
        End If

        '** groupid
        If Len(Request.QueryString("groupid")) > 0 Then
            If IsNumeric(Request.QueryString("groupid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), ",") Then
                    groupid = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("groupid")), ",") + 1)
                Else
                    groupid = SQLStringWithOutSingleQuotes(Request.QueryString("groupid"))
                End If
            Else
                Response.Redirect("/default.aspx")
            End If
        End If
        If (categoryid = AppConstants.CAT_ID_PROBES) Then
            If Not (String.IsNullOrEmpty(groupid)) Then
                Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
                sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES.ToString()))
                sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTGROUPID", groupid.ToString()))
                Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT [PRODUCT_SERIES_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [CATEGORY_ID] = @CATEGORYID AND [PRODUCT_ID] IN (SELECT [PRODUCTFKID] FROM [PRODUCT_GROUP_XREF] WHERE [PRODUCTGROUPFKID] = @PRODUCTGROUPID)", sqlParameters.ToArray()).ToList()
                If (productSeriesCategories.Count = 1) Then
                    Response.Redirect(String.Format("~/probes/probeseries.aspx?mseries={0}", productSeriesCategories.FirstOrDefault().ProductSeriesId))
                End If
            End If
            Response.Redirect("~/probes/")
        End If
        Session("cataid") = categoryid

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
        If Not Page.IsPostBack Then
            Dim captionID As String = ""
            Dim menuID As String = ""
            ' if URL contains query string, validate all query string variables
            '** captionID
            If Request.QueryString("capid") Is Nothing Or Len(Request.QueryString("capid")) = 0 Then
                captionID = AppConstants.SCOPE_CAPTION_ID
            Else
                If IsNumeric(Request.QueryString("capid")) Then
                    captionID = Request.QueryString("capid")
                Else
                    captionID = AppConstants.SCOPE_CAPTION_ID
                End If
            End If

            '** menuID
            If Request.QueryString("mid") Is Nothing Or Len(Request.QueryString("mid")) = 0 Then
                If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_H_MENU
                ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                    menuID = AppConstants.OPTION_S_MENU
                ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    menuID = AppConstants.OPTION_A_MENU
                ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                    'menuID = AppConstants.OPTION_P_MENU
                    Response.Redirect("~/probes/")
                ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                    menuID = AppConstants.OPTION_M_MENU
                End If
            Else
                If IsNumeric(Request.QueryString("mid")) Then
                    menuID = Request.QueryString("mid")
                Else
                    If categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Then
                        menuID = AppConstants.OPTION_H_MENU
                    ElseIf categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                        menuID = AppConstants.OPTION_S_MENU
                    ElseIf categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                        menuID = AppConstants.OPTION_A_MENU
                    ElseIf categoryid = AppConstants.CAT_ID_PROBES Then
                        'menuID = AppConstants.OPTION_P_MENU
                        Response.Redirect("~/probes/")
                    ElseIf categoryid = AppConstants.CAT_ID_MIXEDSIGNAL Then
                        menuID = AppConstants.OPTION_M_MENU

                    End If
                End If
            End If

            Session("menuSelected") = captionID
            Dim tempVal As Int32 = 0
            If (Int32.TryParse(captionID, tempVal)) Then
                menuURL = "&capid=" + captionID
            Else
                menuURL = "&capid="
            End If
            menuURL += "&mid=" + menuID
            Session("SeriesID") = Nothing

            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

            ucResources.CategoryId = categoryid
            If Not (String.IsNullOrEmpty(groupid)) Then ucResources.ProductGroupId = groupid
            If Not categoryid Is Nothing And Len(categoryid) > 0 Then
                strSQL = "select CATEGORY_ID,CATEGORY_NAME, DESCRIPTION from CATEGORY where category_id=@CATEGORYID"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        'content
                        Me.lblCatName.Text = dr("CATEGORY_NAME").ToString()
                        Me.Master.PageContentHeader.BottomText = dr("CATEGORY_NAME").ToString()
                        If Not (String.IsNullOrEmpty(groupid)) Then
                            Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(groupid))
                            If Not (productGroup Is Nothing) Then
                                litSubCategoryName.Text = String.Format(":&nbsp;{0}", productGroup.GroupName)
                            End If
                        End If
                        If Not Len(Me.Title) > 0 Then
                            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + dr("CATEGORY_NAME").ToString()
                        End If
                        Me.lblCatDesc.Text = dr("DESCRIPTION").ToString()
                        If categoryid = 3 Then
                            strSQL = "select distinct a.group_id, a.group_name, a.sortid ,'1' as URLType" +
                            " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                            " where a.MENU_YN='y' and b.disabled='n' and a.category_id=@CATEGORYID order by a.sortid "
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
                        Else
                            If categoryid = 18 Then
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                " where a.MENU_YN='y' and b.disabled='n' " +
                                " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                " aa.product_id=bb.productid and " +
                                " aa.category_id = @CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 UNION SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID AS group_id, " +
                                " PRODUCT_SERIES.NAME AS group_name, PRODUCT_SERIES.SORTID ,'2' as URLType  " +
                                " FROM PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY ON " +
                                " PRODUCT.PRODUCTID = PRODUCT_SERIES_CATEGORY.PRODUCT_ID INNER JOIN " +
                                " PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID " +
                                " WHERE(PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 18) order by sortid "
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            Else
                                strSQL = "select distinct a.group_id, a.group_name, a.sortid  ,'1' as URLType" +
                                        " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
                                        " INNER JOIN config c ON c.optionid = b.PRODUCTID " +
                                        " where a.MENU_YN='y' and b.disabled='n' " +
                                        " and c.std in ('y','n')  and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa, product bb where " +
                                        " aa.product_id=bb.productid and " +
                                        " aa.category_id = @CATEGORYID and bb.disabled='n' )   and a.category_id=@CATEGORYID2 order by sortid "
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                                sqlParameters.Add(New SqlParameter("@CATEGORYID2", categoryid))
                            End If

                        End If
                        'Response.Write(strSQL)
                        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        If dss.Tables(0).Rows.Count > 0 Then
                            Me.menulabel.Text += "<ul>"
                            For Each drr As DataRow In dss.Tables(0).Rows
                                If groupid.ToString = drr("group_id").ToString Then
                                    If drr("URLType").ToString = "1" Then
                                        Me.menulabel.Text += "<li class='current'><a href='/options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + menuURL + "'>" + drr("group_name").ToString() + "</a></li>"
                                    Else
                                        Me.menulabel.Text += "<li class='current'><a href='/oscilloscope/oscilloscopeseries.aspx?mseries=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                    End If
                                Else
                                    If drr("URLType").ToString = "1" Then
                                        Me.menulabel.Text += "<li><a href='/options/default.aspx?categoryid=" + dr("CATEGORY_ID").ToString() + "&groupid=" + drr("group_id").ToString() + menuURL + "'>" + drr("group_name").ToString() + "</a></li>"
                                    Else
                                        Me.menulabel.Text += "<li><a href='/oscilloscope/oscilloscopeseries.aspx?mseries=" + drr("group_id").ToString() + "'>" + drr("group_name").ToString() + "</a></li>"
                                    End If
                                End If
                            Next
                            Me.menulabel.Text += "</ul>"
                        Else
                            Response.Redirect("/default.aspx")
                        End If
                    Next
                End If

                'If Len(Functions.CategoryHasManuals(categoryid, rootDir).ToString) > 0 Then
                '    lblManuals.Text = Functions.CategoryHasManuals(categoryid, rootDir).ToString
                'End If

                'If Len(Functions.CategoryHasAppNotes(categoryid, rootDir).ToString) > 0 Then
                '    lblAppNotes.Text = Functions.CategoryHasAppNotes(categoryid, rootDir).ToString
                'End If
                ucResources.ShowRequestQuote = True
                'lblRequestQuote.Text = "<li class='link'><a href='" & rootDir & "/shopper/requestquote/configure_small_step1.aspx?catid=" & categoryid.ToString & "'>Request Quote</a></li>"

            End If
            If Not groupid Is Nothing And Len(groupid) > 0 Then
                If Not Functions.CheckGroupExists(categoryid, groupid) Then
                    Response.Redirect("default.aspx")
                End If
                If categoryid = AppConstants.CAT_ID_SOFTWARE_OPTIONS Then
                    'GetProductsByScopeSeries()
                    GetSeries()
                    ucResources.ShowRequestQuote = True
                    'lblRequestQuote.Text = "<li class='link'><a href='" & rootDir & "/shopper/requestquote/select_scope.aspx?seriesid=" & groupid.ToString & "'>Request Quote</a></li>"
                ElseIf categoryid = AppConstants.CAT_ID_HARDWARE_OPTIONS Or categoryid = AppConstants.CAT_ID_ACCESSORIES Then
                    'GetProductsByScopeSeries()
                    getProductLineByScopeSeries()
                    ucResources.ShowRequestQuote = True
                    'lblRequestQuote.Text = "<li class='link'><a href='" & rootDir & "/shopper/requestquote/configure_small_step2.aspx?seriesid=" & groupid.ToString & "'>Request Quote</a></li>"
                Else
                    If groupid = 147 Then
                        ' GetProductsByScopeSeries()
                        getProductLineByScopeSeries()
                    Else
                        GetProducts()
                    End If
                    ucResources.ShowRequestQuote = True
                    'lblRequestQuote.Text = "<li class='link'><a href='" & rootDir & "/shopper/requestquote/configure_small_step2.aspx?seriesid=" & groupid.ToString & "'>Request Quote</a></li>"
                End If
                'Product Resources
                'If Len(Functions.GroupHasDatasheet(groupid, rootDir).ToString) > 0 Then
                '    lblDataSheet.Text = Functions.GroupHasDatasheet(groupid, rootDir).ToString
                'End If

                'If Len(Functions.GroupHasSpecs(groupid, rootDir).ToString) > 0 Then
                '    lblSpecifications.Text = Functions.GroupHasSpecs(groupid, rootDir).ToString
                'End If



                'Buy Now 
                If Not Session("CountryCode") Is Nothing Then
                    If Len(Session("CountryCode").ToString) > 0 Then
                        If Session("CountryCode").ToString = "208" Then
                            If Functions.GroupHasEStoreLink(groupid, Session("contactid")).ToString.Length > 0 Then
                                ucResources.ShowBuy = True
                                'lblEStore.Text = Functions.GroupHasEStoreLink(groupid, Session("contactid")).ToString
                            End If
                        End If
                    End If
                End If
                'Additional Resources
                If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_GROUP.ToString, groupid.ToString).ToString) > 0 Then
                    lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_GROUP.ToString, groupid.ToString)
                End If

                strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW where ID = @GROUPID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
                sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.GROUP_OVERVIEW_TYPE))
                overview = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If Not overview = "" Then
                    Me.lb_overview.Text = overview
                    tabsToShow.Append("<li><a href='#overview'><img src='")
                    tabsToShow.Append(rootDir)
                    tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
                    lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
                End If
                lblTabsToShow.Text = tabsToShow.ToString()
            Else
                'Buy Now 
                If Not Session("CountryCode") Is Nothing Then
                    If Len(Session("CountryCode").ToString) > 0 Then
                        If Session("CountryCode").ToString = "208" Then
                            If Functions.CategoryHasEStoreLink(categoryid, Session("contactid")).ToString.Length > 0 Then
                                ucResources.ShowBuy = True
                                'lblEStore.Text = Functions.CategoryHasEStoreLink(categoryid, Session("contactid")).ToString
                            End If
                        End If
                    End If
                End If
            End If
            'lblTabsToShow.Text = tabsToShow.ToString()


            ' lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"

            Me.lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            Me.menulabel.Visible = False

        End If
    End Sub
    Protected Function getTitle(ByVal partnum As String, ByVal introduction As String, ByVal pid As String) As String
        If introduction = "" Then
            Return "<strong>" + partnum + "</strong>"
        Else
            Return "<a href='/options/productdetails.aspx?modelid=" + pid + "&categoryid=" + categoryid + "&groupid=" + groupid + menuURL + "'><strong>" + partnum + "</strong></a>"
        End If
    End Function
    Protected Sub GetProducts()
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")

            Me.Panel1.Visible = True
            Me.lblGroupName.Text = Functions.getTitleForGroup(groupid)
            strSQL = "SELECT distinct b.PRODUCTID, b.PARTNUMBER, B.PRODUCTDESCRIPTION, b.IMAGEFILENAME,b.INTRODUCTION, " +
            " a.URL, a.URL_SPEC, b.sort_id " +
            " from product_group a INNER JOIN product b ON a.group_id = b.group_id  " +
            " WHERE b.disabled='n' and a.group_id = @GROUPID order by b.sort_id "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
            Me.Repeater1.DataSource = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Me.Repeater1.DataBind()
            'ds = DbHelperSQL.Query(strSQL)
            'dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL"))
            'specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
            'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
            '    lblDataSheet.Text = "<li class='pdf'><a href='" & rootDir & dataSheetURL & "'>Datasheet</a></li>"

            'End If
            'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
            '    lblSpecifications.Text = "<li class='pdf'><a href='" & rootDir & specificationURL & "'>Specifications</a></li>"
            'End If

        End If
    End Sub

    Protected Sub GetProductsByScopeSeries()
        Dim pNum As Integer = 0
        Dim j As Integer = 0
        Dim oNum As Integer = 0
        Dim k As Integer = 0
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")

            Me.Panel1.Visible = True
            Me.lblGroupName.Text = Functions.getTitleForGroup(groupid)

            strSQL = "SELECT DISTINCT e.PRODUCT_SERIES_ID,e.NAME, e.SORTID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                    " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                    " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                    " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                    " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                    " WHERE b.disabled='n' " +
                    " and c.std = 'n' " +
                    " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                    " ON aa.product_id=bb.productid WHERE aa.category_id = @CATEGORYID and bb.disabled='n' ) AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by e.SORTID"
            'Response.Write(strSQL + "<br>")
            'Response.End()
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            dsSeries = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            pNum = dsSeries.Tables(0).Rows.Count()
            If pNum > 0 Then
                ' Me.content.Text += "<div class='accordion'>"
                For j = 0 To pNum - 1
                    Dim dr As DataRow = dsSeries.Tables(0).Rows(j)
                    'Me.content.Text += "<h3>"

                    'Me.content.Text += dr("NAME")
                    'Me.content.Text += "</h3>"

                    ' Me.content.Text += "<div class='faq'>"
                    strSQL = "SELECT DISTINCT b.PARTNUMBER, b.PRODUCTDESCRIPTION, b.INTRODUCTION, b.PRODUCTID, b.SORT_ID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                            " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                            " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                            " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                            " WHERE b.disabled='n' " +
                            " and c.std ='n' " +
                            " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                            " ON aa.product_id=bb.productid WHERE " +
                            " aa.category_id = @CATEGORYID and bb.disabled='n') AND e.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by b.SORT_ID"
                    'Response.Write(strSQL + "<br>")
                    ' Response.End()
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("PRODUCT_SERIES_ID").ToString()))
                    dsProducts = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    oNum = dsProducts.Tables(0).Rows.Count()
                    If oNum > 0 Then
                        Me.content.Text += "<table width=450>"
                        Me.content.Text += "<tr bgcolor='#ebebeb'>"
                        Me.content.Text += "<td valign='top' class='cell'  >"
                        Me.content.Text += "<strong>" + dr("NAME") + "</strong>&nbsp;<a href='" + "/oscilloscope/oscilloscopeseries.aspx?mseries=" + dr("PRODUCT_SERIES_ID").ToString + "' target='window'><img src='" + rootDir + "/images/configure/ico_popup.jpg' title='Learn more' border='0'></a>"
                        Me.content.Text += "</td>"
                        Me.content.Text += "</tr>"
                        For k = 0 To oNum - 1
                            Dim drr As DataRow = dsProducts.Tables(0).Rows(k)
                            Me.content.Text += "<tr>"
                            Me.content.Text += "<td valign='top' class='cell'>"

                            Me.content.Text += "<a href='" + "/options/productdetails.aspx?modelid=" + drr("PRODUCTID").ToString + _
                                       "&categoryid=" + categoryid.ToString + "&groupid=" + groupid.ToString + " '>" + drr("PARTNUMBER").ToString() + "</a>" + _
                                       "- " + Functions.GetProdDescriptionOnProdID(drr("PRODUCTID").ToString).ToString


                            Me.content.Text += "</td>"
                            Me.content.Text += "</tr>"
                        Next
                        Me.content.Text += "</table>"
                    End If
                    ' Me.content.Text += "</div>"
                Next
                'Me.content.Text += "</div>"
            End If

        End If
    End Sub

    Protected Sub GetSeries()
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Dim strSeries As String = ""
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")

            Me.Panel1.Visible = True
            Me.lblGroupName.Text = Functions.getTitleForGroup(groupid)
            'strSQL = " SELECT DISTINCT b.PRODUCT_SERIES_ID, b.NAME " + _
            '        " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON " + _
            '        " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID  INNER JOIN PRODUCT c " + _
            '        " ON a.PRODUCT_ID = c.PRODUCTID  WHERE  c.GROUP_ID = " + groupid + _
            '        " AND c.DISABLED = 'n' AND a.CATEGORY_ID=" + categoryid + " ORDER BY b.NAME"
            strSQL = " SELECT DISTINCT b.PRODUCT_SERIES_ID, b.NAME " +
                    " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON " +
                    " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID  INNER JOIN PRODUCT c " +
                    " ON a.PRODUCT_ID = c.PRODUCTID INNER JOIN PRODUCT_GROUP_XREF d ON c.PRODUCTID = d.PRODUCTFKID  WHERE  d.PRODUCTGROUPFKID = @PRODUCTGROUPFKID AND c.DISABLED = 'n' AND a.CATEGORY_ID=@CATEGORYID ORDER BY b.NAME"
            'Response.Write(strSQL)
            'Response.End()
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTGROUPFKID", groupid))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count() > 0 Then
                ' strSeries = "<table width=450>"

                For Each dr As DataRow In ds.Tables(0).Rows
                    strSeries += "<td valign='top' class='cell'>"
                    strSeries += "<a href='/options/productseries.aspx?mseries=" + dr("PRODUCT_SERIES_ID").ToString + "&groupid=" + groupid.ToString + "'>"
                    strSeries += dr("NAME").ToString
                    strSeries += "</a>"
                    strSeries += "</td>"
                    strSeries += "</tr>"
                Next
                '  strSeries += "</table>"
                lblSeriesList.Text = strSeries
            End If
        End If
    End Sub

    Private Sub getProductLineByScopeSeries()
        Dim pNum As Integer = 0
        Dim j As Integer = 0
        Dim oNum As Integer = 0
        Dim k As Integer = 0
        Dim strProductLineBySeries As String = ""
        If Not groupid Is Nothing And Len(groupid) > 0 Then
            Session("SeriesID") = groupid
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForCat(categoryid) + " - " + getTitleForGroup(groupid)
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
            strProductLineBySeries = "<p><span class='blue'>&nbsp;&nbsp;&nbsp;<strong> " + _
                        Functions.getTitleForGroup(groupid) + _
                        "</strong></span></p><br /> "
            strSQL = "SELECT DISTINCT e.PRODUCT_SERIES_ID,e.NAME, e.SORTID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                    " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                    " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                    " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                    " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                    " WHERE b.disabled='n' " +
                    " and c.std = 'n' " +
                    " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                    " ON aa.product_id=bb.productid WHERE " +
                    " aa.category_id = @CATEGORYID and bb.disabled='n' ) AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by e.SORTID"
            'Response.Write(strSQL)
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            dsSeries = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            pNum = dsSeries.Tables(0).Rows.Count()
            If pNum > 0 Then
                strProductLineBySeries += "<div class='accordion'>"
                For j = 0 To pNum - 1
                    Dim dr As DataRow = dsSeries.Tables(0).Rows(j)
                    strProductLineBySeries += "<div class='list'>"

                    strProductLineBySeries += dr("NAME")
                    strProductLineBySeries += "</div>"

                    strProductLineBySeries += "<div class='faq'>"
                    strSQL = "SELECT DISTINCT b.PARTNUMBER, b.PRODUCTDESCRIPTION, b.INTRODUCTION, b.PRODUCTID, b.SORT_ID FROM  PRODUCT_SERIES_CATEGORY AS d INNER JOIN" +
                            " PRODUCT_SERIES AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID INNER JOIN " +
                            " PRODUCT_GROUP AS a INNER JOIN PRODUCT AS b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " CONFIG AS c ON c.OPTIONID = b.PRODUCTID INNER JOIN " +
                            " PRODUCT AS f ON c.PRODUCTID = f.PRODUCTID ON d.PRODUCT_ID = f.PRODUCTID " +
                            " WHERE b.disabled='n' " +
                            " and c.std ='n' " +
                            " and a.group_id = @GROUPID and c.productid in (select distinct productid from PRODUCT_SERIES_CATEGORY aa INNER JOIN product bb " +
                            " ON aa.product_id=bb.productid WHERE " +
                            " aa.category_id = @CATEGORYID and bb.disabled='n') AND e.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND (d.CATEGORY_ID = 1) AND (f.DISABLED = 'n') ORDER by b.SORT_ID"
                    ' Response.Write(strSQL + "<br><br>")
                    'Response.End()
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@GROUPID", groupid))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("PRODUCT_SERIES_ID").ToString()))
                    dsProducts = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    oNum = dsProducts.Tables(0).Rows.Count()
                    If oNum > 0 Then
                        strProductLineBySeries += "<table width=450>"
                        For k = 0 To oNum - 1
                            Dim drr As DataRow = dsProducts.Tables(0).Rows(k)
                            strProductLineBySeries += "<td valign='top' class='cell'>"
                            strProductLineBySeries += getTitle(drr("PARTNUMBER").ToString, drr("INTRODUCTION").ToString, drr("PRODUCTID").ToString) + " - " + Functions.GetProdDescriptionOnProdID(drr("PRODUCTID").ToString).ToString
                            strProductLineBySeries += "</td>"
                            strProductLineBySeries += "</tr>"
                        Next
                        strProductLineBySeries += "</table>"
                    End If
                    strProductLineBySeries += "</div>"
                Next
                strProductLineBySeries += "</div>"
                Me.Panel1.Visible = True
                lb_productLine.Text = strProductLineBySeries
            End If

        End If
    End Sub
End Class