﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Oscilloscope_Configure_configure_step2" Codebehind="configure_step2.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="contentLeft">
                        <h2><asp:Literal ID="litOne" runat="server" /></h2>
                        <div class="stepNav">
                            <div class="leftSelection">
                                <asp:Button ID="btnQuickQuote1" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'btnQuickQuote1', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Quick Quote" ValidationGroup="next" />
                                &nbsp;<asp:Button ID="btnConfigure" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'btnConfigure', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Configure System" ValidationGroup="next" />
                            </div>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="modellist" ErrorMessage="Please select a product first!" ValidationGroup="next" />
                        <asp:RadioButtonList ID="modellist" runat="server" AutoPostBack="True" />
                        <br />
                        <div class="stepNav">
                            <div class="leftSelection">
                                <asp:Button ID="btnQuickQuote2" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'btnQuickQuote2', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Quick Quote" ValidationGroup="next" />
                            </div>
                        </div>
                        <div class="stepNav">
                            <div class="leftSelection">
                                <asp:Button ID="priviousBtn" runat="server" Text="<< Previous Step" />
                            </div>
                            <div class="rightSelection">
                                <asp:Button ID="nextBtn" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'nextBtn', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Next Step >>" ValidationGroup="next" />
                            </div>
                        </div>
                    </td>
                    <td valign="top" class="contentRight">
                        <h2><asp:Literal ID="litTwo" runat="server" /></h2><br /><br /><img src="<%=img %>" border="0"><br />
                        <p>
                            <asp:Literal ID="litThree" runat="server" /><br />
                            <strong><%=ser%></strong><br /><br />
                            <asp:Panel ID="p_rightmenu" runat="server" />
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>