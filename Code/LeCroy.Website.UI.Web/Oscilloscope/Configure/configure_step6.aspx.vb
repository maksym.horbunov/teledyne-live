﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities

Partial Class Oscilloscope_Configure_configure_step6
    Inherits QuoteBasePage
    Public img As String = ""
    Public ser As String = ""
    Dim customerID As String = "0"
    Dim strProfile As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Len(Session("seriesid")) = 0 Then
            Response.Redirect("default.aspx")
        End If

        Initial()
        If Not Session("ContactID") Is Nothing Then
            If Len(Session("ContactID")) > 0 Then
                customerID = Session("ContactID").ToString()
            End If
        End If
        'Translations
        litOne.Text = Functions.LoadI18N("SLBCA0811", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0806", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0805", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        priviousBtn.Text = LoadI18N("SLBCA0803", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        nextBtn.Text = LoadI18N("SLBCA0804", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        'Dim area As String = ""
        'area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", localeid) + "</a></div>"
        'area += "<div class='greyEndGrey'></div>"
        'area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", localeid) + "</a></div>"
        'area += "<div class='greyEndGrey'></div>"
        'If Session("hasHardware") = True Then
        '    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", localeid) + "</a></div>"
        '    area += "<div class='greyEndGrey'></div>"
        'End If
        'If Session("hasSoftware") = True Then
        '    area += "<div class='grey'><a href='configure_step4.aspx'>" + LoadI18N("SLBCA0758", localeid) + "</a></div><div class='greyEndGrey'></div>"
        'End If
        'If Session("hasProbes") = True Then
        '    area += "<div class='grey'><a href='configure_step5.aspx'>" + LoadI18N("SLBCA0759", localeid) + "</a></div><div class='greyEnd'></div>"
        'Else
        '    area += "<div class='greyEnd'></div>"
        'End If
        'area += "<div class='white'><a href='javascript:step6()'>" + LoadI18N("SLBCA0760", localeid) + "</a></div><div class='whiteEnd'></div>"

        'If Session("hasWarranty") = True Then
        '    area += "<div class='blue'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", localeid) + "</a></div><div class='blueEnd'></div>"
        'End If
        'area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", localeid) + "</a></div><div class='blueFinish'></div>"


        'Me.lbArea.Text = area
    End Sub

    Private Sub Initial()
        If Len(Session("productid" & Session("seriesid").ToString())) = 0 Then
            Response.Redirect("default.aspx")
        End If
        Dim sqlstr As String
        Dim lb As Label
        Dim rbl As RadioButtonList
        Dim cbl As CheckBoxList
        Dim ddl As DropDownList
        Dim strSQL As String = ""
        Dim tb As TextBox
        Dim lbx As ListBox
        Dim productid As String
        Dim li As ListItem
        Dim objList As ArrayList
        Dim ou As OsciUC
        Dim addnew As Boolean
        addnew = False
        Dim a As ArrayList
        a = New ArrayList()

        If Not Page.IsPostBack Then
            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
                Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                'ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                ' End If
            End If
        Else
            Session("localeid") = ddlLanguage.SelectedValue
        End If

        objList = Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString())

        productid = Session("productid" & Session("seriesid").ToString())
        ucquotes.selectedmodel = productid
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
        sqlParameters.Add(New SqlParameter("@GROUPID", AppConstants.GROUP_ID_WARRANTY))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_ACCESSORIES))

        sqlstr = ""
        sqlstr = String.Format("SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
             " FROM CONFIG a INNER JOIN  PRODUCT c ON  a.OPTIONID = c.PRODUCTID " +
             " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
             " WHERE a.PRODUCTID = @PRODUCTID AND c.PRODUCTID NOT IN ({0}) AND b.GROUP_ID <> @GROUPID and b.CATEGORY_ID=@CATEGORYID ORDER BY b.SORTID", disabledProductContent.Item1)
        Dim ds As DataSet= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count = 0 Then
            If Not Request.QueryString("way") Is Nothing Then
                If Request.QueryString("way") = "forward" Then
                    Response.Redirect("configure_step6.aspx?way=forward")
                Else
                    Response.Redirect("configure_step4.aspx?way=back")
                End If
            Else
                Response.Redirect("configure_step6.aspx?way=forward")
            End If
        End If
        For Each dr As DataRow In ds.Tables(0).Rows
            ou = New OsciUC()
            ou.Group_name = dr("GROUP_NAME").ToString()
            ou.CataID = "14"
            ou.Group_ID = dr("group_id").ToString()
            sqlParameters = New List(Of SqlParameter)
            disabledProductContent = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))

            sqlstr = String.Format("SELECT DISTINCT c.productid,d.productid as selproduct, d.qty, c.partnumber, c.productdescription,c.sort_id " +
                    " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID " +
                    " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
                    " LEFT OUTER JOIN (select productid, qty from shopper where sessionid=@SESSIONID and masterproductid=@MASTERPRODUCTID) d" +
                    " ON c.productid=d.productid WHERE a.PRODUCTID = @PRODUCTID AND b.group_id = @GROUPID AND c.PRODUCTID NOT IN ({0}) Order by c.sort_id", disabledProductContent.Item1)
            Dim dst As DataSet= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If dr("FORM_SELECT_TYPE").ToString() = "d" Then
                rbl = New RadioButtonList 'DropDownList
                lbx = New ListBox
                For Each drt As DataRow In dst.Tables(0).Rows
                    li = New ListItem(drt("partnumber").ToString() + getProductPriceNoHTMLIfApproved(Int32.Parse(drt("productid").ToString()), Int32.Parse(productid)) + Functions.GetProdDescriptionOnProdID(drt("productid").ToString).ToString, drt("productid").ToString())
                    lbx.Items.Add(li)
                    If drt("selproduct").ToString() <> "" Then
                        li.Selected = True
                    End If
                    rbl.Items.Add(li)
                Next
                ' li = New ListItem("-- Select Option --", "")
                'ddl.Items.Insert(0, li)
                ou.Obj = rbl
                ou.ControlType = "r"
            End If

            If dr("FORM_SELECT_TYPE").ToString() = "t" Then
                tb = New TextBox
                tb.ID = dst.Tables(0).Rows(0)("productid").ToString()
                ou.Obj = tb
                ou.ControlType = "t"
            End If

            If dr("FORM_SELECT_TYPE").ToString() = "c" Then
                cbl = New CheckBoxList
                For Each drt As DataRow In dst.Tables(0).Rows
                    li = New ListItem("<u><strong>" + drt("partnumber").ToString() + "</strong></u> " + getProductPriceIfApproved(Int32.Parse(drt("productid").ToString()), Int32.Parse(productid)) + Functions.GetProdDescriptionOnProdID(drt("productid").ToString).ToString, drt("productid").ToString())
                    cbl.Items.Add(li)
                    If drt("selproduct").ToString() <> "" Then
                        li.Selected = True
                    End If
                Next
                ou.Obj = cbl
                ou.ControlType = "c"
            End If

            If dr("FORM_SELECT_TYPE").ToString() = "s" Then
                lbx = New ListBox
                lbx.SelectionMode = ListSelectionMode.Multiple
                For Each drt As DataRow In dst.Tables(0).Rows
                    li = New ListItem(drt("partnumber").ToString() + getProductPriceNoHTMLIfApproved(Int32.Parse(drt("productid").ToString()), Int32.Parse(productid)) + Functions.GetProdDescriptionOnProdID(drt("productid").ToString).ToString, drt("productid").ToString())
                    lbx.Items.Add(li)
                    lbx.Items.Add(li)
                    If drt("selproduct").ToString() <> "" Then
                        li.Selected = True
                    End If
                Next
                ou.Obj = lbx
                ou.ControlType = "s"
            End If
            a.Add(ou)
        Next
        'End If

        For Each ot As OsciUC In a
            lb = New Label()
            lb.Text = "<strong>" + ot.Group_name + "</strong><br />"
            p_main.Controls.Add(lb)
            If ot.ControlType = "d" Then
                ddl = ot.Obj
                p_main.Controls.Add(CType(ddl, DropDownList))
            End If

            If ot.ControlType = "r" Then
                rbl = ot.Obj
                p_main.Controls.Add(CType(rbl, RadioButtonList))
            End If

            If ot.ControlType = "t" Then
                tb = ot.Obj
                p_main.Controls.Add(CType(tb, TextBox))
            End If

            If ot.ControlType = "c" Then
                cbl = ot.Obj
                p_main.Controls.Add(CType(cbl, CheckBoxList))
            End If

            If ot.ControlType = "s" Then
                lbx = ot.Obj
                p_main.Controls.Add(CType(lbx, ListBox))
            End If
            lb = New Label()
            lb.Text = "<br/>"
            p_main.Controls.Add(lb)
            If addnew Then
                objList.Add(ot)
            End If
        Next
        Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString()) = a

        RightInitial()
    End Sub

    Private Sub RightInitial()
        Dim lb As Literal
        Dim sqlstr As String
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If Not Session("seriesid") Is Nothing Then
            sqlstr = "select * from PRODUCT_SERIES where PRODUCT_SERIES_ID=@PRODUCTSERIESID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
            Dim ds As DataSet= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                img = rootDir + Me.convertToString(ds.Tables(0).Rows(0)("PRODUCTIMAGE"))
                ser = Me.convertToString(ds.Tables(0).Rows(0)("name"))
            End If
        Else
            Return
        End If

        If Not Session("productid" & Session("seriesid").ToString()) Is Nothing Then
            sqlstr = "select partnumber from PRODUCT where PRODUCTID=@PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", Session("productid" & Session("seriesid").ToString()).ToString()))
            Dim ds As DataSet= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                ser += "</strong><br/><br/>" + LoadI18N("SLBCA0807", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "<br/><strong>" + ds.Tables(0).Rows(0)("partnumber").ToString() + "</strong>"
            End If
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Configuration Step5 - " + ds.Tables(0).Rows(0)("partnumber").ToString()
        Else
            Return
        End If


        lb = New Literal()
        Dim gpid As String = ""
        sqlstr = "select a.group_id,b.group_name,c.partnumber,c.productid,c.productdescription from shopper a , PRODUCT_GROUP b, PRODUCT c " &
        "where a.sessionid=@SESSIONID and a.productid<> a.masterproductid and a.productid=c.productid and a.group_id=b.group_id and a.masterproductid=@MASTERPRODUCTID order by a.group_id"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", Session("productid" & Session("seriesid").ToString()).ToString()))
        Dim shopperProductInfos As List(Of ShopperProductInfo) = ShopperRepository.FetchShopperProductInfo(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray()).ToList()
        For Each spi In shopperProductInfos
            If (spi.GroupId.HasValue) Then
                If gpid <> spi.GroupId.Value.ToString() Then
                    lb.Text += "<br>"
                    lb.Text += spi.GroupName.ToString() & "<br>"
                    gpid = spi.GroupId.Value.ToString()
                End If
                lb.Text += "<u><strong>" + spi.PartNumber + "</strong></u>" + getProductPriceIfApproved(spi.ProductId, Int32.Parse(Session("productid" & Session("seriesid").ToString()).ToString())) + Functions.GetProdDescriptionOnProdID(spi.ProductId).ToString + "<br/>"
            End If
        Next
        p_rightmenu.Controls.Add(lb)
    End Sub

    Protected Sub priviousBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles priviousBtn.Click
        SaveCart()


        If Session("hasProbes") = True Then
            Response.Redirect("configure_step5.aspx")
        End If
        If Session("hasSoftware") = True Then
            Response.Redirect("configure_step4.aspx")
        End If
        If Session("hasHardware") = True Then
            Response.Redirect("configure_step3.aspx")
        Else
            Response.Redirect("configure_step2.aspx")
        End If
    End Sub

    Protected Sub SaveCart()
        Dim objList, a As ArrayList
        a = New ArrayList()
        objList = Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString())
        Functions.OsciSave(Me.Context, Session.SessionID, Session("seriesid").ToString(), customerID, Session("productid" & Session("seriesid").ToString()), objList, a)
    End Sub

    Protected Sub nextBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles nextBtn.Click
        SaveCart()
        If Session("hasWarranty") = True Then
            Response.Redirect("configure_step7.aspx")
        Else
            Response.Redirect("configure_step8.aspx")
        End If
    End Sub

    Protected Sub btn_Hidden_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Hidden.ServerClick
        Dim objList, a As ArrayList
        a = New ArrayList()
        objList = Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString())
        Functions.OsciSave(Me.Context, Session.SessionID, Session("seriesid").ToString(), customerID, Session("productid" & Session("seriesid").ToString()), objList, a)

        Dim cfgStep As String = ""
        cfgStep = Me.hidenStep.Value
        Select Case cfgStep
            Case "step1"
                Response.Redirect("default.aspx")
            Case "step2"
                Response.Redirect("configure_step2.aspx")
            Case "step3"
                Response.Redirect("configure_step3.aspx")
            Case "step4"
                Response.Redirect("configure_step4.aspx")
            Case "step5"
                Response.Redirect("configure_step5.aspx")
            Case "step6"
                Response.Redirect("configure_step6.aspx")
            Case "step7"
                Response.Redirect("configure_step7.aspx")
            Case "step8"
                Response.Redirect("configure_step8.aspx")
        End Select
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class