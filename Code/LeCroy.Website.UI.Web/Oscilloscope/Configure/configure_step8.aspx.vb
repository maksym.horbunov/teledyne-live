﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Oscilloscope_Configure_configure_step8
    Inherits QuoteBasePage
    Public masterProductID As String = ""
    Dim strProfile As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Initial()

        'Translations
        litOne.Text = Functions.LoadI18N("SLBCA0813", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0772", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0773", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0790", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0774", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0814", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnUpdate.Text = LoadI18N("SLBCA0769", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnAddToCart.Text = LoadI18N("SLBCA0768", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnSubmit.Text = LoadI18N("SLBCA0768", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnRemoveAll.Text = LoadI18N("SLBCA0770", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnStartOver.Text = LoadI18N("SLBCA0774", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

    End Sub

    Private Sub Initial()

        If Session("seriesid") Is Nothing Or Not Len(Session("seriesid")) > 0 Then
            Response.Redirect("default.aspx")
        End If

        Dim exists As Boolean = False
        Dim showUpdate As Boolean = False
        Dim content As StringBuilder = New StringBuilder()
        content.Append("<table width='600' border='0' cellspacing='0' cellpadding='0' border='0'>")

        Dim strSQL As String = ""
        Dim masterProductName As String = ""
        Dim masterCount As Integer = 0
        Dim productid As String
        productid = Session("productid" & Session("seriesid").ToString())
        ucquotes.selectedmodel = productid

        If Not Page.IsPostBack Then
            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                'If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
                Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                'ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                'End If
            End If
        Else
            Session("localeid") = ddlLanguage.SelectedValue
        End If

        '*get all master productid for this sessionid
        Dim sessionid As String
        sessionid = Me.Session.SessionID
        strSQL = "SELECT DISTINCT a.masterproductid, b.partnumber FROM SHOPPER a INNER JOIN PRODUCT b ON a.masterproductid = b.productid WHERE a.sessionid=@SESSIONID AND a.masterproductid=@MASTERPRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        masterCount = ds.Tables(0).Rows.Count
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim j As Integer = 0
            masterProductID = dr("masterproductid").ToString()
            masterProductName = "<a href='" + rootDir + "/Oscilloscope/OscilloscopeModel.aspx?modelid=" + dr("masterproductid").ToString() + "'>" + _
                     dr("partnumber").ToString() + "</a> "
            exists = True
            If (masterCount = 1) Then
                strSQL = "SELECT a.PARTNUMBER,a.PRODUCTDESCRIPTION,a.PRODUCTREMARK,b.productimage,d.qty,d.masterproductid,d.productid " &
                    " FROM PRODUCT a INNER JOIN PRODUCT_SERIES_CATEGORY c ON a.productid = c.product_id " +
                    " INNER JOIN PRODUCT_SERIES b ON c.product_series_id = b.product_series_id " +
                    " INNER JOIN shopper d ON a.productid = d.masterproductid " &
                    " where c.category_id = @CATEGORYID and a.productid = @PRODUCTID and d.productid = d.masterproductid and d.sessionid = @SESSIONID"
                'Response.Write(sql)
                'Response.End()
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", masterProductID))
                sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
                Dim dsimage As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If dsimage.Tables(0).Rows.Count > 0 Then
                    Dim drimage As DataRow = dsimage.Tables(0).Rows(0)
                    content.Append("<tr valign='top'>" +
                              "<td class='cell' width='30%'>Selected Series &amp; Model: <br />" +
                              "<img src='" + rootDir + drimage("productimage").ToString() + "' border='0'></td>" +
                              "<td class='cell' width='55%'><u><strong>" + drimage("PARTNUMBER").ToString() + "</strong></u>" +
                              getProductPriceIfApproved(Int32.Parse(masterProductID), Int32.Parse(drimage("masterproductid").ToString())) + "<br/>" +
                              Functions.GetProdDescriptionOnProdID(drimage("productid").ToString).ToString + "</td>")

                    content.Append("<td class='cell' width='4%'>" & _
                         "<strong>" + _
                         "&nbsp;" + _
                         "</td><td class='cell' width='11%'>&nbsp;</td>" + _
                         "</tr>")
                    Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Configuration Step7 - " + drimage("partnumber").ToString()
                End If
            End If

            'Dim cat(4) As String
            'cat(0) = AppConstants.CAT_ID_HARDWARE_OPTIONS
            'cat(1) = AppConstants.CAT_ID_SOFTWARE_OPTIONS
            'cat(2) = AppConstants.CAT_ID_PROBES
            'cat(3) = AppConstants.CAT_ID_ACCESSORIES
            'cat(4) = AppConstants.CAT_ID_ACCESSORIES

            'Get Categories for Options & Accessories
            strSQL = " SELECT DISTINCT CATEGORY.CATEGORY_ID, CATEGORY.CATEGORY_NAME,CATEGORY.SORT_ID FROM  SHOPPER INNER JOIN PRODUCT_GROUP ON SHOPPER.GROUP_ID = PRODUCT_GROUP.GROUP_ID INNER JOIN " +
                    " CATEGORY ON PRODUCT_GROUP.CATEGORY_ID = CATEGORY.CATEGORY_ID WHERE SHOPPER.SESSIONID = @SESSIONID AND SHOPPER.MASTERPRODUCTID = @MASTERPRODUCTID AND  SHOPPER.productid <> SHOPPER.masterproductid order by  CATEGORY.SORT_ID"
            ' Response.Write(strSQL)
            'Response.End()
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProductID.ToString()))
            Dim dsCat As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            'Dim k As Integer = 0
            'For k = 0 To cat.Length - 1
            '*
            If dsCat.Tables(0).Rows.Count > 0 Then
                For Each drCat As DataRow In dsCat.Tables(0).Rows
                    'strSQL = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " + _
                    '      " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID " + _
                    '      " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID  " + _
                    '      " WHERE a.PRODUCTID = " + masterProductID
                    strSQL = " SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE FROM  SHOPPER as a INNER JOIN PRODUCT_GROUP as b ON a.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " CATEGORY as c ON b.CATEGORY_ID = c.CATEGORY_ID WHERE a.SESSIONID = @SESSIONID AND a.MASTERPRODUCTID = @MASTERPRODUCTID AND b.CATEGORY_ID = @CATEGORYID ORDER by b.SORTID"

                    ' This is to show Warranty & Calibration group within Accessories Category 
                    ' separately.
                    'If (k = 3) Then
                    '    strSQL += " AND b.GROUP_ID <> " + AppConstants.GROUP_ID_WARRANTY
                    'End If
                    'If (k = 4) Then
                    '    strSQL += " AND b.GROUP_ID = " + AppConstants.GROUP_ID_WARRANTY
                    'End If
                    'strSQL += " and b.CATEGORY_ID=" + cat(k) + " ORDER BY b.SORTID"
                    ' Response.Write(strSQL)
                    ' Response.End()
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProductID))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", drCat("CATEGORY_ID").ToString()))
                    Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    For Each drr As DataRow In dss.Tables(0).Rows
                        Dim sqlstr As String = ""
                        Dim groupID As String = ""
                        Dim groupName As String = ""

                        groupID = drr("GROUP_ID").ToString()
                        groupName = drr("GROUP_NAME").ToString()

                        sqlstr = "SELECT DISTINCT a.productid AS 'masterproductid', c.productid, c.partnumber, c.productdescription,c.sort_id, d.qty " +
                                " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID " +
                                " INNER JOIN product_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
                                " INNER JOIN Shopper d ON  d.productid = c.productid " +
                                " WHERE a.PRODUCTID = @PRODUCTID AND b.group_id = @GROUPID AND d.masterproductid=a.productid AND d.sessionid=@SESSIONID Order by c.sort_id"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@PRODUCTID", masterProductID))
                        sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                        Dim dsss As DataSet= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                        Dim i As Integer = 0
                        For Each drrr As DataRow In dsss.Tables(0).Rows
                            Dim groupNameDisplay As String = ""
                            If (i = 0) Then
                                groupNameDisplay = groupName
                            End If
                            content.Append("<tr class='top'>")
                            If masterCount > 1 And j = 0 Then
                                content.Append("<td class='cell' width='30%'>" + masterProductName + "</td>")
                            ElseIf masterCount > 1 And j <> 0 Then
                                content.Append("<td class='cell' ></td>")
                            End If
                            content.Append("<td class='cell'   width='30%'>" + groupNameDisplay + "</td><td class='cell' width='55%'>" +
                             "<input type='hidden' name='subproductid' id='subproductid' value='" + drrr("productid").ToString() + "'>" +
                             "<input type='hidden' name='masterproductid' id='masterproductid' value='" + drrr("masterproductid").ToString() + "'>" +
                             "&nbsp;&nbsp;<strong><u>" +
                             drrr("partnumber").ToString() + "</u></strong>&nbsp;" +
                            getProductPriceIfApproved(Int32.Parse(drrr("productid").ToString()), Int32.Parse(drrr("masterproductid").ToString())) +
                            Functions.GetProdDescriptionOnProdID(drrr("productid").ToString).ToString + "</td><td class='cell' width='4%'><input name='qty' type='text' maxlength='2' id='qty' size='1' value='" +
                             drrr("qty").ToString() + "'  onKeyUp='javascript:onlyNum(this, event);' onKeyDown='javascript:onlyNum(this, event);'/>" +
                             "</td><td class='cell' width='11%'>&nbsp;&nbsp;<input id='chkDel' type='checkbox' name='chkDel' text='delete' value='" + drrr("masterproductid").ToString() + "*" + drrr("productid").ToString() + "'>&nbsp;" + LoadI18N("SLBCA0771", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</input></td>" +
                             "</tr>")
                            showUpdate = True
                            i = i + 1
                            j = j + 1
                        Next
                    Next
                Next
            End If
        Next
        If Not exists Then
            'Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString()) = Nothing
            'Session("productid" & Session("seriesid").ToString()) = Nothing
            'Session("seriesid") = Nothing
            Response.Redirect("default.aspx")
        End If
        If Not showUpdate Then
            btnUpdate.Visible = False
            btnRemoveAll.Visible = False
        End If
        content.Append("</table>")
        lbContent.Text = content.ToString()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Update("N") Then
            Initial()
        End If
    End Sub

    Private Function Update(ByVal updateAddToCart As String) As Boolean

        Dim sessionid As String
        Dim productid As String
        Dim sql As String = ""
        Dim masterProductClicked As Boolean = False
        Dim masterProdID As String = ""
        productid = Session("productid" & Session("seriesid").ToString())
        sessionid = Me.Session.SessionID

        Dim subProductIDs As String = ""
        Dim qty As String = ""
        Dim dels As String = ""
        Dim masterIDs As String = ""
        'subProductIDs = Request("subproductid").ToString()
        'qty = Request("qty").ToString()
        'masterIDs = Request("masterproductid").ToString()

        If Request("subproductid") Is Nothing Then
            subProductIDs = ""
        Else
            subProductIDs = Request("subproductid").ToString()
        End If
        If Request("masterproductid") Is Nothing Then
            masterIDs = ""
        Else
            masterIDs = Request("masterproductid").ToString()
        End If
        If Request("qty") Is Nothing Then
            qty = ""
        Else
            qty = Request("qty").ToString()
        End If

        If Request("chkDel") Is Nothing Then
            dels = ""
        Else
            dels = Request("chkDel").ToString()
        End If

        Dim sqlStatementsAndParameters As List(Of Tuple(Of String, List(Of SqlParameter))) = New List(Of Tuple(Of String, List(Of SqlParameter)))   ' If lists stop keeping order of insert, then this is out the door
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Not subProductIDs.Equals("") Then
            Dim arrayProductID() As String
            Dim arrayQty() As String
            Dim arrayDel() As String
            Dim arrayMasterId() As String

            If Not subProductIDs Is Nothing And Len(subProductIDs) > 0 Then
                arrayProductID = subProductIDs.Split(",")
                arrayQty = qty.Split(",")
                arrayDel = dels.Split(",")
                arrayMasterId = masterIDs.Split(",")
            End If

            Dim i As Integer = 0
            Dim SQLStringList As ArrayList = New ArrayList()
            If Not arrayProductID Is Nothing Then
                For i = 0 To arrayProductID.Length - 1
                    If arrayQty(i).ToString().Equals("") Then
                        lb_alert.Text = "<br/><font color=red>" + LoadI18N("SLBCA0796", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</font><br/><br/>"
                        Return False
                    End If
                    Try
                        Integer.Parse(arrayQty(i).ToString())
                    Catch
                        lb_alert.Text = "<br/><font color=red>" + LoadI18N("SLBCA0797", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</font><br/><br/>"
                        Return False
                    End Try
                Next
                lb_alert.Text = ""
                Dim objList As ArrayList
                objList = Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString())
                Dim del0 As String = ""
                Dim del1 As String = ""
                For i = 0 To arrayProductID.Length - 1
                    del0 = arrayMasterId(i)
                    del1 = arrayProductID(i)
                    Dim sqlString As String = "UPDATE SHOPPER SET QTY=@QTY WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QTY", arrayQty(i).ToString()))
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", del0.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", del1.ToString()))
                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                    If arrayQty(i) = "0" Then
                        If del0 = del1 Then
                            masterProductClicked = True
                            masterProdID = del1
                            RemoveSessionForMaster()
                        Else
                            ClearObjList(objList, del1)
                        End If
                    End If
                Next
                DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)

                If dels.Length > 0 Then
                    masterProductClicked = False
					sqlStatementsAndParameters = New List(Of Tuple(Of String, List(Of SqlParameter)))
                    For i = 0 To arrayDel.Length - 1
                        Dim aa() As String = arrayDel(i).Split("*")
                        del0 = aa(0)
                        del1 = aa(1)
                        Dim sqlString As String = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", del0.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTID", del1.ToString()))
                        sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                        If del0 = del1 Then
                            masterProdID = del1
                            masterProductClicked = True
                            RemoveSessionForMaster()
                        Else
                            ClearObjList(objList, del1)
                        End If
                    Next
                    DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)
                End If
            End If
        End If
        ' This will delete any options for which user has updated the Quantity to Zero
        ' If user updates quantity of the master product to Zero then delete all the options
        ' selected for that Model.
        sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND QTY = 0 "
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If masterProductClicked Then
            UpdateOptionsForMaster(masterProdID)
        End If

        If updateAddToCart.Equals("Y") Then
            ' This will update the ADD_TO_CART flag in SHOPPER table to 'y' 
            sql = "UPDATE SHOPPER SET ADD_TO_CART = 'y' WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", Session("productid" & Session("seriesid").ToString())))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        End If
        Return True
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        AddToCart()
    End Sub

    Protected Sub btnStartOver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStartOver.Click
        If Update("N") Then
            Session("seriesid") = Nothing
            Session("productid") = Nothing
            Session("objList") = Nothing
            Response.Redirect("default.aspx")
        Else
            Initial()
        End If

    End Sub

    Protected Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAll.Click
        Dim sql As String
        sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID and MASTERPRODUCTID = @MASTERPRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProductID))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        RemoveSessionForMaster()
        Response.Redirect("default.aspx")
    End Sub

    Protected Sub RemoveSessionForMaster()
        Session("objList" & Session("productid" & Session("seriesid").ToString())) = Nothing
        Session("productid" & Session("seriesid").ToString()) = Nothing
        Session("seriesid") = Nothing
        Session("productid") = Nothing
        Session("objList") = Nothing
    End Sub

    Protected Sub btnAddToCart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToCart.Click
        AddToCart()
    End Sub

    Protected Sub AddToCart()
        Session("netSessionID") = Me.Session.SessionID
        If Update("Y") Then
            Response.Redirect(rootDir + "/Shopper/")
        Else
            Initial()
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class