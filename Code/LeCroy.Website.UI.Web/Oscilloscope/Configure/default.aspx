<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Oscilloscope_Configure_configure_step1" Codebehind="default.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="javascript" type="text/javascript">
        ga('send', 'event', 'QuoteSystem', 'EnteredQuoteSystem', 'Scope_Default', { 'nonInteraction': 1 });
    </script> 
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br />
                        <div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr><td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td></tr>
                                <tr><td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td></tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <h2><asp:Literal ID="litOne" runat="server" /></h2>
            <div class="bandwidth">
                <strong><asp:Literal ID="litTwo" runat="server" /></strong>
                <asp:DropDownList ID="bandwidth" runat="server" AutoPostBack="True" />
            </div>
            <table width="700" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                <tr>
                    <td valign="top" width="700" style="height: 392px">
                        <asp:DataList ID="prodList" runat="server" RepeatColumns="4" ItemStyle-CssClass="item" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <div id="info">
                                    <div class="option"><%#Eval("name")%></div>
                                    <div class="product">
                                        <a href="configure_step2.aspx?seriesid=<%#Eval("PRODUCT_SERIES_ID")%>" class="info" onclick="ga('send', 'event', 'QuoteSystem', 'InitiatedQuoteSystem', 'Scope_Default', {'nonInteraction': 1});"><img src="<%=rootDir %><%#Eval("PRODUCTIMAGE")%>" border="0"></a><br /><br />
                                        <a href="configure_step2.aspx?seriesid=<%#Eval("PRODUCT_SERIES_ID")%>"><asp:Literal ID="litThree" runat="server" /></a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>