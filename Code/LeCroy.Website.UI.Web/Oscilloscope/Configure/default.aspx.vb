﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Oscilloscope_Configure_configure_step1
    Inherits QuoteBasePage
    Dim strSQL As String = ""
    Dim strProfile As String = ""
    Dim mseries As String = ""
    Dim modelid As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
        Else
            strSQL = "SELECT * from BANDWIDTH_RANGE where active_ind='y' ORDER BY SORT_ID"
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            bandwidth.DataSource = ds
            bandwidth.DataValueField = "RANGE_ID"
            bandwidth.DataTextField = "RANGE_VALUE"
            bandwidth.DataBind()
            bandwidth.Items.Insert(0, "ALL")
        End If
        showAllModels()
        litOne.Text = Functions.LoadI18N("SLBCA0753", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0799", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Sub showAllModels()
        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()
            ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
            Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
            'ddlLanguage.Visible = True
            'lbllanguage.Visible = True
            ' End If
        End If

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
        strSQL = String.Format("SELECT DISTINCT b.* " +
            " FROM PRODUCT a INNER JOIN product_series_category c ON  a.PRODUCTID = c.PRODUCT_ID INNER JOIN PRODUCT_SERIES b ON b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID " +
            " WHERE b.disabled='n' and c.CATEGORY_ID=@CATEGORYID AND a.PRODUCTID NOT IN ({0}) ", disabledProductContent.Item1)

        If Me.bandwidth.SelectedIndex <> 0 Then
            strSQL += " and a.RANGE_ID=@RANGEID"
            sqlParameters.Add(New SqlParameter("@RANGEID", Me.bandwidth.SelectedValue))
        End If
        strSQL += "  order by b.sortid "
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Me.prodList.DataSource = ds
        Me.prodList.DataBind()
    End Sub

    Protected Sub bandwidth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bandwidth.SelectedIndexChanged
        showAllModels()
    End Sub

    Protected Function getTitle(ByVal catid As String) As String
        Dim title As String = ""
        Dim seriesid As String = ""
        seriesid = Session("seriesid")
        strSQL = "SELECT name FROM product_series WHERE product_series_id = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
        title = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).ToString()
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + title
        Return title
    End Function

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub

    Private Sub prodList_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles prodList.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim litThree As Literal = CType(e.Item.FindControl("litThree"), Literal)
            litThree.Text = Functions.LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
    End Sub
End Class