﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Oscilloscope_Configure_configure_modular
    Inherits QuoteBasePage
    Dim strSQL As String = ""
    Public img As String = ""
    Public ser As String = ""
    Dim ds, ds1, ds2 As DataSet
    Dim seriesid As String = ""
    Dim modelid As String = ""
    Dim seriesidFromSession As String = ""
    Dim customerID As String = "0"
    Public strStepTitle As String = ""
    Dim strProfile As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'seriesid
        If Len(Request.QueryString("seriesid")) > 0 Then
            If Not IsNumeric(Request.QueryString("seriesid")) Then
                Response.Redirect("/oscilloscope/configure/")
            End If
            seriesid = Request.QueryString("seriesid")
            Session("seriesid") = seriesid
        End If
        'modelid

        If Len(Request.QueryString("modelid")) > 0 Then
            If Not IsNumeric(Request.QueryString("modelid")) Then
                Response.Redirect("/oscilloscope/configure/")
            End If
            modelid = Request.QueryString("modelid")
            Session("seriesSelected") = Functions.GetScopeSeriesIDonProductID(modelid)
        End If
        If Len(modelid) > 0 Then
            Session("seriesid") = Session("seriesSelected")
        End If
        If Len(Session("seriesSelected")) > 0 And Len(Request.QueryString("seriesid")) = 0 Then
            seriesid = Session("seriesSelected")
            seriesidFromSession = Session("seriesSelected")
        Else
            seriesid = Request.QueryString("seriesid")
            If Not Session("seriesid") Is Nothing Then
                seriesidFromSession = Session("seriesid")
            Else
                seriesidFromSession = seriesid
            End If
            Session("seriesSelected") = seriesidFromSession
        End If

        If Not Session("ContactID") Is Nothing Then
            If Len(Session("ContactID")) > 0 Then
                customerID = Session("ContactID").ToString()
            End If
        End If

        If (Not seriesid Is Nothing) And (Not seriesidFromSession Is Nothing) Then
            If Not seriesid.Equals(seriesidFromSession) Then
                'Session("productid" & seriesid) = Nothing
                Session("objList") = Nothing
                Session("seriesid") = Nothing
            End If
        End If

        If seriesid Is Nothing Then
            seriesid = seriesidFromSession
        End If


        If Len(seriesid) = 0 Then
            Response.Redirect("/oscilloscope/configure/")
        End If
        Session("seriesid") = seriesid

        Dim mNum As Integer
        Dim i As Integer
        If Not Me.IsPostBack Then
            strSQL = "SELECT DISTINCT a.*" +
                    " FROM PRODUCT a INNER JOIN PRODUCT_SERIES_CATEGORY c ON a.PRODUCTID = c.PRODUCT_ID " +
                    " INNER JOIN PRODUCT_SERIES b  ON b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID " +
                    " WHERE c.CATEGORY_ID=@CATEGORYID AND  a.disabled='n' and b.disabled='n' AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID order by a.sort_id "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            mNum = ds.Tables(0).Rows.Count
            If mNum > 0 Then
                For i = 0 To mNum - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    modellist.Items.Add(New ListItem("<u><strong>" + dr("PARTNUMBER").ToString + "</strong></u>&nbsp;" + getProductPriceIfApproved(Int32.Parse(dr("productid").ToString()), Nothing) +
                    "<a href='" + rootDir + "/Oscilloscope/OscilloscopeModel.aspx?modelid=" +
                    dr("PRODUCTID").ToString + "' target='window'>" + LoadI18N("SLBCA0012", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "&nbsp;<img src='" + rootDir + "/images/configure/ico_popup.jpg' title='" + LoadI18N("SLBCA0818", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "'  border='0' />" +
                     "</a><br /><div class='pad20'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</div>", dr("productid").ToString))
                    '"&nbsp;<img src='" + rootDir + "/images/configure/ico_popup.jpg' title='Click to learn more (will open in new window).'  border='0'/>" + _
                    If mNum = 1 Then
                        Dim selectedProduct As ListItem = modellist.Items.FindByValue(dr("productid").ToString)
                        selectedProduct.Selected = True
                        Session("productid" & Session("seriesid").ToString()) = dr("productid").ToString
                    End If
                Next

            Else
                Response.Redirect("/oscilloscope/configure/")
            End If

            If Not Session("productid" & Session("seriesid").ToString()) Is Nothing Then
                Dim selectedProduct As ListItem = modellist.Items.FindByValue(Session("productid" & Session("seriesid").ToString()).ToString())
                If Not selectedProduct Is Nothing Then
                    selectedProduct.Selected = True
                End If
            End If




            If Not Request.QueryString("modelid") Is Nothing Then
                If modelid.Length > 0 Then
                    Dim selectedProduct As ListItem = modellist.Items.FindByValue(modelid)
                    If Not selectedProduct Is Nothing Then
                        selectedProduct.Selected = True
                    End If
                    modellist_SelectedIndexChanged(Nothing, Nothing)
                End If
            End If
            If mNum = 1 And modellist.SelectedItem Is Nothing Then
                modellist.Items(0).Selected = True
                modellist_SelectedIndexChanged(Nothing, Nothing)
            End If

            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                'If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
                ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                'ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                ' End If
            End If
            RightInitial()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
            SetSelectedSeriesInRightSide()
        End If
        'set sessions: hasSoftware, hasProbes, hasAccessories and hasWarranty
        If modellist.SelectedValue <> "" Then
            Session("hasHardware") = hasOptions(modellist.SelectedValue, AppConstants.CAT_ID_HARDWARE_OPTIONS, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            Session("hasSoftware") = hasOptions(modellist.SelectedValue, AppConstants.CAT_ID_SOFTWARE_OPTIONS, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            Session("hasProbes") = hasOptions(modellist.SelectedValue, AppConstants.CAT_ID_PROBES, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            Session("hasAccessories") = hasOptions(modellist.SelectedValue, AppConstants.CAT_ID_ACCESSORIES, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            Session("hasWarranty") = hasOptions(modellist.SelectedValue, AppConstants.GROUP_ID_WARRANTY, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Else
            Session("hasHardware") = ""
            Session("hasSoftware") = ""
            Session("hasProbes") = ""
            Session("hasAccessories") = ""
            Session("hasWarranty") = ""
        End If

        'Translations
        litOne.Text = Functions.LoadI18N("SLBCA0815", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0806", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0805", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        priviousBtn.Text = LoadI18N("SLBCA0803", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        nextBtn.Text = LoadI18N("SLBCA0804", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        RequiredFieldValidator1.ErrorMessage = LoadI18N("SLBCA0802", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If seriesid.ToString = "332" Then
            strStepTitle = " - " + LoadI18N("SLBCA0816", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Else
            strStepTitle = " - " + LoadI18N("SLBCA0817", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
    End Sub

    Protected Sub modellist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles modellist.SelectedIndexChanged
        Session("productid" & Session("seriesid").ToString()) = Me.modellist.SelectedValue
        Session("objList") = Nothing
        SetSelectedSeriesInRightSide()
        SetSelectedModelInRightSide()
    End Sub

    Private Sub SetSelectedSeriesInRightSide()
        Dim sqlString As String = "SELECT * FROM [PRODUCT_SERIES] WHERE PRODUCT_SERIES_ID = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            img = rootDir + Me.convertToString(ds.Tables(0).Rows(0)("PRODUCTIMAGE"))
            ser = Me.convertToString(ds.Tables(0).Rows(0)("name"))
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Configuration Step2 - " + ser
        End If
    End Sub

    Private Sub SetSelectedModelInRightSide()
        Dim sqlString As String = "SELECT [PARTNUMBER] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", Session("productid" & Session("seriesid").ToString()).ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            ser += "</strong><br/><br/>" + LoadI18N("SLBCA0807", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "<br/><strong>" + ds.Tables(0).Rows(0)("partnumber").ToString() + "</strong>"
        End If
    End Sub

    Private Sub RightInitial()
        Dim lb As Literal
        Dim sqlstr As String

        If Not Session("seriesid") Is Nothing Then
            SetSelectedSeriesInRightSide()
        Else
            Return
        End If

        If Not Session("productid" & Session("seriesid")) Is Nothing And Len(Session("productid" & Session("seriesid"))) Then
            SetSelectedModelInRightSide()
        Else
            Return
        End If

        lb = New Literal()
        Dim gpid As String = ""
        sqlstr = "select a.group_id,b.group_name,c.partnumber,c.productid,c.productdescription from shopper a , PRODUCT_GROUP b, PRODUCT c " &
        "where a.sessionid=@SESSIONID and a.productid<> a.masterproductid and a.productid=c.productid and a.group_id=b.group_id and a.masterproductid=@MASTERPRODUCTID order by a.group_id"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", Session("productid" & Session("seriesid").ToString()).ToString()))
        Dim shopperProductInfos As List(Of ShopperProductInfo) = ShopperRepository.FetchShopperProductInfo(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray()).ToList()
        For Each spi In shopperProductInfos
            If (spi.GroupId.HasValue) Then
                If gpid <> spi.GroupId.Value.ToString() Then
                    lb.Text += "<br>"
                    lb.Text += spi.GroupName.ToString() & "<br>"
                    gpid = spi.GroupId.Value.ToString()
                End If
                lb.Text += "<u><strong>" + spi.PartNumber + "</strong></u>" + getProductPriceIfApproved(spi.ProductId, Int32.Parse(Session("productid" & Session("seriesid").ToString()).ToString())) + Functions.GetProdDescriptionOnProdID(spi.ProductId).ToString + "<br/>"
            End If
        Next
        p_rightmenu.Controls.Add(lb)
    End Sub

    Protected Sub priviousBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles priviousBtn.Click
        If modellist.SelectedIndex <> -1 Then
            Dim objList, a As ArrayList
            a = New ArrayList()
            objList = Session("objList" & modellist.SelectedValue)
            Functions.OsciSave(Me.Context, Session.SessionID, Session("seriesid").ToString(), customerID, modellist.SelectedValue, objList, a)
        End If
        Response.Redirect("/oscilloscope/configure/")
    End Sub

    Protected Sub nextBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles nextBtn.Click
        NextStep()
    End Sub

    'Protected Sub btnConfigure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfigure.Click
    '    NextStep()
    'End Sub

    Protected Sub NextStep()
        If modellist.SelectedValue Is Nothing And Len(modellist.SelectedValue) = 0 Then
            Return
        End If
        Dim objList, a As ArrayList
        a = New ArrayList()
        objList = Session("objList" & modellist.SelectedValue)
        Functions.OsciSave(Me.Context, Session.SessionID, Session("seriesid").ToString(), customerID, modellist.SelectedValue, objList, a)

        Response.Redirect("step1.aspx")

    End Sub

    Protected Sub QuickQuote()
        If modellist.SelectedValue Is Nothing And Len(modellist.SelectedValue) = 0 Then
            Return
        End If
        Dim sqlstr As String = ""
        Dim sessionid As String = Session.SessionID
        Dim masterproductid = modellist.SelectedValue
        Dim qty As Integer = 1
        If Len(masterproductid) > 0 Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlstr = "select * from shopper where sessionid=@SESSIONID and PRODUCTID=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            ds2= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
            If ds2.Tables(0).Rows.Count > 0 Then
                Dim dr1 As DataRow = ds2.Tables(0).Rows(0)
                qty = dr1("QTY") + 1
            End If

            '*save master product
            Dim groupID As String = ""
            Dim PROPERTYGROUPID As String = ""
            Dim ds As DataSet
            strSQL = "select * from PRODUCT where PRODUCTID=@PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                groupID = dr("group_id").ToString()
                PROPERTYGROUPID = dr("propertygroupid").ToString()
            End If

            sqlParameters = New List(Of SqlParameter)
            If qty = 1 Then
                strSQL = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, ADD_TO_CART) values (" +
                                    "@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@ADDTOCART)"
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
                sqlParameters.Add(New SqlParameter("@QTY", qty.ToString()))
                sqlParameters.Add(New SqlParameter("@PRICE", (Functions.GetProductPriceonProductID(Int32.Parse(masterproductid), Int32.Parse(masterproductid))).ToString()))
                sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
                sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerID))
                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
                sqlParameters.Add(New SqlParameter("@ADDTOCART", "Y"))
            Else
                strSQL = "UPDATE shopper set qty=@QTY where sessionid=@SESSIONID and PRODUCTID=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                sqlParameters.Add(New SqlParameter("@QTY", qty.ToString()))
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", masterproductid))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            End If
            'Response.Write(strSQL)
            DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Response.Redirect(rootDir + "/shopper/oscilloscope/configure/")
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class