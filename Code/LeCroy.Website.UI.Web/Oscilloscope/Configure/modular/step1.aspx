﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" Inherits="LeCroy.Website.Oscilloscope_Configure_Modular_Step1" Codebehind="step1.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br />
                        <div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr><td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td></tr>
                                <tr><td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td></tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="contentLeft">
                        <h2><asp:Literal ID="litOne" runat="server" /></h2>
                        <h3>-<asp:Literal ID="litTwo" runat="server" /></h3><br />
                        <strong><asp:Label ID="lblCount20" runat="server" Text="Please select up to 20 Acquisition Modules" ForeColor="Red" Visible="false" /></strong>
                        <asp:GridView ID="gvSlaves" runat="server" AutoGenerateColumns="False" CellPadding="1" CellSpacing="5" GridLines="None" ShowHeader="False" EnableTheming="False" Width="606px">
                            <RowStyle VerticalAlign="Top" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table border="0">
                                            <tr>
                                                <td valign="top" style="width: 30px">
                                                    <asp:TextBox ID="txtQTY" runat="server" CausesValidation="True" Text='<%# Bind("QTY") %>' Width="24px" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" /><br />
                                                    <asp:RegularExpressionValidator ID="RegExQTY" runat="server" ControlToValidate="txtQTY" ErrorMessage="Enter a number!" Font-Bold="True" Font-Size="XX-Small" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[0-9]+$" />
                                                </td>
                                                <td valign="top" align="left">
                                                    <asp:Label ID="Partnumber" runat="server" Font-Bold="True" Font-Underline="True" Text='<%# Bind("slavepartnumber") %>' /><br />
                                                    <asp:Label ID="Description" runat="server" Text='<%# Bind("slavedesc") %>' />
                                                    <asp:Label ID="lblPRODUCTID" runat="server" Text='<%# Bind("slaveproduct") %>' Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#C4ECFF" />
                        </asp:GridView>
                        <br />
                        <asp:Panel ID="p_ChannelSync" runat="server" Visible="false">
                            <h2><asp:Literal ID="litThree" runat="server" /></h2>
                            <h3><asp:Literal ID="litFour" runat="server" /></h3><br />
                            <table>
                                <tr><td><label for="ContentPlaceHolder1_ctl07_0"><u><strong>LabMaster CMH20-Zi</strong></u><br /><asp:Literal ID="litFive" runat="server" /></label></td><br /></tr>
                                <tr><td><strong><%= aCount %></strong>&nbsp;x&nbsp;<label for="ContentPlaceHolder1_ctl07_1"><u><strong>LabMaster CMH-1ACQMODULE-Zi</strong></u><br /><asp:Literal ID="litSix" runat="server" /></label></td></tr>
                            </table><br />
                        </asp:Panel>
                        <div class="stepNav">
                            <div class="leftSelection"><asp:Button ID="priviousBtn" runat="server" Text="<< Previous Step" /></div>
                            <div class="rightSelection">
                                <asp:Button ID="nextBtn" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'nextBtn', 'Scope_modular_Step1', {'nonInteraction': 1});" Text="Next Step >>" ValidationGroup="next" />
                                <asp:Button ID="nextBtn1" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'nextBtn1', 'Modular_channelSync', {'nonInteraction': 1});" Text="Continue To Next Step >>" ValidationGroup="next" Visible="false" />
                            </div>
                        </div>
                    </td>
                    <td valign="top" class="contentRight">
                        <h2><asp:Literal ID="litSeven" runat="server" /></h2><br /><br /><img src="<%=img %>" border="0" /><br />
                        <p>
                            <asp:Literal ID="litEight" runat="server" /><br /><strong><%=ser%></strong><br /><br />
                            <asp:Panel ID="p_rightmenu" runat="server" />
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>