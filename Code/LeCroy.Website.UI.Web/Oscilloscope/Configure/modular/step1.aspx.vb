﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Oscilloscope_Configure_Modular_Step1
    Inherits QuoteBasePage
    Dim strSQL As String = ""
    Public img As String = ""
    Public ser As String = ""
    Dim ds1, ds3, ds2 As DataSet
    Dim seriesid As String = ""
    Dim customerID As String = "0"
    Dim productid As String = ""
    Dim strProfile As String = ""
    Public aCount As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Len(Session("seriesid")) = 0 Then
            Response.Redirect("default.aspx")
        End If
        If Len(Session("productid" & Session("seriesid").ToString())) = 0 Then
            Response.Redirect("default.aspx")
        End If

        productid = Session("productid" & Session("seriesid").ToString())
        seriesid = Session("seriesid")

        If Not Me.IsPostBack Then
            strSQL = "SELECT DISTINCT c.productid as slaveproduct, c.partnumber as slavepartnumber, c.productdescription as slavedesc,c.sort_id,0 as qty " +
                                        " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID " +
                                        " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
                                        " LEFT OUTER JOIN (select productid, qty from shopper where sessionid=@SESSIONID and masterproductid=@MASTERPRODUCTID ) d" +
                                        " ON c.productid=d.productid WHERE a.PRODUCTID = @PRODUCTID AND b.group_id = 179 Order by c.sort_id"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            ds2 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            gvSlaves.DataSource = ds2
            gvSlaves.DataBind()

            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
                ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                ' ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                'End If
            End If
            RightInitial()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
            SetSelectedSeriesInRightSide()
        End If

        'Translations
        litOne.Text = Functions.LoadI18N("SLBCA0819", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0820", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0823", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0824", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0825", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0826", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0806", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = Functions.LoadI18N("SLBCA0805", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        priviousBtn.Text = LoadI18N("SLBCA0803", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        nextBtn.Text = LoadI18N("SLBCA0804", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        nextBtn1.Text = LoadI18N("SLBCA0827", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblCount20.Text = LoadI18N("SLBCA0822", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        For Each drGrid As GridViewRow In gvSlaves.Rows
            Dim regEx1 As RegularExpressionValidator = drGrid.FindControl("RegExQTY")
            If Not regEx1 Is Nothing Then
                regEx1.Text = LoadI18N("SLBCA0821", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            End If
        Next
    End Sub


    Private Sub SetSelectedSeriesInRightSide()
        Dim sqlString As String = "SELECT * FROM [PRODUCT_SERIES] WHERE PRODUCT_SERIES_ID = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            img = rootDir + Me.convertToString(ds.Tables(0).Rows(0)("PRODUCTIMAGE"))
            ser = Me.convertToString(ds.Tables(0).Rows(0)("name"))
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Configuration Step2 - " + ser
        End If
    End Sub

    Private Sub SetSelectedModelInRightSide()
        Dim sqlString As String = "SELECT [PARTNUMBER] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            ser += "</strong><br/><br/>" + LoadI18N("SLBCA0807", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "<br/><strong>" + ds.Tables(0).Rows(0)("partnumber").ToString() + "</strong>"
        End If
    End Sub

    Private Sub RightInitial()
        Dim lb As Literal
        Dim sqlstr As String

        If Not Session("seriesid") Is Nothing Then
            SetSelectedSeriesInRightSide()
        Else
            Return
        End If

        If Not Session("productid" & Session("seriesid")) Is Nothing And Len(Session("productid" & Session("seriesid"))) Then
            SetSelectedModelInRightSide()
        Else
            Return
        End If

        lb = New Literal()
        Dim gpid As String = ""
        sqlstr = "select a.group_id,b.group_name,c.partnumber,c.productid,c.productdescription from shopper a , PRODUCT_GROUP b, PRODUCT c " &
        "where a.sessionid=@SESSIONID and a.productid<> a.masterproductid and a.productid=c.productid and a.group_id=b.group_id and a.masterproductid=@MASTERPRODUCTID order by a.group_id"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
        Dim shopperProductInfos As List(Of ShopperProductInfo) = ShopperRepository.FetchShopperProductInfo(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray()).ToList()
        For Each spi In shopperProductInfos
            If (spi.GroupId.HasValue) Then
                If gpid <> spi.GroupId.Value.ToString() Then
                    lb.Text += "<br>"
                    lb.Text += spi.GroupName.ToString() & "<br>"
                    gpid = spi.GroupId.Value.ToString()
                End If
                lb.Text += "<u><strong>" + spi.PartNumber + "</strong></u>" + getProductPriceIfApproved(spi.ProductId, Int32.Parse(Session("productid" & Session("seriesid").ToString()).ToString())) + Functions.GetProdDescriptionOnProdID(spi.ProductId).ToString + "<br/>"
            End If
        Next
        p_rightmenu.Controls.Add(lb)
    End Sub

    Protected Sub SaveCart()
        Dim a As ArrayList
        Dim sqlstr As String
        Dim qty As String
        Dim ds As DataSet
        Dim sessionid As String = Session.SessionID

        a = New ArrayList()
        Dim sqlStatementsAndParameters As List(Of Tuple(Of String, List(Of SqlParameter))) = New List(Of Tuple(Of String, List(Of SqlParameter)))   ' If lists stop keeping order of insert, then this is out the door
        For Each drGrid As GridViewRow In gvSlaves.Rows
            Dim txtQtyCount As TextBox = drGrid.FindControl("txtQTY")
            Dim lblProdId As Label = drGrid.FindControl("lblPRODUCTID")
            If Not txtQtyCount Is Nothing Then

                sqlstr = "select * from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", lblProdId.Text.ToString()))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
                ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                If ds.Tables(0).Rows.Count = 0 And Len(txtQtyCount.Text.ToString) > 0 Then
                    qty = txtQtyCount.Text
                    If IsNumeric(qty) Then
                        If qty.ToString <> "0" Then
                            aCount = aCount + qty
                            sqlstr = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID)"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", lblProdId.Text.ToString()))
                            sqlParameters.Add(New SqlParameter("@QTY", qty.ToString()))
                            sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(Int32.Parse(lblProdId.Text), Int32.Parse(productid))).ToString()))
                            sqlParameters.Add(New SqlParameter("@GROUPID", "179"))
                            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
                            sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerID))
                            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", "152"))
                            sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                        End If
                    End If
                Else
                    sqlstr = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", lblProdId.Text.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlstr, sqlParameters))
                End If
            End If
        Next
        DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)
        If aCount > 20 Or aCount = 0 Then
            lblCount20.Visible = True
        ElseIf aCount >= 5 Then
            lblCount20.Visible = False
            p_ChannelSync.Visible = True
            For Each drGrid As GridViewRow In gvSlaves.Rows
                Dim txtQtyCount As TextBox = drGrid.FindControl("txtQTY")
                If Not txtQtyCount Is Nothing Then
                    txtQtyCount.ReadOnly = True
                End If
            Next
            nextBtn.Visible = False
            nextBtn1.Visible = True
        Else
            Response.Redirect("step2.aspx")
        End If
    End Sub


    Protected Sub priviousBtn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles priviousBtn.Click
        Response.Redirect("default.aspx")
    End Sub
    Protected Sub nextBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles nextBtn.Click
        SaveCart()

    End Sub


    Protected Sub nextBtn1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles nextBtn1.Click
        Dim sessionid As String = Session.SessionID

        strSQL = "Select sum(QTY) as aCount from SHOPPER where  sessionid=@SESSIONID and group_id=179 and MASTERPRODUCTID=@MASTERPRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
        ds3 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        If ds3.Tables(0).Rows.Count > 0 Then

            Dim dr As DataRow = ds3.Tables(0).Rows(0)
            strSQL = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID) values (" +
                        "@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID)"

            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", "6893"))
            sqlParameters.Add(New SqlParameter("@QTY", "1"))
            sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(6893, Int32.Parse(productid))).ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", "214"))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerID))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", "155"))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            strSQL = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID) values (" +
                        "@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID)"

            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", "6894"))
            sqlParameters.Add(New SqlParameter("@QTY", dr("aCount").ToString()))
            sqlParameters.Add(New SqlParameter("@PRICE", (GetProductPriceonProductID(6894, Int32.Parse(productid))).ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", "214"))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerID))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", "155"))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If
        Response.Redirect("step2.aspx")
    End Sub
    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class