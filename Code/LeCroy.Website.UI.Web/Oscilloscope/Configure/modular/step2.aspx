﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" Inherits="LeCroy.Website.Oscilloscope_Configure_Modular_Step2" Codebehind="step2.aspx.vb" EnableSessionState="True" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="javascript" type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            alert(rdBtn);
            var rdBtnList = document.getElementsByTagName("input");

            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br />
                        <div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr><td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td></tr>
                                <tr><td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td></tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="contentLeft">
                        <h2><asp:Literal ID="litOne" runat="server" /></h2>
                        <asp:Panel ID="p_proList" runat="server" BorderStyle="None">
                            <h3>-<asp:Literal ID="litTwo" runat="server" /></h3>
                            <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="False" CellPadding="1" CellSpacing="5" GridLines="None" ShowHeader="False" EnableTheming="False" Visible="False">
                                <RowStyle VerticalAlign="Top" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td><strong><%= aCount %></strong> X</td>
                                                    <td valign="top">
                                                        <asp:RadioButton runat="server" ID="rbScopes" GroupName="gScopes" AutoPostBack="true" />
                                                        <asp:Label ID="lblPRODUCTID" runat="server" Text='<%# Bind("PRODUCTID") %>' Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="Partnumber" runat="server" Font-Bold="True" Font-Underline="True" Text='<%# Bind("PARTNUMBER") %>' /><br />
                                                        <asp:Label ID="Description" runat="server" Text='<%# Bind("PRODUCTDESCRIPTION") %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Underline="False" />
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#C4ECFF" />
                            </asp:GridView>
                            <asp:RadioButtonList ID="modellist" runat="server" AutoPostBack="True" />
                        </asp:Panel>
                        <br /><br />
                        <div class="stepNav">
                            <div class="leftSelection">
                                <asp:Button ID="btnQuickQuote2" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'btnQuickQuote1', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Quick Quote" ValidationGroup="next" />&nbsp;
                                <asp:Button ID="btnConfigure" runat="server" OnClientClick="ga('send', 'event', 'QuoteSystem', 'btnConfigure', 'Scope_Configure_Step2', {'nonInteraction': 1});" Text="Configure System" ValidationGroup="next" />
                            </div>
                        </div>
                        <div class="stepNav">
                            <div class="leftSelection"><asp:Button ID="priviousBtn" runat="server" Text="<< Previous Step" /></div>
                        </div>
                    </td>
                    <td valign="top" class="contentRight">
                        <h2><asp:Literal ID="litThree" runat="server" /></h2><br /><br /><img src="<%=img %>" border="0"><br />
                        <p>
                            <asp:Literal ID="litFour" runat="server" /><br /><strong><%=ser%></strong><br /><br />
                            <asp:Panel ID="p_rightmenu" runat="server" />
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>