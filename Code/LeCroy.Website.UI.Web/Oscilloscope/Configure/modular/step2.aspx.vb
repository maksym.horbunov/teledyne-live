﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities

Partial Class Oscilloscope_Configure_Modular_Step2
    Inherits QuoteBasePage
    Dim strSQL As String = ""
    Public img As String = ""
    Public ser As String = ""
    Dim ds, ds1, ds2, ds3 As DataSet
    Dim seriesid As String = ""
    Dim modelid As String = ""
    Dim seriesidFromSession As String = ""
    Dim customerID As String = "0"
    Dim productid As String = ""
    Dim mempryproductid As String = ""
    Public aCount As Integer = 0
    Dim strProfile As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Len(Session("seriesid")) = 0 Then
            Response.Redirect("default.aspx")
        End If
        If Len(Session("productid" & Session("seriesid").ToString())) = 0 Then
            Response.Redirect("default.aspx")
        End If
        productid = Session("productid" & Session("seriesid").ToString())

        If Len(productid) = 0 Then

            Response.Redirect("default.aspx")
        End If

        Session("seriesid") = Functions.GetScopeSeriesIDonProductID(productid)
        seriesid = Session("seriesid")



        If Not Session("ContactID") Is Nothing Then
            If Len(Session("ContactID")) > 0 Then
                customerID = Session("ContactID").ToString()
            End If
        End If



        If Len(seriesid) = 0 Then
            Response.Redirect("default.aspx")
        End If
        Session("seriesid") = seriesid


        Dim mNum As Integer


        If Not Me.IsPostBack Then
            Dim sessionid As String = Session.SessionID

            strSQL = "Select sum(QTY) as aCount from SHOPPER where  sessionid=@SESSIONID and group_id=179 and MASTERPRODUCTID=@MASTERPRODUCTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            ds3 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            If ds3.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds3.Tables(0).Rows(0)
                aCount = dr("aCount")
            End If

            strSQL = "SELECT DISTINCT c.productid , d.qty, c.partnumber , c.productdescription ,c.sort_id " +
                " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID " +
                " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
                " LEFT OUTER JOIN (select productid, qty from shopper where sessionid=@SESSIONID and masterproductid=@MASTERPRODUCTID) d" +
                " ON c.productid=d.productid WHERE a.PRODUCTID = @PRODUCTID AND b.group_id = 24 Order by c.sort_id"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            mNum = ds.Tables(0).Rows.Count

            gvProducts.DataSource = ds
            gvProducts.DataBind()

            If mNum > 0 Then
                For i = 0 To mNum - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    modellist.Items.Add(New ListItem("<strong>" + aCount.ToString + " x <u>" + dr("PARTNUMBER").ToString + "</strong></u>&nbsp;" + getProductPriceIfApproved(Int32.Parse(dr("productid").ToString()), Int32.Parse(productid)) +
                    "<br /><div class='pad20'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</div>", dr("productid").ToString))

                    If mNum = 1 Then
                        Dim selectedProduct As ListItem = modellist.Items.FindByValue(dr("productid").ToString)
                        selectedProduct.Selected = True
                    End If
                Next
            End If

            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                'If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
                ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                'ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                'End If
            End If
            RightInitial()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
            SetSelectedSeriesInRightSide()
        End If

        litOne.Text = Functions.LoadI18N("SLBCA0819", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0828", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0806", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0805", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnQuickQuote2.Text = LoadI18N("SLBCA0415", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnConfigure.Text = LoadI18N("SLBCA0406", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        priviousBtn.Text = LoadI18N("SLBCA0803", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0800", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Protected Sub modellist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles modellist.SelectedIndexChanged
        mempryproductid = Me.modellist.SelectedValue
        Session("objList") = Nothing
        SetSelectedSeriesInRightSide()
        SetSelectedModelInRightSide()
    End Sub

    Private Sub SetSelectedSeriesInRightSide()
        Dim sqlString As String = "SELECT * FROM [PRODUCT_SERIES] WHERE PRODUCT_SERIES_ID = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("seriesid").ToString()))
        Dim ds As DataSet = New DataSet()
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            img = rootDir + Me.convertToString(ds.Tables(0).Rows(0)("PRODUCTIMAGE"))
            ser = Me.convertToString(ds.Tables(0).Rows(0)("name"))
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Configuration Step2 - " + ser
        End If
    End Sub

    Private Sub SetSelectedModelInRightSide()
        Dim sqlString As String = "SELECT [PARTNUMBER] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            ser += "</strong><br/><br/>" + LoadI18N("SLBCA0807", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "<br/><strong>" + ds.Tables(0).Rows(0)("partnumber").ToString() + "</strong>"
        End If
    End Sub

    Private Sub RightInitial()
        Dim lb As Literal
        Dim sqlstr As String

        If Not Session("seriesid") Is Nothing Then
            SetSelectedSeriesInRightSide()
        Else
            Return
        End If

        If Not Session("productid" & Session("seriesid")) Is Nothing And Len(Session("productid" & Session("seriesid"))) Then
            SetSelectedModelInRightSide()
        Else
            Return
        End If

        lb = New Literal()
        Dim gpid As String = ""
        sqlstr = "select a.group_id,b.group_name,c.partnumber,c.productid,c.productdescription from shopper a , PRODUCT_GROUP b, PRODUCT c " &
        "where a.sessionid=@SESSIONID and a.productid<> a.masterproductid " &
        "and a.productid=c.productid and a.group_id=b.group_id and a.masterproductid=@MASTERPRODUCTID order by a.group_id"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
        Dim shopperProductInfos As List(Of ShopperProductInfo) = ShopperRepository.FetchShopperProductInfo(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray()).ToList()
        For Each spi In shopperProductInfos
            If (spi.GroupId.HasValue) Then
                If gpid <> spi.GroupId.Value.ToString() Then
                    lb.Text += "<br>"
                    lb.Text += spi.GroupName.ToString() & "<br>"
                    gpid = spi.GroupId.Value.ToString()
                End If
                lb.Text += "<u><strong>" + spi.PartNumber + "</strong></u>" + getProductPriceIfApproved(spi.ProductId, Int32.Parse(Session("productid" & Session("seriesid").ToString()).ToString())) + Functions.GetProdDescriptionOnProdID(spi.ProductId).ToString + "<br/>"
            End If
        Next
        p_rightmenu.Controls.Add(lb)
    End Sub

    Protected Sub SaveModel()

        Dim sessionid As String = Session.SessionID
        Dim mempryproductid = modellist.SelectedValue
        Dim qty As Integer = 1
        If Len(mempryproductid) > 0 Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            strSQL = "delete from shopper where sessionid=@SESSIONID and productid=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID"
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", mempryproductid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            strSQL = "Select sum(QTY) as aCount from SHOPPER where  sessionid=@SESSIONID and group_id=179 and MASTERPRODUCTID=@MASTERPRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            ds3 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            If ds3.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds3.Tables(0).Rows(0)
                aCount = dr("aCount")
            End If

            strSQL = "insert into shopper (sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,CUSTOMERID,Date_entered,PROPERTYGROUPID, ADD_TO_CART) values (" +
                           "@SESSIONID,@PRODUCTID,@QTY,@PRICE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@CUSTOMERID,@DATEENTERED,@PROPERTYGROUPID,@ADDTOCART)"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", mempryproductid))
            sqlParameters.Add(New SqlParameter("@QTY", aCount.ToString()))
            sqlParameters.Add(New SqlParameter("@PRICE", (Functions.GetProductPriceonProductID(Int32.Parse(mempryproductid), Int32.Parse(productid))).ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", "24"))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", customerID))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", "24"))
            sqlParameters.Add(New SqlParameter("@ADDTOCART", "N"))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If
    End Sub

    Private Sub gvProducts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProducts.SelectedIndexChanged
        For Each drGrid As GridViewRow In gvProducts.Rows
            Dim chkP As RadioButton = drGrid.FindControl("rbScopes")
            If Not chkP Is Nothing Then
                chkP.Checked = False
            End If
        Next
    End Sub

    Protected Sub btnQuickQuote2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnQuickQuote2.Click
        SaveModel()

        strSQL = "UPDATE shopper set ADD_TO_CART='y' where sessionid=@SESSIONID and ADD_TO_CART='n' and MASTERPRODUCTID=@MASTERPRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", productid))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Session("flgQuickQUote") = "true"
        Response.Redirect(rootDir + "/shopper/default.aspx")
    End Sub

    Protected Sub btnConfigure_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfigure.Click
        SaveModel()
        If Session("hasHardware") = True Then
            Response.Redirect("/oscilloscope/configure/configure_step3.aspx")
        End If
        If Session("hasSoftware") = True Then
            Response.Redirect("/oscilloscope/configure/configure_step4.aspx")
        End If
        If Session("hasProbes") = True Then
            Response.Redirect("/oscilloscope/configure/configure_step5.aspx")
        End If
        If Session("hasAccessories") = True Then
            Response.Redirect("/oscilloscope/configure/configure_step6.aspx")
        End If
        If Session("hasWarranty") = True Then
            Response.Redirect("/oscilloscope/configure/configure_step7.aspx")
        Else
            Response.Redirect("/oscilloscope/configure/configure_step8.aspx")
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class