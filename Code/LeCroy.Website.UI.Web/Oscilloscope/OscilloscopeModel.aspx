<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Oscilloscope_OscilloscopeModel" Codebehind="OscilloscopeModel.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="current">
                    <asp:Literal ID="lbSeries1" runat="server" />
                    <ul><asp:Literal ID="lbProduct1" runat="server" /></ul>
                </li>
            </ul>
        </asp:Panel>
        <asp:Literal ID="lb_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Image ID="imgModel" runat="server" Height="211" Width="540" AlternateText="Oscilloscope" />
    <asp:Label ID="lbModelImage" runat="server" />
    <div class="content2">
        <h1><asp:Literal ID="landTitle" runat="server" /></h1>
        <p><asp:Literal ID="landContent" runat="server" /></p>
    </div><br />
	<div class="tabs">
		<div class="bg">
			<ul class="tabNavigation"><asp:Literal  ID="lblTabsToShow" runat="server" /></ul>					
			<div id="product_line">
                <asp:Label ID="lbQuickSpecs" runat="server" />
                    <div class="modelCompare">
                    <asp:Literal ID="litProductSeries" runat="server" />
                    <input type="hidden" id="modelid1" name="modelid1" />
                    <input type="hidden" id="modelid2" name="modelid2" />
                    <input type="hidden" id="modelid3" name="modelid3" />
                    <input type="hidden" id="hdnCallingPage" name="hdnCallingPage" value="ScopeModelPage" />
                    Check up to three models. <asp:Button ID="btnCompare" runat="server" CausesValidation="false" PostBackUrl="~/resources/compare/compare.aspx" Text="Compare" />
                </div>
            </div>
            <div id="overview"><asp:Literal ID="lblOverview" runat="server" /></div>
            <div id="product_details"><asp:Literal ID="lblProductDetail" runat="server" /></div>
            <div id="spec"><asp:Literal ID="lblSpecs" runat="server" /></div>
            <div id="options"><asp:Literal ID="lblOptions" runat="server" /></div>
            <div id="probes"><asp:Literal ID="lblProbes" runat="server" /></div>
            <div id="listprice"><asp:Literal ID="lblListPrice" runat="server" /></div > 
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
    <script language="javascript" type="text/javascript">
        function checkProducts(obj) {
            var products = document.getElementsByName("chkProductID");
            var c = 0
            for (i = 0; i < products.length; i++) {
                if (products[i].checked) {
                    c = c + 1;
                }
            }
            jQuery('#modelid1').val('');
            jQuery('#modelid2').val('');
            jQuery('#modelid3').val('');
            if (c <= 0) {
                return;
            }
            if (c > 3) {
                jQuery('#<%= btnCompare.ClientID %>').attr('disabled', true);
                alert('Please only select 3 models');
                return;
            }
            if (c > 0) {
                var x = 0;
                jQuery('#<%= btnCompare.ClientID %>').removeAttr('disabled');
                for (i = 0; i < products.length; i++) {
                    if (x == 0 && products[i].checked) jQuery('#modelid1').val(products[i].value);
                    if (x == 1 && products[i].checked) jQuery('#modelid2').val(products[i].value);
                    if (x == 2 && products[i].checked) jQuery('#modelid3').val(products[i].value);
                    if (products[i].checked) x = x + 1;
                    if (x >= 3) break;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="lbPromotion" runat="server" />
    <asp:HyperLink ID="hlkQuote" runat="server"><img src="<%=rootDir %>/images/promos/rt_promo_configure.gif" border="0" height="65" width="203" /></asp:HyperLink>
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="1" ShowBuy="false" ShowRequestDemo="true" ShowRequestQuote="true" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>