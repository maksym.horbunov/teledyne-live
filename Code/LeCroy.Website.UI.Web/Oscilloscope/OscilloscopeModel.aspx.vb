Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class Oscilloscope_OscilloscopeModel
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, ds1 As DataSet
    Public Shared modelName As String = ""
    Public Shared dataSheetURL As String = ""
    Public Shared specificationURL As String = ""
    'Dim promoImages As String = ""
    Dim firstmenuid As Integer = 0
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Private _productSeriesId As Int32 = 0
    Protected _productId As Int32 = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscopes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HandleRoute()
        Dim imageIndex As Integer = 0
        Dim captionID As String = ""
        Dim menuID As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If (_productId <= 0) Then
            If IsNumeric(Request.QueryString("modelid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ",") Then
                    _productId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ",") + 1)
                Else
                    _productId = SQLStringWithOutSingleQuotes(Request.QueryString("modelid"))
                End If

                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
                _productSeriesId = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT  [PRODUCT_SERIES_ID] FROM [dbo].[PRODUCT_SERIES_CATEGORY] " +
                            " where CATEGORY_ID = @CATEGORYID AND PRODUCT_ID=@PRODUCTID UNION " +
                            " SELECT DISTINCT a.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY AS a INNER JOIN " +
                            " CONFIG AS b ON a.PRODUCT_ID = b.PRODUCTID WHERE a.CATEGORY_ID = @CATEGORYID AND b.OPTIONID = @PRODUCTID", sqlParameters.ToArray())
                If _productSeriesId <= 0 Then
                    Response.Redirect("default.aspx")
                End If

                Dim innerPassThroughProductSeriesId As Int32 = _productSeriesId
                If (_productSeriesId = 591) Then
                    innerPassThroughProductSeriesId = 511
                End If

                Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), innerPassThroughProductSeriesId)
                If (productSeries Is Nothing) Then Response.Redirect("Default.aspx")

                Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId)
                If (product Is Nothing) Then Response.Redirect("Default.aspx")

                If (String.IsNullOrEmpty(productSeries.SeoValue) Or String.IsNullOrEmpty(product.SeoValue)) Then
                    'Fall through
                Else
                    CreateMeta(productSeries.SeoValue, product.SeoValue)
                    Response.Redirect(String.Format("~/oscilloscope/{0}/{1}", productSeries.SeoValue, product.SeoValue))
                End If
            Else
                Response.Redirect("Default.aspx")
            End If
        End If

        captionID = AppConstants.SCOPE_CAPTION_ID

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        Dim passThroughProductSeriesId As Int32 = _productSeriesId
        If (_productSeriesId = 591) Then    ' HACK: Special for TASK0162804
            Dim productIds As List(Of Int32) = New List(Of Int32)
            productIds.Add(_productId)
            Dim productSeriesIds As List(Of Int32) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForProductIdsAndCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), CategoryIds.OSCILLOSCOPES, productIds).Select(Function(x) x.ProductSeriesId).Distinct().ToList()
            If (productSeriesIds.Contains(511)) Then
                passThroughProductSeriesId = 511
            End If
        End If

        If Not (FeProductManager.CanProductShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.ToString(), CategoryIds.OSCILLOSCOPES, passThroughProductSeriesId.ToString())) Then
            Response.Redirect("~/")
        End If

        If Not CheckSeriesExists(_productSeriesId.ToString(), 1) Then
            Response.Redirect("default.aspx")
        End If
        If Not Page.IsPostBack Then
            ucResources.ProductSeriesId = _productSeriesId
            hlkQuote.NavigateUrl = String.Format("~/oscilloscope/configure/configure_step2.aspx?modelid={0}", _productId)
            Functions.CreateMetaTags(Me.Header, _productId.ToString(), AppConstants.PRODUCT_OVERVIEW_TYPE)
            getProductLine()
            getOverview()
            getProductDetail()
            getSpecs()
            getOptions()
            getprobes()
            If Not Session("CountryCode") Is Nothing Then
                If Len(Session("CountryCode").ToString) > 0 Then
                    If Session("CountryCode").ToString = "208" Then
                        getListPrice()
                    End If
                End If
            End If
            lblTabsToShow.Text = tabsToShow.ToString()

            strSQL = "SELECT NAME,PRODUCTDESCRIPTION,INTRODUCTION,INTRO_IMAGE_PATH FROM [PRODUCT] where PRODUCTID=@PRODUCTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                modelName = dr("NAME").ToString()
                imgModel.ImageUrl = String.Format("{0}{1}", rootDir, ds.Tables(0).Rows(0)("INTRO_IMAGE_PATH").ToString())
                imgModel.AlternateText = modelName + " Oscilloscope"
                Me.landTitle.Text = dr("NAME").ToString()
                Me.landContent.Text = dr("INTRODUCTION").ToString()
                'Me.landDesc.Text = Functions.GetProdDescriptionOnProdID(modelid.ToString).ToString
                If Len(Functions.GetPageTitle(_productId.ToString(), AppConstants.PRODUCT_OVERVIEW_TYPE)) > 0 Then
                    Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.GetPageTitle(_productId.ToString(), AppConstants.PRODUCT_OVERVIEW_TYPE)
                Else
                    Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + modelName
                End If
            End If
            'lblRequestDemo.Text = "<a href='" + rootDir + "/oscilloscope/demo.aspx?mseries=" & mseries & "'>Request Demo</a>"
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _productSeriesId.ToString()).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _productSeriesId.ToString())
            End If
            lb_leftmenu.Text = Functions.LeftMenuScope(captionID, rootDir, pn_leftmenu, menuID, _productSeriesId.ToString(), _productId.ToString())
            pn_leftmenu.Visible = False
        End If
        If hasDetail Then
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
        ElseIf hasOverview Then
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
        Else
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
        End If
        For i = 1 To 3
            litProductSeries.Text += String.Format("<input type=""hidden"" id=""mseries{0}"" name=""mseries{0}"" value=""{1}"" />", i, _productSeriesId.ToString())
        Next
    End Sub

    Private Sub getOverview()
        'check if model overview exists
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        sqlParameters.Add(New SqlParameter("@ID", _productId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_MODEL_OVERVIEW_TYPE))
        ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds1.Tables(0).Rows.Count > 0 Then
            Dim dr1 As DataRow = ds1.Tables(0).Rows(0)
            Me.lblOverview.Text = dr1("OVER_VIEW_DETAILS").ToString()
            firstmenuid = 1
            hasOverview = True
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        Else
            'if model overview doesn't exist, show series overview
            strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", _productSeriesId.ToString()))
            sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
                firstmenuid = 1
                hasOverview = True
                tabsToShow.Append("<li><a href='#overview'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getProductDetail()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _productId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PRODUCT_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            firstmenuid = 2
            If Len(Trim(dr("OVER_VIEW_DETAILS").ToString())) > 0 Then
                Me.lblProductDetail.Text = dr("OVER_VIEW_DETAILS").ToString()
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getSpecs()
        Dim propName As String = ""
        Dim propValue As String = ""
        Dim specString As String = ""
        Dim pNum As Integer
        Dim i As Integer
        Dim ii As Integer
        strSQL = "SELECT distinct b.prop_cat_id, a.prop_cat_name,a.sort_id FROM " +
                " property_category a INNER JOIN  PROPERTY b ON  a.prop_cat_id = b.prop_cat_id " +
                " INNER JOIN PRODUCTPROPVALUE c  ON c.PROPERTYID = b.PROPERTYID " +
                " WHERE c.PRODUCTID=@PRODUCTID and c.VALUE>' ' and c.QUICK_SPEC_IND = 'N' ORDER by a.SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            specString = " " +
                " <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                specString = specString + " <tr> " +
                    " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>" +
                   " </tr> "


                strSQL = "SELECT b.PROPERTYNAME, c.VALUE " +
                        "FROM  PROPERTY as b INNER JOIN PRODUCTPROPVALUE as c On b.PROPERTYID=c.PROPERTYID " +
                        "WHERE b.prop_cat_id = @PROPCATID and c.PRODUCTID=@PRODUCTID and c.VALUE>' ' and c.QUICK_SPEC_IND = 'N' ORDER by b.SORTID"
                Dim ds2 As DataSet
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id"))))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
                ds2 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                pNum = ds2.Tables(0).Rows.Count
                If pNum > 0 Then
                    For ii = 0 To pNum - 1
                        propName = ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()
                        propValue = ds2.Tables(0).Rows(ii)("VALUE").ToString()
                        specString = specString + " <tr> " +
                            " <td width='35%' class='cell'>" + propName + "</td>" +
                            " <td width='65%' class='cell'>" + propValue + "</td> " +
                           " </tr> "
                    Next
                    specString = specString + " <tr> " +
                        " <td colspan='2' class='cell'>&nbsp;</td>" +
                       " </tr> "
                End If
            Next
            specString = specString + " </table> "
            firstmenuid = 3
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
        Me.lblSpecs.Text = specString
    End Sub

    Private Sub getOptions()
        Dim optionString As String = ""
        Dim sqlGetGroup As String = ""
        Dim gNum As Integer
        Dim i As Integer
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        ' First get all the distinct GROUP_NAME for various options available for the series
        If _productSeriesId = 484 Or _productSeriesId = 332 Then
            sqlGetGroup = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID " +
              " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " +
              " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" +
              " INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID" +
              " INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID" +
              " WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND b.CATEGORY_ID in (@CATEGORYID1,@CATEGORYID2,@CATEGORYID3,@CATEGORYID4,@CATEGORYID5,@CATEGORYID6,@CATEGORYID7) And c.disabled='n' AND e.disabled='n' ORDER BY b.SORTID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_HARDWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SOFTWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@CATEGORYID3", AppConstants.CAT_ID_MIXEDSIGNAL))
            sqlParameters.Add(New SqlParameter("@CATEGORYID4", AppConstants.CAT_ID_ACCESSORIES))
            sqlParameters.Add(New SqlParameter("@CATEGORYID5", AppConstants.CAT_ID_FEATURES))
            sqlParameters.Add(New SqlParameter("@CATEGORYID6", AppConstants.CAT_ID_LOGICSTUDIO))
            sqlParameters.Add(New SqlParameter("@CATEGORYID7", "34"))
        Else
            sqlGetGroup = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID " +
             " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " +
             " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" +
             " WHERE a.PRODUCTID = @PRODUCTSERIESID And b.CATEGORY_ID In (@CATEGORYID1,@CATEGORYID2,@CATEGORYID3,@CATEGORYID4,@CATEGORYID4,@CATEGORYID5,@CATEGORYID6,@CATEGORYID7) And c.disabled='n' ORDER BY b.SORTID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_HARDWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SOFTWARE_OPTIONS))
            sqlParameters.Add(New SqlParameter("@CATEGORYID3", AppConstants.CAT_ID_MIXEDSIGNAL))
            sqlParameters.Add(New SqlParameter("@CATEGORYID4", AppConstants.CAT_ID_ACCESSORIES))
            sqlParameters.Add(New SqlParameter("@CATEGORYID5", AppConstants.CAT_ID_FEATURES))
            sqlParameters.Add(New SqlParameter("@CATEGORYID6", AppConstants.CAT_ID_LOGICSTUDIO))
            sqlParameters.Add(New SqlParameter("@CATEGORYID7", "34"))
        End If

        'Response.Write(sqlGetGroup)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetGroup, sqlParameters.ToArray())
        gNum = ds.Tables(0).Rows.Count
        If gNum > 0 Then
            For i = 0 To gNum - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                Dim sqlGetOptions As String = ""
                Dim optionDs As DataSet
                Dim oNum As Integer
                Dim j As Integer

                optionString += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>" +
                               " <tr> " +
                               " <td class='cell'><h3>" + dr("GROUP_NAME").ToString() + "</h3></td> " +
                               " </tr> "

                sqlParameters = New List(Of SqlParameter)
                If _productSeriesId = 484 Or _productSeriesId = 332 Then
                    sqlGetOptions = " SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, " +
                     " f.PRODUCT_SERIES_ID, case when(SELECT count(PRODUCT_SERIES.PRODUCT_SERIES_ID) " +
                                       " FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT " +
                                       " ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN " +
                                       " CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID " +
                                       " INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 " +
                                       " ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID " +
                                       " ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID " +
                                       " WHERE PRODUCT.GROUP_ID = c.GROUP_ID and c.GROUP_ID=@GROUPID AND  PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1  AND PRODUCT_1.DISABLED = 'n' " +
                                       " AND PRODUCT_1.DISABLED = 'n')>0 and  b.CATEGORY_ID in (12) then " +
                                       "'/options/productseries.aspx?mseries=' + " +
                                       " cast(f.PRODUCT_SERIES_ID as varchar(5)) +  '&groupid=' + cast(b.group_id as varchar(5)) " +
                                       " else '/options/productdetails.aspx?modelid=' + cast(c.PRODUCTID as varchar(5))  +" +
                                       "'&categoryid=' + cast(b.category_id as varchar(5)) + '&groupid=' +cast(b.group_id as varchar(5))  end as URL " +
                              " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " +
                              " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" +
                              " INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID" +
                              " INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID" +
                              " LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS f ON " +
                              " b.CATEGORY_ID = f.CATEGORY_ID AND c.PRODUCTID = f.PRODUCT_ID " +
                              " WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID and b.group_id = @GROUPID2 AND c.disabled='n' Order by c.sort_id"
                    sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
                    sqlParameters.Add(New SqlParameter("@GROUPID2", dr("GROUP_ID").ToString()))
                Else
                    sqlGetOptions = " SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, " +
                                       " d.PRODUCT_SERIES_ID, case when(SELECT count(PRODUCT_SERIES.PRODUCT_SERIES_ID) " +
                                       " FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT " +
                                       " ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN " +
                                       " CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID " +
                                       " INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 " +
                                       " ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID " +
                                       " ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID " +
                                       " WHERE PRODUCT.GROUP_ID = c.GROUP_ID and c.GROUP_ID=@GROUPID AND  PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1  AND PRODUCT_1.DISABLED = 'n' " +
                                       " AND PRODUCT_1.DISABLED = 'n')>0 and  b.CATEGORY_ID in (12) then " +
                                       "'/options/productseries.aspx?mseries=' + " +
                                       " cast(d.PRODUCT_SERIES_ID as varchar(5)) +  '&groupid=' + cast(c.group_id as varchar(5)) " +
                                       " else '/options/productdetails.aspx?modelid=' + cast(c.PRODUCTID as varchar(5))  +" +
                                       "'&categoryid=' + cast(b.category_id as varchar(5)) + '&groupid=' +cast(b.group_id as varchar(5))  end as URL " +
                                " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " +
                                " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID " +
                                "  LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS d " +
                                " ON c.PRODUCTID = d.PRODUCT_ID AND b.CATEGORY_ID = d.CATEGORY_ID " +
                                " WHERE a.PRODUCTID = @PRODUCTID AND b.group_id = @GROUPID2 AND c.disabled='n' Order by c.sort_id"
                    sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
                    sqlParameters.Add(New SqlParameter("@GROUPID2", dr("GROUP_ID").ToString()))
                End If

                'Response.Write(sqlGetOptions + "<br><br>")
                'Response.End()
                optionDs = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetOptions, sqlParameters.ToArray())
                oNum = optionDs.Tables(0).Rows.Count
                If oNum > 0 Then
                    For j = 0 To oNum - 1
                        Dim optionDr As DataRow = optionDs.Tables(0).Rows(j)
                        optionString = optionString + "<tr>" +
                                       " <td class='cell'> " +
                                       " <strong><u>"

                        'If optionDr("INTRODUCTION").ToString = "" Then
                        'optionString = optionString + optionDr("PARTNUMBER").ToString()
                        ' Else
                        optionString = optionString + "<a href='" + optionDr("URL").ToString() + "'>" + optionDr("PARTNUMBER").ToString() + "</a>"
                        ' End If

                        optionString = optionString + "</u></strong><br /> " +
                                       Functions.GetProdDescriptionOnProdID(optionDr("productid").ToString).ToString +
                                       " </td> " +
                                       " </tr> "
                    Next
                End If
            Next
            optionString = optionString + "</table>"
            firstmenuid = 4
            tabsToShow.Append("<li><a href=""#options""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_options_off.gif' id='options' border='0'></a></li>")
        End If

        lblOptions.Text = optionString
    End Sub

    Private Sub getprobes()
        Dim probesString As String = ""
        Dim sqlGetGroup As String = ""
        Dim gNum As Integer
        Dim i As Integer

        ' First get all the distinct GROUP_NAME for various options available for the series
        'Category ID for Probes
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        If _productSeriesId = 484 Or _productSeriesId = 332 Then
            sqlGetGroup = String.Format("SELECT DISTINCT PRODUCT_SERIES_CATEGORY.CATEGORY_ID, PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME, PRODUCT_SERIES.SORTID " +
                                        "FROM PRODUCT_SERIES INNER JOIN " +
                                        "PRODUCT_SERIES_CATEGORY ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID INNER JOIN " +
                                        "CONFIG AS a INNER JOIN " +
                                        "PRODUCT AS c ON a.OPTIONID = c.PRODUCTID INNER JOIN " +
                                        "PRODUCT_SERIES_CATEGORY AS d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN " +
                                        "PRODUCT AS e ON e.PRODUCTID = d.PRODUCT_ID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = c.PRODUCTID " +
                                        "WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND c.PRODUCTID NOT IN " +
                                        "(SELECT PRODUCTID FROM PRODUCT " +
                                        "WHERE DISABLED = 'y' OR (PRODUCTID IN " +
                                        "(SELECT DISTINCT OPTIONID FROM CONFIG " +
                                        "WHERE (STD = 'y') AND (OPTIONID NOT IN " +
                                        "(SELECT OPTIONID FROM CONFIG AS CONFIG_3 " +
                                        "WHERE(STD = 'n')))))) AND (e.PRODUCTID NOT IN " +
                                        "(SELECT PRODUCTID FROM PRODUCT AS PRODUCT_1 " +
                                        "WHERE (DISABLED = 'y') OR (PRODUCTID IN " +
                                        "(SELECT DISTINCT OPTIONID FROM CONFIG AS CONFIG_2 " +
                                        "WHERE (STD = 'y') AND (OPTIONID NOT IN " +
                                        "(SELECT OPTIONID FROM CONFIG AS CONFIG_1 " +
                                        "WHERE (STD = 'n'))))))) AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = @CATEGORYID) " +
                                        "AND c.PRODUCTID NOT IN ({0}) AND e.PRODUCTID NOT IN ({0}) " +
                                        "order by PRODUCT_SERIES.SORTID", disabledProductContent.Item1)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES))
            '      sqlGetGroup = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID " + _
            '" FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " + _
            '" INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" + _
            '" INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID" + _
            '" INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID" + _
            '" WHERE d.PRODUCT_SERIES_ID = " + mseries.ToString + " " + _
            '" AND b.CATEGORY_ID in (  " + AppConstants.CAT_ID_PROBES + ") " + _
            '" AND c.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " + _
            '" AND e.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " + _
            '" ORDER BY b.SORTID"
        Else
            sqlGetGroup = String.Format("SELECT DISTINCT c.PRODUCT_SERIES_ID, c.NAME, c.SORTID " +
                          "FROM PRODUCT_SERIES_CATEGORY AS b INNER JOIN " +
                          "PRODUCT_SERIES AS c ON b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID INNER JOIN " +
                          "CONFIG AS a ON b.PRODUCT_ID = a.OPTIONID INNER JOIN PRODUCT AS p on a.OPTIONID = p.PRODUCTID " +
                          "WHERE a.PRODUCTID = @PRODUCTID AND b.CATEGORY_ID = @CATEGORYID AND p.PRODUCTID NOT IN ({0})", disabledProductContent.Item1)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES))
            'sqlGetGroup = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.sortid " + _
            '  " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " + _
            '  " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" + _
            '  " WHERE a.PRODUCTID = " + modelid + " " + _
            '  " AND b.category_id= " + AppConstants.CAT_ID_PROBES + _
            '  " AND c.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " + _
            '  " Order by b.sortid"
        End If

        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetGroup, sqlParameters.ToArray())
        gNum = ds.Tables(0).Rows.Count
        If gNum > 0 Then
            For i = 0 To gNum - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                Dim sqlGetProbes As String = ""
                Dim probesDs As DataSet
                Dim pNum As Integer
                Dim j As Integer

                probesString += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>" +
                               " <tr> " +
                               " <td class='cell'><h3>" + dr("NAME").ToString() + "</h3></td> " +
                               " </tr> "

                sqlParameters = New List(Of SqlParameter)
                disabledProductContent = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
                If _productSeriesId = 484 Or _productSeriesId = 332 Then
                    sqlGetProbes = String.Format("SELECT DISTINCT PRODUCT_SERIES_CATEGORY.CATEGORY_ID, PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME, PRODUCT_SERIES.SORTID, c.INTRODUCTION, c.PARTNUMBER, c.PRODUCTID " +
                                        "FROM PRODUCT_SERIES INNER JOIN " +
                                        "   PRODUCT_SERIES_CATEGORY ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID INNER JOIN " +
                                        "   CONFIG AS a INNER JOIN " +
                                        "   PRODUCT AS c ON a.OPTIONID = c.PRODUCTID INNER JOIN " +
                                        "   PRODUCT_SERIES_CATEGORY AS d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN " +
                                        "   PRODUCT AS e ON e.PRODUCTID = d.PRODUCT_ID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = c.PRODUCTID " +
                                        "WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = @PRODUCTSERIESID1 AND PRODUCT_SERIES_CATEGORY.CATEGORY_ID = @CATEGORYID AND c.PRODUCTID NOT IN " +
                                        "(SELECT PRODUCTID FROM PRODUCT " +
                                        "WHERE DISABLED = 'y' OR (PRODUCTID IN " +
                                        "(SELECT DISTINCT OPTIONID FROM CONFIG " +
                                        "WHERE (STD = 'y') AND (OPTIONID NOT IN " +
                                        "(SELECT OPTIONID FROM CONFIG AS CONFIG_3 " +
                                        "WHERE(STD = 'n')))))) AND (e.PRODUCTID NOT IN " +
                                        "(SELECT PRODUCTID FROM PRODUCT AS PRODUCT_1 " +
                                        "WHERE (DISABLED = 'y') OR (PRODUCTID IN " +
                                        "(SELECT DISTINCT OPTIONID FROM CONFIG AS CONFIG_2 " +
                                        "WHERE (STD = 'y') AND (OPTIONID NOT IN " +
                                        "(SELECT OPTIONID FROM CONFIG AS CONFIG_1 " +
                                        "WHERE (STD = 'n'))))))) AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = @CATEGORYID) " +
                                        "AND c.PRODUCTID NOT IN ({0}) AND e.PRODUCTID NOT IN ({0}) " +
                                        "order by c.PARTNUMBER", disabledProductContent.Item1)
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID1", dr("PRODUCT_SERIES_ID").ToString()))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES))
                    'sqlGetProbes = " SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID " + _
                    '         " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " + _
                    '         " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" + _
                    '         " INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID" + _
                    '         " INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID" + _
                    '         " WHERE d.PRODUCT_SERIES_ID = " + mseries.ToString + _
                    '         " and b.group_id = " + dr("GROUP_ID").ToString() + _
                    '         " AND c.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " + _
                    '        " Order by c.sort_id"
                Else
                    sqlGetProbes = String.Format("SELECT DISTINCT c.PRODUCT_SERIES_ID, c.NAME, c.SORTID, b.CATEGORY_ID, p.PRODUCTID, p.PARTNUMBER, p.INTRODUCTION " +
                          "FROM PRODUCT_SERIES_CATEGORY AS b INNER JOIN " +
                          "PRODUCT_SERIES AS c ON b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID INNER JOIN " +
                          "CONFIG AS a ON b.PRODUCT_ID = a.OPTIONID INNER JOIN PRODUCT AS p on a.OPTIONID = p.PRODUCTID " +
                          "WHERE a.PRODUCTID = @PRODUCTID AND b.CATEGORY_ID = @CATEGORYID AND p.PRODUCTID NOT IN ({0}) AND c.PRODUCT_SERIES_ID = @PRODUCTSERIESID", disabledProductContent.Item1)
                    'sqlGetProbes = "SELECT DISTINCT c.*, b.category_id " + _
                    '           " FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID    " + _
                    '           " INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID" + _
                    '           " WHERE a.PRODUCTID = " + modelid + " " + _
                    '           " AND b.group_id = " + dr("GROUP_ID").ToString() + _
                    '           " AND c.PRODUCTID NOT IN (" + GetDisabledProductSQL(localeid) + " ) " + _
                    '           " Order by c.sort_id"
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
                    sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROBES))
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("PRODUCT_SERIES_ID").ToString()))
                End If

                probesDs = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProbes, sqlParameters.ToArray())
                If (probesDs.Tables.Count > 0) Then
                    pNum = probesDs.Tables(0).Rows.Count
                    If pNum > 0 Then
                        For j = 0 To pNum - 1
                            Dim probesDr As DataRow = probesDs.Tables(0).Rows(j)
                            probesString = probesString + "<tr>" +
                                           " <td class='cell'> " +
                                           " <u><strong>"

                            If probesDr("INTRODUCTION").ToString = "" Then
                                probesString += probesDr("PARTNUMBER").ToString()
                            Else
                                probesString += String.Format("<a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}'>{3}</a>", probesDr("productid").ToString(), probesDr("CATEGORY_ID").ToString(), probesDr("PRODUCT_SERIES_ID").ToString(), probesDr("PARTNUMBER").ToString())
                            End If

                            probesString += "</strong></u><br /> " +
                                          Functions.GetProdDescriptionOnProdID(probesDr("productid").ToString).ToString +
                                           " </td> " +
                                           " </tr> "
                        Next
                    End If
                End If
                probesString = probesString + "</table>"
            Next
            tabsToShow.Append("<li><a href=""#probes""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_probes_off.gif' id='probes' border='0'></a></li>")
        End If
        lblProbes.Text = probesString
    End Sub

    Private Sub getProductLine()
        '*left menu for series
        Dim seriesName As String = ""
        Dim sqlGetSeries As String
        Dim ds As New DataSet
        Dim productSeriesSeo As String = String.Empty
        If _productSeriesId > 0 Then
            sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                seriesName = Me.convertToString(ds.Tables(0).Rows(0)("NAME"))
                dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
                specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
                'promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
                If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("SEOVALUE").ToString())) Then
                    productSeriesSeo = ds.Tables(0).Rows(0)("SEOVALUE").ToString()
                    lbSeries1.Text = String.Format("<a href='/oscilloscope/{0}'>{1}</a> ", ds.Tables(0).Rows(0)("SEOVALUE").ToString(), seriesName)
                Else
                    lbSeries1.Text = "<a href='oscilloscopeseries.aspx?mseries=" + _productSeriesId.ToString() + menuURL + "'> " + seriesName + " </a> "
                End If

            End If
            'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
            '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a></li>"
            'End If
            'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
            '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
            'End If

            'If Not promoImages = Nothing And Len(promoImages) > 0 Then
            '    lbPromotion.Text = promoImages
            'End If

            Dim promotionHtml As String = String.Empty
            Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
            If (countryCode >= 0) Then
                promotionHtml = Functions.GetPromoImagesForSeries(_productSeriesId, rootDir, countryCode)
            End If
            If Not (String.IsNullOrEmpty(promotionHtml)) Then
                lbPromotion.Text = promotionHtml
            Else
                lbPromotion.Text = "<div class=""greyPadding""></div>"
            End If

            '*left menu for products
            Dim sqlGetProducts As String
            Dim sqlGetQuickSpecs As String
            Dim productID As String
            Dim quickSpecsDs As New DataSet
            Dim pNum As Integer
            Dim specsNum As Integer
            Dim i As Integer
            Dim j As Integer
            Dim products As String
            Dim productLine As String
            Dim specs As String
            'If mseries.ToString = "332" Then
            '    sqlGetProducts = "SELECT DISTINCT a.*" + _
            '         " FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE " + _
            '         " a.PRODUCTID = c.PRODUCT_ID " + _
            '         " AND b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID " + _
            '         " AND c.CATEGORY_ID= " + AppConstants.CAT_ID_SCOPE + _
            '         " AND c.PRODUCT_SERIES_ID= " + mseries + _
            '         " AND a.disabled='n' and a.productid not in (6889,6890,6891) order by a.sort_id"


            'Else
            sqlParameters = New List(Of SqlParameter)
            If _productSeriesId = 484 Or _productSeriesId = 332 Then
                sqlGetProducts = "SELECT DISTINCT d.*" +
                     " FROM PRODUCT a, CONFIG b, PRODUCT_SERIES_CATEGORY c,PRODUCT d WHERE " +
                     " a.PRODUCTID = c.PRODUCT_ID  and b.OPTIONID = d.PRODUCTID" +
                     " AND a.PRODUCTID = b.PRODUCTID " +
                     " AND c.CATEGORY_ID=@CATEGORYID AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND d.GROUP_ID = 179 and d.disabled='n' and a.disabled='n' order by d.sort_id"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            ElseIf (_productSeriesId = 591) Then    ' HACK: Special for TASK0162804
                sqlGetProducts = "SELECT DISTINCT a.* FROM [PRODUCT] a, [PRODUCT_SERIES] b, [PRODUCT_SERIES_CATEGORY] c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID = 1 AND c.PRODUCT_SERIES_ID IN (591,511) AND a.DISABLED = 'n' AND a.LIM_YN = 'N' ORDER BY a.sort_id"
            Else
                sqlGetProducts = "SELECT DISTINCT a.*" +
                     " FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE " +
                     " a.PRODUCTID = c.PRODUCT_ID " +
                     " AND b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID " +
                     " AND c.CATEGORY_ID=@CATEGORYID AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND a.disabled='n' order by a.sort_id"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            End If
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
            pNum = ds.Tables(0).Rows.Count
            products = ""
            productLine = ""
            If pNum > 0 Then
                For i = 0 To pNum - 1
                    Dim modelID As String = ds.Tables(0).Rows(i)("NAME").ToString()
                    productID = ds.Tables(0).Rows(i)("PRODUCTID").ToString()
                    If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SEOVALUE").ToString())) Then
                        products = products + String.Format("<li{0}><a href='/oscilloscope/{1}/{2}'>{3}</a></li>", IIf(productID = _productId.ToString(), " class='current'", String.Empty), productSeriesSeo, ds.Tables(0).Rows(i)("SEOVALUE").ToString(), modelID)
                    Else
                        products = products + String.Format("<li{0}><a href='/oscilloscope/oscilloscopemodel.aspx?modelid={1}{2}'>{3}</a></li>", IIf(productID = _productId.ToString(), " class='current'", String.Empty), productID, menuURL, modelID)
                    End If

                    productLine = productLine + "<div class='quickBox'>"
                    If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SEOVALUE").ToString())) Then
                        productLine = productLine + String.Format("<a href='/oscilloscope/{0}/{1}'><strong>{2}</strong></a>&nbsp;&nbsp;{3}", productSeriesSeo, ds.Tables(0).Rows(i)("SEOVALUE").ToString(), modelID, ds.Tables(0).Rows(i)("SHORT_SPEC").ToString())
                    Else
                        productLine = productLine + "<a href='/oscilloscope/oscilloscopemodel.aspx?modelid=" + productID + menuURL + "'><strong>" + modelID + "</strong></a> " +
                            "&nbsp;&nbsp;" + ds.Tables(0).Rows(i)("SHORT_SPEC").ToString()
                    End If
                    If Len(ds.Tables(0).Rows(i)("TAG_LINE").ToString) > 0 Then
                        productLine = productLine + "<BR />" + ds.Tables(0).Rows(i)("TAG_LINE").ToString()
                    End If
                    If LCase(ds.Tables(0).Rows(i)("LIM_YN").ToString) = "y" Then
                        productLine = productLine + "<BR />" + "<font color=#FF0000>Limited Availability</font>"
                    End If
                    productLine = productLine + "<div class='quickSpecs'>"
                    productLine = productLine + "<div class='checkBox'><input name='chkProductID' type='checkbox' value='" + productID + "' onclick='javascript:checkProducts(this)'></div>"

                    sqlGetQuickSpecs = "SELECT DISTINCT b.PROPERTYNAME, c.VALUE, b.SORTID " +
                                       "FROM PRODUCT AS a INNER JOIN  PRODUCTPROPVALUE AS c ON a.PRODUCTID = c.PRODUCTID INNER JOIN  PROPERTY AS b ON c.PROPERTYID = b.PROPERTYID " +
                                       "WHERE c.QUICK_SPEC_IND = 'Y' AND a.PRODUCTID=@PRODUCTID ORDER by b.SORTID"
                    'Response.Write(sqlGetQuickSpecs)
                    'Response.End()
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", productID))
                    quickSpecsDs = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetQuickSpecs, sqlParameters.ToArray())
                    specsNum = quickSpecsDs.Tables(0).Rows.Count
                    specs = ""
                    If specsNum > 0 Then
                        productLine = productLine + "<div class='title'>Quick Specs</div>"
                        productLine = productLine + "<div id='specs'>"
                        productLine = productLine + "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
                        For j = 0 To specsNum - 1
                            specs = specs + "<tr>"
                            specs = specs + "<td width='35%' class='cell'>" + quickSpecsDs.Tables(0).Rows(j)("PROPERTYNAME").ToString() + "</td>"
                            specs = specs + "<td width='65%' class='cell'>" + quickSpecsDs.Tables(0).Rows(j)("VALUE").ToString() + "</td>"
                            specs = specs + "</tr>"
                        Next
                        productLine = productLine + specs
                        productLine = productLine + "</table>"
                        productLine = productLine + "</div>"
                    Else
                        productLine = productLine + "<div>&nbsp;</div>"
                    End If

                    productLine = productLine + "</div>"
                    productLine = productLine + "</div>"

                Next
                lbProduct1.Text = products
                lbQuickSpecs.Text = productLine
                tabsToShow.Append("<li><a href=""#product_line""><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
            End If
        End If

    End Sub

    Private Sub getListPrice()
        Dim pNum As Integer
        Dim j As Integer
        Dim DsList As DataSet
        Dim strListPrice As String = ""

        If Functions.hasListPrice(_productSeriesId.ToString()) Then
            strSQL = " SELECT distinct PRODUCT.PRODUCTID, PRODUCT.PARTNUMBER,PRODUCT.SORT_ID, case when PRODUCT.PRODUCTID=@PRODUCTID then 0 else 1 end as SELECTEDSORT FROM  PRODUCT_SERIES_CATEGORY INNER JOIN  PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
                     " WHERE PRODUCT.DISABLED = 'n' AND PRODUCT_SERIES_CATEGORY.CATEGORY_ID=1 AND PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = @PRODUCTSERIESID ORDER BY SELECTEDSORT,PRODUCT.SORT_ID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", _productId.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
            DsList = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            pNum = DsList.Tables(0).Rows.Count()
            If pNum > 0 Then
                strListPrice = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>"
                strListPrice += "<tr>"
                strListPrice += "<td width='96' align='left' valign='top' class='title2'>Product</td>"
                strListPrice += "<td width='61' align='left' valign='top' class='title2'>Price*</td>"
                strListPrice += "<td width='176' align='left' valign='top' class='title2'>Description</td>"
                strListPrice += "<td width='115' align='left' valign='top'>&nbsp;</td>"
                strListPrice += "</tr>"
                For j = 0 To pNum - 1
                    Dim dr As DataRow = DsList.Tables(0).Rows(j)
                    If j = 0 Then
                        strListPrice += "<tr bgcolor='#cdcdcd'>"
                        strListPrice += "<td width='96' align='left' valign='top' class='title2'>" + dr("PARTNUMBER").ToString + "</td>"
                        strListPrice += "<td width='61' align='left' valign='top' class='cell'>" + FormatCurrency(Functions.GetProductListPriceonProductID(dr("PRODUCTID")).ToString(), 2) + "</td>"
                        strListPrice += "<td width='176' align='left' valign='top' class='cell'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</td>"
                        strListPrice += "<td width='115' align='left' valign='top'><a href='/oscilloscope/configure/configure_step2.aspx?seriesid=" + _productSeriesId.ToString() + "&modelid=" + dr("PRODUCTID").ToString() + "'><img src='/images/icons/icons_link.gif' align='absmiddle' border='0' />Request Quote</a></td>"
                        ' strListPrice += "</tr><tr><td colspan='4' align='top'><hr></td></tr>"
                    Else
                        strListPrice += "<tr>"
                        strListPrice += "<td width='96' align='left' valign='top' class='title2'>" + dr("PARTNUMBER").ToString + "</td>"
                        strListPrice += "<td width='61' align='left' valign='top' class='cell'>" + FormatCurrency(Functions.GetProductListPriceonProductID(dr("PRODUCTID")).ToString(), 2) + "</td>"
                        strListPrice += "<td width='176' align='left' valign='top' class='cell'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</td>"
                        strListPrice += "<td width='115' align='left' valign='top'><a href='/oscilloscope/configure/configure_step2.aspx?seriesid=" + _productSeriesId.ToString() + "&modelid=" + dr("PRODUCTID").ToString() + "'><img src='/images/icons/icons_link.gif' align='absmiddle' border='0' />Request Quote</a></td>"
                        strListPrice += "</tr>"
                    End If
                Next
                strListPrice += "</table>"
                strListPrice += "<p class='smaller'>* Prices Shown Are US Domestic Prices, MSRP, and are exclusive of <strong>local and state taxes, shipping, levies, fees, duties, exportation/importation costs, service and warranties outside the United States</strong>, and assume standard 30 day payment terms.</p>"
                strListPrice += "<p class='smaller'>Information and/or pricing may be changed or updated without notice. Teledyne LeCroy may also make improvements and/or changes in the products and/or the programs, as well as associated pricing, described in this information at any time without notice.</p>"
                strListPrice += "<p class='smaller'>Because international information is provided at this Web Site, not all products, programs or pricing mentioned will be available in your country. Please contact your local sales representative for information as to products and services available in your country.</p>"

                tabsToShow.Append("<li><a href='#listprice'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_list_off.gif' id='listprice' border='0'></a></li>")
                Me.lblListPrice.Text = strListPrice

            End If
        End If
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _productSeriesId = xParam.ProductSeriesId

                    Dim passThroughProductSeriesId As Int32 = _productSeriesId
                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (_productSeriesId = 591) Then    ' HACK: Special for TASK0162804
                        Dim productSeriesIds As List(Of Int32) = ProductSeriesCategoryRepository.GetProductSeriesIdsBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value.ToString())
                        If (productSeriesIds.Contains(511)) Then
                            passThroughProductSeriesId = 511
                        End If
                    End If

                    If Not (param2.Key Is Nothing) Then
                        Dim yParam As Product = ProductRepository.GetProductBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), passThroughProductSeriesId, param2.Value)
                        If Not (yParam Is Nothing) Then
                            _productId = yParam.ProductId
                            CreateMeta(param1.Value, param2.Value)
                            Return
                        End If
                    End If
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue1 As String, ByVal seoValue2 As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/oscilloscope/{1}/{2}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue1, seoValue2)
                head.Controls.Add(link)
                Dim meta As New HtmlMeta()
                meta.Name = "series-id"
                meta.Content = _productSeriesId
                head.Controls.Add(meta)
                Dim meta2 As New HtmlMeta()
                meta2.Name = "model-id"
                meta2.Content = _productId
                head.Controls.Add(meta2)
            End If
        End If
    End Sub
End Class