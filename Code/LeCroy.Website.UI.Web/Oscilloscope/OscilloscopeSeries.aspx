<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Oscilloscope_OscilloscopeSeries" Codebehind="OscilloscopeSeries.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="current">
                    <asp:Literal ID="lbSeries1" runat="server" />
                    <ul><asp:Literal ID="lbProduct1" runat="server" /></ul>
                </li>
            </ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td valign="top">
                <asp:Image ID="imgSeries" runat="server" ImageAlign="right" Width="260" AlternateText="oscilloscope" />
                <div class="intro">
                    <h1><asp:Label ID="lbSeriesTileTop" runat="server" /></h1>
                    <p><asp:Label ID="lbSeriesDesc" runat="server" /></p>
                    <asp:Label ID="lbSeriesTileBelow" runat="server" CssClass="lecroyblue" />
                </div>
            </td>
        </tr>
    </table>
	<div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Label ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line">
                <asp:Label ID="lbQuickSpecs" runat="server" />
                <div class="modelCompare">
                    <asp:Literal ID="litProductSeries" runat="server" />
                    <input type="hidden" id="modelid1" name="modelid1" />
                    <input type="hidden" id="modelid2" name="modelid2" />
                    <input type="hidden" id="modelid3" name="modelid3" />
                    <input type="hidden" id="hdnCallingPage" name="hdnCallingPage" value="ScopeSeriesPage" />
                    Check up to three models.
                    <asp:Button ID="btnCompare" runat="server" CausesValidation="false" PostBackUrl="~/resources/compare/compare.aspx" Text="Compare" />
                </div>
            </div>
            <div id="overview"><asp:Label ID="lblOverview" runat="server" /></div>
            <div id="spec"><asp:Label ID="lblSpecs" runat="server" /></div>
            <div id="options"><asp:Literal ID="lblOptions" runat="server" /></div>
            <div id="probes"><asp:Literal ID="lblProbes" runat="server" /></div>
            <div id="listprice"><asp:Literal ID="lblListPrice" runat="server" /></div>
            <div id="poweranalyzer"><asp:Literal ID="litPowerAnalyzer" runat="server" /></div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />

    <script language="javascript" type="text/javascript">
        function checkProducts(obj) {
            var products = document.getElementsByName("chkProductID");
            var c = 0
            for (i = 0; i < products.length; i++) {
                if (products[i].checked) {
                    c = c + 1;
                }
            }
            jQuery('#modelid1').val('');
            jQuery('#modelid2').val('');
            jQuery('#modelid3').val('');
            if (c <= 0) {
                return;
            }
            if (c > 3) {
                jQuery('#<%= btnCompare.ClientID %>').attr('disabled', true);
                alert('Please only select 3 models');
                return;
            }
            if (c > 0) {
                var x = 0;
                jQuery('#<%= btnCompare.ClientID %>').removeAttr('disabled');
                for (i = 0; i < products.length; i++) {
                    if (x == 0 && products[i].checked) jQuery('#modelid1').val(products[i].value);
                    if (x == 1 && products[i].checked) jQuery('#modelid2').val(products[i].value);
                    if (x == 2 && products[i].checked) jQuery('#modelid3').val(products[i].value);
                    if (products[i].checked) x = x + 1;
                    if (x >= 3) break;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="lbPromotion" runat="server" />
    <asp:HyperLink ID="hlkQuote" runat="server"><img src="<%=rootDir %>/images/promos/rt_promo_configure.gif" border="0" height="65" width="203" /></asp:HyperLink>
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="1" ShowBuy="false" ShowRequestDemo="true" ShowRequestQuote="false" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>