Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class Oscilloscope_OscilloscopeSeries
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Dim seriesName As String = ""
    Dim seriesExplorerText As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim firstmenuid As Integer = 0
    Dim menuURL As String = ""
    'Dim promoImages As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Private _productSeriesId As Int32 = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscopes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HandleRoute()
        Dim captionID As String = Request.QueryString("capid")
        Dim menuID As String = Request.QueryString("mid")

        If (_productSeriesId <= 0) Then
            If Len(Request.QueryString("mseries")) = 0 Then
                Response.Redirect("default.aspx")
            Else
                If IsNumeric(Request.QueryString("mseries")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") Then
                        _productSeriesId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") + 1)
                    Else
                        _productSeriesId = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                    End If
                    If Not CheckSeriesExists(_productSeriesId, 1) Then
                        Response.Redirect("default.aspx")
                    End If

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
                    If Not (productSeries Is Nothing) Then
                        If Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                            CreateMeta(productSeries.SeoValue)
                            Response.Redirect(String.Format("~/oscilloscope/{0}", productSeries.SeoValue))
                        End If
                    End If
                Else
                    Response.Redirect("default.aspx")
                End If
            End If
        End If

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId, CategoryIds.OSCILLOSCOPES)) Then
            Response.Redirect("~/")
        End If

        captionID = AppConstants.SCOPE_CAPTION_ID

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("seriesSelected") = _productSeriesId

        If Not Page.IsPostBack Then
            ucResources.ProductSeriesId = _productSeriesId
            hlkQuote.NavigateUrl = "~/oscilloscope/configure/configure_step2.aspx?from=overview"
            getProductLine()
            getOverview()
            getSpecs()
            getOptions()
            getprobes()
            LoadSoftware()
            If Not Session("CountryCode") Is Nothing Then
                If Len(Session("CountryCode").ToString) > 0 Then
                    If Session("CountryCode").ToString = "208" Then
                        getListPrice()
                    End If
                End If
            End If
            lblTabsToShow.Text = tabsToShow.ToString()

            Functions.CreateMetaTags(Me.Header, _productSeriesId, AppConstants.SERIES_OVERVIEW_TYPE)
            '*left menu for series
            Dim sqlGetSeries As String
            Dim ds As New DataSet
            If _productSeriesId > 0 Then
                sqlGetSeries = "Select * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
                If (ds.Tables(0).Rows.Count > 0) Then
                    seriesName = Me.convertToString(ds.Tables(0).Rows(0)("NAME"))
                    dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
                    specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
                    'promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
                    seriesExplorerText = Me.convertToString(ds.Tables(0).Rows(0)("SHORT_NAME"))
                    lbSeries1.Text = seriesName
                    lbSeriesTileTop.Text = seriesName
                    lbSeriesTileBelow.Text = "Explore " + seriesExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + seriesExplorerText + "' >"
                    imgSeries.ImageUrl = String.Format("{0}{1}", rootDir, ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString())
                    imgSeries.AlternateText = seriesExplorerText
                    lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                    If Len(Functions.GetPageTitle(_productSeriesId, AppConstants.SERIES_OVERVIEW_TYPE)) > 0 Then
                        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.GetPageTitle(_productSeriesId, AppConstants.SERIES_OVERVIEW_TYPE)
                    Else
                        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + seriesName
                    End If
                End If
                'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
                '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "' onClick=""_gaq.push(['_trackPageview', '" & dataSheetURL & "']);"" >Datasheet</a></li>"
                'End If
                'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
                '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
                'End If
                'If Not promoImages = Nothing And Len(promoImages) > 0 Then
                '    lbPromotion.Text = promoImages
                'End If
                'lblRequestDemo.Text = "<a href='" + rootDir + "/oscilloscope/demo.aspx?mseries=" & mseries & "'>Request Demo</a>"
                'Additional Resources
                If Len(Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _productSeriesId.ToString).ToString) > 0 Then
                    lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _productSeriesId.ToString)
                End If

                Dim promotionHtml As String = String.Empty
                Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
                If (countryCode >= 0) Then
                    promotionHtml = Functions.GetPromoImagesForSeries(_productSeriesId, rootDir, countryCode)
                End If
                If Not (String.IsNullOrEmpty(promotionHtml)) Then
                    lbPromotion.Text = promotionHtml
                Else
                    lbPromotion.Text = "<div class=""greyPadding""></div>"
                End If

                lb_leftmenu.Text = Functions.LeftMenuScope(captionID, rootDir, pn_leftmenu, menuID, _productSeriesId, "")
                pn_leftmenu.Visible = False

                Dim ma As MasterPage = CType(Me.Master.Master, MasterPage)
                Dim head1 As HtmlHead = CType(ma.FindControl("Head1"), HtmlHead)
                Dim li As HtmlGenericControl = New HtmlGenericControl()
                li.TagName = "link"
                li.Attributes.Add("rel", "canonical")
                li.Attributes.Add("href", String.Format("{0}/oscilloscope/{1}", ConfigurationManager.AppSettings("BaseUrl"), Me.convertToString(ds.Tables(0).Rows(0)("SeoValue"))))
                head1.Controls.Add(li)
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        ' lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"

        For i = 1 To 3
            litProductSeries.Text += String.Format("<input type=""hidden"" id=""mseries{0}"" name=""mseries{0}"" value=""{1}"" />", i, _productSeriesId.ToString())
        Next
    End Sub

    Private Sub LoadSoftware()
        Dim cmsOverview As String = Functions.GetOverviewHTML("20", _productSeriesId.ToString()).ToString()
        If Not (String.IsNullOrEmpty(cmsOverview)) Then
            litPowerAnalyzer.Text = cmsOverview
            firstmenuid = 5
            tabsToShow.Append("<li><a href='#poweranalyzer'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs-poweranalyzer-off.gif' id='poweranalyzer' border='0'></a></li>")
        End If
    End Sub

    Private Sub getSpecs()
        Dim propName As String = ""
        Dim propValue As String = ""
        Dim specString As String = ""
        Dim pNum As Integer
        Dim i As Integer
        Dim ii As Integer
        strSQL = "SELECT distinct b.prop_cat_id, a.prop_cat_name,a.sort_id FROM " +
                " property_category a, PROPERTY b, SERIES_PROPERTY c WHERE " +
                " a.prop_cat_id = b.prop_cat_id" +
                " and b.PROPERTYID = c.PROPERTY_ID and c.PRODUCT_SERIES_ID=@PRODUCTSERIESID ORDER by a.SORT_ID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            specString = " " +
                " <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                specString = specString + " <tr> " +
                    " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>" +
                   " </tr> "

                strSQL = "SELECT b.PROPERTYNAME, c.PROPERTY_VALUE FROM " +
                        " PROPERTY b, SERIES_PROPERTY c WHERE " +
                        " b.PROPERTYID = c.PROPERTY_ID " +
                        " and b.prop_cat_id = @PROPCATID and c.PRODUCT_SERIES_ID=@PRODUCTSERIESID ORDER by b.SORTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id"))))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
                Try
                    Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
                    Dim country As Country = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString").ToString(), countryCode)
                    If Not (country Is Nothing) Then
                        If (country.RegionId = 2) Then  ' Exclude properties for EMEA (456=Starting from)
                            strSQL = "SELECT b.PROPERTYNAME, c.PROPERTY_VALUE FROM [PROPERTY] b, [SERIES_PROPERTY] c WHERE b.PROPERTYID = c.PROPERTY_ID AND b.prop_cat_id = @PROPCATID AND c.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND b.PROPERTYID NOT IN (456) ORDER BY b.SORTID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id"))))
                            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.ToString()))
                        End If
                    End If
                Catch
                End Try
                Dim ds2 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                pNum = ds2.Tables(0).Rows.Count
                If pNum > 0 Then
                    For ii = 0 To pNum - 1
                        propName = ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()
                        propValue = ds2.Tables(0).Rows(ii)("PROPERTY_VALUE").ToString()
                        specString = specString + " <tr> " +
                            " <td width='35%' class='cell'>" + propName + "</td>" +
                            " <td width='65%' class='cell'>" + propValue + "</td> " +
                           " </tr> "
                    Next
                    specString = specString + " <tr> " +
                        " <td colspan='2' class='cell'>&nbsp;</td>" +
                       " </tr> "
                End If
            Next
            specString = specString + " </table> "
            firstmenuid = 2
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_options_off.gif' id='spec' border='0'></a></li>")
        Else
            'Me.Menu1.Items.RemoveAt(2)
        End If
        Me.lblSpecs.Text = specString

    End Sub

    Private Sub getOverview()
        Dim strOverview As String = ""
        strOverview = Functions.GetOverviewHTML(AppConstants.SERIES_OVERVIEW_TYPE.ToString, _productSeriesId.ToString).ToString
        If Len(strOverview) > 0 Then
            lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _productSeriesId.ToString)
            Me.lblOverview.Text = strOverview
            firstmenuid = 1
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If
    End Sub

    Private Sub getProductLine()
        Dim products As String = String.Empty
        Dim productLine As String = String.Empty
        Dim specs As String
        '*left menu for series
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
        If Not (productSeries Is Nothing) Then
            lbSeries1.Text = productSeries.Name
        End If

        Dim productList As List(Of Product) = GetProductList()
        If (productList.Count > 0) Then
            For Each product In productList
                If Not (String.IsNullOrEmpty(product.SeoValue)) And Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                    products = products + String.Format("<li><a href='/oscilloscope/{0}/{1}'>{2}</a></li>", productSeries.SeoValue, product.SeoValue, product.Name)
                Else
                    products = products + String.Format("<li><a href='/oscilloscope/oscilloscopemodel.aspx?modelid={0}{1}'>{2}</a></li>", product.ProductId, menuURL, product.Name)
                End If
                productLine = productLine + "<div class='quickBox'>"

                If Not (String.IsNullOrEmpty(product.SeoValue)) And Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                    productLine = productLine + String.Format("<a href='/oscilloscope/{0}/{1}'><strong>{2}</strong></a> &nbsp;&nbsp;{3}", productSeries.SeoValue, product.SeoValue, product.Name, product.ShortSpec)
                Else
                    productLine = productLine + String.Format("<a href='/oscilloscope/oscilloscopemodel.aspx?modelid={0}{1}'><strong>{2}</strong></a> &nbsp;&nbsp;{3}", product.ProductId, menuURL, product.Name, product.ShortSpec)
                End If
                If Not (String.IsNullOrEmpty(product.TagLine)) Then
                    productLine = productLine + String.Format("<BR />{0}", product.TagLine)
                End If
                If (String.Compare(product.LimYN, "y", True) = 0) Then
                    productLine = productLine + "<BR /><font color=#FF0000>Limited Availability</font>"
                End If
                productLine = productLine + "<div class='quickSpecs'>"
                productLine = productLine + String.Format("<div class='checkBox'><input name='chkProductID' type='checkbox' value='{0}' onclick='javascript:checkProducts(this)'></div>", product.ProductId)

                Dim parameters2 As SqlParameter() = {New SqlParameter("@productID", SqlDbType.Int)}
                parameters2(0).Value = product.ProductId
                Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT c.PROPERTYNAME, b.VALUE, c.SORTID  FROM PRODUCT a INNER JOIN PRODUCTPROPVALUE b ON a.PRODUCTID = b.PRODUCTID INNER JOIN PROPERTY c ON c.PROPERTYID=b.PROPERTYID WHERE b.QUICK_SPEC_IND = 'Y'  AND a.PRODUCTID=@productid and b.VALUE>' ' ORDER BY c.SORTID", parameters2)
                specs = ""
                If (ds.Tables(0).Rows.Count > 0) Then
                    productLine = productLine + "<div class='title'>Quick Specs</div>"
                    productLine = productLine + "<div id='specs'>"
                    productLine = productLine + "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
                    For Each dr As DataRow In ds.Tables(0).Rows
                        specs = specs + "<tr>"
                        specs = specs + "<td width='35%' class='cell'>" + dr("PROPERTYNAME").ToString() + "</td>"
                        specs = specs + "<td width='65%' class='cell'>" + dr("VALUE").ToString() + "</td>"
                        specs = specs + "</tr>"
                    Next
                    productLine = productLine + specs
                    productLine = productLine + "</table>"
                    productLine = productLine + "</div>"
                Else
                    productLine = productLine + "<div>&nbsp;</div>"
                End If
                productLine = productLine + "</div>"
                productLine = productLine + "</div>"
            Next
            lbProduct1.Text = products
            lbQuickSpecs.Text = productLine
            firstmenuid = 0
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub

    Private Sub getOptions()
        Dim optionString As String = ""
        Dim sqlString As String = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID FROM CONFIG a INNER JOIN PRODUCT c On a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b On c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d On a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e On e.PRODUCTID = d.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @mseries And b.CATEGORY_ID In ( 11 , 12 , 18 , 23,28 , 31, 34) And c.disabled='n' AND e.disabled='n' ORDER BY b.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@mseries", _productSeriesId.ToString()))
        Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (productGroups.Count > 0) Then
            For Each pg In productGroups
                optionString += String.Format("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='cell'><h3>{0}</h3></td></tr> ", pg.GroupName)
                Dim parameters1 As SqlParameter() = {New SqlParameter("@mseries", SqlDbType.Int), New SqlParameter("@groupid", SqlDbType.Int)}
                parameters1(0).Value = _productSeriesId.ToString()
                parameters1(1).Value = pg.GroupId.ToString()
                Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, f.PRODUCT_SERIES_ID, case when(SELECT count(PRODUCT_SERIES.PRODUCT_SERIES_ID) FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID WHERE PRODUCT.GROUP_ID = c.GROUP_ID and c.GROUP_ID=@groupid AND PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1 AND PRODUCT_1.DISABLED = 'n' AND PRODUCT_1.DISABLED = 'n')>0 and b.CATEGORY_ID in (12) then '/options/productseries.aspx?mseries=' + cast(f.PRODUCT_SERIES_ID as varchar(5)) + '&groupid=' + cast(b.group_id as varchar(5)) else '/options/productdetails.aspx?modelid=' + cast(c.PRODUCTID as varchar(5)) +'&categoryid=' + cast(b.category_id as varchar(5)) + '&groupid=' +cast(b.group_id as varchar(5)) end as URL FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS f ON b.CATEGORY_ID = f.CATEGORY_ID AND c.PRODUCTID = f.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @mseries and b.group_id = @groupid AND c.disabled='n' Order by c.sort_id", parameters1.ToArray())
                If (ds.Tables(0).Rows.Count > 0) Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        optionString = optionString + String.Format("<tr><td class='cell'><strong><u><a href='{0}'>{1}</a></u></strong><br />{2}</td></tr> ", dr("URL").ToString(), dr("PARTNUMBER").ToString(), Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString)
                    Next
                End If
            Next
            optionString = optionString + "</table>"
            firstmenuid = 4
            tabsToShow.Append("<li><a href=""#options""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_options_off.gif' id='options' border='0'></a></li>")
        End If
        lblOptions.Text = optionString
    End Sub

    Private Sub getprobes()
        Dim probesString As String = ""
        ' First get all the distinct GROUP_NAME for various options available for the series
        'Category ID for Probes
        Dim parameters As SqlParameter() = {New SqlParameter("@mseries", SqlDbType.Int)}
        parameters(0).Value = _productSeriesId.ToString()
        Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @mseries AND b.CATEGORY_ID in ( 3) AND c.disabled='n' AND e.disabled='n' ORDER BY b.SORTID", parameters.ToArray())
        If (productGroups.Count > 0) Then
            For Each pg In productGroups
                probesString += String.Format("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='cell'><h3>{0}</h3></td></tr> ", pg.GroupName)
                Dim parameters1 As SqlParameter() = {New SqlParameter("@mseries", SqlDbType.Int), New SqlParameter("@groupid", SqlDbType.Int)}
                parameters1(0).Value = _productSeriesId.ToString()
                parameters1(1).Value = pg.GroupId.ToString()
                Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, f.PRODUCT_SERIES_ID, case when(SELECT count(PRODUCT_SERIES.PRODUCT_SERIES_ID) FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID WHERE PRODUCT.GROUP_ID = c.GROUP_ID and c.GROUP_ID=@groupid AND PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1 AND PRODUCT_1.DISABLED = 'n' AND PRODUCT_1.DISABLED = 'n')>0 and b.CATEGORY_ID in (12) then '/options/productseries.aspx?mseries=' + cast(f.PRODUCT_SERIES_ID as varchar(5)) + '&groupid=' + cast(b.group_id as varchar(5)) else '/options/productdetails.aspx?modelid=' + cast(c.PRODUCTID as varchar(5)) +'&categoryid=' + cast(b.category_id as varchar(5)) + '&groupid=' +cast(b.group_id as varchar(5)) end as URL FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS f ON b.CATEGORY_ID = f.CATEGORY_ID AND c.PRODUCTID = f.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @mseries and b.group_id = @groupid AND c.disabled='n' Order by c.sort_id", parameters1.ToArray())
                If (ds.Tables(0).Rows.Count > 0) Then
                    For Each probesDr As DataRow In ds.Tables(0).Rows
                        probesString = probesString + "<tr><td class='cell'><u><strong>"
                        If probesDr("INTRODUCTION").ToString = "" Then
                            probesString += probesDr("PARTNUMBER").ToString()
                        Else
                            probesString += String.Format("<a href='../probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}'>{3}</a>", probesDr("productid").ToString(), probesDr("CATEGORY_ID").ToString(), probesDr("PRODUCT_SERIES_ID").ToString(), probesDr("PARTNUMBER").ToString())
                        End If
                        probesString += String.Format("</strong></u><br />{0}</td></tr> ", Functions.GetProdDescriptionOnProdID(probesDr("productid").ToString).ToString)
                    Next
                End If
                probesString = probesString + "</table>"
            Next
            tabsToShow.Append("<li><a href=""#probes""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_probes_off.gif' id='probes' border='0'></a></li>")
        End If
        lblProbes.Text = probesString
    End Sub

    Private Sub getListPrice()
        Dim DsList As New DataTable
        Dim strListPrice As String = ""

        If Functions.hasListPrice(_productSeriesId.ToString()) Then
            Dim productList As List(Of Product) = GetProductList()
            If productList.Count > 0 Then
                strListPrice = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>"
                strListPrice += "<tr>"
                strListPrice += "<td width='96' align='left' valign='top' class='title2'>Product</td>"
                strListPrice += "<td width='61' align='left' valign='top' class='title2'>Price*</td>"
                strListPrice += "<td width='176' align='left' valign='top' class='title2'>Description</td>"
                strListPrice += "<td width='115' align='left' valign='top'></td>"
                strListPrice += "</tr>"
                For Each p In productList
                    strListPrice += "<tr>"
                    strListPrice += String.Format("<td width='96' align='left' valign='top' class='title2'>{0}</td>", p.PartNumber)
                    strListPrice += String.Format("<td width='61' align='left' valign='top' class='cell'>{0}</td>", FormatCurrency(Functions.GetProductListPriceonProductID(p.ProductId).ToString(), 2))
                    strListPrice += String.Format("<td width='176' align='left' valign='top' class='cell'>{0}</td>", Functions.GetProdDescriptionOnProdID(p.ProductId.ToString()).ToString())
                    strListPrice += String.Format("<td width='115' align='left' valign='top'><a href='/oscilloscope/configure/configure_step2.aspx?seriesid={0}&modelid={1}'><img src='/images/icons/icons_link.gif' align='absmiddle' border='0' />Request Quote</a></td>", _productSeriesId, p.ProductId)
                    strListPrice += "</tr>"
                Next
                strListPrice += "</table>"
                strListPrice += "<p class='smaller'>* Prices Shown Are US Domestic Prices, MSRP, and are exclusive of <strong>local and state taxes, shipping, levies, fees, duties, exportation/importation costs, service and warranties outside the United States</strong>, and assume standard 30 day payment terms.</p>"
                strListPrice += "<p class='smaller'>Information and/or pricing may be changed or updated without notice. Teledyne LeCroy may also make improvements and/or changes in the products and/or the programs, as well as associated pricing, described in this information at any time without notice.</p>"
                strListPrice += "<p class='smaller'>Because international information is provided at this Web Site, not all products, programs or pricing mentioned will be available in your country. Please contact your local sales representative for information as to products and services available in your country.</p>"

                tabsToShow.Append("<li><a href='#listprice'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_list_off.gif' id='listprice' border='0'></a></li>")
                Me.lblListPrice.Text = strListPrice
            End If
        End If
    End Sub

    Private Function GetProductList() As List(Of Product)
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@mseries", _productSeriesId.ToString()))
        If (_productSeriesId = 484 Or _productSeriesId = 332) Then
            sqlString = "SELECT DISTINCT d.* FROM PRODUCT a, CONFIG b, PRODUCT_SERIES_CATEGORY c,PRODUCT d WHERE  a.PRODUCTID = c.PRODUCT_ID  and b.OPTIONID = d.PRODUCTID  AND a.PRODUCTID = b.PRODUCTID  AND c.CATEGORY_ID= 1 AND c.PRODUCT_SERIES_ID= @mseries  AND d.GROUP_ID = 179 and d.disabled='n' and a.disabled='n' order by d.sort_id"
        ElseIf (_productSeriesId = 591) Then    ' HACK: Special for TASK0162804
            sqlString = "SELECT DISTINCT a.* FROM [PRODUCT] a, [PRODUCT_SERIES] b, [PRODUCT_SERIES_CATEGORY] c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID = 1 AND c.PRODUCT_SERIES_ID IN (591,511) AND a.DISABLED = 'n' AND a.LIM_YN = 'N' ORDER BY a.sort_id"
        Else
            sqlString = "Select DISTINCT a.* FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE  a.PRODUCTID = c.PRODUCT_ID  And b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID  And c.CATEGORY_ID= 1 And c.PRODUCT_SERIES_ID=@mseries  And a.disabled='n' order by a.sort_id"
        End If
        Return ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    End Function

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _productSeriesId = xParam.ProductSeriesId
                    CreateMeta(param1.Value)
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/oscilloscope/{1}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue)
                head.Controls.Add(link)
                Dim meta As New HtmlMeta()
                meta.Name = "series-id"
                meta.Content = _productSeriesId
                head.Controls.Add(meta)
            End If
        End If
    End Sub
End Class