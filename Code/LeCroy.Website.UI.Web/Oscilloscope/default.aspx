﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false"  MaintainScrollPositionOnPostback="true" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
     <script type="text/javascript" src="../../js/isotopeFilter.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
    window.onbeforeunload = ResetForm;
    jQuery(document).ready(function ($) {
        $('.seriesCheckBox').change(function () {
            var checked = $('.seriesCheckBox input:checked').length
            if (checked <= 0) {
                ResetForm();
                return;
            }
            if (checked > 0) {
                var n = $('.seriesCheckBox input:not(:checked)');
                n.each(function () {
                    n.removeAttr('disabled');
                    $('#btnCompare-' + $(this).val()).hide();
                });
            }
            if (checked >= 3) {
                var n = $('.seriesCheckBox input:not(:checked)');
                n.each(function () {
                    n.attr('disabled', true);
                });
            }
            if (checked > 0) SetSelected();
        });
    });

    function ResetForm() {
        var n = jQuery(':checkbox');
        n.each(function () {
            n.removeAttr('checked');
            n.removeAttr('disabled');
            jQuery('#btnCompare-' + jQuery(this).val()).hide();
        });
    }

    function SetSelected() {
        jQuery('#mseries1').val('');
        jQuery('#mseries2').val('');
        jQuery('#mseries3').val('');
        var n = jQuery('.seriesCheckBox input:checked');
        var x = 0;
        n.each(function () {
            if (x == 0) {
                jQuery('#mseries1').val(jQuery(this).val());
                jQuery('#btnCompare-' + jQuery(this).val()).show();
            }
            if (x == 1) {
                jQuery('#mseries2').val(jQuery(this).val());
                jQuery('#btnCompare-' + jQuery(this).val()).show();
            }
            if (x == 2) {
                jQuery('#mseries3').val(jQuery(this).val());
                jQuery('#btnCompare-' + jQuery(this).val()).show();
            }
            x = x + 1;
        });
    }

    function CanSubmit() {
        console.log('canSubmit');
        if (jQuery('#mseries1').val().length > 0 || jQuery('#mseries2').val().length > 0 || jQuery('#mseries3').val().length > 0) {
            return true;
        }
        return false;
    }
</script>
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
<div class="bg-video-holder">
            <video class="bg-video" width="640" height="360" loop="" autoplay="" muted="" playsinline="">
                <source type="video/mp4" src="https://assets.lcry.net/images/oscilloscopes/video-osc.mp4">
            </video>
            <div class="bg-overlay"></div>
            <div class="banner">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-sm-10 col-md-11 col-lg px-sm-0 text-center">
                            <h1>Oscilloscopes </h1>
                            <div class="row justify-content-center big m-auto"> 
                                <div class="col-md-12 col-sm">
                                    <p class="mb-0 bold">High resolution oscilloscopes  |  12 bits all the time  |  Capture every detail  |  Powerful deep toolbox</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section-izotope section-white section-angle oscilloscopes">
            <div class="container"> 
                <div class="izotope-sub-filters">
                    <div class="acc-pci tabs-pci izotope-filters"> 
                        <div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-2">Bandwidth</a></div>
                        <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3">Resolution</a></div>
                        <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4">Channels</a></div>
                        <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5">Memory Length</a></div>
                        <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6">Sample Rate</a></div>
                        <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7">Analysis Toolbox</a></div>
                    </div>
                    <div class="tab-holder" id="filters">
                        <div class="slide-content" id="tab-pci-2">
                            <div class="button-group sub-filters">
                                <label>Bandwidth:</label>
                                <span class="button is-checked" data-filter=".b-all">All</span>
                                <span class="button" data-filter=".1GHz"><1 GHz</span>
                                <span class="button" data-filter=".1to2GHz">1 to 2 GHz</span>
                                <span class="button" data-filter=".2-5to8GHz">2.5 to 8 GHz</span>
                                <span class="button" data-filter=".13to25GHz">13 to 30 GHz</span>
                                <span class="button" data-filter=".25GHz">>30 GHz</span>
                            </div>
                        </div>
                        <div class="slide-content" id="tab-pci-3">
                            <div class="button-group sub-filters">
                                <label>Resolution:</label>
                                <span class="button is-checked" data-filter=".r-all">All</span>
                                <span class="button" data-filter=".8bits">8 bit</span>
                                <span class="button" data-filter=".12bits">12 bit</span>
                            </div>
                        </div>
                        <div class="slide-content" id="tab-pci-4">
                            <div class="button-group sub-filters">
                                <label>Channels:</label>
                                <span class="button is-checked" data-filter=".c-all">All</span>
                                <span class="button" data-filter=".2cha">2</span>
                                <span class="button" data-filter=".4cha">4</span>
                                <span class="button" data-filter=".8cha">8</span>
                                <span class="button" data-filter=".16cha">16</span>
                            </div>
                        </div>
                        <div class="slide-content" id="tab-pci-5">
                            <div class="button-group sub-filters">
                                <label>Memory Length:</label>
                                <span class="button is-checked" data-filter=".m-all">All</span>
                                <span class="button" data-filter=".10-50Mpts">10 to 50 Mpts</span>
                                <span class="button" data-filter=".50-250Mpts">50 to 250 Mpts</span>
                                <span class="button" data-filter=".250-Mpts-Gpt">250 Mpts to 1 Gpt</span>
                                <span class="button" data-filter=".GptGpts">1 Gpt to 5 Gpts</span>
                            </div>
                        </div>
                        <div class="slide-content" id="tab-pci-6">
                            <div class="button-group sub-filters">
                                <label>Sample Rate:</label>
                                <span class="button is-checked" data-filter=".s-all">All</span>
                                <span class="button" data-filter=".25-s">≤2.5 GS/s</span>
                                <span class="button" data-filter=".5GS-s"><5 GS/s</span>
                                <span class="button" data-filter=".GS-s">≥5 GS/s </span>
                                <span class="button" data-filter=".10GS-s">≥10 GS/s</span>
                                <span class="button" data-filter=".20GS-s">≥20 GS/s</span>
                                <span class="button" data-filter=".40GS-s">≥40 GS/s</span>
                                <span class="button" data-filter=".80GS-s">≥80 GS/s</span>
                                <span class="button" data-filter=".100GS-s">≥100 GS/s</span>
                            </div>
                        </div>
                        <div class="slide-content" id="tab-pci-7">
                            <div class="button-group sub-filters">
                                <label>Analysis Toolbox:</label>
                                <span class="button is-checked" data-filter=".a-all">All</span>
                                <span class="button" data-filter=".Basic">Basic</span>
                                <span class="button" data-filter=".Advanced">Advanced</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid small height">
                    

                    
                    <div class="col-xl-3 col-md-6 element-item transition 2-5to8GHz b-all r-all c-all m-all s-all a-all 10GS-s 12bits 4cha 10-50Mpts GS-s 50-250Mpts 250-Mpts-Gpt GptGpts 20GS-s Advanced" data-category="1GHz">                                                        
                        <div class="card-line p-0 register" id="SiteContent_CenterColumn_rptProduct_trRowData_10">
                            <a href="/waveprohd/">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-2.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> 
                                        <a href="/waveprohd/">WavePro HD</a>
                                    </h4>
                                    <div class="card-text green"> 
                                        <p>High Definition Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>2.5 GHz - 8 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels        </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_10" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl11$cbxSeriesId" value="566">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-566" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl11$btnCompare-566&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition b-all r-all c-all m-all s-all a-all 1GHz 1to2GHz 12bits 8cha GS-s 16cha 10-50Mpts 50-250Mpts 250-Mpts-Gpt GptGpts 10GS-s Advanced" data-category="1GHz">                                                           
                        <div class="card-line p-0 register">
                            <a href="/wr8000hd/">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-3.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/wr8000hd/">WaveRunner 8000HD</a></h4>
                                    <div class="card-text green">
                                        <p>High Definition Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span> 350 MHz - 2 GHz</span>bandwidth</li>
                                            <li><span>8</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_8" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl09$cbxSeriesId" value="607">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-607" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl09$btnCompare-607&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 12bits 8cha 16cha 10-50Mpts GS-s 50-250Mpts 250-Mpts-Gpt GptGpts 10GS-s Advanced" data-category="1GHz">                                                     
                        <div class="card-line p-0 register">
                            <a href="/static-dynamic-complete/">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-4.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/static-dynamic-complete/">MDA 8000HD</a></h4>
                                    <div class="card-text green">
                                        <p>Motor Drive Analyzers</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>350 MHz - 2 GHz</span>bandwidth</li>
                                            <li><span>8</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_9" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl10$cbxSeriesId" value="606">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-606" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl10$btnCompare-606&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 12bits GS-s 4cha 10-50Mpts 50-250Mpts 250-Mpts-Gpt 10GS-s Advanced" data-category="1GHz">                                                        
                        <div class="card-line p-0 register">
                            <a href="hdo6000.aspx">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/hdo6000b.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="hdo6000.aspx">HDO6000B</a></h4>
                                    <div class="card-text green">
                                        <p>High Definiton Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>350 MHz - 1 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_7" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl08$cbxSeriesId" value="647">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-647" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl08$btnCompare-551&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all 10GS-s m-all s-all a-all 1to2GHz 2-5to8GHz GS-s 8bits 4cha 10-50Mpts 50-250Mpts 20GS-s 40GS-s Advanced" data-category="1GHz">                                                     
                        <div class="card-line p-0 register">
                            <a href="/wr9000/">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-6.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/wr9000/">WaveRunner 9000</a></h4>
                                    <div class="card-text">
                                        <p>Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>500 MHz - 4 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_6" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl07$cbxSeriesId" value="591">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-591" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl07$btnCompare-591&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 2-5to8GHz b-all r-all 10GS-s 40GS-s 20GS-s c-all m-all s-all a-all 13to25GHz GS-s 8bits 4cha 10-50Mpts 50-250Mpts 250-Mpts-Gpt 80GS-s Advanced" data-category="1GHz">                                                      
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-7.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">WaveMaster/SDA 8 Zi-B</a></h4>
                                    <div class="card-text">
                                        <p>Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>4 GHz - 30 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_11" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl12$cbxSeriesId" value="480">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-480" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl12$btnCompare-480&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display: none;">Compare</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="col-xl-3 col-md-6 element-item transition b-all r-all c-all s-all a-all 13to25GHz 40GS-s 10GS-s 250-Mpts-Gpt m-all 10-50Mpts 50-250Mpts 250-Mpts-Gpt 50-250Mpts 10-50Mpts GS-s 20GS-s 25GHz 8bits 4cha 8cha 16cha 80GS-s 100GS-s Advanced" (data-category='1GHz')>                                                      
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-8.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
                                    <div class="card-text">
                                        <p>Modular Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>20 GHz - 65 GHz</span>bandwidth</li>
                                            <li><span>4 to 80</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_12" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl13$cbxSeriesId" value="484">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-484" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl13$btnCompare-484&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 12bits 4cha 10-50Mpts 5GS-s Basic" data-category="1GHz">                                                  
                        <div class="card-line p-0 register">
                            <a href="/ws4000hd/">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-9.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/ws4000hd/">WaveSurfer 4000HD</a></h4>
                                    <div class="card-text green">
                                        <p>High Definition Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>200 MHz - 1 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_3" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl04$cbxSeriesId" value="609">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-609" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl04$btnCompare-609&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 12bits GS-s 4cha 10-50Mpts 50-250Mpts 250-Mpts-Gpt 10GS-s Advanced" data-category="1GHz">                                                        
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/hdo6000a-high-definition-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-5.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/hdo6000a-high-definition-oscilloscopes">HDO6000A</a></h4>
                                    <div class="card-text green">
                                        <p>High Definiton Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>350 MHz - 1 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_7" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl08$cbxSeriesId" value="551">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-551" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl08$btnCompare-551&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 12bits GS-s 4cha 10-50Mpts 50-250Mpts 10GS-s Basic" data-category="1GHz">                                                                  
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/hdo4000a-high-definition-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-10.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/hdo4000a-high-definition-oscilloscopes">HDO4000A</a></h4>
                                    <div class="card-text green">
                                        <p>High Definition Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span class="green">12-bit</span>resolution</li>
                                            <li><span>200 MHz - 1 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_5" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl06$cbxSeriesId" value="550">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-550" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl06$btnCompare-550&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                    <div class="card-footer-img"><a href="/hdo/"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 1to2GHz 8bits 4cha 10-50Mpts 5GS-s Basic" data-category="1GHz">                                                   
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/wavesurfer-3000z-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-11.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/wavesurfer-3000z-oscilloscopes">WaveSurfer 3000z</a></h4>
                                    <div class="card-text">
                                        <p>Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>100 MHz - 1 GHz</span>bandwidth</li>
                                            <li><span>4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_2" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl03$cbxSeriesId" value="565">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-565" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl03$btnCompare-565&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 8bits 2cha 4cha 10-50Mpts 50-250Mpts 25-s Basic" data-category="1GHz">                                          
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/t3dso2000-series-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-12.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/t3dso2000-series-oscilloscopes">T3DSO2000/T3DSO2000A</a></h4>
                                    <div class="card-text">
                                        <p>Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>100 MHz - 500 MHz</span>bandwidth</li>
                                            <li><span>2, 4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_1" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl02$cbxSeriesId" value="572">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-572" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl02$btnCompare-572&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 element-item transition 1GHz b-all r-all c-all m-all s-all a-all 8bits 2cha 4cha 10-50Mpts 25-s Basic" data-category="1GHz">                                                   
                        <div class="card-line p-0 register">
                            <a href="/oscilloscope/t3dso1000-series-oscilloscopes">
                                <div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/product-13.png"></span></div>
                            </a>
                            <div class="inner-content">
                                <div class="inner-info">
                                    <h4 class="title"> <a href="/oscilloscope/t3dso1000-series-oscilloscopes">T3DSO1000/1000A</a></h4>
                                    <div class="card-text">
                                        <p>Oscilloscopes</p>
                                        <ul class="list-characteristics">
                                            <li><span>8-bit</span>resolution</li>
                                            <li><span>100 MHz - 350 MHz</span>bandwidth</li>
                                            <li><span>2, 4</span>channels       </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-bottom d-flex justify-space">
                                    <div class="form-row m-0"> 
                                        <label class="checkbox-label d-flex">

                                            <span class="seriesCheckBox">
                                                <input id="SiteContent_CenterColumn_rptProduct_cbxSeriesId_0" type="checkbox" name="ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl01$cbxSeriesId" value="571">
                                                <i class="checkbox-icon"></i>
                                                <span class="text">Compare </span>
                                            </span><br>
                                            <a onclick="return CanSubmit();" id="btnCompare-571" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$SiteContent$CenterColumn$rptProduct$ctl01$btnCompare-571&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../resources/compare/compare.aspx&quot;, false, true))" style="display:none;">Compare</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<section class="section-gray angle-small new-footer section-probes">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Oscilloscope Probes and Accessories</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-11 col-md-11 col-lg-10 col-xl-11">
									<p>Select from a large variety of probes and accessories to customize your oscilloscope to your specific applications.</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4>Differential  </h4>
											</div>
											<div class="card-text mb-3">
												<p>From 200 MHz to 30 GHz</p>
											</div>
											<div class="card-link"> 
												<ul>
													<li><a href="/probes/differential-probes-1500-mhz">≤ 1.5 GHz</a></li>
													<li><a href="/probes/differential-probes-4-6-ghz">4-6 GHz</a></li>
													<li><a href="/probes/dh-series-differential-probes">8-30 GHz</a></li>
												</ul>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-1.png"></span></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4> <a href="/probes/60v-common-mode-differential-probes ">60 V Common-mode Differential</a></h4>
											</div>
											<div class="card-text">
												<p>Up to 1 GHz bandwidth and 60 V common-mode rating</p>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-2.png"></span></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4> <a href="/probes/high-voltage-differential-probes">HV Differential</a></h4>
											</div>
											<div class="card-text">
												<p>Up to 6 kV, 400 MHz, ≤ 1% accuracy and exceptional CMRR</p>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-3.png">    </span></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4> <a href="/probes/current-probes">Current</a></h4>
											</div>
											<div class="card-text">
												<p>Up to 700A and sensitivities to 1 mA/div</p>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-4.png"></span></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4> <a href="/probes/active-voltage-probes">Active, Single-ended</a></h4>
											</div>
											<div class="card-text">
												<p>Up to 4 GHz, low input capacitance</p>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-5.png"></span></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 small-2">  
									<div class="card-line horizontale d-flex">
										<div class="inner-content">
											<div class="card-title mb-8">
												<h4> <a href="/probes/high-voltage-fiber-optically-isolated-probes"> HV Fiber-optic Isolated</a></h4>
											</div>
											<div class="card-text">
												<p>150 MHz, low-loading, exceptional CMRR</p>
											</div>
										</div>
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/probes-6.png">            </span></div>
									</div>
								</div>
							</div>
						</div>
						<div class="holder-btn-more"><a class="btn btn-default" href="/probes/">See all Oscilloscope Probes</a></div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-lg-12 mb-32">
									<div class="card-line p-0">
										<div class="inner-content grey-light green-text">
											<div class="row w-100 mb-0">
												<div class="col-md-6 m-auto">
													<h2 class="title text-center">12 bits <b>all the time                                      </b></h2>
													<div class="card-text">
														<p class="text-center">High Definition Oscilloscopes (HDO) use<b> HD4096 </b>Technology to provide 12 bits of resolution all the time from 200 MHz up to 8 GHz.</p>
														<ul class="list-check mt-4 green">
															<li><i class="icon-chek"></i><span>No tradeoff of resolution, sample rate, bandwidth</span></li>
															<li><i class="icon-chek"></i><span>Clean, crisp waveforms</span></li>
															<li><i class="icon-chek"></i><span>More signal details    </span></li>
															<li><i class="icon-chek"></i><span>Unmatched measurement precision</span></li>
														</ul>
													</div>
													<div class="footer-card text-center">                                            <a class="link-arrow" href="hdo.aspx"><span>Explore High Definition Oscilloscopes</span><i class="icon-btn-right-01"></i></a></div>
												</div>
											</div>
											<div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/hdo12bit-family.png"></span></div>
											<div class="d-flex bottom-block m-auto">
												<div class="bottom-block-img mr-3"><img src="https://assets.lcry.net/images/oscilloscopes/bottom-icon.jpg" alt="img"></div>
												<div class="bottom-block-text">
													   High Definition <br>
													Oscilloscopes    
												</div>
											</div>											
										</div>
									</div>
								</div>
							</div>
							<div class="row">                   
								<div class="col-lg-12">
									<div class="card-line p-0">
										<div class="inner-content grey-light blue-text pb-0">
											<div class="row w-100 mb-32">
												<div class="col-md-6 m-auto">
													<h2 class="title text-center"> <b>MAUI Studio </b>– Works Where You Are                               </h2>
													<div class="card-text">
														<p class="text-center">Unleash the power of a Teledyne LeCroy oscilloscope anywhere using a PC with MAUI Studio Pro. Work remotely from your oscilloscope and be more productive. Download and register <a href="/doc/maui-studio-software">here</a>.</p>
														<ul class="list-check mt-4">
															<li><i class="icon-chek"></i><span> <b>  Flexibility</b> – work remotely, simulate lab setups, portable software options</span></li>
															<li><i class="icon-chek"></i><span> <b>  Productivity </b>  – easily share data, better collaboration, move faster</span></li>
															<li><i class="icon-chek"></i><span> <b>  Power</b>   – unbelievable analytical capability at your fingertips</span></li>
														</ul>
													</div>
													<div class="footer-card text-center">                                            <a class="link-arrow" href="/mauistudio/"><span>Explore MAUI Studio</span><i class="icon-btn-right-01"></i></a></div>
												</div>
											</div>
											<div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/oscilloscopes/oscilloscopes-img-5-3.png"></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle-before section-gray angle-small section-visibility">
					<div class="container">
						<div class="row">
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
								<div class="card-line">
									<div class="inner-content h-auto">     
										<div class="card-title mb-3">                                         
											<h4 class="text-center">Complete PCI Express® Electrical Test Solutions                                  </h4>
										</div>
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/oscilloscopes/oscilloscopes-img-6.jpg" alt="img-description"></div>
									</div>
									<div class="inner-visible text-center pb-40">                                          <a class="link-arrow" href="/pcie-electrical-test/"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
								<div class="card-line">
									<div class="inner-content h-auto">     
										<div class="card-title mb-3">                                         
											<h4 class="text-center">Static, Dynamic, Complete Power Analysis                                     </h4>
										</div>
										<div class="card-images text-center">                                        <img src="https://assets.lcry.net/images/oscilloscopes/oscilloscopes-img-7.jpg" alt="img-description"></div>
									</div>
									<div class="inner-visible text-center pb-40">                                                  <a class="link-arrow" href="/static-dynamic-complete/"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">        
								<div class="card-line">
									<div class="inner-content h-auto">  
										<div class="card-title mb-3">                                         
											<h4 class="text-center">USB and USB Type-C® Electrical Test Solutions                                       </h4>
										</div>
										<div class="card-images text-center">                                        <img src="https://assets.lcry.net/images/oscilloscopes/oscilloscopes-img-8.2.png" alt="img-description"></div>
									</div>
									<div class="inner-visible text-center pb-40">                                                     <a class="link-arrow" href="/usb-electrical-test/"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions section-angle new-footer-3"> 
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12">
								<div class="section-heading text-center mx-auto">
									<h2 class="title px-0 mx-md-0">More Learning Opportunities</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title mb-3">                                         
											<h3>Live Events </h3>
										</div>
										<div class="card-text">
											<p>Attend live webinars or events hosted by industry and application experts to gain expertise and learn how to solve problems faster.</p>
										</div>
										<div class="footer-card">                                            <a class="link-arrow" href="/events/"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title mb-3">                                         
											<h3>On-demand Webinars</h3>
										</div>
										<div class="card-text">
											<p>Access our archive of on-demand webinars to get up-to-speed quickly on the topics that matter to you most.</p>
										</div>
										<div class="footer-card">                                            <a class="link-arrow" href="/support/techlib/webcasts.aspx"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title mb-3">                                         
											<h3>Application Notes</h3>
										</div>
										<div class="card-text">
											<p>Access our archive of application notes to further your understanding of how to apply Teledyne LeCroy oscilloscopes, protocol analyzers and other test tools to your unique challenges.</p>
										</div>
										<div class="footer-card">                                             <a class="link-arrow" href="/support/techlib/library.aspx?type=1"><span>Explore </span><i class="icon-btn-right-01"></i></a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
        <!-- div for compare -->
        <input type="hidden" id="mseries1" name="mseries1" />
        <input type="hidden" id="mseries2" name="mseries2" />
        <input type="hidden" id="mseries3" name="mseries3" />
        <input type="hidden" id="hdnCallingPage" name="hdnCallingPage" value="ScopeIndexPage" />
    </main>  
</asp:Content>