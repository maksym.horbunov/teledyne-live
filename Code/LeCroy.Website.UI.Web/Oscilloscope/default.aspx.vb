﻿Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Partial Class Oscilloscope_Oscilloscope
    Inherits BasePage

#Region "Variables/Keys"
    Private _propertyIds As SortedList(Of Int32, Int32) = New SortedList(Of Int32, Int32)
    Private _rowCount As Int32 = 0
    Private _seriesProperties As List(Of SeriesProperty) = New List(Of SeriesProperty)
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Master.MetaOgTitle = "Oscilloscopes from Teledyne LeCroy"
        Master.MetaOgImage = "https://teledynelecroy.com/images/og-oscilloscopes.jpg"
        Master.MetaOgDescription = "Our oscilloscopes offer a powerful combination of large and informative displays combined with advanced waveshape analysis capabilities - typically tailored to enhance the productivity of engineers in specific applications areas such as serial data test, disk drive test and automotive bus analysis."
        Master.MetaOgUrl = "https://teledynelecroy.com/oscilloscope/"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope"
            BindSeries(False, Nothing)
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptProduct_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) 'Handles rptProduct.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header
                Dim ddlBandwidths As DropDownList = CType(e.Item.FindControl("ddlBandwidths"), DropDownList)
                Dim trHeaders As HtmlTableRow = CType(e.Item.FindControl("trHeaders"), HtmlTableRow)
                Dim tdSpacer As HtmlTableCell = CType(e.Item.FindControl("tdSpacer"), HtmlTableCell)

                SetDynamicColumnHeaders(trHeaders)
                BindBandwidthData(ddlBandwidths)
                tdSpacer.ColSpan = (1 + _propertyIds.Count)
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ProductSeries = CType(e.Item.DataItem, ProductSeries)
                Dim imgProduct As Image = CType(e.Item.FindControl("imgProduct"), Image)
                Dim hlkProduct As HyperLink = CType(e.Item.FindControl("hlkProduct"), HyperLink)
                Dim cbxSeriesId As CheckBox = CType(e.Item.FindControl("cbxSeriesId"), CheckBox)
                Dim litBandwidth As Literal = CType(e.Item.FindControl("litBandwidth"), Literal)
                Dim trRowData As HtmlTableRow = CType(e.Item.FindControl("trRowData"), HtmlTableRow)
                Dim trAlternateRowData As HtmlTableRow = CType(e.Item.FindControl("trAlternateRowData"), HtmlTableRow)
                Dim ph As PlaceHolder = CType(e.Item.FindControl("ph"), PlaceHolder)

                Dim btn As LinkButton = New LinkButton()
                btn.ID = String.Format("btnCompare-{0}", row.ProductSeriesId)
                btn.ClientIDMode = UI.ClientIDMode.Static
                btn.CausesValidation = False
                btn.OnClientClick = "return CanSubmit();"
                btn.PostBackUrl = "~/resources/compare/compare.aspx"
                btn.Style.Add("display", "none")
                btn.Text = "Compare"
                ph.Controls.Add(btn)

                imgProduct.ImageUrl = row.ProductImage
                imgProduct.AlternateText = row.Name
                If Not (String.IsNullOrEmpty(row.SubsiteUrl)) Then
                    hlkProduct.NavigateUrl = row.SubsiteUrl
                Else
                    If Not (String.IsNullOrEmpty(row.SeoValue)) Then
                        hlkProduct.NavigateUrl = String.Format("~/oscilloscope/{0}", row.SeoValue)
                    Else
                        hlkProduct.NavigateUrl = String.Format("oscilloscopeseries.aspx?mseries={0}", row.ProductSeriesId)
                    End If
                End If
                hlkProduct.Text = row.Name
                cbxSeriesId.InputAttributes.Add("value", row.ProductSeriesId.ToString())
                If ((_rowCount Mod 2) = 0) Then
                    SetDynamicTableCellData(row.ProductSeriesId, "grey", trRowData)
                Else
                    SetDynamicTableCellData(row.ProductSeriesId, String.Empty, trAlternateRowData)
                End If
                _rowCount += 1
        End Select
    End Sub

    Protected Sub ddlBandwidths_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlBandwidths As DropDownList = CType(sender, DropDownList)
        If (ddlBandwidths.SelectedIndex > 0) Then
            ViewState("SelectedBandwidthValue") = Int32.Parse(ddlBandwidths.SelectedValue)
            BindSeries(True, Int32.Parse(ddlBandwidths.SelectedValue))
        Else
            ViewState.Remove("SelectedBandwidthValue")
            BindSeries(False, Nothing)
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindSeries(ByVal isFilteredBind As Boolean, ByVal rangeId As Nullable(Of Int32))
        Dim seriesRetVal As List(Of ProductSeries) = FeProductManager.GetProductSeriesForCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), 1, isFilteredBind, rangeId, String.Empty)
        If (seriesRetVal.Count > 0) Then
            _seriesProperties = FePropertyManager.GetSeriesProperties(ConfigurationManager.AppSettings("ConnectionString").ToString(), (From sr In seriesRetVal Select sr.ProductSeriesId).Distinct().ToList())
            'rptProduct.DataSource = seriesRetVal
            'rptProduct.DataBind()
        End If
    End Sub

    Private Sub SetDynamicColumnHeaders(ByRef trHeaders As HtmlTableRow)
        Dim excludeTheseProperties As List(Of Int32) = New List(Of Int32)({456, 458})
        Dim properties As List(Of [Property]) = FePropertyManager.GetProperties(ConfigurationManager.AppSettings("ConnectionString").ToString()).Where(Function(x) Not (excludeTheseProperties.Contains(x.PropertyId))).OrderBy(Function(x) x.CategorySortId).ToList()
        Dim startingPosition As Int32 = 0
        For Each prop As [Property] In properties
            _propertyIds.Add(startingPosition, prop.PropertyId)
            startingPosition += 1
            If Not (String.Compare(prop.CategoryDisplayName, "Bandwidth", True) = 0) Then
                Dim td As HtmlTableCell = New HtmlTableCell("th")
                td.InnerText = prop.CategoryDisplayName
                trHeaders.Controls.Add(td)
            End If
        Next
    End Sub

    Private Sub BindBandwidthData(ByRef ddlBandwidths As DropDownList)
        Dim bandwidthData As List(Of BandwidthRange) = FeBandwidthManager.GetActiveBandwidthRanges(ConfigurationManager.AppSettings("ConnectionString").ToString()).OrderBy(Function(x) x.SortId).ToList()
        If (bandwidthData.Count > 0) Then
            For Each bd As BandwidthRange In bandwidthData
                ddlBandwidths.Items.Add(New ListItem(bd.RangeValue, bd.RangeId.ToString()))
            Next
        End If
        ddlBandwidths.Items.Insert(0, New ListItem("Bandwidth", "-1"))
        If Not (ViewState("SelectedBandwidthValue") Is Nothing) Then
            If Not (String.IsNullOrEmpty(ViewState("SelectedBandwidthValue").ToString().Trim())) Then
                ddlBandwidths.SelectedValue = ViewState("SelectedBandwidthValue")
            End If
        End If
    End Sub

    Private Sub SetDynamicTableCellData(ByVal productSeriesId As Int32, ByVal cssClass As String, ByRef trDataRow As HtmlTableRow)
        Dim columnCount As Int32 = 0
        For Each kvp As KeyValuePair(Of Int32, Int32) In _propertyIds
            Dim propertyId As Int32 = kvp.Value
            Dim td As HtmlTableCell = New HtmlTableCell()
            Dim propValue As String = (From spd In _seriesProperties
                            Where spd.ProductSeriesId = productSeriesId And spd.PropertyId = propertyId
                            Select spd.PropertyValue).FirstOrDefault()
            td.InnerText = propValue
            Dim lastColModifier As String = String.Empty
            If (columnCount = _propertyIds.Count - 1) Then
                lastColModifier = " last"
            End If
            td.Attributes.Add("class", String.Format("{0}{1}", cssClass, lastColModifier))
            If Not (trDataRow Is Nothing) Then
                trDataRow.Cells.Add(td)
            End If
            columnCount += 1
        Next
    End Sub
#End Region
End Class