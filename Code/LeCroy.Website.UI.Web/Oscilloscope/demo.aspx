﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="demo.aspx.vb" Inherits="LeCroy.Website.Oscilloscope_RequestInfoDemo" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="current">
                    <asp:Literal ID="lbSeries1" runat="server" />
                    <ul><asp:Literal ID="lbProduct1" runat="server" /></ul>
                </li>
            </ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro"><asp:Label ID="lblMessage" runat="server" Visible="False" /></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    &nbsp;
</asp:Content>