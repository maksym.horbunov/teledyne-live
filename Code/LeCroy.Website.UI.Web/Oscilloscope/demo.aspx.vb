﻿Imports System.Data.SqlClient
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Public Class Oscilloscope_RequestInfoDemo
    Inherits BasePage

    Dim strSQL As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim mseries As String = ""
    Dim strEmailBody As String = ""
    Dim strConfirmation As String = ""
    Dim strSpace As String = Chr(13) & Chr(10)

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscopes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' if URL contains query string, validate all query string variables
        If Len(Request.QueryString("mseries")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("mseries")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") Then
                    mseries = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") + 1)
                Else
                    mseries = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                End If
                If Not CheckSeriesExists(mseries, 1) Then
                    Response.Redirect("default.aspx")
                End If
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Demo Request"

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries, CategoryIds.OSCILLOSCOPES)) Then
            Response.Redirect("~/")
        End If

        Session("RedirectTo") = rootDir & "/oscilloscope/demo.aspx?mseries=" & mseries
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/support/user/")
        End If
        ValidateUserNoRedir()

        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SCOPE_CAPTION_ID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        Session("seriesSelected") = mseries
        If Not Me.IsPostBack Then
            getProductLine()
            lb_leftmenu.Text = Functions.LeftMenuScope(captionID, rootDir, pn_leftmenu, menuID, mseries, "")
            pn_leftmenu.Visible = False

        End If
        Functions.InsertRequest(Session("ContactId"), 2, mseries, "", "", Session("PageID"), Session("campaignid"))

        strEmailBody = LoadI18N("SLBCA0340", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + Functions.GetSeriesNameonID(mseries).ToString + strSpace

        If Len(strEmailBody) > 0 Then
            lblMessage.Text = "<strong>Thank you for requesting a demo for " & Functions.GetSeriesNameonID(mseries).ToString & "</strong><br /><br /><img src='/images/icons/icon_small_arrow_left.gif' />&nbsp;<a href='/oscilloscope/oscilloscopeseries.aspx?mseries=" & mseries & "'><strong>Back to " & Functions.GetSeriesNameonID(mseries).ToString & "</strong></a>"
            lblMessage.Visible = True
            strConfirmation = strEmailBody
            strEmailBody = "New request(s)" + strSpace + strSpace + Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + strSpace + strEmailBody
            Dim strSubject As String = "New request(s)"

            strSubject += " Demo "
            strSubject += " - " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            If Len(Functions.GetTacticIdOnCampaignID(Session("CampaignID"))) > 0 Then
                strSubject += " - " & Functions.GetTacticIdOnCampaignID(Session("CampaignID"))
            End If
            Functions.SendEmail(Session("country").ToString, strEmailBody, Functions.GetSalesRepEmail(Session("country"), 2, CategoryIds.OSCILLOSCOPES), Session("email").ToString(), "", "", strSubject, "")
        End If


    End Sub
    Private Sub getProductLine()
        '*left menu for series
        Dim sqlGetSeries As String
        Dim ds As New DataSet
        sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            lbSeries1.Text = ds.Tables(0).Rows(0)("NAME").ToString()
        End If

        '*left menu for products
        Dim sqlGetProducts As String
        Dim productID As String
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String
        Dim productLine As String
        sqlGetProducts = "SELECT DISTINCT a.*" +
                         " FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE " +
                         " a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID " +
                         " AND c.CATEGORY_ID=@CATEGORYID AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND a.disabled='n' order by a.sort_id"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        products = ""
        productLine = ""
        If pNum > 0 Then
            For i = 0 To pNum - 1
                Dim modelID As String = ds.Tables(0).Rows(i)("NAME").ToString()
                productID = ds.Tables(0).Rows(i)("PRODUCTID").ToString()
                products = products + "<li><a href='/oscilloscope/oscilloscopemodel.aspx?modelid=" + productID + menuURL + "'> " + modelID + "</a></li>"
            Next
            lbProduct1.Text = products
        Else
            'Me.Menu1.Items.RemoveAt(0)
        End If
    End Sub
End Class