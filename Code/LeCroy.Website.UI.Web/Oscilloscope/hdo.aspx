﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="hdo.aspx.vb" Inherits="LeCroy.Website.Oscilloscope_hdo" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner-slide section-white small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>High Definition Oscilloscopes</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-text">
										<p>High Definition Oscilloscopes (HDO) use  <a class="green" href="#technology">HD4096 Technology </a>to provide <b>12 bits of resolution all the time </b>from 200 MHz up to 8 GHz.</p>
										<ul class="list-dots mb-3 green">
											<li>No tradeoff of resolution, sample rate, bandwidth</li>
											<li>Clean, crisp waveforms </li>
											<li>More signal details</li>
											<li>Unmatched measurement precision</li>
										</ul>
										<p>Once you use a Teledyne LeCroy HDO, you’ll never want to go back.</p>
									</div>
							<div class="page-button"><span class="btn btn-default datasheet-dropdown-menu btn-green-outline">Datasheet
											<ul class="datasheet-dropdown">
												<li><a href="/doc/wavesurfer-4000hd-datasheet">WaveSurfer 4000HD Datasheet</a></li>
												<li><a href="/doc/hdo6000b-datasheet">HDO6000B Datasheet</a></li>
												<li><a href="/doc/waverunner-8000hd-datasheet">WaveRunner 8000HD Datasheet</a></li>
												<li><a href="/doc/waveprohd-datasheet">WavePro HD Datasheet</a></li>
											</ul></span><a class="btn btn-default btn-green-outline" href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x">Download White Paper                </a><span class="btn btn-default datasheet-dropdown-menu btn-green-outline">Configure
											<ul class="datasheet-dropdown">
												<li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=609">WaveSurfer 4000HD Configure</a></li>
												<li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=647">HDO6000B Configure</a></li>
												<li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=607">WaveRunner 8000HD Configure</a></li>
												<li><a href="	/oscilloscope/configure/configure_step2.aspx?seriesid=566">WavePro HD Configure</a></li>
											</ul></span></div>
								</div>
							</div>
							<div class="col-md-6"><a href="#compare"><img src="https://assets.lcry.net/images/hdo-img-new.png" alt="img-description"></a></div>
						</div>
					</div>
				</section>
				<div class="section-default-page page-video section-gray">
					<div class="container">
						<div class="section-content">   
							<div class="row">
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_osn5ftuk70 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-osn5ftuk70-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_45.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_218.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;">
															<div id="wistia_233.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_233.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div><img id="wistia_218.thumbnail_img" alt="wr8000hd-hdo4096" src="https://embedwistia-a.akamaihd.net/deliveries/8ec3c36c24f9f92cde2c557a2b9d2d3d.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_jlen8kljmq wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-jlen8kljmq-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_73.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_193.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;">
															<div id="wistia_208.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_208.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div><img id="wistia_193.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/389805752677ef89b8f98645b256f07e.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_2kced34urr wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-2kced34urr-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_86.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_117.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;">
															<div id="wistia_133.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_133.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div><img id="wistia_117.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/d82fd1391bfa501add964c1ea7eb23f7.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="section-default-page" id="technology">
					<div class="container">
						<div class="row">
							<div class="col-md-12 mb-4">
								<div class="page-content">
									<div class="page-title text-center">
										<h2>HD4096 Technology – 12 bits <b>all the time    </b></h2>
									</div>
									<div class="page-text mb-4">
										<p>Teledyne LeCroy High Definition Oscilloscopes use a systems design approach to achieve high resolution and low noise all the time. Other manufacturers use software post-processing, a single high-resolution component, or a combination of both with reduced overall performance. Only Teledyne LeCroy gives you 12 bits all the time.</p>
									</div>
									<div class="div-button text-center"><a class="btn btn-default btn-green-outline" href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x">Download White Paper</a></div>
								</div>
							</div>
							<div class="col-md-12"><a data-fancybox="gallery" href="https://assets.lcry.net/images/hdo-img-3.png"><img class="big-img" src="https://assets.lcry.net/images/hdo-img-3.png" alt="img-description"></a></div>
						</div>
					</div>
				</div>
				<div class="section-default-page section-gray">
					<div class="container">
						<div class="row mb-4"> 
							<div class="col-md-12">
								<div class="page-content">
									<div class="page-title text-center">
										<h2>HD4096 Technology Systems Approach</h2>
									</div>
									<div class="page-text">
										<p>
											HD4096 High Definition Technology consists of high sample rate 12-bit ADCs, high signal-to-noise ratio amplifiers and a low-noise system architecture. This total system approach provides an ideal signal path to ensure that signal details are 
											delivered accurately to the oscilloscope display.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row mb-4">    
							<div class="col-md-4">
								<div class="card-line number">
									<div class="inner-content small">                                         
										<div class="card-title">                                      
											<h3> <i class="icon">A</i>Clean, Crisp Waveforms                                       </h3>
										</div>
										<div class="card-text">
											<p>Waveforms captured and displayed on oscilloscopes with HD4096 technology are cleaner and crisper.       </p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card-line number">
									<div class="inner-content small">                                         
										<div class="card-title">                                      
											<h3> <i class="icon">B</i>More Signal Details                                    </h3>
										</div>
										<div class="card-text">
											<p>Signal details often lost in the noise are clearly visible and easy to distinguish.  </p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="card-line number">
									<div class="inner-content small">                                         
										<div class="card-title">                                      
											<h3> <i class="icon">C</i>Unmatched Precision                          </h3>
										</div>
										<div class="card-text">
											<p>HD4096 enables oscilloscopes to deliver unmatched measurement precision for improved debug and analysis.   </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">     
							<div class="col-md-6"><img src="https://assets.lcry.net/images/hdo-img-6.png" alt="img-description"></div>
							<div class="col-md-6"><img src="https://assets.lcry.net/images/hdo-img-5.png" alt="img-description"></div>
						</div>
					</div>
				</div>
				<section class="section-default-page section-gray section-angle-before pt-0">
					<div class="container border-top-green">
						<div class="row">   
							<div class="col-md-12 mb-3 justify-center text-center">
								<div class="page-content">
									<p>The 4096 discrete vertical levels reduce the quantization error compared to 256 vertical levels.</p>
									<p>This improves the accuracy and precision of the signal capture and increases measurement confidence.  </p>
								</div>
							</div>
							<div class="col-md-12 text-center"><img src="https://assets.lcry.net/images/hdo-img-7.jpg" alt="img-description"></div>
						</div>
					</div>
				</section>
				<div class="section-default-page section-angle pb-0" id="compare">
					<div class="container">
						<div class="row text-center">      
							<div class="col-md-12">
								<div class="page-title">
									<h2>Compare High Definition Oscilloscopes</h2>
								</div>
							</div>
							<div class="col-md-12"><img src="https://assets.lcry.net/images/hdo-family-2021-large.png" alt="img-description"></div>
						</div>
					</div>
				</div>
				<div class="section-default-page pt-0"> 
					<div class="container">
						<div class="table-responsive">
							<table>
								<thead class="green">
									<tr> 
										<th></th>
										<th>WaveSurfer 4000HD</th>
										<th>HDO6000B</th>
										<th>WaveRunner 8000HD</th>
										<th>WavePro HD              </th>
									</tr>
								</thead>
								<tbody>
									<tr> 
										<td>HD Technology</td>
										<td>
											 HD4096 <br>12 bits</td>
										<td>
											 HD4096<br>12 bits	</td>
										<td>
											 HD4096<br>12 bits	 </td>
										<td>
											 HD4096<br>12 bits</td>
									</tr>
									<tr>
										<td>Bandwidth	</td>
										<td>200 MHz - 1 GHz	</td>
										<td>350 MHz - 1 GHz	</td>
										<td>350 MHz - 2 GHz	</td>
										<td>2.5 GHz - 8 GHz    </td>
									</tr>
									<tr>
										<td>Input Channels	</td>
										<td>4</td>
										<td>4</td>
										<td>8</td>
										<td>4</td>
									</tr>
									<tr>
										<td>Sample Rate	</td>
										<td>5 GS/s</td>
										<td>10 GS/s</td>
										<td>10 GS/s</td>
										<td>20 GS/s   </td>
									</tr>
									<tr>
										<td>Acquisition Memory	</td>
										<td>25 Mpts</td>
										<td>250 Mpts</td>
										<td>5 Gpts</td>
										<td>5 Gpts  </td>
									</tr>
									<tr>
										<td>Analysis Capability</td>
										<td>Basic</td>
										<td>Advanced</td>
										<td>Advanced</td>
										<td>Advanced   </td>
									</tr>
									<tr>
										<td>Serial Data Tools</td>
										<td>TD</td>
										<td>TDME, QPHY</td>
										<td>TDME, SDAIII, QPHY</td>
										<td>TDME, SDAIII, QPHY       </td>
									</tr>
									<tr>
										<td>User Experience</td>
										<td>MAUI with OneTouch</td>
										<td>MAUI with OneTouch</td>
										<td>MAUI with OneTouch</td>
										<td>MAUI with OneTouch  </td>
									</tr>
									<tr>
										<td> </td>
										<td> <a class="btn btn-default btn-green" href="/oscilloscope/wavesurfer-4000hd-oscilloscopes">WaveSurfer 4000HD</a></td>
										<td> <a class="btn btn-default btn-green" href="/oscilloscope/hdo6000b-high-definition-oscilloscopes">HDO6000B</a></td>
										<td> <a class="btn btn-default btn-green" href="/oscilloscope/waverunner-8000hd-oscilloscopes">WaveRunner 8000HD</a></td>
										<td> <a class="btn btn-default btn-green" href="/oscilloscope/wavepro-hd-oscilloscope">WavePro HD                   </a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</main>
</asp:Content>
