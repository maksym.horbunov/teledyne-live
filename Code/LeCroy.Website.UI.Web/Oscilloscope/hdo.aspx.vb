﻿Public Class Oscilloscope_hdo
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

        Me.Title = "Teledyne LeCroy - High Definition Oscilloscopes"
        Me.MetaDescription = "High Definition Oscilloscopes from Teledyne LeCroy"

        Master.MetaOgTitle = "Teledyne LeCroy High Definition Oscilloscopes"
        Master.MetaOgImage = "https://assets.lcry.net/images/hdo-img-new.png"
        Master.MetaOgDescription = "High Definition Oscilloscopes (HDO) with HD4096 Technology capture and display signals from 200 MHz up to 8 GHz with high sample rate and 12 bits of resolution all the time."
        Master.MetaOgUrl = "https://teledynelecroy.com/oscilloscope/hdo.aspx"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE
    End Sub

End Class