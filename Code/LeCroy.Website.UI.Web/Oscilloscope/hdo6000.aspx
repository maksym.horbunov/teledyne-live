﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="hdo6000.aspx.vb" Inherits="LeCroy.Website.Oscilloscope_hdo6000" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner-slide section-white small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>HDO6000B High Definition Oscilloscopes</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-title">
										<h2 class="green">Capture Every Detail</h2>
									</div>
									<div class="page-text">
										<ul class="list-dots mb-3 green">
											<li>4 channels, 1 GHz, 250 Mpts</li>
											<li>12 bits <strong>all the time</strong></li>
										</ul>
									</div>
									<div class="page-button"><a class="btn btn-default btn-green-outline" href="/oscilloscope/hdo6000b-high-definition-oscilloscopes">HDO6000B</a><a class="btn btn-default btn-green-outline" href="/doc/hdo6000b-datasheet">Datasheet</a><a class="btn btn-default btn-green-outline" href="/oscilloscope/configure/configure_step2.aspx?seriesid=647">Configure</a><a class="btn btn-default btn-green-outline" href="#videos">Videos</a><a class="btn btn-default btn-green-outline" href="#docs">Technical Docs</a></div>
									<div class="page-video">
										<div class="row">
											<div class="col-md-8">
												<div class="visual"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ta5d31ncp9 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6"><a href="hdo6000b-high-definition-oscilloscopes"><img src="https://assets.lcry.net/images/hdo6000b.png" alt="img-description"></a></div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big mb-2 p-24 pb-0 h-auto"><img src="https://assets.lcry.net/images/oscilloscopes/hdo6000b02.png"></div>
									<div class="inner-content">
										<div class="card-title">                                      
											<h3>Highest Resolution</h3>
										</div>
										<div class="card-content mb-3"> 
											<p>HD4096 technology enables 12 bits of vertical resolution with 1 GHz bandwidth   </p>
										</div>
										<ul class="list-dots green">
											<li>Clean, Crisp Waveforms</li>
											<li>More Signal Details</li>
											<li>Unmatched Measurement Precision</li>
										</ul>
									</div>
									<div class="inner-visible text-center mb-40"><span class="wistia_embed wistia_async_xk1872z2rv wistia_embed_initialized popover=true popoverContent=link" id="wistia-xk1872z2rv-1" style="display:inline;position:relative">
											<div class="wistia_click_to_play" id="wistia_61.thumb_container" style="position: relative; display: inline;"><a class="btn btn-default btn-green" href="#">Explore</a></div></span></div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big mb-2 p-24 pb-0 h-auto"><a data-fancybox="gallery30" href="https://assets.lcry.net/images/oscilloscopes/hdo6000b03.png"><img src="https://assets.lcry.net/images/hdo6000b03.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Bigger display</h3>
										</div>
										<div class="card-content mb-3"> 
											<p>Capture every detail with a bigger 15.6" display</p>
										</div>
										<ul class="list-dots green">
											<li>1920x1080 Full HD resolution</li>
											<li>Sleekest instrument, smallest footprint</li>
											<li>New design, most bench space</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big mb-2 p-24 pb-0 h-auto"><img src="https://assets.lcry.net/images/oscilloscopes/hdo6000b04.png"></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>More Capability</h3>
										</div>
										<div class="card-content mb-3"> 
											<p>Increase productivity with specialized<br />tools</p>
										</div>
										<ul class="list-dots green">
											<li>MAUI Studio</li>
											<li>Spectrum Analysis</li>
											<li>QualiPHY Compliance Testing </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="videos">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Videos</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ta5d31ncp9 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-ta5d31ncp9-2" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_88.thumb_container" style="position: relative; height: 143.156px; width: 254.516px;">
														<div id="wistia_387.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 143.141px; overflow: hidden; outline: none; position: relative; width: 254.516px;">
															<div id="wistia_402.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 97px; top: 53px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_402.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 97px; top: 53px;"></div><img id="wistia_387.thumbnail_img" alt="WaveSurfer 4000HD Overview" src="https://embed-fastly.wistia.com/deliveries/f4e46d5fe9782ce93038218481a66241.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 143.141px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 254.516px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_xk1872z2rv wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-xk1872z2rv-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_101.thumb_container" style="position: relative; height: 143.156px; width: 254.516px;">
														<div id="wistia_412.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 143.141px; overflow: hidden; outline: none; position: relative; width: 254.516px;">
															<div id="wistia_427.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 97px; top: 53px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_427.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 97px; top: 53px;"></div><img id="wistia_412.thumbnail_img" alt="HDO6000B" src="https://embedwistia-a.akamaihd.net/deliveries/40cde1cc4d81428eaed725567e9c5b9c.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 143.141px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 254.516px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_osn5ftuk70 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-osn5ftuk70-2" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_114.thumb_container" style="position: relative; height: 143.156px; width: 254.516px;">
														<div id="wistia_542.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 143.141px; overflow: hidden; outline: none; position: relative; width: 254.516px;">
															<div id="wistia_557.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 97px; top: 53px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_557.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 97px; top: 53px;"></div><img id="wistia_542.thumbnail_img" alt="WaveSurfer 4000HD Probes" src="https://embed-fastly.wistia.com/deliveries/19f7634b094591299107e5f971027c90.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 143.141px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 254.516px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="greenbar">
					<div class="container">
							<div class="row">
								<div class="col-md-12">
									<p>Today’s  highly complex power-conversion systems and embedded control systems are becoming smaller and denser, with more sensor inputs and lower-voltage power rails than ever.</p><p>HDO6000B's combination of 12-bit resolution at 1 GHz bandwidth and a competitive software toolset makes it ideal for these key applications.
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xl-3 mt-4 text-center"><a href="#one"><img src="https://assets.lcry.net/images/hdo6000b05.png" alt="img-description"></a></div>
								<div class="col-md-6 col-xl-3 mt-4 text-center"><a href="#two"><img src="https://assets.lcry.net/images/hdo6000b06.png" alt="img-description"></a></div>
								<div class="col-md-6 col-xl-3 mt-4 text-center"><a href="#three"><img src="https://assets.lcry.net/images/hdo6000b07.png" alt="img-description"></a></div>
								<div class="col-md-6 col-xl-3 mt-4 text-center"><a href="#four"><img src="https://assets.lcry.net/images/hdo6000b08.png" alt="img-description"></a></div>
							</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="card-line">
									<div class="inner-content">  <a data-fancybox="gallery1" href="https://assets.lcry.net/images/hdo6000b09.png"><img src="https://assets.lcry.net/images/hdo6000b09.png" alt="img-description"></a></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-line">
									<div class="inner-content">  
										<ul class="number green"><li><i>1</i>15.6" 1920 x 1080 capacitive touchscreen display</li>
<li><i>2</i>4 analog input channels</li>
<li><i>3</i>ProBus input supports every Teledyne LeCroy probe</li>
<li><i>4</i>MAUI with OneTouch user interface for intuitive and efficient operation</li>
<li><i>5</i>Q-Scape multi-tab display architecture </li>
<li><i>6</i>Up to 250 Mpts of acquisition memory</li>
<li><i>7</i>HD4096 technology - 12 bits all the time</li>
<li><i>8</i>Buttons/indicators color-coded to associated waveform on display</li>
<li><i>9</i>Use cursors and adjust settings without opening a menu</li>
<li><i>10</i>Mixed Signal capability with 16 integrated digital channels</li>
<li><i>11</i>6 USB 3.1 ports (2 front, 4 side)</li>
</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
		<section class="section-default-page section-gray" id="one">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>Power Conversion</h2>
									</div>
									<div class="page-text mb-4">
										<p>HDO6000B is great for inverter subsection, power system, and control testing.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4"><a data-fancybox="gallery5" href="https://assets.lcry.net/images/hdo6000b10.png"><img src="https://assets.lcry.net/images/hdo6000b10.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Flexible Power Calculations</h3>
										</div>
										<div class="card-content">
											<p>Use Numerics table for static performance and per-cycle waveforms for dynamic analysis</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Comprehensive Probing</h3>
										</div>
										<div class="card-content">
											<p>Voltage, Current, Differential, Single-Ended, Fiber Optic, the HDO6000B supports it all!</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Two-wattmeter Support</h3>
										</div>
										<div class="card-content">
											<p>Enable the two-wattmeter method on the HDO6000B and make 3-phase measurements on 4 channels instead of 6</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</section>
				<section class="section-default-page" id="two">
						<div class="container">
							<div class="row mb-4">
								<div class="col-md-8">
									<div class="page-content">
										<div class="page-title">
											<h2>Automotive Electronics</h2>
										</div>
										<div class="page-text mb-4">
											<p>HDO6000B 12-bit oscilloscopes best address the specific test needs of the automotive industry. 
</p>
										</div>
									</div>
								</div>
								<div class="col-md-4"><a data-fancybox="gallery6" href="https://assets.lcry.net/images/hdo6000b11.png"><img src="https://assets.lcry.net/images/hdo6000b11.png" alt="img-description"></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Ideal for 48 V systems</h3>
											</div>
											<div class="card-content">
												<p>Plug DL-HCM probes into the HDO6000B and get great insight into 48 V battery-powered motor and drive systems</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Superior IVN tools</h3>
											</div>
											<div class="card-content">
												<p>Cover all aspects of physical layer Automotive Ethernet compliance testing and debug on an HDO6000B</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>EMI/EMC pre-compliance test</h3>
											</div>
											<div class="card-content">
												<p>HDO6000B provides Spectrum Analysis and specialized EMI/EMC parameters for more insight and maximum flexibility</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				<section class="section-default-page section-gray" id="three">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>Power Integrity</h2>
									</div>
									<div class="page-text mb-4">
										<p>12 bits all the time with HDO6000B means complete confidence for power integrity applications</p>
									</div>
								</div>
							</div>
							<div class="col-md-4"><a data-fancybox="gallery6" href="https://assets.lcry.net/images/hdo6000b12.png"><img src="https://assets.lcry.net/images/hdo6000b12.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Accurate PDN measurements</h3>
										</div>
										<div class="card-content">
											<p>Use built-in WaveScan or conditional Smart triggers to find anomalous clock edges.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Specialized power probes</h3>
										</div>
										<div class="card-content">
											<p>Capture long records to build statistics faster. All-instance measurements provide statistics for every clock edge for any acquisition length.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Spectrum Analysis</h3>
										</div>
										<div class="card-content">
											<p>Draw a Trend of measurement values over time. Use Histicons (miniature histograms) to show statistical distributions.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="four">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-8">
								<div class="page-content">
									<div class="page-title">
										<h2>Embedded System Debug</h2>
									</div>
									<div class="page-text mb-4">
										<p>HDO6000B gives the most comprehensive embedded system analysis (analog, digital, serial data, and sensor).</p>
									</div>
								</div>
							</div>
							<div class="col-md-4"><a data-fancybox="gallery6" href="https://assets.lcry.net/images/hdo6000b13.png"><img src="https://assets.lcry.net/images/hdo6000b13.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Clock Analysis</h3>
										</div>
										<div class="card-content">
											<p>Use built-in WaveScan or conditional Smart triggers to find anomalous clock edges.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Protocol Analysis</h3>
										</div>
										<div class="card-content">
											<p>Capture long records to build statistics faster. All-instance measurements provide statistics for every clock edge for any acquisition length.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Power Management Tools</h3>
										</div>
										<div class="card-content">
											<p>Draw a Trend of measurement values over time. Use Histicons (miniature histograms) to show statistical distributions.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page technical-docs section-gray" id="docs">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Technical Docs</h2>
						</div>
						<div class="section-content">
							<ul class="collection technical-docs-ul">
								<div class="row">
									<div class="col-md-6">
										<ul><li><a href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x">
												<h5>Comparing High Resolution Oscilloscope Design Approaches
</h5>This white paper provides an overview of the various high resolution design approaches, with examples of their impact on oscilloscope performance.
</a></li></ul>
									</div>
								</div>
							</ul>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
