﻿Public Class Oscilloscope_hdo6000
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

        Me.Title = "Teledyne LeCroy - HDO6000B High Definition Oscilloscopes"
        Me.MetaDescription = "HDO6000B High Definition Oscilloscopes from Teledyne LeCroy"

    End Sub

End Class