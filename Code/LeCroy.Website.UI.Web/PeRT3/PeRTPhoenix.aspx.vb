Imports System.Data.SqlClient
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class PeRTPhoenix
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Public mseries As String = ""
    Dim standardid As String = ""
    Dim firstmenuid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim promoImages As String = ""
    Dim seriesName As String = ""
    Dim seriesExplorerText As String = ""
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Dim hasOptions As Boolean = False
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "PeRT<sup>3</sup>"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imageIndex As Integer = 0
        Dim captionID As String
        Dim menuID As String
        ' if URL contains query string, validate all query string variables
        '** caption_id
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = 131
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = 131
            End If
        End If

        '** MenuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.PERT_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.PERT_MENU
            End If
        End If

        '** mseries
        mseries = 340

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries, CategoryIds.PERT3_SYSTEMS)) Then
            Response.Redirect("~/")
        End If

        Session("cataid") = 24
        Session("menuSelected") = captionID
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        If Not Me.IsPostBack Then
            getProductLine()
            getSeriesOverview()
            getSpecs()
            getOptions()
            lblTabsToShow.Text = tabsToShow.ToString()

            If hasDetail Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
            ElseIf hasOverview Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            Else
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
            End If

            Functions.CreateMetaTags(Me.Header, mseries, AppConstants.SERIES_OVERVIEW_TYPE)

            strSQL = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID=@PRODUCTSERIESID "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                seriesName = dr("NAME").ToString()
                dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
                specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
                promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
                seriesExplorerText = Me.convertToString(ds.Tables(0).Rows(0)("SHORT_NAME"))
                lbSeriesTileTop.Text = seriesName
                lbSeriesTileBelow.Text = "Explore " + seriesExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + seriesExplorerText + "' >"
                lbSeriesImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString() + "' width='260' />"
                lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - PeRT3 Phoenix Test System  "
            End If
            'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
            '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a></li>"
            'End If
            'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
            '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
            'End If

            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString)
            End If
            lbProduct1.Text = "<li class='current'><a href='/PeRT3/PeRTPhoenix.aspx'>PeRT3 Phoenix</a></li>"
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Private Sub getProductDetail()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If Len(dr("OVER_VIEW_DETAILS").ToString()) > 0 Then
                Me.lblProductDetail.Text = dr("OVER_VIEW_DETAILS").ToString()
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub
    Private Sub getSpecs()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_SPECS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblSpecs.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
    End Sub

    Private Sub getSeriesOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            hasOverview = True
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If

    End Sub

    Private Sub getOptions()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        ' Response.Write(strSQL)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_OPTIONS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOptions.Text = dr("OVER_VIEW_DETAILS").ToString()
            hasOptions = True
            tabsToShow.Append("<li><a href=""#options""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_options_off.gif' id='options' border='0'></a></li>")
        End If

    End Sub

    Private Sub getProductLine()
        '*left menu for series
        Dim sqlGetSeries As String
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            seriesName = Me.convertToString(ds.Tables(0).Rows(0)("NAME"))
            dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
            specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
            promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
        End If
        'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
        '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a><br />Learn more about " & seriesName & "</li>"
        'End If
        'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
        '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
        'End If

        If Not promoImages = Nothing And Len(promoImages) > 0 Then
            lbPromotion.Text = promoImages
        End If

        '*left menu for products
        Dim sqlGetProducts As String
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String = ""
        Dim productLine As String = ""

        strSQL = " SELECT DISTINCT b.GROUP_NAME, b.GROUP_ID , b.SORTID " +
                 " FROM PRODUCT a INNER JOIN PRODUCT_GROUP b ON a.GROUP_ID=b.GROUP_ID " +
                 " INNER JOIN    PRODUCT_SERIES_CATEGORY AS c ON a.PRODUCTID = c.PRODUCT_ID " +
                 " WHERE a.DISABLED='N'  AND c.CATEGORY_ID=@CATEGORYID and c.PRODUCT_SERIES_ID = @PRODUCTSERIESID order by b.sortid"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PERT3))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds1.Tables(0).Rows.Count Then
            For Each dr As DataRow In ds1.Tables(0).Rows
                sqlGetProducts = " SELECT DISTINCT a.* " +
                                 " FROM  PRODUCT AS a INNER JOIN  PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCTID = b.PRODUCT_ID " +
                                 " WHERE  a.DISABLED='N' and a.GROUP_ID=@GROUPID and b.PRODUCT_SERIES_ID = @PRODUCTSERIESID order by a.sort_id"
                'Response.Write(sqlGetProducts & "<br>")
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
                pNum = ds.Tables(0).Rows.Count
                If pNum > 0 Then
                    productLine += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>" + _
                               " <tr> " + _
                               " <td class='cell'><h3>" + dr("GROUP_NAME").ToString() + "</h3></td> " + _
                               " </tr> "
                    For i = 0 To pNum - 1
                        Dim modelID As String = ds.Tables(0).Rows(i)("NAME").ToString()

                        productLine = productLine + "<tr>" + _
                                       " <td class='cell'> " + _
                                       " <strong><u>"

                        productLine = productLine + ds.Tables(0).Rows(i)("PARTNUMBER").ToString()
                        productLine = productLine + "</u></strong><br /> " + _
                                       Functions.GetProdDescriptionOnProdID(ds.Tables(0).Rows(i)("PRODUCTID").ToString()).ToString + _
                                       " </td> " + _
                                       " </tr> "
                    Next
                    productLine = productLine + "</table> "
                End If
            Next
            lbProduct1.Text = products
            lbQuickSpecs.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub
End Class