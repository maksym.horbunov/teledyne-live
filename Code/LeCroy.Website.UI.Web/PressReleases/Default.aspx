<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.PressReleases_Default" Codebehind="Default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
	    <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="contentMiddle">
        <div class="intro2">
            <img src="<%= rootDir%>/images/category/headers/hd_press_releases.gif" alt="Press Releases">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <br /><br />
                        <asp:Panel ID="pnlNoNews" runat="server" Visible="false">
                            More news to come soon
                        </asp:Panel>
                        <asp:Panel ID="pnlNews" runat="server">
                            <asp:Repeater ID="rptNews" runat="server">
                                <HeaderTemplate>
                                    <table border="0" cellpadding="3" cellspacing="5" width="100%">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 25px;" valign="top">
                                            <asp:HyperLink ID="hlkUrl" runat="server"><img src="<%= rootDir%>/images/icons/icons_link.gif" alt="" border="0" /></asp:HyperLink>
                                        </td>
                                        <td valign="top">
                                            <asp:HyperLink ID="hlkUrl2" runat="server" /><br /><br />
                                        </td>
                                    </tr>
                                <tr><td colspan="3"><hr size="1" /></td></tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>