﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PressReleases_Default

    '''<summary>
    '''lb_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lb_leftmenu As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pn_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftmenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlNoNews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNoNews As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlNews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNews As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rptNews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptNews As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As LeCroy.Website.MasterPage_twocolumn_left
        Get
            Return CType(MyBase.Master, LeCroy.Website.MasterPage_twocolumn_left)
        End Get
    End Property
End Class
