Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class PressReleases_Default
    Inherits BasePage

#Region "Variables/Keys"
    Private menuURL As String = String.Empty
    Private menuURL2 As String = String.Empty
    Private _newsId As Nullable(Of Int32) = Nothing
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "About Us"
        Me.Master.PageContentHeader.BottomText = "Press Releases"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Press Releases"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        Dim newsID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.PRESS_REL_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.PRESS_REL_MENU
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            BindRepeater()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptNews_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptNews.ItemDataBound
        Select e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As News = CType(e.Item.DataItem, News)
                Dim hlkUrl As HyperLink = CType(e.Item.FindControl("hlkUrl"), HyperLink)
                Dim hlkUrl2 As HyperLink = CType(e.Item.FindControl("hlkUrl2"), HyperLink)

                Dim urlString As String = String.Format("document.aspx?news_id={0}{1}", row.NewsId.ToString(), menuURL)
                hlkUrl.NavigateUrl = urlString
                hlkUrl2.NavigateUrl = urlString
                hlkUrl2.Text = String.Format("{0}<br /><b>{1}</b>", row.StartDate.Value.ToString("MM/dd/yyyy"), row.Title)
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindRepeater()
        pnlNews.Visible = False
        pnlNoNews.Visible = False

        ValidateQueryString()
        Dim now As DateTime = DateTime.Now
        Dim startDate As DateTime = Convert.ToDateTime(String.Format("1/1/{0} 12:00:00 AM", now.AddYears(-3).Year))
        Dim news As List(Of News) = NewsRepository.GetNewsForDateRange(ConfigurationManager.AppSettings("ConnectionString").ToString(), startDate, now, _newsId)
        If (news.Count <= 0) Then
            pnlNoNews.Visible = True
            Return
        End If

        pnlNews.Visible = True
        rptNews.DataSource = news.OrderByDescending(Function(x) x.StartDate).ToList()
        rptNews.DataBind()
    End Sub

    Private Sub ValidateQueryString()
        If Not (String.IsNullOrEmpty(Request.QueryString("news_id"))) Then
            Dim newsId As Int32 = -1
            Integer.TryParse(Request.QueryString("news_id"), newsId)
            If (newsId <= 0) Then
                HandleBouncedRedirect()
            End If
            _newsId = newsId
        End If
    End Sub

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/")
    End Sub
#End Region
End Class