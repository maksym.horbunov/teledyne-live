﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.PressReleases_document" Codebehind="Document.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
	    <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <img alt="Press Releases" src="/images/category/headers/hd_press_releases.gif" />
        <h1><asp:Literal ID="PR_title" runat="server" /></h1>
        <p><asp:Literal ID="PDF_link" runat="server" /></p>
        <asp:Literal ID="lbDetails" runat="server" />
    </div>
</asp:Content>