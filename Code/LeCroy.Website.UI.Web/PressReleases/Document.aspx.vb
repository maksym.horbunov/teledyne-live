﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class PressReleases_document
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim strSQL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "About Us"
        Me.Master.PageContentHeader.BottomText = "Press Releases"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Press Releases"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        Dim newsID As String = ""
        Dim ds As DataSet
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Request.QueryString("capid") Is Nothing Or Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.ABOUT_LECROY_CAPTION_ID
            End If
        End If

        '** menuID
        menuID = AppConstants.PRESS_REL_MENU

        '** news_id
        If Not Request.QueryString("news_id") Is Nothing Or Len(Request.QueryString("news_id")) > 0 Then
            If IsNumeric(Request.QueryString("news_id")) Then
                newsID = Request.QueryString("news_id")
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID

        If Len(newsID) > 0 Then
            strSQL = "SELECT TITLE,NEWS_ID,URL,FILE_FULL_PATH FROM NEWS WHERE NEWS_ID = @NEWSID AND TOPIC_ID in (13,32) and GETDATE() > START_DATE AND DIVISION_ID='1' AND COUNTRY_ID=208 ORDER BY NEWS.START_DATE DESC"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@NEWSID", newsID.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                PR_title.Text = dr("title").ToString
                If Len(dr("FILE_FULL_PATH").ToString) > 0 Then
                    PDF_link.Text = "<img src='/images/Icons/IconPDF.gif'>&nbsp;<a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & "/files/pressreleases/" & dr("FILE_FULL_PATH").ToString().ToLower() + "'>Press Release (PDF)</a>"
                Else
                    If Len(dr("URL".ToString)) > 0 Then
                        If Mid(dr("URL").ToString, 1, 4) = "http" Then
                            Response.Redirect(dr("URL").ToString)
                        End If
                    End If
                End If
                getOverview(newsID.ToString)
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        If Not Me.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub
    Private Sub getOverview(ByVal newsID As String)
        Dim content As StringBuilder = New StringBuilder()
        If Len(newsID) > 0 Then
            Dim ds1 As DataSet
            strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", newsID))
            sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PRESS_RELEASES_OVERVIEW_TYPE))
            ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Dim num1 As Integer = 0
            num1 = ds1.Tables(0).Rows.Count
            If num1 > 0 Then
                Dim dr1 As DataRow = ds1.Tables(0).Rows(0)
                content.Append(dr1("OVER_VIEW_DETAILS").ToString)

            Else
                content.Append("<br><br><br>") '"No Data Available"
            End If
            lbDetails.Text = content.ToString
        End If
    End Sub
End Class