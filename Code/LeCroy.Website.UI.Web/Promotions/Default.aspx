<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Promotions_Default" Codebehind="Default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <asp:Literal ID="litPromotionsHeader" runat="server" />
        <hr />
        <div id="divPromotionsExist" runat="server" visible="false">
            <asp:Repeater ID="rptPromotions" runat="server">
                <HeaderTemplate>
                    <table border="0" cellpadding="3" cellspacing="5" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="sasr">
                            <td valign="top"><asp:Image ID="imgPromotionImage" runat="server" Visible="false" Width="150" /></td>
                            <td valign="top">
                                <asp:HyperLink ID="hlkPromotion" runat="server" /><br />
                                <span id="spanPromotionRunDate" runat="server" visible="false"><strong><asp:Label ID="lblPromotionRunDate" runat="server" /></strong></span>
                                <asp:Label ID="lblPromotionDescription" runat="server" /><br /><br />
                            </td>
                        </tr>
                        <tr><td colspan="3"><hr size="1" /></td></tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divNoPromotionsExist" runat="server" visible="true">
            No Promotions are available. Please come back later.
        </div>
    </div>
</asp:Content>