Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Promotions_Default
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

#Region "Variables/Keys"
    'Private Const LCCOUNTRY_COOKIEKEY As String = "LCCOUNTRY"
    Private Const CATEGORYID_QSKEY As String = "CategoryId"
    Private Const GROUPID_QSKEY As String = "GroupId"
    Private Const PRODUCTID_QSKEY As String = "ProductId"
    Private Const PRODUCTSERIESID_QSKEY As String = "ProductSeriesId"
    Private Const PROTOCOLSTANDARDID_QSKEY As String = "ProtocolStandardId"

    Private Const _promotionsSqlString As String = "SELECT PROMOTION.PROMO_ID, PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID, PROMOTION.URL " + _
                                                   "FROM PROMOTION INNER JOIN PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID " + _
                                                   "WHERE PROMOTION_COUNTRY.WW_YN = 'y' AND PROMOTION.POST_YN = 'y' " + _
                                                   "UNION SELECT PROMOTION.PROMO_ID, PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID, PROMOTION.URL " + _
                                                   "FROM PROMOTION INNER JOIN PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID INNER JOIN COUNTRY ON PROMOTION_COUNTRY.REGION_ID = COUNTRY.REGIONID " + _
                                                   "WHERE COUNTRY.COUNTRY_ID = @COUNTRYCODE AND PROMOTION.POST_YN = 'y' " + _
                                                   "UNION SELECT PROMOTION.PROMO_ID, PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID, PROMOTION.URL " + _
                                                   "FROM PROMOTION INNER JOIN PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID " + _
                                                   "WHERE PROMOTION.POST_YN = 'y' AND PROMOTION_COUNTRY.COUNTRY_ID = @COUNTRYCODE " + _
                                                   "ORDER BY PROMOTION.SORT_ID"
    Private Const _promotionCategorySqlString As String = "SELECT KEY_ID, PROMO_ID, CATEGORY_ID, GROUP_ID, PRODUCT_SERIES_ID, PRODUCT_ID, PRODUCT_STANDARD_ID FROM PROMOTION_CATEGORY WHERE PROMO_ID IN ({0})"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Special Offers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        HandleMenuLoad()
        SetPageContent()
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptPromotions_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptPromotions.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Promotion = CType(e.Item.DataItem, Promotion)
                Dim imgPromotionImage As Image = CType(e.Item.FindControl("imgPromotionImage"), Image)
                Dim hlkPromotion As HyperLink = CType(e.Item.FindControl("hlkPromotion"), HyperLink)
                Dim spanPromotionRunDate As HtmlGenericControl = CType(e.Item.FindControl("spanPromotionRunDate"), HtmlGenericControl)
                Dim lblPromotionRunDate As Label = CType(e.Item.FindControl("lblPromotionRunDate"), Label)
                Dim lblPromotionDescription As Label = CType(e.Item.FindControl("lblPromotionDescription"), Label)

                If Not (String.IsNullOrEmpty(row.Url)) Then
                    hlkPromotion.NavigateUrl = row.Url
                Else
                    If Not (String.IsNullOrEmpty(Functions.GetOverviewHTML(AppConstants.PROMOTION_OVERVIEW_TYPE, row.PromoId))) Then
                        hlkPromotion.NavigateUrl = String.Format("PromotionOverview.aspx?promoid={0}{1}", row.PromoId.ToString(), menuURL)
                    End If
                End If
                hlkPromotion.Text = String.Format("<h1>{0}</h1>", row.PromoName)    ' Original code did it this way, this IS NOT valid html...
                If Not (String.IsNullOrEmpty(row.PromoImage)) Then
                    imgPromotionImage.Visible = True
                    imgPromotionImage.ImageUrl = String.Format("~{0}", row.PromoImage)
                End If
                If (row.PromoStartDate.HasValue) Then
                    spanPromotionRunDate.Visible = True
                    If (row.PromoEndDate.HasValue) Then
                        lblPromotionRunDate.Text = String.Format("{0}&nbsp;-&nbsp;{1}<br /><br />", row.PromoStartDate.Value.ToShortDateString(), row.PromoEndDate.Value.ToShortDateString())
                    Else
                        lblPromotionRunDate.Text = row.PromoStartDate.Value.ToShortDateString()
                    End If
                Else
                    If (row.PromoEndDate.HasValue) Then
                        spanPromotionRunDate.Visible = True
                        lblPromotionRunDate.Text = String.Format("<strong>Valid Until:&nbsp;</strong>{0}<br /><br />", row.PromoEndDate.Value.ToShortDateString())
                    End If
                End If
                lblPromotionDescription.Text = row.PromoDescription
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub HandleMenuLoad()
        Dim captionID As String = ""
        Dim menuID As String = ""
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
    End Sub

    Private Sub SetPageContent()
        Dim countryCode As Int32 = AppConstants.USA_COUNTRY_CODE
        If Not (Session("CountryCode") Is Nothing) Then
            If Not (String.IsNullOrEmpty(Session("CountryCode").ToString())) Then
                countryCode = Int32.Parse(Session("CountryCode").ToString())
            End If
        End If
        If (countryCode = 208) Then
            litPromotionsHeader.Text = "<h1>Promotions</h1><p>Offers valid for US Customers only.</p><p>Contact us now to get more information about the Teledyne LeCroy special offers:</p><p>&nbsp;1-800-5-LeCroy<br />(1-800-553-2769)</p><p>or send an email to: <a href=""mailto:customercare@teledynelecroy.com"">customercare@teledynelecroy.com</a></p>"
        Else
            litPromotionsHeader.Text = "This is non-US content"
        End If
        LoadPromotions(countryCode)
    End Sub

    Private Sub LoadPromotions(ByVal countryCode As Int32)
        Dim promotions As List(Of Promotion) = GetPromotions(countryCode)
        If (promotions.Count > 0) Then
            divNoPromotionsExist.Visible = False
            divPromotionsExist.Visible = True
            rptPromotions.DataSource = promotions
            rptPromotions.DataBind()
        Else
            divPromotionsExist.Visible = False
            divNoPromotionsExist.Visible = True
        End If
    End Sub

    Private Function GetPromotions(ByVal countryCode As Int32) As IList(Of Promotion)
        Dim promotions As List(Of Promotion) = PromotionRepository.FetchPromotions(ConfigurationManager.AppSettings("ConnectionString").ToString(), _promotionsSqlString, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@COUNTRYCODE", countryCode.ToString())}).ToArray()).ToList()
        If (DoesQueryStringContainValidPromotionCategoryValues()) Then
            If (promotions.Count > 0) Then
                Return GetFilteredPromotionsForPromotionCategories(promotions.Select(Function(x) x.PromoId).Distinct().ToList(), promotions)
            End If
        End If
        Return promotions.ToList()
    End Function

    Private Function DoesQueryStringContainValidPromotionCategoryValues() As Boolean
        For Each key As String In Request.QueryString().AllKeys()
            If (String.Compare(key, CATEGORYID_QSKEY, True) = 0 Or String.Compare(key, GROUPID_QSKEY, True) = 0 Or String.Compare(key, PRODUCTSERIESID_QSKEY, True) = 0 Or String.Compare(key, PRODUCTID_QSKEY, True) = 0 Or String.Compare(key, PROTOCOLSTANDARDID_QSKEY, True) = 0) Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function GetQueryStringValue(ByVal queryStringKey As String) As Nullable(Of Int32)
        If Not (Request.QueryString(queryStringKey) Is Nothing AndAlso Not (String.IsNullOrEmpty(Request.QueryString(queryStringKey)))) Then
            Dim queryStringValue As Int32 = -1
            If (Int32.TryParse(Request.QueryString(queryStringKey), queryStringValue)) Then
                Return queryStringValue
            End If
        End If
        Return Nothing
    End Function

    Private Function GetFilteredPromotionsForPromotionCategories(ByVal promotionIds As List(Of Int32), ByVal promotions As List(Of Promotion)) As List(Of Promotion)
        If (promotionIds.Count <= 0) Then
            Return New List(Of Promotion)
        End If

        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
        Dim dalHelper As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
        dalHelper.CreateSqlInStatement(Of Int32)(promotionIds, "PROMOTIONID", sqlKeys, sqlParameters)
        If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count() > 0) Then
            Return New List(Of Promotion)
        End If
        Dim promotionCategories As List(Of PromotionCategory) = PromotionCategoryRepository.FetchPromotionCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), String.Format(_promotionCategorySqlString, sqlKeys), sqlParameters.ToArray()).ToList()
        If (promotionCategories.Count > 0) Then
            Dim categoryId As Nullable(Of Int32) = GetQueryStringValue(CATEGORYID_QSKEY)
            Dim groupId As Nullable(Of Int32) = GetQueryStringValue(GROUPID_QSKEY)
            Dim productSeriesId As Nullable(Of Int32) = GetQueryStringValue(PRODUCTSERIESID_QSKEY)
            Dim productId As Nullable(Of Int32) = GetQueryStringValue(PRODUCTID_QSKEY)
            Dim protocolStandardId As Nullable(Of Int32) = GetQueryStringValue(PROTOCOLSTANDARDID_QSKEY)

            If (categoryId.HasValue) Then
                promotionCategories = (From pc In promotionCategories
                                        Where pc.CategoryId = categoryId.Value
                                        Select pc).AsQueryable()
            End If
            If (groupId.HasValue) Then
                promotionCategories = (From pc In promotionCategories
                                        Where pc.GroupId = groupId.Value
                                        Select pc).AsQueryable()
            End If
            If (productSeriesId.HasValue) Then
                promotionCategories = (From pc In promotionCategories
                                        Where pc.ProductSeriesId = productSeriesId.Value
                                        Select pc).AsQueryable()
            End If
            If (productId.HasValue) Then
                promotionCategories = (From pc In promotionCategories
                                        Where pc.ProductId = productId.Value
                                        Select pc).AsQueryable()
            End If
            If (protocolStandardId.HasValue) Then
                promotionCategories = (From pc In promotionCategories
                                        Where pc.ProductStandardId = protocolStandardId.Value
                                        Select pc).AsQueryable()
            End If

            If (promotionCategories.Count > 0) Then
                Dim filteredPromotionIds As String = String.Join(",", promotionCategories.Select(Function(x) x.PromoId).Distinct().ToList())
                If Not (String.IsNullOrEmpty(filteredPromotionIds)) Then
                    Return (From p In promotions Where filteredPromotionIds.Contains(p.PromoId) Select p Order By p.SortId).Distinct().ToList()
                End If
            End If
        End If
        Return promotions
    End Function
#End Region
End Class