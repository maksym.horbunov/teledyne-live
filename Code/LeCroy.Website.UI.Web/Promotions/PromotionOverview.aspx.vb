Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class PromotionOverview
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Public Shared strPromoTitle As String = ""
    'Private Const LCCOUNTRY_COOKIEKEY As String = "LCCOUNTRY"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Special Offers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        Dim promoID As String = ""
        If Len(Request.QueryString("promoid")) > 0 Then
            If IsNumeric(Request.QueryString("promoid")) Then
                promoID = Request.QueryString("promoid")
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID

        Session("menuSelected") = captionID

        Dim countryCode As Int32 = AppConstants.USA_COUNTRY_CODE
        If Not (Session("CountryCode") Is Nothing) Then
            If Not (String.IsNullOrEmpty(Session("CountryCode").ToString())) Then
                countryCode = Int32.Parse(Session("CountryCode").ToString())
            End If
        End If

        If Len(getPromoTitle(promoID, countryCode).ToString) > 0 Then
            strPromoTitle = getPromoTitle(promoID, countryCode).ToString
            If Len(Functions.GetOverviewHTML(AppConstants.PROMOTION_OVERVIEW_TYPE, promoID)) > 0 Then
                lbDetails.Text = Functions.GetOverviewHTML(AppConstants.PROMOTION_OVERVIEW_TYPE, promoID)
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If
        lb_leftmenu.Text = "&nbsp;<a href='/promotions/'><img src='/images/icons/icon_small_arrow_left.gif' border='0'>&nbsp;Back to Promotions</a>"
        pn_leftmenu.Visible = True
    End Sub

    Function getPromoTitle(ByVal promoID As String, ByVal Country_ID As String) As String
        getPromoTitle = ""
        If Len(promoID) > 0 Then
            Dim content As StringBuilder = New StringBuilder()
            If Len(Country_ID) > 0 Then
                Dim ds1 As DataSet
                Dim strSQL As String = " SELECT    PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, " +
                                       " PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID FROM   PROMOTION INNER JOIN   PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID " +
                                       " WHERE     (PROMOTION_COUNTRY.WW_YN = 'y') and PROMOTION.POST_YN IN ('y','d') and PROMOTION.URL is null and PROMOTION.PROMO_ID=@PROMOID UNION " +
                                       " SELECT      PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, " +
                                       " PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID FROM  PROMOTION INNER JOIN  PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID " +
                                       " INNER JOIN COUNTRY ON PROMOTION_COUNTRY.REGION_ID = COUNTRY.REGIONID " +
                                       " WHERE COUNTRY.COUNTRY_ID = @COUNTRYID and PROMOTION.POST_YN IN ('y','d') and PROMOTION.URL is null and PROMOTION.PROMO_ID=@PROMOID UNION " +
                                       " SELECT     PROMOTION.PROMO_NAME, PROMOTION.PROMO_DESCRIPTION, PROMOTION.PROMO_START_DATE, PROMOTION.PROMO_END_DATE, " +
                                       " PROMOTION.PROMO_IMAGE, PROMOTION.SORT_ID FROM  PROMOTION INNER JOIN PROMOTION_COUNTRY ON PROMOTION.PROMO_ID = PROMOTION_COUNTRY.PROMO_ID " +
                                       " WHERE  PROMOTION.URL is null  and (PROMOTION.POST_YN IN ('y','d')) AND (PROMOTION_COUNTRY.COUNTRY_ID = @COUNTRYID) and PROMOTION.PROMO_ID=@PROMOID order by PROMOTION.SORT_ID"
                'Response.Write(strSQL)
                ' Response.End()
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROMOID", promoID.ToString()))
                sqlParameters.Add(New SqlParameter("@COUNTRYID", Country_ID.ToString()))
                ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                Dim num1 As Integer = 0
                num1 = ds1.Tables(0).Rows.Count

                If num1 > 0 Then
                    content.Append("<table border=""0"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
                    content.Append("<tr>")
                    content.Append("<td valign=""top""><br /><br />")
                    For i As Integer = 0 To num1 - 1
                        Dim dr1 As DataRow = ds1.Tables(0).Rows(i)
                        content.Append("<h1>")
                        content.Append(Me.convertToString(dr1("PROMO_NAME")))
                        content.Append("</h1>")
                        content.Append("<strong>")
                        If Not IsDBNull(dr1("PROMO_START_DATE")) Then
                            content.Append(Me.convertToString(FormatDateTime(dr1("PROMO_START_DATE"), DateFormat.ShortDate)))
                            If Not IsDBNull(dr1("PROMO_END_DATE")) Then
                                content.Append(Me.convertToString("<strong>&nbsp;-&nbsp;</strong>" + FormatDateTime(dr1("PROMO_END_DATE"), DateFormat.ShortDate)))
                                content.Append("<br /><br />")
                            End If
                        Else
                            If Not IsDBNull(dr1("PROMO_END_DATE")) Then
                                content.Append(Me.convertToString("<strong>Valid Until:&nbsp</strong>" + FormatDateTime(dr1("PROMO_END_DATE"), DateFormat.ShortDate)))
                                content.Append("<br /><br />")
                            End If
                        End If
                        content.Append("</strong>")
                        content.Append(Me.convertToString(dr1("PROMO_DESCRIPTION")))
                        content.Append("<br /><br />")
                    Next
                    content.Append("</td>")
                    content.Append("</tr>")
                    content.Append("</table>")
                End If
            End If
            getPromoTitle = content.ToString()
        End If
    End Function
End Class