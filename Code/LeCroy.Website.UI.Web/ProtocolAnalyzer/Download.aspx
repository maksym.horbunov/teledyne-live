﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="Download.aspx.vb" Inherits="LeCroy.Website.Download" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr>
            <td width="200px">&nbsp;</td>
            <td>
                <div style="width:600px;">
                <h1 style="color:#000;">Thank you for your interest in LeCroy's DDR3 memory testing solutions.</h1><p>&nbsp;</p>
                <img src="/images/protocolanalyzer/ddr3paper.jpg" width="200" height="259" align="right" />
                <h2>New DDR3 Debug Techniques</h2><h3 style="color:#000; margin:-5px 0 5px 0;">Fast Setup, Zero Calibration Time</h3>
                <strong>Roy Chestnut<br />Director, Peripherals Product Line</strong>
                <p style="width:475px;">The Complex nature of DDR3 signaling has long been a problem for engineers attempting to validate and debug DDR3 implementations. The traditional approach for debug is the use a general purpose logic analyzer with dedicated probes or interposers to tap the necessary signals.</p>
                <p>&nbsp;</p>
                <p><strong>Please enter your email address below to download<br /><em style="color:#007ac3;">New DDR3 Debug Techniques</em></strong></p>
                <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
                    <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="50" TabIndex="1" ValidationGroup="vgSubmit" Width="200px" />
                    <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" TabIndex="2" Text="Submit" ValidationGroup="vgSubmit" /><br />
                    <span style="color: #ff0000;"><asp:CustomValidator ID="cvEmailAddress" runat="server" Display="Dynamic" EnableClientScript="false" ErrorMessage="Please enter a valid email address" ValidationGroup="vgSubmit" /></span>
                </asp:Panel>
                <div id="divPostSubmit" runat="server" visible="false">
                     Thank you! 
                     <asp:Button ID="btnDownload" runat="server" CausesValidation="false" Text="Download Paper" />
                </div>
                <p>For additional information contact</p><p><strong>LeCroy</strong><br />1-800-909-7211 or 408-653-1262;<br /><a href="mailto:PSGsales@teledynelecroy.com">PSGsales@teledynelecroy.com</a></p>
                </div>
            </td>
        </tr>
        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
    </table>
</asp:Content>