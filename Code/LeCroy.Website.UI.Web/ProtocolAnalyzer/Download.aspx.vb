﻿Public Class Download
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Protocol Analyzers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            ValidateUserNoRedir()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvEmailAddress_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmailAddress.ServerValidate
        If (String.IsNullOrEmpty(txtEmailAddress.Text)) Then
            cvEmailAddress.ErrorMessage = "*Email address is required"
            args.IsValid = False
            Return
        End If
        If Not (BLL.Utilities.checkValidEmail(txtEmailAddress.Text)) Then
            cvEmailAddress.ErrorMessage = "*Please provide a valid email address"
            args.IsValid = False
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            Dim contactId As String = String.Empty
            If Not Session("ContactID") Is Nothing And Len(Session("ContactID")) > 0 Then
                contactId = Session("ContactID")
            End If
            If (Functions.InsertSubscription(72, 208, contactId, txtEmailAddress.Text.Trim())) Then
                pnlSubmit.Visible = False
                divPostSubmit.Visible = True
            End If
        End If
    End Sub

    Private Sub btnDownload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDownload.Click
        Response.Redirect("https://cdn.teledynelecroy.com/files/whitepapers/new_ddr3_debug_techniques.pdf")
    End Sub
#End Region

#Region "Page Methods"
#End Region
End Class