Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class ProtocolAnalyzer_ProtocolOverview
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, ds1, ds2, dspartnmers As DataSet
    'Public mseries As String = ""
    'Dim standardid As String = ""
    Dim firstmenuid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim promoImages As String = ""
    Dim seriesName As String = ""
    Dim seriesExplorerText As String = ""
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Dim hasOptions As Boolean = False
    Dim hasKeyFeatures As Boolean = False
    Dim hasJammer As Boolean = False
    Dim menuURL As String = ""
    Dim partnerid As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Private _seriesId As Int32 = 0
    Private _standardId As Int32 = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Protocol Analyzers"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HandleRoute()
        Dim imageIndex As Integer = 0
        Dim captionID As String = ""
        Dim menuID As String
        Dim SeriesPartnerDescr As String = ""

        captionID = AppConstants.PROTOCOL_CAPTION_ID
        menuID = getDefaultMenu(captionID)

        If (_seriesId <= 0) Then
            ''** mseries
            If Len(Request.QueryString("seriesid")) = 0 Then
                Response.Redirect("Default.aspx")
            Else
                If IsNumeric(Request.QueryString("seriesid")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("seriesid")), ",") Then
                        _seriesId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("seriesid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("seriesid")), ",") + 1)
                    Else
                        _seriesId = SQLStringWithOutSingleQuotes(Request.QueryString("seriesid"))
                    End If
                    If Not CheckSeriesExists(_seriesId.ToString(), 19) Then
                        Response.Redirect("Default.aspx")
                    End If

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seriesId)
                    If (productSeries Is Nothing) Then Response.Redirect("Default.aspx")

                    Dim protocolStandardSeries As ProtocolStandardSeries = New ProtocolStandardSeries()
                    If ((Not Session("StandardIdForFirstOrDefault")) Is Nothing AndAlso Int32.TryParse(Session("StandardIdForFirstOrDefault").ToString(), _standardId)) Then
                        protocolStandardSeries = ProtocolStandardSeriesRepository.GetProtocolStandardSeriesByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seriesId).Where(Function(x) x.ProtocolStandardId = _standardId).FirstOrDefault()
                    Else
                        protocolStandardSeries = ProtocolStandardSeriesRepository.GetProtocolStandardSeriesByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seriesId).FirstOrDefault()
                        If Not (protocolStandardSeries Is Nothing) Then _standardId = protocolStandardSeries.ProtocolStandardId
                    End If
                    If (protocolStandardSeries Is Nothing) Then Response.Redirect("Default.aspx")

                    Dim protocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), protocolStandardSeries.ProtocolStandardId)
                    If (protocolStandard Is Nothing) Then Response.Redirect("Default.aspx")

                    If (String.IsNullOrEmpty(protocolStandard.SeoValue) Or String.IsNullOrEmpty(productSeries.SeoValue)) Then
                        ' Fall through
                    Else
                        CreateMeta(protocolStandard.SeoValue, productSeries.SeoValue)
                        Response.Redirect(String.Format("~/protocolanalyzer/{0}/{1}", protocolStandard.SeoValue, productSeries.SeoValue))
                    End If
                Else
                    Response.Redirect("Default.aspx")
                End If
            End If
        End If

        '** partnerid
        If Len(Request.QueryString("pid")) > 0 Then
            If IsNumeric(Request.QueryString("pid")) Then
                partnerid = Request.QueryString("pid")
            End If
        End If
        '** standardID
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        'sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _seriesId.ToString()))
        'standardid = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), "Select protocol_standard_id from PROTOCOL_STANDARD_SERIES where  PRODUCT_SERIES_ID = @PRODUCTSERIESID", sqlParameters.ToArray())

        'If _standardId Is Nothing Or _standardId = "" Then
        If (_standardId <= 0) Then
            Response.Redirect("Default.aspx")
        End If

        If Not (FeProductManager.CanProtocolShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _standardId, _seriesId)) Then
            Response.Redirect("~/")
        End If

        Session("standardid") = _standardId
        Session("cataid") = 19
        Session("menuSelected") = captionID
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        If Not Me.IsPostBack Then
            ucResources.ProtocolStandardId = _standardId
            ucResources.ProductSeriesId = _seriesId
            getProductLine()
            getSeriesOverview()
            getProductDetail()
            getKeyFeatures()
            getSpecs()
            getOptions()
            GetJammer()
            lblTabsToShow.Text = tabsToShow.ToString()


            If hasKeyFeatures Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.KEY_FEATURES_TAB & "');</script>"
            ElseIf hasDetail Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
            ElseIf hasOverview Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            ElseIf hasJammer Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#jammer');</script>"
            Else
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
            End If

            Functions.CreateMetaTags(Me.Header, _seriesId.ToString(), AppConstants.SERIES_OVERVIEW_TYPE)

            strSQL = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID=@PRODUCTSERIESID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _seriesId.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                seriesName = dr("NAME").ToString()
                dataSheetURL = Me.convertToString(dr("URL_DATASHEET"))
                specificationURL = Me.convertToString(dr("URL_SPEC"))
                promoImages = Functions.GetPromoImagesForSeries(_seriesId, rootDir)
                seriesExplorerText = Me.convertToString(dr("SHORT_NAME"))
                lbSeriesTileTop.Text = seriesName
                lbSeriesTileBelow.Text = "Explore " + seriesExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + seriesExplorerText + "' >"
                lbSeriesImage.Text = "<img src='" + rootDir + dr("TITLE_IMAGE").ToString() + "' width='260' />"
                If Len(partnerid) > 0 Then
                    strSQL = "Select PARTNERS_SERIES.DESCRIPTION FROM  PARTNERS INNER JOIN PARTNERS_SERIES ON PARTNERS.PARTNER_ID = PARTNERS_SERIES.PARTNER_ID " +
                             " WHERE  PARTNERS_SERIES.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND PARTNERS.PARTNER_ID=@PARTNERID AND PARTNERS.ACTIVE_YN = 'y' ORDER BY PARTNERS.SORT_ID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _seriesId.ToString()))
                    sqlParameters.Add(New SqlParameter("@PARTNERID", partnerid))
                    ds2 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If ds2.Tables(0).Rows.Count > 0 Then
                        Dim dr1 As DataRow = ds2.Tables(0).Rows(0)
                        If Len(dr1("DESCRIPTION")) > 0 Then
                            SeriesPartnerDescr = dr1("DESCRIPTION")
                        End If
                    End If
                End If
                If Len(SeriesPartnerDescr) > 0 Then
                    lbSeriesDesc.Text = SeriesPartnerDescr.ToString
                Else
                    lbSeriesDesc.Text = dr("DESCRIPTION").ToString()
                End If
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Protocol Analyzer - " + dr("NAME").ToString() ' TODO: Change to pull from cms overview title/meta/key
            End If

            If Not Session("CountryCode") Is Nothing Then
                If Len(Session("CountryCode").ToString) > 0 Then
                    If Session("CountryCode").ToString = "208" Then
                        ucResources.ShowBuy = True
                    End If
                    If Not (ContactOfficeXRefProductRepository.DoesXRefExistForSeriesAndCountryIdForWhereToBuy(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seriesId, Int32.Parse(Session("CountryCode").ToString()))) Then
                        ucResources.ShowWhereToBuy = False
                    End If
                End If
            End If

            If Functions.SeriesHasRequestQuoteLInk(_seriesId).ToString.Length > 0 Then
                ucResources.ShowRequestQuote = True
            End If
            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _seriesId.ToString()).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, _seriesId.ToString())
            End If
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _standardId = xParam.ProtocolStandardId

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If Not (param2.Key Is Nothing) Then
                        Dim yParam As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value)
                        If Not (yParam Is Nothing) Then
                            _seriesId = yParam.ProductSeriesId
                            CreateMeta(param1.Value, param2.Value)
                            Return
                        End If
                    End If
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue1 As String, ByVal seoValue2 As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/protocolanalyzer/{1}/{2}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue1, seoValue2)
                head.Controls.Add(link)
                Dim meta1 As New HtmlMeta()
                meta1.Name = "standard-id"
                meta1.Content = _standardId
                head.Controls.Add(meta1)
                Dim meta2 As New HtmlMeta()
                meta2.Name = "series-id"
                meta2.Content = _seriesId
                head.Controls.Add(meta2)
            End If
        End If
    End Sub

    Private Sub getProductDetail()
        Dim ProductDetails As String = ""
        'get partners overview
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(partnerid) > 0 Then
            strSQL = "Select PARTNERS.PARTNER_ID FROM  PARTNERS INNER JOIN PARTNERS_SERIES ON PARTNERS.PARTNER_ID = PARTNERS_SERIES.PARTNER_ID " +
                     " WHERE  PARTNERS_SERIES.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND PARTNERS.PARTNER_ID=@PARTNERID AND PARTNERS.ACTIVE_YN = 'y' ORDER BY PARTNERS.SORT_ID"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _seriesId.ToString()))
            sqlParameters.Add(New SqlParameter("@PARTNERID", partnerid))
            dspartnmers = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dspartnmers.Tables(0).Rows.Count > 0 Then
                For Each dr1 As DataRow In dspartnmers.Tables(0).Rows
                    strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID =@ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@ID", dr1("PARTNER_ID").ToString))
                    sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PARTNERS_OVERVIEW_TYPE))
                    ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim dr2 As DataRow = ds.Tables(0).Rows(0)
                        If Len(dr2("OVER_VIEW_DETAILS").ToString()) > 0 Then
                            ProductDetails += dr2("OVER_VIEW_DETAILS").ToString()
                        End If
                    End If
                Next
            End If
        Else
            ' get protocol overview
            strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", _seriesId.ToString()))
            sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If Len(dr("OVER_VIEW_DETAILS").ToString()) > 0 Then

                    ProductDetails += dr("OVER_VIEW_DETAILS").ToString()
                End If
            End If
        End If
        If Len(ProductDetails) > 0 Then
            Me.lblProductDetail.Text = ProductDetails
            hasDetail = True
            tabsToShow.Append("<li><a href='#product_details'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
        End If

    End Sub
    Private Sub getSpecs()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _seriesId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_SPECS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblSpecs.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
    End Sub

    Private Sub getSeriesOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _standardId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.STANDARD_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            hasOverview = True
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If

    End Sub

    Private Sub getOptions()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        ' Response.Write(strSQL)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _seriesId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_OPTIONS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)

            hasOptions = True
            If _standardId = 3 Or _standardId = 13 Or _standardId = 17 Then
                Me.lblProbes.Text = dr("OVER_VIEW_DETAILS").ToString()
                tabsToShow.Append("<li><a href=""#probes""><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_probes_off.gif' id='probes' border='0'></a></li>")
            Else
                Me.lblOptions.Text = dr("OVER_VIEW_DETAILS").ToString()
                tabsToShow.Append("<li><a href=""#options""><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_options_off.gif' id='options' border='0'></a></li>")
            End If
        End If

    End Sub

    Private Sub GetJammer()
        Dim cmsOverview As LeCroy.Library.Domain.Common.DTOs.CmsOverview = LeCroy.Library.DAL.Common.CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seriesId, AppConstants.PROMOTION_OVERVIEW_TYPE)
        If Not (cmsOverview Is Nothing) Then
            hasJammer = True
            litJammer.Text = cmsOverview.OverviewDetails
            tabsToShow.AppendFormat("<li><a href=""#jammer""><img src=""{0}/images/tabs/tabs-infusion-off.gif"" id=""jammer"" border=""0"" /></a></li>", rootDir)
        End If
    End Sub

    Private Sub getProductLine()
        '*left menu for series
        Dim sqlGetSeries As String
        Dim ds As New DataSet
        sqlGetSeries = "SELECT NAME, PROPERTYGROUPID, [SeoValue] FROM PROTOCOL_STANDARD WHERE PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _standardId.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(0)("SeoValue").ToString())) Then
                lbSeries1.Text = String.Format("<a href='/protocolanalyzer/{0}'>{1}</a>", ds.Tables(0).Rows(0)("SeoValue").ToString(), ds.Tables(0).Rows(0)("NAME").ToString())
            Else
                lbSeries1.Text = "<a href='/protocolanalyzer/protocolstandard.aspx?standardID=" + _standardId.ToString() + menuURL + "'> " +
                            ds.Tables(0).Rows(0)("NAME").ToString() + " </a> "
            End If
            'lblSoftware.Text = "<li class='software'><a href='" + rootDir + "/support/softwaredownload/documents.aspx?standardid=" + standardid.ToString + "'>Analysis Software Download</a></li>"
        End If

        '*left menu for products
        Dim sqlGetProducts As String
        Dim productID As String
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String
        Dim productLine As String
        sqlGetProducts = "SELECT DISTINCT a.PRODUCT_SERIES_ID, a.NAME, a.DESCRIPTION,a.PRODUCTIMAGEDESC, a.sortid, a.SeoValue as SeriesSeoValue, c.SeoValue as StandardSeoValue " +
                         " FROM PRODUCT_SERIES a, PROTOCOL_STANDARD_SERIES b, PROTOCOL_STANDARD c WHERE " +
                         " a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
                         " AND a.DISABLED='n' AND b.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID AND b.PROTOCOL_STANDARD_ID = c.PROTOCOL_STANDARD_ID Order by a.sortid "
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _standardId.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        products = ""
        productLine = ""
        If pNum > 0 Then
            For i = 0 To pNum - 1
                Dim modelID As String = ds.Tables(0).Rows(i)("NAME").ToString()
                productID = Int32.Parse(ds.Tables(0).Rows(i)("PRODUCT_SERIES_ID").ToString())
                If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SeriesSeoValue").ToString())) And Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("StandardSeoValue"))) Then
                    products = products + String.Format("<li{0}><a href='/protocolanalyzer/{1}/{2}'>{3}</a></li>", IIf(productID = _seriesId, " class='current'", String.Empty), ds.Tables(0).Rows(i)("StandardSeoValue"), ds.Tables(0).Rows(i)("SeriesSeoValue").ToString(), modelID)
                Else
                    products = products + String.Format("<li{0}><a href='/protocolanalyzer/protocoloverview.aspx?seriesid={1}{2}'>{3}</a></li>", IIf(productID = _seriesId, " class='current'", String.Empty), productID, menuURL, modelID)
                End If
                productLine = productLine + "<div class='quickBox'>"
                If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SeriesSeoValue").ToString())) And Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("StandardSeoValue"))) Then
                    productLine = productLine + String.Format("<a href='/protocolanalyzer/{0}/{1}'><strong>{2}</strong></a>&nbsp;&nbsp;{3}", ds.Tables(0).Rows(i)("StandardSeoValue").ToString(), ds.Tables(0).Rows(i)("SeriesSeoValue").ToString(), modelID, ds.Tables(0).Rows(i)("PRODUCTIMAGEDESC").ToString())
                Else
                    productLine = productLine + "<a href='/protocolanalyzer/protocoloverview.aspx?seriesid=" + productID + menuURL + "'><strong>" + modelID + "</strong></a> " +
                                                                "&nbsp;&nbsp;" + ds.Tables(0).Rows(i)("PRODUCTIMAGEDESC").ToString()
                End If
                productLine = productLine + "</div>"
            Next
            lbProduct1.Text = products
            lbQuickSpecs.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub

    Private Sub getKeyFeatures()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _seriesId.ToString()))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.KEY_FEATURES_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            hasKeyFeatures = True
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblKeyFeatures.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#key_features'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/key_features_off.gif' id='key_features' border='0'></a></li>")
        End If
    End Sub
End Class