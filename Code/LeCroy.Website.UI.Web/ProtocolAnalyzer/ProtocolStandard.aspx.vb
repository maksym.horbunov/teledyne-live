Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class ProtocolAnalyzer_ProtocolStandard
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Dim firstmenuid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim promoImages As String = ""
    Dim standardName As String = ""
    Dim standardExplorerText As String = ""
    Public softwareDownloadURL As String = ""
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Private _standardId As Int32 = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Protocol Analyzers"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HandleRoute()
        Dim captionID As String = ""
        Dim menuID As String = Request.QueryString("mid")

        If (_standardId <= 0) Then
            If Len(Request.QueryString("standardID")) = 0 Then
                Response.Redirect("default.aspx")
            Else
                If IsNumeric(Request.QueryString("standardID")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("standardid")), ",") Then
                        _standardId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("standardid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("standardid")), ",") + 1)
                    Else
                        _standardId = SQLStringWithOutSingleQuotes(Request.QueryString("standardid"))
                    End If

                    Session("StandardIdForFirstOrDefault") = _standardId
                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _standardId)
                    If Not (protocolStandard Is Nothing) Then
                        If Not (String.IsNullOrEmpty(protocolStandard.SeoValue)) Then
                            CreateMeta(protocolStandard.SeoValue)
                            Response.Redirect(String.Format("~/protocolanalyzer/{0}", protocolStandard.SeoValue))
                        End If
                    End If
                Else
                    Response.Redirect("default.aspx")
                End If
            End If
        End If

        captionID = AppConstants.PROTOCOL_CAPTION_ID

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        If Not (FeProductManager.CanProtocolShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _standardId, Nothing)) Then
            Response.Redirect("~/")
        End If

        If Not Me.IsPostBack Then
            ucResources.ProtocolStandardId = _standardId
            Functions.CreateMetaTags(Me.Header, _standardId, AppConstants.STANDARD_OVERVIEW_TYPE)
            getOverview()

            ' HACK!
            If (_standardId = 18) Then
                AddHackedInTabs()
                lblTabsToShow.Text = tabsToShow.ToString()
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            Else
                getProductLine()
                getSpecs()
                lblTabsToShow.Text = tabsToShow.ToString()
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
            End If

            '*left menu for series
            Dim sqlGetSeries As String
            Dim ds As New DataSet

            sqlGetSeries = "SELECT * From PROTOCOL_STANDARD WHERE PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _standardId))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                'If (String.Compare(convertToString(ds.Tables(0).Rows(0)("DISABLED")), "y", True) = 0) Then
                '    Response.Redirect("default.aspx")
                'End If
                standardName = ds.Tables(0).Rows(0)("NAME").ToString()
                dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
                specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
                promoImages = Functions.GetPromoImagesForStandard(_standardId, rootDir)
                standardExplorerText = Me.convertToString(ds.Tables(0).Rows(0)("SHORT_NAME"))
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Protocol Analyzer - " + standardName
                lbSeriesTileTop.Text = standardName
                lbSeriesTileBelow.Text = "Explore " + standardExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + standardExplorerText + "' >"
                lbSeries1.Text = "<a href=""#"">" & standardName & "</a>"
                lbSeriesImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString() + "' width='260' />"
                lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Protocol Analyzer - " + standardName
                'lblSoftware.Text = "<li class='software'><a href='" + rootDir + "/support/softwaredownload/documents.aspx?standardid=" + standardid.ToString + "'>Analysis Software Download</a></li>"

                'Buy Now
                'If Not Request.Cookies("LCCOUNTRY") Is Nothing Then
                '    If Len(Request.Cookies("LCCOUNTRY").Value.ToString) > 0 Then
                '        If Request.Cookies("LCCOUNTRY").Value.ToString = "208" Then
                '            ucResources.ShowBuy = True
                '        End If
                '    End If
                'End If
            Else
                Response.Redirect("default.aspx")
            End If
            'If Not dataSheetURL = Nothing And Len(dataSheetURL) > 0 Then
            '    lblDataSheet.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dataSheetURL.ToLower() & "'>Datasheet</a></li>"
            'End If
            'If Not specificationURL = Nothing And Len(specificationURL) > 0 Then
            '    lblSpecifications.Text = "<li class='pdf'><a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & specificationURL.ToLower() & "'>Specifications</a></li>"
            'End If

            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_STANDARD.ToString, _standardId.ToString).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_STANDARD.ToString, _standardId.ToString)
            End If
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False

        End If

    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _standardId = xParam.ProtocolStandardId
                    Session("StandardIdForFirstOrDefault") = _standardId
                    CreateMeta(param1.Value)
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/protocolanalyzer/{1}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue)
                head.Controls.Add(link)
                Dim meta As New HtmlMeta()
                meta.Name = "standard-id"
                meta.Content = _standardId
                head.Controls.Add(meta)
            End If
        End If
    End Sub

    Private Sub getOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _standardId))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.STANDARD_OVERVIEW_TYPE))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            If Me.lblOverview.Text.Length > 0 Then
                tabsToShow.Append("<li><a href='#overview'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
            End If
        End If
    End Sub
    Private Sub getProductLine()
        '*left menu for series
        'mseries = Request("mseries").ToString()

        '*left menu for products
        Dim sqlGetProducts As String
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String
        Dim productLine As String
        sqlGetProducts = "SELECT DISTINCT a.PRODUCT_SERIES_ID, a.NAME, a.DESCRIPTION,a.PRODUCTIMAGEDESC, a.sortid, a.SeoValue as SeriesSeoValue, c.SeoValue as StandardSeoValue " &
        " FROM PRODUCT_SERIES a, PROTOCOL_STANDARD_SERIES b, [PROTOCOL_STANDARD] c " &
        " WHERE a.PRODUCT_SERIES_ID = b. PRODUCT_SERIES_ID AND a.DISABLED='n' AND b.PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID and b.PROTOCOL_STANDARD_ID = c.PROTOCOL_STANDARD_ID ORDER by a.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _standardId))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetProducts, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        products = ""
        productLine = ""
        If pNum > 0 Then
            For i = 0 To pNum - 1
                Dim seriesName As String = ds.Tables(0).Rows(i)("NAME").ToString()
                Dim seriesid As String = ds.Tables(0).Rows(i)("PRODUCT_SERIES_ID").ToString()
                If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SeriesSeoValue").ToString())) And Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("StandardSeoValue"))) Then
                    products = products + String.Format("<li><a href='/protocolanalyzer/{0}/{1}'>{2}</a></li>", ds.Tables(0).Rows(i)("StandardSeoValue").ToString(), ds.Tables(0).Rows(i)("SeriesSeoValue").ToString(), seriesName)
                Else
                    products = products + "<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid=" + seriesid + menuURL + "'> " + seriesName + "</a></li>"
                End If

                productLine = productLine + "<div class='quickBox'>"
                If Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("SeriesSeoValue").ToString())) And Not (String.IsNullOrEmpty(ds.Tables(0).Rows(i)("StandardSeoValue"))) Then
                    productLine = productLine + String.Format("<a href='/protocolanalyzer/{0}/{1}'><strong>{2}</strong></a>&nbsp;&nbsp;{3}", ds.Tables(0).Rows(i)("StandardSeoValue").ToString(), ds.Tables(0).Rows(i)("SeriesSeoValue").ToString(), seriesName, ds.Tables(0).Rows(i)("PRODUCTIMAGEDESC").ToString())
                Else
                    productLine = productLine + "<a href='/protocolanalyzer/protocoloverview.aspx?seriesid=" + seriesid + menuURL + "'><strong>" + seriesName + "</strong></a> " +
                                            "&nbsp;&nbsp;" + ds.Tables(0).Rows(i)("PRODUCTIMAGEDESC").ToString()
                End If

                'productLine = productLine + "<div class='quickSpecs'>"

                'sqlGetQuickSpecs = "select * from SERIES_PROPERTY where PRODUCT_SERIES_ID=" + seriesid

                'quickSpecsDs = DbHelperSQL.Query(sqlGetQuickSpecs)
                'specsNum = quickSpecsDs.Tables(0).Rows.Count
                'specs = ""
                'If specsNum > 0 Then
                '    productLine = productLine + "<div class='title'>Quick Specs</div>"
                '    productLine = productLine + "<div id='specs'>"
                '    productLine = productLine + "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
                '    For j = 0 To specsNum - 1
                '        specs = specs + "<tr>"
                '        specs = specs + "<td width='35%' class='cell'>" + quickSpecsDs.Tables(0).Rows(j)("PROPERTYNAME").ToString() + "</td>"
                '        specs = specs + "<td width='65%' class='cell'>" + quickSpecsDs.Tables(0).Rows(j)("PROPERTY_VALUE").ToString() + "</td>"
                '        specs = specs + "</tr>"
                '    Next
                '    productLine = productLine + specs
                '    productLine = productLine + "</table>"
                '    productLine = productLine + "</div>"
                'Else
                '    productLine = productLine + "<div>&nbsp;</div>"
                'End If

                'productLine = productLine + "</div>"
                productLine = productLine + "</div>"

            Next
            lbProduct1.Text = products
            lbQuickSpecs.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub

    Private Sub getSpecs()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", _standardId))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_STANDARD_SPECS))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblSpecs.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
    End Sub

    Private Sub AddHackedInTabs()
        Dim analyzerIds As List(Of Int32) = ConfigurationManager.AppSettings("NvmAnalyzers").ToString().Split(",").[Select](AddressOf Int32.Parse).ToList()
        Dim exerciserIds As List(Of Int32) = ConfigurationManager.AppSettings("NvmExercisers").ToString().Split(",").[Select](AddressOf Int32.Parse).ToList()
        Dim complianceIds As List(Of Int32) = ConfigurationManager.AppSettings("NvmCompliance").ToString().Split(",").[Select](AddressOf Int32.Parse).ToList()
        TabHack(litAnalyzers, analyzerIds, "analyzers") 'New List(Of Int32)({219, 357, 467, 358, 445, 532}), "analyzers")
        TabHack(litExerciser, exerciserIds, "exerciser") 'New List(Of Int32)({525, 526, 468}), "exerciser")
        TabHack(litCompliance, complianceIds, "compliance") 'New List(Of Int32)({111}), "compliance")
        Dim addedOn As List(Of String) = ConfigurationManager.AppSettings("NvmAddedOn").ToString().Split(";").ToList()
        For Each ao As String In addedOn
            Dim split As String() = ao.Split("|")
            lbProduct1.Text += String.Format("<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid={0}{1}'>{2}</a></li>", split(0), menuURL, split(1))
        Next
        'lbProduct1.Text += String.Format("<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid=536{0}'>NVMe Testing</a></li>", menuURL)
        'lbProduct1.Text += String.Format("<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid=410{0}'>BitTracer</a></li>", menuURL)
        'lbProduct1.Text += String.Format("<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid=397{0}'>PCI Express host interface SSD/Drive Decode Support</a></li>", menuURL)
    End Sub

    Private Sub TabHack(ByVal lit As Literal, ByVal seriesIds As List(Of Int32), ByVal keyword As String)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@ID", _standardId), New SqlParameter("@KEYWORDS", keyword)})
        Dim cmsOverview As CmsOverview = CmsOverviewRepository.FetchCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [CMS_OVERVIEW] WHERE [ID] = @ID AND [OVERVIEW_TYPE_ID] = 19 AND [KEYWORDS] = @KEYWORDS", sqlParameters.ToArray()).FirstOrDefault()
        lit.Text = cmsOverview.OverviewDetails

        Dim sqlKeys As String = String.Empty
        sqlParameters = New List(Of SqlParameter)
        CreateSqlInStatement(Of Int32)(seriesIds, "ID", sqlKeys, sqlParameters)
        Dim series As StringBuilder = New StringBuilder()
        Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), String.Format("SELECT DISTINCT a.PRODUCT_SERIES_ID,a.NAME,a.DESCRIPTION,a.PRODUCTIMAGEDESC,a.SORTID FROM [PRODUCT_SERIES] a, [PROTOCOL_STANDARD_SERIES] b WHERE a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID AND a.DISABLED='n' AND b.PROTOCOL_STANDARD_ID=18 AND b.PRODUCT_SERIES_ID IN ({0}) ORDER BY a.SORTID", sqlKeys), sqlParameters.ToArray())
        If (productSeries.Count > 0) Then
            For Each ps As ProductSeries In productSeries
                series.AppendFormat("<li><a href='/protocolanalyzer/protocoloverview.aspx?seriesid={0}{1}'>{2}</a></li>", ps.ProductSeriesId, menuURL, ps.Name)
            Next
            lbProduct1.Text += series.ToString()
            tabsToShow.AppendFormat("<li><a href='#{0}'><img src='{1}/images/tabs/tabs_{0}_on.gif' id='{0}' border='0'></a></li>", keyword, rootDir)
        End If
    End Sub

    Private Sub CreateSqlInStatement(Of T)(ByVal valuesToIterate As List(Of T), ByVal keyName As String, ByRef sqlKeys As String, ByRef sqlParameters As List(Of SqlParameter))
        Dim keyNames As String = String.Empty
        Dim sqlParams As List(Of SqlParameter) = New List(Of SqlParameter)
        For i = 0 To valuesToIterate.Count - 1
            keyNames += String.Format("@{0}{1},", keyName, i)
            AppendToSqlParameterList(sqlParams, New KeyValuePair(Of String, String)(String.Format("@{0}{1}", keyName, i), valuesToIterate(i).ToString()))
        Next
        If Not (String.IsNullOrEmpty(keyNames)) Then
            keyNames = keyNames.Substring(0, keyNames.Length - 1)
        End If
        sqlKeys = keyNames
        sqlParameters.AddRange(sqlParams)
    End Sub

    Private Sub AppendToSqlParameterList(ByRef sqlParameters As List(Of SqlParameter), ByVal sqlParameter As KeyValuePair(Of String, String))
        sqlParameters.Add(New SqlParameter(sqlParameter.Key, sqlParameter.Value))
    End Sub
End Class