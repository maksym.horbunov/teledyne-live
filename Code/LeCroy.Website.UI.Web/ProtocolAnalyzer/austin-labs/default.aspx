<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.ProtocolAnalyzer_AustinLabs" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop" style="background-image:none;background-color:#f5f5f5;height:420px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="370" valign="top">
                    
                    <p style="padding-top:55px; font-size:14px;">Austin Labs is a leading provider of testing and training services. We focus on server, storage, and networking interfaces and protocols. Our engineers and trainers are experts in SAS, SCSI, RAID, iSCSI, SATA, SAS, FC, FCoE, PCIe, and NVMe.</p><p style="font-size:14px;">Our engineers helped develop some of the industry�s key technologies and continue to have a vigorous passion for improving products and sharing their knowledge. This experience and enthusiasm translates into the highest quality testing and training services possible.</p>
                </td>
                <td width="248" valign="top"><img src="<%=rootDir %>/images/austin-labs-01.png" width="242" height="278" border="0" /></td>
                <td width="203" valign="top">
                    <div class="subNav" style="width: 230px;background-image:none;">
                        <ul style="padding-top: 15px;">
                            <li><a href="<%=rootDir %>/doc/austin-labs-testing-and-training">Austin Labs Testing and Training</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-fibre-channel">Austin Labs Fibre Channel</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-fibre-channel-over-ethernet">Austin Labs Fibre Channel over Ethernet</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-nvme">Austin Labs NVME</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-nvme-over-fibre-channel">Austin Labs NVME over Fibre Channel</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-nvme-over-roce">Austin Labs NVME over RoCE</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-nvme-over-tcp">Austin Labs NVMe over TCP</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-pcie-compliance-testing">Austin Labs PCIe Compliance Testing</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-pcie-intensive">Austin Labs PCIe - One-Day Intensive</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-pcie-protocol">Austin Labs PCIe Protocol</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-sas">Austin Labs SAS</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-iscsi">Austin Labs iSCSI</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-intro-usb">Austin Labs Intro to USB</a></li>
                            <li><a href="<%=rootDir %>/doc/austin-labs-usb4">Austin Labs USB4&trade;</a></li>
  </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                    <p style="font-size:14px;line-height:18px;">Teledyne LeCroy�s Austin Labs is the premier third-party test and validation center for servers, storage, and network devices. With decades of testing experience, the lab provides customized services to help our customers deliver fully tested products to market on time and within budget. Experience the best test equipment available including protocol analyzers, jammers, exercisers, and protocol compliance with this new Teledyne LeCroy test and validation team. Teledyne LeCroy�s Austin Labs is designed for highly accelerated testing, interoperability, and validation of enterprise products and solutions. At Austin Labs, we are very proud of the results we have achieved. The testing value we provide at Austin Labs is unmatched. We are confident that you will not find comparable technical talent or comparable results anywhere in our industry.</p>
				    <h3 style="font-size:16px;line-height:18px;">Benefits</h3>
                    <ul style="margin-left:-25px;">
                        <li style="font-size:14px;">Top notch test engineers with decades of industry experience</li>
                        <li style="font-size:14px;">Intense focus on working with our customers to help make your products the best they can be.</li>
                        <li style="font-size:14px;">A synergistic work environment including Testing, Training, and Tools</li>
                        <li style="font-size:14px;">Completely confidential results</li>
                        <li style="font-size:14px;">World Class testing facility</li>
                        <li style="font-size:14px;">Real world test environments using the latest hardware and software</li>
                        <li style="font-size:14px;">Teledyne LeCroy equipment including Analyzers, Jammers, and Exercisers</li>
                        <li style="font-size:14px;">Certification and compliance testing for storage and network protocols</li>
                        <li style="font-size:14px;">Complete analysis and Debug of issues including protocol traces</li>
                        <li style="font-size:14px;">Complete product evaluation from a user perspective as well as the vision of an engineer</li>
                        <li style="font-size:14px;">Ability to provide custom reports for industry analysis</li>
                    </ul>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top">
<table cellspacing="0" cellpadding="10" border="0" width="400">
    <tr>
        <td colspan="2" align="center"  style="background-color:#8aa3d1;color:#ffffff;font-size:18px;">What do we Test?</td>
    </tr>
    <tr style="background-color:#f5f5f5;font-size:14px;">
        <td align="center">Servers</td>
        <td align="center">Data Integrity</td>
    </tr>
    <tr style="background-color:#c8c8c8;font-size:14px;">
        <td align="center">Storage</td>
        <td align="center">Protocol Compliance</td>
    </tr>
    <tr style="background-color:#f5f5f5;font-size:14px;">
        <td align="center">Switches</td>
        <td align="center">Performance</td>
    </tr>
    <tr style="background-color:#c8c8c8;font-size:14px;">
        <td align="center">Networking</td>
        <td align="center">Interoperability</td>
    </tr>
    <tr style="background-color:#f5f5f5;font-size:14px;">
        <td align="center">Solutions</td>
        <td align="center">Error Injection</td>
    </tr>
    <tr style="background-color:#c8c8c8;font-size:14px;">
        <td align="center">NVMe/SSD</td>
        <td align="center">Product Specification</td>
    </tr>
    <tr style="background-color:#f5f5f5;font-size:14px;">
        <td align="center">OpenStack</td>
        <td align="center">Compliance</td>
    </tr>
</table>


                </td>
            </tr>
        </table>
        <div style="background-color:#f5f5f5;border-radius:10px;">
           <h1 style="padding-top:20px; text-align:center;">Austin Labs - Why are we different?</h1>
            <div style="display:inline-block; width:275px;vertical-align:top;padding-left:10px;"><h2 style="color:#000000;">Attitude</h2>
                              <p style="padding-left:10px; font-size:14px;">We will find serious flaws and bugs with our testing</p>
                              <p style="padding-left:10px; font-size:14px;">We will help you understand how to isolate and fix these problems</p>
                              <p style="padding-left:10px; font-size:14px;">We are your partner</p></div>
            <div style="display:inline-block; width:275px;vertical-align:top;padding-left:25px"><h2 style="color:#000000;">Approach</h2>
                              <p style="padding-left:10px; font-size:14px;">Our focus is on the entire product from documentation to critical issues.</p>
                              <p style="padding-left:10px; font-size:14px;">We specialize in data corruption, data loss, disruptions</p>
                              <p style="padding-left:10px; font-size:14px;">We are protocol experts and have the tools to test protocol compliance</p>
                              <p style="padding-left:10px; font-size:14px;">We are attentive to issues that get overlooked</p></div>
            <div style="display:inline-block; width:275px;vertical-align:top;padding-left:25px"><h2 style="color:#000000;">Quality</h2>
                              <p style="padding-left:10px; font-size:14px;">We use Teledyne LeCroy test and analysis tools along with industry tools</p>
                              <p style="padding-left:10px; font-size:14px;">We supplement internal testing and provide an external validation</p>
                              <p style="padding-left:10px; font-size:14px;">Our customers always come back for more</p></div>
        </div>
    </div>
</asp:Content>