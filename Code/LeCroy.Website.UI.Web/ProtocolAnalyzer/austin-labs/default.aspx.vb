Partial Class ProtocolAnalyzer_AustinLabs
    Inherits BasePage
    Public Shared menuURL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Austin Labs"
        Me.Master.PageContentHeader.BottomText = "Testing and Training"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Austin Labs - Testing and Solutions"

        Master.MetaOgTitle = "Teledyne LeCroy - Austin Labs - Testing and Solutions"
        Master.MetaOgImage = "http://teledynelecroy.com/images/austinlabs_og.png"
        Master.MetaOgDescription = "Teledyne LeCroy�s Austin Labs is the premier third-party test and validation center for servers, storage, and network devices."
        Master.MetaOgUrl = "http://teledynelecroy.com/protocolanalyzer/austin-labs/"
        Master.MetaOgType = "website"
        Master.BindMetaTags(False) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
    End Sub
End Class