<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false" Inherits="LeCroy.Website.ProtocolAnalyzer_ProtocolAnalyzer" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<main id="main" role="main"  class="has-header-transparented skew-style v1">
	<div class="bg-video-holder" style="background-image: url(https://assets.lcry.net/images/video.jpg);">
		<video class="bg-video" width="640" height="360" loop="" autoplay="" muted="" playsinline="">
			<source type="video/mp4" src="https://assets.lcry.net/images/video-psg.mp4">
		</video>
		<div class="bg-overlay"></div>
		<div class="banner">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-sm-10 col-md-11 col-lg px-sm-0 text-center">
						<!--span.text-uppercase Protocol Solution Group-->
						<h1>Protocol Test Solutions to Analyze, <br> Debug and Validate your Products</h1>
						<div>
							<div class="col-12 col-sm">
								<p class="mb-0">Reducing engineering development time, issue resolution cycles, and overall costs; increasing product quality, system reliability, and customer satisfaction.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="section-technologies section-white">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12">
					<div class="section-heading text-center mx-auto">
						<h2>Get the protocol test tools you need</h2>
						<div class="row justify-content-center">
							<div class="col-11 col-md-11 col-lg-10 col-xl-11 mx-auto">
								<p class="mb-0">Teledyne LeCroy offers a wide range of protocol test solutions - from protocol analyzers to conformance testers we've got the tools you need to thoroughly test your products. Select from the below technologies to see how we can help.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tech-list-cards">
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/pci-express">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/pci-express.png, https://assets.lcry.net/images/pci-express@2x.png 2x"/><img src="https://assets.lcry.net/images/pci-express.png" alt="PCI Express"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">PCI Express</h6>
												<p class="mb-0">High-speed interconnect technology enabling computer systems and servers</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/nvm.png, https://assets.lcry.net/images/nvm@2x.png 2x"/><img src="https://assets.lcry.net/images/nvm.png" alt="NVM Express"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">NVM Express</h6>
												<p class="mb-0">Host software communication protocol for non-volatile memory</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/cxl">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/cxl.png, https://assets.lcry.net/images/cxl@2x.png 2x"/><img src="https://assets.lcry.net/images/cxl.png" alt="CXL"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">CXL</h6>
												<p class="mb-0">High-speed CPU-to-Device and CPU-to-Memory interconnect</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/gen-z">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/gen-z.png, https://assets.lcry.net/images/gen-z@2x.png 2x"/><img src="https://assets.lcry.net/images/gen-z.png" alt="Gen-Z"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">Gen-Z</h6>
												<p class="mb-0">Provide high-speed, low-latency, memory-semantic access to data</p>
											</div>
										</div></a></div>
							</div>							
							<div class="col-tech mb-xl-0 mb-md-0 pb-md-0">
								<div class="card-tech h-100"><a href="/protocolanalyzer/ccix"> 
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/ccix.png, https://assets.lcry.net/images/ccix@2x.png 2x"/><img src="https://assets.lcry.net/images/ccix.png" alt="CCIX"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>CCIX</span></h6>
												<p class="mb-0">Extending well-established data center hardware and software infrastructure                                                                                                                                                </p>
											</div>
										</div></a></div>
							</div>                            
                            <div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/usb">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/usb.png, https://assets.lcry.net/images/usb@2x.png 2x"/><img src="https://assets.lcry.net/images/usb.png" alt="USB"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">USB</h6>
												<p class="mb-0">The ubiquitous consumer electronics standard to transfer data and power</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech mb-md-0 pb-md-0">
								<div class="card-tech h-100"><a href="/protocolanalyzer/usb/voyager-m4x">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/Thunderbolt.png, https://assets.lcry.net/images/Thunderbolt@2x.png 2x"/><img src="https://assets.lcry.net/images/Thunderbolt.png" alt="Thunderbolt"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>Thunderbolt</span></h6>
												<p class="mb-0">Fastest, most versatile connection to any dock, display, or data device     </p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/fibre-channel"> 
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/Fiber-channel.png, https://assets.lcry.net/images/Fiber-channel@2x.png 2x"/><img src="https://assets.lcry.net/images/Fiber-channel.png" alt="Fibre Channel"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">Fibre Channel</h6>
												<p class="mb-0">The storage fabric for secure, mission critical and time sensitive applications</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/ethernet">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/ethernet-ea.png, https://assets.lcry.net/images/ethernet-ea@2x.png 2x"/><img src="https://assets.lcry.net/images/ethernet-ea.png" alt="Ethernet"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">Ethernet</h6>
												<p class="mb-0">The most common communication protocol on the planet</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/serial-attached-scsi">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/scsi.png, https://assets.lcry.net/images/scsi@2x.png 2x"/><img src="https://assets.lcry.net/images/scsi.png" alt="Serial Attached SCSI"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"> Serial Attached SCSI</h6>
												<p class="mb-0">The dominant storage interface for large-scale data center applications </p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/serial-ata">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/serial-ata.png, https://assets.lcry.net/images/serial-ata@2x.png 2x"/><img src="https://assets.lcry.net/images/serial-ata.png" alt="Serial ATA"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">Serial ATA</h6>
												<p class="mb-0">Point-to-point serial protocol to move data to and from storage devices</p>
											</div>
										</div></a></div>
							</div>

							<div class="col-tech">
								<div class="card-tech h-100"><a href="/protocolanalyzer/mipi"> 
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/MiPi.png, https://assets.lcry.net/images/MiPi@2x.png 2x"/><img src="https://assets.lcry.net/images/MiPi.png" alt="MIPI"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title">MIPI</h6>
												<p class="mb-0">Interconnecting integrated circuits in mobile and mobile-influenced devices</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech pb-lg-0">
								<div class="card-tech h-100"><a href="https://www.quantumdata.com/">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/display-port.png, https://assets.lcry.net/images/display-port@2x.png 2x"/><img src="https://assets.lcry.net/images/display-port.png" alt="Display Port"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>DisplayPort</span></h6>
												<p class="mb-0">Digital video display PC interface standard developed by VESA</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech pb-xl-0">
								<div class="card-tech h-100"><a href="https://www.quantumdata.com/">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/HDMI.png, https://assets.lcry.net/images/HDMI@2x.png 2x"/><img src="https://assets.lcry.net/images/HDMI.png" alt="HDMI"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>HDMI</span><i class="icon-target-link"></i></h6>
												<p class="mb-0">Digital video and audio interface for consumer electronics</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech pb-lg-0">
								<div class="card-tech h-100"><a href="https://www.quantumdata.com/">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/HDBT.png, https://assets.lcry.net/images/HDBT@2x.png 2x"/><img src="https://assets.lcry.net/images/HDBT.png" alt="HDBaseT"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>HDBaseT</span></h6>
												<p class="mb-0">Video and audio distribution standard for operation over standard network </p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech mb-xl-0 mb-md-0 pb-md-0">
								<div class="card-tech h-100"><a href="https://www.quantumdata.com/">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/HDCP.png, https://assets.lcry.net/images/HDCP@2x.png 2x"/><img src="https://assets.lcry.net/images/HDCP.png" alt="HDCP"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>HDCP</span></h6>
												<p class="mb-0">Digital content protection standard for HDMI and DisplayPort interfaces                                                                                                                                              </p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="http://fte.com/bluetooth">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/bluetooth.png, https://assets.lcry.net/images/bluetooth@2x.png 2x"/><img src="https://assets.lcry.net/images/bluetooth.png" alt="Bluetooth"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>Bluetooth</span></h6>
												<p class="mb-0">Wireless protocol for point-to-point and mesh communication in PAN</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech">
								<div class="card-tech h-100"><a href="http://fte.com/products/x240.aspx">
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/WiFi.png, https://assets.lcry.net/images/WiFi@2x.png 2x"/><img src="https://assets.lcry.net/images/WiFi.png" alt="WiFi"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>WiFi</span></h6>
												<p class="mb-0">Wireless protocols enabling LAN of devices and internet access</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech mb-xl-0 mb-md-0 pb-md-0">
								<div class="card-tech h-100"><a href="https://fte.com/products/x240.aspx"> 
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/thread.png, https://assets.lcry.net/images/thread@2x.png 2x"/><img src="https://assets.lcry.net/images/thread.png" alt="Thread"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>Thread</span></h6>
												<p class="mb-0">Aims to transform people�s lives through smart homes businesses</p>
											</div>
										</div></a></div>
							</div>
							<div class="col-tech mb-xl-0 mb-md-0 pb-md-0">
								<div class="card-tech h-100"><a href="https://fte.com/products/x240.aspx"> 
										<div class="content">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/zigbee.png, https://assets.lcry.net/images/zigbee@2x.png 2x"/><img src="https://assets.lcry.net/images/zigbee.png" alt="Zigbee"/>
												</picture>
											</div>
											<div class="text">
												<h6 class="title"><span>Zigbee</span></h6>
												<p class="mb-0">A mesh based, full stack solution that enables smart devices to work together</p>
											</div>
										</div></a></div>
							</div>




						</div>
		</div>
	</section>
	<section class="section-testing section-gray-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12">
					<div class="section-heading text-center mx-auto">
						<h2>Protocol Test Services</h2>
						<div class="row">
							<div class="col-11 col-md-11 col-lg-10 col-xl-11 mx-auto">
								<p>Accelerate your protocol testing - take advantage of Teledyne Testing services. We have the staff, the expertise, the testing facilities, the test equipment, and the device libraries to help you get your products ready for market.
									<!--Let us help you accelerate your protocol testing efforts. Whether you just need a little help or you are looking for a complete turn-key testing solution we have the staff, the expertise, the testing facilities, the test equipment, and the device libraries to help you fully test your products.--></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-8 col-xl-6">
					<div class="card-line h-100">
						<div class="inner-content"><span class="sub-title">Austin Labs</span>
							<h4 class="title">Enterprise Solutions Testing</h4>
							<p>
								Austin Labs is the premier third-party test and validation center for servers, storage, and network devices across multiple technologies.We provide customized expert-level test and training services for physical and link-layer technologies to help you deliver fully tested products to market on time and within budget.</p>
							<ul class="p-0 list-items">
								<li><span> <img src="https://assets.lcry.net/images/protocol-img.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img2.png" alt="PCIe"></span></li>
								<li><span><img src="https://assets.lcry.net/images/protocol-img3.png" alt="PCIe"></span></li>
								<li><span><img src="https://assets.lcry.net/images/protocol-img4.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img5.png" alt="PCIe"></span></li>
								<li><span><img src="https://assets.lcry.net/images/protocol-img6.png" alt="PCIe"></span></li>
								<li><span><img src="https://assets.lcry.net/images/protocol-img7.png" alt="PCIe"></span></li>
								<li><span><img src="https://assets.lcry.net/images/protocol-img8.png" alt="PCIe"></span></li>
							</ul>
						</div>
						<aside>
							<h5 class="text-uppercase text-blue">Testing services</h5>
							<ul class="list-check">
								<li><i class="icon-chek"></i><span>Performance</span></li>
								<li><i class="icon-chek"></i><span>Solutions</span></li>
								<li><i class="icon-chek"></i><span>Data Integrity</span></li>
								<li><i class="icon-chek"></i><span>Storage</span></li>
								<li><i class="icon-chek"></i><span>Networking</span></li>
								<li><i class="icon-chek"></i><span>Regression</span></li>
							</ul>
							<p class="m-0"><a class="btn btn-default" href="/services/austinlabs.aspx">Explore Austin Labs Test Center</a></p>
						</aside>
					</div>
				</div>
				<div class="col-lg-8 col-xl-6">
					<div class="card-line h-100">
						<div class="inner-content"><span class="sub-title">Frontline Test Services</span>
							<h4 class="title">Wireless Product Testing</h4>
							<p>Teledyne LeCroy offers test and consultancy services to help in all stages of product development from Specification to Market Launch and beyond. Whether your products are for Automotive, Medical, IoT or consumer, we provide expert-level help to ensure they are interoperable, to minimize field issues, reduce development and support cost.</p>
							<ul class="p-0 list-items" style="margin-bottom: 0;">
								<li><span> <img src="https://assets.lcry.net/images/protocol-img9.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img10.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img7.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img11.png" alt="PCIe"></span></li>
								<li><span> <img src="https://assets.lcry.net/images/protocol-img12.png" alt="PCIe"></span></li>
							</ul>
						</div>
						<aside>
							<h5 class="text-uppercase text-blue">Testing services</h5>
							<ul class="list-check">
								<li><i class="icon-chek"></i><span>Test Strategy</span></li>
                                            <li><i class="icon-chek"></i><span>Supplier Specifications</span></li>
											<li><i class="icon-chek"></i><span>Validation</span></li>
											<li><i class="icon-chek"></i><span>Benchmarking</span></li>
											<li><i class="icon-chek"></i><span>Pre-production</span></li>
											<li><i class="icon-chek"></i><span>Product Launch</span></li>
											<li><i class="icon-chek"></i><span>Post-production</span></li>
                                            <li><i class="icon-chek"></i><span>Phone Apps</span></li>
							</ul>
							<p class="m-0"><a class="btn btn-default" href="https://www.fte.com/services/default.aspx">Explore Frontline Testing Services</a></p>
						</aside>
					</div>
				</div>
			</div>
		</div>
	</section>
<section class="section-events-slider section-white events-fix">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm col-lg-9">
					<div class="section-heading text-center mx-auto">
						<h2 class="mb-0">Protocol Training and Events</h2>
					</div>
				</div>
			</div>
			<div class="holder-slider">
				<div class="slider-events">

				</div>
				<div class="slider-controls">
					<div class="wrap-arrows"></div>
				</div>
				<div class="holder-btn-more"><a class="link-arrow" href="/events/"><span>Show More Events</span><i class="icon-btn-right-01"></i></a></div>
			</div>
		</div>
	</section>
	<section class="section-resources section-gray-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-lg-9">
					<div class="section-heading text-center pb-md-2 pb-lg-1 pb-xl-0 mx-auto">
						<h2 class="title px-0 m-0">Protocol Resources</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-4"><a href="/support/softwaredownload/psgdocuments.aspx?capid=106&amp;mid=533&amp;smid=">
						<div class="card-line">                              
							<div class="inner-content">
								<h4 class="title">Software Downloads</h4>
								<p>Get our latest software suites to view, analyze, troubleshoot, and collaborate with peers</p><span class="link-arrow"><span>Explore</span><i class="icon-btn-right-01"></i></span>
							</div>
						</div></a></div>
				<div class="col-xl-4"><a href="/support/techlib/webcasts.aspx">
						<div class="card-line"> 
							<div class="inner-content">
								<h4 class="title">On Demand Webinars</h4>
								<p>Browse these previously recorded webinars presented by Teledyne LeCroy technical experts.</p><span class="link-arrow"><span>Explore</span><i class="icon-btn-right-01"></i></span>
							</div>
						</div></a></div>
				<div class="col-xl-4"><a href="/support/?capid=106&amp;capid=103&amp;mid=515">
						<div class="card-line">
							<div class="inner-content">
								<h4 class="title">Technical Support</h4>
								<p>Need help? Reach out to our support staff to help resolve technical issues or protocol challenges.</p><span class="link-arrow"><span>Explore</span><i class="icon-btn-right-01"></i></span>
							</div>
						</div></a></div>
			</div>
		</div>
	</section>
</main>
	<script type="text/javascript">


		jQuery(document).ready(function () {
			var jsonURL = "/events/output.aspx";
			console.log('setting up events json: %s', jsonURL);
			console.log('site.master relinquished control of the $ variable, must use jQuery instead');
			var now;
			try { now = $.now(); } catch { now = 'undefined' }
			console.log('Now based on $ is %s', now);
			now = jQuery.now();
			console.log('Now based on jQuery is %s', now);

            jQuery.getJSON(jsonURL, { countryid: 208 }, doAll);
            //jQuery(".slider-events").html(outHtml);

			//SliderNew();
		});
		//await json to append before adding js slider styles
		async function doAll(json) {
			console.log(json);
			await processRequest(json);
			SliderNew();
        }

        function processRequest(json) {
            var gethtml = "";
			for (i = 0; i < 6; i++) {
                
				var jsonStartDate = json[i].ExhibitionStartDate.replace("/Date(", "").replace("-0400)/", "").replace("-0500)/", "").replace("-0600)/", "");
				var date = new Date(jsonStartDate);
				const month = date.toLocaleString('default', { month: 'long' });
				var dateString = ("0" + date.getUTCDate()).slice(-2);
				dateString = month + "&nbsp;" + dateString;
				var jsonType = json[i].TypeId;
				var jsonTitle = json[i].Title;
				var jsonURL = json[i].Url;
				var jsonDescription = json[i].Description.replace(/(<([^>]+)>)/ig, "");
				var jsonAddress = json[i].Address.replace(/(<([^>]+)>)/ig, " ");
				var jsonCity = json[i].City;
				var jsonState = json[i].State;
				var jsonTimeZone = json[i].TimeZone;
				var location = "";

				jsonTitle = '<h6 class="title-slide">' + jsonTitle + '</h6>';
				jsonURL = '<a class="btn btn-default" href="' + jsonURL + '">';
				switch (jsonType) {
					case 2: { jsonType = "<span class='type type-trade-show'>Trade Show</span>"; location = "Digital Event"; }; break;
					case 3: { jsonType = "<span class='type type-seminar'>Seminar</span>"; location = "Online"; }; break;
					case 4: { jsonType = "<span class='type type-webinar'>Webinar</span>"; location = "Online"; }; break;
					case 5: { jsonType = "<span class='type type-training'>Training</span>"; location = "Online"; }; break;
				}

				gethtml += '<div class="slide-event">' + jsonType + jsonTitle
					+ '<div class="footer-slide d-flex"><div class="hold-date d-flex align-items-center"><span class="local"><i class="icon-local-1"></i>' + location + '</span><span class="date"><i class="icon-calendar-1"></i>' + dateString + '</span></div></div>'
					+ jsonURL + 'Explore</a></div>';
                
                
            }
            jQuery('.slider-events').html(gethtml);
            
            
		}

        function SliderNew() {
            jQuery(".slider-events").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                variableWidth: false,
                appendArrows: ".slider-controls .wrap-arrows",
                infinite: false,
                mobileFirst: true,
                // rew: (() => console.log($slider.slickGetOption('slidesToShow')))(),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                            variableWidth: false,
                        },
                    },
                    {
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 3,
                            variableWidth: false,
                        },
                    },
                ],
            }); 
        }
    </script>
</asp:Content>