Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class ProtocolAnalyzer_ProtocolAnalyzer
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        'Me.Master.PageContentHeader.TopText = "Products"
        'Me.Master.PageContentHeader.BottomText = "Protocol Analyzers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Protocol Analyzer"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindControls()
            'lb_topmenu.Text = Functions.TopMenu(AppConstants.PROTOCOL_CAPTION_ID, rootDir)
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindControls()
        Dim sqlString As String = "SELECT DISTINCT ps.* FROM [PROTOCOL_STANDARD] ps, [PROTOCOL_STANDARD_SERIES] pss, [PRODUCT_SERIES] prs, [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT] p " & vbCrLf &
                                  "     WHERE ps.PROTOCOL_STANDARD_ID = pss.PROTOCOL_STANDARD_ID AND pss.PRODUCT_SERIES_ID = prs.PRODUCT_SERIES_ID AND prs.PRODUCT_SERIES_ID = psc.PRODUCT_SERIES_ID AND " & vbCrLf &
                                  "     psc.PRODUCT_ID = p.PRODUCTID AND psc.CATEGORY_ID = 19 AND ps.DISABLED = 'N' AND p.DISABLED = 'N' AND p.EOL_YN = 'N' AND prs.DISABLED = 'N' AND prs.VALIDATE_YN = 'Y' UNION " & vbCrLf &
                                  "SELECT DISTINCT ps.* FROM [PROTOCOL_STANDARD] ps, [PROTOCOL_STANDARD_SERIES] pss, [PRODUCT_SERIES] prs " & vbCrLf &
                                  "     WHERE ps.PROTOCOL_STANDARD_ID = pss.PROTOCOL_STANDARD_ID AND pss.PRODUCT_SERIES_ID = prs.PRODUCT_SERIES_ID AND ps.DISABLED = 'N' AND ((prs.DISABLED = 'N' AND prs.VALIDATE_YN = 'N') OR (ps.URL_B IS NOT NULL))"
        Dim protocolStandards As List(Of ProtocolStandard) = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of System.Data.SqlClient.SqlParameter)().ToArray())
        If (protocolStandards.Count > 0) Then
            'ucMainProtocols.ProtocolStandards = protocolStandards.Where(Function(x) x.IsFeatured.ToUpper() = "Y").OrderBy(Function(x) x.Name).ToList()
            'ucAdditionalProtocols.ProtocolStandards = protocolStandards.Where(Function(x) x.IsFeatured.ToUpper() = "N").OrderBy(Function(x) x.Name).ToList()
        End If
    End Sub
#End Region
End Class