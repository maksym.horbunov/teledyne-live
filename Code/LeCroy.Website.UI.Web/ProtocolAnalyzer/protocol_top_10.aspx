<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.ProtocolAnalyzer_ProtocolAnalyzer_top10" CodeBehind="protocol_top_10.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" width="370">
                    <img src="../images/category/headers/hd_protocol.gif" alt="Protocol Analyzers" width="370" height="112">
                    <p>Teledyne LeCroy is a leading provider of protocol analyzers and verification tools for existing and emerging digital communications standards. Designed to generate, capture, and analyze high-speed communications traffic, Teledyne LeCroy�s tools help developers to discover and correct persistent and intermittent errors and flaws in their product design.</p>
                    <h2>Explore Protocol Analyzers<img src="/images/icons/icon_small_arrow_down.gif" /></h2>
                </td>
                <td valign="top" width="248"><img src="/images/title-summit-t24.png" width="260" height="163" vspace="25px" border="0"></td>
                <td valign="top" width="203">
                    <div class="subNav"><asp:Label ID="lb_topmenu" runat="server" /></div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <h1>Top 10 Reasons to choose Teledyne LeCroy Protocol Solutions Group</h1>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" align="left">
                    <h2>Industry Leader</h2>
                    <ul><li style="list-style-type: none;">Teledyne LeCroy expertise in probing high-speed links gives PSG industry-leading performance for testing today�s most challenging serial interfaces</li></ul>
                    <h2>Reliability</h2>
                    <ul><li style="list-style-type: none;">Teledyne LeCroy protocol solutions provide unmatched accuracy thanks to exhaustive testing with early component vendors to allow broad interoperability</li></ul>
                    <h2>Market Acceptance</h2>
                    <ul><li style="list-style-type: none;">Used by key developers in the silicon, system, and software markets, Teledyne LeCroy's products allow test teams to easily exchange information with other ecosystem suppliers</li></ul>
                    <h2>Ease of Use</h2>
                    <ul><li style="list-style-type: none;">Teledyne LeCroy's protocol analysis interface offers the shortest learning curve.</li><li style="list-style-type: none;">Knowledge developed using Teledyne LeCroy's protocol test tools can be directly extended to other serial technologies</li></ul>
                    <h2>Broad Product Line</h2>
                    <ul><li style="list-style-type: none;">Whether the project is a multi-stage silicon development or simple software validation, Teledyne LeCroy has a configuration to meet the requirements</li><li style="list-style-type: none;">Teledyne LeCroy's broad product line provides the critical mass needed to deliver reliable instruments at the earliest stages of new technology development</li></ul>
                    <h2>Partnerships</h2>
                    <ul><li style="list-style-type: none;">Key partnerships with IP, training and test labs provides real-world solutions to today's development challenges</li></ul>
                    <h2>Service</h2>
                    <ul><li style="list-style-type: none;">Regional Sales Engineers are available world-wide to assist in specification and delivery of the right equipment at the right time</li></ul>
                    <h2>Support</h2>
                    <ul><li style="list-style-type: none;">Access expert technical support engineers that are well versed in both design and compliance validation</li></ul>
                    <h2>Warranty</h2>
                    <ul><li style="list-style-type: none;">Industry-leading warranty and "cross-ship" service ensures Teledyne LeCroy protocol equipment is ready when you are</li></ul>
                    <h2>Investment Protection</h2>
                    <ul><li style="list-style-type: none;">Take advantage of Teledyne LeCroy volume purchase and upgrade programs to maximize your T&amp;M budget.</li></ul>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>