Imports System.Collections.Generic
Imports System.Data
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class ProtocolAnalyzer_ProtocolAnalyzer_top10
    Inherits BasePage

    Dim sqlstr As String = ""
    Dim ds As DataSet
    Dim mseries As String = ""
    Dim modelid As String = ""
    Public menuURL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Protocol Analyzers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Protocol Analyzer"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String
        ' if URL contains query string, validate all query string variables
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.PROTOCOL_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.PROTOCOL_CAPTION_ID
            End If
        End If

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        lb_topmenu.Text = Functions.TopMenu(captionID, rootDir)
    End Sub



End Class
