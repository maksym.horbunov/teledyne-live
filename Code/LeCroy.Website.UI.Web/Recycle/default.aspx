﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/twocolumn-left.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.Recycle_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li class="current">RoHS &amp; EU WEEE</li>
        <li style="padding-left: 30px;"><a href="../support/rohs.aspx">EU RoHS</a></li>
        <li style="padding-left: 30px;"><a href="../support/rohs.aspx">China RoHS</a></li>
        <li style="padding-left: 30px;"><a href="/recycle/default.aspx">EU WEEE</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" align="left" height="600" width="500">
                    <p><strong><font color="#1871cd" size="3">EU WEEE</font></strong></p>
                    <p><font size="2">LeCroy has established a communication link for customers to access information related to the European WEEE directive.<br><br>The following symbol appears on all LeCroy products shipped after August 13, 2005:</font></p>
                    <p align="center">&nbsp; <font size="2"><img src="images/icon_recycle.gif" width="75" height="131" border="0">&nbsp;</font></p>
                    <p align="left">
                        <font size="2">This symbol is a reminder not to dispose of your waste electronic equipment in standard trash receptacles.<br><br>LeCroy is actively defining the take-back and recycling program for EU customers.<br /><br />We are registered in the following countries: </font>
                        <font size="2">
                            <ul>
                                <li>Germany: WEEE-Reg.-Nr. DE 14397960</li>
                                <li>Sweden</li>
                                <li>France</li>
                                <li>England: WEEE REGISTRATION NUMBER WEE/AD0495TY</li>
                            </ul>
                        </font>
                    </p>
                    <p>
                        <font size="2">If you are a customer in the EU and need assistance, please contact LeCroy for instructions on how to return your product by emailing <a href="mailto:eurecycling@teledynelecroy.com">eurecycling@teledynelecroy.com</a> and include your: </font>
                        <font size="2">
                            <ul>
                                <li>Company Name and Address</li>
                                <li>Contact Information (Name, Email, Phone)</li>
                                <li>Model</li>
                                <li>Serial Number</li>
                            </ul>
                        </font>
                    </p>
                    <p align="left"><font size="2"><br></font></p>
                </td>
                <td valign="top" align="center" height="38" width="10">&nbsp;</td>
                <td valign="top" align="center" height="38">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>