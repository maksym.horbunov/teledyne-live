﻿<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="/include/global.asp" -->
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%
'If information came from reg web page
'Response.Write Request.form
'Response.end

if len(Request.form)>0 then
	strRegID=Request.form("regid")
	if len(strModel)>0 then
		strModel=replace(Request.form("model"),"'","''")
	end if
	if len(strSerialNumber)>0 then
		strSerialNumber=replace(Request.form("serialnum"),"'","''")
	end if
	if (strScopeID)>0 then
		strScopeID=replace(Request.form("scopeid"),"'","''")
	end if
	strFirstName=replace(Request.form("FirstName"),"'","''")
	strLastName=replace(Request.form("LastName"),"'","''")
	strTitle=replace(Request.form("Title"),"'","''")
	strCompany=replace(Request.form("Company"),"'","''")
	strAddress=replace(Request.form("Address"),"'","''")
	strAddress2=replace(Request.form("Address2"),"'","''")
	strCity=replace(Request.form("City"),"'","''")
	'strState=Request.form("State")
	if len(Request.Form("State"))>0 then
		if not cstr(trim(Request.Form("State")))="Select State" then
			if len (Request.form("Country"))>0 then
				if cstr(Request.form("Country"))="United States" then
					strState=replace(Request.Form("State"),"'","''")
				else
					strState=replace(Request.form("OtherState"),"'","''")
				end if
			end if
		end if
	end if
	strZip=replace(Request.form("Zip"),"'","''")
	strCountry=replace(Request.form("Country"),"'","''")
	strPhone=replace(Request.form("Phone"),"'","''")
	strFax=replace(Request.form("Fax"),"'","''")
	strEmail=replace(Request.form("Email"),"'","''")
	strGift=0'Request.form("scoperegpresent")
	strProcessed="y"
	strJobFunction=replace(Request.Form("JOB_FUNCTION"),"'","''")
end if
'if information came form scope 
'Response.Write Request.Form
'Response.End
if len(Request.QueryString)>0 then
	strModel=replace(Request.QueryString("model"),"'","''")
	strSerialNumber=replace(Request.QueryString("SERIAL_NUM"),"'","''")
	strScopeID=replace(Request.QueryString("scope_id"),"'","''")
	strFirstName=replace(Request.QueryString("First_Name"),"'","''")
	strLastName=replace(Request.QueryString("Last_Name"),"'","''")
	strTitle=replace(Request.QueryString("Title"),"'","''")
	strCompany=replace(Request.QueryString("Company"),"'","''")
	strAddress=replace(Request.QueryString("Address1"),"'","''")
	strAddress2=replace(Request.QueryString("Address2"),"'","''")
	strCity=replace(Request.QueryString("City"),"'","''")
	strState=replace(Request.QueryString("State"),"'","''")
	strOtherState=replace(Request.QueryString("OtherState"),"'","''")
	strZip=replace(Request.QueryString("POSTAL_CODE"),"'","''")
	strCountry=replace(Request.QueryString("Country"),"'","''")
	strPhone=replace(Request.QueryString("Phone"),"'","''")
	strFax=replace(Request.QueryString("Fax"),"'","''")
	strEmail=replace(Request.QueryString("Email"),"'","''")
	strGift=0
	strProcessed="n"
	strJobFunction=replace(Request.QueryString("JOB_FUNCTION"),"'","''")
end if
if len(Request.QueryString)>0 or len(Request.form)>0 then
	if len(strRegID)>0 then
	'Update existing record
		if len(strModel)>0 and len(strSerialNumber)>0 then
		strSQL=	"update CONTACT_SCOPE SET MODEL ='" & strModel & "',SERIAL_NUM ='" &_
				strSerialNumber & "',FIRST_NAME ='" & strFirstName & "',LAST_NAME ='" &_
				strLastName & "',TITLE ='" & strTitle & "',COMPANY ='" & strCompany &_
				"',ADDRESS1 ='" & strAddress & "', ADDRESS2 ='" & strAddress2 &_
				"', CITY ='" & strCity & "', STATE ='" & strState & "',POSTAL_CODE ='" &_
				strZip & "',COUNTRY ='" & strCountry & "', PHONE ='" & strPhone &_
				"', FAX ='" & strFax & "', EMAIL ='" & strEmail & _
				"', DATE_LAST_MODIFIED = '" & now() & "', PRESENT_ID=" & strGift &_
				",PROC_YN='" & strProcessed & "',SEND_EMAIL='y',JOB_FUNCTION='" & strJobFunction & "' where REG_ID=" & strRegID
		else
		strSQL=	"update CONTACT_SCOPE SET FIRST_NAME ='" & strFirstName & "',LAST_NAME ='" &_
				strLastName & "',TITLE ='" & strTitle & "',COMPANY ='" & strCompany &_
				"',ADDRESS1 ='" & strAddress & "', ADDRESS2 ='" & strAddress2 &_
				"', CITY ='" & strCity & "', STATE ='" & strState & "',POSTAL_CODE ='" &_
				strZip & "',COUNTRY ='" & strCountry & "', PHONE ='" & strPhone &_
				"', FAX ='" & strFax & "', EMAIL ='" & strEmail & _
				"', DATE_LAST_MODIFIED = '" & now() & "', PRESENT_ID=" & strGift &_
				",PROC_YN='" & strProcessed & "',SEND_EMAIL='y',JOB_FUNCTION='" & strJobFunction & "' where REG_ID=" & strRegID
		end if
	else
	'Insert
		REG_KEY= GeneratedREGKEY()

		strSQL=	"Insert into CONTACT_SCOPE (REG_KEY,MODEL, SERIAL_NUM, SCOPE_ID, FIRST_NAME," &_
				"LAST_NAME, TITLE, COMPANY, ADDRESS1, ADDRESS2, CITY, STATE, POSTAL_CODE,"&_
				"COUNTRY, PHONE, FAX, EMAIL, DATE_ENTERED,PRESENT_ID,JOB_FUNCTION) " &_
				"VALUES('" & REG_KEY & "','" & strModel & "','" & strSerialNumber & "','" &_
				strScopeID & "','" & strFirstName & "','" & strLastName &_
				"','" & strTitle & "','" & strCompany & "','" & strAddress &_
				"','" & strAddress2 & "','" & strCity & "','" & strState &_
				"','" & strZip & "','" & strCountry & "','" & strPhone &_
				"','" & strFax & "','" & strEmail & "','" & now() & "'," & strGift & ",'" & strJobFunction & "')"
	end if
	'Response.Write strSQL
	'Response.end
	set rsInsertUpdate=server.CreateObject("ADODB.Recordset")
	rsInsertUpdate.LockType=3
	rsInsertUpdate.Open strSQL, dbconnstr()
	'get latest RegID if inserted a new record
	if len(strRegID)=0 then
		set oRegID=server.CreateObject("ADODB.Recordset")
		strSQL = "select max(REG_ID) as REG_ID from CONTACT_SCOPE  where MODEL='" & strModel & "' and SERIAL_NUM='" & strSerialNumber & "'"
		oRegID.Open strSQL, dbconnstr()
		'Response.Write strSQL
		if not oRegID.EOF then
			'strRegID=oRegID("REG_ID")
			strRegID=GetREGKEYOnREGId(oRegID("REG_ID"))
		end if
	
	'send an email to the customer
		strCRet=chr(13)&chr(10)
		strBody = strBody & "Thank you for regestering your product."  & strCRet & strCRet 
		strBody = strBody & "Follow the link to update your information >>" & strCRet & strCRet 
		strBody = strBody & "https://teledynelecroy.com/rs.asp?rid=" & strRegID & strCRet & strCRet
		 
		 
		'Response.Write strBody
		If len(strBody)<> 0 Then 
			Set oMail=Server.CreateObject("CDO.Message")
			oMail.To =strEmail
			oMail.Bcc="kate.kaplan@teledynelecroy.com"
			oMail.From = "scopereg@teledynelecroy.com"
			oMail.Subject ="LeCroy DSO Registration Confirmation"
			oMail.TextBody= strBody
			oMail.send
			Set oMail = Nothing ' Release the object
		end if
	else
		'send an email to the customer
		strCRet=chr(13)&chr(10)
		strBody = strBody & "Dear Customer,"  & strCRet & strCRet 
		strBody = strBody & "Thank you for registering your product."' The scope accessory you requested will be mailed to the address you provided:" & strCRet & strCRet 
		'strBody = strBody &  strFirstName & " " & strLastName & strCRet 
		'strBody = strBody &  strCompany & strCRet 
		'strBody = strBody &  strAddress & strCRet 
		'If Len(strAddress2) > 0 then
	'		strBody = strBody &  strAddress2 & strCRet 
	'	end if
	'	strBody = strBody &  strCity & ", " & strState & ", " & strZip & strCRet 
	'	strBody = strBody &  strCountry & strCRet & strCRet 
	'	strBody = strBody & "Follow the link to update your information >>" & strCRet & strCRet 
	'	strBody = strBody & "https://teledynelecroy.com/rs.asp?rid=" & GetREGKEYOnREGId(strRegID) & strCRet & strCRet
		 
		'Response.Write strBody
		If len(strBody)<> 0 Then 
			Set oMail=Server.CreateObject("CDO.Message")
			oMail.To =strEmail
			oMail.Bcc="kate.kaplan@teledynelecroy.com"
			oMail.From = "scopereg@teledynelecroy.com"
			oMail.Subject ="LeCroy DSO Registration Confirmation"
			oMail.TextBody= strBody
			oMail.send
			Set oMail = Nothing ' Release the object
		end if
	end if
end if
%>
<html>
<head>
<!--#include virtual="/include/meta_info.asp" -->
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<table cellpadding=0 cellspacing=0 border=0 width="70%" ID="Table1">
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top>
	<table cellpadding="0" cellspacing=0 border=0 width="70%" bgcolor="#423472" ID="Table2">
        <tr>
    		<td valign="top" bgcolor="#1E90FF">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" ID="Table3">

 					<tr><!-- row 2 -->
   						<td valign=top width="439"><map name="FPMap0"><area href="/default.asp" shape="rect" coords="0, 0, 119, 48"></map><img name="Top_Nav_723036_r2_c1" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c1.gif" width="439" height="48" border="0" alt="Click to go back to Home Page" usemap="#FPMap0"></td>
   						<td valign=top width="100%" background="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c5.gif"><img name="Top_Nav_723036_r2_c5" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c5.gif" width="19" height="48" border="0" alt=""></td>
   						<td valign=top width="212"><img name="Top_Nav_723036_r2_c6" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c6.gif" width="212" height="48" border="0" alt=""></td>
   						<td valign=top width="130" bgcolor=dodgerblue>
						</td>
  					</tr>
  
				</table>
    		</td>
        </tr>
  		<tr>
    		<td bgcolor="#FFFFFF" height="2">
                <img border="0" src="../../images/spacer.gif">
    		</td>
  		</tr>
  		<tr>
    		<td valign="middle" bgcolor="#1671CC" height="45">
                <font face="Verdana,Arial" color="#FFFFFF" size="3">&nbsp;</font>
                <font face="Verdana,Arial" color="#FFFFFF" size="3">Product Registration Confirmation</font>
    		</td>
  		</tr>
  		<tr>
    		<td valign="middle" bgcolor="#423472" height="45">
    			<table cellpadding="0" cellspacing=0 border=0 width="100%" ID="Table4">
				<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td valign=top>
				<br>
                <font face="Verdana,Arial" color="#FFFF00" size="2">Thank you for registering your LeCroy Oscilloscope.<br>
                <br><br>
                Your free scope accessory will be sent by mail shortly.<br>An email confirming your entry will be sent to the provided address.</font>
                <br><br><br>
    		</td>
  		</tr>
<script LANGUAGE="JavaScript">
</script>
<%
'Check Request.querystring("rid")
'if it's not NULL Select the data from the database
if len(Request.QueryString("rid"))>0 then
	set rsRegScope=server.CreateObject("ADODB.REcordset")
	strSQL="select * from CONTACT_SCOPE where REG_ID=" & Request.QueryString("rid")
	rsRegScope.Open strSQL, dbconnstr()
	if not rsRegScope.EOF then
		Session("model")=rsRegScope("MODEL")
		Session("SerialNum")=rsRegScope("SERIAL_NUM")
		Session("ScopeID")=rsRegScope("SCOPE_ID")
		Session("FirstName")=rsRegScope("FIRST_NAME")
		Session("LastName")=rsRegScope("LAST_NAME")
		Session("Title")=rsRegScope("TITLE")
		Session("Company")=rsRegScope("COMPANY")
		Session("Address")=rsRegScope("ADDRESS1")
		Session("Address2")=rsRegScope("ADDRESS2")
		Session("City")=rsRegScope("CITY")
		if len(rsRegScope("STATE"))>0 then
				Session("State")=rsRegScope("STATE")
		end if
		Session("Zip")=rsRegScope("POSTAL_CODE")
		if len(rsRegScope("COUNTRY")) >0 then
			Session("Country")=rsRegScope("COUNTRY")
		end if
		Session("Phone") =rsRegScope("PHONE")
		Session("Fax")=rsRegScope("FAX")
		Session("Email") =rsRegScope("EMAIL")
	end if
end if

Public Function GeneratedREGKEY()
    ' Author:           Kate Kaplan
    ' Date:             March 15, 2004
    ' Description:      Generate a new REG_KEY
    ' Returns:          the REG_KEY
    ' Revisions:        1

    bl = False
    Do While Not bl
        Do Until Len(strGIUDFinal) > 14 ' number of the strlen -1
            Randomize
            ' Get a character
            strGUIDTemp = Chr(Int(44 * Rnd) + 48)
            
            ' Check if character is in the 0-9 and A-Z range
            If Asc(strGUIDTemp) < 58 Or (Asc(strGUIDTemp) > 64 And Asc(strGUIDTemp) < 91) Then
                strGIUDFinal = strGIUDFinal & strGUIDTemp
            End If
        Loop
        If UCase(CheckREGKEYNotExistance(strGIUDFinal)) = "TRUE" Then
            bl = True
        Else
            strGIUDFinal = ""
            bl = False
        End If
    Loop
    GeneratedREGKEY = strGIUDFinal
End Function

Function CheckREGKEYNotExistance(strREGKEY)
	' Author:           Kate Kaplan	
    ' Date:             March 15, 2004
    ' Description:      Gets the Reg_ID based on Reg_key
    ' Returns:          String "TRUE" if not found, or
    '                   String "FALSE" if found
    ' Revisions:        0
    set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT REG_ID FROM CONTACT_SCOPE WHERE UPPER(REG_KEY) = '"  & Trim(UCase(strREGKEY)) & "'"
    rs.ActiveConnection = dbconnstr()
    rs.Open strSQL
	If Not rs.EOF Then
		CheckREGKEYNotExistance = "FALSE"
	Else
		CheckREGKEYNotExistance = "TRUE"
	End If
	rs.Close
	set rs=nothing
End Function

Function GetREGKEYOnREGId(IREGID)
    set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT REG_KEY FROM CONTACT_SCOPE WHERE REG_ID = " & IREGID
    rs.ActiveConnection = dbconnstr()
    rs.Open strSQL
	If Not rs.EOF Then
		GetREGKEYOnREGId = rs("REG_KEY").Value
	Else
		GetREGKEYOnREGId = 0
	End If
	rs.Close
	set rs=nothing
End Function
%>
    </table>
