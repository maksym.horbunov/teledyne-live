<%@ Language=VBScript %>
<% sAgent = Request.ServerVariables("HTTP_USER_AGENT")%>
<!--#include virtual="/cgi-bin/profile/AllFunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	'PageTitle="LeCroy Product Registration" ' Title of Web Page
	'PageDescription="You can register your product on this page." 'Description of Page
	'PageKeywords="Product, Registration, Register, Scopes, Serial, Serial Number, Serial#" ' List of Keywords for Search Engines
	'banner="testmeasurement" 'other available - see banners.txt for more details
	'topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	'bgimage="" 'sets the background image for the document
	'language="us" ' Sets the language of the current page
	'division_id=1 'Sets the Division ID
	'topic_id=1
	'session("language") = "us"
	'country_id=208
	'if len(Session("localeid"))=0 then
	 Session("localeid")=1033
	 'end if
	localeid=Session("localeid")
	if len(Request.QueryString("rid"))=0 then
		'Response.Redirect "/sitemap/error.asp"
	end if
%>
<html>

<head>
<meta name="Title" content="<% = PageTitle %>">
<meta name="Description" content="<% = PageDescription %>">
<meta name="keywords" content="<% = PageKeywords %>">

</head>

<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<table cellpadding=0 cellspacing=0 border=0 width="70%" ID="Table1">
<form action="confirm.asp" method="POST"  id=scopereg name=scopereg>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign=top>
	<table cellpadding="0" cellspacing=0 border=0 width="70%" bgcolor="#423472" ID="Table2">
	
        <tr>
    		<td valign="top" bgcolor="#1E90FF">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" ID="Table3">

 					<tr><!-- row 2 -->
   						<td valign=top width="439"><map name="FPMap0"><area href="/default.asp" shape="rect" coords="0, 0, 119, 48"></map><img name="Top_Nav_723036_r2_c1" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c1.gif" width="439" height="48" border="0" alt="Click to go back to Home Page" usemap="#FPMap0"></td>
   						<td valign=top width="100%" background="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c5.gif"><img name="Top_Nav_723036_r2_c5" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c5.gif" width="19" height="48" border="0" alt=""></td>
   						<td valign=top width="212"><img name="Top_Nav_723036_r2_c6" src="/Navigation/FY04/Images/Top_Nav_72303-6_r2_c6.gif" width="212" height="48" border="0" alt=""></td>
   						<td valign=top width="130" bgcolor=dodgerblue>
						</td>
  					</tr>
  
				</table>
    		</td>
        </tr>
  		<tr>
    		<td bgcolor="#FFFFFF" height="2">
                <img border="0" src="../../images/spacer.gif">
    		</td>
  		</tr>
  		<tr>
    		<td valign="middle" bgcolor="#1671CC" height="45">
                <font face="Verdana,Arial" size="5" color="#FFFFFF">&nbsp;Product Registration Form</font>
    		</td>
  		</tr>
<script language="JavaScript">
	
function openWinScreen(URL)
{
aWindow = window.open(URL,"scopescreen","width=600,scrollbars=no,height=450 bgcolor=white resizable=no");
}
function validate()
//this function redirect user to the next page if all fields of
//registration form are correct
{
    var msg;
    msg ="";
    var flg=false;
     if(document.scopereg.model.value == "")
     {
      msg += "Model" + "\n";
	  flg=true;
      document.scopereg.model.focus();
	 }
     if(document.scopereg.serialnum.value == "")
     {
      msg += "Serial Number" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.serialnum.focus();
	  }
     }
    if(document.scopereg.scopeid.value == "")
     {
      msg += "Scope ID" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.scopeid.focus();
	  }
     }
	if(document.scopereg.FirstName.value == "")
     {
      msg += "<%=LoadI18N("SLBCA0161",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.FirstName.focus();
	  }
     }
    if(document.scopereg.LastName.value == "")
     {
      msg += "<%=LoadI18N("SLBCA0162",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.LastName.focus();
	  }
     }
  	if(document.scopereg.JOB_FUNCTION.value == "")
	{
	 msg += "Job Function" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.JOB_FUNCTION.focus();
		}	
	}

     
     if (document.scopereg.Company.value == "")
    {
      msg += "<%=LoadI18N("SLBCA0078",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.Company.focus();
	  }
    }
    if(document.scopereg.Address.value == "")
    {
      msg += "<%=LoadI18N("SLBCA0079",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.scopereg.Address.focus();
	  }	
    }
    if(document.scopereg.City.value == "")
    {
     msg += "<%=LoadI18N("SLBCA0080",localeid)%>" + "\n";
     if (flg!=true)
       {
        flg=true;
		document.scopereg.City.focus();
	   }	
    }
  	
  	if(document.scopereg.Country.value == "Select Country")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.Country.focus();
		}	
	}

    if (document.scopereg.Country.value =="United States")
    {
		if (document.scopereg.State.value == "") 
		{
		msg += "<%=LoadI18N("SLBCA0081",localeid)%>" + "\n";
		 if (flg!=true)
		 {
		  flg=true;
		  document.scopereg.State.focus();
		 }
		}
	}
	if(document.scopereg.Zip.value == "")
	{
	 msg += "<%=LoadI18N("SLBCA0082",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.Zip.focus();
		}	
	}
	if(document.scopereg.Country.value == "")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.Country.focus();
		}	
	}
	if(document.scopereg.Phone.value == "")
    {
     msg += "<%=LoadI18N("SLBCA0084",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.Phone.focus();
		}	
   }

    if(document.scopereg.Email.value == "")
    {
     msg += "<%=LoadI18N("SLBCA0086",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.scopereg.Email.focus();
		}	
   }

    if(msg != "")
    {
      alert("<%=LoadI18N("SLBCA0172",localeid)%>" + ":"+  "\n" + "\n" + msg);
      return;
    }
    document.scopereg.submit();
}

function changecountry()
{
    if((document.scopereg.sessioncountry.value)!=(document.scopereg.Country.options[document.scopereg.Country.selectedIndex].value))
    {
    var countryname=document.scopereg.Country.options[document.scopereg.Country.selectedIndex].value;
 
    document.location.href="changecountry.asp?country="+countryname;
    }
}	
</script>
<%
'Check Request.querystring("rid")
'if it's not NULL Select the data from the database
if len(Request.QueryString("rid"))>0 then
	set rsRegScope=server.CreateObject("ADODB.REcordset")
	strSQL="select * from CONTACT_SCOPE where REG_KEY='" & Request.QueryString("rid") & "'"
	rsRegScope.Open strSQL, dbconnstr()
	if not rsRegScope.EOF then
		RegID=rsRegScope("REG_ID")
		Session("model")=rsRegScope("MODEL")
		Session("SerialNum")=rsRegScope("SERIAL_NUM")
		Session("ScopeID")=rsRegScope("SCOPE_ID")
		Session("FirstName")=rsRegScope("FIRST_NAME")
		Session("LastName")=rsRegScope("LAST_NAME")
		Session("Title")=rsRegScope("TITLE")
		Session("Company")=rsRegScope("COMPANY")
		Session("Address")=rsRegScope("ADDRESS1")
		Session("Address2")=rsRegScope("ADDRESS2")
		Session("City")=rsRegScope("CITY")
		if len(rsRegScope("STATE"))>0 then
				Session("State")=rsRegScope("STATE")
		end if
		Session("Zip")=rsRegScope("POSTAL_CODE")
		if len(rsRegScope("COUNTRY")) >0 then
			Session("Country")=rsRegScope("COUNTRY")
		end if
		Session("Phone") =rsRegScope("PHONE")
		Session("Fax")=rsRegScope("FAX")
		Session("Email") =rsRegScope("EMAIL")
		Session("PresentID")=rsRegScope("PRESENT_ID")
		Session("JobFunction")=rsRegScope("JOB_FUNCTION")
	end if
end if
%>
  <tr>
    <td width="540">

		<table border="0" cellspacing="2" cellpadding="2" width="100%" ID="Table4" bgcolor=#423472>

			<tr>
				
				<td valign=top colspan=2 align=center>
<%
		set rsGiftExist=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT SCOPEREG_PRESENT FROM  MODEL WHERE SCOPEREG_PRESENT IS NOT NULL"
		'Response.Write strSQL
		rsGiftExist.Open strSQL, dbconnstr()
		if not rsGiftExist.EOF then
			set rsScopeRegPresent=server.CreateObject("ADODB.Recordset")
			strSQL="SELECT * FROM PRESENTS WHERE SCOPEREG = 'y' AND COUNTRY_ID = 208 AND PRESENT_ID IN (" & rsGiftExist("SCOPEREG_PRESENT") & ")" 
			'Response.Write strSQL
			rsScopeRegPresent.Open strSQL, dbconnstr(),adOpenStatic
			if not rsScopeRegPresent.EOF then	
				Response.Write "<table cellscacing=2 cellpadding=0 border=0 >"
				if cint(rsScopeRegPresent.RecordCount)>1 then
				Response.Write "<tr>"
				Response.Write "<td valign=middle colspan=3>"
				Response.Write "<font face=Verdana, Arial size=2 color=#FFFF00>"
				Response.Write "<input type=hidden name=giftcount value=" & rsScopeRegPresent.RecordCount & ">"
				Response.Write "Select the scope accessory of your preference:"
				Response.Write "</font><br><br>"
				Response.Write "</td>"
				Response.Write "</tr>"
				while not rsScopeRegPresent.EOF
					Response.Write "<tr>"
					Response.Write "<td valign=middle>"
					Response.Write "<input type=radio name=scoperegpresent value=" & rsScopeRegPresent("PRESENT_ID")& ">"
					Response.Write "</td>"
					Response.Write "<td valign=middle><nobr>"
					if cint(rsScopeRegPresent("QTY"))>1 then
						Response.Write "<font face=Verdana, Arial size=1 color=#FFFF00>"
						Response.Write rsScopeRegPresent("QTY") & " x "
						Response.Write "</font>"
					end if
					Response.Write "<font face=Verdana, Arial size=1 color=#FFFF00>"
					Response.Write rsScopeRegPresent("PRESENT_NAME")
					Response.Write "</font>"
					Response.Write "</nobr></td>"
					Response.Write "<td valign=top>"
					Response.Write "<a href=""Javascript:openWinScreen('" & rsScopeRegPresent("IMAGE_GIF") & "');"">"
					Response.Write "<img src="""& rsScopeRegPresent("IMAGE_GIF") & """ width=55 height=30 border=0></a>"
					Response.Write "</td>"
					Response.Write "</tr>"
					rsScopeRegPresent.MoveNext
				wend
				else
					Response.Write "<tr>"
					Response.Write "<td valign=middle colspan=3>"
					Response.Write "<font face=Verdana, Arial size=2 color=#FFFF00>"
					Response.Write "You will receive the following gift after you register the product:"
					Response.Write "</font><br><br>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td valign=middle>"
					Response.Write "<input type=hidden name=giftcount value='1'>"
					Response.Write "<input type=hidden name=scoperegpresent value=" & rsScopeRegPresent("PRESENT_ID")& ">"
					Response.Write "</td>"
					Response.Write "<td valign=middle><nobr>"
					if cint(rsScopeRegPresent("QTY"))>1 then
						Response.Write "<font face=Verdana, Arial size=1 color=#FFFF00>"
						Response.Write rsScopeRegPresent("QTY") & " x "
						Response.Write "</font>"
					end if
					Response.Write "<font face=Verdana, Arial size=1 color=#FFFF00>"
					Response.Write rsScopeRegPresent("PRESENT_NAME")
					Response.Write "</font>"
					Response.Write "</nobr></td>"
					Response.Write "<td valign=top>"
					Response.Write "<a href=""Javascript:openWinScreen('" & rsScopeRegPresent("IMAGE_GIF") & "');"">"
					Response.Write "<img src="""& rsScopeRegPresent("IMAGE_GIF") & """ width=55 height=30 border=0></a>"
					Response.Write "</td>"
					Response.Write "</tr>"
					'Response.Write "<tr>"
					'Response.Write "<td valign=top colspan=3>"
					'Response.Write "<font face=Verdana, Arial size=1 color=#FFFF00>"
					'Response.Write "<br>As of October 1, 2004, we will be offering ONLY the USB Memory Stick."
					'Response.Write "</font><br><br>"
					'Response.Write "</td>"
					'Response.Write "</tr>"
				end if
				Response.write "</table><br>"
				
			end if
		end if
%>	
		</td>
			</tr>
        <tr>
		  <td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0479",localeID)%>:</font></small>
		  </td>
		  <td width="200">
 				<input type=hidden name="regid" value="<% = RegID %>" size="30" ID="Hidden1"><font color="#FFFFFF">
				<%if len(Session("Model"))>0 then%>
	 				<input type="text" name="model" value="<% = server.HTMLEncode(Session("Model")) %>" size="30" maxlength=100 ID="Text14" disabled=disabled readonly=readonly>
				<%else%>
	 				<input type="text" name="model" value="<% = server.HTMLEncode(Session("Model")) %>" size="30" maxlength=100 ID="Text15">
				<%end if%>
                </font>

		  <!--<select name="model" maxlength="50" id="Select4">
              <option value='' selected><%=LoadI18N("SLBCA0481",localeID)%> <%call showmodels()%>
			</select>-->
		  </td>
		</tr>
		<tr>
			<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0480",localeid)%>:</font></small>
			</td>
			<td width="200">
			<%if len(Session("SerialNum"))>0 then%>
				<input type="text" name="serialnum" value="<% = server.HTMLEncode(Session("SerialNum")) %>" size="30" maxlength=100 ID="Text17" disabled=disabled readonly=readonly>
             <%else%>
				<input type="text" name="serialnum" value="<% = server.HTMLEncode(Session("SerialNum")) %>" size="30" maxlength=100 ID="Text1">
             <%end if%>
             </td>
		</tr>

		<tr>
			<td width="200" align="right"><small><font face="Verdana" color="white">ScopeID:</font></small>
			</td>
			<td width="200">
			<%if len(Session("SerialNum"))>0 then%>
				<input type="text" name="scopeid" value="<% = server.HTMLEncode(Session("ScopeID")) %>" size="12" maxlength=9 ID="Text2" disabled=disabled readonly=readonly>
             <%else%>
				<input type="text" name="scopeid" value="<% = server.HTMLEncode(Session("ScopeID")) %>" size="12" maxlength=9 ID="Text16">
             <%end if%>
			</td>
		</tr>
		<tr>
			<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0161",localeid)%>:</font></small>
			</td>
			<td width="200">
				<input type="text" name="FirstName" value="<% = server.HTMLEncode(Session("FirstName")) %>" size="30" maxlength="30" ID="Text3">
			</td>
		</tr>
		<tr>
			<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0162",localeid)%>:</font></small>
			</td>
			<td width="200"><input type="text" name="LastName" size="30" value="<% = server.HTMLEncode(Session("LastName")) %>" maxlength="30" ID="Text4">
			</td>
		</tr>
		<tr>
			<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0077",localeid)%>:</font></small>
			</td>
			<td width="200"><input type="text" name="Title" size="30" maxlength="60" value="<% = server.HTMLEncode(Session("Title")) %>" ID="Text5">
			</td>
		</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0163",localeid)%>:</font></small>
				</td>
				<td width="200"><font face="Verdana">
				<select name="JOB_FUNCTION" ID="Select4">
				<%if len(Session("JobFunction"))=0 or isnull(Session("JobFunction")) then%>
					<option value=''>Select Job Function
				<%end if%>
				<%if len(Session("JobFunction"))=0 or isnull(Session("JobFunction")) then%>
					<option value='Digital HW Design Engineering'>Digital HW Design Engineering
					<option value='Analog HW Design Engineering'>Analog HW Design Engineering
					<option value='RF/Wireless Design Engineering'>RF/Wireless Design Engineering
					<option value='SW Engineering'>SW Engineering
					<option value='Technician'>Technician
					<option value='Calibration/Standards'>Calibration/Standards
					<option value='Field Service'>Field Service
					<option value='Student'>Student
					<option value='Management'>Management
					<option value='Purchasing'>Purchasing
					<option value='R&D'>R&D
					<option value='Manufacturing Test'>Manufacturing Test
					<option value='Quality Control'>Quality Control
					<option value='Other'>Other 
				<%else
					if cstr(Session("JobFunction"))="Digital HW Design Engineering" then%>
						<option value='Digital HW Design Engineering' selected>Digital HW Design Engineering
					<%else%>
						<option value='Digital HW Design Engineering'>Digital HW Design Engineering
					<%end if%>
					<%if cstr(Session("JobFunction"))="Analog HW Design Engineering" then%>
						<option value='Analog HW Design Engineering' selected>Analog HW Design Engineering
					<%else%>
						<option value='Analog HW Design Engineering'>Analog HW Design Engineering
					<%end if%>
					<%if cstr(Session("JobFunction"))="RF/Wireless Design Engineering" then%>
						<option value='RF/Wireless Design Engineering' selected>RF/Wireless Design Engineering
					<%else%>
						<option value='RF/Wireless Design Engineering'>RF/Wireless Design Engineering
					<%end if%>
					<%if cstr(Session("JobFunction"))="SW Engineering" then%>
						<option value='SW Engineering' selected>SW Engineering
					<%else%>
						<option value='SW Engineering'>SW Engineering
					<%end if%>
					<%if cstr(Session("JobFunction"))="Technician" then%>
						<option value='Technician' selected>Technician
						<option value='Technician'>Technician
					<%end if%>
					<%if cstr(Session("JobFunction"))="Calibration/Standards" then%>
						<option value='Calibration/Standards' selected>Calibration/Standards
					<%else%>
						<option value='Calibration/Standards'>Calibration/Standards
					<%end if%>
					<%if cstr(Session("JobFunction"))="Field Service" then%>
						<option value='Field Service' selected>Field Service
					<%else%>
						<option value='Field Service'>Field Service
					<%end if%>
					<%if cstr(Session("JobFunction"))="Student" then%>
						<option value='Student' selected>Student
					<%else%>
						<option value='Student'>Student
					<%end if%>
					<%if cstr(Session("JobFunction"))="Management" then%>
						<option value='Management' selected>Management
					<%else%>
						<option value='Management'>Management
					<%end if%>
					<%if cstr(Session("JobFunction"))="Purchasing" then%>
						<option value='Purchasing' selected>Purchasing
					<%else%>
						<option value='Purchasing'>Purchasing
					<%end if%>
					<%if cstr(Session("JobFunction"))="R&D" then%>
						<option value='R&D' selected>R&D
					<%else%>
						<option value='R&D'>R&D
					<%end if%>
					<%if cstr(Session("JobFunction"))="Manufacturing Test" then%>
						<option value='Manufacturing Test' selected>Manufacturing Test
					<%else%>
						<option value='Manufacturing Test'>Manufacturing Test
					<%end if%>
					<%if cstr(Session("JobFunction"))="Quality Control" then%>
						<option value='Quality Control' selected>Quality Control
					<%else%>
						<option value='Quality Control'>Quality Control
					<%end if%>
					<%if cstr(Session("JobFunction"))="Other" then%>
						<option value='Other' selected>Other
					<%else%>
						<option value='Other'>Other
					<%end if%>
				<%end if%>
				</select>
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0078",localeid)%>:</font></small>
				</td>
				<td width="200"><input type="text" name="Company" size="30" maxlength="50" value="<% = server.HTMLEncode(Session("Company")) %>" ID="Text6">
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0079",localeid)%>:</font></small>
				</td>
				<td width="200"><input type="text" name="Address" size="40" maxlength="255" value="<% = server.HTMLEncode(Session("Address")) %>" ID="Text7">
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0079",localeid)%>2:</font></small>
				</td>
				<td width="200"><input type="text" name="Address2" size="40" maxlength="255" value="<% = server.HTMLEncode(Session("Address2")) %>" ID="Text8">
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0080",localeid)%>:</font></small>
				</td>
				<td width="200"><input type="text" name="City" size="30" maxlength="50" value="<% = server.HTMLEncode(Session("City")) %>" ID="Text9">
				</td>
			</tr>
				<%if len(Session("Country"))>0 then
					if Session("Country")="United States" then
					%>
					<tr>
					<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0081",localeid)%>&nbsp;</font><font face="Verdana,Arial" color="white">(<%=LoadI18N("SLBCA0164",localeid)%>):</font></small>
					</td>
					<td width="200">
						<%
						Response.Write "<select name=State size=1 ID=Select1>"
						If Session("Country") = "United States" and len(Session("State"))=0 Then
							Response.Write "<option VALUE=''" & Session("State") & """ SELECTED>Select State"
						end if
								
						set rsState=server.CreateObject("ADODB.Recordset")
						strSQL="Select * from STATE order by NAME"
						rsState.Open strSQL, dbconnstr()
						if not rsState.EOF then
							while not rsState.EOF
							If Len (Session("State"))>0 Then 
								if cstr(Session("State"))=cstr(rsState("NAME")) then
									Response.Write "<option VALUE='" & rsState("NAME") & "' SELECTED>" & rsState("NAME")
								else
									Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
								end if
							else
								Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
							end if
							
							rsState.MoveNext
							wend
						end if
						Response.Write "</select>"
					else%>
					<tr>
					<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0081",localeid)%>&nbsp;</font><font face="Verdana,Arial" color="white">(<%=LoadI18N("SLBCA0164",localeid)%>):</font></small>
					</td>
					<td width="200">
				<select name="State" maxlength="50" value size="1" ID="Select3">
	    		<% 'If Len (Session("State"))= 0 Then %>
					<option VALUE=''>Select State	
				<% 'end if 	
				set rsState=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from STATE order by NAME"
				rsState.Open strSQL, dbconnstr()
				if not rsState.EOF then
					while not rsState.EOF
					If Len (Session("State"))>0 Then 
						if cstr(Session("State"))=cstr(rsState("NAME")) then
							Response.Write "<option VALUE='" & rsState("NAME") & "' SELECTED>" & rsState("NAME")
						else
							Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
						end if
					else
						Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
					end if
					
					rsState.MoveNext
					wend
				end if%>	
				</select>
				</td>
			</tr>
						<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0165",localeid)%> (<%=LoadI18N("SLBCA0166",localeid)%></font><font face="Verdana" color="white">):</small>
				</td>
				<td width="200"><%
						Response.Write "<input type=""Text"" name=""State"" size=""30"" maxlength=""50"" value='" & server.HTMLEncode(Session("State")) & "'></td>"
				%></td>
				</tr>
					<%end if%>
					</td>
					</tr>
				<%else%>
					<tr>
					<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0081",localeid)%>&nbsp;</font><font face="Verdana,Arial" color="white">(<%=LoadI18N("SLBCA0164",localeid)%>):</font></small>
					</td>
					<td width="200">
				<select name="State" maxlength="50" value size="1" ID="Select1">
	    		<% If Len (Session("State"))= 0 Then %>
					<option VALUE=''>Select State	
				<% end if 	
				set rsState=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from STATE order by NAME"
				rsState.Open strSQL, dbconnstr()
				if not rsState.EOF then
					while not rsState.EOF
					If Len (Session("State"))>0 Then 
						if cstr(Session("State"))=cstr(rsState("NAME")) then
							Response.Write "<option VALUE='" & rsState("NAME") & "' SELECTED>" & rsState("NAME")
						else
							Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
						end if
					else
						Response.Write "<option VALUE='" & rsState("NAME") & "'>" & rsState("NAME")
					end if
					
					rsState.MoveNext
					wend
				end if%>	
				</select>
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0165",localeid)%> (<%=LoadI18N("SLBCA0166",localeid)%></font><font face="Verdana" color="white">):</small>
				</td>
				<td width="200"><input type="text" name="OtherState" size="30" maxlength="50" value="<% = server.HTMLEncode(Session("OtherState")) %>" ID="Text10">
				</td>
			</tr>
			<%end if%>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0082",localeid)%>:</font></small>
				</td>
				<td width="200"><input type="text" name="Zip" size="20" maxlength="20" value="<% = Session("Zip") %>"  ID="Text11">
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0083",localeid)%>:</font></small></td>
				<td width="200">
				<select name="Country"   size="1" ID="Select2" ><!--onchange="changecountry()">-->
				<% If Len (Session("Country"))=0 Then %>
					<option VALUE><%=LoadI18N("SLBCA0171",localeid)%>&nbsp;<%=LoadI18N("SLBCA0083",localeid)%>
                    <%end if
				set rsCountry=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from COUNTRY order by NAME"
				rsCountry.Open strSQL, dbconnstr()
				if not rsCountry.EOF then
					while not rsCountry.EOF
					If Len (Session("Country"))>0 Then 
						if cstr(Session("Country"))=cstr(rsCountry("NAME")) then
							Response.Write "<option VALUE='" & rsCountry("NAME") & "' SELECTED>" & rsCountry("NAME")
						else
							Response.Write "<option VALUE='" & rsCountry("NAME") & "'>" & rsCountry("NAME")
						end if
					else
						Response.Write "<option VALUE='" & rsCountry("NAME") & "'>" & rsCountry("NAME")
					end if
					
					rsCountry.MoveNext
					wend
				end if
					%>		 
					</select></td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0084",localeid)%>:</font></small>
				</td>
				<td width="200"><input type="text" name="Phone" size="30" maxlength="30" value="<% = server.HTMLEncode(Session("Phone")) %>"  ID="Text12">
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0085",localeid)%>:</font></small>
				</td>
				<td width="200"><font face="Verdana"><input type="text" name="Fax" size="30" maxlength="30" value="<% = server.HTMLEncode(Session("Fax")) %>"  ID="Text13"></font>
				</td>
			</tr>
			<tr>
				<td width="200" align="right"><small><font face="Verdana" color="white"><%=LoadI18N("SLBCA0086",localeid)%>:</font></small>
				</td>
				<td width="200"><font face="Verdana"><input type="text" name="Email" size="30" maxlength="60" value="<% = server.HTMLEncode(Session("Email")) %>"  ID="Text18"></font>
				</td>
			</tr>

			<%
			'select all enabled subscription type 
			'and show checked checkboxes
		'	set rsSubType=server.CreateObject("ADODB.Recordset")
		'	strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y'"
		'	rsSubType.Open strSQL,dbconnstr()
		'	if not rsSubType.EOF then
		'		Response.Write "<tr>"
		'		Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
		'		Response.Write "<br><br><font face=""Verdana,Arial"" size=2 color=""DodgerBlue""><b>" & LoadI18N("SLBCA0382",localeid) & "</b></font>"
		'		Response.Write "</td>"
		'		Response.Write "</tr>"
		'		Response.Write "<tr>"
		'		Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
		'		Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
		'	
		'		while not rsSubType.EOF
		'			subcount=subcount+1
		'			Response.Write "<tr>"
		'			Response.Write "<td valign=""top"" width=""30"">"
		'			Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription" & rsSubType("DOCUMENT_ID") & "' checked>"
		'			Response.Write "</td>"
		'			Response.Write "<td valign=""middle"">"
		'			Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubType("NAME") &  "</b></nobr></font>"
		'			Response.Write "</td>"
		'			Response.Write "</tr>"
		'			redim preserve SubTypeID(subcount)				
		'			SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")
		'			
		'		rsSubType.MoveNext
		'		wend
				
		'		Response.Write "</table>"
		'		Response.Write "</td>"
		'		Response.Write "</tr>"
				'ask customer in which format he prefer to recive an email
		'		set rsSubFormat=server.CreateObject("ADODB.Recordset")
		'		strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y'"
		'		rsSubFormat.Open strSQL,dbconnstr()
		'		if not rsSubFormat.EOF then
		'			Response.Write "<tr>"
		'			Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
		'			Response.Write "<br><br><font face=""Verdana,Arial"" size=2 color=""DodgerBlue""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font>"
		'			Response.Write "</td>"
		'			Response.Write "</tr>"
		'			Response.Write "<tr>"
		'			Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
		'			Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
		'		
		'			while not rsSubFormat.EOF
		'				if cstr(rsSubFormat("NAME"))="Text" then
		'					strSelected="CHECKED"
		'				else
		'					strSelected=""
		'				end if
		'				Response.Write "<tr>"
		'				Response.Write "<td valign=""top"" width=""30"">"
		'				Response.Write "<input type=radio name='subformat' id='subformat' value='" & rsSubFormat("FORMAT_ID")  & "'" & " " & strSelected & ">"
		'				Response.Write "</td>"
		'				Response.Write "<td valign=""middle"">"
		'				Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
		'				Response.Write "</td>"
		'				Response.Write "</tr>"
		'			rsSubFormat.MoveNext
		'			wend
		'			
		'			Response.Write "</table>"
		'			Response.Write "</td>"
		'			Response.Write "</tr>"
		'			end if
		'	end if
						
						
		'					Session("SubCount")=subcount
		'					Session("SubTypeID") = SubTypeID						
						
						%>
						
							<!--<tr>
							<td>
							</td>
							<td align="right">
								<hr size="1" color="RoyalBlue">
								<%
								if localeid="1036" then
								%>
								<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="document.regform.submit();" ID="Button1">
								<%else%>
								<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="validate()" ID="Button2">
								<%end if%>
							</td>
						</tr>-->
			<tr>
				<td align=right valign=top colspan=2><br>
				<input type=button id="Submit1"  value='<%=LoadI18N("SLBCA0528",localeID)%>' NAME="Submit1" onclick="validate()">
				</td>
			</tr>
			
		</table>

    </td>
  </tr>
</table>
</td>
</tr>
</form>
</table>
<%
sub showmodels()
	AllModels = True
	CurrentModels = False
	GetModels rs, CurrentModels
	rs.MoveFirst
	Do While Not rs.BOF And Not rs.EOF 
		iModelLooper = iModelLooper + 1
		Redim Preserve CurrModelsIdAndName(1,iModelLooper) 
		CurrModelsIdAndName(0,iModelLooper)  = rs.Fields(0).Value  
		CurrModelsIdAndName(1,iModelLooper)  = rs.Fields(1).Value 
		if len(Session("Demo"))>0 then
			if checkdemoselected(rs.Fields(0).Value) then
				Response.Write "<option VALUE='" & CurrModelsIdAndName(1,iModelLooper) & "' selected>"
			else
				Response.Write "<option VALUE='" & CurrModelsIdAndName(1,iModelLooper) & "'>"
			end if
		else 
			Response.Write "<option VALUE='" & CurrModelsIdAndName(1,iModelLooper) & "'>"
		end if
		Response.write "<font face=""Verdana"" color=""#000000"" size=""2"">" & CurrModelsIdAndName(1,iModelLooper)& "</font>"
		rs.MoveNext
	Loop
	rs.Close
	'Session("DemoCount")=iModelLooper
	'Session("StoredCurrModelsIdAndName") = CurrModelsIdAndName
end sub
%>

</body>

</html>
