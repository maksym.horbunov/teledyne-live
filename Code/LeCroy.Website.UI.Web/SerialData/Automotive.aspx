﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Automotive.aspx.vb" Inherits="LeCroy.Website.SerialData_Automotive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Automotive</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Automotive</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#testing">Testing</a></li>
									<li><a href="#automotiveethernet">Automotive Ethernet</a></li>
									<li><a href="#invehiclenetworking">In-Vehicle Networking</a></li>
									<li><a href="#tdme">TDME</a></li>
									<li><a href="#videos">Videos</a></li>
									<li><a href="#webinars">Webinars</a></li>
									<li><a href="#documents">Technical Documents</a></li>
									<li><a href="#contacts">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="section-cards-reverse section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-automotive-01.png, https://assets.lcry.net/images/images/img-automotive-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-automotive-01.png" alt="Physical Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<p>Tools tailored towards In-Vehicle Network test and debug.</p>
										<ul class="list-dots">
											<li>The best next generation <a class="text-blue" href="#automotiveethernet">Automotive Ethernet</a> solutions</li>
											<li>Superior <a class="text-blue" href="#invehiclenetworking">In-Vehicle Networking</a> debug and analysis</li>
											<li><a class="text-blue" href="#tdme">TDME</a> options for faster debug and validation efficiency</li>
											<li><a class="text-blue" href="#impedence">Impedance Profile and S-parameter measurements</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-auto-prot_01.jpg, https://assets.lcry.net/images/img-auto-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-auto-prot_01.jpg" alt="Protocol Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Layer Test Solutions</h3>
										<p>Protocol analysis – capture, analyze, and test devices that use the 2.4 GHz and 5GHz ISM bands and Conformance testing (Bluetooth LE)</p>
										<ul class="list-dots">
											<li>Complete captures of Bluetooth LE, Bluetooth Classic BR/EDR, 802.11 a/b/g/n and ac, and 802.15.4 Technologies</li>
											<li>Capture Wired technologies (HCI UART & Logic) alongside Wireless for comprehensive debugging</li>
											<li>Small footprint, USB bus powered and car adapter (included) for exceptional mobility</li>
											<li>Highly portable</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="http://fte.com/products/bluetooth.aspx">Explore Bluetooth Protocol Analysis Solutions</a><a class="large-link text-blue font-weight-medium pb-lg-0" href="http://fte.com/products/harmony.aspx">Explore Bluetooth LE Conformance Testing</a><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="http://teledyne-ts.com/autotqtest.html">Explore Automotive Torque Testing</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Consulting Services</h3>
										<p>Make sure your team and your products are ready – Frontline Test Services offers both world class consultancy services and interoperability testing services.</p>
										<ul class="list-dots">
											<li>Leverage Teledyne LeCroy’s expertise and experience with technologies including Bluetooth, Wi-Fi, USB and more</li>
											<li>The FTS Team provides help during any part of your products lifecycle from design concept to after market</li>
											<li>Take advantage of one of the largest test device libraries in the industry</li>
											<li>Expand or enhance your QA department</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="http://fte.com/services/default.aspx">Explore Frontline Test Services</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="automotiveethernet">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">The Best Next Generation Automotive Ethernet Solutions</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Cover all aspects of physical layer Automotive Ethernet compliance testing and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/100Base-T1-TDME-Startup2.jpg, https://assets.lcry.net/images/100Base-T1-TDME-Startup2@2x.jpg 2x"/><img src="https://assets.lcry.net/images/100Base-T1-TDME-Startup2.jpg" alt="100Base-T1 TDME"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Debug Tools for Link Startup</h3>
										<ul class="list-dots">
											<li>Trigger on Link Startup</li>
											<li>Debug link startup handshaking</li>
											<li>Identify link startup and packet errors</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=620&groupid=88">Explore 100Base-T1</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/auto-enet-setup_5247-hires.png, https://assets.lcry.net/images/auto-enet-setup_5247-hires@2x.png 2x"/><img src="https://assets.lcry.net/images/auto-enet-setup_5247-hires.png" alt="Automotive Ethernet Debug"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Debug and Characterize System Performance</h3>
										<ul class="list-dots">
											<li>Enhanced Signal Separation</li>
											<li>Dedicated PAM3 debug environment</li>
											<li>12 eye diagram parameters</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=598&groupid=140">Explore Automotive Ethernet Debug</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/QPHY-1000Base-T1_cover.jpg, https://assets.lcry.net/images/QPHY-1000Base-T1_cover@2x.jpg 2x"/><img src="https://assets.lcry.net/images/QPHY-1000Base-T1_cover.jpg" alt="QPHY-BroadR-Reach"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Comprehensive Compliance Testing</h3>
										<ul class="list-dots">
											<li>100Base-T1, TC8, and 1000Base-T1</li>
											<li>Support for IEEE and OPEN Alliance</li>
											<li>Pass/fail report Generation</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=392&groupid=140">Explore 100Base-T1</a><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=586&groupid=140">Explore 1000Base-T1</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="invehiclenetworking">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Superior In-Vehicle Networking Tools</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards In-Vehicle Network test and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/can-fd.png, https://assets.lcry.net/images/can-fd@2x.png 2x"/><img src="https://assets.lcry.net/images/can-fd.png" alt="CAN, CAN FD, J1939"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">CAN, CAN FD, J1939</h3>
										<ul class="list-dots">
											<li>Symbolic (.ARXML and .dbc) trigger and decode</li>
											<li>Automated timing measurements</li>
											<li>Eye diagram with mask testing per ID</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=497&groupid=88">Explore CAN, CAN FD, J1939</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/title-lin-decode.png, https://assets.lcry.net/images/title-lin-decode@2x.png 2x"/><img src="https://assets.lcry.net/images/title-lin-decode.png" alt="LIN"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">LIN</h3>
										<ul class="list-dots">
											<li>LIN 1.3, 2.x and J2602 support</li>
											<li>Conditional ID/Address triggering</li>
											<li>Flexible Error-frame trigger</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=517&amp;groupid=88">Explore LIN</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/FlexRay_8.png, https://assets.lcry.net/images/FlexRay_8@2x.png 2x"/><img src="https://assets.lcry.net/images/FlexRay_8.png" alt="FlexRay"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">FlexRay</h3>
										<ul class="list-dots">
											<li>Trigger on frames, symbols, and errors</li>
											<li>Eye diagram with mask testing</li>
											<li>FlexRay physical layer measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=515&groupid=88">Explore FlexRay</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/sent.png, https://assets.lcry.net/images/sent@2x.png 2x"/><img src="https://assets.lcry.net/images/sent.png" alt="SENT and SENT SPC"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SENT and SENT SPC</h3>
										<ul class="list-dots">
											<li>Decode and analyze fast and slow channels</li>
											<li>Extract and plot embedded SENT data</li>
											<li>Characterize SENT SPC MTP Stability</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=618&groupid=88">Explore SENT and SENT SPC</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/most.png, https://assets.lcry.net/images/most@2x.png 2x"/><img src="https://assets.lcry.net/images/most.png" alt="MOST50, MOST150"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">MOST50, MOST150</h3>
										<p>Compliance testing for:</p>
										<ul class="list-dots">
											<li>MOST 50 ePHY</li>
											<li>MOST150 oPHY</li>
											<li>MOST150 cPHY</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=391&amp;groupid=140">Explore MOST150 cPHY and oPHY</a><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=395&groupid=140">Explore MOST50 ePHY</a>
									</div>
								</div>
							</div>
							<div class="col-content-item pb-0">
								<div class="visual mb-3">
									<picture>
										<source srcset="https://assets.lcry.net/images/psi5.png, https://assets.lcry.net/images/psi5@2x.png 2x"/><img src="https://assets.lcry.net/images/psi5.png" alt="Manchester/PSI5"/>
									</picture>
								</div>
								<div class="card-body p-0">
									<h3 class="title">Manchester/PSI5</h3>
									<ul class="list-dots">
										<li>Decode 125 and 189 kb/s</li>
										<li>User-Definable Protocol Decoding</li>
										<li>Quickly search long records</li>
									</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=437&groupid=88">Explore Manchester/PSI5</a>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/USB-Software-View.png, https://assets.lcry.net/images/USB-Software-View@2x.png 2x"/><img src="https://assets.lcry.net/images/USB-Software-View.png" alt="USB"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">USB</h3>
										<ul class="list-dots">
											<li>Mature, proven technology</li>
											<li>Backward-compatible and low cost</li>
											<li>Data transfer speeds suitable for a variety of applications</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/usb">Explore USB Solutions</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/Bluetooth-Software-View.png, https://assets.lcry.net/images/Bluetooth-Software-View@2x.png 2x"/><img src="https://assets.lcry.net/images/Bluetooth-Software-View.png" alt="Bluetooth"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Bluetooth</h3>
										<ul class="list-dots">
											<li>Live or post-capture decryption and analysis</li>
											<li>All Bluetooth profiles and protocols supported through the latest specification</li>
											<li>Excursion Mode allows for one-button PC-free captures</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="https://fte.com/products/bluetooth.aspx">Explore Bluetooth Solutions</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="tdme">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Trigger, Decode, Measure/Graph, Eye Diagram</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Powerful and unique TDME options for faster debug and validation efficiency</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_D.png, https://assets.lcry.net/images/TDME_D@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_D.png" alt="Highest Performance Triggers"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Powerful Triggers and Decoders</h3>
										<ul class="list-dots">
											<li>Flexible Trigger Capability</li>
											<li>Intuitive, Color-Coded Overlays</li>
											<li>Filter decoder table on specific data</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#trigger">Explore Highest Performance Triggers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_M.png, https://assets.lcry.net/images/TDME_M@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_M.png" alt="Measure and Graph Tools for Validation Efficiency"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unique Measure and Graph Tools</h3>
										<ul class="list-dots">
											<li>Serial Data DAC and Graphing Tools</li>
											<li>Automated Timing Measurements</li>
											<li>Bus Status Measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#measure">Explore Measure and Graph Tools for Validation Efficiency</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_E.png, https://assets.lcry.net/images/TDME_E@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_E.png" alt="Eye Diagrams &amp; Physical Layer Testing"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Flexible Eye Diagram Capability</h3>
										<ul class="list-dots">
											<li>4 Simultaneous eyes</li>
											<li>User-defined or pre-defined Masks</li>
											<li>Additional Physical Layer Testing</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#eye">Explore Eye Diagrams &amp; Physical Layer Testing</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="impedence">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Impedance Profile and S-parameters</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy WavePulser 40iX and the Teledyne Test Tools T3SP15D provide S-parameter and impedance profile measurements on cables, backplanes, connectors, transmission lines on boards and interconnects.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/wavepulser-ix-top.png, https://assets.lcry.net/images/wavepulser-ix-top@2x.png 2x"/><img src="https://assets.lcry.net/images/wavepulser-ix-top.png" alt="WavePulser 40iX"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">WavePulser 40iX – Unmatched Characterization Insight</h3>
										<ul class="list-dots">
											<li>S-parameters, DC to 40 GHz</li>
											<li>Impedance profile with spatial resolution < 1mm</li>
											<li>Deep toolbox for simulation, de-embedding and time-gating</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/wavepulser/">Explore WavePulser 40iX High-speed Interconnect Analyzer</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/T3SP15D.png, https://assets.lcry.net/images/T3SP15D@2x.png 2x"/><img src="https://assets.lcry.net/images/T3SP15D.png" alt="T3SP15D Time Domain Reflectometer"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">T3SP15D Time Domain Reflectometer</h3>
										<ul class="list-dots">
											<li>S-parameters, up to 15 GHz</li>
											<li>Impedance profile with spatial resolution < 3mm</li>
											<li>Small form factor and battery-powered</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/tdr-and-s-parameters/series.aspx?mseries=570">Explore T3SP Time Domain Reflectometers</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray mb-0" id="videos">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="section-heading text-center">
									<h2 class="text-black title">Videos</h2>
								</div>
							</div>
						</div>
						<div class="row content-wrap">
                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0">
                        							<iframe src="https://fast.wistia.net/embed/iframe/db0o7hntk7" title="Understanding the 100Base-T1 Link Startup Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                    <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>

                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0">
                        							<iframe src="https://fast.wistia.net/embed/iframe/16kp2hnoxf" title="Fundamentals of the 100Base-T1 Frame Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                    <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>
                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0"><iframe src="https://fast.wistia.net/embed/iframe/cpikz5fnzb" title="What is PAM3 Signaling? Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                        <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>
                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0"><iframe src="https://fast.wistia.net/embed/iframe/3jp966ium4" title="Understanding the Basics of the SENT Protocol Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                        <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>
                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0"><iframe src="https://fast.wistia.net/embed/iframe/g4q7dws75y" title="What is SENT SPC? Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                        <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>
                        							<div class="col-12 col-sm-6 col-lg-3 mb-4 pb-sm-2 pb-md-0"><iframe src="https://fast.wistia.net/embed/iframe/iwfjyq7gfz" title="Verifying the Stability of the SENT SPC MTP Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                        <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                        							</div>
                        						</div>
						<div class="row text-center justify-content-center"><a class="btn btn-default" href="/support/techlib/videos.aspx">Video Library</a></div>
					</div>
				</section>
				<section class="section-padding" id="webinars">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading text-center">
									<h2>On-Demand Webinars</h2>
								</div>
							</div>
						</div>
						<div class="row">
    <div class="col">
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2020-05-07/7yb11x"><h5 class="title mb-sm-2 mb-lg-0">Automotive Ethernet and Transmitter Distortion Testing</h5></a>
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2017-10-12/5rhwq8"><h5 class="title mb-sm-2 mb-lg-0">Automotive Ethernet Physical Layer Compliance Testing</h5></a>
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2020-08-12/817d47"><h5 class="title mb-sm-2 mb-lg-0">Debugging Automotive Ethernet Links</h5></a>
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2019-08-05/7rqjm8"><h5 class="title mb-sm-2 mb-lg-0">Demystifying ADAS Serial Link Debug Requirements</h5></a>
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2017-04-25/51cvl8"><h5 class="title mb-sm-2 mb-lg-0">The Fundamentals of Automotive Ethernet</h5></a>
        <a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2020-05-15/7yfqxz"><h5 class="title mb-sm-2 mb-lg-0">Measuring and Debugging CAN/CAN FD Signals with an Oscilloscope Webinar </h5></a></div>
</div></div>
						
				</section>
				<section class="section-padding section-gray" id="documents">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading text-center">
									<h2>Technical Documents</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col"><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/can-sensor-characterization">CAN Sensor Characterization</a>
								<p>CAN-specific parameters measure sensor performance speed up validation of CAN/CAN FD ECU or network performance.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/decoding-a-sent-protocol-signal">Decoding a SENT Protocol Signal</a>
								<p>Review the basics of the SENT protocol and how to use a Teledyne LeCroy oscilloscope equipped with configurable SENT protocol decode software to properly decode SENT signals.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/decoding-psl5-with-the-manchester-configurable-protocol-decoder">Decoding PSl5 with the Manchester Configurable Protocol Decoder</a>
								<p>Learn how to use the Manchester configurable protocol decode software to properly decode PSI5 signals</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/verifying-mtp-for-sent-spc-appnote">Verifying the Stability of the SENT SPC MTP</a>
								<p>Learn the basics of the SENT SPC protocol and see how to use a Teledyne LeCroy oscilloscope to verify a consistent Master Trigger Pulse (MTP) length.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/smart-can-triggering">Smart CAN Triggering</a>
								<p>Time and Event qualified CAN Triggers will greatly increase your ability to debug a CAN/CAN FD ECU or network.</p>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
