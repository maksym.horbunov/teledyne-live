﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Embedded.aspx.vb" Inherits="LeCroy.Website.SerialData_Embedded" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Embedded</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Embedded</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#testing">Testing</a></li>
									<li><a href="#standards">Standards</a></li>
									<li><a href="#ddr">DDR</a></li>
									<li><a href="#tdme">TDME</a></li>
									<li><a href="#videos">Videos</a></li>
									<li><a href="#webinars">Webinars</a></li>
									<li><a href="#documents">Technical Documents</a></li>
									<li><a href="#contacts">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-embedded-01.png, https://assets.lcry.net/images/img-embedded-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-embedded-01.png" alt="Physical Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<p>Tools tailored towards embedded computing speed up test and debug.</p>
										<ul class="list-dots">
											<li>Support for widely adopted inter-chip communication <a class="text-blue" href="#standards">standards</a></li>
											<li>Comprehensive <a class="text-blue" href="#ddr">DDR</a> solutions</li>
											<li><a class="text-blue" href="#tdme">TDME</a> options for faster debug and validation efficiency</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Complete Ethernet Network and Fibre Channel Fabric on site or virtual training classes</li>
											<li>Extensive hardware, integration/interoperation, failure analysis, and characterization test services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="standards">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">I2C, SPI, UART-RS232, USB 2.0 HSIC, 10/100/1000Base-T</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards embedded computing speed up test and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/title-i2c.png, https://assets.lcry.net/images/title-i2c@2x.png 2x"/><img src="https://assets.lcry.net/images/title-i2c.png" alt="I2C"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">I2C</h3>
										<ul class="list-dots">
											<li>Set ACK condition in all frame triggers</li>
											<li>EEPROM read/write 2048-byte capability</li>
											<li>Frame Length trigger capability</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=516&amp;groupid=88">Explore I2C</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/title-spi.png, https://assets.lcry.net/images/title-spi@2x.png 2x"/><img src="https://assets.lcry.net/images/title-spi.png" alt="SPI"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SPI</h3>
										<ul class="list-dots">
											<li>Universal SPI support (no CS required)</li>
											<li>Frame definition for USART protocols</li>
											<li>Flexible Bits/Word Decode Setup</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=519&amp;groupid=88">Explore SPI</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/title-uart.png, https://assets.lcry.net/images/title-uart@2x.png 2x"/><img src="https://assets.lcry.net/images/title-uart.png" alt="UART-RS232"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">UART-RS232</h3>
										<ul class="list-dots">
											<li>Configurable byte structure</li>
											<li>Customizable Message Frame for proprietary protocols</li>
											<li>Supports 9-bit address</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=521&groupid=88">Explore UART-RS232</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/usb2.png, https://assets.lcry.net/images/usb2@2x.png 2x"/><img src="https://assets.lcry.net/images/usb2.png" alt="USB 2.0 HSIC"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">USB 2.0 HSIC</h3>
										<ul class="list-dots">
											<li>480 Mbps USB2-HSIC decode capable</li>
											<li>ProtoSync shows HSIC traffic at transaction level</li>
											<li>Quick search for specific messages</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=385&groupid=88">Explore USB2-HSIC</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/10_100_1000_BaseT.png, https://assets.lcry.net/images/10_100_1000_BaseT@2x.png 2x"/><img src="https://assets.lcry.net/images/10_100_1000_BaseT.png" alt="10/100/1000Base-T"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">10/100/1000Base-T</h3>
										<ul class="list-dots">
											<li>Decode 10/100Base-T and MDIO</li>
											<li>10/100/1000Base-T Compliance Testing</li>
											<li>Perform compliance tests without a probe</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=252&groupid=140">Explore 10/100/1000Base-T</a>
									</div>
								</div>
							</div>
<!--							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="images/title-audiobus-screen.png, images/title-audiobus-screen@2x.png 2x"/><img src="images/title-audiobus-screen.png" alt="I2S, LJ, RJ, and TDM"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">I2S, LJ, RJ, and TDM</h3>
										<ul class="list-dots">
											<li>Display digital audio serial data as an analog waveform</li>
											<li>Trigger on glitch, clip, and mute</li>
											<li>Decode left and right channels simultaneously</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="https://teledynelecroy.com/options/productseries.aspx?mseries=263&amp;groupid=88">Explore I2S, LJ, RJ, and TDM</a>
									</div>
								</div>
							</div>-->
						</div>
					</div>
				</section>
				<section class="section-padding" id="ddr">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Comprehensive DDR Solutions</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">The most comprehensive mixed signal oscilloscope solution for DDR2/3/4/5 and LPDDR2/3/4/5 physical layer testing.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/ddr-suite-02.png, https://assets.lcry.net/images/ddr-suite-02@2x.png 2x"/><img src="https://assets.lcry.net/images/ddr-suite-02.png" alt="DDR Toolkit"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">DDR Toolkit</h3>
										<ul class="list-dots">
											<li>Effortless Burst Separation</li>
											<li>Eye Diagram Analysis</li>
											<li>DDR-Specific Parameters</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/ddr-suite-05.png, https://assets.lcry.net/images/ddr-suite-05@2x.png 2x"/><img src="https://assets.lcry.net/images/ddr-suite-05.png" alt="Physical Layer Compliance"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Compliance</h3>
										<ul class="list-dots">
											<li>Complete JEDEC Test Coverage</li>
											<li>Fully Annotated Screenshots</li>
											<li>Automatic Report Generation</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/dh-probes-07.png, https://assets.lcry.net/images/dh-probes-07@2x.png 2x"/><img src="https://assets.lcry.net/images/dh-probes-07.png" alt="Unmatched Probing Versatility"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unmatched Probing Versatility</h3>
										<ul class="list-dots">
											<li>Robust solder-In tips</li>
											<li>Superior signal fidelity</li>
											<li>Simple and cost-effective</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="row text-center justify-content-center pt-4"><a class="btn btn-default" href="/ddr/">Explore DDR Physical Layer Testing Solutions</a></div>
					</div>
				</section>
				<section class="section-padding section-gray" id="tdme">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Trigger, Decode, Measure/Graph, Eye Diagram</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Powerful and unique TDME options for faster debug and validation efficiency.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_D.png, https://assets.lcry.net/images/TDME_D@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_D.png" alt="Highest Performance Triggers"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Powerful Triggers and Decoders</h3>
										<ul class="list-dots">
											<li>Flexible Trigger Capability</li>
											<li>Intuitive, Color-Coded Overlays</li>
											<li>Filter decoder table on specific data</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#trigger">Explore Highest Performance Triggers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_M.png, https://assets.lcry.net/images/TDME_M@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_M.png" alt="Measure and Graph Tools for Validation Efficiency"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unique Measure and Graph Tools</h3>
										<ul class="list-dots">
											<li>Serial Data DAC and Graphing Tools</li>
											<li>Automated Timing Measurements</li>
											<li>Bus Status Measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#measure">Explore Measure and Graph Tools for Validation Efficiency</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_E.png, https://assets.lcry.net/images/TDME_E@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_E.png" alt="Eye Diagrams &amp; Physical Layer Testing"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Flexible Eye Diagram Capability</h3>
										<ul class="list-dots">
											<li>4 Simultaneous eyes</li>
											<li>User-defined or pre-defined Masks</li>
											<li>Additional Physical Layer Testing</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#eye">Explore Eye Diagrams &amp; Physical Layer Testing</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding mb-0" id="videos">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="section-heading text-center">
									<h2 class="text-black title">Videos</h2>
								</div>
							</div>
						</div>
						<div class="row content-wrap">

							<div class="col-12 col-sm-6 col-lg-3 pb-sm-2 pb-md-0">
								<iframe src="https://fast.wistia.net/embed/iframe/dhb034e8au" title="SMBus TDME Tutorial Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
							</div>
						</div>
						<div class="row text-center justify-content-center"><a class="btn btn-default" href="/support/techlib/videos.aspx">Video Library</a></div>
					</div>
				</section>
				<section class="section-padding section-gray" id="webinars">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading mb-0">
									<h2>On-Demand Webinars</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col"><a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2019-04-01/7jwd17">Debugging Complex Embedded Computing System Issues (Part 1)</a><a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2019-04-01/7jwdjf">Debugging Complex Embedded Computing System Issues (Part 2)</a><a class="d-block large-link text-blue font-weight-medium pb-0" href="https://go.teledynelecroy.com/l/48392/2020-03-19/7xkhxf">DDR4/5 & LPDDR4/5 - Probing and Debug Solutions</a></div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="documents">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading mb-0">
									<h2>Technical Documents</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col"><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=6073">Advanced Concepts for Characterizing and Troubleshooting Low Speed Serial Data Devices</a>
								<p>Learn about various diagnostics that can be used for systems which incorporate various embedded computing standards.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=6003">Capture, Decode and Debug of Low Speed Serial Buses</a>
								<p>An overview of methods to capture, view, decode, and debug a variety of low speed serial data protocols.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=1007">i2C Conditional Data Triggering</a>
								<p>Learn how to isolate and identify specific I2C bus events to improve validation efficiency.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=1008">i2C Sub-Addressing Trigger Made Easy</a>
								<p>Learn how to identify address spaces used internally by devices, quickly isolate, and improve validation efficiency.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=1009">Triggering on I2C Frames with no Data - An Example in I2C Data Length Triggering</a>
								<p>Are you looking to focus special bus events? Do you have frames that contain no data at all? Learn how to isolate these and more.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=168">Jitter Effects on 100BASE-T Timing</a>
								<p>Jitter causes major problems in communications systems. Learn how to take a closer look at phase/time interval jitter to reveal major difficulties.</p>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
