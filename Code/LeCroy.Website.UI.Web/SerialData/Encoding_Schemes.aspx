﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Encoding_Schemes.aspx.vb" Inherits="LeCroy.Website.SerialData_Encoding_Schemes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Encoding Schemes</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Encoding Schemes</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#testing">Testing</a></li>
									<li><a href="#standards">Standards</a></li>
									<li><a href="#documents">Technical Documents</a></li>
									<li><a href="#contacts">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-_encodingschemes-01.png, https://assets.lcry.net/images/img-_encodingschemes-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-_encodingschemes-01.png" alt="Physical Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<p>Tools tailored towards encoding schemes speed up test and debug.</p>
										<ul class="list-dots">
											<li>8b/10b</li>
											<li>64b/66b</li>
											<li>Manchester</li>
											<li>NRZ</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="#documents">Technical Documents</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Complete Ethernet Network and Fibre Channel Fabric on site or virtual training classes.</li>
											<li>Extensive hardware, integration/interoperation, failure analysis, and characterization test services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="standards">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">8b/10b, 64b/66b, Manchester, NRZ</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards encoding schemes speed up test and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/8b10bdecode.png, https://assets.lcry.net/images/8b10bdecode@2x.png 2x"/><img src="https://assets.lcry.net/images/8b10bdecode.png" alt="8b/10b"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">8b/10b</h3>
										<ul class="list-dots">
											<li>8b/10b decoding with intuitive, easy-to-read decode overlay on the waveform</li>
											<li>Available hardware serial trigger up to 14.1 Gb/s</li>
											<li>Trigger on 8b/10b symbols, symbol strings, primitives or errors</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/default.aspx?categoryid=11&groupid=216">Explore Serial Trigger Hardware Options</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/ser_trig21.png, https://assets.lcry.net/images/ser_trig21@2x.png 2x"/><img src="https://assets.lcry.net/images/ser_trig21.png" alt="64/66b"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">64/66b</h3>
										<ul class="list-dots">
											<li>64b/66b decoding with intuitive, easy-to-read decode overlay on the waveform</li>
											<li>Available hardware serial trigger up to 14.1 Gb/s</li>
											<li>Specify data, control and error blocks, or trigger on invalid sync bits or types through the trigger dialog.</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=387&groupid=88">Explore 64b/66b Symbol Decodes</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/manchester.png, https://assets.lcry.net/images/manchester@2x.png 2x"/><img src="https://assets.lcry.net/images/manchester.png" alt="64b/66b Symbol Decode"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Manchester and NRZ</h3>
										<ul class="list-dots">
											<li>User-Definable Protocol Decoding</li>
											<li>Configurable word and packet grouping</li>
											<li>Supports bit rates up to 60 Gb/s</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=437&groupid=88">Explore Manchester</a><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=436&groupid=88">Explore NRZ</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="documents">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading mb-0">
									<h2>Technical Documents</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col"><a class="large-link text-blue font-weight-medium pb-0" href="/doc/using-manchester-and-nrz-configurable-protocol-decoders">Using Manchester and NRZ Configurable Protocol Decoders</a>
								<p>Learn how to use the Manchester and NRZ configurable decoders to decode industry-standard and customer protocols.</p>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
