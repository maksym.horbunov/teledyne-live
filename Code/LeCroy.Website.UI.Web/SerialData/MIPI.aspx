﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="MIPI.aspx.vb" Inherits="LeCroy.Website.SerialData_MIPI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">	
			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for MIPI</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-cards-reverse section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/LCRY_QPHY-MIPI-DPHY-2.png, https://assets.lcry.net/images/LCRY_QPHY-MIPI-DPHY-2@2x.png 2x"/><img src="https://assets.lcry.net/images/LCRY_QPHY-MIPI-DPHY-2.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Electrical Test Solutions</h3><strong class="d-block mb-1 font-weight-medium">Debug and Test – SPMI, I3C, and DigRF</strong>
										<p>Unique <a class="text-blue" href="#tdme">TDME</a> (Trigger, Decode, Measure/Graph, Eye Diagram) capabilities for common embedded technologies defined by the MIPI alliance.</p><a class="large-link text-blue font-weight-medium pb-lg-0" href="#standards">Explore SMPI, I3C, DigRF</a><strong class="d-block pt-3 pt-lg-4 font-weight-medium">Electrical Conformance Test</strong>
										<p>Proving electrical conformance to a MIPI PHY specification requires performing a specific set of measurements. Teledyne LeCroy QualiPHY automated test software simplifies this complex process, for faster testing, better repeatability, and complete documentation.</p><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=316&amp;groupid=140">Explore QPHY-MIPI-DPHY</a><br><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=479&amp;groupid=140">Explore QPHY-MIPI-MPHY</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-mipi-prot_01.jpg, https://assets.lcry.net/images/img-mipi-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-mipi-prot_01.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Test Solutions</h3><strong class="d-block font-weight-medium">UniPro/UFS</strong>
										<ul class="list-dots">
											<li>Full UniPro and UFS specification support</li>
											<li>Industry leader in UniPro and UFS protocol analysis</li>
											<li>Full range of tools – Analyzer, exerciser, CTS Test Suites, etc.</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/mipi">Explore MIPI UniPro/UFS Analyzers</a>
                                        <strong class="d-block pt-3 pt-lg-4 font-weight-medium">CSI-2/DSI-2</strong>
										<ul class="list-dots">
											<li>Supports both D-PHY and C-PHY</li>
											<li>Analyzer, Exerciser and CTS Test Suites</li>
											<li>Image extraction and Playback</li>
											<li>Packet Generation or Video Generation modes</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/mipi/envision-x84-analyzer">Explore MIPI CSI/DSI Analyzers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Intermediate and advanced PCIe training classes - on site, on location, and virtual</li>
											<li>Hardware and device testing services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-lifecycle section-padding section-gray mb-0" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">MIPI Product Lifecycle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy has the right test tool for every stage of the product lifecycle and for every layer, from the physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
												</picture>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Training</h3>
												<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant PCI Express specification.</p>
												<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get know Austin Labs</a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Physical Layer</h3>
												<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
											</div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-bring-up"></i><span>Bring Up</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Protocol Exercisers, Analyzers and Jammers can provide a full range of testing by identifying any operational issues, emulating devices and systems or injecting errors into the communication path for live traffic testing.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Debug state machine and other digital issues using ProtoSync oscilloscope-based Protocol Analysis Software, DH Series Active Differential Probes and protocol analyzer cross triggering.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
															<p class="mb-0">Differential Probes</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-compliance"></i><span>Conformance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/qphy-mipi-mphy-5.png, https://assets.lcry.net/images/qphy-mipi-mphy-5@2x.png 2x"/><img src="https://assets.lcry.net/images/qphy-mipi-mphy-5.png" alt="image description"/>
												</picture>
											</div>
										</div>
										<div class="col-xl-6 order-xl-first">
											<p>Proving electrical conformance to a MIPI PHY specification requires performing a specific set of measurements. Teledyne LeCroy QualiPHY automated test software simplifies this complex process, for faster testing, better repeatability, and complete documentation.</p>
											<div class="wrap-links pt-lg-0 mb-3 mb-lg-4 mb-xl-0"><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=316&amp;groupid=140">Explore QPHY-MIPI-DPHY</a><br><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=479&amp;groupid=140">Explore QPHY-MIPI-MPHY</a></div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-9"><i class="icon-testing"></i><span>Regression Testing</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T48.jpg, https://assets.lcry.net/images/Summit-T48@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t48-analyzer">Summit T48</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant PCI Express specification.</p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get know Austin Labs</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Physical Layer</h3>
											<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Protocol Exercisers, Analyzers and Jammers can provide a full range of testing by identifying any operational issues, emulating devices and systems or injecting errors into the communication path for live traffic testing.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Debug state machine and other digital issues using ProtoSync oscilloscope-based Protocol Analysis Software, DH Series Active Differential Probes and protocol analyzer cross triggering.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
														<p class="mb-0">Differential Probes</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/qphy-mipi-mphy-5.png, https://assets.lcry.net/images/qphy-mipi-mphy-5@2x.png 2x"/><img src="https://assets.lcry.net/images/qphy-mipi-mphy-5.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6 order-xl-first">
										<p>Proving electrical conformance to a MIPI PHY specification requires performing a specific set of measurements. Teledyne LeCroy QualiPHY automated test software simplifies this complex process, for faster testing, better repeatability, and complete documentation.</p>
										<div class="wrap-links pt-lg-0 mb-3 mb-lg-4 mb-xl-0"><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=316&amp;groupid=140">Explore QPHY-MIPI-DPHY</a><br><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=479&amp;groupid=140">Explore QPHY-MIPI-MPHY</a></div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-9">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T48.jpg, https://assets.lcry.net/images/Summit-T48@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t48-analyzer">Summit T48</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="SPMII3CDIGRF">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">SPMI, I3C and DigRF</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards test and debug</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/spmi-cover-title.png, https://assets.lcry.net/images/spmi-cover-title@2x.png 2x"/><img src="https://assets.lcry.net/images/spmi-cover-title.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SPMI</h3>
										<ul class="list-dots">
											<li>MIPI command sequence capable</li>
											<li>Non-standard frame support</li>
											<li>Full arbitration sequence support</li><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=602&amp;groupid=88">Explore SPMIbus TDME</a>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/i3cbus-title.png, https://assets.lcry.net/images/i3cbus-title@2x.png 2x"/><img src="https://assets.lcry.net/images/i3cbus-title.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">I3C</h3>
										<ul class="list-dots">
											<li>SDAR, HDR-DDR, and Legacy I2C capable</li>
											<li>Triggering in “ANY” supported mode</li>
											<li>Customized complex triggering capable</li><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=604&amp;groupid=88">Explore IC3bus TDME</a>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/digrf-3g-I-Q-demod.png, https://assets.lcry.net/images/digrf-3g-I-Q-demod@2x.png 2x"/><img src="https://assets.lcry.net/images/digrf-3g-I-Q-demod.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">DigRF</h3>
										<ul class="list-dots">
											<li>Decode DigRF 3G and v4 signals</li>
											<li>Serial DAC - I and Q digital data converts to analog waveform</li>
											<li>Correlate analog waveforms protocol decode</li><a class="large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=347&amp;groupid=88">Explore DigRF v4 bus</a>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="tdme">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Trigger, Decode, Measure/Graph, Eye Diagram</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Powerful and unique TDME options for faster debug and validation efficiency</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_D.png, https://assets.lcry.net/images/TDME_D@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_D.png" alt="Highest Performance Triggers"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Powerful Triggers and Decoders</h3>
										<ul class="list-dots">
											<li>Flexible Trigger Capability</li>
											<li>Intuitive, Color-Coded Overlays</li>
											<li>Filter decoder table on specific data</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#trigger">Explore Highest Performance Triggers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_M.png, https://assets.lcry.net/images/TDME_M@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_M.png" alt="Measure and Graph Tools for Validation Efficiency"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unique Measure and Graph Tools</h3>
										<ul class="list-dots">
											<li>Serial Data DAC and Graphing Tools</li>
											<li>Automated Timing Measurements</li>
											<li>Bus Status Measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#measure">Explore Measure and Graph Tools for Validation Efficiency</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_E.png, https://assets.lcry.net/images/TDME_E@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_E.png" alt="Eye Diagrams &amp; Physical Layer Testing"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Flexible Eye Diagram Capability</h3>
										<ul class="list-dots">
											<li>4 Simultaneous eyes</li>
											<li>User-defined or pre-defined Masks</li>
											<li>Additional Physical Layer Testing</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#eye">Explore Eye Diagrams &amp; Physical Layer Testing</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
