﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Memory.aspx.vb" Inherits="LeCroy.Website.SerialData_Memory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Memory</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-cards-reverse section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-04.jpg, https://assets.lcry.net/images/img-testing-04@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-04.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">DDR, CCIX, CXL and Gen-Z Electrical Test Solutions</h3>
										<ul class="list-dots">
											<li>Comprehensive DDR solutions and toolkits</li>
											<li>JEDEC physical layer compliance</li>
											<li>PCIe physical layer compliance and tools for CCIX, CXL and Gen-Z</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/ddr/">Explore DDR Solutions</a><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/pcie-electrical-test/">Explore PCIe Electrical Solutions</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-memory-prot_01.jpg, https://assets.lcry.net/images/img-memory-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-memory-prot_01.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Layer Test Solutions</h3>
										<p>The leading provider of protocol analyzers, exercisers/emulators, jammers and verification/compliance tools.</p>
										<ul class="list-dots">
											<li>Full PCIe specification support</li>
											<li>Industry leader in PCIe protocol analysis</li>
											<li>Full range of tools - excercisers, jammers, interposers, etc.</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/gen-z">Explore Protocol Test Solutions for Gen-Z</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Intermediate and advanced PCIe training classes - on site, onlocation, and virtual</li>
											<li>Hardware and device testing services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
