﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Mil_Aero_Avionics.aspx.vb" Inherits="LeCroy.Website.SerialData_Mil_Aero_Avionics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
						<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Mil/Aero/Avionics</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Mil/Aero/Avionics</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#testing">Testing</a></li>
									<li><a href="#standards">ARINC 429</a></li>
									<li><a href="#standards">MIL-STD-1553</a></li>
									<li><a href="#standards">SpaceWire</a></li>
									<li><a href="#1000BaseT1">100/1000Base-T1</a></li>
									<li><a href="#tdme">TDME</a></li>
									<li><a href="#documents">Technical Documents</a></li>
									<li><a href="#contacts">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="section-cards-reverse section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-milaero-01.png, https://assets.lcry.net/images/img-milaero-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-milaero-01.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<p>Tools tailored towards military, aeronautics, and avionics standards speed up test and debug.</p>
										<ul class="list-dots">
											<li><a class="text-blue" href="#arinc">ARINC 429</a></li>
											<li><a class="text-blue" href="#mil-std">MIL-STD-1553</a></li>
											<li><a class="text-blue" href="#spacewire">SpaceWire</a></li>
											<li><a class="text-blue" href="#1000BaseT1">100Base-T1, 1000Base-T1</a></li>
											<li><a class="text-blue" href="#tdme">TDME</a> options for faster debug and validation efficiency</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-milaero-prot_01.jpg, https://assets.lcry.net/images/img-milaero-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-milaero-prot_01.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Test Solutions</h3>
										<p>Teledyne LeCroy’s SierraNet family of protocol analyzers and jammers lead the way for today’s high-speed Ethernet and Fibre Channel manufacturers and Data Center Managers.</p>
										<ul class="list-dots">
											<li>Industries only PHY layer tapping and protocol decoding solution</li>
											<li>Fully integrated analysis and impairment tool set for full technology lifecycle support</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/ethernet">Explore Protocol Solutions for Ethernet</a><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/fibre-channel">Explore Protocol Solutions for Fibre Channel</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Complete Ethernet Network and Fibre Channel Fabric on site or virtual training classes</li>
											<li>Extensive hardware, integration/interoperation, failure analysis, and characterization test services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="standards">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">ARINC 429, MIL-STD-1553, SpaceWire</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards military, aeronautics, and avionics standards speed up test and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/arinc.png, https://assets.lcry.net/images/arinc@2x.png 2x"/><img src="https://assets.lcry.net/images/arinc.png" alt="ARINC 429"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">ARINC 429</h3>
										<ul class="list-dots">
											<li>ULDF symbolic decode</li>
											<li>Control selection for 8+24, 8+2+19+2+1, or user-defined</li>
											<li>Automated timing measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=513&amp;groupid=88">Explore ARINC 429</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/MIL-STD-1553.png, https://assets.lcry.net/images/MIL-STD-1553@2x.png 2x"/><img src="https://assets.lcry.net/images/MIL-STD-1553.png" alt="MIL-STD-1553"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">MIL-STD-1553</h3>
										<ul class="list-dots">
											<li>Conditional ID/Address triggering</li>
											<li>Configure trigger at transfer or word level</li>
											<li>Support for MIL-STD-1553 A or B</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=518&amp;&amp;groupid=88">Explore MIL-STD-1553</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/space-wire.png, https://assets.lcry.net/images/space-wire@2x.png 2x"/><img src="https://assets.lcry.net/images/space-wire.png" alt="SpaceWire"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SpaceWire</h3>
										<ul class="list-dots">
											<li>Decode ECSS-E-ST-50-12C</li>
											<li>View bits, N/L-characters, or packets</li>
											<li>Search for Control and Data characters</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=494&amp;&amp;groupid=88">Explore SpaceWire</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="1000BaseT1">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Next-Generation Vehicle Ethernet Solutions</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Cover all aspects of 100Base-T1 and 1000Base-T1 physical layer compliance testing and debug.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/100Base-T1-TDME-Startup2.jpg, https://assets.lcry.net/images/100Base-T1-TDME-Startup2@2x.jpg 2x"/><img src="https://assets.lcry.net/images/100Base-T1-TDME-Startup2.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Debug Tools for Link Startup</h3>
										<ul class="list-dots">
											<li>Trigger on Link Startup</li>
											<li>Debug link startup handshaking</li>
											<li>Identify link startup and packet errors</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="mil-aero-avionics.html">Explore 100Base-T1</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/auto-enet-setup_5247-hires.png, https://assets.lcry.net/images/auto-enet-setup_5247-hires@2x.png 2x"/><img src="https://assets.lcry.net/images/auto-enet-setup_5247-hires.png" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Debug and Characterize System Performance</h3>
										<ul class="list-dots">
											<li>Enhanced Signal Separation</li>
											<li>Dedicated PAM3 debug environment</li>
											<li>12 eye diagram parameters</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=598&groupid=140">Explore Automotive Ethernet Debug</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4 mb-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/QPHY-1000Base-T1_cover.jpg, https://assets.lcry.net/images/QPHY-1000Base-T1_cover@2x.jpg 2x"/><img src="https://assets.lcry.net/images/QPHY-1000Base-T1_cover.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Comprehensive Compliance Testing</h3>
										<ul class="list-dots">
											<li>100Base-T1, TC8, and 1000Base-T1</li>
											<li>Support for IEEE and OPEN Alliance</li>
											<li>Pass/fail report Generation</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=392&groupid=140">Explore 100Base-T1</a><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=586&amp;&amp;groupid=140">Explore 1000Base-T1</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="tdme">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Trigger, Decode, Measure/Graph, Eye Diagram</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Powerful and unique TDME options for faster debug and validation efficiency.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_D.png, https://assets.lcry.net/images/TDME_D@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_D.png" alt="Highest Performance Triggers"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Powerful Triggers and Decoders</h3>
										<ul class="list-dots">
											<li>Flexible Trigger Capability</li>
											<li>Intuitive, Color-Coded Overlays</li>
											<li>Filter decoder table on specific data</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#trigger">Explore Highest Performance Triggers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_M.png, https://assets.lcry.net/images/TDME_M@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_M.png" alt="Measure and Graph Tools for Validation Efficiency"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unique Measure and Graph Tools</h3>
										<ul class="list-dots">
											<li>Serial Data DAC and Graphing Tools</li>
											<li>Automated Timing Measurements</li>
											<li>Bus Status Measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#measure">Explore Measure and Graph Tools for Validation Efficiency</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_E.png, https://assets.lcry.net/images/TDME_E@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_E.png" alt="Eye Diagrams &amp; Physical Layer Testing"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Flexible Eye Diagram Capability</h3>
										<ul class="list-dots">
											<li>4 Simultaneous eyes</li>
											<li>User-defined or pre-defined Masks</li>
											<li>Additional Physical Layer Testing</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#eye">Explore Eye Diagrams &amp; Physical Layer Testing</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="documents">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="section-heading mb-0">
									<h2>Technical Documents</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col"><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=5967">Testing & Debugging Avionics Systems that Use ARINC 429 or MIL-STD-1553 Data Busses</a>
								<p>Learn about useful examples of how to view, test and troubleshoot various military, aeronautics, and avionics standards.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=6073">Advanced Concepts for Characterizing and Troubleshooting Low Speed Serial Data Devices</a>
								<p>Learn about various diagnostics that can be used for systems which incorporate various embedded computing standards.</p><a class="d-block large-link text-blue font-weight-medium pb-0" href="/doc/docview.aspx?id=6003">Capture, Decode and Debug of Low Speed Serial Buses</a>
								<p>An overview of methods to capture, view, decode, and debug a variety of low speed serial data protocols.</p>
							</div>
						</div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
