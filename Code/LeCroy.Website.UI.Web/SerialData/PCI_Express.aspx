﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="PCI_Express.aspx.vb" Inherits="LeCroy.Website.SerialData_PCI_Express" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for PCI Express</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/LabMaster10Zi-A-collage.png, https://assets.lcry.net/images/LabMaster10Zi-A-collage@2x.png 2x"/><img src="https://assets.lcry.net/images/LabMaster10Zi-A-collage.png" alt="LabMaster10Zi-A"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<ul class="list-dots">
											<li>Automated Tx, Rx and LEQ testing up to PCIe 5.0 32 GT/s</li>
											<li>Visibility from physical layer through protocol operations</li>
											<li>Expanded capabilities for PAM4, USB and other technologies</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/pcie-electrical-test/">Explore Electrical Test Solutions for PCIe</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/protocol-layer-test-solutions-2.jpg, https://assets.lcry.net/images/protocol-layer-test-solutions-2@2x.jpg 2x"/><img src="https://assets.lcry.net/images/protocol-layer-test-solutions-2.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Layer Test Solutions</h3>
										<p>The leading provider of protocol analyzers, exercisers/emulators, jammers and verification/compliance tools</p>
										<ul class="list-dots">
											<li>Full PCIe specification support</li>
											<li>Industry leader in PCIe protocol analysis</li>
											<li>Full range of tools - excercisers, jammers, interposers, etc.</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/pci-express">Explore Protocol Test Solutions for PCIe</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Intermediate and advanced PCIe training classes - on site, onlocation, and virtual</li>
											<li>Hardware and device testing services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-lifecycle section-padding section-gray" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">PCI Express Product Lifecycle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy has the right test tool for every stage of the product lifecycle and for every layer, from the physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, test and debug the relevant PCI Express specification.</p>
											<div class="wrap-btn"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Physical Layer</h3>
												<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
											</div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Physical Layer</h3>
												<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Protocol Layer</h3>
												<p>PCIe Exercisers and Protocol Analyzers provide early status of bus operation and fundamental host device communication.</p>
											</div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-bring-up"></i><span>Bring Up</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Protocol Layer</h3>
												<p>Protocol Exercisers, Analyzers and Jammers can provide a full range of testing by identifying any operational issues, emulating devices and systems or injecting errors into the communication path for live traffic testing.</p>
											</div>
										</div>
										<div class="col-xl-12">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Debug state machine and other digital issues using ProtoSync oscilloscope-based Protocol Analysis Software, DH Series Active Differential Probes and protocol analyzer cross triggering.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>PCIe Protocol Analyzers and exercisers provide PCIe, NVMe, and CXL emulation and monitoring of different types of systems giving maximum range of testing platforms.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/proto-sync.jpg, https://assets.lcry.net/images/proto-sync@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/options/productseries.aspx?mseries=287&amp;groupid=88">ProtoSync</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The ProtoSync option extends the Teledyne LeCroy decode options by enabling industry-standard protocol analyzer software to be run directly on the oscilloscope.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
															<p class="mb-0">Differential Probes</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-compliance"></i><span>Compliance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Perform PCI-SIG® automated compliance tests (transmitter, receiver, and industry leading Tx/Rx LinkEQ) using Teledyne LeCroy WaveMaster and LabMaster oscilloscopes, CLB/CBB fixtures from the PCI-SIG, and QualiPHY compliance test software</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-lg-4"><a class="btn btn-default" href="/pcie-electrical-test/">Show PCIe Electrical Test Solutions</a></div>
										</div>
										<div class="col-xl-6">
											<h3>Protocol Layer</h3>
											<p>Provide PCIe and NVMe compliance test suites for both hosts and devices. Ideal for self-testing your device in the comfort of your own lab. View detail results indicating specific failures. Export results to disseminate to other colleagues or subject matter experts.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-4-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-4-tx-rx@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/pcie-electrical-test/">QualiPHY</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>QualiPHY software options QPHY-PCIE3-TX-RX, QPHY-PCIE4-TX-RX, and QPHY-PCIE5-TX-RX fully automate all aspects of PCIe compliance testing. Teledyne LeCroy provides full application support for all integrated electrical test solutions.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-9"><i class="icon-testing"></i><span>Regression Testing</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
															<p class="mb-0">Protocol Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
															<p class="mb-0">Protocol Analyzer /Jammer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
															<p class="mb-0">Protocol Exerciser / Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, test and debug the relevant PCI Express specification.</p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Physical Layer</h3>
											<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Physical Layer</h3>
											<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Protocol Layer</h3>
											<p>PCIe Exercisers and Protocol Analyzers provide early status of bus operation and fundamental host device communication.</p>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Protocol Layer</h3>
											<p>Protocol Exercisers, Analyzers and Jammers can provide a full range of testing by identifying any operational issues, emulating devices and systems or injecting errors into the communication path for live traffic testing.</p>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Debug state machine and other digital issues using ProtoSync oscilloscope-based Protocol Analysis Software, DH Series Active Differential Probes and protocol analyzer cross triggering.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>PCIe Protocol Analyzers and exercisers provide PCIe, NVMe, and CXL emulation and monitoring of different types of systems giving maximum range of testing platforms.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/proto-sync.jpg, https://assets.lcry.net/images/proto-sync@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/options/productseries.aspx?mseries=287&amp;groupid=88">ProtoSync</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The ProtoSync option extends the Teledyne LeCroy decode options by enabling industry-standard protocol analyzer software to be run directly on the oscilloscope.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
														<p class="mb-0">Differential Probes</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Perform PCI-SIG® automated compliance tests (transmitter, receiver, and industry leading Tx/Rx LinkEQ) using Teledyne LeCroy WaveMaster and LabMaster oscilloscopes, CLB/CBB fixtures from the PCI-SIG, and QualiPHY compliance test software</p>
										<div class="wrap-btn pt-lg-0 mb-3 mb-lg-4"><a class="btn btn-default" href="/pcie-electrical-test/">Show PCIe Electrical Test Solutions</a></div>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Provide PCIe and NVMe compliance test suites for both hosts and devices. Ideal for self-testing your device in the comfort of your own lab. View detail results indicating specific failures. Export results to disseminate to other colleagues or subject matter experts.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-4-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-4-tx-rx@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/pcie-electrical-test/">QualiPHY</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>QualiPHY software options QPHY-PCIE3-TX-RX, QPHY-PCIE4-TX-RX, and QPHY-PCIE5-TX-RX fully automate all aspects of PCIe compliance testing. Teledyne LeCroy provides full application support for all integrated electrical test solutions.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-9">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer /Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="generations">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-md-2 pb-lg-1 pb-xl-4 mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Test Solutions for Every Data Rate</h2>
									<div class="row">
										<div class="col mx-auto px-lg-5">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy provides solutions for every data rate: PCI Express® 1.0 through PCI Express 5.0. Protocol analyzers and oscilloscopes allow for a comprehensive view of your PCIe® environment, and ProtoSyncTM links Teledyne LeCroy protocol analyzers and oscilloscopes for a unified view of physical layer and protocol layer activities.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row flex-lg-row-reverse mb-xl-4">
							<div class="col-lg-6 mb-3 mb-sm-4 pb-sm-2 mb-md-4 pb-md-2 mb-lg-0 pb-lg-0">
								<div class="visual">
									<picture>
										<source srcset="https://assets.lcry.net/images/PCIe-Leadership.png, https://assets.lcry.net/images/PCIe-Leadership@2x.png 2x"/><img src="https://assets.lcry.net/images/PCIe-Leadership.png" alt="PCIe Leadership"/>
									</picture>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="text pr-lg-5 mr-xl-5">
									<h3 class="title">PCIe Generations</h3>
									<p>PCI Express is the leading interconnect technology. Teledyne LeCroy offers a rich set of tools that aid in developing, testing, analyzing, and debugging PCI Express across ALL generations of the technology. Each generation offers a unique set of speed, capabilities, and features.</p>
									<p>Teledyne LeCroy is widely recognized as having the most extensive range of test solutions and is the industry leader for all aspects of physical and protocol layer testing, and simultaneous time- correlated physical/protocol layer tests. Our physical layer and protocol layer tools work together to permit faster debug and more comprehensive support.</p>
								</div>
							</div>
						</div>
						<div class="box-pci-generations tabs-area pt-lg-5">
							<ul class="list-gen acc-gen tabs-gen" id="products">
								<li class="gen-item"><a class="opener-gen" href="#" data-href="#tab-gen-1"><span>Gen 5.0</span><br><span class="speed">32 GT/s</span></a>
									<div class="slide-gen">
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
														<p class="mb-0">Protocol Exerciser / Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
													</div>
												</div>
											</li>
                                            <li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
														<p class="mb-0">Protocol Analyzer / Jammer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
													</div>
												</div>
											</li>
										    <li class="product-card">
											    <div class="header-product">
												    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
												    <div class="text">
													    <h4 class="title"><a href="/oscilloscope/oscilloscopemodel.aspx?modelid=9141&amp;capid=102&amp;mid=504">50 GHz LabMaster 10-50Zi-A</a></h4>
													    <p class="mb-0">Oscilloscope</p>
												    </div>
											    </div>
											    <div class="content">
												    <div class="main">
													    <p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
												    </div>
											    </div>
										    </li>
										</ul>
									</div>
								</li>
								<li class="gen-item active-tab active"><a class="opener-gen" href="#" data-href="#tab-gen-2"><span>Gen 4.0</span><br><span class="speed">16 GT/s</span></a>
									<div class="slide-gen">
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-Z416.jpg, https://assets.lcry.net/images/Summit-Z416@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z416-protocol-exerciser">Summit Z416</a></h4>
														<p class="mb-0">Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>PCIe Gen 4.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
														<p class="mb-0">Protocol Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T48.jpg, https://assets.lcry.net/images/Summit-T48@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t48-analyzer">Summit T48</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/25-GHz-LabMaster-10-Zi-A.jpg, https://assets.lcry.net/images/25-GHz-LabMaster-10-Zi-A@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/oscilloscopemodel.aspx?modelid=9138&amp;capid=102&amp;mid=504">25 GHz LabMaster 10-25Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</li>
								<li class="gen-item"><a class="opener-gen" href="#" data-href="#tab-gen-3"><span>Gen 3.0</span><br><span class="speed">8 GT/s</span></a>
									<div class="slide-gen">
										<ul class="list-product-cards">
                                            
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-Z3-16.jpg, https://assets.lcry.net/images/Summit-Z3-16@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z3-16-exerciser-with-smbus-support">Summit Z3-16</a></h4>
														<p class="mb-0">Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It can emulate PCI Express root complexes or device endpoints, allowing new designs to be tested against corner case issues. In addition, it can emulate SMBus traffic as a master or slave.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T34.jpg, https://assets.lcry.net/images/Summit-T34@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t34-analyzer">Summit T34</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures, decodes and displays PCIe 3.0 protocol traffic data rates for x1, x2, x4 lane widths</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T3-8.jpg, https://assets.lcry.net/images/Summit-T3-8@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t3-8-analyzer">Summit T3-8</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures, decodes and displays PCIe 3.0 protocol traffic.data rates for x1, x2, x4, x8 lane widths</p>
													</div>
												</div>
											</li>
                                            <li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit_T3-16.png, https://assets.lcry.net/images/Summit_T3-16@2x.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t3-16-analyzer">Summit T3-16</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The Summit T3-16 Protocol Analyzer captures, decodes and displays PCIe 3.0 protocol traffic data rates for x1, x2, x4, x8, x16 lane widths.</p>
													</div>
												</div>
											</li>
									        <li class="product-card">
										        <div class="header-product">
											        <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
											        <div class="text">
												        <h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes/wavemaster-813zi-b">13 GHz SDA 813Zi-B</a></h4>
												        <p class="mb-0">Oscilloscope</p>
											        </div>
										        </div>
										        <div class="content">
											        <div class="main">
												        <p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
											        </div>
										        </div>
									        </li>
									        <li class="product-card">
										        <div class="header-product">
											        <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
											        <div class="text">
												        <h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
												        <p class="mb-0">Oscilloscope</p>
											        </div>
										        </div>
										        <div class="content">
											        <div class="main">
												        <p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
											        </div>
										        </div>
									        </li>
										</ul>
									</div>
								</li>
								<li class="gen-item"><a class="opener-gen" href="#" data-href="#tab-gen-4"><span>Gen 2.0 &amp; Gen 1.0</span><br><span class="speed">5 GT/s - 2.5 GT/s</span></a>
									<div class="slide-gen">
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T24.jpg, https://assets.lcry.net/images/Summit-T24@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t24-analyzer">Summit T24</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It is for customers developing PCIe 1.0 or 2.0 x4 lane width server, workstation, desktop, graphics, storage, and network card applications</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T28.jpg, https://assets.lcry.net/images/Summit-T28@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t28-analyzer">Summit T28</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>captures, decodes and displays PCIe 2.5GT/s and 5GT/s data rates for x1, x2, x4, x8 lane widths</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes/wavemaster-808zi-b">8 GHz SDA 808Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</li>
							</ul>
							<div class="tab-holder p-0 m-0">
								<div id="tab-gen-1">
									<ul class="list-product-cards">
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/z-58.jpg, https://assets.lcry.net/images/z-58@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z58-analyzer">Summit Z58</a></h4>
													<p class="mb-0">Protocol Exerciser / Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The Summit Z58 is a PCIe 5.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems. The Summit Z58 can emulate PCI Express 5.0 root complexes or device endpoints, allowing new designs to be tested against corner case issues.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m-5-x.jpg, https://assets.lcry.net/images/m-5-x@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-m5x">Summit M5x</a></h4>
													<p class="mb-0">Protocol Analyzer /Jammer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The Summit M5x is Teledyne LeCroy's PCIe/ NVMe Jammer solution and is the latest protocol analyzer targeted at high speed PCI Express 5.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T54.jpg, https://assets.lcry.net/images/Summit-T54@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t54-analyzer">Summit T54</a></h4>
													<p class="mb-0">Protocol Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The Summit T54 Protocol Analyzer captures, decodes and displays PCIe 5.0 protocol traffic data rates for x1, x2, x4 lane widths.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/oscilloscope/oscilloscopemodel.aspx?modelid=9141&amp;capid=102&amp;mid=504">50 GHz LabMaster 10-50Zi-A</a></h4>
													<p class="mb-0">Oscilloscope</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="tab-gen-2">
									<ul class="list-product-cards">
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-Z416.jpg, https://assets.lcry.net/images/Summit-Z416@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z416-protocol-exerciser">Summit Z416</a></h4>
													<p class="mb-0">Exerciser</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>PCIe Gen 4.0 protocol traffic generation test tool used for critical test and verification intended to assist engineers in developing and improving the reliability of their systems.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T416.jpg, https://assets.lcry.net/images/Summit-T416@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t416-analyzer">Summit T416</a></h4>
													<p class="mb-0">Protocol Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The Summit T416 is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T48.jpg, https://assets.lcry.net/images/Summit-T48@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t48-analyzer">Summit T48</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>It is Teledyne LeCroy's latest generation of protocol analyzers targeted at high speed PCI Express 4.0 I/O-based applications such as workstation, desktop, graphics, storage, and network card applications.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/25-GHz-LabMaster-10-Zi-A.jpg, https://assets.lcry.net/images/25-GHz-LabMaster-10-Zi-A@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/oscilloscope/oscilloscopemodel.aspx?modelid=9138&amp;capid=102&amp;mid=504">25 GHz LabMaster 10-25Zi-A</a></h4>
													<p class="mb-0">Oscilloscope</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="tab-gen-3">
									<ul class="list-product-cards">
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-Z3-16.jpg, https://assets.lcry.net/images/Summit-Z3-16@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-z3-16-exerciser-with-smbus-support">Summit Z3-16</a></h4>
													<p class="mb-0">Exerciser</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>It can emulate PCI Express root complexes or device endpoints, allowing new designs to be tested against corner case issues. In addition, it can emulate SMBus traffic as a master or slave.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T34.jpg, https://assets.lcry.net/images/Summit-T34@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t34-analyzer">Summit T34</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>It captures, decodes and displays PCIe 3.0 protocol traffic data rates for x1, x2, x4 lane widths</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T3-8.jpg, https://assets.lcry.net/images/Summit-T3-8@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t3-8-analyzer">Summit T3-8</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>It captures, decodes and displays PCIe 3.0 protocol traffic.data rates for x1, x2, x4, x8 lane widths</p>
												</div>
											</div>
										</li>
                                        <li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit_T3-16.png, https://assets.lcry.net/images/Summit_T3-16@2x.png 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t3-16-analyzer">Summit T3-16</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The Summit T3-16 Protocol Analyzer captures, decodes and displays PCIe 3.0 protocol traffic data rates for x1, x2, x4, x8, x16 lane widths</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes/wavemaster-813zi-b">13 GHz SDA 813Zi-B</a></h4>
													<p class="mb-0">Oscilloscope</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
													<p class="mb-0">Oscilloscope</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.</p>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="tab-gen-4">
									<ul class="list-product-cards">
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T24.jpg, https://assets.lcry.net/images/Summit-T24@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t24-analyzer">Summit T24</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>It is for customers developing PCIe 1.0 or 2.0 x4 lane width server, workstation, desktop, graphics, storage, and network card applications</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/Summit-T28.jpg, https://assets.lcry.net/images/Summit-T28@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/protocolanalyzer/pci-express/summit-t28-analyzer">Summit T28</a></h4>
													<p class="mb-0">Analyzer</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>captures, decodes and displays PCIe 2.5GT/s and 5GT/s data rates for x1, x2, x4, x8 lane widths</p>
												</div>
											</div>
										</li>
										<li class="product-card">
											<div class="header-product">
												<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
												<div class="text">
													<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes/wavemaster-808zi-b">8 GHz SDA 808Zi-B</a></h4>
													<p class="mb-0">Oscilloscope</p>
												</div>
											</div>
											<div class="content">
												<div class="main">
													<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding mb-0 section-gray" id="videos">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="section-heading text-center">
									<h2 class="text-black title">On Demand Videos</h2>
								</div>
							</div>
						</div>
						<div class="row content-wrap">
							<script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async=""></script>
							<script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script>
							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/9h464x50vi" title="PCI Express® Electrical Test Solutions Video" allow="fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>


							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/wg2qton0b6" title="PCI Express® 5.0 Transmitter Electrical Test Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/be67dze7ak" title="PCI Express® Transmitter Link Equalization Testing Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

                          <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                     								<iframe src="https://fast.wistia.net/embed/iframe/awdr4t1sca" title="PCI Express® Receiver Link Equalization Calibration and Test Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                                                     <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                     							</div>
                          <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            <iframe src="https://fast.wistia.net/embed/iframe/vm89wbg45s" title="Summit M5x Jammer Part 1 - Introduction Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="340"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                          </div>

						</div>
						<div class="row text-center justify-content-center"><a class="btn btn-default" href="/support/techlib/videos.aspx">Video Library</a></div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
