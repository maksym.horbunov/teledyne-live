﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Peripherals.aspx.vb" Inherits="LeCroy.Website.SerialData_Peripherals" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Peripherals</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/physical-layer-test-solutions-2.png, https://assets.lcry.net/images/physical-layer-test-solutions-2@2x.png 2x"/><img src="https://assets.lcry.net/images/physical-layer-test-solutions-2.png" alt="Physical Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<ul class="list-dots">
											<li>Fast and accurate transmitter (Tx) and receiver (Rx) compliance testing for USB4 and Thunderbolt 3</li>
											<li>DisplayPort™ over USB-C electrical PHY compliance</li>
											<li>USB 3.x, USB 2.0, USB PD compliance and debug</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/usb-electrical-test/">Explore Physical Layer Test Solutions</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-periph-prot_01.jpg, https://assets.lcry.net/images/img-periph-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-periph-prot_01.jpg" alt="Protocol Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Layer Test Solutions</h3>
										<p>100% Coverage for USB4, USB 3.2, PD & USB Type-C™ Verification.</p>
										<ul class="list-dots">
											<li>USB-IF Approved solutions for PD, Type-C, USB Link and Hub Compliance</li>
											<li>The industry’s first USB4 / Thunderbolt™ 3 Analyzer / Exerciser platform</li>
											<li>Defacto Standard CATC Trace display for fast analysis and problem resolution</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/usb">Explore Protocol Test Solutions</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Intermediate and advanced host or peripheral training classes - on site, on location and virtual</li>
											<li>Hardware and device testing services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-lifecycle section-padding section-gray" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Peripherals Product Lifecycle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy has the right test tool for every stage of the product lifecycle and for every layer, from the physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
												</picture>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Training</h3>
												<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, test and debug the relevant PCI Express specification.</p>
												<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>In addition to compliance verification and error injection, the Voyager exerciser can emulate a USB endpoint allowing developers to test power-on behaviors with complete control of timing and link states. Fully configurable, the exerciser supports simultaneous generation of USB and PD traffic allowing users to verify low-level protocol behaviors.</p>
										</div>
										<div class="col-xl-12">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>In addition to compliance verification and error injection, the Voyager exerciser can emulate a USB endpoint allowing developers to test power-on behaviors with complete control of timing and link states. Fully configurable, the exerciser supports simultaneous generation of USB and PD traffic allowing users to verify low-level protocol behaviors.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-bring-up"></i><span>Bring Up</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Protocol Layer</h3>
												<p>Protocol Exercisers / Analyzers can provide a full range of testing by identifying any operational issues, emulating devices and systems for live traffic testing.</p>
											</div>
										</div>
										<div class="col-xl-12">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Since the introduction of serial storage technologies, Teledyne LeCroy has evolved their storage analyzer family with a range of solutions that address every stage of the product validation process.  With best-in-class features including non-intrusive probing, automated compliance suites and easy to use software, Teledyne LeCroy leads the industry with countless innovations in data analysis to help reduce time-to-market for storage devices, systems and software.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Debug link training state machine and other digital issues ProtoSync™ oscilloscope-based protocol analysis software and DH Series Active Differential Probes. Debug USB Power Delivery issues using Passive Probes with Power Delivery TDME software.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Teledyne LeCroy’s legendary Voyager analyzer platform provides the industry’s most accurate and reliable capture of USB4, USB 3.2 and Thunderbolt 3™ protocols for fast debug, analysis and problem solving. Featuring the CATC Trace - the industry's de facto standard display for USB protocol analysis, this customizable application shows all packets labeled and decoded in a single display allowing designers and validation teams to quickly debug problems and verify interoperability for next-generation USB systems.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/proto-sync.jpg, https://assets.lcry.net/images/proto-sync@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/options/productseries.aspx?mseries=287&amp;groupid=88">ProtoSync</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The ProtoSync option extends the Teledyne LeCroy decode options by enabling industry-standard protocol analyzer software to be run directly on the oscilloscope.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
															<p class="mb-0">Differential Probes</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-lecroy-advisor-t3.jpg, https://assets.lcry.net/images/title-lecroy-advisor-t3@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/advisor-t3">Advisor T3</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Ultra-portable SuperSpeed USB analyzer delivers market leading accuracy at an extraordinary price.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-compliance"></i><span>Compliance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>
												Teledyne LeCroy LabMaster and WaveMaster oscilloscopes with QualiPHY compliance test software is the heart of Tx and Rx compliance testing.
												<a class="text-blue" href="#">QPHY-USB4-TX-RX</a>
												and
												<a class="text-blue" href="#">QPHY-USB3.2-TX-RX</a>
												works in conjunction with the Anritsu MP1900A to provide fully automated testing. The
												<a class="text-blue" href="#">LabMaster 10 Zi-A</a>
												features the industry’s only ‘Multi-Lane Compliance Test Architecture’, which results in faster compliance test throughput for USB4 and DisplayPort 2.0 over USB-C (two and four lane serial) device configurations.
											</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>The Voyager M310P with USB 3.1 and PD compliance options provide comprehensive support for Type-C, PD, Link Layer and Hub compliance testing. The M310P utilizes an integrated exerciser capable of emulating host and endpoint behaviors for USB 2.0, 3,1, in addition to PD Source, Sink and Dual-Role devices.  Now enhanced to support all the latest Type-C compliance checks plus the Power Delivery eye-diagram tests, the M310P is the ‘one-stop’ solution for USB certification.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-4-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-4-tx-rx@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/options/productseries.aspx?mseries=612&amp;groupid=140">QPHY-USB4-TX-RX</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>QualiPHY automates USB4™ (Universal Serial Bus 4) physical layer (PHY) testing in accordance with the USB-IF USB4 and Thunderbolt 3 CTS (Compliance Test Specifications) over the USB Type-C® connector.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-3-2-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-3-2-tx-rx@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/options/productseries.aspx?mseries=584&amp;groupid=140">QPHY-USB3.2-TX-RX</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It offers an automated test package for USB 3.2 transmitter and receiver compliance testing, characterization, and debug.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Resolving corner-case interoperability issues requires flexible triggering across the protocol stack. The Voyager family features hardware triggering with multi-level sequential event monitoring that can detect timing, protocol or handshake errors. For system level test, the Voyager provides visibility to software issues by automatically assigning USB decodes including MTP, CCD, Video and Mass storage class.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-9"><i class="icon-testing"></i><span>Regression Testing</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>USB remains the dominant interconnect technology in the consumer electronics market and continues to deliver innovative features in power management, ease of use and reliability. Teledyne LeCroy offers the broadest and deepest solution set for USB test and continues to support developers with free software updates, automation tools, and timely support for the latest enhancements in the USB specifications.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
															<p class="mb-0">Analyzer / Exerciser</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-lecroy-advisor-t3.jpg, https://assets.lcry.net/images/title-lecroy-advisor-t3@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/protocolanalyzer/usb/advisor-t3">Advisor T3</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Ultra-portable SuperSpeed USB analyzer delivers market leading accuracy at an extraordinary price.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, test and debug the relevant PCI Express specification.</p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>In addition to compliance verification and error injection, the Voyager exerciser can emulate a USB endpoint allowing developers to test power-on behaviors with complete control of timing and link states. Fully configurable, the exerciser supports simultaneous generation of USB and PD traffic allowing users to verify low-level protocol behaviors.</p>
									</div>
									<div class="col-xl-12 pt-lg-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>In addition to compliance verification and error injection, the Voyager exerciser can emulate a USB endpoint allowing developers to test power-on behaviors with complete control of timing and link states. Fully configurable, the exerciser supports simultaneous generation of USB and PD traffic allowing users to verify low-level protocol behaviors.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Protocol Layer</h3>
											<p>Protocol Exercisers / Analyzers can provide a full range of testing by identifying any operational issues, emulating devices and systems for live traffic testing.</p>
										</div>
									</div>
									<div class="col-xl-12">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Since the introduction of serial storage technologies, Teledyne LeCroy has evolved their storage analyzer family with a range of solutions that address every stage of the product validation process.  With best-in-class features including non-intrusive probing, automated compliance suites and easy to use software, Teledyne LeCroy leads the industry with countless innovations in data analysis to help reduce time-to-market for storage devices, systems and software.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Debug link training state machine and other digital issues ProtoSync™ oscilloscope-based protocol analysis software and DH Series Active Differential Probes. Debug USB Power Delivery issues using Passive Probes with Power Delivery TDME software.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Teledyne LeCroy’s legendary Voyager analyzer platform provides the industry’s most accurate and reliable capture of USB4, USB 3.2 and Thunderbolt 3™ protocols for fast debug, analysis and problem solving. Featuring the CATC Trace - the industry's de facto standard display for USB protocol analysis, this customizable application shows all packets labeled and decoded in a single display allowing designers and validation teams to quickly debug problems and verify interoperability for next-generation USB systems.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/proto-sync.jpg, https://assets.lcry.net/images/proto-sync@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/options/productseries.aspx?mseries=287&amp;groupid=88">ProtoSync</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The ProtoSync option extends the Teledyne LeCroy decode options by enabling industry-standard protocol analyzer software to be run directly on the oscilloscope.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
														<p class="mb-0">Differential Probes</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-lecroy-advisor-t3.jpg, https://assets.lcry.net/images/title-lecroy-advisor-t3@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/advisor-t3">Advisor T3</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Ultra-portable SuperSpeed USB analyzer delivers market leading accuracy at an extraordinary price.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>
											Teledyne LeCroy LabMaster and WaveMaster oscilloscopes with QualiPHY compliance test software is the heart of Tx and Rx compliance testing.
											<a class="text-blue" href="/options/productseries.aspx?mseries=612&amp;groupid=140">QPHY-USB4-TX-RX</a>
											and
											<a class="text-blue" href="/options/productseries.aspx?mseries=584&amp;groupid=140">QPHY-USB3.2-TX-RX</a>
											works in conjunction with the Anritsu MP1900A to provide fully automated testing. The
											<a class="text-blue" href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a>
											features the industry’s only ‘Multi-Lane Compliance Test Architecture’, which results in faster compliance test throughput for USB4 and DisplayPort 2.0 over USB-C (two and four lane serial) device configurations.
										</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>The Voyager M310P with USB 3.1 and PD compliance options provide comprehensive support for Type-C, PD, Link Layer and Hub compliance testing. The M310P utilizes an integrated exerciser capable of emulating host and endpoint behaviors for USB 2.0, 3,1, in addition to PD Source, Sink and Dual-Role devices.  Now enhanced to support all the latest Type-C compliance checks plus the Power Delivery eye-diagram tests, the M310P is the ‘one-stop’ solution for USB certification.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-4-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-4-tx-rx@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/options/productseries.aspx?mseries=612&amp;groupid=140">QPHY-USB4-TX-RX</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>QualiPHY automates USB4™ (Universal Serial Bus 4) physical layer (PHY) testing in accordance with the USB-IF USB4 and Thunderbolt 3 CTS (Compliance Test Specifications) over the USB Type-C® connector.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/qphy-usb-3-2-tx-rx.jpg, https://assets.lcry.net/images/qphy-usb-3-2-tx-rx@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/options/productseries.aspx?mseries=584&amp;groupid=140">QPHY-USB3.2-TX-RX</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It offers an automated test package for USB 3.2 transmitter and receiver compliance testing, characterization, and debug.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Capture and review traffic ensuring that all errors and issues have been resolved and that performance meets design requirements.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Resolving corner-case interoperability issues requires flexible triggering across the protocol stack. The Voyager family features hardware triggering with multi-level sequential event monitoring that can detect timing, protocol or handshake errors. For system level test, the Voyager provides visibility to software issues by automatically assigning USB decodes including MTP, CCD, Video and Mass storage class.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m4x.jpg, https://assets.lcry.net/images/title-voyager-m4x@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m4x">Voyager M4x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The industry’s first USB 3.2, USB4 and Thunderbolt™ 3 testing and verification platform.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-9">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Bug fixes as well as software and hardware updates need to be tested, reviewed and matched against previous releases to provide forward compatibility and compliance to the specification.</p>
									</div>
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>USB remains the dominant interconnect technology in the consumer electronics market and continues to deliver innovative features in power management, ease of use and reliability. Teledyne LeCroy offers the broadest and deepest solution set for USB test and continues to support developers with free software updates, automation tools, and timely support for the latest enhancements in the USB specifications.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyager-m310p.jpg, https://assets.lcry.net/images/title-voyager-m310p@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m310p">Voyager M310P</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The workhorse protocol verification system for testing and emulating USB 2.0, USB 3.1, Type-C and Power Delivery protocol behaviors.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-voyagerm3x-title.jpg, https://assets.lcry.net/images/title-voyagerm3x-title@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/voyager-m3x">Voyager M3x</a></h4>
														<p class="mb-0">Analyzer / Exerciser</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Flexible and configurable validation platform for USB 2.0 and 3.0 verification.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/title-lecroy-advisor-t3.jpg, https://assets.lcry.net/images/title-lecroy-advisor-t3@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/protocolanalyzer/usb/advisor-t3">Advisor T3</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Ultra-portable SuperSpeed USB analyzer delivers market leading accuracy at an extraordinary price.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding mb-0" id="videos">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="section-heading text-center">
									<h2 class="text-black title">On Demand Videos</h2>
								</div>
							</div>
						</div>
						<div class="row content-wrap">
							<script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async=""></script>
							<script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script>
							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/b0obbucpmn" title="USB Type-C® Test Solutions Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/mkr0gourio" title="Getting Started with USB4 &amp; Thunderbolt 3 Protocol Webinar Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/dk6ebf8mpp" title="Debugging USB-C® Power Delivery Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/ixagdou9d4" title="Voyager M4x Analyzing USB4 and Thunderbolt 3 Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>
						</div>
						<div class="row text-center justify-content-center"><a class="btn btn-default" href="/support/techlib/videos.aspx">Video Library</a></div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
