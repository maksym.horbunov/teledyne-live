﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Power_Management.aspx.vb" Inherits="LeCroy.Website.SerialData_Power_Management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Power Management</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Power Management</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#testing">Testing</a></li>
									<li><a href="#standards">Standards</a></li>
									<li><a href="#tdme">TDME</a></li>
									<li><a href="#videos">Videos</a></li>
									<li><a href="#contacts">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-04.jpg, https://assets.lcry.net/images/img-testing-04@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-04.jpg" alt="Physical Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Physical Layer Test Solutions</h3>
										<p>Tools tailored towards power management speed up test and debug.</p>
										<ul class="list-dots">
											<li>Support for widely adopted <a class="text-blue" href="#standards">power management standards</a></li>
											<li><a class="text-blue" href="/usb-electrical-test/#four">USB Power Delivery</a> Electrical and Protocol Solutions</li>
											<li><a class="text-blue" href="#tdme">TDME</a> options for faster debug and validation efficiency</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-periph-prot_01.jpg, https://assets.lcry.net/images/img-periph-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-periph-prot_01.jpg" alt="Protocol Layer Test Solutions"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Layer Test Solutions</h3>
										<p>100% Coverage for USB4, USB 3.2, PD & USB Type-C™ Verification.</p>
										<ul class="list-dots">
											<li>USB-IF Approved solutions for Power Delivery, Type-C, USB Link and Hub Compliance</li>
											<li>Industry's only PHY layer tapping and protocol decoding solution</li>
											<li>Defacto Standard CATC Trace display for fast analysis and problem resolution</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="/protocolanalyzer/usb">Explore Protocol Test Solutions</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Training</h3>
										<p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
										<ul class="list-dots">
											<li>Intermediate and advanced host or peripheral training classes - on site, on location, and virtual</li>
											<li>Extensive hardware, integration/interoperation, failure analysis, and characterization test services</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding section-gray" id="standards">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">SPMI, SMBus, PMBus</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Tools tailored towards power management speed up test and debug</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/spmi-cover-title.png, https://assets.lcry.net/images/spmi-cover-title@2x.png 2x"/><img src="https://assets.lcry.net/images/spmi-cover-title.png" alt="SPMI"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SPMI</h3>
										<ul class="list-dots">
											<li>MIPI command sequence capable</li>
											<li>Non-standard frame support</li>
											<li>Full arbitration sequence support</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=602&amp;groupid=88">Explore SPMI</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/SMBus.png, https://assets.lcry.net/images/SMBus@2x.png 2x"/><img src="https://assets.lcry.net/images/SMBus.png" alt="SMBus"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">SMBus</h3>
										<ul class="list-dots">
											<li>All Command Protocols supported</li>
											<li>ARP, SMBALERT#, PEC error capable</li>
											<li>Compatible with Clock Stretching</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=616&amp;groupid=88">Explore SMBus</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/PMBus.png, https://assets.lcry.net/images/PMBus@2x.png 2x"/><img src="https://assets.lcry.net/images/PMBus.png" alt="PMBus"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">PMBus</h3>
										<ul class="list-dots">
											<li>Popular command code support</li>
											<li>Numeric format automatically filled</li>
											<li>Custom file for command definition</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=634&amp;groupid=88">Explore PMBus</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding" id="tdme">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Trigger, Decode, Measure/Graph, Eye Diagram</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Powerful and unique TDME options for faster debug and validation efficiency</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_D.png, https://assets.lcry.net/images/TDME_D@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_D.png" alt="Highest Performance Triggers"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Powerful Triggers and Decoders</h3>
										<ul class="list-dots">
											<li>Flexible Trigger Capability</li>
											<li>Intuitive, Color-Coded Overlays</li>
											<li>Filter decoder table on specific data</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#trigger">Explore Highest Performance Triggers</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_M.png, https://assets.lcry.net/images/TDME_M@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_M.png" alt="Measure and Graph Tools for Validation Efficiency"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Unique Measure and Graph Tools</h3>
										<ul class="list-dots">
											<li>Serial Data DAC and Graphing Tools</li>
											<li>Automated Timing Measurements</li>
											<li>Bus Status Measurements</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#measure">Explore Measure and Graph Tools for Validation Efficiency</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/TDME_E.png, https://assets.lcry.net/images/TDME_E@2x.png 2x"/><img src="https://assets.lcry.net/images/TDME_E.png" alt="Eye Diagrams &amp; Physical Layer Testing"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Flexible Eye Diagram Capability</h3>
										<ul class="list-dots">
											<li>4 Simultaneous eyes</li>
											<li>User-defined or pre-defined Masks</li>
											<li>Additional Physical Layer Testing</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="/trigger-decode/#eye">Explore Eye Diagrams &amp; Physical Layer Testing</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-padding mb-0 section-gray" id="videos">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-9">
								<div class="section-heading text-center">
									<h2 class="text-black title">Videos</h2>
								</div>
							</div>
						</div>
						<div class="row content-wrap">
							<script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async=""></script>
							<script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script>
							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/ii1h558jjx" title="SMBus Basics Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/mkr0gourio" title="Getting Started with USB4 &amp; Thunderbolt 3 Protocol Webinar Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/dk6ebf8mpp" title="Debugging USB-C® Power Delivery Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>

							<div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0 pb-sm-2 pb-md-0">
                            								<iframe src="https://fast.wistia.net/embed/iframe/ixagdou9d4" title="Voyager M4x Analyzing USB4 and Thunderbolt 3 Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="640" height="360"></iframe>
                            <script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>
                            							</div>
						</div>
						<div class="row text-center justify-content-center"><a class="btn btn-default" href="/support/techlib/videos.aspx">Video Library</a></div>
					</div>
				</section>
				<div id="contacts">
					<section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
				</div>
			</main>
</asp:Content>
