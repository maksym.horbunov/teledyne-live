Imports LeCroy.Library.VBUtilities

Partial Class SerialData_SDScopesOverview
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.SERIAL_CAPTION_ID
        Dim menuID As String = String.Empty
        Dim modelid As String = String.Empty

        If Len(Request.QueryString("modelid")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("modelid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ",") Then
                    modelid = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ",") + 1)
                Else
                    modelid = SQLStringWithOutSingleQuotes(Request.QueryString("modelid"))
                End If
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        Response.Redirect("/oscilloscope/oscilloscopemodel.aspx?modelid=" + modelid.ToString)
    End Sub
End Class