﻿Imports LeCroy.Library.VBUtilities

Partial Class SerialData_ScopeSeries
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.SERIAL_CAPTION_ID
        Dim menuID As String = String.Empty
        Dim mseries As String = String.Empty

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        Session("menuSelected") = captionID

        '** mseries
        If Len(Request.QueryString("mseries")) > 0 Then
            If IsNumeric(Request.QueryString("mseries")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") Then
                    mseries = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") + 1)
                Else
                    mseries = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                End If
                If Not Functions.CheckSeriesExists(mseries, 1) Then
                    Response.Redirect("default.aspx")
                End If
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        Response.Redirect("/oscilloscope/oscilloscopeseries.aspx?mseries=" + mseries.ToString)
    End Sub
End Class