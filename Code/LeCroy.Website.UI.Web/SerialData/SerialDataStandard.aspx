<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.SerialData_SerialDataStandard" Codebehind="SerialDataStandard.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="current"><a href="#"><asp:Label ID="lbSeries1" runat="server" /></a>
                    <ul>
                        <asp:Label ID="lbProduct1" runat="server" />
                    </ul>
                </li>
            </ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <div class="intro">
                    <h1><asp:Label ID="lbSeriesTileTop" runat="server" /></h1>
                    <p><asp:Label ID="lbSeriesDesc" runat="server" /></p>
                    <asp:Label ID="lbSeriesTileBelow" runat="server" CssClass="lecroyblue" />
                </div>
            </td>
            <td valign="top"><asp:Label ID="lbSeriesImage" runat="server" /></td>
        </tr>
    </table>
    <div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Label ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line"><asp:Label ID="lbQuickSpecs" runat="server" /></div>
            <div id="overview"><asp:Label ID="lblOverview" runat="server" /></div>
            <div id="rec_config"><asp:Label ID="lblRecConfig" runat="server" /></div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="lbPromotion" runat="server" />
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
    <div class="greyPadding"></div>
</asp:Content>