Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class SerialData_SerialDataStandard
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Dim mseries As String = ""
    Dim modelid As String = ""
    Dim firstmenuid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim promoImages As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Serial Data"
        Me.Master.PageContentHeader.BottomText = String.Empty
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        ' If Len(Request.QueryString("capid")) = 0 Then
        captionID = AppConstants.SERIAL_CAPTION_ID
        'Else
        'If IsNumeric(Request.QueryString("capid")) Then
        '    captionID = Request.QueryString("capid")
        'Else
        '    captionID = AppConstants.SERIAL_CAPTION_ID
        'End If
        'End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        '** mseries
        If Len(Request.QueryString("standardid")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("standardid")) Then
                mseries = Request.QueryString("standardid")
                If Not CheckSeriesExists(mseries, 21) Then
                    Response.Redirect("default.aspx")
                End If
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        Session("seriesSelected") = mseries

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries, 21)) Then
            Response.Redirect("~/")
        End If

        If Not Me.IsPostBack Then

            getProductLine()
            getOverview()
            getRecommendedConfigurations()
            lblTabsToShow.Text = tabsToShow.ToString()
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"

            '*left menu for series
            Dim sqlGetSeries As String
            Dim ds As New DataSet

            sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                lbSeries1.Text = ds.Tables(0).Rows(0)("NAME").ToString()
                lbSeriesTileTop.Text = ds.Tables(0).Rows(0)("SHORT_NAME").ToString()
                lbSeriesTileBelow.Text = "Explore " + ds.Tables(0).Rows(0)("NAME").ToString() + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + ds.Tables(0).Rows(0)("NAME").ToString() + "' >"
                lbSeriesImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString() + "' width='260' />"
                lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Serial Data Standard - " + ds.Tables(0).Rows(0)("NAME").ToString()
                promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_STANDARD.ToString, mseries.ToString).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_STANDARD.ToString, mseries.ToString)
            End If
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
        End If
    End Sub
    Private Sub getOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.STANDARD_OVERVIEW_TYPE.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If
    End Sub
    Private Sub getProductLine()
        '*left menu for series
        Dim sqlGetSeries As String
        sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            lbSeries1.Text = ds.Tables(0).Rows(0)("NAME").ToString()
        End If

        '*left menu for products
        Dim quickSpecsDs As New DataSet
        Dim pNum As Integer
        Dim i As Integer
        Dim products As String
        Dim productLine As String
        ds = New DataSet()
        Dim dss As DataSet = New DataSet()
        Dim countFromScope As String = ""
        Dim countFromSerialData As String = ""

        sqlParameters = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
        sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        strSQL = String.Format("select distinct product_series_id from PRODUCT_SERIES_CATEGORY where category_id = @CATEGORYID AND PRODUCT_ID NOT IN ({0}) and product_id in (select product_id from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID2) and PRODUCT_SERIES_ID=@PRODUCTSERIESID)", disabledProductContent.Item1)
        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        For Each dr As DataRow In dss.Tables(0).Rows
            'Count # of scopes assigned to serial data stndard
            sqlParameters = New List(Of SqlParameter)
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SCOPE))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID2", dr("product_series_id").ToString()))

            strSQL = String.Format("SELECT product_id FROM PRODUCT_SERIES_CATEGORY " +
                "where category_id = @CATEGORYID and PRODUCT_SERIES_ID=@PRODUCTSERIESID AND PRODUCT_ID NOT IN ({0}) and PRODUCT_ID in (select PRODUCT_ID from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID2) and product_series_id = @PRODUCTSERIESID2)", disabledProductContent.Item1)
            countFromSerialData = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count().ToString()

            'Count # scopes in the scope series
            'if  countFromSerialData=countFromScope, display scope series name, if not - partnumbers
            sqlParameters = New List(Of SqlParameter)
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("product_series_id").ToString()))
            strSQL = String.Format("SELECT product_id FROM PRODUCT_SERIES_CATEGORY where category_id = @CATEGORYID AND PRODUCT_ID NOT IN ({0}) and PRODUCT_SERIES_ID = @PRODUCTSERIESID", disabledProductContent.Item1)
            countFromScope = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count().ToString()
            'Response.Write(countFromSerialData & "<br>")
            ' Response.Write(countFromScope & "<br>")

            sqlParameters = New List(Of SqlParameter)
            If countFromSerialData = countFromScope Then
                strSQL = " SELECT NAME as partnumber,0 as PRODUCT_ID,1 as CATEGORY_ID," & dr("product_series_id").ToString() & " as PRODUCT_SERIES_ID , productimage as IMAGEFILENAME," +
                        " '/Oscilloscope/OscilloscopeSeries.aspx?mseries=' + cast(PRODUCT_SERIES_ID as varchar(5)) as target, " +
                        " '" & mseries & "' as standardid, PRODUCTIMAGEDESC as DESCRIPTION,  'quickBoxBlue' as css , 1 as sortid " +
                        " FROM PRODUCT_SERIES " +
                        " where PRODUCT_SERIES_ID=@PRODUCTSERIESID and disabled='n' order by sortid"
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("product_series_id").ToString()))
            Else
                strSQL = " SELECT     b.PARTNUMBER, a.PRODUCT_ID,1 as CATEGORY_ID," & dr("product_series_id").ToString() & " as PRODUCT_SERIES_ID, b.SMALL_IMAGE_PATH AS IMAGEFILENAME," +
                        " '/Oscilloscope/OscilloscopeModel.aspx?modelid=' + cast(a.PRODUCT_ID as varchar(5)) AS target, " +
                        " '" & mseries & "' AS standardid, b.PARTNUMBER AS DESCRIPTION,'quickBoxBlue' AS css, b.sort_id as sortid " +
                        " From PRODUCT_SERIES_CATEGORY a INNER JOIN product b  On a.product_id=b.productid " +
                        " where category_id = @CATEGORYID1 and PRODUCT_SERIES_ID=@PRODUCTSERIESID1 AND b.DISABLED ='n'  " +
                        " and PRODUCT_ID in ( " +
                        " select PRODUCT_ID from PRODUCT_SERIES_CATEGORY " +
                        " where category_id = @CATEGORYID2 and product_series_id = @PRODUCTSERIESID2) " +
                        " order by sort_id"
                sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_SERIAL_DATA.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID1", mseries))
                sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SCOPE.ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID2", dr("product_series_id").ToString()))
            End If
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Next

        'Protocol analyzers
        sqlParameters = New List(Of SqlParameter)
        strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME," +
                " '/ProtocolAnalyzer/ProtocolOverview.aspx?seriesid=' + cast(a.PRODUCT_SERIES_ID as varchar(5))  as target, " +
                " '" & mseries & "' as standardid, b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid " +
                " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " +
                " INNER JOIN product c  On a.product_id=c.productid " +
                " where(a.category_id = @CATEGORYID1) " +
                " AND c.disabled='n'  AND b.disabled='n' " +
                " and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY  " +
                " where(category_id = @CATEGORYID2) " +
                " and PRODUCT_SERIES_ID=@PRODUCTSERIESID1) " +
                " order by b.sortid"
        sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_PROTOCOL.ToString()))
        sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SERIAL_DATA.ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID1", mseries))
        ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()))

        'Product Groups - series of groups of products assigned to serieal data standard
        sqlParameters = New List(Of SqlParameter)
        strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME, " +
                " '/Options/ProductSeries.aspx?mseries=' + " +
                " cast(a.PRODUCT_SERIES_ID as varchar(5)) +  '&groupid=' + cast(c.group_id as varchar(5)) as target," +
                " '" & mseries & "' as standardid, b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid " +
                " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " +
                " INNER JOIN PRODUCT as c on a.product_id=c.productid where(a.category_id NOT in ( 1,19,21,20) )" +
                " AND c.disabled='n'  " +
                " and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY  " +
                " where(category_id = @CATEGORYID1) and PRODUCT_SERIES_ID=@PRODUCTSERIESID) " +
                " order by b.sortid"
        sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_SERIAL_DATA.ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()))
        'Response.Write(strSQL)
        'Response.End()
        pNum = ds.Tables(0).Rows.Count
        products = ""
        productLine = ""
        If pNum > 0 Then
            For i = 0 To pNum - 1
                Dim modelID As String = ds.Tables(0).Rows(i)("PARTNUMBER").ToString()
                Dim modelSeries As String = ds.Tables(0).Rows(i)("PRODUCT_SERIES_ID").ToString()
                productLine = productLine + "<div class='quickBox'>"

                products = products + "<li><a href='" & ds.Tables(0).Rows(i)("target").ToString() & "'> " + modelID + "</a></li>"

                productLine = productLine + "<a href='" & ds.Tables(0).Rows(i)("target").ToString() & "'><strong>" + modelID + "</strong></a> "

                productLine = productLine + "&nbsp;&nbsp;" + ds.Tables(0).Rows(i)("DESCRIPTION").ToString()
                productLine = productLine + "</div>"
            Next
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
        lbProduct1.Text = products
        lbQuickSpecs.Text = productLine
    End Sub

    Private Sub getRecommendedConfigurations()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.RECOMMENDED_CONFIGURATIONS.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblRecConfig.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#rec_config'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_rec_config_off.gif' id='rec_config' border='0'></a></li>")
        End If
    End Sub
End Class