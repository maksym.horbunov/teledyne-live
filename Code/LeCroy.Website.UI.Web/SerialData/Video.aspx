﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Video.aspx.vb" Inherits="LeCroy.Website.SerialData_Video" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main">
        <div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Video</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        <section class="section-cards-reverse section-padding" id="testing">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="col-content-item">
                            <div class="visual mb-3">
                                <picture>
											<source srcset="https://assets.lcry.net/images/physical-layer-test-solutions.jpg, https://assets.lcry.net/images/physical-layer-test-solutions@2x.jpg 2x"/><img src="https://assets.lcry.net/images/physical-layer-test-solutions.jpg" alt="image description"/>
										</picture>
                            </div>
                            <div class="card-body p-0">
                                <h3 class="title">Physical Layer Test Solutions</h3>
                                <ul class="list-dots">
                                    <li>Fully automated HDMI and DisplayPort Source PHY compliance testing (and beyond)</li>
                                    <li>VESA and HDMI Forum approved tests</li>
                                    <li>Comprehensive compliance report generation</li>
                                </ul>
                                <a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=241&amp;groupid=140">Explore DisplayPort PHY</a><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="/options/productseries.aspx?mseries=504&amp;groupid=140">Explore HDMI PHY</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="col-content-item">
                            <div class="visual mb-3">
                                <picture>
											<source srcset="https://assets.lcry.net/images/img-testing-05.jpg, https://assets.lcry.net/images/img-testing-05@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-05.jpg" alt="image description"/>
										</picture>
                            </div>
                            <div class="card-body p-0">
                                <h3 class="title">Protocol Layer Test Solutions</h3>
                                <p>The leading provider of protocol analyzers, signal generators and verification/compliance tools.</p>
                                <ul class="list-dots">
                                    <li>Complete solution for DisplayPort and HDMI compliance testing</li>
                                    <li>Full DisplayPort and HDMI specification support</li>
                                    <li>Industry leader in Video protocol analysis</li>
                                    <li>Offer source and sink testing with video analyzers and generators</li>
                                </ul>
                                <a class="large-link text-blue font-weight-medium pb-lg-0" href="http://quantumdata.com/">Explore Video Analyzers/Generators</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="col-content-item pb-0">
                            <div class="visual mb-3">
                                <picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
                            </div>
                            <div class="card-body p-0">
                                <h3 class="title">Testing and Training</h3>
                                <p>Teledyne LeCroy’s Austin Labs is a leading provider of independent testing and training services focused on server, storage, and networking interfaces, including PHY testing and protocols analysis.</p>
                                <ul class="list-dots">
                                    <li>Intermediate and advanced host or peripheral training classes - on site, on location and virtual</li>
                                    <li>Hardware and device testing services</li>
                                </ul>
                                <a class="large-link text-blue font-weight-medium pb-0" href="/protocolanalyzer/austin-labs/?capid=103&amp;mid=1150">Explore Austin Labs</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-lifecycle section-padding section-gray mb-0" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Video Product Lifecycle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy has the right test tool for every stage of the product lifecycle and for every layer, from the physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
												</picture>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Training</h3>
												<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant PCI Express specification.</p>
												<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Physical Layer</h3>
												<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
											</div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
															<p class="mb-0">Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-bring-up"></i><span>Bring Up</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
										    <h3 class="title">Protocol Layer</h3>
										    <p>HDMI and DisplayPort Video Generators and Protocol Analyzers provide a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
									    </div>
									    <div class="col-xl-12 pt-xl-3">
										    <h3 class="title">Featured Products</h3>
										    <ul class="list-product-cards">
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
														    <p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 provides a full range of testing for identifying operational issues through emulation of source and sink DP 1.4 and 2.0 devices and running complex diagnostic tests.</p>
													    </div>
												    </div>
											    </li>
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
														    <p class="mb-0">Video Analyzer / Generator</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing provides a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
													    </div>
												    </div>
											    </li>
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
														    <p class="mb-0">980B Series Test Instrument</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The 980B HDMI and DisplayPort Video Generator and Protocol Analyzer modules provide a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
													    </div>
												    </div>
											    </li>
										    </ul>
									    </div>
								    </div>
                                </div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and problem resolution.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
															<p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 is a compact versatile test instrument offering entry level functional testing that can be extended through software licenses to a full compliance tester with sophisticated analysis and diagnostic capabilities.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
															<p class="mb-0">Video Analyzer / Generator</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing is a compact versatile test instrument offering entry level functional testing that can be extended through software licenses to a full compliance tester with sophisticated analysis and diagnostic capabilities.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
															<p class="mb-0">980B Series Test Instrument</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The 980B HDMI and DisplayPort Video Generators and Protocol Analyzer modules enable a wide variety of functional tests and compliance tests for validating source and sink products.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p>Debug analog and digital issues on Main Link signals using DH Series active probes and sideband signals with high impedance passive probes – The WaveMaster 8 Zi-B provides best of both for analog and digital debug with its unique ‘upper and lower deck’ probing inputs.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
															<p class="mb-0">Oscilloscope</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
														</div>
													</div>
												</li>
                                                <li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
															<p class="mb-0">Differential Probes</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-compliance"></i><span>Compliance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
												<p>Teledyne LeCroy LabMaster and WaveMaster oscilloscopes with QualiPHY compliance test software (<a class="font-weight-medium text-blue" href="/options/productseries.aspx?mseries=504&groupid=140">QPHY-HDMI2</a> and <a class="text-blue font-weight-medium" href="/options/productseries.aspx?mseries=241&groupid=140">QPHY-DisplayPort</a>) automates Source compliance tests defined by VESA and HDMI PHY test specifications. The LabMaster 10 Zi-A features the industry’s only ‘Multi-Lane Compliance Test Architecture’, which results in faster compliance test throughput for HDMI 2.1 and DisplayPort 2.0 (two and four lane serial) device  configurations.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Perform full set of HDMI, DisplayPort and HDCP compliance test suites for both source and sink devices. Ideal for self-testing your device in the comfort of your own lab. View detail results indicating specific failures. Export results to disseminate to other colleagues or subject matter experts.</p>
										</div>
										<div class="col-xl-12">
										    <div class="col-xl-12 pt-xl-3">
											    <h3 class="title">Featured Products</h3>
											    <ul class="list-product-cards">
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															    <p class="mb-0">Oscilloscope</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
															    <p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 provides a full range of compliance testing for DisplayPort 1.4 including Link Layer, FEC/DSC and HDCP for both source and sink devices. Compliance test suites for DisplayPort 2.0 devices will be offered in the future as these tests are developed.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
															    <p class="mb-0">Video Analyzer / Generator</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing provides a full range of compliance test suites enabling you to self-test products for HDMI 2.1 protocol compliance.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
															    <p class="mb-0">980B Series Test Instrument</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The 980B HDMI 2.1 and DisplayPort 1.4 Video Analyzer/Generator modules provide a full range of compliance test suites enabling you to self-test products for protocol compliance.</p>
														    </div>
													    </div>
												    </li>
											    </ul>
										    </div>
									    </div>
								    </div>
                                </div>								
                                <div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Capture and analyze HDMI and DisplayPort video streams ensuring that all errors and issues have been resolved and that the device meets design requirements.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
														    <p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 is a compact versatile test instrument that will enable you to resolve errors and issues that may arise during DisplayPort 2.0 compliance testing for both source and sink devices.</p>
													    </div>
												    </div>
											    </li>
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
														    <p class="mb-0">Video Analyzer / Generator</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing is a compact versatile test instrument enabling you to resolve errors and issues that arise during compliance testing for both HDMI source and sink products.</p>
													    </div>
												    </div>
											    </li>
											    <li class="product-card">
												    <div class="header-product">
													    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
													    <div class="text">
														    <h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
														    <p class="mb-0">980B Series Test Instrument</p>
													    </div>
												    </div>
												    <div class="content">
													    <div class="main">
														    <p>The 980B HDMI 2.1 and DisplayPort 1.4 Video Analyzer/Generator modules enable you to resolve errors and issues that arise during HDMI and DisplayPort compliance testing for both source and sink products.</p>
													    </div>
												    </div>
											    </li>
											</ul>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant host or peripheral specification.</p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="/protocolanalyzer/austin-labs/">Get to know Austin Labs</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Physical Layer</h3>
											<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s WavePulser 40iX High-speed Interconnect Analyzer helps quickly and economically identify layout and interconnect issues. High Definition Oscilloscopes from 200 MHz – 8 GHz and power rail probes sniff out power integrity problems.</p>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wave-pulser-40-ix.jpg, https://assets.lcry.net/images/wave-pulser-40-ix@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/wavepulser/">WavePulser 40iX</a></h4>
														<p class="mb-0">Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox provides unmatched characterization insight of high-speed interconnects.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wavepro-hd.jpg, https://assets.lcry.net/images/wavepro-hd@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/waveprohd/">WavePro HD</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>Up to 5 Gpt of highly responsive acquisition memory gives more visibility into system behavior, and the exceptional analysis toolbox enables deep insight.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>8 GHz – 65 GHz high-bandwidth real-time oscilloscopes provide the required performance for multi-lane signal integrity testing at all data rates and all phases of the lifecycle.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>HDMI and DisplayPort Video Generators and Protocol Analyzers provide a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
														<p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 provides a full range of testing for identifying operational issues through emulation of source and sink DP 1.4 and 2.0 devices and running complex diagnostic tests.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
														<p class="mb-0">Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing provides a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
														<p class="mb-0">980B Series Test Instrument</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The 980B HDMI and DisplayPort Video Generator and Protocol Analyzer modules provide a full range of testing for identifying operational issues through emulation of source and sink devices and running complex diagnostic tests.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>

								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
														<p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 is a compact versatile test instrument offering entry level functional testing that can be extended through software licenses to a full compliance tester with sophisticated analysis and diagnostic capabilities.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
														<p class="mb-0">Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing is a compact versatile test instrument offering entry level functional testing that can be extended through software licenses to a full compliance tester with sophisticated analysis and diagnostic capabilities.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
														<p class="mb-0">980B Series Test Instrument</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The 980B HDMI and DisplayPort Video Generators and Protocol Analyzer modules enable a wide variety of functional tests and compliance tests for validating source and sink products.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p>Debug analog and digital issues on Main Link signals using DH Series active probes and sideband signals with high impedance passive probes – The WaveMaster 8 Zi-B provides best of both for analog and digital debug with its unique ‘upper and lower deck’ probing inputs</p>
									</div>.
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/8-zi-b.jpg, https://assets.lcry.net/images/8-zi-b@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/oscilloscope/wavemaster-sda-dda-8-zi-b-oscilloscopes">SDA 8 Zi-B</a></h4>
														<p class="mb-0">Oscilloscope</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The SDA 8 Zi-B combines the performance, signal fidelity and feature set needed for today’s high-speed measurements with the ease of use of a standard benchtop oscilloscope, at bandwidths from 4 to 30 GHz.</p>
													</div>
												</div>
											</li>
                                            <li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/differential-probes.jpg, https://assets.lcry.net/images/differential-probes@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="/probes/dh-series-differential-probes">DH Series (8-30 GHz)</a></h4>
														<p class="mb-0">Differential Probes</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The DH series of 8 to 30 GHz active differential probes provides high input dynamic range, large offset capability, low loading and excellent signal fidelity with a range of connection options.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
												<p>Teledyne LeCroy LabMaster and WaveMaster oscilloscopes with QualiPHY compliance test software (<a class="font-weight-medium text-blue" href="/options/productseries.aspx?mseries=504&groupid=140">QPHY-HDMI2</a> and <a class="text-blue font-weight-medium" href="/options/productseries.aspx?mseries=241&groupid=140">QPHY-DisplayPort</a>) automates Source compliance tests defined by VESA and HDMI PHY test specifications. The LabMaster 10 Zi-A features the industry’s only ‘Multi-Lane Compliance Test Architecture’, which results in faster compliance test throughput for HDMI 2.1 and DisplayPort 2.0 (two and four lane serial) device  configurations.</p>
										</div>
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Perform full set of HDMI, DisplayPort and HDCP compliance test suites for both source and sink devices. Ideal for self-testing your device in the comfort of your own lab. View detail results indicating specific failures. Export results to disseminate to other colleagues or subject matter experts.</p>
										</div>
										<div class="col-xl-12">
										    <div class="col-xl-12 pt-xl-3">
											    <h3 class="title">Featured Products</h3>
											    <ul class="list-product-cards">
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/labmaster-10-zi-a.jpg, https://assets.lcry.net/images/labmaster-10-zi-a@2x.jpg 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A</a></h4>
															    <p class="mb-0">Oscilloscope</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The LabMaster 10 Zi-A has superior signal fidelity from 20 GHz to 65 GHz, with a 20-core processor and up to 192 GB of RAM for significantly more processing power than competitors.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
															    <p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 provides a full range of compliance testing for DisplayPort 1.4 including Link Layer, FEC/DSC and HDCP for both source and sink devices. Compliance test suites for DisplayPort 2.0 devices will be offered in the future as these tests are developed.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
															    <p class="mb-0">Video Analyzer / Generator</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing provides a full range of compliance test suites enabling you to self-test products for HDMI 2.1 protocol compliance.</p>
														    </div>
													    </div>
												    </li>
												    <li class="product-card">
													    <div class="header-product">
														    <div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
														    <div class="text">
															    <h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
															    <p class="mb-0">980B Series Test Instrument</p>
														    </div>
													    </div>
													    <div class="content">
														    <div class="main">
															    <p>The 980B HDMI 2.1 and DisplayPort 1.4 Video Analyzer/Generator modules provide a full range of compliance test suites enabling you to self-test products for protocol compliance.</p>
														    </div>
													    </div>
												    </li>
											    </ul>
										    </div>
									    </div>
								    </div>
                                </div>
                                
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Capture and analyze HDMI and DisplayPort video streams ensuring that all errors and issues have been resolved and that the device meets design requirements.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m42d_angle.png, https://assets.lcry.net/images/m42d_angle.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m42d.html">quantumdata M42d</a></h4>
														<p class="mb-0">DisplayPort 80Gbps Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M42d 80G Video Analyzer/Generator for DisplayPort 2.0 is a compact versatile test instrument that will enable you to resolve errors and issues that may arise during DisplayPort 2.0 compliance testing for both source and sink devices.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/m41h.jpg, https://assets.lcry.net/images/m41h@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/m41h.html">quantumdata M41h</a></h4>
														<p class="mb-0">Video Analyzer / Generator</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The M41h 48G Video Analyzer/Generator for HDMI 8K Testing is a compact versatile test instrument enabling you to resolve errors and issues that arise during compliance testing for both HDMI source and sink products.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/980_capture.png, https://assets.lcry.net/images/980_capture.png 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://www.quantumdata.com/980b.html">quantumdata 980B</a></h4>
														<p class="mb-0">980B Series Test Instrument</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>The 980B HDMI 2.1 and DisplayPort 1.4 Video Analyzer/Generator modules enable you to resolve errors and issues that arise during HDMI and DisplayPort compliance testing for both source and sink products.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
        <div id="contacts">
            <section class="section-contact">
                <div class="container">
                    <div class="row text-center align-items-center">
                        <div class="col-12 col-sm-6 text-sm-right">
                            <p>Still have questions?</p>
                        </div>
                        <div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
                    </div>
                </div>
            </section>
        </div>
    </main>
</asp:Content>
