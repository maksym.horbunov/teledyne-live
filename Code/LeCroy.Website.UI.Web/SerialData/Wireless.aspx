﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Wireless.aspx.vb" Inherits="LeCroy.Website.SerialData_Wireless" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main">
				<div class="banner-top">
					<div class="jumbotron jumbotron-fluid bg-retina mb-0">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9">
									<h1 class="mb-0">Test Solutions for Wireless</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-lg">
									<div class="inner-footer">
										<div class="list-bullets">
											<div class="item-bullet">
												<p>Most extensive range <span>of serial data test solutions</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Acknowledged <span>industry leader</span>
												</p>
											</div>
											<div class="item-bullet">
												<p>Coordinated physical &amp; protocol <span>solutions accelerate designs</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section class="section-cards-reverse section-padding" id="testing">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-wireless-elec_01.jpg, https://assets.lcry.net/images/img-wireless-elec_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-wireless-elec_01.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">RF PHY Test Solution covering ISM band technologies Bluetooth (BR/EDR and LE) and 802.15.4</h3>
										<p>Highly versatile RF PHY test solution covering – Bluetooth LE, Bluetooth Classic (BR/EDR), and 802.15.4 Technologies and 2 modes of functionality covering Developer and Production line test environments.</p>
										<ul class="list-dots">
											<li>Bluetooth licenses include in built test scripts to execute test cases from Bluetooth RF Test Specification</li>
											<li>Signal Generator and Signal Analyzer modes to fully test your Product</li>
											<li>All the RF tools you need in one solution</li>
										</ul><a class="large-link text-blue font-weight-medium pb-lg-0" href="http://fte.com/products/TLF3000.aspx">Explore RF-PHY Testing</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-wireless-prot_01.jpg, https://assets.lcry.net/images/img-wireless-prot_01@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-wireless-prot_01.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Protocol Test Solutions for Bluetooth (BR/EDR and LE), 802.11, and 802.15.4</h3>
										<p>Protocol analysis – capture, analyze, and test devices that use the 2.4 GHz and 5GHz ISM bands and Conformance testing (Bluetooth LE)</p>
										<ul class="list-dots">
											<li>Complete captures of Bluetooth LE, Bluetooth Classic BR/EDR, 802.11 a/b/g/n and ac, and 802.15.4 Technologies</li>
											<li>Capture Wired technologies (HCI UART & Logic) alongside Wireless for comprehensive debugging</li>
											<li>Small footprint, USB bus powered and car adapter (included) for exceptional mobility</li>
											<li>Highly portable</li>
										</ul><a class="d-block large-link text-blue font-weight-medium pb-lg-0" href="http://fte.com/products/bluetooth.aspx">Explore Bluetooth Protocol Analysis Solutions</a><a class="large-link text-blue font-weight-medium pb-lg-0" href="http://fte.com/products/harmony.aspx">Explore Bluetooth LE Conformance Testing</a>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="col-content-item pb-0">
									<div class="visual mb-3">
										<picture>
											<source srcset="https://assets.lcry.net/images/img-testing-06.jpg, https://assets.lcry.net/images/img-testing-06@2x.jpg 2x"/><img src="https://assets.lcry.net/images/img-testing-06.jpg" alt="image description"/>
										</picture>
									</div>
									<div class="card-body p-0">
										<h3 class="title">Testing and Consulting Services for Wireless with Frontline Test Services</h3>
										<p>Make sure your team and your products are ready – Frontline Test Services offers both world class consultancy services and interoperability testing services.</p>
										<ul class="list-dots">
											<li>Leverage Teledyne LeCroy’s expertise and experience with technologies including Bluetooth, Wi-Fi, USB and more</li>
											<li>The FTS Team provides help during any part of your products lifecycle from design concept to after market</li>
											<li>Take advantage of one of the largest test device libraries in the industry</li>
											<li>Expand or enhance your QA department</li>
										</ul><a class="large-link text-blue font-weight-medium pb-0" href="http://fte.com/services/default.aspx">Explore Frontline Test Services</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-lifecycle section-padding section-gray mb-0" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Wireless Product Lifecycle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Teledyne LeCroy has the right test tool for every stage of the product lifecycle and for every layer, from the physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="visual">
												<picture>
													<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
												</picture>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Training</h3>
												<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant PCI Express specification.</p>
												<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="http://fte.com/services/default.aspx">Get know Frontline Test Services</a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Protocol Layer</h3>
												<p>
													Teledyne LeCroy’s Frontline Test Services is an invaluable resource during the prototype phase.  From finding issues to avoiding obstacles, our team can help you develop your wireless prototype and proof of concept.  They’ll use their technology expertise and the latest tools like the
													<a class="text-blue" href="http://fte.com/products/x240.aspx">Frontline X240 Wireless Wideband Analyzer</a>
													to ensure functionality, interoperability and troubleshoot issues.
												</p>
												<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
											</div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
															<p class="mb-0">Wireless Wideband Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Physical Layer</h3>
											<p><a class="text-blue" href="http://fte.com/products/TLF3000.aspx">TLF 3000 RF PHY Tester</a>
												captures and analyzes the entire 2402-2480 MHz band simultaneously providing RF PHY testing, signal generation, signal analysis in one powerful package.  It allows you to run your device through scripted RF tests from the Bluetooth SIG specifications and see how it responds to normal and out-of-spec scenarios.  Or, expand your QA and development department with
												<a class="text-blue" href="https://fte.com/services/default.aspx">Frontline Test Services</a>, who can take care of the testing and resolution of issues for you.
											</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/tlf-3000.jpg, https://assets.lcry.net/images/tlf-3000@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/TLF3000.aspx">Frontline TLF3000</a></h4>
															<p class="mb-0">RF PHY Tester</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures and analyzes the entire 2402-2480 MHz band simultaneously providing RF PHY testing, signal generation, signal analysis in one powerful package. The TLF 3000 hardware supports a number of different licensing options covering different RF technologies and customer use cases.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing to make sure your product is robust and works with your target market.</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
															<p class="mb-0">Wireless Wideband Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Simplify wireless debugging with the Frontline X240 Wireless Wideband Analyzer.  It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
															<p class="mb-0">Wireless Wideband Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-compliance"></i><span>Compliance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>The Frontline Harmony LE Tester is a robust conformance testing platform, focused on the Bluetooth low energy specification. It is an integrated software/hardware test platform for Bluetooth protocol qualification testing that provides test manager, upper tester, lower tester all within a single, portable box.</p>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/harmony-le.jpg, https://assets.lcry.net/images/harmony-le@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/harmony.aspx">Frontline Harmony LE</a></h4>
															<p class="mb-0">Conformance Tester</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It performs complete Bluetooth LE controller qualification testing for LE Link Layer and HCI test specifications, providing coverage for Bluetooth LE 4.0, 4.1, 4.2, 5, 5.1, 5.2 and support of upcoming Bluetooth specifications. Harmony is a reconized by the Bluetooth SIG as a validated test platform for Bluetooth LE conformance testing.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>You need the right solution to capture and decode complex wireless environments ensuring that errors are found early in the development cycle and not after your product is in the market. Expertise is available to help you find the bugs and Route Cause Analysis (RCA) to fix them.</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
															<p class="mb-0">Wireless Wideband Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-testing"></i><span>Regression Testing</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Protocol Layer</h3>
											<p>Thorough testing requires the best tools.  Use the Frontline X240 Wireless Wideband Analyzer with the latest Wireless Protocol Suite (WPS) software, including an automation API where you can test and retest every facet of your product, making sure you find any issues or problems before your customers do.  Frontline Test Services is a valuable resource to help you do complete testing of your products.</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
										</div>
										<div class="col-xl-12 pt-xl-3">
											<h3 class="title">Featured Products</h3>
											<ul class="list-product-cards">
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
															<p class="mb-0">Wireless Wideband Analyzer</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
														</div>
													</div>
												</li>
												<li class="product-card">
													<div class="header-product">
														<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wifi-software.jpg, https://assets.lcry.net/images/wifi-software@2x.jpg 2x"></span></div>
														<div class="text">
															<h4 class="title"><a href="https://fte.com/products/x240-wps.aspx">Frontline Wireless Protocol Suite</a></h4>
															<p class="mb-0">Software</p>
														</div>
													</div>
													<div class="content">
														<div class="main">
															<p>This is the analysis software that can display, decode, and analyze wireless data. It is powerful and flexible, supporting the lastest specification release but also including a streamlined interface that brings all of the analysis tools you need into a single dockable framework.</p>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools  needed to develop, test and debug the relevant PCI Express specification.</p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="http://fte.com/services/default.aspx">Get know Frontline Test Services</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Protocol Layer</h3>
											<p>
												Teledyne LeCroy’s Frontline Test Services is an invaluable resource during the prototype phase.  From finding issues to avoiding obstacles, our team can help you develop your wireless prototype and proof of concept.  They’ll use their technology expertise and the latest tools like the
												<a class="text-blue" href="http://fte.com/products/x240.aspx">Frontline X240 Wireless Wideband Analyzer</a>
												to ensure functionality, interoperability and troubleshoot issues.
											</p>
											<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
										</div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
														<p class="mb-0">Wireless Wideband Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<h3 class="title">Physical Layer</h3>
										<p><a class="text-blue" href="http://fte.com/products/TLF3000.aspx">TLF 3000 RF PHY Tester</a>
											captures and analyzes the entire 2402-2480 MHz band simultaneously providing RF PHY testing, signal generation, signal analysis in one powerful package.  It allows you to run your device through scripted RF tests from the Bluetooth SIG specifications and see how it responds to normal and out-of-spec scenarios.  Or, expand your QA and development department with
											<a class="text-blue" href="https://fte.com/services/default.aspx">Frontline Test Services</a>, who can take care of the testing and resolution of issues for you.
										</p>
										<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/tlf-3000.jpg, https://assets.lcry.net/images/tlf-3000@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/TLF3000.aspx">Frontline TLF3000</a></h4>
														<p class="mb-0">RF PHY Tester</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures and analyzes the entire 2402-2480 MHz band simultaneously providing RF PHY testing, signal generation, signal analysis in one powerful package. The TLF 3000 hardware supports a number of different licensing options covering different RF technologies and customer use cases.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Use of Protocol tools at this stage will ensure high performance error free operation and allow for various forms of corner case and regression testing to make sure your product is robust and works with your target market.</p>
										<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
														<p class="mb-0">Wireless Wideband Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Simplify wireless debugging with the Frontline X240 Wireless Wideband Analyzer.  It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
														<p class="mb-0">Wireless Wideband Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>The Frontline Harmony LE Tester is a robust conformance testing platform, focused on the Bluetooth low energy specification. It is an integrated software/hardware test platform for Bluetooth protocol qualification testing that provides test manager, upper tester, lower tester all within a single, portable box.</p>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/harmony-le.jpg, https://assets.lcry.net/images/harmony-le@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/harmony.aspx">Frontline Harmony LE</a></h4>
														<p class="mb-0">Conformance Tester</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It performs complete Bluetooth LE controller qualification testing for LE Link Layer and HCI test specifications, providing coverage for Bluetooth LE 4.0, 4.1, 4.2, 5, 5.1, 5.2 and support of upcoming Bluetooth specifications. Harmony is a reconized by the Bluetooth SIG as a validated test platform for Bluetooth LE conformance testing.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>You need the right solution to capture and decode complex wireless environments ensuring that errors are found early in the development cycle and not after your product is in the market. Expertise is available to help you find the bugs and Route Cause Analysis (RCA) to fix them.</p>
										<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
														<p class="mb-0">Wireless Wideband Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Protocol Layer</h3>
										<p>Thorough testing requires the best tools.  Use the Frontline X240 Wireless Wideband Analyzer with the latest Wireless Protocol Suite (WPS) software, including an automation API where you can test and retest every facet of your product, making sure you find any issues or problems before your customers do.  Frontline Test Services is a valuable resource to help you do complete testing of your products.</p>
										<div class="wrap-btn pt-lg-0 mb-3 mb-md-4"><a class="btn btn-default" href="https://fte.com/services/default.aspx">Frontline Test Services</a></div>
									</div>
									<div class="col-xl-12 pt-xl-3">
										<h3 class="title">Featured Products</h3>
										<ul class="list-product-cards">
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/x-240.jpg, https://assets.lcry.net/images/x-240@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="http://fte.com/products/x240.aspx">Frontline X240</a></h4>
														<p class="mb-0">Wireless Wideband Analyzer</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>It captures information streaming between wireless devices and is able to do so utilizing various technologies including Bluetooth, Wi-Fi, and 802.15.4. It is paired with powerful software to provide developers with a robust set of analysis tools including Bluetooth protocol analysis, Bluetooth audio analysis, and spectrum analysis.</p>
													</div>
												</div>
											</li>
											<li class="product-card">
												<div class="header-product">
													<div class="visual bg-retina"><span data-srcset="https://assets.lcry.net/images/wifi-software.jpg, https://assets.lcry.net/images/wifi-software@2x.jpg 2x"></span></div>
													<div class="text">
														<h4 class="title"><a href="https://fte.com/products/x240-wps.aspx">Frontline Wireless Protocol Suite</a></h4>
														<p class="mb-0">Software</p>
													</div>
												</div>
												<div class="content">
													<div class="main">
														<p>This is the analysis software that can display, decode, and analyze wireless data. It is powerful and flexible, supporting the lastest specification release but also including a streamlined interface that brings all of the analysis tools you need into a single dockable framework.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section><section class="section-contact">
						<div class="container">
							<div class="row text-center align-items-center">
								<div class="col-12 col-sm-6 text-sm-right">
									<p>Still have questions?</p>
								</div>
								<div class="col-12 col-sm-6 text-sm-left"><a class="btn btn-white" href="/support/contact/">Contact Us</a></div>
							</div>
						</div>
					</section>
			</main>
</asp:Content>
