<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.SerialData_SerialData" MaintainScrollPositionOnPostback="true" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" width="370">
                    <img src="<%=rootDir %>/images/category/headers/hd_serialdata.gif" alt="Serial Data" width="370" height="137">
                    <p>When it comes to serial data testing, Teledyne LeCroy oscilloscopes and analyzers lead the way in providing optimum solutions for high-speed signal and data analysis. In offering a complete toolbox for the busy test and measurement engineer, Teledyne LeCroy devices provide the ultimate end-to-end solution for the design cycle.</p>
                    <h2>Explore Serial Data<img src="/images/icons/icon_small_arrow_down.gif" /></h2>
                </td>
                <td valign="top" width="248"><img src="<%=rootDir %>/images/pcie4-01.png" style="width:365px;margin-top:30px;" border="0"></td>
                <td valign="top" width="203"><div class="subNav"><asp:Label ID="lb_topmenu" runat="server" /></div></td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <img src="<%=rootDir %>/images/category/headers/hd3_selection_guide.gif" alt="Selection Guide" width="105" height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="selectionGuide" style="border-left: 1px solid #dedede; border-bottom: 1px solid #dedede; border-right: 1px solid #dedede; border-top: 0px solid #dedede;">
            <tr>
                <td valign="top" width="132" style="border-top: 1px solid #dedede;">
                    <div class="type">Show Standards:</div>
                    <asp:CheckBoxList ID="standardlist" runat="server" AutoPostBack="True" CssClass="selection" />
                </td>
                <td valign="top" width="700" style="border-left: 1px solid #dedede;">
                    <asp:Repeater ID="serilRepeater" runat="server">
                        <ItemTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="productList">
                                <tr>
                                    <td colspan="4">
                                        <div class="description-first">
                                            <div class="heading"><asp:HyperLink ID="hlkStandard" runat="server" /></div>
                                            <%#Eval("PRODUCTIMAGEDESC")%>
                                            <asp:Label ID="PRODUCT_SERIES_ID" runat="server" Text='<%#Eval("PRODUCT_SERIES_ID")%>' Visible="false" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <ul>
                                            <asp:DataList ID="prodList" runat="server" RepeatDirection="Vertical">
                                                <ItemTemplate>
                                                    <li class="sdlink sasr round"><asp:HyperLink ID="hlkUrl" runat="server" /></li>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </ul>
                                    </td>
                                </tr>
                            </table><br />
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>