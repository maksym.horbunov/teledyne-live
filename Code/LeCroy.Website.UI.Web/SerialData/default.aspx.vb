Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class SerialData_SerialData
    Inherits BasePage

    Dim strSQL As String = ""
    Dim ds As DataSet
    Dim mseries As String = ""
    Dim modelid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Serial Data"
        Me.Master.PageContentHeader.BottomText = String.Empty
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Serial Data"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        ' If Len(Request.QueryString("capid")) = 0 Then
        captionID = AppConstants.SERIAL_CAPTION_ID
        'Else
        'If IsNumeric(Request.QueryString("capid")) Then
        '    captionID = Request.QueryString("capid")
        'Else
        '    captionID = AppConstants.SERIAL_CAPTION_ID
        'End If
        'End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            strSQL = "SELECT DISTINCT a. PRODUCT_SERIES_ID, a.NAME, a.PRODUCTIMAGEDESC, a.sortid " +
                    " FROM PRODUCT_SERIES a INNER JOIN " +
                    " PRODUCT_SERIES_CATEGORY b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
                    " INNER JOIN PRODUCT c ON b.PRODUCT_ID = c.PRODUCTID " +
                    " WHERE b.CATEGORY_ID=@CATEGORYID AND a.disabled='n' and (c.DISABLED='n' or a.VALIDATE_YN='n') order by a.sortid"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Me.standardlist.DataSource = ds
            Me.standardlist.DataTextField = "NAME"
            Me.standardlist.DataValueField = "PRODUCT_SERIES_ID"
            Me.standardlist.DataBind()
            Me.standardlist.Items.Insert(0, "ALL")
            Me.standardlist.Items(0).Selected = True
            Session("optionAllSelected") = "Y"
            showAllModels()
        End If
        lb_topmenu.Text = Functions.TopMenu(captionID, rootDir)
    End Sub
    Private Sub showAllModels()
        Dim ItemCount As Integer
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
        strSQL = String.Format("SELECT DISTINCT a. PRODUCT_SERIES_ID, a.NAME, a.PRODUCTIMAGEDESC, a.sortid, a.SUBSITE_URL FROM " +
        " PRODUCT_SERIES a INNER JOIN PRODUCT_SERIES_CATEGORY b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
        " INNER JOIN PRODUCT c ON b.product_id=c.productid WHERE a.disabled='n' and (c.PRODUCTID NOT IN ({0}) or a.VALIDATE_YN='n') AND b.CATEGORY_ID=@CATEGORYID", disabledProductContent.Item1)

        If Session("optionAllSelected") Is Nothing Then
            Response.Redirect("default.aspx" + menuURL2)
        End If

        Dim seriesIds As List(Of Int32) = New List(Of Int32)
        If Session("optionAllSelected").ToString() = "Y" Then
            For Each item As ListItem In Me.standardlist.Items
                If item.Selected = True And Not item.Value = "ALL" Then
                    seriesIds.Add(Int32.Parse(item.Value))
                End If
            Next
            If (seriesIds.Count > 0) Then
                Me.standardlist.Items(0).Selected = False
                Session("optionAllSelected") = "N"
            Else
                Me.standardlist.Items(0).Selected = True
                Session("optionAllSelected") = "Y"
            End If
        Else
            If Me.standardlist.Items(0).Selected = True Then
                For Each item As ListItem In Me.standardlist.Items
                    Me.standardlist.Items(ItemCount).Selected = False
                    ItemCount += 1
                Next
                Me.standardlist.Items(0).Selected = True
                Session("optionAllSelected") = "Y"
            Else
                For Each item As ListItem In Me.standardlist.Items
                    If item.Selected = True And Not item.Value = "ALL" Then
                        seriesIds.Add(Int32.Parse(item.Value))
                    End If
                Next
                If (seriesIds.Count <= 0) Then
                    Me.standardlist.Items(0).Selected = True
                    Session("optionAllSelected") = "Y"
                End If
            End If
        End If

        If (seriesIds.Count > 0) Then
            Dim sqlKeys As String = String.Empty
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(seriesIds, "ID", sqlKeys, sqlParameters)
            strSQL += String.Format(" and b. PRODUCT_SERIES_ID in ({0})", sqlKeys)
        End If
        strSQL += " order by a.sortid"
        ' Response.Write(strSQL)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Me.serilRepeater.DataSource = ds
        Me.serilRepeater.DataBind()
    End Sub
    Private Sub showModel(ByVal catid As String, ByRef dl As DataList)
        Dim dss As DataSet = New DataSet
        Dim ds As DataSet = New DataSet
        Dim countFromScope As String = ""
        Dim countFromSerialData As String = ""

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
        sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
        'select serial data standards for scopes based on config standard by product
        strSQL = String.Format("Select distinct product_series_id from PRODUCT_SERIES_CATEGORY " +
        "where(category_id = @CATEGORYID) And PRODUCT_ID Not In ({0}) And product_id In (Select product_id from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID2) And PRODUCT_SERIES_ID=@PRODUCTSERIESID)", disabledProductContent.Item1)
        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        For Each dr As DataRow In dss.Tables(0).Rows
            'Count # of scopes assigned to serial data stndard
            sqlParameters = New List(Of SqlParameter)
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
            sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SCOPE))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID2", dr("product_series_id").ToString()))
            strSQL = String.Format("Select product_id FROM PRODUCT_SERIES_CATEGORY " +
                "where(category_id = @CATEGORYID) And PRODUCT_SERIES_ID=@PRODUCTSERIESID And PRODUCT_ID Not In ({0}) " +
              "And PRODUCT_ID In (Select PRODUCT_ID from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID2) And product_series_id = @PRODUCTSERIESID2)", disabledProductContent.Item1)
            countFromSerialData = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count().ToString()

            'Count # scopes in the scope series
            'if  countFromSerialData=countFromScope, display scope series name, if not - partnumbers
            sqlParameters = New List(Of SqlParameter)
            If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("product_series_id").ToString()))
            strSQL = String.Format("Select product_id FROM PRODUCT_SERIES_CATEGORY where category_id = @CATEGORYID And PRODUCT_ID Not In ({0}) And PRODUCT_SERIES_ID = @PRODUCTSERIESID", disabledProductContent.Item1)
            countFromScope = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()).Count().ToString()
            'Response.Write(countFromSerialData & "<br>")
            ' Response.Write(countFromScope & "<br>")
            sqlParameters = New List(Of SqlParameter)
            If countFromSerialData = countFromScope Then
                strSQL = "Select NAME As partnumber,0 As PRODUCT_ID,1 As CATEGORY_ID," & dr("product_series_id").ToString() & " As PRODUCT_SERIES_ID, productimage As IMAGEFILENAME, " +
                         " '/oscilloscope/oscilloscopeseries.aspx?mseries=' + cast(PRODUCT_SERIES_ID as varchar(5)) as target, " +
                         " PRODUCTIMAGEDESC as DESCRIPTION,  'quickBoxBlue' as css , 1 as sortid, 5 as group_id, SUBSITE_URL  FROM PRODUCT_SERIES " +
                         " where PRODUCT_SERIES_ID=@PRODUCTSERIESID order by sortid"
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr("product_series_id").ToString()))
            Else
                strSQL = "SELECT     b.PARTNUMBER,a.PRODUCT_ID ,1 as CATEGORY_ID," & dr("product_series_id").ToString() & " as PRODUCT_SERIES_ID, b.SMALL_IMAGE_PATH AS IMAGEFILENAME, " +
                    "'/oscilloscope/oscilloscopemodel.aspx?modelid=' + cast(a.PRODUCT_ID as varchar(5)) AS target, " +
                    " b.PARTNUMBER AS DESCRIPTION,'quickBoxBlue' AS css, b.sort_id as sortid, ps.SUBSITE_URL " +
                    " From PRODUCT_SERIES_CATEGORY a INNER JOIN product b  On a.product_id=b.productid INNER JOIN [PRODUCT_SERIES] ps ON a.PRODUCT_SERIES_ID = ps.PRODUCT_SERIES_ID " +
                    " where category_id = @CATEGORYID and PRODUCT_SERIES_ID=@PRODUCTSERIESID AND b.DISABLED ='n'  " +
                    " and PRODUCT_ID in (select PRODUCT_ID from PRODUCT_SERIES_CATEGORY where category_id = @CATEGORYID2 and product_series_id = @PRODUCTSERIESID2) order by sort_id"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
                sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SCOPE))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID2", dr("product_series_id").ToString()))
            End If
            ' Response.Write(strSQL & "<br>")
            'Response.End()
            ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()))
        Next

        'Protocol analyzers
        strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME," +
                " '/protocolanalyzer/protocoloverview.aspx?seriesid=' + cast(a.PRODUCT_SERIES_ID as varchar(5))  as target, " +
                " b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid, 90 as group_id, b.SUBSITE_URL " +
                " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " +
                " INNER JOIN product c  On a.product_id=c.productid " +
                " where(a.category_id = @CATEGORYID AND c.disabled='n' AND b.disabled='n' " +
                " and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID2) and PRODUCT_SERIES_ID=@PRODUCTSERIESID)) order by b.sortid"
        'Response.Write(strSQL & "<br>")
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_PROTOCOL))
        sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
        ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()))

        'Product Groups - series of groups of products assigned to serial data standard
        strSQL = "SELECT distinct b.NAME as partnumber,0 as PRODUCT_ID,a.CATEGORY_ID,a.PRODUCT_SERIES_ID, b.productimage as IMAGEFILENAME, " +
                " '/options/productseries.aspx?mseries=' + " +
                " cast(a.PRODUCT_SERIES_ID as varchar(5)) +  '&groupid=' + cast(c.group_id as varchar(5)) as target," +
                " b.DESCRIPTION,  'quickBoxBlue' as css, b.sortid ,c.group_id, b.SUBSITE_URL " +
                " FROM PRODUCT_SERIES_CATEGORY a INNER JOIN PRODUCT_SERIES b ON a.PRODUCT_SERIES_ID=b.PRODUCT_SERIES_ID " +
                " INNER JOIN PRODUCT as c on a.product_id=c.productid where(a.category_id NOT in ( 1,19,21,20) )" +
                " AND c.disabled='n'  AND b.disabled='n' and a.product_id in (select product_id from PRODUCT_SERIES_CATEGORY where(category_id = @CATEGORYID) and PRODUCT_SERIES_ID=@PRODUCTSERIESID)  order by b.sortid"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", catid))
        'Response.Write(strSQL & "<br><br>")
        ' Response.End()
        ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray()))

        dl.DataSource = ds.Tables(0)
        AddHandler dl.ItemDataBound, AddressOf prodList_ItemDataBound
        dl.DataBind()
    End Sub

    Protected Sub serilRepeater_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles serilRepeater.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim hlkStandard As HyperLink = CType(e.Item.FindControl("hlkStandard"), HyperLink)
                Dim rpt As DataList = e.Item.FindControl("prodList")
                Dim lblseri As Label = e.Item.FindControl("PRODUCT_SERIES_ID")

                If Not (drv("SUBSITE_URL") Is DBNull.Value) Then
                    hlkStandard.NavigateUrl = drv("SUBSITE_URL").ToString()
                Else
                    hlkStandard.NavigateUrl = String.Format("serialdatastandard.aspx?standardid={0}{1}", drv("PRODUCT_SERIES_ID").ToString(), menuURL)
                End If
                hlkStandard.Text = drv("NAME").ToString()
                showModel(lblseri.Text, rpt)
        End Select
    End Sub

    Protected Sub prodList_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)
        Select e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim hlkUrl As HyperLink = CType(e.Item.FindControl("hlkUrl"), HyperLink)

                If Not (drv("SUBSITE_URL") Is DBNull.Value) Then
                    hlkUrl.NavigateUrl = drv("SUBSITE_URL").ToString()
                Else
                    hlkUrl.NavigateUrl = drv("target").ToString()
                End If
                hlkUrl.Text = drv("PARTNUMBER").ToString()
        End Select
    End Sub

    Protected Sub standardlist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles standardlist.SelectedIndexChanged
        showAllModels()
    End Sub

End Class