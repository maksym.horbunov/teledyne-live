﻿Imports System.Data
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Shopper_AfterSendQuote
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Submit Request Quote"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidateUserNoRedir()
        Dim strPSGQuoteID As String = ""
        Dim strQuoteID As String = ""
        'Dim strPert3QuoteID As String = ""
        Dim strWSQuoteID As String = ""
        Dim strLAQuoteID As String = ""
        Dim strDistPSGQuoteID As String = ""
        Session("RedirectTo") = rootDir + "/shopper/RequestQuote/"
        If Not Request("pq") Is Nothing And Len(Request("pq")) > 0 Then
            strPSGQuoteID = Request("pq")
        End If
        If Not Request("sq") Is Nothing And Len(Request("sq")) > 0 Then
            strQuoteID = Request("sq")
        End If
        If Not Request("sdq") Is Nothing And Len(Request("sdq")) > 0 Then

            strWSQuoteID = Request("sdq")
        End If
        If Not Request("lq") Is Nothing And Len(Request("lq")) > 0 Then
            strLAQuoteID = Request("lq")
        End If
        If Not Request("pdq") Is Nothing And Len(Request("pdq")) > 0 Then
            strDistPSGQuoteID = Request("pdq")
        End If
        'strWJQuoteID = Request("strWJQuoteID")
        ' strPert3QuoteID = Request("strPert3QuoteID")
        '*Pre Display
        Dim bufferPre As StringBuilder = New StringBuilder()
        If Session("localeid") <> "1041" Then
            bufferPre.Append(Session("FirstName"))
            bufferPre.Append(",&nbsp;")
        Else
            bufferPre.Append(Session("LastName"))
            bufferPre.Append("&nbsp;")
            bufferPre.Append(Functions.LoadI18N("SLBCA0160", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        End If

        bufferPre.Append(Functions.LoadI18N("SLBCA0071", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        bufferPre.Append("&nbsp;")
        bufferPre.Append(Functions.LoadI18N("SLBCA0073", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        bufferPre.Append("&nbsp;")
        bufferPre.Append(Functions.LoadI18N("SLBCA0074", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        bufferPre.Append("<br><br>")

        '    If Session("Country") = "United States" And Len(strPert3QuoteID) > 0 Then
        'bufferPre.Append("For PeRT3 System please reference the quote number&nbsp;<b>" + strPert3QuoteID + "</b><br /><br />")
        'End If

        If Session("Country") = "United States" And Len(strWSQuoteID) > 0 Then
            bufferPre.Append("For WaveSurfer, WaveJet and WaveAce Oscilloscope products, Arbitrary Waveform Generators and Logic Analyzers please reference the quote number&nbsp;<b>" + strWSQuoteID + "</b><br /><br />")
        End If

        If Session("Country") = "United States" And Len(strPSGQuoteID) > 0 Then
            bufferPre.Append("For Protocol Analyzer products please reference the quote number&nbsp;<b>" + strPSGQuoteID + "</b><br /><br />")
        End If

        If Session("Country") = "United States" And Len(strLAQuoteID) > 0 Then
            bufferPre.Append("For Logic Analyzer products please reference the quote number&nbsp;<b>" + strLAQuoteID + "</b><br /><br />")
        End If

        If Session("Country") = "United States" And Len(strDistPSGQuoteID) > 0 Then
            bufferPre.Append("For USB Advisor,Conquest or USB Mobile T2 products please reference the quote number&nbsp;<b>" + strDistPSGQuoteID + "</b><br /><br />")
        End If
        If Len(strQuoteID) > 0 Then
            If Session("Country") = "United States" Then
                bufferPre.Append("For Oscilloscope products or PeRT3 System please reference the quote number&nbsp;<b>" + strQuoteID + "</b>")
            Else
                bufferPre.Append("").Append(Functions.LoadI18N("SLBCA0075", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                bufferPre.Append("&nbsp;<b>")
                bufferPre.Append(strQuoteID)
                If Len(strPSGQuoteID) > 0 Then
                    bufferPre.Append(" ,")
                    bufferPre.Append(strPSGQuoteID)
                End If
                bufferPre.Append("</b>")
            End If
            ExecuteGA()
        End If

        If Session("Country") = "United States" Then
            If Len(strQuoteID) > 0 Or Len(strPSGQuoteID) > 0 Then
                bufferPre.Append("<br><br>").Append("We are reviewing your request and will send you a quotation <strong>within 24 hours</strong>. We want to provide the best possible service available.  If we have questions about your request, we will attempt to contact you.")
                bufferPre.Append("<br><br>").Append("Please be aware that the SPAM filter of your service provider and/or company may filter our e-mail to you. If the e-mail is not delivered to your in-box within 5 min, most likely your corporate Spam filter has trapped it and you will not receive your quote. Therefore, please print this page for your reference which contains the Quote ID# and customer Care Center phone number, should you not receive your quote.")
            Else
                bufferPre.Append("<br><br>").Append(Functions.LoadI18N("SLBCA0576", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                bufferPre.Append("<br><br>").Append(Functions.LoadI18N("SLBCA0577", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            End If
        End If

        lblPreDisplay.Text = bufferPre.ToString()

        If Session("FirstName").ToString().Length > 0 Then
            lblUserData.Text = WriteUserData()
        End If
    End Sub

    Private Function WriteUserData() As String
        Dim rlt As StringBuilder = New StringBuilder()
        rlt.Append("<table width=""400"" cellpadding=5>")
        rlt.Append("<tr>")
        rlt.Append("<td>")
        rlt.Append("        </b>")
        rlt.Append("		<br><strong>" + Session("FirstName") + " " + Session("LastName") + "</strong><br>")
        rlt.Append(Session("Company") + "<br>")
        rlt.Append(Session("Address") + "<br>")
        If Session("Address2").ToString().Length > 0 Then
            rlt.Append(Session("Address2") + "<br>")
        End If
        rlt.Append(Session("City") + ", &nbsp;" + Session("State") + ",&nbsp;" + Session("Zip") + "<br>")
        rlt.Append(Session("Country") + "<br>")
        rlt.Append(Session("Phone") + "<br>")
        rlt.Append(Session("Email") + "<br>")
        rlt.Append("		")
        rlt.Append("<br /><a href='").Append(rootDir).Append("/support/user/userprofile.aspx'> ")
        rlt.Append("<strong>" + Functions.LoadI18N("SLBCA0788", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</strong> </a>")
        rlt.Append("	</td>")
        rlt.Append("</tr>")
        rlt.Append("</table>")

        WriteUserData = rlt.ToString()

    End Function

    Private Sub ExecuteGA()
        litJavascript.Text = "<script language=""javascript"" type=""text/javascript"">ga('send', 'event', 'QuoteSystem', 'PostSubmit_OnLoad', 'Shopper_AfterSendQuote', {'nonInteraction': 1})</script>"
        litJavascript.Visible = True
    End Sub

End Class
