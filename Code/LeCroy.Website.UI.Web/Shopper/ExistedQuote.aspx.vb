﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_ExistedQuote
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("menuSelected") = ""
        If Not Page.IsPostBack Then
            Initial()
        End If
    End Sub

    Private Sub Initial()
        If Request("quoteid") Is Nothing Or Not IsNumeric(Request("quoteid")) Then
            Response.Redirect("QuoteHistory.aspx")
        End If

        Dim strSQL As String
        Dim QuoteID As String
        QuoteID = Request("quoteid").ToString()
        Session("QuoteID") = QuoteID

        strSQL = "Delete from SHOPPER where SESSIONID=@SESSIONID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        strSQL = "Select PRODUCTID,PROPERTYGROUPID,MASTERPRODUCTID,CUSTOMERID,QTY,PRICE,GROUP_ID,PRODUCT_SERIES_ID from SHOPPERQUOTE where QUOTEID=@QUOTEID"
        'Response.Write(strSQL & "<br>")
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID))
        Dim shopperQuotes As List(Of ShopperQuote) = ShopperQuoteRepository.FetchShopperQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        For Each sq In shopperQuotes
            strSQL = "Insert into SHOPPER  (SESSIONID, PRODUCTID, QTY, PRICE, DATE_ENTERED, PROPERTYGROUPID, CUSTOMERID, MASTERPRODUCTID, OFFER_ID,GROUP_ID, PRODUCT_SERIES_ID, ADD_TO_CART) values (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@DATEENTERED,@PROPERTYGROUPID,@CUSTOMERID,@MASTERPRODUCTID,@OFFERID,@GROUPID,@PRODUCTSERIESID,@ADDTOCART)"
            'Response.Write(strSQL & "<br>")
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", Session.SessionID.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", sq.ProductId.ToString()))
            sqlParameters.Add(New SqlParameter("@QTY", sq.Qty.ToString()))
            sqlParameters.Add(New SqlParameter("@PRICE", Functions.GetProductPriceonProductID(sq.ProductId, sq.MasterProductId).ToString()))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", sq.PropertyGroupId.ToString()))
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", sq.CustomerId))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", sq.MasterProductId.ToString()))
            sqlParameters.Add(New SqlParameter("@OFFERID", "0"))
            sqlParameters.Add(New SqlParameter("@GROUPID", IIf(sq.GroupId.HasValue, sq.GroupId.Value.ToString(), DBNull.Value)))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", IIf(sq.ProductSeriesId.HasValue, sq.ProductSeriesId.Value.ToString(), DBNull.Value)))
            sqlParameters.Add(New SqlParameter("@ADDTOCART", "Y"))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Next
        Response.Redirect("default.aspx")
    End Sub
End Class