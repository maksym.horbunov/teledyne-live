<%@ Language=VBScript %>
<!--#include virtual="/shopper/functions.asp" -->
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY>
<script language="JavaScript">
function validate()
{
if (document.modquote.desc.value=="")
{
alert ("Please type instructions for CSRs to modify quote");
return false;
}
return true;
}

</script>

<%
emaillocaleid=1033
strCret = chr(13) & chr(10)
if len(Session("modquote"))=0 then
	strQuote=Request.QueryString("qid")
	ContactID=Request.QueryString("cust")
	Session("modquote")=strQuote
	Session("customerID")=ContactID
end if
'Response.Write Session("modquote") & "<br>"
'Response.Write Session("customerID") & "<br>"
'Check if quote was approved

if len(Session("modquote"))>0 and len(Session("customerID"))>0 then
	set rsCheckQuoteApproved=server.CreateObject("ADODB.recordset")
	strSQL ="select * from EMAIL_CSR where QUOTEID=" & Session("modquote")
	rsCheckQuoteApproved.Open strSQL, dbconnstr()
	if not rsCheckQuoteApproved.EOF then
		
		if lcase(rsCheckQuoteApproved("RESPONSE_FLAG"))="m" then
			Response.Write "<font face=Verdana,Arial size=2 color=DodgerBlue><b>The instructions to modify this quote were already sent to CSRs:</b></font><br><br>"
			Response.Write "<font face=Verdana,Arial size=2 color=""#000000"">" & rsCheckQuoteApproved("DESCRIPTION") & "</font><br><br>"
			Response.Write "<a href=""Javascript:parent.window.close()"" title=""Close Window"" alt=""Close Window"" >"
			Response.Write "CLOSE WINDOW"
			Response.Write "</a>"
        elseif not CheckScopesQuote(Session("modquote")) and lcase(rsCheckQuoteApproved("RESPONSE_FLAG"))="y" then
			Response.Write "<font face=Verdana,Arial size=2 color=DodgerBlue><b>This quote was approved.</b></font><br><br>"
			Response.Write "<a href=""Javascript:parent.window.close()"" title=""Close Window"" alt=""Close Window"" >"
			Response.Write "CLOSE WINDOW"
			Response.Write "</a>"
		else
			strDesc=Request.Form("desc")
			strDesc=Replace(strDesc,"'","''")
			Response.Write "<font face=""Verdana,Arial"" size=""2"" color=""#000000""><b>"
			Response.Write "</b></font><br><br>"
			Response.Write "<form action=""modifyquote.asp"" method=""post"" name=""modifyquote"" onsubmit=validate()>"
			Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
			Response.Write "<tr>"
			Response.Write "<td valign=""top"">"
			Response.Write "<font face=""Verdana,Arial"" size=""2"" color=""DodgerBlue""><b>"
			Response.Write "Please type instructions for CSRs to modify quote:"
			Response.Write "</b></font>"
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
			Response.Write "<td valign=""top"">"
			Response.Write "<textarea name=""desc"" cols=""80"" rows=""20"" >" & strDesc & "</textarea><br><br>"
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
			Response.Write "<td valign=""top"">"
			'Insert CSR Name  and Description into table EMAIL_CSR (webdata)
			if  len(strDesc)>0 then
				set rsUpdate=server.CreateObject("ADODB.recordset")
				strSQL="Update EMAIL_CSR set CSR_NAME='" & strName  & "', DESCRIPTION='" & strDesc & "',RESPONSE_FLAG='m' where QUOTEID=" & Session("modquote")
				rsUpdate.Open strSQL, dbconnstr()
				'Send Email to all CSRs
				if len(GetALLCSREmails)>0 then
				
					'Get Customer FirstName and Email
					set rsFirstName=server.CreateObject("ADODB.Recordset")
					strSQL="Select FIRST_NAME from CONTACT where CONTACT_ID=" & Session("customerID")
					rsFirstName.Open strSQL, dbconnstr()
					if not rsFirstName.EOF then
						strFirstName=rsFirstName("FIRST_NAME")
					end if 
					
					set rsCheckResponse=server.CreateObject("ADODB.Recordset")
					strSQL="Select * from QUOTE where QUOTEID=" & Session("modquote")
					rsCheckResponse.Open strSQL, dbconnstr()
					Session("offerid")=""
					if len (trim(rsCheckResponse("OFFER_ID")))>0 then
						if clng(trim(rsCheckResponse("OFFER_ID")))>0 then
							Session("offerid")=trim(rsCheckResponse("OFFER_ID"))
						else
							Session("offerid")=""
						end if
					end if
					if len (trim(rsCheckResponse("CAMPAIGN_ID")))>0 then
						if clng(trim(rsCheckResponse("CAMPAIGN_ID")))>0 then
							Session("CampaignID")=trim(rsCheckResponse("CAMPAIGN_ID"))
							if len(Session("CampaignID"))>0 then
								if clng(Session("CampaignID"))=223 then
									Session("offerid")=5
								end if
							end if
						else
							Session("CampaignID")=""
						end if
					end if
					strQuoteTime=rsCheckResponse("QUOTE_TIME")
					if  CheckScopesQuote(Session("modquote") ) then
						strPartFooter= GetCSRName(GetSENumber( Session("customerID"))) & ", " & LoadI18N("SLBCA0062",emaillocaleid) & strCRet & strCRet 
						strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetSEName(GetSENumber( Session("customerID"))) & ", " & LoadI18N("SLBCA0064",emaillocaleid) & strCRet & strCRet
					else
						if len(GetPSGCSRName(Session("modquote"),Session("customerID")))>0 then
							strPartFooter= GetPSGCSRName(Session("modquote"),Session("customerID")) & ", SALES OPERATIONS REPRESENTATIVE(S)" & strCRet & strCRet 
						else
							strPartFooter= "Teresa Zachary, SALES OPERATIONS REPRESENTATIVE" & strCRet & strCRet 
						end if
						'******** Protocol Specialist *********
						set rsSP=server.CreateObject("ADODB.Recordset")
						strSQL=	" SELECT DISTINCT  PROPERTYGROUP.SUBCAT_ID FROM SHOPPERQUOTE INNER JOIN"&_
								" PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID"&_
								" INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID"&_
								" WHERE SHOPPERQUOTE.QUOTEID = " & Session("modquote") & " AND PROPERTYGROUP.CATEGORY_ID = 19 "
						'Response.Write strSQL
						rsSP.Open strSQL, dbconnstr()
						if not rsSP.EOF then
							'strSP=GetPSGSpecialistsName(Session("modquote"),Session("customerID"))
							strPartFooter= strPartFooter & LoadI18N("SLBCA0063",1033) & " " & GetPSGSpecialistsName(Session("modquote"),Session("customerID")) & ", PROTOCOL SALES SPECIALIST(S)"  & strCRet & strCRet
						end if
					end if
					strFooterCC="-------------------------------------------------------------------------" & strCret& strCret
			
					if len(strPartFooter)>0 then
						strFooterCC=strFooterCC & strPartFooter
					end if
					if len(Session("offerid"))>0 then
						strFooterCC=strFooterCC & strCret & "For Order Placement or Technical Information, Please call 1-800-5-LECROY "
						'strFooterCC=strFooterCC & "(1-800-553-2769). Standard Warranty is 1 year including Parts, Labor and Return Shipping. "
						strFooterCC=strFooterCC & "These commodities licensed by U.S. for ultimate destination United States. "
						strFooterCC=strFooterCC & "Diversion contrary to U.S. law prohibited."
					else
						strFooterCC=strFooterCC & strCret & LoadI18N("SLBCA0039",emaillocaleid)& "." & strCRet
						strFooterCC=strFooterCC & LoadI18N("SLBCA0040",emaillocaleid) & "." & strCRet
						strFooterCC=strFooterCC & LoadI18N("SLBCA0041",emaillocaleid) & "." & strCRet
						strFooterCC=strFooterCC & LoadI18N("SLBCA0042",emaillocaleid) & "." & strCRet
					end if
					strBodySE=strCret& strCRet &  strFirstName &  " " & LoadI18N("SLBCA0068",emaillocaleid) & ":" & strCRet 
					strBodySE=strCret& strCret & strBodySE & EMailBodyNew(Session("modquote")) & strCret & strCret
					if len(Session("offerid"))>0 then
						strBodySE =strBodySE & "Special pricing not combinable with other discounts"  & strCret 
					end if
					strEMailBodySE=strBodySE & strFooterCC
					strInfoCC=strCRet & LoadI18N("SLBCA0007",emaillocaleid) & ": " & Session("modquote") & strCRet
					strInfoCC=strInfoCC & LoadI18N("SLBCA0069",emaillocaleid) & ": " & FormatDateTime(strQuoteTime,2)& strCRet
					if len(Session("offerid"))>0 then
						if Session("offerid")=17 or Session("offerid")=18 then
							strInfoCC=strInfoCC & LoadI18N("SLBCA0070",emaillocaleid) & ": 6/22/2007" & strCRet
						else
							strInfoCC=strInfoCC & LoadI18N("SLBCA0070",emaillocaleid) & ": " & FormatDateTime((strQuoteTime+30),2) & strCRet& strCRet
						end if
					else
						strInfoCC=strInfoCC & LoadI18N("SLBCA0070",emaillocaleid) & ": " & FormatDateTime((strQuoteTime+30),2) & strCRet& strCRet
					end if
					'strInfoCC=strInfoCC & LoadI18N("SLBCA0070",emaillocaleid) & ": " & FormatDateTime((strQuoteTime+30),2) & strCRet
					strInfoCC=strInfoCC & CustomerData(Session("customerID")) 
					'set rsEmailBody=server.CreateObject("ADODB.recordset")
					'strSQL="Select * from EMAIL_CSR where QUOTEID=" & Session("modquote")
					'rsEmailBody.Open strSQL, dbconnstr()
					'if not rsEmailBody.EOF then
						strBody= GetSEName(GetSENumber(Session("customerID"))) & " - " & GetSENumber(Session("customerID")) & " requested the following modifications to the quote request: " & strCret & strCret
						strBody=strBody & strDesc & strCret& strCret 
						strBody=strBody & strInfoCC & strEMailBodySE
						Set oMailMessage=Server.CreateObject("CDO.Message")
						if  CheckScopesQuote(Session("modquote") ) then
							oMailMessage.To = GetALLCSREmails
							oMailMessage.CC = "webquote@teledynelecroy.com,hilary.lustig@teledynelecroy.com"
							oMailMessage.Subject = "Request from SE to Modify Web Quote" & " - " & GetALLCSREmails
							strBody= GetSEName(GetSENumber(Session("customerID"))) & " - " & GetSENumber(Session("customerID")) & " requested the following modifications to the quote request: " & strCret & strCret
						else
							oMailMessage.To = "protocolsales@teledynelecroy.com"
							oMailMessage.CC = "webquote@teledynelecroy.com"
							oMailMessage.Subject = "Request from PSG Sales Specialist to Modify Web Quote" 
							strBody= GetPSGSpecialistsName(Session("modquote"),Session("customerID")) &  " requested the following modifications to the quote request: " & strCret & strCret
						end if
						strBody=strBody & strDesc & strCret& strCret 
						strBody=strBody & strInfoCC & strEMailBodySE
						
						oMailMessage.From = "webquote@teledynelecroy.com"
						oMailMessage.Bcc = "james.chan@teledyne.com"
						oMailMessage.TextBody = strBody
						oMailMessage.send
					'end if
				end if
				Session("modquote")=""
				Session("customerID")=""
				Response.Write "<font face=""Verdana,Arial"" size=""2"" color=""#000000""><b>"
				Response.Write "Information was submitted.&nbsp;&nbsp;"
				Response.Write "</b></font>"
				Response.Write "<a href=""Javascript:parent.window.close()"" title=""Close Window"" alt=""Close Window"" >"
				Response.Write "CLOSE WINDOW"
				Response.Write "</a>"
			else
				Response.Write "<input type=""submit"" value=""  submit  "" style=""background-color: #6082FF; color: #FFFFFF; font-weight: bold; border-style: outset"">"
			end if
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
			Response.Write "</form>"
		end if
	end if
end if
%>
</body>
</HTML>
