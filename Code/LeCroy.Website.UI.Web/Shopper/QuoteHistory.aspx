﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_QuoteHistory" Codebehind="QuoteHistory.aspx.vb" %>
<%@ Import Namespace="LeCroy.Website.Functions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top"><img src="<%=rootDir %>/images/category/headers/quoteHistory.jpg" alt="Request a Quote. - Select a product category" width="158" height="93"></td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <ul><li><a href="<%=rootDir %>/Support/User/userprofile.aspx">Personal Profile</a></li></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="categoryBottom">
        <div class="content">
            <asp:Panel ID="pData" runat="server">
                <table cellpadding="0">
                    <tr>
                        <td width="100"><b><asp:Literal ID="litOne" runat="server" /></b></td>
                        <td width="300"><b><asp:Literal ID="litTwo" runat="server" /></b></td>
                    </tr>
                    <table width="832" border="0" cellspacing="0" cellpadding="0">
                        <asp:DataList ID="dl_main" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td width="100"><b><a href="ExistedQuote.aspx?quoteid=<%#Eval("QUOTEID")%>"><%#Eval("QUOTEID")%></a></b></td>
                                    <td width="300"><b><a href="ExistedQuote.aspx?quoteid=<%#Eval("QUOTEID")%>"><%#Eval("DATE_ENTERED")%></a></b></td>
                                </tr>
                            </ItemTemplate>
                        </asp:DataList>
                    </table>
                </table>
            </asp:Panel>
            <asp:Panel ID="pMsg" runat="server">
                <strong><font color="red">You did not submit any quotes in last 30 days.</font></strong>
            </asp:Panel>
        </div>
    </div>
</asp:Content>