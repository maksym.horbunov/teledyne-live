﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_QuoteHistory
    Inherits BasePage
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Quote History"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0007", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0057", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Session("menuSelected") = ""
        ValidateUserNoRedir()
        If Not Page.IsPostBack Then
            Initial()
        End If
    End Sub
    Private Sub Initial()
        Dim strSQL As String
        Dim ds As DataSet
        Session("RedirectTo") = rootDir + "/shopper/QuoteHistory.aspx" 'here is the name of the file itself
        If Session("ContactId") Is Nothing Or Len(Session("ContactId")) = 0 Then
            Response.Redirect(rootDir + "/support/User/")
            'Session("ContactId") = 1
        End If

        If Session("ContactId") > 0 Then
            strSQL = "Select DISTINCT SHOPPERQUOTE.QUOTEID, convert(varchar(30),MAX(SHOPPERQUOTE.DATE_ENTERED),101) as DATE_ENTERED from " +
            " SHOPPERQUOTE inner join QUOTE on SHopperQuote.QUOTEID=QUOTE.QUOTEID   where SHOPPERQUOTE.CUSTOMERID=@CUSTOMERID and " +
            " SHOPPERQUOTE.DATE_ENTERED >=@DATEENTERED and  QUOTE.OFFER_ID =0 and EStoreYN = 'n' group by SHOPPERQUOTE.QUOTEID order by SHOPPERQUOTE.QUOTEID desc, DATE_ENTERED desc"
            'Response.Write(strSQL)
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CUSTOMERID", Session("ContactID").ToString()))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.AddDays(-30).ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            If ds.Tables(0).Rows.Count > 0 Then
                dl_main.DataSource = ds
                dl_main.DataBind()
                pData.Visible = True
                pMsg.Visible = False
            Else
                pData.Visible = False
                pMsg.Visible = True
            End If
        Else
            pData.Visible = False
            pMsg.Visible = True
        End If
    End Sub
End Class