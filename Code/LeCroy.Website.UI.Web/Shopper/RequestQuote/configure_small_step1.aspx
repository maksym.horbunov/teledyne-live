﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_RequestQuote_configure_small_step1" Codebehind="configure_small_step1.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="false" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="false" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <div class="contentLeft">
                <h2><asp:Literal ID="litOne" runat="server" /></h2>
                <table width="700" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                    <tr>
                        <td valign="top" width="700">
                            <asp:DataList ID="dl_pro" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" GridLines="Both" BorderStyle="None" CaptionAlign="Top" CellPadding="0" ShowFooter="False" ShowHeader="False" HorizontalAlign="Left">
                                <ItemTemplate>
                                    <div id="info">
                                        <div class="product">
                                            <%#Eval("SUBCAT_NAME")%><br /><br />
                                            <a href="<%#Eval("URL")%>"><%#Eval("NAME")%></a><br />
                                            <a href="<%#Eval("URL")%>"><img src="<%=rootDir %><%#Eval("PRODUCTIMAGE")%>" border="0"></a><br />
                                            <a href="<%#Eval("URL")%>"><asp:Literal ID="litTwo" runat="server" /></a>
                                        </div>
                                        <%#GetIsNew(Eval("IS_NEW").ToString())%>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>