﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Shopper_RequestQuote_configure_small_step1

    '''<summary>
    '''lbHeadTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbHeadTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbHeadSubTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbHeadSubTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbSubNav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbSubNav As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbllanguage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllanguage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlLanguage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlLanguage As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ucquotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucquotes As Global.LeCroy.Website.UserControls_Quotes_Head

    '''<summary>
    '''litOne control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litOne As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''dl_pro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dl_pro As Global.System.Web.UI.WebControls.DataList
End Class
