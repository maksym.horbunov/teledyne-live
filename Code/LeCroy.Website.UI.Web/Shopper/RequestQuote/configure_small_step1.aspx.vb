﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote_configure_small_step1
    Inherits QuoteBasePage
    Dim strProfile As String = ""
    Dim strSQL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("menuSelected") = ""
        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
        Else
            Initial()
        End If
        litOne.Text = Functions.LoadI18N("SLBCA0753", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Sub Initial()
        If Not Request("catid") Is Nothing And Len(Request("catid")) > 0 Then
            ' Response.Write(IsNumeric(Request("catid")))
            ' Response.End()
            If IsNumeric(Request("catid")) Then
                Session("cataid") = Request("catid").ToString()
            Else
                Response.Redirect("default.aspx")
            End If
        ElseIf Session("cataid") Is Nothing Then
            Response.Redirect("default.aspx")
        End If

        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()
            ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y'  ORDER BY sort_id"
            Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
            ddlLanguage.Visible = True
            lbllanguage.Visible = True
            'End If
        End If
        Session("SeriesID") = ""

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Session("cataid").ToString() = "19" Or Session("cataid").ToString() = "20" Or Session("cataid").ToString() = "21" Then
            strSQL = "SELECT DISTINCT a.PRODUCT_SERIES_ID, a.NAME, '<strong>' + n.NAME + '</strong>' as SUBCAT_NAME , a.PRODUCTIMAGE, " +
            " a.is_new,d.CATEGORY_NAME as title, n.SORTID,'/shopper/RequestQuote/configure_small_step2.aspx?SeriesID='+ cast(a.PRODUCT_SERIES_ID as varchar(5)) as URL  FROM PRODUCT_SERIES a " +
            " INNER JOIN PRODUCT_SERIES_CATEGORY b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
            " INNER JOIN product c ON  b.product_id=c.productid INNER JOIN CATEGORY d " +
            " ON  b.CATEGORY_ID=d.CATEGORY_ID INNER JOIN SUBCATEGORY s ON a.SUBCAT_ID=s.SUBCAT_ID " +
            " INNER JOIN PROTOCOL_STANDARD_SERIES m ON a.PRODUCT_SERIES_ID=m.PRODUCT_SERIES_ID " +
            " INNER JOIN PROTOCOL_STANDARD n ON m.PROTOCOL_STANDARD_ID=n.PROTOCOL_STANDARD_id " +
            " WHERE b.CATEGORY_ID=@CATEGORYID and a.disabled='n' and c.disabled='n' order by n.SORTID,a.Name "
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        ElseIf (Session("cataid").ToString() = "3") Then
            strSQL = "select distinct a.group_id as PRODUCT_SERIES_ID,a.IMAGE as PRODUCTIMAGE, a.group_name as name, a.sortid,b.is_new,c.CATEGORY_NAME as title,'' as SUBCAT_NAME, " +
                " '/shopper/RequestQuote/configure_small_step2.aspx?SeriesID='+ cast(a.group_id as varchar(5)) as URL " +
           " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
           " INNER JOIN CATEGORY c ON a.category_id=c.CATEGORY_ID " +
           " WHERE b.disabled='n' and a.category_id=@CATEGORYID order by a.sortid"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        Else
            strSQL = "select distinct a.group_id as PRODUCT_SERIES_ID,a.IMAGE as PRODUCTIMAGE, a.group_name as name, a.sortid,b.is_new,c.CATEGORY_NAME as title,'' as SUBCAT_NAME, " +
                " case when(SELECT count(PRODUCT_SERIES.PRODUCT_SERIES_ID) " +
                                       " FROM PRODUCT AS PRODUCT_1 INNER JOIN PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT " +
                                       " ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN " +
                                       " CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID " +
                                       " INNER JOIN PRODUCT_SERIES INNER JOIN PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 " +
                                       " ON PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID " +
                                       " ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID " +
                                       " WHERE PRODUCT.GROUP_ID = a.GROUP_ID and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=@CATEGORYID and PRODUCT_SERIES_CATEGORY.CATEGORY_ID not in (31,2) " +
                                       " AND  PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID = 1  AND PRODUCT_1.DISABLED = 'n' " +
                                       " AND PRODUCT_1.DISABLED = 'n')>0 then '/shopper/RequestQuote/select_scope.aspx?SeriesID=' + cast(a.group_id as varchar(5)) else '/shopper/RequestQuote/configure_small_step2.aspx?SeriesID='+ cast(a.group_id as varchar(5)) end as URL " +
           " from product_group a INNER JOIN product b ON a.group_id = b.group_id " +
           " INNER JOIN CATEGORY c ON a.category_id=c.CATEGORY_ID " +
           " WHERE b.disabled='n' and a.category_id=@CATEGORYID order by a.sortid"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        End If
        ' Response.Write(strSQL)
        ' Response.End()
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        If ds.Tables(0).Rows.Count > 0 Then
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Configuration Step1 - " + ds.Tables(0).Rows(0)("title").ToString()
        End If

        dl_pro.DataSource = ds
        dl_pro.DataBind()
    End Sub

    Public Function GetIsNew(ByVal is_new As String) As String
        Dim result As String = ""
        If is_new = "1" Then
            result = "<div class='new'></div>"
        End If
        Return result
    End Function
    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub

    Private Sub dl_pro_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dl_pro.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim litTwo As Literal = CType(e.Item.FindControl("litTwo"), Literal)
            litTwo.Text = Functions.LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
    End Sub
End Class