﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote_configure_small_step2
    Inherits QuoteBasePage
    Dim strProfile As String = ""
    Public name As String = ""
    Dim customerID As String = "0"
    Dim strSQL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
        Else
            Initial()
            LoadProducts()
        End If

        litOne.Text = Functions.LoadI18N("SLBCA0801", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_sel.Text = LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_sum.Text = LoadI18N("SLBCA0851", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Sub LoadProducts()
        Me.p_proList.Visible = True
        Me.plOptionlist.Visible = False

        Dim a As ArrayList
        Dim priceForProduct As String = ""

        a = New ArrayList()

        Dim fromsession As Boolean = False

        If Session("cataid") Is Nothing Then
            Response.Redirect("default.aspx")
            Return
        End If
        If Request("SeriesID") Is Nothing And Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If

        If Not Request("SeriesID") Is Nothing Then
            If IsNumeric(Request("SeriesID")) Then
                Session("SeriesID") = Request("SeriesID").ToString()
                If Not Session("cataid") Is Nothing Then
                    If Not Session("cataid").ToString() = "19" Then
                        Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                    End If
                Else
                    Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                End If

            Else
                Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
            End If
        ElseIf Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If

        If Len(Session("cataid")) = 0 Then
            Response.Redirect("/shopper/default.aspx")
        End If
        If Not Request("SeriesID") Is Nothing Then
            If Not Session("SeriesID") Is Nothing Then
                If Session("SeriesID").ToString() <> Request("SeriesID").ToString() Then
                    Session("SeriesID") = Request("SeriesID").ToString()
                Else
                    fromsession = True
                End If
            Else
                Session("SeriesID") = Request("SeriesID").ToString()
            End If
        End If

        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Session("cataid").ToString() = "19" Or Session("cataid").ToString() = "20" Or Session("cataid").ToString() = "21" Then
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(Session("SeriesID").ToString()), "PRODSERIESID", sqlKeys, sqlParameters)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            strSQL = String.Format("SELECT a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
            " c.PROPERTYGROUPID, c.group_id, d.category_name as NAME, " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,c.SORT_ID " +
            " FROM PRODUCT_SERIES a INNER JOIN PRODUCT_SERIES_CATEGORY b ON  a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
            " INNER JOIN product c ON  b.product_id=c.productid INNER JOIN CATEGORY d ON b.CATEGORY_ID=d.CATEGORY_ID " +
            " WHERE  b.CATEGORY_ID = @CATEGORYID and b.product_series_id in ({0}) and c.disabled='n' ORDER BY hasOptions desc,c.SORT_ID ", sqlKeys)
            Session("stype") = "series"
        Else
            strSQL = "SELECT a.group_name as name,b.PRODUCTID,b.PRODUCTDESCRIPTION , b.PARTNUMBER, b.IMAGEFILENAME, " +
            " b.productdescription,b.PROPERTYGROUPID, b.group_id,c.CATEGORY_NAME," +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=b.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,b.SORT_ID " +
            " from product_group a INNER JOIN product b ON a.group_id = b.group_id INNER JOIN CATEGORY c ON a.CATEGORY_ID=c.CATEGORY_ID  where " +
            " b.disabled='n' and a.category_id=@CATEGORYID and a.group_id = @GROUPID ORDER BY hasOptions desc,b.SORT_ID"
            Session("stype") = "group"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", Session("SeriesID").ToString()))
        End If
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If Not ds.Tables(0).Rows.Count > 0 Then
            Session("SeriesID") = Nothing
            Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
        End If
        Session("step2title") = "Configuration Step2 - " + ds.Tables(0).Rows(0)("NAME").ToString()
        name = ds.Tables(0).Rows(0)("name").ToString()
        Session("pname") = name

        Session("PROPERTYGROUPID") = ds.Tables(0).Rows(0)("PROPERTYGROUPID").ToString()
        Session("GROUP_ID") = ds.Tables(0).Rows(0)("GROUP_ID").ToString()
        gvProducts.DataSource = ds
        gvProducts.DataBind()
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvProducts.Rows
            Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
            If drGrid.RowState = DataControlRowState.Edit Then
                If i <> gvProducts.EditIndex Then
                    Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                    If Not chkP Is Nothing Then
                        chkP.Enabled = False
                    End If
                    If Not lblHasOptions Is Nothing Then
                        If LCase(lblHasOptions.Text.ToString) = "y" Then
                            Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                            If Not btnHasOptions Is Nothing Then
                                btnHasOptions.Visible = True
                                btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                            End If
                        End If
                    End If
                Else
                    Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                    If Not chkP Is Nothing Then
                        chkP.Checked = True
                        Dim btnConfigureCancel As Button = drGrid.FindControl("btnConfigure")
                        btnConfigureCancel.Text = LoadI18N("SLBCA0765", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                        Dim btnSaveCancel As Button = drGrid.FindControl("btnSave")
                        btnSaveCancel.Text = LoadI18N("SLBCA0766", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    End If
                End If

            Else
                If Not lblHasOptions Is Nothing Then
                    If LCase(lblHasOptions.Text.ToString) = "y" Then
                        Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                        If Not btnHasOptions Is Nothing Then
                            btnHasOptions.Visible = True
                            btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                        End If
                    End If
                End If
            End If


            i = i + 1
        Next

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Session("step2title").ToString()

    End Sub

    Protected Sub btn_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sel.Click
        Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
    End Sub

    Protected Sub btn_sum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sum.Click
        SaveToShopper()
    End Sub

    Protected Sub Button3_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.ServerClick
        Dim cfgStep As String = ""
        cfgStep = Me.hidenStep.Value
        Select Case cfgStep
            Case "step1"
                Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
            Case "step3"
                SaveToShopper()
        End Select
    End Sub

    Protected Sub gvProducts_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvProducts.RowCancelingEdit
        gvProducts.EditIndex = -1
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Session("cataid").ToString() = "19" Or Session("cataid").ToString() = "20" Or Session("cataid").ToString() = "21" Then
            strSQL = "SELECT a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
            " c.PROPERTYGROUPID, c.group_id, d.category_name as NAME, " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions, c.SORT_ID " +
            " FROM PRODUCT_SERIES a INNER JOIN PRODUCT_SERIES_CATEGORY b ON  a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
            " INNER JOIN product c ON  b.product_id=c.productid INNER JOIN CATEGORY d ON b.CATEGORY_ID=d.CATEGORY_ID " +
            " WHERE  b.CATEGORY_ID = @CATEGORYID and b.product_series_id = @PRODUCTSERIESID and c.disabled='n' ORDER BY hasOptions desc,c.SORT_ID "
            Session("stype") = "series"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("SeriesID").ToString()))
        Else
            strSQL = "SELECT a.group_name as name,b.PRODUCTID,b.PRODUCTDESCRIPTION , b.PARTNUMBER, b.IMAGEFILENAME, " +
            " b.productdescription,b.PROPERTYGROUPID, b.group_id,c.CATEGORY_NAME,'false' as PRODUCTSELECTED , " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE   PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=b.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,b.SORT_ID  " +
            " from product_group a, product b,CATEGORY c where " +
            " a.group_id = b.group_id and a.CATEGORY_ID=c.CATEGORY_ID " +
            " and b.disabled='n'  and a.category_id= @CATEGORYID and a.group_id = @PRODUCTSERIESID ORDER BY hasOptions desc,b.SORT_ID"
            Session("stype") = "group"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("SeriesID").ToString()))
        End If
        'Response.Write(strSQL)
        'Response.End()
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        gvProducts.DataSource = ds
        gvProducts.DataBind()
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvProducts.Rows
            ' If i <> gvProducts.EditIndex Then
            Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
            Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            'If Not chkP Is Nothing Then
            ' chkP.Enabled = False
            'End If
            If Not lblHasOptions Is Nothing Then
                If LCase(lblHasOptions.Text.ToString) = "y" Then
                    Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                    If Not btnHasOptions Is Nothing Then
                        btnHasOptions.Visible = True
                        btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    End If
                End If
            End If
            ' Else
            'Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            'If Not chkP Is Nothing Then
            'chkP.Checked = True
            'End If
            ' End If
            i = i + 1
        Next
    End Sub


    Protected Sub gvProducts_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvProducts.RowEditing
        gvProducts.EditIndex = e.NewEditIndex
        languagechanged()

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Session("cataid").ToString() = "19" Or Session("cataid").ToString() = "20" Or Session("cataid").ToString() = "21" Then
            strSQL = "SELECT a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
            " c.PROPERTYGROUPID, c.group_id, d.category_name as NAME, " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,c.SORT_ID " +
            " FROM PRODUCT_SERIES a INNER JOIN PRODUCT_SERIES_CATEGORY b ON  a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
            " INNER JOIN product c ON  b.product_id=c.productid INNER JOIN CATEGORY d ON b.CATEGORY_ID=d.CATEGORY_ID " +
            " WHERE  b.CATEGORY_ID = @CATEGORYID and b.product_series_id = @PRODUCTSERIESID and c.disabled='n' ORDER BY hasOptions desc,c.SORT_ID  "
            Session("stype") = "series"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("SeriesID").ToString()))
        Else
            strSQL = "SELECT a.group_name as name,b.PRODUCTID,b.PRODUCTDESCRIPTION , b.PARTNUMBER, b.IMAGEFILENAME, " +
            " b.productdescription,b.PROPERTYGROUPID, b.group_id,c.CATEGORY_NAME,'false' as PRODUCTSELECTED , " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE   PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=b.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,b.SORT_ID " +
            " from product_group a, product b,CATEGORY c where " +
            " a.group_id = b.group_id and a.CATEGORY_ID=c.CATEGORY_ID " +
            " and b.disabled='n' and a.category_id=@CATEGORYID and a.group_id = @PRODUCTSERIESID ORDER BY hasOptions desc,b.SORT_ID"
            Session("stype") = "group"
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("SeriesID").ToString()))
        End If
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        gvProducts.DataSource = ds
        gvProducts.DataBind()
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvProducts.Rows
            If i <> gvProducts.EditIndex Then
                Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
                Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                If Not chkP Is Nothing Then
                    chkP.Enabled = False
                End If
                If Not lblHasOptions Is Nothing Then
                    If LCase(lblHasOptions.Text.ToString) = "y" Then
                        Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                        If Not btnHasOptions Is Nothing Then
                            btnHasOptions.Visible = True
                            btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                        End If
                    End If
                End If
            Else
                Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                If Not chkP Is Nothing Then
                    chkP.Checked = True
                    Dim btnConfigureCancel As Button = drGrid.FindControl("btnConfigure")
                    btnConfigureCancel.Text = LoadI18N("SLBCA0765", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    Dim btnSaveCancel As Button = drGrid.FindControl("btnSave")
                    btnSaveCancel.Text = LoadI18N("SLBCA0766", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                End If
            End If
            i = i + 1
        Next
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveToShopper()
    End Sub

    Protected Sub SaveToShopper()
        Dim i As Integer = 0
        Dim countSelected As Integer = 0
        Dim SeriesID As String = ""
        Dim masterproductid As String = ""

        For Each drGrid As GridViewRow In gvProducts.Rows
            Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            If Not chkP Is Nothing Then
                If chkP.Checked Then
                    Dim lblP As Label = drGrid.FindControl("lblPRODUCTID")
                    If Not lblP Is Nothing Then
                        countSelected = countSelected + 1

                        Dim cblO As CheckBoxList = drGrid.FindControl("cblOptions")
                        If Not cblO Is Nothing Then
                            If cblO.Items.Count > 0 Then
                                If Session("cataid").ToString() = "19" Then
                                    SeriesID = Session("SeriesID").ToString()
                                Else
                                    SeriesID = 0
                                End If
                                Functions.ShopperSave(Session.SessionID, SeriesID, customerID, lblP.Text.ToString, lblP.Text.ToString, 0)
                                masterproductid = lblP.Text.ToString
                                Session("masterproductid") = lblP.Text.ToString
                                For i = 0 To cblO.Items.Count - 1
                                    If cblO.Items(i).Selected Then
                                        countSelected = countSelected + 1
                                        Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, cblO.Items(i).Value.ToString, 0)
                                    End If
                                Next
                            Else
                                Session("masterproductid") = 0
                                masterproductid = 0
                                Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, lblP.Text.ToString, 0)
                            End If
                        Else
                            Session("masterproductid") = 0
                            masterproductid = 0
                            Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, lblP.Text.ToString, 0)
                        End If
                    End If
                    End If
                End If
        Next
        If countSelected = 0 Then
            lblRequiredFields.Visible = True
            lblRequiredFields.Text = LoadI18N("SLBCA0781", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Else
            Response.Redirect("configure_small_step3.aspx")
        End If
    End Sub

    Private Sub Initial()
        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()

            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y'  ORDER BY sort_id"
            Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        languagechanged()
        LoadProducts()
    End Sub

    Protected Sub languagechanged()
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class