﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_RequestQuote_configure_small_step3" Codebehind="configure_small_step3.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript">
        function onlyNum(obj, evt) {

            var rtn;
            if (evt.shiftKey && evt.keyCode != 9) {
                evt.returnValue = false;
                return;
            }
            if ((evt.keyCode == 17) || (evt.keyCode == 18) || (evt.keyCode == 20) || (evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode == 110) || (evt.keyCode == 8) || (evt.keyCode == 9) || (evt.keyCode >= 96 && evt.keyCode <= 105) || (evt.keyCode == 46))
                rtn = true;
            else
                rtn = false;

            if (!rtn) {
                evt.returnValue = false;
                obj.value = '';
                obj.focus();
            }
        }
    </script>
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="contentLeft">
                        <asp:Label ID="lb_alert" runat="server" />
                        <h2><asp:Literal ID="litOne" runat="server" /></h2><br />
                        <asp:Button ID="btnAddtoCart2" runat="server" Text="Add to Cart" /><br />
                        <asp:Label ID="lbContent" runat="server" />
                        <div class="stepNav">
                            <div align="center"><asp:Button ID="btn_update" runat="server" Text="Update" />&nbsp;&nbsp;<asp:Button ID="btn_remove" runat="server" Text="Remove All" /></div>
                        </div>
                    </td>
                    <td valign="top" class="contentRightGrey" style="width: 343px">
                        <h2><asp:Literal ID="litTwo" runat="server" /></h2><br />
                        <strong><asp:Literal ID="litThree" runat="server" /></strong>
                        <div class="nextStep">
                            <asp:Literal ID="litFour" runat="server" /><br /><br />
                            <asp:Button ID="btn_sq" runat="server" Text="Add to Cart" />
                        </div>
                        <br /><strong><asp:Literal ID="litFive" runat="server" /></strong>
                        <div class="nextStep">
                            <asp:Literal ID="litSix" runat="server" /><br /><br />
                            <asp:Button ID="btn_so" runat="server" Text="Start Over" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>