﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Shopper_RequestQuote_configure_small_step3

    '''<summary>
    '''lbHeadTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbHeadTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbHeadSubTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbHeadSubTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbSubNav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbSubNav As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbllanguage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllanguage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlLanguage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlLanguage As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ucquotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucquotes As Global.LeCroy.Website.UserControls_Quotes_Head

    '''<summary>
    '''lb_alert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lb_alert As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''litOne control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litOne As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btnAddtoCart2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddtoCart2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lbContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbContent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btn_update control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_update As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btn_remove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_remove As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''litTwo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litTwo As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litThree control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litThree As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litFour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litFour As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btn_sq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_sq As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''litFive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litFive As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litSix control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litSix As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btn_so control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_so As Global.System.Web.UI.WebControls.Button
End Class
