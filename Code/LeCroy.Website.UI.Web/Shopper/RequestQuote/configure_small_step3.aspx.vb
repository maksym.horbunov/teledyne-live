﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote_configure_small_step3
    Inherits QuoteBasePage
    Dim strProfile As String = ""
    Dim strSQL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("cataid") Is Nothing Then
            Response.Redirect("default.aspx")
        End If
        If Session("SeriesID") Is Nothing Then
            Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
        End If

        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
        Else
            Initial()
        End If
        litOne.Text = Functions.LoadI18N("SLBCA0767", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0772", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0773", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0775", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0774", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0776", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnAddtoCart2.Text = LoadI18N("SLBCA0768", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_update.Text = LoadI18N("SLBCA0769", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_remove.Text = LoadI18N("SLBCA0770", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_sq.Text = LoadI18N("SLBCA0768", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_so.Text = LoadI18N("SLBCA0774", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Sub Initial()
        Dim content As StringBuilder = New StringBuilder()
        Dim masterCount As Integer = 0
        Dim showUpdate As Boolean = False
        Dim sessionid As String = Me.Session.SessionID

        content.Append("<table width='570' border='0' cellspacing='0' cellpadding='0' border='0'>")

        strSQL = "SELECT DISTINCT b.masterproductid AS 'masterproductid', a.productid, a.partnumber, a.productdescription, b.qty " +
              " FROM PRODUCT a INNER JOIN Shopper b ON b.productid = a.productid " +
              " WHERE b.sessionid=@SESSIONID and b.add_to_cart='n'"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Dim priceForProduct As String = ""
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Quote Summary - " + dr("partnumber").ToString()

            For Each dr In ds.Tables(0).Rows
                If Session("cataid").ToString() = "19" Then
                    priceForProduct = ""
                Else
                    priceForProduct = getProductPriceIfApproved(Int32.Parse(dr("PRODUCTID").ToString()), Int32.Parse(dr("MASTERPRODUCTID").ToString()))
                End If
            Next
        Else
            Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
        End If

        strSQL = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID , b.FORM_SELECT_TYPE " +
              " FROM PRODUCT_GROUP  b INNER JOIN Shopper d ON  d.GROUP_ID = b.GROUP_ID " +
              " WHERE b.CATEGORY_ID=@CATEGORYID and  d.sessionid=@SESSIONID and d.add_to_cart='n' ORDER BY b.SORTID"
        'Response.Write(strSQL)
        'Response.End()
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
        Dim ds2 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds2.Tables(0).Rows.Count > 0 Then
            For Each dr1 As DataRow In ds2.Tables(0).Rows
                Dim groupID As String = ""
                ' Dim groupName As String = ""

                'groupID = dr1("GROUP_ID").ToString()
                'groupName = dr1("GROUP_NAME").ToString()

                content.Append("<tr class='top'>")
                content.Append("<td class='cell' width=""20%""><strong>" + dr1("GROUP_NAME").ToString() + "</strong></td><td class='cell' width=""60%"">&nbsp;</td>" +
                     "<td class='cell' width=""10%"">&nbsp;</td><td class='cell' width=""10%"">&nbsp;</td>" +
                 "</tr>")
                sqlParameters = New List(Of SqlParameter)
                If Not Session("masterproductid") Is Nothing Then
                    strSQL = "SELECT DISTINCT d.masterproductid, c.productid, c.partnumber, c.productdescription,c.sort_id, d.qty " +
                            " FROM PRODUCT AS c INNER JOIN  PRODUCT_GROUP AS b ON c.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " SHOPPER AS d ON d.PRODUCTID = c.PRODUCTID " +
                            " WHERE  b.group_id = @GROUPID AND d.masterproductid=@MASTERPRODUCTID AND d.sessionid=@SESSIONID and d.add_to_cart='n' Order by c.sort_id"
                    sqlParameters.Add(New SqlParameter("@GROUPID", dr1("GROUP_ID").ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", Session("masterproductid").ToString()))
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                Else
                    strSQL = "SELECT DISTINCT d.masterproductid, c.productid, c.partnumber, c.productdescription,c.sort_id, d.qty " +
                            " FROM PRODUCT AS c INNER JOIN  PRODUCT_GROUP AS b ON c.GROUP_ID = b.GROUP_ID INNER JOIN " +
                            " SHOPPER AS d ON d.PRODUCTID = c.PRODUCTID " +
                            " WHERE  b.group_id = @GROUPID AND d.masterproductid=0 AND d.sessionid=@SESSIONID and d.add_to_cart='n' Order by c.sort_id"
                    sqlParameters.Add(New SqlParameter("@GROUPID", dr1("GROUP_ID").ToString()))
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                End If
                ' Response.Write(strSQL & "<br>")
                'Response.End()
                Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                Dim i As Integer = 0
                If dss.Tables(0).Rows.Count > 0 Then
                    For Each drr As DataRow In dss.Tables(0).Rows
                        Dim groupNameDisplay As String = ""
                        'If (i = 0) Then
                        'groupNameDisplay = groupName
                        'End If

                        Dim canBuy As Boolean = False
                        Dim product As Product = Nothing
                        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
                        If (countryCode = 208) Then
                            product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(drr("productid").ToString()))
                            If Not (product Is Nothing) Then
                                If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                    canBuy = True
                                End If
                            End If
                        End If

                        content.Append("<tr class='top'>")
                        content.Append("<td class='cell' valign='top' width=""20%"">&nbsp;</td><td class='cell' valign=""top"" width=""50%"">")
                        content.AppendFormat("<input type='hidden' name='subproductid' id='subproductid' value='{0}'><input type='hidden' name='masterproductid' id='masterproductid' value='{1}'>&nbsp;&nbsp;", drr("productid").ToString(), drr("masterproductid").ToString())
                        content.AppendFormat("<strong><u>{0}</u></strong>&nbsp;{1}", drr("partnumber").ToString(), Functions.GetProdDescriptionOnProdID(drr("productid").ToString()).ToString())
                        content.Append("</td><td class='cell' valign=""top"" width=""15%"">")
                        content.AppendFormat("<input name='qty' type='text' maxlength='2' id='qty' size='1' value='{0}' onKeyUp='javascript:onlyNum(this, event);' onKeyDown='javascript:onlyNum(this,event)'/>", drr("qty").ToString())
                        If (canBuy) Then
                            content.AppendFormat("<br /><span style=""font-size: 11pt;"">@ ${0}</span>", Functions.GetProductPriceonProductID(product.ProductId, Nothing))
                            content.AppendFormat("<br /><input name=""btnBuyNow"" type=""button"" value=""Buy now"" onclick=""location.href='{0}';"" />", String.Format("https://store.teledynelecroy.com{0}", product.ShopifyHandle))
                        End If
                        content.Append("</td><td class='cell' valign=""top"" width=""15%"">")
                        content.AppendFormat("<input id='chkDel' type='checkbox' name='chkDel' value='{0}*{1}'/>&nbsp;{2}</label>", drr("masterproductid").ToString(), drr("productid").ToString(), LoadI18N("SLBCA0771", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                        content.Append("</td></tr><tr><td colspan=""4"">&nbsp;</td></tr>")
                        i = i + 1
                        showUpdate = True
                    Next
                End If
            Next
        End If
        If Not showUpdate Then
            btn_update.Visible = False
            btn_remove.Visible = False
        End If

        content.Append("</table>")
        lbContent.Text = content.ToString()

        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()
            ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y'  ORDER BY sort_id"
            Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
            ' ddlLanguage.Visible = True
            ' lbllanguage.Visible = True
            ' End If
        End If
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_update.Click
        If savetodb("N") Then
            Response.Redirect("configure_small_step3.aspx")
        End If
    End Sub

    Protected Function savetodb(ByVal updateAddToCart As String) As Boolean
        Dim productid As String
        Dim masterProductClicked As Boolean = False
        Dim masterProdID As String = ""
        Dim sessionid As String = Me.Session.SessionID
        productid = Session("productid" & Session("seriesid").ToString())
        Dim subProductIDs As String = ""
        Dim qty As String = ""
        Dim dels As String = ""
        Dim masterIDs As String = ""
        'subProductIDs = Request("subproductid").ToString()
        'qty = Request("qty").ToString()
        'masterIDs = Request("masterproductid").ToString()

        If Request("subproductid") Is Nothing Then
            subProductIDs = ""
        Else
            subProductIDs = Request("subproductid").ToString()
        End If
        If Request("masterproductid") Is Nothing Then
            masterIDs = ""
        Else
            masterIDs = Request("masterproductid").ToString()
        End If
        If Request("qty") Is Nothing Then
            qty = ""
        Else
            qty = Request("qty").ToString()
        End If

        If Request("chkDel") Is Nothing Then
            dels = ""
        Else
            dels = Request("chkDel").ToString()
        End If

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Not subProductIDs.Equals("") Then

            Dim arrayProductID() As String
            Dim arrayQty() As String
            Dim arrayDel() As String
            Dim arrayMasterId() As String

            If Not subProductIDs Is Nothing And Len(subProductIDs) > 0 Then
                arrayProductID = subProductIDs.Split(",")
                arrayQty = qty.Split(",")
                arrayDel = dels.Split(",")
                arrayMasterId = masterIDs.Split(",")
            End If

            Dim i As Integer = 0
            Dim SQLStringList As ArrayList = New ArrayList()
            Dim sqlStatementsAndParameters As List(Of Tuple(Of String, List(Of SqlParameter))) = New List(Of Tuple(Of String, List(Of SqlParameter)))   ' If lists stop keeping order of insert, then this is out the door

            If Not arrayProductID Is Nothing Then
                For i = 0 To arrayProductID.Length - 1
                    If arrayQty(i).ToString().Equals("") Then
                        lb_alert.Text = "<br/><font color=red>Please input the quantity to update.</font><br/><br/>"
                        Return False
                    End If
                    Try
                        Integer.Parse(arrayQty(i).ToString())
                    Catch
                        lb_alert.Text = "<br/><font color=red>Please input only numbers.</font><br/><br/>"
                        Return False
                    End Try
                Next
                lb_alert.Text = ""
                'Dim objList As ArrayList
                'objList = Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString())
                Dim del0 As String = ""
                Dim del1 As String = ""
                For i = 0 To arrayProductID.Length - 1
                    del0 = arrayMasterId(i)
                    del1 = arrayProductID(i)
                    Dim sqlString As String = "UPDATE SHOPPER SET QTY=@QTY WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QTY", arrayQty(i).ToString()))
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", del0.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", del1.ToString()))
                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                    If arrayQty(i) = "0" Then
                        If del0 = del1 Then
                            masterProductClicked = True
                            masterProdID = del1
                            RemoveSessionForMaster()
                        Else
                            'ClearObjList(objList, del1)
                        End If
                    End If
                Next
                DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)

                sqlStatementsAndParameters = New List(Of Tuple(Of String, List(Of SqlParameter)))
                If dels.Length > 0 Then
                    masterProductClicked = False
                    For i = 0 To arrayDel.Length - 1
                        Dim aa() As String = arrayDel(i).Split("*")
                        del0 = aa(0)
                        del1 = aa(1)
                        Dim sqlString As String = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", del0.ToString()))
                        sqlParameters.Add(New SqlParameter("@PRODUCTID", del1.ToString()))
                        sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                        If del0 = del1 Then
                            masterProdID = del1
                            masterProductClicked = True
                            RemoveSessionForMaster()
                        Else
                            ' ClearObjList(objList, del1)
                        End If
                    Next
                    DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)
                End If
            End If
        End If
        ' This will delete any options for which user has updated the Quantity to Zero
        ' If user updates quantity of the master product to Zero then delete all the options
        ' selected for that Model.
        strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND QTY = 0 "
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        sqlParameters = New List(Of SqlParameter)
        If updateAddToCart.Equals("Y") Then
            ' This will update the ADD_TO_CART flag in SHOPPER table to 'y' 
            If Not Session("masterproductid") Is Nothing Then
                strSQL = "UPDATE SHOPPER SET ADD_TO_CART = 'y' WHERE SESSIONID = @SESSIONID AND masterproductid=@MASTERPRODUCTID"
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", Session("masterproductid").ToString()))
            Else
                strSQL = "UPDATE SHOPPER SET ADD_TO_CART = 'y' " +
                " WHERE SESSIONID = @SESSIONID AND PRODUCTID in (select PRODUCTID FROM PRODUCT INNER JOIN PRODUCT_GROUP ON PRODUCT.GROUP_ID=PRODUCT_GROUP.GROUP_ID " +
                " where PRODUCT_GROUP.CATEGORY_ID=@CATEGORYID)"
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            End If
            'Response.Write(strSQL)
            'Response.End()
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If
        Return True
    End Function

    Protected Sub btn_sq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sq.Click
        If savetodb("Y") Then
            Session("PrefixScopeSeries") = ""
            Response.Redirect(rootDir + "/Shopper/")
        Else
            Initial()
        End If
    End Sub

    Protected Sub btn_so_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_so.Click
        If savetodb("N") Then
            Session("SeriesID") = Nothing
            Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
        End If
    End Sub

    Protected Sub btn_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_remove.Click
        strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND add_to_cart='n'"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Session("proListRQ" + Session("mproductid" + Session("SeriesID").ToString())) = Nothing
        Session("mproductid" + Session("SeriesID").ToString()) = Nothing
        Session("SeriesID") = Nothing
        Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
    End Sub

    Protected Sub RemoveSessionForMaster()
        Session("objList" & Session("productid" & Session("seriesid").ToString())) = Nothing
        Session("productid" & Session("seriesid").ToString()) = Nothing
        Session("seriesid") = Nothing
        Session("productid") = Nothing
        Session("objList") = Nothing
    End Sub

    Protected Sub btnAddtoCart2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoCart2.Click
        If savetodb("Y") Then
            Response.Redirect(rootDir + "/Shopper/")
        Else
            Initial()
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class