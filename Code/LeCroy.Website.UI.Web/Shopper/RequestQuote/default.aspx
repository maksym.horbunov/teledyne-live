<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_RequestQuote" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" Text=""></asp:Label><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <div class="contentMiddle">
                <table width="700" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                    <tr>
                        <td valign="top" width="700">
                            <asp:DataList ID="dl_series" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="item" valign="top" id="1">
                                                <div id="info">
                                                    <div class="product">
											            <%#Eval("category_name").ToString()%><br /><br/ >
                                                        <a href="<%#getUrl(Eval("category_id").ToString())%>"><img src="<%=rootDir %><%#Eval("image").ToString()%>" border="0"></a><br /><br />
                                                        <a href="<%#getUrl(Eval("category_id").ToString())%>"><asp:Literal ID="litOne" runat="server" /></a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>