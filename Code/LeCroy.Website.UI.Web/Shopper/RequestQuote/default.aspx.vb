﻿Imports System.Data.SqlClient
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote
    Inherits QuoteBasePage
    Dim strProfile As String = ""
    Dim strSQL As String = ""
    Dim ds, ds1 As DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("RedirectTo") = rootDir + "/shopper/requestquote/" 'here is the name of the file itself
        Session("menuSelected") = ""

        Dim strSQL As String = ""


        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
        Else
            Initial()
        End If

        strSQL = "SELECT DISTINCT a.category_id, a.image, a.category_name, a.sort_id " +
      " FROM category a INNER JOIN product_group b ON a.category_id = b.category_id  " +
      " INNER JOIN product c ON b.group_id=c.group_id " +
      " WHERE c.disabled='n' AND a.category_id not in (@CATEGORYID1,@CATEGORYID2,@CATEGORYID3,@CATEGORYID4) ORDER BY a.sort_id"
        'Response.Write(strSQL)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID1", AppConstants.CAT_ID_APPLICATIONS))
        sqlParameters.Add(New SqlParameter("@CATEGORYID2", AppConstants.CAT_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@CATEGORYID3", AppConstants.CAT_ID_FEATURES))
        sqlParameters.Add(New SqlParameter("@CATEGORYID4", CategoryIds.SERIAL_DATA_TEST_SUITES.ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Me.dl_series.DataSource = ds
        Me.dl_series.DataBind()

        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadSubTitle.Text = "<h3><strong>" + Functions.LoadI18N("SLBCA0829", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</h3>"
        lbHeadSubTitle.Visible = True
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Request Quote"
    End Sub

    Protected Function getUrl(ByVal catID As String) As String
        If catID = "1" Then
            Return rootDir + "/oscilloscope/configure/"
        Else
            Return rootDir + "/shopper/requestquote/configure_small_step1.aspx?catid=" + catID
        End If
    End Function
    Private Sub Initial()
        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()
            ' If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y' ORDER BY sort_id"
            ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
            'ddlLanguage.Visible = True
            'lbllanguage.Visible = True
            'End If
        End If

    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub

    Private Sub dl_series_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dl_series.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim litOne As Literal = CType(e.Item.FindControl("litOne"), Literal)
            litOne.Text = Functions.LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
    End Sub
End Class