﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_RequestQuote_select_model" MaintainScrollPositionOnPostback="true" Codebehind="select_model.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript">
        <!--
        function step1() {
            document.getElementById("ctl00_ContentPlaceHolder1_hidenStep").value = "step1";
            __doPostBack('ctl00$ContentPlaceHolder1$Button3', '')
        }
        function step3() {
            document.getElementById("ctl00_ContentPlaceHolder1_hidenStep").value = "step3";
            //document.getElementById("ctl00$ContentPlaceHolder1$Button3").click();
            if (typeof (Page_ClientValidate) == 'function') Page_ClientValidate('');
            __doPostBack('ctl00$ContentPlaceHolder1$Button3', '')
        }
        -->
    </script>
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="contentLeft">
                        <h2><%=GategoryName %> - <asp:Literal ID="litOne" runat="server" />:</h2>
                        <p>
                            <strong><%=Models%><br /><asp:Literal ID="litTwo" runat="server" />&nbsp;<%= ScopeSeries%></strong><br />
                            <asp:Panel ID="p_proList" runat="server" BorderStyle="None">
                                <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="False" CellPadding="1" CellSpacing="5" GridLines="None" ShowHeader="False" EnableTheming="False">
                                    <RowStyle VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkProducts" runat="server" Checked="false" />
                                                <asp:Label ID="lblPRODUCTID" runat="server" Text='<%# Bind("PRODUCTID") %>' Visible="False" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                <asp:Label ID="Partnumber" runat="server" Font-Bold="True" Font-Underline="True" Text='<%# Bind("PARTNUMBER") %>' />&nbsp;
                                                <asp:Button ID="btnConfigure" runat="server" Text="Close Configure" Visible="true" CausesValidation="False" CommandName="Cancel" Width="100px" Height="23px" /><br />
                                                <asp:Label ID="Description" runat="server" />
                                                <asp:CheckBoxList ID="cblOptions" runat="server" DataSourceID="SqlDS_COnfig" DataTextField="PRODUCTINFO" DataValueField="OPTIONID" BackColor="#DFF4FF" />
                                                <asp:SqlDataSource ID="SqlDS_COnfig" runat="server" ConnectionString="<%$ appSettings:connectionString %>" SelectCommand="SELECT     CONFIG.OPTIONID, '&lt;strong&gt;&lt;u&gt;'+PRODUCT.PARTNUMBER + '&lt;/u&gt;&lt;/strong&gt;&amp;nbsp;' +PRODUCT.PRODUCTDESCRIPTION as PRODUCTINFO
                                                                                                                                                                            FROM         CONFIG INNER JOIN
                                                                                                                                                                                                  PRODUCT ON CONFIG.OPTIONID = PRODUCT.PRODUCTID
                                                                                                                                                                            WHERE     (CONFIG.PRODUCTID = @PRODUCTID) AND (CONFIG.STD = 'n') AND (PRODUCT.DISABLED = 'n')
                                                                                                                                                                            ORDER BY PRODUCT.sort_id">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="lblCONFIGPRODUCTID" Name="PRODUCTID" PropertyName="Text" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                                <asp:Label ID="lblCONFIGPRODUCTID" runat="server" Text='<%# Bind("PRODUCTID") %>' Visible="False" />
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Partnumber" runat="server" Font-Bold="True" Font-Underline="True" Text='<%# Bind("PARTNUMBER") %>' />&nbsp;
                                                <asp:Button ID="btnConfigure" runat="server" Text="Configure" Visible="False" CausesValidation="False" CommandName="Edit" Width="69px" /><br />
                                                <asp:Label ID="Description" runat="server" />
                                                <asp:Label ID="hasOptions" runat="server" Text='<%# Bind("hasOptions") %>' Visible="False" />
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Underline="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#C4ECFF" />
                                </asp:GridView>
                                <asp:CheckBoxList ID="modellist" runat="server" Visible="False" />
                                <asp:Label ID="lblRequiredFields" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="Small" ForeColor="Red" Text="Please select a model first!" Visible="False" />
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="plOptionlist" runat="server" Visible="true" />
                        </p>
                        <div class="stepNav">
                            <div class="leftSelection"><asp:Button ID="btn_sel" Text="Select Series" runat="server" CausesValidation="False" /></div>
                            <div class="rightSelection"><asp:Button ID="btn_sum" Text="Next" runat="server" /></div>
                            <input name="" id="Button3" type="button" style="display: none" runat="server" />
                            <asp:HiddenField ID="hidenStep" runat="server" />
                        </div>
                    </td>
                    <td valign="top" class="contentRight">&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>