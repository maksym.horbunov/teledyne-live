﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote_select_model
    Inherits QuoteBasePage
    Public Shared Models As String = ""
    Public Shared GategoryName As String = ""
    Public Shared ScopeSeries As String = ""
    Dim customerID As String = "0"
    Dim strSQL As String = ""
    Dim strProfile As String = ""
    Dim ds1 As DataSet


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Session("localeid") = ddlLanguage.SelectedValue
            LoadProducts()
        Else
            LoadProducts()
        End If
        litOne.Text = Functions.LoadI18N("SLBCA0780", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0063", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_sel.Text = LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_sum.Text = LoadI18N("SLBCA0851", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile

        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not Session("ContactID") Is Nothing Then
            If Len(Session("ContactID")) > 0 Then
                customerID = Session("ContactID").ToString()
            End If
        End If
    End Sub


    Private Sub LoadProducts()
        Me.p_proList.Visible = True
        Me.plOptionlist.Visible = False

        Dim ds As DataSet
        Dim a As ArrayList
        Dim priceForProduct As String = ""

        a = New ArrayList()

        Dim fromsession As Boolean = False

        If Session("cataid") Is Nothing Then
            Response.Redirect("default.aspx")
            Return
        End If
        If Request("SeriesID") Is Nothing And Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If

        If Not Request("SeriesID") Is Nothing Then
            If IsNumeric(Request("SeriesID")) Then
                Session("SeriesID") = Request("SeriesID").ToString()
                If Not Session("cataid") Is Nothing Then
                    If Not Session("cataid").ToString() = "19" Then
                        'Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                    End If
                Else
                    'Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                End If

            Else
                Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
            End If
        ElseIf Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If

        If Len(Session("cataid")) = 0 Then
            Response.Redirect("/shopper/default.aspx")
        End If
        If Not Request("SeriesID") Is Nothing Then
            If Not Session("SeriesID") Is Nothing Then
                If Session("SeriesID").ToString() <> Request("SeriesID").ToString() Then
                    Session("SeriesID") = Request("SeriesID").ToString()

                    'Session("mproductid") = Nothing
                    'Session("proListRQ") = Nothing
                Else
                    fromsession = True
                End If
            Else
                Session("SeriesID") = Request("SeriesID").ToString()
            End If
        End If
        If Len(Request("SID").ToString) > 0 Then
            If IsNumeric(Request("SID")) Then
                Session("PrefixScopeSeries") = Request("SID").ToString
            Else
                Response.Redirect("/shopper/default.aspx")
            End If
        Else
            Response.Redirect("/shopper/default.aspx")
        End If


        If Not Me.IsPostBack Then
            If Not Request("from") = "current" Then
                '               Session("mproductid") = Nothing
                '               Session("proListRQ") = Nothing
            End If

            Dim sqlKeys As String = String.Empty
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(Session("SeriesID").ToString()), "PRODSERID", sqlKeys, sqlParameters)
            strSQL = String.Format("SELECT distinct a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
            " c.PROPERTYGROUPID, c.group_id, d.category_name as Category, " +
            " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,c.SORT_ID, h.NAME AS ScopeSeries " +
            " FROM  PRODUCT_SERIES AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " +
            " PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID INNER JOIN CATEGORY AS d ON b.CATEGORY_ID = d.CATEGORY_ID INNER JOIN " +
            " CONFIG AS e ON c.PRODUCTID = e.OPTIONID INNER JOIN PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCTID = f.PRODUCT_ID INNER JOIN " +
            " PRODUCT AS g ON f.PRODUCT_ID = g.PRODUCTID INNER JOIN  PRODUCT_SERIES AS h ON f.PRODUCT_SERIES_ID = h.PRODUCT_SERIES_ID " +
            " WHERE  b.CATEGORY_ID = @CATEGORYID AND (b.PRODUCT_SERIES_ID IN ({0})) " +
            " AND c.DISABLED = 'n' AND f.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND g.DISABLED = 'n' AND f.CATEGORY_ID = 1 ORDER BY hasOptions desc,c.SORT_ID ", sqlKeys)
            Session("stype") = "series"
            ' Response.Write(strSQL)
            Models = ""
            sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("PrefixScopeSeries").ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If Not ds.Tables(0).Rows.Count > 0 Then
                Session("SeriesID") = Nothing
                Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
            Else
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                Dim listOfModels As List(Of String) = New List(Of String)
                For Each dr In ds.Tables(0).Rows
                    If Not (String.IsNullOrEmpty(dr("name").ToString())) Then
                        listOfModels.Add(dr("name").ToString())
                    End If
                Next
                Models = String.Join(", ", listOfModels.Distinct())
                ' name = ds.Tables(0).Rows(0)("name").ToString()
                Session("pname") = Models
                Session("step2title") = "LeCroy - Configuration - Select Models " + Models
                Session("PROPERTYGROUPID") = ds.Tables(0).Rows(0)("PROPERTYGROUPID").ToString()
                Session("GROUP_ID") = ds.Tables(0).Rows(0)("GROUP_ID").ToString()
                GategoryName = ds.Tables(0).Rows(0)("Category").ToString()
                ScopeSeries = ds.Tables(0).Rows(0)("ScopeSeries").ToString()
                gvProducts.DataSource = ds
                gvProducts.DataBind()

                Dim i As Integer = 0
                For Each drGrid As GridViewRow In gvProducts.Rows

                    ' If Not flgEdit Or (flgEdit And i <> strEditIndex) Then
                    Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
                    If Not lblHasOptions Is Nothing Then
                        If LCase(lblHasOptions.Text.ToString) = "y" Then
                            Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                            If Not btnHasOptions Is Nothing Then
                                drGrid.FindControl("btnConfigure").Visible = True
                                btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                            End If
                        End If
                    End If
                    'End If
                    i = i + 1
                Next

            End If

            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Session("step2title").ToString()

            Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
            If (localeIdFromGeoIp >= 0) Then
                ddlLanguage.ClearSelection()
                '  If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y'  ORDER BY sort_id"
                ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                ddlLanguage.DataSource = ds1
                ddlLanguage.DataBind()
                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If Len(localeId) > 0 Then
                    ddlLanguage.SelectedValue = localeId
                Else
                    ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
                End If
                'ddlLanguage.Visible = True
                'lbllanguage.Visible = True
                ' End If
            End If
        End If
    End Sub

    Protected Sub btn_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sel.Click
        Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
    End Sub

    Protected Sub btn_sum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sum.Click
        SaveToShopper()
    End Sub

    Protected Sub Button3_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.ServerClick
        Dim cfgStep As String = ""
        cfgStep = Me.hidenStep.Value
        Select Case cfgStep
            Case "step1"
                Response.Redirect("configure_small_step1.aspx?catid=" + Session("cataid").ToString())
            Case "step3"
                SaveToShopper()
        End Select
    End Sub

    Protected Sub gvProducts_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvProducts.RowCancelingEdit
        gvProducts.EditIndex = -1
        Dim ds As DataSet
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(Session("SeriesID").ToString()), "PRODSERID", sqlKeys, sqlParameters)
        strSQL = String.Format("SELECT distinct a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
        " c.PROPERTYGROUPID, c.group_id, d.category_name as NAME, " +
        " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,c.SORT_ID " +
        " FROM  PRODUCT_SERIES AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " +
        " PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID INNER JOIN CATEGORY AS d ON b.CATEGORY_ID = d.CATEGORY_ID INNER JOIN " +
        " CONFIG AS e ON c.PRODUCTID = e.OPTIONID INNER JOIN PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCTID = f.PRODUCT_ID INNER JOIN " +
        " PRODUCT AS g ON f.PRODUCT_ID = g.PRODUCTID WHERE  b.CATEGORY_ID = @CATEGORYID AND (b.PRODUCT_SERIES_ID IN ({0})) " +
        " AND c.DISABLED = 'n' AND f.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND g.DISABLED = 'n' AND f.CATEGORY_ID = 1 ORDER BY hasOptions desc,c.SORT_ID ", sqlKeys)
        Session("stype") = "series"
        'Response.Write(strSQL)
        'Response.End()
        sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("PrefixScopeSeries").ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        gvProducts.DataSource = ds
        gvProducts.DataBind()
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvProducts.Rows
            ' If i <> gvProducts.EditIndex Then
            Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
            Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            If Not chkP Is Nothing Then
                ' chkP.Enabled = False
            End If
            If Not lblHasOptions Is Nothing Then
                If LCase(lblHasOptions.Text.ToString) = "y" Then
                    Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                    If Not btnHasOptions Is Nothing Then
                        drGrid.FindControl("btnConfigure").Visible = True
                        btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    End If
                End If
            End If
            ' Else
            'Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            'If Not chkP Is Nothing Then
            'chkP.Checked = True
            'End If
            ' End If
            i = i + 1
        Next
    End Sub

    Private Sub gvProducts_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvProducts.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim row As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim Description As Label = CType(e.Row.FindControl("Description"), Label)
            Description.Text = Functions.GetProdDescriptionOnProdID(Int32.Parse(row("ProductID").ToString()))
        End If
    End Sub

    Protected Sub gvProducts_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvProducts.RowEditing
        gvProducts.EditIndex = e.NewEditIndex
        'Response.Write(gvProducts.EditIndex)
        'LoadProducts(True, gvProducts.EditIndex.ToString)
        Dim ds As DataSet
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(Session("SeriesID").ToString()), "PRODSERID", sqlKeys, sqlParameters)
        strSQL = String.Format("SELECT distinct a.name,c.PRODUCTID,c.PRODUCTDESCRIPTION ,c.PARTNUMBER, c.IMAGEFILENAME,c.productdescription, " +
        " c.PROPERTYGROUPID, c.group_id, d.category_name as NAME, " +
        " CASE WHEN (select count(*) from CONFIG INNER JOIN PRODUCT ON CONFIG.OPTIONID=PRODUCT.PRODUCTID WHERE  PRODUCT.DISABLED = 'n' AND CONFIG.STD='n' AND CONFIG.PRODUCTID=c.PRODUCTID) >0 then 'y' else 'n' END AS hasOptions,c.SORT_ID " +
        " FROM  PRODUCT_SERIES AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " +
        " PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID INNER JOIN CATEGORY AS d ON b.CATEGORY_ID = d.CATEGORY_ID INNER JOIN " +
        " CONFIG AS e ON c.PRODUCTID = e.OPTIONID INNER JOIN PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCTID = f.PRODUCT_ID INNER JOIN " +
        " PRODUCT AS g ON f.PRODUCT_ID = g.PRODUCTID WHERE  b.CATEGORY_ID = @CATEGORYID AND (b.PRODUCT_SERIES_ID IN ({0})) " +
        " AND c.DISABLED = 'n' AND f.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND g.DISABLED = 'n' AND f.CATEGORY_ID = 1 ORDER BY hasOptions desc,c.SORT_ID ", sqlKeys)
        Session("stype") = "series"
        'Response.Write(strSQL)
        'Response.End()
        sqlParameters.Add(New SqlParameter("@CATEGORYID", Session("cataid").ToString()))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Session("PrefixScopeSeries").ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        gvProducts.DataSource = ds
        gvProducts.DataBind()
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvProducts.Rows
            If i <> gvProducts.EditIndex Then
                Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
                Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                If Not chkP Is Nothing Then
                    chkP.Enabled = False
                End If
                If Not lblHasOptions Is Nothing Then
                    If LCase(lblHasOptions.Text.ToString) = "y" Then
                        Dim btnHasOptions As Button = drGrid.FindControl("btnConfigure")
                        If Not btnHasOptions Is Nothing Then
                            drGrid.FindControl("btnConfigure").Visible = True
                            btnHasOptions.Text = LoadI18N("SLBCA0046", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                        End If
                    End If
                End If
            Else
                Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
                If Not chkP Is Nothing Then
                    chkP.Checked = True
                    Dim btnConfigureCancel As Button = drGrid.FindControl("btnConfigure")
                    btnConfigureCancel.Text = LoadI18N("SLBCA0765", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    Dim btnSaveCancel As Button = drGrid.FindControl("btnSave")
                    btnSaveCancel.Text = LoadI18N("SLBCA0766", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                End If
            End If
            i = i + 1
        Next
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveToShopper()
    End Sub

    Protected Sub SaveToShopper()
        Dim i As Integer = 0
        Dim countSelected As Integer = 0
        Dim SeriesID As String = ""
        Dim masterproductid As String = ""

        For Each drGrid As GridViewRow In gvProducts.Rows
            Dim chkP As CheckBox = drGrid.FindControl("chkProducts")
            If Not chkP Is Nothing Then
                If chkP.Checked Then
                    Dim lblP As Label = drGrid.FindControl("lblPRODUCTID")
                    If Not lblP Is Nothing Then
                        countSelected = countSelected + 1

                        Dim cblO As CheckBoxList = drGrid.FindControl("cblOptions")
                        If Not cblO Is Nothing Then
                            If cblO.Items.Count > 0 Then
                                If Session("cataid").ToString() = "19" Then
                                    SeriesID = Session("SeriesID").ToString()
                                Else
                                    SeriesID = 0
                                End If
                                Functions.ShopperSave(Session.SessionID, SeriesID, customerID, lblP.Text.ToString, lblP.Text.ToString, Session("PrefixScopeSeries").ToString)
                                masterproductid = lblP.Text.ToString
                                Session("masterproductid") = lblP.Text.ToString
                                For i = 0 To cblO.Items.Count - 1
                                    If cblO.Items(i).Selected Then
                                        countSelected = countSelected + 1
                                        Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, cblO.Items(i).Value.ToString, Session("PrefixScopeSeries").ToString)
                                    End If
                                Next
                            Else
                                Session("masterproductid") = 0
                                masterproductid = 0
                                Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, lblP.Text.ToString, Session("PrefixScopeSeries").ToString)
                            End If
                        Else
                            Session("masterproductid") = 0
                            masterproductid = 0
                            Functions.ShopperSave(Session.SessionID, SeriesID, customerID, masterproductid, lblP.Text.ToString, Session("PrefixScopeSeries").ToString)
                        End If
                    End If
                End If
            End If
        Next
        If countSelected = 0 Then
            lblRequiredFields.Visible = True
            lblRequiredFields.Text = LoadI18N("SLBCA0781", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Else
            Response.Redirect("configure_small_step3.aspx")
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class