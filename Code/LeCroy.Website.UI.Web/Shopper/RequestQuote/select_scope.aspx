﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" Inherits="LeCroy.Website.Shopper_RequestQuote_selectscope" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" Codebehind="select_scope.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <br /><br /><div class="font28bluebold"><asp:Label ID="lbHeadTitle" runat="server" /></div>
                        <div><asp:Label ID="lbHeadSubTitle" runat="server" Visible="false" /></div>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav2">
                            <asp:Label ID="lbSubNav" runat="server" /><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                                <tr>
                                    <td align="left"><asp:Label ID="lbllanguage" runat="server" Visible="True" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" Visible="True" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="configNav"><LeCroy:quotes ID="ucquotes" runat="server" /></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <div class="contentMiddle">
                <h2><%=GroupName %></h2>
                <strong><asp:Literal ID="litOne" runat="server" />&nbsp;<%=GategoryName %>&nbsp;<asp:Literal ID="litTwo" runat="server" />:</strong><br /><br />
                <asp:CheckBoxList ID="cblGroups" runat="server" DataSourceID="SqlDS_Groups" DataTextField="NAME" DataValueField="PRODUCT_SERIES_ID" BackColor="#ffffff" RepeatColumns="4" RepeatDirection="Horizontal" AutoPostBack="True" />
                <asp:SqlDataSource ID="SqlDS_Groups" runat="server" ConnectionString="<%$ appSettings:connectionString %>" SelectCommand="SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME
                                                                                                                                            FROM         PRODUCT_SERIES INNER JOIN
                                                                                                                                                                  PRODUCT AS PRODUCT_1 INNER JOIN
                                                                                                                                                                  PRODUCT_SERIES_CATEGORY INNER JOIN
                                                                                                                                                                  PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN
                                                                                                                                                                  CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID INNER JOIN
                                                                                                                                                                  PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID ON 
                                                                                                                                                                  PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID  INNER JOIN
                                                                                                                                                                  PRODUCT_GROUP_XREF ON PRODUCT.PRODUCTID = PRODUCT_GROUP_XREF.ProductFkId WHERE     (PRODUCT_1.DISABLED = 'n') AND (PRODUCT.DISABLED = 'n') AND (PRODUCT_GROUP_XREF.ProductGroupFkId = @groupid) AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID=@categoryid) AND (PRODUCT_SERIES_CATEGORY_1.CATEGORY_ID IN (1))
                                                                                                                                            ORDER BY PRODUCT_SERIES.NAME">
                    <SelectParameters>
                        <asp:SessionParameter Name="groupid" SessionField="SeriesID" />
                        <asp:SessionParameter Name="categoryid" SessionField="cataid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br /><asp:Label ID="lblRequiredFields" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="Small" ForeColor="Red" Text="Please select first!" Visible="False" /><br />
                <table width="700" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                    <tr>
                        <td valign="top" width="700">
                            <asp:GridView ID="gvScopes" runat="server" AutoGenerateColumns="False" CellPadding="0" GridLines="Horizontal" ShowHeader="False">
                                <RowStyle VerticalAlign="Top" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <strong><%#Eval("name").ToString()%></strong><br /><br />
                                            <a href="/Oscilloscope/OscilloscopeSeries.aspx?mseries=<%#Eval("PRODUCT_SERIES_ID").ToString()%>" class="info" target="_blank"><img src="<%=rootDir %><%#Eval("PRODUCTIMAGE").ToString()%>" border="0" /></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <br /><asp:Button ID="btnSelect" runat="server" Text="  Select  " CommandName="Edit" />
                                            <span><%#Eval("PRODUCTIMAGEDESC")%></span><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <br /><asp:CheckBoxList ID="cblOptions" runat="server" DataSourceID="SqlDS_COnfig" DataTextField="NAME" DataValueField="PRODUCT_SERIES_ID" BackColor="Gainsboro" Enabled="False" RepeatColumns="2" RepeatDirection="Horizontal" AutoPostBack="True" ForeColor="Black" />
                                            <asp:SqlDataSource ID="SqlDS_COnfig" runat="server" ConnectionString="<%$ appSettings:connectionString %>" SelectCommand="SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME
                                                                                                                                                                        FROM         PRODUCT_SERIES INNER JOIN
                                                                                                                                                                                              PRODUCT AS PRODUCT_1 INNER JOIN
                                                                                                                                                                                              PRODUCT_SERIES_CATEGORY INNER JOIN
                                                                                                                                                                                              PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN
                                                                                                                                                                                              CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID INNER JOIN
                                                                                                                                                                                              PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID ON 
                                                                                                                                                                                              PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID INNER JOIN
                                                                                                                                                                                              PRODUCT_GROUP_XREF ON PRODUCT.PRODUCTID = PRODUCT_GROUP_XREF.ProductFkId
                                                                                                                                                                        WHERE     (PRODUCT_1.DISABLED = 'n') AND (PRODUCT.DISABLED = 'n') AND (PRODUCT_SERIES.DISABLED = 'n')  AND (PRODUCT_GROUP_XREF.ProductGroupFkId= @groupid) AND (PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID = @scope_series_id) 
                                                                                                                                                                                              AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = @categoryid)
                                                                                                                                                                        ORDER BY PRODUCT_SERIES.NAME">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lblSCOPESERIESID" Name="scope_series_id" PropertyName="Text" />
                                                    <asp:SessionParameter Name="groupid" SessionField="SeriesID" />
                                                    <asp:SessionParameter Name="categoryid" SessionField="cataid" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:Label ID="lblSCOPESERIESID" runat="server" Text='<%# Bind("PRODUCT_SERIES_ID") %>' Visible="False" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <strong><asp:Literal ID="litThree" runat="server" />&nbsp;<%=GategoryName%>:</strong><br />
                                            <asp:CheckBoxList ID="cblOptions" runat="server" DataSourceID="SqlDS_COnfig" DataTextField="NAME" DataValueField="PRODUCT_SERIES_ID" BackColor="#DFF4FF" Enabled="true" RepeatColumns="2" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="cblOptions_SelectedIndexChanged" />
                                            <asp:SqlDataSource ID="SqlDS_COnfig" runat="server" ConnectionString="<%$ appSettings:connectionString %>" SelectCommand="SELECT DISTINCT PRODUCT_SERIES.PRODUCT_SERIES_ID, PRODUCT_SERIES.NAME
                                                                                                                                                                        FROM         PRODUCT_SERIES INNER JOIN
                                                                                                                                                                                              PRODUCT AS PRODUCT_1 INNER JOIN
                                                                                                                                                                                              PRODUCT_SERIES_CATEGORY INNER JOIN
                                                                                                                                                                                              PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID INNER JOIN
                                                                                                                                                                                              CONFIG ON PRODUCT.PRODUCTID = CONFIG.OPTIONID ON PRODUCT_1.PRODUCTID = CONFIG.PRODUCTID INNER JOIN
                                                                                                                                                                                              PRODUCT_SERIES_CATEGORY AS PRODUCT_SERIES_CATEGORY_1 ON PRODUCT_1.PRODUCTID = PRODUCT_SERIES_CATEGORY_1.PRODUCT_ID ON 
                                                                                                                                                                                              PRODUCT_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID INNER JOIN
                                                                                                                                                                                              PRODUCT_GROUP_XREF ON PRODUCT.PRODUCTID = PRODUCT_GROUP_XREF.ProductFkId
                                                                                                                                                                        WHERE     (PRODUCT_1.DISABLED = 'n') AND (PRODUCT.DISABLED = 'n') AND (PRODUCT_SERIES.DISABLED = 'n') AND (PRODUCT_GROUP_XREF.ProductGroupFkId= @groupid) AND (PRODUCT_SERIES_CATEGORY_1.PRODUCT_SERIES_ID = @scope_series_id) 
                                                                                                                                                                                              AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = @categoryid)
                                                                                                                                                                        ORDER BY PRODUCT_SERIES.NAME">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lblSCOPESERIESID" Name="scope_series_id" PropertyName="Text" />
                                                    <asp:SessionParameter Name="groupid" SessionField="SeriesID" />
                                                    <asp:SessionParameter Name="categoryid" SessionField="cataid" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:Label ID="lblSCOPESERIESID" runat="server" Text='<%# Bind("PRODUCT_SERIES_ID") %>' Visible="False" />
                                            <asp:Label ID="lblSID" runat="server" Text='<%# Bind("PRODUCT_SERIES_ID") %>' Visible="False" />
                                        </EditItemTemplate>
                                        <ItemStyle Font-Bold="False" Font-Underline="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <br /><asp:Button ID="btnSave" runat="server" Text="   Next   " OnClick="btnSave_Click" Enabled="true" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#C4ECFF" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>