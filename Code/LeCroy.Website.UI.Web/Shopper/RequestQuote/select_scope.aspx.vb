﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Shopper_RequestQuote_selectscope
    Inherits QuoteBasePage
    Public name As String = ""
    Dim customerID As String = "0"
    Dim strSQL As String = ""
    Public Shared GroupName As String = ""
    Public Shared GategoryName As String = ""
    Public Shared mseries As String = ""
    Dim i As Integer = 0
    Dim j As Integer = 0
    Dim k As Integer = 0
    Dim strGroups As String = ""
    Dim strProfile As String = ""
    Dim ds, ds1 As DataSet
    Dim flgBind As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("ContactID") Is Nothing Then
            If Len(Session("ContactID")) > 0 Then
                customerID = Session("ContactID").ToString()
            End If
        End If
        Dim priceForProduct As String = ""

        If Session("cataid") Is Nothing Then
            Response.Redirect("default.aspx")
            Return
        End If
        If Request("SeriesID") Is Nothing And Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If

        If Not Request("SeriesID") Is Nothing Then
            If IsNumeric(Request("SeriesID")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("SeriesID")), ",") Then
                    Session("SeriesID") = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("SeriesID")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("SeriesID")), ",") + 1)
                Else
                    Session("SeriesID") = SQLStringWithOutSingleQuotes(Request.QueryString("SeriesID"))
                End If
                If Not Session("cataid") Is Nothing Then
                    If Not Session("cataid").ToString() = "19" Then
                        Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                    End If
                Else
                    Session("cataid") = Functions.GetCategoryIDonGroupID(Session("SeriesID"))
                End If
            Else
                Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
            End If
        ElseIf Session("SeriesID") Is Nothing Then
            Response.Redirect("Configure_Small_Step1.aspx?catid=" & Session("cataid"))
        End If
        If Len(Session("cataid")) = 0 Then
            Response.Redirect("/shopper/default.aspx")
        End If

        '** mseries
        If Len(Request.QueryString("mseries")) > 0 Then

            If IsNumeric(Request.QueryString("mseries")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") Then
                    mseries = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") + 1)
                Else
                    mseries = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                End If
                If Not Functions.CheckSeriesExists(mseries, Session("cataid")) Then
                    Response.Redirect("default.aspx")
                End If
            End If
        End If

        If Not Page.IsPostBack Then
            'show selected series
            If Len(mseries) > 0 Then
                'top selection
                cblGroups.DataBind()
                If Not cblGroups Is Nothing Then
                    If cblGroups.Items.Count > 0 Then
                        For j = 0 To cblGroups.Items.Count - 1
                            If cblGroups.Items(j).Value.ToString = mseries.ToString Then
                                cblGroups.Items(j).Selected = True
                            End If

                        Next
                    End If
                End If
                'selection under each scope series
                For Each drGrid As GridViewRow In gvScopes.Rows
                    Dim cblO As CheckBoxList = CType(drGrid.FindControl("cblOptions"), CheckBoxList)
                    If Not cblO Is Nothing Then
                        cblO.DataBind()
                        If cblO.Items.Count > 0 Then
                            For i = 0 To cblO.Items.Count - 1
                                If cblO.Items(i).Value.ToString = mseries.ToString Then
                                    cblO.Items(i).Selected = True
                                End If
                            Next
                        End If
                    End If
                Next
                strGroups = mseries
            End If
        Else

            If Not cblGroups Is Nothing Then
                If cblGroups.Items.Count > 0 Then
                    For j = 0 To cblGroups.Items.Count - 1
                        If cblGroups.Items(j).Selected Then
                            If Len(strGroups) = 0 Then
                                strGroups = cblGroups.Items(j).Value.ToString
                            Else
                                strGroups = strGroups + "," + cblGroups.Items(j).Value.ToString
                            End If
                        End If

                    Next
                End If
            End If
        End If
        If Not Page.IsPostBack Then
            LoadProducts()
            CheckGroupSelection()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
        End If
        litOne.Text = Functions.LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0778", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lbHeadTitle.Text = Functions.LoadI18N("SLBCA0752", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        strProfile = "<ul><li><a href='/support/user/userprofile.aspx'>" + LoadI18N("SLBCA0005", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "<li><a href='/shopper/quotehistory.aspx'>" + LoadI18N("SLBCA0003", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></li>"
        strProfile += "</ul>"
        lbSubNav.Text = strProfile
        lbllanguage.Text = LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Sub LoadProducts()

        ' If Not Me.IsPostBack Then
        ' Response.Write(strGroups)

        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(strGroups) > 0 Then
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strGroups), "PRODSERID", sqlKeys, sqlParameters)
            strSQL = String.Format(" SELECT DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.PRODUCTIMAGE, REPLACE(e.PRODUCTIMAGEDESC, 'Quick Specs:', '') AS PRODUCTIMAGEDESC, " +
                     " e.SORTID FROM  PRODUCT AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b INNER JOIN PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID " +
                     " INNER JOIN CONFIG AS d ON c.PRODUCTID = d.OPTIONID ON a.PRODUCTID = d.PRODUCTID INNER JOIN PRODUCT_SERIES AS e INNER JOIN " +
                     " PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCT_SERIES_ID = f.PRODUCT_SERIES_ID ON a.PRODUCTID = f.PRODUCT_ID " +
                     " INNER JOIN PRODUCT_GROUP_XREF AS x ON c.PRODUCTID = x.ProductFkId " +
                     " where x.ProductGroupFkId = @PRODUCTGROUPFKID and b.product_series_id in ({0}) and  f.category_id = 1  and a.disabled = 'n' and c.disabled = 'n' order by e.sortid", sqlKeys)
        Else
            strSQL = " SELECT DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.PRODUCTIMAGE, REPLACE(e.PRODUCTIMAGEDESC, 'Quick Specs:', '') AS PRODUCTIMAGEDESC, " +
                     " e.SORTID FROM  PRODUCT AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b INNER JOIN PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID " +
                     " INNER JOIN CONFIG AS d ON c.PRODUCTID = d.OPTIONID ON a.PRODUCTID = d.PRODUCTID INNER JOIN PRODUCT_SERIES AS e INNER JOIN " +
                     " PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCT_SERIES_ID = f.PRODUCT_SERIES_ID ON a.PRODUCTID = f.PRODUCT_ID " +
                     " INNER JOIN PRODUCT_GROUP_XREF AS x ON c.PRODUCTID = x.ProductFkId where x.ProductGroupFkId = @PRODUCTGROUPFKID and  f.category_id = 1  and a.disabled = 'n' and c.disabled = 'n' order by e.sortid"
        End If
        'Response.Write(strSQL)
        sqlParameters.Add(New SqlParameter("@PRODUCTGROUPFKID", Session("seriesid").ToString()))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        'Response.End()
        If ds.Tables(0).Rows.Count > 0 Then
            gvScopes.DataSource = ds
            gvScopes.DataBind()
        End If
        Dim i As Integer = 0
        For Each drGrid As GridViewRow In gvScopes.Rows
            If drGrid.RowState = DataControlRowState.Edit Then
                If i = gvScopes.EditIndex Then
                    Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
                    Dim lblScopeSeriesId As CheckBox = CType(drGrid.FindControl("lblScopeSeriesIdroducts"), CheckBox)
                    If Not lblScopeSeriesId Is Nothing Then
                        lblScopeSeriesId.Enabled = False
                    End If
                    Dim btnSelect1 As Button = drGrid.FindControl("btnSelect")
                    btnSelect1.Text = LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    Dim btnNext1 As Button = drGrid.FindControl("btnSave")
                    btnNext1.Text = LoadI18N("SLBCA0851", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                Else

                    Dim lblScopeSeriesId As CheckBox = CType(gvScopes.Rows(i).FindControl("lblScopeSeriesIdroducts"), CheckBox)
                    If Not lblScopeSeriesId Is Nothing Then
                        lblScopeSeriesId.Checked = True
                    End If

                End If
            Else

                Dim btnSelect1 As Button = drGrid.FindControl("btnSelect")
                btnSelect1.Text = LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            End If

            i = i + 1
        Next
      '  End If

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Select Scope - " & Functions.getTitleForGroup(Session("SeriesID").ToString())
        GroupName = Functions.getTitleForGroup(Session("SeriesID").ToString)
        GategoryName = Functions.getTitleForCat(Session("cataid").ToString)

        Dim localeIdFromGeoIp As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("LocaleIdFromGeoIp"))
        If (localeIdFromGeoIp >= 0) Then
            ddlLanguage.ClearSelection()
            '  If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
            strSQL = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' and QUOTES_YN='y'  ORDER BY sort_id"
            ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            ddlLanguage.DataSource = ds1
            ddlLanguage.DataBind()
            Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
            If Len(localeId) > 0 Then
                ddlLanguage.SelectedValue = localeId
            Else
                ddlLanguage.SelectedValue = localeIdFromGeoIp.ToString()
            End If
            'ddlLanguage.Visible = True
            ' lbllanguage.Visible = True
            '  End If
        End If
    End Sub

    Private Sub gvScopes_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvScopes.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If (e.Row.RowState And DataControlRowState.Edit) > 0 Then
                Dim litThree As Literal = CType(e.Row.FindControl("litThree"), Literal)
                litThree.Text = Functions.LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            End If
        End If
    End Sub

    Protected Sub gvScopes_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvScopes.RowEditing
        languagechanged()
        gvScopes.EditIndex = e.NewEditIndex

        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(strGroups) > 0 Then
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strGroups), "PRODSERID", sqlKeys, sqlParameters)
            strSQL = String.Format(" SELECT DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.PRODUCTIMAGE, REPLACE(e.PRODUCTIMAGEDESC, 'Quick Specs:', '') AS PRODUCTIMAGEDESC, " +
                     " e.SORTID FROM  PRODUCT AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b INNER JOIN PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID " +
                     " INNER JOIN CONFIG AS d ON c.PRODUCTID = d.OPTIONID ON a.PRODUCTID = d.PRODUCTID INNER JOIN PRODUCT_SERIES AS e INNER JOIN " +
                     " PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCT_SERIES_ID = f.PRODUCT_SERIES_ID ON a.PRODUCTID = f.PRODUCT_ID " +
                     " INNER JOIN PRODUCT_GROUP_XREF AS x ON c.PRODUCTID = x.ProductFkId " +
                     " where x.ProductGroupFkId = @PRODUCTGROUPFKID and b.product_series_id in ({0}) and  f.category_id = 1  and a.disabled = 'n' and c.disabled = 'n' order by e.sortid", sqlKeys)
        Else
            strSQL = " SELECT DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.PRODUCTIMAGE, REPLACE(e.PRODUCTIMAGEDESC, 'Quick Specs:', '') AS PRODUCTIMAGEDESC, " +
                     " e.SORTID FROM  PRODUCT AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b INNER JOIN PRODUCT AS c ON b.PRODUCT_ID = c.PRODUCTID " +
                     " INNER JOIN CONFIG AS d ON c.PRODUCTID = d.OPTIONID ON a.PRODUCTID = d.PRODUCTID INNER JOIN PRODUCT_SERIES AS e INNER JOIN " +
                     " PRODUCT_SERIES_CATEGORY AS f ON e.PRODUCT_SERIES_ID = f.PRODUCT_SERIES_ID ON a.PRODUCTID = f.PRODUCT_ID " +
                     " INNER JOIN PRODUCT_GROUP_XREF AS x ON c.PRODUCTID = x.ProductFkId " +
                     " where x.ProductGroupFkId = @PRODUCTGROUPFKID and  f.category_id = 1  and a.disabled = 'n' and c.disabled = 'n' order by e.sortid"
        End If
        sqlParameters.Add(New SqlParameter("@PRODUCTGROUPFKID", Session("seriesid").ToString()))
        'Response.Write(strSQL)
        'Response.End()
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        gvScopes.DataSource = ds
        gvScopes.DataBind()


        Dim i As Integer = 0
        Dim k As Integer = 0
        For Each drGrid As GridViewRow In gvScopes.Rows
            If i = gvScopes.EditIndex Then
                Dim lblHasOptions As Label = drGrid.FindControl("hasOptions")
                Dim lblScopeSeriesId As CheckBox = CType(drGrid.FindControl("lblScopeSeriesIdroducts"), CheckBox)
                If Not lblScopeSeriesId Is Nothing Then
                    lblScopeSeriesId.Enabled = False
                End If
                  Dim btnSelect1 As Button = drGrid.FindControl("btnSelect")
                btnSelect1.Text = LoadI18N("SLBCA0171", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                Dim btnNext1 As Button = drGrid.FindControl("btnSave")
                btnNext1.Text = LoadI18N("SLBCA0851", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            Else

                Dim lblScopeSeriesId As CheckBox = CType(gvScopes.Rows(i).FindControl("lblScopeSeriesIdroducts"), CheckBox)
                If Not lblScopeSeriesId Is Nothing Then
                    lblScopeSeriesId.Checked = True
                End If
                
            End If
            i = i + 1
        Next
        CheckGroupSelection()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveToShopper()
    End Sub

    Protected Sub SaveToShopper()
        Dim i As Integer = 0
        Dim countSelected As Integer = 0
        Dim ScopeSeriesID As String = ""
        Dim SeriesID As String = ""
        Dim masterproductid As String = ""

        For Each drGrid As GridViewRow In gvScopes.Rows
            Dim lblScopeSeriesId As Label = drGrid.FindControl("lblSID")
            If Not lblScopeSeriesId Is Nothing Then
                If Not lblScopeSeriesId Is Nothing Then
                    Dim cblO As CheckBoxList = drGrid.FindControl("cblOptions")
                    If Not cblO Is Nothing Then
                        If cblO.Items.Count > 0 Then
                            ScopeSeriesID = lblScopeSeriesId.Text.ToString
                            For i = 0 To cblO.Items.Count - 1
                                If cblO.Items(i).Selected Then
                                    countSelected = countSelected + 1
                                    If Len(SeriesID) > 0 Then
                                        SeriesID = SeriesID + "," + cblO.Items(i).Value.ToString
                                    Else
                                        SeriesID = cblO.Items(i).Value.ToString
                                    End If

                                End If
                            Next
                        End If

                    End If
                    'End If
                End If
            End If
        Next
        'Response.Write(ScopeSeriesID)
        If countSelected = 0 Or Len(SeriesID) = 0 Then
            lblRequiredFields.Text = LoadI18N("SLBCA0779", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + " " + GroupName.ToString + "!"
            lblRequiredFields.Visible = True
        Else
            Response.Redirect("select_model.aspx?SeriesID=" + SeriesID.ToString + "&sid=" + ScopeSeriesID.ToString)
        End If
    End Sub

    Protected Sub cblGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblGroups.SelectedIndexChanged
        LoadProducts()
        CheckGroupSelection()
    End Sub

    Protected Sub cblOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim k As Integer = 0
        Dim m As Integer = 0
        Dim flgBind As Boolean = False

        For Each drGrid As GridViewRow In gvScopes.Rows
            If k = gvScopes.EditIndex Then
                Dim cblO As CheckBoxList = drGrid.FindControl("cblOptions")
                If Not cblO Is Nothing Then
                    If cblO.Items.Count > 0 Then
                        For i = 0 To cblO.Items.Count - 1
                            If Not cblGroups Is Nothing Then
                                If cblGroups.Items.Count > 0 Then
                                    For j = 0 To cblGroups.Items.Count - 1
                                        If cblO.Items(i).Selected Then
                                            If cblO.Items(i).Value.ToString = cblGroups.Items(j).Value.ToString Then
                                                cblGroups.Items(j).Selected = True
                                            End If
                                        Else
                                            If cblO.Items(i).Value.ToString = cblGroups.Items(j).Value.ToString Then
                                                cblGroups.Items(j).Selected = False
                                                'Response.Write(cblO.Items(i).Value.ToString)
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        Next
                    End If
                End If
            Else
                Dim cblO1 As CheckBoxList = drGrid.FindControl("cblOptions")
                If Not cblO1 Is Nothing Then
                    If Not flgBind Then
                        cblO1.DataBind()
                        flgBind = True
                    End If
                    If cblO1.Items.Count > 0 Then
                        For m = 0 To cblO1.Items.Count - 1
                            If Not cblGroups Is Nothing Then
                                If cblGroups.Items.Count > 0 Then
                                    For j = 0 To cblGroups.Items.Count - 1
                                        If cblGroups.Items(j).Selected Then
                                            If cblO1.Items(m).Value.ToString = cblGroups.Items(j).Value.ToString Then
                                                cblO1.Items(m).Selected = True
                                            End If
                                        Else
                                            If cblO1.Items(m).Value.ToString = cblGroups.Items(j).Value.ToString Then
                                                cblO1.Items(m).Selected = False
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        Next

                    End If
                End If
            End If
            k = k + 1
        Next

    End Sub

    Protected Sub CheckGroupSelection()

        For Each drGrid As GridViewRow In gvScopes.Rows
            flgBind = False
            If Not cblGroups Is Nothing Then
                If cblGroups.Items.Count > 0 Then
                    For j = 0 To cblGroups.Items.Count - 1
                        If cblGroups.Items(j).Selected Then
                            'selection under each scope series

                            Dim cblO As CheckBoxList = CType(drGrid.FindControl("cblOptions"), CheckBoxList)
                            If Not cblO Is Nothing Then
                                If Not flgBind Then
                                    cblO.DataBind()
                                    flgBind = True
                                End If
                                If cblO.Items.Count > 0 Then
                                    For i = 0 To cblO.Items.Count - 1
                                        If cblO.Items(i).Value.ToString = cblGroups.Items(j).Value.ToString Then
                                            cblO.Items(i).Selected = True
                                        End If
                                    Next
                                End If
                            End If

                        End If
                    Next
                End If
            End If

        Next

    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
languagechanged
        LoadProducts()
    End Sub
    Protected Sub languagechanged()
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
    End Sub
End Class