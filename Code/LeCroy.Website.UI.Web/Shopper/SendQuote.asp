<%@ Language=VBScript %>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<%
On Error Resume Next
'ServerName = Request.ServerVariables("SERVER_NAME")
'if len(ServerName) = 0 then
	ServerName="teledynelecroy.com"
'end if 
%>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/shopper/includes/ViewPendingQuotes.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="/shopper/functions.asp" -->
<%
	if GetCountofProdInBasket = 0 then
		Response.Redirect "default.aspx"
	end if

	Session("ConfigureScopeFlag")=false
	Session("QuoteNumberFlag")=true
	Session("QuoteID") = ""
	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="DigitalOscilloscopes" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
	bannername="DigitalOscilloscopes"
'***************added by YK 5/24/01******************	
	if len(Session("localeid"))=0 then Session("localeid")=1033
	localeid=Session("localeid")
	'set localeid for email body
	if Session("Country")="Germany" or Session("Country")="Austria" then
		emaillocaleid=1031
	elseif Session("Country")="France" then
		emaillocaleid=1033
	elseif Session("Country")="Italy" then
		emaillocaleid=1040
	elseif Session("Country")="Japan" then
		emaillocaleid=1041
	else
		emaillocaleid=1033
	end if
	Session("emaillocaleid")=emaillocaleid
'*************************************************************	
%>
<% 
Session("RedirectTo") = "/shopper/SendQuote.asp" 'here is the name of the file itself
Session("fromid") = "1"
If Not ValidateUser Then
	Response.redirect "/cgi-bin/profile/SentToReg.asp"
End If
ValidateUserNoRedir()
%>
<%	
strCRet = chr(13) & chr(10)
strQuote = chr(34)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Logic for sending US quotes for WS products to Distributors
'Added by Kate Kaplan 7/18/2005
'if there are WS products in the basket
'and customer is from United States,
'Send Email to the Distributor, copy SE and send the quote 
'for CSR approval
'for other products send E-Mail to CSR and SE
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Logic for PSG products quotes
'Added by Kate Kaplan 3/10/05
'****International Quotes **********************************
'Send to LeCroy Sales Rep and Copy to PSG emails
'****United States******************************************
'Quote request for PSG products will be sent via Email 
'to SE and PSG AM, not to CSRs.
'Quote request email for PSG products doesn't include prices
'No CSRs reminder for PSG products quote
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'If County is other than US then send to local country
		if not Session("COUNTRY") = "United States" or Session("COUNTRY") = "Select Country" then
				'Generate a separate quote and send it to the Distributor, CC SE
				strQuoteID=GenerateQuote
				'Subjectline
				if len(Session("offerid"))=0 then
				    if Session("COUNTRY")="Japan" or Session("COUNTRY")="China" or Mid(Session("COUNTRY")="Korea",1,5) then
		                set rsSubject=Server.CreateObject("ADODB.Recordset")
                        strSQL="Select  COUNTRY,Company,First_Name,Last_name from CONTACT where CONTACT_ID=" & Session("contactid")
                        'response.Write strSQL
                        rsSubject.Open strSQL, dbconnstr()
                        if not rsSubject.eof then
           					strSubject="Request for Quotation - " & rsSubject("COUNTRY") & ", " & rsSubject("Company") & ", " & rsSubject("First_Name") & " " & rsSubject("Last_Name")
                        else
					        strSubject="Request for Quotation - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
                        end if
				    else
					    strSubject="Request for Quotation - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
				    end if
				else
					strSubject="Request for Quotation - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")  & " - " & GetOfferInfo(Session("CampaignID"),0)
				end if
	
				'if Session("localeid")=1041 then
				'else
					if len(Session("offerid"))>0 then
						call SendQuoteEmail(GetSalesRepEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBodyNew(strQuoteID)& "Promotion pricing cannot be combined with any other discounts"  & strCret & strCret)
						'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBodyNew(strQuoteID)& "Promotion pricing cannot be combined with any other discounts"  & strCret & strCret)
					else
						if Session("COUNTRY")="Canada" then
					        if  CheckPSGQuote(strQuoteID) then
							    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",strSubject & " - PSG",GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
							    call SendQuoteEmail(GetSalesRepEmail & ",Adam.Frisius@teledynelecroy.com,Debbie.Savoie@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
					        else
							    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
							    call SendQuoteEmail(GetSalesRepEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
			                end if	
						'elseif Session("COUNTRY")="Japan" then
					     '   call SendQuoteEmail("contact.jp@teledynelecroy.com", "webquote@teledynelecroy.com","ai.yamamoto@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBodyNew(strQuoteID,emaillocaleid))
							'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
						elseif Session("COUNTRY")="Mexico" or Session("COUNTRY")="Brazil" then
							if  CheckPSGQuote(strQuoteID) then
								if CheckScopesQuote(strQuoteID) then
									call SendQuoteEmail(GetSalesRepEmail , "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
									'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & "1",GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
								else
									if Session("COUNTRY")="Mexico" then
										call SendQuoteEmail("jmendolia@teledynelecroy.com,Jeff.Ross@teledynelecroy.com,Adam.Frisius@teledynelecroy.com,Debbie.Savoie@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
										'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & "2",GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
									else
										call SendQuoteEmail("jmendolia@teledynelecroy.com,Debbie.Savoie@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
										'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & "2",GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
									end if
								end if
							else
									call SendQuoteEmail(GetSalesRepEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
									'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & "3",GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
							end if
						else
						    if Session("COUNTRY")="Japan" then
							    call SendQuoteEmail(GetSalesRepEmail, "webquote@teledynelecroy.com","ai.yamamoto@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
							    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
						    else
							    call SendQuoteEmail(GetSalesRepEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
							    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & EMailBody(strQuoteID,emaillocaleid))
						    end if
						end if
					end if
			'	end if

				'SS added for EU customers receive confirmation 10/25/04
				if len(Session("Country") ) > 0 and len(Session("email")) > 0 then
					if(GetRegionOnCountry(Session("Country"))) = 2 then
						call SendQuoteEmail(Session("email"), "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com","LeCroy Quote Request Confirmation","Thank you for your interest in LeCroy products. You will receive the quotation on the selected products shortly from your local LeCroy representative.")
					end if 
				end if 
				'SS 10/25/04 end of added code
		Else
		'++++++++++++United States
		'Subjectline
		if len(Session("offerid"))=0 then
			strSubject=LoadI18N("SLBCA0105",emaillocaleid) & " - " &  Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
		else
			strSubject=LoadI18N("SLBCA0105",emaillocaleid) & " - " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")  & " - " & GetOfferInfo(Session("CampaignID"),0)
		end if

			'*****************************************
			'Quote Generation   
			'*****************************************
       
			'Generate a separate quote for WS (includes WS 400, WS XS and WS MXS)
			if CheckDistProductInBasket then
				strWSQuoteID=GenerateWSQuote
				'call SendQuoteCSRSE (strWSQuoteID,emaillocaleid,ServerName)
                call SendQuoteToCustomer (strWSQuoteID,Session("ContactID"),emaillocaleid,ServerName)
			end if
			
			'Check if there are PSG distributor products are in the basket. Generate a separate quote 
			if CheckDistProtocolsInBasket then
				strDistPSGQuoteID=GenerateDistProtocolsQuote
				call SendQuoteToCustomer (strDistPSGQuoteID,Session("ContactID"),emaillocaleid,ServerName)
			end if
			
			'Check if there are PSG products in the basket. Generate a separate quote 
			if CheckProtocolsInBasket then
				strPSGQuoteID=GeneratePSGQuote
				strSubjectPSG=LoadI18N("SLBCA0105",emaillocaleid) & " - " & Session("Company") & " - " & Session("LastName")
				call SendQuoteCSRSE (strPSGQuoteID,emaillocaleid,ServerName)
			end if

			 'Logic Analyzers
			'if CheckLogicAnalyzerInBasket then
			'	strLogicQuoteID=GenerateLogicAnalyzerQuote
			'	'strSubjectPSG=LoadI18N("SLBCA0105",emaillocaleid) & " - " & Session("Company") & " - " & Session("LastName")
			'	call SendQuoteToCustomer (strLogicQuoteID,Session("ContactID"),emaillocaleid,ServerName)
			'end if

			'For all scope products generate another quote
			if cint(GetCountofProdInBasket)>0 then
				strQuoteID=GenerateQuote
				call SendQuoteCSRSE (strQuoteID,emaillocaleid,ServerName)
				'call SendQuoteToCustomer (strQuoteID,emaillocaleid,ServerName)
			end if
			
			'*****************************************
			'Send Quote confirmation email to Customer   
			'*****************************************

			'Create E-Mail Body	
			if len(strPSGQuoteID)>0 or len(strQuoteID)>0 then
				strBody="Dear " & Session("FirstName") & ","  & strCRet  & strCRet
				strBody=strBody & "Thank you for requesting a quote from LeCroy Website." & strCRet  & strCRet
				strBody=strBody & "We are reviewing your request and will send you a quotation within 24 hours. We want to provide the best possible service available.  If we have questions about your request, we will attempt to contact you." & strCRet
				strBody=strBody & strCRet & "Please note that your webquote will not reflect any educational, government or promotional pricing that may be available." & strCRet
				strBody=strBody & "Please contact our Customer Support Center at (800) 5-LECROY if you feel you may be eligible for these discounts." & strCRet
				strBody=strBody & strCRet & "You can see a list of our current promotions here:"& strCRet
				strBody=strBody & "http://" & ServerName & "/promotions/" & strCRet & strCRet
				strBody=strBody & strCRet & "Thank you for your consideration." & strCRet 
				strBody=strBody & strCRet & "LeCroy Customer Support" 
				strBody=strBody & strCRet & "1-800-553-2769" 
				strBody=strBody & strCRet & "mailto:customersupport@teledynelecroy.com" 
				strEMailBody = strBody 
				call SendQuoteEmail(Session("Email"), "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com","LeCroy Quote Confirmation",strEMailBody)
				'call SendQuoteEmail(Session("Email"), "webquote@teledynelecroy.com","","","LeCroy Quote Confirmation",strEMailBody)
            end if
		end if
	Session("offerid")=""
	'response.write strQuoteID & "<br>" & strPSGQuoteID& "<br>" &strPert3QuoteID
	'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Response.Redirect "AfterSendQuote.aspx?sq=" & strQuoteID & "&pq=" & strPSGQuoteID & "&sdq=" & strWSQuoteID & "&lq=" & strLogicQuoteID & "&pdq=" & strDistPSGQuoteID
%>