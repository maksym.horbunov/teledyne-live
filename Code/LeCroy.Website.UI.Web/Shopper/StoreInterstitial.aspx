﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="StoreInterstitial.aspx.vb" Inherits="LeCroy.Website.Shopper_StoreInterstitial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit">
        <div class="span8">
            <p>Thank you for your interest in purchasing <asp:Label ID="lblSoftware" runat="server" />.</p>
            <p>We need the following information to ensure that the software is compatible with your oscilloscope.</p>
            <fieldset>
                <label><asp:Label ID="lblScopeId" runat="server" /> <span class="small"><a href="#" id="hlkScopeIdDialog"><asp:Label ID="lblScopeIdLocation" runat="server" /></a></span></label>
                <asp:TextBox ID="txtScopeId" runat="server" ClientIdMode="Static" MaxLength="9" TabIndex="1" />&nbsp;<asp:CustomValidator ID="cvScopeId" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSubmit" />
                <label><asp:Label ID="lblSerialNumber" runat="server" /> <span class="small"><a href="#" id="hlkSerialNumberDialog"><asp:Label ID="lblSerialNumberLocation" runat="server" /></a></span></label>
                <asp:TextBox ID="txtSerialNumber" runat="server" ClientIdMode="Static" MaxLength="15" TabIndex="2" />&nbsp;<asp:CustomValidator ID="cvSerialNumber" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSubmit" />
                 <fieldset>
                    <asp:Button ID="btnValidateEntries" runat="server" CausesValidation="false" CssClass="btn" TabIndex="4" Text="Validate" />
                </fieldset>
            </fieldset>
            <div id="divOtherOptions" runat="server" visible="false">
                Your oscilloscope can also use these options. Would you like to add them to your order as well?<br />
                <asp:CheckBoxList ID="cblOtherOptions" runat="server" RepeatColumns="3" />
                <fieldset>
                    <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" CssClass="btn" TabIndex="5" Text="Go to checkout" />
                </fieldset>
            </div>
        </div>

        <div id="dialog-confirm" style="background-color: #ffffff; margin: 0 0 0 0;">
        <div>
            <img id="imgDisplayed" alt="" border="0" height="400" width="640" />
        </div>
    </div>
    </asp:Panel>

    <script language="javascript" type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $('#dialog:ui-dialog').dialog('destroy');
            $('#dialog-confirm').dialog('open');
            var dlg = $("#dialog-confirm").dialog({
                autoOpen: false,
                closeOnEscape: true,
                closeText: 'X',
                draggable: true,
                height: 483,
                modal: true,
                resizable: false,
                width: 680
            });

            $('#hlkScopeIdDialog').click(function () {
                $('#dialog-confirm').dialog('open');
                $('span.ui-dialog-title').text($('#<%= lblScopeIdLocation.ClientID %>').html());
                $('span.ui-dialog-title').append('<br /><span style="font-size: 9pt;">Click "Utilities" on the top menu and select "Utilities Setup" from the drop down list. Then select the<br /> "Options" tab.  Your Scope ID and Serial Number are displayed on the left side of the screen.</span>');
                $('#imgDisplayed').attr('src', './images/scopeid_serialnum_from_scope_menu.jpg');
                return false;
            });
            $('#hlkSerialNumberDialog').click(function () {
                $('#dialog-confirm').dialog('open');
                $('span.ui-dialog-title').text($('#<%= lblSerialNumberLocation.ClientID %>').html());
                $('span.ui-dialog-title').append('<br /><span style="font-size: 9pt;">Click "Utilities" on the top menu and select "Utilities Setup" from the drop down list. Then select the<br /> "Options" tab.  Your Scope ID and Serial Number are displayed on the left side of the screen.</span>');
                $('#imgDisplayed').attr('src', './images/scopeid_serialnum_from_scope_menu.jpg');
                return false;
            });
            $('.ui-widget-overlay').live('click',
		    function () {
		        $('#dialog-confirm').dialog('X');
		    });
        });
    </script>
</asp:Content>