﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.Domain.Common.Constants

Public Class Shopper_StoreInterstitial
    Inherits BasePage

#Region "Variables/Keys"
    Private _productSeriesId As Int32 = -1
    Private _productGroupId As Int32 = -1
    Private _serialNumber As String = String.Empty
    Private _selectedProductId As Int32 = -1
    Private _scopeId As String = String.Empty
    Private _scopeProductId As Int32 = -1
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        lblScopeIdLocation.Text = Functions.LoadI18N("SLBCA0913", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblSerialNumberLocation.Text = Functions.LoadI18N("SLBCA0914", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblScopeId.Text = Functions.LoadI18N("SLBCA0862", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblSerialNumber.Text = Functions.LoadI18N("SLBCA0480", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Me.Title = Functions.LoadI18N("SLBCA0927", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Dim onBlurScriptScopeId = Page.ClientScript.GetPostBackEventReference(txtScopeId, "OnBlur")
        txtScopeId.Attributes.Add("onblur", onBlurScriptScopeId)
        Dim onBlurScriptSerial = Page.ClientScript.GetPostBackEventReference(txtSerialNumber, "OnBlur")
        txtSerialNumber.Attributes.Add("onblur", onBlurScriptSerial)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ValidateQueryString()
        If Not (IsPostBack) Then
            BindPage()
        Else
            Dim ctrlName As String = Request.Params(Page.postEventSourceID)
            Dim args As String = Request.Params(Page.postEventArgumentID)
            HandleCustomPostBackEvent(ctrlName, args)
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvScopeId_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvScopeId.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtScopeId.Text)) Then
            cvScopeId.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0863", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If

        If (String.IsNullOrEmpty(ConfigurationManager.AppSettings("RegexScopeId"))) Then
            cvScopeId.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0863", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If

        _scopeId = Me.SQLStringWithOutSingleQuotes(txtScopeId.Text)
        Dim regex As Regex = New Regex(ConfigurationManager.AppSettings("RegexScopeId"))
        If Not (regex.IsMatch(_scopeId)) Then
            cvScopeId.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0944", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvSerialNumber_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvSerialNumber.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtSerialNumber.Text)) Then
            cvSerialNumber.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0864", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If
        _serialNumber = Me.SQLStringWithOutSingleQuotes(txtSerialNumber.Text)
        Dim serNumsScopes As List(Of SerNumsScopes) = GetSerNumScopes(_serialNumber)
        If (serNumsScopes.Count <= 0) Then
            cvSerialNumber.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0864", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If
        _scopeProductId = serNumsScopes.OrderByDescending(Function(x) x.ProductId).FirstOrDefault().ProductId
        Dim scopeProduct As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString"), _scopeProductId)
        If (scopeProduct Is Nothing) Then
            cvSerialNumber.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0864", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If
        Dim configs As List(Of Config) = ConfigRepository.GetConfigs(ConfigurationManager.AppSettings("ConnectionString"), _scopeProductId)
        If (configs.Count <= 0) Then
            cvSerialNumber.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0864", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If
        Dim productSeriesCategoriesForSelectedSeries As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeriesAndCategory(ConfigurationManager.AppSettings("ConnectionString"), CategoryIds.SOFTWARE_OPTIONS, _productSeriesId)
        Dim productIds As List(Of Int32) = productSeriesCategoriesForSelectedSeries.Select(Function(x) x.ProductId).Distinct().ToList()
        If Not (configs.Select(Function(x) x.OptionId).Distinct().Intersect(productIds).Any()) Then
            cvSerialNumber.ErrorMessage = String.Format("*{0}", Functions.LoadI18N("SLBCA0947", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        Else
            _selectedProductId = configs.Select(Function(x) x.OptionId).Distinct().Intersect(productIds).FirstOrDefault()
        End If
        args.IsValid = True
    End Sub

    Private Sub btnValidateEntries_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnValidateEntries.Click
        cvScopeId.Validate()    ' NOTE: Needs to be ordered in this manner to trigger the correct S/N regex
        If Not (cvScopeId.IsValid) Then Return
        cvSerialNumber.Validate()
        If (cvScopeId.IsValid And cvSerialNumber.IsValid) Then
            LoadOtherOptions()
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        cvScopeId.Validate()    ' NOTE: Needs to be ordered in this manner to trigger the correct S/N regex
        If Not (cvScopeId.IsValid) Then Return
        cvSerialNumber.Validate()
        If (cvScopeId.IsValid And cvSerialNumber.IsValid) Then
            Dim productIds As List(Of Int32) = New List(Of Int32)
            For Each li As ListItem In cblOtherOptions.Items
                If (li.Selected) Then
                    If Not (productIds.Contains(Int32.Parse(li.Value))) Then productIds.Add(Int32.Parse(li.Value))
                End If
            Next
            If (productIds.Count <= 0) Then BounceAway()

            Dim sb As StringBuilder = New StringBuilder()
            For Each pid In productIds
                sb.AppendFormat("{0}:1,", ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString"), pid).ShopifyHandle)
            Next
            If (sb.Length > 0) Then
                Dim parameters As String = sb.ToString().Substring(0, sb.Length - 1)
                Dim notes As String = String.Format("{0}|{1}|{2}", Session("ContactID").ToString(), _scopeId, _serialNumber)
                Response.Redirect(String.Format("{0}/cart/{1}?email={2}&note={3}", ConfigurationManager.AppSettings("StoreBaseUrl"), parameters, Session("Email").ToString(), notes))
            Else
                BounceAway()
            End If
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("seriesid"))) Then
            BounceAway()
        End If
        Dim testVal As Int32 = 0
        If Not (Int32.TryParse(Request.QueryString("seriesid"), testVal)) Then
            BounceAway()
        End If
        _productSeriesId = testVal

        If (String.IsNullOrEmpty(Request.QueryString("groupid"))) Then
            BounceAway()
        End If
        testVal = 0
        If Not (Int32.TryParse(Request.QueryString("groupid"), testVal)) Then
            BounceAway()
        End If
        _productGroupId = testVal
    End Sub

    Private Sub BindPage()
        Session("RedirectTo") = String.Format("{0}/shopper/storeinterstitial.aspx?seriesid={1}&groupid={2}", rootDir, _productSeriesId, _productGroupId)
        If Not (ValidateUser()) Then
            Response.Redirect(rootDir + "/support/user/")
        End If
        ValidateUserNoRedir()

        Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeriesAndCategory(ConfigurationManager.AppSettings("ConnectionString"), CategoryIds.SOFTWARE_OPTIONS, _productSeriesId)
        If (productSeriesCategories.Count <= 0) Then BounceAway()

        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString"), _productSeriesId)
        If (productSeries Is Nothing) Then BounceAway()
        lblSoftware.Text = productSeries.Name
    End Sub

    Private Sub HandleCustomPostBackEvent(ByVal ctrlName As String, ByVal args As String)
        If ((String.Compare(ctrlName, txtScopeId.UniqueID, True) = 0 Or String.Compare(ctrlName, txtSerialNumber.UniqueID, True) = 0) And String.Compare(args, "OnBlur", True) = 0) Then
            If Not (String.IsNullOrEmpty(txtScopeId.Text)) Then cvScopeId.Validate()
            If Not (String.IsNullOrEmpty(txtSerialNumber.Text)) Then
                If Not (String.IsNullOrEmpty(txtScopeId.Text)) Then cvSerialNumber.Validate()
            End If
            If (cvScopeId.IsValid And cvSerialNumber.IsValid) Then
                LoadOtherOptions()
            End If
        End If
    End Sub

    Private Sub LoadOtherOptions()
        Dim sqlString As String = "SELECT p.* FROM [CONFIG] c, [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc WHERE c.PRODUCTID = @PRODUCTID AND c.OPTIONID = p.PRODUCTID AND p.PRODUCTID = psc.PRODUCT_ID AND psc.CATEGORY_ID = 12 AND p.DISABLED = 'N'"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", _scopeProductId.ToString()))
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray()).ToList()
        If (products.Count > 0) Then
            divOtherOptions.Visible = True
            cblOtherOptions.DataSource = products
            cblOtherOptions.DataTextField = "Partnumber"
            cblOtherOptions.DataValueField = "ProductId"
            cblOtherOptions.DataBind()
        End If
        For Each li As ListItem In cblOtherOptions.Items
            If (Int32.Parse(li.Value) = _selectedProductId) Then
                li.Selected = True
            End If
        Next
    End Sub

    Private Function GetSerNumScopes(ByVal serialNumber As String) As List(Of SerNumsScopes)
        Return SerNumsScopeRepository.GetSerNumsScopes(ConfigurationManager.AppSettings("ConnectionString"), serialNumber).ToList()
    End Function

    Private Sub BounceAway()
        Response.Redirect("~/")
    End Sub
#End Region
End Class