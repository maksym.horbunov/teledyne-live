﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Security.Cryptography
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports Newtonsoft.Json

Public Class Shopper_StoreWebHookOrderCreation
    Inherits Page

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Not (IsPostBack) Then
                Dim data As String = New StreamReader(Request.InputStream).ReadToEnd().ToString().Trim()
                If (String.IsNullOrEmpty(data)) Then BounceAway()
                Dim computedHash As String = GetComputedHash()
                If (String.IsNullOrEmpty(computedHash)) Then BounceAway()
                ValidateRequest(computedHash, data)

                'Dim data As String = "{""id"":820982911946154508,""email"":""jon@doe.ca"",""closed_at"":null,""created_at"":""2017-05-25T14:06:03-04:00"",""updated_at"":""2017-05-25T14:06:03-04:00"",""number"":234,""note"":null,""token"":""123456abcd"",""gateway"":null,""test"":true,""total_price"":""4790.00"",""subtotal_price"":""4780.00"",""total_weight"":0,""total_tax"":""0.00"",""taxes_included"":false,""currency"":""USD"",""financial_status"":""voided"",""confirmed"":false,""total_discounts"":""5.00"",""total_line_items_price"":""4785.00"",""cart_token"":null,""buyer_accepts_marketing"":true,""name"":""#9999"",""referring_site"":null,""landing_site"":null,""cancelled_at"":""2017-05-25T14:06:03-04:00"",""cancel_reason"":""customer"",""total_price_usd"":null,""checkout_token"":null,""reference"":null,""user_id"":null,""location_id"":null,""source_identifier"":null,""source_url"":null,""processed_at"":null,""device_id"":null,""phone"":null,""browser_ip"":null,""landing_site_ref"":null,""order_number"":1234,""discount_codes"":[],""note_attributes"":[],""payment_gateway_names"":[""visa"",""bogus""],""processing_method"":"""",""checkout_id"":null,""source_name"":""web"",""fulfillment_status"":""pending"",""tax_lines"":[],""tags"":"""",""contact_email"":""jon@doe.ca"",""order_status_url"":null,""line_items"":[{""id"":141249953214522974,""variant_id"":null,""title"":""MIL-STD-1553 Trigger and Decode Option"",""quantity"":1,""price"":""3205.00"",""grams"":0,""sku"":""WSXS-1553 TD"",""variant_title"":null,""vendor"":null,""fulfillment_service"":""manual"",""product_id"":9513640721,""requires_shipping"":true,""taxable"":true,""gift_card"":false,""name"":""MIL-STD-1553 Trigger and Decode Option"",""variant_inventory_management"":null,""properties"":[],""product_exists"":true,""fulfillable_quantity"":1,""total_discount"":""5.00"",""fulfillment_status"":null,""tax_lines"":[]}],""shipping_lines"":[ ],""billing_address"":{""first_name"":""Bob"",""address1"":""123 Billing Street"",""phone"":""555-555-BILL"",""city"":""Billtown"",""zip"":""K2P0B0"",""province"":""Kentucky"",""country"":""United States"",""last_name"":""Biller"",""address2"":null,""company"":""My Company"",""latitude"":null,""longitude"":null,""name"":""Bob Biller"",""country_code"":""US"",""province_code"":""KY""},""shipping_address"":{""first_name"":""Steve"",""address1"":""123 Shipping Street"",""phone"":""555-555-SHIP"",""city"":""Shippington"",""zip"":""K2P0S0"",""province"":""Kentucky"",""country"":""United States"",""last_name"":""Shipper"",""address2"":null,""company"":""Shipping Company"",""latitude"":null,""longitude"":null,""name"":""Steve Shipper"",""country_code"":""US"",""province_code"":""KY""},""fulfillments"":[],""refunds"":[],""customer"":{""id"":115310627314723954,""email"":""john@test.com"",""accepts_marketing"":false,""created_at"":null,""updated_at"":null,""first_name"":""John"",""last_name"":""Smith"",""orders_count"":0,""state"":""disabled"",""total_spent"":""0.00"",""last_order_id"":null,""note"":null,""verified_email"":true,""multipass_identifier"":null,""tax_exempt"":false,""phone"":null,""tags"":"""",""last_order_name"":null,""default_address"":{""id"":715243470612851245,""first_name"":null,""last_name"":null,""company"":null,""address1"":""123 Elm St."",""address2"":null,""city"":""Ottawa"",""province"":""Ontario"",""country"":""Canada"",""zip"":""K2H7A8"",""phone"":""123-123-1234"",""name"":"""",""province_code"":""ON"",""country_code"":""CA"",""country_name"":""Canada"",""default"":false}}}"
                Dim order As ShopifyOrder = JsonConvert.DeserializeObject(Of ShopifyOrder)(data)

                If (order.LineItems.Count() > 1) Then BounceAway()
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PARTNUMBER", order.LineItems.FirstOrDefault().Sku))
                If (ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString"), "SELECT p.PRODUCTID,p.PARTNUMBER,p.ShopifyHandle,psc.CATEGORY_ID FROM [PRODUCT] p, [PRODUCT_SERIES_CATEGORY] psc WHERE p.PRODUCTID = psc.PRODUCT_ID AND p.PARTNUMBER = @PARTNUMBER AND psc.CATEGORY_ID = 12", sqlParameters.ToArray()).ToList().Count <= 0) Then BounceAway()

                Dim notes As String = order.Note
                If (String.IsNullOrEmpty(notes)) Then BounceAway()
                Dim split As String() = notes.Split("|")  ' ContactId|ScopeId|Serial
                ValidateScopeId(split(1))
                ValidateSerial(split(2))
                CreateSwRequest(split(0), split(1), split(2))

            End If
        Catch ex As Exception
            'Response.Write(ex.Message.ToString() + " " + ex.InnerException.ToString())
        End Try
        Response.StatusCode = 200
        Response.End()
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub ValidateRequest(ByVal requestComputedHash As String, ByVal requestMessageBody As String)
        Dim encoding As New ASCIIEncoding()
        Dim keyByte As Byte() = encoding.GetBytes(ConfigurationManager.AppSettings("StoreWebHookKey"))
        Dim messageBytes As Byte() = encoding.GetBytes(requestMessageBody)
        Using hmacSha256 As HMACSHA256 = New HMACSHA256(keyByte)
            Dim hashMessage As Byte() = hmacSha256.ComputeHash(messageBytes)
            If Not (String.Compare(requestComputedHash, Convert.ToBase64String(hashMessage), False) = 0) Then BounceAway()
        End Using
    End Sub

    Private Function GetComputedHash() As String
        For Each hk As String In Request.Headers.Keys
            If (String.Compare(hk, "X-Shopify-Hmac-SHA256", True) = 0) Then
                Return Request.Headers.GetValues(hk.ToString()).FirstOrDefault()
            End If
        Next
        Return String.Empty
    End Function

    Private Sub BounceAway()
        Response.Redirect("~/")
    End Sub

    Private Sub ValidateScopeId(ByVal scopeId As String)
        If (String.IsNullOrEmpty(scopeId)) Then BounceAway()
        If (String.IsNullOrEmpty(ConfigurationManager.AppSettings("RegexScopeId"))) Then BounceAway()
        Dim regex As Regex = New Regex(ConfigurationManager.AppSettings("RegexScopeId"))
        If Not (regex.IsMatch(scopeId)) Then BounceAway()
    End Sub

    Private Sub ValidateSerial(ByVal serialNumber As String)
        If (String.IsNullOrEmpty(serialNumber)) Then BounceAway()
        Dim serNumsScopes As List(Of SerNumsScopes) = SerNumsScopeRepository.GetSerNumsScopes(ConfigurationManager.AppSettings("ConnectionString"), serialNumber).ToList()
        If (serNumsScopes.Count <= 0) Then BounceAway()
    End Sub

    Private Sub CreateSwRequest(ByVal contactId As Int32, ByVal scopeId As String, ByVal serialNumber As String)
        Dim swRequest As SwRequest = New SwRequest()
        swRequest.ContactId = contactId
        swRequest.ScopeId = scopeId
        swRequest.Model = " "
        swRequest.SerialNumber = serialNumber
        swRequest.CardNumber = "GENERIC CODE"
        swRequest.SendYN = "N"
        swRequest.KeymasterYN = "N"
        SwRequestRepository.InsertSwRequest(ConfigurationManager.AppSettings("ConnectionString"), swRequest)
    End Sub
#End Region
End Class

Public Class ShopifyOrder
    <JsonProperty(PropertyName:="id")>
    Public Property Id() As Long
        Get
            Return m_Id
        End Get
        Set
            m_Id = Value
        End Set
    End Property
    Private m_Id As Long

    <JsonProperty(PropertyName:="note")>
    Public Property Note() As String
        Get
            Return m_Note
        End Get
        Set
            m_Note = Value
        End Set
    End Property
    Private m_Note As String

    <JsonProperty(PropertyName:="line_items")>
    Public Property LineItems() As List(Of ShopifyLineItem)
        Get
            Return m_LineItems
        End Get
        Set
            m_LineItems = Value
        End Set
    End Property
    Private m_LineItems As List(Of ShopifyLineItem)
End Class

Public Class ShopifyLineItem
    <JsonProperty(PropertyName:="id")>
    Public Property Id() As Long
        Get
            Return m_Id
        End Get
        Set
            m_Id = Value
        End Set
    End Property
    Private m_Id As Long

    <JsonProperty(PropertyName:="sku")>
    Public Property Sku() As String
        Get
            Return m_Sku
        End Get
        Set
            m_Sku = Value
        End Set
    End Property
    Private m_Sku As String
End Class