Partial Class Shopper_TermsUSAPSG
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Purchase Terms and Conditions"
        Response.RedirectPermanent("~/terms/")
    End Sub
End Class