Imports LeCroy.Library.VBUtilities

Partial Class Shopper_TransferSession
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim guidSave As String
        'Add the session information to the database, and redirect to the
        'ASP implementation of SessionTransfer.
        guidSave = AddSessionToDatabase()
        Response.Redirect("setSession.asp?guid=" + guidSave)
    End Sub

    'This method adds the session information to the database and returns the GUID
    '  used to identify the data.
    Private Function AddSessionToDatabase() As String

        Dim strSql As String, guidTemp As String = GetGuid()
        For Each Item As String In Session.Keys
            Dim temp As String = ""
            If Not Session(Item) Is Nothing Then
                'If Session(Item).GetType Is GetType(String) Then
                temp = Session(Item).ToString
                'End If
            End If
            strSql = "INSERT INTO ASPSessionState (GUID, SessionKey, SessionValue) " + _
             "VALUES (" + SQLStringWithSingleQuotes(guidTemp) + "," + _
             SQLStringWithSingleQuotes(Item) + "," + _
             SQLStringWithSingleQuotes(temp) + ")"
            DbHelperSQL.ExecuteSql(strSql)
        Next
        Return guidTemp
    End Function
    'This method returns a new GUID as a string.
    Private Function GetGuid() As String
        Return System.Guid.NewGuid().ToString()
    End Function


End Class