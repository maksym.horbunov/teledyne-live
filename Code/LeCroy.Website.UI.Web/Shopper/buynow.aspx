﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="buynow.aspx.vb" Inherits="LeCroy.Website.Shopper_buynow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="height: 400px;">
        <div style="padding-bottom: 12px; padding-left: 30px; padding-top: 30px;">
            Thank you for your interest in <b><asp:Literal ID="litProductSeries" runat="server" /></b>. You can purchase this item in the Teledyne LeCroy Protocol Solutions store:<br /><br /><br />
            <div style="padding-left: 300px;">
                <asp:HyperLink ID="hlkLeCroy" runat="server">
                    <img src="/images/tl_weblogo_blkblue_189x30.png" alt="Teledyne LeCroy" border="0" class="logo" height="30" width="189" />
                </asp:HyperLink><br /><br /><br /><br />
                <asp:HyperLink ID="hlkLeCroy2" runat="server" Text="Teledyne LeCroy Protocol Solutions Store" />
            </div>
        </div><br /><br />
        <asp:Panel ID="pnlDistributors" runat="server" Visible="true">
            <div style="padding-bottom: 12px; padding-left: 30px; padding-top: 30px;">Or, you can choose from a distributor below:</div>
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Repeater ID="rptDistributors" runat="server">
                            <HeaderTemplate>
                                <table cellpadding="5" cellspacing="0">
                                    <tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <td align="center" valign="top">
                                    <div style="height: 120px; vertical-align: middle; width: 230px;">
                                        <asp:HyperLink ID="hlkDistributor" runat="server"><asp:Image ID="imgDistributor" runat="server" Width="160" /></asp:HyperLink>
                                    </div>
                                    <asp:HyperLink ID="hlkDistributor2" runat="server" />
                                </td>
                            </ItemTemplate>
                            <FooterTemplate>
                                    </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>