﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Shopper_buynow
    Inherits BasePage

#Region "Variables/Keys"
    Private _productSeriesId As Int32 = 0
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ValidateQueryString()
        If Not (IsPostBack) Then
            BindLeCroyStore()
            BindRepeater()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptDistributors_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDistributors.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As BuyNowDTO = CType(e.Item.DataItem, BuyNowDTO)
                Dim hlkDistributor As HyperLink = CType(e.Item.FindControl("hlkDistributor"), HyperLink)
                Dim imgDistributor As Image = CType(e.Item.FindControl("imgDistributor"), Image)
                Dim hlkDistributor2 As HyperLink = CType(e.Item.FindControl("hlkDistributor2"), HyperLink)

                hlkDistributor.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", row.Url))
                hlkDistributor.NavigateUrl = row.Url
                imgDistributor.ImageUrl = row.ImageUrl
                hlkDistributor2.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", row.Url))
                hlkDistributor2.NavigateUrl = row.Url
                hlkDistributor2.Text = row.Name
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("mseries"))) Then
            Response.Redirect("~/")
        End If
        Dim testVal As Int32 = 0
        Int32.TryParse(Request.QueryString("mseries"), testVal)
        If (testVal <= 0) Then
            Response.Redirect("~/")
        End If
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), testVal)
        If (productSeries Is Nothing) Then
            Response.Redirect("~/")
        End If
        If (String.Compare(productSeries.Disabled, "y", True) = 0) Then
            Response.Redirect("~/")
        End If
        litProductSeries.Text = productSeries.Name
        _productSeriesId = testVal
    End Sub

    Private Sub BindLeCroyStore()
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
        If Not (productSeries Is Nothing) Then
            If Not (String.IsNullOrEmpty(productSeries.ShopifyCollectionId)) Then
                hlkLeCroy.NavigateUrl = String.Format("https://store.teledynelecroy.com{0}", productSeries.ShopifyCollectionId)
            Else
                hlkLeCroy.NavigateUrl = "https://store.teledynelecroy.com/"
            End If
        Else
            hlkLeCroy.NavigateUrl = "https://store.teledynelecroy.com/"
        End If
        hlkLeCroy2.NavigateUrl = hlkLeCroy.NavigateUrl
    End Sub

    Private Sub BindRepeater()
        Dim data As List(Of BuyNowDTO) = GetData().Where(Function(x) x.ProductSeriesId = _productSeriesId).ToList()
        If (data.Count > 0) Then
            rptDistributors.DataSource = data
            rptDistributors.DataBind()
        ElseIf (_productSeriesId = 491) Then
            ' Don't redirect; at the time of adding this, T2C wasn't listed on any distributors so force LCRY store to show
            pnlDistributors.Visible = False
        Else
            Response.Redirect("~/")
        End If
    End Sub

    Private Function GetData() As IList(Of BuyNowDTO)
        Dim retVal As List(Of BuyNowDTO) = New List(Of BuyNowDTO)
        Select Case _productSeriesId
            Case 280
                Dim advisor As BuyNowDTO = New BuyNowDTO()
                advisor.ProductSeriesId = 280
                advisor.Name = "Stanley Supply &amp; Services"
                advisor.Url = "http://www.stanleysupplyservices.com/teledyne-lecroy-advisor-8482-t3-analyzer/g/23322"
                advisor.ImageUrl = "~/images/logo_external_stanley-supply-services.jpg"
                retVal.Add(advisor)

                advisor = New BuyNowDTO()
                advisor.ProductSeriesId = 280
                advisor.Name = "Mouser Electronics"
                advisor.Url = "http://www.mouser.com/new/lecroy/Teledyne-LeCroy-Advisor-T3/"
                advisor.ImageUrl = "~/images/logo_external_mouser-electronics.gif"
                retVal.Add(advisor)

                advisor = New BuyNowDTO()
                advisor.ProductSeriesId = 280
                advisor.Name = "Digi-Key Corporation"
                advisor.Url = "http://www.digikey.com/scripts/dksearch/dksus.dll?vendor=0&keywords=advisor"
                advisor.ImageUrl = "~/images/logo_external_digi-key.jpg"
                retVal.Add(advisor)
            Case 414
                Dim mercury As BuyNowDTO = New BuyNowDTO()
                mercury.ProductSeriesId = 414
                mercury.Name = "Stanley Supply &amp; Services"
                mercury.Url = "http://www.stanleysupplyservices.com/product-group.aspx?id=23947"
                mercury.ImageUrl = "~/images/logo_external_stanley-supply-services.jpg"
                retVal.Add(mercury)

                mercury = New BuyNowDTO()
                mercury.ProductSeriesId = 414
                mercury.Name = "Mouser Electronics"
                mercury.Url = "http://www.mouser.com/new/lecroy/Teledyne-LeCroy-Mercury-T2/"
                mercury.ImageUrl = "~/images/logo_external_mouser-electronics.gif"
                retVal.Add(mercury)

                mercury = New BuyNowDTO()
                mercury.ProductSeriesId = 414
                mercury.Name = "Digi-Key Corporation"
                mercury.Url = "http://www.digikey.com/product-detail/en/USB-TMS2-M01-X/USB-TMS2-M01-X-ND/4173085"
                mercury.ImageUrl = "~/images/logo_external_digi-key.jpg"
                retVal.Add(mercury)
        End Select
        Return retVal
    End Function
#End Region
End Class

Public Class BuyNowDTO
    Public ProductSeriesId As Int32
    Public Name As String
    Public Url As String
    Public ImageUrl As String
End Class