﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Shopper_campaign
    Inherits BasePage

#Region "Variables/Keys"
    Private _subscriptionCampaignId As Nullable(Of Int32) = Nothing
    Private _offerId As Nullable(Of Int32) = Nothing
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ValidateQueryString()
        If Not (IsPostBack) Then
            ProcessPage()
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("bid"))) Then
            HandleBouncedRedirect()
        End If

        Dim keyValuePair As String() = Request.QueryString("bid").ToString().Split("|")
        If Not (keyValuePair.Length = 2) Then
            HandleBouncedRedirect()
        End If

        Dim subscriptionCampaignId As Int32 = -1
        Int32.TryParse(keyValuePair(0), subscriptionCampaignId)
        If (subscriptionCampaignId <= 0) Then
            HandleBouncedRedirect()
        End If

        Dim offerId As Int32 = -1
        Int32.TryParse(keyValuePair(1), offerId)
        If (offerId <= 0) Then
            HandleBouncedRedirect()
        End If

        _subscriptionCampaignId = subscriptionCampaignId
        _offerId = offerId
    End Sub

    Private Sub ProcessPage()
        Dim subscriptionCampaign As SubscriptionCampaign = ValidateSubscriptionCampaign()
        Dim offer As Offer = ValidateOffer()

        If Not (String.IsNullOrEmpty(Session("QuickQuotePreloaded"))) Then
            Dim testVal As Boolean = False
            Boolean.TryParse(Session("QuickQuotePreloaded").ToString(), testVal)
            If (testVal) Then
                CheckIfLoggedInAndRedirect(subscriptionCampaign.CampaignId, offer.OfferPkId)
                Return
            End If
        End If

        If Not (String.IsNullOrEmpty(Session.SessionID)) Then   ' 1) Empty cart of current session
            ShopperRepository.DeleteShopperBySessionId(ConfigurationManager.AppSettings("ConnectionString").ToString(), Session.SessionID)
        End If

        Dim offerXrefs As List(Of OfferXRef) = OfferXRefRepository.GetOfferXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), offer.OfferPkId)
        Dim masterProductId As Int32 = 0
        Dim masterProductSeriesId As Int32 = 0
        For Each offerXref As OfferXRef In offerXrefs
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), offerXref.ProductFkId)
            If (product Is Nothing) Then Continue For

            If (offerXref.IsPrimary) Then
                masterProductId = offerXref.ProductFkId
                masterProductSeriesId = offerXref.ProductSeriesFkId
            End If
            Dim productSeriesId As Nullable(Of Int32) = Nothing
            Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT TOP 1 psc.PRODUCT_SERIES_ID FROM [PRODUCT_SERIES_CATEGORY] psc INNER JOIN [PRODUCT_SERIES] ps ON ps.PRODUCT_SERIES_ID = psc.PRODUCT_SERIES_ID WHERE psc.CATEGORY_ID = (SELECT pg.CATEGORY_ID FROM [PRODUCT] AS p INNER JOIN [PRODUCT_GROUP] AS pg ON p.GROUP_ID = pg.GROUP_ID WHERE p.PRODUCTID = @PRODUCTID) AND ps.DISABLED = 'n' AND psc.CATEGORY_ID IN (2,31) AND ps.PRODUCT_ID = @PRODUCTID", New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@PRODUCTID", product.ProductId.ToString())}).ToArray())
            If (productSeriesCategories.Count > 0) Then
                productSeriesId = productSeriesCategories.FirstOrDefault().ProductSeriesId
            End If
            Dim shopper As Shopper = New Shopper()
            shopper.SessionId = Session.SessionID
            shopper.ProductId = offerXref.ProductFkId
            shopper.Qty = offerXref.Quantity
            shopper.Price = Functions.GetProductPriceonProductID(offerXref.ProductFkId, masterProductId)
            shopper.DateEntered = DateTime.Now.ToString()
            shopper.PropertyGroupId = product.PropertyGroupId
            shopper.CustomerId = "0"
            shopper.MasterProductId = masterProductId
            shopper.OfferId = offer.OfferPkId
            shopper.GroupId = product.GroupId
            shopper.ProductSeriesId = masterProductSeriesId
            shopper.AddToCart = "Y"
            shopper.RefSeriesId = masterProductSeriesId
            ShopperRepository.InsertShopper(ConfigurationManager.AppSettings("ConnectionString").ToString(), shopper)    ' 2) Insert into shopper (check series -> function already exists)
        Next
        CheckIfLoggedInAndRedirect(subscriptionCampaign.CampaignId, offer.OfferPkId)
    End Sub

    Private Function ValidateSubscriptionCampaign() As SubscriptionCampaign
        Dim retVal As SubscriptionCampaign = SubscriptionCampaignRepository.GetSubscriptionCampaign(ConfigurationManager.AppSettings("ConnectionString").ToString(), _subscriptionCampaignId)
        If (retVal Is Nothing) Then
            HandleBouncedRedirect()
        End If
        Return retVal
    End Function

    Private Function ValidateOffer() As Offer
        Dim retVal As Offer = OfferRepository.GetOffer(ConfigurationManager.AppSettings("ConnectionString").ToString(), _offerId)
        If (retVal Is Nothing) Then
            HandleBouncedRedirect()
        End If
        If Not (retVal.SubscriptionCampaignFkId = _subscriptionCampaignId) Then
            HandleBouncedRedirect()
        End If
        Return retVal
    End Function

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/")
    End Sub

    Private Sub CheckIfLoggedInAndRedirect(ByVal subscriptionCampaignId As Int32, ByVal offerId As Int32)
        Session("RedirectTo") = String.Format("{0}/shopper/campaign.aspx?bid={1}", rootDir, String.Concat(subscriptionCampaignId, "|", offerId))
        Session("QuickQuotePreloaded") = True
        If (ValidateUser()) Then  ' 3) Check registration
            ShopperRepository.UpdateShopper(ConfigurationManager.AppSettings("ConnectionString").ToString(), Session.SessionID, Int32.Parse(Session("ContactID").ToString()))
            Session("CampaignID") = subscriptionCampaignId
            Session("offerid") = offerId
            Session("QuickQuotePreloaded") = Nothing
            Session.Remove("QuickQuotePreloaded")
            Response.Redirect("/Shopper/sendquote.aspx")    ' 4) Generate/send quote
        End If
    End Sub
#End Region
End Class