﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Oscilloscope_Configure_quoteCart" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript">
        function onlyNum(obj, evt) {
            var rtn;
            if (evt.shiftKey && evt.keyCode != 9) {
                evt.returnValue = false;
                return;
            }
            if ((evt.keyCode == 17) || (evt.keyCode == 18) || (evt.keyCode == 20) || (evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode == 8) || (evt.keyCode == 9) || (evt.keyCode >= 96 && evt.keyCode <= 105) || (evt.keyCode == 46))
                rtn = true;
            else
                rtn = false;

            if (!rtn) {
                evt.returnValue = false;
                obj.value = '';
            }
        }
    </script>
    <div id="categoryTop">
        <div class="content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="520" valign="top">
                        <div class="font28bluebold"><asp:Literal ID="litOne" runat="server" /></div>
                        <p><asp:Label ID="lblIntro" runat="server" /></p>
                        <h3><asp:Literal ID="litTwo" runat="server" /></h3>
                        <p><asp:Label ID="lblQuoteDetails" runat="server" /></p>
                    </td>
                    <td width="203" valign="top">
                        <div class="subNav">
                            <ul>
                                <li><a href="<%=rootDir %>/support/user/userprofile.aspx"><asp:Literal ID="litThree" runat="server" /></a></li>
                                <li><a href="quotehistory.aspx"><asp:Literal ID="litFour" runat="server" /></a></li>
                                <li><a href="#" onclick="window.print()"><asp:Literal ID="litFive" runat="server" /></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="configureBottom">
        <div class="content">
            <asp:Label ID="lb_alert" runat="server" />
            <table width="832" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <table width="612" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                                <td colspan="3"><asp:Button ID="btnAddProducts1" runat="server" Text="Add More Products" /></td>
                                <td align="center" class="cellBottom" valign="middle"><asp:Button ID="UpdateQty1" runat="server" Text="Update Cart" /></td>
                                <td align="center" class="cellBottom" valign="middle">&nbsp;</td>
                                <td align="right" class="cellBottom"><asp:Button ID="btn_remove1" runat="server" Text="Remove All" /></td>
                            </tr>
                            <asp:Label ID="lbContent" runat="server" />
                            <tr>
                                <td colspan="3"><asp:Button ID="btnAddProducts" runat="server" Text="Add More Products" /></td>
                                <td align="center" class="cellBottom" valign="middle"><asp:Button ID="UpdateQty" runat="server" Text="Update Cart" /></td>
                                <td align="center" class="cellBottom" valign="middle">&nbsp;</td>
                                <td align="right" class="cellBottom"><asp:Button ID="btn_remove" runat="server" Text="Remove All" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="cell">&nbsp;</td>
                                <td class="cell">&nbsp;</td>
                                <td align="center" class="cell">&nbsp;</td>
                                <td align="right" class="cell">&nbsp;</td>
                                <td align="right" class="cell"><asp:Label ID="totalLB" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="cellBottom" colspan="6"><asp:Label ID="lblFooter" runat="server" /></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" class="contentRightGrey" width="180">
                        <h2><asp:Literal ID="litSix" runat="server" /></h2><hr /><br />
                        <strong><asp:Literal ID="litSeven" runat="server" /></strong>
                        <div class="nextStep">
                            <table border="0">
                                <tr>
                                    <td valign="top" align="left">
                                        <asp:Button ID="btnLogIn" runat="server" Text="Log In to Submit Quote" Visible="False" />
                                        <asp:HiddenField ID="hdnIsScope" runat="server" />
                                        <asp:Button ID="SubmitQuote" runat="server" Text="Submit Quote" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:LinkButton ID="btnReg" runat="server" Visible="False"><asp:Literal ID="litEight" runat="server" /></asp:LinkButton>
                                        <asp:Label ID="lblUserData" runat="server" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr /><br /><strong><asp:Literal ID="litNine" runat="server" /></strong>
                        <div class="nextStep">
                            <asp:Literal ID="litTen" runat="server" /><br /><br />
                            <asp:Button ID="btn_so" runat="server" Text="Add More Products" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>