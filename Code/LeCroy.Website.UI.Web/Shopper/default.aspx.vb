﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Oscilloscope_Configure_quoteCart
    Inherits BasePage
    Dim sql As String = ""
    Dim ds As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Dim priceTotal As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("menuSelected") = ""
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        btnAddProducts1.Text = LoadI18N("SLBCA0004", localeId)
        UpdateQty1.Text = LoadI18N("SLBCA0789", localeId)
        btn_remove1.Text = LoadI18N("SLBCA0770", localeId)
        btnAddProducts.Text = LoadI18N("SLBCA0004", localeId)
        UpdateQty.Text = LoadI18N("SLBCA0789", localeId)
        btn_remove.Text = LoadI18N("SLBCA0770", localeId)
        btnLogIn.Text = LoadI18N("SLBCA0791", localeId)
        SubmitQuote.Text = LoadI18N("SLBCA0792", localeId)
        btn_so.Text = LoadI18N("SLBCA0004", localeId)
        litOne.Text = LoadI18N("SLBCA0052", localeId)
        litTwo.Text = LoadI18N("SLBCA0762", localeId)
        litThree.Text = LoadI18N("SLBCA0005", localeId)
        litFour.Text = LoadI18N("SLBCA0003", localeId)
        litFive.Text = LoadI18N("SLBCA0050", localeId)
        litSix.Text = LoadI18N("SLBCA0795", localeId)
        litSeven.Text = LoadI18N("SLBCA0795", localeId)
        litEight.Text = LoadI18N("SLBCA0793", localeId)
        litNine.Text = LoadI18N("SLBCA0004", localeId)
        litTen.Text = LoadI18N("SLBCA0794", localeId)

        If Not Page.IsPostBack Then
            Initial()
        End If

        If (IsScope()) Then
            SubmitQuote.Attributes.Add("OnClick", "ga('send', 'event', 'QuoteSystem', 'SubmitQuote', 'Shopper_Default', {'nonInteraction': 1});")
            btnLogIn.Attributes.Add("OnClick", "ga('send', 'event', 'QuoteSystem', 'btnLogIn', 'Shopper_Default', {'nonInteraction': 1});")
            btnReg.Attributes.Add("OnClick", "ga('send', 'event', 'QuoteSystem', 'btnReg', 'Shopper_Default', {'nonInteraction': 1});")
        End If
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Quote Cart"
    End Sub

    Private Sub Initial()
        If GetCountofProdInBasket() = 0 Then
            Response.Redirect(rootDir + "/Shopper/RequestQuote/")
        End If
        If Not Session("flgQuickQUote") Is Nothing Then
            btnAddProducts.Visible = False
        End If
        Functions.ClearSessionInShopper(Me.Context)

        Dim sql As String = ""
        Dim masterProductID As String = ""
        Dim masterProductName As String = ""
        Dim masterCount As Integer = 0
        Dim total As Single = 0
        priceTotal = 0
        Session("QuoteTotalPrice") = 0
        Session("LanguagePage") = rootDir + "/shopper/"
        Session("RedirectTo") = rootDir + "/shopper/" 'here is the name of the file itself 
        Session("flgDisabledProducts") = ""

        ValidateUserNoRedir()

        If Not Session("ContactID") Is Nothing Then
            If Session("ContactID") > 0 Then
                SubmitQuote.Visible = True
            Else
                btnReg.Visible = True
                btnLogIn.Visible = True
            End If
        Else
            btnReg.Visible = True
            btnLogIn.Visible = True
        End If

        'Check Approval status of customer and show him prices only if ApprovalStatus=true
        Dim ApprovalStatus As Boolean = CheckApprovalStatus()

        Dim introText As String = ""
        If Session("localeid") <> 1041 Then
            If Len(Session("FirstName")) > 0 Then
                introText = Session("FirstName") & ",&nbsp;"
            End If
        Else
            If Len(Session("LastName")) > 0 Then
                introText = Session("LastName") & "&nbsp;" & LoadI18N("SLBCA0160", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + _
                "&nbsp;" & LoadI18N("SLBCA0071", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "<br>"
            End If
        End If
        introText += LoadI18N("SLBCA0017", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "&nbsp;<b>" & GetCountofProdInBasket() & "</b> " + LoadI18N("SLBCA0001", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + ".<br>"
        If Len(Session("FirstName")) > 0 Then
            introText += LoadI18N("SLBCA0782", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Else
            introText += LoadI18N("SLBCA0783", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
        lblIntro.Text = introText


        'If customer click the link on his e-mail and his ApprovalStatus=true we set READ_FLAG=true 
        'and READ_DATE=Now() in TABLE SHOPPERQUOTE for this quote

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Session("QuoteFlag") = True And ApprovalStatus = True Then
            sql = "UPDATE SHOPPERQUOTE SET READ_FLAG='TRUE',READ_DATE=@READDATE WHERE QUOTEID=@QUOTEID"
            sqlParameters.Add(New SqlParameter("@READDATE", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@QUOTEID", Session("QuoteIDFromEmail").ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            Session("QuoteFlag") = False
        End If
        If Len(Session("QuoteID")) > 0 Then
            sql = "SELECT distinct QUOTEID,DATE_ENTERED FROM SHOPPERQUOTE WHERE quoteid=@QUOTEID"
            sqlParameters.Add(New SqlParameter("@QUOTEID", Session("QuoteID").ToString()))
            Dim quotes As List(Of ShopperQuote) = ShopperQuoteRepository.FetchShopperQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If (quotes.Count > 0) Then
                Dim temp As String = "'"
                temp = LoadI18N("SLBCA0784", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "&nbsp;" + quotes.FirstOrDefault().QuoteId.ToString()
                temp += "<br/>" + LoadI18N("SLBCA0785", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "&nbsp;" + quotes.FirstOrDefault().DateEntered.ToShortDateString()
                temp += "<br/>" + LoadI18N("SLBCA0786", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "&nbsp;" + quotes.FirstOrDefault().DateEntered().AddDays(30).ToShortDateString()
                lblQuoteDetails.Text = temp
            End If
        End If

        Dim content As String = ""
        Dim hasdata As Boolean = False
        content = getContentsForScope(ApprovalStatus, hasdata)
        content += getContentsForSeries(ApprovalStatus, hasdata)
        content += getContentsForGroups(ApprovalStatus, hasdata)
        If Not hasdata Then
            UpdateQty.Visible = False
        End If


        lbContent.Text = content
        If ApprovalStatus = True And priceTotal > 0 Then
            totalLB.Text = "<h3>" + LoadI18N("SLBCA0787", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "&nbsp;&nbsp;" + _
                        FormatCurrency(priceTotal, 2) + "</h3>&nbsp;"
        Else
            totalLB.Text = "&nbsp;"
        End If
        lblFooter.Text = "<strong>" + LoadI18N("SLBCA0039", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "<br><br>" & LoadI18N("SLBCA0745", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "</strong>"

        If Len(Session("ContactID")) > 0 And Len(Session("FirstName")) > 0 Then
            lblUserData.Visible = True
            lblUserData.Text = WriteUserData()
        End If
        'Session("objList" & Session("productid" & Session("seriesid").ToString()).ToString()) = Nothing
        'Session("productid" & Session("seriesid").ToString()) = Nothing
        'Session("seriesid") = Nothing
    End Sub

    Function GetProductPriceonProductID(ByVal ProdID As String, ByVal ProductQty As Integer, ByVal GroupID As String, ByVal masterProductId As Int32) As String 'pass qty for e-mail quote
        '*************************************
        'Product Pricing
        'Arguments - prodid, Quantity and Group ID
        'Returns string
        '*************************************
        Dim result As String = ""
        Dim ProductPrice As Decimal = Functions.GetProductPriceonProductID(ProdID, masterProductId)
        Dim TotalPrice As Integer = 0
        TotalPrice = (ProductQty * ProductPrice)
        If CInt(GroupID) = 90 Then
            result = "<td align='left' class='cell' valign='top'>&nbsp;</td><td align='right' class='cell'>&nbsp;</td>"
        Else
            If ProductPrice = 0 Then
                result += "<td align='left' class='cell'>&nbsp;</td><td align='right' class='cell' valign='top'>" + LoadI18N("SLBCA0010", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</td>"
            Else
                On Error Resume Next
                If ProductQty > 1 Then
                    If ProductPrice > 0 Then
                        result += "&nbsp;<td align='left' class='cell' valign='top'><font color='#007ac3'>X&nbsp;&nbsp;" + FormatCurrency(ProductPrice, 2) + "</font></td>"
                    Else
                        result += "<td align='left' class='cell'>&nbsp;</td>"
                    End If
                Else
                    result += "<td align='left' class='cell'>&nbsp;</td>"
                End If
                result += "<td align='right' class='cell' valign='top'><strong><font color='#007ac3'>&nbsp;&nbsp" + FormatCurrency(TotalPrice, 2) & "</font></strong></td>"
                priceTotal += TotalPrice
            End If
        End If
        GetProductPriceonProductID = result
    End Function

    Protected Sub UpdateQty_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateQty.Click
        If Update() Then
            Response.Redirect(rootDir + "/Shopper/")
        End If
    End Sub

    Private Function Update() As Boolean

        Dim sessionid As String
        Dim productid As String
        Session("QuoteID") = Nothing
        productid = Session("productid")
        sessionid = Me.Session.SessionID

        Dim subProductIDs As String = ""
        Dim qty As String = ""
        Dim masterIDs As String = ""
        subProductIDs = Request("subproductid").ToString()
        qty = Request("qty").ToString()
        masterIDs = Request("masterproductid").ToString()

        Dim masterProdIDArray As ArrayList = New ArrayList()
        Dim masterProdIDClicked As Boolean = False
        Dim arrayProductID() As String
        Dim arrayQty() As String
        Dim arrayMasterId() As String
        Dim dels As String
        Dim arrayDel() As String
        Dim del0 As String = ""
        Dim del1 As String = ""

        If Not subProductIDs Is Nothing And Len(subProductIDs) > 0 Then
            arrayProductID = subProductIDs.Split(",")
            arrayQty = qty.Split(",")
            arrayMasterId = masterIDs.Split(",")

            Dim i As Integer = 0
            Dim SQLStringList As ArrayList = New ArrayList()

            Dim sql As String = ""

            For i = 0 To arrayProductID.Length - 1
                If arrayQty(i).ToString().Equals("") Then
                    lb_alert.Text = "<font color=red>" + LoadI18N("SLBCA0796", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</font><br/><br/>"
                    Return False
                End If

                Try
                    Integer.Parse(arrayQty(i).ToString())
                Catch
                    lb_alert.Text = "<font color=red>" + LoadI18N("SLBCA0797", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</font><br/><br/>"
                    Return False
                End Try


            Next
            lb_alert.Text = ""
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            Dim sqlStatementsAndParameters As List(Of Tuple(Of String, List(Of SqlParameter))) = New List(Of Tuple(Of String, List(Of SqlParameter)))   ' If lists stop keeping order of insert, then this is out the door
            For i = 0 To arrayProductID.Length - 1
                del0 = arrayMasterId(i)
                del1 = arrayProductID(i)
                Dim sqlString As String = "UPDATE SHOPPER SET QTY=@QTY WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@QTY", arrayQty(i).ToString()))
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", arrayMasterId(i).ToString()))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", arrayProductID(i).ToString()))
                sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                If arrayQty(i) = "0" Then
                    If del0 = del1 Then
                        masterProdIDClicked = True
                        masterProdIDArray.Add(del1)
                    End If
                End If
            Next
            DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)

            ' This will delete any options for which user has updated the Quantity to Zero
            ' If user updates quantity of the master product to Zero then delete all the options
            ' selected for that Model.

            sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND QTY = 0 "
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            If Request("chkDel") Is Nothing Then
                dels = ""
            Else
                dels = Request("chkDel").ToString()
            End If

            If dels.Length > 0 Then
                arrayDel = dels.Split(",")

				sqlStatementsAndParameters = New List(Of Tuple(Of String, List(Of SqlParameter)))
                For i = 0 To arrayDel.Length - 1
                    Dim aa() As String = arrayDel(i).Split("*")
                    del0 = aa(0)
                    del1 = aa(1)
                    Dim sqlString As String = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND MASTERPRODUCTID = @MASTERPRODUCTID AND PRODUCTID = @PRODUCTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                    sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", del0.ToString()))
                    sqlParameters.Add(New SqlParameter("@PRODUCTID", del1.ToString()))
                    sqlStatementsAndParameters.Add(New Tuple(Of String, List(Of SqlParameter))(sqlString, sqlParameters))
                    If del0 = del1 Then
                        masterProdIDClicked = True
                        masterProdIDArray.Add(del1)
                    End If
                Next
                DbHelperSQL.ExecuteSqlTran(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlStatementsAndParameters)
            End If
            If masterProdIDArray.Count > 0 Then
                For i = 0 To masterProdIDArray.Count - 1
                    UpdateOptionsForMaster(masterProdIDArray(i))
                Next
            End If

        End If

        Return True

    End Function

    Private Function getContentsForScope(ByVal ApprovalStatus As Boolean, ByRef hasdata As Boolean) As String
        Dim sql As String = ""
        Dim Content As String = ""
        Dim ds As DataSet
        Dim dss As DataSet
        Dim dsss As DataSet
        Dim masterProductID As String = ""
        Dim masterProductName As String = ""
        Dim masterCount As Integer = 0
        Dim sessionid As String
        sessionid = Me.Session.SessionID
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        hdnIsScope.Value = "1"
        '*get all master productid for this sessionid
        sql = "SELECT DISTINCT a.masterproductid, a.productid,a.qty, b.name, b.productdescription, c.group_id, " +
                " c.group_name, c.sortid FROM SHOPPER a INNER JOIN PRODUCT b On a.masterproductid = b.productid " +
                " INNER JOIN product_group c ON b.group_id=c.group_id " +
                " WHERE a.masterproductid=a.productid and a.add_to_cart='y' AND a.group_id=@GROUPID AND sessionid=@SESSIONID order by c.sortid,group_name"
        sqlParameters.Add(New SqlParameter("@GROUPID", AppConstants.GROUP_ID_SCOPE.ToString()))
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        masterCount = ds.Tables(0).Rows.Count
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim j As Integer = 0
            masterProductID = dr("masterproductid").ToString()
            masterProductName = Me.convertToString(dr("name").ToString())
            Content += "<tr>"

            If CheckStandardOptionsExist(masterProductID) Then
                Content += "<td class='cell' colspan='8' valign='top'><h2>" + dr("group_name") + _
                "</h2><strong>" + dr("name") + "</strong><span class='title'>&nbsp;+&nbsp;" + _
                "<b>" + _
                LoadI18N("SLBCA0023", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</b></span>" + _
                "<div id='specs'>" + GetStandardOptions(masterProductID) + "</div></td>"
            Else
                Content += "<td class='cell' colspan='8' valign='top'><h2>" + dr("group_name") + _
                "</h2><strong>" + dr("name") + "</strong></td>"
            End If

            Content += "</tr>"
            Content += "<tr><td>&nbsp;</td>"
            Content += "<td class='cell' valign='top'><u><strong>" + masterProductName + "</u></strong></td>"
            Content += "<td class='cell' valign='top'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</td>"
            Content += "<td align='center' class='cell' valign='top'><input name='qty' type='text' maxlength='2' id='qty' size='1' value='"
            Content += dr("qty").ToString() + "' onKeyUp='javascript:onlyNum(this, event);'  onKeyDown='javascript:onlyNum(this,event);'/> </td>"
            hasdata = True
            If ApprovalStatus = True Then
                Content += GetProductPriceonProductID(masterProductID, CInt(dr("qty")), dr("group_id"), Int32.Parse(masterProductID))
            Else
                Content += "<td align='right' class='cell'>&nbsp;</td>"
            End If
            Content += "<td align='right' class='cell'  width='11%' valign='top'><input id='chkDel' type='checkbox' name='chkDel' text='delete' value='" + dr("masterproductid").ToString() + "*" + dr("productid").ToString() + "'>&nbsp;delete</input></td>"
            Content += "<input type='hidden' name='subproductid' id='subproductid' value='" + dr("masterproductid").ToString() + "'>"
            Content += "<input type='hidden' name='masterproductid' id='masterproductid' value='" + dr("masterproductid").ToString() + "'>"
            Content += "</tr>"
            sql = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID " +
                      "FROM SHOPPER a INNER JOIN PRODUCT_GROUP b ON a.group_id = b.group_id " +
                      " WHERE a.productid<>a.masterproductid and a.masterproductid=@MASTERPRODUCTID AND sessionid=@SESSIONID ORDER BY b.SORTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProductID))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            For Each drr As DataRow In dss.Tables(0).Rows
                Dim sqlstr As String = ""
                Dim groupID As String = ""
                Dim groupName As String = ""

                groupID = drr("GROUP_ID").ToString()
                groupName = drr("GROUP_NAME").ToString()
                Content += "<tr>"
                Content += "<td>&nbsp;</td>"
                Content += "<td class='cell' valign='top'><strong>" + groupName + "</strong></td>"
                Content += "<td class='cell'>&nbsp;</td>"
                Content += "<td align='center' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td></tr>"

                sqlstr = "SELECT DISTINCT a.masterproductid,a.productid, a.qty, b.partnumber, b.productdescription, c.group_id, " +
                " c.group_name, c.sortid FROM SHOPPER a INNER JOIN PRODUCT b ON a.productid = b.productid " +
                " INNER JOIN product_group c ON b.group_id=c.group_id" +
                " WHERE sessionid=@SESSIONID AND a.masterproductid = @MASTERPRODUCTID AND a.group_id = @GROUPID order by c.sortid,group_name"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterProductID))
                sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                dsss= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

                For Each drrr As DataRow In dsss.Tables(0).Rows
                    Content += "<tr><td>&nbsp;</td>"
                    Content += "<td class='cell' valign='top'><u><strong>" + drrr("partnumber") + "</u></strong></td>"
                    Content += "<td class='cell' valign='top'>" + Functions.GetProdDescriptionOnProdID(drrr("productid").ToString).ToString + "</td>"
                    Content += "<td align='center' class='cell' valign='top'><input name='qty' type='text' maxlength='2' id='qty' size='1' value='"
                    Content += drrr("qty").ToString() + "' onKeyUp='javascript:onlyNum(this, event);' onKeyDown='javascript:onlyNum(this,event);'/> </td>"
                    If ApprovalStatus = True Then
                        Content += GetProductPriceonProductID(drrr("productid"), CInt(drrr("qty")), drrr("group_id"), Int32.Parse(drrr("masterproductid").ToString()))
                    Else
                        Content += "<td align='right' class='cell'></td>"
                    End If
                    hasdata = True
                    Content += "<td align='right' class='cell' width='11%' valign='top'><input id='chkDel' type='checkbox' name='chkDel' text='delete' value='" + drrr("masterproductid").ToString() + "*" + drrr("productid").ToString() + "'>&nbsp;delete</input></td>"
                    Content += "<input type='hidden' name='subproductid' id='subproductid' value='" + drrr("productid").ToString() + "'>"
                    Content += "<input type='hidden' name='masterproductid' id='masterproductid' value='" + drrr("masterproductid").ToString() + "'>"
                    Content += "</tr>"
                Next
            Next
            Content += "<tr><td colspan='8'>&nbsp;</td></tr>"
        Next
        getContentsForScope = Content
    End Function

    Private Function getContentsForSeries(ByVal ApprovalStatus As Boolean, ByRef hasdata As Boolean) As String
        Dim sql As String = ""
        Dim Content As String = ""
        Dim ds As DataSet
        Dim dss As DataSet
        Dim dsss As DataSet
        Dim groupID As String = ""
        Dim seriesID As String = ""
        Dim masterProductID As String = ""
        Dim masterProductName As String = ""
        Dim masterCount As Integer = 0
        Dim sessionid As String
        sessionid = Me.Session.SessionID
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        '*get all master productid for this sessionid
        sql = "SELECT DISTINCT c.group_id, " +
                " c.group_name, c.sortid FROM SHOPPER a INNER JOIN product_group c " +
                " ON a.group_id=c.group_id WHERE a.add_to_cart='y' " +
                " and a.product_series_id > 0 " +
                " AND a.group_id  in (@GROUPID1,@GROUPID2,@GROUPID3) And sessionid=@SESSIONID order by c.sortid,group_name"
        sqlParameters.Add(New SqlParameter("@GROUPID1", AppConstants.GROUP_ID_PROTOCOL))
        sqlParameters.Add(New SqlParameter("@GROUPID2", AppConstants.GROUP_ID_APPLICATIONS))
        sqlParameters.Add(New SqlParameter("@GROUPID3", AppConstants.GROUP_ID_SERIAL_DATA))
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim j As Integer = 0
            groupID = dr("group_id")
            Content += "<tr>"
            Content += "<td class='cell' colspan='8' valign='top'><h2>" + ds.Tables(0).Rows(0)("group_name") + "</h2></td></tr>"
            sql = "SELECT DISTINCT b.product_series_id, b.NAME,b.SORTID " +
              " FROM SHOPPER a INNER JOIN PRODUCT_SERIES b " +
              " ON a.product_series_id = b.product_series_id WHERE a.group_id=@GROUPID and a.add_to_cart='y' AND sessionid=@SESSIONID ORDER BY b.SORTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            For Each drr As DataRow In dss.Tables(0).Rows
                Dim sqlstr As String = ""
                Dim groupName As String = ""
                seriesID = drr("product_series_id").ToString()
                Content += "<tr>"
                Content += "<td>&nbsp;</td>"
                Content += "<td class='cell' valign='top'><strong>" + drr("NAME") + "</strong></td>"
                Content += "<td class='cell'>&nbsp;</td>"
                Content += "<td align='center' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td></tr>"
                sqlstr = "SELECT DISTINCT a.masterproductid,a.productid, a.qty, b.partnumber, b.productdescription, c.group_id, " +
                " c.group_name, c.sortid FROM SHOPPER a INNER JOIN PRODUCT b ON a.productid = b.productid " +
                " INNER JOIN product_group c ON b.group_id=c.group_id" +
                " WHERE a.add_to_cart='y' " +
                " AND sessionid=@SESSIONID AND a.product_series_id = @PRODUCTSERIESID AND a.group_id = @GROUPID order by a.masterproductid,a.productid desc"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesID))
                sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                dsss= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

                For Each drrr As DataRow In dsss.Tables(0).Rows
                    Content += "<tr><td>&nbsp;</td>"
                    Content += "<td class='cell' valign='top'><u><strong>" + drrr("partnumber") + "</u></strong></td>"
                    Content += "<td class='cell' valign='top'>" + Functions.GetProdDescriptionOnProdID(drrr("productid").ToString).ToString + "</td>"
                    Content += "<td align='center' class='cell' valign='top'><input name='qty' type='text' maxlength='2' id='qty' size='1' value='"
                    Content += drrr("qty").ToString() + "' onKeyUp='javascript:onlyNum(this, event);' onKeyDown='javascript:onlyNum(this,event);'/> </td>"
                    If ApprovalStatus = True Then
                        Content += GetProductPriceonProductID(drrr("productid"), CInt(drrr("qty")), drrr("group_id"), Int32.Parse(drrr("masterproductid").ToString()))
                    Else
                        Content += "<td align='right' class='cell'>&nbsp;</td>"
                    End If
                    hasdata = True
                    Content += "<td align='right' class='cell'  width='11%' valign='top'><input id='chkDel' type='checkbox' name='chkDel' text='delete' value='" + drrr("masterproductid").ToString() + "*" + drrr("productid").ToString() + "'>&nbsp;delete</input></td>"
                    Content += "<input type='hidden' name='subproductid' id='subproductid' value='" + drrr("productid").ToString() + "'>"
                    Content += "<input type='hidden' name='masterproductid' id='masterproductid' value='" + drrr("masterproductid").ToString() + "'>"
                    Content += "</tr>"
                Next
            Next
            Content += "<tr><td colspan='8'>&nbsp;</td></tr>"
        Next
        getContentsForSeries = Content
    End Function

    Private Function getContentsForGroups(ByVal ApprovalStatus As Boolean, ByRef hasdata As Boolean) As String
        Dim sql As String = ""
        Dim Content As String = ""
        Dim ds As DataSet
        Dim dss As DataSet
        Dim dsss As DataSet
        Dim categoryID As String = ""
        Dim groupID As String = ""
        Dim masterProductID As String = ""
        Dim masterProductName As String = ""
        Dim masterCount As Integer = 0
        Dim sessionid As String
        sessionid = Me.Session.SessionID
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        '*get all master productid for this sessionid
        sql = "SELECT DISTINCT b.category_id, " +
                " b.category_name FROM SHOPPER a INNER JOIN product_group c ON a.group_id=c.group_id" +
                " INNER JOIN category b On b.category_id = c.category_id " +
                " WHERE  a.add_to_cart='y' and (a.product_series_id = 0 or c.category_id in (2,31)) AND sessionid=@SESSIONID order by category_name"
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim j As Integer = 0
            categoryID = dr("category_id").ToString()
            Content += "<tr>"
            Content += "<td class='cell' colspan='8' valign='top'><h2>" + dr("category_name") + "</h2></td></tr>"
            sql = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID " +
                      "FROM SHOPPER a INNER JOIN  PRODUCT_GROUP b  ON a.group_id = b.group_id " +
                      " WHERE (product_series_id=0 or b.category_id in (2,31)) " +
                      " and b.category_id=@CATEGORYID and a.add_to_cart='y' " +
                      " AND sessionid=@SESSIONID ORDER BY b.SORTID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryID))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            For Each drr As DataRow In dss.Tables(0).Rows
                Dim sqlstr As String = ""
                Dim groupName As String = ""

                groupID = drr("GROUP_ID").ToString()
                groupName = drr("GROUP_NAME").ToString()
                Content += "<tr>"
                Content += "<td>&nbsp;</td>"
                Content += "<td class='cell' valign='top'><strong>" + groupName + "</strong></td>"
                Content += "<td class='cell'>&nbsp;</td>"
                Content += "<td align='center' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td>"
                Content += "<td align='right' class='cell'>&nbsp;</td></tr>"

                sqlstr = "SELECT DISTINCT a.masterproductid,a.productid, a.qty, b.partnumber, b.productdescription, c.group_id, " +
                " c.group_name, c.sortid FROM SHOPPER a INNER JOIN PRODUCT b ON a.productid = b.productid " +
                " INNER JOIN product_group c ON b.group_id=c.group_id " +
                " WHERE (product_series_id=0 or c.category_id in (2,31)) " +
                " and a.add_to_cart='y' " +
                " AND sessionid=@SESSIONID AND a.group_id = @GROUPID order by c.sortid,group_name"
                'Response.Write(sqlstr)
                'Response.End()
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
                sqlParameters.Add(New SqlParameter("@GROUPID", groupID))
                dsss= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

                For Each drrr As DataRow In dsss.Tables(0).Rows
                    Dim canBuy As Boolean = False
                    Dim product As Product = Nothing
                    If (categoryID = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES) Then
                        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
                        If (countryCode = 208) Then
                            product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(drrr("productid").ToString()))
                            If Not (product Is Nothing) Then
                                If Not (String.IsNullOrEmpty(product.ShopifyHandle)) Then
                                    canBuy = True
                                End If
                            End If
                        End If
                    End If

                    Content += "<tr><td>&nbsp;</td>"
                    Content += "<td class='cell' valign='top'><u><strong>" + drrr("partnumber") + "</u></strong></td>"
                    Content += "<td class='cell' valign='top'>" + Functions.GetProdDescriptionOnProdID(drrr("productid").ToString).ToString + "</td>"
                    Content += "<td align='center' class='cell' valign='top'><input name='qty' type='text' maxlength='2' id='qty' size='1' value='"
                    Content += drrr("qty").ToString() + "'onKeyUp='javascript:onlyNum(this, event);'  onKeyDown='javascript:onlyNum(this,event);'/>"
                    If (canBuy) Then
                        Content += String.Format("<br /><span style=""font-size: 11pt;"">@ ${0}</span>", Functions.GetProductPriceonProductID(product.ProductId, Nothing))
                        Content += String.Format("<br /><input name=""btnBuyNow"" type=""button"" value=""Buy now"" onclick=""location.href='{0}';"" />", String.Format("https://store.teledynelecroy.com{0}", product.ShopifyHandle))
                    End If
                    Content += "</td>"
                    If ApprovalStatus = True Then
                        Content += GetProductPriceonProductID(drrr("productid"), CInt(drrr("qty")), drrr("group_id"), Int32.Parse(drrr("masterproductid").ToString()))
                    Else
                        Content += "<td align='right' class='cell'>&nbsp;</td>"
                    End If
                    hasdata = True
                    Content += "<td align='right' class='cell'  width='11%' valign='top'><input id='chkDel' type='checkbox' name='chkDel' text='delete' value='" + drrr("masterproductid").ToString() + "*" + drrr("productid").ToString() + "'>&nbsp;delete</input>"
                    Content += "</td>"
                    Content += "<input type='hidden' name='subproductid' id='subproductid' value='" + drrr("productid").ToString() + "'>"
                    Content += "<input type='hidden' name='masterproductid' id='masterproductid' value='" + drrr("masterproductid").ToString() + "'>"
                    Content += "</tr>"
                    Content += "<tr><td colspan=""6"">&nbsp;</td></tr>"
                Next
            Next
            Content += "<tr><td colspan='8'>&nbsp;</td></tr>"
        Next
        getContentsForGroups = Content
    End Function

    Function CheckStandardOptionsExist(ByVal ProdID As String) As Boolean
        'Open recordset
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", ProdID))
        Dim strSQL As String = String.Format("Select optionid from CONFIG where PRODUCTID=@PRODUCTID and STD='y' and OPTIONID not in ({0})", disabledProductContent.Item1)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            CheckStandardOptionsExist = True
        Else
            CheckStandardOptionsExist = False
        End If
    End Function

    Function GetStandardOptions(ByVal ProdID As String) As String
        Dim result As String
        If Len(ProdID) > 0 Then
            result = "<table><tr><td colspan='2' valign='top'><b>" +
            LoadI18N("SLBCA0009", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "</nobr></b></td></tr>"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTID", ProdID))
            sqlParameters.Add(New SqlParameter("@bStd", "y"))
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM V_PRODUCT_OPTIONS WHERE PRODUCTID=@Productid and STD = @bStd order by PRODUCT_SORTID", sqlParameters.ToArray())
            For Each dr As DataRow In ds.Tables(0).Rows
                result += "<tr><td width='50px' valign='top'>"
                Dim temp As String = dr("qty").ToString
                If [Object].Equals(temp, System.DBNull.Value) Then
                    temp = ""
                End If

                If temp > "1" Then
                    result += String.Format("({0}) X", dr("qty"))
                Else
                    result += "&nbsp;"
                End If
                result += String.Format("</td><td valign='top'>{0}</td></tr>", Functions.GetProdDescriptionOnProdID(dr("optionid").ToString()).ToString())
            Next
            result += "</table>"
        Else
            result = ""
        End If
        Return result
    End Function

    Protected Sub btn_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_remove.Click
        Dim sql As String
        sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

        Functions.ClearSessionInShopper(Me.Context)
        Session("QuoteID") = Nothing
        Session("flgQuickQUote") = Nothing

        Response.Redirect(rootDir & "/Shopper/RequestQuote/")
    End Sub

    Protected Sub SubmitQuote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitQuote.Click
        Session("flgQuickQUote") = Nothing
        If Update() Then
            PrepareCart()
            Response.Redirect("/Shopper/sendquote.aspx")
        Else
            Initial()
        End If
    End Sub

    Protected Sub btnLogIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogIn.Click
        PrepareCart()
        Session("RedirectTo") = rootDir + "/Shopper/sendquote.aspx"
        Response.Redirect(rootDir + "/Support/User/")
    End Sub

    Protected Sub PrepareCart()
        Dim sql As String
        sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID AND ADD_TO_CART = 'n' "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        Functions.ClearSessionInShopper(Me.Context)
        Session("QuoteID") = Nothing
        Session("netSessionID") = Me.Session.SessionID
        Session("flgQuickQUote") = Nothing
    End Sub

    Protected Sub btnAddProducts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProducts.Click
        Session("QuoteID") = Nothing
        Response.Redirect(rootDir + "/Shopper/RequestQuote/")
    End Sub

    Protected Sub btnReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReg.Click
        PrepareCart()
        Session("RedirectTo") = rootDir + "/shopper/default.aspx"
        Response.Redirect(rootDir + "/Support/User/UserRegisterForm.aspx")
    End Sub

    Private Function WriteUserData() As String
        Dim rlt As StringBuilder = New StringBuilder()
        rlt.Append("<table width=""200"" cellpadding=0>")
        rlt.Append("<tr>")
        rlt.Append("<td>")
        rlt.Append("        </b>")
        rlt.Append("		<br><strong>" + Session("FirstName") + " " + Session("LastName") + "</strong><br>")
        rlt.Append(Session("Company") + "<br>")
        rlt.Append(Session("Address") + "<br>")
        If Len(Session("Address2")) > 0 Then
            rlt.Append(Session("Address2") + "<br>")
        End If
        rlt.Append(Session("City") + ", &nbsp;" + Session("State") + ",&nbsp;" + Session("Zip") + "<br>")
        rlt.Append(Session("Country") + "<br>")
        rlt.Append(Session("Phone") + "<br>")
        rlt.Append(Session("Email") + "<br>")
        rlt.Append("		")
        rlt.Append("<br /><a href='").Append(rootDir).Append("/support/user/userprofile.aspx'> ")
        rlt.Append("<strong>" + LoadI18N("SLBCA0788", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</strong> </a>")
        rlt.Append("	</td>")
        rlt.Append("</tr>")
        rlt.Append("</table>")

        WriteUserData = rlt.ToString()

    End Function

    Protected Sub btnAddProducts1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProducts1.Click
        Session("QuoteID") = Nothing
        Response.Redirect(rootDir + "/Shopper/RequestQuote/")
    End Sub

    Protected Sub btn_remove1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_remove1.Click
        Dim sql As String
        sql = "DELETE FROM SHOPPER WHERE SESSIONID = @SESSIONID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", Me.Session.SessionID.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

        Functions.ClearSessionInShopper(Me.Context)
        Session("QuoteID") = Nothing
        Session("flgQuickQUote") = Nothing

        Response.Redirect(rootDir & "/Shopper/RequestQuote/")
    End Sub

    Protected Sub UpdateQty1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateQty1.Click
        If Update() Then
            Response.Redirect(rootDir + "/Shopper/")
        End If
    End Sub

    Protected Sub btn_so_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_so.Click
        Session("QuoteID") = Nothing
        Response.Redirect(rootDir + "/Shopper/RequestQuote/")
    End Sub

    Private Function IsScope() As Boolean
        If Not (String.IsNullOrEmpty(hdnIsScope.Value)) Then
            If (String.Compare(hdnIsScope.Value, "1", True) = 0) Then
                Return True
            End If
        End If
        Return False
    End Function
End Class