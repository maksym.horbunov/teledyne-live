<% 

'*************************************
'Check if Product is a valid product
'rev 1 AC 1/30/00
'Revision 2 Kate Kaplan 1/20/2004
'Revision 3 Kate Kaplan 2/28/2006
'rev 4 - Kate Kaplan - 7/31/2009 (Added Voyager)
'arguments (productid)
'Returns Boolean
'*************************************
function CheckProductisScope(prodid)
		'If user manually changes productid in QueryString do not allow them to view product
		if not IsNumeric(prodid) then
			CheckProductisScope = false
			exit Function
		end if
		
		'Or if productid does not exist
		if len(prodid) = 0 then
			CheckProductisScope = false
			exit Function
		else
			'Get all Property Groups and check to see if product is valid
			Set rsProductIsScope = Server.CreateObject("ADODB.Recordset")
			Set cmdProductIsScope=server.CreateObject("ADODB.COmmand")
			strSQL = "SELECT PROPERTYGROUPID FROM PRODUCT WHERE PRODUCTID = " & prodid 
			cmdProductIsScope.CommandText=strSQL
			cmdProductIsScope.ActiveConnection=dbconnstr()
			cmdProductIsScope.CommandType=1'TEXT
			'rsProductIsScope.open strSQL, dbconnstr()
			set rsProductIsScope=cmdProductIsScope.Execute
			if not rsProductIsScope.EOF then
				'fix to be simpler
				if rsProductIsScope("PROPERTYGROUPID") = "5" or rsProductIsScope("PROPERTYGROUPID") = "28" or rsProductIsScope("PROPERTYGROUPID") = "29" or rsProductIsScope("PROPERTYGROUPID") = "30" or rsProductIsScope("PROPERTYGROUPID") = "31" or rsProductIsScope("PROPERTYGROUPID") = "32" or rsProductIsScope("PROPERTYGROUPID") = "43" or rsProductIsScope("PROPERTYGROUPID") = "42" or rsProductIsScope("PROPERTYGROUPID") = "46"  or rsProductIsScope("PROPERTYGROUPID") = "59" or rsProductIsScope("PROPERTYGROUPID") = "81" or rsProductIsScope("PROPERTYGROUPID") = "65" or rsProductIsScope("PROPERTYGROUPID") = "66" or rsProductIsScope("PROPERTYGROUPID") = "32" or rsProductIsScope("PROPERTYGROUPID") = "43" or rsProductIsScope("PROPERTYGROUPID") = "42" or rsProductIsScope("PROPERTYGROUPID") = "46"  or rsProductIsScope("PROPERTYGROUPID") = "59" or rsProductIsScope("PROPERTYGROUPID") = "81" or rsProductIsScope("PROPERTYGROUPID") = "65" or rsProductIsScope("PROPERTYGROUPID") = "60" then
					CheckProductisScope = True
					exit function
				else
					CheckProductisScope = False
					exit Function
				end if
			else
				'if product doesnt exist -redirect back to scopes page
				CheckProductisScope = false
				exit Function
			end if
			rsProductIsScope.Close
			set rsProductIsScope = nothing
		end if
end function

'***********************************************
'Receives the Category name based on CategoryID
'AC 1/30/00
'arguments (categoryid)
'Returns String
'***********************************************
function GetProductCategoryNameonID(catid)
	Set rsCat = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT CATEGORYNAME FROM PRODUCTCATEGORY WHERE CATEGORYID = '" & catid & "'"
	rsCat.Open strSQL, dbconnstr()
		if not rsCat.EOF then
			GetProductCategoryNameonID = rsCat("CATEGORYNAME")
		else
			GetProductCategoryNameonID = ""
		end if
	rsCat.Close
	Set rsCat = Nothing	
end function

'*************************************
'List all product specifications
'AC 1/30/00
'Modified by KK 1/23/2004 (Store procedure)
'Modified by KK 10/25/2005 (black header, no arrows)
'arguments (productid)
'Returns String
'*************************************
sub GetSpecifications(intProdID)
	'Get all properties for product
	Set rsProp = Server.CreateObject("ADODB.Recordset")
	Set cmdProp=server.CreateObject("ADODB.COmmand")
	cmdProp.CommandText="sp_GetSpecifications"
	cmdProp.ActiveConnection=dbconnstr()
	cmdProp.CommandType=4'STORE PROCEDURE
	'create Parameter
	set ParamPRODUCTID=cmdProp.CreateParameter("@PRODUCTID",3,1)'3 adIntereg, 1 adParamInput
	cmdProp.Parameters.Append ParamPRODUCTID
	cmdProp.Parameters("@PRODUCTID")= intProdID
	set rsProp=cmdProp.Execute
	if not rsProp.EOF then
	'Create Specifications Table
			Response.Write "<table border=0 bgcolor=""#ffffff"" cellpadding=2 cellspacing=0>"
			Response.Write "<tr bgcolor=#000000>"
			Response.Write "<td valign=top colspan=3><font face=Verdana,Arial color=#ffffff size=1><b>" & LoadI18N("SLBCA0015",localeID) & "</nobr></b></font></td>"
			Response.Write "</tr>"
			While not rsProp.EOF
				if len(rsProp("Value"))>0 then
				'Show Icon
				Response.Write "<tr>"
				Response.Write "<td valign=top class=PropNameTitle>"
				Response.Write "&nbsp;"
				Response.Write "</td>"
			
				'List Property Names
				Response.Write "<td valign=top class=PropNameTitle>"
				Response.Write "<font face=Verdana,Arial size=1><b>"
				Response.Write translation("PROPERTY","PROPERTYNAME","PropertyID",rsProp("PropertyID"))
				Response.Write ":</b></font>"
				Response.Write "</td>"
			
				'List Property Values
				Response.Write "<td valign=top class=PropNameTitle>"
				Response.Write "<font face=Verdana,Arial size=1>" & rsProp("VALUE") & "</font>"
					
				'Look for "href" hyperlink in string and display icon
				If instr(rsProp("VALUE"),"href") then
					posofhref = instr(1,rsProp("VALUE"),">")
					hrefvalue = left(rsProp("VALUE"),posofhref)
					Response.Write hrefvalue
					Response.Write "<img src=/shopper/images/ZoomWindow.gif border=0 alt=" & rsProp("PROPERTYNAME") & "></a>"
				End if
				Response.Write "</td>"
				Response.Write "</tr>"
			
				'Seperator Lines
				Response.Write "<tr>"
				Response.Write "<td colspan=3>"
				Response.Write "<IMG SRC=""images/DottedLine250.gif"" width=""250"" alt=""line"">"
				Response.Write "</td>"
				Response.Write "</tr>"
			end if						
			rsProp.MoveNext
		wend				
	Response.Write "</table>"
	end if
end sub

'*************************************
'View Product Thumbnail either full resolution or thumbnail
'AC 1/30/00
'arguments (imagename,height,boolean(y/n))
'Returns String
'Change to function and return image file and extension only
'*************************************
sub DisplayThumb(strImage,Height,bThumb)
	
		if len(strImage ) > 0 then
			fn = strImage
			pos = instr(fn, ".")
			fname = mid(fn, 1, pos - 1) 
			fext = mid(fn, pos)
			
			if bThumb = "y" then
				'Thumbnail
				Response.Write "<img border=0 src=https://teledynelecroy.com/images/e-catalog/ProductImages/" & fname & "_thumb" & fext & " height=" & height & ">"
			else
				'Full Resolution 300 pix width
				Response.Write "<img border=0 src=https://teledynelecroy.com/images/e-catalog/ProductImages/" & fname & fext & " height=" & height & ">"
			end if
			
			'Add extra space below thumbnail
			Response.Write "<br><br>" & lbr
		end if
	
end sub	
	
	
'*************************************
'Product Info
'AC 1/30/00
'modified by KK 1/9/2002
'modified by KK 1/20/2004
'modified by KK 4/10/2007 (added PRODUCTREMARK)
'arguments (prodid,dbfieldid)
'Returns String
'Change to function
'*************************************
function GetProdInfo(prodid,arrayid)
	if len(prodid)>0 then
			Set rsProdInfo = Server.CreateObject("ADODB.Recordset")
			'strSQL = "SELECT PARTNUMBER, PRODUCTDESCRIPTION, PROPERTYGROUPID, URL ,URL_1031, URL_1041, PRODUCTREMARK FROM PRODUCT WHERE PRODUCTID ='" & prodid  & "'"
			Set cmdProdInfo=server.CreateObject("ADODB.COmmand")
			cmdProdInfo.CommandText="sp_productinfo"
			cmdProdInfo.ActiveConnection=dbconnstr()
			cmdProdInfo.CommandType=4'STORE PROCEDURE
			'create Parameter
			set ParamPRODUCTID=cmdProdInfo.CreateParameter("@PRODUCTID",3,1)'3 adIntereg, 1 adParamInput
			cmdProdInfo.Parameters.Append ParamPRODUCTID
			cmdProdInfo.Parameters("@PRODUCTID")= prodid
			set rsProdInfo=cmdProdInfo.Execute
			'rsProdInfo.Open strSQL, dbconnstr()
			if not rsProdInfo.EOF then
				'Create Array
				Dim prodarray(1)
				proddata = Array(rsProdInfo.Fields(0),rsProdInfo.Fields(1),rsProdInfo.Fields(2),rsProdInfo.Fields(3),rsProdInfo.Fields(4),rsProdInfo.Fields(5),rsProdInfo.Fields(6))
				GetProdInfo = proddata(arrayid)
			end if
			rsProdInfo.Close
			set rsProdInfo = nothing
	end if
end function

'*************************************
'Product Group Name On PRODUCTGROUPID
'AC 1/30/00
'Changed YK 5/7/01
'arguments prodid
'Returns String
'*************************************
		
function GetProductGroupNameonProdGroupID(prodgroupid)
	Set rsPropGroupName = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT PROPERTYGROUPNAME FROM PROPERTYGROUP WHERE PROPERTYGROUPID = '" & prodgroupid & "'"
	rsPropGroupName.Open strSQL, dbconnstr()
	if not rsPropGroupName.EOF or BOF then
	rsPropGroupName.MoveFirst
		GetProductGroupNameonProdGroupID = rsPropGroupName("PROPERTYGROUPNAME")
	end if
end function
'*************************************
'ALL Products in Property Group
'AC 1/30/00
'arguments prodgroupid
'*************************************
	
sub GetAllProdinProdGroup(prodgroupid)
'Check Approval status of customer and show him prices only if ApprovalStatus=true
	ApprovalStatus=CheckApprovalStatus()

	Set rsProd = Server.CreateObject("ADODB.Recordset")
	if len(GetDisabledProducts)>0 then
		strSQL = "SELECT PRODUCTID, PARTNUMBER, PRODUCTDESCRIPTION, URL FROM PRODUCT WHERE PRODUCTID not in (" & GetDisabledProducts & ") and PROPERTYGROUPID = " & prodgroupid 
	else
		strSQL = "SELECT PRODUCTID, PARTNUMBER, PRODUCTDESCRIPTION, URL FROM PRODUCT WHERE  PROPERTYGROUPID = '" & prodgroupid & "'"
	end if
	'Response.Write strSQL
	rsProd.Open strSQL, dbconnstr()
	if not rsProd.EOF or BOF then
	rsProd.MoveFirst
	Do While not rsProd.EOF
			
		if CheckProductinBasket(rsProd("PRODUCTID")) then
			Response.Write "<img src=/nav/images/icons/SmallArrowRightGreen.gif border=0>"
		else
			Response.Write "<img src=/nav/images/icons/SmallArrowRightGreen.gif border=0>"
		end if

		Response.write "<nobr><font face=""Verdana,Arial""  size=""1"" color=""Red""><b>" &  checkproductisnew(rsProd("PRODUCTID")) & "</b></font>"
		if ApprovalStatus=true then
			if GetProductPriceonProductID(rsProd("PRODUCTID"))=0 then
				Response.Write "&nbsp;<font face=Verdana,Arial size=1 color=""DodgerBlue""><a href=/tm/Products/ViewProduct.asp?prodid=" & rsProd("PRODUCTID") & ">" & rsProd("PARTNUMBER") & "</a> - " & LoadI18N("SLBCA0010",localeID)& "</font>"
			else
                Response.Write "&nbsp;<font face=Verdana,Arial size=1 color=""DodgerBlue""><a href=/tm/Products/ViewProduct.asp?prodid=" & rsProd("PRODUCTID") & ">" & rsProd("PARTNUMBER") & "</a> - " & FormatCurrency(GetProductPriceonProductID(rsProd("PRODUCTID")),2)& "</font>"
			end if	
		else
			Response.Write "&nbsp;<font face=Verdana,Arial size=1 color=""DodgerBlue""><a href=/tm/Products/ViewProduct.asp?prodid=" & rsProd("PRODUCTID") & ">" & rsProd("PARTNUMBER") & "</a></font>"
		end if		

			
		Response.Write "</nobr><br>"
		'if Session("localeid")=1033 then
		'	Response.Write "<font face=Verdana,Arial size=1 color=Gray>" & rsProd("PRODUCTDESCRIPTION") & "</font><br>"
		'else
			Response.Write "<font face=Verdana,Arial size=1 color=Gray>" & translation("PRODUCT","PRODUCTDESCRIPTION","ProductID",rsProd("ProductID")) & "</font><br>"
		'end if
		'Response.Write rsProd("PARTNUMBER")
	rsProd.MoveNext
	Loop
	end if
end sub
	
'*************************************
'Check if product is in shoppers basket
'AC 1/30/00
'arguments (prodid)
'Returns boolean
'*************************************	
function CheckProductinBasket(prodid)
	Set rs = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID = '" & prodid & "' AND SESSIONID = '" & Session("netSessionID") & "'"
	rs.Open strSQL, dbconnstr()
	if not rs.EOF or BOF then
		rs.MoveFirst
		Do While not rs.EOF or BOF
			if Cint(rs("PRODUCTID")) = Cint(prodid) then
				CheckProductinBasket = True
			else
				CheckProductinBasket = False
			end if
		rs.MoveNext
		Loop	
		InBasket = False
		rs.Close
		set rs = Nothing
	end if
end function

	
'sub GetProductInfoonPartNumber(partnumber)
'	Set rsProdInfo = Server.CreateObject("ADODB.Recordset")
'	strSQL = "SELECT PRODUCTDESCRIPTION FROM PRODUCT WHERE PARTNUMBER = '" & partnumber & "'"
'	rsProdInfo.Open strSQL, dbconnstr()
'	if not rsProdInfo.EOF or BOF then
'	rsProdInfo.MoveFirst
	'Do While not rsProdName.EOF
'		Call FontBegin(rsProdInfo("PRODUCTDESCRIPTION"))
'		Call FontEnd()
	'rsProdName.MoveNext
	'Loop
'	end if
'end sub
	
	
'*************************************
'Show product file attachments
'AC 1/30/00
'arguments (prodid,widthinpixels)
'Returns string
'*************************************	
sub GetProductAttachmentonProdID(intProdID,intWidth)
	'on error resume next
	Set rsAttach = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT ATTACHEDFILE.FILEID as fid," & _
				"ATTACHEDFILE.FILENAME AS fname, " & _ 
				"ATTACHEDFILE.FILEDESCRIPTION AS fdesc, " & _
			    "ATTACHEDFILE.ICONID AS iconid, " & _
			    "ICON.ICONFILENAME AS fnameicon, " & _
			    "ATTACHEDFILE.PRODUCTID AS pid " & _
				"FROM ATTACHEDFILE INNER JOIN " & _
			    "ICON ON ATTACHEDFILE.ICONID = ICON.ICONID " & _
				"WHERE (ATTACHEDFILE.PRODUCTID = '" & intProdID & "')"
	rsAttach.Open strSQL, dbconnstr()
	'Response.Write strSQL
	if not rsAttach.EOF or BOF then
		Response.Write "<table cellpadding=0 cellspacing=0 border=0>"
		rsAttach.MoveFirst
		Do While not rsAttach.EOF
			Response.Write "<tr>"
			Response.Write "<td>"
			'Display zoomed picture within frame if current page is ViewProduct.asp
			if instr(1,Request.ServerVariables("SCRIPT_NAME"),"ViewProduct.asp") > 0 then
				Response.Write "<a href=/images/E-Catalog/ProductAttachments/" & intProdID & "/" & rsAttach("fname") & ">"
			else
				'Check to see if it's a PDF file
				if instr(1,rsAttach("fname"),"pdf") > 0 then
					Response.Write "<a href=/images/E-Catalog/ProductAttachments/" & intProdID & "/" & rsAttach("fname") & ">"
				else
					Response.Write "<a href=Javascript:openWin('/shopper/Includes/ZoomPicture.asp?image=/images/E-Catalog/ProductAttachments/" & intProdID & "/" & rsAttach("fname") & "');>"
				end if
			end if
			Response.Write "<img border=1 src=/images/E-Catalog/images/" & rsAttach("fnameicon") & " width=" & intWidth & " vspace=5>"
			Response.Write "</a>"
			Response.Write "</td>"
			Response.Write "<td><img src=/shopper/images/1pixspacer.gif width=10>"
			Response.Write "</td>"
			Response.Write "<td>"
			Response.Write "<font face=Verdana,Arial size=1 color=Gray>"
			'if Session("localeid")=1033 then
			'	Response.write  rsAttach("fdesc")
			'else
				Response.Write translation("ATTACHEDFILE","FILEDESCRIPTION","fILEID",rsAttach("fid"))
			'end if
			Response.Write "</font></td>"
			Response.Write "</tr>"
			Response.Write "<tr><td height=1 bgcolor=Gray colspan=3><img src=/shopper/images/1pixspacer.gif height=1></td></tr>"
		rsAttach.MoveNext
		Loop
		Response.Write "</table>"
	end if
end sub

'*************************************
'Calculate Total Basket Price
'AC 1/30/00
'Arguments - none
'Returns integer
'*************************************	
function CalculateTotalBasket ()
	Set rs = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT SUM(BAANPRICE.[Sales price]) AS Total " &_
				"FROM BAANPRICE INNER JOIN " &_
				"SHOPPER ON BAANPRICE.[PRODUCT ID] = SHOPPER.PRODUCTID " &_
				"WHERE (SHOPPER.SESSIONID = '" & Session("netSessionID") & "')"
				rs.Open strSQL, dbconnstr()
		if not rs.EOF then
			rs.MoveFirst
			if not rs("Total")="" then
				CalculateTotalBasket = FormatCurrency(rs("Total"),2)
			end if
		end if
		rs.Close
		set rs = nothing
end function
	
	
'*************************************
'Show recommended (alternative) products
'AC 1/30/00
'Arguments - prodid,boolean(y,n)
'Returns string
'*************************************
sub GetAlternativeProductsVert(productid,shwCheckbox)

'Select 10 top products for this product and 
'Open recordset rsTopProd

	Set rsAlt = Server.CreateObject("ADODB.Recordset")

	'if we have product between 10 top products we don't need to show it as recommended product	
	if len(GetDisabledProducts)>0 then
			strSQL =	"SELECT ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT AS AltProdID, " &_
						"ALTERNATIVEPRODUCTS.ORIGINALPRODUCT, " &_
						"PRODUCT.PRODUCTDESCRIPTION, PRODUCT.PARTNUMBER, " &_
						"PRODUCT.IMAGEFILENAME, " &_
						"PRODUCT.PROPERTYGROUPID " &_
						"FROM ALTERNATIVEPRODUCTS INNER JOIN " &_
						"PRODUCT ON " &_
						"ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT = PRODUCT.PRODUCTID " &_
						"WHERE (ALTERNATIVEPRODUCTS.ORIGINALPRODUCT = '" & productid & "') and PRODUCT.PRODUCTID not in (" & GetDisabledProducts & ")"
	else
			strSQL =	"SELECT ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT AS AltProdID, " &_
						"ALTERNATIVEPRODUCTS.ORIGINALPRODUCT, " &_
						"PRODUCT.PRODUCTDESCRIPTION, PRODUCT.PARTNUMBER, " &_
						"PRODUCT.IMAGEFILENAME, " &_
						"PRODUCT.PROPERTYGROUPID " &_
						"FROM ALTERNATIVEPRODUCTS INNER JOIN " &_
						"PRODUCT ON " &_
						"ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT = PRODUCT.PRODUCTID " &_
						"WHERE (ALTERNATIVEPRODUCTS.ORIGINALPRODUCT = '" & productid & "')"
	end if
			rsAlt.Open strSQL, dbconnstr()
			'if not rsAlt.EOF or BOF then
				if not rsAlt.EOF or BOF then
					'Move to configurescope page
					Response.Write "<br><font face=Verdana,Arial size=3 color=DodgerBlue><b>Recommended Products</b></font>"
					'Response.Write "<font face=Verdana,Arial color=DodgerBlue size=2><b>Related Products & Accessories</b></font><br>"
					rsAlt.MoveFirst
					Do While not rsAlt.EOF	
							Response.Write "<br>"
								 
							'Display Checkbox
							if shwCheckbox = "1" then
								Response.Write "<input type=""checkbox"" name=" & rsAlt("AltProdID") & " value=" & rsAlt("AltProdID") & ">"
							end if
							
							'Display Hyperlink
							Hyperlink = "WindowFrame.asp?" & "prodid=" & rsAlt("AltProdID")	
							Response.Write "<A HREF=""#"" onClick=""window.open('/shopper/WindowFrame.asp?prodid=" & rsAlt("AltProdID") & "','thewindow','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
							Response.Write "<font face=Verdana,Arial size=1><b>" & rsAlt("PARTNUMBER") & "</b></font><br>"
							Response.Write "</a>"
							Response.Write "<font face=Verdana,Arial size=1 color=Gray>" & rsAlt("PRODUCTDESCRIPTION") & "</font><br>"
							'Call DisplayThumbonProdID(rsAlt("AltProdID"),50,"y")		
					rsAlt.MoveNext
					Loop
				else
					Response.Write ""
				end if
				rsAlt.Close
				set rsAlt = nothing
				rsTopProd.Close
				set rsTopProd = nothing
end sub	
	
	
'*************************************
'Display Product Thumbnail
'AC 1/30/00
'Arguments - prodid,heightinpixels,boolean(y/n)
'Returns string
'*************************************
sub DisplayThumbonProdID(ProdID,Height,bThumb)
	Set rsProdImage = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT IMAGEFILENAME, PRODUCTDESCRIPTION FROM PRODUCT WHERE (PRODUCTID = '" & ProdID & "')"
	rsProdImage.Open strSQL, dbconnstr()
	if rsProdImage.EOF then
		Response.Write ""
	else
		rsProdImage.MoveFirst
		'Response.Write rsProdImage("IMAGEFILENAME")
		if len(trim(rsProdImage("IMAGEFILENAME"))) > 0 then
			fn = rsProdImage("IMAGEFILENAME")
			pos = instr(fn, ".")
			fname = mid(fn, 1, pos - 1) 
			fext = mid(fn, pos)
			
			if bThumb = "y" then
				Response.Write "<img vspace=5 hspace=5 border=0 src=""/images/e-catalog/ProductImages/" & fname & "_thumb" & fext & " height=" & height & " vspace=5 hspace=5>"
			elseif bThumb = "n" then
				Response.Write "<img hspace=5 vspace=5 border=0 alt='" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",ProdID) & ".'  src=/images/e-catalog/ProductImages/" & fname & fext & " height=" & height & " vspace=5 hspace=5>"
			else
				'Response.Write "<a href=ViewProduct.asp?prodid=" & ProdID & "&zoom=y><img hspace=5 vspace=5 border=0 alt='" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",ProdID) & ".  " & Chr(13) & Chr(10) & "' TITLE='" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",ProdID) & Chr(13) & Chr(10) & "' src=/images/e-catalog/ProductImages/" & fname & fext & " height=" & height & " vspace=5 hspace=5></a>"
				Response.Write "<img hspace=5 vspace=5 border=0 alt='" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",ProdID) & ".  " & Chr(13) & Chr(10) & "' TITLE='" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",ProdID) & Chr(13) & Chr(10) & "' src=/images/e-catalog/ProductImages/" & fname & fext & " height=" & height & " vspace=5 hspace=5>"
			end if
		end if
	end if
	
end sub
	
	
'*************************************
'Product Pricing
'Rev 1 - AC 1/30/00
'Rev 2 - KK 1/22/2004 (store procedures)
'Rev 3 - KK 4/5/2006 (UWB Promo)
'Rev 4 - KK 10/31/2006 (WJ Promo)
'Rev 5 - KK 12/1/2006 (UW002MAA-X)
'Arguments - prodid
'Returns string
'*************************************
function GetProductPriceonProductID(ProdID)'pass qty for e-mail quote
	if len(ProdID)>0 then
		
		'if cint(ProdID)=1315 then
		'	GetProductPriceonProductID=2500
		'elseif cint(ProdID)=1314 then
		'	GetProductPriceonProductID=4000
		'elseif cint(ProdID)=1263 then
		'	GetProductPriceonProductID=3390
		'elseif cint(ProdID)=1267 then
		'	GetProductPriceonProductID=2890
		'elseif cint(ProdID)=1262 then
		'	GetProductPriceonProductID=2990
		'elseif cint(ProdID)=1266 then
		'	GetProductPriceonProductID=2690
	'	else
			Set rsPrice = Server.CreateObject("ADODB.Recordset")
			strSQL =	"SELECT [SALES PRICE] AS PRICE FROM BAANPRICE  WHERE [PRODUCT ID] = " & ProdID 
			Set cmdPrice=server.CreateObject("ADODB.COmmand")
			cmdPrice.CommandText=strSQL
			cmdPrice.ActiveConnection=dbconnstr()
			cmdPrice.CommandType=1'TEXT
			'cmdLabels.CommandType=4'Store Procedure
			set rsPrice=cmdPrice.Execute
			'rsPrice.Open strSQL, dbconnstr()
			if not rsPrice.EOF then
					
				rsPrice.MoveFirst
				ProdPrice = rsPrice("PRICE")
				'Response.Write  rsPrice("PRICE")
				'Response.end
				'strBody = strBody & FormatCurrency(ProdPrice,2) & strTab
				if  isnumeric(ProdPrice) then
					if csng(ProdPrice)>0 then
						GetProductPriceonProductID = csng(ProdPrice)
						exit function
					else
						GetProductPriceonProductID=0
						exit function
					end if
				else
					GetProductPriceonProductID=0
					exit function
				end if
				
			else
				GetProductPriceonProductID=0
			end if
	'	end if
	else
		GetProductPriceonProductID=0
	end if
end function
	
'*************************************
'Get Quantity of product in shoppers basket
'AC 1/30/00
'Arguments - prodid
'Returns integer
'*************************************
function GetQuantityofProdInBasket(prodid)
	
	Set rsQuantity = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT QTY as Quantity FROM SHOPPER WHERE PRODUCTID = '" & prodid & "' AND SESSIONID = '" & Session("netSessionID") & "'"
	rsQuantity.Open strSQL, dbconnstr()
	if not rsQuantity.EOF then
		GetQuantityofProdInBasket = rsQuantity("Quantity")
	else
		'Use 1 as default as most users default to ordering a quantity of 1
		'This quantity is displayed in the add product form
		GetQuantityofProdInBasket = 1
	end if
end Function
	
	
'*************************************
'Get Total items in shopping basket
'AC 1/30/00
'Arguments - prodid
'Returns integer
'*************************************	
function GetCountofProdInBasket()
	Set rsQuantity = Server.CreateObject("ADODB.Recordset")
	'strSQL = "SELECT count(PRODUCTID) as TotalProd FROM SHOPPER WHERE SESSIONID = " & Session("netSessionID") 
	strSQL = "SELECT QTY  FROM SHOPPER WHERE SESSIONID ='" & Session("netSessionID")  & "'"
	rsQuantity.Open strSQL, dbconnstr()
	if not rsQuantity.EOF then
		rsQuantity.MoveFirst
		GetCountofProdInBasket=0
		While not rsQuantity.EOF
			GetCountofProdInBasket=GetCountofProdInBasket+cint(rsQuantity("QTY"))
			rsQuantity.MoveNext
		wend
		
		'GetCountofProdInBasket = cint(rsQuantity("TotalProd"))
	else
		GetCountofProdInBasket = 0
	end if	
end Function
	
	
'*************************************
'Check to see if item is currently in basket and check or select if already in basket
'AC 1/30/00
'Arguments - prodid,strFormatType(c=checkbox,s=select,d=dropdown)
'Returns string
'*************************************
sub CheckItemInBasket(prodid,strFormatType)
	'Check to see if item is already in shopper basket
	'If so, check or select item on Quote Form to reflect database each time page is refreshed
				Set rsCheck = Server.CreateObject("ADODB.Recordset")
				strSQL = "SELECT QTY FROM SHOPPER WHERE PRODUCTID='" & prodid & "' and SESSIONID = '" & Session("netSessionID") & "' AND MASTERPRODUCTID=" & Session("intProdID")
				'Response.Write strSQL
				rsCheck.Open strSQL, dbconnstr()
				if rsCheck.EOF or BOF then
				else
					rsCheck.MoveFirst
							if rsCheck("QTY") >= "1" then
								Select Case strFormatType
								Case "c"
									Response.Write " checked "
								Case "d"
									Response.Write " selected "
								Case "s"
									Response.Write " selected "
								End Select
						end if
						
				end if
				rsCheck.Close
				set rsCheck = nothing
end sub
	
'*************************************
'Check if product is a standard item shipped with main product
'AC 1/30/00
'Arguments - optionprodid
'Returns boolean
'*************************************
function CheckProductidStandard(optionprodid)
	'Check if there is a main product in basket or currently selected
	if not Session("intProdID") = "" then			
		Set rsStandard = Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM CONFIG WHERE OPTIONID = '" & optionprodid & "' AND PRODUCTID = '" & Session("intProdID") & "'"
		rsStandard.Open strSQL, dbconnstr()
		if not rsStandard.EOF then
			if rsStandard("std") = "y" then
				CheckProductidStandard = True
			else
				CheckProductidStandard = False
			end if
		end if
	else
		CheckProductidStandard = False
	end if
	
end function
	
'****************************************************************************************
'Display Optional Products recommended for purchase
'AC 1/30/00
'Modified by KK 10/29/2003
'Modified by KK 1/23/2004 (STORE PROCEDURE)
'Modified by KK 5/5/2005 (grey lines)
'Modified by KK 10/25/2005 (black header, no arrows)
'Arguments - prodid,strFormatType(c=checkbox,s=select,d=dropdown),Qty,bStd(boolean(y/n)
'Returns string
'****************************************************************************************
sub GetOptionData(intProdID,strFormatType,Qty,bStd)

	Response.Write "<table border=0 bordercolor=Gray cellpadding=0>"
	
	'Get list of options available for current product
	Set rsGetOptionData = Server.CreateObject("ADODB.Recordset")
	Set cmdGetOptionData=server.CreateObject("ADODB.COmmand")
	cmdGetOptionData.CommandText="sp_GetOptionData"
	cmdGetOptionData.ActiveConnection=dbconnstr()
	cmdGetOptionData.CommandType=4'STORE PROCEDURE
	'create Parameter
	set ParamPRODUCTID=cmdGetOptionData.CreateParameter("@PRODUCTID",3,1)'3 adIntereg, 1 adParamInput
	cmdGetOptionData.Parameters.Append ParamPRODUCTID
	cmdGetOptionData.Parameters("@PRODUCTID")= intProdID
	'create Parameter
	set ParambStd=cmdGetOptionData.CreateParameter("@bStd",129,1,1)'129 adChar, 1 adParamInput
	cmdGetOptionData.Parameters.Append ParambStd
	cmdGetOptionData.Parameters("@bStd")= bStd
	set rsGetOptionData=cmdGetOptionData.Execute
	'rsGetOptionData.Open strSQL, dbconnstr()
	if not rsGetOptionData.EOF then
		Response.Write "<tr bgcolor=#000000>"
		Response.Write "<td valign=top colspan=2><font face=Verdana,Arial color=#ffffff size=1><b>" & LoadI18N("SLBCA0009",localeID) & "</nobr></b></font></td>"
		Response.Write "</tr>"

		While not rsGetOptionData.EOF
			'Determine Text Formatting
			'Response.Write rsGetOptionData("optionid")
			if strFormatType = "t" then
				if bStd="y" then
					Response.Write "<tr><td valign=top><nobr><b><font face=Verdana,Arial size=1 color=""#0000cd"">"
				else
					Response.Write "<tr><td valign=top width=""30%""><nobr><b><a href=javascript:openWin('WindowFrame.asp?prodid=" & rsGetOptionData("OPTIONID") & "');><font face=Verdana,Arial size=1><IMG SRC=""/nav/images/icons/SmallArrowRightGreen.gif"" border=""0"" align=""top"" hspace=""5"">"
				end if
				'Display Quantity if available (this only displays in the standard config area)
				if rsGetOptionData("qty") > "1" then
					Response.Write "(" & rsGetOptionData("qty") & ") X "
				end if
				if bStd="y" then
				Response.Write "&nbsp;</font>"
				else
					Response.Write rsGetOptionData("partnumber") & "</font>"
				end if
				if bStd<>"y" then
					Response.Write "</a>"
				end if

				Response.Write "</b></td><td valign=top><font face=Verdana,Arial size=1 "
				Response.Write ">" & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rsGetOptionData("optionid")) & "</font></nobr></td></tr>"
				Response.Write "<tr><td valign=top colspan=2 background=""/shopper/images/DottedLine250.gif""><img src=""images/spacer.gif""  height=1 border=0></td></tr>"
			end if
			
		rsGetOptionData.MoveNext
		wend
	'Response.Write "<tr height=1><td colspan=""2"" height=""1"" bgcolor=""DodgerBlue""></td></tr>"
	else
		Response.Write ""
	end if
	Response.Write "</table>"
end sub

'*************************************
'Check the approval status of customer
'KK 2/07/01
'Modified by KK 1/22/2004 (added store procedure)
'Arguments - none
'Returns boolean
'*************************************	

Function CheckApprovalStatus()
'Get first and second cookies from user. They should exist.
FirstCookie=Request.Cookies("LCDATA")("GUID")
SecondCookie=Request.Cookies("VFDATA")("GUID")

if len(FirstCookie)>0 and len(SecondCookie)>0 then

'Select Contact_id from table CONTACT 
	set rsContactID=server.CreateObject("ADODB.Recordset")
	Set cmdContactID=server.CreateObject("ADODB.Command")
	cmdContactID.CommandText="sp_getcontactidonwebid"
	cmdContactID.ActiveConnection=dbconnstr()
	cmdContactID.CommandType=4'STORE PROCEDURE
	'create Parameter
	set ParamCONTACT_WEB_ID=cmdContactID.CreateParameter("@CONTACT_WEB_ID",200,1,15)'200 adVarchar, 1 adParamInput, 15 -size
	cmdContactID.Parameters.Append ParamCONTACT_WEB_ID
	cmdContactID.Parameters("@CONTACT_WEB_ID")= Trim(Ucase(FirstCookie))
	set rsContactID=cmdContactID.Execute
	'rsContactID.Open strSQL,dbconnstr()
	if not rsContactID.EOF then
		ContID=rsContactID("CONTACT_ID")
'Check contact_id for second cookie and compare it with this customer's Contact_id in table CONTACT_SECOND
		set rsSecondCookie=server.CreateObject("ADODB.Recordset")
		Set cmdSecondCookie=server.CreateObject("ADODB.Command")
		cmdSecondCookie.CommandText="sp_getcontactidonsecondwebid"
		cmdSecondCookie.ActiveConnection=dbconnstr()
		cmdSecondCookie.CommandType=4'STORE PROCEDURE
		'create Parameter
		set ParamSECOND_CONTACT_WEB_ID=cmdSecondCookie.CreateParameter("@SECOND_CONTACT_WEB_ID",200,1,15)'200 adVarchar, 1 adParamInput, 15 -size
		cmdSecondCookie.Parameters.Append ParamSECOND_CONTACT_WEB_ID
		cmdSecondCookie.Parameters("@SECOND_CONTACT_WEB_ID")=Trim(Ucase(SecondCookie))
		set rsSecondCookie =cmdSecondCookie.Execute
		'rsSecondCookie.Open strSQL, dbconnstr()
		if not rsSecondCookie.EOF then
			RealContID=rsSecondCookie("CONTACTID")
			if strComp(RealContID,ContID)=0 then
				Session("ResponseFlag") = true
				CheckApprovalStatus=true
				Exit Function
			else 
				CheckApprovalStatus=false
				exit Function
			end if
		else
			CheckApprovalStatus=false
			exit Function
		end if
	else
		CheckApprovalStatus=false
		exit Function
	end if
else
	CheckApprovalStatus=false
	exit Function
end if
end Function


'*************************************
'Show links for existing quotes
'YK 2/09/01
'Arguments - none
'Returns list of links
'*************************************	

Sub ShowLinkToExistQuotes()

	'Select all quotes for this customer from SHOOPERQUOTE table 
	'Open recordset rsQuotes
	if not Session("ContactID")="" then
		set rsQuotes=server.CreateObject ("ADODB.Recordset")
		strSQL="Select DISTINCT QUOTEID, MAX(DATE_ENTERED) as DATE_ENTERED from SHOPPERQUOTE where estoreYN='n' and CUSTOMERID=" & Session("ContactID")& " and DATE_ENTERED >='" & (Now()-30) & "' group by QUOTEID"
		rsQuotes.Open strSQL, dbconnstr()
	'If quotes exist show link to page ShowQuotes.asp 
		if not rsQuotes.EOF then
			Response.Write "<a href='ShowQuotes.asp'><font face=Verdana size=1 color=SandyBrown><b>Quote History"  & "</b></font></a>"
		end if
	end if
	'Close recordset rsQuotes
		'rsQuotes.Close
		'set rsQuotes=nothing
	
end Sub

'*************************************
'Show 10 top products which are related this current product 
'YK 2/09/01
'Arguments - ProdID,boolean(y,n)
'Creates list of Products 
'*************************************	
Sub ShowTopProducts(ProdID,shwCheckbox)
	'Check if there is a scope in the basket
	if not CheckProductisScope(ProdID)then exit sub
	
	'Select 10 Top Product which have MAX requests from customers and related current product
	'Open recordset rsTopProduct
	set rsTopProduct=server.CreateObject("ADODB.Recordset")
	strSQL=	"Select TOP 10 PRODUCTID, COUNT(PRODUCTID) from SHOPPERQUOTE where " & _
			"PRODUCTID in (SELECT ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT " & _
			"from ALTERNATIVEPRODUCTS WHERE " & _
			"ALTERNATIVEPRODUCTS.ORIGINALPRODUCT=" & ProdID & ") group by PRODUCTID order by Count(PRODUCTID)"
	'Response.Write strSQL
	rsTopProduct.Open strSQL, dbconnstr()
	if not rsTopProduct.EOF then
		'Response.Write "<br><font face=Verdana size=1 color=SandyBrown><b><u>Top products: "  & "</u></b></font><br><br>"
		Response.Write "<br><font face=Verdana,Arial size=3 color=DodgerBlue><b>Top products</b></font><br>"
		rsTopProduct.MoveFirst
		
		While not rsTopProduct.EOF 
	'Display Checkbox
			if shwCheckbox = "1" then
				Response.Write "<input type=""checkbox"" name=" & rsTopProduct("PRODUCTID") & " value=" & rsTopProduct("PRODUCTID") & ">"
			end if

	'Open recordset rsProdName to show names of 10 top products
			set rsProdName=server.CreateObject("ADODB.Recordset")
			strSQL="Select PARTNUMBER,PRODUCTDESCRIPTION from PRODUCT where PRODUCTID=" & rsTopProduct("PRODUCTID")
			rsProdName.Open strSQL, dbconnstr()
			if not rsProdName.EOF then
				
	'Display Hyperlink
				Hyperlink = "WindowFrame.asp?" & "prodid=" & rsTopProduct("PRODUCTID")	
				Response.Write "<A HREF=""#"" onClick=""window.open('/shopper/WindowFrame.asp?prodid=" & rsTopProduct("PRODUCTID") & "','thewindow','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
				Response.Write "<font face=Verdana,Arial size=1><b>" & rsProdName("PARTNUMBER") & "</b></font><br>"
				Response.Write "</a>"
				Response.Write "<font face=Verdana,Arial size=1 color=Gray>" & rsProdName("PRODUCTDESCRIPTION") & "</font><br>"
			end if
			
			rsTopProduct.MoveNext
		Wend
	end if

end Sub


'*******************************************************
'Check does this product have recommended products
'YK 2/12/01
'Arguments - ProdID,boolean(y,n)
'Creates table with recommended products if they exist
'*******************************************************
Sub RecommendedProducts(ProdID,MasterID,shwCheckbox)

'Check if this product has recomended products in table RECOMMENDEDPRODUCT in ecatalog database 

'Open recordset rsRecProduct

set rsRecProduct=server.CreateObject("ADODB.Recordset")

if MasterID=0 then
	strSQL="Select RECOMMENDEDPRODUCT from RECOMMENDEDPRODUCTS where MASTERPRODUCT=" & PRODID & " and RECOMMENDEDPRODUCT not in (Select SHOPPER.PRODUCTID from SHOPPER where SHOPPER.SESSIONID='" &  Session("netSessionID") & "' and SHOPPER.MASTERPRODUCTID='')"
else
	strSQL="Select RECOMMENDEDPRODUCT from RECOMMENDEDPRODUCTS where MASTERPRODUCT=" & PRODID & " and RECOMMENDEDPRODUCT not in (Select SHOPPER.PRODUCTID from SHOPPER where SHOPPER.SESSIONID='" &  Session("netSessionID") & "' and SHOPPER.MASTERPRODUCTID='" & MasterID & "')"
end if

rsRecProduct.Open strSQL, dbconnstr()

if not  rsRecProduct.EOF then
	Response.Write "<table border=0 cellpadding=0 cellspacing=0><tr> "
	Response.Write "<td colspan=2><font face=Verdana,Arial size=1 color=#696969><br><b>" & LoadI18N("SLBCA0024",localeID)& ":</b></font></td></tr>"
	rsRecProduct.MoveFirst
	
	While not rsRecProduct.EOF
			Response.Write "<tr>"
'Display Checkbox
			if shwCheckbox = "1" then
				Response.Write "<td valign=""top""><input type=""checkbox"" id=" & rsRecProduct("RECOMMENDEDPRODUCT") & " value=" & rsRecProduct("RECOMMENDEDPRODUCT") & " OnClick=""CheckBox_OnClick('" & rsRecProduct("RECOMMENDEDPRODUCT") & "','" & MasterID & "')"" ></td> "
			end if

'Open recordset rsProductName to show names of  products
				set rsProductName=server.CreateObject("ADODB.Recordset")
				strSQL="Select PARTNUMBER,PRODUCTDESCRIPTION from PRODUCT where PRODUCTID=" & rsRecProduct("RECOMMENDEDPRODUCT")
				rsProductName.Open strSQL, dbconnstr()
				if not rsProductName.EOF then
					
'Display Hyperlink
					Response.Write "<td valign=top>"
					Hyperlink = "WindowFrame.asp?" & "prodid=" & rsRecProduct("RECOMMENDEDPRODUCT")	
					Response.Write "<A HREF=""#"" onClick=""window.open('/shopper/WindowFrame.asp?prodid=" & rsRecProduct("RECOMMENDEDPRODUCT") & "','thewindow','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
					Response.Write "<nobr><font face=Verdana,Arial size=1 color=#696969><b>" & rsProductName("PARTNUMBER") & " - </b></font></nobr></td>"
					Response.Write "</a>"
					Response.Write "<td valign=top><font face=Verdana,Arial size=1 color=#696969>" 
					'if Session("localeid")=1033 then
					'	Response.Write rsProductName("PRODUCTDESCRIPTION")
					'else
						Response.Write TRANSLATION("product","productdescription","PRODUCTID",rsRecProduct("RECOMMENDEDPRODUCT"))
					'end if
					Response.Write "</font></td>"
					if ApprovalStatus=true then
						Response.Write "<td align=right width=10 valign=top>"
						if GetProductPriceonProductID(rsRecProduct("RECOMMENDEDPRODUCT")) = "" then
							Response.Write "<table border=0 cellpadding=0 cellspacing=0><tr> "
							Response.Write "<td><img src=/nav/images/1pixspacer.gif height=1 width=5></td>"
							Response.Write "<td align=right valign=top><nobr><font face=Verdana,Arial size=1 color=#696969><b>" & LoadI18N("SLBCA0010",localeID)& "</b></font></nobr></td>"
							Response.Write "</tr></table>"	
						else
							Response.Write "<table border=0 cellpadding=0 cellspacing=0><tr> "
							Response.Write "<td valign=top><img src=/nav/images/1pixspacer.gif height=1 width=5></td>"
							Response.Write "<td align=right valign=top><font face=Verdana,Arial size=1 color=#696969><b><nobr>" & LoadI18N("SLBCA0025",localeID)& "&nbsp;&nbsp;&nbsp;</nobr><b></font></td>"
							Response.Write "<td align=right valign=top><font face=Verdana,Arial size=1 color=#696969><b><nobr>"  & FormatCurrency(GetProductPriceonProductID(rsRecProduct("RECOMMENDEDPRODUCT")),2) & "</nobr></b></font></td>"
							Response.Write "</tr></table>"	
						end if
						Response.Write "</td>"
					end if

					Response.Write "</tr>"
				end if
				
	rsRecProduct.MoveNext
	Wend
	Response.Write "</table>"
end if
End Sub

'*********************************************
'Create Body string for EMAIL for customer
'YK 2/23/01
'modified by KK 4/14/2005
'Arguments - QuoteID
'Returns string

'*********************************************
function EMailBody(QuoteID,localeid)	
	strTab = chr(9)
	strCret = chr(13) & chr(10)
'Check if this customer has oscilloscopes in his quote	
	set rsScopes=Server.CreateObject("ADODB.Recordset")

	strSQL="Select * from SHOPPERQUOTE where QUOTEID=" & QuoteID & " and PRODUCTID=MASTERPRODUCTID"
	rsScopes.Open  strSQL, dbconnstr()
	if not rsScopes.EOF then
		rsScopes.MoveFirst
		While not rsScopes.EOF
			Set rsGroup = Server.CreateObject("ADODB.Recordset")
				  strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
							"SHOPPERQUOTE.PROPERTYGROUPID AS Expr2, PROPERTYGROUP.SORTID " &_
							"FROM SHOPPERQUOTE INNER JOIN " &_
							"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
							"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
							"and SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopes("PRODUCTID") & "ORDER BY PROPERTYGROUP.SORTID"
							rsGroup.Open strSQL, dbconnstr()
														
						 While not rsGroup.EOF
		
								Set rsBasket = Server.CreateObject("ADODB.Recordset")
								strSQL = "SELECT DISTINCT SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, PRODUCT.PRODUCTDESCRIPTION AS ProdDesc, " &_
										"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
										"FROM SHOPPERQUOTE INNER JOIN " &_
										"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
										"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
										"WHERE (SHOPPERQUOTE.QUOTEID = " & QuoteID & ") AND ( SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopes("PRODUCTID") & ") AND (PRODUCT.PROPERTYGROUPID = " & rsGroup("Expr2") & ") " &_
										"ORDER BY PROPERTYGROUP.SORTID"
					           
					           rsBasket.Open strSQL, dbconnstr()
 
					'Check to see if any items exist in basket
						if not rsBasket.EOF or BOF then
								rsBasket.MoveFirst
					
						Do While not rsBasket.EOF
												
					'Get Quantity of each product in basket
								Set rsProdQty = Server.CreateObject("ADODB.Recordset")
								strSQL = "SELECT QTY AS Qty FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsBasket("PRODUCTID") & "' AND QUOTEID = " & QuoteID & " AND  SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopes("PRODUCTID") 
								rsProdQty.Open strSQL, dbconnstr()
								rsProdQty.MoveFirst
							
							Do While not rsProdQty.EOF
								if rsGroup("Expr2") = "5" or rsGroup("Expr2") = "28" or rsGroup("Expr2")= "29" or rsGroup("Expr2") = "30" or  rsGroup("Expr2") = "31" or ("Expr2") = "32" or rsGroup("Expr2") = "33" or rsGroup("Expr2") = "42" then

									strBody = strBody & strCret & strCret & rsBasket("PartNum")	& strCret & strCret		
								end if
		
									strBody = strBody  & rsProdQty("qty") & "  "
									
									if Len(rsBasket("PartNum")) > 12 then
										strBody = strBody & rsBasket("PartNum") & strTab
									else
										strBody = strBody & rsBasket("PartNum") & strTab & strTab  
									end if
		
									
									if Session("localeid")<>1033 then
										strBody = strBody & strCret & "   " & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rsBasket("PRODUCTID"))  & strCret  & strCret    
									else
									if cint(Instr(1,rsBasket("ProdDesc"),"<font")) > 0 then
										strBody = strBody & strCret & "   " & mid(rsBasket("ProdDesc"),1,(Instr(1,rsBasket("ProdDesc"),"<font"))-1)  & strCret  & strCret    
									else
										strBody = strBody & strCret & "   " & rsBasket("ProdDesc") & strCret  & strCret 
									end if
										'strBody = strBody & strCret & "   " & mid(rsBasket("ProdDesc"),1,Instr(rsBasket("ProdDesc"),"<font")-1)  & strCret  & strCret    
									end if
									
									rsProdQty.MoveNext
							Loop
						
							rsBasket.MoveNext
						Loop
						
					end if
			
				rsGroup.MoveNext
			Wend

			rsScopes.MoveNext
		Wend
	end if
strBody=strBody & strCRet & strCRet & strCRet	
'Show Protocol Analyzers if they in the cart
	Set rsAdditionalGroup = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
				"SHOPPERQUOTE.PROPERTYGROUPID AS Expr2, PROPERTYGROUP.SORTID " &_
				"FROM SHOPPERQUOTE INNER JOIN " &_
				"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
				"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
				"and (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='') and PROPERTYGROUP.CATEGORY_ID=19 ORDER BY PROPERTYGROUP.SORTID"
	rsAdditionalGroup.Open strSQL, dbconnstr()

	if not rsAdditionalGroup.EOF then
		rsAdditionalGroup.MoveFirst

		strBody = strBody & strCret & strCret & LoadI18N("SLBCA0595",localeid) & strCret & strCret		

		While not rsAdditionalGroup.EOF
			Set rsAdditionalBasket = Server.CreateObject("ADODB.Recordset")
			strSQL =	"SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, PRODUCT.PRODUCTDESCRIPTION AS ProdDesc, " &_
						"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
						"FROM SHOPPERQUOTE INNER JOIN " &_
						"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
						"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
						"WHERE (SHOPPERQUOTE.QUOTEID =" & QuoteID & ") AND (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='') AND (PRODUCT.PROPERTYGROUPID = '" & rsAdditionalGroup("Expr2") & "') " &_
						"ORDER BY PROPERTYGROUP.SORTID"
	        rsAdditionalBasket.Open strSQL, dbconnstr()
 
		'Check to see if any items exist in basket
			if not rsAdditionalBasket.EOF or BOF then
			
				rsAdditionalBasket.MoveFirst
	
			    While not rsAdditionalBasket.EOF
						
		'Get Quantity of each product in basket
					Set rsAddProdQty = Server.CreateObject("ADODB.Recordset")
					strSQL = "SELECT QTY AS Qty FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsAdditionalBasket("PRODUCTID") & "' AND QUOTEID = " & QuoteID	& " and (MASTERPRODUCTID ='' or MASTERPRODUCTID=0)"				
					
					rsAddProdQty.Open strSQL, dbconnstr()
					rsAddProdQty.MoveFirst
					While not rsAddProdQty.EOF
			
						strBody = strBody & StrCRet & rsAddProdQty("qty") & "  "
						
						if Len(rsAdditionalBasket("PartNum")) > 8 then
							strBody = strBody & rsAdditionalBasket("PartNum") & strTab
						else
							strBody = strBody & rsAdditionalBasket("PartNum") & strTab & strTab & strTab 
						end if

						if Session("localeid")<>1033 then
							strBody = strBody & strCret & "   " & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rsAdditionalBasket("PRODUCTID"))  & strCret      
						else
							if cint(Instr(1,rsAdditionalBasket("ProdDesc"),"<font")) > 0 then
								strBody = strBody & strCret & "   " & mid(rsAdditionalBasket("ProdDesc"),1,(Instr(1,rsAdditionalBasket("ProdDesc"),"<font"))-1)  & strCret  & strCret    
							else
								strBody = strBody & strCret & "   " & rsAdditionalBasket("ProdDesc") & strCret  & strCret 
							end if
							'strBody = strBody & strCret & "   " & mid(rsAdditionalBasket("ProdDesc"),1,Instr(rsAdditionalBasket("ProdDesc"),"<font")-1)  & strCret  & strCret    
						
						end if
						
						rsAddProdQty.MoveNext
					Wend
			
					rsAdditionalBasket.MoveNext
				Wend
					
			end if
			rsAdditionalGroup.MoveNext
		wend
End if
'Show Additional options if they exist
	Set rsAdditionalGroup = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
				"SHOPPERQUOTE.PROPERTYGROUPID AS Expr2, PROPERTYGROUP.SORTID " &_
				"FROM SHOPPERQUOTE INNER JOIN " &_
				"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
				"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
				"and (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  and PROPERTYGROUP.CATEGORY_ID<>19 ORDER BY PROPERTYGROUP.SORTID"
	rsAdditionalGroup.Open strSQL, dbconnstr()

	if not rsAdditionalGroup.EOF then
		rsAdditionalGroup.MoveFirst

		strBody = strBody & strCret & strCret & LoadI18N("SLBCA0027",localeid) & strCret & strCret		

		While not rsAdditionalGroup.EOF
			Set rsAdditionalBasket = Server.CreateObject("ADODB.Recordset")
			strSQL =	"SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, PRODUCT.PRODUCTDESCRIPTION AS ProdDesc, " &_
						"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
						"FROM SHOPPERQUOTE INNER JOIN " &_
						"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
						"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
						"WHERE (SHOPPERQUOTE.QUOTEID =" & QuoteID & ") AND (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  AND (PRODUCT.PROPERTYGROUPID = '" & rsAdditionalGroup("Expr2") & "') " &_
						"ORDER BY PROPERTYGROUP.SORTID"
	        rsAdditionalBasket.Open strSQL, dbconnstr()
 
		'Check to see if any items exist in basket
			if not rsAdditionalBasket.EOF or BOF then
			
				rsAdditionalBasket.MoveFirst
	
			    While not rsAdditionalBasket.EOF
						
		'Get Quantity of each product in basket
					Set rsAddProdQty = Server.CreateObject("ADODB.Recordset")
					strSQL = "SELECT QTY AS Qty FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsAdditionalBasket("PRODUCTID") & "' AND QUOTEID = " & QuoteID	& " and (MASTERPRODUCTID ='' or MASTERPRODUCTID=0)"				
					
					rsAddProdQty.Open strSQL, dbconnstr()
					rsAddProdQty.MoveFirst
					While not rsAddProdQty.EOF
			
						strBody = strBody & StrCRet & rsAddProdQty("qty") & "  "
						
						if Len(rsAdditionalBasket("PartNum")) > 8 then
							strBody = strBody & rsAdditionalBasket("PartNum") & strTab
						else
							strBody = strBody & rsAdditionalBasket("PartNum") & strTab & strTab & strTab 
						end if

						if Session("localeid")<>1033 then
							strBody = strBody & strCret & "   " & translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rsAdditionalBasket("PRODUCTID"))  & strCret      
						else
							if cint(Instr(1,rsAdditionalBasket("ProdDesc"),"<font")) > 0 then
								strBody = strBody & strCret & "   " & mid(rsAdditionalBasket("ProdDesc"),1,(Instr(1,rsAdditionalBasket("ProdDesc"),"<font"))-1)  & strCret  & strCret    
							else
								strBody = strBody & strCret & "   " & rsAdditionalBasket("ProdDesc")& strCret  & strCret 
							end if 
							'strBody = strBody & strCret & "   " & mid(rsAdditionalBasket("ProdDesc"),1,Instr(rsAdditionalBasket("ProdDesc"),"<font")-1)  & strCret  & strCret    
						end if
						
						rsAddProdQty.MoveNext
					Wend
			
					rsAdditionalBasket.MoveNext
				Wend
					
			end if
			rsAdditionalGroup.MoveNext
		wend
End if
EMailBody=strBody & strCRet & strCRet & strCRet
end function

'*******************************************************
'Check does this products have product for update
'YK 2/26/01
'Arguments - strProducts, strProductsInfo,UpdateTotalPrice
'Creates table with  product for update if it exist
'*******************************************************

sub CheckForUpdateProduct(strProducts, strProductsInfo,UpdateTotalPrice,MasterProdID)

'Check if	strProducts equal any value in PRODUCTS_ID  in TABLE UPDATEPRODUCTS db=ecatalog
	
	set rsUpdateProducts=server.CreateObject("ADODB.Recordset")
	strSQL="Select * from UPDATEPRODUCTS where PRODUCTS_ID ='" & strProducts & "'"
	rsUpdateProducts.Open strSQL, dbconnstr()
	
	if not rsUpdateProducts.EOF then
			
		Response.Write "<table width=""100%"" border=0 cellpadding=0 cellspacing=0>"
		Response.Write "<tr>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=#bdb76b><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td colspan=4 height=2  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=#bdb76b><img src=/nav/images/1pixspacer.gif height=1</td>"
		Response.Write "<td width=1 bgcolor=#dcdcdc><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "</tr>"
		
		Response.Write "<tr> "
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=""#ffffff"" ><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td  valign=top><img src=info_sign2.gif width=18 height=18></td><td colspan=2><font face=Verdana,Arial size=2 color=red align=left><b>" &  LoadI18N("SLBCA0031",localeID) & ": </b></font></td>"

'Price of Upgrate Products and string strUpgrateProdInfo

		strUpgrateProduct=trim(rsUpdateProducts("UPDATEPRODUCT_ID"))
		strUpgrateProduct=Replace(strUpgrateProduct,","," ")
		count=0
		while not len(strUpgrateProduct)=0

			Position=instr(1,strUpgrateProduct," ")

			if not Position=0 then
				strProdID=trim(Mid(strUpgrateProduct,1,Position))
				strUpgrateProduct=trim(Mid(strUpgrateProduct,Position,(len(strUpgrateProduct)-Position+1)))
			else
				strProdID=trim(strUpgrateProduct)
				strUpgrateProduct=""
			end if
			
			UpdateProductPrice=UpdateProductPrice + clng(GetProductPriceonProductID(strProdID))
			Redim preserve arrUpgrateProdID(count+1)	
			arrUpgrateProdID(count)=strProdID
			count=count+1
		wend
		
		if not UpdateProductPrice=0 then
			Response.Write "<td align=right>"
			Save=round(UpdateTotalPrice-UpdateProductPrice)
			Response.Write "<font face=Verdana,Arial size=2 color=red ><b>" & LoadI18N("SLBCA0032",localeID) & "&nbsp;&nbsp;&nbsp;&nbsp; " & FormatCurrency(Save,0) & " </b></font>"
			Response.Write "</td>"
		else
			Response.Write "<td></td>"
		end if
		
		Response.Write "<td width=10 height=2  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"

		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=""#ffffff"" ><img src=/nav/images/1pixspacer.gif height=1></td>"

'Create strProductsInfo 

		LastComma=len(strProductsInfo)-instr(1,strReverse(strProductsInfo),",")+1
		strLastPart= Replace (strProductsInfo,","," " & LoadI18N("SLBCA0045",localeID)& " " ,LastComma)
		strProductsInfo=mid(strProductsInfo,1,(LastComma-1)) & strLastPart
		
		Response.Write "<td>&nbsp;</td><td colspan=3><font size=1 color=#696969 face=Verdana,Arial><b>" & LoadI18N("SLBCA0033",localeID) & "&nbsp;" & strProductsInfo & "&nbsp;" & LoadI18N("SLBCA0034",localeID) & "</b></font></td>"
		Response.Write "<td width=10 height=2  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "</tr>"	
	
		Response.Write "<tr>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
											
		Response.Write "<td valign=top>"
		Response.Write "<input type=""checkbox"" id=" & rsUpdateProducts("UPDATEPRODUCT_ID") & " value=" & rsUpdateProducts("UPDATEPRODUCT_ID") & " OnClick=""UpdateProduct_OnClick('" & strProducts & "','" & rsUpdateProducts("UPDATEPRODUCT_ID") & "','" & MasterProdID & "')""></td> "

'Display Hyperlinks

		Response.Write "<td colspan=2 bgcolor=""#ffffff"">"
		Response.Write "<table width=""100%"" border=0 cellpadding=0 cellspacing=0>"
		for i=0 to (count-1)
			Response.Write "<tr>"
			Response.Write "<td>"
			Hyperlink = "WindowFrame.asp?" & "prodid=" & arrUpgrateProdID(i)	
			Response.Write "<A HREF=""#"" onClick=""window.open('/shopper/WindowFrame.asp?prodid=" & arrUpgrateProdID(i) & "','thewindow','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
			Response.Write "<font face=Verdana,Arial size=2 color=#696969><b>" & GetProdInfo(cint(arrUpgrateProdID(i)),0) & "</b></font>"
			Response.Write "</a>"
			Response.Write "<font face=Verdana,Arial size=1 color=#696969> - "
			'if session("localeid")=1033 then
			'	Response.write GetProdInfo(cint(arrUpgrateProdID(i)),1)
			'else
				Response.Write translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",cint(arrUpgrateProdID(i)))
			'end if
			Response.Write  "</font><br>"
			Response.Write "</td>"
			if ApprovalStatus=true then
				Response.Write "<td align=right>"
				if GetProductPriceonProductID(arrUpgrateProdID(i)) = 0 then
					Response.Write "<font face=Verdana,Arial size=1 color=#696969>" & LoadI18N("SLBCA0010",localeID)& "</font>"
				else
					Response.Write "<font face=Verdana,Arial size=1 color=#696969>"  & FormatCurrency(GetProductPriceonProductID(arrUpgrateProdID(i)),2) & "</font>"
				end if
				Response.Write "</td>"
			else
				Response.Write "<td>&nbsp;</td>"
			end if
			Response.Write "</tr>"
		next
		Response.Write "</table>"
		Response.Write "</td>"
		
		Response.Write "<td>&nbsp;</td>"
		Response.Write "<td width=10  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"

		Response.Write "</tr>"

		Response.Write "<tr>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=#bdb76b><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td colspan=4 height=2  bgcolor=""#ffffff""><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=10 height=2  bgcolor=#bdb76b><img src=/nav/images/1pixspacer.gif height=1></td>"
		Response.Write "<td width=1 bgcolor=#bdb76b><img src=/nav/images/spacer.gif width=1 height=1></td>"
		Response.Write "</tr>"
		Response.Write "</table><br>"
		
	
		
	end if

'Close all recordsets

	rsUpdateProducts.Close
	set rsUpdateProducts=nothing

end sub

'*******************************************************
'Check does product have standard options shipped with
'YK 3/12/01
'Arguments - ProdID
'Returns boolean
'*******************************************************
function CheckStandardOptionsExist(ProdID)
'Open recordset
	set rsCheckStandard=server.CreateObject("ADODB.Recordset")
	if len(GetDisabledProducts)>0 then
		strSQL=	"Select * from CONFIG where PRODUCTID=" & ProdID & " and STD='y'" & _
				" and OPTIONID not in (" & GetDisabledProducts & ")"
	else
		strSQL="Select * from CONFIG where PRODUCTID=" & ProdID & " and STD='y'"
	end if
	rsCheckStandard.Open strSQL, dbconnstr()
	if not rsCheckStandard.EOF then
		CheckStandardOptionsExist=true
	else
		CheckStandardOptionsExist=false
	end if
'Close recordset
	rsCheckStandard.Close
	set rsCheckStandard=nothing
end function

'************************************************************************************
'Check does new Total will be not more then 2,147,483,647 (max for Long data type)
'YK 3/12/01
'Arguments - Total,Price
'If Total+Price > 2,147,483,647 redirect to error page
'************************************************************************************
sub CheckNewTotal(Total,Price)
	if Total>(2147483647-Price)then
		Response.Redirect "ErrorConnection.asp"
	end if
end sub
'************************************************************************************
'Calculate Total for all products in the basket
'YK 3/14/01
'Arguments - none
'Returns TotalPrice currency
'************************************************************************************

function CalculateTotalPrice()
'Select QTY and PRODUCTID from SHOPPER for current session
	set rsAllProducts=server.CreateObject("ADODB.Recordset")
	strSQL="Select * from SHOPPER where SESSIONID='" & Session("netSessionID") & "'"
	rsAllProducts.Open strSQL, dbconnstr()
	while not rsAllProducts.EOF 
		if len(Session("offerid"))=0 then
			ProductPrice=(cint(rsAllProducts("QTY")))*(csng(GetProductPriceonProductID(rsAllProducts("PRODUCTID"))))
		else
			ProductPrice=(cint(rsAllProducts("QTY")))*(csng(rsAllProducts("PRICE")))
		end if
		CalculateTotalPrice=CalculateTotalPrice+ProductPrice
		rsAllProducts.MoveNext
	wend

CalculateTotalPrice=FormatCurrency(CalculateTotalPrice,2)

end function

'*************************************
'Check does image exist for this product
'YK 3/14/01
'Arguments - prodid
'Returns boolean
'*************************************
function CheckImageExist(ProdID)
	
	Set rsImageExist = Server.CreateObject("ADODB.Recordset")
	strSQL =	"SELECT IMAGEFILENAME FROM PRODUCT WHERE (PRODUCTID = '" & ProdID & "')"
	rsImageExist.Open strSQL, dbconnstr()
	if not rsImageExist.EOF then
		if len(trim(rsImageExist("IMAGEFILENAME")))>0  then
			CheckImageExist=true
		end if
	end if
end function


'*************************************
'ShowRelatedProductsinFamily
'AC 3/15/01
'Change by YK 5/4/01
'Arguments - prodid
'Returns boolean
'*************************************
sub ShowRelatedProducts(productid,shwCheckbox)
		Set rsRel = Server.CreateObject("ADODB.Recordset")

		if len(GetDisabledProducts)>0 then
			strSQL =	"SELECT ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT AS AltProdID, " &_
						"ALTERNATIVEPRODUCTS.ORIGINALPRODUCT, " &_
						"PRODUCT.PRODUCTDESCRIPTION, PRODUCT.PARTNUMBER, " &_
						"PRODUCT.IMAGEFILENAME, " &_
						"PRODUCT.PROPERTYGROUPID " &_
						"FROM ALTERNATIVEPRODUCTS INNER JOIN " &_
						"PRODUCT ON " &_
						"ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT = PRODUCT.PRODUCTID " &_
						"WHERE (ALTERNATIVEPRODUCTS.ORIGINALPRODUCT = '" & productid & "')" &_
						" and ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT not in (" & GetDisabledProducts & ")"
		else
			strSQL =	"SELECT ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT AS AltProdID, " &_
						"ALTERNATIVEPRODUCTS.ORIGINALPRODUCT, " &_
						"PRODUCT.PRODUCTDESCRIPTION, PRODUCT.PARTNUMBER, " &_
						"PRODUCT.IMAGEFILENAME, " &_
						"PRODUCT.PROPERTYGROUPID " &_
						"FROM ALTERNATIVEPRODUCTS INNER JOIN " &_
						"PRODUCT ON " &_
						"ALTERNATIVEPRODUCTS.ALTERNATIVEPRODUCT = PRODUCT.PRODUCTID " &_
						"WHERE (ALTERNATIVEPRODUCTS.ORIGINALPRODUCT = '" & productid & "')"
		end if
		rsRel.Open strSQL, dbconnstr()
				'if not rsAlt.EOF or BOF then
					if not rsRel.EOF then
						'Check Approval status of customer and show him prices
						'only if ApprovalStatus=true
						ApprovalStatus=CheckApprovalStatus()
			
						Response.Write "<font face=Verdana,Arial color=DodgerBlue size=2><b>" & LoadI18N("SLBCA0119",localeID)& "</b></font><br>"
						rsRel.MoveFirst
						Do While not rsRel.EOF
									
								Response.Write "<br>"
								 
								'Display Checkbox
								if shwCheckbox = "1" then
									Response.Write "<input type=""checkbox"" name=" & rsRel("AltProdID") & " value=" & rsRel("AltProdID") & ">"
								end if
								
								'Call CheckProductinBasket(rsAlt("AltProdID"))
								
								
								Hyperlink = "WindowFrame.asp?" & "prodid=" & rsRel("AltProdID")	
								'Response.Write hyperlink
								Response.Write "<A HREF=""#"" onClick=""window.open('/shopper/WindowFrame.asp?prodid=" & rsRel("AltProdID") & "','thewindow','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
								'Response.Write "<A HREF= ""#"" onClick=""window.open('" & Hyperlink & "','scopescreen','toolbar=no,width=600,height=400, status=no,scrollbars=auto,resize=no');return false"">"
								if ApprovalStatus=true then
									if GetProductPriceonProductID(ProdID)=0 then
										Response.Write "<font face=Verdana,Arial size=1><b>" & rsRel("PARTNUMBER") & "</font></a><font face=Verdana,Arial size=1 color=""DodgerBlue""><b> - " & LoadI18N("SLBCA0010",localeID)& "</b></font><br>"
									else
										Response.Write "<font face=Verdana,Arial size=1><b>" & rsRel("PARTNUMBER") & "</font></a><font face=Verdana,Arial size=1 color=""DodgerBlue""><b> - " & FormatCurrency(GetProductPriceonProductID(rsRel("AltProdID")),2) & "</b></font><br>"
									end if	
								else
									Response.Write "<font face=Verdana,Arial size=1><b>" & rsRel("PARTNUMBER") & "</b></font></a><br>"
								end if
								Response.Write "<font face=Verdana,Arial size=1 color=Gray>"								
								'if Session("localeid")=1033 then
								'	Response.Write  rsRel("PRODUCTDESCRIPTION") 
								'else
									Response.Write translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rsRel("AltProdID"))	
								'end if
								Response.write  "</font><br>"
								'Call DisplayThumbonProdID(rsAlt("AltProdID"),50,"y")
								
			
						rsRel.MoveNext
						Loop
					else
						Response.Write ""
					end if
					'rsRel.Close
					'set rsRel = nothing
	end sub	
	
'*********************************************
'Create Body string for EMAIL for CSRs and SE
'KK 5/4/01
'Modified by KK 3/11/2003
'Modified by KK 8/19/2004
'Modified by KK 3/3/2006
'Modified by KK 9/6/2006
'rev6 - Kate Kaplan 1/30/2009 - added Pert 
'rev 7 - Kate Kaplan 8/13/2009 - added Serial Data Bundles 
'rev 8 - Kate Kaplan 3/11/2010 - added promo footer for US quotes
'Arguments - QuoteID
'Returns string

'*********************************************
function EMailBodyNew(QuoteID)	
	strTab = chr(9)
	strCret = chr(13) & chr(10)
	if len(Session("emaillocaleid"))=0 then Session("emaillocaleid")=1033
	localeid=Session("emaillocaleid")
	'If this customer has scopes in the basket 

	count=0
	TotalPrice=0	
	Total=0
	set rsScopesNew=Server.CreateObject("ADODB.Recordset")

	strSQL="Select * from SHOPPERQUOTE where QUOTEID=" & QuoteID & " and PRODUCTID=MASTERPRODUCTID"
	'Response.Write " 1 - " &strSQL &"<br>"
	rsScopesNew.Open  strSQL, dbconnstr()

	if not rsScopesNew.EOF then
		'rsScopesNew.MoveFirst
		While not rsScopesNew.EOF
			if len(Session("offerid"))=0 then
				strBody=strBody & strCret & strCret &  GetProdInfo(rsScopesNew("PRODUCTID"),0) &  strCret & strCret
			else
				strBody=strBody & strCret & strCret '&  GetOfferInfo(Session("offerid"),0) &  strCret & strCret
			end if
			'Response.Write strBody
			SubTotal=0
			Session("BasketFlag")=true
			Set rsGroupNew = Server.CreateObject("ADODB.Recordset")
			strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
						"SHOPPERQUOTE.PROPERTYGROUPID AS PROPERTYGROUPID, PROPERTYGROUP.SORTID " &_
						"FROM SHOPPERQUOTE INNER JOIN " &_
						"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
						"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
						" and SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopesNew("PRODUCTID") & " ORDER BY PROPERTYGROUP.SORTID"
			'Response.Write  " 2 - " & strSQL & "<br>"
			rsGroupNew.Open strSQL, dbconnstr()
			if not rsGroupNew.EOF then
				'rsGroupNew.MoveFirst
				While not rsGroupNew.EOF
'Count of product groups to show "View Standard Options shipped with product"
					count=count+1
					divcount="divcount" & count
					imgcount="imgcount" & count
			
					strProducts=""
					strProductsInfo=""
				
								
					Set rsBasketNew = Server.CreateObject("ADODB.Recordset")
									strSQL = " SELECT DISTINCT SHOPPERQUOTE.QTY AS Qty,SHOPPERQUOTE.PRICE as PRICE, PRODUCT.PARTNUMBER AS PartNum, replace(PRODUCT.PRODUCTDESCRIPTION,'<br>','" & strCret & "') AS ProdDesc, " &_
										" SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
										" FROM SHOPPERQUOTE INNER JOIN " &_
										" PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
										" PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
										" WHERE (SHOPPERQUOTE.QUOTEID = " & QuoteID & ") AND ( SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopesNew("PRODUCTID") & ") AND (PRODUCT.PROPERTYGROUPID = '" & rsGroupNew("PROPERTYGROUPID") & "') " &_
										" ORDER BY PROPERTYGROUP.SORTID"
				'Response.Write  " 3 - " & strSQL & "<br>"

				rsBasketNew.Open strSQL, dbconnstr()
'Check to see if any items exist in basket
				if not rsBasketNew.EOF  then
'Display Standard Items shipped with Scope
					'rsBasketNew.MoveFirst
					
					if CheckProductisScope(rsBasketNew("PRODUCTID")) then
						if not len(strPageBody)>0 then
							if len(Session("offerid"))>0 then
								strPageBody=rsBasketNew("PartNum") & " Promotion"
							else
								strPageBody= rsBasketNew("PartNum")
							end if
						end if
					end if
					
					While not rsBasketNew.EOF
												
				'Get Quantity of each product in basket
						Set rsProdQtyNew = Server.CreateObject("ADODB.Recordset")
						strSQL = "SELECT QTY,PRICE  FROM SHOPPERQUOTE WHERE PRODUCTID = " & rsBasketNew("PRODUCTID") & " AND QUOTEID = " & QuoteID   & " AND  SHOPPERQUOTE.MASTERPRODUCTID=" & rsScopesNew("PRODUCTID") 
						'Response.Write strSQL & "<br>"
						rsProdQtyNew.Open strSQL, dbconnstr()
					'			
						if not rsProdQtyNew.EOF then
							'rsProdQtyNew.MoveFirst
							cnt=cnt+1
							textcount="text"&cnt
							bincount="bin" & cnt
							btncount="btn" & cnt
							rsBasketCount=0
							While not rsProdQtyNew.EOF
					'						
								rsBasketCount = rsBasketCount + 1
								strBody = strBody  & rsProdQtyNew("qty") & "  "
					'				
								if Len(rsBasketNew("PartNum")) > 12 then
									strBody = strBody & rsBasketNew("PartNum") & strTab
								elseif Len(rsBasketNew("PartNum"))>8 and len(rsBasketNew("PartNum"))<12 then
									strBody = strBody & rsBasketNew("PartNum") & strTab & strTab
								else
									strBody = strBody & rsBasketNew("PartNum") & strTab & strTab & strTab 
								end if
								'if len(Session("offerid"))=0 then
								'	ProductPrice =(GetProductPriceonProductID(rsBasketNew("PRODUCTID")))
								'else
								if  len(rsBasketNew("Price"))>0 and cstr(rsBasketNew("Price"))<>"0"  then
									ProductPrice = csng(rsBasketNew("Price"))
								else
									ProductPrice=0
								end if	
								'end if
								'Response.Write ProductPrice & "<br>"
								ProductQty = cint(rsProdQtyNew("QTY"))/1
								'Response.Write ProductQty
								TotalPrice=(ProductQty * ProductPrice)
								'Response.Write TotalPrice
								Call CheckNewTotal (SubTotal,TotalPrice)
															
								SubTotal=SubTotal+TotalPrice
								'Response.Write SubTotal
								if len(Session("Country"))>0 then
									if cstr(Session("Country"))="United States" then
							 			if rsProdQtyNew("PRICE")>0  then
											strBody=strBody  & FormatCurrency(rsProdQtyNew("PRICE"),2) & strTab 
										else
											if len(Session("offerid"))=0 then' and rsBasketNew("PRODUCTID")<>1200 and rsBasketNew("PRODUCTID")<>1201 and rsBasketNew("PRODUCTID")<>535 and rsBasketNew("PRODUCTID")<>1151 and rsBasketNew("PRODUCTID")<>570 then
												strBody=strBody  & LoadI18N("SLBCA0010",localeID)  & strTab 
											else
												strBody=strBody  & "Free"  & strTab 
											end if
										end if
									else
										if rsProdQtyNew("PRICE")=0 and len(Session("offerid"))>0 then
											strBody=strBody  & "Free"  & strTab 
										end if
									end if
								end if
								if cint(rsProdQtyNew("QTY"))>1 and rsProdQtyNew("PRICE")>0 and len(Session("offerid"))=0 then
									strBody=strBody & LoadI18N("SLBCA0030",localeID) & ": " & FormatCurrency(TotalPrice,2) & strTab
								end if
								'else
								'	if cstr(rsProdQtyNew("Price"))<>"0"  then
								'		strBody=strBody  & FormatCurrency(rsProdQtyNew("PRICE"),2) & strTab 
								'	else
								'		strBody=strBody  & "Free"  & strTab 
								'	end if
								'end if	
								if cint(Instr(1,rsBasketNew("ProdDesc"),"<font")) > 0 then
									strBody = strBody & strCret & "   " & mid(rsBasketNew("ProdDesc"),1,(Instr(1,rsBasketNew("ProdDesc"),"<font"))-1)  & strCret  & strCret    
								else
									strBody = strBody & strCret & "   " & rsBasketNew("ProdDesc") & strCret  & strCret 
								end if
								'strBody = strBody & strCret & "   " & mid(rsBasketNew("ProdDesc"),1,Instr(rsBasketNew("ProdDesc"),"<font")-1)  & strCret  & strCret    
												
								rsProdQtyNew.MoveNext
							wend
						end if
'Close recordset rsProdQtyNew
					'	rsProdQtyNew.Close
					'	set rsProdQtyNew=nothing

						rsBasketNew.MoveNext
					wend
			end if

'Close recordset rsBasketNew
				'rsBasketNew.Close
				'set rsBasketNew=nothing
					
				rsGroupNew.MoveNext
			Wend
		end if

'Close recordset rsGroupNew
			'rsGroupNew.Close
			'set rsGroupNew=nothing

'Show subtotal  for each scope
		if len(Session("Country"))>0 then
			if cstr(Session("Country"))="United States" then
			'if len(Session("offerid"))=0 then
				Call CheckNewTotal (Total,SubTotal)
				Total=Total+SubTotal
				strBody=strBody & strCret & LoadI18N("SLBCA0026",localeID) & " " &  GetProdInfo(rsScopesNew("PRODUCTID"),0) & " " & LoadI18N("SLBCA0111",localeID) & ": "
				if SubTotal>0 then
					strBody=strBody & FormatCurrency(SubTotal,2) & strCret & strCret
				else	
					strBody=strBody & LoadI18N("SLBCA0010",localeID)  & strCret & strCret
				end if
			end if
		end if
		    strBody= strBody & strCret & strCret
	
			rsScopesNew.MoveNext
		Wend
	end if

'Close recordset rsScopesNew
	'rsScopesNew.Close
	'set rsScopesNew=nothing
	'Response.Write strBody
	'********************************************
'Show Protocal Analyzers if they are in the cart
'***********************************************
	
	Set rsAdditionalGroupNew = Server.CreateObject("ADODB.Recordset")
		strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
					"SHOPPERQUOTE.PROPERTYGROUPID AS Expr2, PROPERTYGROUP.SORTID " &_
					"FROM SHOPPERQUOTE INNER JOIN " &_
					"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
					"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
					"and (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  and PROPERTYGROUP.CATEGORY_ID=19 ORDER BY PROPERTYGROUP.SORTID"
	rsAdditionalGroupNew.Open strSQL, dbconnstr()
	if not rsAdditionalGroupNew.EOF then
		SubTotal=0
		rsAdditionalGroupNew.MoveFirst

		strBody = strBody & strCret & strCret & LoadI18N("SLBCA0595",localeID) & strCret & strCret		
		
		if  len(strPageBody)>0 then
			strPageBody=strPageBody & strCret  & LoadI18N("SLBCA0595",localeID)
		else
			strPageBody= LoadI18N("SLBCA0595",localeID)
		end if
		
		While not rsAdditionalGroupNew.EOF
			
			
		'For each group create string of product IDs strProducts
		'and string of product Infos strProductsInfo and UpdateTotalPrice
			
			strProducts=""
			strProductsInfo=""
			UpdateTotalPrice=0

			Set rsAdditionalBasketNew = Server.CreateObject("ADODB.Recordset")
				strSQL =	"SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, replace(PRODUCT.PRODUCTDESCRIPTION,'<br>','" & strCret & "') AS ProdDesc, " &_
							"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
							"FROM SHOPPERQUOTE INNER JOIN " &_
							"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
							"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
							"WHERE (SHOPPERQUOTE.QUOTEID =" & QuoteID & ") AND (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  AND (PRODUCT.PROPERTYGROUPID = '" & rsAdditionalGroupNew("Expr2") & "') " &_
							"ORDER BY PROPERTYGROUP.SORTID"
	      
	        rsAdditionalBasketNew.Open strSQL, dbconnstr()
			
		'Check to see if any items exist in basket
			if not rsAdditionalBasketNew.EOF or BOF then
								
				rsAdditionalBasketNew.MoveFirst
	
			    While not rsAdditionalBasketNew.EOF
		'Get Quantity of each product in basket
					Set rsAddProdQtyNew = Server.CreateObject("ADODB.Recordset")
						strSQL = "SELECT QTY AS Qty,PRICE FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsAdditionalBasketNew("PRODUCTID") & "' AND QUOTEID = " & QuoteID	& " and (MASTERPRODUCTID ='' or MASTERPRODUCTID=0)"				

					rsAddProdQtyNew.Open strSQL, dbconnstr()
					
					if not rsAddProdQtyNew.EOF then
						rsAddProdQtyNew.MoveFirst
						cnt=cnt+1
						textcount="text"&cnt
						bincount="bin" & cnt
						btncount="btn" & cnt
						minuscnt="minuscnt" & cnt

						While not rsAddProdQtyNew.EOF
							strBody = strBody & rsAddProdQtyNew("qty") & "  "
						
							if Len(rsAdditionalBasketNew("PartNum")) > 8 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab
							elseif Len(rsAdditionalBasketNew("PartNum"))>8 and len(rsAdditionalBasketNew("PartNum"))<12 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab
							else
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab & strTab 
							end if
							


							'if len(Session("offerid"))=0 then
								'TotalPrice=cint(rsAddProdQtyNew("QTY")) * (GetProductPriceonProductID(rsAdditionalBasketNew("PRODUCTID")))
								'Call CheckNewTotal (SubTotal,TotalPrice)
								TotalPrice=(ProductQty * ProductPrice)
								'SubTotal=SubTotal+TotalPrice
								if len(Session("Country"))>0 then
									if cstr(Session("Country"))="United States" then

										if rsAddProdQtyNew("Price")>0 then
											strBody=strBody  & FormatCurrency(rsAddProdQtyNew("Price"),2) & strTab
										else
											if len(Session("offerid"))=0 then'and rsAdditionalBasketNew("PRODUCTID")<>1200 and rsAdditionalBasketNew("PRODUCTID")<>1201 and rsAdditionalBasketNew("PRODUCTID")<>535 and rsAdditionalBasketNew("PRODUCTID")<>1151 and rsAdditionalBasketNew("PRODUCTID")<>570 then	
												strBody=strBody  & LoadI18N("SLBCA0010",localeID)  & strTab 
											else
												strBody=strBody  & "Free"  & strTab 
											end if
										end if
									else
										if rsAddProdQtyNew("PRICE")=0 and len(Session("offerid"))>0 then
											strBody=strBody  & "Free"  & strTab 
										end if
									end if
								end if
						

							'else
								if  len(rsAddProdQtyNew("Price"))>0 and cstr(rsAddProdQtyNew("Price"))<>"0"  then
									ProductPrice = csng(rsAddProdQtyNew("Price"))
								else
									ProductPrice=0
								end if	
								ProductQty = cint(rsAddProdQtyNew("QTY"))/1
								'Response.Write ProductQty
								TotalPrice=(ProductQty * ProductPrice)
								Call CheckNewTotal (SubTotal,TotalPrice)
								SubTotal=SubTotal+TotalPrice
								if cint(rsAddProdQtyNew("QTY"))>1 and rsAddProdQtyNew("Price")>0 and len(Session("offerid"))=0 then
									strBody=strBody & LoadI18N("SLBCA0030",localeID) & ": " & FormatCurrency(TotalPrice,2) & strTab
								end if
								'	if cstr(rsAddProdQtyNew("Price"))<>"0"  then
								'		strBody=strBody  & FormatCurrency(rsAddProdQtyNew("PRICE"),2) & strTab 
								'	else
								'		strBody=strBody  & "Free"  & strTab 
								'	end if
							'end if
							if cint(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font")) > 0 then
									strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font"))-1)  & strCret  & strCret    
							else
								strBody = strBody & strCret & "   " & rsAdditionalBasketNew("ProdDesc")  & strCret  & strCret 
							end if
							'strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font"))-1)  & strCret  & strCret    

							rsAddProdQtyNew.MoveNext
						Wend
					
					end if

'Close recordset rsAddProdQtyNew
					rsAddProdQtyNew.Close
					set rsAddProdQtyNew=nothing
					
					
					rsAdditionalBasketNew.MoveNext
				Wend
			end if
			
'Close recordset rsAdditionalBasketNew
			rsAdditionalBasketNew.Close
			set rsAdditionalBasketNew=nothing
			

			strBody=strBody &  strCRet &  strCRet		
			rsAdditionalGroupNew.MoveNext
		Wend
		
'Show subTotal for additional options
	if len(Session("Country"))>0 then
		if cstr(Session("Country"))="United States" then
			strBody=strBody & strCret & LoadI18N("SLBCA0026",localeID) & " " & LoadI18N("SLBCA0595",localeID) & ": "
			if SubTotal>0 then
				strBody=strBody & FormatCurrency(SubTotal,2) & strCret & strCret
			else
				strBody=strBody & LoadI18N("SLBCA0010",localeID)& strCret & strCret
			end if

			Total=Total+SubTotal
		end if
	end if
end if
'***************************************
'Show Serial Data Generators
'***************************************
	
	Set rsAdditionalGroupNew = Server.CreateObject("ADODB.Recordset")
		strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
					"SHOPPERQUOTE.PROPERTYGROUPID as Expr2,PROPERTYGROUP.CATEGORY_ID, PROPERTYGROUP.SORTID " &_
					"FROM SHOPPERQUOTE INNER JOIN " &_
					"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
					"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
					"and (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  and PROPERTYGROUP.CATEGORY_ID in (24,25,2,30,31) ORDER BY PROPERTYGROUP.SORTID"
	rsAdditionalGroupNew.Open strSQL, dbconnstr()
	if not rsAdditionalGroupNew.EOF then
		SubTotal=0
		rsAdditionalGroupNew.MoveFirst
	    strBody = strBody & strCret & strCret & translation("CATEGORY","CATEGORY_NAME","CATEGORY_ID",rsAdditionalGroupNew("CATEGORY_ID")) & strCret & strCret		
		strCategoryId=rsAdditionalGroupNew("CATEGORY_ID")
		While not rsAdditionalGroupNew.EOF
			
			
		'For each group create string of product IDs strProducts
		'and string of product Infos strProductsInfo and UpdateTotalPrice
			
			strProducts=""
			strProductsInfo=""
			UpdateTotalPrice=0

			Set rsAdditionalBasketNew = Server.CreateObject("ADODB.Recordset")
				strSQL =	"SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum,replace(PRODUCT.PRODUCTDESCRIPTION,'<br>','" & strCret & "') AS ProdDesc, " &_
							"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
							"FROM SHOPPERQUOTE INNER JOIN " &_
							"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
							"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
							"WHERE (SHOPPERQUOTE.QUOTEID =" & QuoteID & ") AND (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  AND (PRODUCT.PROPERTYGROUPID = '" & rsAdditionalGroupNew("Expr2") & "') " &_
							"ORDER BY PROPERTYGROUP.SORTID"
	      
	        rsAdditionalBasketNew.Open strSQL, dbconnstr()
			
		'Check to see if any items exist in basket
			if not rsAdditionalBasketNew.EOF or BOF then
								
				rsAdditionalBasketNew.MoveFirst
	
			    While not rsAdditionalBasketNew.EOF
		'Get Quantity of each product in basket
					Set rsAddProdQtyNew = Server.CreateObject("ADODB.Recordset")
						strSQL = "SELECT QTY AS Qty,PRICE FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsAdditionalBasketNew("PRODUCTID") & "' AND QUOTEID = " & QuoteID	& " and (MASTERPRODUCTID='' or MASTERPRODUCTID=0)"				

					rsAddProdQtyNew.Open strSQL, dbconnstr()
					
					if not rsAddProdQtyNew.EOF then
						rsAddProdQtyNew.MoveFirst
						cnt=cnt+1
						textcount="text"&cnt
						bincount="bin" & cnt
						btncount="btn" & cnt
						minuscnt="minuscnt" & cnt

						While not rsAddProdQtyNew.EOF
							strBody = strBody & rsAddProdQtyNew("qty") & "  "
						
							if Len(rsAdditionalBasketNew("PartNum")) > 8 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab
							elseif Len(rsAdditionalBasketNew("PartNum"))>8 and len(rsAdditionalBasketNew("PartNum"))<12 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab
							else
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab & strTab 
							end if
							


							'if len(Session("offerid"))=0 then
								'TotalPrice=cint(rsAddProdQtyNew("QTY")) * (GetProductPriceonProductID(rsAdditionalBasketNew("PRODUCTID")))
								'Call CheckNewTotal (SubTotal,TotalPrice)
								TotalPrice=(ProductQty * ProductPrice)
								'SubTotal=SubTotal+TotalPrice
								if len(Session("Country"))>0 then
									if cstr(Session("Country"))="United States" then

										if rsAddProdQtyNew("Price")>0 then
											strBody=strBody  & FormatCurrency(rsAddProdQtyNew("Price"),2) & strTab
										else
											if len(Session("offerid"))=0 then'and rsAdditionalBasketNew("PRODUCTID")<>1200 and rsAdditionalBasketNew("PRODUCTID")<>1201 and rsAdditionalBasketNew("PRODUCTID")<>535 and rsAdditionalBasketNew("PRODUCTID")<>1151 and rsAdditionalBasketNew("PRODUCTID")<>570 then
												strBody=strBody  & LoadI18N("SLBCA0010",localeID)  & strTab 
											else
												strBody=strBody  & "Free"  & strTab 
											end if
							
										end if
									else
										if rsAddProdQtyNew("PRICE")=0 and len(Session("offerid"))>0 then
											strBody=strBody  & "Free"  & strTab 
										end if
									end if
								end if
						

							'else
								if  len(rsAddProdQtyNew("Price"))>0 and cstr(rsAddProdQtyNew("Price"))<>"0"  then
									ProductPrice = csng(rsAddProdQtyNew("Price"))
								else
									ProductPrice=0
								end if	
								ProductQty = cint(rsAddProdQtyNew("QTY"))/1
								'Response.Write ProductQty
								TotalPrice=(ProductQty * ProductPrice)
								Call CheckNewTotal (SubTotal,TotalPrice)
								SubTotal=SubTotal+TotalPrice
								if cint(rsAddProdQtyNew("QTY"))>1 and rsAddProdQtyNew("Price")>0 and len(Session("offerid"))=0 then
		                       ' if PeRT3QUOTE_YN(QuoteID) then
                                'else
										strBody=strBody & LoadI18N("SLBCA0030",localeID) & ": " & FormatCurrency(TotalPrice,2) & strTab
		                        'end if	
							
								end if
								'	if cstr(rsAddProdQtyNew("Price"))<>"0"  then
								'		strBody=strBody  & FormatCurrency(rsAddProdQtyNew("PRICE"),2) & strTab 
								'	else
								'		strBody=strBody  & "Free"  & strTab 
								'	end if
							'end if
							if cint(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font")) > 0 then
									strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font"))-1)  & strCret  & strCret    
							else
									strBody = strBody & strCret & "   " & rsAdditionalBasketNew("ProdDesc") & strCret  & strCret 
							end if 	
							'strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,Instr(rsAdditionalBasketNew("ProdDesc"),"<font")-1)  & strCret  & strCret    

							rsAddProdQtyNew.MoveNext
						Wend
					
					end if

'Close recordset rsAddProdQtyNew
					rsAddProdQtyNew.Close
					set rsAddProdQtyNew=nothing
					
					
					rsAdditionalBasketNew.MoveNext
				Wend
			end if
			
'Close recordset rsAdditionalBasketNew
			rsAdditionalBasketNew.Close
			set rsAdditionalBasketNew=nothing
			

			strBody=strBody &  strCRet &  strCRet		
			rsAdditionalGroupNew.MoveNext
		Wend
		
'Show subTotal for additional options
	if len(Session("Country"))>0 then
		if cstr(Session("Country"))="United States" then
			strBody=strBody & strCret & LoadI18N("SLBCA0026",localeID) & " " & translation("CATEGORY","CATEGORY_NAME","CATEGORY_ID",strCategoryId) & ": "
			if SubTotal>0 then
				strBody=strBody & FormatCurrency(SubTotal,2) & strCret & strCret
			else
				strBody=strBody & LoadI18N("SLBCA0010",localeID)& strCret & strCret
			end if

			Total=Total+SubTotal
		end if
	end if
end if
    Session("strPageBody")=strPageBody
	'if len(Session("offerid"))=0 then
		Session("QuoteTotalPrice")=Total
	'else
	'	Session("QuoteTotalPrice")=GetOfferInfo(Session("offerid"),2)
	'	Total=GetOfferInfo(Session("offerid"),2)
	'end if
'Close recordset rsAdditionalGroupNew
rsAdditionalGroupNew.Close
set rsAdditionalGroupNew=nothing
'***************************************
'Show Additional options if they exist
'***************************************
	
	Set rsAdditionalGroupNew = Server.CreateObject("ADODB.Recordset")
		strSQL =	"SELECT DISTINCT PROPERTYGROUP.PROPERTYGROUPNAME AS Expr1, " &_
					"SHOPPERQUOTE.PROPERTYGROUPID AS Expr2, PROPERTYGROUP.SORTID " &_
					"FROM SHOPPERQUOTE INNER JOIN " &_
					"PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = " &_
					"PROPERTYGROUP.PROPERTYGROUPID WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID  &_
					"and (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='') and PROPERTYGROUP.CATEGORY_ID not in (19,24,25,2,30,31) ORDER BY PROPERTYGROUP.SORTID"
	rsAdditionalGroupNew.Open strSQL, dbconnstr()
	if not rsAdditionalGroupNew.EOF then
		SubTotal=0
		rsAdditionalGroupNew.MoveFirst
		if CheckLogicAnalyzerQuote(QuoteID) then
            strBody = strBody & strCret & strCret & "Logic Analyzers" & strCret & strCret
		else
	         strBody = strBody & strCret & strCret & LoadI18N("SLBCA0027",localeID) & strCret & strCret		
	    end if
	    if len(strPageBody)>0 then
			strPageBody=strPageBody  & strCret &"Options"
		else
			strPageBody="Options"
		end if
		
		While not rsAdditionalGroupNew.EOF
			
			
		'For each group create string of product IDs strProducts
		'and string of product Infos strProductsInfo and UpdateTotalPrice
			
			strProducts=""
			strProductsInfo=""
			UpdateTotalPrice=0

			Set rsAdditionalBasketNew = Server.CreateObject("ADODB.Recordset")
				strSQL =	"SELECT  SHOPPERQUOTE.QTY AS Qty, PRODUCT.PARTNUMBER AS PartNum, replace(PRODUCT.PRODUCTDESCRIPTION,'<br>','" & strCret & "') AS ProdDesc, " &_
							"SHOPPERQUOTE.PRODUCTID, PRODUCT.PROPERTYGROUPID as GroupID, PROPERTYGROUP.PROPERTYGROUPNAME, PROPERTYGROUP.SORTID " &_
							"FROM SHOPPERQUOTE INNER JOIN " &_
							"PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN " &_
							"PROPERTYGROUP ON PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " &_
							"WHERE (SHOPPERQUOTE.QUOTEID =" & QuoteID & ") AND (SHOPPERQUOTE.MASTERPRODUCTID=0 or SHOPPERQUOTE.MASTERPRODUCTID='')  AND (PRODUCT.PROPERTYGROUPID = '" & rsAdditionalGroupNew("Expr2") & "') " &_
							"ORDER BY PROPERTYGROUP.SORTID"
	      
	        rsAdditionalBasketNew.Open strSQL, dbconnstr()
			
		'Check to see if any items exist in basket
			if not rsAdditionalBasketNew.EOF or BOF then
								
				rsAdditionalBasketNew.MoveFirst
	
			    While not rsAdditionalBasketNew.EOF
		'Get Quantity of each product in basket
					Set rsAddProdQtyNew = Server.CreateObject("ADODB.Recordset")
						strSQL = "SELECT QTY AS Qty,PRICE FROM SHOPPERQUOTE WHERE PRODUCTID = '" & rsAdditionalBasketNew("PRODUCTID") & "' AND QUOTEID = " & QuoteID	& " and (MASTERPRODUCTID=''or MASTERPRODUCTID=0)"				

					rsAddProdQtyNew.Open strSQL, dbconnstr()
					
					if not rsAddProdQtyNew.EOF then
						rsAddProdQtyNew.MoveFirst
						cnt=cnt+1
						textcount="text"&cnt
						bincount="bin" & cnt
						btncount="btn" & cnt
						minuscnt="minuscnt" & cnt

						While not rsAddProdQtyNew.EOF
							strBody = strBody & rsAddProdQtyNew("qty") & "  "
						
							if Len(rsAdditionalBasketNew("PartNum")) > 8 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab
							elseif Len(rsAdditionalBasketNew("PartNum"))>8 and len(rsAdditionalBasketNew("PartNum"))<12 then
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab
							else
								strBody = strBody & rsAdditionalBasketNew("PartNum") & strTab & strTab & strTab 
							end if
							


							'if len(Session("offerid"))=0 then
								'TotalPrice=cint(rsAddProdQtyNew("QTY")) * (GetProductPriceonProductID(rsAdditionalBasketNew("PRODUCTID")))
								'Call CheckNewTotal (SubTotal,TotalPrice)
								TotalPrice=(ProductQty * ProductPrice)
								'SubTotal=SubTotal+TotalPrice
								if len(Session("Country"))>0 then
									if cstr(Session("Country"))="United States" then

										if rsAddProdQtyNew("Price")>0 then
											strBody=strBody  & FormatCurrency(rsAddProdQtyNew("Price"),2) & strTab
										else
											if len(Session("offerid"))=0 then'and rsAdditionalBasketNew("PRODUCTID")<>1200 and rsAdditionalBasketNew("PRODUCTID")<>1201 and rsAdditionalBasketNew("PRODUCTID")<>535 and rsAdditionalBasketNew("PRODUCTID")<>1151 and rsAdditionalBasketNew("PRODUCTID")<>570 then
												strBody=strBody  & LoadI18N("SLBCA0010",localeID)  & strTab 
											else
												strBody=strBody  & "Free"  & strTab 
											end if
							
										end if
									else
										if rsAddProdQtyNew("PRICE")=0 and len(Session("offerid"))>0 then
											strBody=strBody  & "Free"  & strTab 
										end if
									end if
								end if
						

							'else
								if  len(rsAddProdQtyNew("Price"))>0 and cstr(rsAddProdQtyNew("Price"))<>"0"  then
									ProductPrice = csng(rsAddProdQtyNew("Price"))
								else
									ProductPrice=0
								end if	
								ProductQty = cint(rsAddProdQtyNew("QTY"))/1
								'Response.Write ProductQty
								TotalPrice=(ProductQty * ProductPrice)
								Call CheckNewTotal (SubTotal,TotalPrice)
								SubTotal=SubTotal+TotalPrice
								if cint(rsAddProdQtyNew("QTY"))>1 and rsAddProdQtyNew("Price")>0 and len(Session("offerid"))=0 then
		                        'if PeRT3QUOTE_YN(QuoteID) then
                                'else
										strBody=strBody & LoadI18N("SLBCA0030",localeID) & ": " & FormatCurrency(TotalPrice,2) & strTab
		                        'end if	
							
								end if
								'	if cstr(rsAddProdQtyNew("Price"))<>"0"  then
								'		strBody=strBody  & FormatCurrency(rsAddProdQtyNew("PRICE"),2) & strTab 
								'	else
								'		strBody=strBody  & "Free"  & strTab 
								'	end if
							'end if
							if cint(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font")) > 0 then
									strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,(Instr(1,rsAdditionalBasketNew("ProdDesc"),"<font"))-1)  & strCret  & strCret    
							else
									strBody = strBody & strCret & "   " & rsAdditionalBasketNew("ProdDesc") & strCret  & strCret 
							end if 	
							'strBody = strBody & strCret & "   " & mid(rsAdditionalBasketNew("ProdDesc"),1,Instr(rsAdditionalBasketNew("ProdDesc"),"<font")-1)  & strCret  & strCret    

							rsAddProdQtyNew.MoveNext
						Wend
					
					end if

'Close recordset rsAddProdQtyNew
					rsAddProdQtyNew.Close
					set rsAddProdQtyNew=nothing
					
					
					rsAdditionalBasketNew.MoveNext
				Wend
			end if
			
'Close recordset rsAdditionalBasketNew
			rsAdditionalBasketNew.Close
			set rsAdditionalBasketNew=nothing
			

			strBody=strBody &  strCRet &  strCRet		
			rsAdditionalGroupNew.MoveNext
		Wend
		
'Show subTotal for additional options
	if len(Session("Country"))>0 then
		if cstr(Session("Country"))="United States" then
		    if CheckLogicAnalyzerQuote(QuoteID) then
			    strBody=strBody & strCret & LoadI18N("SLBCA0026",localeID) & " Logic Analyzers: "
            else
			    strBody=strBody & strCret & LoadI18N("SLBCA0026",localeID) & " " & LoadI18N("SLBCA0027",localeID) & ": "
			end if
			if SubTotal>0 then
				strBody=strBody & FormatCurrency(SubTotal,2) & strCret & strCret
			else
				strBody=strBody & LoadI18N("SLBCA0010",localeID)& strCret & strCret
			end if

			Total=Total+SubTotal
		end if
	end if
end if
    Session("strPageBody")=strPageBody
	'if len(Session("offerid"))=0 then
		Session("QuoteTotalPrice")=Total
	'else
	'	Session("QuoteTotalPrice")=GetOfferInfo(Session("offerid"),2)
	'	Total=GetOfferInfo(Session("offerid"),2)
	'end if
'Close recordset rsAdditionalGroupNew
rsAdditionalGroupNew.Close
set rsAdditionalGroupNew=nothing

'**************TOTAL
if not cstr(Session("COUNTRY")) = "United States" or Session("COUNTRY") = "Select Country" then
	EMailBodyNew=strBody& strCRet & strCRet & strCRet
else
	
	if len(Session("Country"))>0 then
		if cstr(Session("Country"))="United States" then
			if Total>0 then
				strBody=strBody & LoadI18N("SLBCA0006",localeID)& ": " & FormatCurrency(Total,2) & strCRet & strCRet & strCRet
			else
				strBody=strBody & LoadI18N("SLBCA0006",localeID)& ": " & LoadI18N("SLBCA0010",localeID)& strCRet & strCRet & strCRet
			end if
		end if
	end if
		EMailBodyNew=strBody
end if

end function
'*******************************************************
'Loads label for current localeid
'rev 1 KK 6/1/01
'rev 2 KK 1/20/2004
'rev 3 KK 9/19/2007 (changed European translations to English)
'Arguments - Label,LocaleID
'Returns a string
'*******************************************************
Function LoadI18N(Label,LocaleID)
    if len(Label)>0 and LocaleID>0 then
		if LocaleID=1031 or LocaleID=1040 or LocaleID=1036 then
			 LocaleID=1033
		end if
		Set rsLabels=server.CreateObject("ADODB.Recordset")
		'strSQL="Select TEXT from I18NTEXT where LOCALEID=" & LocaleID & " and LABEL='" & Label & "'"
		Set cmdLabels=server.CreateObject("ADODB.COmmand")
		cmdLabels.CommandText="sp_LoadLabels"'strSQL
		cmdLabels.ActiveConnection=dbconnstr()
		'cmdLabels.CommandType=1'TEXT
		cmdLabels.CommandType=4'Store Procedure
		set ParamLOCALEID=cmdLabels.CreateParameter("@LOCALEID",3,1)'3 adInteger, 1 adParamInput
		cmdLabels.Parameters.Append ParamLOCALEID
		cmdLabels.Parameters("@LOCALEID")= LocaleID
		set ParamLabel=cmdLabels.CreateParameter("@Label",200,1,9)'200 varchar, 1 adParamInput
		cmdLabels.Parameters.Append ParamLabel
		cmdLabels.Parameters("@Label")= Label
		set rsLabels=cmdLabels.Execute
		'rsLabels.Open strSQL, dbconnstr()
		if not rsLabels.EOF then
			LoadI18N=trim(rsLabels("Text"))
		else  
			'LoadI18N=" Label " & Label & " "
			set rsLabelsUS=server.CreateObject("ADODB.Recordset")
			strSQL="Select TEXT from I18NTEXT where LOCALEID=1033 and LABEL='" & Label & "'"
			rsLabelsUS.Open strSQL, dbconnstr()
			if not rsLabelsUS.EOF then
				LoadI18N=trim(rsLabelsUS("Text"))
			else  
				LoadI18N=" Label " & Label & " "
			end if
		end if
	end if
	'rsLabel.Close
	'set rsLabel=nothing
End Function

'*****************************************************
' Author:           Nick Benes
' Date:             Jan. 05, 1999
' Description:      Returns a string with Client Data
' Revisions:        0
'***********************************************************
'Modified by Kate Kaplan 5/25/2001 
'Modified by Kate Kaplan 01/14/2004
'Modified by Kate Kaplan 01/05/2010
'***********************************************************

Function EmailBodyClientData(strContactID)
    Dim strBody
	Dim strCRet	
	if len(emaillocaleID)=0 then emaillocaleID=1033
	strCRet = chr(13) & chr(10)
    if len(strContactID)>0 then
        set rs=Server.CreateObject("ADODB.Recordset")
        strSQL="Select  * from CONTACT where CONTACT_ID=" & strContactID
        'response.Write strSQL
        rs.Open strSQL, dbconnstr()
        if not rs.eof then

                if emaillocaleID<>1041 then	
            	
	                ' Client data
	                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": " & rs("Honorific") & " " & rs("First_Name") & " " & rs("Last_Name") & strCRet
	                if len((rs("Title")))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleid) & ": " & rs("Title")& strCRet
	                end if
	                if len(rs("Department"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0465",emaillocaleid) & ": " & rs("Department")& strCRet
	                end if
	                if len(rs("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleid) & ": " & rs("Company")& strCRet
	                end if
	                if len(rs("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleid) & ": " & rs("Address")& " " & rs("Address2") & strCRet
	                end if
	                if len(rs("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleid) & ": " & rs("City")& strCRet
	                end if
	                if len(rs("STATE_PROVINCE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0081",emaillocaleid) & ": " & rs("STATE_PROVINCE")& strCRet
	                end if
	                if len(rs("POSTALCODE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleid) & ": " & rs("POSTALCODE")& strCRet
	                end if
	                if len( rs("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleid) & ": " & rs("Country")& strCRet
	                end if
	                if len(rs("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleid) & ": " & rs("Phone")& strCRet
	                end if
	                if len(rs("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleid) & ": " & rs("Fax")& strCRet
	                end if
	                if len(rs("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleid) & ": mailto:" & rs("Email")& strCRet
	                end if
	              '  if len(rs("URL"))>0 then
		           '     strBody = strBody & LoadI18N("SLBCA0087",emaillocaleid) & ": " & rs("URL")& strCRet
	                'end if
	                'if len(rs("APPLICATION"))>0 then
		             '   strBody = strBody & LoadI18N("SLBCA0088",emaillocaleid) & ": " & rs("APPLICATION")& strCRet
	                'end if

	                ' Added Technical Support Question 2-3-99 Adrian Cake
	                'if len(Request.QueryString("Remarks4"))>0 then
		             '   strBody = strBody & LoadI18N("SLBCA0089",emaillocaleid) & ": " & Request.QueryString("Remarks4")& strCRet
	                'end if
                else
	                ' Client data for Japan
	                'response.Write "test" & len(rs("Country"))
                    if len(rs("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleID) & ": " & rs("Country")& strCRet
	                end if
	                	

	                if len(rs("POSTALCODE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleID) & ": " & rs("POSTALCODE")& strCRet
	                end if
	                	

	                if len(rs("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleID) & ": " & rs("City")& strCRet
	                end if
	                	

	                if len(rs("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleID) & ": " & rs("Address")& strCRet
	                end if
	                	

	                if len(rs("Address2"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleID) & "2: " & rs("Address2")& strCRet
	                end if
	                	

	                if len(rs("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleID) & ": " & rs("Company")& strCRet
	                end if
	                	

	                if len(rs("Title"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleID) & ": " & rs("Title")& strCRet
	                end if
	                	

	                if len(rs("FIRST_NAME"))>0 and  len(rs("Last_Name"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": "  &  rs("Last_Name")& " " & rs("FIRST_NAME") & " " & LoadI18N("SLBCA0160",emaillocaleID)& strCRet
	                end if
	                	

	                if len(rs("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleID) & ": " & rs("Phone")& strCRet
	                end if
	                
	                if len(rs("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleID) & ": " & rs("Fax")& strCRet
	                end if
	                
	                if len(rs("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleID) & ": " & rs("Email")& strCRet
	                end if
	                
	                if len(rs("URL"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0087",emaillocaleID) & ": " & rs("URL")& strCRet
	                end if
	                
	                if len(rs("APPLICATION"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0088",emaillocaleID) & ": " & rs("APPLICATION")& strCRet
	                end if
	                
	                ' Added Technical Support Question
	                if len(Request.QueryString("Remarks4"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0089",emaillocaleID) & ": " & Request.QueryString("Remarks4")& strCRet
	                end if
	                
                end if
        else
        	                ' Client data
	                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": " & Session("Honorific") & " " & Session("FirstName") & " " & rSessions("LastName") & strCRet
	                if len((Session("Title")))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleid) & ": " & Session("Title")& strCRet
	                end if
	                if len(Session("Department"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0465",emaillocaleid) & ": " & Session("Department")& strCRet
	                end if
	                if len(Session("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleid) & ": " & Session("Company")& strCRet
	                end if
	                if len(Session("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleid) & ": " & Session("Address")& " " & Session("Address2") & strCRet
	                end if
	                if len(Session("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleid) & ": " & Session("City")& strCRet
	                end if
	                if len(Session("State"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0081",emaillocaleid) & ": " & Session("Stage")& strCRet
	                end if
	                if len(Session("Zip"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleid) & ": " & Session("Zip")& strCRet
	                end if
	                if len( Session("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleid) & ": " & Session("Country")& strCRet
	                end if
	                if len(Session("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleid) & ": " & Session("Phone")& strCRet
	                end if
	                if len(Session("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleid) & ": " & Session("Fax")& strCRet
	                end if
	                if len(Session("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleid) & ": mailto:" & Session("Email")& strCRet
	                end if
	                if len(Session("URL"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0087",emaillocaleid) & ": " & Session("URL")& strCRet
	                end if
	                if len(Session("APPLICATION"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0088",emaillocaleid) & ": " & Session("APPLICATION")& strCRet
	                end if
	    end if
	end if
	' Insert empty line 
	strBody = strBody & strCRet
	' Return data 
	EmailBodyClientData = strBody
End Function

'*******************************************************
'Check if product is a new product
'YK 6/1/01
'Arguments - ProdID
'Returns a string
'*******************************************************
function checkproductisnew(ProdID)
'Select DateUpdated from Product table
'and check if this date is less than 30 days from now
'if so show the word "NEW"
	set rsProductDateUpdated=server.CreateObject("ADODB.Recordset")
	strSQL="Select DATEUPDATED from PRODUCT where PRODUCTID='" & ProdID & "'"
	rsProductDateUpdated.Open strSQL, dbconnstr()
	if not rsProductDateUpdated.EOF then
	    if DateDiff("d",rsProductDateUpdated("DATEUPDATED"),now())<=60 then
			checkproductisnew=LoadI18N("SLBCA0112",localeID)& "&nbsp;"
		end if 
	end if
end function
	

'*************************************
'Create Customer Info for customer email
'YK 6/5/01
'modified by KK 11/4/2004
'Arguments - CustomerID
'Returns string
'*************************************

function CustomerData(CustomerID)

'Select everything from CONTACT table for CustomerID
if len(CustomerID)>0 then
set rsCustomerData=server.CreateObject("ADODB.Recordset")
strSQL="Select * from CONTACT where CONTACT_ID=" & CustomerID
'response.Write strSQL
'response.end
rsCustomerData.Open strSQL, dbconnstr()
if not rsCustomerData.EOF then
	strCRet = chr(13) & chr(10)
		
	' Client data
	strCustomerInfo = "Client: " & rsCustomerData("Honorific") & " " & rsCustomerData("First_Name") & " " & rsCustomerData("Last_Name") & strCRet
	if len(trim(rsCustomerData("Title")))>0 then
		strCustomerInfo = strCustomerInfo & "Title: " & rsCustomerData("Title")& strCRet
	end if
	if len(trim(rsCustomerData("Company")))>0 then
		strCustomerInfo = strCustomerInfo & "Company: " & rsCustomerData("Company")& strCRet
	end if
	if len(trim(rsCustomerData("Address")))>0 then
		strCustomerInfo = strCustomerInfo & "Address: " & rsCustomerData("Address")& " " & rsCustomerData("Address2") & strCRet
	end if
	if len(trim(rsCustomerData("City")))>0 then
		strCustomerInfo = strCustomerInfo & "City: " & rsCustomerData("City")& strCRet
	end if
	if len(trim(rsCustomerData("State_Province")))>0 then
		strCustomerInfo = strCustomerInfo & "State: " & rsCustomerData("State_Province")& strCRet
	end if
	if len(trim(rsCustomerData("PostalCode")))>0 then
		strCustomerInfo = strCustomerInfo & "Zip: " & rsCustomerData("PostalCode")& strCRet
	end if
	if len(trim(rsCustomerData("Country")))>0 then
		strCustomerInfo = strCustomerInfo & "Country: " & rsCustomerData("Country")& strCRet
	end if
	if len(trim(rsCustomerData("Phone")))>0 then
		strCustomerInfo = strCustomerInfo & "Phone: " & rsCustomerData("Phone")& strCRet
	end if
	if len(trim(rsCustomerData("Fax")))>0 then
		strCustomerInfo = strCustomerInfo & "Fax: " & rsCustomerData("Fax")& strCRet
	end if
	if len(trim(rsCustomerData("Email")))>0 then
		strCustomerInfo = strCustomerInfo & "Email: " & rsCustomerData("Email")& strCRet
	end if
	'if len(trim(rsCustomerData("URL")))>0 then
	'	strCustomerInfo = strCustomerInfo & "URL: " & rsCustomerData("URL")& strCRet
	'end if
	'if len(trim(rsCustomerData("APPLICATION")))>0 then
	'	strCustomerInfo = strCustomerInfo & "Application: " & rsCustomerData("APPLICATION")& strCRet
	'end if

	CustomerData= strCustomerInfo
end if
end if
end function
'*************************************
'Select translation
'rev1 KK 7/27/01
'rev2 KK 9/18/07 (Europe - English only)  
'Arguments -tablename,columnname, id
'Returns string
'*************************************

function translation(tablename,columnname,columnid,id)

if len(Session("localeid"))=0 then Session("localeid")=1033
localeid=Session("localeid")

if Session("localeid")=1031 or Session("localeid")=1040 or Session("localeid")=1036 then
	localeid=1033
end if

if cint(localeid)=1033 then
	set rsUsTranslation=CreateObject("ADODB.Recordset")
	strSQL="Select " & columnname & " as VALUE from " & tablename &  " where " & columnid & "=" & id
	Set cmdUsTranslation=server.CreateObject("ADODB.COmmand")
	cmdUsTranslation.CommandText=strSQL
	cmdUsTranslation.ActiveConnection=dbconnstr()
	cmdUsTranslation.CommandType=1'TEXT
	set rsUsTranslation=cmdUsTranslation.Execute
	'rsUsTranslation.Open strSQL, dbconnstr()
	if not rsUsTranslation.EOF then
		translation=rsUsTranslation("VALUE")
	else
		translation=""
	end if
	rsUsTranslation.Close
	set rsUsTranslation=nothing
	set cmdUsTranslation=nothing
else
	'select tableid from table "TABLE_ID" (ecatalog)
	set rsTable=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Table_id From Table_id where Table_name='" & tablename & "'"
	'Response.Write strSQL
	rsTable.Open strSQL, dbconnstr()
	if not rsTable.EOF then
		TableID=rsTable("Table_ID")
	end if
	'Response.write TableID & "<br>"
	'close recordset rsTable
	rsTable.Close
	set rsTable=nothing

	'select columnid from table"COLUMN_ID" (ecatalog)
	set rsColumn=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Column_id From Column_id where Column_name='" & columnname & "'"
	rsColumn.Open strSQL, dbconnstr()
	if not rsColumn.EOF then
		ColID=rsColumn("Column_ID")
	end if
	'Response.Write ColumnID & "<br>"
	'Response.Write ID
	'close recordset  rsColumn
	 rsColumn.Close
	set  rsColumn=nothing

	'Select value from table "TABLESBYLANGUAGE" (ecatalog)
	'for TableID,COlumnID,ID and LOCALEID
	set rsTranslation=server.CreateObject("ADODB.Recordset")
	strSQL=	"Select VALUE from TABLESBYLANGUAGE where TABLE_ID=" & TableID & _
			" and COLUMN_ID=" & ColID & " and IDINTABLE=" & ID  & _
			" and LOCALEID=" & localeid
			
			'Response.Write strSQL
	rsTranslation.Open strSQL, dbconnstr()
	if not rsTranslation.EOF then
		translation=rsTranslation("Value")
	else
		'translation="No Translation exists"
			set rsUsTranslation1033=CreateObject("ADODB.Recordset")
			strSQL="Select " & columnname & " as VALUE from " & tablename &  " where " & columnid & "=" & id
			if ucase(tablename)="SURVEYQUESTION" then
				rsUsTranslation1033.Open strSQL, dbconnstr()
			else
				rsUsTranslation1033.Open strSQL, dbconnstr()
			end if
			if not rsUsTranslation1033.EOF then
				translation=rsUsTranslation1033("VALUE")
			else
				translation=""
			end if
	end if
	rsTranslation.Close
	set rsTranslation=nothing
end if
end function


'*************************************
'Get property group category id on product id
'KK 10/23/01
'modified by KK 1/20/2004 (add Command object)
'Arguments - prodid
'Returns integer
'*************************************
function getpropgroupcatidonprodid(prodid)
	if len(prodid) > 0 then
		Set rsPropCat = Server.CreateObject("ADODB.Recordset")
		'strSQL =" SELECT     PROPGROUPCATEGORY.CATEGORY_ID AS CATEGORY_ID " & _
		'		"FROM  PRODUCT INNER JOIN PROPERTYGROUP ON " & _
		'		"PRODUCT.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID " & _
		'		"INNER JOIN PROPGROUPCATEGORY ON " & _
		'		"PROPERTYGROUP.CATEGORY_ID = PROPGROUPCATEGORY.CATEGORY_ID " & _
		'		" Where PRODUCT.PRODUCTID=" & prodid
		Set PropCat=server.CreateObject("ADODB.COmmand")
		
		'Response.Write strSQL
		PropCat.CommandText="sp_getpcatidonprodid"'strSQL
		PropCat.ActiveConnection=dbconnstr()
		PropCat.CommandType=4'TEXT
		'create Parameter
		set ParamPRODUCTID=PropCat.CreateParameter("@PRODUCTID",3,1)'3 adIntereg, 1 adParamInput
		PropCat.Parameters.Append ParamPRODUCTID
		PropCat.Parameters("@PRODUCTID")= Trim(Ucase(prodid))
		set rsPropCat=PropCat.Execute
		'rsPropCat.Open strSQL, dbconnstr()
		
		if not rsPropCat.EOF then
			getpropgroupcatidonprodid=rsPropCat("CATEGORY_ID")
		else
			getpropgroupcatidonprodid=0
		end if
	else
		getpropgroupcatidonprodid=0
	end if
end function


'*************************************
'Display Optional Products recommended for purchase
'YK 6/25/01
'Arguments - prodid
'Returns boolean
'*************************************
function CheckProductHasOptions	(intProdID)
	Set rsCheckProductOptions = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM V_PRODUCT_OPTIONS WHERE PRODUCTID='" & intProdID & "' and STD = 'n'"
	rsCheckProductOptions.Open strSQL, dbconnstr()
	if not rsCheckProductOptions.EOF then
		CheckProductHasOptions=true
	else
		CheckProductHasOptions=false
	end if

end function

'*****************************************************************
'Create a list of products not available for selected localeid
'YK 1/7/02
'Arguments - localeid
'Returns string
'*****************************************************************
function productsdisabledforlocale(localeid)
if len(localeid)>0 then
	set rsProductDisabled=server.CreateObject("ADODB.Recordset")
	strSQL="Select * from PRODUCTBYLOCALEID where LOCALE_ID=" & localeid
	rsProductDisabled.Open strSQL, dbconnstr()

	if not rsProductDisabled.EOF then
		while not rsProductDisabled.EOF
			if len(strDisabledProductList)=0 then
				strDisabledProductList=rsProductDisabled("PRODUCT_ID")
			else
				strDisabledProductList=strDisabledProductList & "," & rsProductDisabled("PRODUCT_ID")
			end if	
		rsProductDisabled.MoveNext
		wend
	else
		strDisabledProductList=""
	end if
else
	strDisabledProductList=""
end if
productsdisabledforlocale=strDisabledProductList
end function

'*****************************************************************
'Create a list of products disabled in PRODUCT table
'KK 1/13/02
'Modified KK 10/30/2003
'Modified KK 01/20/2004 (Add Command object)
'Arguments - none
'Returns string
'*****************************************************************
function GetDisabledProducts()
	set rsProductDisabled=server.CreateObject("ADODB.Recordset")
	if len(Session("localeid"))>0 then
		if Session("localeid")="1033" then
			strSQL="Select PRODUCTID from PRODUCT  where ( DISABLED='y' ) or productid in (SELECT DISTINCT OPTIONID FROM CONFIG WHERE (STD = 'y')  AND (OPTIONID NOT IN (SELECT optionid FROM CONFIG WHERE std = 'n'))) "
		else
			strSQL="Select PRODUCTID from PRODUCT where ( DISABLED='y' and productid not in (select PRODUCTID FROM PRODUCTSENABLEDBYLOCALEID where localeid=" & Session("localeid") & ") ) or productid in (select PRODUCT_ID FROM PRODUCTBYLOCALEID where locale_id=" & Session("localeid") & ") or productid in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
		end if
	else
		strSQL="Select PRODUCTID from PRODUCT where  DISABLED='y' or productid in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
	end if
	'Response.Write strSQL
	Set cmdProductDisabled=server.CreateObject("ADODB.COmmand")
	cmdProductDisabled.CommandText=strSQL
	cmdProductDisabled.ActiveConnection=dbconnstr()
	cmdProductDisabled.CommandType=1'TEXT
	set rsProductDisabled=cmdProductDisabled.Execute
	'Response.Write strSQL
	'rsProductDisabled.Open strSQL, dbconnstr()
	
	if not rsProductDisabled.EOF then
		while not rsProductDisabled.EOF
			if len(strDisabledProductList)=0 then
				strDisabledProductList=rsProductDisabled("PRODUCTID")
			else
				strDisabledProductList=strDisabledProductList & "," & rsProductDisabled("PRODUCTID")
			end if	
		rsProductDisabled.MoveNext
		wend
	else
		strDisabledProductList=""
	end if
	
rsProductDisabled.Close
set rsProductDisabled=nothing

GetDisabledProducts=strDisabledProductList
end function
'*****************************************************************
'Create a list of discontinued products disabled ecatalog db by localeid
'KK 2/4/2004
'Arguments - none
'Returns string
'*****************************************************************
function GetDiscontinuedProductsbyLocaleID()
	set rsProductDisabled=server.CreateObject("ADODB.Recordset")
	if len(Session("localeid"))>0 then
			strSQL="Select PRODUCTID from PRODUCT where  (DISABLED='y' and productid not in (select PRODUCTID FROM PRODUCTSENABLEDBYLOCALEID where localeid=" & Session("localeid") & ")  and productid not in (select PRODUCT_ID FROM PRODUCTBYLOCALEID where locale_id=" & Session("localeid") & ") ) or ( disabled='n' and productid in (select PRODUCT_ID FROM PRODUCTBYLOCALEID where locale_id=" & Session("localeid") & ") and productid not in (select PRODUCTID FROM PRODUCTSENABLEDBYLOCALEID where localeid=" & Session("localeid") & ")  )"'or productid not in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
	else
			strSQL="Select PRODUCTID from PRODUCT where  DISABLED='y' or productid not in (SELECT DISTINCT OPTIONID  FROM CONFIG WHERE (STD = 'y') AND (OPTIONID NOT IN  (SELECT optionid FROM  CONFIG  WHERE   std = 'n')))"
	end if
	'Response.Write strSQL
	Set cmdProductDisabled=server.CreateObject("ADODB.COmmand")
	cmdProductDisabled.CommandText=strSQL
	cmdProductDisabled.ActiveConnection=dbconnstr()
	cmdProductDisabled.CommandType=1'TEXT
	set rsProductDisabled=cmdProductDisabled.Execute
	'Response.Write strSQL
	'rsProductDisabled.Open strSQL, dbconnstr()
	
	if not rsProductDisabled.EOF then
		while not rsProductDisabled.EOF
			if len(strDisabledProductList)=0 then
				strDisabledProductList=rsProductDisabled("PRODUCTID")
			else
				strDisabledProductList=strDisabledProductList & "," & rsProductDisabled("PRODUCTID")
			end if	
		rsProductDisabled.MoveNext
		wend
	else
		strDisabledProductList=""
	end if
	
rsProductDisabled.Close
set rsProductDisabled=nothing

GetDiscontinuedProductsbyLocaleID=strDisabledProductList
end function

'*****************************************************************
'Returns "true" if product is disabled in PRODUCT table
'YK 1/31/02
'Arguments - prodid
'Returns boolean
'*****************************************************************
function CheckProductDisabled(prodid)
	if len(prodid)>0 then
		if len(GetDisabledProducts)>0 then
			set rsCheckProdDisabled=server.CreateObject("ADODB.Recordset")
			strSQL=	"Select * from PRODUCT where PRODUCTID in (" & GetDisabledProducts & ")"
			Set cmdCheckProdDisabled=server.CreateObject("ADODB.COmmand")
			cmdCheckProdDisabled.CommandText=strSQL
			cmdCheckProdDisabled.ActiveConnection=dbconnstr()
			cmdCheckProdDisabled.CommandType=1'TEXT
			set rsCheckProdDisabled=cmdCheckProdDisabled.Execute
			'rsCheckProdDisabled.Open strSQL, dbconnstr()
			
			if not rsCheckProdDisabled.EOF then
				while not rsCheckProdDisabled.EOF
					'Response.Write rsCheckProdDisabled("PRODUCTID") & "  "
					if cint(rsCheckProdDisabled("PRODUCTID"))=cint(prodid) then
						CheckProductDisabled=true
						exit function
					else
						CheckProductDisabled=false
					end if
			
				rsCheckProdDisabled.MoveNext
				wend
					
			else
				CheckProductDisabled=false
			end if
		else
			CheckProductDisabled=false
		end if
		
	else
		CheckProductDisabled=false
	end if
end function
'*****************************************************************
'Delete product from SHOPPER table
'YK 1/13/02
'Arguments - prodid

'*****************************************************************
sub DeleteFromBasket(prodid,masterid)
	if len(prodid)>0 then
	
		'if this is scope delete it and set masterproductid="" in shopper table for its options
		if ProdID=MasterID then
		'Select All Products from SHOPPER for this MASTER product for current Session
			set rsOptions=Server.CreateObject("ADODB.Recordset")
			strSQL=	"Select * from SHOPPER where  SESSIONID = '" & Session("netSessionID") & "' " &_
					"AND PRODUCTID!=MASTERPRODUCTID AND MASTERPRODUCTID='" & MasterID & "'"
			rsOptions.LockType=3
			rsOptions.Open strSQL, dbconnstr()
			if not rsOptions.EOF then
				while not rsOptions.EOF
			'Check if this product already exist in Additional options.
			'If yes, we update	QTY for product in Additional Options and delete current product
			'If not, we update current product and set MASTERPRODUCTID=''
			'Open recordset rsAdditonalOptions
					set rsAdditonalOptions=Server.CreateObject("ADODB.Recordset")
					strSQL=	"Select * from SHOPPER where SESSIONID = '" & Session("netSessionID") & "' " &_
							"AND (MASTERPRODUCTID ='' or MASTERPRODUCTID=0) AND PRODUCTID=" & rsOptions("PRODUCTID")
					rsAdditonalOptions.LockType=3
					rsAdditonalOptions.Open  strSQL, dbconnstr()
						
					if not rsAdditonalOptions.EOF then
						rsAdditonalOptions("QTY")=cint(rsAdditonalOptions("QTY"))+1
						rsAdditonalOptions.Update
						rsOptions.Delete
						rsOptions.Update
					else
						rsOptions("MASTERPRODUCTID")=""
						rsOptions.Update
					end if	
					
					rsOptions.MoveNext
				wend
			end if
		end if
		'Delete selected product
		set rsDelProduct=server.CreateObject("ADODB.Recordset")
		strSQL="Delete from SHOPPER where SESSIONID='" & Session("netSessionID") & "' and PRODUCTID=" & ProdID & " and MASTERPRODUCTID='" & MasterID & "'"
		rsDelProduct.Open strSQL, dbconnstr()
	end if
end sub

'*****************************************************************
'Returns "true" if product is disabled for current midelserie
'YK 1/31/02
'Arguments -modelseriesid
'Returns boolean
'*****************************************************************
function CheckProductExistForModelSerie(modelseriesid)
	set rsDisabledProduct=server.CreateObject("ADODB.Recordset")
	if len(GetDisabledProducts)>0 then
		strSQL="Select * from PRODUCT where MODEL_SERIES=" & modelseriesid &_
				" and PRODUCTID not in (" & GetDisabledProducts & ")"
	else
		strSQL="Select * from PRODUCT where MODEL_SERIES=" & modelseriesid 
	end if
	rsDisabledProduct.Open strSQL, dbconnstr()
	if not rsDisabledProduct.EOF then
		CheckProductExistForModelSerie=true
	else
		CheckProductExistForModelSerie=false
	end if
end function

'****************************************************************************************
'Display alternate products in model series (only applies to configurable products)
'KK 4/7/02
'Modified by KK 1/22/2004
'Modified by KK 10/25/2005 (no arrows)
'Arguments - prodid
'Returns string
'****************************************************************************************
function GetAlternateProducts(intProdID)
	Set rs2 = Server.CreateObject("ADODB.Recordset")
	'on error resume next
	if cint(getpropgroupcatidonprodid(Session("intProdID")))=13  then
	strSQL = "SELECT distinct PRODUCT.PRODUCTID AS PRODUCTID, PRODUCT.PARTNUMBER "&_
			 "AS PARTNUMBER, BAANPRICE.[Sales price] AS price "&_
			 "FROM PRODUCT LEFT OUTER JOIN BAANPRICE "&_
			 "ON PRODUCT.PRODUCTID = BAANPRICE.[Product ID] "&_
			 "WHERE PRODUCT.DISABLED='n' and (PRODUCT.PROPERTYGROUPID IN "&_
			 " (SELECT PROPERTYGROUP.propertygroupid FROM PROPERTYGROUP "&_
             "WHERE      CATEGORY_ID =" & getpropgroupcatidonprodid(Session("intProdID")) &_
             ")) AND (PRODUCT.PRODUCTID <> " & intProdID & ")ORDER BY BAANPRICE.[Sales price] DESC "
	elseif cint(getpropgroupcatidonprodid(Session("intProdID")))=1 then
	strSQL =	" SELECT     PRODUCTID AS PRODUCTID, PARTNUMBER AS PARTNUMBER FROM PRODUCT "&_
				" WHERE (PRODUCT.DISABLED='n'or (PRODUCTID IN (SELECT productid FROM PRODUCTSENABLEDBYLOCALEID " &_
				" WHERE localeid = " & Session("localeid")& "))) and (PRODUCT.MODEL_SERIES = " &_
				GetModelSeriesonProdID(intProdID) & ") AND (PRODUCT.PRODUCTID <> " & intProdID & ")" &_
				" ORDER BY SORT_ID"
	else
	strSQL =	" SELECT PRODUCT.PRODUCTID as PRODUCTID,PRODUCT.PARTNUMBER as PARTNUMBER, BAANPRICE.[Sales price] AS price FROM PRODUCT LEFT OUTER JOIN " &_
				" BAANPRICE ON PRODUCT.PRODUCTID = BAANPRICE.[Product ID] " &_
				" WHERE (PRODUCT.DISABLED='n'or (PRODUCTID IN (SELECT productid FROM PRODUCTSENABLEDBYLOCALEID WHERE localeid = " & Session("localeid")& "))) "&_
				" and (PRODUCT.PROPERTYGROUPID  = " & GetProdInfo(intProdID,2) &_
				") AND (PRODUCT.PRODUCTID <> " & intProdID & ")" &_
				" ORDER BY PARTNUMBER"
	end if
	'Response.Write strSQL
	Set cmdAltProducts=server.CreateObject("ADODB.COmmand")
	cmdAltProducts.CommandText=strSQL
	cmdAltProducts.ActiveConnection=dbconnstr()
	cmdAltProducts.CommandType=1'TEXT
	'create Parameter
	set rs2=cmdAltProducts.Execute
	if not rs2.EOF then
		
		Response.Write "<table width=100&#37; border=0 cellpadding=0 cellspacing=0>"
		Response.Write "<tr><td bgcolor=#000000><font face=Verdana,Arial color=#ffffff size=1><b>"
		if cint(getpropgroupcatidonprodid(Session("intProdID")))=13 or cint(getpropgroupcatidonprodid(Session("intProdID")))=19 then
			Response.Write  "Similar Products in Series"
		else
			Response.Write  LoadI18N("SLBCA0416",Session("localeID")) 
		end if
		Response.Write "</b></font></td></tr>"
		Response.Write "<tr><td>"
		Response.Write "<table width=100&#37; border=0 cellpadding=0 cellspacing=0>"
		Do While not rs2.EOF
		'Other Products in Series were found
			
			Response.Write "<tr>"
			Response.Write "<td  valign=top colspan=2><nobr>"
			Response.Write "&nbsp;<b><font face=Verdana,Arial color=#000000 size=1><a href=/shopper/configurequote.asp?prodid=" & rs2("productid") & ">" & rs2("PARTNUMBER") & "</a></font></b>"
			Response.Write "</nobr></td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
			Response.Write "<td  valign=top align=left width=5>"
			Response.Write "<IMG SRC=""/nav/images/1pixspacer.gif"" width=5>"
			Response.write "</td>"
			Response.Write "<td valign=top align=left>"
			Response.Write "<font face=""Verdana,Arial"" size=1 color=#000000>" 
			'& getspecstring(rs2("productid"),"7;39;45;38") & "</font>"
			Response.write translation("PRODUCT","PRODUCTDESCRIPTION","PRODUCTID",rs2("productid"))	
			Response.Write "</font>"
			Response.Write "</td>"

			Response.Write "</tr>"
		rs2.MoveNext
		Loop
		Response.Write "</table>"
		Response.Write "</td></tr></table>"
	end if	

end function

'*****************************************************************
'Returns  string of comma delitetered properties
'YK 4/22/02
'Arguments -prodid,strPropid
'Returns string
'*****************************************************************
function getspecstring(prodid,strPropid)
	i=1
	Start=1
	strAllProp=strPropid
	
	While not strAllProp = ""
    L = InStr(strAllProp, ";")
    'Response.Write L
		If L = 0 Then
			propid = strAllProp
			strAllProp = ""
		Else
			propid = Mid(strAllProp, Start, (L - Start))
			strAllProp = Trim(Mid(strAllProp, L+1, Len(strAllProp)))
		End If
		
		
		redim preserve props(i)
		props(i)=propid
		'Response.Write props(i) & "<br>"
		i=i+1
	Wend
	
	'get property value from PRODUCTPROPVALUe table
	if cint(i)>1 then
		for j=1 to i-1
	
			set rsPropValue=server.CreateObject("ADODB.Recordset")
			strSQL="Select * from PRODUCTPROPVALUE where PRODUCTID=" & prodid & " and PROPERTYID=" & props(j)
			
			rsPropValue.Open strSQL, dbconnstr()
			if not rsPropValue.EOF then
			
				if len(rsPropValue("VALUE"))>0 then
					
					if len(strPropValue)=0 then
						if cint(props(j))=39 then
							strPropValue=rsPropValue("VALUE") & " Ch"
						else
							strPropValue=rsPropValue("VALUE")
						end if
					else
						if cint(props(j))=39 then
							strPropValue=strPropValue  & ", " & rsPropValue("VALUE") & " Ch"
						else
							strPropValue=strPropValue & ", " & rsPropValue("VALUE")
						end if
					end if
				end if
			end if
			
		next
	else
		strPropValue=""
	end if

	getspecstring= strPropValue
end function
'*****************************************************************
'Returns  array of a string that is comma demilimited
'SS 4/8/04 list
'Returns array of the parsed string
'*****************************************************************
function parseString(list)
	'create array of model_series ids
	i=1
	Start=1
	strAll=cstr(list)
	
	While not strAll = ""
		L = InStr(strAll, ",")
		If L = 0 Then
			modelid = strAll
			strAll = ""
		Else
			modelid = Mid(strAll, Start, (L - Start))
			strAll = Trim(Mid(strAll, L+1, Len(strAll)))
		End If
		redim preserve models1(i)
		models1(i)=modelid
		i=i+1
	Wend
	Session("temp") = models1
	
	parseString = Session("temp")
end function 

'*****************************************************************
'This function compares a product in an string that consists of other productID's 
'SS 4/13/04 
'Returns true if the product is found in the string
'*****************************************************************
function compareString(notes,productID)
	if len(notes) > 0 then 
		noteProductIDs = parseString(notes)
		for j=1 to ubound(noteProductIDs) 
			if cint(noteProductIDs(j)) = cint(productID) then 
				compareString = true
			end if
		next
	else
		compareString = false
	end if 
end function

'*****************************************************************
'This function displays the compatible code for software/hardware options.
'SS 4/13/04 
'Returns nothing.
'*****************************************************************
function displaySoftwareOptionsCodeProducts(compatible_code, note1, note2)
	models = parseString(compatible_code)
	if cint(ubound(models))>=1 then
		for k= 1 to ubound(models)
			set rsCode=server.CreateObject("ADODB.Recordset")
			strSQL="Select * from MODELSERIES where model_series_id=" &  models(k)
			'Response.Write strSQL
			rsCode.Open strSQL, dbconnstr()
			if k=1 then 
				temp = ""
			else
				temp = ", "
			end if 
			if (len(note1) > 0) or (len(note2)> 0)  then 
				if (compareString(note1,models(k))) then 
					Response.Write temp & "<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"<sup>2</sup></a>"
				elseif (compareString(note2,models(k))) then 
					Response.Write temp & "<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"*</a>"
				else
					Response.Write temp & "<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"</a>"
				end if 
			else
				Response.Write temp & "<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"</a>"
			end if 
		next
	end if
end function 

'*****************************************************************
'Returns  string of comma delitetered modelSeries_code
'YK 4/24/02
'Arguments -strModelCodeID
'Returns string
'*****************************************************************
function GetModelCodeString(strModelCodeID)
	if len(strModelCodeID)>0 then
		models = parseString(strModelCodeID)
		'get property value from PRODUCTPROPVALUE table
		if cint(ubound(models))>=1 then
			for j=1 to ubound(models) 
				set rsCode=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from MODELSERIES where model_series_id=" &  models(j)
				rsCode.Open strSQL, dbconnstr()
				if not rsCode.EOF then
					strCode="<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"</a>"
					strCode=strCode & ", " & "<a title='" & rsCode("NAME") & "'>"& rsCode("MODELSERIES_CODE") &"</a>"
				else
					strCode=""
				end if
			next
			GetModelCodeString=strCode
		else
			GetModelCodeString=""
		end if
	else
		GetModelCodeString=""
	end if
end function

'*****************************************************************
'Returns  string of comma delitetered modelSeries_code
'AC 4/20/02 intProdID
'Modified by KK 1/22/2004
'Returns string
'*****************************************************************
function GetModelSeriesonProdID(intProdID)
	if len(intProdID)>0 then
		Set rsModSer = Server.CreateObject("ADODB.Recordset")
		Set cmdModSer=server.CreateObject("ADODB.COmmand")
		cmdModSer.CommandText="sp_GetModelSeriesonProdID"
		cmdModSer.ActiveConnection=dbconnstr()
		cmdModSer.CommandType=4'STORE PROCEDURE
		'create Parameter
		set ParamPRODUCTID=cmdModSer.CreateParameter("@PRODUCTID",3,1)'3 adIntereg, 1 adParamInput
		cmdModSer.Parameters.Append ParamPRODUCTID
		cmdModSer.Parameters("@PRODUCTID")= intProdID
		set rsModSer=cmdModSer.Execute
		'rsModSer.Open strSQL, dbconnstr()
		if not rsModSer.EOF then
			'Product is Scope
			GetModelSeriesonProdID = rsModSer("MODEL_SERIES")
		end if
	end if
end function

'*************************************
'Offer Info
'YK 4/29/2002
'arguments (offerid,dbfieldid)
'Returns String
'*************************************
function GetOfferInfo(campaignid,arrayid)
if len(campaignid )>0  then
	set rsOfferProduct=server.CreateObject("ADODB.Recordset")
	strSQL="Select TACTIC_ID from SUBSCRIPTION_CAMPAIGN where CAMPAIGN_ID=" & campaignid 
	'REsponse.Write strSQL
	'REsponse.end
	rsOfferProduct.Open strSQL, dbconnstr()
	if not rsOfferProduct.EOF then
			GetOfferInfo=rsOfferProduct("TACTIC_ID")
	end if
	rsOfferProduct.Close
	set rsOfferProduct=nothing

end if
end function
'function GetOfferInfo(offerid,arrayid)
'if len(offerid )>0 and isnumeric(offerid ) then
'	set rsOfferProduct=server.CreateObject("ADODB.Recordset")
'	strSQL="Select OFFER_NAME, PRODUCTID, PRICE from OFFERS where OFFER_ID=" & offerid 
'	'REsponse.Write strSQL
'	'REsponse.end
'	rsOfferProduct.Open strSQL, dbconnstr()
'	if not rsOfferProduct.EOF then
'		if cint(arrayid)=0 then
'			GetOfferInfo=rsOfferProduct("OFFER_NAME")
'		elseif cint(arrayid)=1 then
'			GetOfferInfo=rsOfferProduct("PRODUCTID")
'		elseif cint(arrayid)=2 then
'			GetOfferInfo=rsOfferProduct("PRICE")
'		end if
'	end if
'	rsOfferProduct.Close
'	set rsOfferProduct=nothing

'end if
'end function
'*****************************************************************
'Shows  image ItemsAdded.gif
'YK 5/15/02 ProdID
'Returns string
'*****************************************************************
	sub ShowProductinBasket(prodid)
		'on error resume next
		Set rsInBasket = Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID = '" & prodid & "' AND SESSIONID = '" & Session("netSessionID") & "'"
		rsInBasket.Open strSQL, dbconnstr()
		if not rsInBasket.EOF or BOF then
			rsInBasket.MoveFirst
			Do While not rsInBasket.EOF or BOF
				if Cint(rsInBasket("PRODUCTID")) = Cint(prodid) then
					InBasket = True
				end if
			rsInBasket.MoveNext
			Loop	
			if InBasket then
				Response.Write "<IMG SRC=""ItemsAdded.gif""><br>" 
			end if
			InBasket = False
			rsInBasket.Close
			set rsInBasket = Nothing
		end if
	end sub
'*************************************
'CheckProductExists
'KK 7/26/2002
'Revision 2 KK 1/20/2004
'arguments (prodid)
'Returns boolean
'*************************************
function CheckProductExists(prodid)
if len(prodid)>0  then
	set rsProductExist=server.CreateObject("ADODB.Recordset")
	strSQL="Select * from PRODUCT where PRODUCTID=" & prodid & " and disabled<>'y' and EOL_YN<>'y'"
	'Response.Write strSQL
	Set cmdProductExist=server.CreateObject("ADODB.COmmand")
	cmdProductExist.CommandText=strSQL
	cmdProductExist.ActiveConnection=dbconnstr()
	cmdProductExist.CommandType=1'TEXT
	set rsProductExist=cmdProductExist.Execute
	'rsProductExist.Open strSQL, dbconnstr()
	if not rsProductExist.EOF then
		CheckProductExists=true
	else
		CheckProductExists=false
	end if
	rsProductExist.Close
	set rsProductExist=nothing
else
	CheckProductExists=false
end if
end function	
	

'*************************************
'Gets offer for product's model series
'YK 9/12/2002
'arguments (produid)
'Returns table
'*************************************
	
function GetOffersForModelSeries(prodid)

if len(prodid)>0 then
	set rsOffers=server.CreateObject("adodb.recordset")
	strSQL=	"Select * from Offers WHERE (PRODUCTID IN "&_
		    "(SELECT productid FROM product WHERE      model_series in"&_
            "(SELECT model_series FROM product WHERE productid=" & prodid & "))) AND (DISABLED = 'n')"
	rsOffers.Open strSQL, dbconnstr()
	if not rsOffers.EOF then
		Response.Write "<table border=0 cellpadding=0 cellspacing=0>"
		while not rsOffers.EOF
			Response.Write "<tr>"
			Response.Write "<td valign=top>"
			Response.Write "<a href=/shopper/offers.asp?offerid=" & rsOffers("OFFER_ID") & ">"
			if Session("localeid")=1033 then
				Response.Write "<img src='" & rsOffers("IMAGE") & "' border=0></a><br>"
			else
				if len( rsOffers("IMAGE_EUR"))>0 then
					Response.Write "<img src='" & rsOffers("IMAGE_EUR") & "' border=0></a><br>"
				else
					Response.Write "<img src='" & rsOffers("IMAGE") & "' border=0></a><br>"
				end if
			end if
			Response.Write "</td></tr>"
			rsOffers.MoveNext
		wend
		Response.Write "</table><br>"
	end if
end if
end function


'*************************************
'Gets offer for product
'YK 9/12/2002
'arguments (produid)
'Returns table
'*************************************

function GetOffersForProduct(prodid) 
	if len(prodid)>0 then
		set rsOffers=server.CreateObject("adodb.recordset")
		strSQL=	"Select * from Offers WHERE (PRODUCTID=" & prodid & ") AND (DISABLED = 'n')"
		rsOffers.Open strSQL, dbconnstr()
		if not rsOffers.EOF then
			Response.Write "<table border=0 cellpadding=0 cellspacing=0>"
			while not rsOffers.EOF
				Response.Write "<tr>"
				Response.Write "<td valign=top>"
				Response.Write "<a href=/shopper/offers.asp?offerid=" & rsOffers("OFFER_ID") & ">"
				Response.Write "<img src='" & rsOffers("IMAGE") & "' border=0></a><br>"
				Response.Write "</td></tr>"
				rsOffers.MoveNext
			wend
			Response.Write "</table><br>"
		end if
	end if
end function

'*************************************
'Gets customer's approval status
'YK 3/28/2003
'arguments (Session("ContactID"))
'Returns boolean
'*************************************

function GetCustomerApprovalStatus(ContactID) 
	if len(ContactID)>0 then
		set rsGetCustApproval=server.CreateObject("adodb.recordset")
		strSQL=	"Select * from CONTACT WHERE (CONTACT_ID=" & contactid & ") AND (COUNTRY = 'United States') and APPROVED_FLAG is not null"
		rsGetCustApproval.Open strSQL, dbconnstr()
		if not rsGetCustApproval.EOF then
			GetCustomerApprovalStatus=true
		else
			GetCustomerApprovalStatus=false
		end if
	else
		GetCustomerApprovalStatus=false
	end if
end function	
'*********************************************
'Draws the product categories for the discontinued products
'rev1 - SS 6/10/03
'rev2 - KK 7/20/05 (display 5 categories per row)
'rev3 - KK 8/13/2009 (display 4 categories per row, table width=951 px)
'arguments(statusofProduct, strDisabledProductList,pagename)
'Does not return anything
'*********************************************
function DrawProductCategories(statusOfProduct, strDisabledProductList,pagename)
	dim rsProductIsScope
	Set rsProductIsScope = Server.CreateObject("ADODB.Recordset")
	dim counterSQL

	dim rsCounter
	Set rsCounter = Server.CreateObject("ADODB.Recordset")
	if statusOfProduct = "y" then 	
		strSQL2 = "Select count(distinct PROPGROUPCATEGORY.CATEGORY_ID) as counter"&_
				" from PROPGROUPCATEGORY  inner join PROPERTYGROUP " & _
				" on PROPGROUPCATEGORY.CATEGORY_ID=PROPERTYGROUP.CATEGORY_ID " & _
				" inner join PRODUCT on PROPERTYGROUP.PROPERTYGROUPID=PRODUCT.PROPERTYGROUPID " & _
				" where  PRODUCT.PRODUCTID in (" & strDisabledProductList & ") AND PRODUCT.EOL_YN = 'n'" &_
				"AND  PROPGROUPCATEGORY.CATEGORY_ID <>  2 OR PRODUCT.MODEL_SERIES = 7  "
		'Response.Write strSQL
	else 
		strSQL2 = "Select count(distinct PROPGROUPCATEGORY.CATEGORY_ID)as counter"&_
				" from PROPGROUPCATEGORY  inner join PROPERTYGROUP " & _
				" on PROPGROUPCATEGORY.CATEGORY_ID=PROPERTYGROUP.CATEGORY_ID " & _
				" inner join PRODUCT on PROPERTYGROUP.PROPERTYGROUPID=PRODUCT.PROPERTYGROUPID " & _
				" where  PRODUCT.PRODUCTID not in (" & strDisabledProductList & ")"&_
				" OR PROPGROUPCATEGORY.CATEGORY_ID =2"
	end if
	rsCounter.Open strSQL2, dbconnstr()

	'totalCategories =11 
	totalCategories = rsCounter("counter")+1+1
	rsCounter.close
	
	
	'the product is a discontinued product
	if statusOfProduct = "y" then ' non Available product or is a discontinued analog scope
		strSQL = " SELECT DISTINCT PROPGROUPCATEGORY.CATEGORY_ID AS CATEGORY_ID, PROPGROUPCATEGORY.IMAGE AS IMAGE, PROPGROUPCATEGORY.CATEGORY_NAME AS CATEGORY_NAME, PROPGROUPCATEGORY.SORT_ID "&_
					"FROM PROPGROUPCATEGORY INNER JOIN "&_
				"PROPERTYGROUP ON PROPGROUPCATEGORY.CATEGORY_ID = PROPERTYGROUP.CATEGORY_ID INNER JOIN "&_
				"PRODUCT ON PROPERTYGROUP.PROPERTYGROUPID =  PRODUCT .PROPERTYGROUPID "&_
					" WHERE PRODUCT .PRODUCTID IN (" & strDisabledProductList & ")AND "&_
					"( PRODUCT.EOL_YN = 'n') AND (PROPGROUPCATEGORY.CATEGORY_ID <> 2) OR "&_
					"( PRODUCT.MODEL_SERIES = 7) "&_
					"UNION "&_
					"SELECT  PROPGROUPCATEGORY.CATEGORY_ID AS CATEGORY_ID, PROPGROUPCATEGORY.IMAGE AS IMAGE, "&_
					" PROPGROUPCATEGORY.CATEGORY_NAME AS CATEGORY_NAME, PROPGROUPCATEGORY.SORT_ID "&_
					"FROM   PROPGROUPCATEGORY  "&_
					" WHERE   PROPGROUPCATEGORY.CATEGORY_ID = 16 or PROPGROUPCATEGORY.CATEGORY_ID = 9"&_
					" ORDER BY PROPGROUPCATEGORY.SORT_ID, PROPGROUPCATEGORY.CATEGORY_NAME " 
		
	else if statusOfProduct = "n" then 'get all the products that are continued
		'the product is  not a discontinous product
		strSQL=	"Select distinct PROPGROUPCATEGORY.CATEGORY_ID as CATEGORY_ID,"&_
			"PROPGROUPCATEGORY.IMAGE AS IMAGE,"&_
			"PROPGROUPCATEGORY.CATEGORY_NAME as CATEGORY_NAME," & _
			"PROPGROUPCATEGORY.CATEGORY_ID as CATEGORY_ID, PROPGROUPCATEGORY.SORT_ID" & _
			" from PROPGROUPCATEGORY  inner join PROPERTYGROUP " & _
			" on PROPGROUPCATEGORY.CATEGORY_ID=PROPERTYGROUP.CATEGORY_ID " & _
			" inner join PRODUCT on PROPERTYGROUP.PROPERTYGROUPID=PRODUCT.PROPERTYGROUPID " & _
			" where  PRODUCT.PRODUCTID not in (" & strDisabledProductList & ") " &_
			" or PROPGROUPCATEGORY.CATEGORY_ID =2 ORDER BY PROPGROUPCATEGORY.SORT_ID, PROPGROUPCATEGORY.CATEGORY_NAME"
		end if
	end if
	rsProductIsScope.open strSQL, dbconnstr()
	dim tempCounter 
	'Response.Write strSQL
	
	Response.Write "<TABLE BORDER =0 width=""951"" cellpadding=0 cellspacing=0>"
	' Draws the products in the table
	Response.Write "<tr>"'start the new row
	tempCounter =0 
	
	IF NOT rsProductIsScope.eof then 
		do until rsProductIsScope.eof
			
			
		'only 6 products on one line
		if totalCategories > 4 then
			if((totalCategories mod 2) = 0 )then 
				if( tempCounter =4 or tempCounter = int(totalCategories/2+1) ) then 
					
					Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td colspan=" & tempCounter &" height=3 >"
					
					Response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=3>"
					Response.Write "</td>"
					Response.Write "</tr>"
					'Response.Write "<tr>"
									
					'Response.Write "<td colspan=" & tempCounter &" height=1 bgcolor=#999999>"
					'response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=1>"
					'Response.Write "</td>"
					'Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td colspan=" & tempCounter &" height=3>"
					Response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=3>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
					tempCounter =0
				end if
			else 
				if(tempCounter = 4 or tempCounter = int(totalCategories/2) )then 					
					Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td colspan=" & tempCounter &" height=3 >"
					
					Response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=3>"
					Response.Write "</td>"
					Response.Write "</tr>"
					'Response.Write "<tr>"
				
					'Response.Write "<td colspan=" & tempCounter &" height=1 bgcolor=#999999>"
					'Response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=1>"
					'Response.Write "</td>"
					'Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td colspan=" & tempCounter &" height=3>"
					
					Response.Write "<img src=""/Nav/Images/1pixspacer.gif"" height=3>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
					tempCounter =0
					
				end if
			end if
		end if
		tempCounter = tempCounter + 1
		
		Response.Write "<TD vAlign=""top""  align=""center"" width = ""16%"">"

		if(rsProductIsScope("CATEGORY_ID") =16) then 
			Response.Write "<a href = ""https://teledynelecroy.com/lrs/"">"
		else
			if(rsProductIsScope("CATEGORY_ID") = 9) then 
				Response.Write "<a href = ""/tm/products/signalsources/default.asp"">"
			else 	
				Response.Write "<a href =""" & pagename & "?propcatid=" & rsProductIsScope("CATEGORY_ID") & """>"		
			end if 
		end if 
		Response.Write "<img src= " & rsProductIsScope("IMAGE")& " border =0></a>" 
		Response.Write "<br>"
		Response.Write " <font face=""=Verdana,Arial"" size=1 >"
		if(rsProductIsScope("CATEGORY_ID") =16) then 
			Response.Write "<a href = ""https://teledynelecroy.com/lrs/"">"
		else
			if(rsProductIsScope("CATEGORY_ID") = 9) then 
				Response.Write "<a href = ""/tm/products/signalsources/default.asp"">"
			else 	
				Response.Write "<a href =""" & pagename & "?propcatid=" & rsProductIsScope("CATEGORY_ID") & """>"		
			end if 
		end if
		IF(rsProductIsScope("CATEGORY_ID") =2) then 
			Response.Write "Analog Scope "
		else 

			if Session("localeid")=1041 and rsProductIsScope("CATEGORY_ID") =19 then
				Response.Write LoadI18N("SLBCA0595",localeID)
			else
				Response.Write translation("PROPGROUPCATEGORY","CATEGORY_NAME","CATEGORY_ID",rsProductIsScope("CATEGORY_ID"))
			end if
		end if
		Response.Write "</a>"
		Response.Write "</font>"
		Response.Write "</TD>"
		
		rsProductIsScope.MoveNext
		loop
	end if
	Response.Write "</tr>"
	Response.Write "<tr>"
	Response.Write "<td colspan=" & tempCounter &" height=3 >"
	Response.Write "<img src=""/nav/images/1pixspacer.gif"" height=3>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "<tr>"
	Response.Write "<td colspan=" & tempCounter &" height=1 bgcolor=#cccccc>"
	Response.Write "<img src=""/nav/images/1pixspacer.gif"" height=1>"
	Response.Write "</td>"
	Response.Write "</tr>"
	
	Response.Write "</table>"
	
	rsProductIsScope.Close
	set rsProductIsScope = nothing
end function

'********************************
'Creates the link to the substitute product of the discontinued product 
'SS 6/13/2003
'rev1 KK 6/25/2007
'arguments(prodID)
'This function does not return anything
'********************************
function DisplaySubstituteLink(prodID)

	DIM rsDisplay
	set rsDisplay = Server.CreateObject("ADODB.Recordset")
	'getting the substsitute product and its categoryID
	strSQL = "	SELECT PRODUCT.BEST_SUBSTITUTE AS SUBSTITUTE,PROPERTYGROUP.CATEGORY_ID AS CATEGORY_ID"&_
			 "	FROM PROPERTYGROUP INNER JOIN"&_
             " PRODUCT ON PROPERTYGROUP.PROPERTYGROUPID = PRODUCT.PROPERTYGROUPID"&_
		      " WHERE PRODUCT.DISABLED = 'y' AND PRODUCT.EOL_YN = 'n' AND PRODUCT.PRODUCTID = "& prodID
	
	rsDisplay.Open strSQL, dbconnstr()
	
	'if it is an analog scope, then show the series as the substitute 
	if(rsDisplay("CATEGORY_ID") = 15) then 
		response.write "<font face=""=Verdana,Arial"" color=""DodgerBlue"" size=2><b> &nbsp;&nbsp;  Best Substitute  &nbsp;&nbsp;"
		response.write "<A href=""/tm/products/Scopes/WaveRunner_Xi/default.asp"">Waverunner Xi Series</a></font>"
	
	else 
		if (rsDisplay("CATEGORY_ID") = 7) then 
		response.write "<font face=""=Verdana,Arial"" color=""DodgerBlue"" size=2><b> &nbsp;&nbsp;  Best Substitute  &nbsp;&nbsp;"
		response.write "<A href=""/tm/products/Scopes/WavePro/default.asp"">WavePro 7000A Series</a></font>"	
		else 
			if 	(rsDisplay("CATEGORY_ID") = 16) then 
				response.write "<A href=""https://teledynelecroy.com/lrs/""></a>"	
			else 
			'if it is not an analog scope then parse the substitute text and then display
			'the substitutes and their links
				if not rsDisplay("SUBSTITUTE") = "" THEN 
					STR = GetSubstituteProductString(rsDisplay("SUBSTITUTE"))
				END IF
			end if 
		end if 
	end if
	
	
end function

'SS 10/28/04
'Modified discontinued products page
function BestSubstituteExists(productid)
	strSQL = "SELECT COUNT(*) AS COUNTER FROM BEST_SUBSTITUTES WHERE PRODUCTID = "&  productid
	set rsCheckBestSubstitute = Server.CreateObject("ADODB.Recordset")
	rsCheckBestSubstitute.Open strSQL, dbconnstr()
	if not rsCheckBestSubstitute.EOF then
		if rsCheckBestSubstitute("counter") > 0 then
			BestSubstituteExists = true
		else
			BestSubstituteExists = false
		end if
	else
		BestSubstituteExists = false
	end if
end function

function DisplayBestSubstitute(productid)
	if BestSubstituteExists(productid) then
		'best substitute exists
		strSQL = "SELECT * FROM BEST_SUBSTITUTES WHERE PRODUCTID = "&  productid
		set rsDisplaySubstitute = Server.CreateObject("ADODB.Recordset")
		rsDisplaySubstitute.Open strSQL, dbconnstr()
		
	
	end if 

end function
'*****************************************************************
'Parses the Substitute product string to get the substitute products
' and displays them with their part number and link
'SS 6/16/03
'Arguments (SubstituteString)
'Returns string
'*****************************************************************
function GetSubstituteProductString(SubstituteString)
	if len(SubstituteString)>0 then
		'create array of substitute products
		i=1
		Start=1
		strAll=cstr(SubstituteString)
	
		While not strAll = ""
			L = InStr(strAll, ",")
				If L = 0 Then
					substituteProductid = strAll
					strAll = ""
				Else
					substituteProductid = Mid(strAll, Start, (L - Start))
					strAll = Trim(Mid(strAll, L+1, Len(strAll)))
				End If		
			redim preserve models(i)
			models(i)=substituteProductid
			i=i+1
		Wend
		
		if cint(i)>1 then
		
			
			Response.Write  "<font face=""=Verdana,Arial"" color=""DodgerBlue"" size=1><b>" & "&nbsp;" & LoadI18N("SLBCA0464",localeID) & "&nbsp;</font></b>"
			
			
			for j=1 to i-1
				'displaying a slash between 2 or more substitutes
				if j > 1 then 
					Response.Write " / "
					
				end if 
				set rsCode=server.CreateObject("ADODB.Recordset")
				strSQL="Select PARTNUMBER AS PARTNUMBER,PRODUCTID AS PRODUCTID "&_
						 "from PRODUCT where PRODUCTID=" &  models(j)
			
				rsCode.Open strSQL, dbconnstr()
				
				if not rsCode.EOF then
					if len(strCode)=0 then
						Response.Write "<a HREF = '/tm/products/ViewProduct.asp?prodid=" & rsCode("PRODUCTID")& "'>"  
						Response.Write "<font face=""=Verdana,Arial""  size=2><b>" & rsCode("PARTNUMBER")
						response.write "</b></font>"
						Response.Write "</a>"
						
					else
						Response.Write "<a HREF = '/tm/products/ViewProduct.asp?prodid=" &  rsCode("PRODUCTID") & "'>"  
						Response.Write "<font face=""=Verdana,Arial""  size=2><b>" & rsCode("PARTNUMBER")
						response.write "</b></font>"
						Response.Write"	</a>"
							
					end if
							
				else
					strCode=""
				end if
			
			next
			
		else
			GetModelCodeString=""
			
		end if
		
	else
		GetModelCodeString=""
	end if

end function
'*****************************************************************
'Creates a list of PARTNUMBERS of available products by category_id
'KK 11/14/2003
'Arguments (categoryid -integer)
'Returns string
'*****************************************************************

function GetKeyWords(category_id)
if len(category_id)>0 then
	set rsKeyWords=server.CreateObject("ADODB.RECORDSET")
	strSQL=	"SELECT PRODUCT.PARTNUMBER,PRODUCT.PRODUCTDESCRIPTION FROM PROPGROUPCATEGORY INNER JOIN " &_
			"PROPERTYGROUP ON PROPGROUPCATEGORY.CATEGORY_ID = PROPERTYGROUP.CATEGORY_ID INNER JOIN "&_
            "PRODUCT ON PROPERTYGROUP.PROPERTYGROUPID = PRODUCT.PROPERTYGROUPID INNER JOIN "&_
            "DSO_OPTION_LIST ON PRODUCT.PRODUCTID = DSO_OPTION_LIST.PRODUCTID "&_
			"WHERE(PROPGROUPCATEGORY.CATEGORY_ID = " & category_id & ") AND (PROPERTYGROUP.PROPERTYGROUP_CODE "&_
			" IN ('HW', 'SW')) ORDER BY PRODUCT.PARTNUMBER"
	rsKeyWords.Open strSQL, dbconnstr()
	if not rsKeyWords.EOF then
		while not rsKeyWords.EOF
			if len(GetKeyWords)=0 then
				GetKeyWords=rsKeyWords("PARTNUMBER") & ", " & rsKeyWords("PRODUCTDESCRIPTION")
			else
			GetKeyWords=GetKeyWords & ", " & rsKeyWords("PARTNUMBER") & ", " & rsKeyWords("PRODUCTDESCRIPTION")
			end if
			rsKeyWords.MoveNext
		wend
	else
		GetKeyWords	=""
	end if
else
	GetKeyWords	=""
end if
end function
'*************************************
'Display the category drop down on the Techhelp page
'Shraddha Shah 2/1/2004, 
' modified 3/2/05 SS to add PSG products
'arguments - none
'Returns none
'*************************************
function TechHelp_showCategoryDropdown(company)
	if cint(company) = 1 then
		'CATC / PSG categories
		Response.write "<select  id = categories name=categories onChange=JavaScript:FillGroups()>"
		If Len (Session("categories"))= 0 Then
				response.write "<option VALUE=''>"& "&nbsp;"'LoadI18N("SLBCA0543",localeID)
		end if 
		'this is for categories
		'if Session("localeid")<>1041 then
		'	strSQL = "select DISTINCT category_id , category_name "&_
					'" from PROPGROUPCATEGORY where category_id not in (2,16,9) order by category_name "
		'else
		strSQL = "select subcat_id as category_id, subcat_name as category_name"&_
				" from PROPGROUP_SUBCATEGORY where subcat_id <>4 and subcat_id <>5 order by subcat_name "

		'end if
		Set rsCategories =server.CreateObject("ADODB.Recordset")
		rsCategories.open strSQL, dbconnstr()
		rsCategories.MoveFirst
		Do While Not rsCategories.BOF And Not rsCategories.EOF 
			iLooper = iLooper +1
			if len(Session("categories")) > 0 then 
				if cstr(Session("categories")) = cstr(rsCategories("category_id")) then  
					Response.Write "<option VALUE='" & rsCategories("category_id") & "' selected>" & rsCategories("category_name")
				else
					Response.Write "<option VALUE='" & rsCategories("category_id") & "'>" & rsCategories("category_name")
				end if
			else 
				Response.Write "<option VALUE='" & rsCategories("category_id") & "'>" & rsCategories("category_name")
			end if
			rsCategories.MoveNext
		Loop
		rsCategories.Close
		Session("CategoryCount")=iLooper
		Response.Write "</option></select>"
	
	else
		Response.write "<select  id = categories name=categories onChange=JavaScript:FillGroups()>"
		If Len (Session("categories"))= 0 Then
				response.write "<option VALUE=''>"& LoadI18N("SLBCA0543",localeID)
		end if 
		'this is for categories
		if Session("localeid")<>1041 then
			strSQL = "select DISTINCT category_id , category_name "&_
					" from PROPGROUPCATEGORY where category_id not in (2,16,9,19) order by category_name "
		else
			strSQL = "select DISTINCT category_id , category_name "&_
				" from PROPGROUPCATEGORY where category_id not in (2,15,16,9,19) order by category_name "

		end if
		Set rsCategories =server.CreateObject("ADODB.Recordset")
		rsCategories.open strSQL, dbconnstr()
		rsCategories.MoveFirst
		Do While Not rsCategories.BOF And Not rsCategories.EOF 
			iLooper = iLooper +1
			if len(Session("categories")) > 0 then 
				if cstr(Session("categories")) = cstr(rsCategories("category_id")) then  
					Response.Write "<option VALUE='" & rsCategories("category_id") & "' selected>" & translation("PROPGROUPCATEGORY","CATEGORY_NAME","CATEGORY_ID",rsCategories("CATEGORY_ID"))'rsCategories("category_name")
				else
					Response.Write "<option VALUE='" & rsCategories("category_id") & "'>"&  translation("PROPGROUPCATEGORY","CATEGORY_NAME","CATEGORY_ID",rsCategories("CATEGORY_ID"))'rsCategories("category_name")
				end if
			else 
				Response.Write "<option VALUE='" & rsCategories("category_id") & "'>"& translation("PROPGROUPCATEGORY","CATEGORY_NAME","CATEGORY_ID",rsCategories("CATEGORY_ID"))'rsCategories("category_name")
			end if
			rsCategories.MoveNext
		Loop
		rsCategories.Close
		Session("CategoryCount")=iLooper
		Response.Write "</option></select>"
	
	
	end if
end function

'*************************************
'Displays the 2nd drop down on the Techhelp page, if there is more than 1 propertygroup/modelseries
'Shraddha Shah 2/1/2004
'modified on 3/2/05 by SS to accomodate PSG products
'arguments (categoryID)
'Returns none
'*************************************
function Techhelp_showProductModels(categoryID,company)
	if len(categoryID) > 0 then 
		Response.Write "<select  id = propGroup name=propGroup onChange=JavaScript:FillGroups2()>"
		If Len (Session("propGroup"))= 0 Then
			response.write "<option VALUE=''>" & "&nbsp;"'LoadI18N("SLBCA0544",localeID)
		end if 
		if len(company) > 0 and cint(company)= 1 then
			'PSG products
			strSQL = "select  propertygroupid  , propertygroupname  "&_
					" from PROPertygroup where SUBCAT_ID=" & categoryID
			Set rsGroup =server.CreateObject("ADODB.Recordset")
			rsGroup.open strSQL, dbconnstr()
			rsGroup.MoveFirst
			Do While Not rsGroup.BOF And Not rsGroup.EOF 
				iLooper2 = iLooper2 +1
				if len(Session("propGroup"))>0 then 
					if cstr(Session("propGroup")) = cstr(rsGroup("propertygroupid")) then  
						Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "' selected>" & rsGroup("propertygroupname")
					else
						Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "'>"& rsGroup("propertygroupname")
					end if
				else 
					Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "'>"& rsGroup("propertygroupname")
				end if
				rsGroup.MoveNext
			Loop
			rsGroup.Close
			Session("GroupCount")=iLooper2
			Response.Write "</select>"
		else ' LCRY products
			'digital scope
			
			
			if cstr(categoryID) = "1" then 
				if Session("localeid")<>1041 then
					strSQL = " SELECT DISTINCT MODELSERIES.model_series_id AS id,  MODELSERIES.name AS name, MOdelseries.sortid "& _
							" FROM  MODELSERIES where MODELSERIES.model_series_id <> 7 and "&_
							" (disabled='n' or model_series_id =15) "& _
							" ORDER BY	MODELSERIES.sortid "
				else
					strSQL = " SELECT DISTINCT MODELSERIES.model_series_id AS id,  MODELSERIES.name AS name, MOdelseries.sortid "& _
							" FROM  MODELSERIES where MODELSERIES.model_series_id not in (6,7) and "&_
							" disabled='n' ORDER BY	MODELSERIES.sortid "
				end if
				'Response.End 
				Set rsGroup1 =server.CreateObject("ADODB.Recordset")
				rsGroup1.open strSQL, dbconnstr()
				rsGroup1.MoveFirst
				Do While Not rsGroup1.BOF And Not rsGroup1.EOF 
					iLooper2 = iLooper2 +1
					if len(Session("propGroup"))>0then 
						if cstr(Session("propGroup")) = cstr(rsGroup1("id")) then  
							Response.Write "<option VALUE='" & rsGroup1("id") & "' selected>" & translation("MODELSERIES","name","model_series_id",rsGroup1("id"))	'rsGroup1("msname")
						else
							Response.Write "<option VALUE='" & rsGroup1("id") & "'>"& translation("MODELSERIES","name","model_series_id",rsGroup1("id"))	'rsGroup1("msname")
						end if
					else 
						Response.Write "<option VALUE='" & rsGroup1("id") & "'>"& translation("MODELSERIES","name","model_series_id",rsGroup1("id"))	'rsGroup1("msname")
					end if
					rsGroup1.MoveNext
				Loop
				rsGroup1.Close
				Session("GroupCount")=iLooper2
				Response.Write "</select>"
			else
			'other products
				strSQL = "select  propertygroupid  , propertygroupname  "&_
						" from PROPertygroup where category_id=" & categoryID
				Set rsGroup =server.CreateObject("ADODB.Recordset")
				rsGroup.open strSQL, dbconnstr()
				rsGroup.MoveFirst
				Do While Not rsGroup.BOF And Not rsGroup.EOF 
					iLooper2 = iLooper2 +1
					if len(Session("propGroup"))>0 then 
						if cstr(Session("propGroup")) = cstr(rsGroup("propertygroupid")) then  
							Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "' selected>" & translation("PROPERTYGROUP","propertygroupname","propertygroupid",rsGroup("propertygroupid"))'rsGroup("propertygroupname")
						else
							Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "'>"& translation("PROPERTYGROUP","propertygroupname","propertygroupid",rsGroup("propertygroupid"))'rsGroup("propertygroupname")
						end if
					else 
						Response.Write "<option VALUE='" & rsGroup("propertygroupid") & "'>"& translation("PROPERTYGROUP","propertygroupname","propertygroupid",rsGroup("propertygroupid"))'rsGroup("propertygroupname")
					end if
					rsGroup.MoveNext
				Loop
				rsGroup.Close
				Session("GroupCount")=iLooper2
				Response.Write "</select>"
			end if
		end if 'company len
	end if'len(catid) > 0 
end function

'*************************************
'Displays the continued and/or discontinued products 
'Shraddha Shah 2/1/2004
'arguments (categoryID,propid)
'Returns none
'*************************************
function Techhelp_showProducts(categoryID,propid)
	Response.Write "<table cellspacing = 0 cellpadding =5 border =0 width= 90% >"
	Response.Write "<tr>"
	Response.Write "<td valign = top width= 25% ><font face=verdana, arial color=#008080 size=2><b>"
	Response.Write "<nobr>"&LoadI18N("SLBCA0593",localeID)&"</nobr>"
	Response.Write "</b></font></td>"
	'show continued products
	if len(propid) > 0 then 
		PropertygroupID = propid
	else
		PropertygroupID = Techhelp_getPropertyIdonCategoryId(categoryID)
	end if 
	if (Techhelp_countProducts(categoryID,PropertygroupID,"n") >0) then
		Session("ContCreated") = "yes"
		Response.Write "<td valign = top width= 30% align=center><font face=verdana, arial color=#008080 size=2><b>"
		Response.Write LoadI18N("SLBCA0550",localeID)
		Response.Write "</b></font></td>"
	else
		Session("ContCreated") = "no"
	end if 
	'show dicontinued products
	if (Techhelp_countProducts(categoryID,PropertygroupID,"y") >0) then
		Session("DiscCreated") = "yes"
		Response.Write "<td valign = top width= 40% align=center><font face=verdana, arial color=#008080 size=2><b>"
		Response.Write LoadI18N("SLBCA0551",localeID)
		Response.Write "</b></font></td>"
	else
		Session("DiscCreated") = "no"
	end if 
	Response.Write "</tr>"
	Response.Write "<tr>"
	'checkbox
	Response.Write "<td valign = top width= 25%  align=center>"
	response.Write "<INPUT type=checkbox id=checkbox1 name=AllCheck " 
	if Session("checkb")= "on"  then 
		%>checked <%
	end if
	response.write ">"
	Response.Write "</td>"
	
	'display cont products
	if (Techhelp_countProducts(categoryID,PropertygroupID,"n") >0) then
		Response.Write "<td valign = top width= 30% align=center>"
		call Techhelp_showcontProducts(categoryID,PropertygroupID)
		Response.Write "</td>"	
	end if
	'display discont products
	if (Techhelp_countProducts(categoryID,PropertygroupID,"y") >0) then
		Response.Write "<td valign = top width= 30% align=center>"
		call Techhelp_showDiscontProducts(categoryID,PropertygroupID)
		Response.Write "</td>"
	end if 
	if (Techhelp_countProducts(categoryID,PropertygroupID,"n") = 0 or Techhelp_countProducts(categoryID,PropertygroupID,"y") =0 ) then
		Response.Write "<td valign = top width= 30% >&nbsp;"
		Response.Write "</td>"
	end if 
	Response.Write "</tr>"
	Response.Write "</table>"
end function

'*************************************
'Returns the propertyID of a given categoryID; This function is used to get the propertyID
'of the categories which do not have more than 1 propertyID
'Shraddha Shah 2/1/2004
'arguments (categoryID)
'Returns string
'*************************************
function Techhelp_getPropertyIdonCategoryId(categoryID)
	strSQL = "select propertygroupid  as propid from propertygroup where category_id = " & categoryID
	'response.Write strSQL
	Set rs1 =server.CreateObject("ADODB.Recordset")
	rs1.open strSQL, dbconnstr()
	
	Techhelp_getPropertyIdonCategoryId = rs1("propid")
end function 

'*************************************
'Counts the continued or discontinued products of a given property ID
'Shraddha Shah 2/1/2004
'arguments (categoryID,PropertygroupID,disabled)
'Returns integer
'*************************************
function Techhelp_countProducts(categoryID,PropertygroupID,disabled)
	if len(PropertygroupID) > 0 then 
		PropertygroupID = PropertygroupID
	else
		PropertygroupID = Techhelp_getPropertyIdonCategoryId(categoryID)
	end if 

	if len(disabled)>0 then
		if cstr(disabled)="y" then
			if cstr(categoryID) = cstr("1") then 
				strSQL ="Select count(productid) as counter from PRODUCT where model_series=" & PropertygroupID & _
					" and product.eol_yn = 'n' and productid  in (" & GetDiscontinuedProductsbyLocaleID & ") " 
			else	
				strSQL ="Select count(productid) as counter from PRODUCT where  PROPERTYGROUPID=" & PropertygroupID & _
					" and product.eol_yn = 'n' and productid  in (" & GetDiscontinuedProductsbyLocaleID & ") " 
										 
			end if 
		else
			if cstr(categoryID) = cstr("1") then 
				strSQL = "Select count(productid) as counter from PRODUCT where  model_series =" & PropertygroupID & _
					" and  productid not in (" & GetDisabledProducts & ")"
			else
				strSQL = "Select count(productid) as counter from PRODUCT where  PROPERTYGROUPID=" & PropertygroupID & _
					" and productid not in (" & GetDisabledProducts & ")"
			end if 
		end if
		'Response.Write strSQL
		'response.end
		Set rs2 =server.CreateObject("ADODB.Recordset")
		rs2.open strSQL, dbconnstr()
		if not rs2.eof then
			Techhelp_countProducts = rs2("counter")
		else
			Techhelp_countProducts=0
		end if 
	end if
end function

'*************************************
'Displays the continued products of a given category or property group
'Shraddha Shah 2/1/2004
'arguments (categoryID,PropertygroupID)
'Returns none
'*************************************
function Techhelp_showcontProducts(categoryID,PropertygroupID)
	
	'this is for categories
	if cstr(categoryID) = cstr("1") then 
		strSQL = "Select productid, partnumber from PRODUCT where  model_series =" & PropertygroupID & _
			  " and  productid not in (" & GetDisabledProducts & ")  order by SORT_ID,PARTNUMBER desc"
	else
		strSQL = "Select productid, partnumber from PRODUCT where  PROPERTYGROUPID=" & PropertygroupID & _
			 " and productid not in (" & GetDisabledProducts & ")  order by SORT_ID,PARTNUMBER desc"
	end if 
	Set rsProducts =server.CreateObject("ADODB.Recordset")
	rsProducts.open strSQL, dbconnstr()
	'Response.Write strSQL
	Response.Write "<select  id = propGroup2 multiple name=propGroup2>"
	rsProducts.MoveFirst
	iLooper = 0
	Do While Not rsProducts.BOF And Not rsProducts.EOF 
		iLooper = iLooper +1
		'Response.Write rsProducts.Fields(1).Value
		
		redim preserve techhelp_ContinuedProducts(1,iLooper)
		techhelp_ContinuedProducts(0,iLooper) = rsProducts.Fields(0).Value
		techhelp_ContinuedProducts(1,iLooper) = rsProducts.Fields(1).Value
		if isArray(Session("SelectedContinued")) then 
				if (checkContinuedProductselected(rsProducts.Fields(0).Value)) then  
					Response.Write "<option VALUE='" & rsProducts.Fields(0).Value & "' selected>" &  rsProducts.Fields(1).Value
				else
					Response.Write "<option VALUE='" &  rsProducts.Fields(0).Value & "'>"&  rsProducts.Fields(1).Value
				end if
		else 
			Response.Write "<option VALUE='" &  rsProducts.Fields(0).Value & "'>"&  rsProducts.Fields(1).Value
		end if
	
		rsProducts.MoveNext
	Loop
	rsProducts.Close
	Session("ContinuedCount")=iLooper
	Session("StoreContinuedProducts") = techhelp_ContinuedProducts
	Response.write "</select>"

end function

'*************************************
'Displays the Discontinued products of a given category or property group
'Shraddha Shah 2/1/2004
'arguments (categoryID,PropertygroupID)
'Returns none
'*************************************
function Techhelp_showDiscontProducts(categoryID,PropertygroupID)
	'if a model series/digital scope
	if cstr(categoryID) = cstr("1") then 
		strSQL = "Select productid, partnumber from PRODUCT where  model_series =" & PropertygroupID & _
			  " and product.eol_yn = 'n' and  productid  in (" & GetDiscontinuedProductsbyLocaleID & ")  order by SORT_ID,PARTNUMBER desc"
	'other than digital scope
	else
		strSQL = "Select productid, partnumber from PRODUCT where  PROPERTYGROUPID=" & PropertygroupID & _
			 " and product.eol_yn = 'n' and productid  in (" & GetDiscontinuedProductsbyLocaleID & ")  order by SORT_ID,PARTNUMBER desc"
	end if 
	'Response.Write strSQL
	Set rsProducts1 =server.CreateObject("ADODB.Recordset")
	rsProducts1.open strSQL, dbconnstr()
	'Response.Write strSQL1
	rsProducts1.MoveFirst
	Response.Write "<select  id = propGroup3 multiple name=propGroup3>"
	Do While Not rsProducts1.BOF And Not rsProducts1.EOF 
		iLooper1 = iLooper1 +1
		Redim Preserve techhelp_DiscontinuedProducts(1,iLooper1)	
		techhelp_DiscontinuedProducts(0,iLooper1) = rsProducts1.Fields(0).Value
		techhelp_DiscontinuedProducts(1,iLooper1) = rsProducts1.Fields(1).Value	
		if isArray(Session("SelectedDiscontinued")) then 
			if (checkDiscontinuedProductselected(rsProducts1.Fields(0).Value) = true) then  
				Response.Write "<option VALUE='" & rsProducts1.Fields(0).Value	 & "' selected>" & rsProducts1.Fields(1).Value	
			else
				Response.Write "<option VALUE='" & rsProducts1.Fields(0).Value	& "'>"& rsProducts1.Fields(1).Value	
			end if
		else 
			Response.Write "<option VALUE='" & rsProducts1.Fields(0).Value	 & "'>"& rsProducts1.Fields(1).Value	
		end if
	rsProducts1.MoveNext
	Loop
	rsProducts1.Close
	Session("DiscontinuedCount")=iLooper1
	Session("StoreDiscontinuedProducts") = techhelp_DiscontinuedProducts
	Response.Write "</select>"
end function  

'*************************************
'Displays the question box
'Shraddha Shah 2/1/2004
'arguments none
'Returns none
'*************************************
function Techhelp_showQuestionbox()
	if len(trim(Session("Question")))>0 then
		response.write "<textarea rows=10 name=TextQuestion cols=40>" & Session("Question") &"</textarea>"
	else
		Response.Write "<textarea rows=10 name=TextQuestion cols=40></textarea>"
	end if
end function

'*************************************
'Displays the change profile button
'Shraddha Shah 2/1/2004
'arguments none
'Returns none
'*************************************
function Techhelp_showProfileButton()
	Response.Write "<input type=button value='" & LoadI18N("SLBCA0217",localeID)& "' id=submit1 name=submit1 onClick = ValidateAddress()>"
end function 

'*************************************
'Displays profile of the user
'Shraddha Shah 2/1/2004
'arguments none
'Returns none
'*************************************
function Techhelp_showProfile()
	Response.Write "<table cellpadding = 0 cellspacing = 2 border =0>"
	Response.Write "<tr><td valign=top><font face=Verdana,arial  size=2><strong>"
	if Session("localeid")<>1041 then
		if len(Session("FirstName"))>0 then
			Response.write Session("FirstName")& "&nbsp;" & Session("LastName")
		end if
	else
		if len(Session("LastName"))>0 then
		      Response.Write  Session("LastName")& "&nbsp;" &  LoadI18N("SLBCA0160",Session("localeid")) 
		end if
	end if
	Response.Write "</strong></td></tr>"
	Response.Write "<tr><td valign=top><font face=Verdana,arial  size=2>" & Session("Phone")&"</font></td></tr>"
	Response.Write "<tr><td valign=top><font face=Verdana,arial  size=2>" & Session("Email")&"</font></td></tr>"
	Response.Write "</table>"
end function

'*************************************
'Delete products from basket (SHOPPER table) if products
'are disabled for selected language
'Kate Kaplan 2/11/04
'arguments none
'Returns none
'*************************************
function DeleteFromBasketForLanguage()
	'delete all disabled products from basked
	set rsDeleteAllOptions=server.CreateObject("ADODB.Recordset")
	strSQL="delete  from SHOPPER where PRODUCTID in (" & GetDisabledProducts &  ") and SESSIONID='" &  Session("netSessionID") & "'"
	rsDeleteAllOptions.LockType=3
	rsDeleteAllOptions.Open strSQL, dbconnstr()
	'update masterproductid for options if scope was deleted
	set rsUpdateAllOptions=server.CreateObject("ADODB.Recordset")
	strSQL="update SHOPPER set MASTERPRODUCTID='' where MASTERPRODUCTID in (" & GetDisabledProducts &  ") and SESSIONID='" &  Session("netSessionID") & "'"
	rsUpdateAllOptions.LockType=3
	rsUpdateAllOptions.Open strSQL, dbconnstr()
	
end function

'*************************************
'Gets the region based on the country
'Shraddha Shah 6/7/04
'arguments none
'Returns the regionID for each country
'*************************************
function GetRegionOnCountry(country)
	'delete all disabled products from basked
	set rsCountry1 = server.CreateObject ("ADODB.Recordset")
	strSQL = "SELECT REGIONID FROM COUNTRY WHERE NAME LIKE '" & country & "'"
	rsCountry1.Open strSQL, dbconnstr()
	'response.write rsCountry1("REGIONID")
	'response.End 
	if len(rsCountry1("REGIONID")) > 0 then
		GetRegionOnCountry = rsCountry1("REGIONID")
	else
		GetRegionOnCountry = 0
	end if 

end function

'*************************************
'Checks whether the customer is from LNA or not.
'Shraddha Shah 6/7/04
'arguments none
'Returns true if customer from LNA
'*************************************
function CheckcustomerfromLNA
	'delete all disabled products from basked
	ValidateUserNoRedir()
	If Len(Session("country")) > 0 then 

		RegionID = GetRegionOnCountry(Session("country"))
		'response.Write RegionID
		'response.End 
		if cint(RegionID) <> 0 then
			if cint(RegionID) = 3 then
				CheckcustomerfromLNA= true
			else
				CheckcustomerfromLNA= false
			end if 
		else
			CheckcustomerfromLNA = false
		end if 
	else
		CheckcustomerfromLNA = false	
	end if 
end function

'*************************************
'Checks whether the product is a WaveSurfer
'rev1 -Shraddha Shah 8/23/04
'rev2 - Kate Kaplan 9/20/05
'rev3 - Kate Kaplan 2/8/06 (added WX Xs and WaveJet)
'rev4 - Kate Kaplan 11/5/08 (WS only)
'QuoteID
'Returns true if any of the products from a quote is a wavesurfer
'*************************************

function WaveSurferQUOTE_YN(QuoteID)
	WaveSurferQUOTE_YN = false
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE WHERE QUOTEID = '" & QuoteID & "'"
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
		do while not rsMasterID.EOF 
			if len(trim(rsMasterID("MasterProductID"))) > 0 then
				strSQL = "SELECT MODEL_SERIES FROM PRODUCT WHERE PRODUCTID = " & rsMasterID("MasterProductID")
				set rsModelSeries = server.CreateObject ("ADODB.Recordset")
				rsModelSeries.Open strSQL, dbconnstr()
				if not rsModelSeries.EOF then
					if len(rsModelSeries("Model_Series"))>0 then
						'if 	(cint(rsModelSeries("Model_Series")) =26 and rsMasterID("MasterProductID")<>1149 and rsMasterID("MasterProductID")<>1150) or cint(rsModelSeries("Model_Series")) =33 or cint(rsModelSeries("Model_Series")) =34 then
						if 	(cint(rsModelSeries("Model_Series")) =15 or cint(rsModelSeries("Model_Series"))=34 or cint(rsModelSeries("Model_Series"))=43 or cint(rsModelSeries("Model_Series"))=49) then
    						WaveSurferQUOTE_YN = true
							exit function
						end if 
					end if
				end if 
			end if 	
			rsMasterID.MoveNext 
		loop
	else
		WaveSurferQUOTE_YN = false
		exit function
	end if 
	
end function
'*************************************
'Checks if Download document exists
'Kate Kaplan 2/17/2005
'documentid int
'Returns true if document exists
'*************************************
function CheckDocExists(documentid)
	CheckDocExists=false
	if len(documentid)>0 then
		set rsCheckDocExists=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD  WHERE DOWNLOAD_ID=" & documentid
		rsCheckDocExists.Open strSQL, dbconnstr()
		if not rsCheckDocExists.eof then
			CheckDocExists=true
		end if
	end if
end function
'*************************************
'Checks if Download document needs obtain key
'Kate Kaplan 2/17/2005
'documentid int
'Returns true if document needs obtain key
'*************************************
function CheckObtainKey(documentid)
	CheckObtainKey=false
	if len(documentid)>0 then
		set rsCheckObtainKey=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD  WHERE POST='Y' and OBTAINKEY_YN='y' and DOWNLOAD_ID=" & documentid
		rsCheckObtainKey.Open strSQL, dbconnstr()
		if not rsCheckObtainKey.eof then
			CheckObtainKey=true
		end if
	end if
end function
'*************************************
'Get file path for Download document
'Kate Kaplan 2/17/2005
'documentid int
'Returns string
'*************************************
function GetDOwnloadFilePath (documentid)
	if len(documentid)>0 then
		set rsGetDOwnloadFilePath=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD  WHERE  DOWNLOAD_ID=" & documentid
		rsGetDOwnloadFilePath.Open strSQL, dbconnstr()
		if not rsGetDOwnloadFilePath.eof then
			GetDOwnloadFilePath=rsGetDOwnloadFilePath("FILE_PATH")
		end if
	end if
end function
'*************************************
'Get desc for Download document
'Kate Kaplan 2/17/2005
'documentid int
'Returns string
'*************************************
function getdownloaddescr(documentid)
	if len(documentid)>0 then
		set rsGetDescription=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD WHERE DOWNLOAD_ID=" & documentid
		rsGetDescription.Open strSQL, dbconnstr()
		if not rsGetDescription.eof then
			getdownloaddescr=replace(replace(rsGetDescription("NAME"),"<i>",""),"</i>","") & " - Version " & rsGetDescription("Version") & " - " & mid(GetDOwnloadFilePath(Session("PSGdownloadID")),instrrev(GetDOwnloadFilePath(Session("PSGdownloadID")),"/")+1)
		end if
	end if
end function
'*************************************
'Get desc for Download document
'Kate Kaplan 2/17/2005
'documentid int
'Returns string
'*************************************
function getdownloadname(documentid)
	if len(documentid)>0 then
		set rsGetDownloadName=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD WHERE DOWNLOAD_ID=" & documentid
		rsGetDownloadName.Open strSQL, dbconnstr()
		if not rsGetDownloadName.eof then
			getdownloadname=rsGetDownloadName("NAME")
		end if
	end if
end function
'*************************************
'Get subcat_id for Download document
'Kate Kaplan 2/17/2005
'documentid int
'Returns int
'*************************************
function GetDownloadSubCategory(documentid)
	if len(documentid)>0 then
		set rsSubCategory=server.CreateObject("ADODB.Recordset")
		strSQL= " Select PROPGROUP_SUBCATEGORY.SUBCAT_ID" &_
				" FROM DOWNLOAD_CATEGORY INNER JOIN  DOWNLOAD ON"&_
				" DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID INNER JOIN"&_
				" PROPGROUP_SUBCATEGORY INNER JOIN  PROPERTYGROUP ON "&_
				" PROPGROUP_SUBCATEGORY.SUBCAT_ID = PROPERTYGROUP.SUBCAT_ID ON "&_
				" DOWNLOAD_CATEGORY.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID "&_
				" WHERE DOWNLOAD.DOWNLOAD_ID=" & documentid
		'Response.Write strSQL
		'Response.end
		rsSubCategory.Open strSQL, dbconnstr()
		if not rsSubCategory.eof then
			GetDownloadSubCategory=rsSubCategory("SUBCAT_ID") 
		end if
	end if
end function
'*************************************
'Checks if Download document category exists
'Kate Kaplan 2/17/2005
'documentid int
'Returns boolean
'*************************************
function CheckDownloadCategoryExists(downcatid)
	CheckDownloadCategoryExists=false
	if len(downcatid)>0 then
		set rsCheckDocExists=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM   DOWNLOAD_CATEGORY INNER JOIN "&_
				" DOWNLOAD ON DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID"&_
				" WHERE DOWNLOAD_CATEGORY.DOWN_CAT_ID=" & downcatid & " AND DOWNLOAD.POST = 'y'"
		rsCheckDocExists.Open strSQL, dbconnstr()
		if not rsCheckDocExists.eof then
			CheckDownloadCategoryExists=true
		end if
	end if
end function

'*******************************************************
'Check if SW download is new
'KK 2/25/2005
'Arguments - DOWN_CAT_ID
'Returns a string
'*******************************************************
function checkSWisnew(DOWN_CAT_ID)
'Select max Date_Entered from DOWNLOAD table
'and check if this date is less than 60 days from now
'if so show the word "NEW"
if len(DOWN_CAT_ID)>0 then
	set rsSWisnew=server.CreateObject("ADODB.Recordset")
	strSQL="Select max(DATE_ENTERED) as DATE_ENTERED from DOWNLOAD where DOWN_CAT_ID='" & DOWN_CAT_ID & "'"
	'response.Write strSQL
	rsSWisnew.Open strSQL, dbconnstr()
	if not rsSWisnew.EOF then
	    if DateDiff("d",rsSWisnew("DATE_ENTERED"),now())<=60 then
			checkSWisnew=LoadI18N("SLBCA0112",localeID)& "&nbsp;"
		end if 
	end if
end if
end function

'*************************************
'Returns product category or model series
'Shraddha Shah 3/10/2005
'*************************************
function Techhelp_getPropertygroupname(categoryID,propgroupid)
	if len(categoryID) > 0 then 
		if cint(categoryID) = 1 then
			strSQL = " SELECT MODELSERIES.name AS name FROM  MODELSERIES where MODELSERIES.model_series_id = " & propgroupid
		else
			'PSG products
			strSQL = "select  propertygroupname as name "&_
					" from PROPertygroup where propertygroupid=" & propgroupid
		end if 
		
		Set rspropertyname =server.CreateObject("ADODB.Recordset")
		rspropertyname.open strSQL, dbconnstr()
		Techhelp_getPropertygroupname = rspropertyname("name")
	else 
		Techhelp_getPropertygroupname=""		
	end if'len(catid) > 0 
end function
'*************************************
'Checks if Download document has an agreement
'Kate Kaplan 4/7/2005
'documentid int
'Returns true if document needs agreement
'*************************************
function CheckDownloadAgreement(documentid)
	CheckDownloadAgreement=false
	if len(documentid)>0 then
		set rsDownloadAgreement=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD  WHERE POST='Y' and AGREEMENT_YN='y' and DOWNLOAD_ID=" & documentid
		'Response.Write strSQL
		'Response.end
		rsDownloadAgreement.Open strSQL, dbconnstr()
		if not rsDownloadAgreement.eof then
			CheckDownloadAgreement=true
		end if
	end if
end function

'*************************************
'Checks if quote contains Protocol Analyzers 
'Kate Kaplan 4/12/2005
'quoteid int
'Returns true if quote contains Protocol Analyzers
'*************************************
function CheckPSGQuote(quoteid)
	CheckPSGQuote=false
	if len(quoteid)>0 then
		set rsCheckPSGQuote=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT DISTINCT PROPERTYGROUP.CATEGORY_ID FROM SHOPPERQUOTE INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID WHERE  CATEGORY_ID=19 and SHOPPERQUOTE.QUOTEID = " & quoteid
		'Response.Write strSQL
		'Response.end
		rsCheckPSGQuote.Open strSQL, dbconnstr()
		if not rsCheckPSGQuote.eof then
			CheckPSGQuote=true
		end if
	end if
end function
'*************************************
'Checks if quote contains Scopes Products
'rev 1 Kate Kaplan 4/12/2005
'rev 2 Kate Kaplan  1/30/2009 (check for Pert3)
'rev 3 - Kate Kaplan 2/8/2010 (removed Pert3 logic)
'quoteid int
'Returns true if quote contains Scopes Products
'*************************************
function CheckScopesQuote(quoteid)
	CheckScopesQuote=false
	if len(quoteid)>0 then
		set rsCheckScopesQuote=server.CreateObject("ADODB.Recordset")
		'strSQL= " SELECT DISTINCT PROPERTYGROUP.CATEGORY_ID FROM SHOPPERQUOTE INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID WHERE  CATEGORY_ID not in (19,24) and SHOPPERQUOTE.QUOTEID = " & quoteid
		strSQL= " SELECT DISTINCT PROPERTYGROUP.CATEGORY_ID FROM SHOPPERQUOTE INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID WHERE  CATEGORY_ID not in (19) and SHOPPERQUOTE.QUOTEID = " & quoteid
		'Response.Write strSQL
		'Response.end
		rsCheckScopesQuote.Open strSQL, dbconnstr()
		if not rsCheckScopesQuote.eof then
			CheckScopesQuote=true
		end if
	end if
end function
'*************************************
'Create Customer Info for SE pager message
'KK 4/18/2005
'Arguments - CustomerID
'Returns string
'*************************************

function PagerCustomerData(CustomerID)

'Select everything from CONTACT table for CustomerID

set rsInfoCC=server.CreateObject("ADODB.Recordset")
strSQL="Select * from CONTACT where CONTACT_ID=" & CustomerID
rsInfoCC.Open strSQL, dbconnstr()
if not rsInfoCC.EOF then
	strCRet = chr(13) & chr(10)
	' Client data
	if len(trim(rsInfoCC("Company")))>0 then
		strInfoCC =  rsInfoCC("Company")& strCRet
	end if
	strInfoCC =  strInfoCC & rsInfoCC("First_Name") & " " & rsInfoCC("Last_Name") & strCRet
	if len(trim(rsInfoCC("Phone")))>0 then
		strInfoCC = strInfoCC & rsInfoCC("Phone")& strCRet
	end if
	if len(trim(rsInfoCC("Email")))>0 then
		strInfoCC = strInfoCC & rsInfoCC("Email")& strCRet
	end if
	PagerCustomerData= strInfoCC
end if
end function
'*************************************
'Get Download Category name (SW)
'Kate Kaplan 5/10/2005
'down_cat_id int
'Returns string
'*************************************
function getdowncatname(down_cat_id)
	if len(down_cat_id)>0 then
		set rsGetdowncatname=server.CreateObject("ADODB.Recordset")
		strSQL= " Select *  FROM DOWNLOAD_CATEGORY WHERE down_cat_id=" & down_cat_id
		
		rsGetdowncatname.Open strSQL, dbconnstr()
		if not rsGetdowncatname.eof then
			getdowncatname=rsGetdowncatname("NAME")
		end if
	end if
end function
'*************************************
'Get the text field from LONGDESCR table
'KK 7/01/2005
'Arguments - tablename,columnname,idintable
'Returns string
'*************************************
Sub GetLongDescription(tablename,columnname,idintable)
	if len(tablename)> 0 and len(columnname)>0 and len(idintable)>0 then
	'select tableid from table "TABLE_ID" (ecatalog)
	set rsTable=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Table_id From Table_id where Table_name='" & tablename & "'"
	'Response.Write strSQL
	rsTable.Open strSQL, dbconnstr()
	if not rsTable.EOF then
		TableID=rsTable("Table_ID")
	end if
	'Response.write TableID & "<br>"
	'close recordset rsTable
	rsTable.Close
	set rsTable=nothing

	'select columnid from table"COLUMN_ID" (ecatalog)
	set rsColumn=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Column_id From Column_id where Column_name='" & columnname & "'"
	rsColumn.Open strSQL, dbconnstr()
	if not rsColumn.EOF then
		ColumnID=rsColumn("Column_ID")
	end if
	'Response.Write ColumnID & "<br>"
	'Response.Write ID
	'close recordset  rsColumn
	 rsColumn.Close
	set  rsColumn=nothing

	'Select value from table "TABLESBYLANGUAGE" (ecatalog)
	'for TableID,COlumnID,ID and LOCALEID
	set rsLongDescr=server.CreateObject("ADODB.Recordset")
	strSQL=	"Select TEXT from LONGDESCR where TABLE_ID=" & TableID & _
			" and COLUMN_ID=" & ColumnID & " and IDINTABLE=" & idintable  & _
			" Order by SEQ_ID"
			'REsponse.Write strSQL
	rsLongDescr.Open strSQL, dbconnstr()
	while not rsLongDescr.EOF 
		'Response.Write rsLongDescr("TEXT")
		LongDescription=LongDescription & rsLongDescr("TEXT")
		rsLongDescr.MoveNext
	wend
	
	Response.write LongDescription
	rsLongDescr.Close
	set rsLongDescr=nothing
	end if
end sub
'*************************************
'Checks whether the WaveSurfer products
'are in the basket (to separate WS quote in US)
'rev1- Kate Kaplan 7/18/05
'rev2- Kate Kaplan 2/8/06 (added WS Xs and WaveJet)
'rev3- Kate Kaplan 5/27/08 (WaveJet only)
'rev4- Kate Kaplan 9/15/08 (added WaveAce only)
'rev5- Kate Kaplan 11/5/08 ( change for WS only)
'Returns boolean
'*************************************
function CheckWaveSurferInBasket()
	CheckWaveSurferInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where model_series in (11,34,43,49)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckWS = server.CreateObject("ADODB.Recordset")
	rsCheckWS.Open strSQL, dbconnstr()
	if not rsCheckWS.eof then
		CheckWaveSurferInBasket = true
	else
		CheckWaveSurferInBasket = false
	end if 
end function
'*************************************
'Gets SENumber on ContactID for US customer
'rev1 - Kate Kaplan 7/18/05
'Rev2 - Kate Kaplan	1/15/2007 (to use new CCMS_ tables)
'rev3 - Kate Kaplan 3/8/2011 (added chack by domain)
'Arguments - ContactID
'Returns String
'*************************************
function GetSENumber(ContactID)
	if len(ContactID)>0 then
	'****************************************************************
	'Check if the customer company is in the company exceptions list
	'****************************************************************
	'Select company name from contact table
		set rsCompany=server.CreateObject("ADODB.Recordset")
		strSQL="Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL,  SUBSTRING(LTRIM(RTRIM(CONTACT.EMAIL)), PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 1, { fn LENGTH(LTRIM(RTRIM(CONTACT.EMAIL))) } - PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 2)  AS DOMAIN  from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=" & ContactID
		rsCompany.Open strSQL, dbconnstr()
		if not rsCompany.EOF then
			strCompany=trim(rsCompany("Company"))
			SpacePosition=instr(1,strCompany," ")
			strCompanyName=strCompany
		'If the company is in exceptions list
		'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES 
			set rsExistCompany=server.CreateObject("ADODB.Recordset")
			strSQL="SELECT a.SECode FROM CCMS_PERSONNEL_DATA AS b INNER JOIN CCMS_EXCEPTION_COMPANIES AS a ON b.ID = a.SECode  where a.STATE='"  & rsCompany("State") & "' and a.CompanyName ='" & replace(strCompanyName,"'","") & "' and a.CITY='" & replace(rsCompany("City"),"'","") & "' AND b.RecType = 'se' and b.Active = 'y' and a.SECode in (Select SECode from CCMS_SECSR)"
            rsExistCompany.Open strSQL,dbconnstr()
			if not rsExistCompany.EOF then
				strSENumber=trim(rsExistCompany("SECode"))
			end if
            if len(strSENumber)=0 then
                set rsExistDomain=server.CreateObject("ADODB.Recordset")
                strSQL="Select a.SECode from CCMS_PERSONNEL_DATA AS b INNER JOIN CCMS_EXCEPTION_DOMAINS AS a ON b.ID = a.SECode where a.STATE='"  & rsCompany("State") & "' and a.CITY='" & replace(rsCompany("City"),"'","") & "' and a.MainDomain ='" & rsCompany("Domain")  & "' AND b.RecType = 'se' and b.Active = 'y'  and a.SECode in (Select SECode from CCMS_SECSR)"
			    rsExistDomain.Open strSQL,dbconnstr()
			    if not rsExistDomain.EOF then
				    strSENumber=trim(rsExistDomain("SECode"))
                else
                    set rsExistDomain2=server.CreateObject("ADODB.Recordset")
                    strSQL="Select a.SECode from CCMS_PERSONNEL_DATA AS b INNER JOIN CCMS_EXCEPTION_DOMAINS AS a ON b.ID = a.SECode where a.STATE='"  & rsCompany("State") & "' and a.MainDomain ='" & rsCompany("Domain")  & "' AND b.RecType = 'se' and b.Active = 'y'  and a.SECode in (Select SECode from CCMS_SECSR)"
			        rsExistDomain2.Open strSQL,dbconnstr()
			        if not rsExistDomain2.EOF then
				        strSENumber=trim(rsExistDomain2("SECode"))
			        end if
			    end if
            end if
		end if
	'****************************************************************
	'If customer company is not in the company exceptions list or
	'Get SENumber by Zip
	'****************************************************************
		if len(strSENumber)=0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SECode for this ZIP CODE from table CCMS_MAP_US
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsSENumber=	server.CreateObject("ADODB.Recordset")
					strSQL="Select SECode from CCMS_MAP_US where ZipCode=" & strZip
					rsSENumber.Open strSQL, dbconnstr()
					if not rsSENumber.EOF then
						strSENumber=trim(rsSENumber("SECode"))
					end if	
		'Close recordset rsSENumber
					rsSENumber.Close
					set rsSENumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		end if
		GetSENumber	=strSENumber
	end if
end function

'*************************************
'Gets SE name on SENumber 
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	1/15/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetSEName(SECode)
	if len(SECode)>0 then
		set rsSEName=server.CreateObject("ADODB.Recordset")
		strSQL="Select FirstName+ ' ' + LastName as SEName from CCMS_PERSONNEL_DATA where RecType='SE' and ID=" & SECode
		on error resume next
		rsSEName.Open strSQL, dbconnstr()
		if not rsSEName.EOF then
			strSEName=trim(rsSEName("SEName"))
		end if
		GetSEName=strSEName
		rsSEName.close
		set rsSEName=nothing
	end if
end function

'*************************************
'Gets SE email on SENumber 
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	1/15/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetSEEmail(SECode)
	if len(SECode)>0 then
		set rsSEEmail=server.CreateObject("ADODB.Recordset")
		strSQL="Select EMail as SEEmail from CCMS_PERSONNEL_DATA where RecType='SE' and ID='" & SECode & "'"
		'Response.Write strSQL
		on error resume next
		rsSEEmail.Open strSQL, dbconnstr()
		if not rsSEEmail.EOF then
			strSEEmail=trim(rsSEEmail("SEEmail"))
		end if
		GetSEEmail=strSEEmail
		rsSEEmail.Close
		set rsSEEmail=nothing
	end if
end function

'*************************************
'Gets SE Pager on SENumber 
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	1/15/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetSEPager(SECode)
	if len(SECode)>0 then
		set rsSEPager=server.CreateObject("ADODB.Recordset")
		strSQL="Select Pager as SEPager from CCMS_PERSONNEL_DATA where RecType='SE' and ID='" & SECode &"'"
		on error resume next
		rsSEPager.Open strSQL, dbconnstr()
		if not rsSEPager.EOF then
			strSEPager=trim(rsSEPager("SEPager"))
		end if
		GetSEPager=strSEPager
		rsSEPager.Close
		set rsSEPager=nothing
	end if
end function

'*************************************
'Gets CSR name on SENumber 
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	1/15/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetCSRName(SECode)
	if len(SECode)>0 then
		set rsCSR=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT  FirstName+ ' ' + LastName as CSR FROM CCMS_PERSONNEL_DATA WHERE RecType='CS' and ID=(select CSRCode from CCMS_SECSR Where SeCode=" & SECode & ")"
		'Response.Write strSQL
		on error resume next
		rsCSR.Open strSQL, dbconnstr()
		if not rsCSR.EOF then
			strCSRName=trim(rsCSR("CSR"))
		else
			strCSRName="Amy Varasteh"
		end if
		GetCSRName=strCSRName
		rsCSR.Close
		set rsCSR=nothing
	end if
end function

'*************************************
'Gets CSR email 
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	1/15/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetCSREmail(SECode)
	if len(SECode)>0 then
		set rsCSREmail=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT Email as CSREmail FROM CCMS_PERSONNEL_DATA WHERE RecType='CS' and ID=(select CSRCode from CCMS_SECSR Where SeCode=" & SECode & ")"
		on error resume next
		rsCSREmail.Open strSQL, dbconnstr()
		if not rsCSREmail.EOF then
			strCSREmail=trim(rsCSREmail("CSREmail"))
		end if
		GetCSREmail=strCSREmail
		rsCSREmail.Close
		set rsCSREmail=nothing
	end if
end function

'************************************************
'Generates QuoteID  for scope products
'and moves requested products
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan 6/23/2009 (remove quote approval for US)
'rev3 - Kate Kaplan 3/4/2010 (added quote approval for US)
'rev4 - Kate Kaplan 9/7/2010 (added REF_SERIES_ID)
'Returns String
'************************************************
function GenerateQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				'strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE,RESPONSE_FLAG,RESPONSE_DATE) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "','true','" & strDate & "')"
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			else
				'strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE,RESPONSE_FLAG,RESPONSE_DATE) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "','true','" & strDate & "')"
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if

	rsQuote.LockType = 3
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"

	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID,REF_SERIES_ID  FROM shopper where SESSIONID = '" & Session("netSessionID") & "'"
					'Response.write strSQl & "<br>"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID,REF_SERIES_ID )" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & "," & rsFromShopper("REF_SERIES_ID") & ")"
			rsIns.LockType = 3
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "'"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
	GenerateQuote=strQuoteID
end function

'************************************************
'Generates QuoteID for WS products and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan	2/8/2006 ( added WS Xs and WaveJet)
'rev3 - Kate Kaplan	5/27/2008 (WaveJet only)
'rev4 - Kate Kaplan	11/5/2008 (WS only)
'rev5 - Kate Kaplan 3/4/2010 (ALL distributor products)
'rev6 - Kate Kaplan 4/23/2010 (added ArbStudio)
'rev6 - Kate Kaplan 11/22/2010 (added LogicbStudio)
'Returns String
'************************************************
function GenerateWSQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if
	rsQuote.LockType = 3
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID  FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  (PRODUCT_SERIES_ID in (13, 33, 34, 43, 44, 49, 50,217,339,399,402,419) OR group_id in (165,166,210))" 
	
	'response.Write strSQL & "<br>"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID )" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.LockType = 3
			'response.Write strSQL & "<br>"
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  (PRODUCT_SERIES_ID in (13, 33, 34, 43, 44, 49, 50,217,339,399,402,419) or group_id in (165,166,210))"
	rsDel.LockType = 3
	'response.Write strSQL & "<br>"
	rsDel.Open strSQL, dbconnstr()
	GenerateWSQuote=strQuoteID
end function
'*************************************
'Gets Quote Header - QuoteID, Dates, Customer info
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan 10/31/06 (WJ Promo)
'rev3 - Kate Kaplan 3/11/2010 - added PROMO valid until date logic
'Arguments - QuoteID integer,emaillocaleid string
'Returns String
'*************************************
function GetQuoteInfo(QuoteID,emaillocaleid)
	        strCRet = chr(13) & chr(10)

	if len(QuoteID)>0 then
        QuoteTime=GetQuoteTimeOnQuoteID(quoteid)
		strInfo=strCRet & LoadI18N("SLBCA0007",emaillocaleid) & ": " & QuoteID & strCRet
		strInfo=strInfo & LoadI18N("SLBCA0069",emaillocaleid) & ": " & FormatDateTime(QuoteTime,2)& strCRet
    	if CheckPromoQuote(QuoteID) then
	        strInfo=strInfo & LoadI18N("SLBCA0070",emaillocaleid) & ": " & GetPromoQuoteTimeOnQuoteID(QuoteID) & strCRet
	    else
	        strInfo=strInfo & LoadI18N("SLBCA0070",emaillocaleid) & ": " & FormatDateTime((QuoteTime+30),2) & strCRet
		end if
		'if len(Session("ContactID"))>0 then
		 '   strQuoteContactID=Session("ContactID")
		'else
		    set rsQuoteContactID=Server.CreateObject("ADODB.Recordset")
	        strSQL="SELECT CONTACTID FROM QUOTE where QUOTEID=" & QuoteID
	        rsQuoteContactID.Open strSQL, dbconnstr()
	        if not rsQuoteContactID.EOF then
	         strQuoteContactID=rsQuoteContactID("CONTACTID")
	         strInfo=strInfo & EmailBodyClientData(strQuoteContactID) 
            end if
		'end if
		GetQuoteInfo=strInfo
	end if
end function
'*************************************
'Creates Quote Footer for US quotes
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan 12/14/05
'rev3 - Kate Kaplan 2/27/2006 (removed Distributor)
'rev4 - Kate Kaplan 9/20/2006 (added 3 year warranty for WS/WJ)
'rev5 - Kate Kaplan 9/28/2006 (added distributor's website link and phone for WS/WJ)
'rev6 - Kate Kaplan 1/3/2007 (removed distributor info)
'rev7 - Kate Kaplan 1/19/2007 (added logic for MR)
'rev8 - Kate Kaplan 6/7/2007 (removed logic for MR)
'rev9 - Kate Kaplan 6/25/2007 (added Protocol Specialist logic)
'rev10 - Kate Kaplan 1/30/2009(added Pert3 logic)
'rev11 - Kate Kaplan 2/11/2009 (added logic for WS IM name if MR exists to be in the quote footer
'rev12 - Kate Kaplan 2/8/2010 (removed Pert3 logic)
'Arguments - QuoteID integer,emaillocaleid string
'Returns String
'*************************************
function GetSECSRQuoteFooter(QuoteID,emaillocaleid)
	if len(QuoteID)>0 then
		'Protocol analyzers Quotes qisplay name of the Protocol Specialist
		'Oscilloscopes - SE name on the quote footer

			if  CheckScopesQuote(QuoteID) then
				if not WaveJetQUOTE_YN(QuoteID)  then
				    strPartFooter= GetCSRName(GetSENumber( Session("ContactID"))) & ", " & LoadI18N("SLBCA0062",emaillocaleid) & strCRet & strCRet 
			        'if len(GetCCMS_Email("MR",GetScopeMRNumber(Session("ContactId"))))>0 and WaveSurferQUOTE_YN(QuoteID) and len(GetCCMS_Email("IM",GetIndirectManagerCode(GetScopeMRNumber(Session("ContactId")))))>0 then
        			'    strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetIMName(GetIndirectManagerCode(GetScopeMRNumber(Session("ContactId")))) & ", US Channel Sales Manager" & strCRet & strCRet
                    'else  
        			    strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetSEName(GetSENumber( Session("ContactID"))) & ", " & LoadI18N("SLBCA0064",emaillocaleid) & strCRet & strCRet
  				    'end if
  				end if
			'elseif PeRT3QUOTE_YN(QuoteID) then
				'if len(GetPSGCSRName(QuoteID,Session("ContactID")))>0 then
				'	strPartFooter= GetPSGCSRName(strQuoteID,ContactID) & ", SALES OPERATIONS REPRESENTATIVE(S)" & strCRet & strCRet 
				'else
				'	strPartFooter= "Teresa Zachary, SALES OPERATIONS REPRESENTATIVE" & strCRet & strCRet 
				'end if
				'	strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " DAVID LI, Business Development Manager"  & strCRet & strCRet
			else
				'******** Protocol Specialist *********
				if len(GetPSGCSRName(QuoteID,Session("ContactID")))>0 then
					strPartFooter= GetPSGCSRName(strQuoteID,ContactID) & ", SALES OPERATIONS REPRESENTATIVE(S)" & strCRet & strCRet 
				else
					strPartFooter= "Teresa Zachary, SALES OPERATIONS REPRESENTATIVE" & strCRet & strCRet 
				end if
				set rsSP=server.CreateObject("ADODB.Recordset")
				strSQL=	" SELECT DISTINCT  PROPERTYGROUP.SUBCAT_ID FROM SHOPPERQUOTE INNER JOIN"&_
						" PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID "&_
						" INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID"&_
						" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND PROPERTYGROUP.CATEGORY_ID = 19"
				rsSP.Open strSQL, dbconnstr()
				if not rsSP.EOF then
					strSP=GetPSGSpecialistsName(QuoteID,Session("ContactID"))
					strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetPSGSpecialistsName(QuoteID,Session("ContactID")) & ", PROTOCOL SALES SPECIALIST(S)"  & strCRet & strCRet
				end if
			end if
			'Common footer fot Email page
			strFooter="-------------------------------------------------------------------------" & strCret& strCret
			if len(strPartFooter)>0 then 'and CheckScopesQuote(QuoteID) then
					strFooter=strFooter & strPartFooter
			end if
			
			if len(Session("offerid"))>0 then
				strFooter=strFooter & strCret & "For Order Placement or Technical Information, Please call 1-800-5-LECROY "
				strFooter=strFooter & "(1-800-553-2769)."
				if WaveSurferQUOTE_YN(QuoteID) then
					strFooter=strFooter & "Standard Warranty is 3 year including Parts, Labor and Return Shipping. "
				else
					strFooter=strFooter & "Standard Warranty is 1 year including Parts, Labor and Return Shipping. "
				end if
				strFooter=strFooter & "These commodities licensed by U.S. for ultimate destination United States. "
				strFooter=strFooter & "Diversion contrary to U.S. law prohibited."
			else
				if  CheckScopesQuote(QuoteID) then 
				    if WaveJetQUOTE_YN(QuoteID) then
	                    strFooter=strFooter & strCret &  "For Order Placement, please contact an authorized LeCroy distributor.  You can find a complete listing here:"& strCret 
	                    strFooter=strFooter & "http://" & ServerName & "/Support/Contact/officelisting.aspx?regionid=5&officetype=D&capid=106&mid=538"& strCret & strCret & strCret
	                    strFooter=strFooter & "For Technical Information, please call 1-800-5-LECROY (1-800-553-2769)." & strCret & strCret & strCret
				    else
				        strFooter=strFooter & strCret & LoadI18N("SLBCA0039",emaillocaleid)& "." & strCRet
					    strFooter=strFooter & LoadI18N("SLBCA0041",emaillocaleid) & "." & strCRet
					    strFooter=strFooter & LoadI18N("SLBCA0042",emaillocaleid) & "." & strCRet
					end if
				end if
			end if
			GetSECSRQuoteFooter=strFooter
	end if
end function

'*************************************
'Sends an email in TEXT format
'rev1 - Kate Kaplan 7/19/05
'Arguments - strTo (String),
'			 strFrom (String),
'			 strCC (String),
'			 strBCC (String),
'			 strSubject (String),
'			 strBody (String)
'*************************************
sub SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject,strBody)
	'if len(strTo)>0 and len(strFrom)>0 and len(strBody)>0 then
	'	Set oMailMessage = Server.CreateObject("CDO.Message")
	'	oMailMessage.To = strTo
	'	oMailMessage.From = strFrom
	'	if len(strCC)>0 then
	'		oMailMessage.cc=strCC
	'	end if
	'	if len(strBCC)>0 then
	'		oMailMessage.Bcc = strBCC
	'	end if
	'	oMailMessage.Subject = strSubject
	'	oMailMessage.TextBody = strBody
	'	if Session("localeid")=1041 or Session("country")="Japan" then
	'		oMailMessage.textBodyPart.Charset = "shift-JIS"
	'	elseif Session("localeid")=1042 or mid(Session("country"),1,5)="Korea" then
	'		oMailMessage.textBodyPart.Charset = "korean"
	'	end if
	'	oMailMessage.Send
	'	Set oMailMessage = Nothing ' Release the object
	'end if
    if len(strTo)>0 and len(strFrom)>0 and len(strBody)>0 then
	    Set oMailMessage = CreateObject("CDO.Message")
	    oMailMessage.Configuration.Fields.Item _
	    ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
	    'Name or IP of remote SMTP server
	    oMailMessage.Configuration.Fields.Item _
	    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") _
	    =Application("GLBLstrMailServer")
	    'Server port
	    oMailMessage.Configuration.Fields.Item _
	    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") _
	    =Application("GLBLstrMailServerPort")
	    oMailMessage.Configuration.Fields.Update
	    oMailMessage.To =strTo
	    oMailMessage.From = strFrom
	    if len(strCC)>0 then
		    oMailMessage.cc = strCC
	    end if
	    if len(strBCC)>0 then
		    oMailMessage.Bcc = strBCC
	    end if
	    oMailMessage.Subject = strSubject
	    oMailMessage.TextBody = strBody
	    if Session("localeid")=1041 or Session("country")="Japan" then
		    oMailMessage.textBodyPart.Charset = "shift-JIS"
	    elseif Session("localeid")=1042 or mid(Session("country"),1,5)="Korea" then
		    oMailMessage.textBodyPart.Charset = "korean"
	    elseif Session("country")="China" then
		    oMailMessage.textBodyPart.Charset = "GB2312"
	    end if
	    oMailMessage.Send
	    Set oMailMessage = Nothing ' Release the object
	    Set iConf = Nothing
	    Set Flds = Nothing
    end if
end sub
'*************************************
'Sends an quotes in TEXT format
'to CSR for approval
'to SE (emails to email and cell phone)
'rev1 - Kate Kaplan 7/18/05
'rev2 - Kate Kaplan 1/3/2007 (removed WS/WJ logic for distributor,
'							 quotes for all products go to SE via
'							 email and pager message
'------------------------------------------------------------------
'rev3 - Kate Kaplan 6/7/2007 
'Scopes quotes will be sent for approval to SEs instead of CSRs
'No change for Protocols - still will be sent to CSRs for approval
'------------------------------------------------------------------
'rev 4 - Kate Kaplan    5/15/2008 - WJ quotes goes to Hilary for approval
'rev 5 - Kate Kaplan    9/15/2008 - WJ and WA quotes goes to Hilary for approval
'rev 6 - Kate Kaplan    11/5/2008 - added WS quotes to go to Hilary for approval
'rev 7 - Kate Kaplan    1/30/2009 - added Serial Data Generators to go to David Li (APG) for approval
'rev 8 - Kate Kaplan    2/11/2009 - WS quotes to go to IMs for approval if there is an MR or to SE if there is no MR
'Arguments - strQuoteID (Integer), ServerName (String),emaillocaleid
'*************************************
sub SendQuoteCSRSE(strQuoteID,emaillocaleid,ServerName)
		'Create E-Mail Body							
		strBodyCSR =  Session("FirstName") & " " & LoadI18N("SLBCA0068",emaillocaleid) & ":" & strCRet 
		strBodyCSR =  EMailBodyNew(strQuoteID) & strCret & strCret
		
		strBodySE=strCret& strCRet &  Session("FirstName") &  " " & LoadI18N("SLBCA0068",emaillocaleid) & ":" & strCRet 
		strBodySE=strCret& strCret & strBodySE & EMailBodyNew(strQuoteID) & strCret 

		strBodyCSR=strBodyCSR & "-------------------------------------------------------------------------" & strCret& strCret

		strBodyCSR= strBodyCSR & strCret & "Click on the link below to approve quote request and send email to customer:" & strCret & strCret & strCret
		
		strBodyCSR = strBodyCSR & "http://" & ServerName & "/ca.asp?cust=" & Session("ContactId") & "&qid=" & strQuoteID & "&lid=" & localeid & strCret & strCret & strCret
		'********* Refuse Quote 
		strBodyCSR=strBodyCSR  & strCret & "-------------------------------------------------------------------------" & strCret& strCret
		strBodyCSR=strBodyCSR  & "If this quote should not be approved, please click on the following link:" & strCret & strCret
		strBodyCSR = strBodyCSR & "http://" & ServerName & "/shopper/ApprovalRefuse.asp?quoteid=" & strQuoteID  & strCret & strCret & strCret
		strInsertReminder=strBodyCSR
		strBodyCSR=strBodyCSR  & strCret & "-------------------------------------------------------------------------" & strCret& strCret
		'**********
		
		'if CheckScopesQuote(strQuoteID) then
			strBodyCSR=strBodyCSR  & "To request this quote to be modified by CSRs and sent to the customer, please click on the following link:" & strCret & strCret
			strBodyCSR = strBodyCSR & "http://" & ServerName & "/shopper/ModifyQuote.asp?cust=" & Session("ContactId") & "&qid=" & strQuoteID & strCret & strCret & strCret
		'end if
		if len(Session("offerid"))>0 then
			strBodyCSR=strBodyCSR & "Special pricing not combinable with other discounts" & strCret 
			strBodySE =strBodySE & "Special pricing not combinable with other discounts"  & strCret 
		end if
		'if CheckScopesQuote(strQuoteID) then
			strEMailBodyCSR=strBodySE & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
			strEMailBodySE=strBodyCSR & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
		'else
		'	strEMailBodyCSR=strBodyCSR & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
		'	strEMailBodySE=strBodySE & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
		'end if
		strInsertReminder = strInsertReminder & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
		'*****************************************
		'Send E-Mail to SE
		'*****************************************
            strSEEmail = "The quote was requested 24 hours ago. The system approved the quote and sent it to the customer" & strCret
 		'Subjectline
		if len(Session("offerid"))=0 then
			strSubject=LoadI18N("SLBCA0105",emaillocaleid) & " - " &  Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
		else
			strSubject=LoadI18N("SLBCA0105",emaillocaleid) & " - " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")  & " - " & GetOfferInfo(Session("CampaignID"),0)
		end if
			if DistributorProductQUOTE(strQuoteID)  then
			   ' strToCustomerFrom="Hilary.Lustig@teledynelecroy.com"
			    'strToSE= "customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com"
                strChangeDate=FormatDateTime(now(),2)
                strToCustomerFrom="Hilary.Lustig@teledynelecroy.com"
                
               strToSE= "customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com"
               
                strSEEmail = strSEEmail & "-------------------------------------------------------------------------" & strCret & strCret
		        strSEEmail = strSEEmail & "To forward the quote to the distributor, please click on the following link:" & strCret & strCret
		        strSEEmail = strSEEmail & "http://" & ServerName & "/shopper/sendtodistributor.asp?cust=" &  Session("ContactId") & "&qid=" & strQuoteID & strCret & strCret 
		        strSEEmail = strSEEmail & "-------------------------------------------------------------------------" & strCret & strCret           	    
			    call SendQuoteEmail("Hilary.Lustig@teledynelecroy.com", "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & strEMailBodySE)
			    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & " - To Hilary " ,GetQuoteInfo(strQuoteID,emaillocaleid) & strEMailBodySE)
			else
			    if len( SendQuoteToSE(strQuoteID,Session("ContactId")) )>0 then
			        strToSE="customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com," & SendQuoteToSE(strQuoteID,Session("ContactId"))
			        strToCustomerFrom =SendQuoteToSE(strQuoteID,Session("ContactId"))
				    call SendQuoteEmail(strToSE, "webquote@teledynelecroy.com","webquote@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",strSubject,GetQuoteInfo(strQuoteID,emaillocaleid) & strEMailBodySE)
				    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & " - " & SendQuoteToSE(strQuoteID,Session("ContactId")),GetQuoteInfo(strQuoteID,emaillocaleid) & strEMailBodySE)
			    else
			    	strToSE="customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com"
			        strToCustomerFrom="webquote@teledynelecroy.com"
				    call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",strSubject & " - " & SendQuoteToSE(strQuoteID,Session("ContactId")),GetQuoteInfo(strQuoteID,emaillocaleid) & strEMailBodySE)
			    end if
                if APGSerialDataBundleQUOTE(strQuoteID) then
                    strToSE=strToSE & "," & "Roy.Chestnut@teledynelecroy.com"
                end if
                if PSGUSBSerialDataBundleQUOTE(strQuoteID) then
                    strToSE=strToSE & "," & GetCCMS_Email("SP",GetPSGSPecialistsNumber(strContactID,1))
                end if
	    
			end if
							
			'Insert Quote Info Into EMAIL_CSR
			set rsInsertMAIL=server.CreateObject("ADODB.Recordset")
			if  CheckScopesQuote(strQuoteID) then
				strSQL="INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION,PSG_YN) values(" & strQuoteID & ",1,'" & Now() & "','n','" & strToCSR & "','webquote@teledynelecroy.com','" & replace(strInsertReminder,"'","''") & "','" & replace(GetQuoteInfo(strQuoteID,emaillocaleid),"'","''") & "','','','n')"
			else
				strSQL="INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION,PSG_YN) values(" & strQuoteID & ",1,'" & Now() & "','n','" & SendQuoteToSE(strQuoteID,Session("ContactId")) & "','webquote@teledynelecroy.com','" & replace(strEMailBodySE,"'","''") & "','" & replace(GetQuoteInfo(strQuoteID,emaillocaleid),"'","''") & "','','','y')"
			end if
			on error resume next
			rsInsertMAIL.Open strSQL, dbconnstr()
			'on error resume next
			call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote " & strQuoteID,strSQL)


			'Insert Quote Info Into EMAIL_CUSTOMER and EMAIL_SE
			if  CheckScopesQuote(strQuoteID) then
			    set rsInsertEMAIL_CUSTOMER=server.CreateObject("ADODB.Recordset")
				strSQL="INSERT into EMAIL_CUSTOMER (QUOTEID, [TO], [FROM], EMAIL_BODY) values(" & strQuoteID & ",'" & Session("Email") & "','" & strToCustomerFrom & "',N'" & replace(QuoteEmailBodyCustomer(strQuoteID,Session("ContactId"),Session("FirstName"),emaillocaleid,ServerName,"CUSTOMER"),"'","''") & "')"
			    on error resume next
			    rsInsertEMAIL_CUSTOMER.Open strSQL, dbconnstr()
			    'on error resume next
			    call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote - EMAIL_CUSTOMER - " & strQuoteID,strSQL)
           
           	    set rsInsertEMAIL_SE=server.CreateObject("ADODB.Recordset")
				strSQL="INSERT into EMAIL_QUOTE_SE (QUOTEID, [TO], [FROM], EMAIL_BODY) values(" & strQuoteID & ",'" & strToSE & "','webquote@teledynelecroy.com',N'" & strSEEmail & replace(QuoteEmailBodyCustomer(strQuoteID,Session("ContactId"),Session("FirstName"),emaillocaleid,ServerName,"SE"),"'","''") & "')"
			    on error resume next
			    rsInsertEMAIL_CUSTOMER.Open strSQL, dbconnstr()
			    'on error resume next
			    call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote - EMAIL_QUOTE_SE - " & strQuoteID,strSQL)

           
            end if
    

			'end if

			'*****************************************
			'Create and send a message to page SE
			'*****************************************
			
			'if len(SendQuotePager(strQuoteID))>0 then
			'		strPageMessage=strPageMessage & Session("Company")& " " & strCRet
			'		strPageMessage=strPageMessage & Session("FirstName") & " " & Session("LastName")& " " & strCRet
			'		if len(Session("Phone"))>0 then
			'			strPageMessage=strPageMessage & Session("Phone")& " " & strCRet 
			'		end if
			'		if len(Session("Phone"))=0 and len(Session("Email"))>0 then
			'			strPageMessage=strPageMessage &  Session("Email")& " " & strCRet 	
			'		end if
			'		strPageMessage=strPageMessage & "Quote #" & strQuoteID& " " & strCRet 
			'		strPageMessage=strPageMessage & Session("strPageBody") & " " & strCRet
			'		strPageMessage=strPageMessage & "Total: " & formatcurrency(Session("QuoteTotalPrice"),2)
			'		'call SendQuoteEmail(SendQuotePager(strQuoteID), "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com","",strPageMessage)
			'		call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webquote@teledynelecroy.com","","",SendQuotePager(strQuoteID),strPageMessage)
			'end if

end sub
'*************************************
'Gets Distributor Email on ContactID for US customer
'rev1 - Kate Kaplan 7/18/05
'Arguments - ContactID
'Returns String
'*************************************
function GetDistMainEmail(ContactID)
	if len(ContactID)>0 then
	'****************************************************************
	'Check if the customer company is in the company exceptions list
	'****************************************************************
	'Select State from contact table
		set rsCompany=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT CONTACT_OFFICE.COMPANY, CONTACT_OFFICE.EMAIL_LEADS FROM CONTACT_OFFICE INNER JOIN tblWSDistributor ON CONTACT_OFFICE.OFFICE_ID = tblWSDistributor.OFFICE_ID INNER JOIN CONTACT ON tblWSDistributor.ZipCode = CONTACT.POSTALCODE WHERE CONTACT.CONTACT_ID =" & ContactID
		rsCompany.Open strSQL, dbconnstr()
		if not rsCompany.EOF then
			strCompany=trim(rsCompany("Company"))
			SpacePosition=instr(1,strCompany," ")
			strCompanyName=strCompany
		'If the company is in exceptions list
		'We check if this company for customer's STATE_PROVINCE exist in tblCOMPANYLOOKUP 
			set rsExistCompany=server.CreateObject("ADODB.Recordset")
			strSQL="Select * from tblCOMPANYLOOKUP where STATE='"  & rsCompany("State") & "' and CompanyName ='" & strCompanyName & "' and CITY='" & replace(rsCompany("City"),"'","") & "'"
			rsExistCompany.Open strSQL,dbconnstr()
			if not rsExistCompany.EOF then
				strSENumber=trim(rsExistCompany("SENumber"))
			end if
		end if
	'****************************************************************
	'If customer company is not in the company exceptions list
	'Get SENumber by Zip
	'****************************************************************
		if len(strSENumber)=0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & Session("ContactID")
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SENumber for this ZIP CODE from table tblAllZips
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsSENumber=	server.CreateObject("ADODB.Recordset")
					strSQL="Select SENumber from tblAllZips where ZipCode=" & strZip
					rsSENumber.Open strSQL, dbconnstr()
					if not rsSENumber.EOF then
						strSENumber=trim(rsSENumber("SENumber"))
					else
					'send error message 
						Set oMailMessage = Server.CreateObject("CDO.Message")
						oMailMessage.To = "kate.kaplan@teledynelecroy.com"
						oMailMessage.From = "SendQuote.asp"
						oMailMessage.Subject = "ERROR!!!"
						oMailMessage.TextBody = "Table tblAllZips in database webdb32 is EMPTY!"
						'oMailMessage.Send
						Set oMailMessage = Nothing ' Release the object
						Response.Redirect "ErrorConnection.asp" 
					end if	
		'Close recordset rsSENumber
					rsSENumber.Close
					set rsSENumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		end if
		GetDistMainEmail=strSENumber
	end if
end function
'*************************************
'Checks if ID WS Distributor exists 
'based on customer's state
'rev1 - Kate Kaplan 7/19/05
'Arguments - CONTACTID
'Returns boolean
'*************************************
function CheckDistExist(ContactID)
	if len(ContactID)>0 then
		CheckDistExist = false
		strSQL = "SELECT DISTINCT tblWSDistributor.OFFICE_ID FROM  STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5) = tblWSDistributor.ZipCode WHERE tblWSDistributor.OFFICE_ID is not null and CONTACT.CONTACT_ID =" & ContactID
		
		'Response.Write strSQL
		set rsCheckDistExist = server.CreateObject("ADODB.Recordset")
		rsCheckDistExist.Open strSQL, dbconnstr()
		if not rsCheckDistExist.eof then
			CheckDistExist = true
		else
			CheckDistExist = false
		end if 
	end if
end function

'*******************************************
'Gets WS Distributor Main Contact Lead Email
'rev1 - Kate Kaplan 7/19/05
'Arguments - ContactID
'Returns String
'*******************************************
function GetWSDistEmail(ContactID)
	if len(ContactID)>0 then
		set rsWSDistEmail=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT EMAIL_LEADS FROM STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5)  = tblWSDistributor.ZipCode INNER JOIN  CONTACT_OFFICE ON tblWSDistributor.OFFICE_ID = CONTACT_OFFICE.OFFICE_ID WHERE     (tblWSDistributor.OFFICE_ID IS NOT NULL) AND CONTACT.CONTACT_ID =" & ContactID
		on error resume next
		rsWSDistEmail.Open strSQL, dbconnstr()
		if not rsWSDistEmail.EOF then
			strGetWSDistEmail=trim(rsWSDistEmail("EMAIL_LEADS"))
		end if
		GetWSDistEmail=strGetWSDistEmail
		rsWSDistEmail.Close
		set rsWSDistEmail=nothing
	end if
end function

'*******************************************
'Gets WS Distributor Name
'rev1 - Kate Kaplan 7/19/05
'Arguments - ContactID
'Returns String
'*******************************************
function GetWSDistName(ContactID)
	if len(ContactID)>0 then
		set rsWSDistName=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT CONTACT_OFFICE.COMPANY FROM STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5)  = tblWSDistributor.ZipCode INNER JOIN  CONTACT_OFFICE ON tblWSDistributor.OFFICE_ID = CONTACT_OFFICE.OFFICE_ID WHERE     (tblWSDistributor.OFFICE_ID IS NOT NULL) AND CONTACT.CONTACT_ID = '" & ContactID & "'"
		'Response.Write strSQL & "<br>"
		on error resume next
		rsWSDistName.Open strSQL, dbconnstr()
		if not rsWSDistName.EOF then
			strWSDistName=trim(rsWSDistName("COMPANY"))
		end if
		GetWSDistName=strWSDistName
		rsWSDistName.Close
		set rsWSDistEmail=nothing
	end if
end function

'*******************************************
'Gets WS Distributor Email CC
'rev1 - Kate Kaplan 7/19/05
'Arguments - ContactID
'Returns String
'*******************************************
function GetWSDistEmailCC(ContactID)
	if len(ContactID)>0 then
		set rsWSDistEmailCC=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT EMAIL_CC FROM STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5)  = tblWSDistributor.ZipCode INNER JOIN  CONTACT_OFFICE ON tblWSDistributor.OFFICE_ID = CONTACT_OFFICE.OFFICE_ID WHERE     (tblWSDistributor.OFFICE_ID IS NOT NULL) AND CONTACT.CONTACT_ID = '" & ContactID & "'"
		on error resume next
		rsWSDistEmailCC.Open strSQL, dbconnstr()
		if not rsWSDistEmailCC.EOF then
			strWSDistEmailCC=trim(rsWSDistEmailCC("EMAIL_CC"))
		end if
		GetWSDistEmailCC=strWSDistEmailCC
		rsWSDistEmailCC.Close
		set rsWSDistEmailCC=nothing
	end if
end function
'Checks if quote contains Protocol Analyzers 
'Kate Kaplan 4/12/2005
'quoteid int
'Returns true if quote contains Protocol Analyzers
'*************************************
function GetPSGEmailCC(quoteid)
	if len(quoteid)>0 then
		set rsGetPSGEmailCC=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT DISTINCT SALES_REP_PSG.EMAIL,"&_
				" PROPERTYGROUP.PROPERTYGROUPID "&_
				" FROM SHOPPERQUOTE INNER JOIN  PROPERTYGROUP "&_
				" ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID "&_
				" INNER JOIN CONTACT ON SHOPPERQUOTE.CUSTOMERID = CONTACT.CONTACT_ID "&_
				" INNER JOIN STATE ON CONTACT.STATE_PROVINCE = STATE.NAME " &_
				" INNER JOIN SALES_PSG ON STATE.SHORT_NAME = SALES_PSG.STATE "&_
				" AND PROPERTYGROUP.SUBCAT_ID = SALES_PSG.SUBCAT_ID INNER JOIN "&_
				" SALES_REP_PSG ON SALES_PSG.SALES_REP_PSG = SALES_REP_PSG.SALES_REP_ID "&_
				" WHERE SHOPPERQUOTE.QUOTEID = " & quoteid & " AND (SALES_PSG.COUNTRY_ID = 208)"
		'Response.Write strSQL
		'Response.end
		rsGetPSGEmailCC.Open strSQL, dbconnstr()
		if not rsGetPSGEmailCC.eof then
			while not rsGetPSGEmailCC.eof 
				if len(strEmail)=0 then
					strEmail=rsGetPSGEmailCC("Email")
				else
					strEmail=strEmail & "," & rsGetPSGEmailCC("Email")
				end if
			rsGetPSGEmailCC.MoveNext
			wend
			GetPSGEmailCC=strEmail
		end if
	end if
end function
'*************************************
'Gets PSG SENumber on ContactID for US customer
'rev1 - Kate Kaplan 12/8/05
'Arguments - ContactID
'Returns String
'*************************************
function GetPSGSENumber(ContactID)
	if len(ContactID)>0 then

	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SENumber for this ZIP CODE from table tblAllZips
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsSENumber=	server.CreateObject("ADODB.Recordset")
					strSQL="Select SENumber from tblPSGZips where ZipCode=" & strZip
					rsSENumber.Open strSQL, dbconnstr()
					if not rsSENumber.EOF then
						strSENumber=trim(rsSENumber("SENumber"))
					end if	
		'Close recordset rsSENumber
					rsSENumber.Close
					set rsSENumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		GetPSGSENumber	=strSENumber
	end if
end function

'*************************************
'Formats phone number to area_code - 3digits - 4 digits format
'rev1 - Kate Kaplan 12/16/05
'Arguments - strPhone
'Returns String
'*************************************
function FormatPhoneNumber(strPhone)
if len(strPhone)>0 then
	FormatPhoneNumber=mid(strPhone,1,3) & "-" & mid(strPhone,4,3) & "-" & mid(strPhone,7)
end if
end function

'*******************************************************
'Check if Model Series has Firmware Downloads available
'KK 9/26/2006
'Arguments - mseries
'Returns boolean
'*******************************************************
function CheckFirmwareExist(mseries)
'Open recordset
	if len(mseries)>0 then
		set rsCheckFirmwareExist=server.CreateObject("ADODB.Recordset")
		strSQL=	" SELECT *  FROM  DOWNLOAD_XREF WHERE PRODUCT_ID IN  " &_
				" (SELECT  productid FROM PRODUCT "&_
				" WHERE      model_series = " & mseries& ") AND " &_
				" DOWNLOAD_ID IN (SELECT  download_id FROM download WHERE  post = 'y')"
	'response.Write strSQL
		rsCheckFirmwareExist.Open strSQL, dbconnstr()
		if not rsCheckFirmwareExist.EOF then
			CheckFirmwareExist=true
		else
			CheckFirmwareExist=false
		end if
	'Close recordset
		rsCheckFirmwareExist.Close
		set rsCheckFirmwareExist=nothing
	else
		CheckFirmwareExist=false
	end if
end function

'*******************************************
'Gets WS Distributor Phone
'rev1 - Kate Kaplan 9/28/2006
'Arguments - ContactID
'Returns String
'*******************************************
function GetWSDistPhone(ContactID)
	if len(ContactID)>0 then
		set rsWSDistPhone=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT CONTACT_OFFICE.PHONE FROM STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5)  = tblWSDistributor.ZipCode INNER JOIN  CONTACT_OFFICE ON tblWSDistributor.OFFICE_ID = CONTACT_OFFICE.OFFICE_ID WHERE     (tblWSDistributor.OFFICE_ID IS NOT NULL) AND CONTACT.CONTACT_ID ='" & ContactID & "'"
		'Response.Write strSQL & "<br>"
		on error resume next
		rsWSDistPhone.Open strSQL, dbconnstr()
		if not rsWSDistPhone.EOF then
			strWSDistPhone=trim(rsWSDistPhone("PHONE"))
		end if
		GetWSDistPhone=strWSDistPhone
		rsWSDistPhone.Close
		set rsWSDistPhone=nothing
	end if
end function

'*******************************************
'Gets WS Distributor WEB
'rev1 - Kate Kaplan 9/28/2006
'Arguments - ContactID
'Returns String
'*******************************************
function GetWSDistWEB(ContactID)
	if len(ContactID)>0 then
		set rsWSDistWEB=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT DISTINCT CONTACT_OFFICE.WEB FROM STATE INNER JOIN  CONTACT ON STATE.NAME = CONTACT.STATE_PROVINCE INNER JOIN  tblWSDistributor ON STATE.SHORT_NAME = tblWSDistributor.State AND SUBSTRING(CONTACT.POSTALCODE, 1, 5)  = tblWSDistributor.ZipCode INNER JOIN  CONTACT_OFFICE ON tblWSDistributor.OFFICE_ID = CONTACT_OFFICE.OFFICE_ID WHERE     (tblWSDistributor.OFFICE_ID IS NOT NULL) AND CONTACT.CONTACT_ID ='" & ContactID & "'"
		'Response.Write strSQL & "<br>"
		on error resume next
		rsWSDistWEB.Open strSQL, dbconnstr()
		if not rsWSDistWEB.EOF then
			strWSDistWeb=trim(rsWSDistWEB("WEB"))
		end if
		GetWSDistWEB=strWSDistWeb
		rsWSDistWEB.Close
		set rsWSDistWEB=nothing
	end if
end function

'*************************************
'Checks whether the product is a WaveJet
'rev1 - Kate Kaplan 10/20/06 
'rev2 - Kate Kaplan 9/15/08 ( added WaveAce)
'rev3 - Kate Kaplan (3/30/2009 - added WJ 300A)
'QuoteID
'Returns true if any of the products from a quote
'is for WJ or WA products
'*************************************

function WaveJetQUOTE_YN(QuoteID)
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE WHERE QUOTEID = '" & QuoteID & "'"
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	WaveJetQUOTE_YN = false
	
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
		do while not rsMasterID.EOF 
			if len(trim(rsMasterID("MasterProductID"))) > 0 then
				strSQL = "SELECT MODEL_SERIES FROM PRODUCT WHERE PRODUCTID = " & rsMasterID("MasterProductID")
				set rsModelSeries = server.CreateObject ("ADODB.Recordset")
				rsModelSeries.Open strSQL, dbconnstr()
				if not rsModelSeries.EOF then
					if len(rsModelSeries("Model_Series"))>0 then
						if 	cint(rsModelSeries("Model_Series")) =33 or cint(rsModelSeries("Model_Series")) =44 or cint(rsModelSeries("Model_Series")) =50 then
							WaveJetQUOTE_YN = true
							exit function
						end if 
					end if
				end if 
			end if 	
			rsMasterID.MoveNext 
		loop
	else
		WaveJetQUOTE_YN = false
		exit function
	end if 
	
end function
'*************************************
'Check if product is in quote
'KK - 10/30/2006
'arguments (quoteid,prodid)
'Returns boolean
'*************************************	
function CheckProductinQuote(quoteid,prodid)
	Set rs = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE WHERE PRODUCTID = '" & prodid & "' AND QUOTEID = '" &  quoteid & "'"
	rs.Open strSQL, dbconnstr()
	if not rs.EOF then
		rs.MoveFirst
		Do While not rs.EOF 
			if Cint(rs("PRODUCTID")) = Cint(prodid) then
				CheckProductinQuote = True
			else
				CheckProductinQuote = False
			end if
		rs.MoveNext
		Loop	
		rs.Close
		set rs = Nothing
	end if
end function
'*************************************
'Checks whether the PSG products
'are in the basket (to separate PSG quotes in US)
'rev1- Kate Kaplan 12/07/06
'Returns boolean
'*************************************
function CheckProtocolsInBasket()
	CheckProtocolsInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where propertygroupid in(select propertygroupid from propertygroup where category_id=19)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckProtocolsInBasket = server.CreateObject("ADODB.Recordset")
	rsCheckProtocolsInBasket.Open strSQL, dbconnstr()
	if not rsCheckProtocolsInBasket.eof then
		CheckProtocolsInBasket = true
	else
		CheckProtocolsInBasket = false
	end if 
end function

'************************************************
'Generates QuoteID for PSG products and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 12/7/2006
'Returns String
'************************************************
function GeneratePSGQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID")  & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID  FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where PROPERTYGROUPID in (SELECT PROPERTYGROUPID From PROPERTYGROUP where CATEGORY_ID=19))"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID)" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext

			
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where PROPERTYGROUPID in (SELECT PROPERTYGROUPID From PROPERTYGROUP where CATEGORY_ID=19))"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
'Update Quote table with SENumber
		set rsPSGSECode=server.CreateObject("ADODB.Recordset")
		strSQL=" Update QUOTE set SE_CODE='" & GetPSGSpecialistSECode(strQuoteID) & "' where QUOTEID=" & strQuoteID
		'Response.Write strSQL
		rsPSGSECode.Open strSQL, dbconnstr()
		GeneratePSGQuote=strQuoteID
end function

'*************************************
'Checks whether the customer is from Europe or not.
'rev1 - 12/15/2006 - Kate Kaplan
'arguments none
'Returns true if customer from Europe
'*************************************
function CheckcustomerfromEurope
	'delete all disabled products from basked
	ValidateUserNoRedir()
	If Len(Session("country")) > 0 then 

		RegionID = GetRegionOnCountry(Session("country"))
		'response.Write RegionID
		'response.End 
		if cint(RegionID) <> 0 then
			if cint(RegionID) = 2 then
				CheckcustomerfromEurope= true
			else
				CheckcustomerfromEurope= false
			end if 
		else
			CheckcustomerfromEurope = false
		end if 
	else
		CheckcustomerfromEurope = false	
	end if 
end function


'*************************************
'Gets Email from CCMS_PERSONNEL_DATA
'rev1 - Kate Kaplan 12/26/2006
'rev2 - Kate Kaplan	2/8/2007 (added EmailCC)
'rev3 - Kate Kaplan	7/29/2008(added  ACTIVE='Y')
'Arguments - RecType, ID
'Returns String
'*************************************
function GetCCMS_Email(RecType, ID)
	if len(RecType)>0 and len(ID)>0 then
		set rsCCMS_Email=server.CreateObject("ADODB.Recordset")
		strSQL="Select EMAIL, EMAILCC from CCMS_PERSONNEL_DATA where RecType='" & RecType & "' and ID='" & ID & "' and ACTIVE='Y'"
		'Response.Write strSQL
		on error resume next
		rsCCMS_Email.Open strSQL, dbconnstr()
		if not rsCCMS_Email.EOF then
			if len(trim(rsCCMS_Email("Email")))>0 then
				strGetCCMS_Email=trim(rsCCMS_Email("Email"))
			end if
			if len(trim(rsCCMS_Email("EmailCC")))>0 then
				if len(strGetCCMS_Email)=0 then
					strGetCCMS_Email=trim(rsCCMS_Email("EmailCC"))
				else
					strGetCCMS_Email= strGetCCMS_Email & "," &trim(rsCCMS_Email("EmailCC"))
				end if
			end if
		end if
		GetCCMS_Email=strGetCCMS_Email
		rsCCMS_Email.Close
		set rsCCMS_Email=nothing
	end if
end function

'*************************************
'Gets Protocol MR on ContactID and product category for US customer
'rev1 - Kate Kaplan 12/26/2006
'Arguments - ContactID, CategoryID
'Returns String
'*************************************
function GetPSGMRNumber(ContactID, CategoryID)
	if len(ContactID)>0 and len(CategoryID)>0 then
	'****************************************************************
	'Check if the customer company is in the company exceptions list
	'****************************************************************
	'Select company name from contact table
		set rsCompany=server.CreateObject("ADODB.Recordset")
		strSQL="Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL as EMAIL from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=" & ContactID
		rsCompany.Open strSQL, dbconnstr()
		if not rsCompany.EOF then
			strCompany=trim(rsCompany("Company"))
			SpacePosition=instr(1,strCompany," ")
			strCompanyName=strCompany
		'If the company is in exceptions list
		'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES 
			set rsExistCompany=server.CreateObject("ADODB.Recordset")
			select case CategoryID
			case 1
				strSQL="Select PeripheralMRCode  as SECode from CCMS_EXCEPTION_COMPANIES where STATE='"  & rsCompany("State") & "' and CompanyName ='" & replace(strCompanyName,"'","") & "' and CITY='" & replace(rsCompany("City"),"'","") & "'"
			case 2
				strSQL="Select PCIEMRCode as SECode from CCMS_EXCEPTION_COMPANIES where STATE='"  & rsCompany("State") & "' and CompanyName ='" & replace(strCompanyName,"'","")  & "' and CITY='" & replace(rsCompany("City"),"'","") & "'"
			case 3
				strSQL="Select StorageMRCode as SECode from CCMS_EXCEPTION_COMPANIES where STATE='"  & rsCompany("State") & "' and CompanyName ='" & replace(strCompanyName,"'","")  & "' and CITY='" & replace(rsCompany("City"),"'","") & "'"
			case else
				strSQL=""
			end select
						
			rsExistCompany.Open strSQL,dbconnstr()
			if not rsExistCompany.EOF then
				strSENumber=trim(rsExistCompany("SECode"))
			end if
		end if
	'****************************************************************
	'If customer company is not in the company exceptions list
	'Get SENumber by Zip
	'****************************************************************
		if len(strSENumber)=0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SENumber for this ZIP CODE from table tblAllZips
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsPSGMRNumber=	server.CreateObject("ADODB.Recordset")
					select case CategoryID
					case 1
						strSQL="Select PeripheralMRCode as PSGMRCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 2
						strSQL="Select PCIEMRCode as PSGMRCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 3
						strSQL="Select StorageMRCode as PSGMRCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case else
						strSQL=""
					end select
					'Response.Write strSQL
					rsPSGMRNumber.Open strSQL, dbconnstr()
					if not rsPSGMRNumber.EOF then
						strPSGMRNumber=trim(rsPSGMRNumber("PSGMRCODE"))
					end if	
		'Close recordset rsSENumber
					rsPSGMRNumber.Close
					set rsPSGMRNumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		end if
		GetPSGMRNumber	=strPSGMRNumber
end if
end function
'*************************************
'Gets list of PSG Specialists by Product Category
'rev1 - Kate Kaplan 12/26/2006
'rev2 - Kate Kaplan 11/5/2008 (added PCI/PCI-X and Accessories logic)
'Arguments - QuoteID
'Returns String
'*************************************
function GetPSGSpecialists( QuoteID)

if len(QuoteID)>0 then
	set rsGetQuoteProducts=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT DISTINCT c.CUSTOMERID, b.SUBCAT_ID " &_
            " FROM PRODUCT_SERIES_CATEGORY AS a INNER JOIN PRODUCT_SERIES AS b " &_
            " ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " &_
            " SHOPPERQUOTE AS c INNER JOIN PRODUCT AS d ON c.PRODUCTID = d.PRODUCTID ON " &_
            " a.PRODUCT_ID = d.PRODUCTID " &_
            " WHERE   (a.CATEGORY_ID = 19) and c.QUOTEID = " & QuoteID
	'Response.Write strSQL &"<br>"
	rsGetQuoteProducts.open strSQL, dbconnstr()
	if not rsGetQuoteProducts.EOF then
		while not rsGetQuoteProducts.EOF
			select case rsGetQuoteProducts("SUBCAT_ID") 
			case 1
			'Peripheral Solutions
				strGetPSGSpecialists=GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),1))
			case 2
			'PCI Express Solutions
				if len(strGetPSGSpecialists)=0 then
					strGetPSGSpecialists=GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),2))
				else
					strGetPSGSpecialists=strGetPSGSpecialists & ", " & GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),2))
				end if
			case 3
			'Storage Protocol Solutions
				if len(strGetPSGSpecialists)=0 then
					strGetPSGSpecialists=GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),3))
				else
					strGetPSGSpecialists=strGetPSGSpecialists & ", " & GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),3))
				end if
			case 6
			'Accessories
				if len(strGetPSGSpecialists)=0 then
					strGetPSGSpecialists=GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),6))
				else
					strGetPSGSpecialists=strGetPSGSpecialists & ", " & GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),6))
				end if				
			case 7
			'PCI/PCI-X Solutions
				if len(strGetPSGSpecialists)=0 then
					strGetPSGSpecialists=GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),7))
				else
					strGetPSGSpecialists=strGetPSGSpecialists & ", " & GetCCMS_Email("SP",GetPSGSPecialistsNumber(Session("ContactID"),7))
				end if				
			end select
			rsGetQuoteProducts.MoveNext
		wend
		GetPSGSpecialists=strGetPSGSpecialists
	end if
end if
end function
'*************************************
'Gets PSG Specialist's # by Product Category and Location
'rev1 - Kate Kaplan 12/29/2006
'rev2 - Kate Kaplan 11/5/2008 (added PCI/PCI-X and Accessories logic)
'Arguments - ContactID, Category
'Returns String
'*************************************
function GetPSGSPecialistsNumber(ContactID, CategoryID)
	if len(ContactID)>0 and len(CategoryID)>0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SENumber for this ZIP CODE from table CCMS_MAP_US
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsPSGSPNumber=	server.CreateObject("ADODB.Recordset")
					select case CategoryID
					case 1
						strSQL="Select PeripheralSpecCode as PSGSPCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 2
						strSQL="Select PCIESpecCode as PSGSPCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 3
						strSQL="Select StorageSpecCode as PSGSPCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 6
						strSQL="Select AccessoriesSpecCode as PSGSPCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case 7
						strSQL="Select PCIPCIXSpecCode  as PSGSPCODE from CCMS_MAP_US where ZipCode='" & strZip & "'"
					case else
						strSQL=""
					end select
					'Response.Write strSQL
					rsPSGSPNumber.Open strSQL, dbconnstr()
					if not rsPSGSPNumber.EOF then
						strPSGSPNumber=trim(rsPSGSPNumber("PSGSPCODE"))
					end if	
		'Close recordset rsSENumber
					rsPSGSPNumber.Close
					set rsPSGSPNumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		GetPSGSPecialistsNumber	=strPSGSPNumber
end if
end function

'*************************************
'Gets list of MR email by Product Group
'rev1 - Kate Kaplan 12/28/2006
'Arguments - QuoteID
'Returns String
'*************************************
function GetMREmails( QuoteID)

if len(QuoteID)>0 then
	set rsGetMREmails=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT DISTINCT SHOPPERQUOTE.PROPERTYGROUPID "&_
			" FROM         SHOPPERQUOTE INNER JOIN   PROPERTYGROUP "&_
			" ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID "&_
            " WHERE PROPERTYGROUP.CATEGORY_ID=19 AND (PROPERTYGROUP.MR_YN = 'y') and SHOPPERQUOTE.QUOTEID = " & QuoteID
	'Response.Write strSQL &"<br>"
	rsGetMREmails.open strSQL, dbconnstr()
	if not rsGetMREmails.EOF then
		GetMREmails=GetCCMS_Email("MR",GetMRNumber(Session("ContactID")))
	end if
end if
end function
'*************************************
'Gets list of  PSG MR by Product Category
'rev1 - Kate Kaplan 1/11/2007
'Arguments - ContactID
'Returns String
'*************************************
function GetPSGMRs(QuoteID)

if len(QuoteID)>0 then
	set rsGetMRProducts=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT DISTINCT PROPERTYGROUP.SUBCAT_ID "&_
			" FROM  SHOPPERQUOTE INNER JOIN  PROPERTYGROUP "&_
			" ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID "&_
            " WHERE PROPERTYGROUP.CATEGORY_ID=19 and PROPERTYGROUP.MR_YN='y' and SHOPPERQUOTE.QUOTEID = " & QuoteID 
	'Response.Write strSQL &"<br>"
	rsGetMRProducts.open strSQL, dbconnstr()
	if not rsGetMRProducts.EOF then
		while not rsGetMRProducts.EOF
			select case rsGetMRProducts("SUBCAT_ID") 
			case 1
			'Peripheral Solutions
				strGetPSGMRs=GetCCMS_Email("MR",GetPSGMRNumber(Session("ContactID"),1))
			case 2
			'PCI Express Solutions
				if len(strGetPSGMRs)=0 then
					strGetPSGMRs=GetCCMS_Email("MR",GetPSGMRNumber(Session("ContactID"),2))
				else
					strGetPSGMRs=strGetPSGMRs & ", " & GetCCMS_Email("MR",GetPSGMRNumber(Session("ContactID"),2))
				end if
			case 3
			'Storage Protocol Solutions
				if len(strGetPSGMRs)=0 then
					strGetPSGMRs=GetCCMS_Email("MR",GetPSGMRNumber(Session("ContactID"),3))
				else
					strGetPSGMRs=strGetPSGMRs & ", " & GetCCMS_Email("MR",GetPSGMRNumber(Session("ContactID"),3))
				end if
			end select
			rsGetMRProducts.MoveNext
		wend
		GetPSGMRs=strGetPSGMRs
	end if
end if
end function
'*************************************
'Gets list of emails of SEs, MRs or SPs 
'to send a quote to
'rev1 - Kate Kaplan 1/15/2007
'rev2 - Kate Kaplan 5/16/2007 (sending quotes to scope MRs)
'rev3 - Kate Kaplan 6/19/2007 (added ContactID argument)
'rev4 - Kate Kaplan 9/5/2007 (added SEEmailCC)
'rev5 - Kate Kaplan 9/7/2007 (added PSG specialist logic)
'Arguments - QuoteID,ContactID
'Returns String
'*************************************
Function SendQuoteToSE(QuoteID,ContactID)
	if len(QuoteID)>0 and len(ContactID)>0 then
	'******SE Email**********
	'check if there are products in the quote
	'that are not listed in the CCMS_PRODUCT_NOTIFY table
	'if yes, the quote needs to be sent to SE
	if CheckScopesQuote(QuoteID) then
		set rsEmailSE=server.CreateObject("ADODB.Recordset")
		strSQL=	" SELECT SHOPPERQUOTE.PRODUCTID FROM SHOPPERQUOTE INNER JOIN PRODUCT" &_
				" ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID "&_
				" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND PRODUCT.PARTNUMBER NOT IN"&_
				" (SELECT     PRODUCTCODE    FROM   CCMS_PRODUCT_NOTIFY)"
		'Response.Write strSQL
		rsEmailSE.Open strSQL, dbconnstr()
		if not rsEmailSE.EOF then
			strEmailSE=GetSEEmail(GetSENumber(ContactID))
			if len(GetSEEmailCC(GetSENumber(ContactID)))>0 then
				if len(strEmailSE)>0 then
					strEmailSE=strEmailSE & "," & GetSEEmailCC(GetSENumber(ContactID))
				else
					strEmailSE= GetSEEmailCC(GetSENumber(ContactID))
				end if
			end if
		end if
		'if strEmailSE is empty,
		'Check if SEEmail flag in the CCNS_PRODUCT_NOTIFY table 
		'for any products in the Quote is "Y" 
		if len(strEmailSE)=0 then
			set rsCheckSeMail=server.CreateObject("ADODB.Recordset")
			strSQL=	" SELECT * FROM SHOPPERQUOTE INNER JOIN PRODUCT ON "&_
					" SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN"&_
					" CCMS_PRODUCT_NOTIFY ON PRODUCT.BAAN_PARTNUMBER ="&_
					" CCMS_PRODUCT_NOTIFY.ProductCode "&_
					" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND CCMS_PRODUCT_NOTIFY.SEMail = 'Y'"
			rsCheckSeMail.Open strSQL, dbconnstr()
			if not rsCheckSeMail.EOF then
				strEmailSE=GetSEEmail(GetSENumber(ContactID))
			end if
		end if
		'********* Oscilloscope MRs
	'		set rsScopeMR=server.CreateObject("ADODB.Recordset")
	'		strSQL=	" SELECT * FROM SHOPPERQUOTE INNER JOIN "&_
	'				" PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN"&_
	'				" CCMS_PRODUCT_NOTIFY ON PRODUCT.BAAN_PARTNUMBER = CCMS_PRODUCT_NOTIFY.ProductCode"&_
	'				" INNER JOIN PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID "&_
	'				" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND PROPERTYGROUP.SUBCAT_ID IS  NULL AND CCMS_PRODUCT_NOTIFY.MRMail = 'Y'"
	'		rsScopeMR.Open strSQL, dbconnstr()
	'		if not rsScopeMR.EOF then
	'			if len(GetCCMS_Email("MR",GetScopeMRNumber(ContactID)))>0 then
	'				strScopeMR=GetCCMS_Email("MR",GetScopeMRNumber(ContactID)) '& ",Barry.Kitaen@teledynelecroy.com"
	'			end if
	'			if len(strEmailSE)=0 then
	'				strEmailSE=GetSEEmail(GetSENumber(ContactID))
	'			end if
	'		end if
	   
		else
		'******** Protocol Specialist *********
			'set rsSP=server.CreateObject("ADODB.Recordset")
			'strSQL=	" SELECT DISTINCT  PROPERTYGROUP.SUBCAT_ID FROM SHOPPERQUOTE INNER JOIN"&_
			'		" PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN"&_
			'		" CCMS_PRODUCT_NOTIFY ON PRODUCT.BAAN_PARTNUMBER = CCMS_PRODUCT_NOTIFY.ProductCode"&_
			'		" INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID"&_
			'		" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND PROPERTYGROUP.CATEGORY_ID = 19 AND CCMS_PRODUCT_NOTIFY.SpecMail = 'Y'"
			'rsSP.Open strSQL, dbconnstr()
			'if not rsSP.EOF then
			'	strSP=GetPSGSpecialists(QuoteID)
			'end if
			if len(GetCCMS_Email("SP",GetSPNumber(ContactID)))>0 then
                 strSP=GetCCMS_Email("SP",GetSPNumber(ContactID))
            end if

		'	if len(GetPSGSpecialists(QuoteID))>0 then
		'		strSP=GetPSGSpecialists(QuoteID)
		'	end if

			'********* Protocol MRs
			set rsProtocolMR=server.CreateObject("ADODB.Recordset")
	        strSQL= " SELECT DISTINCT PRODUCT_SERIES.SUBCAT_ID FROM PRODUCT_SERIES_CATEGORY INNER JOIN "&_
                     " PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN"&_
                     " SHOPPERQUOTE INNER JOIN PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN "&_
                     " CCMS_PRODUCT_NOTIFY ON PRODUCT.BAAN_PARTNUMBER = CCMS_PRODUCT_NOTIFY.ProductCode ON "&_ 
                     " PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID "&_
                     " WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & "  AND (CCMS_PRODUCT_NOTIFY.MRMail = 'Y')  "&_
                     " AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 19)"
              rsProtocolMR.Open strSQL, dbconnstr()
			if not rsProtocolMR.EOF then
				if len(GetPSGMRs(QuoteID))>0 then
					strProtocolMR=GetPSGMRs(QuoteID) '& ",Adam.Frisius@teledynelecroy.com"
				end if
			end if

	end if
		'Get a string of all emails
		if len(strEmailSE)>0 then
			strSendQuoteToSE=strEmailSE
		end if
		if len(strSP)>0 then
			if len(strSendQuoteToSE)=0 then
				strSendQuoteToSE=strSP
			else
				strSendQuoteToSE=strSendQuoteToSE & "," & strSP
			end if
		end if
		if len(strProtocolMR)>0 then
			if len(strSendQuoteToSE)=0 then
				strSendQuoteToSE=strProtocolMR
			else
				strSendQuoteToSE=strSendQuoteToSE & "," & strProtocolMR
			end if		
		end if
'		if len(strScopeMR)>0 then
'			if len(strSendQuoteToSE)=0 then
'				strSendQuoteToSE=strScopeMR
'			else
'				strSendQuoteToSE=strSendQuoteToSE & "," & strScopeMR
'			end if		
'		end if
		SendQuoteToSE=strSendQuoteToSE
	end if
end function

'*************************************
'Gets Scope MR on ContactID for US customer
'rev1 - Kate Kaplan 12/26/2006
'Arguments - ContactID, CategoryID
'Returns String
'*************************************
function GetScopeMRNumber(ContactID)
	if len(ContactID)>0  then
	'****************************************************************
	'Check if the customer company is in the company exceptions list
	'****************************************************************
	'Select company name from contact table
		set rsCompany=server.CreateObject("ADODB.Recordset")
		strSQL="Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL as EMAIL from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=" & ContactID
		rsCompany.Open strSQL, dbconnstr()
		if not rsCompany.EOF then
			strCompany=trim(rsCompany("Company"))
			SpacePosition=instr(1,strCompany," ")
			strCompanyName=strCompany
		'If the company is in exceptions list
		'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES 
			set rsExistCompany=server.CreateObject("ADODB.Recordset")
			strSQL="Select ScopeMRCode as SECode from CCMS_EXCEPTION_COMPANIES where STATE='"  & rsCompany("State") & "' and CompanyName ='" & replace(strCompanyName,"'","") & "' and CITY='" & replace(rsCompany("City"),"'","") & "' and SECode in (Select SECode from CCMS_SECSR)"
			rsExistCompany.Open strSQL,dbconnstr()
			if not rsExistCompany.EOF then
				strSENumber=trim(rsExistCompany("SECode"))
			end if
		end if
	'****************************************************************
	'If customer company is not in the company exceptions list
	'Get SENumber by Zip
	'****************************************************************
		if len(strSENumber)=0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SENumber for this ZIP CODE from table CCMS_MAP_US
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsScopeMRNumber=	server.CreateObject("ADODB.Recordset")
					strSQL="Select ScopeMRCode  from CCMS_MAP_US where ZipCode='" & strZip & "'"
					'Response.Write strSQL
					rsScopeMRNumber.Open strSQL, dbconnstr()
					if not rsScopeMRNumber.EOF then
						strScopeMRNumber=trim(rsScopeMRNumber("ScopeMRCode"))
					end if	
		'Close recordset rsSENumber
					rsScopeMRNumber.Close
					set rsScopeMRNumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		end if
		GetScopeMRNumber=strScopeMRNumber
end if
end function
'*************************************
'Gets list of pagers of SEs, MEs or SPs 
'to send a quote to
'rev1 - Kate Kaplan 1/18/2007
'Arguments - QuoteID
'Returns String
'*************************************
Function SendQuotePager(QuoteID)
	if len(QuoteID)>0 and len(Session("ContactID"))>0 then
	'******SE Email**********
	'check if there are products in the quote
	'that are not listed in the CCNS_PRODUCT_NOTIFY table
	'if yes, the quote needs to be sent to SE
		set rsPagerSE=server.CreateObject("ADODB.Recordset")
		strSQL=	" SELECT SHOPPERQUOTE.PRODUCTID FROM SHOPPERQUOTE INNER JOIN PRODUCT" &_
				" ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID "&_
				" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND PRODUCT.PARTNUMBER NOT IN"&_
				" (SELECT     PRODUCTCODE    FROM   CCMS_PRODUCT_NOTIFY)"
		'Response.Write strSQL
		rsPagerSE.Open strSQL, dbconnstr()
		if not rsPagerSE.EOF then
			strPagerSE=GetSEPager(GetSENumber(Session("ContactID")))
		end if
		'if strEmailSE is empty,
		'Check if SEEmail flag in the CCNS_PRODUCT_NOTIFY table 
		'for any products in the Quote is "Y" 
		if len(strPagerSE)=0 then
			set rsCheckSeMail=server.CreateObject("ADODB.Recordset")
			strSQL=	" SELECT * FROM SHOPPERQUOTE INNER JOIN PRODUCT ON "&_
					" SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID INNER JOIN"&_
					" CCMS_PRODUCT_NOTIFY ON PRODUCT.BAAN_PARTNUMBER ="&_
					" CCMS_PRODUCT_NOTIFY.ProductCode "&_
					" WHERE SHOPPERQUOTE.QUOTEID = " & QuoteID & " AND CCMS_PRODUCT_NOTIFY.SEPager = 'Y'"
			rsCheckSeMail.Open strSQL, dbconnstr()
			if not rsCheckSeMail.EOF then
				strPagerSE=GetSEPager(GetSENumber(Session("ContactID")))
			end if
		end if

		'Get a string of all emails
		if len(strPagerSE)>0 then
			strSendQuotePager=strPagerSE
		end if

		SendQuotePager=strSendQuotePager
	end if
end function

'*************************************
'Gets list of all CSRs from CCMS
'to send a quote to
'rev1 - Kate Kaplan 6/12/2007
'Returns String
'*************************************
function GetALLCSREmails()
	set rsAllCSREmail=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT DISTINCT CCMS_PERSONNEL_DATA_1.Email AS CSREmail FROM  CCMS_SECSR INNER JOIN CCMS_PERSONNEL_DATA AS CCMS_PERSONNEL_DATA_1 ON CCMS_SECSR.CSRCode = CCMS_PERSONNEL_DATA_1.ID INNER JOIN CCMS_PERSONNEL_DATA ON CCMS_SECSR.SECode = CCMS_PERSONNEL_DATA.ID WHERE (CCMS_PERSONNEL_DATA.RecType = 'SE') AND (CCMS_PERSONNEL_DATA_1.RecType = 'CS') AND (CCMS_PERSONNEL_DATA_1.Active = 'Y') AND (CCMS_PERSONNEL_DATA.Active = 'Y') AND (CCMS_PERSONNEL_DATA.ID <> '1016')"
	rsAllCSREmail.Open strSQL, dbconnstr() 
	while not rsAllCSREmail.EOF
		if len(strCSREmail)=0 then
			strCSREmail=rsAllCSREmail("CSREmail")
		else
			strCSREmail=strCSREmail & "," & rsAllCSREmail("CSREmail")
		end if
	rsAllCSREmail.movenext
	wend
	GetALLCSREmails=strCSREmail
end function

'*************************************
'Gets list of PSG Specialists by Product Category
'rev1 - Kate Kaplan 6/25/2007
'rev2 - Kate Kaplan 11/5/2008 (added PCI/PCI-X and Accessories logic)
'Arguments - QuoteID,ContactID
'Returns String
'*************************************
function GetPSGSpecialistsName( QuoteID, ContactID)

if len(QuoteID)>0 and len(ContactID)>0 then
	set rsGetProtocolGroups=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT DISTINCT c.CUSTOMERID, b.SUBCAT_ID " &_
            " FROM PRODUCT_SERIES_CATEGORY AS a INNER JOIN PRODUCT_SERIES AS b " &_
            " ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " &_
            " SHOPPERQUOTE AS c INNER JOIN PRODUCT AS d ON c.PRODUCTID = d.PRODUCTID ON " &_
            " a.PRODUCT_ID = d.PRODUCTID " &_
            " WHERE   (a.CATEGORY_ID = 19) and c.QUOTEID = " & QuoteID
	'Response.Write strSQL &"<br>"
	rsGetProtocolGroups.open strSQL, dbconnstr()
	if not rsGetProtocolGroups.EOF then
		while not rsGetProtocolGroups.EOF
			select case rsGetProtocolGroups("SUBCAT_ID") 
			case 1
			'Peripheral Solutions
				strGetPSGSpecialistName=GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,1))
			case 2
			'PCI Express Solutions
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGSpecialistName=GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,2))
				else
					strGetPSGSpecialistName=strGetPSGSpecialistName & ", " & GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,2))
				end if
			case 3
			'Storage Protocol Solutions
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGSpecialistName=GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,3))
				else
					strGetPSGSpecialistName=strGetPSGSpecialistName & ", " & GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,3))
				end if
			case 6
			'Accessories
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGSpecialistName=GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,6))
				else
					strGetPSGSpecialistName=strGetPSGSpecialistName & ", " & GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,6))
				end if				
			case 7
			'PCI/PCI-X Solutions
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGSpecialistName=GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,7))
				else
					strGetPSGSpecialistName=strGetPSGSpecialists & ", " & GetCCMS_SPName(GetPSGSPecialistsNumber(ContactID,7))
				end if							
			end select
			rsGetProtocolGroups.MoveNext
		wend
		GetPSGSpecialistsName=strGetPSGSpecialistName
	end if
end if
end function

'*************************************
'Gets Protocal Specialist name  
'rev1 - Kate Kaplan 6/25/2007
'Arguments - ID
'Returns String
'*************************************
function GetCCMS_SPName(ID)
	if len(ID)>0 then
		set rsSPName=server.CreateObject("ADODB.Recordset")
		strSQL="Select FirstName+ ' ' + LastName as SPName from CCMS_PERSONNEL_DATA where RecType='SP' and ID=" & ID
		'Response.write strSQL
		on error resume next
		rsSPName.Open strSQL, dbconnstr()
		if not rsSPName.EOF then
			strSPName=trim(rsSPName("SPName"))
		end if
		GetCCMS_SPName=strSPName
		rsSPName.close
		set rsSPName=nothing
	end if
end function

'*************************************
'Gets list of PSG CSR(s) by Product Category
'rev1 - Kate Kaplan 8/27/2007
'rev2 - Kate Kaplan 11/5/2008 (added PCI/PCI-X and Accessories logic)
'Arguments - QuoteID,ContactID
'Returns String
'*************************************
function GetPSGCSRName( QuoteID, ContactID)

if len(QuoteID)>0 and len(ContactID)>0 then
	set rsGetProtocolGroups=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT DISTINCT c.CUSTOMERID, b.SUBCAT_ID " &_
            " FROM PRODUCT_SERIES_CATEGORY AS a INNER JOIN PRODUCT_SERIES AS b " &_
            " ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " &_
            " SHOPPERQUOTE AS c INNER JOIN PRODUCT AS d ON c.PRODUCTID = d.PRODUCTID ON " &_
            " a.PRODUCT_ID = d.PRODUCTID " &_
            " WHERE   (a.CATEGORY_ID = 19) and c.QUOTEID = " & QuoteID
	'Response.Write strSQL &"<br>"
	rsGetProtocolGroups.open strSQL, dbconnstr()
	if not rsGetProtocolGroups.EOF then
		while not rsGetProtocolGroups.EOF
			select case rsGetProtocolGroups("SUBCAT_ID") 
			case 1
			'Peripheral Solutions
				strGetPSGCSRName=GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,1))
			case 2
			'PCI Express Solutions
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGCSRName=GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,2))
				else
					strGetPSGCSRName=strGetPSGCSRName & ", " & GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,2))
				end if
			case 3
			'Storage Protocol Solutions
				if len(strGetPSGCSRName)=0 then
					strGetPSGCSRName=GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,3))
				else
					strGetPSGCSRName=strGetPSGCSRName & ", " & GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,3))
				end if
			case 6
			'Accessories
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGCSRName=GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,6))
				else
					strGetPSGCSRName=strGetPSGCSRName & ", " & GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,6))
				end if				
			case 7
			'PCI/PCI-X Solutions
				if len(strGetPSGSpecialistName)=0 then
					strGetPSGCSRName=GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,7))
				else
					strGetPSGCSRName=strGetPSGCSRName & ", " & GetCCMSPSGCSRName(GetPSGSPecialistsNumber(ContactID,7))
				end if		
			end select
			rsGetProtocolGroups.MoveNext
		wend
		GetPSGCSRName=strGetPSGCSRName
	end if
end if
end function

'*************************************
'Gets PSG CSR name on PSG Sales Specialist # 
'rev1 - Kate Kaplan 9/4/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetCCMSPSGCSRName(SP_SECode)
	if len(SP_SECode)>0 then
		set rsPSGCSR=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT  FirstName+ ' ' + LastName as CSR FROM CCMS_PERSONNEL_DATA WHERE RecType='CS' and ID=(select CSRCode from CCMS_SECSR Where SeCode=" & SECode & ")"
		'Response.Write strSQL
		on error resume next
		rsPSGCSR.Open strSQL, dbconnstr()
		if not rsPSGCSR.EOF then
			strPSGCSRName=trim(rsPSGCSR("CSR"))
		end if
		GetCCMSPSGCSRName=strPSGCSRName
		rsPSGCSR.Close
		set rsPSGCSR=nothing
	end if
end function

'*************************************
'Gets SE email CC on SENumber 
'rev1 - Kate Kaplan 9/5/2007
'Arguments - SENumber
'Returns String
'*************************************
function GetSEEmailCC(SECode)
	if len(SECode)>0 then
		set rsSEEmailCC=server.CreateObject("ADODB.Recordset")
		strSQL="Select EMailCC as SEEmailCC from CCMS_PERSONNEL_DATA where RecType='SE' and ID='" & SECode & "'"
		'Response.Write strSQL
		on error resume next
		rsSEEmailCC.Open strSQL, dbconnstr()
		if not rsSEEmailCC.EOF then
			strSEEmail=trim(rsSEEmailCC("SEEmailCC"))
		end if
		GetSEEmailCC=strSEEmail
		rsSEEmailCC.Close
		set rsSEEmailCC=nothing
	end if
end function

'*************************************
'Gets list of PSG Specialist Se Code
'rev1 - Kate Kaplan 9/11/2007
'rev2 - Kate Kaplan 11/5/2008 (added PCI/PCI-X and Accessories logic)
'Arguments - ContactID
'Returns String
'*************************************
function GetPSGSpecialistSECode( QuoteID)

if len(QuoteID)>0 then
	set rsGetPSGSpecialistSECode=server.CreateObject("ADODB.Recordset")
    strSQL=	" SELECT DISTINCT c.CUSTOMERID,  b.SUBCAT_ID " &_
            " FROM PRODUCT_SERIES_CATEGORY AS a INNER JOIN PRODUCT_SERIES AS b " &_
            " ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID INNER JOIN " &_
            " SHOPPERQUOTE AS c INNER JOIN PRODUCT AS d ON c.PRODUCTID = d.PRODUCTID ON " &_
            " a.PRODUCT_ID = d.PRODUCTID " &_
            " WHERE   (a.CATEGORY_ID = 19) and c.QUOTEID = " & QuoteID
	'Response.Write strSQL &"<br>"
	rsGetPSGSpecialistSECode.open strSQL, dbconnstr()
	if not rsGetPSGSpecialistSECode.EOF then
		'while not rsGetQuoteProducts.EOF
			select case rsGetPSGSpecialistSECode("SUBCAT_ID") 
			case 1
			'Peripheral Solutions
				strGetPSGSpecialistSECode=GetPSGSPecialistsNumber(rsGetPSGSpecialistSECode("CUSTOMERID"),1)
			case 2
			'PCI Express Solutions
				strGetPSGSpecialistSECode=GetPSGSPecialistsNumber(rsGetPSGSpecialistSECode("CUSTOMERID"),2)
			case 3
			'Storage Protocol Solutions
				strGetPSGSpecialistSECode=GetPSGSPecialistsNumber(rsGetPSGSpecialistSECode("CUSTOMERID"),3)
			case 6
			'Accessories
				strGetPSGSpecialistSECode=GetPSGSPecialistsNumber(rsGetPSGSpecialistSECode("CUSTOMERID"),6)
			case 7
			'PCI/PCI-X Solutions
				strGetPSGSpecialistSECode=GetPSGSPecialistsNumber(rsGetPSGSpecialistSECode("CUSTOMERID"),7)
			end select
		'	rsGetQuoteProducts.MoveNext
		'wend
		GetPSGSpecialistSECode=strGetPSGSpecialistSECode
	end if
end if
end function

'************************************************
'Generates QuoteID for WJ and WA products and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 11/5/2008
'rev2 - Kate Kaplan (3/30/2009 - added WJ 300A)
'Returns String
'************************************************
function GenerateWJQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ",'" & GetSENumber(Session("ContactID")) & "')"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if
	rsQuote.LockType = 3
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID   FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  MASTERPRODUCTID in (select productid from PRODUCT where model_series in (33,44,50))"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID )" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.LockType = 3
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  MASTERPRODUCTID in (select productid from PRODUCT where model_series in (33,44,50))"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
	GenerateWJQuote=strQuoteID
end function

'*************************************
'Checks whether the WJ or WA products
'are in the basket (to separate WJ and WA quote in US)
'rev1 - Kate Kaplan 11/5/2008
'rev2 - Kate Kaplan (3/30/2009 - added WJ 300A)
'Returns boolean
'*************************************
function CheckWJInBasket()
	CheckWJInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where model_series in (33,44,50)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckWS = server.CreateObject("ADODB.Recordset")
	rsCheckWS.Open strSQL, dbconnstr()
	if not rsCheckWS.eof then
		CheckWJInBasket = true
	else
		CheckWJInBasket = false
	end if 
end function

'*************************************
'Checks whether quote is for WaveJet
'rev1 - Kate Kaplan 12/16/2008
''rev2 - Kate Kaplan (3/30/2009 - added WJ 300A)
'QuoteID
'Returns true if any of the products from a quote
'*************************************

function ShowWaveJetNote_YN(QuoteID)
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE WHERE QUOTEID = '" & QuoteID & "'"
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	ShowWaveJetNote_YN = false
	
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
		do while not rsMasterID.EOF 
			if len(trim(rsMasterID("MasterProductID"))) > 0 then
				strSQL = "SELECT MODEL_SERIES FROM PRODUCT WHERE PRODUCTID = " & rsMasterID("MasterProductID")
				set rsModelSeries = server.CreateObject ("ADODB.Recordset")
				rsModelSeries.Open strSQL, dbconnstr()
				if not rsModelSeries.EOF then
					if len(rsModelSeries("Model_Series"))>0 then
						if 	cint(rsModelSeries("Model_Series")) =33 then 'or cint(rsModelSeries("Model_Series")) =50 then
							ShowWaveJetNote_YN = true
							exit function
						end if 
					end if
				end if 
			end if 	
			rsMasterID.MoveNext 
		loop
	else
		ShowWaveJetNote_YN = false
		exit function
	end if 
	
end function

'*************************************
'Checks whether quote is for WaveJet
'rev1 - Kate Kaplan 12/16/2008
'QuoteID
'Returns true if any of the products from a quote
'*************************************

function ShowWaveAceNote_YN(QuoteID)
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE WHERE QUOTEID = '" & QuoteID & "'"
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	ShowWaveAceNote_YN = false
	
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
		do while not rsMasterID.EOF 
			if len(trim(rsMasterID("MasterProductID"))) > 0 then
				strSQL = "SELECT MODEL_SERIES FROM PRODUCT WHERE PRODUCTID = " & rsMasterID("MasterProductID")
				set rsModelSeries = server.CreateObject ("ADODB.Recordset")
				rsModelSeries.Open strSQL, dbconnstr()
				if not rsModelSeries.EOF then
					if len(rsModelSeries("Model_Series"))>0 then
						if 	cint(rsModelSeries("Model_Series")) =44 then
							ShowWaveAceNote_YN = true
							exit function
						end if 
					end if
				end if 
			end if 	
			rsMasterID.MoveNext 
		loop
	else
		ShowWaveAceNote_YN = false
		exit function
	end if 
	
end function

'*************************************
'Checks whether the Pert3 products
'are in the basket (to separate Pert3 quotes in US)
'rev1- Kate Kaplan 1/30/2009
'Returns boolean
'*************************************
function CheckPert3InBasket()
	CheckPert3InBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where propertygroupid in(select propertygroupid from propertygroup where category_id=24)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckPert3InBasket = server.CreateObject("ADODB.Recordset")
	rsCheckPert3InBasket.Open strSQL, dbconnstr()
	if not rsCheckPert3InBasket.eof then
		CheckPert3InBasket = true
	else
		CheckPert3InBasket = false
	end if 
end function

'************************************************
'Generates QuoteID for Pert3 products and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 1/30/2009
'Returns String
'************************************************
function GeneratePert3Quote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID")  & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if

	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID   FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where PROPERTYGROUPID in (SELECT PROPERTYGROUPID From PROPERTYGROUP where CATEGORY_ID=24))"
	
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID )" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext
	'Update Quote table with SENumber
	'	set rsPSGSECode=server.CreateObject("ADODB.Recordset")
	'	strSQL=" Update QUOTE set SE_CODE='" & GetPSGSpecialistSECode(strQuoteID) & "' where QUOTEID=" & strQuoteID
	'	'Response.Write strSQL
	'	rsPSGSECode.Open strSQL, dbconnstr()
			
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where PROPERTYGROUPID in (SELECT PROPERTYGROUPID From PROPERTYGROUP where CATEGORY_ID=24))"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
	GeneratePert3Quote=strQuoteID
end function
'*************************************
'Checks whether the product is PeRT3
'rev1 - Kate Kaplan 1/30/2009 
'QuoteID
'Returns true if any of the products from a quote
'is Pert3 products
'*************************************

function PeRT3QUOTE_YN(QuoteID)
	PeRT3QUOTE_YN=false
	if len(quoteid)>0 then
		set rsPeRT3QUOTE_YN=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT DISTINCT PROPERTYGROUP.CATEGORY_ID FROM SHOPPERQUOTE INNER JOIN  PROPERTYGROUP ON SHOPPERQUOTE.PROPERTYGROUPID = PROPERTYGROUP.PROPERTYGROUPID WHERE  CATEGORY_ID=24 and SHOPPERQUOTE.QUOTEID = " & quoteid
		'Response.Write strSQL
		'Response.end
		rsPeRT3QUOTE_YN.Open strSQL, dbconnstr()
		if not rsPeRT3QUOTE_YN.eof then
			PeRT3QUOTE_YN=true
		end if
	end if
	
end function


'*************************************
'Gets Indirect Manager Number on MRCode
'rev1 - Kate Kaplan 2/11/2009
'Arguments - ScopeMRCode
'Returns String
'*************************************
function GetIndirectManagerCode(MRCode)
if len(MRCode)>0  then
		set rsIM=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT  IndirectManager  FROM CCMS_PERSONNEL_DATA WHERE RecType = 'mr'  AND ID ='" & MRCode & "'"
		rsIM.Open strSQL, dbconnstr()
		if not rsIM.EOF then
		    strIMCode=rsIM("IndirectManager")
		end if
		GetIndirectManagerCode=strIMCode
end if
end function

'*************************************
'Gets IM name on IM Code
'rev1 - Kate Kaplan 2/11/2009
'Arguments - SECode
'Returns String
'*************************************
function GetIMName(SECode)
	if len(SECode)>0 then
		set rsSEName=server.CreateObject("ADODB.Recordset")
		strSQL="Select FirstName+ ' ' + LastName as SEName from CCMS_PERSONNEL_DATA where RecType='IM' and ID='" & SECode & "'"
		on error resume next
		rsSEName.Open strSQL, dbconnstr()
		if not rsSEName.EOF then
			strSEName=trim(rsSEName("SEName"))
		end if
		GetIMName=strSEName
		rsSEName.close
		set rsSEName=nothing
	end if
end function


'*************************************
'Sends an quotes in TEXT format
'To US Customer and quote copy to SEs for WR and abobe and
'IM for products sold thru distributors
'rev1 - Kate Kaplan 6/23/2009
'rev2 - Kate Kaplan 11/22/2010 (added distributor products, combined with LogicStudio)
'Arguments - strQuoteID (Integer), ServerName (String),emaillocaleid
'*************************************
sub SendQuoteToCustomer(strQuoteID,strContactID,emaillocaleid,ServerName)
	        strCRet = chr(13) & chr(10)

        '********Create E-Mail Body							
	       ' strEmailBodyCustomer=  EMailBodyNew(strQuoteID)  & strCret
		   ' strInsertReminder=strEmailBodyCustomer
    		'strInsertReminder = strInsertReminder & GetSECSRQuoteFooter(strQuoteID,emaillocaleid)
			'set rsInsertMAIL=server.CreateObject("ADODB.Recordset")
			'strSQL="INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION,PSG_YN) values(" & strQuoteID & ",1,'" & Now() & "','y','" & strToCSR & "','webquote@teledynelecroy.com','" & replace(strInsertReminder,"'","''") & "','" & replace(GetQuoteInfo(strQuoteID,emaillocaleid),"'","''") & "','','','n')"
			'on error resume next
			'rsInsertMAIL.Open strSQL, dbconnstr()
			'on error resume next
			'call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote " & strQuoteID,strSQL)
		set rsFirstName=server.CreateObject("ADODB.Recordset")
        strSQL="Select FIRST_NAME,EMAIL, LAST_NAME, COMPANY from CONTACT where CONTACT_ID=" & strContactID
        rsFirstName.Open strSQL, dbconnstr()
        if not rsFirstName.EOF then
            strFirstName=rsFirstName("FIRST_NAME")
            strCustomerEmail=trim(rsFirstName("EMAIL"))
            strLastName=rsFirstName("LAST_NAME")
            strCompany=rsFirstName("COMPANY")
            
	    '****** Send Quote to the customer
   	         strEMailBody =QuoteEmailBodyCustomer(strQuoteID,strContactID,strFirstName,emaillocaleid,ServerName,"CUSTOMER")

            if CheckDistPSGQuote(strQuoteID) then
             'call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
              call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","webquote@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
            
            'Insert into Quote table flag Response_flag=true and Response time
		        set rsInsertRespFlag=server.CreateObject("ADODB.Recordset")
		        strSQL="Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=" & strQuoteID
		        rsInsertRespFlag.Open strSQL, dbconnstr()

			    'Insert Quote Info Into EMAIL_CSR
			    set rsInsertMAIL=server.CreateObject("ADODB.Recordset")
				    strSQL="INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION,PSG_YN,APPROVED_BY) values(" & strQuoteID & ",1,'" & Now() & "','y','" & strCustomerEmail & "','webquote@teledynelecroy.com','" & replace(strEMailBody,"'","''") & "','" & replace(GetQuoteInfo(strQuoteID,emaillocaleid),"'","''") & "','','','y','SYSTEM')"
			    on error resume next
			    rsInsertMAIL.Open strSQL, dbconnstr()
			    call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote " & strQuoteID,strSQL)


			    'Insert Quote Info Into EMAIL_CUSTOMER 
			        set rsInsertEMAIL_CUSTOMER=server.CreateObject("ADODB.Recordset")
				    strSQL="INSERT into EMAIL_CUSTOMER (QUOTEID, [TO], [FROM], EMAIL_BODY) values(" & strQuoteID & ",'" & strCustomerEmail & "','webquote@lecroy.cpm',N'" & replace(strEMailBody,"'","''") & "')"
			        on error resume next
			        rsInsertEMAIL_CUSTOMER.Open strSQL, dbconnstr()
			       'on error resume next
			       call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote - EMAIL_CUSTOMER - " & strQuoteID,strSQL)
          elseif CheckPSGQuote(strQuoteID) then
            'call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
            call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","webquote@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
          else

            if NoDistributorProductQUOTE(strQuoteID) and len(GetSEEmail(GetSENumber(strContactID)))>0 then
		        'call SendQuoteEmail(strCustomerEmail, GetSEEmail(GetSENumber(strContactID)),"","kate.kaplan@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid),strEMailBody)
		        call SendQuoteEmail(strCustomerEmail, GetSEEmail(GetSENumber(strContactID)),"","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
	        elseif  DistributorProductQUOTE(strQuoteID) and not NoDistributorProductQUOTE(strQuoteID) then
		        'call SendQuoteEmail(strCustomerEmail, "Hilary.Lustig@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid),strEMailBody)
		        call SendQuoteEmail(strCustomerEmail, "Hilary.Lustig@teledynelecroy.com","webquote@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
     
'Insert into Quote table flag Response_flag=true and Response time
		        set rsInsertRespFlag=server.CreateObject("ADODB.Recordset")
		        strSQL="Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=" & strQuoteID
		        rsInsertRespFlag.Open strSQL, dbconnstr()

                 'Insert Quote Info Into EMAIL_CSR
			    set rsInsertMAIL=server.CreateObject("ADODB.Recordset")
				strSQL="INSERT into EMAIL_CSR  (QUOTEID, SEND_COUNTER, SEND_EMAIL_DATE, RESPONSE_FLAG, [TO], [FROM], EMAIL_BODY, CUSTOMER_INFO, CSR_NAME, DESCRIPTION,PSG_YN,APPROVED_BY) values(" & strQuoteID & ",1,'" & Now() & "','y','" & strCustomerEmail & "','webquote@teledynelecroy.com','" & replace(strEMailBody,"'","''") & "','" & replace(GetQuoteInfo(strQuoteID,emaillocaleid),"'","''") & "','','','n','SYSTEM')"
				on error resume next
			    rsInsertMAIL.Open strSQL, dbconnstr()
			    call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote " & strQuoteID,strSQL)


			    'Insert Quote Info Into EMAIL_CUSTOMER 
			        set rsInsertEMAIL_CUSTOMER=server.CreateObject("ADODB.Recordset")
				    strSQL="INSERT into EMAIL_CUSTOMER (QUOTEID, [TO], [FROM], EMAIL_BODY) values(" & strQuoteID & ",'" & strCustomerEmail & "','webquote@lecroy.cpm',N'" & replace(strEMailBody,"'","''") & "')"
			        on error resume next
			        rsInsertEMAIL_CUSTOMER.Open strSQL, dbconnstr()
			       'on error resume next
			       call SendQuoteEmail("kate.kaplan@teledynelecroy.com","kate.kaplan@teledynelecroy.com","","","Insert Quote - EMAIL_CUSTOMER - " & strQuoteID,strSQL)	
			   else
		        'call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
		        call SendQuoteEmail(strCustomerEmail, "webquote@teledynelecroy.com","webquote@teledynelecroy.com","kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com",LoadI18N("SLBCA0104",emaillocaleid), strEMailBody)
	        end if
	    end if
	    
	    '********************************************************
	       'Send email to SE, CustomerCare or Marketing Coordinator
	    '********************************************************
	        strEMailBodySE=QuoteEmailBodyCustomer(strQuoteID,strContactID,strFirstName,emaillocaleid,ServerName,"SE")

   
	            strSubject="WEB QUOTE APPROVED AND SENT TO CUSTOMER  - " &  strCompany & ", " & strFirstName & " " & strLastName
  	            strBCC="kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com"
                'strBCC="kate.kaplan@teledynelecroy.com"
                strFrom="webquote@teledynelecroy.com"
                ServerName="teledynelecroy.com"
	            if  DistributorProductQUOTE(strQuoteID) and not NoDistributorProductQUOTE(strQuoteID) then
	                strSelectDistributor = "The quote was approved and sent to the customer" & strCret
			        strSelectDistributor = strSelectDistributor & "-------------------------------------------------------------------------" & strCret & strCret
			        strSelectDistributor = strSelectDistributor & "To forward the quote to the distributor, please click on the following link:" & strCret & strCret
			        strSelectDistributor = strSelectDistributor & "http://" & ServerName & "/shopper/sendtodistributor.asp?cust=" & strContactID & "&qid=" & strQuoteID & strCret & strCret 
			        strSelectDistributor = strSelectDistributor & "-------------------------------------------------------------------------" & strCret & strCret
		            strTo="customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com,Dan.Payne@teledynelecroy.com,webquote@teledynelecroy.com"
   		            'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strSelectDistributor  & strEMailBodySE & strCret & strCret )
  		           call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject ,strSelectDistributor & strInfoCC & strEMailBodySE & strCret & strCret )

		        end if
		        
		          if CheckDistPSGQuote(strQuoteID) then
                    strTo="Adam.Frisius@teledynelecroy.com,GMooney@stanleyworks.com, SRiel@stanleyworks.com, PLynch@stanleyworks.com,Teresa.Zachary@teledynelecroy.com"
                    strCC="webquote@teledynelecroy.com"
                    if len(SendQuoteToSE(strQuoteID,strContactID))>0 then
                        strTo=strTo & "," & SendQuoteToSE(strQuoteID,strContactID)
                    end if
                   ' call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strInfoCC & strEMailBodySE )
                    call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject,strInfoCC & strEMailBodySE )
		        
                elseif CheckPSGQuote(strQuoteID) then
                    if len(SendQuoteToSE(strQuoteID,strContactID))>0 then
                        strTo=SendQuoteToSE(strQuoteID,strContactID) & ",Teresa.Zachary@teledynelecroy.com"
                    else
		                strTo="Teresa.Zachary@teledynelecroy.com"
                    end if
                    strCC="webquote@teledynelecroy.com"
                   'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strInfoCC & strEMailBodySE )
                    call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject,strInfoCC & strEMailBodySE )
              ' elseif CheckLogicAnalyzerQuote(strQuoteID) then
               '     strTo="Hilary.Lustig@teledynelecroy.com"
                '    strSelectDistributor = strSelectDistributor & "To forward the quote to the distributor, please click on the following link:" & strCret & strCret
			     '   strSelectDistributor = strSelectDistributor & "http://" & ServerName & "/shopper/SendToDistributor.asp?s=y&cust=" & strContactID & "&qid=" & strQuoteID & strCret & strCret 
			      '  strSelectDistributor = strSelectDistributor & "-------------------------------------------------------------------------" & strCret & strCret
                   ' 'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strInfoCC & strEMailBodySE )
                    'call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject, strSelectDistributor & strInfoCC & strEMailBodySE )
               
                else
		            if NoDistributorProductQUOTE(strQuoteID)  then
		                if len(GetSEEmail(GetSENumber(strContactID)))>0 then
		                 'SE can request CSR to modify a quote
	                       ' strModify = "The quote was already sent to the customer" & strCret
			                'strModify = strModify & "-------------------------------------------------------------------------" & strCret & strCret
			                'strModify = strModify & "To request this quote to be modified by CSRs and sent to the customer from BAAN, please click on the following link:" & strCret & strCret
			                'strModify = strModify & "http://" & ServerName & "/shopper/ModifyQuote.asp?cust=" & strContactID & "&qid=" & strQuoteID & strCret & strCret 
			                'strModify = strModify & "-------------------------------------------------------------------------" & strCret & strCret
		                    if  DistributorProductQUOTE(strQuoteID) then
		                        strSelectDistributor = strSelectDistributor & "To forward the quote to the distributor, please click on the following link:" & strCret & strCret
			                    strSelectDistributor = strSelectDistributor & "http://" & ServerName & "/shopper/sendtodistributor.asp?s=y&cust=" & strContactID & "&qid=" & strQuoteID & strCret & strCret 
			                    strSelectDistributor = strSelectDistributor & "-------------------------------------------------------------------------" & strCret & strCret
                            end if
		                    strTo="customercare@teledynelecroy.com" & "," & SendQuoteToSE(strQuoteID,strContactID)
		                    'Serial Data Bundles
	                        if APGSerialDataBundleQUOTE(strQuoteID) then
	                            strTo=strTo & "," & "Roy.Chestnut@teledynelecroy.com"
	                        end if
	                        if PSGUSBSerialDataBundleQUOTE(strQuoteID) then
	                            strTo=strTo & "," & GetCCMS_Email("SP",GetPSGSPecialistsNumber(strContactID,1))
	                        end if
                            strCC="webquote@teledynelecroy.com"
   		                    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strModify & strSelectDistributor & strInfoCC & strEMailBodySE & strCret & strCret)
   		                    call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject ,strModify & strSelectDistributor & strInfoCC & strEMailBodySE & strCret & strCret)
		                else
		                    strTo="customercare@teledynelecroy.com"
                            strCC="webquote@teledynelecroy.com"
   		                    'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", strFrom,"","",strSubject & " - " & strTo & " - " & strCC & " - " & strBCC ,strInfoCC & strEMailBodySE )
   		                    call SendQuoteEmail(strTo, strFrom,strCC,strBCC,strSubject,strInfoCC & strEMailBodySE )
		                end if	
		            end if
		        end if
		 end if
end sub

'*************************************
'Checks whether there are distributors products in the quote
'rev1 - Kate Kaplan 7/6/2009
'rev2 - Kate Kaplan 4/23/2010 (added ArbStudio)
'QuoteID
'Returns true if the quote
'has WJ, WS or WA products
'*************************************
function DistributorProductQUOTE(QuoteID)
	DistributorProductQUOTE = false
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE where (MASTERPRODUCTID <= ' ' OR (masterproductid > ' ' AND PRODUCT_SERIES_ID IN (13, 33, 34, 43, 44, 49, 50,217,339,399,402,419) ) or group_id in (165,166,210)) AND QUOTEID = " & QuoteID 
	'response.write strSQL
	'response.end
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
        DistributorProductQUOTE=true
	else
		DistributorProductQUOTE = false
	end if 
end function

'*************************************
'Checks whether there are distributors products in the quote
'rev1 - Kate Kaplan 7/6/2009
'QuoteID
'Returns true if the quote
'has other than WJ, WS or WA products
'*************************************
function NoDistributorProductQUOTE(QuoteID)
	NoDistributorProductQUOTE = false
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE where (MASTERPRODUCTID <= ' ' OR (masterproductid > ' ' AND PRODUCT_SERIES_ID NOT IN  (13, 33, 34, 43, 44, 49, 50,217,339,399,402,419) ) and group_id not in (165,166,210)) AND QUOTEID = " & QuoteID 
	'response.write strSQL
	'response.end
	set rsMasterID = server.CreateObject("ADODB.Recordset")
	rsMasterID.Open strSQL, dbconnstr()
	if not rsMasterID.eof then
        NoDistributorProductQUOTE=true
	else
		NoDistributorProductQUOTE = false
	end if 
end function

'*******************************************
'Gets WS Distributor Main Contact Lead Email on OfficeID
'rev1 - Kate Kaplan 7/13/09
'Arguments - OfficeID
'Returns String
'*******************************************
function GetDistEmailOnOfficeID(OfficeID)
    GetDistEmailOnOfficeID=""
	if len(OfficeID)>0 then
		set rsDistEmail=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT EMAIL_LEADS FROM CONTACT_OFFICE  WHERE OFFICE_ID =" & OfficeID
		on error resume next
		rsDistEmail.Open strSQL, dbconnstr()
		if not rsDistEmail.EOF then
			GetDistEmailOnOfficeID=trim(rsDistEmail("EMAIL_LEADS"))
		end if
		rsDistEmail.Close
		set rsDistEmail=nothing
	end if
end function

'*******************************************
'Gets Quote_time on QUOTEID
'rev1 - Kate Kaplan 7/15/09
'Arguments - OfficeID
'Returns String
'*******************************************
function GetQuoteTimeOnQuoteID(QuoteID)
	if len(QuoteID)>0 then
		set rsQuoteTime=server.CreateObject("ADODB.Recordset")
		strSQL="SELECT QUOTE_TIME FROM QUOTE  WHERE QUOTEID =" & QuoteID
		'response.write strSQL
		on error resume next
		rsQuoteTime.Open strSQL, dbconnstr()
		if not rsQuoteTime.EOF then
			GetQuoteTimeOnQuoteID=rsQuoteTime("QUOTE_TIME")
		end if
		rsQuoteTime.Close
		set rsQuoteTime=nothing
	end if
end function

'*************************************
'Checks if quote contains 
'scope serial data bundles
'rev1 - Kate Kaplan 8/12/2009
'QuoteID
'Returns true if the quote
'has scope
'*************************************
function ScopeSerialDataBundleQUOTE(QuoteID)
	ScopeSerialDataBundleQUOTE = false
	set rsScopeSDBundle = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE where Productid IN (2073,2075,2076,2078) and QUOTEID = " & QuoteID 
	set rsScopeSDBundle = server.CreateObject("ADODB.Recordset")
	rsScopeSDBundle.Open strSQL, dbconnstr()
	if not rsScopeSDBundle.eof then
        ScopeSerialDataBundleQUOTE=true
	else
		ScopeSerialDataBundleQUOTE = false
	end if 
end function

'*************************************
'Checks if quote contains 
'APG serial data bundles
'rev1 - Kate Kaplan 8/12/2009
'QuoteID
'Returns true if the quote
'has scope
'*************************************
function APGSerialDataBundleQUOTE(QuoteID)
	APGSerialDataBundleQUOTE = false
	set rsAPGSDBundle = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE where Productid IN (2074,2075,2078) and QUOTEID = " & QuoteID 
	set rsAPGSDBundle = server.CreateObject("ADODB.Recordset")
	rsAPGSDBundle.Open strSQL, dbconnstr()
	if not rsAPGSDBundle.eof then
        APGSerialDataBundleQUOTE=true
	else
		APGSerialDataBundleQUOTE = false
	end if 
end function

'*************************************
'Checks if quote contains 
'PSG USB serial data bundles
'rev1 - Kate Kaplan 8/12/2009
'QuoteID
'Returns true if the quote
'has scope
'*************************************
function PSGUSBSerialDataBundleQUOTE(QuoteID)
	PSGUSBSerialDataBundleQUOTE = false
	set rsPSGUSBSDBundle = server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM SHOPPERQUOTE where Productid IN (2077,2078) and QUOTEID = " & QuoteID 
	set rsPSGUSBSDBundle = server.CreateObject("ADODB.Recordset")
	rsPSGUSBSDBundle.Open strSQL, dbconnstr()
	if not rsPSGUSBSDBundle.eof then
        PSGUSBSerialDataBundleQUOTE=true
	else
		PSGUSBSerialDataBundleQUOTE = false
	end if 
end function

'*************************************
'Checks whether The Distributor products
'are in the basket (to separate quotes in US)
'rev1 - Kate Kaplan 3/4/2010
'rev2 - Kate Kaplan 4/23/2010 (added arbstudio)
'Returns boolean
'*************************************
function CheckDistProductInBasket()
	CheckDistProductInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID in (13, 33, 34, 43, 44, 49, 50,217,339,399,402,419) or group_id in (165,166,210)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckDistProduct = server.CreateObject("ADODB.Recordset")
	rsCheckDistProduct.Open strSQL, dbconnstr()
	if not rsCheckDistProduct.eof then
		CheckDistProductInBasket = true
	else
		CheckDistProductInBasket = false
	end if 
end function
'*************************************
'Generate US Customer Quote Email Body 
'rev1 - Kate Kaplan 3/4/2010
'rev2 - Kate Kaplan 4/23/2010 (added LogicStudio)
'rev3 - Kate Kaplan 5/7/2010 (added PSG distributor products)
'rev4 - Kate Kaplan 2/18/2011 (re-arranged text)
'Arguments - strQuoteID (Integer), ServerName (String),emaillocaleid
'*************************************
function QuoteEmailBodyCustomer(strQuoteID,strContactID,strFirstName,emaillocaleid,ServerName,BodyType)
	        strCRet = chr(13) & chr(10)
	        strQuote = chr(34)

	        strQuoteTime=GetQuoteTimeOnQuoteID(strQuoteID)
	        

            '***************************************************
	           ' Customer's email
        	'***************************************************
	        strFooter=strFooter & "-------------------------------------------------------------------------" & strCret & strCret
	
	        if  CheckScopesQuote(strQuoteID) then
                    if NoDistributorProductQUOTE(strQuoteID)  then
 	                    strPartFooter= GetCSRName(GetSENumber(strContactID)) & ", " & LoadI18N("SLBCA0062",emaillocaleid) & strCRet & strCRet 
	                    strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetSEName(GetSENumber(strContactID)) & ", " & LoadI18N("SLBCA0064",emaillocaleid) & strCRet & strCRet
	                end if
	        else 
		        if len(GetPSGCSRName(strQuoteID,strContactID))>0 then
			        strPartFooter= GetPSGCSRName(strQuoteID,strContactID) & ", SALES OPERATIONS REPRESENTATIVE(S)" & strCRet & strCRet 
		        else
			        strPartFooter= "Teresa Zachary, SALES OPERATIONS REPRESENTATIVE" & strCRet & strCRet 
		        end if
		        '******** Protocol Specialist *********
		       ' set rsSP=server.CreateObject("ADODB.Recordset")
		       ' strSQL=	" SELECT DISTINCT  PRODUCT_SERIES.SUBCAT_ID FROM SHOPPERQUOTE INNER JOIN"&_
				'        " PRODUCT ON SHOPPERQUOTE.PRODUCTID = PRODUCT.PRODUCTID"&_
				 '       " INNER JOIN  PRODUCT_SERIES ON SHOPPERQUOTE.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID"&_
				  '      " INNER JOIN PRODUCT_SERIES_CATEGORY ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID= PRODUCT_SERIES.PRODUCT_SERIES_ID "&_
				   '     " WHERE SHOPPERQUOTE.QUOTEID = " & strQuoteID & " AND PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 19 "
		        'rsSP.Open strSQL, dbconnstr()
		        'if not rsSP.EOF then
			        'strSP=GetPSGSpecialistsName(strQuoteID,ContactID)
			        strPartFooter= strPartFooter & LoadI18N("SLBCA0063",emaillocaleid) & " " & GetPSGSpecialistsName(strQuoteID,strContactID) & ", PROTOCOL SALES SPECIALIST(S)"  & strCRet & strCRet
		        'end if
	        end if	        

	        if len(strPartFooter)>0 and not CheckLogicAnalyzerQuote(strQuoteID) and  not CheckDistPSGQuote(strQuoteID) then 
		        strFooter=strFooter & strPartFooter
	        end if

            'Serial Data Bundles
            if APGSerialDataBundleQUOTE(strQuoteID) or PSGUSBSerialDataBundleQUOTE(strQuoteID) then
                strFooter=strFooter & strCret &  "The Serial Data Bundle offer is only valid in countries where LeCroy operates directly." & strCret & strCret
            end if	           
   
           ' if  DistributorProductQUOTE(strQuoteID)  then
               ' strFooter=strFooter & strCret &  "Please note that WaveSurfer,WaveJet and WaveAce Series are sold through distribution only.  Please contact an authorized LeCroy distributor.  You can find a complete listing here:"& strCret 
               ' strFooter=strFooter & "http://" & ServerName & "/tm/contact/officelisting.asp?menuid=41&regionid=5&officetype=D"& strCret & strCret & strCret
               ' strFooter=strFooter & "For Technical Information, please call 1-800-5-LECROY (1-800-553-2769)." & strCret & strCret & strCret
            	'if ShowWaveJetNote_YN(strQuoteID) then
	              '  strFooter=strFooter & "Interested in using the scope for a few days? Click here:" & strCret 
	              '  strFooter=strFooter & "http://" & ServerName & "/goto/TestDrive/"& strCret & strCret
                'end if
           ' end if
            

             if NoDistributorProductQUOTE(strQuoteID)  then
                  if CheckPSGQuote(strQuoteID) then
                    strFooter=strFooter & strCret & "For Order Placement please fax to 408-727-0800 or email to protocolsales@teledynelecroy.com. For Credit Card Orders or Technical Information please call 1-800-909-7211. Teledyne LeCroy standard terms of sale are Net30 days after invoice date. As with all credit items, satisfactory credit accommodations must be arranged. Minimum order is $50.00. "  
                  else
                    strFooter=strFooter & strCret & LoadI18N("SLBCA0039",emaillocaleid) & ". "  
                  end if
                strFooter=strFooter & LoadI18N("SLBCA0041",emaillocaleid) & ". " 
                strFooter=strFooter & LoadI18N("SLBCA0042",emaillocaleid) & "." & strCRet & strCRet

            end if

	        strFooterCC=strFooter
           ' if NoDistributorProductQUOTE(strQuoteID)  then
	        '    if len(Name)=0 then
	                
		     '       strFooter=strFooter & LoadI18N("SLBCA0091",emaillocaleid) & ", " 
		      '       if CheckPSGQuote(strQuoteID) then
		       '          strFooter=strFooter & "Teresa Zachary"
		        '     else
		         '        strFooter=strFooter & GetCSRName(GetSENumber(strContactID))
		          '  end if 
		           ' strFooter=strFooter & ". " 
		           ' strFooter=strFooter & LoadI18N("SLBCA0092",emaillocaleid) & " " 
		            'strFooter=strFooter & LoadI18N("SLBCA0093",emaillocaleid) & " " 
		             'if CheckPSGQuote(strQuoteID) then
		              '   strFooter=strFooter & "Teresa Zachary"
		             'else
		              '   strFooter=strFooter & GetCSRName(GetSENumber(strContactID))
		            'end if 
		             'strFooter=strFooter & " "
		             'if CheckPSGQuote(strQuoteID) then
		             '	strFooter=strFooter & "is not available,  please contact our Customer Care Center at 1-800-909-7211, and another CSR will be able to help you." & strCRet& strCRet
		             'else
		              '    strFooter=strFooter & LoadI18N("SLBCA0096",emaillocaleid) & strCRet& strCRet
		            ' end if
	           ' end if
           ' end if

	        strFooter=strFooter & LoadI18N("SLBCA0114",emaillocaleid) & " (" & LoadI18N("SLBCA0115",emaillocaleid) & ")" & strCRet & strCRet
	
	        strFooter=strFooter & CustomerData(strContactID)

        'Create Customer info 
	        strInfo=LoadI18N("SLBCA0098",emaillocaleid)& " " & strFirstName & ","  & strCRet  & strCRet
	        strInfo=strInfo & LoadI18N("SLBCA0099",emaillocaleid) & "." & strCRet & strCRet & strCRet
            if NoDistributorProductQUOTE(strQuoteID) and not CheckDistPSGQuote(strQuoteID)  then
	 
			if CheckPromoQuote(strQuoteID) then
				strInfo=strInfo & strCRet   & GetPromoQuoteFooter (strQuoteID)
				strInfo=strInfo &  strCret & strCRet
			end if
	            if CheckPSGQuote(strQuoteID) then
                    strInfo=strInfo & "If you are eligible for Education or Government pricing, please contact Customer Support at 800-909-7211 for a discounted quotation."& strCRet& strCRet
	            else
                    strInfo=strInfo & "If you are eligible for Education or Government pricing, please contact Customer Support at 800-553-2769 or CustomerSupport@teledynelecroy.com for a discounted quotation."& strCRet& strCRet
                end if
                     
            end if

	        if  (DistributorProductQUOTE(strQuoteID) and not NoDistributorProductQUOTE(strQuoteID)) or CheckDistPSGQuote(strQuoteID) then
	            if CheckDistPSGQuote(strQuoteID) then
                    strInfo=strInfo & "This product(s) is sold through our distributor Stanley Supply Services only.  " & strCRet
	                strInfo=strInfo & "http://" & ServerName & "/Support/Contact/officelisting.aspx?regionid=5&officetype=D&capid=106&mid=538" & strCRet& strCRet
	                strInfo=strInfo & strCRet & "Please note that your webquote will not reflect any educational, government or promotional pricing that may be available." & strCRet 
	                strInfo=strInfo & "If you feel you may be eligible for these discounts, please contact an authorized LeCroy distributor.  You can find a complete listing here:" & strCRet
	            else
                    strInfo=strInfo & "This product(s) is sold through distribution only.  Your inquiry will be forwarded to one of our distributors." & strCRet
                    'strInfo=strInfo & "List pricing is provided below:" & strCRet & strCRet
	                strInfo=strInfo & strCRet & "Please note that your webquote will not reflect any educational, government or promotional pricing that may be available." & strCRet 
	                strInfo=strInfo & "If you feel you may be eligible for these discounts, please contact an authorized LeCroy distributor.  You can find a complete listing here:" & strCRet
	                strInfo=strInfo & "http://" & ServerName & "/Support/Contact/officelisting.aspx?regionid=5&officetype=D&capid=106&mid=538" & strCRet& strCRet
	            end if
		        '********Promotions Link ****
                strInfo=strInfo &  "You can see a list of our current promotions here:" & strCRet
                strInfo=strInfo & "http://" & ServerName & "/promotions/"& strCRet& strCRet
                strInfo=strInfo & LoadI18N("SLBCA0100",emaillocaleid) & ":" & strCRet
	        end if


        '********Create E-Mail Body							
	        strEmailBodyCustomer=  EMailBodyNew(strQuoteID)  & strCret
		    
		'********Terms and Conditions Link ****
	        if NoDistributorProductQUOTE(strQuoteID)  then
                strEmailBodyCustomer=strEmailBodyCustomer & "To review our Purchase Terms and Conditions for United States please follow the link below:"& strCret 
      
                    strEmailBodyCustomer=strEmailBodyCustomer & "http://" & ServerName & "/terms/"& strCret & strCret & strCret
            
              '********Promotions Link ****
                strEmailBodyCustomer=strEmailBodyCustomer &   "You can see a list of our current promotions here:" & strCRet
                strEmailBodyCustomer=strEmailBodyCustomer &  "http://" & ServerName & "/promotions/"& strCRet& strCRet
	        end if
   
              
	
	        if BodyType="SE" then
	            if not CheckDistPSGQuote(strQuoteID) then 
	                QuoteEmailBodyCustomer = GetQuoteInfo(strQuoteID,emaillocaleid)  & strEmailBodyCustomer &  strFooterCC
	            else
	                QuoteEmailBodyCustomer = GetQuoteInfo(strQuoteID,emaillocaleid)  & strEmailBodyCustomer 
	            end if
	        else
	            if not CheckDistPSGQuote(strQuoteID) then 
	                QuoteEmailBodyCustomer = strInfo & strCret & GetQuoteInfo(strQuoteID,emaillocaleid)  & strEmailBodyCustomer &  strFooter
	            else
	                QuoteEmailBodyCustomer = strInfo & strCret & GetQuoteInfo(strQuoteID,emaillocaleid)  & strEmailBodyCustomer 
	            end if 
	        end if
end function

'*************************************
'Checks if quote contains Promo Products
'rev 1 Kate Kaplan 3/11/2010
'quoteid int
'Returns true 
'*************************************
function CheckPromoQuote(quoteid)
	CheckPromoQuote=false
	if len(quoteid)>0 then
		set rsCheckPromoQuote=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT * FROM SHOPPERQUOTE INNER JOIN BAANPRICE ON SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID] WHERE   BAANPRICE.PROMO_YN = 'y'  and SHOPPERQUOTE.QUOTEID =" & quoteid
		'Response.Write strSQL
		'Response.end
		rsCheckPromoQuote.Open strSQL, dbconnstr()
		if not rsCheckPromoQuote.eof then
			CheckPromoQuote=true
		end if
	end if
end function

'*************************************
'creates footer promo products in quote
'rev 1 Kate Kaplan 3/11/2010
'quoteid int
'*************************************
function GetPromoQuoteFooter(quoteid)
	
	strCRet = chr(13) & chr(10)

	if len(quoteid)>0 then
		set rsPromoQuoteFooter=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT   BAANPRICE.PARTNUMBER, BAANPRICE.DISCOUNT_PCT, BAANPRICE.PROMO_END_DATE  FROM SHOPPERQUOTE INNER JOIN BAANPRICE ON SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID]  INNER JOIN     CONTACT ON SHOPPERQUOTE.CUSTOMERID = CONTACT.CONTACT_ID WHERE   BAANPRICE.PROMO_YN = 'y' AND CONTACT.COUNTRY = 'United States'  and SHOPPERQUOTE.QUOTEID =" & quoteid
		'Response.Write strSQL
		'Response.end
		rsPromoQuoteFooter.Open strSQL, dbconnstr()
		if not rsPromoQuoteFooter.eof then
		    while not rsPromoQuoteFooter.eof 
		            strResults="NOTE: This quote includes the following promotional prices:"& strCret 
			        strResults=strResults & strCret & rsPromoQuoteFooter("Partnumber") & " (" & rsPromoQuoteFooter("DISCOUNT_PCT") & "% OFF) until " & FormatDateTime(rsPromoQuoteFooter("PROMO_END_DATE"),2)& strCret 
				    rsPromoQuoteFooter.movenext
		    wend
		end if
	end if
	GetPromoQuoteFooter=strResults
end function

'*************************************
'gets promo quote time
'rev 1 Kate Kaplan 3/11/2010
'quoteid int
'*************************************

function GetPromoQuoteTimeOnQuoteID(quoteid)
	if len(quoteid)>0 then
		set rsPromoQuoteDate=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT MIN(BAANPRICE.PROMO_END_DATE) AS PROMO_DATE  FROM SHOPPERQUOTE INNER JOIN BAANPRICE ON SHOPPERQUOTE.PRODUCTID = BAANPRICE.[Product ID] WHERE   BAANPRICE.PROMO_YN = 'y'  and SHOPPERQUOTE.QUOTEID =" & quoteid
		'Response.Write strSQL
		'Response.end
		rsPromoQuoteDate.Open strSQL, dbconnstr()
		if not rsPromoQuoteDate.eof then
			if DateDiff("d", GetQuoteTimeOnQuoteID(quoteid),rsPromoQuoteDate("PROMO_DATE")) <30 then
			    GetPromoQuoteTimeOnQuoteID=FormatDateTime(rsPromoQuoteDate("PROMO_DATE"),2)
			else
			    GetPromoQuoteTimeOnQuoteID=FormatDateTime((GetQuoteTimeOnQuoteID(quoteid)+30),2)
			end if
		end if
	end if


end function

'*************************************
'Checks if quote contains Logic Analyzers 
'Kate Kaplan 4/23/2010
'quoteid int
'Returns true if quote contains Logic Analyzers
'*************************************
function CheckLogicAnalyzerQuote(quoteid)
	CheckLogicAnalyzerQuote=false
	if len(quoteid)>0 then
		set rsCheckLogicAnalyzerQuote=server.CreateObject("ADODB.Recordset")
		strSQL= " SELECT DISTINCT PRODUCT_GROUP.CATEGORY_ID FROM SHOPPERQUOTE INNER JOIN  PRODUCT_GROUP ON SHOPPERQUOTE.GROUP_ID = PRODUCT_GROUP.GROUP_ID WHERE  CATEGORY_ID=31 and SHOPPERQUOTE.QUOTEID = " & quoteid
		'Response.Write strSQL
		'Response.end
		rsCheckLogicAnalyzerQuote.Open strSQL, dbconnstr()
		if not rsCheckLogicAnalyzerQuote.eof then
			CheckLogicAnalyzerQuote=true
		end if
	end if
end function

'*************************************
'Checks whether the Logic Analyzers
'are in the basket (to separate Logic Analyzers quotes in US)
'rev1- Kate Kaplan 4/23/2010
'Returns boolean
'*************************************
function CheckLogicAnalyzerInBasket()
	CheckLogicAnalyzerInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where group_id in(select GROUP_ID from PRODUCT_GROUP where category_id=31)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckLogicAnalyzerInBasket = server.CreateObject("ADODB.Recordset")
	rsCheckLogicAnalyzerInBasket.Open strSQL, dbconnstr()
	if not rsCheckLogicAnalyzerInBasket.eof then
		CheckLogicAnalyzerInBasket = true
	else
		CheckLogicAnalyzerInBasket = false
	end if 
end function


'************************************************
'Generates QuoteID for Logic Analyzers and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1- Kate Kaplan 4/23/2010
'Returns String
'************************************************
function GenerateLogicAnalyzerQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID")   & ",'" & GetSENumber(Session("ContactID")) & "')"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id,SE_CODE) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID")  & ",'" & GetSENumber(Session("ContactID")) & "')"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID  FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where GROUP_ID in (SELECT GROUP_ID From PRODUCT_GROUP where CATEGORY_ID=31))"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID)" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext

			
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select productid from PRODUCT where GROUP_ID in (SELECT GROUP_ID From PRODUCT_GROUP where CATEGORY_ID=31))"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
	GenerateLogicAnalyzerQuote=strQuoteID
end function

'*************************************
'Checks whether The Distributor PSG products are in the basket (to separate quotes in US)
'rev1 - Kate Kaplan 5/6/2010
'Returns boolean
'*************************************
function CheckDistProtocolsInBasket()
	CheckDistProtocolsInBasket = false
	strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007)) and sessionid='" & Session("netSessionID") & "'"
	set rsCheckDistProtocols = server.CreateObject("ADODB.Recordset")
	rsCheckDistProtocols.Open strSQL, dbconnstr()
	if not rsCheckDistProtocols.eof then
		CheckDistProtocolsInBasket = true
	else
		CheckDistProtocolsInBasket = false
	end if 
end function


'************************************************
'Generates QuoteID for PSG Distributor products and moves them
'from SHOPPER to SHOPPERQUOTE table
'rev1 - Kate Kaplan 5/6/2010
'Returns String
'************************************************
function GenerateDistProtocolsQuote()
'Create Quote_id for current quote of this customer in Quote table
	if len(Session("CampaignID"))=0 then Session("CampaignID")=0
	set rsQuote= Server.CreateObject("ADODB.Recordset")
	strDate= NOW()
	if len(Session("Country"))>0 then
		if cstr(Session("country"))="United States" then
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID")  & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		else
			if len(Session("offerid"))>0 then
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "'," & Session("offerid") & "," & Session("CampaignID") & ")"
			else
				strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
			end if
		end if
	else
		strSQL="Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES ('" & Session("ContactID") & "','" & strDate & "',0," & Session("CampaignID") & ")"
	end if
	rsQuote.Open strSQL, dbconnstr()

'Select Quote_id which was created
	set rsQUOTEID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT QUOTEID from QUOTE where CONTACTID=" & Session("ContactID") & " and QUOTE_TIME='" & strDate & "'"
	rsQUOTEID.Open strSQL, dbconnstr()
	if not rsQUOTEID.EOF then
		strQuoteID=rsQUOTEID("QUOTEID")
	end if

'Close recordset rsQUOTEID
	rsQUOTEID.Close
	set rsQUOTEID=nothing

'Copy Cart Contents to ShopperQuote Table
'Select everything from shopper table end insert to ShopperQuote Table with QuoteID and CustomerID
	set rsFromShopper=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID  FROM shopper where SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007))"
	rsFromShopper.Open strSQL, dbconnstr()
	if not rsFromShopper.EOF then
		While not rsFromShopper.EOF 
			Set rsIns = Server.CreateObject("ADODB.Recordset")
			strSQL =	"INSERT into shopperquote (QUOTEID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID)" &_
						" values(" & strQuoteID & ",'" & rsFromShopper("SESSIONID") & "','" & rsFromShopper("PRODUCTID")& "'," & rsFromShopper("QTY")& "," & rsFromShopper("PRICE") & ",'" & rsFromShopper("DATE_ENTERED")& "'," & rsFromShopper("PROPERTYGROUPID") & ",'" & Session("ContactID") & "','" & rsFromShopper("MASTERPRODUCTID") & "'," & rsFromShopper("GROUP_ID") & "," & rsFromShopper("PRODUCT_SERIES_ID") & ")"
			rsIns.Open strSQL, dbconnstr()
			rsFromShopper.MoveNext

			
		Wend
	end if
'Close recordset rsFromShopper
	rsFromShopper.Close
	set rsFromShopper=nothing
	
'Clear Database contents for current sessionID
	Set rsDel = Server.CreateObject("ADODB.Recordset")
	strSQL = "DELETE FROM SHOPPER WHERE SESSIONID = '" & Session("netSessionID") & "' and  PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007))"
	rsDel.LockType = 3
	rsDel.Open strSQL, dbconnstr()
'Update Quote table with SENumber
		set rsPSGSECode=server.CreateObject("ADODB.Recordset")
		strSQL=" Update QUOTE set SE_CODE='" & GetPSGSpecialistSECode(strQuoteID) & "' where QUOTEID=" & strQuoteID
		'Response.Write strSQL
		rsPSGSECode.Open strSQL, dbconnstr()
		GenerateDistProtocolsQuote=strQuoteID
end function

'*************************************
'Checks if quote contains PSG Distributor
'Kate Kaplan 5/6/2010
'quoteid int
'Returns true if quote contains PSG Distributor
'*************************************
function CheckDistPSGQuote(quoteid)
	CheckDistPSGQuote=false
	if len(quoteid)>0 then
		set rsCheckDistPSGQuote=server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM SHOPPERQUOTE WHERE PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007)) and SHOPPERQUOTE.QUOTEID = " & quoteid

		'Response.Write strSQL
		'Response.end
		rsCheckDistPSGQuote.Open strSQL, dbconnstr()
		if not rsCheckDistPSGQuote.eof then
			CheckDistPSGQuote=true
		end if
	end if
end function

'*************************************
'Gets SPNumber on ContactID for US customer
'rev1 - Kate Kaplan 12/15/2011
'Arguments - ContactID
'Returns String
'*************************************
function GetSPNumber(ContactID)
	if len(ContactID)>0 then
	'****************************************************************
	'Check if the customer company is in the company exceptions list
	'****************************************************************
	'Select company name from contact table
	'	set rsCompany=server.CreateObject("ADODB.Recordset")
	'	strSQL="Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL,  SUBSTRING(LTRIM(RTRIM(CONTACT.EMAIL)), PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 1, { fn LENGTH(LTRIM(RTRIM(CONTACT.EMAIL))) } - PATINDEX('%@%', LTRIM(RTRIM(CONTACT.EMAIL))) + 2)  AS DOMAIN  from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=" & ContactID
	'	rsCompany.Open strSQL, dbconnstr()
	'	if not rsCompany.EOF then
	'		strCompany=trim(rsCompany("Company"))
	'		SpacePosition=instr(1,strCompany," ")
	'		strCompanyName=strCompany
	'	'If the company is in exceptions list
	'	'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES 
	'		set rsExistCompany=server.CreateObject("ADODB.Recordset")
	'		strSQL="SELECT a.SPCode FROM CCMS_PERSONNEL_DATA AS b INNER JOIN CCMS_EXCEPTION_COMPANIES AS a ON b.ID = a.SPCode  where a.STATE='"  & rsCompany("State") & "' and a.CompanyName ='" & replace(strCompanyName,"'","") & "' and a.CITY='" & replace(rsCompany("City"),"'","") & "' AND b.RecType = 'sp' and b.Active = 'y'"
	'		rsExistCompany.Open strSQL,dbconnstr()
	'		if not rsExistCompany.EOF then
	'			strSENumber=trim(rsExistCompany("SPCode"))
	'		end if
    '       if len(strSENumber)=0 then
    '            set rsExistDomain=server.CreateObject("ADODB.Recordset")
	'		     strSQL="Select a.SPCode from CCMS_PERSONNEL_DATA  AS b INNER JOIN CCMS_EXCEPTION_DOMAINS AS a ON b.ID = a.SPCode where a.STATE='"  & rsCompany("State") & "' and a.MainDomain ='" & rsCompany("Domain")  & "' AND b.RecType = 'sp' and b.Active = 'y'"
	'		    if not rsExistDomain.EOF then
	'			    strSENumber=trim(rsExistDomain("SPCode"))
	'		    end if
    '       end if
	'	end if
	'****************************************************************
	'If customer company is not in the company exceptions list or
	'Get SENumber by Zip
	'****************************************************************
		if len(strSENumber)=0 then
	'Select ZIP for current customer from table CONTACT
			set rsZip=server.CreateObject("ADODB.Recordset")
			strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & ContactID
			rsZip.Open strSQL, dbconnstr()
			if not rsZip.EOF then
				strCustomerEmail=trim(rsZip("EMAIL"))
				strZip=trim(rsZip("POSTALCODE"))
				if len(strZip)>=5 then
	'If Zip code have more then 5 digits we cut and use first five to find SENumber
					if len(strZip)>5 then 
						strZip=mid(strZip,1,5)
					end if
	'Select SECode for this ZIP CODE from table CCMS_MAP_US
					if not isnumeric(strZip) then
						Response.Redirect "/shopper/error.asp"
					end if
					set rsSENumber=	server.CreateObject("ADODB.Recordset")
					strSQL="Select SPCode from CCMS_MAP_US where ZipCode=" & strZip
					rsSENumber.Open strSQL, dbconnstr()
					if not rsSENumber.EOF then
						strSENumber=trim(rsSENumber("SPCode"))
					end if	
		'Close recordset rsSENumber
					rsSENumber.Close
					set rsSENumber=nothing
				end if
			end if	
		'Close recordset rsZip
			rsZip.Close
			set rsZip=nothing
		end if
		GetSPNumber	=strSENumber
	end if
end function
%>


 