<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<%
Function ValidateUserX()
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Validate a user
    ' Returns:          TRUE or FALSE
    ' Revisions:        0
    
    ' Get the user contact id
    
    If Session("ContactId") > 0 Then
        ValidateUserX = True
        Exit Function
    Else
        ' Check the cookie
        GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
        ' If Cookie is there
        If Len(GUIDFromUSER) > 0 Then
            ContactIdTemp = GetContactIDOnWebId(GUIDFromUSER)
            ' If we found the id in the database
            If ContactIdTemp > 0 Then
                Session("ContactId") = ContactIdTemp
				Session("GUID") = GUIDFromUSER
				GetUserData(GUIDFromUSER)
                ValidateUserX = True
                Exit Function
            ' If not found
            Else
                'There is a problem:
                'We have the GUID but somewhat we don't have the
                ' contactID
                ValidateUserX = False
                Exit Function
            End If
        Else
        ' If we do not have the GUID
        ' There are 2 possibilities
        ' He refused the GUID but he is with username and password
        ' or he is first time here
        ' so:
			ValidateUserX = False
            Exit Function
        End If
    End If
End Function
%>
<% ValidateUserX %>
<table width="400">
	<tr>
		<td>
			<hr size="1"><font face="verdana" size="2" color="#6082FF"><b>If you are requesting a quotation, please verify the information shown below:<br>
            </b></font><font face="verdana" size="2">
			<br><strong><% = Session("FirstName") %> <%' = Session("LastName") %></strong><br>
			<% = Session("Company") %><br>
			<% = Session("Address") %><br>
			<% If Len(Session("Address2")) > 0 then %>
			<% = Session("Address2") %><br>
			<% end if %>
			<% = Session("City") %>,&nbsp;<% = Session("State") %>,&nbsp;<% = Session("Zip") %><br>
			<% = Session("Country") %><br>
			<% = Session("Phone") %><br>
			<br></font>
			<font size="2">If any of this shipping information is incorrect or your address has changed, please click the button below.</font>
		<form action="/cgi-bin/profile/modform.asp" method="get" id="form1" name="form1">
			<% Session("RedirectToFirst") = "/wavepro/viewbasket.asp" %>
			<input type="submit" value="Change Address" id="submit1" name="submit1">
		</form>
			<hr size="1">
		</td>
	</tr>
</table>