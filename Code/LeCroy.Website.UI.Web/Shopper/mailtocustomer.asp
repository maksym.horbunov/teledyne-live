<%@ Language=VBScript %>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<% ServerName = Request.ServerVariables("SERVER_NAME")
if len(ServerName)>0 then
	if cstr(ServerName)<>"ny-wwwtest" and cstr(ServerName)<>"US1-VTSQL-GEN01" and cstr(ServerName)<>"US1-VTSQL-GEN01.tdy.teledyne.com" then
		ServerName="teledynelecroy.com"
	end if
else
    ServerName="teledynelecroy.com"
end if%>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="/shopper/functions.asp" -->

<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY>
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="DigitalOscilloscopes" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
	bannername="DigitalOscilloscopes"
'***************added by YK 12/27/01******************	
'++++++++++++++modified for PSG quotes by KK 4/18/2005
'if len(Session("PSGApproval"))>0 then
'	ContactID=ContactID
'	strQuoteID=Session("strQuoteID")
'	LocaleID=Session("localeid")
'else
	ContactID=Request.QueryString("cust")
	strQuoteID=Request.QueryString("qid")
	LocaleID=Request.QueryString("lid")
'end if	
if len(LocaleID)>0 then
	localeid=LocaleID
else
	localeid=1033
end if
if clng(localeid)=1033 then Session("Country")="United States"
emaillocaleid=1033
Session("localeid")=localeid
Session("emaillocaleid")=emaillocaleid

'if len(request.QueryString("name"))>0 then
'	Name=request.QueryString("name")
'	select case Name 
'	case "Adam Frisius"
'		Phone="408-486-7789"
'	case "Matthew Hallberg"
'		Phone="408-486-7215"
'	case "Other"
'		if len(request.QueryString("manualname"))>0 then
'			Name=replace(request.QueryString("manualname"),"'","''")
'			Phone=replace(request.QueryString("phone"),"'","''")
'		end if
'	end select
'end if
'+++++++++++++++++++++++++++++++++++++++++++++++++++
'****************************************************		
'Check if RESPONSE_FLAG is 'true' 
set rsCheckResponse=server.CreateObject("ADODB.Recordset")
strSQL="Select * from QUOTE where QUOTEID=" & strQuoteID
rsCheckResponse.Open strSQL, dbconnstr()
Session("offerid")=""
if len (trim(rsCheckResponse("OFFER_ID")))>0 then
	 if clng(trim(rsCheckResponse("OFFER_ID")))>0 then
		Session("offerid")=trim(rsCheckResponse("OFFER_ID"))
	 else
		Session("offerid")=""
	end if
end if
if len (trim(rsCheckResponse("CAMPAIGN_ID")))>0 then
	 if clng(trim(rsCheckResponse("CAMPAIGN_ID")))>0 then
		Session("CampaignID")=trim(rsCheckResponse("CAMPAIGN_ID"))
		if len(Session("CampaignID"))>0 then
			if clng(Session("CampaignID"))=223 then
				Session("offerid")=5
			end if
		end if
	 else
		Session("CampaignID")=""
	end if
end if
strQuoteTime=rsCheckResponse("QUOTE_TIME")

if isnull(rsCheckResponse("RESPONSE_FLAG"))=true then
'Insert into Quote table flag Response_flag=true and Response time
		set rsInsertRespFlag=server.CreateObject("ADODB.Recordset")
		strDate=Now()
		strSQL="Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE='" & strDate & "' where QUOTEID=" & strQuoteID
		rsInsertRespFlag.LockType = 3
		rsInsertRespFlag.Open strSQL, dbconnstr()
'*******************************************************************************************
'Update EMAIL_CSR TABLE	
	set rsUpdateMail=server.CreateObject("ADODB.Recordset")
	strSQL="UPDATE EMAIL_CSR set RESPONSE_FLAG='y',APPROVED_BY='SE' where QUOTEID=" & strQuoteID
	
	rsUpdateMail.LockType=3
	rsUpdateMail.Open strSQL, dbconnstr()

'*******************************************************************************************		
'Generate link to customer

'Create second Cookie id 
		'strGUID_2=GeneratedGUID()
		
'Insert second Cookie id and Customer id into table CONTACT_SECOND
		'set rsInsContactSecond=server.CreateObject("ADODB.Recordset")
		'strSQL="Insert into CONTACT_SECOND (CONTACTID, CONTACT_WEB_ID_SECOND) VALUES(" & ContactID & ",'" & strGUID_2 & "')"
		'rsInsContactSecond.LockType = 3
		'rsInsContactSecond.Open strSQL, dbconnstr()

'Select CONTSECID from CONTACT_SECOND. It willbe LINKID
		'set rsContactSecond=server.CreateObject("ADODB.Recordset")
		'strSQL="Select CONTSECID from CONTACT_SECOND where CONTACTID=" & ContactID & " and CONTACT_WEB_ID_SECOND='" & strGUID_2 & "'"
		'rsContactSecond.Open strSQL, dbconnstr()
		'if not rsContactSecond.EOF then
'This is link for Customer E-Mail
		'	strLINKID=rsContactSecond("CONTSECID")
		'	strLINK="http://" & ServerName & "/cm.asp?l=" & strLINKID & "&qid=" & strQuoteID & "&flg=true" & "&lid=" & localeid 
		'end if
'Close recordset rsContactSecond
		'rsContactSecond.Close
		'set rsContactSecond=nothing

'********************************************************************************
'****** Send Emails (to Customer, Telesales, Data Entry, SE (for Protocols)
'********************************************************************************

    Call SendQuoteToCustomer(strQuoteID,ContactID,emaillocaleid,ServerName)
    
	Response.Write "<font face=Verdana,Arial size=2 color=DodgerBlue><b>" & LoadI18N("SLBCA0102",emaillocaleid) & ".</b></font>"
else
	Response.Write "<font face=Verdana,Arial size=2 color=DodgerBlue><b>" & LoadI18N("SLBCA0103",emaillocaleid) & ".</b></font>"
end if
Response.Write "<br><br><a href=""Javascript:parent.window.close()"" title=""Close Window"" alt=""Close Window"" >"
Response.Write "CLOSE WINDOW"
Response.Write "</a>"
Session("offerid")=""
Session("strQuoteID")=""
Session("PSGApproval")=""
%>
</BODY>
</HTML>
