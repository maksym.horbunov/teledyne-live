﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Shopper_mailtocustomer" Codebehind="mailtocustomer.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryBottom">
        <div class="content">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">
                        <table cellpadding="8">
                            <tr>
                                <td valign="top"><asp:Label ID="lblPreDisplay" runat="server" /></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" bgcolor="#FFFFFF" align="center">
                        <img src="<%=rootDir%>/Shopper/images/1pixspacer.gif" width="1" height="10"><br>
                        <asp:Label ID="lblToc" runat="server" />
                    </td>
                    <td valign="top" bgcolor="#FFFFFF" align="center"><img src="<%=rootDir%>/Shopper/images/1pixspacer.gif" width="10" height="10"></td>
                </tr>
            </table>
        </div>
    </div>
    <asp:Literal ID="litJavascript" runat="server" Visible="false" />
</asp:Content>