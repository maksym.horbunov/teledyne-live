﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions
Imports LeCroy.Website.BLL

Partial Class Shopper_mailtocustomer
    Inherits BasePage
    Dim strCC As String = ""
    Dim strTo As String = ""
    Dim strBCC As String = "kate.kaplan@teledynelecroy.com,webquote@teledynelecroy.com,customercare@teledynelecroy.com"
    Dim strFrom As String = "webquote@teledynelecroy.com"
    Dim strSQL As String = ""
    Dim strCret As String = "<br />"
    Dim strSubject As String = ""
    Dim strSubjectInfo As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Submit Request Quote"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ContactID As String = ""
        Dim strQuoteID As Long = 0
        Dim strLocaleID As String = 1033
        Dim strDate As DateTime = Now()
        Dim strQuoteType As String = ""
        Dim bufferPre As StringBuilder = New StringBuilder()
        If String.IsNullOrEmpty(Request.QueryString("cust")) Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("cust")) Then
                ContactID = Request.QueryString("cust")
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        If String.IsNullOrEmpty(Request.QueryString("qid")) Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("qid")) Then
                strQuoteID = Request.QueryString("qid")
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        strQuoteType = GetQuoteType(strQuoteID)

        'Check if RESPONSE_FLAG is 'true' (quote is already approved)
        Dim dsq As DataSet
        Dim content As StringBuilder = New StringBuilder()
        strSQL = "SELECT QUOTE.QUOTEID, QUOTE.RESPONSE_FLAG, QUOTE.CAMPAIGN_ID, CONTACT.FIRST_NAME, CONTACT.LAST_NAME, CONTACT.COMPANY, CONTACT.EMAIL  FROM  QUOTE INNER JOIN CONTACT ON QUOTE.CONTACTID = CONTACT.CONTACT_ID where QUOTE.QUOTEID=@QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
        dsq = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If dsq.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In dsq.Tables(0).Rows
                Session("CampaignID") = dr("CAMPAIGN_ID").ToString

                If IsDBNull(dr("RESPONSE_FLAG")) Then
                    'Update Quote table flag Response_flag=true and Response time
                    strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=@RESPONSEDATE where QUOTEID=@QUOTEID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@RESPONSEDATE", strDate.ToString()))
                    sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
                    DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

                    'Update EMAIL_CSR TABLE	
                    strSQL = "UPDATE EMAIL_CSR set RESPONSE_FLAG='y',APPROVED_BY='SE' where QUOTEID=@QUOTEID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
                    DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    '*****send quote to Sales
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        strTo = GetSEQuoteEmail("scopes", strQuoteID).ToString
                    Else
                        strTo = GetSEQuoteEmail("psg", strQuoteID).ToString
                    End If
                    strSubjectInfo = dr("Company").ToString & ", " & dr("FIRST_NAME").ToString & " " & dr("LAST_NAME").ToString
                    strSubject = "WEB QUOTE APPROVED AND SENT TO CUSTOMER  - " + strSubjectInfo
                    Dim distreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    strSubject = "Teledyne LeCroy Quote Request - " + strSubject
                    distreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                    distreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                    distreplacements.Add("<#QuoteSubject#>", strSubject)
                    distreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    distreplacements.Add("<#TClink#>", QuoteTermsLink)
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                    Else
                        distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strQuoteID))
                    End If
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distreplacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                    Else
                        distreplacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("psg", strQuoteID, 1033))
                    End If
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                    Else
                        distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("psg", strQuoteID))
                    End If
                    distreplacements.Add("<#ProfileInfo#>", String.Empty)
                    distreplacements.Add("<#SElinks#>", String.Empty)
                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distreplacements)

                    'Send Email To Customer
                    strTo = dr("EMAIL").ToString
                    strCC = ""
                    If Not String.IsNullOrEmpty(GetSEQuoteEmail("scopes", strQuoteID).ToString) Then
                        If String.Compare(strQuoteType, "scopes") = 0 Then
                            strFrom = GetSEQuoteEmail("scopes", strQuoteID).ToString
                        Else
                            strFrom = GetSEQuoteEmail("psg", strQuoteID).ToString
                        End If
                    End If
                    strSubject = "Teledyne LeCroy Quote Request"
                    Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    distcustreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                    distcustreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                    distcustreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                    distcustreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    distcustreplacements.Add("<#TClink#>", QuoteTermsLink)
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                    Else
                        distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strQuoteID))
                    End If
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                    Else
                        distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("psg", strQuoteID))
                    End If
                    If String.Compare(strQuoteType, "scopes") = 0 Then
                        distcustreplacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                    Else
                        distcustreplacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("psg", strQuoteID, 1033))
                    End If

                    distcustreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strQuoteID, 1033))

                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distcustreplacements)


                    bufferPre.Append("<font face=Verdana,Arial size=2 color=DodgerBlue><b>" + LoadI18N("SLBCA0102", strLocaleID) + ".</b></font>")
                Else
                    bufferPre.Append("<font face=Verdana,Arial size=2 color=DodgerBlue><b>" + LoadI18N("SLBCA0103", strLocaleID) + ".</b></font>")
                End If
            Next
        End If
        dsq.Dispose()
        dsq = Nothing

        lblPreDisplay.Text = bufferPre.ToString()
    End Sub
End Class