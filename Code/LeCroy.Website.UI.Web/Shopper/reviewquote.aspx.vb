﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions
Imports LeCroy.Website.BLL

Partial Class Shopper_reviewquote
    Inherits BasePage
    Dim flgShow As Boolean = False
    Dim strCC As String = "customercare@teledynelecroy.com,webquote@teledynelecroy.com"
    Dim strTo As String = ""
    Dim strBCC As String = "kate.kaplan@teledyne.com,webquote@teledynelecroy.com,james.chan@teledyne.com"
    Dim strFrom As String = "webquote@teledynelecroy.com"
    Dim strSQL As String = ""
    Dim strCret As String = "<br />"
    Dim strSubject As String = ""
    Dim strSubjectInfo As String = ""
    Dim selecteddist As String = ""
    Dim strDesc As String = ""
    Dim bufferPre As StringBuilder = New StringBuilder()
    Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Submit Request Quote"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strUser As String = Page.User.Identity.Name
        Dim strType As String = ""
        Dim strQuoteID As Long = 0
        Dim strLocaleID As String = 1033
        Dim strDate As DateTime = Now()

       
        If Not String.IsNullOrEmpty(Request.QueryString("qid")) Then
            If IsNumeric(Request.QueryString("qid")) Then
                strQuoteID = Request.QueryString("qid")
            End If
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            If String.Compare(Request.QueryString("t"), "dm") = 0 Then
                strType = "dist"
            Else
                strType = "dir"
            End If
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("g")) Then
            If String.Compare(Request.QueryString("g"), GetQuoteLogGUID(strQuoteID), True) = 0 Then
                flgShow = True
            End If
        End If


        If strQuoteID > 0 And flgShow Then
            'Check if Quote exists

            Dim content As StringBuilder = New StringBuilder()
            strSQL = "SELECT q.QUOTEID, ql.REVIEWER,ql.SENT_TIME,ql.rowguid,q.RESPONSE_FLAG, q.CAMPAIGN_ID, c.FIRST_NAME, c.LAST_NAME, c.COMPANY  FROM  QUOTE q INNER JOIN CONTACT c  ON q.CONTACTID = c.CONTACT_ID INNER JOIN dbo.QUOTE_LOG ql ON ql.QUOTEID=q.QUOTEID where ql.SENT_TIME is NULL and q.QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
            Dim dsq As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsq.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsq.Tables(0).Rows
                    strSubjectInfo = dr("Company").ToString & ", " & dr("FIRST_NAME").ToString & " " & dr("LAST_NAME").ToString
                    Session("SubjectInfo") = strSubjectInfo
                    If String.Compare(strType, "dist") = 0 Then
                        SendQuoteToDSM(strQuoteID)
                    Else
                        SendQuoteToDirectSE(strQuoteID)
                    End If
                Next

            End If
            dsq.Dispose()
            dsq = Nothing
        End If

    End Sub


    Protected Sub SendQuoteToDirectSE(ByVal QuoteId As Long)
        '*****send SendQuoteCSRSE
        If QuoteId > 0 Then
            strSubjectInfo = Session("SubjectInfo")
            Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
            strSubject = "Teledyne LeCroy Quote Request - " + strSubjectInfo
            replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(QuoteId, True, "1033"))
            replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(QuoteId, 1033))
            replacements.Add("<#QuoteSubject#>", strSubject)
            replacements.Add("<#OrderPlacementInfo#>", String.Empty)
            replacements.Add("<#TClink#>", QuoteTermsLink)
            replacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", QuoteId))
            replacements.Add("<#SElinks#>", GetSELinks(QuoteId, 1033))
            replacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", QuoteId))
            replacements.Add("<#ProfileInfo#>", String.Empty)
            replacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", QuoteId, 1033))
            strTo = GetSEQuoteEmail("scopes", QuoteId).ToString

            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)
            UpdatedQuoteSECode(QuoteId, GetSENumber(GetContactidOnQuoteId(QuoteId)))
            bufferPre.Append("Quote was sent to Direct SE")

            strSQL = "UPDATE QUOTE_LOG SET SENTTO='DIRECT_SE',SENT_TIME = @SENTTIME where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SENTTIME", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteId.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            lblPreDisplay.Text = bufferPre.ToString()
        End If
    End Sub

    Protected Sub SendQuoteToDSM(ByVal QuoteId As Long)
        If QuoteId > 0 Then
            strTo = GetEmailContactid(GetContactidOnQuoteId(QuoteId))

            Dim strFrom As String = "webquote@teledynelecroy.com"
            '*****send quote to customer
            strSubject = "Teledyne LeCroy Quote Request"
            Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
            distcustreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(QuoteId, True, "1033"))
            distcustreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(QuoteId, 1033))
            distcustreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
            distcustreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
            distcustreplacements.Add("<#TClink#>", String.Empty)
            distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", QuoteId))
            distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", QuoteId))
            distcustreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(QuoteId, 1033))
            distcustreplacements.Add("<#CSRSE#>", String.Empty)
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distcustreplacements)

            '*****send quote to DSM
            strTo = "webquote@teledynelecroy.com,hilary.lustig@teledyne.com"
            If (Functions.HACK_AddMiPersonnelToWebQuotes(QuoteId)) Then strTo += String.Concat(",", ConfigurationManager.AppSettings("MIReRouting"))
            strFrom = "webquote@teledynelecroy.com"
            strSubjectInfo = Session("SubjectInfo")
            strSubject = "WEB QUOTE APPROVED AND SENT TO CUSTOMER  - " + strSubjectInfo
            Dim distreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
            strSubject = "Teledyne LeCroy Quote Request - " + strSubject
            distreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(QuoteId, True, "1033"))
            distreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(QuoteId, 1033))
            distreplacements.Add("<#QuoteSubject#>", strSubject)
            distreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
            distreplacements.Add("<#TClink#>", String.Empty)
            distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", QuoteId))
            distreplacements.Add("<#CSRSE#>", String.Empty)
            distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", QuoteId))
            distreplacements.Add("<#ProfileInfo#>", String.Empty)
            distreplacements.Add("<#SElinks#>", GetSendToDistLink(QuoteId, 1033))
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distreplacements)
            bufferPre.Append("Quote was sent to Distribution Managers and to Customer")

            strSQL = "UPDATE QUOTE_LOG SET SENTTO='DIST_MANAGERS',SENT_TIME = @SENTTIME where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SENTTIME", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteId.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            lblPreDisplay.Text = bufferPre.ToString()
        End If
    End Sub
End Class