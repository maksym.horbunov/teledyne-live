﻿Imports System.Data.SqlClient
Imports System.Net
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Partial Class Shopper_SendQuote
    Inherits BasePage
    Dim sql As String = ""
    Dim ds As DataSet
    Dim strWRHDO6KList As String = ConfigurationManager.AppSettings("WRHDO6KScopes").ToString
    Dim strDistScopesList As String = ConfigurationManager.AppSettings("DistScopes").ToString
    Dim strDistPSGQuoteID, strPSGQuoteID, strWSQuoteID, strQuoteID As Long
    Dim strSubject As String = ""
    Dim strSubjectInfo As String = ""
    Dim strCC As String = ""
    Dim strTo As String = ""
    Dim strBCC As String = "kate.kaplan@teledyne.com,webquote@teledynelecroy.com,james.chan@teledyne.com"
    Dim strFrom As String = "webquote@teledynelecroy.com"
    Dim strSQL As String = ""
    Dim strCret As String = "<br />"
    Dim flgCustConfSent As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("RedirectTo") = rootDir & "/shopper/"
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/support/user/")
        End If

        ' ValidateUserNoRedir()
        If GetCountofProdInBasket() = 0 Then
            Response.Redirect(rootDir + "/Shopper/RequestQuote/")
        End If
        If String.IsNullOrEmpty(Session("CampaignID")) Then Session("CampaignID") = 0
        If String.IsNullOrEmpty(Session("offerid")) Then Session("offerid") = 0
        'Subject
        strSubjectInfo = Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
        If Session("CampaignID") > 0 Then
            strSubjectInfo = strSubjectInfo & " - " & GetTacticIdOnCampaignID(Session("CampaignID"))
        End If

        If Session("Country") = "United States" Then
            '*****United States - split quotes by type
            '1 - PSG distributor;
            '2 - PSG; 
            '3 - scopes Direct (>=WP and if includes WR/HDO6K - mid range);
            '4 - scopes Distributor (<=WS only);
            '5- WR/HDO6K 

            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

            If (IsProbeOnlyUsQuote(Session.SessionID)) Then
                strQuoteID = GenerateQuote(6, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                Dim emailReplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                emailReplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                emailReplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                emailReplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                emailReplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                emailReplacements.Add("<#TClink#>", String.Empty)
                emailReplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                emailReplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                emailReplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strQuoteID, 1033))
                emailReplacements.Add("<#CSRSE#>", String.Empty)
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), emailReplacements)

                '*****Update Quote table flag Response_flag=true and Response time
                strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=@QUOTEID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@QUOTEID", strDistPSGQuoteID.ToString()))
                DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

                UpdatedQuoteSECode(strQuoteID, GetSENumber(GetContactidOnQuoteId(strQuoteID)))
                '*****send quote to Sales
                strTo = GetSEQuoteEmail("scopes", strQuoteID).ToString()
                If (Functions.HACK_AddMiPersonnelToWebQuotes(strQuoteID)) Then strTo = ConfigurationManager.AppSettings("MIReRouting")

                emailReplacements = New Dictionary(Of String, String)
                emailReplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                emailReplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                emailReplacements.Add("<#QuoteSubject#>", String.Format("Teledyne LeCroy Quote Request - WEB QUOTE APPROVED AND SENT TO CUSTOMER  - {0}", strSubjectInfo))
                emailReplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                emailReplacements.Add("<#TClink#>", String.Empty)
                emailReplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                emailReplacements.Add("<#CSRSE#>", String.Empty)
                emailReplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                emailReplacements.Add("<#ProfileInfo#>", String.Empty)
                emailReplacements.Add("<#SElinks#>", String.Empty)

                Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), emailReplacements)
            Else
                '*****PSG distributor
                If QuoteCheckProductsInBasket(1, Session.SessionID) Then
                    strDistPSGQuoteID = GenerateQuote(1, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                    '*****send quote to customer
                    strSubject = "Teledyne LeCroy Quote Request"

                    Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    distcustreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strDistPSGQuoteID, True, "1033"))
                    distcustreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strDistPSGQuoteID, 1033))
                    distcustreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                    distcustreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    distcustreplacements.Add("<#TClink#>", String.Empty)
                    distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strDistPSGQuoteID))
                    distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", strDistPSGQuoteID))
                    distcustreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strDistPSGQuoteID, 1033))
                    distcustreplacements.Add("<#CSRSE#>", String.Empty)
                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distcustreplacements)

                    '*****Update Quote table flag Response_flag=true and Response time
                    strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=@QUOTEID"
                    sqlParameters.Add(New SqlParameter("@QUOTEID", strDistPSGQuoteID.ToString()))
                    DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

                    UpdatedQuoteSECode(strDistPSGQuoteID, GetSENumber(GetContactidOnQuoteId(strDistPSGQuoteID)))
                    '*****send quote to Sales
                    strSubject = "WEB QUOTE APPROVED AND SENT TO CUSTOMER  - " + strSubjectInfo
                    strTo = "Adam.Frisius@teledynelecroy.com,GMooney@stanleyworks.com, SRiel@stanleyworks.com, PLynch@stanleyworks.com,Teresa.Zachary@teledynelecroy.com"

                    Dim distreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    strSubject = "Teledyne LeCroy Quote Request - " + strSubject
                    distreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strDistPSGQuoteID, True, "1033"))
                    distreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strDistPSGQuoteID, 1033))
                    distreplacements.Add("<#QuoteSubject#>", strSubject)
                    distreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    distreplacements.Add("<#TClink#>", String.Empty)
                    distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strDistPSGQuoteID))
                    distreplacements.Add("<#CSRSE#>", String.Empty)
                    distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", strDistPSGQuoteID))
                    distreplacements.Add("<#ProfileInfo#>", String.Empty)
                    distreplacements.Add("<#SElinks#>", String.Empty)
                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distreplacements)
                End If

                '*****PSG; 
                If QuoteCheckProductsInBasket(2, Session.SessionID) Then

                    strPSGQuoteID = GenerateQuote(2, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                    '*****send SendQuoteCSRSE
                    If Not String.IsNullOrEmpty(GetSEQuoteEmail("psg", strPSGQuoteID).ToString) Then
                        strTo = GetSEQuoteEmail("psg", strPSGQuoteID).ToString
                    End If
                    strCC = Functions.GetSpCsrEmail(Functions.GetSPNumber(Functions.GetContactidOnQuoteId(strPSGQuoteID), False))
                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    strSubject = "Teledyne LeCroy Quote Request - " + strSubjectInfo
                    replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strPSGQuoteID, True, "1033"))
                    replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strPSGQuoteID, 1033))
                    replacements.Add("<#QuoteSubject#>", strSubject)
                    replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    replacements.Add("<#TClink#>", QuoteTermsLink)
                    replacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strPSGQuoteID))
                    replacements.Add("<#SElinks#>", GetSELinks(strPSGQuoteID, 1033))
                    replacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("psg", strPSGQuoteID))
                    replacements.Add("<#ProfileInfo#>", String.Empty)
                    replacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("psg", strPSGQuoteID, 1033))
                    InsertQuoteReminder("psg", strPSGQuoteID, GetSEQuoteEmail("psg", strPSGQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, replacements))
                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)

                    '********Send Customer Confirmation
                    Dim replacements13 As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    If Not flgCustConfSent Then
                        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 13, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements13)
                        flgCustConfSent = True
                    End If
                    'INSERT into EMAIL REMINDER 
                    Dim custreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    custreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strPSGQuoteID, True, "1033"))
                    custreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strPSGQuoteID, 1033))
                    custreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                    custreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    custreplacements.Add("<#TClink#>", QuoteTermsLink)
                    custreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("psg", strPSGQuoteID))
                    custreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("psg", strPSGQuoteID))
                    custreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strPSGQuoteID, 1033))
                    custreplacements.Add("<#CSRSE#>", String.Empty)
                    UpdatedQuoteSECode(strPSGQuoteID, GetSPNumber(GetContactidOnQuoteId(strPSGQuoteID)))
                End If

                'if not WR&HDO6K, split Direct and Dist scope quotes
                If Not QuoteCheckProductsInBasket(3, Session.SessionID) Then

                    If QuoteCheckProductsInBasket(5, Session.SessionID) Then
                        '*****Scopes Distributor (<=WS only)
                        strWSQuoteID = GenerateQuote(5, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        If strWSQuoteID > 0 Then

                            '*****send quote to customer
                            strSubject = "Teledyne LeCroy Quote Request"
                            Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            distcustreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strWSQuoteID, True, "1033"))
                            distcustreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strWSQuoteID, 1033))
                            distcustreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                            distcustreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            distcustreplacements.Add("<#TClink#>", String.Empty)
                            distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strWSQuoteID))
                            distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", strWSQuoteID))
                            distcustreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strWSQuoteID, 1033))
                            distcustreplacements.Add("<#CSRSE#>", String.Empty)
                            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distcustreplacements)

                            InsertQuoteReminder("scopes", strWSQuoteID, strTo, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, distcustreplacements))

                            '*****Update Quote table flag Response_flag=true and Response time
                            strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=@QUOTEID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@QUOTEID", strWSQuoteID.ToString()))
                            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            strSQL = "Update  EMAIL_CSR set RESPONSE_FLAG='y' where QUOTEID=@QUOTEID"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@QUOTEID", strWSQuoteID.ToString()))
                            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            UpdatedQuoteSECode(strWSQuoteID, GetSENumber(GetContactidOnQuoteId(strWSQuoteID)))
                            '*****send quote to DSM
                            strSubject = "WEB QUOTE APPROVED AND SENT TO CUSTOMER  - " + strSubjectInfo

                            strTo = "webquote@teledynelecroy.com,hilary.lustig@teledyne.com"
                            If (Functions.HACK_AddMiPersonnelToWebQuotes(strWSQuoteID)) Then strTo += String.Concat(",", ConfigurationManager.AppSettings("MIReRouting"))
                            Dim distreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            strSubject = "Teledyne LeCroy Quote Request - " + strSubject
                            distreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strWSQuoteID, True, "1033"))
                            distreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strWSQuoteID, 1033))
                            distreplacements.Add("<#QuoteSubject#>", strSubject)
                            distreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            distreplacements.Add("<#TClink#>", String.Empty)
                            distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strWSQuoteID))
                            distreplacements.Add("<#CSRSE#>", String.Empty)
                            distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", strWSQuoteID))
                            distreplacements.Add("<#ProfileInfo#>", String.Empty)
                            distreplacements.Add("<#SElinks#>", GetSendToDistLink(strWSQuoteID, 1033))

                            Dim t3cc As String = ModifyCCForT3HVP(strWSQuoteID)
                            If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                            Dim t3to As String = ModifyToForT3LowEnd(strWSQuoteID)
                            If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), distreplacements)
                        End If
                    End If
                    '*****Scopes Direct
                    If QuoteCheckProductsInBasket(4, Session.SessionID) Then
                        strQuoteID = GenerateQuote(4, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        If strQuoteID > 0 Then
                            '*****send SendQuoteCSRSE
                            strTo = GetSEQuoteEmail("scopes", strQuoteID).ToString
                            If (Functions.HACK_AddMiPersonnelToWebQuotes(strQuoteID)) Then strTo = ConfigurationManager.AppSettings("MIReRouting")
                            strFrom = "webquote@teledynelecroy.com"
                            Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            strSubject = "Teledyne LeCroy Quote Request - " + strSubjectInfo
                            replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                            replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                            replacements.Add("<#QuoteSubject#>", strSubject)
                            replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            replacements.Add("<#TClink#>", QuoteTermsLink)
                            replacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                            replacements.Add("<#SElinks#>", GetSELinks(strQuoteID, 1033))
                            replacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                            replacements.Add("<#ProfileInfo#>", String.Empty)
                            replacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                            InsertQuoteReminder("scopes", strQuoteID, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, replacements))

                            Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                            If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                            Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                            If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)
                            strCC = String.Empty

                            '********Send Customer Confirmation
                            strFrom = "webquote@teledynelecroy.com"
                            Dim replacements13 As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            If Not flgCustConfSent Then
                                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 13, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements13)
                                flgCustConfSent = True
                            End If
                            'INSERT into EMAIL REMINDER 
                            'INSERT into EMAIL_CUSTOMER 
                            Dim custreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            custreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                            custreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                            custreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                            custreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            custreplacements.Add("<#TClink#>", QuoteTermsLink)
                            custreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                            custreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                            custreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strQuoteID, 1033))
                            custreplacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                            InsertQuoteCustomerEmail(strQuoteID, Session("email").ToString, strFrom, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, custreplacements))
                            UpdatedQuoteSECode(strQuoteID, GetSENumber(GetContactidOnQuoteId(strQuoteID)))
                        End If
                    End If
                Else
                    'if direct product in the cart- sent to direct
                    If QuoteCheckProductsInBasket(4, Session.SessionID) Then
                        strQuoteID = GenerateQuote(6, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        If strQuoteID > 0 Then
                            '*****send SendQuoteCSRSE
                            strTo = GetSEQuoteEmail("scopes", strQuoteID).ToString
                            If (Functions.HACK_AddMiPersonnelToWebQuotes(strQuoteID)) Then strTo = ConfigurationManager.AppSettings("MIReRouting")
                            strFrom = "webquote@teledynelecroy.com"
                            Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            strSubject = "Teledyne LeCroy Quote Request - " + strSubjectInfo
                            replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                            replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                            replacements.Add("<#QuoteSubject#>", strSubject)
                            replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            replacements.Add("<#TClink#>", QuoteTermsLink)
                            replacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                            replacements.Add("<#SElinks#>", GetSELinks(strQuoteID, 1033))
                            replacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                            replacements.Add("<#ProfileInfo#>", String.Empty)
                            replacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                            InsertQuoteReminder("scopes", strQuoteID, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, replacements))

                            Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                            If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                            Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                            If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)
                            strCC = String.Empty

                            '********Send Customer Confirmation
                            Dim replacements13 As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            If Not flgCustConfSent Then
                                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 13, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements13)
                                flgCustConfSent = True
                            End If
                            'INSERT into EMAIL REMINDER 
                            'INSERT into EMAIL_CUSTOMER 
                            Dim custreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                            custreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                            custreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                            custreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                            custreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                            custreplacements.Add("<#TClink#>", QuoteTermsLink)
                            custreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                            custreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                            custreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strQuoteID, 1033))
                            InsertQuoteCustomerEmail(strQuoteID, Session("email").ToString, strFrom, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, custreplacements))
                            UpdatedQuoteSECode(strQuoteID, GetSENumber(GetContactidOnQuoteId(strQuoteID)))
                        End If
                    End If
                    'if there Dist Scopes products and WR&HDO6K in the cart
                    'send for manual review
                    If GetCountofProdInBasket() > 0 Then
                        strQuoteID = GenerateQuote(6, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        'NEW PROCESS
                        '*****SEND QUOTE FOR REVIEW
                        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                        strSubject = "Teledyne LeCroy Quote Request - " + strSubjectInfo
                        replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                        replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                        replacements.Add("<#QuoteSubject#>", strSubject)
                        replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                        replacements.Add("<#TClink#>", QuoteTermsLink)
                        replacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                        replacements.Add("<#SElinks#>", GetSendQuoteForReviewLink(strQuoteID, 1033))
                        replacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                        replacements.Add("<#ProfileInfo#>", String.Empty)
                        replacements.Add("<#CSRSE#>", GetSECSRQuoteFooter("scopes", strQuoteID, 1033))
                        strTo = "hilary.lustig@teledynelecroy.com"
                        If (Functions.HACK_AddMiPersonnelToWebQuotes(strQuoteID)) Then strTo += String.Concat(",", ConfigurationManager.AppSettings("MIReRouting"))
                        strFrom = "webquote@teledynelecroy.com"
                        InsertQuoteReminder("scopes", strQuoteID, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, replacements))

                        Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                        If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                        Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                        If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)
                        strCC = String.Empty

                        '********Send Customer Confirmation
                        Dim replacements13 As Dictionary(Of String, String) = New Dictionary(Of String, String)
                        If Not flgCustConfSent Then
                            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 13, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements13)
                            flgCustConfSent = True
                        End If
                        'INSERT into QUOTE_LOG
                        'INSERT into EMAIL_CUSTOMER 
                        Dim custreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                        custreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, True, "1033"))
                        custreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                        custreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                        custreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                        custreplacements.Add("<#TClink#>", QuoteTermsLink)
                        custreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", strQuoteID))
                        custreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("scopes", strQuoteID))
                        custreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(strQuoteID, 1033))
                        strSQL = "INSERT INTO QUOTE_LOG (QUOTEID) VALUES(@QUOTEID)"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
                        DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        InsertQuoteCustomerEmail(strQuoteID, Session("email").ToString, strFrom, GetSEQuoteEmail("scopes", strQuoteID).ToString, EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, custreplacements))
                        UpdatedQuoteSECode(strQuoteID, GetSENumber(GetContactidOnQuoteId(strQuoteID)))
                    End If
                End If
            End If
        ElseIf Session("Country") = "Canada" Then
            strSubject = "Teledyne LeCroy Quote Request - " + Session("COUNTRY") + ", " + strSubjectInfo
            If GetCountofProdInBasket() > 0 Then
                If QuoteCheckProductsInBasket(2, Session.SessionID) Then
                    If (GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(Session("COUNTRY"), 19, 1).ToList).Count > 0 Then
                        strPSGQuoteID = GenerateQuote(2, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        Dim replacementsPSG As Dictionary(Of String, String) = New Dictionary(Of String, String)
                        replacementsPSG.Add("<#QuoteProductInfo#>", GetQuoteBody(strPSGQuoteID, False, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                        replacementsPSG.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strPSGQuoteID, 1033))
                        replacementsPSG.Add("<#QuoteSubject#>", strSubject)
                        replacementsPSG.Add("<#OrderPlacementInfo#>", String.Empty)
                        replacementsPSG.Add("<#TClink#>", String.Empty)
                        replacementsPSG.Add("<#USDiscountStatement#>", String.Empty)
                        replacementsPSG.Add("<#CSRSE#>", String.Empty)
                        replacementsPSG.Add("<#OrderConditionsStatement#>", String.Empty)
                        replacementsPSG.Add("<#ProfileInfo#>", String.Empty)
                        replacementsPSG.Add("<#SElinks#>", String.Empty)
                        strTo = GetSalesRepEmail(Session("COUNTRY"), 1, 19)
                        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacementsPSG)
                    End If
                End If
                If GetCountofProdInBasket() > 0 Then
                    '*********Send Email to Sales
                    strQuoteID = GenerateQuote(6, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, False, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                    replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                    replacements.Add("<#QuoteSubject#>", strSubject)
                    replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    replacements.Add("<#TClink#>", String.Empty)
                    replacements.Add("<#USDiscountStatement#>", String.Empty)
                    replacements.Add("<#CSRSE#>", String.Empty)
                    replacements.Add("<#OrderConditionsStatement#>", String.Empty)
                    replacements.Add("<#ProfileInfo#>", String.Empty)
                    replacements.Add("<#SElinks#>", String.Empty)
                    strTo = GetSalesRepEmail(Session("COUNTRY"), 1, 1)

                    Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                    If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                    Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                    If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)
                End If
            End If

            '********Send Customer Email
            Dim caReplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
            caReplacements.Add("<#CustomerFirstName#>", Session("FirstName"))
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 35, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), caReplacements)
        Else
            '*******International Quotes
            'Email is sent to Sales list + distributors by country
            'confirmation note sent to customer in Europe only
            strSubject = "Teledyne LeCroy Quote Request - " + Session("COUNTRY") + ", " + strSubjectInfo
            If GetCountofProdInBasket() > 0 Then
                If QuoteCheckProductsInBasket(2, Session.SessionID) Then
                    If (GetSalesRepEmailAddressXRefsForCountryCategoryRequestType(Session("COUNTRY"), 19, 1).ToList).Count > 0 Then
                        strPSGQuoteID = GenerateQuote(2, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                        Dim replacementsPSG As Dictionary(Of String, String) = New Dictionary(Of String, String)
                        replacementsPSG.Add("<#QuoteProductInfo#>", GetQuoteBody(strPSGQuoteID, False, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                        replacementsPSG.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strPSGQuoteID, 1033))
                        replacementsPSG.Add("<#QuoteSubject#>", strSubject)
                        replacementsPSG.Add("<#OrderPlacementInfo#>", String.Empty)
                        replacementsPSG.Add("<#TClink#>", String.Empty)
                        replacementsPSG.Add("<#USDiscountStatement#>", String.Empty)
                        replacementsPSG.Add("<#CSRSE#>", String.Empty)
                        replacementsPSG.Add("<#OrderConditionsStatement#>", String.Empty)
                        replacementsPSG.Add("<#ProfileInfo#>", String.Empty)
                        replacementsPSG.Add("<#SElinks#>", String.Empty)
                        strTo = GetSalesRepEmail(Session("COUNTRY"), 1, 19)
                        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacementsPSG)
                    End If
                End If
                If GetCountofProdInBasket() > 0 Then
                    '*********Send Email to Sales
                    strQuoteID = GenerateQuote(6, Session("ContactID"), Session.SessionID, Session("offerid"), Session("CampaignID"))
                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    replacements.Add("<#QuoteProductInfo#>", GetQuoteBody(strQuoteID, False, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                    replacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(strQuoteID, 1033))
                    replacements.Add("<#QuoteSubject#>", strSubject)
                    replacements.Add("<#OrderPlacementInfo#>", String.Empty)
                    replacements.Add("<#TClink#>", String.Empty)
                    replacements.Add("<#USDiscountStatement#>", String.Empty)
                    replacements.Add("<#CSRSE#>", String.Empty)
                    replacements.Add("<#OrderConditionsStatement#>", String.Empty)
                    replacements.Add("<#ProfileInfo#>", String.Empty)
                    replacements.Add("<#SElinks#>", String.Empty)
                    strTo = GetSalesRepEmail(Session("COUNTRY"), 1, 1)

                    Dim t3cc As String = ModifyCCForT3HVP(strQuoteID)
                    If Not (String.IsNullOrEmpty(t3cc)) Then strCC = t3cc
                    Dim t3to As String = ModifyToForT3LowEnd(strQuoteID)
                    If Not (String.IsNullOrEmpty(t3to)) Then strTo += String.Concat(",", t3to)

                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 20, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({strTo}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements)

                End If
                '********Send Customer Confirmation for Europe
                If GetRegionOnCountry(Session("COUNTRY")) = 2 Then
                    Dim replacements19 As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 19, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), strFrom, New List(Of String)({Session("email").ToString}), New List(Of String)({strCC}), New List(Of String)({strBCC}), replacements19)
                End If
            End If
        End If

        Dim quoteId As String = String.Empty

        Dim strredirect As String = "AfterSendQuote.aspx?sq="
        If strQuoteID > 0 Then
            quoteId = strQuoteID
            strredirect = strredirect + strQuoteID.ToString
        End If
        strredirect = strredirect + "&pq="
        If strPSGQuoteID > 0 Then
            quoteId = strPSGQuoteID
            strredirect = strredirect + strPSGQuoteID.ToString
        End If
        strredirect = strredirect + "&sdq="
        If strWSQuoteID > 0 Then
            quoteId = strWSQuoteID
            strredirect = strredirect + strWSQuoteID.ToString
        End If
        strredirect = strredirect + "&pdq="
        If strDistPSGQuoteID > 0 Then
            quoteId = strDistPSGQuoteID
            strredirect = strredirect + strDistPSGQuoteID.ToString
        End If
        HandlePardot(quoteId)
        Response.Redirect(strredirect)
    End Sub

    Private Function IsProbeOnlyUsQuote(ByVal sessionId As String) As Boolean
        Dim sqlString As String = "SELECT DISTINCT pg.* FROM [SHOPPER] s INNER JOIN [PRODUCT_GROUP] pg ON s.GROUP_ID = pg.GROUP_ID WHERE s.SESSIONID = @SESSIONID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionId.ToString()))
        Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray())
        Return Not (productGroups.Where(Function(x) x.CategoryId.Value <> CategoryIds.PROBES).Count > 0)
    End Function

    Private Function ModifyCCForT3HVP(ByVal quoteId As String) As String    'GL
        Dim sqlString As String = "SELECT DISTINCT pg.* FROM [SHOPPERQUOTE] sq INNER JOIN [PRODUCT_GROUP] pg ON sq.GROUP_ID = pg.GROUP_ID WHERE sq.QUOTEID = @QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId))

        Dim configs As String() = ConfigurationManager.AppSettings("T3HVPCatId").ToString().Split(",")
        Dim t3HvpCatIdsList As List(Of Int32) = New List(Of Int32)
        For Each c In configs
            If Not (String.IsNullOrEmpty(c)) Then t3HvpCatIdsList.Add(Convert.ToInt32(c.ToString()))
        Next

        Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray())
        If (productGroups.Select(Function(x) x.CategoryId.Value).Distinct().Intersect(t3HvpCatIdsList).Any()) Then ' Time Domain Reflectometers
            Return ConfigurationManager.AppSettings("T3HVPCC")
        End If
        Return String.Empty
    End Function

    Private Function ModifyToForT3LowEnd(ByVal quoteId As String) As String 'FM
        Dim sqlString As String = "SELECT DISTINCT pg.* FROM [SHOPPERQUOTE] sq INNER JOIN [PRODUCT_GROUP] pg ON sq.GROUP_ID = pg.GROUP_ID WHERE sq.QUOTEID = @QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId))

        Dim configs As String() = ConfigurationManager.AppSettings("T3LowCatId").ToString().Split(",")
        Dim t3CatIdsList As List(Of Int32) = New List(Of Int32)
        For Each c In configs
            If Not (String.IsNullOrEmpty(c)) Then t3CatIdsList.Add(Convert.ToInt32(c.ToString()))
        Next

        ' HACK: GL wants AWG only, not AFG
        If (t3CatIdsList.Contains(2)) Then
            Dim temp As Int32
            Dim exceptionProductSeriesIds As List(Of Int32) = ConfigurationManager.AppSettings("T3LowCatIdExceptionSeriesIds").ToString().Split(",").Where(Function(x) Int32.TryParse(x, temp)).Select(Function(x) temp).ToList()
            Dim psLookup As String = "SELECT DISTINCT psc.PRODUCT_SERIES_ID FROM [SHOPPERQUOTE] sq INNER JOIN [PRODUCT_SERIES_CATEGORY] psc ON sq.PRODUCTID = psc.PRODUCT_ID WHERE sq.QUOTEID = @QUOTEID"
            Dim psLookupParams As List(Of SqlParameter) = New List(Of SqlParameter)
            psLookupParams.Add(New SqlParameter("@QUOTEID", quoteId))
            Dim productSeriesIdsForException As List(Of Int32) = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString"), psLookup, psLookupParams.ToArray()).Select(Function(x) x.ProductSeriesId).Distinct().ToList()
            If (productSeriesIdsForException.Intersect(exceptionProductSeriesIds).Any()) Then
                Return ConfigurationManager.AppSettings("T3HVPCC")
            End If
        End If

        Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.FetchProductGroups(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray())
        If (productGroups.Select(Function(x) x.CategoryId.Value).Distinct().Intersect(t3CatIdsList).Any()) Then
            Return ConfigurationManager.AppSettings("T3LowEndTo")
        End If

        ' If category didn't trip; look at shared category series IDs
        configs = ConfigurationManager.AppSettings("T3SharedSeriesIds").ToString().Split(",")
        Dim t3SeriesIdsList As List(Of Int32) = New List(Of Int32)
        For Each c In configs
            If Not (String.IsNullOrEmpty(c)) Then t3SeriesIdsList.Add(Convert.ToInt32(c.ToString()))
        Next
        sqlString = "SELECT DISTINCT * FROM [SHOPPERQUOTE] WHERE QUOTEID = @QUOTEID"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId))
        Dim shopperQuotes As List(Of ShopperQuote) = ShopperQuoteRepository.FetchShopperQuotes(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray())
        If (shopperQuotes.Select(Function(x) x.ProductSeriesId.Value).Distinct().Intersect(t3SeriesIdsList).Any() Or shopperQuotes.Select(Function(x) x.RefSeriesId).Distinct().Intersect(t3SeriesIdsList).Any()) Then
            Return ConfigurationManager.AppSettings("T3LowEndTo")
        End If

        ' Last chance to catch
        configs = ConfigurationManager.AppSettings("T3SharedGroupIds").ToString().Split(",")
        Dim t3GroupIdsList As List(Of Int32) = New List(Of Int32)
        For Each c In configs
            If Not (String.IsNullOrEmpty(c)) Then t3GroupIdsList.Add(Convert.ToInt32(c.ToString()))
        Next
        If (shopperQuotes.Select(Function(x) x.GroupId.Value).Distinct().Intersect(t3GroupIdsList).Any()) Then
            Return ConfigurationManager.AppSettings("T3LowEndTo")
        End If
        Return String.Empty
    End Function

    Private Function GetDataForPardot(ByVal quoteId As String) As PardotData
        Dim sqlString As String = "SELECT pg.CATEGORY_ID, p.PARTNUMBER, sq.PRICE, sq.PRODUCTID FROM [SHOPPERQUOTE] sq INNER JOIN [PRODUCT_GROUP] pg ON sq.GROUP_ID = pg.GROUP_ID INNER JOIN [PRODUCT] p ON sq.PRODUCTID = p.PRODUCTID WHERE sq.QUOTEID = @QUOTEID ORDER BY sq.PRICE DESC"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", quoteId))

        Dim retVal As PardotData = New PardotData()
        Dim productGroups As List(Of ExtendedProductGroup) = ProductGroupRepository.FetchExtendedProductGroup(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray())
        retVal.Partnumber = productGroups.FirstOrDefault().Partnumber
        retVal.TotalPrice = productGroups.Sum(Function(x) x.Price)
        If (productGroups.FirstOrDefault().CategoryId.Value = 19) Then
            retVal.IsPsgQuote = True
            Return retVal
        End If
        retVal.IsPsgQuote = False
        Return retVal
    End Function

    Public Function QuoteCheckProductsInBasket(ByVal quotetypeid As Integer, ByVal sessionid As String) As Boolean
        QuoteCheckProductsInBasket = False
        Dim dtsCheckBasket As DataSet
        If quotetypeid > 0 And Not String.IsNullOrEmpty(quotetypeid) Then
            Dim strSQL As String = ""
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            Select Case quotetypeid
                Case 1 'PSG distributor
                    strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007)) and sessionid=@SESSIONID"
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                Case 2 'PSG
                    strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where propertygroupid in(select propertygroupid from propertygroup where category_id=19)) and sessionid=@SESSIONID"
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                Case 3 'Scopes - WR &HDO6K
                    Dim sqlKeys As String = String.Empty
                    Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
                    dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString()), "PRODSERID", sqlKeys, sqlParameters)
                    strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0}) ) and sessionid=@SESSIONID", sqlKeys)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                Case 4 ' Scopes Direct
                    Dim sqlKeys As String = String.Empty
                    Dim excludes As List(Of String) = Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString())
                    excludes.AddRange(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()))
                    Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
                    dbUtils.CreateSqlInStatement(excludes, "IDS", sqlKeys, sqlParameters)
                    strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID not in ({0})  and  REF_SERIES_ID not in ({0})) and sessionid=@SESSIONID", sqlKeys)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
                Case 5 ' Scopes Distr
                    Dim sqlKeys As String = String.Empty
                    Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
                    dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()), "PRODSERID", sqlKeys, sqlParameters)
                    strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0}) OR (exists(SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID   AND psc.category_id=1 AND  psc.PRODUCT_SERIES_ID in  ({0}) ) AND NOT EXISTS (SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID  AND psc.category_id=1 AND psc.PRODUCT_SERIES_ID  NOT in  ({0})  ))) and sessionid=@SESSIONID", sqlKeys)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid.ToString()))
            End Select
            dtsCheckBasket = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dtsCheckBasket.Tables(0).Rows.Count > 0 Then
                QuoteCheckProductsInBasket = True
            End If
            dtsCheckBasket.Dispose()
            dtsCheckBasket = Nothing
        End If
    End Function

    Public Function GenerateQuote(ByVal quotetypeid As Integer, ByVal ContactID As String, ByVal SessionID As String, ByVal offerid As String, ByVal CampaignID As String) As Double
        '********* function Generate Quote 
        '********* author Kate Kaplan
        '********* rev 1 - 8/13/2013
        '********* creates QuoteID
        '********* and moved items from SHOPPER to SHOPPERQUOTE
        Dim strDate As DateTime
        Dim strQuoteID As Long = 0
        Dim dtsQUOTEID As DataSet
        Dim dtsFromShopper As DataSet
        Dim strSQL As String
        strDate = Now()
        If Not String.IsNullOrEmpty(ContactID) And Not String.IsNullOrEmpty(SessionID) Then
            If IsNumeric(ContactID) Then
                'Create Quote_id for current quote in Quote table
                strSQL = "Insert into QUOTE (CONTACTID,QUOTE_TIME,Offer_id,Campaign_id) VALUES (@CONTACTID,@QUOTETIME,@OFFERID,@CAMPAIGNID)"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
                sqlParameters.Add(New SqlParameter("@QUOTETIME", strDate.ToString()))
                sqlParameters.Add(New SqlParameter("@OFFERID", offerid.ToString()))
                sqlParameters.Add(New SqlParameter("@CAMPAIGNID", CampaignID.ToString()))
                DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                'Select Quote_id which was created
                strSQL = "SELECT max(QUOTEID) as QUOTEID from QUOTE where CONTACTID=@CONTACTID and QUOTE_TIME=@QUOTETIME"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
                sqlParameters.Add(New SqlParameter("@QUOTETIME", strDate.ToString()))
                dtsQUOTEID = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If dtsQUOTEID.Tables(0).Rows.Count > 0 Then
                    Dim dr As System.Data.DataRow
                    For Each dr In dtsQUOTEID.Tables(0).Rows
                        strQuoteID = dr("QUOTEID")
                    Next
                    If strQuoteID > 0 Then
                        'Copy Cart to shopperQuote Table
                        sqlParameters = New List(Of SqlParameter)
                        Select Case quotetypeid
                            Case 1 'PSG distributor
                                strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007)) and sessionid=@SESSIONID"
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            Case 2 'PSG
                                strSQL = "SELECT * FROM SHOPPER WHERE PRODUCTID in (select productid from product where propertygroupid in(select propertygroupid from propertygroup where category_id=19)) and sessionid=@SESSIONID"
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            Case 3 'Scopes - WR &HDO6K
                                Dim sqlKeys As String = String.Empty
                                sqlParameters = New List(Of SqlParameter)
                                Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString()), "IDS", sqlKeys, sqlParameters)
                                strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0})) and sessionid=@SESSIONID", sqlKeys)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            Case 4 ' Scopes Direct
                                Dim sqlKeys As String = String.Empty
                                sqlParameters = New List(Of SqlParameter)
                                Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                Dim excludes As List(Of String) = Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString())
                                excludes.AddRange(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()))
                                dbUtils.CreateSqlInStatement(excludes, "IDS", sqlKeys, sqlParameters)
                                strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID not in ({0})  and  REF_SERIES_ID not in ({0})) and sessionid=@SESSIONID", sqlKeys)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            Case 5 ' Scopes Distr
                                Dim sqlKeys As String = String.Empty
                                sqlParameters = New List(Of SqlParameter)
                                Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()), "IDS", sqlKeys, sqlParameters)
                                strSQL = String.Format("SELECT * FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0}) OR (exists(SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID  AND psc.category_id=1 AND psc.PRODUCT_SERIES_ID in  ({0}) ) AND NOT EXISTS (SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID  AND psc.category_id=1 AND psc.PRODUCT_SERIES_ID  NOT in  ({0})  ))) and sessionid=@SESSIONID", sqlKeys)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            Case 6 ' Everyting in the Cart
                                strSQL = "SELECT * FROM SHOPPER WHERE sessionid=@SESSIONID"
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                        End Select
                        dtsFromShopper = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        Dim dr1 As System.Data.DataRow
                        If dtsFromShopper.Tables(0).Rows.Count > 0 Then
                            For Each dr1 In dtsFromShopper.Tables(0).Rows
                                'Insert into Shopperquote
                                strSQL = "INSERT into shopperquote (SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CUSTOMERID,QUOTEID,MASTERPRODUCTID, GROUP_ID, PRODUCT_SERIES_ID,REF_SERIES_ID) VALUES (@SESSIONID,@PRODUCTID,@QTY,@PRICE,@DATEENTERED,@PROPERTYGROUPID,@CUSTOMERID,@QUOTEID,@MASTERPRODUCTID,@GROUPID,@PRODUCTSERIESID,@REFSERIESID)"
                                sqlParameters = New List(Of SqlParameter)
                                sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                sqlParameters.Add(New SqlParameter("@PRODUCTID", dr1("PRODUCTID").ToString()))
                                sqlParameters.Add(New SqlParameter("@QTY", dr1("QTY").ToString()))
                                sqlParameters.Add(New SqlParameter("@PRICE", dr1("PRICE").ToString()))
                                sqlParameters.Add(New SqlParameter("@DATEENTERED", dr1("DATE_ENTERED").ToString()))
                                sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", dr1("PROPERTYGROUPID").ToString()))
                                sqlParameters.Add(New SqlParameter("@CUSTOMERID", ContactID.ToString()))
                                sqlParameters.Add(New SqlParameter("@QUOTEID", strQuoteID.ToString()))
                                sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", dr1("MASTERPRODUCTID").ToString()))
                                sqlParameters.Add(New SqlParameter("@GROUPID", dr1("GROUP_ID").ToString()))
                                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", dr1("PRODUCT_SERIES_ID").ToString()))
                                sqlParameters.Add(New SqlParameter("@REFSERIESID", dr1("REF_SERIES_ID").ToString()))
                                DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            Next

                            sqlParameters = New List(Of SqlParameter)
                            Select Case quotetypeid
                                Case 1 'PSG distributor
                                    strSQL = "DELETE FROM SHOPPER WHERE PRODUCTID in (select product_id from product_series_category where product_series_id in (216,165,121) and category_id=19 and product_id not in (2095,4005,4006,4007)) and sessionid=@SESSIONID"
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                Case 2 'PSG
                                    strSQL = "DELETE FROM SHOPPER WHERE PRODUCTID in (select productid from product where propertygroupid in(select propertygroupid from propertygroup where category_id=19)) and sessionid=@SESSIONID"
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                Case 3 'Scopes - WR &HDO6K
                                    Dim sqlKeys As String = String.Empty
                                    sqlParameters = New List(Of SqlParameter)
                                    Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                    dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString()), "IDS", sqlKeys, sqlParameters)
                                    strSQL = String.Format("DELETE FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0}) and sessionid=@SESSIONID", sqlKeys)
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                Case 4 ' Scopes Direct
                                    Dim sqlKeys As String = String.Empty
                                    sqlParameters = New List(Of SqlParameter)
                                    Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                    Dim excludes As List(Of String) = Functions.ConvertPotentialCommaSeparatedStringIntoList(strWRHDO6KList.ToString())
                                    excludes.AddRange(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()))
                                    dbUtils.CreateSqlInStatement(excludes, "IDS", sqlKeys, sqlParameters)
                                    strSQL = String.Format("DELETE FROM SHOPPER WHERE( PRODUCT_SERIES_ID not in ({0})  and  REF_SERIES_ID not in ({0})) and sessionid=@SESSIONID", sqlKeys)
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                Case 5 ' Scopes Distr
                                    Dim sqlKeys As String = String.Empty
                                    sqlParameters = New List(Of SqlParameter)
                                    Dim dbUtils As BaseClasses.DALHelperUtils = New BaseClasses.DALHelperUtils()
                                    dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strDistScopesList.ToString()), "IDS", sqlKeys, sqlParameters)
                                    strSQL = String.Format("DELETE FROM SHOPPER WHERE( PRODUCT_SERIES_ID in ({0})  or  REF_SERIES_ID  in ({0}) OR (exists(SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID AND psc.category_id=1 AND psc.PRODUCT_SERIES_ID in  ({0}) ) AND NOT EXISTS (SELECT * FROM config c INNER JOIN product_series_category psc ON c.PRODUCTID=psc.PRODUCT_ID WHERE optionid=SHOPPER.PRODUCTID AND psc.category_id=1 AND psc.PRODUCT_SERIES_ID  NOT in  ({0})  ))) and sessionid=@SESSIONID", sqlKeys)
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                                Case 6 ' Everyting in the Cart
                                    strSQL = "DELETE FROM SHOPPER WHERE sessionid=@SESSIONID"
                                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionID.ToString()))
                            End Select
                            DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        End If
                    End If
                End If
            End If
        End If
        GenerateQuote = strQuoteID
    End Function

    Private Sub HandlePardot(ByVal quoteId As String)
        Dim url As String = String.Empty
        Dim response As String = String.Empty
        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            Dim psgOrScopeQuote As String = "Oscilloscope"
            Dim pardotData As PardotData = GetDataForPardot(quoteId)
            If (pardotData.IsPsgQuote) Then
                psgOrScopeQuote = "Protocol"
            End If

            Dim nvc As New NameValueCollection
            ' Add existing data
            nvc.Add("hdnEmailAddress", Session("email"))
            nvc.Add("requestType", psgOrScopeQuote)
            nvc.Add("quoteNumber", quoteId)
            nvc.Add("partNumber", pardotData.Partnumber)
            nvc.Add("total", pardotData.TotalPrice)
            ' Add new fields requested in INC0572969
            nvc.Add("cfname", Session("FirstName"))
            nvc.Add("clname", Session("LastName"))
            nvc.Add("cphone", Session("Phone"))
            Dim country As String = Session("Country")
            ' Pardot country values differ from the web catalog site
            nvc.Add("ccountry", CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), country).PardotCountry)
            nvc.Add("caddress", Session("Address"))
            nvc.Add("caddress2", Session("Address2"))
            nvc.Add("ccity", Session("City"))
            ' Only supply State/Province for US/Canada
            If (String.Compare(Session("Country"), "United States", True) = 0 Or String.Compare(Session("Country"), "Canada", True) = 0) Then
                nvc.Add("cstate", Session("State"))
            Else
                nvc.Add("cstate", "")
            End If
            nvc.Add("czip", Session("Zip"))
            nvc.Add("ccompany", Session("Company"))

            Dim postData As String = String.Empty
            For Each k As String In nvc.Keys
                If (postData.Length > 0) Then
                    postData &= "&"
                End If
                postData &= k & "=" & nvc(k)
            Next
            Dim data As Byte() = Encoding.UTF8.GetBytes(postData)

            url = ConfigurationManager.AppSettings("PardotQuoteRequest").ToString()
            Dim httpRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpRequest.ContentType = "application/x-www-form-urlencoded"
            httpRequest.Method = "POST"

            httpRequest.ContentLength = data.Length
            Using s As IO.Stream = httpRequest.GetRequestStream()
                s.Write(data, 0, data.Length)
            End Using

            Using httpResponse As HttpWebResponse = CType(httpRequest.GetResponse(), HttpWebResponse)
                response = New IO.StreamReader(httpResponse.GetResponseStream()).ReadToEnd()
            End Using
        Catch ex As Exception
            Dim exM As String = String.Format("{0} -- {1} -- {2} -- {3}", ex.Message, ex.InnerException, ex.StackTrace, ex.Source)
            Dim toAddresses As List(Of String) = New List(Of String)
            toAddresses.Add("james.chan@teledyne.com")
            Utilities.SendEmail(toAddresses, "webmaster@teledynelecroy.com", "Exception in quote request async call", exM + " -------URL: " + url + " -------Response: " + response, New List(Of String), New List(Of String))
        End Try
    End Sub
End Class

Public Class PardotData
    Public IsPsgQuote As Boolean
    Public Partnumber As String
    Public TotalPrice As Decimal
End Class