﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Shopper_SendToDistributor" Codebehind="sendtodistributor.aspx.vb" %>
<%@ Register TagPrefix="LeCroy" TagName="quotes" Src="~/UserControls/quotes.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryBottom">
        <div class="content">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">
                        <table cellpadding="8">
                            <tr>
                                <td valign="top">
                                    <asp:Label ID="lblPreDisplay" runat="server" /><br /><br />
                                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                                        <h2><asp:Label ID="Label2" runat="server" Text="Select the distributor to forward the quote to" /></h2><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="distlist" ErrorMessage="Please select a distributor!" ValidationGroup="grDist" Font-Bold="True" ForeColor="#FF3300" />
                                        Select country:&nbsp;<asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" /><br />
                                        <asp:Button ID="btnMarkAsDuplicate" runat="server" CausesValidation="false" Text="Mark this quote as duplicate" /><br />
                                        <asp:RadioButtonList ID="distlist" runat="server" CausesValidation="True" ValidationGroup="grDist" /><br /><hr />
                                        <span>Add other CC recipients (optional):<br />
                                        <div style="border: 1px solid #a9a9a9; height:153px; overflow:auto; width:350px;">
                                            <asp:CheckBoxList ID="cblCCAddresses" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" />
                                        </div><br />
                                        <span>Other CC recipient (optional):&nbsp;<asp:TextBox ID="txtAdditionalCc" runat="server" MaxLength="200" />&nbsp;please separate emails with a comma&nbsp;<asp:CustomValidator ID="cvAdditionalCc" runat="server" CssClass="error" ValidationGroup="grDist" /></span><br />
                                        <span>Message to include:</span>
                                        <asp:Label ID="Label1" runat="server" Text="*" Font-Bold="True" ForeColor="Red" />&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="desc" ErrorMessage="Please type comments!" ValidationGroup="grDist" /><br />
                                        <asp:TextBox ID="desc" runat="server" Rows="10" TextMode="MultiLine" Width="307px" CausesValidation="True" ValidationGroup="grDist" /><br />
                                        <br /><br /><asp:Button ID="btnSend" runat="server" Text="Send Quote To Selected Distributor" /><br />
                                        <asp:Label ID="lbQuote" runat="server" />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>