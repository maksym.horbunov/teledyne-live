﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Partial Class Shopper_SendToDistributor
    Inherits BasePage
    Dim strCC As String = ""
    Dim strTo As String = ""
    Dim strBCC As String = "kate.kaplan@teledyne.com,webquote@teledynelecroy.com,james.chan@teledyne.com"
    Dim strFrom As String = "hilary.lustig@teledyne.com"
    Dim strSQL As String = ""
    Dim strCret As String = "<br />"
    Dim strSubject As String = ""
    Dim strSubjectInfo As String = ""
    Dim selecteddist As String = ""
    Dim strDesc As String = ""
    Dim bufferPre As StringBuilder = New StringBuilder()
    Dim distcustreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ContactID As String = ""
        Dim strQuoteID As Long = 0
        Dim strLocaleID As String = 1033
        Dim strDate As DateTime = Now()
        strCC = "webquote@teledynelecroy.com,hilary.lustig@teledyne.com"

        If Len(Session("distquote")) = 0 Then
            If Not String.IsNullOrEmpty(Request.QueryString("cust")) Then
                If IsNumeric(Request.QueryString("cust")) Then
                    ContactID = Request.QueryString("cust")
                    Session("distContactID") = ContactID
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("qid")) Then
                If IsNumeric(Request.QueryString("qid")) Then
                    strQuoteID = Request.QueryString("qid")
                    Session("distquote") = strQuoteID
                End If
            End If
        End If

        If Len(Session("distquote")) > 0 And Len(Session("ContactID")) > 0 Then
            'Check if Quote was not send to distrubutor yet

            Dim content As StringBuilder = New StringBuilder()
            strSQL = "select * from EMAIL_CSR where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@QUOTEID", Session("distquote").ToString()))
            Dim dsq As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsq.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsq.Tables(0).Rows
                    If String.Compare((dr("RESPONSE_FLAG")), "d") = 0 Then
                        If (String.Compare(dr("DESCRIPTION"), "Marked as duplicate", True) = 0) Then
                            bufferPre.Append("<font face=Verdana,Arial size=2 color=DodgerBlue><b>This quote was already marked as a duplicate.</b></font>")
                        Else
                            bufferPre.Append("<font face=Verdana,Arial size=2 color=DodgerBlue><b>This quote was already sent to the distributor.</b></font>")
                        End If
                    Else
                        distcustreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(Session("distquote"), True, "1033"))
                        distcustreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(Session("distquote"), 1033))
                        distcustreplacements.Add("<#QuoteSubject#>", "Teledyne LeCroy Quote Request")
                        distcustreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
                        distcustreplacements.Add("<#TClink#>", String.Empty)
                        distcustreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", Session("distquote")))
                        distcustreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", Session("distquote")))
                        distcustreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(Session("distquote"), 1033))
                        distcustreplacements.Add("<#CSRSE#>", String.Empty)
                        If Not Me.IsPostBack Then
                            'If strGeoIPLocaleId.ToString = "1041" Or strGeoIPLocaleId.ToString = "1042" Then
                            BindCountryDropDown()
                            SetListBasedOnCountrySelection()
                            BindCheckBoxList()
                        End If

                    End If
                Next
            End If
            dsq.Dispose()
            dsq = Nothing
        End If
        lblPreDisplay.Text = bufferPre.ToString()
    End Sub

    Protected Sub btnMarkAsDuplicate_Click(sender As Object, e As EventArgs) Handles btnMarkAsDuplicate.Click
        strSQL = "Update EMAIL_CSR set DESCRIPTION=@DESCRIPTION,RESPONSE_FLAG='d',SENTTO_OFFICE_ID=61 where QUOTEID=@QUOTEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DESCRIPTION", "Marked as duplicate"))
        sqlParameters.Add(New SqlParameter("@QUOTEID", Session("distquote").ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=@QUOTEID"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUOTEID", Session("distquote").ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        Panel1.Visible = False
        lblPreDisplay.Text = "Quote was marked as duplicate"
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        cvAdditionalCc.Validate()
        If Not (cvAdditionalCc.IsValid) Then Return

        selecteddist = Me.distlist.SelectedValue
        strDesc = Replace(Me.desc.Text, "'", "")
        Dim distreplacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        If Not String.IsNullOrEmpty(selecteddist) And Not String.IsNullOrEmpty(strDesc) Then
            Dim quote As Quote = QuoteRepository.GetQuote(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("distquote").ToString()))
            If (quote Is Nothing) Then Throw New ApplicationException(String.Format("Could not lookup quote record for quote {0}", Session("distquote").ToString()))
            Dim shopperQuote As ShopperQuote = ShopperQuoteRepository.GetShopperQuotes(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("distquote").ToString())).OrderBy(Function(x) x.DateEntered).FirstOrDefault()
            If (shopperQuote Is Nothing) Then Throw New ApplicationException(String.Format("Could not lookup shopperquote record for quote {0}", Session("distquote").ToString()))
            Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), quote.ContactId)
            If (contact Is Nothing) Then Throw New ApplicationException(String.Format("Could not lookup contact record for quote {0}", Session("distquote").ToString()))
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), shopperQuote.ProductId)
            If (product Is Nothing) Then Throw New ApplicationException(String.Format("Could not lookup product record for quote {0}", Session("distquote").ToString()))
            Dim contactOffice As ContactOffice = ContactOfficeRepository.GetContactOffice(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(selecteddist))
            If (contactOffice Is Nothing) Then Throw New ApplicationException(String.Format("Could not lookup contact_office record for quote {0}", Session("distquote").ToString()))

            distreplacements.Add("<#QuoteProductInfo#>", GetQuoteBody(Session("distquote"), True, "1033"))
            distreplacements.Add("<#QuoteCustomerInfo#>", GetQuoteInfo(Session("distquote"), 1033))
            distreplacements.Add("<#QuoteSubject#>", String.Format("Teledyne LeCroy Quote Request - {0}|{1}|{2}|{3}|{4}|{5}", contact.Company, contact.City, contact.StateProvince, String.Concat(contact.FirstName, " ", contact.LastName), product.PartNumber, contactOffice.Company))
            distreplacements.Add("<#OrderPlacementInfo#>", String.Empty)
            distreplacements.Add("<#TClink#>", String.Empty)
            distreplacements.Add("<#USDiscountStatement#>", GetQuoteDiscountNote("scopes", Session("distquote")))
            distreplacements.Add("<#OrderConditionsStatement#>", GetQuoteOrderConditionsStatement("dist", Session("distquote")))
            distreplacements.Add("<#ProfileInfo#>", GetQuoteProfileInfo(Session("distquote"), 1033))
            distreplacements.Add("<#CSRSE#>", String.Empty)

            'Update EMAIL_CSR TABLE	
            'get strFrom
            Dim strDMEmail As String = GetCCMS_Email("DM", GetDSMNumber(GetContactidOnQuoteId(Session("distquote"))))
            If Not String.IsNullOrEmpty(strDMEmail) Then
                strFrom = strDMEmail
            End If

            strSQL = "Update EMAIL_CSR set CSR_NAME=' ', DESCRIPTION=@DESCRIPTION,RESPONSE_FLAG='d',SENTTO_OFFICE_ID=@SENTTOOFFICEID,[TO]=@TO where QUOTEID=@QUOTEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DESCRIPTION", strDesc))
            sqlParameters.Add(New SqlParameter("@SENTTOOFFICEID", selecteddist))
            sqlParameters.Add(New SqlParameter("@TO", GetDistEmailOnOfficeID(selecteddist)))
            sqlParameters.Add(New SqlParameter("@QUOTEID", Session("distquote").ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            'Send Email To Distributor
            strTo = GetDistEmailOnOfficeID(selecteddist).ToString
            If Not String.IsNullOrEmpty(strTo) Then
                Dim ccAddresses As List(Of String) = New List(Of String)(strCC.Split(","c))
                If (cblCCAddresses.Items.Cast(Of ListItem)().Any(Function(item) item.Selected)) Then
                    For Each li As ListItem In cblCCAddresses.Items
                        If (li.Selected) Then ccAddresses.Add(li.Value)
                    Next
                End If
                If Not (String.IsNullOrEmpty(txtAdditionalCc.Text.Trim())) Then
                    Dim writtenInCC As String = txtAdditionalCc.Text.Trim()
                    If (writtenInCC.Contains(";")) Then
                        writtenInCC = writtenInCC.Replace(";", ",")
                    End If
                    If (writtenInCC.Contains(",")) Then
                        ccAddresses.AddRange(New List(Of String)(writtenInCC.Split(","c)))
                    End If
                End If
                strSQL = "Update  QUOTE set RESPONSE_FLAG='true',RESPONSE_DATE=getdate() where QUOTEID=@QUOTEID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@QUOTEID", Session("distquote").ToString()))
                DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                UpdatedQuoteSECode(Session("distquote"), GetSENumber(GetContactidOnQuoteId(Session("distquote"))))
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), 2, strFrom, New List(Of String)({strTo}), ccAddresses, New List(Of String)({strBCC}), strDesc, distreplacements)
            End If
            Panel1.Visible = False
            bufferPre.Append("Information was sent")
            lblPreDisplay.Text = bufferPre.ToString()
        End If
    End Sub

    Private Sub SetListBasedOnCountrySelection()
        Dim contactOffices As List(Of ContactOffice) = GetDistributorContactOffices()
        distlist.Items.Clear()
        distlist.ClearSelection()
        Dim filteredOffices As List(Of ContactOffice) = contactOffices.Where(Function(x) x.CountryIdWeb.Value = Int32.Parse(ddlCountry.SelectedValue)).OrderBy(Function(x) x.Company).ToList()
        For Each co In filteredOffices
            If Not (String.IsNullOrEmpty(selecteddist)) Then
                distlist.SelectedValue = selecteddist
            End If
            distlist.Items.Add(New ListItem(String.Format("<font face=Verdana,Arial,Helvetica size=2>{0} ({1}, {2})<br><nobr>&nbsp;&nbsp;&nbsp;&nbsp;<b><font color=dodgerblue>To: </font>{3}</nobr></b></font>", co.Company, co.Address1, co.Address2, co.EmailLeads), co.OfficeId.ToString()))
        Next
        bufferPre.Append("<font face=Verdana, Geneva, sans-serif size=1 color=#000000>CC: " + strCC + "</font>")
        lbQuote.Text = EmailTemplateDetailRepository.BuildEmailContent(ConfigurationManager.AppSettings("ConnectionString").ToString(), 14, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), False, distcustreplacements)
        Panel1.Visible = True
    End Sub

    Private Function GetDistributorContactOffices() As List(Of ContactOffice)
        Dim contactOffices As List(Of ContactOffice) = ContactOfficeRepository.GetContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString()).Where(Function(x) x.Region = 5 And x.WsYN.ToLower() = "y" And x.Type.ToLower() = "d" And x.ActiveYN.ToLower() = "y").ToList()
        If (contactOffices.Count <= 0) Then
            Response.Write("No Data Available")
        End If
        Return contactOffices
    End Function

    Private Sub BindCountryDropDown()
        Dim contactOffices As List(Of ContactOffice) = GetDistributorContactOffices()
        Dim countryIds As List(Of Int32) = contactOffices.Select(Function(x) x.CountryIdWeb.Value).Distinct().ToList()
        ddlCountry.Items.Clear()
        ddlCountry.DataSource = CountryRepository.GetCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), countryIds).Distinct().ToList()
        ddlCountry.DataTextField = "Name"
        ddlCountry.DataValueField = "CountryId"
        ddlCountry.DataBind()
        If (countryIds.Contains(208)) Then
            ddlCountry.SelectedValue = "208"
        Else
            ddlCountry.SelectedIndex = 0
        End If
    End Sub

    Private Sub BindCheckBoxList()
        cblCCAddresses.DataSource = FeCcmsManager.GetDistinctActiveSeAndDmEmails(ConfigurationManager.AppSettings("ConnectionString").ToString())
        cblCCAddresses.DataBind()
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        SetListBasedOnCountrySelection()
    End Sub

    Private Sub cvAdditionalCc_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvAdditionalCc.ServerValidate
        If (String.IsNullOrEmpty(txtAdditionalCc.Text)) Then
            args.IsValid = True
            Return
        End If

        Dim writtenInCC As String = txtAdditionalCc.Text.Trim()
        If (writtenInCC.Contains(";")) Then
            writtenInCC = writtenInCC.Replace(";", ",")
        End If
        If (writtenInCC.Contains(",")) Then
            Dim addresses As List(Of String) = New List(Of String)(writtenInCC.Split(","c))
            For Each cc In addresses
                If Not (Utilities.checkValidEmail(cc)) Then
                    args.IsValid = False
                    cvAdditionalCc.ErrorMessage = "*Invalid email address - either leave empty or provide a valid email"
                    Return
                End If
            Next
        Else
            If Not (Utilities.checkValidEmail(txtAdditionalCc.Text)) Then
                args.IsValid = False
                cvAdditionalCc.ErrorMessage = "*Invalid email address - either leave empty or provide a valid email"
                Return
            End If
        End If
        args.IsValid = True
    End Sub
End Class