<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.SiteMap_404" Codebehind="404.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="configureTop">
        <div class="text22" style="color: #008ecd; padding-left: 30px; padding-top: 50px;">
            Site Map.
        </div>
    </div>
    <table width="700" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
	    <asp:Literal ID="siteMap" runat="server" />
    </table>
</asp:Content>