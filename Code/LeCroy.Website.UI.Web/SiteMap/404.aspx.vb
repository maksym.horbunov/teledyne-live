Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class SiteMap_404
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - 404 Not Found"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Status = "404 Not Found"
        Session("menuSelected") = ""
        Dim sql As String = "SELECT CAPTION_ID, LOCALEID, CAPTION_NAME, URL,PROTOCOL_YN " +
        " FROM MENU_CAPTION where ENABLED='y' and expand_yn='y' and LOCALEID = @LOCALEID order by sort_id "
        Dim i As Integer
        Dim menuStr As String = "<tr>"
        Dim menuURL As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@LOCALEID", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim cls As String = ""
                menuURL = dr("URL")
                menuStr += "<td valign='top' >"
                If Not menuURL Is Nothing Then
                    If menuURL.Contains("?") Then
                        menuURL += "&capid=" + dr("CAPTION_ID").ToString()
                    Else
                        menuURL += "?capid=" + dr("CAPTION_ID").ToString()
                    End If
                End If

                menuStr += "<a href=""" + rootDir + menuURL + """><h2>" + dr("CAPTION_NAME") + "</h2></a>"

                If dr("PROTOCOL_YN").ToString() = "y" Then
                    sql = "SELECT * FROM MENU_MAIN where PROTOCOL_STANDARD_ID is NOT NULL and CAPTION_ID=@CAPTIONID and ENABLED='y' order by SORT_ID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@CAPTIONID", dr("CAPTION_ID").ToString()))
                    Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        menuStr += "<ul>"
                        For Each drr As DataRow In dss.Tables(0).Rows
                            menuURL = drr("URL")
                            If Not menuURL Is Nothing Then
                                If menuURL.Contains("?") Then
                                    menuURL += "&capid=" + dr("CAPTION_ID").ToString()
                                Else
                                    menuURL += "?capid=" + dr("CAPTION_ID").ToString()
                                End If
                            End If

                            menuStr += "<li><a href=""" + rootDir + menuURL + """>" + drr("MENU_NAME").ToString + "</a>"
                        Next
                        menuStr += "</ul>"
                    End If
                Else
                    sql = " SELECT MENU_ID, MENU_NAME, URL, DISPLAY_NEW FROM MENU_MAIN where CAPTION_ID=@CAPTIONID and ENABLED='y' order by SORT_ID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@CAPTIONID", dr("CAPTION_ID").ToString()))
                    Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        menuStr += "<ul>"
                        For Each drr As DataRow In dss.Tables(0).Rows
                            menuURL = drr("URL")
                            If Not menuURL Is Nothing Then
                                If menuURL.Contains("?") Then
                                    menuURL += "&capid=" + dr("CAPTION_ID").ToString() +
                                    "&mid=" + drr("MENU_ID").ToString()
                                Else
                                    menuURL += "?capid=" + dr("CAPTION_ID").ToString() +
                                    "&mid=" + drr("MENU_ID").ToString()
                                End If
                            End If
                            menuStr += "<li><a href=""" + rootDir + menuURL + """>" + drr("MENU_NAME").ToString + "</a>"
                        Next
                        menuStr += "</ul>"
                    End If
                End If
                menuStr += "</td>"
                If i > 0 And (i + 1) Mod 3 = 0 Then
                    menuStr += "</tr><tr>"
                End If
            Next
        End If
        menuStr += "</tr>"
        Me.siteMap.Text = menuStr
    End Sub
End Class