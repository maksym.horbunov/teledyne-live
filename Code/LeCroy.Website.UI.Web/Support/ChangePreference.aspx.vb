Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_ChangePreference
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "International Sites"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - International Sites"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim strSQL As String = "SELECT MENU_ID, MENU_NAME, URL FROM MENU_MAIN WHERE CAPTION_ID = 11 AND ENABLED = 'y' ORDER BY SORT_ID"
        Dim menuStr As String = ""
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                menuStr += "<tr><td valign='top' >"
                menuStr += "<a href='" + Me.convertToString(dr("URL")) +
                "'><h2>" + dr("MENU_NAME") + "</h2></a>"
                menuStr += "</td></tr>"
                menuStr += "<tr><td><br /></td></tr>"
            Next
        End If
        Me.internationalSites.Text = menuStr
    End Sub
End Class