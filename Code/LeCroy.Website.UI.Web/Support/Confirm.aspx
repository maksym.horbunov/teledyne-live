﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Confirm" Codebehind="Confirm.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="<%=rootDir %>/Support/TechLib/">Tech Library</a>
        <li><a href="<%=rootDir %>/Support/Service.aspx">Instrument Service</a></li>
        <li><a href="#">FAQ/Knowledgebase</a></li>
        <li><a href="<%=rootDir %>/Support/DSOsecurity.aspx">Oscilloscope Security</a></li>
        <li class="current"><a href="<%=rootDir %>/Support/Register/">Product Registration</a></li>
        <li><a href="<%=rootDir %>/Support/SoftwareDownload/">Software Downloads</a></li>
        <li><a href="<%=rootDir %>/Support/TechHelp/">Technical Help</a></li>
        <li><a href="<%=rootDir %>/Support/RoHs.aspx">RoHS & WEEE</a></li>
        <li><a href="<%=rootDir %>/Support/Training.aspx">Training</a></li>
        <li><a href="<%= rootDir %>/Support/User/userprofile.aspx">Update Profile</a></li>
        <li><a href="<%=rootDir %>/Support/Contact/">Contact Us</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_reg.gif" alt="Product Registration"></p><br/>
        <asp:Literal ID="litOne" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources"></div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>