﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Contact_Directions" Codebehind="Directions.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <p><img src="<%= rootDir%>/images/category/headers/hd_contact.gif" alt="Contact Us"></p>
        <h1>Directions to Corporate Office</h1><hr />
        <div style="text-align: right">
            <a href="http://www.mapquest.com/maps/map.adp?dtype=s&mapdata=cbPRZSKSHGSTZ0stM%2fx42nD0UOYUF9Zua%2bZ7yTF89L8AQLGoWG%2bArLFhLw437SM4jHdvuF08LGqsfOcTA1v99uOiHiy2EEOXbRvf5AI4U4YsBFMjYsw4BQ%2fD1nU%2bzw%2b3M81kzFutzlNeraH0XTSzge6AlkoSZSpfNkhrT7b1E39pAfY2MEsZinE2urwVNJYg%2fCyLsKsmk5R1jhL3tqOZdrceZU5f3OYGLCuF0uc8QWgoYQe%2b%2fSRfnt4JbnseivJnNE39GjfEdNnH%2brFSdR%2b%2bCHjsDGpkWRT5qk52ZemyzQhfU9YAS%2bOss06KneQSIotoxAyMQTJ%2f1dmj0KIBWb5%2beHNjfz533Sijo4ZDluxPXwGqSCrgDtzSQ2GXlFddEtMRqNBfFnAwOIfCQ5iNOQB7M0aO9JmgJ7XyCQRAEkNRKiH3QPRg5sW76rHTCd3jwSBxq%2byNhqmCCItBLXrTS51yb4LKToG7jnZvfHDfpp2XKrKqpCEae3kGzkJZ4XpC32AFIIZ%2fI9u04qs%3d" onclick="GaEventPush("OutboundLink", "http://www.mapquest.com/maps/map.adp?dtype=s&mapdata=cbPRZSKSHGSTZ0stM%2fx42nD0UOYUF9Zua%2bZ7yTF89L8AQLGoWG%2bArLFhLw437SM4jHdvuF08LGqsfOcTA1v99uOiHiy2EEOXbRvf5AI4U4YsBFMjYsw4BQ%2fD1nU%2bzw%2b3M81kzFutzlNeraH0XTSzge6AlkoSZSpfNkhrT7b1E39pAfY2MEsZinE2urwVNJYg%2fCyLsKsmk5R1jhL3tqOZdrceZU5f3OYGLCuF0uc8QWgoYQe%2b%2fSRfnt4JbnseivJnNE39GjfEdNnH%2brFSdR%2b%2bCHjsDGpkWRT5qk52ZemyzQhfU9YAS%2bOss06KneQSIotoxAyMQTJ%2f1dmj0KIBWb5%2beHNjfz533Sijo4ZDluxPXwGqSCrgDtzSQ2GXlFddEtMRqNBfFnAwOIfCQ5iNOQB7M0aO9JmgJ7XyCQRAEkNRKiH3QPRg5sW76rHTCd3jwSBxq%2byNhqmCCItBLXrTS51yb4LKToG7jnZvfHDfpp2XKrKqpCEae3kGzkJZ4XpC32AFIIZ%2fI9u04qs%3d");">
                <h2>Link To Map<img border="0" src="../../images/icons/icons_lft_nav_arrow.gif" width="7" height="8"></h2>
            </a>
        </div>
        <h1>TAPPAN ZEE BRIDGE</h1>
        From the Tappan Zee Bridge it will take you approximately 15 minutes to get to LeCroy Corporation. As you cross the Tappan Zee Bridge you will be on the New York State Thruway (Interstate 87& 287) headed north toward Albany. You will continue on the Thruway to Exit 14A ( Garden State Parkway). Exit 14A will put you on the Garden State Parkway south. Take the Parkway to the first exit which is Schoolhouse Road. You will exit here and at the end of the off ramp make a right onto Schoolhouse Road. You will travel approximately 0.9 miles to the traffic light (Route 45/Chestnut Ridge Road). At the traffic light make a right onto Chestnut Ridge Road and travel approximately 0.3 miles to LeCroy. LeCroy will be on your right.<br /><br />
        <h1>NEWARK AIRPORT</h1>
        From Newark Airport follow signs for Route 78 West to the sign for the Garden State Parkway North. Take the Garden State Parkway to Exit 172 (Grand Avenue). Turn left onto Grand Avenue. At the first traffic light turn right onto Chestnut Ridge Road. Proceed North on Chestnut Ridge Road from New Jersey into New York ( approx. 1.5 miles tot he New York border). Continue approx. another 1.5 miles until you see the LeCroy on the right.<br /><br />
        <h1>GARDEN STATE PARKWAY NORTH</h1>
        Follow direction from Newark Airport.<br /><br />
        <h1>JFK AIRPORT</h1>
        Proceed via the main airport exit to the Van Wyck Expressway. Stay on the Van Wyck for approx. 3.8 miles and then take the exit to the Grand Central Parkway West (toward New York). After traveling on the Grand Central Parkway for 8 miles you will pass LaGuardia Airport on your right. Cross the Triboro Bridge and continue on Route 87 which is also called the Major Deegan Expressway. Take the Major Deegan Exit to the George Washington Bridge and cross the George Washington Bridge on the upper deck. Stay on the right side as you approach the New Jersey side of the Bridge and turn onto the Palisades Interstate Parkway. Continue on the Palisades Pkwy to Exit 9 (New York State Thruway) . Continue on the Thruway to Exit 14A (Garden State Parkway). See direction above from TAPPAN ZEE BRIDGE<br /><br />
        <h1>LAGUARDIA AIRPORT</h1>
        When leaving the airport, pickup the Grand Central Parkway West toward New York and the Triboro Bridge. Cross the Triboro Bridge and continue on Route 87 which is also called the Major Deegan Expressway. See directions above from JFK Airport..<br /><br />
        <h1>FROM ROUTE 17 NORTH (NEW JERSEY)</h1>
        Take Route 17 North to the exit for Lake Street. When you exit from Route 17 North , make a right at the end of the off ramp. At the first traffic light, you will need to stay right to make a right hand turn onto Lake Street. Continue on Lake Street for several miles. Lake Street turns into Grand Avenue. At the second traffic light, make a left hand turn onto Chestnut Ridge Road. There will be a Shopping Center on your Left. Take Chestnut Ridge road into New York State. LeCroy will be on your right.<br /><br />
        <h1>FROM 287 NORTH</h1>
        Take 287 North to the end. You will follow signs for 87 South (Tappan Zee Bridge). Take 87 South to Exit 14A. Exit 14A will bring you onto the Garden State Parkway South. Exit the Garden State Parkway at the first exit (Schoolhouse Road). At the end of the off ramp make a right onto Schoolhouse Road. At the end of Schoolhouse Road, you will make a right at the traffic light onto Chestnut Ridge Road/Route 45. LeCroy will be less than a half mile on your right.
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources">
        <div class="sidenav">
            <ul>
                <li><a href="<%=rootDir %>/Support/TechHelp/">Technical Help inquiries and Support</a></li>
                <li>&nbsp;</li>
                <li><a href="/support/requestinfo/default.aspx<%= menuURL2 %>">Request Product Demos and hard copy Literature</a></li>
                <li>&nbsp;</li>
                <li><a href="mailto:webmaster@teledynelecroy.com">E-mail Webmaster</a></li>
                <li>&nbsp;</li>
                <li><a href="Directions.aspx<%= menuURL2 %>">Directions to Corporate Office</a></li>
                <li>&nbsp;</li>
                <li><a href="officelisting.aspx<%= menuURL2 %>&regionid=5&officetype=S">Direct World Sales Representation</a></li>
                <li>&nbsp;</li>
                <li><a href="officelisting.aspx<%= menuURL2 %>&regionid=5&officetype=D">LeCroy WaveJet Distributors</a></li>
                <li>&nbsp;</li>
                <li><a href="officelisting.aspx<%= menuURL2 %>&regionid=5&officetype=M">LeCroy Manufacturer Representatives</a></li>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</asp:Content>