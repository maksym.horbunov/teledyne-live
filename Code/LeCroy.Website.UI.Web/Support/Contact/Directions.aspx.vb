﻿Imports LeCroy.Library.VBUtilities

Partial Class Support_Contact_Directions
    Inherits BasePage

    Public menuURL As String = String.Empty
    Public menuURL2 As String = String.Empty

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Contact Us"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Contact Us"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.SUPPORT_CAPTION_ID
        Dim menuID As String = AppConstants.CONTACT_US_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Page.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub
End Class