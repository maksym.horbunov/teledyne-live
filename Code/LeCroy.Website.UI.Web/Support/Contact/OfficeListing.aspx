﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Contact_OfficeListing" Codebehind="OfficeListing.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
<script type="text/javascript">
<!--
    function changecountry() {
        //alert (document.mr.mrcountry.options[document.mr.mrcountry.selectedIndex].value);
        document.forms.productManuals.submit();

    }
    function changestate() {
        document.forms.productManuals.mrzip.value = ""
        document.forms.productManuals.submit();
    }
    function WSQuote() {
        window.location = '../../shopper/RequestQuote/'
    }
    function validate() {
        if (document.forms.productManuals.mrcountry.options[document.forms.productManuals.mrcountry.selectedIndex].value == "208") {
            if ((document.forms.productManuals.mrstate.options[document.forms.productManuals.mrstate.selectedIndex].value == "") && (document.forms.productManuals.mrzip.value == "")) {
                alert("Please select State or Enter a Zip Code");
                return false;
            }
        }
        document.forms.productManuals.submit();
    }
-->
</script>
    <div class="intro2">
        <div><h1>LeCroy Sales Offices</h1></div>
        <p>To contact a sales office in your area, select a LeCroy Region from the menu. Results will display at the bottom of this page.</p><br />
        <table cellspacing="0" cellpadding="0" width="370" border="0">
            <asp:Repeater ID="rptCategory" runat="server">
                <ItemTemplate>
                    <tr><%#Container.DataItem%></tr>
                    <tr><td colspan="4"><img border="0" src="<%=rootDir %>/images/spacer.gif" height=4></td></tr>
                </ItemTemplate>
            </asp:Repeater>	
        </table>
        <br/><br/>
        <asp:Label ID="lblGlobalDist" runat="server" Text="Label" Visible="false">
            <table cellspacing="0" cellpadding="0" width="80%" border=0>
                <tr><td>&nbsp;</td></tr>
                <tr><td valign=top width=300><nobr><h1>Global Distributors</h1></nobr><hr color=#dcdcdc size=1></td></tr>
                <tr><td valign=top><h2>Digi-Key Corporation</h2></td></tr>
                <tr><td valign=top><nobr>701 Brooks Avenue South</nobr></td></tr>
                <tr><td valign=top><nobr>Thief River Falls, MN 56701</nobr></td></tr>
                <tr><td valign=top><nobr>USA</nobr></td></tr>
                <tr><td valign=top><nobr><b>Phone:&nbsp;</b>1-800-344-4539</nobr></td></tr>
                <tr><td valign=top><nobr><b>Fax:&nbsp;</b>218-681-3380</nobr></td></tr>
                <tr><td valign=top CLASS="AWN"><nobr><b>Web Site:&nbsp;</b><a href="http://www.digikey.com/Suppliers/us/lecroy.page?lang=en">http://www.digikey.com/Suppliers/us/lecroy.page?lang=en</a></nobr></td></tr>
                <tr><td >&nbsp;</td></tr>
                <tr><td valign=top width=300><hr color=#dcdcdc size=1></td></tr>
                <tr><td valign=top><h2>Mouser Electronics</h2></td></tr>
                <tr><td valign=top><nobr>1000 North Main Street</nobr></td></tr>
                <tr><td valign=top><nobr>Mansfield, TX 76063-1514</nobr></td></tr>
                <tr><td valign=top><nobr><b>Phone:&nbsp;</b>1-800-346-6873</nobr></td></tr>
                <tr><td valign=top CLASS="AWN"><nobr><b>Email Sales:&nbsp;</b><a href="mailto:sales@mouser.com">sales@mouser.com</a></nobr></td></tr>
                <tr><td valign=top CLASS="AWN"><nobr><b>Web Site:&nbsp;</b><a href="http://www.mouser.com/lecroy/">http://www.mouser.com/lecroy/</a></nobr></td></tr>
                <tr><td valign=top width=300><hr color=#dcdcdc size=1></td></tr>
            </table><br /><br />
        </asp:Label>
        <asp:panel ID="plContent" runat="server" Visible="true"> 
            <asp:Label ID="lbDetails" runat="server" />
        </asp:panel>
    </div>
    <input type="hidden" id="regionid" name="regionid" value="<%=Request.QueryString("regionid")%>" />
    <input type="hidden" id="officetype" name="officetype" value="<%=Request.QueryString("officetype")%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources">
        <div class="sidenav">
            <ul>
                <li><a href="<%=rootDir %>/Support/TechHelp/">Technical Help inquiries and Support</a></li>
                <li>&nbsp;</li>
                <li><a href="/support/requestinfo/default.aspx<%=menuURL2 %>">Request Product Demos and hard copy Literature</a></li>
                <li>&nbsp;</li>
                <li><a href="mailto:webmaster@teledynelecroy.com">E-mail Webmaster</a></li>
                <li>&nbsp;</li>
                <li><a href="Directions.aspx<%=menuURL2 %>">Directions to Corporate Office</a></li>
                <li>&nbsp;</li>
                <asp:Literal ID="dynamicURL" runat="server" />
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</asp:Content>