﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Contact_OfficeListing
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Public distLink As String = ""
    Public MRLink As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim officetype As String = ""
    Dim regionID As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Contact Teledyne LeCroy"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Redirect("default.aspx")
        Dim URL As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.CONTACT_US_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Page.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If

        If Len(Request.QueryString("regionid")) > 0 Then
            If IsNumeric(Request.QueryString("regionid")) Then
                regionID = Request.QueryString("regionid")
            Else
                regionID = "5"
            End If
        Else
            regionID = "5"
        End If
        If Len(Request.QueryString("officetype")) Then
            officetype = SQLStringWithOutSingleQuotes(Request.QueryString("officetype"))
        Else
            officetype = "S"
        End If

        If regionID = "5" Then
            If officetype = "D" Then
                URL = "<li><a href=""officelisting.aspx?regionid=5&officetype=M" + menuURL + """>LeCroy Manufacturer Representatives</a></li>"
                URL += "<li>&nbsp;</li>"
                URL += "<li><a href=""officelisting.aspx?regionid=5&officetype=S" + menuURL + """>Direct World Sales Representation</a></li>"
                URL += "<li>&nbsp;</li>"
            ElseIf officetype = "M" Then
                URL = "<li><a href=""officelisting.aspx?regionid=5&officetype=S" + menuURL + """>Direct World Sales Representation</a></li>"
                URL += "<li>&nbsp;</li>"
                URL += "<li><a href=""officelisting.aspx?regionid=5&officetype=D" + menuURL + """>LeCroy Distributors</a></li>"
                URL += "<li>&nbsp;</li>"
            Else
                URL = "<li><a href=""officelisting.aspx?regionid=5&officetype=M" + menuURL + """>LeCroy Manufacturer Representatives</a></li>"
                URL += "<li>&nbsp;</li>"
                URL += "<li><a href=""officelisting.aspx?regionid=5&officetype=D" + menuURL + """>LeCroy Distributors</a></li>"
                URL += "<li>&nbsp;</li>"
            End If
        Else
            If officetype = "W" Then
                URL += "<li><a href=""officelisting.aspx?regionid=" + regionID + "&officetype=S" + menuURL + """>Direct World Sales Representation</a></li>"
                URL += "<li>&nbsp;</li>"
            Else
                URL = "<li><a href=""officelisting.aspx?regionid=" + regionID + "&officetype=W" + menuURL + """>LeCroy Manufacturer Representatives & Distributors</a></li>"
                URL += "<li>&nbsp;</li>"
            End If
        End If
        dynamicURL.Text = URL

        Dim temp As String = SQLStringWithOutSingleQuotes(Request.QueryString("mrcountry"))
        'Dim officetype As String = "S"
        'Dim regionID As String = "2"

        getCategory()

        lbDetails.Text = getDetails(officetype, regionID)
    End Sub

    Public Sub getCategory()

        Dim linkList As New ArrayList()
        Dim listItem As String = ""
        If CStr(officetype) = "W" Then
            strSQL = "SELECT DISTINCT REGION.DISPLAYNAME, REGION.REGION_ID, REGION.sortid FROM  REGION INNER JOIN   CONTACT_OFFICE ON REGION.REGION_ID = CONTACT_OFFICE.REGION WHERE CONTACT_OFFICE.WS_YN = 'y' and CONTACT_OFFICE.active_yn='y' ORDER BY REGION.sortid"
        Else
            strSQL = "SELECT DISTINCT REGION.DISPLAYNAME, REGION.REGION_ID, REGION.sortid FROM  REGION INNER JOIN   CONTACT_OFFICE ON REGION.REGION_ID = CONTACT_OFFICE.REGION WHERE ((CONTACT_OFFICE.TYPE='D' and CONTACT_OFFICE.WS_YN = 'n') or CONTACT_OFFICE.TYPE='S') and CONTACT_OFFICE.active_yn='y' ORDER BY REGION.sortid"
        End If

        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                listItem = "<td align=center><a href='OfficeListing.aspx?regionid=" + dr("REGION_ID").ToString() + "&officetype=S" + menuURL + "'>" + _
                            "<img src=""" + rootDir + "/Images/icons/icons_link.gif"" border=""0""></a></td>"
                If CStr(officetype) = "W" Then

                    listItem = listItem + "<td bgcolor=""#ffffff"" widh=120 ><nobr><a href='OfficeListing.aspx?regionid=" + _
                                          dr("REGION_ID").ToString() + "&officetype=W" + menuURL + "'><strong>" + dr("DISPLAYNAME").ToString() + "</strong></a></nobr></td>"
                Else

                    listItem = listItem + "<td bgcolor=""#ffffff"" widh=120 ><nobr><a href='OfficeListing.aspx?regionid=" + _
                               dr("REGION_ID").ToString() + "&officetype=S" + menuURL + "'><strong>" + dr("DISPLAYNAME").ToString() + "</strong></a></nobr></td>"
                End If
                linkList.Add(listItem)
            Next
        End If
        Me.rptCategory.DataSource = linkList
        Me.rptCategory.DataBind()
    End Sub

    Protected Function getDetails(ByVal officetype As String, ByVal regionID As String) As String
        Dim content As StringBuilder = New StringBuilder()

        If Len(regionID) > 0 Then
            content.Append("<table border=""0"" width=""90%"" cellspacing=""0"">")

            If officetype = "W" Then
                strSQL = "SELECT DISTINCT REGION.DISPLAYNAME, REGION.REGION_ID, REGION.sortid FROM  REGION INNER JOIN CONTACT_OFFICE ON REGION.REGION_ID = CONTACT_OFFICE.REGION WHERE CONTACT_OFFICE.WS_YN = 'y' and CONTACT_OFFICE.active_yn='y' and REGION_ID=@REGIONID"
            Else
                strSQL = "SELECT DISTINCT REGION.DISPLAYNAME, REGION.REGION_ID, REGION.sortid FROM  REGION INNER JOIN CONTACT_OFFICE ON REGION.REGION_ID = CONTACT_OFFICE.REGION WHERE  (CONTACT_OFFICE.WS_YN = 'n' or CONTACT_OFFICE.TYPE='S') and CONTACT_OFFICE.active_yn='y' and REGION_ID=@REGIONID"
            End If
            'Response.Write(strSQL)
            'Response.End()
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@REGIONID", regionID))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Dim num1 As Integer = 0
            num1 = ds.Tables(0).Rows.Count
            If num1 > 0 Then

                For i As Integer = 0 To num1 - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    'content.Append("<table cellspacing=""0"" cellpadding=""0"" border=""0"" width=""450"" >")
                    If officetype <> "S" Then
                        content.Append("<tr><td>&nbsp;</td><td colspan=2>")
                        content.Append("<table cellspacing=""0"" cellpadding=""0""  width=""350"" border=0>")
                        content.Append("<tr>")
                        content.Append("<td>&nbsp;</td><td colspan=2>")
                        If CInt(dr("Region_id")) <> "5" Then
                            content.Append("<h2>WaveSurfer, WaveJet & WaveAce Series Oscilloscopes and Arbitrary Waveform Generators</h2>")
                        End If
                        content.Append("</nobr><br /></td>")
                        content.Append("</tr>")
                        content.Append("</table>")
                        content.Append("</td></tr>")
                    End If
                    'content.Append("<tr><td>&nbsp;</td><td colspan=2>")
                    'content.Append("<br><table cellspacing=""0"" cellpadding=""0""  width=""350"" border=0>")
                    content.Append("<tr>")
                    content.Append("<td colspan=3><nobr>")
                    content.Append("<a NAME='" + dr("REGION_ID").ToString() + "'><strong>" + _
                                   dr("DISPLAYNAME").ToString() + "</strong></a>")
                    content.Append("</nobr><br /></td>")
                    content.Append("</tr>")

                    If CStr(officetype) <> "W" Then
                        If CStr(dr("Region_id").ToString()) = "5" And CStr(officetype) = "S" Then
                            content.Append("<tr>")
                            content.Append("<td width=""20"" align=right>&nbsp;&nbsp;</td>")
                            content.Append("<td valign=top colspan=2 ><br>")
                            content.Append("<table cellspacing=""0"" cellpadding=""0"" border=""0"" width=""500"">")
                            content.Append("<tr>")
                            content.Append("<td width=""80%"" >")
                            content.Append("For Sales, Repair,&nbsp; Technical Support or Calibration in United States and Canada")
                            content.Append("</td>")
                            content.Append("<td align=""left"">")
                            content.Append("<b><nobr>Call 1-800-5-LeCroy</nobr><br>")
                            content.Append("(1-800-553-2769)<br><hr>")
                            content.Append("<nobr>Fax 1-845-578-5985</nobr>")
                            content.Append("</b></td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("<HR>")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("<b>Rental Partners</b>")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("Click on the link below for a direct connection to our Rental Partners or&nbsp; send a message to a representative by clicking on our Rental Partner's Logo.")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("<br /><b>NOTE:</b>")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("Before sending a message, you may be redirected to our Registration Page if you are not registered at LeCroy web site.</td>")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")

                            strSQL = "SELECT DISTINCT RENTAL.COUNTRY_ID as COUNTRY_ID,COUNTRY.NAME AS COUNTRY FROM RENTAL " & _
            " INNER JOIN COUNTRY ON RENTAL.COUNTRY_ID = COUNTRY.COUNTRY_ID  ORDER BY COUNTRY.NAME "

                            Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
                            Dim num2 As Integer = 0
                            num2 = dss.Tables(0).Rows.Count
                            If num2 > 0 Then
                                For j As Integer = 0 To num2 - 1
                                    Dim drr As DataRow = dss.Tables(0).Rows(j)

                                    strSQL = "SELECT * from RENTAL where COUNTRY_ID=@COUNTRYID order by SORT_ID"
                                    Dim dsss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                                    sqlParameters = New List(Of SqlParameter)
                                    sqlParameters.Add(New SqlParameter("@COUNTRYID", drr("COUNTRY_ID").ToString()))
                                    Dim num3 As Integer = 0
                                    num3 = dss.Tables(0).Rows.Count
                                    If num3 > 0 Then
                                        content.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0"">")
                                        For k As Integer = 0 To num3 - 1
                                            Dim drrr As DataRow = dsss.Tables(0).Rows(k)

                                            content.Append("<tr>")

                                            content.Append("<td valign='top'><br/>")
                                            content.Append("<a href=""rental.aspx?rentid=" + drrr("RENTAL_ID").ToString() + menuURL + """ title=""Go to " + drrr("Title").ToString() + " web site"">")
                                            content.Append("<b>")
                                            content.Append(drrr("Title").ToString())
                                            content.Append("</b>")
                                            content.Append("</a>")
                                            content.Append("</td>")
                                            content.Append("<td valign='top'>")
                                            content.Append("&nbsp;&nbsp;")
                                            content.Append("</td>")
                                            content.Append("<td valign='bottom' align=center>")
                                            content.Append("<b><nobr>")
                                            content.Append("<a href=""rentaldefault.aspx?rentalid=" + drrr("Rental_ID").ToString() + menuURL + """>")
                                            content.Append("Send Message")
                                            content.Append("</a>")
                                            content.Append("</nobr></b>")
                                            content.Append("</td>")
                                            content.Append("<td valign='top'>")
                                            content.Append("&nbsp;&nbsp;")
                                            content.Append("</td>")
                                            content.Append("</tr>")
                                        Next
                                        content.Append("</table>")
                                    End If
                                Next
                            End If

                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("<tr>")
                            content.Append("<td colspan=2>")
                            content.Append("<HR>")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("</table>")
                            content.Append("</td>")
                            content.Append("</tr>")
                        Else
                            'If CInt(regionID) = 5 And CStr(officetype) = "D" Then
                            '    content.Append("</table>")
                            'End If
                        End If
                    Else
                        If CInt(dr("Region_id").ToString()) <> 3 And CInt(dr("Region_id").ToString()) <> 5 Then

                            If Session("WSPriceRequest") Is Nothing Then
                                content.Append("<tr bgcolor=""#ffffff"">")
                                content.Append("<td>&nbsp;</td><td valign=top  align=center colspan=2>")
                                content.Append("<br /><INPUT type= button  value=""Request Quote"" onclick=WSQuote(); >")
                                content.Append("</a>")
                                content.Append("</td>")
                                content.Append("</tr>")
                            End If
                            content.Append("<tr>")
                            content.Append("<td>&nbsp;</td><td valign=top colspan=2><br>")
                            content.Append("<table cellspacing=""3"" cellpadding=""0"" border=""0"" width=""100%"" bgcolor=""white"" >")
                            content.Append("<tr>")
                            content.Append("<td valign=top width=""400"" >")
                            content.Append("")
                            content.Append("Click on the button for regional price information on LeCroy WaveSurfer, WaveJet and WaveAce Series.")
                            content.Append(" If you are not a LeCroy web site registered user you will be asked to complete a short registration form.<br>")
                            content.Append("")
                            content.Append("</td>")
                            content.Append("</tr>")
                            content.Append("</table>")
                            content.Append("</td></tr>")
                            content.Append("<tr><td>&nbsp;</td><td colspan=2><br>")
                            content.Append("</td>")
                            content.Append("</tr>")

                        Else

                        End If
                    End If

                    If CInt(dr("Region_id").ToString()) = 5 And (CStr(officetype) = "M" Or CStr(officetype) = "D") Then
                        content.Append(getWhereToBuyUS(officetype))
                    ElseIf CStr(officetype) = "W" And CInt(dr("Region_id").ToString()) = 2 Then
                        content.Append(getWhereToBuyEurope(officetype))
                    Else
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@REGIONID", dr("Region_id").ToString()))
                        If CStr(officetype) = "W" Then
                            If dr("Region_id").ToString() = "1" Then
                                strSQL = "SELECT * FROM CONTACT_OFFICE WHERE ((REGION = @REGIONID and WS_YN='y') or " +
                                      "(REGION = @REGIONID and TYPE='D' and WS_YN='n' and country not like '%protocol%')) and active_yn='y' ORDER BY sortid"
                                lblGlobalDist.Visible = True
                            ElseIf dr("Region_id").ToString() = "3" Or dr("Region_id").ToString() = "5" Then
                                strSQL = "SELECT * FROM CONTACT_OFFICE WHERE REGION = @REGIONID and WS_YN='y' and active_yn='y' ORDER BY sortid"
                                lblGlobalDist.Visible = True
                            Else
                                strSQL = "SELECT * FROM CONTACT_OFFICE WHERE REGION = @REGIONID AND type='S' and WS_YN='y' and active_yn='y' ORDER BY sortid"
                            End If
                        Else
                            strSQL = "SELECT * FROM CONTACT_OFFICE WHERE REGION = @REGIONID AND type='S' and active_yn='y' ORDER BY sortid"
                        End If
                        ' Response.Write(strSQL & "<br>")
                        Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        Dim num2 As Integer = 0
                        num2 = dss.Tables(0).Rows.Count

                        If num2 > 0 Then
                            If CheckDistributor(dr("REGION_ID").ToString()) And officetype <> "W" And dr("REGION_ID").ToString() = "2" Then
                                content.Append("<tr><td valign=top><br/><h1>Direct Sales Offices</h1></td></tr>")
                            End If
                            content.Append("<table cellspacing=""0"" cellpadding=""0"" width=""80%"" border=0>")

                            For j As Integer = 0 To num2 - 1

                                Dim drr As DataRow = dss.Tables(0).Rows(j)

                                content.Append("<tr>")
                                content.Append("<td >&nbsp;</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top width=300><nobr>")
                                content.Append(" <h2>")
                                'If drr("COUNTRY").ToString().IndexOf("<br>") > 0 Then
                                'content.Append(Mid(drr("COUNTRY"), 1, InStr(1, drr("COUNTRY"), "<br>") - 1))
                                'content.Append(drr("COUNTRY").ToString().Substring(0, drr("COUNTRY").ToString().IndexOf("<br>")))
                                'Else
                                content.Append(drr("COUNTRY"))
                                'End If
                                content.Append("</h2></nobr>")
                                content.Append("<hr color=#dcdcdc size=1>")
                                content.Append("</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top>")
                                content.Append("" + drr("COMPANY").ToString() + "")
                                content.Append("</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top><nobr>")
                                content.Append("" + drr("ADDRESS1").ToString() + "")
                                content.Append("</nobr></td>")
                                content.Append("</tr>")
                                If drr("ADDRESS2").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS2").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS3").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS3").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS4").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS4").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS5").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS5").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("PHONE").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("")
                                    If dr("Region_id").ToString() = "2" Then
                                        content.Append("<b>Phone Sales:&nbsp;</b>")
                                    Else
                                        content.Append("<b>Phone:&nbsp;</b>")
                                    End If
                                    content.Append(drr("PHONE") + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("FAX").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("")
                                    If CInt(dr("Region_id")).ToString() = "2" Then
                                        content.Append("<b>Fax Sales:&nbsp;</b>")
                                    Else
                                        content.Append("<b>Fax:&nbsp;</b>")
                                    End If
                                    content.Append(drr("FAX") + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("SALES_FAX").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("<b>Fax(Sales&Service):&nbsp;</b>" + drr("SALES_FAX").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("EMAIL").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN""><nobr>")
                                    content.Append("<b>Email Sales:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""mailto:" + drr("EMAIL").ToString() + """>" + drr("EMAIL").ToString() + "</a>")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("WEB").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN""><nobr>")
                                    content.Append("<b>Web Site:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""" + drr("WEB").ToString() + """>" + drr("WEB").ToString() + "</a></nobr>")
                                    content.Append("</td>")
                                    content.Append("</tr>")
                                End If
                                If drr("PHONE_SERVICE").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><br><nobr>")
                                    content.Append("<b>Phone Service:&nbsp;</b>" + drr("PHONE_SERVICE").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("EMAIL_SERVICE").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN""><nobr>")
                                    content.Append("<b>Email Service:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""mailto:" + drr("EMAIL_SERVICE").ToString() + """>" + drr("EMAIL_SERVICE").ToString() + "</a>")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("PHONE_SUPPORT").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><br><nobr>")
                                    content.Append("<b>Phone Support:&nbsp;</b>" + drr("PHONE_SUPPORT").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If

                                If drr("EMAIL_SUPPORT").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN""><nobr>")
                                    content.Append("<b>Email Support:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""mailto:" + drr("EMAIL_SUPPORT").ToString() + """>" + drr("EMAIL_SUPPORT").ToString() + "</a>")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                            Next
                            If officetype = "M" Then
                                content.Append("</table>")
                            End If
                        End If

                    End If 'line495

                    If officetype <> "W" Then

                        strSQL = "SELECT * FROM CONTACT_OFFICE WHERE REGION = @REGIONID AND type='D' and WS_YN='n' and active_yn='y' ORDER BY sortid"
                        ' Response.Write(strSQL & "<br>")
                        Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@REGIONID", dr("REGION_ID").ToString()))
                        Dim num2 As Integer = 0
                        num2 = dss.Tables(0).Rows.Count

                        If num2 > 0 Then
                            If CheckDistributor(dr("REGION_ID").ToString()) Then
                                content.Append("<tr><td valign=top><br/><br/><h1>Distributors</h1><br><h2>For all countries that are not listed below, please contact<br><a href=""#TopofPage"">LeCroy Headquarters International Sales in Switzerland</a></h2></td></tr>")
                            End If
                            content.Append("<table cellspacing=""0"" cellpadding=""0"" width=""80%"" border=0>")

                            For j As Integer = 0 To num2 - 1

                                Dim drr As DataRow = dss.Tables(0).Rows(j)
                                content.Append("<tr>")
                                content.Append("<td>&nbsp;</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top><nobr>")
                                content.Append("<b>" + drr("COUNTRY").ToString() + "</b></nobr>")
                                content.Append("<hr color=#dcdcdc size=1>")
                                content.Append("</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top>")
                                content.Append("" + drr("COMPANY").ToString() + "")
                                content.Append("</td>")
                                content.Append("</tr>")
                                content.Append("<tr>")
                                content.Append("<td valign=top><nobr>")
                                content.Append("" + drr("ADDRESS1").ToString() + "")
                                content.Append("</nobr></td>")
                                content.Append("</tr>")
                                If drr("ADDRESS2").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS2").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS3").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS3").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS4").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS4").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("ADDRESS5").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("" + drr("ADDRESS5").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("PHONE").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top>")
                                    content.Append("<b>Phone:&nbsp;</b>" + drr("PHONE").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("FAX").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top><nobr>")
                                    content.Append("<b>Fax:&nbsp;</b>" + drr("FAX").ToString() + "")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("EMAIL").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN""><nobr>")
                                    content.Append("<b>Email:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""mailto:" + drr("EMAIL").ToString() + """>" + drr("EMAIL").ToString() + "</a>")
                                    content.Append("</nobr></td>")
                                    content.Append("</tr>")
                                End If
                                If drr("WEB").ToString().Trim().Length > 0 Then
                                    content.Append("<tr>")
                                    content.Append("<td valign=top CLASS=""AWN"">")
                                    content.Append("<b>Web Site:&nbsp;</b>")
                                    content.Append("")
                                    content.Append("<a href=""" + drr("WEB").ToString() + """>" + drr("WEB").ToString() + "</a><br><br>")
                                    content.Append("</td>")
                                    content.Append("</tr>")
                                End If
                            Next

                            content.Append("</table>")

                        Else
                            If CStr(officetype) <> "M" And Right(content.ToString(), 8) <> "</table>" Then
                                content.Append("</table>")
                            End If
                        End If
                    Else

                    End If
                    If officetype = "W" Then
                        content.Append("</td>")
                        content.Append("</tr>")
                    End If
                Next

                If officetype = "W" Then
                    content.Append("</table>")
                End If
            End If

            content.Append("<br /><br />")
            Return content.ToString()
        End If
    End Function

    Protected Function getWhereToBuyUS(ByVal officetype As String) As String
        Dim content As StringBuilder = New StringBuilder()

        Dim mrcountry As String = ""
        Dim mrstate As String = ""
        Dim mrzip As String = ""
        lblGlobalDist.Visible = True
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If CStr(officetype) = "M" Then
            If Len(Request.QueryString("mrcountry")) > 0 Then
                mrcountry = Request.QueryString("mrcountry")
            End If
            If Len(Request.QueryString("mrstate")) > 0 Then
                mrstate = Request.QueryString("mrstate")
            End If
            If Len(Request.QueryString("mrzip")) > 0 Then
                mrzip = Request.QueryString("mrzip")
            End If
            If Len(mrzip) > 0 Then
                Dim ds As DataSet
                strSQL = "SELECT CCMS_MAP_US.STATE FROM CCMS_MAP_US WHERE CCMS_MAP_US.ZIPCode = @ZIPCODE and State is not null"
                sqlParameters.Add(New SqlParameter("@ZIPCODE", mrzip))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                Dim num1 As Integer = 0
                num1 = ds.Tables(0).Rows.Count
                If num1 > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    mrstate = dr("State").ToString()
                End If
            End If

            content.Append("<table cellspacing=""0"" cellpadding=""0"" width=""400"" border=0>")
            content.Append("<tr>")
            content.Append("<td valign=top><nobr>")
            content.Append("<h2>LeCroy Manufacturer Representatives</h2><br/>")
            content.Append("</nobr></td></tr></table>")
            content.Append("<table cellspacing=""0"" cellpadding=""0"" width=""400"" border=0>")



            strSQL = " SELECT DISTINCT COUNTRY.NAME, COUNTRY.COUNTRY_ID FROM CONTACT_OFFICE INNER JOIN " +
                   " COUNTRY ON CONTACT_OFFICE.COUNTRY_ID_WEB = COUNTRY.COUNTRY_ID " +
                   " WHERE CONTACT_OFFICE.REGION = 5 AND CONTACT_OFFICE.WS_YN = 'y' and CONTACT_OFFICE.active_yn='y' and ((COUNTRY_ID_WEB=208 and TYPE='M') or (COUNTRY_ID_WEB<>208 and TYPE<>'M'))  ORDER BY COUNTRY.NAME"
            Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            Dim num2 As Integer = 0
            num2 = dss.Tables(0).Rows.Count
            If num2 > 0 Then
                content.Append("<tr>")
                content.Append("<td valign=top><nobr>")
                content.Append("Select Country")
                content.Append("</nobr></td>")

                'If Len(mrcountry) > 0 Then
                '    If mrcountry = "208" Then
                '        content.Append("<td valign=top><nobr>")
                '        content.Append("Select State")
                '        content.Append("</nobr></td>")
                '        content.Append("<td valign=top><nobr>")
                '        content.Append("Enter Zip Code")
                '        content.Append("</nobr></td>")
                ' content.Append("<td valign=top>&nbsp;</td>")
                '    End If
                'End If
                content.Append("</tr>")
                content.Append("<tr>")
                content.Append("<td height=""25"">")
                content.Append("<select name=mrcountry id=mrcountry style=""border: 1 outset #6082FF"" onchange=changecountry()>")
                If Len(mrcountry) = 0 Then
                    content.Append("<option Selected  value=''>Select Country")
                End If

                For j As Integer = 0 To num2 - 1
                    Dim drr As DataRow = dss.Tables(0).Rows(j)
                    If Len(mrcountry) > 0 Then
                        If mrcountry = drr("COUNTRY_ID").ToString() Then
                            content.Append("<option Selected  value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                        Else
                            content.Append("<option value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                        End If
                    Else
                        content.Append("<option value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                    End If
                Next
                content.Append("</select>")
                content.Append("</td>")

                'If Len(mrcountry) > 0 Then
                '    If mrcountry = "208" Then
                '        content.Append("<td><nobr>")
                '        content.Append("<select name=mrstate id=mrstate style=""border: 1 outset #6082FF""  onchange=changestate()>")
                '        If mrstate.Length = 0 Then
                '            content.Append("<option VALUE=''>Select State")
                '        End If

                '        Dim dsss As DataSet
                '        strSQL = "Select * from STATE order by NAME"
                '        dsss = DbHelperSQL.Query(strSQL)
                '        Dim num3 As Integer = 0
                '        num3 = dsss.Tables(0).Rows.Count
                '        If num3 > 0 Then
                '            For k As Integer = 0 To num3 - 1
                '                Dim drrr As DataRow = dsss.Tables(0).Rows(k)
                '                If mrstate.Length > 0 Then
                '                    If mrstate.ToUpper() = drrr("SHORT_NAME").ToString().ToUpper() Then
                '                        content.Append("<option VALUE=""" + drrr("SHORT_NAME").ToString() + """ SELECTED>" + drrr("NAME").ToString())
                '                    Else
                '                        content.Append("<option VALUE=""" + drrr("SHORT_NAME").ToString() + """>" + drrr("NAME").ToString())
                '                    End If
                '                Else
                '                    content.Append("<option VALUE=""" + drrr("SHORT_NAME").ToString() + """>" + drrr("NAME").ToString())
                '                End If

                '            Next
                '        End If
                '        content.Append("")
                '        content.Append("</nobr></td>")
                '        content.Append("<td ><nobr>")
                '        content.Append("")
                '        content.Append("<input type=""text"" name=mrzip id=mrzip size=""10"" maxlength=""5"" value='" + mrzip + "' style=""border: 1 outset #6082FF"">")
                '        content.Append("")
                '        content.Append("<td valign=top><nobr>")
                '        content.Append("<input type=""button"" name=btnsubmit id=btnsubmit value='Go' style=""background-color: #dcdcdc; color: #000000;font-weight: bold; border-style: outset; width:35px;height=20px"" onclick=validate();>")
                '        content.Append("</nobr></td>")
                '    End If
                'End If
                content.Append("</tr>")
            End If

            content.Append("</table>")
        Else
            content.Append("<h2>WaveSurfer, WaveJet & WaveAce Series Oscilloscopes and Arbitrary Waveform Generators Distributors</h2>")
        End If

        Dim flgGo As String = ""
        Dim strMRSearch As String = ""
        If Len(mrcountry) > 0 Then
            sqlParameters = New List(Of SqlParameter)
            Select Case mrcountry
                Case 0
                    strMRSearch = "SELECT * FROM CONTACT_OFFICE WHERE REGION = 5 and WS_YN='y' and ((COUNTRY_ID_WEB=208 and TYPE='M') or (COUNTRY_ID_WEB<>208 and TYPE<>'M')) and active_yn='y'  ORDER BY sortid"
                Case 208
                    strMRSearch = "SELECT * FROM CONTACT_OFFICE WHERE REGION = 5 and WS_YN='y' and (COUNTRY_ID_WEB=208 and TYPE='M')  and active_yn='y'  ORDER BY sortid"

                'If Len(mrstate) > 0 Or Len(mrzip) > 0 Then
                '    If Len(mrzip) > 0 Then
                '        Dim parameters As SqlParameter
                '        parameters = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                '        parameters.Value = mrzip
                '        reader1 = DbHelperSQL.RunProcedure("getMRsbyZip", New IDataParameter() {parameters})
                '    Else
                '        If Len(mrstate) Then
                '            Dim parameters As SqlParameter
                '            parameters = New SqlParameter("@STATE", SqlDbType.Char, 2)
                '            parameters.Value = mrstate
                '            reader1 = DbHelperSQL.RunProcedure("getMRsbyState", New IDataParameter() {parameters})
                '        End If
                '    End If
                'Else
                '    flgGo = "no"
                'End If
                Case Else
                    strMRSearch = "SELECT * FROM CONTACT_OFFICE WHERE REGION = 5 and WS_YN='y' and COUNTRY_ID_WEB=@COUNTRYWEBID and active_yn='y' ORDER BY sortid"
                    sqlParameters.Add(New SqlParameter("@COUNTRYWEBID", mrcountry))
            End Select
        ElseIf CStr(officetype) = "D" Then
            strMRSearch = "SELECT * FROM CONTACT_OFFICE WHERE REGION = 5 and WS_YN='y' and TYPE='D' and active_yn='y'  ORDER BY sortid"
        Else
            flgGo = "no"
        End If
        ' Response.Write(strMRSearch)
        ' Response.End()
        Dim contactOffices As List(Of ContactOffice) = New List(Of ContactOffice)
        If strMRSearch.Length > 0 Then
            contactOffices = ContactOfficeRepository.FetchContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString(), strMRSearch, sqlParameters.ToArray())
        End If

        If Len(flgGo) = 0 Then
            If (contactOffices.Count > 0) Then
                content.Append("<table cellspacing=""0"" cellpadding=""0""  border=0>")
                For Each co In contactOffices
                    content.Append("<tr><td><br><br></td></tr>")
                    content.AppendFormat("<tr><td valign=top><h2>{0}</h2><nobr><hr color=#aaaaaa size=1></td></tr>", co.Country)
                    content.AppendFormat("<tr><td valign=top>{0}</td></tr>", co.Company)
                    content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address1)
                    If co.Address2.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address2)
                    End If
                    If co.Address3.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address3)
                    End If
                    If co.Address4.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address4)
                    End If
                    If co.Address5.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address5)
                    End If
                    If co.Phone.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr><b>Phone: &nbsp;</b>{0}</nobr></td></tr>", co.Phone)
                    End If
                    If co.Fax.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr><b>Fax:&nbsp;</b>{0}</nobr></td></tr>", co.Fax)
                    End If
                    If co.SalesFax.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><nobr><b>Fax(Sales&Service):&nbsp;</b>{0}</nobr></td></tr>", co.SalesFax)
                    End If
                    If co.Email.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Sales:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.Email)
                    End If
                    If co.Web.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Web Site:&nbsp;</b><a href=""{0}"">{0}</a></nobr></td></tr>", co.Web)
                    End If
                    If co.PhoneService.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><br><nobr><b>Phone Service:&nbsp;</b>{0}</nobr></td></tr>", co.PhoneService)
                    End If
                    If co.EmailService.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Service:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.EmailService)
                    End If
                    If co.PhoneSupport.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top><br><nobr><b>Phone Support:&nbsp;</b>{0}</nobr></td></tr>", co.PhoneSupport)
                    End If
                    If co.EmailSupport.Trim().Length > 0 Then
                        content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Support:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.EmailSupport)
                    End If
                Next
                content.Append("</table>")
            End If
        End If
        Return content.ToString()
    End Function

    Protected Function getWhereToBuyEurope(ByVal officetype As String) As String
        Dim content As StringBuilder = New StringBuilder()

        Dim mrcountry As String = ""
        Dim mrstate As String = ""
        Dim mrzip As String = ""
        If Len(Request.QueryString("mrcountry")) > 0 Then
            mrcountry = Request.QueryString("mrcountry")
        End If

        lblGlobalDist.Visible = True


        strSQL = " Select DISTINCT COUNTRY.NAME, Country.COUNTRY_ID FROM CONTACT_OFFICE INNER JOIN " + _
               " COUNTRY ON CONTACT_OFFICE.COUNTRY_ID_WEB = COUNTRY.COUNTRY_ID " + _
               " WHERE ((CONTACT_OFFICE.REGION = 2 And CONTACT_OFFICE.WS_YN = 'y') or (CONTACT_OFFICE.REGION = 2 and CONTACT_OFFICE.TYPE='D' and CONTACT_OFFICE.WS_YN='n' and country not like '%protocol%'))and CONTACT_OFFICE.active_yn='y' ORDER BY COUNTRY.NAME"
                        'Response.Write(strSQL)
                        'Response.End()
                        Dim dss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        Dim num2 As Integer = 0
        num2 = dss.Tables(0).Rows.Count
        content.Append("<table cellspacing=""0"" cellpadding=""0"" width=""400"" border=0>")

        If num2 > 0 Then
            content.Append("<tr>")
            content.Append("<td valign=top><nobr>")
            content.Append("<strong>Select Country</strong>")
            content.Append("</nobr></td>")
            content.Append("</tr>")
            content.Append("<tr>")
            content.Append("<td height=""25"">")
            content.Append("<select name=mrcountry id=mrcountry style=""border: 1 outset #6082FF"" onchange=changecountry()>")
            If Len(mrcountry) = 0 Then
                content.Append("<option Selected  value=''>Select Country")
            Else
                content.Append("<option Selected  value=''>All")
            End If

            For j As Integer = 0 To num2 - 1
                Dim drr As DataRow = dss.Tables(0).Rows(j)
                If Len(mrcountry) > 0 Then
                    If mrcountry = drr("COUNTRY_ID").ToString() Then
                        content.Append("<option Selected  value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                    Else
                        content.Append("<option value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                    End If
                Else
                    content.Append("<option value='" + drr("COUNTRY_ID").ToString() + "'>" + drr("NAME").ToString())
                End If
            Next
            content.Append("</select>")
            content.Append("</td>")
            content.Append("</tr>")
        End If

        content.Append("</table>")

        Dim flgGo As String = ""
        Dim strMRSearch As String = ""
        'Response.Write(mrcountry)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(mrcountry) > 0 Then
            strSQL = " SELECT CONTACT_OFFICE.* FROM CONTACT_OFFICE INNER JOIN " +
                    " COUNTRY ON CONTACT_OFFICE.COUNTRY_ID_WEB = COUNTRY.COUNTRY_ID " +
                    " WHERE ((CONTACT_OFFICE.REGION = 2 AND CONTACT_OFFICE.WS_YN = 'y') or (CONTACT_OFFICE.REGION = 2 and CONTACT_OFFICE.TYPE='D' and CONTACT_OFFICE.WS_YN='n' and country not like '%protocol%'))and CONTACT_OFFICE.active_yn='y' and COUNTRY.COUNTRY_ID=@COUNTRYID ORDER BY COUNTRY.NAME"
            sqlParameters.Add(New SqlParameter("@COUNTRYID", mrcountry))
        Else
            strSQL = " SELECT CONTACT_OFFICE.* FROM CONTACT_OFFICE INNER JOIN " + _
                    " COUNTRY ON CONTACT_OFFICE.COUNTRY_ID_WEB = COUNTRY.COUNTRY_ID " + _
                    " WHERE ((CONTACT_OFFICE.REGION = 2 AND CONTACT_OFFICE.WS_YN = 'y') or (CONTACT_OFFICE.REGION = 2 and CONTACT_OFFICE.TYPE='D' and CONTACT_OFFICE.WS_YN='n' and country not like '%protocol%'))and CONTACT_OFFICE.active_yn='y' ORDER BY COUNTRY.NAME"
        End If
        ' Response.Write(strMRSearch)
        ' Response.End()
        Dim contactOffices As List(Of ContactOffice) = New List(Of ContactOffice)
        If strSQL.Length > 0 Then
            contactOffices = ContactOfficeRepository.FetchContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If

        If (contactOffices.Count > 0) Then
            content.Append("<table cellspacing=""0"" cellpadding=""0""  border=0>")
            For Each co In contactOffices
                content.Append("<tr><td><br><br></td></tr>")
                content.AppendFormat("<tr><td valign=top><h2>{0}</h2><nobr><hr color=#aaaaaa size=1></td></tr>", co.Country)
                content.AppendFormat("<tr><td valign=top>{0}</td></tr>", co.Company)
                content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address1)
                If co.Address2.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address2)
                End If
                If co.Address3.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address3)
                End If
                If co.Address4.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address4)
                End If
                If co.Address5.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr>{0}</nobr></td></tr>", co.Address5)
                End If
                If co.Phone.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr><b>Phone: &nbsp;</b>{0}</nobr></td></tr>", co.Phone)
                End If
                If co.Fax.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr><b>Fax:&nbsp;</b>{0}</nobr></td></tr>", co.Fax)
                End If
                If co.SalesFax.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><nobr><b>Fax(Sales&Service):&nbsp;</b>{0}</nobr></td></tr>", co.SalesFax)
                End If
                If co.Email.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Sales:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.Email)
                End If
                If co.Web.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Web Site:&nbsp;</b><a href=""{0}"">{0}</a></nobr></td></tr>", co.Web)
                End If
                If co.PhoneService.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><br><nobr><b>Phone Service:&nbsp;</b>{0}</nobr></td></tr>", co.PhoneService)
                End If
                If co.EmailService.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Service:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.EmailService)
                End If
                If co.PhoneSupport.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top><br><nobr><b>Phone Support:&nbsp;</b>{0}</nobr></td></tr>", co.PhoneSupport)
                End If
                If co.EmailSupport.Trim().Length > 0 Then
                    content.AppendFormat("<tr><td valign=top CLASS=""AWN""><nobr><b>Email Support:&nbsp;</b><a href=""mailto:{0}"">{0}</a></nobr></td></tr>", co.EmailSupport)
                End If
            Next
            content.Append("</table>")
        End If
        getWhereToBuyEurope = content.ToString()
    End Function

    Protected Function CheckDistributor(ByVal regionID As String) As Boolean

        Dim rlt As Boolean = False

        strSQL = "SELECT * FROM CONTACT_OFFICE WHERE REGION = @REGIONID AND type='D' and active_yn='y' "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@REGIONID", regionID))
        Dim dssss As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim num4 As Integer = 0
        num4 = dssss.Tables(0).Rows.Count
        If num4 > 0 Then
            rlt = True
        Else
            rlt = False
        End If

        CheckDistributor = rlt

    End Function
End Class