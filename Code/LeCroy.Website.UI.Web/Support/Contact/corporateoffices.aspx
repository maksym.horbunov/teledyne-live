﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MasterPage.master" CodeBehind="corporateoffices.aspx.vb" Inherits="LeCroy.Website.Support_Contact_corporateoffices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td class="contentPadding" rowspan="2" width="7">&nbsp;</td>
                <td valign="top">
                    <div>
                        <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" />
                    </div>
                </td>
                <td class="contentRight" rowspan="2" valign="top"></td>
                <td class="contentPadding" rowspan="2" width="7">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top">
                    <div>
                        <asp:Repeater ID="rptContactOffices" runat="server">
                            <HeaderTemplate>
                                <table border="0" cellpadding="0" cellspacing="0">
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr>
                                        <td>
                                            <h3><asp:Literal ID="litOfficeName" runat="server" /></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Literal ID="litAddress1" runat="server" Visible="false" />
                                            <asp:Literal ID="litAddress2" runat="server" Visible="false" />
                                            <asp:Literal ID="litAddress3" runat="server" Visible="false" />
                                            <asp:Literal ID="litAddress4" runat="server" Visible="false" />
                                            <asp:Literal ID="litAddress5" runat="server" Visible="false" />
                                            <asp:Literal ID="litPhone" runat="server" Visible="false" />
                                            <asp:Literal ID="litFax" runat="server" Visible="false" />
                                            <asp:Literal ID="litFaxSales" runat="server" Visible="false" />
                                            <asp:Literal ID="litFaxService" runat="server" Visible="false" />
                                            <asp:Literal ID="litEmailSales" runat="server" Visible="false" /><asp:PlaceHolder ID="phEmailSales" runat="server" /><span id="spanBreak1" runat="server" visible="false"><br /></span>
                                            <asp:Literal ID="litEmailService" runat="server" Visible="false" /><asp:PlaceHolder ID="phEmailService" runat="server" /><span id="spanBreak2" runat="server" visible="false"><br /></span>
                                            <asp:Literal ID="litEmailSupport" runat="server" Visible="false" /><asp:PlaceHolder ID="phEmailSupport" runat="server" /><span id="spanBreak3" runat="server" visible="false"><br /></span>
                                            <asp:Literal ID="litWebsite" runat="server" Visible="false" /><asp:HyperLink ID="hlkWebsite" runat="server" Visible="false" />
                                            <asp:Literal ID="litPhoneService" runat="server" Visible="false" />
                                            <asp:Literal ID="litPhoneSupport" runat="server" Visible="false" />
                                        </td>
                                    </tr>
                            </ItemTemplate>
                            <SeparatorTemplate>
                                    <tr><td><br /></td></tr>
                            </SeparatorTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>