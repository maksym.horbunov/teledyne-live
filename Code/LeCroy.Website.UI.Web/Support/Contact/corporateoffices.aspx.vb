﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common

Public Class Support_Contact_corporateoffices
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindPage(Nothing, True)
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub ddlCountries_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCountries.SelectedIndexChanged
        BindPage(ddlCountries.SelectedValue, False)
    End Sub

    Private Sub rptContactOffices_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptContactOffices.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ContactOffice = CType(e.Item.DataItem, ContactOffice)
                Dim litOfficeName As Literal = CType(e.Item.FindControl("litOfficeName"), Literal)
                Dim litAddress1 As Literal = CType(e.Item.FindControl("litAddress1"), Literal)
                Dim litAddress2 As Literal = CType(e.Item.FindControl("litAddress2"), Literal)
                Dim litAddress3 As Literal = CType(e.Item.FindControl("litAddress3"), Literal)
                Dim litAddress4 As Literal = CType(e.Item.FindControl("litAddress4"), Literal)
                Dim litAddress5 As Literal = CType(e.Item.FindControl("litAddress5"), Literal)
                Dim litPhone As Literal = CType(e.Item.FindControl("litPhone"), Literal)
                Dim litFax As Literal = CType(e.Item.FindControl("litFax"), Literal)
                Dim litFaxSales As Literal = CType(e.Item.FindControl("litFaxSales"), Literal)
                Dim litFaxService As Literal = CType(e.Item.FindControl("litFaxService"), Literal)
                Dim litEmailSales As Literal = CType(e.Item.FindControl("litEmailSales"), Literal)
                Dim phEmailSales As PlaceHolder = CType(e.Item.FindControl("phEmailSales"), PlaceHolder)
                Dim spanBreak1 As HtmlGenericControl = CType(e.Item.FindControl("spanBreak1"), HtmlGenericControl)
                Dim litEmailService As Literal = CType(e.Item.FindControl("litEmailService"), Literal)
                Dim phEmailService As PlaceHolder = CType(e.Item.FindControl("phEmailService"), PlaceHolder)
                Dim spanBreak2 As HtmlGenericControl = CType(e.Item.FindControl("spanBreak2"), HtmlGenericControl)
                Dim litEmailSupport As Literal = CType(e.Item.FindControl("litEmailSupport"), Literal)
                Dim phEmailSupport As PlaceHolder = CType(e.Item.FindControl("phEmailSupport"), PlaceHolder)
                Dim spanBreak3 As HtmlGenericControl = CType(e.Item.FindControl("spanBreak3"), HtmlGenericControl)
                Dim litWebsite As Literal = CType(e.Item.FindControl("litWebsite"), Literal)
                Dim hlkWebsite As HyperLink = CType(e.Item.FindControl("hlkWebsite"), HyperLink)
                Dim litPhoneService As Literal = CType(e.Item.FindControl("litPhoneService"), Literal)
                Dim litPhoneSupport As Literal = CType(e.Item.FindControl("litPhoneSupport"), Literal)

                litOfficeName.Text = row.Company
                If Not (String.IsNullOrEmpty(row.Address1)) Then
                    litAddress1.Visible = True
                    litAddress1.Text = String.Format("{0}<br />", row.Address1)
                End If
                If Not (String.IsNullOrEmpty(row.Address2)) Then
                    litAddress2.Visible = True
                    litAddress2.Text = String.Format("{0}<br />", row.Address2)
                End If
                If Not (String.IsNullOrEmpty(row.Address3)) Then
                    litAddress3.Visible = True
                    litAddress3.Text = String.Format("{0}<br />", row.Address3)
                End If
                If Not (String.IsNullOrEmpty(row.Address4)) Then
                    litAddress4.Visible = True
                    litAddress4.Text = String.Format("{0}<br />", row.Address4)
                End If
                If Not (String.IsNullOrEmpty(row.Address5)) Then
                    litAddress5.Visible = True
                    litAddress5.Text = String.Format("{0}<br />", row.Address5)
                End If
                If Not (String.IsNullOrEmpty(row.Phone)) Then
                    litPhone.Visible = True
                    litPhone.Text = String.Format("<b>Phone:</b>{0}<br />", row.Phone)
                End If
                If Not (String.IsNullOrEmpty(row.Fax)) Then
                    litFax.Visible = True
                    litFax.Text = String.Format("<b>Fax:</b>{0}<br />", row.Fax)
                End If
                If Not (String.IsNullOrEmpty(row.SalesFax)) Then
                    litFaxSales.Visible = True
                    litFaxSales.Text = String.Format("<b>Fax Sales:</b>{0}<br />", row.SalesFax)
                End If
                If Not (String.IsNullOrEmpty(row.FaxService)) Then
                    litFaxService.Visible = True
                    litFaxService.Text = String.Format("<b>Fax Service:</b>{0}<br />", row.FaxService)
                End If
                If Not (String.IsNullOrEmpty(row.Email)) Then
                    litEmailSales.Visible = True
                    litEmailSales.Text = "<b>Email Sales:</b>"
                    spanBreak1.Visible = True
                    BindEmailsToOffice(row.Email, phEmailSales)
                End If
                If Not (String.IsNullOrEmpty(row.EmailService)) Then
                    litEmailService.Visible = True
                    litEmailService.Text = "<b>Email Service:</b>"
                    spanBreak2.Visible = True
                    BindEmailsToOffice(row.EmailService, phEmailService)
                End If
                If Not (String.IsNullOrEmpty(row.EmailSupport)) Then
                    litEmailSupport.Visible = True
                    litEmailSupport.Text = "<b>Email Support:</b>"
                    spanBreak3.Visible = True
                    BindEmailsToOffice(row.EmailSupport, phEmailSupport)
                End If
                If Not (String.IsNullOrEmpty(row.Web)) Then
                    litWebsite.Visible = True
                    litWebsite.Text = "<b>Web Site:</b>"
                    hlkWebsite.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", row.Web.Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkWebsite.Visible = True
                    hlkWebsite.Text = String.Format("{0}<br />", row.Web)
                    hlkWebsite.NavigateUrl = row.Web
                End If
                If Not (String.IsNullOrEmpty(row.PhoneService)) Then
                    litPhoneService.Visible = True
                    litPhoneService.Text = String.Format("<b>Phone Service:</b>{0}<br />", row.PhoneService)
                End If
                If Not (String.IsNullOrEmpty(row.PhoneSupport)) Then
                    litPhoneSupport.Visible = True
                    litPhoneSupport.Text = String.Format("<b>Phone Support:</b>{0}<br />", row.PhoneSupport)
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindPage(ByVal countryId As Nullable(Of Int32), ByVal isPageLoad As Boolean)
        Dim contactOffices As List(Of ContactOffice) = ContactOfficeRepository.FetchContactOffices("SELECT * FROM [CONTACT_OFFICE] WHERE [TYPE] = 'S' AND [ACTIVE_YN] = 'Y' ORDER BY [COUNTRY_ID_WEB]", New List(Of SqlParameter)().ToArray())
        Dim countries As List(Of Country) = CountryRepository.GetCountries(contactOffices.Select(Function(x) x.CountryIdWeb.Value).Distinct().ToList()).ToList()
        If (countries.Count > 0) Then
            DropDownListManager.BindDropDown(ddlCountries, countries, "NAME", "COUNTRYID", False, String.Empty, String.Empty)
        End If
        If (isPageLoad) Then
            Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
            If (countryCode > 0) Then
                If (countries.Where(Function(x) x.CountryId = countryCode).Count() > 0) Then
                    countryId = countryCode
                End If
            End If
        End If
        If (countryId.HasValue) Then
            contactOffices = contactOffices.Where(Function(x) x.CountryIdWeb = countryId.Value).ToList()
            ddlCountries.SelectedValue = countryId.ToString()
        End If
        If (contactOffices.Count > 0) Then
            rptContactOffices.DataSource = contactOffices
            rptContactOffices.DataBind()
        End If
    End Sub

    Private Sub BindEmailsToOffice(ByVal emailAddress As String, ByRef ph As PlaceHolder)
        If (emailAddress.Contains("(") And emailAddress.Contains(")")) Then
            If (emailAddress.Contains(";")) Then
                Dim emails As String() = emailAddress.Split(";")
                For i As Int32 = 0 To emails.Length - 1
                    Dim hlk As HyperLink = New HyperLink()
                    hlk.Text = IIf(i = emails.Length - 1, emails(i), String.Format("{0}, ", emails(i)))
                    hlk.NavigateUrl = String.Format("mailto:{0}", emails(i).Substring(0, emails(i).IndexOf("(")).Trim())
                    ph.Controls.Add(hlk)
                Next
            Else
                Dim hlk As HyperLink = New HyperLink()
                hlk.Text = emailAddress
                hlk.NavigateUrl = String.Format("mailto:{0}", emailAddress.Substring(0, emailAddress.IndexOf("(")).Trim())
                ph.Controls.Add(hlk)
            End If
        Else
            If (emailAddress.Contains(";")) Then
                Dim emails As String() = emailAddress.Split(";")
                For i As Int32 = 0 To emails.Length - 1
                    Dim hlk As HyperLink = New HyperLink()
                    hlk.Text = IIf(i = emails.Length - 1, emails(i), String.Format("{0}, ", emails(i)))
                    hlk.NavigateUrl = String.Format("mailto:{0}", emails(i).Trim())
                    ph.Controls.Add(hlk)
                Next
            Else
                Dim hlk As HyperLink = New HyperLink()
                hlk.Text = emailAddress
                hlk.NavigateUrl = String.Format("mailto:{0}", emailAddress)
                ph.Controls.Add(hlk)
            End If
        End If
    End Sub
#End Region
End Class