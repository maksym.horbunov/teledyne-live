<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-right.master" AutoEventWireup="false" Inherits="LeCroy.Website.Contact_Contact" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-right.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script type="text/javascript" language="javascript">
        jQuery(document).ready(function () {
            jQuery('.description-heading-less').hide();
            jQuery('.description-heading-more').live('click', function () {
                jQuery('.description-content').hide(); // Hide content
                jQuery('.description-heading-more').show();
                jQuery('.description-heading-less').hide();
                jQuery(this).parent().children().removeClass('active');
                jQuery(this).next('.description-content').slideDown(500);   // Show content
                jQuery(this).parent().children('.description-heading-less').show();
                jQuery(this).parent().children('.description-heading-less').addClass('active');
                jQuery('.description-heading-less.active').html('<br /><span style="color: #008ecd; font-weight: bold; text-decoration: underline;">Show Less</span><br />');
                jQuery(this).hide();    // Hide ...more link
                jQuery(this).parent().parent().css("height", "auto");
            });
            jQuery('.description-heading-less').live('click', function () {
                jQuery(this).html('&nbsp;');
                jQuery('.description-heading-more').show();
                jQuery('.description-content').slideUp(500); // Hide content
                jQuery(this).parent().children().removeClass('active');
            });
        });
	</script>
    <div id="ContactHeader">
        <div class="clear"></div>
		<p><span id="spanHeader">To contact a Teledyne LeCroy office in your area, select a region from the map.</span></p>
    </div>
    <div id="Map">
		<img src="/../images/contactimages/world-map.jpg" border="0" />
        <div id="Americas"></div>
		<div id="AsiaPac"></div>
		<div id="Europe"></div>
		<img src="/../images/contactimages/clear.gif" border="1" style="border: 1px solid #000000;" usemap="#Map" width="677" height="341" id="MapCover" />
        <map name="Map" id="Map2">
            <area shape="poly" coords="44,74,14,130,132,230,189,275,201,342,238,340,279,232,215,167,299,82,313,32,191,27" href="#" id="AmericasHotspot" />
	        <area shape="poly" coords="339,44,420,35,537,44,635,70,660,114,568,132,498,121,447,144,433,204,415,287,358,304,322,253,288,200,303,138,317,83" href="#" id="EuropeHotspot" />
            <area shape="poly" coords="435,204,448,145,498,122,575,134,621,165,654,301,606,330,539,323,465,261" href="#" id="AsiaPacHotspot" />
        </map>
    </div><br />
    <div style="padding-left: 10px;">
        <table border="0" cellpadding="0" cellspacing="0" width="679">
            <tr>
                <td align="right" valign="top">
                    Current Country:&nbsp;<asp:DropDownList ID="ddlCountryLA" runat="server" ClientIDMode="Static" style="display: none;" />&nbsp;
                    <asp:DropDownList ID="ddlCountryEMEA" runat="server" ClientIDMode="Static" style="display: none;" />&nbsp;
                    <asp:DropDownList ID="ddlCountryLAP" runat="server" ClientIDMode="Static" style="display: none;" />
                </td>
            </tr>
            <tr>
                <td valign="bottom">
                    <ul class="contactTabNavigation">
                        <li id="liOfficeTab" class="first on">Teledyne LeCroy Offices</li>
                        <li id="liOfficeTabClose" class="close close-on"></li>
                        <li id="liDistroTab">Sales &amp; Distribution Partners</li>
                        <li id="liDistroTabClose" class="close"></li>
                    </ul>
                </td>
            </tr>
        </table>
        <div id="RegionResults">
		</div>
    </div>
    <div style="padding: 20px;">
        All exports will be made in full compliance with all applicable U.S. government export regulations. No sales will be made directly or indirectly to any embargoed country, proscribed destination, denied party or restricted individual.
    </div>
    <asp:HiddenField ID="hdnRegionId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCountryId" runat="server" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources">
        <div class="sidenav">
            <ul>
                <li><a href="/support/techhelp/">Technical Help</a></li>
                <li>&nbsp;</li>
                <li><a href="/support/contact/rentalprogram.aspx">Rental Partners</a></li>
                <li>&nbsp;</li>
                <li><a href="/support/requestinfo/default.aspx?capid=106&amp;mid=538">Request Demo</a></li>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</asp:Content>