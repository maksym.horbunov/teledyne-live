﻿Imports System.Data.SqlClient
Imports System.IO
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL
Imports Newtonsoft.Json

Partial Class Contact_Contact
    Inherits BasePage

#Region "Variables/Keys"
    Private _countries As List(Of Country) = New List(Of Country)
    Private _extendedCountries As List(Of ExtendedCountry) = New List(Of ExtendedCountry)
    Private _defaultCountryForRegion As Dictionary(Of Int32, Int32) = New Dictionary(Of Int32, Int32)
    Private _index As Int32 = 0
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Contact Us"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Contact Us"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindData()
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindData()
        Dim contactOffices As List(Of ContactOffice) = ContactOfficeRepository.GetContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString())
        Dim contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct) = FeContactManager.FetchContactOfficeXRefProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), ContactOfficeXRefProductRepository.GetContactOfficeXRefProducts(ConfigurationManager.AppSettings("ConnectionString").ToString()))
        Dim dictionaryOfDefaultDistributorsForRegionAndCountry As Dictionary(Of String, ExtendedContactOffice) = GetDictionaryOfDefaultDistributorsForRegionAndCountry(contactOffices, contactOfficeXRefProducts)
        _countries = CountryRepository.FetchCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM COUNTRY WHERE ISO_CODE NOT IN (SELECT ISO_CODE FROM GEOIP_DENIED_COUNTRY WHERE ACTIVE_YN = 'Y')", New List(Of SqlParameter)().ToArray()).ToList()
        _extendedCountries = FeContactManager.FetchExtendedCountries(_countries, contactOffices, contactOfficeXRefProducts, dictionaryOfDefaultDistributorsForRegionAndCountry).ToList()
    End Sub

    Private Function GetDictionaryOfDefaultDistributorsForRegionAndCountry(ByVal contactOffices As List(Of ContactOffice), ByVal contactOfficeXRefProducts As List(Of ExtendedContactOfficeXRefProduct)) As Dictionary(Of String, ExtendedContactOffice)
        Dim retVal As Dictionary(Of String, ExtendedContactOffice) = New Dictionary(Of String, ExtendedContactOffice)
        Dim defaultDistibutors As String = ConfigurationManager.AppSettings("DefaultDistributorsForRegion")
        If Not (String.IsNullOrEmpty(defaultDistibutors)) Then
            Dim distributors As String() = defaultDistibutors.Split(",")
            For Each distributor As String In distributors
                If Not (retVal.ContainsKey(distributor)) Then
                    Dim regionCountryId As String() = distributor.Split("|")
                    retVal.Add(distributor, FeContactManager.GetDefaultContactOffice(regionCountryId(1), contactOffices, contactOfficeXRefProducts))
                End If
            Next
        End If
        Return retVal
    End Function

    Private Sub BindPage()
        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        Dim regionId As Int32 = 3
        Dim country As Country = (From c In _countries Where c.CountryId = countryCode Select c).FirstOrDefault()
        If Not (country Is Nothing) Then
            regionId = country.RegionId
        End If
        BindDropDowns()
        SetDropDownDefaults(regionId)
        hdnRegionId.Value = regionId.ToString()
        hdnCountryId.Value = countryCode
        InjectContactJsAndCss()
    End Sub

    Private Sub BindDropDowns()
        Dim regionIds As Int32() = New Int32() {2, 3, 4}
        Dim ddls As DropDownList() = New DropDownList() {ddlCountryEMEA, ddlCountryLA, ddlCountryLAP}
        For i As Int32 = 0 To ddls.Length - 1
            Dim countries As List(Of Country) = _countries.Where(Function(x) x.RegionId = regionIds(i)).ToList()
            For Each country In countries
                ddls(i).Items.Add(New ListItem(country.Name, country.CountryId.ToString()))
            Next
            If Not (_defaultCountryForRegion.ContainsKey(regionIds(i))) Then
                _defaultCountryForRegion.Add(regionIds(i), countries.OrderBy(Function(y) y.SortId.Value).Select(Function(x) x.CountryId).ToList().FirstOrDefault())
            End If
        Next
    End Sub

    Private Sub SetDropDownDefaults(ByVal regionId As Int32)
        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        If (regionId = 2) Then
            ddlCountryEMEA.Style.Clear()
            ddlCountryEMEA.Attributes.Add("style", "display: inline;")
            If Not (ddlCountryEMEA.Items.FindByValue(countryCode.ToString()) Is Nothing) Then ddlCountryEMEA.SelectedValue = countryCode
            ddlCountryLA.SelectedValue = _defaultCountryForRegion(3)
            ddlCountryLAP.SelectedValue = _defaultCountryForRegion(4)
        ElseIf (regionId = 4) Then
            ddlCountryLAP.Style.Clear()
            ddlCountryLAP.Attributes.Add("style", "display: inline;")
            If Not (ddlCountryLAP.Items.FindByValue(countryCode.ToString()) Is Nothing) Then ddlCountryLAP.SelectedValue = countryCode
            ddlCountryLA.SelectedValue = _defaultCountryForRegion(3)
            ddlCountryEMEA.SelectedValue = _defaultCountryForRegion(2)
        Else
            ddlCountryLA.Style.Clear()
            ddlCountryLA.Attributes.Add("style", "display: inline;")
            If Not (ddlCountryLA.Items.FindByValue(countryCode.ToString()) Is Nothing) Then ddlCountryLA.SelectedValue = countryCode
            ddlCountryEMEA.SelectedValue = _defaultCountryForRegion(2)
            ddlCountryLAP.SelectedValue = _defaultCountryForRegion(4)
        End If
    End Sub

    Private Sub InjectContactJsAndCss()
        Dim contactCss As HtmlLink = New HtmlLink()
        contactCss.Href = ResolveUrl(ConfigurationManager.AppSettings("CssContactUs"))
        contactCss.Attributes.Add("rel", "stylesheet")
        contactCss.Attributes.Add("type", "text/css")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(contactCss)
        Dim litResults As Literal = New Literal()
        litResults.Text = JsonifyData()
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(litResults)
        Dim contactJs As HtmlGenericControl = New HtmlGenericControl("script")
        contactJs.Attributes.Add("src", ResolveUrl(ConfigurationManager.AppSettings("JsContactUs")))
        contactJs.Attributes.Add("language", "javascript")
        contactJs.Attributes.Add("type", "text/javascript")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(contactJs)
    End Sub

    Private Function JsonifyData() As String
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim data As List(Of Region) = RegionRepository.FetchRegions(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT REGION_REPORTS.REGION_ID, REGION_REPORTS.NAME, REGION_REPORTS.DISPLAYNAME FROM COUNTRY INNER JOIN REGION_REPORTS ON COUNTRY.REGIONID = REGION_REPORTS.REGION_ID", New List(Of SqlParameter)().ToArray()).ToList()
        Using jsonWriter As JsonWriter = New JsonTextWriter(sw)
            jsonWriter.Formatting = Formatting.None
            jsonWriter.WriteStartObject()
            For Each region As Region In data
                Dim extendedCountries As List(Of ExtendedCountry) = GetCountriesForRegion(region.RegionId)
                If (extendedCountries.Count <= 0) Then Continue For
                jsonWriter.WritePropertyName(region.RegionId.ToString())
                jsonWriter.WriteStartObject()
                jsonWriter.WritePropertyName("name")
                jsonWriter.WriteValue(region.DisplayName)
                jsonWriter.WritePropertyName("countries")
                jsonWriter.WriteStartObject()
                Dim defaultSOfficeForRegion As ExtendedContactOffice = New ExtendedContactOffice()
                If (region.RegionId = 4) Then
                    defaultSOfficeForRegion = GetCountriesForRegion(3).FirstOrDefault().ContactOffices.OrderBy(Function(x) x.SortId.Value).FirstOrDefault()
                Else
                    defaultSOfficeForRegion = extendedCountries.FirstOrDefault().ContactOffices.OrderBy(Function(x) x.SortId.Value).FirstOrDefault()
                End If
                For Each country As ExtendedCountry In extendedCountries
                    _index = 0
                    jsonWriter.WritePropertyName(country.CountryId.ToString())
                    jsonWriter.WriteStartObject()
                    jsonWriter.WritePropertyName("name")
                    jsonWriter.WriteValue(country.Name)
                    jsonWriter.WritePropertyName("offices")
                    jsonWriter.WriteStartObject()
                    WriteSOfficeData(country, region.RegionId, defaultSOfficeForRegion, jsonWriter)
                    WriteDOfficeData(country, jsonWriter)
                    jsonWriter.WriteEndObject()
                    jsonWriter.WriteEndObject()
                Next
                jsonWriter.WriteEndObject()
                jsonWriter.WriteEndObject()
            Next
            jsonWriter.WriteEndObject()
        End Using
        Return String.Format("<script language=""javascript"" type=""text/javascript"">var regions = {0}</script>", sw.ToString())
    End Function

    Private Sub WriteSOfficeData(ByVal country As ExtendedCountry, ByVal regionId As Int32, ByVal defaultCountry As ExtendedContactOffice, ByRef jsonWriter As JsonWriter)
        Dim addedDefault As Boolean = False
        Dim sOffices As List(Of ExtendedContactOffice) = country.ContactOffices.Where(Function(x) x.Type.ToUpper() = "S").OrderBy(Function(x) x.SortId.Value).ToList()
        If (sOffices.Count <= 0) Then
            sOffices.Add(defaultCountry)
            addedDefault = True
        End If
        For i As Int32 = 0 To sOffices.Count - 1
            jsonWriter.WritePropertyName(i.ToString())  ' Need a index as the PK here, else it will follow it's own sort order
            jsonWriter.WriteStartObject()
            jsonWriter.WritePropertyName("name")
            jsonWriter.WriteValue(CleanData(Server.HtmlDecode(sOffices(i).Company)))
            jsonWriter.WritePropertyName("nameSubHead")
            Dim companySubHead As String = sOffices(i).CompanySubHead
            If Not (String.IsNullOrEmpty(companySubHead)) Then
                jsonWriter.WriteValue(CleanData(Server.HtmlDecode(companySubHead)))
            Else
                jsonWriter.WriteValue(String.Empty)
            End If
            jsonWriter.WritePropertyName("type")
            jsonWriter.WriteValue(CleanData(sOffices(i).Type))
            jsonWriter.WritePropertyName("isDefault")
            jsonWriter.WriteValue(addedDefault.ToString())
            jsonWriter.WritePropertyName("addressInfo")
            jsonWriter.WriteStartObject()
            jsonWriter.WritePropertyName("0")
            jsonWriter.WriteStartObject()
            jsonWriter.WritePropertyName("address1")
            jsonWriter.WriteValue(CleanData(sOffices(i).Address1))
            jsonWriter.WritePropertyName("address2")
            jsonWriter.WriteValue(CleanData(sOffices(i).Address2))
            jsonWriter.WritePropertyName("address3")
            jsonWriter.WriteValue(CleanData(sOffices(i).Address3))
            jsonWriter.WritePropertyName("address4")
            jsonWriter.WriteValue(CleanData(sOffices(i).Address4))
            jsonWriter.WritePropertyName("address5")
            jsonWriter.WriteValue(CleanData(sOffices(i).Address5))
            jsonWriter.WritePropertyName("phone")
            jsonWriter.WriteValue(CleanData(sOffices(i).Phone))
            jsonWriter.WritePropertyName("fax")
            jsonWriter.WriteValue(CleanData(sOffices(i).Fax))
            jsonWriter.WritePropertyName("faxSales")
            jsonWriter.WriteValue(CleanData(sOffices(i).SalesFax))
            jsonWriter.WritePropertyName("faxService")
            jsonWriter.WriteValue(CleanData(sOffices(i).FaxService))
            jsonWriter.WritePropertyName("email")
            jsonWriter.WriteValue(CleanData(sOffices(i).Email))
            jsonWriter.WritePropertyName("emailService")
            jsonWriter.WriteValue(CleanData(sOffices(i).EmailService))
            jsonWriter.WritePropertyName("emailSupport")
            jsonWriter.WriteValue(CleanData(sOffices(i).EmailSupport))
            jsonWriter.WritePropertyName("website")
            jsonWriter.WriteValue(CleanData(sOffices(i).Web))
            jsonWriter.WritePropertyName("phoneSupport")
            jsonWriter.WriteValue(CleanData(sOffices(i).PhoneSupport))
            jsonWriter.WritePropertyName("phoneService")
            jsonWriter.WriteValue(CleanData(sOffices(i).PhoneService))
            jsonWriter.WritePropertyName("products")
            jsonWriter.WriteStartObject()
            jsonWriter.WriteEndObject()
            jsonWriter.WriteEndObject()
            jsonWriter.WriteEndObject()
            jsonWriter.WriteEndObject()
            _index += 1
        Next
    End Sub

    Private Sub WriteDOfficeData(ByVal country As ExtendedCountry, ByRef jsonWriter As JsonWriter)
        Dim dOffices As List(Of ExtendedContactOffice) = country.ContactOffices.Where(Function(x) x.Type.ToUpper() = "D").OrderByDescending(Function(x) x.IsFeatured).ThenBy(Function(x) x.Company).ToList()
        Dim d As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
        Dim officeNames As List(Of String) = dOffices.Select(Function(x) x.Company).Distinct().ToList()
        'Dim offices As List(Of ExtendedContactOffice) = dOffices.Distinct().ToList()
        For Each officeName As String In officeNames
            'For Each office As ExtendedContactOffice In offices
            Dim contactOffices As List(Of ExtendedContactOffice) = dOffices.Where(Function(x) x.Company.ToUpper() = officeName.ToUpper()).OrderByDescending(Function(x) x.IsFeatured).ThenBy(Function(x) x.Company).ToList()    '.OrderBy(Function(x) x.OfficeId).ToList()
            jsonWriter.WritePropertyName(_index)  ' Need a index as the PK here, else it will follow it's own sort order
            jsonWriter.WriteStartObject()
            jsonWriter.WritePropertyName("name")
            jsonWriter.WriteValue(CleanData(Server.HtmlDecode(officeName)))
            jsonWriter.WritePropertyName("nameSubHead")
            Dim companySubHead As String = contactOffices.Where(Function(x) x.CompanySubHead IsNot DBNull.Value).Select(Function(x) x.CompanySubHead).FirstOrDefault().ToString()
            If Not (String.IsNullOrEmpty(companySubHead)) Then
                jsonWriter.WriteValue(CleanData(Server.HtmlDecode(companySubHead)))
            Else
                jsonWriter.WriteValue(String.Empty)
            End If
            jsonWriter.WritePropertyName("type")
            jsonWriter.WriteValue(CleanData(contactOffices.FirstOrDefault().Type))
            jsonWriter.WritePropertyName("addressInfo")
            jsonWriter.WriteStartObject()
            Dim productsSet As Boolean = False
            For Each co In contactOffices
                jsonWriter.WritePropertyName(_index.ToString())
                jsonWriter.WriteStartObject()
                jsonWriter.WritePropertyName("address1")
                jsonWriter.WriteValue(CleanData(co.Address1))
                jsonWriter.WritePropertyName("address2")
                jsonWriter.WriteValue(CleanData(co.Address2))
                jsonWriter.WritePropertyName("address3")
                jsonWriter.WriteValue(CleanData(co.Address3))
                jsonWriter.WritePropertyName("address4")
                jsonWriter.WriteValue(CleanData(co.Address4))
                jsonWriter.WritePropertyName("address5")
                jsonWriter.WriteValue(CleanData(co.Address5))
                jsonWriter.WritePropertyName("phone")
                jsonWriter.WriteValue(CleanData(co.Phone))
                jsonWriter.WritePropertyName("fax")
                jsonWriter.WriteValue(CleanData(co.Fax))
                jsonWriter.WritePropertyName("faxSales")
                jsonWriter.WriteValue(CleanData(co.SalesFax))
                jsonWriter.WritePropertyName("faxService")
                jsonWriter.WriteValue(CleanData(co.FaxService))
                jsonWriter.WritePropertyName("email")
                jsonWriter.WriteValue(CleanData(co.Email))
                jsonWriter.WritePropertyName("emailService")
                jsonWriter.WriteValue(CleanData(co.EmailService))
                jsonWriter.WritePropertyName("emailSupport")
                jsonWriter.WriteValue(CleanData(co.EmailSupport))
                jsonWriter.WritePropertyName("website")
                jsonWriter.WriteValue(CleanData(co.Web))
                jsonWriter.WritePropertyName("phoneSupport")
                jsonWriter.WriteValue(CleanData(co.PhoneSupport))
                jsonWriter.WritePropertyName("phoneService")
                jsonWriter.WriteValue(CleanData(co.PhoneService))
                If (String.Compare(co.Type, "D", True) = 0) Then
                    jsonWriter.WritePropertyName("products")
                    jsonWriter.WriteStartObject()
                    If Not (productsSet) Then
                        Dim categorizedProducts As List(Of ExtendedContactOfficeXRefProduct) = co.Products.OrderBy(Function(x) x.CategoryName).ToList()
                        For Each product As ExtendedContactOfficeXRefProduct In categorizedProducts
                            Dim temporaryParentId As Int32 = -1
                            If (product.CategoryFkId.Value = LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS) Then
                                If (product.ProtocolStandardFkId.HasValue) Then
                                    temporaryParentId = product.ProtocolStandardFkId.Value
                                End If
                                jsonWriter.WritePropertyName(String.Format("{0}|{1}|{2}|{3}", product.CategoryName, product.ProtocolStandardName, temporaryParentId, product.CategoryFkId.Value))
                            Else
                                If (product.ProductSeriesFkId.HasValue) Then
                                    temporaryParentId = product.ProductSeriesFkId.Value
                                End If
                                jsonWriter.WritePropertyName(String.Format("{0}|{1}|{2}|{3}", product.CategoryName, product.ProductSeriesName, temporaryParentId, product.CategoryFkId.Value))
                            End If
                            jsonWriter.WriteValue(True)
                        Next
                    End If
                    jsonWriter.WriteEndObject()
                Else
                    jsonWriter.WritePropertyName("products")
                    jsonWriter.WriteStartObject()
                    jsonWriter.WriteEndObject()
                End If
                productsSet = True
                jsonWriter.WriteEndObject()
                _index += 1
            Next
            jsonWriter.WriteEndObject()
            jsonWriter.WriteEndObject()
        Next
    End Sub

    Private Function GetCountriesForRegion(ByVal regionId As Int32) As IList(Of ExtendedCountry)
        Return (From c In _extendedCountries Where c.RegionId = regionId Select c).OrderBy(Function(x) x.SortId).ThenBy(Function(y) y.Name).ToList()
    End Function

    Private Function CleanData(ByVal inValue As String) As String
        If Not (String.IsNullOrEmpty(inValue)) Then
            Return inValue.Replace("\r", String.Empty).Replace("\n", String.Empty)
        End If
        Return Nothing
    End Function
#End Region
End Class