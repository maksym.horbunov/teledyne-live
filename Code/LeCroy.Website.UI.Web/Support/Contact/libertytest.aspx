﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="libertytest.aspx.vb" Inherits="LeCroy.Website.libertytest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
<div id="categoryTop" style="padding:0px 10px; font-size:1.25em;">
    <h1 style="padding-top:40px;font-size:25px;">Liberty Test Equipment</h1>
    <a href="http://www.libertytest.com/store/_vendor_landing_lecroy.asp"><img src="https://teledynelecroy.com/images/liberty-products-01.png" align="right" border="0" width="250px" hspace="10px" /></a>
	<p>Teledyne LeCroy has teamed up with Liberty Test Equipment as our certified pre-owned equipment seller for benchtop gear.  In addition, they now offer Teledyne LeCroy rentals up to 6 GHz.</p>
    <p>All instruments units backed by our standard warranty and have been calibrated by Teledyne LeCroy Service professionals in our Chestnut Ridge, NY Service Center.</p>
</div>
<div id="categoryBottom" style="padding:10px 10px;font-size:1.25em;">
	<a href="http://www.libertytest.com/store/_vendor_landing_lecroy.asp" class="thickbox" title="Liberty Test Equipment"><img src="https://teledynelecroy.com/images/liberty-test-logo.png" align="right" border="0" width="300px"/></a>
	<h2>Units Available</h2>
    <ul>
        <li>HDO4000, 12-bit high definition oscilloscopes</li>
        <li>WaveSurfer oscilloscopes up to 1 GHz</li>
        <li>WaveAce Oscilloscopes up to 200 MHz</li>
        <li>Probes and Accessories</li>
    </ul>
    <p><a href="http://www.libertytest.com/store/_vendor_landing_lecroy.asp">Contact them</a> today for more details!</p>
</div>
</asp:Content>