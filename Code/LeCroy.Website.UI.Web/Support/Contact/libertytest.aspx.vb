﻿Public Class libertytest
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        If (countryCode <> 208) Then
            Response.Redirect("~/")
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class