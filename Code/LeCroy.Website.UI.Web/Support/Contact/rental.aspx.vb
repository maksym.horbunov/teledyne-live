﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Partial Class Support_Contact_rental
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Initial()
        End If
    End Sub

    Private Sub Initial()
        If Request("rentid") Is Nothing Then
            Response.Redirect("default.aspx")
        End If

        If Not IsNumeric(Request("rentid")) Then
            Response.Redirect("default.aspx")
        End If

        Dim url As String = ""
        Dim sqlstr As String = "SELECT * from RENTAL where RENTAL_ID=@RENTALID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@RENTALID", Request("rentid").ToString()))
        Dim rentals As List(Of Rental) = RentalRepository.FetchRentals(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If (rentals.Count > 0) Then
            url = rentals.FirstOrDefault().Url
        End If
        If Not url.Equals("") Then
            Response.Redirect(url)
        Else
            Response.Redirect("default.aspx")
        End If
    End Sub
End Class