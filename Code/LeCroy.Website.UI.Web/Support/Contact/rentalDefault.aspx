﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Contact_rentalDefault" Codebehind="rentalDefault.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <br>
        <table border="0" width="480" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" align="left">
                    <h1>Rental Partners</h1><br>
                    <font size="2"><font size="2" face="verdana,arial,helvetica">Welcome to our Rental Partners message board for&nbsp;inquiries in the<b><br>United States and Canada area.</b></font>
                </td>
            </tr>
        </table><br>
        <table cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td rowspan="2"><asp:Image ID="img_logo" runat="server" BorderWidth="0" Visible="false" /></td>
                <td><asp:Label ID="lb_title" runat="server" /></td>
            </tr>
            <tr><td><asp:Label ID="lb_desc" runat="server" /></td></tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="tb_input" runat="server" Height="92px" TextMode="MultiLine" Width="367px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_input" ErrorMessage="<br>Please fill this field." ValidationGroup="1" />
                    <asp:Label ID="lb_confirm" runat="server" />
                </td>
            </tr>
            <tr><td colspan="2" align="right"><asp:Button ID="btn_send" runat="server" Text="send" ValidationGroup="1" />&nbsp;<asp:Label ID="lblalert" runat="server" Font-Bold="True" ForeColor="Red" Text="this form is for United States and Canada only" Visible="False" /></td></tr>
        </table>
        <hr size="1">
        <h1><b>Please verify the information below:<br></b></h1><br>
        <strong>
            <%If Len(Session("Honorific")) > 0 Then
                    Response.Write(Session("Honorific") & "&nbsp;")
                End If
            %>
            <% = Session("FirstName") %>&nbsp;<% = Session("LastName") %>
        </strong><br>
        <% = Session("Company") %><br>
        <% = Session("Address") %><br>
        <% If Len(Session("Address2")) > 0 Then%>
        <% = Session("Address2") %><br>
        <% End If%>
        <% = Session("City") %>
        <%If UCase(Session("State")) <> "SELECT STATE" Then
                Response.Write(",&nbsp;" & Session("State"))
            End If%>
        ,&nbsp;<% = Session("Zip") %><br>
        <% = Session("Country") %><br>
        <br>
        <asp:Button ID="btn_change" runat="server" Text="Change Address" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources">
        <div class="sidenav"></div>
    </div>
</asp:Content>