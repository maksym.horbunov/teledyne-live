﻿Imports System.Data.SqlClient
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Support_Contact_rentalDefault
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Contact Us"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Rental Partners"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.CONTACT_US_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            ValidateUser()
            'If Not Functions.ValidateUser(Me.Context, rootDir) Then
            '    Response.Redirect(rootDir + "/Support/User/")
            'End If
            ValidateUserNoRedir()
            If Len(Request.QueryString("rentalid")) = 0 Then
                Response.Redirect("default.aspx" + menuURL2)
            End If
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            If Not Functions.CheckcustomerfromLNA(Session("COUNTRY")) Then
                lblalert.Visible = True
                Return
            End If
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim sqlstr As String = ""
        Dim ds As DataSet

        sqlstr = "SELECT * from RENTAL where RENTAL_ID=@RENTALID"
        'sqlstr = "SELECT * from RENTAL"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@RENTALID", Request.QueryString("rentalid").ToString()))
        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        ' If Len(ds.Tables(0).Rows(0)("logo")) > 0 Then
        'img_logo.ImageUrl = ds.Tables(0).Rows(0)("logo").ToString()
        ' Else
        img_logo.Visible = False
        'End If
        'img_logo.Visible = False
        lb_title.Text = "<h3>" & ds.Tables(0).Rows(0)("Title") & "</h3>"
        If ds.Tables(0).Rows(0)("PSG_YN").ToString().Equals("n") Then
            lb_desc.Text = "Please type your message in the box below and click on<br>the ""Send"" button to send an email to our representatives and affiliates."
        Else
            lb_desc.Text = "Indicate which protocol products you're interested in and click on<br>the ""Send"" button to send an email to our representatives and affiliates."
        End If
    End Sub

    Protected Sub btn_send_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_send.Click
        If Not Functions.CheckcustomerfromLNA(Session("COUNTRY")) Then
            lblalert.Visible = True
            Return
        End If
        Dim sqlstr As String = ""
        Dim dr As DataRow
        Dim ds As DataSet
        Dim subject As String = ""
        Dim strBody As String = ""
        Dim info As String = Functions.EmailBodyClientData(Session("ContactID"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Dim strCRet As String = Chr(13) & Chr(10)
        Dim strCustomerEmail As String = ""
        Dim strCompany As String = ""
        Dim strSENumber As String = ""
        Dim strZip As String = ""
        Dim strSEName As String = ""
        Dim strSEEmail As String = ""
        Dim strSEPager As String = ""
        Dim strCSRName As String = ""
        Dim strCSREmail As String = ""
        Dim strPrimaryBackupCSR As String = ""
        Dim strPartFooter As String = ""
        Dim strToCSR As String = ""
        Dim strToSE As String = ""
        Dim title As String = ""
        Dim email As String = ""

        sqlstr = "SELECT * from RENTAL where RENTAL_ID=@RENTALID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@RENTALID", Request.QueryString("rentalid").ToString()))
        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        If ds.Tables.Count > 0 Then
            dr = ds.Tables(0).Rows(0)
            title = Me.convertToString(dr("title"))
            email = Me.convertToString(dr("email"))
            If LCase(CStr(dr("PSG_YN"))) = "y" Then
                subject = "Teledyne LeCroy Protocol Analyzer Rental Request"
                strBody = "Rental Request for " & Me.convertToString(dr("Title")) & strCRet & "--------------------------------------------" & strCRet & strCRet
                strBody = strBody & info
                strBody = strBody & strCRet & strCRet & "Comments:" & strCRet & strCRet & tb_input.Text & strCRet & strCRet
                Try
                    Functions.SendEmail(Session("country").ToString, strBody, "trs@trs-rentelco.com,protocolrentals@teledynelecroy.com", Session("email").ToString(), "", "", subject, "")
                Catch ex As Exception

                End Try
            Else
                If Session("COUNTRY") = "United States" Then
                    sqlstr = "Select CONTACT.COMPANY as Company,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL as EMAIL, CONTACT.URL AS URL from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=@CONTACTID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
                    ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    If ds.Tables(0).Rows.Count > 0 Then
                        dr = ds.Tables(0).Rows(0)
                        Dim url As String = dr("URL").ToString()
                        strCustomerEmail = Me.convertToString(dr("EMAIL"))
                        strCompany = Me.convertToString(dr("Company"))
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@STATE", Me.convertToString(dr("State"))))
                        sqlParameters.Add(New SqlParameter("@COMPANYNAME", SQLStringWithSingleQuotes(strCompany)))
                        sqlParameters.Add(New SqlParameter("@CITY", Replace(Me.convertToString(dr("City")), "'", "")))
                        sqlstr = "SELECT * FROM [CCMS_EXCEPTION_COMPANIES] WHERE [STATE] = @STATE AND [COMPANYNAME] = @COMPANYNAME AND [CITY] = @CITY"
                        'sqlstr = "Select * from tblCOMPANYLOOKUP where STATE='" & Me.convertToString(dr("State")) & "' and CompanyName =" & _
                        'SQLStringWithSingleQuotes(strCompany) & " and CITY='" & Replace(Me.convertToString(dr("City")), "'", "") & "'"
                        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                        If (ds.Tables.Count > 0) Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                dr = ds.Tables(0).Rows(0)
                                'strSENumber = Me.convertToString(dr("SENumber"))
                                strSENumber = Me.convertToString(dr("SECode"))
                            End If
                        End If

                        If (String.IsNullOrEmpty(strSENumber)) Then
                            Dim exceptionDomains As List(Of CCMSExceptionDomain) = BLL.FeCcmsManager.GetDomain(ConfigurationManager.AppSettings("ConnectionString").ToString(), Replace(Trim(url), "'", ""), Me.convertToString(dr("State")))
                            If (exceptionDomains.Count > 0) Then
                                Dim city As String = Replace(Trim(dr("CITY")), "'", "")
                                If (exceptionDomains.Where(Function(x) Not (String.IsNullOrEmpty(x.City)) AndAlso x.City.ToLower() = city.ToLower()).Count() > 0) Then
                                    strSENumber = exceptionDomains.Where(Function(x) x.City.ToLower() = city.ToLower()).Select(Function(x) x.SeCode).FirstOrDefault()
                                Else
                                    strSENumber = exceptionDomains.Select(Function(x) x.SeCode).FirstOrDefault()
                                End If
                            End If
                        End If
                    End If

                    If strSENumber = "" Then
                        sqlstr = "Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=@CONTACTID"
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
                        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                        If ds.Tables(0).Rows.Count > 0 Then
                            dr = ds.Tables(0).Rows(0)
                            strCustomerEmail = Me.convertToString(dr("email"))
                            strZip = Me.convertToString(dr("POSTALCODE"))
                            If Len(strZip) >= 5 Then
                                strZip = Mid(strZip, 1, 5)
                            End If

                            sqlstr = "SELECT [SECODE] FROM [CCMS_MAP_US] WHERE [ZIPCODE] = @ZIPCODE"
                            'sqlstr = "Select SENumber from tblAllZips where ZipCode=" & strZip
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@ZIPCODE", strZip))
                            strSENumber = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                        End If
                    End If

                    Dim csrId As String = String.Empty
                    sqlstr = "SELECT * FROM [CCMS_SECSR] WHERE [SECODE] = @SECODE"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SECODE", strSENumber.Trim()))
                    ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    If ds.Tables(0).Rows.Count > 0 Then
                        dr = ds.Tables(0).Rows(0)
                        csrId = Me.convertToString(dr("CSRCode"))
                    End If

                    If Not (String.IsNullOrEmpty(csrId)) Then
                        sqlstr = "SELECT * FROM [CCMS_PERSONNEL_DATA] WHERE [RECTYPE] = 'CS' AND [ID] = @ID"
                        'sqlstr = "Select * from tblSECSR where SENumber=" & strSENumber
                        sqlParameters = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@ID", csrId.Trim()))
                        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                        If ds.Tables(0).Rows.Count > 0 Then
                        dr = ds.Tables(0).Rows(0)
                        strCSRName = String.Format("{0} {1}", Trim(Me.convertToString(dr("FirstName"))), Trim(Me.convertToString(dr("LastName"))))
                        strCSREmail = Trim(Me.convertToString(dr("Email")))
                        'strSEName = Me.convertToString(dr("SEName"))
                        'strSEEmail = Me.convertToString(dr("SEEmail"))
                        'strSEPager = Me.convertToString(dr("SEPager"))
                        'strCSRName = Trim(Me.convertToString(dr("CSR")))
                        'strCSREmail = Trim(Me.convertToString(dr("CSREmail")))
                        'strPrimaryBackupCSR = Trim(Me.convertToString(dr("PrimaryBackupCSR")))
                            strPartFooter = strCSRName & ", " & LoadI18N("SLBCA0062", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & strCRet & strCRet
                        'strPartFooter = strPartFooter & LoadI18N("SLBCA0063", localeid) & " " & strSEName & ", " & LoadI18N("SLBCA0064", localeid) & strCRet & strCRet


                        'If Not strPrimaryBackupCSR = "" Then
                        'sqlstr = "Select [Email Address] as Email from TblEmail where [Employee Name]='" & strPrimaryBackupCSR & "'"
                        'Dim strPrimaryBackupCSREmail As String = DbHelperSQL.GetSingle(sqlstr)
                        'End If
                    End If
                End If

                    sqlstr = "SELECT * FROM [CCMS_PERSONNEL_DATA] WHERE [RECTYPE] = 'SE' AND [ID] = @ID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@ID", strSENumber.Trim()))
                    ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    If ds.Tables(0).Rows.Count > 0 Then
                        dr = ds.Tables(0).Rows(0)
                        strSEName = String.Format("{0} {1}", Me.convertToString(dr("FirstName")), Me.convertToString(dr("LastName")))
                        strSEEmail = Me.convertToString(dr("Email"))
                        strSEPager = Me.convertToString(dr("Pager"))
                        strPartFooter = strPartFooter & LoadI18N("SLBCA0063", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & " " & strSEName & ", " & LoadI18N("SLBCA0064", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & strCRet & strCRet
                    End If
                End If
            If Len(strCSREmail) > 0 Then
                strToCSR = strCSREmail
            Else
                strToCSR = "Martina.Rogan@teledynelecroy.com"
            End If

            If Not strSEEmail = "" Then

                strToSE = strSEEmail

            End If
            subject = "Teledyne LeCroy Rental Request"
            strBody = "Rental Request for " & title & strCRet & "--------------------------------------------" & strCRet & strCRet
            If Session("COUNTRY") = "United States" Then
                strBody = strBody & "This request has been sent to:" & strCRet & strCRet
                strBody = strBody & strCSRName & ", CUSTOMER SERVICE REPRESENTATIVE" & strCRet & strCRet
                strBody = strBody & "and " & strSEName & ", SALES ENGINEER" & strCRet & strCRet
                strBody = strBody & "--------------------------------------------" & strCRet & strCRet
            End If
            strBody = strBody & info

            strBody = strBody & strCRet & strCRet & "Comments:" & strCRet & strCRet & tb_input.Text & strCRet & strCRet

            If Not Session("COUNTRY") = "United States" Or Session("COUNTRY") = "Select Country" Then

                Try
                    Functions.SendEmail(Session("country").ToString, strBody, GetSalesRepEmail(Session("COUNTRY"), 11, LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES), Session("email").ToString(), "", "", subject, "")    ' Assumes scopes (catid) only for rental (request)
                Catch ex As Exception

                End Try
            Else
                Try
                        Functions.SendEmail(Session("country").ToString, strBody, "Martina.Rogan@teledynelecroy.com", Session("email").ToString(), "", "", subject, "")
                    Catch ex As Exception

                End Try

                If Not strToSE = "" Then
                    Try
                        Functions.SendEmail(Session("country").ToString, strBody, strToSE, Session("email").ToString(), "", "", subject & " SE", "")
                    Catch ex As Exception

                    End Try
                End If
            End If

            If Len(email) > 0 Then
                Try
                    Functions.SendEmail(Session("country").ToString, strBody, email, "webmaster@teledynelecroy.com", "", "", subject & "  Rental Partner", "")
                Catch ex As Exception

                End Try
            End If
            End If
        End If
        lb_confirm.Text = "<h3>" & Session("firstname") & ", thank you for your inquiry.<br>You will be contacted by one of our representatives shortly.</h3>"
        tb_input.Visible = False
        btn_send.Visible = False
        lb_desc.Visible = False
    End Sub

    Protected Sub btn_change_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_change.Click
        Response.Redirect(rootDir + "/support/user/userprofile.aspx")
    End Sub
End Class