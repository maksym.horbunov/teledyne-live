﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="rentalprogram.aspx.vb" Inherits="LeCroy.Website.Contact_rentalprogram" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <h2>Rental Partners</h2><br />
        
        <h1>Oscilloscopes and Protocol Analyzers</h1>
        <p><a href="http://www.trs-rentelco.com/Manufacturer/Teledyne_LeCroy.aspx" onclick="GaEventPush('OutboundLink', 'http://www.trs-rentelco.com/Manufacturer/Teledyne_LeCroy.aspx');">TRS-RenTelco</a></p>

        <h1>Oscilloscopes (Less than 6 GHz bandwidth)</h1>
        <p><a href="https://libertytest.com/teledyne-lecroy" onclick="GaEventPush('OutboundLink', 'https://libertytest.com/teledyne-lecroy');">Liberty Test Equipment</a></p>

        <h1>Protocol Analyzers Direct Rental Program</h1>
        <p>For more information on our rental program, including price and availability, please contact Teledyne LeCroy at 1-800-5-LeCroy (1-800-553-2769).</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>