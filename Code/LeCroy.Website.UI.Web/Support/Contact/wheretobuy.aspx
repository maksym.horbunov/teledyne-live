﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/twocolumn-right.master" CodeBehind="wheretobuy.aspx.vb" Inherits="LeCroy.Website.wheretobuy" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-right.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <h1>Where to Buy:&nbsp;<asp:Label ID="lblSeriesName" runat="server" /></h1>
        <table border="0" cellpadding="0" cellspacing="0" width="700">
            <tr>
                <td align="left" valign="top"><asp:Image ID="imgSeries" runat="server" BorderStyle="None" /></td>
                <td align="left" valign="top"><asp:Literal ID="litSeriesDescription" runat="server" /></td>
            </tr>
        </table>
        <br /><br />
        Current Country:&nbsp;<asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" /><br /><br />
        <div id="divLocations" runat="server" visible="true">
            <asp:Repeater ID="rptLocations" runat="server">
                <HeaderTemplate>
                    <table border="0" cellpadding="2" cellspacing="0" width="700">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr style="background-color: #eaeaea;">
                            <td>
                                <div style="padding-bottom: 8px; padding-left: 10px; padding-top: 5px;">
                                    <h2><asp:Literal ID="litOfficeName" runat="server" /></h2>
                                    <h4 style="font-weight: bold; margin-top: -5px;"><asp:Literal ID="litOfficeSubheading" runat="server" /></h4>
                                    <asp:Repeater ID="rptLocationOffices" runat="server">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Literal ID="litOfficeAddress" runat="server" /><br />
                                                <span id="spanPhone" runat="server" visible="false"><b>Phone:</b>&nbsp;<asp:Literal ID="litOfficePhone" runat="server" /><br /></span>
                                                <span id="spanEmail" runat="server" visible="false"><b>Email Sales:</b>&nbsp;<asp:PlaceHolder ID="phOfficeEmail" runat="server" /><br /></span>
                                                <span id="spanWebSite" runat="server" visible="false"><b>Web Site:</b>&nbsp;<asp:HyperLink ID="hlkOfficeWebsite" runat="server" Target="_blank" /><br /></span>
                                                <span id="spanPhoneSupport" runat="server" visible="false"><b>Phone Support:</b>&nbsp;<asp:Literal ID="litOfficePhoneSupport" runat="server" /><br /></span>
                                            </p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <tr>
                            <td>
                                <div style="padding-bottom: 8px; padding-left: 10px; padding-top: 5px;">
                                    <h2><asp:Literal ID="litOfficeName" runat="server" /></h2>
                                    <h4 style="font-weight: bold; margin-top: -5px;"><asp:Literal ID="litOfficeSubheading" runat="server" /></h4>
                                    <asp:Repeater ID="rptLocationOffices" runat="server">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Literal ID="litOfficeAddress" runat="server" /><br />
                                                <span id="spanPhone" runat="server" visible="false"><b>Phone:</b>&nbsp;<asp:Literal ID="litOfficePhone" runat="server" /><br /></span>
                                                <span id="spanEmail" runat="server" visible="false"><b>Email Sales:</b>&nbsp;<asp:PlaceHolder ID="phOfficeEmail" runat="server" /><br /></span>
                                                <span id="spanWebSite" runat="server" visible="false"><b>Web Site:</b>&nbsp;<asp:HyperLink ID="hlkOfficeWebsite" runat="server" Target="_blank" /><br /></span>
                                                <span id="spanPhoneSupport" runat="server" visible="false"><b>Phone Support:</b>&nbsp;<asp:Literal ID="litOfficePhoneSupport" runat="server" /><br /></span>
                                            </p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                </AlternatingItemTemplate>
                <SeparatorTemplate><div class="clear"></div></SeparatorTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divNoLocations" runat="server" visible="false">
            We're sorry, there are no locations for that country.
        </div>
    </div>
    <div style="padding: 20px;">
        All exports will be made in full compliance with all applicable U.S. government export regulations. No sales will be made directly or indirectly to any embargoed country, proscribed destination, denied party or restricted individual.
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources">
        <div class="sidenav">
            <ul>
                <li><a href="/Support/TechHelp/">Technical Help</a></li>
                <li>&nbsp;</li>
                <li><a href="/support/requestinfo/default.aspx?capid=106&amp;mid=538">Request Demo</a></li>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</asp:Content>