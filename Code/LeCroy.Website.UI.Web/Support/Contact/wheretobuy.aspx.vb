﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common
Imports LeCroy.Website.BLL

Public Class wheretobuy
    Inherits BasePage

#Region "Variables/Keys"
    Private _productSeriesId As Nullable(Of Int32) = Nothing
    Private _productGroupId As Nullable(Of Int32) = Nothing
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Contact Us"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Where to Buy"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        InjectJsAndCss()
        ValidateQueryString()
        If Not (IsPostBack) Then
            BindDropDown()
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        BindRepeater()
    End Sub

    Private Sub rptLocations_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptLocations.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ConsolidatedContactOffice = CType(e.Item.DataItem, ConsolidatedContactOffice)
                Dim litOfficeName As Literal = CType(e.Item.FindControl("litOfficeName"), Literal)
                Dim litOfficeSubheading As Literal = CType(e.Item.FindControl("litOfficeSubheading"), Literal)
                Dim rptLocationOffices As Repeater = CType(e.Item.FindControl("rptLocationOffices"), Repeater)

                litOfficeName.Text = row.Name
                If Not (String.IsNullOrEmpty(row.NameSubHeading)) Then litOfficeSubheading.Text = row.NameSubHeading
                AddHandler rptLocationOffices.ItemDataBound, AddressOf rptLocationOffices_ItemDataBound
                rptLocationOffices.DataSource = row.ContactOffices
                rptLocationOffices.DataBind()
        End Select
    End Sub

    Private Sub rptLocationOffices_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ContactOffice = CType(e.Item.DataItem, ContactOffice)
                Dim litOfficeAddress As Literal = CType(e.Item.FindControl("litOfficeAddress"), Literal)
                Dim spanPhone As HtmlGenericControl = CType(e.Item.FindControl("spanPhone"), HtmlGenericControl)
                Dim litOfficePhone As Literal = CType(e.Item.FindControl("litOfficePhone"), Literal)
                Dim spanEmail As HtmlGenericControl = CType(e.Item.FindControl("spanEmail"), HtmlGenericControl)
                Dim phOfficeEmail As PlaceHolder = CType(e.Item.FindControl("phOfficeEmail"), PlaceHolder)
                Dim spanWebSite As HtmlGenericControl = CType(e.Item.FindControl("spanWebSite"), HtmlGenericControl)
                Dim hlkOfficeWebsite As HyperLink = CType(e.Item.FindControl("hlkOfficeWebsite"), HyperLink)
                Dim spanPhoneSupport As HtmlGenericControl = CType(e.Item.FindControl("spanPhoneSupport"), HtmlGenericControl)
                Dim litOfficePhoneSupport As Literal = CType(e.Item.FindControl("litOfficePhoneSupport"), Literal)

                Dim address As String = String.Empty
                If Not (String.IsNullOrEmpty(row.Address1)) Then
                    address += String.Format("{0}", row.Address1)
                End If
                If Not (String.IsNullOrEmpty(row.Address2)) Then
                    address += String.Format("{0}{1}", IIf((Not (String.IsNullOrEmpty(address))), "<br />", String.Empty), row.Address2)
                End If
                If Not (String.IsNullOrEmpty(row.Address3)) Then
                    address += String.Format("{0}{1}", IIf((Not (String.IsNullOrEmpty(address))), "<br />", String.Empty), row.Address3)
                End If
                If Not (String.IsNullOrEmpty(row.Address4)) Then
                    address += String.Format("{0}{1}", IIf((Not (String.IsNullOrEmpty(address))), "<br />", String.Empty), row.Address4)
                End If
                If Not (String.IsNullOrEmpty(row.Address5)) Then
                    address += String.Format("{0}{1}", IIf((Not (String.IsNullOrEmpty(address))), "<br />", String.Empty), row.Address5)
                End If
                litOfficeAddress.Text = address
                If Not (String.IsNullOrEmpty(row.Phone)) Then
                    spanPhone.Visible = True
                    litOfficePhone.Text = row.Phone
                End If
                If Not (String.IsNullOrEmpty(row.Email)) Then
                    spanEmail.Visible = True
                    BindEmailsToOffice(row.Email, phOfficeEmail)
                End If
                If Not (String.IsNullOrEmpty(row.Web)) Then
                    spanWebSite.Visible = True
                    hlkOfficeWebsite.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", row.Web.Replace("https://", String.Empty).Replace("http://", String.Empty)))
                    hlkOfficeWebsite.Text = row.Web
                    hlkOfficeWebsite.NavigateUrl = row.Web
                End If
                If Not (String.IsNullOrEmpty(row.PhoneSupport)) Then
                    spanPhoneSupport.Visible = True
                    litOfficePhoneSupport.Text = row.PhoneSupport
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If Not (String.IsNullOrEmpty(Request.QueryString("mseries"))) Then
            Dim productSeriesId As Int32 = -1
            Integer.TryParse(Request.QueryString("mseries"), productSeriesId)
            If (productSeriesId <= 0) Then
                HandleBouncedRedirect()
            End If
            If Not (FeContactManager.IsProductSeriesIdInSuppliedString(productSeriesId, ConfigurationManager.AppSettings("AllowWhereToBuyForTheseSeries"))) Then
                HandleBouncedRedirect()
            End If
            _productSeriesId = productSeriesId
        ElseIf Not (String.IsNullOrEmpty(Request.QueryString("groupid"))) Then
            Dim groupId As Int32 = -1
            Integer.TryParse(Request.QueryString("groupid"), groupId)
            If (groupId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productGroupId = groupId
        Else
            HandleBouncedRedirect()
        End If
    End Sub

    Private Sub InjectJsAndCss()
        Dim contactCss As HtmlLink = New HtmlLink()
        contactCss.Href = ResolveUrl(ConfigurationManager.AppSettings("CssContactUs"))
        contactCss.Attributes.Add("rel", "stylesheet")
        contactCss.Attributes.Add("type", "text/css")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(contactCss)
    End Sub

    Private Sub BindDropDown()
        DropDownListManager.BindDropDown(Of Country)(ddlCountry, CountryRepository.FetchCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM COUNTRY WHERE ISO_CODE NOT IN (SELECT ISO_CODE FROM GEOIP_DENIED_COUNTRY WHERE ACTIVE_YN = 'Y')", New List(Of SqlParameter)().ToArray()).ToList(), "NAME", "COUNTRYID", False, String.Empty, String.Empty)
        Dim data As List(Of ContactOffice) = GetContactOfficeData(False)
        If (data.Count > 0) Then
            Dim countryIds As List(Of Int32) = data.Where(Function(x) x.ActiveYN.ToLower() = "y" And x.HideOnWhereToBuy.ToLower() = "n").Select(Function(x) x.CountryIdWeb.Value).Distinct().ToList()
            Dim deleteCollection As List(Of ListItem) = New List(Of ListItem)
            For Each ddl As ListItem In ddlCountry.Items
                If Not (countryIds.Contains(ddl.Value)) Then
                    Dim li As ListItem = ddlCountry.Items.FindByValue(ddl.Value)
                    deleteCollection.Add(li)
                End If
            Next
            For Each dc As ListItem In deleteCollection
                ddlCountry.Items.Remove(dc)
            Next
            If (ddlCountry.Items.Count <= 0) Then
                Response.Redirect("~/default.aspx")
            End If

            Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
            If (countryCode > 0) Then
                If Not (ddlCountry.Items.FindByValue(countryCode.ToString()) Is Nothing) Then ddlCountry.SelectedValue = countryCode.ToString()
            End If
        End If
    End Sub

    Private Sub BindPage()
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
        If (productSeries Is Nothing Or Not (productSeries.ProductSeriesId = _productSeriesId)) Then
            HandleBouncedRedirect()
        End If
        lblSeriesName.Text = productSeries.Name
        imgSeries.ImageUrl = productSeries.ProductImage
        imgSeries.AlternateText = productSeries.Name
        litSeriesDescription.Text = productSeries.Description
        BindRepeater()
    End Sub

    Private Function GetDictionaryOfDefaultDistributorsForRegionAndCountry() As Dictionary(Of String, ContactOffice)
        Dim retVal As Dictionary(Of String, ContactOffice) = New Dictionary(Of String, ContactOffice)
        If (DetermineCategoryId() = CategoryIds.PROTOCOL_ANALYZERS) Then Return retVal

        Dim contactOffices As List(Of ContactOffice) = ContactOfficeRepository.GetContactOffices(ConfigurationManager.AppSettings("ConnectionString").ToString()).Where(Function(x) x.ActiveYN.ToUpper() = "Y").ToList()
        Dim defaultDistibutors As String = ConfigurationManager.AppSettings("DefaultDistributorsForRegion")
        If Not (String.IsNullOrEmpty(defaultDistibutors)) Then
            Dim distributors As String() = defaultDistibutors.Split(",")
            For Each distributor As String In distributors
                If Not (retVal.ContainsKey(distributor)) Then
                    Dim regionCountryId As String() = distributor.Split("|")
                    retVal.Add(distributor, contactOffices.Where(Function(x) x.OfficeId = regionCountryId(1)).FirstOrDefault())
                End If
            Next
        End If
        Return retVal
    End Function

    Private Function GetContactOfficeData(ByVal filterToCountry As Boolean) As List(Of ContactOffice)
        Dim data As List(Of ContactOffice) = FeContactManager.GetContactOfficesForProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId, "D")
        If (filterToCountry) Then data = data.Where(Function(x) x.CountryIdWeb = Int32.Parse(ddlCountry.SelectedValue) And x.HideOnWhereToBuy.ToLower() = "n").ToList()
        Dim regionId As Int32 = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(ddlCountry.SelectedValue)).RegionId
        Dim defaultDistributors As Dictionary(Of String, ContactOffice) = GetDictionaryOfDefaultDistributorsForRegionAndCountry()
        Dim d2 As Dictionary(Of String, ContactOffice) = defaultDistributors.Where(Function(k) k.Key.StartsWith(String.Format("{0}|", regionId))).ToDictionary(Function(x) x.Key, Function(x) x.Value)
        Dim ide0 As IDictionaryEnumerator = d2.GetEnumerator()
        While (ide0.MoveNext())
            Dim hasThisGlobalDistributor As Boolean = data.Where(Function(x) x.Company.ToUpper() = CType(ide0.Value, ContactOffice).Company.ToUpper()).Count() > 0
            If Not (hasThisGlobalDistributor) Then
                Dim splitter As String() = ide0.Key.ToString().Split("|")
                Dim officeId As Int32 = Int32.Parse(splitter(1))
                Dim contactOffices As List(Of ContactOffice) = New List(Of ContactOffice)({defaultDistributors(String.Format("{0}|{1}", regionId, officeId))})
                data.Add(contactOffices.FirstOrDefault())
            End If
        End While
        Return data
    End Function

    Private Sub BindRepeater()
        Dim data As List(Of ContactOffice) = GetContactOfficeData(True)
        If (data.Count > 0) Then
            divNoLocations.Visible = False
            divLocations.Visible = True
            rptLocations.DataSource = ConsolidateContactOfficesIfDuplicate(data.OrderByDescending(Function(x) x.IsFeatured).ThenBy(Function(x) x.Company).ToList())
            rptLocations.DataBind()
        Else
            divLocations.Visible = False
            divNoLocations.Visible = True
        End If
    End Sub

    Private Function DetermineCategoryId() As Int32
        If (_productSeriesId.HasValue) Then
            Dim protocolStandardSeries As List(Of ProtocolStandardSeries) = ProtocolStandardSeriesRepository.GetProtocolStandardSeriesByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value)
            If (protocolStandardSeries.Count > 0) Then Return CategoryIds.PROTOCOL_ANALYZERS

            Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeriesAndCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), CategoryIds.PROTOCOL_ANALYZERS, _productSeriesId.Value)
            If (productSeriesCategories.Count > 0) Then Return CategoryIds.PROTOCOL_ANALYZERS
        End If
        If (_productGroupId.HasValue) Then
            Dim productGroups As List(Of ProductGroup) = ProductGroupRepository.GetAllProductGroupsWithCategoryId(ConfigurationManager.AppSettings("ConnectionString").ToString(), CategoryIds.PROTOCOL_ANALYZERS)
            If (productGroups.Select(Function(x) x.GroupId).Distinct().ToList().Contains(_productGroupId.Value)) Then Return CategoryIds.PROTOCOL_ANALYZERS
        End If
        Return CategoryIds.OSCILLOSCOPES    ' Return something...
    End Function

    Private Function ConsolidateContactOfficesIfDuplicate(ByVal data As List(Of ContactOffice)) As IList(Of ConsolidatedContactOffice)
        Dim temps As List(Of ConsolidatedContactOffice) = New List(Of ConsolidatedContactOffice)
        Dim offices As List(Of ContactOffice) = data.Distinct().ToList()
        Dim officeNames As List(Of String) = offices.Select(Function(x) x.Company).Distinct().ToList()
        For Each officeName In officeNames
            Dim temp As ConsolidatedContactOffice = New ConsolidatedContactOffice()
            Dim contactOffices As List(Of ContactOffice) = offices.Where(Function(x) x.Company.ToUpper() = officeName.ToUpper()).OrderByDescending(Function(x) x.IsFeatured).ThenBy(Function(x) x.Company).ToList()
            temp.Name = contactOffices.FirstOrDefault().Company
            temp.NameSubHeading = contactOffices.FirstOrDefault().CompanySubHead
            temp.ContactOffices = data.Where(Function(x) x.Company.ToUpper() = officeName.ToUpper()).ToList()
            temps.Add(temp)
        Next
        Return temps
    End Function

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/")
    End Sub

    Private Sub BindEmailsToOffice(ByVal emailAddress As String, ByRef ph As PlaceHolder)
        If (emailAddress.Contains("(") And emailAddress.Contains(")")) Then
            If (emailAddress.Contains(";")) Then
                Dim emails As String() = emailAddress.Split(";")
                For i As Int32 = 0 To emails.Length - 1
                    Dim hlk As HyperLink = New HyperLink()
                    hlk.Text = IIf(i = emails.Length - 1, emails(i), String.Format("{0}, ", emails(i)))
                    hlk.NavigateUrl = String.Format("mailto:{0}", emails(i).Substring(0, emails(i).IndexOf("(")).Trim())
                    ph.Controls.Add(hlk)
                Next
            Else
                Dim hlk As HyperLink = New HyperLink()
                hlk.Text = emailAddress
                hlk.NavigateUrl = String.Format("mailto:{0}", emailAddress.Substring(0, emailAddress.IndexOf("(")).Trim())
                ph.Controls.Add(hlk)
            End If
        Else
            If (emailAddress.Contains(";")) Then
                Dim emails As String() = emailAddress.Split(";")
                For i As Int32 = 0 To emails.Length - 1
                    Dim hlk As HyperLink = New HyperLink()
                    hlk.Text = IIf(i = emails.Length - 1, emails(i), String.Format("{0}, ", emails(i)))
                    hlk.NavigateUrl = String.Format("mailto:{0}", emails(i).Trim())
                    ph.Controls.Add(hlk)
                Next
            Else
                Dim hlk As HyperLink = New HyperLink()
                hlk.Text = emailAddress
                hlk.NavigateUrl = String.Format("mailto:{0}", emailAddress)
                ph.Controls.Add(hlk)
            End If
        End If
    End Sub
#End Region
End Class

Public Class ConsolidatedContactOffice
    Public Name As String
    Public NameSubHeading As String
    Public ContactOffices As List(Of ContactOffice)
End Class