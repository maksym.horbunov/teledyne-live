﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_DSOsecMore" Codebehind="DSOsecMore.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
	    <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_security.gif" alt="Oscilloscope Security"></p>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>&nbsp;</td>
                <td>
                    <table border="1" cellpadding="6" cellspacing="0">
                        <tr><td colspan="2"><br /><a href="dsosecurity.aspx<%=menuURL2 %>">&lt; Back to DSO Security page</a><br /></td></tr>
                        <tr><td colspan="2"><span>Microsoft Monthly Patch Archive for LeCroy X-Stream Digital Oscilloscopes</span></td></tr>
                        <tr><td valign="top">February 2012:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">January 2012:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">December 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">November 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">October 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">September 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">August 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">July 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th June 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th May 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th April 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th March 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th February 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th January 2011:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th December 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th November 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th October 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th September 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th August 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th July 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th June 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th May 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th April 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th March 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th February 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th January 2010:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th December 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th November 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th October 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th September 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th August 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th July 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th June 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th May 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th April 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th March 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th February 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th January 2009:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th December 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th November 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th October 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th September 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th August 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th July 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th June 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th May 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th Apr 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th Mar 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th Feb 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th Jan 2008:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th Dec 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">13th Nov 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th Oct 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th Sep 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th Aug 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th July 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th June 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">8th May 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th Apr 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th Mar 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th Feb 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">9th Jan 2007:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th Dec 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th Nov 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">10th Oct 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">12th Sep 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th Aug 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.<br><a href="http://www.dhs.gov/dhspublic/display?content=5789" onclick="GaEventPush('OutboundLink', 'www.dhs.gov/dhspublic/display?content=5789');">The U.S. Department of HomelandSecurity Recommends Security Patch to Protect Against a Vulnerability Found In WindowsOperating Systems</a></td></tr>
                        <tr><td valign="top">14th July 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">23rd June 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">19th May 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">14th Apr 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">17th Mar 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">17th Feb 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">20th Jan 2006:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">16th Dec 2005:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">11th Nov 2005:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">17th Aug 2005:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.<br><br>The latest updates will help protect against a newly detected worm that can infectWindows 2000 and some Windows XP based systems. More information is available from Microsoft <a href="http://www.microsoft.com/technet/security/advisory/899588.mspx" onclick="GaEventPush('OutboundLink', 'www.microsoft.com/technet/security/advisory/899588.mspx');">here.</a></td></tr>
                        <tr><td valign="top">27th June 2005:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.</td></tr>
                        <tr><td valign="top">18th May 2005:&nbsp;</td><td>Microsoft monthly patches validated with LeCroy Oscilloscopes.All Digital Oscilloscopes now shipping with Windows XP Service Pack 2, with theexception of the WaveSurfer, which runs XP Embedded, Service Pack 1.</td></tr>
                        <tr><td valign="top">16th Nov 2004:&nbsp;</td><td>All Monthly patches from Microsoft validated with LeCroy Digital Oscilloscopes</td></tr>
                        <tr><td valign="top">12th Oct 2004:&nbsp;</td><td>WaveRunner 6kA launched with Windows XP Professional SP2</td></tr>
                        <tr><td valign="top">6th Aug 2004:&nbsp;</td><td>Microsoft Releases XP SP2, Not compatible with XP Embedded shipped in WaveSurfers,do not update WaveSurfers with this service pack.</td></tr>
                        <tr><td valign="top">31st March 2004:&nbsp;</td><td>WaveSurfer launched with Windows XP Embedded (SP1)</td></tr>
                        <tr><td valign="top">1st March 2003:&nbsp;</td><td>WavePro launched with Windows 2000</td></tr>
                        <tr><td valign="top">3rd March 2002:&nbsp;</td><td>WaveMaster launched with Windows 2000</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources"></div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>