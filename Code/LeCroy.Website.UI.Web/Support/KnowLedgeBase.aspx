﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_KnowLedgeBase" Codebehind="KnowLedgeBase.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <asp:Literal ID="menulabel" runat="server" />
                <asp:Label ID="Label1" runat="server" />
            </ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_faq.gif" alt="FAQ/Knowledgebase"></p>
        <asp:Panel panel ID="plCategory" runat="server" Visible="false">
            <p>Please select the Knowledge Base of your preference. NOTE: FAQ/Knowledge Base requires that you are a valid registered user at the Teledyne LeCroy web site.</p>
                    <p>Teledyne LeCroy’s FAQ/Knowledgebase has been created to help answer some commonly asked questions, and includes the latest technical questions and inquiries addressed by our Applications Engineers.</p>
        </asp:Panel>
    </div>
    <div class="tabs">
        <div class="bg">
            <div class="tab"></div>
            <asp:Panel ID="plCategory1" runat="server" Visible="false">
                <div class="overview">
                    <h2><a href="KnowledgeBase.aspx?typeid=1<%=menuURL %>">Oscilloscope</a></h2>
                    <p>Includes the latest and most frequently asked questions regarding WaveAce Series, WaveJet Series, WaveSurfer Series, HDO Series, WaveRunner Series, WavePro Series, WaveMaster Series, Labmaster Series, Serial Data Analyzers (SDA), Disk Drive Analyzers (DDA), and Other Products.</p>
                </div>
            </asp:Panel>
            <asp:Panel ID="plCategory2" runat="server" Visible="false">
                <div class="overview">
                    <h2><a href="KnowledgeBase.aspx?typeid=2<%=menuURL %>">Protocol Solutions</a></h2>
                    <p>Includes the latest and most frequently asked questions regarding protocol products covering Bluetooth, DDR3 & DDR4, Ethernet, Fibre Channel, IEEE1394, Infiniband, PCI Express, PCI/PCI-X, SAS, SATA, USB, and others.</p>
                </div>
            </asp:Panel>
            <asp:Panel ID="plCategory3" runat="server" Visible="false">
                <div class="overview">
                    <h2><a href="KnowledgeBase.aspx?typeid=3<%=menuURL %>">SPARQ Signal Integrity Network Analyzers</a></h2>
                    <p>Includes the latest and most frequently asked questions regarding the SPARQ Signal Integrity Network Analyzer.</p><br />
                </div>
            </asp:Panel>
            <asp:Panel ID="plSearch" runat="server" Visible="false">
                <table>
                    <tr>
                        <td>Select Product Group<br><asp:DropDownList ID="ddlGroup" runat="server" /></td>
                        <td><asp:Label ID="lblCategory" runat="server" Text="Select Category" Visible="False" /><br><asp:DropDownList ID="ddlCategory" runat="server" Visible="False" /></td>
                    </tr>
                    <tr><td colspan="2">Type Key Words:&nbsp;<asp:TextBox ID="tbxKey" runat="server" size="10" />&nbsp;&nbsp;<asp:Button ID="btnGo" runat="server" Text="Go" /></td></tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <asp:Label ID="resultLB" runat="server" />
                <div class="overview">
                    <div class="overviewLeft"><strong><asp:Label ID="result1LB" runat="server" /></strong></div>
                    <div style="text-align: right"><asp:LinkButton ID="btn_pre" runat="server" Text="<img border='0' src='../images/icons/icons_rgt_nav_arrow.gif'> Previous 10 results" />
                        &nbsp;&nbsp;<asp:LinkButton ID="btn_next" runat="server" Text="next 10 results <img border='0' src='../images/icons/icons_lft_nav_arrow.gif'>" />
                    </div>
                    <div class="searchResults">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="cell"><a href="KnowledgeBase.aspx?docid=<%#Eval("faq_id")%>&typeid=<%#Eval("kb_type_id")%><%=menuURL %>"><%#Eval("problem")%></a></div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="plDetail" runat="server" Visible="false">
                <div class="overview"> <br/>
                    <br/><br/>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><h1>Topic:</h1></td></tr>
                        <tr><td valign="top"><%=topic%></td></tr>
                    </table>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><h1>Problem:</h1></td></tr>
                        <tr><td valign=top><%=problem%></td></tr>
                    </table>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><h1>Solution:</h1></td></tr>
                        <tr><td valign="top"><%=solution%></td></tr>
                    </table>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><h1>Attachments:</h1></td></tr>
                        <tr><td valign="top"><p>(All support files listed here have been scanned and are virus free)</p></td></tr>
                        <asp:Repeater ID="rptAttach" runat="server">
                            <ItemTemplate>
                                <tr><td valign="top"><%#Container.DataItem%></td></tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr><td valign="top"><strong>Product Group:</strong></td></tr>
                                    <tr><td valign="top"><%=productGroup %></td></tr>
                                </table><br />
                            </td>
                            <td>&nbsp;</td>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr><td valign="top"><strong>Model Numbers:</strong></td></tr>
                                    <tr><td valign="top"><%=modelNumber %></td></tr>
                                </table><br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr><td valign="top"><strong>Categories:</strong></td></tr>
                                    <tr><td valign="top"><%=category %></td></tr>
                                </table><br />
                            </td>
                            <td>&nbsp;</td>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr><td valign="top"><strong>Subcategories:</strong></td></tr>
                                    <tr><td valign="top"><%=subcategory%></td></tr>
                                </table><br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr><td class="relatedfaqs" align="center"><img src="/images/icons/icon_small_arrow_left.gif" />&nbsp;<a href="javascript:history.go(-1)"><strong>Back to Search Results</strong></a></td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td align="center">
                                <div style="overflow: auto; width: 300; height: 600; border-left: 1px gray solid; border-bottom: 1px gray solid; padding: 0px; margin: 0px">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <asp:Repeater ID="rptLinks" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td width="20"></td>
                                                    <td width="10" valign="top"><img src="<%=rootdir %>/Support/images/arrow.gif" /></td>
                                                    <td class="links" valign="top"><br /><br /><%#Container.DataItem%></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table><br /><br />
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>