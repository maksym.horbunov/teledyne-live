﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_KnowLedgeBase
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Public typeid As String = ""

    Public topic As String = ""
    Public problem As String = ""
    Public solution As String = "'"

    Public productGroup As String = ""
    Public modelNumber As String = ""
    Public category As String = ""
    Public subcategory As String = ""
    Public osciCurrent As String = ""
    Public protocolCurrent As String = ""
    Public SPARQCurrent As String = ""
    Public menuURL2 As String = ""
    Public menuURL As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "FAQ Knowledgebase"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("typeid") Is Nothing And Len(Request.QueryString("typeid")) > 0 Then
            If Not IsNumeric(Request.QueryString("typeid")) Then
                Response.Redirect("default.aspx")
            End If
        End If
        If Request.QueryString("typeid") = 1 Then
            protocolCurrent = ""
            osciCurrent = "current"
            SPARQCurrent = ""
        ElseIf Request.QueryString("typeid") = 2 Then
            protocolCurrent = "current"
            osciCurrent = ""
            SPARQCurrent = ""
        ElseIf Request.QueryString("typeid") = 3 Then
            protocolCurrent = ""
            osciCurrent = ""
            SPARQCurrent = "current"
        End If
        Dim captionID As String = Request.QueryString("capid")
        Dim subMenuID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.FAQ_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.FAQ_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID

        Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
        lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If Not Me.IsPostBack And Not (Request.QueryString("typeid")) Is Nothing And Len(Request.QueryString("typeid")) > 0 Then
            Dim key As String = ""
            Dim category As String = ""
            Dim group As String = ""
            key = Request.QueryString("key")
            category = Request.QueryString("category")
            group = Request.QueryString("group")

            Dim mNum As Integer
            Dim i As Integer
            Dim sqlKeys As String = String.Empty
            Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
            dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(getProductGroups(Request.QueryString("typeid").ToString())), "PRODGROUPID", sqlKeys, sqlParameters)
            strSQL = String.Format("Select distinct * from KB_PRODUCTGROUP where product_group_id in ({0}) order by product_group_name", sqlKeys)
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            mNum = ds.Tables(0).Rows.Count
            ddlGroup.Items.Add(New ListItem("All", ""))
            If mNum > 0 Then
                For i = 0 To mNum - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    ddlGroup.Items.Add(New ListItem(dr("product_group_name").ToString(), dr("product_group_id").ToString()))
                    Me.ddlGroup.SelectedIndex = Me.ddlGroup.Items.IndexOf(Me.ddlGroup.Items.FindByValue(group))
                Next
            End If

            If Len(getCategories(Request.QueryString("typeid"))) > 0 Then
                sqlKeys = String.Empty
                sqlParameters = New List(Of SqlParameter)
                dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(getCategories(Request.QueryString("typeid").ToString())), "CATID", sqlKeys, sqlParameters)
                strSQL = String.Format("Select distinct * from KB_CATEGORY where category_id in ({0}) order by category_name", sqlKeys)
                'Response.Write(strSQL)
                'Response.End()
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                mNum = ds.Tables(0).Rows.Count
                ddlCategory.Items.Add(New ListItem("All", ""))
                If mNum > 0 Then
                    For i = 0 To mNum - 1
                        Dim dr As DataRow = ds.Tables(0).Rows(i)
                        ddlCategory.Items.Add(New ListItem(dr("category_name").ToString(), dr("category_id").ToString()))
                    Next
                End If
                ddlCategory.Visible = True
                lblCategory.Visible = True
                Me.ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(category))
            End If
            Me.tbxKey.Text = key
        End If
        plCategory.Visible = True
        plCategory1.Visible = True
        plCategory2.Visible = True
        plCategory3.Visible = True
        Dim typeid As String = ""

        Dim docid As String = ""
        '** typeid
        If Len(Request.QueryString("typeid")) > 0 Then
            If IsNumeric(Request.QueryString("typeid")) Then
                typeid = Request.QueryString("typeid")
            End If
        End If
        '** docid
        If Len(Request.QueryString("docid")) > 0 Then
            If IsNumeric(Request.QueryString("docid")) Then
                docid = Request.QueryString("docid")
            End If
        End If
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Knowledge Base"
        If typeid <> "" And docid = "" Then
            bind(typeid)

            Panel1.Visible = True
            plCategory.Visible = False
            plSearch.Visible = True

            If typeid = 1 Then
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Knowledge Base - Oscilloscopes"
                plCategory1.Visible = True
                plCategory2.Visible = False
                plCategory3.Visible = False
            ElseIf typeid = 2 Then
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Knowledge Base - Protocol Solutions"
                plCategory1.Visible = False
                plCategory2.Visible = True
                plCategory3.Visible = False
            ElseIf typeid = 3 Then
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Knowledge Base - SPARQ"
                plCategory1.Visible = False
                plCategory2.Visible = False
                plCategory3.Visible = True
            End If
        End If

        If docid <> "" Then
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Knowledge Base - Detail"
            plSearch.Visible = True
            Me.plCategory.Visible = False
            If typeid <> "" Then
                If typeid = 1 Then
                    Me.plCategory1.Visible = True
                    Me.plCategory2.Visible = False
                    Me.plCategory3.Visible = False
                ElseIf typeid = 2 Then
                    Me.plCategory1.Visible = False
                    Me.plCategory2.Visible = True
                    Me.plCategory3.Visible = False
                ElseIf typeid = 3 Then
                    Me.plCategory1.Visible = False
                    Me.plCategory2.Visible = False
                    Me.plCategory3.Visible = True
                End If
            End If
            Dim strProductGroupID As String = ""
            Dim strModelID As String = ""
            Dim strCategoryID As String = ""
            Dim strSubCatID As String = ""
            Dim attachment As String = ""

            plDetail.Visible = True
            'Dim parameters As IDataParameter = New DataList()
            'parameters.ParameterName = "faq_id"
            'parameters.Value = docid
            'DbHelperSQL.RunProcedure("usp_faq", parameters)
            strSQL = "select *  from KB_FAQ where POST_yn='Y' and KB_FAQ.faq_id = @FAQID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@FAQID", docid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                topic = dr("topic").ToString()
                problem = dr("problem").ToString()

                strProductGroupID = dr("product_group_id").ToString()
                strModelID = dr("model_id").ToString()
                strCategoryID = dr("category_id").ToString()
                strSubCatID = dr("subcategory_id").ToString()
                attachment = dr("attachment").ToString()

                solution = GetLongDescription("KB_FAQ", "SOLUTION", docid)
            End If
            solution = GetLongDescription("KB_FAQ", "SOLUTION", docid)
            If solution.Length < 0 Then
                solution = "There are no solutions for this problem."
            End If

            'attachment
            If attachment.Length > 0 Then
                getAttachment(attachment)
            End If


            If strProductGroupID.Length > 0 Then
                productGroup = getProductGroup(strProductGroupID)
            End If

            If strModelID.Length > 0 Then
                modelNumber = getModelNumber(strModelID)
            End If

            If strCategoryID.Length > 0 Then
                category = getCategory(strCategoryID)
            End If

            If strSubCatID.Length > 0 Then
                subcategory = getSubcategory(strSubCatID)
            End If

            getLinks(docid)

        End If
    End Sub

    Private Sub bind(ByVal typeid As String)
        Dim strSQL As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim strWh As String = " AND 1=1 "
        If tbxKey.Text.Length > 0 Then
            strWh = strWh + "AND problem like @PROBLEMLIKE "
            sqlParameters.Add(New SqlParameter("@PROBLEMLIKE", String.Format("%{0}%", SQLStringWithOutSingleQuotes(tbxKey.Text))))
        End If
        If ddlCategory.SelectedValue <> "" Then
            strWh = strWh + " AND category_id like @CATEGORYID "
            sqlParameters.Add(New SqlParameter("@CATEGORYID", String.Format("%{0}%", ddlCategory.SelectedValue)))
        End If
        If ddlGroup.SelectedValue <> "" Then
            strWh = strWh + " AND product_group_id like @PRODUCTGROUPID "
            sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", String.Format("%{0}%", ddlGroup.SelectedValue)))
        End If

        strSQL = "SELECT faq_id, problem,kb_type_id FROM kb_faq " +
              " WHERE kb_type_id=@KBTYPEID and POST_yn='Y' " + strWh +
              " order by sort_id, date_entered desc"
        ' Response.Write(strSQL)
        sqlParameters.Add(New SqlParameter("@KBTYPEID", typeid))
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim count As Integer = ds.Tables(0).Rows.Count
        GridView1.DataSource = ds
        GridView1.DataBind()
        Me.resultLB.Text = "<span class='blue'>Search Results:</span> Your search found <strong>" + _
        count.ToString() + "</strong> matches. "
        Me.result1LB.Text = ""
        If count > 0 Then
            Me.resultLB.Text += "Page <strong>" + (Me.GridView1.PageIndex + 1).ToString() + _
                    "</strong> of <strong>" + (Me.GridView1.PageCount).ToString() + "</strong> pages."
            Me.result1LB.Text = "Results "
            If ((Me.GridView1.PageIndex + 1) = (Me.GridView1.PageCount)) Then
                Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + count.ToString()
            Else
                Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + (Me.GridView1.PageIndex * 10 + 10).ToString()
            End If
        End If


        'Me.result1LB.Text = (Me.GridView1.PageIndex * 10).ToString() + "-" + (Me.GridView1.PageIndex * 10 + 10).ToString()

        If count > 10 Then
            If GridView1.PageIndex.ToString().Equals("0") Then
                btn_pre.Visible = False
            Else
                btn_pre.Visible = True
            End If

            If GridView1.PageIndex.ToString().Equals((GridView1.PageCount - 1).ToString()) Then
                btn_next.Visible = False
            Else
                btn_next.Visible = True
            End If
        Else
            btn_next.Visible = False
            btn_pre.Visible = False
        End If

    End Sub



    Protected Function getProductGroups(ByVal typeid As String) As String
        Dim strSQL As String = ""
        Dim ds As DataSet
        Dim strProductGroups As String = ""
        If Len(typeid) > 0 Then
            strSQL = "select distinct product_group_id from KB_FAQ where product_group_id is not null and post_YN='y' and kb_type_id=@KBTYPEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@KBTYPEID", typeid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Len(strProductGroups) = 0 Then
                        strProductGroups = dr("product_group_id")
                    Else
                        strProductGroups = strProductGroups + "," + dr("product_group_id")
                    End If
                Next
            End If
        End If
        getProductGroups = strProductGroups
    End Function

    Protected Function getCategories(ByVal typeid As String) As String
        Dim strSQL As String = ""
        Dim ds As DataSet
        Dim rlt As String = ""
        If Len(typeid) > 0 Then
            strSQL = "select distinct category_id from KB_FAQ where category_id is not null and kb_type_id=@KBTYPEID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@KBTYPEID", typeid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Len(rlt) = 0 Then
                        rlt = dr("category_id")
                    Else
                        rlt = rlt + "," + dr("category_id")
                    End If
                Next
            End If
        End If
        getCategories = rlt
    End Function

    Protected Function GetLongDescription(ByVal tablename As String, ByVal columnname As String, ByVal idintable As Integer) As String
        GetLongDescription = ""
        Dim strSQL As String = ""
        Dim num As Integer = 0
        If tablename.Length > 0 And columnname.Length > 0 And idintable > 0 Then
            strSQL = "Select TEXT from LONGDESCR where TABLE_ID=@TABLEID And COLUMN_ID = @COLUMNID And idintable = @IDINTABLE Order by SEQ_ID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", GetTableIdOnTablename(tablename)))
            sqlParameters.Add(New SqlParameter("@COLUMNID", GetColumnIDOnColumnname(columnname)))
            sqlParameters.Add(New SqlParameter("@IDINTABLE", idintable.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            num = ds.Tables(0).Rows.Count
            If num > 0 Then
                Dim LongDescription As String = ""
                For i As Integer = 0 To num - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    LongDescription = LongDescription + dr("TEXT").ToString()
                Next
                GetLongDescription = LongDescription
            Else
                GetLongDescription = ""
            End If
        Else
            GetLongDescription = ""
        End If
    End Function

    Protected Function GetTableIdOnTablename(ByVal tablename As String) As String
        GetTableIdOnTablename = ""
        If tablename.Length > 0 Then
            strSQL = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLENAME", tablename))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                GetTableIdOnTablename = dr("Table_id").ToString()
            End If
            GetTableIdOnTablename = GetTableIdOnTablename
        End If
    End Function

    Protected Function GetColumnIDOnColumnname(ByVal columnname As String) As String
        GetColumnIDOnColumnname = ""
        If columnname.Length > 0 Then
            strSQL = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", columnname))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                GetColumnIDOnColumnname = dr("Column_id").ToString()
            End If
            GetColumnIDOnColumnname = GetColumnIDOnColumnname
        End If
    End Function

    Protected Function getProductGroup(ByVal strProductGroupID As String) As String
        getProductGroup = ""
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strProductGroupID), "PRODGROUPID", sqlKeys, sqlParameters)
        If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count > 0) Then Return String.Empty
        strSQL = String.Format("Select product_group_name from KB_PRODUCTGROUP where product_group_id in ({0})", sqlKeys)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                If getProductGroup.Length = 0 Then
                    getProductGroup = dr("product_group_name").ToString()
                Else
                    getProductGroup = getProductGroup + ", " + dr("product_group_name").ToString()
                End If
            Next
        End If
    End Function

    Protected Function getModelNumber(ByVal strModelID As String) As String
        getModelNumber = ""
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strModelID), "MODELID", sqlKeys, sqlParameters)
        If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count > 0) Then Return String.Empty
        strSQL = String.Format("Select model_number from KB_MODEL where model_id in ({0})", sqlKeys)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                If getModelNumber.Length = 0 Then
                    getModelNumber = dr("model_number").ToString()
                Else
                    getModelNumber = getModelNumber + ", " + dr("model_number").ToString()
                End If
            Next
        End If
    End Function

    Protected Function getCategory(ByVal strCategoryID As String) As String
        getCategory = ""
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(Functions.ConvertPotentialCommaSeparatedStringIntoList(strCategoryID), "CATID", sqlKeys, sqlParameters)
        If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count > 0) Then Return String.Empty
        strSQL = String.Format("Select category_name from KB_CATEGORY where category_id in ({0})", sqlKeys)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                If getCategory.Length = 0 Then
                    getCategory = dr("category_name").ToString()
                Else
                    getCategory = getCategory + ", " + dr("category_name").ToString()
                End If
            Next
        End If
    End Function

    Protected Function getSubcategory(ByVal strSubCatID As String) As String
        getSubcategory = ""
        If (String.IsNullOrEmpty(strSubCatID)) Then Return String.Empty
        Dim subCategories As List(Of String) = strSubCatID.Split(",").ToList()
        Dim sqlKeys As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Dim dbUtils As Library.DAL.Common.BaseClasses.DALHelperUtils = New Library.DAL.Common.BaseClasses.DALHelperUtils()
        dbUtils.CreateSqlInStatement(subCategories, "SUBCAT", sqlKeys, sqlParameters)
        If (String.IsNullOrEmpty(sqlKeys) And sqlParameters.Count > 0) Then Return String.Empty

        strSQL = String.Format("Select subcategory from KB_SUBCATEGORY where subcategory_id in ({0})", sqlKeys)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                If getSubcategory.Length = 0 Then
                    getSubcategory = dr("subcategory").ToString()
                Else
                    getSubcategory = getSubcategory + ", " + dr("subcategory").ToString()
                End If
            Next
        End If
    End Function

    Protected Sub getAttachment(ByVal strStart As String)
        Dim attachList As ArrayList = New ArrayList()
        Dim listItem As String = ""
        Dim start As Integer = 0
        Dim strFileName As String = ""
        strStart = strStart.Replace(",", " ")
        While Not strStart = ""
            Dim l As Integer = 0
            l = strStart.IndexOf(" ")
            If l < 0 Then
                strFileName = strStart
                strStart = ""
            Else
                strFileName = strStart.Substring(start, l)
                strStart = strStart.Substring(l + 1, strStart.Length - l - 1)
            End If
            Dim imageSrc As String = ""
            imageSrc = getImage(strFileName)
            listItem = "<nobr><img SRC=""" + rootDir + "/Support/images/" + imageSrc + """>&nbsp;<a href='" + ConfigurationManager.AppSettings("AwsPublicBaseUrl") + "/files/knowledgebase/" + strFileName.ToLower() + "' target='_blank'>" + strFileName + "</a></nobr>"
            attachList.Add(listItem)
        End While
        Me.rptAttach.DataSource = attachList
        Me.rptAttach.DataBind()
    End Sub

    Public Function getImage(ByVal strFileName As String) As String
        Dim strSRC As String = ""
        Dim strExt As String = ""
        If strFileName.Substring(strFileName.Length - 2, 1) = "." Then
            If strFileName.Substring(strFileName.Length - 1, 1) = "c" Then
                strSRC = "cdoc.gif"
            Else
                strSRC = "otherdoc.gif"
            End If
        Else
            If strFileName.Substring(strFileName.Length - 4, 1) = "." Then
                strExt = strFileName.Substring(strFileName.Length - 3, 3)
                Select Case strExt
                    Case "xls"
                        strSRC = "excel.gif"
                    Case "doc"
                        strSRC = "worddoc.gif"
                    Case "bmp"
                        strSRC = "bmpdoc.gif"
                    Case "dso"
                        strSRC = "dsodoc.gif"
                    Case "jif"
                        strSRC = "jifdoc.gif"
                    Case "jpg"
                        strSRC = "jpgdoc.gif"
                    Case "pdf"
                        strSRC = "pdfdoc.gif"
                    Case "tif"
                        strSRC = "tifdoc.gif"
                    Case "XLS"
                        strSRC = "excel.gif"
                    Case "DOC"
                        strSRC = "worddoc.gif"
                    Case "BMP"
                        strSRC = "bmpdoc.gif"
                    Case "DSO"
                        strSRC = "dsodoc.gif"
                    Case "JIF"
                        strSRC = "jifdoc.gif"
                    Case "JPG"
                        strSRC = "jpgdoc.gif"
                    Case "PDF"
                        strSRC = "pdfdoc.gif"
                    Case "TIF"
                        strSRC = "tifdoc.gif"
                    Case Else
                        strSRC = "otherdoc.gif"
                End Select
            Else
                strSRC = "otherdoc.gif"
            End If
        End If

        getImage = strSRC
    End Function

    Public Sub getLinks(ByVal docid As String)
        Dim linkList As New ArrayList()
        Dim links_list() As String = Session("Links")
        Dim i As Integer = 0
        If Not links_list Is Nothing Then
            If links_list.Length > 0 Then
                For i = 0 To links_list.Length - 1
                    Dim strHref As String = ""
                    If links_list(i)(0).ToString() <> docid Then
                        If links_list(i)(1).ToString().Length > 100 Then
                            strHref = links_list(i)(1).ToString().Substring(0, 100)
                        Else
                            strHref = links_list(i)(1).ToString()
                        End If
                        strHref = "<a href='KnowledgeBase.aspx?docid=" + links_list(i)(0) + menuURL + "'>" + strHref + "</a>"
                    End If
                    linkList.Add(strHref)
                Next
                Me.rptLinks.DataSource = linkList
                Me.rptLinks.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        typeid = Request.QueryString("typeid")
        Dim key As String = ""
        key = Me.tbxKey.Text
        category = Me.ddlCategory.SelectedValue

        Response.Redirect("KnowledgeBase.aspx?typeid=" + typeid + "&category=" + category + "&key=" + key + "&group=" + Me.ddlGroup.SelectedValue + menuURL)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        bind(Request.QueryString("typeid"))
    End Sub

    Protected Sub btn_pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pre.Click
        Dim p As Integer = Me.GridView1.PageIndex - 1
        If p < 0 Then
            Exit Sub
        End If
        Me.GridView1.PageIndex = p
        bind(Request.QueryString("typeid"))
    End Sub

    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_next.Click
        Dim p As Integer = Me.GridView1.PageIndex + 1
        If p > Me.GridView1.PageCount Then
            Exit Sub
        End If
        Me.GridView1.PageIndex = p
        bind(Request.QueryString("typeid"))
    End Sub
End Class