﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Register_Default" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
	    <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro form-row">
        <p><img src="<%= rootDir%>/images/category/headers/hd_reg.gif" alt="Product Registration"></p>
        <asp:DropDownList ID="ddlCategory" runat="server">
            <asp:ListItem Selected="True" Text="Select your product type" Value="0" />
            <asp:ListItem Text="Oscilloscopes" Value="1" />
            <asp:ListItem Text="Protocol Analyzers and Exercisers" Value="19" />
            <asp:ListItem Text="Mixed Signal Solutions" Value="18" />
            <asp:ListItem Text="Probes" Value="3" />
            <asp:ListItem Text="PeRT3 Systems" Value="24" />
            <asp:ListItem Text="Waveform Generators" Value="2" />
            <asp:ListItem Text="Logic Analyzers" Value="31" />
            <asp:ListItem Text="SPARQ Signal Integrity Network Analyzers" Value="30" />
            <asp:ListItem Text="TDR and S-Parameters" Value="35" />
        </asp:DropDownList>
        <p align="center"><input type="submit" value="Register My Product" name="btnSubmit" style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid" id="btnSubmit" runat="server" /></p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>