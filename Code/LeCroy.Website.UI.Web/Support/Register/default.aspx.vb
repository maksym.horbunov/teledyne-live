﻿Imports System.Data
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Register_Default
    Inherits BasePage
    Dim sql As String = ""
    Dim ds As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Product Registration"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Product Registration"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.PROD_REG_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Private Sub btnSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Select Case ddlCategory.SelectedValue
            Case "1"
                Response.Redirect("../registerscope/" + menuURL2)
            Case "2"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_ARBSTUDIO, menuURL))
            Case "3"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_PROBES, menuURL))
            Case "18"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_MIXEDSIGNAL, menuURL))
            Case "19"
                Response.Redirect("protocolanalyzer.aspx" + menuURL2)
            Case "24"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_PERT3, menuURL))
            Case "30"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_SPARQ, menuURL))
            Case "31"
                Response.Redirect(String.Format("register.aspx?categoryid={0}{1}", AppConstants.CAT_ID_LOGICSTUDIO, menuURL))
            Case "35"
                Response.Redirect(String.Format("register.aspx?categoryid=35{0}", menuURL))
        End Select
    End Sub
End Class