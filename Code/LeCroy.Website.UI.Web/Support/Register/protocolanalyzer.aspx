﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Register_protocolanalyzer" EnableEventValidation="false" Codebehind="protocolanalyzer.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_reg.gif" alt="Product Registration"></p>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="top">
                    <table cellpadding="3" cellspacing="0" border="0">
                        <tr><td valign="top" rowspan="2"></td></tr>
                        <tr>
                            <td>
                                <input type="hidden" name="contactid" value="<% = Session("GUID") %>">
                                <input type="hidden" name="dateentered" value="<% = now() %>">
                                <p><h3><asp:Literal ID="litOne" runat="server" /></h3></p>
                                <p>
                                    <asp:Literal ID="litTwo" runat="server" /><br /><br />
                                    <table cellpadding="0" cellspacing="4" border="0">
                                        <tr><td valign="top"><b><asp:Literal ID="litModelNumber" runat="server" /></b></td></tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:DropDownList ID="ddlModels" runat="server" Width="250px" />&nbsp;<asp:RequiredFieldValidator ID="rfvModels" runat="server" ControlToValidate="ddlModels" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*required" ValidationGroup="vgRegister" /><br /><br />
                                            </td>
                                        </tr>
                                        <tr><td valign="top"><b><asp:Literal ID="litSerialNumber" runat="server" /></b></td></tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="50" />&nbsp;<asp:RequiredFieldValidator ID="rfvSerialNumber" runat="server" ControlToValidate="txtSerialNumber" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*required" ValidationGroup="vgRegister" /><br /><br />
                                            </td>
                                        </tr>
                                        <tr><td valign="top"><b><asp:Literal ID="litPurchasedOptions" runat="server" /></b></td></tr>
                                        <tr><td valign="top"><asp:TextBox ID="txtPurchasedOptions" runat="server" Columns="22" Rows="4" TextMode="MultiLine" /></td></tr>
                                        <tr><td valign="top"><b><asp:Literal ID="litDatePurchased" runat="server" /></b></td></tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="120">
                                                    <tr>
                                                        <td valign="top"><asp:DropDownList ID="ddlDatePurchasedYear" runat="server" /></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td valign="top"><asp:DropDownList ID="ddlDatePurchasedMonth" runat="server" /></td>
                                                        <td width="10">&nbsp;</td>
                                                        <td valign="top"><asp:DropDownList ID="ddlDatePurchasedDay" runat="server" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table><br />
                                    <p><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Register My Product" ValidationGroup="vgRegister" /></p>
                            </td>
                            <td valign="top"></td>
                        </tr>
                    </table><br /><br />
                </td>
            </tr>
            <tr><td colspan="3"></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources"></div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>