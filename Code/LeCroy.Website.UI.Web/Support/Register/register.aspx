﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="register.aspx.vb" Inherits="LeCroy.Website.Support_Register_Register" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%=rootDir%>/images/category/headers/hd_reg.gif" alt="Product Registration" border="0" /></p>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr><td valign="top" rowspan="2"></td></tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnContactId" runat="server" />
                                <asp:HiddenField ID="hdnDateEntered" runat="server" />
                                <p><h3><asp:Literal ID="litPageHeading" runat="server" /></h3></p>
                                <p>
                                    <asp:Literal ID="litPageDescription" runat="server" /><br /><br />
                                    <table cellpadding="0" cellspacing="4" border="0">
                                        <tr><td valign="top"><b><asp:Literal ID="litModelNumber" runat="server" /></b></td></tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:DropDownList ID="ddlModels" runat="server" runat="server" />&nbsp;
                                                    <asp:RequiredFieldValidator ID="rfvModels" runat="server" ControlToValidate="ddlModels" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*required" ValidationGroup="vgRegister" /><br /><br />
                                            </td>
                                        </tr>
                                        <tr><td valign="top"><b><asp:Literal ID="litSerialNumber" runat="server" /></b></td></tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="50" />&nbsp;
                                                    <asp:RequiredFieldValidator ID="rfvSerialNumber" runat="server" ControlToValidate="txtSerialNumber" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*required" ValidationGroup="vgRegister" /><br /><br />
                                            </td>
                                        </tr>
                                    </table><br />
                                    <p><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Register My Product" ValidationGroup="vgRegister" /></p>
                                </td>
                            <td valign="top"></td>
                        </tr>
                    </table><br /><br />
                </td>
            </tr>
            <tr><td colspan="3"></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources"></div>
    <div class="resources"></div>
</asp:Content>