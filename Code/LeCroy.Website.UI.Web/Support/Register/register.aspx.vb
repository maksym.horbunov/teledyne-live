﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class Support_Register_Register
    Inherits BasePage

#Region "Variables/Keys"
    Private menuURL2 As String = String.Empty
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Product Registration"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.SUPPORT_CAPTION_ID
        Dim menuID As String = AppConstants.PROD_REG_MENU
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not (Page.IsPostBack) Then
            Session("RedirectTo") = rootDir + "/support/register/register.aspx" + menuURL2 + "&categoryid=" + Request.QueryString("categoryid")
            If Not ValidateUser() Then
                Response.Redirect(rootDir + "/support/user/")
            End If
            ValidateUserNoRedir()
            BindText()
            BindModelDropDown()
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            'SetFormDataFromPreLoginRedirect()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        'HandleSubmit()
        'Try
        '    TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REGISTER").ToString()))   ' DB record is not needed anymore
        'Catch
        'End Try
        ProcessSubmitRequest()
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindText()
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        litPageHeading.Text = Functions.LoadI18N("SLBCA0473", localeId)
        litPageDescription.Text = Functions.LoadI18N("SLBCA0633", localeId)
        litModelNumber.Text = Functions.LoadI18N("SLBCA0479", localeId)
        litSerialNumber.Text = Functions.LoadI18N("SLBCA0480", localeId)
        btnSubmit.Text = Functions.LoadI18N("SLBCA0528", localeId)
    End Sub

    Private Sub BindModelDropDown()
        Dim categoryId As Int32 = -1
        Int32.TryParse(Request.QueryString("categoryid"), categoryId)
        If (categoryId <= 0) Then Response.Redirect("default.aspx")
        Dim sqlString As String = "SELECT p.PRODUCTID, p.PARTNUMBER, p.DISABLED, p.EOL_YN, p.NAME FROM [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT] p WHERE psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_ID = p.PRODUCTID ORDER BY p.NAME"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@CATEGORYID", categoryId.ToString())})
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (products.Count <= 0) Then Response.Redirect("Default.aspx")

        Dim selectedIndex As Int32 = 0
        For Each product In products
            ddlModels.Items.Add(New ListItem(product.Name, product.ProductId))
        Next
        ddlModels.Items.Insert(0, New ListItem(Functions.LoadI18N("SLBCA0481", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), String.Empty))
        ddlModels.SelectedIndex = selectedIndex
    End Sub

    Function getAnswers(ByVal SessionID As String) As String
        Dim strbody As String = ""
        getAnswers = ""
        If Len(SessionID) > 0 Then
            Dim sql As String = "select max(regid) as REGID from SURVEYANSWERS where sessionid=@SESSIONID"
            Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", SessionID.ToString())})
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If Not dr("REGID") Is Nothing And Len(dr("REGID")) > 0 Then
                    sql = "SELECT SURVEYANSWERS.answer, SURVEYQUESTION.questionname, " & _
                  "SURVEYANSWERS.questionid FROM SURVEYANSWERS INNER JOIN " & _
                  "SURVEYQUESTION ON SURVEYANSWERS.questionid = SURVEYQUESTION.questionid " & _
                  "WHERE sessionid=@SESSIONID and regid=@REGID ORDER BY SURVEYANSWERS.questionid DESC"
                    'response.Write strSQL
                    ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", SessionID.ToString()), New System.Data.SqlClient.SqlParameter("@REGID", dr("REGID").ToString())}).ToArray())

                    For Each drr As DataRow In ds.Tables(0).Rows
                        If Len(drr("questionname")) > 0 Then
                            If CInt(drr("questionid")) = 1 Or CInt(drr("questionid")) = 2 Or CInt(drr("questionid")) = 3 Or CInt(drr("questionid")) = 4 Or CInt(drr("questionid")) = 5 Or CInt(drr("questionid")) = 6 Or CInt(drr("questionid")) = 7 Then
                                strbody = strbody & "  " & LoadI18N("SLBCA0517", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                            Else
                                strbody = strbody & "  " & translation_webdb32(drr("questionid"))
                            End If
                        Else
                            strbody = strbody & "  "
                        End If
                        If Len(drr("answer")) > 0 Then
                            If CInt(drr("questionid")) = 8 Then
                                Select Case CStr(drr("answer"))
                                    Case "1"
                                        strbody = strbody & " : " & LoadI18N("SLBCA0483", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                    Case "2"
                                        strbody = strbody & " : " & LoadI18N("SLBCA0484", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                    Case "3"
                                        strbody = strbody & " : " & LoadI18N("SLBCA0485", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                    Case "4"
                                        strbody = strbody & " : " & LoadI18N("SLBCA0486", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                    Case "5"
                                        strbody = strbody & " : " & LoadI18N("SLBCA0487", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                End Select
                            ElseIf CInt(drr("questionid")) = 1 Or CInt(drr("questionid")) = 2 Or CInt(drr("questionid")) = 4 Or CInt(drr("questionid")) = 5 Or CInt(drr("questionid")) = 6 Or CInt(drr("questionid")) = 7 Then
                                strbody = strbody & " : " & translation_webdb32(drr("questionid"))
                            Else
                                strbody = strbody & " : " & drr("answer")
                            End If
                        Else
                            strbody = strbody
                        End If
                        strbody = strbody & "  " & Chr(13) & Chr(10)
                    Next
                    getAnswers = strbody
                End If
            End If
        End If
    End Function

    Function translation_webdb32(ByVal id) As String
        Dim sql As String = String.Empty
        Dim ds As DataSet = New DataSet()
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))

        If (localeId = 1031 Or localeId = 1040 Or localeId = 1036) Then
            localeId = 1033
        End If

        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If CInt(localeId) = 1033 Then
            sql = "Select questionname as VALUE from SURVEYQUESTION where questionid=@ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", id.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                translation_webdb32 = dr("VALUE")
            Else
                translation_webdb32 = ""
            End If
        Else
            'select tableid from table "TABLE_ID" (ecatalog)
            sql = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLENAME", "SURVEYQUESTION"))
            'Response.Write strSQL
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            Dim TableID As String = "", ColID As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                TableID = dr("Table_ID")
            End If

            sql = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", "questionname"))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                ColID = dr("Column_ID")
            End If

            sql = "Select VALUE from TABLESBYLANGUAGE where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@ID and LOCALEID=@LOCALEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
            sqlParameters.Add(New SqlParameter("@COLUMNID", ColID))
            sqlParameters.Add(New SqlParameter("@ID", id.ToString()))
            sqlParameters.Add(New SqlParameter("@LOCALEID", localeId.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                translation_webdb32 = dr("Value")
            Else
                sql = "Select questionname as VALUE from SURVEYQUESTION where questionid=@ID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@ID", id.ToString()))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    translation_webdb32 = Trim(dr("VALUE"))
                Else
                    translation_webdb32 = ""
                End If
            End If
        End If
    End Function

    'Private Sub SetFormDataFromPreLoginRedirect()
    '    If Not (Session("TECHHELPTEMPSTATEID_REGISTER") Is Nothing) Then
    '        Dim techHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REGISTER").ToString()))
    '        If (techHelpTempState Is Nothing) Then
    '            Return
    '        End If
    '        If Not (techHelpTempState.TechHelpTempStateId = Guid.Empty) Then    ' Valid, bind page controls
    '            If Not (String.IsNullOrEmpty(techHelpTempState.ModelNumbers)) Then
    '                ddlModels.SelectedValue = techHelpTempState.ModelNumbers
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SerialNumbers)) Then
    '                txtSerialNumber.Text = techHelpTempState.SerialNumbers
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub InsertSurveyAnswer(ByVal regId As String, ByVal questionId As String, ByVal answer As String)
        Dim sqlString As String = "INSERT INTO [SURVEYANSWERS] ([sessionid],[regid],[questionid],[answer],[contactid],[dateentered]) VALUES (@SESSIONID, @REGID, @QUESTIONID, @ANSWER, @CONTACTID, @DATEENTERED)"
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@SESSIONID", Session.SessionID))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@REGID", regId))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@QUESTIONID", questionId))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@ANSWER", answer))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@CONTACTID", Session("contactid").ToString()))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    End Sub

    Private Function GetRegId() As Int32
        Dim sql As String = "select max(regid) as REGID from SURVEYANSWERS where sessionid=@SESSIONID"
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", Session.SessionID)})
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If Not dr("REGID") Is DBNull.Value Then
                If Len(dr("REGID")) > 0 Then
                    Return CType(dr("REGID").ToString(), Int32) + 1
                End If
            End If
        End If
        Return 1
    End Function

    Private Sub ProcessSubmitRequest()
        Page.Validate()
        'F32 = Models
        'F33 = Serial
        If (Page.IsValid) Then
            Dim sqlString As String = "SELECT MODEL_ID FROM MODEL WHERE NAME LIKE (@MODEL) AND COUNTRY_ID = 208"
            Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@MODEL", ddlModels.SelectedValue)})
            Dim modelid As String = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            Dim remarks As String = String.Format("S/N : {0}", SQLStringWithOutSingleQuotes(txtSerialNumber.Text))
            Dim regId As Int32 = GetRegId()
            ID = Functions.InsertSWDownloadRequest(Session("ContactId"), 15, modelid, SQLStringWithOutSingleQuotes(txtSerialNumber.Text), String.Empty, remarks, String.Empty, 0, 0, DateTime.Now, 0)
            InsertSurveyAnswer(regId, "32", ddlModels.SelectedItem.Text)
            InsertSurveyAnswer(regId, "33", SQLStringWithOutSingleQuotes(txtSerialNumber.Text))
            If Len(Session("contactid")) > 0 And Len(Session("country")) > 0 Then
                sqlString = "Insert Into REQUEST_PRODREG (contact_id) values (@CONTACTID)"
                Dim InsertUser As Integer = DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@CONTACTID", Session("contactid").ToString())}).ToArray())
                If CStr(Session("country")) <> "Japan" And Len(InsertUser) = 0 Then
                    sqlString = "Insert Into SURVEY_USERS (sessionid,contactid,dateentered) values (@SESSIONID,@CONTACTID,@DATEENTERED)"
                    DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", Session.SessionID), New System.Data.SqlClient.SqlParameter("@CONTACTID", Session("contactid").ToString()), New System.Data.SqlClient.SqlParameter("@DATEENTERED", Now().ToString())}).ToArray())
                End If
            End If

            If Len(Session("country")) > 0 Then
                If CStr(Session("country")) = "Japan" Then
                    If Len(Session("contactid")) > 0 Then
                        Try
                            Functions.SendEmail(Session("country").ToString, Functions.EmailBodyClientData(Session("contactid"), "1041") & Chr(10) & Chr(13) & getAnswers(Session.SessionID), "repair.jp@teledynelecroy.com,contact.jp@teledynelecroy.com", "webmaster@teledynelecroy.com", "", "", "Japanese Product Registration", "")
                        Catch ex As Exception
                        End Try
                    End If
                ElseIf CStr(Session("country")) = "Korea" Then
                    If Len(Session("contactid")) > 0 Then
                        Try
                            Functions.SendEmail(Session("country").ToString, Functions.EmailBodyClientData(Session("contactid"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & Chr(10) & Chr(13) & getAnswers(Session.SessionID), Functions.GetSalesRepEmail(Session("COuntry").ToString(), 15, ProcessCategoryIdQueryString()), "webmaster@teledynelecroy.com", "", "", "Web Product Registration", "")
                        Catch ex As Exception
                        End Try
                    End If
                End If
            End If
            Response.Redirect(rootDir + "/support/confirm.aspx" + menuURL2)
        End If
    End Sub

    Private Function ProcessCategoryIdQueryString() As Int32
        If (String.IsNullOrEmpty(Request.QueryString("categoryid"))) Then
            Return LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
        End If
        Dim retVal As Int32
        If Not (Int32.TryParse(Request.QueryString("categoryid"), retVal)) Then
            retVal = LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
        End If
        Return retVal
    End Function

    'Private Sub HandleSubmit()
    '    If Not (Session("ContactId") Is Nothing) Then
    '        Try
    '            If (Int32.Parse(Session("ContactID")) = 0) Then
    '                SaveFormData()
    '            End If
    '        Catch
    '        End Try
    '    End If

    '    If Not ValidateUser() Then
    '        Response.Redirect(rootDir + "/Support/user/")
    '    End If
    'End Sub

    'Private Sub SaveFormData()
    '    If Not (Session("TECHHELPTEMPSTATEID_REGISTER") Is Nothing) Then ' Do not create another session state if user already has one - look it up and verify
    '        Dim fetchedTechHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REGISTER").ToString()))
    '        If Not (fetchedTechHelpTempState.TechHelpTempStateId = Guid.Empty) Then ' Valid, exit routine
    '            TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REGISTER").ToString()))
    '        End If
    '    End If

    '    Dim techHelpTempState As TechHelpTempState = New TechHelpTempState()
    '    techHelpTempState.TechHelpTempStateId = Guid.NewGuid()
    '    techHelpTempState.SelectedGeneralQuestion = False
    '    techHelpTempState.ModelNumbers = ddlModels.SelectedValue
    '    techHelpTempState.SerialNumbers = txtSerialNumber.Text
    '    Session("TECHHELPTEMPSTATEID_REGISTER") = techHelpTempState.TechHelpTempStateId.ToString()
    '    TechHelpTempStateRepository.InsertTechHelpTempStateRecord(techHelpTempState)
    'End Sub
#End Region
End Class