﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_RegisterScope_Register" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p>
            <img src="<%= rootDir%>/images/category/headers/hd_reg.gif" alt="Product Registration"></p>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td valign="top"></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">
                        <table cellpadding="3" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <input type="hidden" name="contactid" value="<% = Session("GUID") %>"><input type="hidden" name="dateentered" value="<% = now() %>">
                                    <p><h3><asp:Literal ID="litOne" runat="server" /></h3></p>
                                    <p><asp:Literal ID="litTwo" runat="server" /></p>
                                    <p>
                                        <img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16"><asp:Literal ID="litThree" runat="server" /><br /><br />
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr><td colspan="2"><asp:CustomValidator ID="cvModelAndSerial" runat="server" Display="Dynamic" EnableClientScript="false" ForeColor="Red" /></td></tr>
                                            <tr>
                                                <td><asp:Literal ID="litFour" runat="server" /></td>
                                                <td><asp:Literal ID="litFive" runat="server" /></td>
                                            </tr>
                                            <tr>
                                                <td><asp:DropDownList ID="ddlModels1" runat="server" /></td>
                                                <td><asp:TextBox ID="txtSerialNumber1" runat="server" MaxLength="20" Width="125" /><asp:Label ID="lblError1" runat="server" ForeColor="Red" Text="*" Visible="false" /></td>
                                            </tr>
                                            <tr>
                                                <td><asp:DropDownList ID="ddlModels2" runat="server" /></td>
                                                <td><asp:TextBox ID="txtSerialNumber2" runat="server" MaxLength="20" Width="125" /><asp:Label ID="lblError2" runat="server" ForeColor="Red" Text="*" Visible="false" /></td>
                                            </tr>
                                            <tr>
                                                <td><asp:DropDownList ID="ddlModels3" runat="server" /></td>
                                                <td><asp:TextBox ID="txtSerialNumber3" runat="server" MaxLength="20" Width="125" /><asp:Label ID="lblError3" runat="server" ForeColor="Red" Text="*" Visible="false" /></td>
                                            </tr>
                                        </table><br />
                                    </p>
                                    &nbsp;
                                    <p>
                                        <img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16">
                                        <asp:Literal ID="litSix" runat="server" />
                                    </p>
                                    <p>
                                        &nbsp;
                                        <table cellspacing="0" cellpadding="3" width="100%" bgcolor="gainsboro" border="0">
                                            <tr>
                                                <td><strong><asp:Literal ID="litSeven" runat="server" /></strong></td>
                                                <td><strong><asp:Literal ID="litEight" runat="server" /></strong></td>
                                                <td><strong><asp:Literal ID="litNine" runat="server" /></strong></td>
                                                <td><strong><asp:Literal ID="litTen" runat="server" /></strong></td>
                                                <td><strong><asp:Literal ID="litEleven" runat="server" /></strong></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;"><asp:RadioButton ID="rbOverallSatisfaction1" runat="server" GroupName="OverallSatisfaction" Text="" TextAlign="Left" /></td>
                                                <td style="font-weight: bold;"><asp:RadioButton ID="rbOverallSatisfaction2" runat="server" GroupName="OverallSatisfaction" Text="" TextAlign="Left" /></td>
                                                <td style="font-weight: bold;"><asp:RadioButton ID="rbOverallSatisfaction3" runat="server" GroupName="OverallSatisfaction" Text="" TextAlign="Left" /></td>
                                                <td style="font-weight: bold;"><asp:RadioButton ID="rbOverallSatisfaction4" runat="server" GroupName="OverallSatisfaction" Text="" TextAlign="Left" /></td>
                                                <td style="font-weight: bold;"><asp:RadioButton ID="rbOverallSatisfaction5" runat="server" GroupName="OverallSatisfaction" Text="" TextAlign="Left" /></td>
                                            </tr>
                                        </table>
                                        <p><img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16"><asp:Literal ID="litTwelve" runat="server" /></p>
                                    </p>
                                    &nbsp;
                                    <table cellspacing="0" cellpadding="3" width="100%" border="0">
                                        <tr>
                                            <td colspan="6"><p align="center"><strong><asp:Literal ID="litThirteen" runat="server" /></strong></p></td>
                                            <td></td>
                                            <td colspan="6"><p align="center"><strong><asp:Literal ID="litFourteen" runat="server" /></strong></p></td>
                                            <td></td>
                                        </tr>
                                        <tr bgcolor="gainsboro">
                                            <td colspan="3"><strong><asp:Literal ID="litFifteen" runat="server" /><br><asp:Literal ID="litSixteen" runat="server" /></strong></td>
                                            <td colspan="3"><p align="right"><strong><asp:Literal ID="litSeventeen" runat="server" /><br><asp:Literal ID="litEighteen" runat="server" /></strong></p></td>
                                            <td><strong></strong></td>
                                            <td colspan="3"><p align="left"><strong><asp:Literal ID="litNineteen" runat="server" /><br><asp:Literal ID="litTwenty" runat="server" /></strong></p></td>
                                            <td colspan="3"><p align="right"><strong><asp:Literal ID="litTwentyOne" runat="server" /><br><asp:Literal ID="litTwentyTwo" runat="server" /></strong></p></td>
                                        </tr>
                                        <tr>
                                            <td><strong><asp:Literal ID="litTwentyThree" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litTwentyFour" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litTwentyFive" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litTwentySix" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litTwentySeven" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litTwentyEight" runat="server" /></strong></td>
                                            <td><strong></strong></td>
                                            <td><strong><asp:Literal ID="litTwentyNine" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litThirty" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litThirtyOne" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litThirtyTwo" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litThirtyThree" runat="server" /></strong></td>
                                            <td><strong><asp:Literal ID="litThirtyFour" runat="server" /></strong></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance1" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance2" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance3" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance4" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance5" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepImportance6" runat="server" GroupName="SalesRepImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litThirtyFive" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction1" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction2" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction3" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction4" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction5" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSalesRepSatisfaction6" runat="server" GroupName="SalesRepSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance1" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance2" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance3" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance4" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance5" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilityImportance6" runat="server" GroupName="AvailabilityImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litThirtySix" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction1" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction2" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction3" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction4" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction5" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbAvailabilitySatisfaction6" runat="server" GroupName="AvailabilitySatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance1" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance2" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance3" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance4" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance5" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessImportance6" runat="server" GroupName="CompletenessImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litThirtySeven" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction1" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction2" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction3" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction4" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction5" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbCompletenessSatisfaction6" runat="server" GroupName="CompletenessSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance1" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance2" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance3" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance4" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance5" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionImportance6" runat="server" GroupName="ConditionImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litThirtyEight" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction1" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction2" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction3" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction4" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction5" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbConditionSatisfaction6" runat="server" GroupName="ConditionSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance1" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance2" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance3" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance4" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance5" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliveryImportance6" runat="server" GroupName="DeliveryImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litThirtyNine" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction1" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction2" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction3" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction4" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction5" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbDeliverySatisfaction6" runat="server" GroupName="DeliverySatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance1" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance2" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance3" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance4" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance5" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupImportance6" runat="server" GroupName="SetupImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litForty" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction1" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction2" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction3" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction4" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction5" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbSetupSatisfaction6" runat="server" GroupName="SetupSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance1" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance2" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance3" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance4" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance5" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionImportance6" runat="server" GroupName="ResolutionImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litFortyOne" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction1" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction2" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction3" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction4" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction5" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbResolutionSatisfaction6" runat="server" GroupName="ResolutionSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance1" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance2" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance3" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance4" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance5" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithImportance6" runat="server" GroupName="BusinessWithImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litFortyTwo" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction1" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction2" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction3" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction4" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction5" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbBusinessWithSatisfaction6" runat="server" GroupName="BusinessWithSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance1" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance2" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance3" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance4" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance5" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerImportance6" runat="server" GroupName="ValuedCustomerImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litFortyThree" runat="server" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction1" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction2" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction3" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction4" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction5" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbValuedCustomerSatisfaction6" runat="server" GroupName="ValuedCustomerSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance1" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance2" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance3" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance4" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance5" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherImportance6" runat="server" GroupName="OtherImportance" Text="" TextAlign="Left" /></td>
                                            <td><strong><asp:Literal ID="litFortyFour" runat="server" /><br /><asp:TextBox ID="txtOtherSurveyQuestion" runat="server" MaxLength="50" Width="125" /></strong></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction1" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction2" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction3" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction4" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction5" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                            <td style="font-weight: bold;"><asp:RadioButton ID="rbOtherSatisfaction6" runat="server" GroupName="OtherSatisfaction" Text="" TextAlign="Left" /></td>
                                        </tr>
                                    </table>
                                    <p><img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16"><asp:Literal ID="litFortyFive" runat="server" /></p>
                                    <p><asp:TextBox ID="txtSurveyReasons" runat="server" Columns="50" MaxLength="100" Rows="3" TextMode="MultiLine" /></p>
                                    <p><img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16"><asp:Literal ID="litFortySix" runat="server" /></p>
                                    <p><asp:TextBox ID="txtComments" runat="server" Columns="50" MaxLength="100" Rows="3" TextMode="MultiLine" /></p>
                                    <p><img src="../images/arrowoff.gif" vspace="5" hspace="5" width="17" height="16"><asp:Literal ID="litFortySeven" runat="server" /></p>
                                    <p>
                                        &nbsp;
                                        <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left"><asp:CheckBoxList ID="cblJobFunctions" runat="server" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table" /></td>
                                                <td valign="top"><asp:Literal ID="litFortyEight" runat="server" /><br /><asp:TextBox ID="txtJobFunctionOther" runat="server" MaxLength="50" Width="125" /></td>
                                            </tr>
                                        </table>
                                        <p><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" /></p>
                                    </p>
                                </td>
                                <td valign="top"></td>
                            </tr>
                        </table><br><br>
                    </td>
                </tr>
                <tr><td colspan="3"></td></tr>
            </table>
        </p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources"></div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>