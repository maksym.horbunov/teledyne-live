﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Support_RegisterScope_Register
    Inherits BasePage
    Dim sql As String = ""
    Dim i As Integer
    Dim iModelLooper As Integer = 0
    Dim ds As DataSet
    Dim strTemp As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Private groupNames As List(Of String) = New List(Of String)({"OverallSatisfaction", "SalesRepImportance", "SalesRepSatisfaction", "AvailabilityImportance", "AvailabilitySatisfaction", "CompletenessImportance", "CompletenessSatisfaction", "ConditionImportance", "ConditionSatisfaction", "DeliveryImportance", "DeliverySatisfaction", "SetupImportance", "SetupSatisfaction", "ResolutionImportance", "ResolutionSatisfaction", "BusinessWithImportance", "BusinessWithSatisfaction", "ValuedCustomerImportance", "ValuedCustomerSatisfaction", "OtherImportance", "OtherSatisfaction"})

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Product Registration"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0473", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0477", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0478", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0479", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0480", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0482", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0483", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = Functions.LoadI18N("SLBCA0484", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNine.Text = Functions.LoadI18N("SLBCA0485", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTen.Text = Functions.LoadI18N("SLBCA0486", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEleven.Text = Functions.LoadI18N("SLBCA0487", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwelve.Text = Functions.LoadI18N("SLBCA0493", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirteen.Text = Functions.LoadI18N("SLBCA0494", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFourteen.Text = Functions.LoadI18N("SLBCA0495", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFifteen.Text = Functions.LoadI18N("SLBCA0496", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSixteen.Text = Functions.LoadI18N("SLBCA0497", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeventeen.Text = Functions.LoadI18N("SLBCA0498", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEighteen.Text = Functions.LoadI18N("SLBCA0499", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNineteen.Text = Functions.LoadI18N("SLBCA0500", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwenty.Text = Functions.LoadI18N("SLBCA0501", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyOne.Text = Functions.LoadI18N("SLBCA0502", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyTwo.Text = Functions.LoadI18N("SLBCA0503", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyThree.Text = Functions.LoadI18N("SLBCA0488", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyFour.Text = Functions.LoadI18N("SLBCA0489", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyFive.Text = Functions.LoadI18N("SLBCA0490", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentySix.Text = Functions.LoadI18N("SLBCA0491", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentySeven.Text = Functions.LoadI18N("SLBCA0492", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyEight.Text = Functions.LoadI18N("SLBCA0504", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyNine.Text = Functions.LoadI18N("SLBCA0488", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirty.Text = Functions.LoadI18N("SLBCA0489", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyOne.Text = Functions.LoadI18N("SLBCA0490", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyTwo.Text = Functions.LoadI18N("SLBCA0491", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyThree.Text = Functions.LoadI18N("SLBCA0492", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyFour.Text = Functions.LoadI18N("SLBCA0504", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyFive.Text = Functions.LoadI18N("SLBCA0505", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtySix.Text = Functions.LoadI18N("SLBCA0506", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtySeven.Text = Functions.LoadI18N("SLBCA0507", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyEight.Text = Functions.LoadI18N("SLBCA0508", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyNine.Text = Functions.LoadI18N("SLBCA0509", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litForty.Text = Functions.LoadI18N("SLBCA0510", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyOne.Text = Functions.LoadI18N("SLBCA0511", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyTwo.Text = Functions.LoadI18N("SLBCA0512", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyThree.Text = Functions.LoadI18N("SLBCA0513", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyFour.Text = Functions.LoadI18N("SLBCA0514", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyFive.Text = Functions.LoadI18N("SLBCA0515", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortySix.Text = Functions.LoadI18N("SLBCA0516", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortySeven.Text = Functions.LoadI18N("SLBCA0517", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFortyEight.Text = Functions.LoadI18N("SLBCA0520", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.PROD_REG_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        If Not Page.IsPostBack Then
            BindText()
            BindModelDropDown()
            Session("RedirectTo") = rootDir + "/support/registerscope/" + menuURL2
            If Not ValidateUser() Then
                Response.Redirect(rootDir + "/support/user/")
            End If
            ValidateUserNoRedir()
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            'SetFormDataFromPreLoginRedirect()
        End If
    End Sub

    Private Sub BindText()
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        rbOverallSatisfaction1.Text = Functions.LoadI18N("SLBCA0488", localeId)
        rbOverallSatisfaction2.Text = Functions.LoadI18N("SLBCA0489", localeId)
        rbOverallSatisfaction3.Text = Functions.LoadI18N("SLBCA0490", localeId)
        rbOverallSatisfaction4.Text = Functions.LoadI18N("SLBCA0491", localeId)
        rbOverallSatisfaction5.Text = Functions.LoadI18N("SLBCA0492", localeId)
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0518", localeId), "1"))
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0521", localeId), "2"))
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0523", localeId), "4"))
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0519", localeId), "8"))
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0522", localeId), "16"))
        cblJobFunctions.Items.Add(New ListItem(Functions.LoadI18N("SLBCA0524", localeId), "32"))
        btnSubmit.Text = Functions.LoadI18N("SLBCA0528", localeId)
    End Sub

    Private Sub BindModelDropDown()
        Dim sqlString As String = "SELECT p.PRODUCTID, p.PARTNUMBER, p.DISABLED, p.EOL_YN, p.NAME FROM [PRODUCT_SERIES_CATEGORY] psc, [PRODUCT] p WHERE psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_ID = p.PRODUCTID ORDER BY p.NAME"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@CATEGORYID", "1")})
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (products.Count <= 0) Then Response.Redirect("Default.aspx")

        Dim selectedIndex As Int32 = 0
        For Each product In products
            ddlModels1.Items.Add(New ListItem(product.Name, product.ProductId))
            ddlModels2.Items.Add(New ListItem(product.Name, product.ProductId))
            ddlModels3.Items.Add(New ListItem(product.Name, product.ProductId))
        Next
        ddlModels1.Items.Insert(0, New ListItem(Functions.LoadI18N("SLBCA0481", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), String.Empty))
        ddlModels1.SelectedIndex = selectedIndex
        ddlModels2.Items.Insert(0, New ListItem(Functions.LoadI18N("SLBCA0481", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), String.Empty))
        ddlModels2.SelectedIndex = selectedIndex
        ddlModels3.Items.Insert(0, New ListItem(Functions.LoadI18N("SLBCA0481", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), String.Empty))
        ddlModels3.SelectedIndex = selectedIndex
    End Sub

    Function InsertRequestWithScopeInfo(ByVal lgContactId, ByVal lgRequestTypeId, ByVal lgObjectId, ByVal strSerialNum, ByVal strScopeID, ByVal strRemarks, ByVal strApplication, ByVal lgPageID, ByVal PresentID) As String
        If Len(Session("CampaignID")) = 0 Then
            Session("CampaignID") = 0
        End If
        Dim strDate As DateTime = Now()

        sql = "INSERT INTO REQUEST (CONTACT_ID, REQUEST_TYPE_ID, OBJECT_ID, SERIAL_NUM, SCOPEID, REMARKS, ENTERED_BY, DATE_ENTERED,PAGEID,CAMPAIGN_ID,PRESENT_ID) VALUES (@CONTACTID,@REQUESTTYPEID,@OBJECTID,@SERIALNUM,@SCOPEID,@REMARKS,@ENTEREDBY,@DATEENTERED,@PAGEID,@CAMPAIGNID,@PRESENTID)"
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@CONTACTID", lgContactId.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@REQUESTTYPEID", lgRequestTypeId.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@OBJECTID", lgObjectId.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@SERIALNUM", strSerialNum.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@SCOPEID", strScopeID.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@ENTEREDBY", "webuser"), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@DATEENTERED", strDate.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@PAGEID", lgPageID.ToString()), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@CAMPAIGNID", Session("Campaignid")), _
                                                                                                                            New System.Data.SqlClient.SqlParameter("@PRESENTID", PresentID.ToString())})
        Dim sqlParam As System.Data.SqlClient.SqlParameter = New System.Data.SqlClient.SqlParameter("@REMARKS", SqlDbType.NVarChar)
        sqlParam.Value = strRemarks
        sqlParameters.Add(sqlParam)
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        sql = "SELECT REQUEST_ID from REQUEST where CONTACT_ID=@CONTACTID and OBJECT_ID=@OBJECTID and DATE_ENTERED=@DATEENTERED and REQUEST_TYPE_ID=@REQUESTTYPEID"
        sqlParameters = New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@CONTACTID", lgContactId.ToString()), New System.Data.SqlClient.SqlParameter("@OBJECTID", lgObjectId.ToString()), New System.Data.SqlClient.SqlParameter("@DATEENTERED", strDate.ToString()), New System.Data.SqlClient.SqlParameter("@REQUESTTYPEID", lgRequestTypeId.ToString())})
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                InsertRequestWithScopeInfo = dr("REQUEST_ID")
            Else
                InsertRequestWithScopeInfo = ""
            End If
        Else
            InsertRequestWithScopeInfo = ""
        End If
    End Function

    Function getAnswers(ByVal SessionID As String) As String
        Dim strbody As String = ""
        Dim ds1 As DataSet
        getAnswers = ""
        If Len(SessionID) > 0 Then
            sql = "select max(regid) as REGID from SURVEYANSWERS where sessionid=@SESSIONID"
            Dim sqlParameters As New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", SessionID.ToString())})
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                If Not dr("REGID") Is Nothing And Len(dr("REGID")) > 0 Then
                    sql = "SELECT SURVEYANSWERS.answer, SURVEYQUESTION.questionname, " & _
                  "SURVEYANSWERS.questionid FROM SURVEYANSWERS INNER JOIN " & _
                  "SURVEYQUESTION ON SURVEYANSWERS.questionid = SURVEYQUESTION.questionid " & _
                  "WHERE sessionid=@SESSIONID and regid=@REGID ORDER BY SURVEYANSWERS.questionid DESC"
                    'response.Write strSQL
                    ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", SessionID.ToString()), New System.Data.SqlClient.SqlParameter("@REGID", dr("REGID").ToString())}).ToArray())
                    If ds1.Tables(0).Rows.Count > 0 Then

                        Dim j As Int32 = 0
                        For Each dr1 As DataRow In ds1.Tables(0).Rows


                            If Len(dr1("questionname")) > 0 Then
                                If CInt(dr1("questionid")) = 1 Or CInt(dr1("questionid")) = 2 Or CInt(dr1("questionid")) = 3 Or CInt(dr1("questionid")) = 4 Or CInt(dr1("questionid")) = 5 Or CInt(dr1("questionid")) = 6 Or CInt(dr1("questionid")) = 7 Then
                                    strbody = strbody & "  " & LoadI18N("SLBCA0517", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                Else
                                    strbody = strbody & "  " & translation_webdb32(dr1("questionid"))
                                End If
                            Else
                                strbody = strbody & "  "
                            End If
                            If Len(dr1("answer")) > 0 Then
                                If CInt(dr1("questionid")) = 8 Then
                                    Select Case CStr(dr1("answer"))
                                        Case "1"
                                            strbody = strbody & " : " & LoadI18N("SLBCA0483", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                        Case "2"
                                            strbody = strbody & " : " & LoadI18N("SLBCA0484", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                        Case "3"
                                            strbody = strbody & " : " & LoadI18N("SLBCA0485", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                        Case "4"
                                            strbody = strbody & " : " & LoadI18N("SLBCA0486", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                        Case "5"
                                            strbody = strbody & " : " & LoadI18N("SLBCA0487", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                                    End Select
                                ElseIf CInt(dr1("questionid")) = 1 Or CInt(dr1("questionid")) = 2 Or CInt(dr1("questionid")) = 4 Or CInt(dr1("questionid")) = 5 Or CInt(dr1("questionid")) = 6 Or CInt(dr1("questionid")) = 7 Then
                                    strbody = strbody & " : " & translation_webdb32(dr1("questionid"))
                                Else
                                    strbody = strbody & " : " & dr1("answer")
                                End If
                            Else
                                strbody = strbody
                            End If
                            strbody = strbody & "  " & Chr(13) & Chr(10)
                        Next
                        getAnswers = strbody
                    End If
                End If
            End If
        End If
    End Function

    Function translation_webdb32(ByVal id) As String
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))

        If (localeId = 1031 Or localeId = 1040 Or localeId = 1036) Then
            localeId = 1033
        End If
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If CInt(localeId) = 1033 Then
            sql = "Select questionname as VALUE from SURVEYQUESTION where questionid=@ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@ID", id))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                translation_webdb32 = dr("VALUE")
            Else
                translation_webdb32 = ""
            End If
        Else
            'select tableid from table "TABLE_ID" (ecatalog)
            sql = "SELECT Table_id From Table_id where Table_name=@TABLENAME"
            'Response.Write strSQL
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLENAME", "SURVEYQUESTION"))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            Dim TableID As String = "", ColID As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                TableID = dr("Table_ID")
            End If

            sql = "SELECT Column_id From Column_id where Column_name=@COLUMNNAME"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@COLUMNNAME", "questionname"))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                ColID = dr("Column_ID")
            End If

            sql = "Select VALUE from TABLESBYLANGUAGE where TABLE_ID=@TABLEID and COLUMN_ID=@COLUMNID and IDINTABLE=@ID and LOCALEID=@LOCALEID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TABLEID", TableID))
            sqlParameters.Add(New SqlParameter("@COLUMNID", ColID))
            sqlParameters.Add(New SqlParameter("@ID", id.ToString()))
            sqlParameters.Add(New SqlParameter("@LOCALEID", localeId.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                translation_webdb32 = dr("Value")
            Else
                sql = "Select questionname as VALUE from SURVEYQUESTION where questionid=@ID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@ID", id))
                ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    translation_webdb32 = Trim(dr("VALUE"))
                Else
                    translation_webdb32 = ""
                End If
            End If
        End If
    End Function

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Page.Validate()
        If (Page.IsValid) Then
            'HandleSubmit()
            'Try
            '    If Not (Session("TECHHELPTEMPSTATEID_SCOPE") Is Nothing) Then
            '        TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_SCOPE").ToString()))   ' DB record is not needed anymore
            '    End If
            'Catch
            'End Try
            ProcessSubmitRequest()
        End If
    End Sub

    'Private Sub SetFormDataFromPreLoginRedirect()
    '    If Not (Session("TECHHELPTEMPSTATEID_SCOPE") Is Nothing) Then
    '        Dim techHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_SCOPE").ToString()))
    '        If (techHelpTempState Is Nothing) Then
    '            Return
    '        End If
    '        If Not (techHelpTempState.TechHelpTempStateId = Guid.Empty) Then    ' Valid, bind page controls
    '            Dim modelNumbers As String() = techHelpTempState.ModelNumbers.Split(",")
    '            ddlModels1.SelectedIndex = 0
    '            ddlModels2.SelectedIndex = 0
    '            ddlModels3.SelectedIndex = 0
    '            For i As Int32 = 0 To modelNumbers.Length - 1
    '                If Not (String.IsNullOrEmpty(modelNumbers(i))) Then
    '                    Dim ddl As DropDownList = CType(ScopeRegistration.FindControl(String.Format("ddlModels{0}", i + 1)), DropDownList)
    '                    ddl.SelectedValue = modelNumbers(i)
    '                End If
    '            Next
    '            Dim serialNumbers As String() = techHelpTempState.SerialNumbers.Split(",")
    '            For i As Int32 = 0 To serialNumbers.Length - 1
    '                If Not (String.IsNullOrEmpty(serialNumbers(i))) Then
    '                    Dim txt As TextBox = CType(ScopeRegistration.FindControl(String.Format("txtSerialNumber{0}", i + 1)), TextBox)
    '                    txt.Text = serialNumbers(i)
    '                End If
    '            Next
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SurveyAnswers)) Then
    '                Dim surveyAnswers As String() = techHelpTempState.SurveyAnswers.Split("|")
    '                For i As Int32 = 0 To groupNames.Count - 1
    '                    Dim groupName As String = UnMaskValue(surveyAnswers(i), groupNames(i))
    '                    If Not (String.IsNullOrEmpty(groupName)) Then
    '                        Dim rb As RadioButton = CType(ScopeRegistration.FindControl(String.Format("rb{0}", groupName)), RadioButton)
    '                        rb.Checked = True
    '                    End If
    '                Next
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SurveyOther)) Then
    '                txtOtherSurveyQuestion.Text = techHelpTempState.SurveyOther
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SurveyReasons)) Then
    '                txtSurveyReasons.Text = techHelpTempState.SurveyReasons
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.EnteredInquiry)) Then
    '                txtComments.Text = techHelpTempState.EnteredInquiry
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SurveyJobFunction)) Then
    '                UnMaskCblValue(techHelpTempState.SurveyJobFunction)
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SurveyJobFunctionOther)) Then
    '                txtJobFunctionOther.Text = techHelpTempState.SurveyJobFunctionOther
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub InsertSurveyAnswer(ByVal regId As String, ByVal questionId As String, ByVal answer As String)
        Dim sqlString As String = "INSERT INTO [SURVEYANSWERS] ([sessionid],[regid],[questionid],[answer],[contactid],[dateentered]) VALUES (@SESSIONID, @REGID, @QUESTIONID, @ANSWER, @CONTACTID, @DATEENTERED)"
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@SESSIONID", Session.SessionID))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@REGID", regId))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@QUESTIONID", questionId))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@ANSWER", answer))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@CONTACTID", Session("contactid").ToString()))
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
    End Sub

    Private Sub ProcessSubmitRequest()
        Dim RegID As Integer
        sql = "select max(regid) as MAXREGID from SURVEYANSWERS where sessionid=@SESSIONID"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SESSIONID", Session.SessionID)}).ToArray())
        If ds.Tables(0).Rows.Count = 0 Then
            RegID = 1
        Else
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If dr("MAXREGID").ToString <> "" Then
                RegID = dr("MAXREGID") + 1
            Else
                RegID = 1
            End If
        End If
        Dim strbody As String = ""

        For Each cbx As ListItem In cblJobFunctions.Items
            If (cbx.Selected) Then
                Dim sqlString As String = String.Empty
                Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
                Dim questionId As String = String.Empty
                If (String.Compare(cbx.Value, "1", True) = 0) Then
                    questionId = "1"
                ElseIf (String.Compare(cbx.Value, "2", True) = 0) Then
                    questionId = "4"
                ElseIf (String.Compare(cbx.Value, "4", True) = 0) Then
                    questionId = "6"
                ElseIf (String.Compare(cbx.Value, "8", True) = 0) Then
                    questionId = "2"
                ElseIf (String.Compare(cbx.Value, "16", True) = 0) Then
                    questionId = "5"
                ElseIf (String.Compare(cbx.Value, "32", True) = 0) Then
                    questionId = "7"
                End If
                If Not (String.IsNullOrEmpty(questionId)) Then
                    InsertSurveyAnswer(RegID.ToString(), questionId, "ON")
                End If
            End If
        Next

        If (ddlModels1.SelectedIndex > 0 And Not (String.IsNullOrEmpty(SQLStringWithOutSingleQuotes(txtSerialNumber1.Text)))) Then
            InsertRequestWithScopeInfo(Session("ContactId"), 15, ddlModels1.SelectedValue, SQLStringWithOutSingleQuotes(txtSerialNumber1.Text.Trim()), "", "", "", 0, 0)
            InsertSurveyAnswer(RegID.ToString(), "32", ddlModels1.SelectedItem.Text)
            InsertSurveyAnswer(RegID.ToString(), "33", SQLStringWithOutSingleQuotes(txtSerialNumber1.Text.Trim()))
        End If
        If (ddlModels2.SelectedIndex > 0 And Not (String.IsNullOrEmpty(SQLStringWithOutSingleQuotes(txtSerialNumber2.Text)))) Then
            InsertRequestWithScopeInfo(Session("ContactId"), 15, ddlModels2.SelectedValue, SQLStringWithOutSingleQuotes(txtSerialNumber2.Text.Trim()), "", "", "", 0, 0)
            InsertSurveyAnswer(RegID.ToString(), "34", ddlModels2.SelectedItem.Text)
            InsertSurveyAnswer(RegID.ToString(), "35", SQLStringWithOutSingleQuotes(txtSerialNumber2.Text.Trim()))
        End If
        If (ddlModels3.SelectedIndex > 0 And Not (String.IsNullOrEmpty(SQLStringWithOutSingleQuotes(txtSerialNumber3.Text)))) Then
            InsertRequestWithScopeInfo(Session("ContactId"), 15, ddlModels3.SelectedValue, SQLStringWithOutSingleQuotes(txtSerialNumber3.Text.Trim()), "", "", "", 0, 0)
            InsertSurveyAnswer(RegID.ToString(), "36", ddlModels3.SelectedItem.Text)
            InsertSurveyAnswer(RegID.ToString(), "37", SQLStringWithOutSingleQuotes(txtSerialNumber3.Text.Trim()))
        End If
        If Not (String.IsNullOrEmpty(txtOtherSurveyQuestion.Text)) Then
            InsertSurveyAnswer(RegID.ToString(), "29", txtOtherSurveyQuestion.Text.Trim())
        End If
        If Not (String.IsNullOrEmpty(txtSurveyReasons.Text)) Then
            InsertSurveyAnswer(RegID.ToString(), "30", txtSurveyReasons.Text.Trim())
        End If
        If Not (String.IsNullOrEmpty(txtComments.Text)) Then
            InsertSurveyAnswer(RegID.ToString(), "31", txtComments.Text.Trim())
        End If
        If Not (String.IsNullOrEmpty(txtJobFunctionOther.Text)) Then
            InsertSurveyAnswer(RegID.ToString(), "3", txtJobFunctionOther.Text.Trim())
        End If
        Dim mappings As Dictionary(Of String, Int32) = GetControlToOldNamingSchemaDictionary()
        Dim ide As IDictionaryEnumerator = GetRbGroupsAndAssociatedRbs().GetEnumerator()
        While (ide.MoveNext())
            Dim rbAnswer As String = String.Empty
            For Each rb As RadioButton In CType(ide.Value, List(Of RadioButton))
                If (rb.Checked) Then
                    rbAnswer = rb.ID.Substring(rb.ID.Length - 1, 1)
                    Exit For
                End If
            Next
            If Not (String.IsNullOrEmpty(rbAnswer)) Then
                InsertSurveyAnswer(RegID.ToString(), mappings(ide.Key.ToString()).ToString(), rbAnswer)
            End If
        End While

        If Len(Session("country")) > 0 Then
            If CStr(Session("country")) = "Japan" Then
                If Len(getAnswers(Session.SessionID)) > 0 And Len(Session("contactid")) > 0 Then
                    Try
                        Functions.SendEmail(Session("country").ToString, Functions.EmailBodyClientData(Session("contactid"), "1041") & Chr(10) & Chr(13) & getAnswers(Session.SessionID), "repair.jp@teledynelecroy.com,contact.jp@teledynelecroy.com", "webmaster@teledynelecroy.com", "", "", "Japanese Product Registration", "")
                    Catch ex As Exception
                    End Try
                End If
            ElseIf Mid(Session("country"), 1, 5) = "Korea" Then
                If Len(Session("contactid")) > 0 Then
                    Try
                        Functions.SendEmail(Session("country").ToString, Functions.EmailBodyClientData(Session("contactid"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & Chr(10) & Chr(13) & getAnswers(Session.SessionID), Functions.GetSalesRepEmail(Session("COuntry").ToString(), 15, LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES), "webmaster@teledynelecroy.com", "", "", "Web Product Registration", "")
                    Catch ex As Exception
                    End Try
                End If
            End If
        Else
            Response.Redirect(rootDir + "/support/register/")
        End If

        Dim widthtable As Integer = 0
        If (Session("localeid") = 1041) Then
            widthtable = 40
        Else
            widthtable = 60
        End If
        Response.Redirect(rootDir + "/support/confirm.aspx")
    End Sub

    'Private Sub HandleSubmit()
    '    If Not (Session("ContactId") Is Nothing) Then
    '        Try
    '            If (Int32.Parse(Session("ContactID")) = 0) Then
    '                SaveFormData()
    '            End If
    '        Catch
    '        End Try
    '    End If

    '    If Not ValidateUser() Then
    '        Response.Redirect(rootDir + "/Support/user/")
    '    End If
    'End Sub

    'Private Sub SaveFormData()
    '    If Not (Session("TECHHELPTEMPSTATEID_SCOPE") Is Nothing) Then ' Do not create another session state if user already has one - look it up and verify
    '        Dim fetchedTechHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_SCOPE").ToString()))
    '        If Not (fetchedTechHelpTempState.TechHelpTempStateId = Guid.Empty) Then ' Valid, exit routine
    '            TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_SCOPE").ToString()))  ' Delete it; there could have been updates, reprocess below
    '        End If
    '    End If

    '    Dim techHelpTempState As TechHelpTempState = New TechHelpTempState()
    '    techHelpTempState.TechHelpTempStateId = Guid.NewGuid()
    '    techHelpTempState.SelectedGeneralQuestion = False
    '    techHelpTempState.ModelNumbers = BuildCommaDelimitedStringForModels()
    '    techHelpTempState.SerialNumbers = BuildCommaDelimitedStringForSerials()
    '    techHelpTempState.SurveyAnswers = BuildSurveyAnswerString()
    '    techHelpTempState.SurveyOther = txtOtherSurveyQuestion.Text
    '    techHelpTempState.SurveyReasons = txtSurveyReasons.Text
    '    techHelpTempState.EnteredInquiry = txtComments.Text
    '    techHelpTempState.SurveyJobFunction = ConvertCbxListToBitMask(cblJobFunctions).ToString()
    '    techHelpTempState.SurveyJobFunctionOther = txtJobFunctionOther.Text
    '    Session("TECHHELPTEMPSTATEID_SCOPE") = techHelpTempState.TechHelpTempStateId.ToString()
    '    TechHelpTempStateRepository.InsertTechHelpTempStateRecord(techHelpTempState)
    'End Sub

    Private Function BuildCommaDelimitedStringForModels() As String
        Dim sb As StringBuilder = New StringBuilder()
        If (ddlModels1.SelectedIndex > 0) Then
            sb.Append(String.Format("{0},", ddlModels1.SelectedValue))
        End If
        If (ddlModels2.SelectedIndex > 0) Then
            sb.Append(String.Format("{0},", ddlModels2.SelectedValue))
        End If
        If (ddlModels3.SelectedIndex > 0) Then
            sb.Append(String.Format("{0},", ddlModels3.SelectedValue))
        End If
        Dim retVal As String = sb.ToString()
        If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 1)
        Return retVal
    End Function

    Private Function BuildCommaDelimitedStringForSerials() As String
        Dim sb As StringBuilder = New StringBuilder()
        If Not (String.IsNullOrEmpty(txtSerialNumber1.Text)) Then
            sb.Append(String.Format("{0},", txtSerialNumber1.Text))
        End If
        If Not (String.IsNullOrEmpty(txtSerialNumber2.Text)) Then
            sb.Append(String.Format("{0},", txtSerialNumber2.Text))
        End If
        If Not (String.IsNullOrEmpty(txtSerialNumber3.Text)) Then
            sb.Append(String.Format("{0},", txtSerialNumber3.Text))
        End If
        Dim retVal As String = sb.ToString()
        If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 1)
        Return retVal
    End Function

    Private Function BuildSurveyAnswerString() As String
        Dim sb As StringBuilder = New StringBuilder()
        Dim rbs As List(Of RadioButton) = New List(Of RadioButton)({rbOverallSatisfaction1, rbOverallSatisfaction2, rbOverallSatisfaction3, rbOverallSatisfaction4, rbOverallSatisfaction5, rbSalesRepImportance1, rbSalesRepImportance2, rbSalesRepImportance3, rbSalesRepImportance4, rbSalesRepImportance5, rbSalesRepImportance6, rbSalesRepSatisfaction1, rbSalesRepSatisfaction2, rbSalesRepSatisfaction3, rbSalesRepSatisfaction4, rbSalesRepSatisfaction5, rbSalesRepSatisfaction6, rbAvailabilityImportance1, rbAvailabilityImportance2, rbAvailabilityImportance3, rbAvailabilityImportance4, rbAvailabilityImportance5, rbAvailabilityImportance6, rbAvailabilitySatisfaction1, rbAvailabilitySatisfaction2, rbAvailabilitySatisfaction3, rbAvailabilitySatisfaction4, rbAvailabilitySatisfaction5, rbAvailabilitySatisfaction6, rbCompletenessImportance1, rbCompletenessImportance2, rbCompletenessImportance3, rbCompletenessImportance4, rbCompletenessImportance5, rbCompletenessImportance6, rbCompletenessSatisfaction1, rbCompletenessSatisfaction2, rbCompletenessSatisfaction3, rbCompletenessSatisfaction4, rbCompletenessSatisfaction5, rbCompletenessSatisfaction6, rbConditionImportance1, rbConditionImportance2, rbConditionImportance3, rbConditionImportance4, rbConditionImportance5, rbConditionImportance6, rbConditionSatisfaction1, rbConditionSatisfaction2, rbConditionSatisfaction3, rbConditionSatisfaction4, rbConditionSatisfaction5, rbConditionSatisfaction6, rbDeliveryImportance1, rbDeliveryImportance2, rbDeliveryImportance3, rbDeliveryImportance4, rbDeliveryImportance5, rbDeliveryImportance6, rbDeliverySatisfaction1, rbDeliverySatisfaction2, rbDeliverySatisfaction3, rbDeliverySatisfaction4, rbDeliverySatisfaction5, rbDeliverySatisfaction6, rbSetupImportance1, rbSetupImportance2, rbSetupImportance3, rbSetupImportance4, rbSetupImportance5, rbSetupImportance6, rbSetupSatisfaction1, rbSetupSatisfaction2, rbSetupSatisfaction3, rbSetupSatisfaction4, rbSetupSatisfaction5, rbSetupSatisfaction6, rbResolutionImportance1, rbResolutionImportance2, rbResolutionImportance3, rbResolutionImportance4, rbResolutionImportance5, rbResolutionImportance6, rbResolutionSatisfaction1, rbResolutionSatisfaction2, rbResolutionSatisfaction3, rbResolutionSatisfaction4, rbResolutionSatisfaction5, rbResolutionSatisfaction6, rbBusinessWithImportance1, rbBusinessWithImportance2, rbBusinessWithImportance3, rbBusinessWithImportance4, rbBusinessWithImportance5, rbBusinessWithImportance6, rbBusinessWithSatisfaction1, rbBusinessWithSatisfaction2, rbBusinessWithSatisfaction3, rbBusinessWithSatisfaction4, rbBusinessWithSatisfaction5, rbBusinessWithSatisfaction6, rbValuedCustomerImportance1, rbValuedCustomerImportance2, rbValuedCustomerImportance3, rbValuedCustomerImportance4, rbValuedCustomerImportance5, rbValuedCustomerImportance6, rbValuedCustomerSatisfaction1, rbValuedCustomerSatisfaction2, rbValuedCustomerSatisfaction3, rbValuedCustomerSatisfaction4, rbValuedCustomerSatisfaction5, rbValuedCustomerSatisfaction6, rbOtherImportance1, rbOtherImportance2, rbOtherImportance3, rbOtherImportance4, rbOtherImportance5, rbOtherImportance6, rbOtherSatisfaction1, rbOtherSatisfaction2, rbOtherSatisfaction3, rbOtherSatisfaction4, rbOtherSatisfaction5, rbOtherSatisfaction6})
        Dim prefix As String = String.Empty
        Dim firstIteration As Boolean = True
        Dim valueSetForGroup As Boolean = False
        For i As Int32 = 0 To rbs.Count - 1
            Dim currentPrefix As String = rbs(i).ID.Substring(0, rbs(i).ID.Length - 1)
            Dim selectedValue As String = rbs(i).ID.Substring(rbs(i).ID.Length - 1, 1)
            If (String.Compare(prefix, currentPrefix, True) <> 0) Then
                If Not (firstIteration) Then
                    If Not (valueSetForGroup) Then
                        sb.Append("0|")
                    End If
                End If
                prefix = currentPrefix
                valueSetForGroup = False
            End If
            If (rbs(i).Checked And Not (valueSetForGroup)) Then
                valueSetForGroup = True
                sb.Append(String.Format("{0}|", ConvertSelectionToBitMask(Convert.ToInt32(selectedValue))))
            End If
            If (i = rbs.Count - 1 And Not (valueSetForGroup)) Then
                sb.Append("0|")
            End If
            firstIteration = False
        Next

        Dim retVal As String = String.Empty
        If (sb.ToString().Length > 0) Then retVal = sb.ToString().Substring(0, sb.ToString().Length - 1)
        Return retVal
    End Function

    Private Function ConvertSelectionToBitMask(ByVal selectedRadioButton As Int32) As Int32
        Dim retVal As Int32 = 0
        Select Case selectedRadioButton
            Case 1
                retVal = 1
            Case 2
                retVal = 2
            Case 3
                retVal = 4
            Case 4
                retVal = 8
            Case 5
                retVal = 16
            Case 6
                retVal = 32
        End Select
        Return retVal
    End Function

    Private Function ConvertCbxListToBitMask(ByVal cbl As CheckBoxList) As Int32
        Return (From cbx In cbl.Items.Cast(Of ListItem)() _
                Where cbx.Selected _
                Select Int32.Parse(cbx.Value)).Sum()
    End Function

    Private Function UnMaskValue(ByVal bitMaskedValue As Int32, ByVal rbName As String) As String
        If Not ((bitMaskedValue And 32) = 0) Then Return String.Format("{0}6", rbName)
        If Not ((bitMaskedValue And 16) = 0) Then Return String.Format("{0}5", rbName)
        If Not ((bitMaskedValue And 8) = 0) Then Return String.Format("{0}4", rbName)
        If Not ((bitMaskedValue And 4) = 0) Then Return String.Format("{0}3", rbName)
        If Not ((bitMaskedValue And 2) = 0) Then Return String.Format("{0}2", rbName)
        If Not ((bitMaskedValue And 1) = 0) Then Return String.Format("{0}1", rbName)
        Return String.Empty
    End Function

    Private Sub UnMaskCblValue(ByVal bitMaskedValue As Int32)
        If Not ((bitMaskedValue And 32) = 0) Then cblJobFunctions.Items(5).Selected = True
        If Not ((bitMaskedValue And 16) = 0) Then cblJobFunctions.Items(4).Selected = True
        If Not ((bitMaskedValue And 8) = 0) Then cblJobFunctions.Items(3).Selected = True
        If Not ((bitMaskedValue And 4) = 0) Then cblJobFunctions.Items(2).Selected = True
        If Not ((bitMaskedValue And 2) = 0) Then cblJobFunctions.Items(1).Selected = True
        If Not ((bitMaskedValue And 1) = 0) Then cblJobFunctions.Items(0).Selected = True
    End Sub

    Private Sub cvModelAndSerial_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvModelAndSerial.ServerValidate
        Dim lbls As List(Of Label) = New List(Of Label)({lblError1, lblError2, lblError3})
        For Each lbl As Label In lbls
            lbl.Visible = False
        Next
        cvModelAndSerial.ErrorMessage = String.Empty
        args.IsValid = False
        If (ddlModels1.SelectedIndex = 0 And ddlModels2.SelectedIndex = 0 And ddlModels3.SelectedIndex = 0) Then
            cvModelAndSerial.ErrorMessage = "Please select at least one model"
            Exit Sub
        End If
        If (ddlModels1.SelectedIndex = 0 And Not (String.IsNullOrEmpty(txtSerialNumber1.Text))) Then
            cvModelAndSerial.ErrorMessage = "Please select a model"
            lblError1.Visible = True
        End If
        If (ddlModels1.SelectedIndex > 0 And String.IsNullOrEmpty(txtSerialNumber1.Text)) Then
            cvModelAndSerial.ErrorMessage += " Please enter a serial number"
            lblError1.Visible = True
        End If
        If (ddlModels2.SelectedIndex = 0 And Not (String.IsNullOrEmpty(txtSerialNumber2.Text))) Then
            cvModelAndSerial.ErrorMessage += " Please select a model"
            lblError2.Visible = True
        End If
        If (ddlModels2.SelectedIndex > 0 And String.IsNullOrEmpty(txtSerialNumber2.Text)) Then
            cvModelAndSerial.ErrorMessage += " Please enter a serial number"
            lblError2.Visible = True
        End If
        If (ddlModels3.SelectedIndex = 0 And Not (String.IsNullOrEmpty(txtSerialNumber3.Text))) Then
            cvModelAndSerial.ErrorMessage += " Please select a model"
            lblError3.Visible = True
        End If
        If (ddlModels3.SelectedIndex > 0 And String.IsNullOrEmpty(txtSerialNumber3.Text)) Then
            cvModelAndSerial.ErrorMessage += " Please enter a serial number"
            lblError3.Visible = True
        End If
        If (String.IsNullOrEmpty(cvModelAndSerial.ErrorMessage) And lbls.Where(Function(x) x.Visible = True).Count() <= 0) Then
            args.IsValid = True
        End If
    End Sub

    Private Function GetControlToOldNamingSchemaDictionary() As Dictionary(Of String, Int32)
        Dim retVal As Dictionary(Of String, Int32) = New Dictionary(Of String, Int32)
        retVal.Add("OverallSatisfaction", 8)
        retVal.Add("SalesRepImportance", 9)
        retVal.Add("SalesRepSatisfaction", 10)
        retVal.Add("AvailabilityImportance", 11)
        retVal.Add("AvailabilitySatisfaction", 12)
        retVal.Add("CompletenessImportance", 13)
        retVal.Add("CompletenessSatisfaction", 14)
        retVal.Add("ConditionImportance", 15)
        retVal.Add("ConditionSatisfaction", 16)
        retVal.Add("DeliveryImportance", 17)
        retVal.Add("DeliverySatisfaction", 18)
        retVal.Add("SetupImportance", 19)
        retVal.Add("SetupSatisfaction", 20)
        retVal.Add("ResolutionImportance", 21)
        retVal.Add("ResolutionSatisfaction", 22)
        retVal.Add("BusinessWithImportance", 23)
        retVal.Add("BusinessWithSatisfaction", 24)
        retVal.Add("ValuedCustomerImportance", 25)
        retVal.Add("ValuedCustomerSatisfaction", 26)
        retVal.Add("OtherImportance", 27)
        retVal.Add("OtherSatisfaction", 28)
        Return retVal
    End Function

    Private Function GetRbGroupsAndAssociatedRbs() As Dictionary(Of String, List(Of RadioButton))
        Dim retVal As Dictionary(Of String, List(Of RadioButton)) = New Dictionary(Of String, List(Of RadioButton))
        retVal.Add("OverallSatisfaction", New List(Of RadioButton)({rbOverallSatisfaction1, rbOverallSatisfaction2, rbOverallSatisfaction3, rbOverallSatisfaction4, rbOverallSatisfaction5}))    '8
        retVal.Add("SalesRepImportance", New List(Of RadioButton)({rbSalesRepImportance1, rbSalesRepImportance2, rbSalesRepImportance3, rbSalesRepImportance4, rbSalesRepImportance5, rbSalesRepImportance6}))   '9
        retVal.Add("SalesRepSatisfaction", New List(Of RadioButton)({rbSalesRepSatisfaction1, rbSalesRepSatisfaction2, rbSalesRepSatisfaction3, rbSalesRepSatisfaction4, rbSalesRepSatisfaction5, rbSalesRepSatisfaction6})) '10
        retVal.Add("AvailabilityImportance", New List(Of RadioButton)({rbAvailabilityImportance1, rbAvailabilityImportance2, rbAvailabilityImportance3, rbAvailabilityImportance4, rbAvailabilityImportance5, rbAvailabilityImportance6}))   '11
        retVal.Add("AvailabilitySatisfaction", New List(Of RadioButton)({rbAvailabilitySatisfaction1, rbAvailabilitySatisfaction2, rbAvailabilitySatisfaction3, rbAvailabilitySatisfaction4, rbAvailabilitySatisfaction5, rbAvailabilitySatisfaction6})) '12
        retVal.Add("CompletenessImportance", New List(Of RadioButton)({rbCompletenessImportance1, rbCompletenessImportance2, rbCompletenessImportance3, rbCompletenessImportance4, rbCompletenessImportance5, rbCompletenessImportance6}))   '13
        retVal.Add("CompletenessSatisfaction", New List(Of RadioButton)({rbCompletenessSatisfaction1, rbCompletenessSatisfaction2, rbCompletenessSatisfaction3, rbCompletenessSatisfaction4, rbCompletenessSatisfaction5, rbCompletenessSatisfaction6})) '14
        retVal.Add("ConditionImportance", New List(Of RadioButton)({rbConditionImportance1, rbConditionImportance2, rbConditionImportance3, rbConditionImportance4, rbConditionImportance5, rbConditionImportance6}))    '15
        retVal.Add("ConditionSatisfaction", New List(Of RadioButton)({rbConditionSatisfaction1, rbConditionSatisfaction2, rbConditionSatisfaction3, rbConditionSatisfaction4, rbConditionSatisfaction5, rbConditionSatisfaction6}))  '16
        retVal.Add("DeliveryImportance", New List(Of RadioButton)({rbDeliveryImportance1, rbDeliveryImportance2, rbDeliveryImportance3, rbDeliveryImportance4, rbDeliveryImportance5, rbDeliveryImportance6}))   '17
        retVal.Add("DeliverySatisfaction", New List(Of RadioButton)({rbDeliverySatisfaction1, rbDeliverySatisfaction2, rbDeliverySatisfaction3, rbDeliverySatisfaction4, rbDeliverySatisfaction5, rbDeliverySatisfaction6})) '18
        retVal.Add("SetupImportance", New List(Of RadioButton)({rbSetupImportance1, rbSetupImportance2, rbSetupImportance3, rbSetupImportance4, rbSetupImportance5, rbSetupImportance6}))    '19
        retVal.Add("SetupSatisfaction", New List(Of RadioButton)({rbSetupSatisfaction1, rbSetupSatisfaction2, rbSetupSatisfaction3, rbSetupSatisfaction4, rbSetupSatisfaction5, rbSetupSatisfaction6}))  '20
        retVal.Add("ResolutionImportance", New List(Of RadioButton)({rbResolutionImportance1, rbResolutionImportance2, rbResolutionImportance3, rbResolutionImportance4, rbResolutionImportance5, rbResolutionImportance6})) '21
        retVal.Add("ResolutionSatisfaction", New List(Of RadioButton)({rbResolutionSatisfaction1, rbResolutionSatisfaction2, rbResolutionSatisfaction3, rbResolutionSatisfaction4, rbResolutionSatisfaction5, rbResolutionSatisfaction6}))   '22
        retVal.Add("BusinessWithImportance", New List(Of RadioButton)({rbBusinessWithImportance1, rbBusinessWithImportance2, rbBusinessWithImportance3, rbBusinessWithImportance4, rbBusinessWithImportance5, rbBusinessWithImportance6}))   '23
        retVal.Add("BusinessWithSatisfaction", New List(Of RadioButton)({rbBusinessWithSatisfaction1, rbBusinessWithSatisfaction2, rbBusinessWithSatisfaction3, rbBusinessWithSatisfaction4, rbBusinessWithSatisfaction5, rbBusinessWithSatisfaction6})) '24
        retVal.Add("ValuedCustomerImportance", New List(Of RadioButton)({rbValuedCustomerImportance1, rbValuedCustomerImportance2, rbValuedCustomerImportance3, rbValuedCustomerImportance4, rbValuedCustomerImportance5, rbValuedCustomerImportance6})) '25
        retVal.Add("ValuedCustomerSatisfaction", New List(Of RadioButton)({rbValuedCustomerSatisfaction1, rbValuedCustomerSatisfaction2, rbValuedCustomerSatisfaction3, rbValuedCustomerSatisfaction4, rbValuedCustomerSatisfaction5, rbValuedCustomerSatisfaction6}))   '26
        retVal.Add("OtherImportance", New List(Of RadioButton)({rbOtherImportance1, rbOtherImportance2, rbOtherImportance3, rbOtherImportance4, rbOtherImportance5, rbOtherImportance6}))    '27
        retVal.Add("OtherSatisfaction", New List(Of RadioButton)({rbOtherSatisfaction1, rbOtherSatisfaction2, rbOtherSatisfaction3, rbOtherSatisfaction4, rbOtherSatisfaction5, rbOtherSatisfaction6}))  '28
        Return retVal
    End Function
End Class