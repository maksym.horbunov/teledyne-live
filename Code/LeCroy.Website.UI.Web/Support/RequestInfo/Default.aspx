<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Support_RequestInfo" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
<ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p>
            <h1><asp:Literal ID="litOne" runat="server" /></h1><br />
            <asp:Label ID="lblMessage" runat="server" Visible="False" />
            <asp:Panel ID="ReqInfoContent" runat="server">
                <table width="500" border="0" cellspacing="0" cellpadding="0" id="Table1">
                    <tr>
                        <td>
                            <font face="verdana" size="2"><br>
                                <asp:Literal ID="litTwo" runat="server" /><asp:Label ID="lblLitIntro" runat="server" /><br><br>
                                <font size="2"><asp:Literal ID="litThree" runat="server" /></font><br>
                                <hr size="1" color="#1671CC">
                            </font>
                            <table border="0" cellspacing="0" cellpadding="0" id="Table2">
                                <tr><td><font face="Verdana" color="#000000" size="2"><a name="demo"></a><font face="Verdana,Arial,Helvetica" size="3" color="#1671CC"><br><strong><asp:Literal ID="litFour" runat="server" /></strong></font></font></td></tr>
                                <tr height="1"><td height="1" bgcolor="#1671CC"></td></tr>
                                <tr>
                                    <td valign="top">
                                        <table border="0" width="100%" cellspacing="0" cellpadding="0" id="Table3">
                                            <tr>
                                                <td valign="top"><font face="Verdana" color="#000000" size="2"><b><asp:Literal ID="litFive" runat="server" /></b></font><br /><br /></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="60%">
                                                    <asp:ListBox ID="lbDemo" runat="server" Rows="8" SelectionMode="Multiple" />
                                                    <input type="hidden" name="RequestType2" value="DEM" id="Hidden1">
                                                    <input type="hidden" name="Remarks2" value="No Remarks for Demo" id="Hidden2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a name="secall"></a><font face="Verdana" color="#000000" size="2"><br><br><font face="Verdana,Arial,Helvetica" size="3" color="#1671CC"><strong><asp:Literal ID="litSix" runat="server" /></strong></font></td>
                                            </tr>
                                            <tr height="1"><td height="1" bgcolor="#1671CC"></td></tr>
                                            <tr>
                                                <td valign="top">
                                                    <table cellpadding="5" cellspacing="0" border="0" id="Table4">
                                                        <tr>
                                                            <td><font face="verdana" size="2"><b><asp:Literal ID="litSeven" runat="server" /></b></font</td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:ListBox ID="lbProdInterest" runat="server" Rows="8" SelectionMode="Multiple" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><font face="verdana" size="2"><b>&nbsp;<br><asp:Literal ID="litEight" runat="server" /></b></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:TextBox ID="SEcallquest" runat="server" Rows="8" TextMode="MultiLine" Width="350px" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a name="lit"></a><font face="Verdana" color="#000000" size="2"><br><font face="Verdana,Arial,Helvetica" size="3" color="#1671CC"><strong><asp:Label ID="lblLiterature" runat="server" Visible="false" /></strong></font></td>
                                            </tr>
                                            <tr height="1"><td height="1" bgcolor="#1671CC"></td></tr>
                                            <tr>
                                                <td>
                                                    <br>
                                                    <asp:Repeater ID="Literature" runat="server">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LITCATID" runat="server" Text='<%#Eval("LITCATID")%>' Visible="false" />
                                                            <strong>
                                                                <%#Eval("LITCATEGORY")%></strong><hr />
                                                            <asp:CheckBoxList ID="lbLit" runat="server" /><br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top"><font face="Verdana" size="2"><input type="hidden" name="MaxRequestsAsked" value="8" id="Hidden5"><asp:Button Style="background-color: #1671CC; color: #FFFFFF; font-weight: bold; border-style: outset" ID="btn_submit" runat="server" EnableViewState="False" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="400" id="Table5">
                                            <tr>
                                                <td>
                                                    <hr /><font face="verdana" size="2" color="#1671CC"><b><asp:Label ID="lblLitMail" runat="server" Text="" Visible="false" /><br></b></font><br>
                                                    <asp:Button ID="btn_info" runat="server" Visible="false" /><br /><% ShowProfile()%><hr />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>