Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_RequestInfo
    Inherits BasePage
    Dim strSQL As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim strEmailBody As String = ""
    Dim strConfirmation As String = ""
    Dim strSpace As String = Chr(13) & Chr(10)

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0218", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0255", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0257", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0259", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0260", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0265", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0625", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = Functions.LoadI18N("SLBCA0626", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Session("RedirectTo") = rootDir & "/Support/RequestInfo/"
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
        ValidateUserNoRedir()

        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.REQUEST_INFO_MENU

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, "")
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            GetDemoModels()
            GetProdInterest()
            ShowLiterature(AppConstants.USA_COUNTRY_CODE)
            'SetFormDataFromPreLoginRedirect()
        End If
        btn_info.Text = LoadI18N("SLBCA0217", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btn_submit.Text = LoadI18N("SLBCA0267", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Protected Sub GetDemoModels()
        Dim i As Integer
        'scopes
        Dim sqlString As String = " SELECT DISTINCT a.PRODUCT_SERIES_ID, a.NAME, a.SORTID,b.CATEGORY_ID " +
                " FROM PRODUCT_SERIES AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
                " INNER JOIN PRODUCT c on b.PRODUCT_ID=c.PRODUCTID WHERE  a.DEMO_YN = 'y'  AND b.CATEGORY_ID in (1) " +
                " and c.disabled='n' ORDER by a.SORTID"
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray())
        'protocols, Pert, USB 3.0 
        sqlString = " SELECT DISTINCT a.PRODUCT_SERIES_ID, a.NAME, a.SORTID,b.CATEGORY_ID " +
        " FROM PRODUCT_SERIES AS a INNER JOIN PRODUCT_SERIES_CATEGORY AS b ON a.PRODUCT_SERIES_ID = b.PRODUCT_SERIES_ID " +
        " INNER JOIN PRODUCT c on b.PRODUCT_ID=c.PRODUCTID WHERE  a.DEMO_YN = 'y'  AND b.CATEGORY_ID in (19,24,25,30) " +
        " and c.disabled='n' ORDER by b.CATEGORY_ID desc, a.NAME"
        ds.Merge(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray()))

        lbDemo.DataSource = ds.Tables(0)
        lbDemo.DataValueField = "PRODUCT_SERIES_ID"
        lbDemo.DataTextField = "NAME"
        lbDemo.DataBind()
        If Not Session("demo") Is Nothing And Not Session("selecteddemocount") Is Nothing Then
            If Len(Session("selecteddemocount")) > 0 Then
                Dim selectedDemo(Session("selecteddemocount")) As String
                selectedDemo = Session("demo")
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Len(Session("demo").ToString) > 0 Then
                        For i = 0 To Session("selecteddemocount") - 1
                            If dr("PRODUCT_SERIES_ID").ToString = selectedDemo(i).ToString Then
                                lbDemo.Items.FindByValue(dr("PRODUCT_SERIES_ID").ToString).Selected = True
                            End If
                        Next
                    End If
                Next
            End If
        End If

    End Sub

    Protected Sub GetProdInterest()
        Dim i As Integer

        'If Not Session("SECall") Is Nothing Then
        '    If Session("SECall").ToString.Length > 0 Then
        '        SEcallckbx.Checked = True
        '    End If
        'End If
        Dim sqlString As String = "SELECT KEY_ID,NAME FROM PRODUCT_INTEREST WHERE POST_YN='y' ORDER BY SORT_ID"
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray())
        lbProdInterest.DataSource = ds.Tables(0)
        lbProdInterest.DataValueField = "KEY_ID"
        lbProdInterest.DataTextField = "NAME"
        lbProdInterest.DataBind()
        If Not Session("SEtocall") Is Nothing And Not Session("SEtocallselectedcount") Is Nothing Then
            If Len(Session("SEtocallselectedcount")) > 0 Then
                Dim selectedProdInterest(Session("SEtocallselectedcount")) As String
                selectedProdInterest = Session("SEtocall")
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Len(Session("SEtocall").ToString) > 0 Then
                        For i = 0 To Session("SEtocallselectedcount") - 1
                            If dr("KEY_ID").ToString = selectedProdInterest(i).ToString Then
                                lbProdInterest.Items.FindByValue(dr("KEY_ID").ToString).Selected = True
                            End If
                        Next
                    End If
                Next
            End If
        End If
        If Not Session("SEcallquest") Is Nothing Then
            If Session("SEcallquest").ToString.Length > 0 Then
                SEcallquest.Text = Session("SEcallquest")
            End If
        End If
    End Sub

    Protected Sub ShowLiterature(ByVal countryid As String) ' Allowed inline sql as the only call was a constant passed into this method
        Dim sqlString As String = "SELECT DISTINCT LITCATID, LITCATEGORY,SORTID FROM V_ALL_LITERATURES WHERE COUNTRY_ID = @COUNTRYID ORDER BY SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@COUNTRYID", countryid.ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Me.Literature.DataSource = ds.Tables(0)
            Me.Literature.DataBind()
            lblLiterature.Visible = True
            lblLiterature.Text = LoadI18N("SLBCA0256", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            lblLitMail.Visible = True
            lblLitMail.Text = LoadI18N("SLBCA0268", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            lblLitIntro.Text = LoadI18N("SLBCA0726", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
    End Sub

    Protected Sub GetLiterature(ByVal LITCATID As String, ByRef dl As CheckBoxList)
        Dim j As Integer
        Dim sqlString As String = "SELECT LITERATURE_ID, NAME FROM V_ALL_LITERATURES WHERE COUNTRY_ID=@COUNTRYID and LITCATID=@LITCATID ORDER BY NAME"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@COUNTRYID", AppConstants.USA_COUNTRY_CODE.ToString()))
        sqlParameters.Add(New SqlParameter("@LITCATID", SQLStringWithOutSingleQuotes(LITCATID.ToString())))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())

        dl.DataSource = ds.Tables(0)
        dl.DataValueField = "LITERATURE_ID"
        dl.DataTextField = "NAME"
        dl.DataBind()
        If Not Session("literature") Is Nothing And Not Session("selectedlitcount") Is Nothing Then
            If Len(Session("selectedlitcount")) > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim selectedLit(Session("selectedlitcount")) As String
                    selectedLit = Session("literature")
                    If Len(Session("literature").ToString) > 0 Then
                        For j = 0 To Session("selectedlitcount") - 1
                            If dr("LITERATURE_ID").ToString = selectedLit(j).ToString Then
                                dl.Items.FindByValue(dr("LITERATURE_ID").ToString).Selected = True
                            End If
                        Next
                    End If
                Next
            End If

        End If

    End Sub

    Protected Sub Literature_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles Literature.ItemDataBound
        Dim rpt As CheckBoxList
        Dim lblseri As Label
        Dim ds As DataSet = New DataSet
        rpt = e.Item.FindControl("lbLit")
        lblseri = e.Item.FindControl("LITCATID")
        GetLiterature(lblseri.Text, rpt)
    End Sub

    Protected Sub ShowProfile()
        Response.Write("<table cellpadding = 0 cellspacing = 2 border =0>")
        Response.Write("<tr><td valign=top><strong>")
        If Session("localeid") <> 1041 Then
            If Len(Session("FirstName")) > 0 Then
                Response.Write(Session("FirstName") & "&nbsp;" & Session("LastName"))
            End If
        Else
            If Len(Session("LastName")) > 0 Then
                Response.Write(Session("LastName") & "&nbsp;" & LoadI18N("SLBCA0160", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            End If
        End If
        Response.Write("</strong></td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Company") & "</td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Address") & "</td></tr>")
        If Len(Session("Address2")) > 0 Then
            Response.Write("<tr><td valign=top>" & Session("Address2") & "</td></tr>")
        End If
        Response.Write("<tr><td valign=top>" & Session("City") & "</td></tr>")
        If Len(Session("State")) > 0 Then
            Response.Write("<tr><td valign=top>" & Session("State") & "</td></tr>")
        End If
        If Len(Session("Zip")) > 0 Then
            Response.Write("<tr><td valign=top>" & Session("Zip") & "</td></tr>")
        End If
        Response.Write("<tr><td valign=top>" & Session("Country") & "</td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Phone") & "</td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Email") & "</td></tr>")
        Response.Write("</table>")
    End Sub

    Protected Sub btn_info_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_info.Click
        SaveRequest(False)
        Response.Redirect("../User/userprofile.aspx")
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click
        HandleSubmit()
        'Try
        '    Functions.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REQUESTINFO").ToString()))   ' DB record is not needed anymore
        'Catch
        'End Try
        'Session("TECHHELPTEMPSTATEID_REQUESTINFO") = Nothing    ' No need for this anymore
        'Session.Remove("TECHHELPTEMPSTATEID_REQUESTINFO")
    End Sub

    Private Sub HandleSubmit()
        If Not (Session("ContactId") Is Nothing) Then
            Try
                'If (Int32.Parse(Session("ContactID")) = 0) Then
                '    SaveFormData()
                'Else
                SaveRequest(True)
                ReqInfoContent.Visible = False
                lblMessage.Text = "<strong>Thank you for your request</strong>" + "<br>" + "<br>" '+ Replace(strConfirmation, strSpace, "<br>") + "<br>" + "<br>"
                lblMessage.Visible = True
                'End If
            Catch
            End Try
        End If

        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
    End Sub

    'Private Sub SaveFormData()
    '    If Not (Session("TECHHELPTEMPSTATEID_REQUESTINFO") Is Nothing) Then ' Do not create another session state if user already has one - look it up and verify
    '        Dim fetchedTechHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REQUESTINFO").ToString()))
    '        If Not (fetchedTechHelpTempState.TechHelpTempStateId = Guid.Empty) Then ' Valid, exit routine
    '            TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REQUESTINFO").ToString()))
    '        End If
    '    End If

    '    Dim techHelpTempState As TechHelpTempState = New TechHelpTempState()
    '    techHelpTempState.TechHelpTempStateId = Guid.NewGuid()
    '    techHelpTempState.ProductForDemo = GetSelectedListItems(lbDemo)
    '    techHelpTempState.SeProductCall = GetSelectedListItems(lbProdInterest)
    '    techHelpTempState.EnteredInquiry = SEcallquest.Text.Trim()
    '    Session("TECHHELPTEMPSTATEID_REQUESTINFO") = techHelpTempState.TechHelpTempStateId.ToString()
    '    TechHelpTempStateRepository.InsertTechHelpTempStateRecord(techHelpTempState)
    'End Sub

    Private Function GetSelectedListItems(ByVal lb As ListBox) As String
        Dim retVal As String = String.Empty
        For i As Int32 = 0 To lb.Items.Count - 1
            If (lb.Items(i).Selected) Then
                retVal += String.Format("{0},", lb.Items(i).Value)
            End If
        Next
        If Not (String.IsNullOrEmpty(retVal)) Then retVal = retVal.Substring(0, retVal.Length - 1)
        Return retVal
    End Function

    'Private Sub SetFormDataFromPreLoginRedirect()
    '    If Not (Session("TECHHELPTEMPSTATEID_REQUESTINFO") Is Nothing) Then
    '        Dim techHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID_REQUESTINFO").ToString()))
    '        If (techHelpTempState Is Nothing) Then
    '            Return
    '        End If
    '        If Not (techHelpTempState.TechHelpTempStateId = Guid.Empty) Then    ' Valid, bind page controls
    '            If Not (String.IsNullOrEmpty(techHelpTempState.ProductForDemo)) Then
    '                Dim selectedProducts As List(Of Int32) = New List(Of Int32)(Array.ConvertAll(techHelpTempState.ProductForDemo.Split(","), Function(x) Int32.Parse(x)).ToList())
    '                For i As Int32 = 0 To lbDemo.Items.Count - 1
    '                    If (selectedProducts.Contains(lbDemo.Items(i).Value)) Then
    '                        lbDemo.Items(i).Selected = True
    '                    End If
    '                Next
    '            End If
    '            If Not (String.IsNullOrEmpty(techHelpTempState.SeProductCall)) Then
    '                Dim selectedSeProducts As List(Of Int32) = New List(Of Int32)(Array.ConvertAll(techHelpTempState.SeProductCall.Split(","), Function(x) Int32.Parse(x)).ToList())
    '                For i As Int32 = 0 To lbProdInterest.Items.Count - 1
    '                    If (selectedSeProducts.Contains(lbProdInterest.Items(i).Value)) Then
    '                        lbProdInterest.Items(i).Selected = True
    '                    End If
    '                Next
    '            End If
    '            SEcallquest.Text = techHelpTempState.EnteredInquiry
    '        End If
    '        btn_info.Visible = True
    '    End If
    'End Sub

    Protected Sub SaveRequest(ByVal flgInsert As Boolean)
        Dim countdemo As Integer = 0
        Dim countprodinterest As Integer = 0
        Dim countlit As Integer = 0
        Dim i, j, k As Integer
        Dim arrDemo(1) As String
        Dim arrProdInterest(1) As String
        Dim arrLit(1) As String
        Dim firstProductSeriesId As Int32 = -1
        Dim firstCategoryId As Int32 = -1
        'demo requests
        For i = 0 To lbDemo.Items.Count - 1
            If lbDemo.Items(i).Selected = True Then
                If (firstProductSeriesId < 0) Then firstProductSeriesId = Int32.Parse(lbDemo.Items(i).Value)
                ReDim Preserve arrDemo(i)
                arrDemo(countdemo) = lbDemo.Items(i).Value
                countdemo = countdemo + 1
                If flgInsert Then
                    Functions.InsertRequest(Session("ContactId"), 2, lbDemo.Items(i).Value, "", "", Session("PageID"), Session("campaignid"))
                    If countdemo = 1 Then
                        strEmailBody = LoadI18N("SLBCA0340", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + Functions.GetSeriesNameonID(lbDemo.Items(i).Value).ToString + strSpace
                    Else
                        strEmailBody += Functions.GetSeriesNameonID(lbDemo.Items(i).Value).ToString + strSpace
                    End If
                End If
            End If
        Next
        If countdemo > 0 Then
            Session("selecteddemocount") = countdemo
            Session("demo") = arrDemo
        Else
            Session("selecteddemocount") = ""
            Session("demo") = ""

        End If
        'Response.Write(Session("selecteddemocount") & "  " & Session("demo").ToString.Length)

        ''product interest
        'If SEcallckbx.Checked Then
        '    Session("SECall") = "true"
        'Else
        '    Session("SECall") = ""
        'End If

        If (firstProductSeriesId > 0) Then firstCategoryId = GetCategoryIdFromProductSeriesId(firstProductSeriesId)

        If SEcallquest.Text.ToString.Length > 0 Then
            Session("SEcallquest") = SEcallquest.Text
        End If
        For j = 0 To lbProdInterest.Items.Count - 1
            If lbProdInterest.Items(j).Selected = True Then
                If (firstCategoryId < 0) Then firstCategoryId = ReverseLookupProductInterestKeyToCategory(lbProdInterest.Items(j).Value)
                ReDim Preserve arrProdInterest(j)
                arrProdInterest(countprodinterest) = lbProdInterest.Items(j).Value
                countprodinterest = countprodinterest + 1
                If flgInsert Then
                    Functions.InsertRequest(Session("ContactId"), 3, SQLStringWithOutSingleQuotes(lbProdInterest.Items(j).Value), SQLStringWithOutSingleQuotes(SEcallquest.Text), "", Session("PageID"), Session("campaignid"))
                    If countprodinterest = 1 Then
                        If Len(strEmailBody) > 0 Then
                            strEmailBody += strSpace
                        End If
                        strEmailBody += LoadI18N("SLBCA0632", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + Functions.GetProductInterestonID(lbProdInterest.Items(j).Value).ToString + strSpace
                    Else
                        strEmailBody += Functions.GetProductInterestonID(lbProdInterest.Items(j).Value).ToString + strSpace
                    End If
                End If
            End If
        Next
        If SEcallquest.Text.ToString.Length > 0 Then
            If Len(strEmailBody) > 0 Then
                strEmailBody += strSpace
                strEmailBody += LoadI18N("SLBCA0631", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + SEcallquest.Text + strSpace
            End If

        End If
        If countprodinterest > 0 Then
            Session("SEtocall") = arrProdInterest
            Session("SEtocallselectedcount") = countprodinterest
        Else
            Session("SEtocall") = ""
            Session("SEtocallselectedcount") = ""
        End If

        ' Response.Write(Session("SEtocallselectedcount") & "  " & Session("SEtocall").ToString.Length)

        'literature
        Dim countalllit As Integer = 0
        For i = 0 To Literature.Items.Count - 1
            Dim currItem As RepeaterItem = Literature.Items(i)
            Dim chk1 As CheckBoxList = CType(currItem.FindControl("lbLit"), CheckBoxList)
            For k = 0 To chk1.Items.Count - 1
                If chk1.Items(k).Selected = True Then
                    ReDim Preserve arrLit(countalllit)
                    arrLit(countlit) = chk1.Items(k).Value
                    countlit = countlit + 1
                    countalllit = countalllit + 1
                    If flgInsert Then
                        Functions.InsertRequest(Session("ContactId"), 5, chk1.Items(k).Value, "", "", Session("PageID"), Session("campaignid"))
                        If countalllit = 1 Then
                            If Len(strEmailBody) > 0 Then
                                strEmailBody += strSpace
                            End If
                            strEmailBody += LoadI18N("SLBCA0339", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + Functions.GetLiteratureNameonID(chk1.Items(k).Value).ToString + strSpace
                        Else
                            strEmailBody += Functions.GetLiteratureNameonID(chk1.Items(k).Value).ToString + strSpace
                        End If

                    End If
                End If
            Next

        Next
        If countlit > 0 Then
            Session("selectedlitcount") = countlit
            Session("literature") = arrLit
            ' Response.Write(Session("selectedlitcount") & "  " & Session("literature").ToString.Length)
        Else
            Session("selectedlitcount") = ""
            Session("literature") = ""
        End If
        If Len(strEmailBody) > 0 And flgInsert Then
            strConfirmation = strEmailBody
            strEmailBody = "New request(s)" + strSpace + strSpace + Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + strSpace + strSpace + strEmailBody
            Dim strSubject As String = "New request(s)"
            If countdemo > 0 Then
                strSubject += " Demo "
            End If
            If countprodinterest > 0 Then
                strSubject += " Call "
            End If
            If countalllit > 0 Then
                strSubject += " Lit "
            End If
            strSubject += " - " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            If Len(Functions.GetTacticIdOnCampaignID(Session("CampaignID"))) > 0 Then
                strSubject += " - " & Functions.GetTacticIdOnCampaignID(Session("CampaignID"))
            End If
            Functions.SendEmail(Session("country").ToString, strEmailBody, Functions.GetSalesRepEmail(Session("country"), 3, firstCategoryId), Session("email").ToString(), "", "", strSubject, "")
        End If
    End Sub

    Private Function GetCategoryIdFromProductSeriesId(ByVal productSeriesId As Int32) As Int32
        Dim productSeriesCategories As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId).Where(Function(x) x.CategoryId <> 12 And x.CategoryId <> 26).ToList()
        If (productSeriesCategories.Count > 0) Then
            Return productSeriesCategories.Select(Function(x) x.CategoryId).FirstOrDefault()
        End If
        Return LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
    End Function

    Private Function ReverseLookupProductInterestKeyToCategory(ByVal productInterestKeyId) As Int32
        Select Case productInterestKeyId
            Case 9
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.PROBES
            Case 10, 11, 12, 13, 14, 15, 16, 17, 19
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS
            Case 24
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.PERT3_SYSTEMS
            Case Else
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
        End Select
    End Function
End Class