﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_RoHS" Codebehind="RoHS.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_rohs.gif" alt="RoHS & WEEE" /></p>
        <p>Teledyne LeCroy has established a communication link for customers to access information related to the European RoHS &amp; WEEE and China RoHS.</p>
        <p>For information on a specific resource, please choose from the navigation menu on left hand side:</p>
    </div>
    <div class="accordion">
        <div class="list">EU RoHS</div>
        <div class="faq">
            <table border="0" cellspacing="0" cellpadding="10">
                <tr>
                    <td valign="top" align="left">
                        <p>Teledyne LeCroy recognizes the importance of the European Union's RoHS (Removal of Hazardous Substances) Directive legislation aimed at the reduction or removal of hazardous substances from electrical and electronic equipment. Teledyne LeCroy products are classified as Industrial Monitoring and Control Instruments as defined in Article 3(24) of the European Union RoHS2 (Recast) Directive 2011/65/EU and these products & their associated accessories have been granted a transition period for elimination of the restricted substances until 22 July 2017 (per Article IV, Paragraph 3).</p><p>At Teledyne LeCroy, we have instituted a RoHS transition plan and are working towards realizing our objective of producing RoHS compliant products in advance of the mandatory compliance date (July 22, 2017).</p>
                        <p>In the below file you will find each of our products, it's RoHS implementation status, as well as what visible indicator to look for on the product to determine RoHS2 compliance status on products received directly from Teledyne LeCroy or our authorized distributors.</p>
                        <p>Should you have any questions about the RoHS transition or compliance status, please contact Mark DeVries, Corporate Quality Manager at <a href="mailto:mark.devries@teledynelecroy.com">mark.devries@teledynelecroy.com</a></p>
                        <p style="background-image:url('/images/icons/datasheets_icon16x16.png');background-position:0 5px;background-repeat:no-repeat;margin:0;padding:5px 0 9px 28px;"><a href="/doc/rohs-compliance-status">Teledyne LeCroy Product List</a></p>
                    </td>
                </tr>
            </table>
        </div>
        <div class="list">China RoHS</div>
        <div class="faq">
            <table border="0" cellspacing="0" cellpadding="10">
                <tr>
                    <td valign="top" align="left">
                        <br />Teledyne LeCroy products are fully compliant with the China RoHS requirements that went into effect on 1 March 2007.<br /><br />
                        In summary, China RoHS requires the following for Teledyne LeCroy products: inclusion of an appropriate Environmental Friendly Use Period (EFUP) label, inclusion of appropriate recycling labeling on packaging materials, and providing detailed RoHS compliance information in our product Manuals. Teledyne LeCroy has taken a very conservative approach in specifying RoHS compliance information for our products. In the case of labeling packaging material for recycling purposes, Teledyne LeCroy will exceed the requirements that China RoHS mandates.<br /><br />
                        Teledyne LeCroy continues to monitor new environmental regulations in each country that we sell our products and will take the necessary actions to ensure that our products comply with all regulatory requirements.<br /><br /><br /><br />
                    </td>
                </tr>
            </table>
        </div>
        <div class="list">EU WEEE</div>
        <div class="faq">
            <table border="0" cellspacing="0" cellpadding="10">
                <tr>
                    <td valign="top" align="left">
                        <br />Teledyne LeCroy has established a communication link for customers to access information related to the European WEEE directive.<br /><br />
                        The following symbol appears on all Teledyne LeCroy products shipped after August 13, 2005:<br />&nbsp;
                        <img alt="" src="images/icon_recycle.jpg" width="75" height="131" />&nbsp;<br /><br />
                        This symbol is a reminder not to dispose of your waste electronic equipment in standard trash receptacles.<br /><br />
                        Teledyne LeCroy is actively defining the take-back and recycling program for EU customers.<br /><br />
                        We are registered in the following countries:
                        <ul>
                            <li>France</li>
                            <li>Germany: WEEE-Reg.-Nr. DE 14397960</li>
                            <li>Italy</li>
                            <li>Sweden</li>
                            <li>United Kingdom</li>
                        </ul>
                        If you are a customer in the EU and need assistance, please contact Teledyne LeCroy for instructions on how to return your product by emailing <a href="mailto:eurecycling@teledynelecroy.com">eurecycling@teledynelecroy.com</a> and include your:
                        <ul>
                            <li>Company Name and Address</li>
                            <li>Contact Information (Name, Email, Phone)</li>
                            <li>Model</li>
                            <li>Serial Number</li>
                        </ul><br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>