﻿Imports LeCroy.Library.VBUtilities

Partial Class Support_RoHS
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "RoHS &amp; WEEE"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - RoHS and WEEE"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = AppConstants.ROHS_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
        pn_leftmenu.Visible = False
    End Sub
End Class