<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SecurityPolicy" Codebehind="SecurityPolicy.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="#1">Privacy Pledge</a></li>
        <li><a href="#2">Unsubscribe Pledge</a></li>
        <li><a href="#3">Information Security</a></li>
        <li><a href="#4">Cookies</a></li>
        <li><a href="#5">Contact Information</a></li>
        <li><a href="#6">Personally Identifiable Data</a></li>
        <li><a href="#7">Privacy Policy Changes</a></li>
        <li><a href="#8">Acceptable Use</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <img src='<%= rootDir%>/images/category/headers/hd_privacy.gif' alt='Web Site Visitor Private Policy'>
        <h3>
            (effective November 2020)
        </h3>
        <table border="0" width="500" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top" align="left" height="38">
                    <p>By visiting this web site, you accept the provisions of these Privacy and Acceptable Use Statements.</p>
                    <p><strong><a name="1">Teledyne LeCroy's Privacy Pledge:</strong><strong><br></strong>Teledyne LeCroy strongly believes in protecting your privacy, and any personal information you share with us. We request information from you on our website only for the purpose of serving you better. If you choose to provide information, we will not sell that information to anyone else.</p><p>Reference our written security policy here:<a href="https://www.teledyne.com/privacy-policy"> teledyne.com/privacy-policy</a>. </p>
                    <p><a href="#top">back to top</a></p>
                    <p><strong><a name="2">Unsubscribe Pledge:</strong><br>If you choose not to receive electronic communications from Teledyne LeCroy ('opt-out'), or choose not to receive a specific category of communication ('unsubscribe'), your choice will be honored promptly.<br><br>Note that you may receive information about Teledyne LeCroy from other sources, such as trade journals, if you have not opted-out of their communication streams.</p>
                    <p><a href="#top">back to top</a></p>
                    <p><strong><a name="3">Information Security:<br></strong>We have implemented a variety of technical, physical and administrative security measures to protect your personal information from improper use or unauthorized access. Teledyne LeCroy enforces strict information security policies to ensure the security of our customer's oscilloscopes, protocol analyzers, and other equipment. On our website we make sure no one else uses your identity through a process of password authentication.</p>
                    <p><a href="#top">back to top</a></p>
                    <p><strong><a name="4">Cookies:&nbsp;<br></strong>To help us recognize you as a return visitor to our site, we place a simple line of recognition text on your hard drive. When you come back to request a demo or to download software, after verifying your username and password, the registration forms will automatically be completed for you.<br><br>Cookied visitors can download materials, get instant quotes, and register for webinars, without filling out long forms.<br><br>Teledyne LeCroy's cookie is completely harmless &ndash; it contains no executable code.&nbsp;</p>
                    <p><a href="#top">back to top</a></p>
                    <p><strong><a name="5">Contact Information:&nbsp;<br></strong>If you request Teledyne LeCroy contact you for a demo, quote, or a salesperson's call, please make sure you've filled in either a phone number or email where we can reach you.<br><br>If you have questions, wish to send us comments about our written privacy policy or other policies, or would like a copy of your personal information on file  , please send an e-mail with your questions or comments to <a href="mailto:Webmaster@teledynelecroy.com">Webmaster@teledynelecroy.com</a> or write us at:&nbsp;<br><br>Teledyne LeCroy<br>Attention: Webmaster<br>700 Chestnut Ridge Road<br>Chestnut Ridge, New York 10977.</p>
                    <p><a href="#top">back to top</a></p>
                    <p class="SmallBody" align="left"><strong><a name="6">Personally Identifiable Data:&nbsp;&nbsp;<br></strong>Teledyne LeCroy will not request personally identifiable data from you online (such as social security number, driver�s license number, bank account or credit card number or password).<br><br>On our website we request only data that enables us to provide you with information helpful in solving your test and measurement challenges; name and address, job title and function, email, website and fax details, and your test, measurement, or analysis application.</p>
                    <p><a href="#top">back to top</a></p>
                    <p class="SmallBody" align="left"><strong><a name="7">Privacy Policy Changes:&nbsp;<br></strong>All material changes to Teledyne LeCroy's Privacy Protection Policy will be posted on this page. The revision date shown at the top of the page will be updated accordingly.</p>
                    <p><a href="#top">back to top</a></p>
                    <p class="SmallBody" align="left"><strong><a name="8">Acceptable Use:&nbsp;<br></strong>Teledyne LeCroy's website is provided for the use of the Test and Measurement community in the furtherance of the field. Those who visit our site out of interest in measurement equipment, and those considering an investment in, or employment by, Teledyne LeCroy, are welcome. Visits to the site for other purposes without the consent of Teledyne LeCroy are prohibited. Prohibited uses include any form of unsolicited testing, changes to site contents or links, malicious acts, and any acts which have the effect of denying access to this site to other users. Intentional damage to the site or its users will be prosecuted.&nbsp;<br><br>Nothing contained in this web site may be reproduced without Teledyne LeCroy's written permission.</p>
                    <p class="SmallBody" align="left">The content of this web site is provided "as is", without warranty.</p>
                    <p><a href="#top">back to top</a></p>
                    <p class="SmallBody" align="left">&nbsp;</p>
                </td>
                <td valign="top" rowspan="2" align="left"></td>
            </tr>
            <tr><td valign="top" height="70"></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding">
        <img src="<%= rootDir%>/images/sidebar/rt_promo_mtype.jpg" alt="Purchase Terms and Conditions" />
    </div>
</asp:Content>