Partial Class Support_SecurityPolicy
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Teledyne LeCroy"
        Me.Master.PageContentHeader.BottomText = "Website Policy"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Web Site Visitor Private Policy"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("menuSelected") = ""
    End Sub
End Class