﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Service" Codebehind="Service.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_instr.gif" alt="Instrument Service"></p>
        <h2>Service and Support:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-800-553-2769</h2>
        <h2>Protocol Solutions Group:&nbsp;&nbsp;&nbsp;1-800-909-7112</h2>
        <p>Building long-term relationships with our customers is a deeply-held commitment at Teledyne LeCroy. This commitment applies to our product development, manufacturing, sales, and support processes. It is reflected in the unprecedented level of customization that we build into our products, enabling the companies who use our equipment to easily tailor solutions for their specific applications.</p>
        <p>At Teledyne LeCroy, we believe an important way that we can provide significant added value for our customers is by extending the usable life of their Teledyne LeCroy equipment. We believe our service practices are the best in the industry:</p>
        <ul>
            <li>Service Centers located around the world to speed repair and calibration of all Teledyne LeCroy products</li>
            <li>3 year warranties on Windows based real time oscilloscopes&nbsp;</li>
            <li>Teledyne LeCroy protocol systems are warranted to operate within our documented specifications for 12 months except for all PCIE Gen 3 card-based systems (e.g. PCIe Z3 Exerciser), interposers, test platforms and probes which are warranted to operate within our documented specifications for 90 days from date of purchase. The PCIe Gen 4 Z416 Exerciser is also warranted for 12 months. All other accessories, adapters, cables, and extenders are warranted to operate within our documented specifications for 30 days from date of purchase.</li>
            <li>1 year warranties on sampling oscilloscopes and associated plug-in modules</li>
            <li>7 year long term oscilloscope product repair support after a product has been discontinued</li>
            <li>Oscilloscope Product calibration support guaranteed for 7 years after the product is discontinued and best efforts after 7 years.&nbsp;</li>
            <li>Goal of less than five days turnaround time in our Service Centers on most repairs</li>
        </ul>
        <hr />
        <div class="accordion">
            <div class="list">Obsolescence Protection</div>
            <div class="faq"><p>We believe we are unique in the T&amp;M Industry in offering a variety of plans to enable our customers to not only get the best equipment for the job at hand, but to finance it in a way that best matches their requirements. We call this program "Open Access", and it contains a family of plans to choose from. The plans are designed to ensure that engineers have the best instrumentation without expensive capital equipment purchases each and every time. They include a rental plan, a lease plan and a subscription service. Contact Teledyne LeCroy Customer Support for details.</p></div>
            <div class="list">After-sale Option Add-ons and Upgrades</div>
            <div class="faq"><p>Teledyne LeCroy has made it a standard policy that customers may add after-sale options at any time without stiff pricing penalties. This practice has enabled our customers to grow their instrument capabilities as required.</p></div>
            <div class="list">Software Enhancements</div>
            <div class="faq"><p>Teledyne LeCroy customers may upgrade to the latest software revisions without charge. Downloads of the latest instrument software can be found under the "Service and Support" menu, Software Download menu selection.</p></div>
            <div class="list">Retrofit of New Features</div>
            <div class="faq"><p>When technically feasible, we make product extensions and new features available for existing equipment. These may include capabilities associated with an "A" version of a product as well as features enabled by software/firmware upgrades.</p></div>
            <div class="list">Sales and Technical Support</div>
            <div class="faq"><p>Our highly trained and knowledgeable sales and technical professionals are always available to help Teledyne LeCroy customers solve their test and measurement challenges. Their application, product and technology understanding is second-to-none. Teledyne LeCroy is committed to setting the standard for expert test and measurement solutions backed by dedicated customer service for the life of your product. Sales and service staff work hand in hand to ensure that Teledyne LeCroy customers get the maximum value from their equipment investment.</p></div>
            <div class="list">How do I get my instrument serviced?</div>
            <div class="faq"><p>Using the list provided below, contact the service center nearest you and provide the instrument model and serial number. Once we determine the warranty status of the instrument, we will issue you a Return Authorization (RA) number. Ship the instrument to us and reference that RA number. We strive to turn around repairs in 5 days.</p></div>
            <div class="list">Teledyne LeCroy Warranty Statement</div>
            <div class="faq">
                <p>Teledyne LeCroy warrants its digital oscilloscopes to operate within specifications under normal use and service for a period of three years from the date of shipment. Teledyne LeCroy protocol systems are warranted to operate within our documented specifications for 12 months except for all PCIE Gen 3 card-based systems (e.g. PCIe Z3 Exerciser), interposers, test platforms and probes which are warranted to operate within our documented specifications for 90 days from date of purchase. The PCIe Gen 4 Z416 Exerciser is also warranted for 12 months.  All other accessories, adapters, cables, and extenders are warranted to operate within our documented specifications for 30 days from date of purchase. All other instruments (including sampling oscilloscopes and associated plug-in modules) are warranted for one year.</p>
                <p>Component products, replacement parts, and repairs are warranted for 90 days. Software is thoroughly tested but is supplied as-is, with no warranty of any kind covering detailed performance. Accessory products not manufactured by Teledyne LeCroy are covered by the original equipment manufacturer's warranty only. In exercising this warranty, Teledyne LeCroy will repair or, at its option, replace any product returned to the factory or an authorized service facility within the warranty period, provided the warrantor's examination discloses that the product is defective due to workmanship or materials and has not been caused by misuse, neglect, accident, or abnormal conditions or operations.</p>
                <p>The purchaser is responsible for the transportation and insurance charges arising from the return of products to the servicing facility. Teledyne LeCroy will return all in-warranty products with transportation prepaid. This warranty is in lieu of all other warranties, express or implied, including but not limited to any implied warranty of merchantability, fitness, or adequacy for any particular purpose or use. Teledyne LeCroy will not be liable for any special, incidental, or consequential damages, whether in contract or otherwise.</p>
            </div>
            <div class="list">Long Term Support for Discontinued Products</div>
            <div class="faq">
                <p>The "Long Term Product Support" period for Teledyne LeCroy oscilloscope products begins on the last date of manufacture of the product. For a period of 7 years thereafter, Teledyne LeCroy will provide technical support, calibration and repair services*, and software upgrades to the last-issued revision. After 7 years of Long Term Support, Teledyne LeCroy products will receive "Limited Support". Teledyne LeCroy will continue to provide calibration services or instructions. Repair services and technical support will be provided for an additional year (making a total of 8 years). All support in the Limited Support phase will be provided on a best efforts basis.</p>
                <p>*<i>In rare cases where a unique component may not be available, Teledyne LeCroy will assist the customer in migrating to a replacement product.</i></p>
            </div>
            <div class="list">Service and Support Center Locations and Contact Info</div>
            <div class="faq"><p><asp:Label ID="lblServiceCenters" runat="server" /></p></div>
        </div><br /><br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>