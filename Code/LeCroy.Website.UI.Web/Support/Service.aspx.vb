﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_Service
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds As DataSet
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Instrument Service"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Service and Support"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim captionID As String = ""
            Dim menuID As String = ""
            captionID = AppConstants.SUPPORT_CAPTION_ID
            menuID = AppConstants.INSTRU_SERV_MENU
            menuURL = "&capid=" + captionID
            menuURL += "&mid=" + menuID
            menuURL2 = "?capid=" + captionID
            menuURL2 += "&mid=" + menuID
            Session("menuSelected") = captionID
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If

        Dim result As StringBuilder = New StringBuilder()
        strSQL = "SELECT * FROM CONTACT_OFFICE WHERE SERVICE_YN='y' ORDER BY ServiceSortID"
        'result.Append strSQL & "<br>"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        Dim num As Integer = 0
        num = ds.Tables(0).Rows.Count
        If num > 0 Then
            result.Append("<table cellspacing=""0"" cellpadding=""0"" width=""80%"" border=0>")
            For i As Integer = 0 To num - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                result.Append("<tr>")
                result.Append("<td >&nbsp;</td>")
                result.Append("</tr>")
                result.Append("<tr>")
                result.Append("<td valign=top width=300>")
                result.Append("<font face=""Verdana,Arial,Helvetica"" size=""2"" color=""#00008b""><b>" & dr("SERVICE_TITLE") & "</b></font>")
                result.Append("<hr color=#dcdcdc size=1>")
                result.Append("</td>")
                result.Append("</tr>")
                result.Append("<tr>")
                result.Append("<td valign=top>")
                result.Append("" & dr("COMPANY"))
                result.Append("</td>")
                result.Append("</tr>")
                result.Append("<tr>")
                result.Append("<td valign=top>")
                result.Append("" & dr("ADDRESS1"))
                result.Append("</td>")
                result.Append("</tr>")
                If Len(Trim(dr("ADDRESS2"))) > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("" & dr("ADDRESS2"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("ADDRESS3").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("" & dr("ADDRESS3"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("ADDRESS4").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("" & dr("ADDRESS4"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("ADDRESS5").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("" & dr("ADDRESS5"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If Len(Trim(dr("PHONE"))) > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("")
                    result.Append("<b>Phone:&nbsp;</b>")
                    result.Append(dr("PHONE"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("FAX").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("")
                    result.Append("<b>Fax:&nbsp;</b>")
                    result.Append(dr("FAX"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("SALES_FAX").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top>")
                    result.Append("<b>Fax(Sales&Service):&nbsp;</b>" & dr("SALES_FAX"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("EMAIL").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top CLASS=""AWN"">")
                    result.Append("<b>Email Sales:&nbsp;</b></font>")
                    result.Append("<font face=""Verdana,Arial,Helvetica"" size=""2"" color=""blue"" CLASS=""AWN"">")
                    result.Append("<a href=""mailto:" & dr("EMAIL") & """>" & dr("EMAIL") & "</a></font>")
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("WEB").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top CLASS=""AWN"">")
                    result.Append("<b>Web Site:&nbsp;</b></font>")
                    result.Append("<font face=""Verdana,Arial,Helvetica"" size=""2"" color=""blue"" CLASS=""AWN"">")
                    result.Append("<a href=""" & dr("WEB") & """>" & dr("WEB") & "</a></font>")
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("PHONE_SERVICE").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top><br>")
                    result.Append("<b>Phone Service:&nbsp;</b>" & dr("PHONE_SERVICE"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("EMAIL_SERVICE").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top CLASS=""AWN"">")
                    result.Append("<b>Email Service:&nbsp;</b></font>")
                    result.Append("<font face=""Verdana,Arial,Helvetica"" size=""2"" color=""blue"" CLASS=""AWN"">")
                    result.Append("<a href=""mailto:" & dr("EMAIL_SERVICE") & """>" & dr("EMAIL_SERVICE") & "</a></font>")
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
                If dr("PHONE_SUPPORT").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top><br>")
                    result.Append("<b>Phone Support:&nbsp;</b>" & dr("PHONE_SUPPORT"))
                    result.Append("</td>")
                    result.Append("</tr>")
                End If

                If dr("EMAIL_SUPPORT").ToString().Trim().Length > 0 Then
                    result.Append("<tr>")
                    result.Append("<td valign=top CLASS=""AWN"">")
                    result.Append("<b>Email Support:&nbsp;</b></font>")
                    result.Append("<font face=""Verdana,Arial,Helvetica"" size=""2"" color=""blue"" CLASS=""AWN"">")
                    result.Append("<a href=""mailto:" & dr("EMAIL_SUPPORT") & """>" & dr("EMAIL_SUPPORT") & "</a></font>")
                    result.Append("</td>")
                    result.Append("</tr>")
                End If
            Next
            result.Append("</table><br><br>")
            lblServiceCenters.Text = result.ToString()
        End If
    End Sub
End Class