﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_ActiveDSO" Codebehind="ActiveDSO.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <table border="0" cellpadding="0" cellspacing="0" width="500">
                        <tr>
                            <td>
                                <h1>ActiveDSO</h1>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="font-size:14px;font-weight:bold;color:#0076c0;padding-left:15px;">
                                    <img src="/images/icons/download_icon16x16.png" /> <a href="download.aspx?did=8147">Download ActiveDSO v2.36</a>
                                    <br />
                                    <img src="/images/icons/manuals_icon16x16.png" /> <a href="/doc/docview.aspx?id=9224">ActiveDSO Developer's Guide</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p class="SmallBody" align="left"><strong>ActiveDSO</strong> is an <b>ActiveX<sup>TM</sup></b> control that enables Teledyne LeCroy oscilloscopes and LSA-1000 series embedded signal analyzers to be <b>controlled by </b>and <b>exchange data</b> <b>with</b> a variety of Windows applications that support the ActiveX standard.&nbsp; MS Office programs, Internet Explorer, Visual Basic, Visual C++, Visual Java, and Matlab (v5.3) are a few of the many applications that support ActiveX controls.</p>
                    <p align="left">The ActiveDSO control can be used programmatically and as an embedded control.&nbsp;&nbsp; Software designers can create instances of the ActiveDSO control within their programs, and use the control&#146;s Methods and Properties for instrument communications, setup, and data transfer.&nbsp; With ActiveDSO, all details of the interface bus used to connect to the Teledyne LeCroy instrument are encapsulated within the ActiveDSO control.&nbsp; The intricacies of programming for each of these interfaces is hidden from the user, allowing the software developer to focus on his or her application and to avoid the complexities of the lower-level interface calls.</p>
                    <p align="left">The ActiveDSO control can also be embedded visually in any OLE automation compatible client.&nbsp; Include ActiveDSO in you Microsoft Word document or PowerPoint presentation and easily transfer the bitmap image of the instrument display into your document.&nbsp;&nbsp; No floppy disk required!</p>
                    <p align="left">Whether using the control programmatically or as an embedded object, ActiveDSO helps to integrate of your oscilloscope data with your application:</p>
                    <ul>
                        <li>Generate a report by importing scope data right into <b>Excel</b> or <b>Word</b>.</li>
                        <li>Analyze your Waveforms by bringing them directly into <b>MathCad</b>.</li>
                        <li>Archive measurement results on the fly in a <b>Microsoft Access</b> Database.</li>
                        <li>Automate tests using <b>Visual Basic</b>, <b>Java</b>, <b>C++</b>, <b>Excel</b> (VBA).</li>
                    </ul>
                    <p class="SmallBody" align="left">&nbsp;</p>
                    <p class="SmallBody" align="left">&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td valign="top" height="70">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>