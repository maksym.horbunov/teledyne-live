﻿Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Support_SoftwareDownload_Agreement
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - SAS analyzers, SATA analyzers, Fibre Channel analyzers, PCI Express protocol analyzers, 1394 analyzer, InfiniBand analyzers, USB protocol analyzers, Bluetooth protocol analyzers"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
    End Sub

    Protected Sub Accept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Accept.Click
        Response.Redirect("download.aspx?accepted=y" + menuURL)
    End Sub

    Protected Sub Reject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Reject.Click
        Response.Redirect("default.aspx" + menuURL2)
    End Sub
End Class
