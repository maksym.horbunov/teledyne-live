﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_ArbStudio" Codebehind="ArbStudio.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p>
            <img src='<%= rootDir%>/images/category/headers/hd_download.gif' alt='Software Download' />
        </p>
        <p>
            <h2>
                Waveform Generators Software Utilities</h2>
        </p>
        <br>
        <b>Note: </b>
        <br>
        You may be redirected to a short form if you are not a registered user.&nbsp;<br>
        <br>
        <img src="images/C0C0C0.gif" width="1" height="1" border="0">
        <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3">
            <tr>
                <td valign="top" height="100%" width="20" style="background: #C0C0C0; color: #C0C0C0">
                    <img src="images/clearpixel.gif" width="1" height="1" border="0">
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0">
                    <b>Download</b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b>Version</b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b>
                        <nobr>&nbsp;&nbsp;&nbsp;File Size</nobr>
                    </b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b>
                        <nobr>&nbsp;&nbsp;Released</nobr>
                    </b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    &nbsp;<b>Download</b>
                </td>
            </tr>
            <asp:Repeater ID="rpt_main" runat="server">
                <ItemTemplate>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" height="1" colspan="5">
                            <hr noshade color="#C0C0C0" size="1">
                        </td>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="/images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" height="19">
                            <b><asp:Literal ID="litTitle" runat="server" /></b><br><asp:Literal ID="litDescription" runat="server" />
                        </td>
                        <td valign="top" height="19" align="center">
                            <asp:Literal ID="litVersion" runat="server" />
                        </td>
                        <td valign="top" height="19" align="center">
                            <asp:Literal ID="litDocSize" runat="server" />
                        </td>
                        <td valign="top" height="19" align="center">
                            <asp:Literal ID="litDateEntered" runat="server" />
                        </td>
                        <td valign="top" height="19" align="center">
                            <p align="center"><asp:HyperLink ID="hlkDownload" runat="server" Text="Download" /></p>
                        </td>
                        <td valign="top" height="100%" width="20">
                            <img src="/images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                    </tr>
                    <tr id="trXrefs" runat="server" visible="false">
                        <td colspan="7" style="padding-left: 20px;" valign="top">
                            <asp:Repeater ID="rptXrefs" runat="server">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlkXRef" runat="server" Target="_blank" /><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" colspan="5">
                            <br><i><asp:Literal ID="litComments" runat="server" /></i><br><br>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="6">
                    <asp:Label ID="lbNoData" runat="server" Text="New Software will be released shortly." Visible="False" Font-Bold="False" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>