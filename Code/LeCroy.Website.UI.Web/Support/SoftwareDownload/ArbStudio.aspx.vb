﻿Imports System.Data
Imports System.Data.SqlClient
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_ArbStudio
    Inherits BasePage
    Public mseries As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Private _documentXrefs As List(Of DocumentXRef) = New List(Of DocumentXRef)
    Private _childDocumentsViaXref As List(Of Document) = New List(Of Document)

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Core Software Download for ArbStudio Arbitrary Waveform Generator"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
        Initial()
    End Sub

    Private Sub Initial()
        Dim documentCategories As List(Of DocumentCategory) = DocumentCategoryRepository.FetchDocumentCategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT [DOCUMENT_ID] FROM [DOCUMENT_CATEGORY] WHERE [CATEGORY_ID] = 2", New List(Of SqlParameter)().ToArray()).ToList()
        If (documentCategories.Count > 0) Then
            Dim documents As List(Of Document) = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), documentCategories.Select(Function(x) x.DocumentId.Value).ToList()).Where(Function(x) x.DocTypeId = 14 And x.Post.ToUpper() = "Y").ToList()
            If (documents.Count > 0) Then
                _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
                If (_documentXrefs.Count > 0) Then
                    _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
                End If
                rpt_main.DataSource = documents.OrderBy(Function(x) x.SortId).ToList()
                rpt_main.DataBind()
            Else
                rpt_main.Visible = False
                lbNoData.Visible = True
            End If
        End If
    End Sub

    Private Sub rpt_main_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rpt_main.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim litTitle As Literal = CType(e.Item.FindControl("litTitle"), Literal)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)
                Dim litVersion As Literal = CType(e.Item.FindControl("litVersion"), Literal)
                Dim litDocSize As Literal = CType(e.Item.FindControl("litDocSize"), Literal)
                Dim litDateEntered As Literal = CType(e.Item.FindControl("litDateEntered"), Literal)
                Dim hlkDownload As HyperLink = CType(e.Item.FindControl("hlkDownload"), HyperLink)
                Dim litComments As Literal = CType(e.Item.FindControl("litComments"), Literal)
                Dim trXrefs As HtmlTableRow = CType(e.Item.FindControl("trXrefs"), HtmlTableRow)
                Dim rptXrefs As Repeater = CType(e.Item.FindControl("rptXrefs"), Repeater)

                litTitle.Text = row.Title
                litDescription.Text = IIf(String.IsNullOrEmpty(row.Description), String.Empty, String.Format("{0}<br /><br />", row.Description))
                litVersion.Text = row.Version
                litDocSize.Text = row.DocSize
                litDateEntered.Text = row.DateEntered.ToString("dd-MMM-yyyy")
                hlkDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}{2}", mseries.ToString(), row.DocumentId.ToString(), menuURL)
                If (_documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Count > 0) Then
                    trXrefs.Visible = True
                    AddHandler rptXrefs.ItemDataBound, AddressOf rptXrefs_ItemDataBound
                    Dim docFetch As List(Of Document) = (From c In _childDocumentsViaXref Where _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).ToList()
                    If (docFetch.Count > 0) Then
                        rptXrefs.DataSource = docFetch
                        rptXrefs.DataBind()
                    End If
                End If
                litComments.Text = row.Comments
        End Select
    End Sub

    Private Sub rptXrefs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkXRef As HyperLink = CType(e.Item.FindControl("hlkXRef"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkXRef.NavigateUrl = redirectUrl
                    hlkXRef.Text = row.Title
                End If
        End Select
    End Sub
End Class