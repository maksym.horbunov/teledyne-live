﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_DSOPrintGateway" Codebehind="DSOPrintGateway.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
            <td valign="top" align="left" height="38">
            <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"> 
                <table border="0" cellpadding="0" cellspacing="0" width="500">
                <tr>
                    <td colspan="2"><b>FREE
                    DSO Network Print Gateway*</b>
                    <p>* For Ethernet
                    equipped DSOs only<br>
                    </p>
                    </td>
                </tr>
                <tr>
                    <td><b><br>
                    </b></td>
                    <td>
                    <p align="right"><b>
                    </b><a href="download.aspx?did=4636"><h2>Download</h2></a></p>
                    </td>
                </tr>
                </table>
                <p align="left"><b>Introduction</b><br>
                <br>
                The Teledyne LeCroy DSO Network Print Gateway is a software package
                that facilitates printing to networked printers from an
                Ethernet-equipped DSO.</p>
                <p align="left">The
                Network Print Gateway runs on a Windows workstation anywhere
                on a corporate network and acts as an intermediary between any
                number of Ethernet-equipped DSOs and any number of networked
                printers. Since the print drivers are provided by the printer
                manufacturer and run on the gateway workstation, the gateway
                can support the majority of existing, and future, printers
                using standard Windows printer drivers and without requiring
                drivers updates to the DSO software.<br>
                <br>
                <b>Two modes of
                operation are supported by the gateway:</b><br>
                <br>
                <b>The first mode</b> allows a DSO user to press the hardcopy
                button on the DSO's front-panel and cause a print to be
                emitted from a selected network printer.&nbsp;<br>
                <br>
                <b>The second mode</b> allows the DSO user to choose output to
                a&nbsp; standard 'bitmap file' at a predefined location on a
                network drive. This file may then be accessed by another
                workstation on the network in order to incorporate
                screen-dumps into reports for example. Files are stored in a
                directory named using the DSO's serial number in order to
                separate prints performed by multiple dsos (and to provide
                some degree of security if required).</p>
                <p align="left"><b>System
                Requirements</b></p>
                <blockquote>
                <p align="left"><b>DSO</b></p>
                </blockquote>
                <ul>
                <li>
                    <p align="left">Teledyne LeCroy
                    Waverunner or WavePro DSO equipped with an Ethernet
                    option.</p>
                </li>
                <li>
                    <p align="left">Firmware
                    revision 8.7.0 or later.</p>
                </li>
                </ul>
                <blockquote>
                <p align="left"><b>Print
                Gateway PC</b></p>
                </blockquote>
                <ul>
                <li>
                    <p align="left">A
                    Windows 98/NT/2000 PC to act as the gateway (only one
                    required per network).<br>
                    * (see FAQ for subtle differences between running the
                    gateway on Win98 and NT/2000)</p>
                </li>
                <li>
                    <p align="left">20Mb
                    of free HDD space</p>
                </li>
                <li>
                    <p align="left">32Mb
                    of RAM</p>
                </li>
                <li>
                    <p align="left">Ethernet
                    Connection</li>
                </ul>
                <p align="left"><b>FAQ&nbsp;</b></p>
                <p align="left"><b>Q.
                </b>What printers are supported by the Gateway?<br>
                <b>A.</b> Any printer that is supported by the Windows
                Operating system, a long list!.<br>
                <br>
                <b>Q.</b> Do I need to load a printer driver into my DSO?<br>
                <b>A. </b>No, everything is handled by the Gateway; the DSO
                does not need any software installed.<br>
                <br>
                <b>Q.</b> What Teledyne LeCroy DSOs support this feature?<br>
                <b>A.</b> Any WaveRunner™/WavePro™ DSO equipped with an
                Ethernet port.<br>
                <br>
                <b>Q.</b> How long does the print take?<br>
                <b>A.</b> Depends mainly upon the speed of the printer. The
                DSO is busy only for a fraction of a second before it is free
                to do other things.<br>
                <br>
                <b>Q.</b> Is the gateway available for Operating Systems other
                than Windows?<br>
                <b>A. </b>Not presently, but this is being actively
                researched, the DSO doesn't care what network it is talking
                to, as long as a TCP/IP connection is available.<br>
                <br>
                <b>Q.</b> Can I print to a printer on the Internet?<br>
                <b>A.</b> Theoretically, yes!, as long as the DSO can 'see'
                the Gateway using the TCP/IP protocol then the printer could
                be in a different country!.<br>
                <br>
                <b>Q.</b> Do all of my DSOs need to be registered on my
                network/domain?<br>
                <b>A. </b>No, this is an advantage of the Print Gateway
                solution; only the machine running the print gateway needs to
                be registered on the domain.<br>
                <br>
                <b>Q.</b> Does my DSO need a username/password to print?<br>
                <b>A. </b>No, only the Print Gateway needs a
                username/password, simplifying the life of the network
                administrator.<br>
                <br>
                <b>Q.</b> Does the machine running the Print Server Gateway
                need any special network setup?<br>
                <b>A. </b>It is important that the machine running the gateway
                be assigned a 'static IP address', this ensures that it's
                address doesn't change with time which would require all of
                the client DSOs to be reconfigured.<br>
                <br>
                <b>Q. </b>Why can't the DSO print directly to a printer on the
                network?<br>
                <b>A. </b>For several reasons. Firstly this would require that
                the DSO be equipped with a built-in driver for the target
                printer, considering the number of different models of
                printers in use it would be almost impossible to maintain.
                Secondly, to print to a printer on a Windows network would
                require that the DSO support the Microsoft Networking
                protocol, a feat that is virtually impossible for a non
                Windows-based embedded instrument.<br>
                <br>
                <b>Q.</b> What is the difference between running the gateway
                under Windows 98 and Windows NT/2000?<br>
                <b>A. </b>There are subtle differences between the print
                support offered by Windows 98 and NT/2000. When running under
                Windows NT/2000 the Gateway can function in one of two modes.
                In the first mode 'Local Printers' it can provide DSO clients
                with the ability to print to 'local' printers, which are
                physically attached to the gateway machine. In the second mode
                'Print Server' it can provide DSO clients with the ability to
                print to any printer connected to a specified print server
                without the machine running the gateway requiring an explicit
                connection to each printer.&nbsp;<br>
                <br>
                Under Windows 98 the gateway provides client DSOs with access
                to all printers that have been explicitly installed in the
                'Printers' setup menu, found under the Start-&gt;Settings
                menu. Note that these printers may either be remote, or local
                printers.
                <p align="left">Configure
                all printers as local!<br>
                <br>
                <b>Q.</b> How do I change the name of the printer as it
                appears on the DSO?<br>
                <b>A.</b> Under Windows 98 right-click on the printer in the
                'Printers' section of the Start-&gt;Settings-&gt;Printers' and
                select 'Rename'. Under Windows NT/2000 in 'Local Printers'
                mode the same can be performed, in 'Print Server' mode the
                name is defined by the print server and must be changed there.<br>
                <br>
                <b>Q. </b>Can I print directly to a file on the network using
                the Gateway?<br>
                <b>A.</b> Yes! when the print gateway is configured a path is
                specified, this may be a local path on the Gateway PC, or a
                network path.<br>
                When the &lt;BitmapFile&gt; option is selected on the DSO's
                menu and Hardcopy is pressed a bitmap file is created in the
                specified location. The name uniquely identifies the DSO
                (using it's serial number).<br>
                <br>
                <b>Q. </b>Can I run more than one Print Gateway on my network?<br>
                <b>A. </b>Yes, there is no limitation to the number of print
                gateways that can run on a network.<br>
                <br>
                <b>Q.</b> Why does the server need a static IP address?<br>
                <b>A.</b> At present the DSO does not support the network
                protocols required to lookup an IP address when given a host
                name. This means that the host (the print gateway server) must
                be identifiable using only it's IP address. In a modern
                network IP addresses are fairly dynamic, they change
                periodically as explained below. In order to ensure that the
                DSO can always 'find' the print gateway it currently needs to
                be assigned a static IP address.<br>
                <br>
                <b>Q. </b>Can I at least try the system without applying for a
                static IP address?<br>
                <b>A.</b> Yes, you can, but beware that the address assigned
                to your host system may change without notice in the future.
                Addresses are usually assigned using a protocol such as DHCP,
                which 'leases' IP addresses to hosts. This lease usually has a
                fixed time period, after which the lease expires and a new
                address is assigned. On some networks this lease expiry time
                can be as short as a day, on others it may be as long as a
                year. So, just be aware that if the system stops working at
                some point in the future it is probably caused by this.&nbsp;</td>
            <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
            <tr>
            <td valign="top" height="70"></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>