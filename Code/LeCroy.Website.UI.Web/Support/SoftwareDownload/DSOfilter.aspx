﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_DSOfilter" Codebehind="DSOfilter.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>
                    <p align="left"><b>About DSOFilter</b></p>
                    <p align="left"><a href="download.aspx?did=4635"><h2>Download</h2></a></p>
                    <p><br>In addition to the seven popular filter types provided with DFP, custom designed filters can be also created and used. The DSOFilter which is an ActiveX control was designed to help to load these filter coefficients into Teledyne LeCroy DSOs.&nbsp;<br><br>DSOFilter can be employed in two ways:</p>
                    <ol>
                        <li>
                            <p align="left">Using a filter design and math program such as MathCad or MATLAB. First, filters are designed with the math program. Then, the coefficients can be loaded directly into the oscilloscope with DSOFilter utility.&nbsp;</p>
                        </li>
                        <li>
                            <p align="left">
                                Specify the filter coefficients on an Excel spreadsheet and load them directly into oscilloscope with DSOFilter utility.&nbsp;<br><br>
                                The coefficients can be sent to the oscilloscope via a GPIB, RS232 or a 10/100 Base- T Ethernet connection, if available. (Check the Excel example for details)<br><br>
                                It is also possible to copy the coefficients to a diskette and then move the diskette to the oscilloscope.
                            </p>
                        </li>
                    </ol>
                    <p align="left">&nbsp;</p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
            <tr><td valign="top" height="70"></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>
