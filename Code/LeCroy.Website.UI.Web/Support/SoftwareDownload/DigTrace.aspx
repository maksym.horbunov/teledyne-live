﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_DigTrace" Codebehind="DigTrace.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br></p>
                    <p align="left">
                        <h2>DigTraceUtility</h2><br>
                        <strong>Version 1.2</strong>
                    </p>
                    <p align="left">
                        <a href="download.aspx?did=4918"><h2>Download</h2></a>
                    </p>
                    <p>
                        <strong>DigTraceUtility features:</strong><br>
                        a. Import digital waveforms (XMLDig files) acquired with the MS-250, MS-500 and MS-32 mixed-signal options.
                        <br>
                        b. Save the digital waveform and bus values in 3 formats: 1) all points, 2) sparsified, and 3) state changes only. Output files include time and bus value information in addition to sample values.<br>
                        c. View the digital waveforms in a graph display that includes bus values.<br>
                        d. Zoom in and out on the digital waveform.<br>
                        e. Use cursors to make measurements.<br>
                        f. Use context help by clicking control-h and hovering over GUI elements.
                    </p>
                    <p>
                        <strong>Simple installation instructions:</strong><br>
                        a. Download the zip file DigTraceUtility_Installer_v1.2.zip<br>
                        b. If using Winzip, open the zip file and click the &quot;Install&quot; toolbar button<br>
                        c. If not using Winzip, extract the zip file contents to a new folder, and then run the setup.exe file that's at the top level.
                    </p>
                    <p>
                        A shortcut for the application installs into the Start Menu, as well as a shortcut to the help file.<br>
                    </p>
                    <p>
                        <strong>System Requirements:</strong><br>
                        1. Runs on typical PCs running Windows 2000, XP or Vista.<br>
                        2. Microsoft's MSXML version 4.0 or 6.0 needs to be present on the target PC. (MSXML is a toolkit used for parsing XML files). Note that MSXML is typically included with Windows XP.<br>
                        3. LabVIEW (TM) Run-Time Engine v8.5, which is included within the DigTraceUtility installation.<br>
                        4. Screen resolution 800x600 or higher.<br>
                    </p>
                    <p>
                        <strong>Version 1.2 Release notes: </strong><br>
                        1. Use compression in digital graph to greatly reduce memory usage and better handling of large files<br>
                        2. Reformat to fit 800x600<br>
                        3. Addition of small menubar that includes Print Window, Close and Show Context Help<br>
                        4. Switch to combo box for Delimeter and Extension controls
                    </p>
                    <p><a href="images/digtrace_lg.jpg"><img src="images/digtrace_sm.jpg" border="0" width="400" height="296"></a></p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>