﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Documents" CodeBehind="Documents.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <p><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download" /></p>
        <p><h2><asp:Label ID="lb_Title" runat="server" /></h2></p>
        <p>Please click on the download button to begin your upgrade.</p>
        <p><b>Note:</b><br />You may be redirected to a short form if you are not a registered user.</p>
        <div id="divSeriesCompatibility" runat="server" visible="false">Select your series:&nbsp;<asp:DropDownList ID="ddlCompatibility" runat="server" AutoPostBack="true" /></div>
        <div id="divProductCompatibility" runat="server" visible="false">Select your model:&nbsp;<asp:DropDownList ID="ddlProductCompatibility" runat="server" AutoPostBack="true" /></div>
        <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3">
            <tr>
                <td valign="top" colspan="5"><asp:Label ID="lblArchiveLink" runat="server" Text="" /></td>
            </tr>
            <asp:Repeater ID="rptDownloads" runat="server">
                <HeaderTemplate>
                    <tr>
                        <td valign="top" bgcolor="#C0C0C0">&nbsp;</td>
                        <td valign="top" bgcolor="#C0C0C0" align="center"><b>Version</b></td>
                        <td valign="top" bgcolor="#C0C0C0" align="center"><b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b></td>
                        <td valign="top" bgcolor="#C0C0C0" align="center">&nbsp;</td>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" height="1" colspan="5"><hr noshade color="#C0C0C0" size="1" /></td>
                    </tr>
                    <tr>
                        <td valign="top"><b><asp:Literal ID="litTitle" runat="server" /></b><br /><asp:Literal ID="litDescription" runat="server" /></td>
                        <td valign="top" align="center"><asp:Literal ID="litVersion" runat="server" /></td>
                        <td valign="top" align="center"><asp:Literal ID="litDocSize" runat="server" /></td>
                        <td valign="top" align="center" rowspan="3">
                            <nobr><asp:HyperLink ID="hlkDownload" runat="server"><asp:Image ID="imgDownloadIcon" runat="server" BorderWidth="0" ImageUrl="~/images/icons/download_icon.png" />&nbsp;Download</asp:HyperLink></nobr><br /><asp:Literal ID="litPasswordProtected" runat="server" />
                        </td>
                    </tr>
                    <tr id="trXrefs" runat="server" visible="false">
                        <td colspan="3" style="padding-left: 20px;" valign="top">
                            <asp:Repeater ID="rptXrefs" runat="server">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlkXRef" runat="server" Target="_blank" /><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3"><br /><i><font size="1"><asp:Literal ID="litComments" runat="server" /></font></i><br /><br /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>