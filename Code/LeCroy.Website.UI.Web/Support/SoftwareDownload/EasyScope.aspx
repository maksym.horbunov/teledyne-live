﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_EasyScope" Codebehind="EasyScope.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left">
                        <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>
                        <p align="left">
                            <h2>EasyScope</h2><br>
                            <strong>Version 3.01.03.01.09</strong>
                        </p>
                        <p align="left">
                            <a href="download.aspx?did=7888"><h2>Download</h2></a><br />
                            <i><a href="/doc/docview.aspx?id=7889"><b>Release Notes</b></a></i>
                        </p>
                        <p>
                            The version of EasyScope available for download is compatible with the following WaveAce models and firmware versions:<br />
                            <table border="0" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td valign="top">
                                        WaveAce 101, 102, 112
                                    </td>
                                    <td valign="top">
                                        2.07.02.160
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        WaveAce 202, 212, 222, 232
                                    </td>
                                    <td valign="top">
                                        2.06.02.19
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        WaveAce 204, 214, 224, 234
                                    </td>
                                    <td valign="top">
                                        1.01.01.13*
                                    </td>
                                </tr>
                            </table>
                        </p>
                        <p>* Latest firmware version 5.05.02.14 works with <a href="/support/softwaredownload/wavestudio.aspx">WaveStudio</a></p>
                        <p>Get the latest version of the <a href="/support/softwaredownload/documents.aspx?sc=15">WaveAce firmware</a></p>
                        <p>After confirming the WaveAce has a compatible firmware version click the Download button below to start the download. Instructions for Installing and Operating the EasyScope software can be found in the <a href="/doc/docview.aspx?id=1758">EasyScope Operator's manual</a></p>
                        <p>EasyScope version 3.01.03.01.03 is compatible with 32-bit and 64-bit versions of Windows.</p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>