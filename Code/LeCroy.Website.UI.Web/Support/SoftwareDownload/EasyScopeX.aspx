﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_EasyScopeX" Codebehind="EasyScopeX.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left">
                        <p align="left"><span style="color:#0077c0; font-size:1.2rem; font-weight:bold">EasyScopeX Software Download</span><p>
                            <pl><strong>Version 1.01.02.01.20</strong>
                        </p>
                        <p align="left">
                            <a href="/doc/easy-scopex-software"><h2>Download</h2></a>
                        </p>
                        <p>
                            The version of EasyScope available for download is compatible with the following Teledyne Test Tool Oscilloscope models and firmware versions:<br />
                            <table border="0" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td valign="top">T3DSO1000/1000A</td>
                                    <td valign="top"><a href="/oscilloscope/t3dso1000-series-oscilloscopes/resources/firmware">Firmware Download</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">T3DSO2000</td>
                                    <td valign="top"><a href="/oscilloscope/t3dso2000-series-oscilloscopes/resources/firmware">Firmware Download</a></td>
                                </tr>
                            </table>
                        </p>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>