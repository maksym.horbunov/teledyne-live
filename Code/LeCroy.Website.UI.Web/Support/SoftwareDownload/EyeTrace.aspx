﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_EyeTrace" Codebehind="EyeTrace.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
		    <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br></p>
                    <p align="left">
                        <h2>EyeTrace Utility</h2><br>
                        <strong>Version 1.0</strong>
                    </p>
                    <p align="left"><a href="download.aspx?did=5015"><h2>Download</h2></a></p>
                    <p>
                        <strong>EyeTraceUtility features:</strong><br>
                        <strong>a.</strong> Import eye diagram waveforms (XMLPer files) acquired with  the SDA series oscilloscopes or with the Serial Data Mask package.<br>
                        <strong>b.</strong> Save the waveform in a CSV format to facilitate offline  analysis. <br>
                        <strong>c.</strong> Zoom in/out on the eye diagram<br>
                        <strong>d.</strong> Use cursors to make measurements.<br>
                        <strong>e.</strong> Use context help by clicking control-h and hovering over  GUI elements.</p>
                        <p><strong>Simple installation  instructions:</strong><br>
                        <strong>a.</strong> Download the zip file EyeTraceUtility_Installer_v1.0.zip<br>
                        <strong>b.</strong> If using Winzip, open the zip file and click the  &quot;Install&quot; toolbar button<br>
                        <strong>c.</strong> If not using Winzip, extract the zip file contents to a  new folder, and then run the setup.exe file that is at the archive&rsquo;s top level.</p>
                        <p>A shortcut for the application installs into the Start Menu,  as well as a shortcut to the help file.</font></p>
                        <p><strong>System Requirements:</strong><br>
                        <strong>1.</strong> Runs on typical PCs running Windows 2000, XP or Vista.<br>
                        <strong>2.</strong> Microsoft's MSXML version 4.0 or 6.0 needs to be present  on the target PC. (MSXML is a toolkit used for parsing XML files). Note that  MSXML is typically included with Windows XP.<br>
                        <strong>3.</strong> LabVIEW (TM) Run-Time Engine v8.5.1, which is included  within the EyeTraceUtility installation.<br>
                        <strong>4.</strong> Screen resolution 800x600 or higher.
                    </p>
                    <p>&nbsp;</p>
                    <p><a href="images/eyetrace_lg.jpg"><img src="images/eyetrace_sm.jpg" border="0" width="400" height="296"></a></p>
                </td>
				<td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
			</tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>