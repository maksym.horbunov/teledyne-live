﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_FirmwareUpgrade" CodeBehind="FirmwareUpgrade.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download" /></p>
        <h2>Firmware Upgrades</h2>
        <p><asp:Literal ID="lb_main" runat="server" /></p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources">
        <font color="#0076c0"><strong><asp:Literal ID="litOne" runat="server" /></strong></font><br /><br />
        <a href="../dsosecurity.aspx"><asp:Literal ID="litTwo" runat="server" /></a>
    </div>
</asp:Content>