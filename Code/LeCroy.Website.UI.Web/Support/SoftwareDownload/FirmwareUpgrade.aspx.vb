﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_SoftwareDownload_FirmwareUpgrade
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Core Software Download"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0690", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0691", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim subMenuId As Int32 = ParseSubMenuIdQueryString()
        Dim captionId As Int32 = ParseCaptionIdQueryString()
        Dim menuId As Int32 = ParseMenuIdQueryString()

        Session("menuSelected") = captionId
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionId.ToString(), rootDir, pn_leftsubmenu, menuId.ToString(), IIf(subMenuId <= 0, String.Empty, subMenuId.ToString()))
            lb_leftmenu.Text = Functions.LeftMenu(captionId.ToString(), rootDir, pn_leftmenu, menuId.ToString())
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
        Initial()
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Function ParseSubMenuIdQueryString() As Int32
        Dim retVal As Int32 = -1
        If Not (Request("smid") Is Nothing) Then
            Int32.TryParse(Request("smid").ToString(), retVal)
        End If
        Return retVal
    End Function

    Private Function ParseCaptionIdQueryString() As Int32
        Dim retVal As Int32 = Int32.Parse(AppConstants.SUPPORT_CAPTION_ID)
        If Not (Request("capid") Is Nothing) Then
            Int32.TryParse(Request("capid").ToString(), retVal)
        End If
        Return retVal
    End Function

    Private Function ParseMenuIdQueryString() As Int32
        Dim retVal As Int32 = Int32.Parse(AppConstants.SOFTWARE_DOWNLOAD_MENU)
        If Not (Request("mid") Is Nothing) Then
            Int32.TryParse(Request("mid").ToString(), retVal)
        End If
        Return retVal
    End Function

    Private Function ParseCategoryIdQueryString() As Nullable(Of Int32)
        Dim retVal As Int32 = 0
        If Not (Request("categoryid") Is Nothing) Then
            Int32.TryParse(Request("categoryid").ToString(), retVal)
        End If
        If (retVal = 0) Then Return Nothing
        Return retVal
    End Function

    Private Sub Initial()
        Dim categoryId As Nullable(Of Int32) = ParseCategoryIdQueryString()
        Dim sqlstr As String = ""
        If (categoryId.HasValue) Then
            If (categoryId.Value = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
                sqlstr = " SELECT DISTINCT SUBCATEGORY.SUBCAT_ID , SUBCATEGORY.SUBCAT_NAME, SUBCATEGORY.SORT_ID " +
                         " FROM PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT_SERIES " +
                         " ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN " +
                         " DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID INNER JOIN " +
                         " PRODUCT ON DOCUMENT_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
                         " INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID" +
                         " WHERE (DOCUMENT.POST IN ('y')) AND (DOCUMENT.DOC_TYPE_ID IN (14,25)) AND (PRODUCT_SERIES.DISABLED = 'n') " +
                         " AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 2) and SUBCATEGORY.CATEGORY_ID=2 ORDER BY SUBCATEGORY.SORT_ID"
            Else
                sqlstr = " SELECT DISTINCT SUBCATEGORY.SUBCAT_ID , SUBCATEGORY.SUBCAT_NAME, SUBCATEGORY.SORT_ID " +
                         " FROM PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT_SERIES " +
                         " ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN " +
                         " DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID INNER JOIN " +
                         " PRODUCT ON DOCUMENT_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
                         " INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID" +
                         " WHERE (DOCUMENT.POST IN ('y')) AND (DOCUMENT.DOC_TYPE_ID = 11) AND (PRODUCT_SERIES.DISABLED = 'n') " +
                         " AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 1) and SUBCATEGORY.CATEGORY_ID=1 ORDER BY SUBCATEGORY.SORT_ID"
            End If
        Else
            sqlstr = " SELECT DISTINCT SUBCATEGORY.SUBCAT_ID , SUBCATEGORY.SUBCAT_NAME, SUBCATEGORY.SORT_ID " +
         " FROM PRODUCT_SERIES_CATEGORY INNER JOIN PRODUCT_SERIES " +
         " ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID INNER JOIN " +
         " DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID INNER JOIN " +
         " PRODUCT ON DOCUMENT_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
         " INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID" +
         " WHERE (DOCUMENT.POST IN ('y')) AND (DOCUMENT.DOC_TYPE_ID = 11) AND (PRODUCT_SERIES.DISABLED = 'n') " +
         " AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 1) and SUBCATEGORY.CATEGORY_ID=1 ORDER BY SUBCATEGORY.SORT_ID"
        End If

        'sqlstr = " SELECT DISTINCT PRODUCT_SERIES.product_series_id, PRODUCT_SERIES.FW_DISPLAY as NAME, PRODUCT_SERIES.FW_SORT_ID " + _
        '" FROM PRODUCT_SERIES_CATEGORY INNER JOIN  PRODUCT_SERIES ON PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID " + _
        '" INNER JOIN DOWNLOAD INNER JOIN  DOWNLOAD_XREF ON DOWNLOAD.DOWNLOAD_ID = DOWNLOAD_XREF.DOWNLOAD_ID INNER JOIN   PRODUCT " + _
        '" ON DOWNLOAD_XREF.PRODUCT_ID = PRODUCT.PRODUCTID ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID" + _
        '" WHERE  (DOWNLOAD.POST in ('y')) AND (DOWNLOAD.DOWN_TOPIC_ID = 4) AND (PRODUCT_SERIES.DISABLED = 'n') AND (PRODUCT_SERIES_CATEGORY.CATEGORY_ID = 1)" + _
        '" ORDER BY PRODUCT_SERIES.FW_SORT_ID"

        ' Response.Write(sqlstr)
        ' Response.End()
        Dim subcategories As List(Of Subcategory) = SubcategoryRepository.FetchSubcategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        Dim s As String = String.Empty
        For Each sc In subcategories
            s += String.Format("<li><b><a href='documents.aspx?sc={0}'>{1}</a></b></li>", sc.SubCatId.ToString(), sc.SubCatName)
        Next
        If (categoryId.HasValue) Then
            If (categoryId.Value = CategoryIds.OSCILLOSCOPES) Then
                s += "<br /><li><a href='other_scopes.aspx'>Other oscilloscopes, including 93xx, LCxxx, LTxxx, and WP9xx series scopes</a><br/><br/></li>"
            End If
        End If
        If (subcategories.Count > 0) Then lb_main.Text = String.Format("<ul>{0}</ul><br /><br />", s)
    End Sub
#End Region
End Class