﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_FlexNet" Codebehind="FlexNet.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br></p>
                    <p align="left"><h2>FlexNet Software Downloads</h2><br></p>
                    <p align="left">
                        The FlexNet software and documentation for Teledyne LeCroy's product licensing is available for download.<br />
                        You should always use the latest version of the license manager daemon (lmgrd) and vendor daemon with your Teledyne LeCroy IOTA Software Suite.<br /><br />
                        These downloads contain the necessary software to setup a license server for Teledyne LeCroy’s IOTA Software Suite.
                    </p>
                    <p align="left">
                        <h2>Documentation:</h2>
                        <ul><li><a href="/doc/docview.aspx?id=6045">FlexNet License Administration Guide</a></li><li><a href="/doc/docview.aspx?id=6046">Teledyne LeCroy License Server Custom Port Configuration</a></li><li><a href="/doc/docview.aspx?id=6047">Teledyne LeCroy Software Licensing Guide</a></li></ul>
                    </p>
                    <p align="left">
                        <h2>Windows:</h2>
                        <ul><li><a href="download.aspx?did=6048">License Server - Windows</a></li><li><a href="download.aspx?did=6052">lmutil</a></li></ul>
                    </p>
                    <p align="left">
                        <h2>Linux:</h2>
                        <ul><li><a href="download.aspx?did=6050">License Server - Linux</a></li><li><a href="download.aspx?did=6053">lmutil</a></li></ul>
                    </p>
                    <p align="left">For additional FlexNet utilities that are not included in the above links, go to Flexera Software website <a href="http://www.globes.com/support/fnp_utilities_download.htm" onclick="GaEventPush('OutboundLink', 'www.globes.com/support/fnp_utilities_download.htm');">http://www.globes.com/support/fnp_utilities_download.htm</a><br /><br /></p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>