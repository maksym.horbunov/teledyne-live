﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_LSIB" Codebehind="LSIB.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left">
                        <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>
                        <h2>LSIB<br>Serial Interface Bus</h2><br>
                        <p align="left">
                            <a href="download.aspx?did=9162"><h2>Download LSIB Installer for Linux</h2></a><br />
                            <a href="download.aspx?did=9163"><h2>Download LSIB Installer for Windows</h2></a>
                        </p><br />
                        <p>
                            <img src="/images/icons/icons_pdf.gif" width="24" height="22"><a href="/doc/docview.aspx?id=8445">LSIB Datasheet</a><br />
                            <img src="/images/icons/icons_pdf.gif" width="24" height="22"><a href="/doc/docview.aspx?id=9044">LSIB Manual</a>
                        </p>
                        <p>LSIB (LeCroy Serial Interface Bus) is a new standard for high-speed data transfer from the oscilloscope with speeds up to 325 MB/s. Teledyne LeCroy's exclusive LSIB solution is based on the wired PCI Express standard that uses a x4 (4 lanes) bus for remote data transfer.</p>
                        <p>LSIB is geared towards users who need high-performance data transfer for use in offline data processing or storage. LSIB offers significant improvements in data transfer rate over GbE, USB, GPIB, and 100Base-T methods.</p>
                    </p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>