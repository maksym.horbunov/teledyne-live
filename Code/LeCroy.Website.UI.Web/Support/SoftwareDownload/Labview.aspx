﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_Labview" Codebehind="Labview.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server"></asp:Literal>
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>Instrument Drivers
                    <p align="left">The following drivers are available for download for Teledyne LeCroy Oscilloscopes:</p>
                    <table border="0" cellpadding="2" width="500" cellspacing="1">
                        <tr><td height="19" bgcolor="#E7EFFF"><b>Downloads</b></td></tr>
                        <tr><td height="1"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10"><b><font size="1">&nbsp; </font><font size="2">X-Stream DSOs</font></b></td></tr>
                        <tr>
                            <td>
                                <p><font size="2"><b>Type:</b> <font color="#FF0000">IVI-C and IVI-COM Driver</font><br />Created by:<br /><a href="http://www.pacificmindworks.com/" onclick="GaEventPush('OutboundLink', 'www.pacificmindworks.com/');"><img src="images/pacific_mindworks_logo.gif" width="288" height="37" border="0" /></a></font></p>
                                <table border="0" width="100%">
                                    <tr>
                                        <td width="100" valign="top"><font size="2"><b>Designed for:</b></font></td>
                                        <td>
                                            <font size="2">WaveMaster 8 Zi (-A) Oscilloscopes<br />
                                                SDA 8 Zi (-A) Oscilloscopes<br />
                                                DDA 8 Zi (-A) Oscilloscopes<br />
                                                SDA Oscilloscopes<br />
                                                SDA 7 Zi (-A) Oscilloscopes<br />
                                                DDA Oscilloscopes<br />
                                                DDA 7 Zi (-A) Oscilloscopes<br />
                                                WaveMaster 8000(a) Oscilloscopes<br />
                                                WavePro 7 Zi Oscilloscopes<br />
                                                WavePro 7000(a) Oscilloscopes<br />
                                                WaveRunner 6 Zi Oscilloscopes<br />
                                                HRO 12-bit High Resolution Oscilloscopes<br />
                                                WaveRunner 6000(a) Oscilloscopes<br />
                                                WaveRunner Xi/MXi(-A) Oscilloscopes<br />
                                                HDO6000 Oscilloscopes<br />
                                                HDO4000 Oscilloscopes<br />
                                                WaveSurfer 3000<br />
                                                WaveSurfer Xs/MXs(-A) Oscilloscopes<br />
                                                WaveSurfer Xs/MXs(-B) Oscilloscopes<br />
                                                WaveSurfer 400 Oscilloscopes
                                            </font>
                                        </td>
                                    </tr>
                                </table>
                                <p><font size="2"><br /><a href="http://www.pacificmindworks.com/" onclick="GaEventPush('OutboundLink', 'www.pacificmindworks.com/');">http://www.pacificmindworks.com/</a></font></p>
                                <p>
                                    <font size="2"><a href="download.aspx?did=7514">IVI Driver 3.2.9.0 x86</a></font><br />
                                    <font size="2"><a href="download.aspx?did=7515">IVI Driver 3.2.9.0 x64</a></font><br />
                                </p>
                                <p>
                                    <font size="2">Required Components: </font><font size="2">&nbsp;</font>
                                    <ul>
                                        <li><font size="2"><a href="vicppassport.aspx">Latest Teledyne LeCroy VICP Passport</a> (for VICP connections only; not required for GPIB or LXI)</font></li>
                                        <li><font size="2"><a href="http://www.ivifoundation.org/shared_components/Default.aspx" onclick="GaEventPush('OutboundLink', 'www.ivifoundation.org/shared_components/Default.aspx');">IVI Shared Components</a></font></li>
                                        <li><font size="2">NI-VISA 3.0 or higher</font></li>
                                    </ul>
                                </p>
                            </td>
                        </tr>
                        <tr><td><hr /></td></tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10"><b><font size="1">&nbsp; </font><font size="2">X-Stream DSOs</font></b></td></tr>
                        <tr>
                            <td height="19">
                                <font size="2">
                                    <p><b>Type:</b> LabVIEW&trade; driver</p>
                                    <p>Follow this link to National Instrument's page for LabVIEW Plug and Play drivers: <a href="http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=07C0AC709BD14C10E0440003BA7CCD71" onclick="GaEventPush('OutboundLink', 'http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=07C0AC709BD14C10E0440003BA7CCD71');">NI X-Stream LabVIEW Drivers</a>. NI has developed a "traditional" driver as well as a "project-style" driver for use in LabVIEW 8.0 and above.</p>
                                </font>
                                <font size="2"><p>Installation instructions can be found in an HTML readme file that is within the ZIP file containing the driver download. Read this first to avoid installation issues. Also contained in the readme file are instructions for finding Example programs.</p></font>
                                <table border="0" width="100%">
                                    <tr>
                                        <td width="100" valign="top"><font size="2"><b>Designed for:</b> </font></td>
                                        <td>
                                            <font size="2">WaveMaster 8 Zi Oscilloscopes<br />
                                                SDA 8 Zi Oscilloscopes<br />
                                                DDA 8 Zi Oscilloscopes<br />
                                                SDA Oscilloscopes<br />
                                                SDA 7 Zi Oscilloscopes<br />
                                                DDA Oscilloscopes<br />
                                                DDA 7 Zi Oscilloscopes<br />
                                                WaveMaster 8000(a) Oscilloscopes<br />
                                                WavePro 7 Zi Oscilloscopes<br />
                                                WavePro 7000(a) Oscilloscopes<br />
                                                WaveRunner 6000(a) Oscilloscopes<br />
                                                WaveRunner Xi/MXi(-A) Oscilloscopes<br />
                                                HDO6000 Oscilloscopes<br />
                                                HDO4000 Oscilloscopes<br />
                                                WaveSurfer 3000<br />
                                                WaveSurfer Xs/MXs(-A) Oscilloscopes<br />
                                                WaveSurfer 400 Oscilloscopes
                                            </font>
                                        </td>
                                    </tr>
                                </table>
                                <font size="2">
                                    <p>
                                        <strong>Software requirements</strong>
                                        <ul><li>LabVIEW 7.0 or higher </li><li>NI-VISA 3.0 or higher</li><li><a href="vicppassport.aspx">Latest Teledyne LeCroy VICP Passport</a> (for VICP connections only; not required for GPIB or LXI)</li></ul>
                                    </p>
                                </font>
                                <hr size="1px" />
                            </td>
                        </tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10"><b><font size="1">&nbsp; </font><font size="2">WaveJet DSOs</font></b></td></tr>
                        <tr>
                            <td height="19">
                                <font size="2">
                                    <p><b>Type:</b>LabVIEW&trade; driver</p>
                                    <p>Follow this link to National Instrument's page for LabVIEW Plug and Play drivers: <a href="http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=81A06850A47F198BE04400144FB7D21D " onclick="GaEventPush('OutboundLink', 'http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=81A06850A47F198BE04400144FB7D21D 
');">NI WaveJet LabVIEW driver</a></p>
                                </font>
                                <font size="2"><p>Installation instructions can be found in an HTML readme file that is within the ZIP file containing the driver download. Read this first to avoid installation issues. Also contained in the readme file are instructions for finding Example programs.</p></font>
                                <table border="0" width="100%">
                                    <tr>
                                        <td width="100" valign="top"><font size="2"><b>Designed for:</b> </font></td>
                                        <td><font size="2">WaveJet 300(A) Oscilloscopes, WaveJet Touch Oscilloscopes </font></td>
                                    </tr>
                                </table>
                                <font size="2">
                                    <p>
                                        <strong>Software requirements</strong>
                                        <ul><li>LabVIEW 7.1 or higher </li><li>NI-VISA 3.0 or higher</li><li><a href="vicppassport.aspx">Latest Teledyne LeCroy VICP Passport</a> (for connecting via the LAN interface)</li></ul>
                                    </p>
                                </font>
                                <hr size="1px" />
                            </td>
                        </tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10"><b><font size="1">&nbsp; </font><font size="2">WaveAce DSOs</font></b></td></tr>
                        <tr>
                            <td height="19">
                                <font size="2">
                                    <p><b>Type:</b>LabVIEW&trade; driver</p>
                                    <p>Please click here to download <a href="download.aspx?did=7628">WaveAce 1000 and 2000 Series LabView Drivers</a></p>
                                </font>
                                <font size="2">
                                    <p>&nbsp;</p>
                                </font>
                                <table border="0" width="100%">
                                    <tr>
                                        <td width="100" valign="top"><font size="2"><b>Designed for:</b></font></td>
                                        <td><font size="2">WaveAce 1000 and 2000 Series Oscilloscopes</font></td>
                                    </tr>
                                </table>
                                <font size="2">
                                    <p><strong>Software requirements</strong><ul><li>LabVIEW 2012 or higher</li><li>NI-VISA 3.0 or higher</li><li><a href="vicppassport.aspx">Latest Teledyne LeCroy VICP Passport</a> (for connecting via the LAN interface)</li></ul></p>
                                </font>
                                <hr size="1px" />
                            </td>
                        </tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10">&nbsp;<b><font size="1"> </font><font size="2">WavePro 9XX Series</font></b></td></tr>
                        <tr>
                            <td height="19">
                                <blockquote>
                                    <p><b><font size="2"><a href="/files/misc/lcwpxxx.exe"><br>Download installation file</a></font></b></p>
                                </blockquote>
                                <p align="left"><font size="2"><b>Type:</b> IVI driver (IVI drivers are compatible for use with LabWindows/CVI, LabVIEW, Visual Basic and Visual C++)<br>&nbsp;<br><b>Designed for:</b> <a href="#one">940/M/L/VL, 950/M/L/VL, 960/M/L/VL/XL<br></a><b>Also works with:</b> 954M/L/VL&nbsp;</font></p>
                                <p align="left"><font size="1"><b><font color="#FF0000">Note: </font></b><font color="#FF0000"></font><font color="#000000">When initializing these models with the IVI driver, disable model number checking.</font></font></p>
                            </td>
                        </tr>
                        <tr><td height="1"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr><td height="19"><img border="0" src="images/yellowsquare.gif" width="10" height="10">&nbsp;<b><font size="1"> </font><font size="2">Waverunner / Waverunner-2 Series</font></b></td></tr>
                        <tr>
                            <td height="19">
                                <blockquote>
                                    <p><b><font size="2"><a href="ftp://ftp.natinst.com/support/instr_drivers/labview/windows/current/library/gpib/lc3345x4.zip"><br></a><a href="/files/misc/lcltxxxx.exe">Download installation file</a></font></b></p>
                                </blockquote>
                                <p><font size="2"><b>Type:</b> IVI driver (IVI drivers are compatible for use with LabWindows/CVI, LabVIEW, Visual Basic and Visual C++)<br>&nbsp;<br><b>Designed for:</b> LT224, LT262, LT264, LT264M, LT322, LT342, LT342L, LT344, LT344L, LT364, LT364L, LT372, LT374, LT374M, LT374L&nbsp;<br><b>Also works with:</b> <a href="#one">LT354, LT354M, LT354ML, LT584, LT584M, LT584L</a></font></p>
                                <p><font size="1"><b><font color="#FF0000">Note: </font></b><font color="#FF0000"></font><font color="#000000">When initializing these models with the IVI driver, disable model number checking.</font></font></p>
                            </td>
                        </tr>
                        <tr><td height="1"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19">
                                <img border="0" src="images/yellowsquare.gif" width="10" height="10">&nbsp; <font size="2"><b>LSA1000 High Speed Signal Digitizer/Analyzer</b> </font>
                                <blockquote>
                                    <p><b><font size="2">Click <a href="http://sine.ni.com/apps/utf8/niid_web_display.model_page?p_model_id=1382" onclick="GaEventPush('OutboundLink', 'http://sine.ni.com/apps/utf8/niid_web_display.model_page?p_model_id=1382');">Link to NI Driver Downloads</a></font></b></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19">
                                <img border="0" src="images/yellowsquare.gif" width="10" height="10"><b><font size="1">&nbsp; </font><font size="2">Digital Oscilloscopes</font></b><b><font size="2"> Including:</font></b>
                                <p><font size="2">7200, 9304, 9310, 9314, 9350, 9354, 9370, 9374, 9384, 9361, 9362, 9384L, 940/M/L/VL, 950/M/L/VL, 960/M/L/VL/XL, 9400, 9400A, 9410, 9414, 9420, 9424, 9430, 9450, LC564A, LC584A/AM/AL, LC584AXL, LS140, LT224, LT262, LT264/M, LT322, LT342/L, LT344/L, LT364/L, LT372, ...</font></p>
                                <p><b><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Click <a href="http://www.ni.com/downloads/instrument-drivers/" onclick="GaEventPush('OutboundLink', 'http://www.ni.com/downloads/instrument-drivers/');">Link to NI Driver Downloads</a></font></b>
                            </td>
                        </tr>
                        <tr><td height="1"><hr noshade color="#C0C0C0" size="1"></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="70">
                    <p><font color="#FF0000" size="2"><b><a name="one"></a></b></font></p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>