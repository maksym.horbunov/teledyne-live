﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_MaskMaker" Codebehind="MaskMaker.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download">
                    <p align="left"><img border="0" src="images/maskmakerlogo.gif" width="200" height="55"></p>
                    <p align="left"><b><h2>MaskMaker, Version 1.3</h2></b></p>
                    <p align="left"><a href="download.aspx?did=4634"><h2>Download</h2></a></p>
                    <p align="left"><b>New with version 1.3</b><br><b>Installer works with Windows 2000 through Windows 10</b></p>
                    <p align="left"><b>New with version 1.2</b><br><b>Colored Circles</b><br>Colored Circles will highlight points of interest such as failure points. Now you can select the circles color, maximum number and if they will be displayed in or out of the mask.</p>
                    <p align="left"><b>XY Masks</b><br>Now you can create XY masks for applications such as power measurements.<br><br>To take advantage of these new features, you must <a href="/support/softwaredownload/firmwareupgrade.aspx<%= menuURL2 %>">upgrade your DSO firmware</a> to version 8.5.0 or higher.</p>
                    <p align="left">MaskMaker is a FREE PC based graphic utility for creating and editing Masks. These Masks are to be later used in Teledyne LeCroy oscilloscopes.&nbsp;<br><br><b><font color="#0000ff">MaskMaker (and PolyMask) System Requirements:</b><br><br><b>MaskMaker:&nbsp;</b></p>
                    <ul><li><p align="left">PC running Windows 2000/XP/Vista </li><li><p align="left">8MB RAM, 10MB HDD free space </li><li><p align="left">Mouse</li><li><p align="left">1.44 MB Floppy Disk, TCPIP, GPIB or RS232 connections </li></ul>
                    <p align="left"><b>PolyMask:&nbsp;</b></p>
                    <ul><li><p align="left">Any Waverunner or LC series Teledyne LeCroy Digital Oscilloscopes.</li><li><p align="left">Software version 8.4.3 or later is required to run PolyMask. To upgrade your scope's firmware, upgrade your version of <a href="scopeexplorer.aspx<%= menuURL2 %>">ScopeExplorer</a>which now features Firmware Upgrades via the Internet.</li><li><p align="left">PolyMask option&#146;s key or any of Mask Testing (MT01, MT02, or MT03) packages.</li></ul>
                    <p align="left"><b>Notes:&nbsp;</b><br>1) a firmware update to 8.4.3 IS NECESSARY in order to activate PolyMask. See <a href="scopeexplorer.aspx<%= menuURL2 %>">ScopeExplorer</a> for more information on upgrading your scope's firmware.</p>
                    <p align="left">&nbsp;</p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
            <tr><td valign="top" height="70"></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>