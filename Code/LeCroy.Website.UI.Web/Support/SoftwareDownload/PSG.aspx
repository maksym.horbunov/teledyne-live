﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_PSG" Codebehind="PSG.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download" /></p>
        <h2>Analysis Software</h2>
        <p>The Teledyne LeCroy Analysis Software (CATC Trace viewer) may be downloaded and installed free of charge. The analysis software allows users to open previously saved trace files. The same software can also be used to update the Teledyne LeCroy protocol analyzer hardware.</p>
        <p><b><a href="//fte.com/products/default.aspx">Frontline Test Equipment - Find downloads here</a></b></p>
        <p><b><a href="//www.quantumdata.com/downloads.html">Quantum Data - Find Downloads here</a></b></p>
        <p><b><font color="#1671CC" face="Verdana,Arial,Helvetica" size="2">Note:</font></b><br>* You may be redirected to a short form if you are not a registered Teledyne LeCroy user.</p>
        <asp:Repeater ID="rptStandards" runat="server">
            <HeaderTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top">
                            <ul>
            </HeaderTemplate>
            <ItemTemplate>
                                <li class="links"><asp:HyperLink ID="hlkStandard" runat="server" Font-Bold="true" /></li>
            </ItemTemplate>
            <FooterTemplate>
                                <li class="links"><a href="psg_swarchive.aspx"><strong>Click here to go to archive</strong></a></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater><br />
       Teledyne LeCroy protocol analysis tools require the installation and operation of software on a computer running a compatible version of the Microsoft Windows&trade; operating system. Please view the Software Release Notes for the latest operating system support.<br /><br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>