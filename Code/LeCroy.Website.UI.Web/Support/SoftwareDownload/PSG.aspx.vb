﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.DAL.Common.BaseClasses
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_SoftwareDownload_PSG
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Fibre Channel, SATA, SAS analyzers, PCIe, USB analysers"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.FAQ_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.FAQ_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim sqlString As String = "SELECT DISTINCT SUBCATEGORY.SUBCAT_ID, SUBCATEGORY.SUBCAT_NAME, SUBCATEGORY.SORT_ID FROM PROTOCOL_STANDARD INNER JOIN  DOCUMENT INNER JOIN DOCUMENT_CATEGORY " +
                " ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID ON PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID " +
                " INNER JOIN PROTOCOL_STANDARD_SERIES INNER JOIN PRODUCT_SERIES ON PROTOCOL_STANDARD_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID ON " +
                " PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = PROTOCOL_STANDARD_SERIES.PROTOCOL_STANDARD_ID INNER JOIN SUBCATEGORY ON PRODUCT_SERIES.SUBCAT_ID = SUBCATEGORY.SUBCAT_ID " +
                " WHERE     (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'y') AND (PROTOCOL_STANDARD.DISABLED = 'n') AND (PRODUCT_SERIES.DISABLED = 'n') ORDER BY SUBCATEGORY.SORT_ID"
        Dim subcategoryIds As List(Of Int32) = SubcategoryRepository.FetchSubcategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray()).Select(Function(x) x.SubCatId).ToList()
        If (subcategoryIds.Count > 0) Then
            Dim dal As DALHelperUtils = New DALHelperUtils()
            Dim sqlKeys As String = String.Empty
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            dal.CreateSqlInStatement(Of Int32)(subcategoryIds, "SUBCATID", sqlKeys, sqlParameters)
            Dim protocolSqlString As String = String.Format("SELECT DISTINCT PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID, PROTOCOL_STANDARD.NAME, PROTOCOL_STANDARD.SoftwareSortId " +
                         " FROM PROTOCOL_STANDARD INNER JOIN DOCUMENT INNER JOIN  DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID ON " +
                         " PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID INNER JOIN  PROTOCOL_STANDARD_SERIES INNER JOIN  PRODUCT_SERIES ON " +
                         " PROTOCOL_STANDARD_SERIES.PRODUCT_SERIES_ID = PRODUCT_SERIES.PRODUCT_SERIES_ID ON PROTOCOL_STANDARD.PROTOCOL_STANDARD_ID = PROTOCOL_STANDARD_SERIES.PROTOCOL_STANDARD_ID " +
                         " WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'y') AND (PROTOCOL_STANDARD.DISABLED = 'n') AND (PRODUCT_SERIES.DISABLED = 'n') AND (PRODUCT_SERIES.SUBCAT_ID IN ({0})) ORDER BY PROTOCOL_STANDARD.SoftwareSortId", sqlKeys)
            rptStandards.DataSource = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString").ToString(), protocolSqlString, sqlParameters.ToArray())
            rptStandards.DataBind()
        End If
    End Sub

    Private Sub rptStandards_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptStandards.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim ps As ProtocolStandard = CType(e.Item.DataItem, ProtocolStandard)
                Dim hlkStandard As HyperLink = CType(e.Item.FindControl("hlkStandard"), HyperLink)

                hlkStandard.NavigateUrl = String.Format("documents.aspx?standardid={0}", ps.ProtocolStandardId.ToString())
                hlkStandard.Text = ps.Name
        End Select
    End Sub
End Class