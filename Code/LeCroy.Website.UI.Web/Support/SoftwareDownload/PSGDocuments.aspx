﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" CodeBehind="PSGDocuments.aspx.vb" Inherits="LeCroy.Website.Support_SoftwareDownload_PSGDocuments" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <link href="../../css/psg-documents.css" rel="stylesheet" />
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="javascript" type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function ($) {

            $.ajax({
                type: "POST",
                url: "psgdocuments.aspx/getprotocoldata",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    addProtocolItems(msg);
                }
            });

            function addProtocolItems(dataStr) {
                var data = JSON.parse(dataStr.d);
                console.log(data);
                var items = '<ul class="menu">';
                for (var i = 0; i < data.length; i++) {
                    items += '<li class="menu-item"><a class="drop-down-item" href="?standardid=' + data[i].StandardId + '" >' + data[i].StandardName + '</a > <ul class="sub-menu">';
                    for (var j = 0; j < data[i].Series.length; j++) {
                        items += '<li class="sub-item"><a href="?standardid=' + data[i].StandardId + "&mseries=" + data[i].Series[j].SeriesId + '">' + data[i].Series[j].SeriesName + '</a></li>'
                    }
                    items += '</ul></li>';
                }
                items += '<li class="menu-item"><a href="/Support/SoftwareDownload/PSGDocuments.aspx?standardid=4">Thunderbolt</a></li><li class="menu-item"><a href="https://fte.com/products/default.aspx">Bluetooth</a></li><li class="menu-item"><a href="https://www.quantumdata.com/m42d.html">DisplayPort</a></li><li class="menu-item"><a href="https://www.quantumdata.com/downloads.html">HDBaseT</a></li><li class="menu-item"><a href="https://www.quantumdata.com/downloads.html">HDCP</a></li><li class="menu-item"><a href="https://www.quantumdata.com/downloads.html">HDMI</a></li><li class="menu-item"><a href="https://fte.com/support/WPS-download.aspx?demo=X240&iid=X240">WiFi (802.11)</a></li><li class="menu-item"><a href="https://fte.com/support/WPS-download.aspx?demo=X240&iid=X240">Thread (802.15.4)</a></li><li class="menu-item"><a href="https://fte.com/support/WPS-download.aspx?demo=X240&iid=X240">Zigbee (802.15.4)</a></li></ul ></li >';

                $("#ProtocolMenu").html(items);

                $(".sub-menu").hover(function () {
                    $(this).parent().find('a.drop-down-item').addClass("dropdown-sub-item-hover");
                }, function () {
                    $(this).parent().find('a.drop-down-item').removeClass("dropdown-sub-item-hover");
                }); 


            }



        });

        document.addEventListener("click", function(event) { 
            // If the user does not click 'Select Protocol' button or the arrow in the button toggle the drop-down
            if(!event.target.classList.contains('dropbtn') && !event.target.classList.contains('arrow')) { 
                // Get the list of classes on the element
                var list = document.getElementById("ProtocolMenu").classList;
                // If 'show' is in the classlist then remove it                
                if(list.contains('show')) {
                   document.getElementById("ProtocolMenu").classList.remove('show');                   
                }
            };
           
        });


        /* When the user clicks on the button, 
  toggle between hiding and showing the dropdown content */
        function selectProtocol() {
            document.getElementById("ProtocolMenu").classList.toggle("show");
            event.preventDefault();
        }

        function checkSubmit(e) {
            if (e && e.keyCode === 13) {
                e.preventDefault();
                document.getElementById('SiteContent_CenterColumn_btnDownloadSearch').click();
            }
        }

       

    </script>
    <style>
        hr { margin: 0; }
        table { margin-top: 24px; }
        tr { height: 4px; }
        td { padding-top: 0; padding-bottom: 4px;}
        input[type=text] { margin-left: 8px; padding: 8px 16px;}
        input[type=submit] { margin-left: 8px; padding: 8px 16px; }
        .column-center { overflow: visible !important;}
        .column-center .intro3 { padding: 40px 20px; min-height: 750px;}
    </style>
    <div class="intro3">
        <div class="container"><h1 style="font-size: 25px;">Protocol Test Solutions Software</h1>
        <img src="https://assets.lcry.net/images/psg_software_downloads.png" style="float: right; padding: 25px 15px; width: 58%;">
        <h2><asp:Literal ID="litContentTitle" runat="server">Analysis Software</asp:Literal></h2>
        <p>Download and install the Teledyne LeCroy Protocol Analysis Software free of charge. The software allows you to operate our Protocol test systems, update them to the latest functionality available, and collaborate with your peers by sharing trace files that can be viewed and analyzed on your computer.</p>
        <p>To get the right software for your needs, please select one of the Protocol Technologies from the list below. Then continue to select the product you use from the menu. Once the product is selected, you will see the list of available software starting with the latest available. You can also search for the software by using the right text box to enter search keyword.<br />Click <a href="#"><img src="../../images/icons/download_icon.png" style="border-width:0px;">&nbsp;Download</a> to begin the installation process. 
</p>
        <p><b><font color="#1671CC" face="Verdana,Arial,Helvetica" size="2">Note:</font></b><br>* You may be redirected to a short form if you are not a registered Teledyne LeCroy user.</p></div>
            <div class="holder-form-items" style="margin: 40px 20px;">
            <div class="dropdown">
                <button onclick="selectProtocol()" class="dropbtn dropdown-toggle" >Select Protocol Technology<span class="arrow">&#x25BC;</span></button>
                <div id="ProtocolMenu" class="dropdown-content" style="position: relative;"></div>
            </div>
        <p >or <asp:TextBox ID="tbDownloadSearchText" onfocus="this.value=''" text="search for software" runat="server" Width="200px" onkeyup="SetDownloadSearchButtonStatus(this, 'SiteContent_CenterColumn_btnDownloadSearch')" onkeydown="return checkSubmit(event);" /><asp:Button ID="btnDownloadSearch" runat="server" Enabled="false" Text="Search" /><br /><br />
        </p></div>
        <asp:literal ID="litNoDownloads" runat="server" Visible="false">There are no downloads currently available.</asp:literal>
        <div style="margin: 40px 20px;">
<%--            <asp:placeholder ID="phActiveVersion" runat="server" Visible="false"><h2>Current release version</h2></asp:placeholder>--%>
            <asp:Panel ID="pnlCurrentRelease" runat="server">
                <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3">
                    <asp:Repeater ID="rptDownloads" runat="server">
                        <HeaderTemplate>
                            <tr>
                                <td valign="top" bgcolor="#C0C0C0"><b>&nbsp;Current Release Version</b></td>
                                <td valign="top" bgcolor="#C0C0C0" align="center"><b>Version</b></td>
                                <td valign="top" bgcolor="#C0C0C0" align="center"><b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b></td>
                                <td valign="top" bgcolor="#C0C0C0" align="center">&nbsp;</td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td valign="top" colspan="4"><hr noshade color="#C0C0C0" size="1" /></td>
                            </tr>
                            <tr>
                                <td valign="top"><b><asp:Literal ID="litTitle" runat="server" /></b><br /><asp:Literal ID="litDescription" runat="server" /></td>
                                <td valign="top" align="center"><asp:Literal ID="litVersion" runat="server" /></td>
                                <td valign="top" align="center"><asp:Literal ID="litDocSize" runat="server" /></td>
                                <td valign="top" align="center" rowspan="3">
                                    <nobr><asp:HyperLink ID="hlkDownload" runat="server"><asp:Image ID="imgDownloadIcon" runat="server" BorderWidth="0" ImageUrl="~/images/icons/download_icon.png" />&nbsp;Download</asp:HyperLink></nobr>
                                    <%--<br /><asp:Literal ID="litPasswordProtected" runat="server" />--%>
                                </td>
                            </tr>
                            <tr id="trXrefs" runat="server" visible="false">
                                <td colspan="3" style="padding-left: 20px;" valign="top">
                                    <asp:Repeater ID="rptXrefs" runat="server">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlkXRef" runat="server" Target="_blank" /><br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            <tr ID="trComments" runat="server">
                                <td ID="tdComments" runat="server" valign="top" colspan="3"><i><asp:Literal ID="litComments" runat="server" /></i></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:placeholder ID="phNoActiveItems" runat="server" Visible="false"><tr><td colspan="4">No results for your search</td></tr></asp:placeholder>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlCurrentReleaseSearch" runat="server">
                <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3Search">
		        <asp:Repeater ID="rptDownloadsSearch" runat="server">
				    <HeaderTemplate>
						<tr>
                            <td valign="top" bgcolor="#C0C0C0"><b>&nbsp;Current Release Version</b></td>
							<td valign="top" bgcolor="#C0C0C0" align="center"><b>Version</b></td>
							<td valign="top" bgcolor="#C0C0C0" align="center"><b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b></td>
							<td valign="top" bgcolor="#C0C0C0" align="center">&nbsp;</td>
						</tr>
				    </HeaderTemplate>
				    <ItemTemplate>
						<tr>
							<td valign="top" colspan="4"><hr noshade color="#C0C0C0" size="1" /></td>
						</tr>
						<tr>
							<td valign="top"><b><asp:Literal ID="litTitleSearch" runat="server" /></b><br /><asp:Literal ID="litDescriptionSearch" runat="server" /></td>
							<td valign="top" align="center"><asp:Literal ID="litVersionSearch" runat="server" /></td>
							<td valign="top" align="center"><asp:Literal ID="litDocSizeSearch" runat="server" /></td>
							<td valign="top" align="center" rowspan="3">
									<nobr><asp:HyperLink ID="hlkDownloadSearch" runat="server"><asp:Image ID="imgDownloadIconSearch" runat="server" BorderWidth="0" ImageUrl="~/images/icons/download_icon.png" />&nbsp;Download</asp:HyperLink></nobr>
                                    <%--<br /><asp:Literal ID="litPasswordProtectedSearch" runat="server" />--%>
							</td>
						</tr>
						<tr id="trXrefsSearch" runat="server" visible="false">
							<td colspan="3" style="padding-left: 20px;" valign="top">
									<asp:Repeater ID="rptXrefsSearch" runat="server">
											<ItemTemplate>
													<asp:HyperLink ID="hlkXRefSearch" runat="server" Target="_blank" /><br />
											</ItemTemplate>
									</asp:Repeater>
							</td>
						</tr>
						<tr ID="trCommentsSearch" runat="server">
							<td ID="tdCommentsSearch" runat="server" valign="top" colspan="3"><i><asp:Literal ID="litCommentsSearch" runat="server" /></i></td>
						</tr>
				    </ItemTemplate>
		        </asp:Repeater>
		        <asp:placeholder ID="phNoActiveItemsSearch" runat="server" Visible="false"><tr><td colspan="4">No results for your search</td></tr></asp:placeholder>
                </table>
            </asp:Panel>
            <%--<asp:PlaceHolder ID="phArchiveVersion" runat="server" Visible="false"><h2 ID="hdrArchivedVersion" runat="server">Previous release versions</h2></asp:PlaceHolder>--%>
            
            
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <asp:Repeater ID="rptBetaReleases" runat="server">
                    <HeaderTemplate>
                        <tr>
                            <td valign="top" bgcolor="#C0C0C0"><b>&nbsp;Current Beta Version</b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center"><b>Version</b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center"><b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center">&nbsp;</td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td valign="top" colspan="4"><hr noshade color="#C0C0C0" size="1" /></td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4"><b><asp:Literal ID="litBetaParent" runat="server" /></b></td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-left: 10px;"><b><asp:Literal ID="litBetaTitle" runat="server" /></b><br /><asp:Literal ID="litBetaDescription" runat="server" /></td>
                            <td valign="top" align="center"><asp:Literal ID="litBetaVersion" runat="server" /></td>
                            <td valign="top" align="center"><asp:Literal ID="litBetaDocSize" runat="server" /></td>
                            <td valign="top" align="center" rowspan="3">
                                <nobr><asp:HyperLink ID="hlkBetaDownload" runat="server"><asp:Image ID="imgArchiveDownloadIcon" runat="server" BorderWidth="0" ImageUrl="~/images/icons/download_icon.png" />&nbsp;Download</asp:HyperLink></nobr>
                            </td>
                        </tr>
                        <tr id="trBetaXrefs" runat="server" visible="false">
							<td colspan="3" style="padding-left: 20px;" valign="top">
									<asp:Repeater ID="rptBetaXrefs" runat="server">
											<ItemTemplate>
													<asp:HyperLink ID="hlkBetaXref" runat="server" Target="_blank" /><br />
											</ItemTemplate>
									</asp:Repeater>
							</td>
						</tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            
            
            
            <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table4">
                <asp:Repeater ID="rptArchiveDownloads" runat="server">
                    <HeaderTemplate>
                        <tr>
                            <td valign="top" bgcolor="#C0C0C0"><b>&nbsp;Previous Release Version</b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center"><b>Version</b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center"><b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b></td>
                            <td valign="top" bgcolor="#C0C0C0" align="center">&nbsp;</td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td valign="top" colspan="4"><hr noshade color="#C0C0C0" size="1" /></td>
                        </tr>
                        <tr>
                            <td valign="top"><b><asp:Literal ID="litArchiveTitle" runat="server" /></b><br /><asp:Literal ID="litArchiveDescription" runat="server" /></td>
                            <td valign="top" align="center"><asp:Literal ID="litArchiveVersion" runat="server" /></td>
                            <td valign="top" align="center"><asp:Literal ID="litArchiveDocSize" runat="server" /></td>
                            <td valign="top" align="center" rowspan="3">
                                <nobr><asp:HyperLink ID="hlkArchiveDownload" runat="server"><asp:Image ID="imgArchiveDownloadIcon" runat="server" BorderWidth="0" ImageUrl="~/images/icons/download_icon.png" />&nbsp;Download</asp:HyperLink></nobr><br /><asp:Literal ID="litArchivePasswordProtected" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3"><i><asp:Literal ID="litArchiveComments" runat="server" /></i></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>
    <script>
        function SetDownloadSearchButtonStatus(senderTextCtl, targetBtnId) {
            var targetBtn = document.getElementById(targetBtnId)
            if (targetBtn) {
                if (senderTextCtl) {
                    targetBtn.disabled = senderTextCtl.value.length < 2;
                } else {
                    console.log('No textbox source...');
                }
            } else {
                console.log('No button target...');
            }
        }
    </script>
</asp:Content>
