﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports Newtonsoft.Json

Partial Class Support_SoftwareDownload_PSGDocuments
    Inherits BasePage

#Region "Variables/Keys"
    Private menuUrl As String = String.Empty
    Private menuUrl2 As String = String.Empty
    Private _protocolStandardId As Nullable(Of Int32) = Nothing
    Private _productSeriesId As Nullable(Of Int32) = Nothing
    Private _documentXrefs As List(Of DocumentXRef) = New List(Of DocumentXRef)
    Private _documentBetaXrefs As List(Of DocumentXRef) = New List(Of DocumentXRef)
    Private _childDocumentsViaXref As List(Of Document) = New List(Of Document)
    Private _childDocumentsBetaViaXref As List(Of Document) = New List(Of Document)
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Analysis Software"
        ValidateQueryStrings()
        ProcessMenus()
        If (_protocolStandardId.HasValue And _productSeriesId.HasValue) Then
            litContentTitle.Text = "Analysis Software for " + Functions.GetProtocolStandardNameOnID(_protocolStandardId.Value)
            GetDocumentsForProtocolProductSeries()
        ElseIf (_protocolStandardId.HasValue) Then
            litContentTitle.Text = "Analysis Software for " + Functions.GetProtocolStandardNameOnID(_protocolStandardId.Value)
            GetDocumentsForProtocolStandard()
        Else
            litContentTitle.Text = "Analysis Software"
        End If
    End Sub
#End Region

#Region "Control Events"
    Protected Sub btnDownloadSearch_Click(sender As Object, e As EventArgs) Handles btnDownloadSearch.Click
        Dim searchVal As String = tbDownloadSearchText.Text
        If (searchVal.Length >= 2) Then ' no specific purpose for using 2 - other than it covers "FC Protocol"
            GetDocumentsBasedOnSearch(searchVal)
            btnDownloadSearch.Enabled = True    ' not sure why but otherwise button is disabled until a new character is pressed
        End If
    End Sub

    Private Sub rptDownloads_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDownloads.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, LeCroy.Library.Domain.Common.DTOs.Document)
                Dim litTitle As Literal = CType(e.Item.FindControl("litTitle"), Literal)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)
                Dim litVersion As Literal = CType(e.Item.FindControl("litVersion"), Literal)
                Dim litDocSize As Literal = CType(e.Item.FindControl("litDocSize"), Literal)
                Dim hlkDownload As HyperLink = CType(e.Item.FindControl("hlkDownload"), HyperLink)
                Dim litComments As Literal = CType(e.Item.FindControl("litComments"), Literal)
                Dim litPasswordProtected As Literal = CType(e.Item.FindControl("litPasswordProtected"), Literal)
                Dim trXrefs As HtmlTableRow = CType(e.Item.FindControl("trXrefs"), HtmlTableRow)
                Dim tdComments As HtmlTableCell = CType(e.Item.FindControl("tdComments"), HtmlTableCell)
                Dim rptXrefs As Repeater = CType(e.Item.FindControl("rptXrefs"), Repeater)

                litTitle.Text = row.Title
                litDescription.Text = IIf(String.IsNullOrEmpty(row.Description), String.Empty, String.Format("{0}<br />", row.Description))
                litVersion.Text = row.Version
                litDocSize.Text = row.DocSize
                hlkDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}&psid={2}", String.Empty, row.DocumentId, ViewState("ProtocolProductSeriesId"))
                If (_documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Count > 0) Then
                    Dim docFetch As List(Of Document) = (From c In _childDocumentsViaXref Where _documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).Where(Function(x) Not x.IsBeta).ToList()
                    If (docFetch.Count > 0) Then
                        trXrefs.Visible = True
                        AddHandler rptXrefs.ItemDataBound, AddressOf rptXrefs_ItemDataBound
                        rptXrefs.DataSource = HackFixSortIdsInCaseOfNulls(docFetch).OrderBy(Function(x) x.SortId)
                        rptXrefs.DataBind()
                    End If
                End If
                litComments.Text = IIf(String.IsNullOrEmpty(row.Comments), String.Empty, String.Format("{0}<br />", row.Comments))
                'If (row.DocTypeId = 9) Then
                '    litPasswordProtected.Text = row.PasswordYN
                'End If
        End Select
    End Sub

    Private Sub rptDownloadsSearch_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDownloadsSearch.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim litTitle As Literal = CType(e.Item.FindControl("litTitleSearch"), Literal)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescriptionSearch"), Literal)
                Dim litVersion As Literal = CType(e.Item.FindControl("litVersionSearch"), Literal)
                Dim litDocSize As Literal = CType(e.Item.FindControl("litDocSizeSearch"), Literal)
                Dim hlkDownload As HyperLink = CType(e.Item.FindControl("hlkDownloadSearch"), HyperLink)
                Dim litComments As Literal = CType(e.Item.FindControl("litCommentsSearch"), Literal)
                Dim litPasswordProtected As Literal = CType(e.Item.FindControl("litPasswordProtectedSearch"), Literal)
                Dim trXrefs As HtmlTableRow = CType(e.Item.FindControl("trXrefsSearch"), HtmlTableRow)
                Dim tdComments As HtmlTableCell = CType(e.Item.FindControl("tdCommentsSearch"), HtmlTableCell)
                Dim rptXrefsSearch As Repeater = CType(e.Item.FindControl("rptXrefsSearch"), Repeater)

                litTitle.Text = row.Title
                litDescription.Text = IIf(String.IsNullOrEmpty(row.Description), row.Description, String.Concat(row.Description, "<br />"))
                litVersion.Text = row.Version
                litDocSize.Text = row.DocSize
                hlkDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}&psid={2}", String.Empty, row.DocumentId, ViewState("ProtocolProductSeriesId"))
                litComments.Text = IIf(String.IsNullOrEmpty(row.Comments), String.Empty, String.Concat(row.Comments, "<br />"))
                ' Add Search result document xrefs
                If (_documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Count > 0) Then
                    trXrefs.Visible = True
                    AddHandler rptXrefsSearch.ItemDataBound, AddressOf rptXrefsSearch_ItemDataBound
                    Dim docFetch As List(Of Document) = (From c In _childDocumentsViaXref Where _documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).ToList()
                    If (docFetch.Count > 0) Then
                        rptXrefsSearch.DataSource = HackFixSortIdsInCaseOfNulls(docFetch).OrderBy(Function(x) x.SortId)
                        rptXrefsSearch.DataBind()
                    End If
                End If
        End Select
    End Sub

    Private Sub rptBetaReleases_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptBetaReleases.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedDocumentForBeta = CType(e.Item.DataItem, ExtendedDocumentForBeta)
                Dim litBetaParent As Literal = CType(e.Item.FindControl("litBetaParent"), Literal)
                Dim litBetaTitle As Literal = CType(e.Item.FindControl("litBetaTitle"), Literal)
                Dim litBetaDescription As Literal = CType(e.Item.FindControl("litBetaDescription"), Literal)
                Dim litBetaVersion As Literal = CType(e.Item.FindControl("litBetaVersion"), Literal)
                Dim litBetaDocSize As Literal = CType(e.Item.FindControl("litBetaDocSize"), Literal)
                Dim hlkBetaDownload As HyperLink = CType(e.Item.FindControl("hlkBetaDownload"), HyperLink)
                Dim trBetaXrefs As HtmlTableRow = CType(e.Item.FindControl("trBetaXrefs"), HtmlTableRow)
                Dim rptBetaXrefs As Repeater = CType(e.Item.FindControl("rptBetaXrefs"), Repeater)

                litBetaParent.Text = row.ParentTitle
                litBetaTitle.Text = row.Document.Title
                litBetaDescription.Text = IIf(String.IsNullOrEmpty(row.Document.Description), String.Empty, String.Format("{0}<br />", row.Document.Description))
                litBetaVersion.Text = row.Document.Version
                litBetaDocSize.Text = row.Document.DocSize
                hlkBetaDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}&psid={2}", String.Empty, row.Document.DocumentId, ViewState("ProtocolProductSeriesId"))
                If (_documentBetaXrefs.Where(Function(x) x.ParentDocumentFkId = row.Document.DocumentId).ToList().Count > 0) Then
                    trBetaXrefs.Visible = True
                    AddHandler rptBetaXrefs.ItemDataBound, AddressOf rptBetaXrefs_ItemDataBound
                    Dim docFetch As List(Of Document) = (From c In _childDocumentsBetaViaXref Where _documentBetaXrefs.Where(Function(x) x.ParentDocumentFkId = row.Document.DocumentId).ToList().Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).ToList()
                    If (docFetch.Count > 0) Then
                        rptBetaXrefs.DataSource = HackFixSortIdsInCaseOfNulls(docFetch).OrderBy(Function(x) x.SortId)
                        rptBetaXrefs.DataBind()
                    End If
                End If
        End Select
    End Sub

    Private Sub rptArchiveDownloads_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptArchiveDownloads.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim litArchiveTitle As Literal = CType(e.Item.FindControl("litArchiveTitle"), Literal)
                Dim litArchiveDescription As Literal = CType(e.Item.FindControl("litArchiveDescription"), Literal)
                Dim litArchiveVersion As Literal = CType(e.Item.FindControl("litArchiveVersion"), Literal)
                Dim litArchiveDocSize As Literal = CType(e.Item.FindControl("litArchiveDocSize"), Literal)
                Dim hlkArchiveDownload As HyperLink = CType(e.Item.FindControl("hlkArchiveDownload"), HyperLink)
                Dim litArchiveComments As Literal = CType(e.Item.FindControl("litArchiveComments"), Literal)

                litArchiveTitle.Text = row.Title
                litArchiveDescription.Text = IIf(String.IsNullOrEmpty(row.Description), String.Empty, String.Format("{0}<br />", row.Description))
                litArchiveVersion.Text = row.Version
                litArchiveDocSize.Text = row.DocSize
                hlkArchiveDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}&psid={2}", String.Empty, row.DocumentId, ViewState("ProtocolProductSeriesId"))
                litArchiveComments.Text = IIf(String.IsNullOrEmpty(row.Comments), String.Empty, String.Concat(row.Comments, "<br />"))
        End Select
    End Sub

    Private Sub rptXrefs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkXRef As HyperLink = CType(e.Item.FindControl("hlkXRef"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkXRef.NavigateUrl = redirectUrl
                    hlkXRef.Text = row.Title
                End If
        End Select
    End Sub

    Private Sub rptBetaXrefs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkBetaXRef As HyperLink = CType(e.Item.FindControl("hlkBetaXRef"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkBetaXRef.NavigateUrl = redirectUrl
                    hlkBetaXRef.Text = row.Title
                End If
        End Select
    End Sub

    Private Sub rptXrefsSearch_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkXRef As HyperLink = CType(e.Item.FindControl("hlkXRefSearch"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkXRef.NavigateUrl = redirectUrl
                    hlkXRef.Text = row.Title
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryStrings()
        If Not (String.IsNullOrEmpty(Request.QueryString("standardid"))) Then
            Dim tempVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("standardid"), tempVal)) Then
                _protocolStandardId = tempVal
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("mseries"))) Then
            Dim tempVal As Int32 = 0
            If (Int32.TryParse(Request.QueryString("mseries"), tempVal)) Then
                _productSeriesId = tempVal
            End If
        End If
    End Sub

    Private Sub ProcessMenus()
        Dim subMenuID As String = String.Empty
        Dim captionID As String = String.Empty
        Dim menuID As String = String.Empty
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuUrl = "&capid=" + captionID
        menuUrl += "&mid=" + menuID
        menuUrl += "&smid=" + subMenuID
        menuUrl2 = "?capid=" + captionID
        menuUrl2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
        lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False
    End Sub

    Private Sub GetDocumentsForProtocolProductSeries()
        Dim documents As List(Of Document)
        pnlCurrentRelease.Visible = True
        pnlCurrentReleaseSearch.Visible = False

        documents = FeDocumentManager.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), DocumentTypeIds.SOFTWARE, CategoryIds.PROTOCOL_ANALYZERS, _productSeriesId.Value, Nothing, _protocolStandardId.Value, Nothing).Where(Function(x) x.IsBeta = 0).ToList()
        rptDownloads.DataSource = documents
        rptDownloads.DataBind()
        phNoActiveItems.Visible = documents.Count = 0
        If (documents.Count > 0) Then
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptDownloads.DataSource = documents '.OrderByDescending(Function(x) x.DateEntered)
        Else
            rptDownloads.DataSource = Nothing
        End If
        rptDownloads.DataBind()

        Dim betaDocs As List(Of ExtendedDocumentForBeta) = DocumentRepository.GetBetaDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), String.Empty, documents.Select(Function(x) x.DocumentId).Distinct().ToList(), DocumentTypeIds.SOFTWARE)
        If (betaDocs.Count > 0) Then
            rptBetaReleases.Visible = True
            _documentBetaXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), betaDocs.Select(Function(x) x.Document.DocumentId).Distinct().ToList())
            If (_documentBetaXrefs.Count > 0) Then
                _childDocumentsBetaViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentBetaXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptBetaReleases.DataSource = betaDocs '.OrderByDescending(Function(x) x.Document.DateEntered)
            rptBetaReleases.DataBind()
        Else
            rptBetaReleases.Visible = False
            rptBetaReleases.DataSource = Nothing
            rptBetaReleases.DataBind()
        End If

        Dim sqlString As String = "SELECT DISTINCT d.* FROM [DOCUMENT] d INNER JOIN [DOCUMENT_CATEGORY] dc ON dc.[DOCUMENT_ID] = d.[DOCUMENT_ID] WHERE d.[POST] = 'D' AND d.[DOC_TYPE_ID] = @DOCTYPEID AND dc.[PRODUCT_SERIES_ID] = @PRODUCTSERIESID  AND dc.[PROTOCOL_STANDARD_ID] = @PROTOCOLSTANDARDID AND dc.[CATEGORY_ID] = @CATEGORYID AND d.[IsBeta] = 0"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", DocumentTypeIds.SOFTWARE))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.Value))
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _protocolStandardId.Value))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", CategoryIds.PROTOCOL_ANALYZERS.ToString()))
        Dim archiveDocuments As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
        If (archiveDocuments.Count > 0) Then
            rptArchiveDownloads.DataSource = archiveDocuments '.OrderByDescending(Function(x) x.DateEntered)
            rptArchiveDownloads.DataBind()
        Else
            rptArchiveDownloads.DataSource = Nothing
            rptArchiveDownloads.DataBind()
        End If
        GenerateJsonString()
    End Sub

    Private Sub GetDocumentsForProtocolStandard()
        Dim documents As List(Of Document)
        pnlCurrentRelease.Visible = True
        pnlCurrentReleaseSearch.Visible = False

        documents = FeDocumentManager.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), DocumentTypeIds.SOFTWARE, CategoryIds.PROTOCOL_ANALYZERS, Nothing, Nothing, _protocolStandardId.Value, Nothing).Where(Function(x) x.IsBeta = 0).ToList()
        rptDownloads.DataSource = documents
        rptDownloads.DataBind()
        phNoActiveItems.Visible = documents.Count = 0
        If (documents.Count > 0) Then
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptDownloads.DataSource = documents '.OrderByDescending(Function(x) x.DateEntered)
        Else
            rptDownloads.DataSource = Nothing
        End If
        rptDownloads.DataBind()

        Dim betaDocs As List(Of ExtendedDocumentForBeta) = DocumentRepository.GetBetaDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), String.Empty, documents.Select(Function(x) x.DocumentId).Distinct().ToList(), DocumentTypeIds.SOFTWARE)
        If (betaDocs.Count > 0) Then
            rptBetaReleases.Visible = True
            _documentBetaXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), betaDocs.Select(Function(x) x.Document.DocumentId).Distinct().ToList())
            If (_documentBetaXrefs.Count > 0) Then
                _childDocumentsBetaViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentBetaXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptBetaReleases.DataSource = betaDocs '.OrderByDescending(Function(x) x.Document.DateEntered)
            rptBetaReleases.DataBind()
        Else
            rptBetaReleases.Visible = False
            rptBetaReleases.DataSource = Nothing
            rptBetaReleases.DataBind()
        End If

        Dim sqlString As String = "SELECT DISTINCT d.* FROM [DOCUMENT] d INNER JOIN [DOCUMENT_CATEGORY] dc ON dc.[DOCUMENT_ID] = d.[DOCUMENT_ID] WHERE d.[POST] = 'D' AND d.[DOC_TYPE_ID] = @DOCTYPEID AND dc.[PROTOCOL_STANDARD_ID] = @PROTOCOLSTANDARDID AND dc.[CATEGORY_ID] = @CATEGORYID AND d.[IsBeta] = 0"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", DocumentTypeIds.SOFTWARE))
        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", _protocolStandardId.Value))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", CategoryIds.PROTOCOL_ANALYZERS.ToString()))
        Dim archiveDocuments As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
        If (archiveDocuments.Count > 0) Then
            rptArchiveDownloads.DataSource = archiveDocuments '.OrderByDescending(Function(x) x.DateEntered)
            rptArchiveDownloads.DataBind()
        Else
            rptArchiveDownloads.DataSource = Nothing
            rptArchiveDownloads.DataBind()
        End If
        GenerateJsonString()
    End Sub

    Private Sub GetDocumentsBasedOnSearch(searchVal As String)
        pnlCurrentRelease.Visible = False
        pnlCurrentReleaseSearch.Visible = True
        searchVal = EscapeSQLSearch(searchVal)
        Dim qdataList As List(Of String) = ConfigurationManager.AppSettings("PSGSwDownloadSearchTerms_FTE").ToString().Split(",").ToList()
        Dim flineList As List(Of String) = ConfigurationManager.AppSettings("PSGSwDownloadSearchTerms_QD").ToString().Split(",").ToList()

        If flineList.Contains(searchVal.ToLower()) Then
            Response.Redirect("https://fte.com/products/default.aspx")
        ElseIf qdataList.Contains(searchVal.ToLower()) Then
            Response.Redirect("https://www.quantumdata.com/downloads.html")
        End If

        Dim sqlString As String = "SELECT DISTINCT d.* FROM [DOCUMENT] d INNER JOIN [DOCUMENT_CATEGORY] dc ON dc.[DOCUMENT_ID] = d.[DOCUMENT_ID] INNER JOIN [PRODUCT_SERIES] ps ON ps.[PRODUCT_SERIES_ID] = dc.[PRODUCT_SERIES_ID] INNER JOIN [PROTOCOL_STANDARD] std ON std.[PROTOCOL_STANDARD_ID] = dc.[PROTOCOL_STANDARD_ID] WHERE d.[POST] IN (@POSTY, @POSTD) AND d.[DOC_TYPE_ID] = @DOCTYPEID AND d.[IsBeta] = 0 AND (d.[TITLE] LIKE ('%' + @SEARCHVALUE + '%') OR d.[DESCRIPTION] LIKE ('%' + @SEARCHVALUE + '%') OR d.[COMMENTS] LIKE ('%' + @SEARCHVALUE + '%') OR d.[KEYWORDS] LIKE ('%' + @SEARCHVALUE + '%')) "
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@POSTY", "Y"))
        sqlParameters.Add(New SqlParameter("@POSTD", "D"))
        sqlParameters.Add(New SqlParameter("@DOCTYPEID", DocumentTypeIds.SOFTWARE.ToString()))
        sqlParameters.Add(New SqlParameter("@SEARCHVALUE", searchVal))
        Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()).ToList()
        Dim activeDocuments As List(Of Document) = documents.Where(Function(x) x.Post.ToUpper() = "Y").ToList()
        If (activeDocuments.Count > 0) Then
            phNoActiveItemsSearch.Visible = False
            Dim docIds As List(Of Int32) = activeDocuments.Select(Function(x) x.DocumentId).Distinct().ToList()
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), docIds)
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").Where(Function(x) x.IsBeta = False).ToList()
            End If
            rptDownloadsSearch.DataSource = activeDocuments '.OrderByDescending(Function(x) x.DateEntered)
            rptDownloadsSearch.DataBind()
        Else
            phNoActiveItemsSearch.Visible = True
            rptDownloadsSearch.DataSource = Nothing
            rptDownloadsSearch.DataBind()
        End If

        Dim betaDocs As List(Of ExtendedDocumentForBeta) = DocumentRepository.GetBetaDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), String.Empty, activeDocuments.Select(Function(x) x.DocumentId).Distinct().ToList(), DocumentTypeIds.SOFTWARE)
        If (betaDocs.Count > 0) Then
            rptBetaReleases.Visible = True
            _documentBetaXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), betaDocs.Select(Function(x) x.Document.DocumentId).Distinct().ToList())
            If (_documentBetaXrefs.Count > 0) Then
                _childDocumentsBetaViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentBetaXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptBetaReleases.DataSource = betaDocs '.OrderByDescending(Function(x) x.Document.DateEntered)
            rptBetaReleases.DataBind()
        Else
            rptBetaReleases.Visible = False
            rptBetaReleases.DataSource = Nothing
            rptBetaReleases.DataBind()
        End If

        Dim archivedDocuments As List(Of Document) = documents.Where(Function(x) x.Post.ToUpper() = "D").ToList()
        If (archivedDocuments.Count > 0) Then
            rptArchiveDownloads.DataSource = archivedDocuments '.OrderByDescending(Function(x) x.DateEntered)
            rptArchiveDownloads.DataBind()
        Else
            rptArchiveDownloads.DataSource = Nothing
            rptArchiveDownloads.DataBind()
        End If
    End Sub

    Private Sub ClearDocuments()
        phNoActiveItems.Visible = False
        rptDownloads.DataSource = Nothing
        rptDownloads.DataBind()
        rptArchiveDownloads.DataSource = Nothing
        rptArchiveDownloads.DataBind()
    End Sub

    Private Function HackFixSortIdsInCaseOfNulls(ByVal documents As List(Of Document)) As List(Of Document)
        If (documents.Count <= 0) Then Return documents
        Dim retVal As List(Of Document) = New List(Of Document)
        For Each document As Document In documents
            Dim matching As DocumentXRef = _documentXrefs.Where(Function(x) x.ChildDocumentFkId = document.DocumentId).FirstOrDefault()
            Dim doc As Document = New Document()
            doc.DocumentId = document.DocumentId
            doc.DocTypeId = document.DocTypeId
            doc.Title = document.Title
            If Not (matching Is Nothing) Then
                doc.SortId = matching.SortOrder
            Else
                If Not (document.SortId.HasValue) Then
                    doc.SortId = Int32.MaxValue
                Else
                    doc.SortId = document.SortId.Value
                End If
            End If
            retVal.Add(doc)
        Next
        Return retVal.OrderBy(Function(x) x.SortId.Value).ToList()
    End Function

    Private Function EscapeSQLSearch(ByVal searchVal As String) As String
        searchVal = searchVal.Replace("\", "\\")
        searchVal = searchVal.Replace("%", "\%")
        searchVal = searchVal.Replace("[", "\[")
        searchVal = searchVal.Replace("]", "\]")
        searchVal = searchVal.Replace("_", "\_")
        Return searchVal
    End Function
#End Region

#Region "WebMethods"
    <WebMethod()>
    Public Shared Function getprotocoldata() As String
        Return GenerateJsonString()
    End Function

    Private Shared Function GenerateJsonString() As String
        Dim excludeStandardIds As List(Of Int32) = ConfigurationManager.AppSettings("PSGSwDownloadExcludeStandardIds").ToString().Split(",").[Select](AddressOf Int32.Parse).ToList()
        Dim excludeSeriesIds As List(Of Int32) = ConfigurationManager.AppSettings("PSGSwDownloadExcludeSeriesIds").ToString().Split(",").[Select](AddressOf Int32.Parse).ToList()
        Dim protocolData As List(Of Specialized.ProtocolStandardData) = ProtocolStandardRepository.GetActiveProtocolStandardsAndSeries(ConfigurationManager.AppSettings("ConnectionString"), excludeStandardIds, excludeSeriesIds).ToList().Where(Function(x) Not (x.StandardId = 11)).ToList()
        Return JsonConvert.SerializeObject(protocolData)
    End Function
#End Region
End Class