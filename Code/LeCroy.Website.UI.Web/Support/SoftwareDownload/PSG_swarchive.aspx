<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_PSG_swarchive" Codebehind="PSG_swarchive.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <p><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download" /></p>
        <h2><asp:Label ID="lb_Title" runat="server" /></h2>
        <p>Please click on the download button to begin your upgrade.</p>
        <p><b>Note:</b><br />You may be redirected to a short form if you are not a registered user.</p>
        <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3">
            <tr>
                <td valign="top" colspan="5">
                    <asp:Label ID="lblSWLink" runat="server" />
                </td>
            </tr>
            <tr>
                <td valign="top" bgcolor="#C0C0C0">
                    &nbsp;
                </td>
                <td valign="top" bgcolor="#C0C0C0" align="center">
                    <b>Version</b>
                </td>
                <td valign="top" bgcolor="#C0C0C0" align="center">
                    <b>
                        <nobr>&nbsp;&nbsp;&nbsp;File Size</nobr>
                    </b>
                </td>
                <td valign="top" bgcolor="#c0c0c0" align="center">
                    <b><nobr>&nbsp;&nbsp;Released</nobr></b>
                </td>
                <td valign="top" bgcolor="#C0C0C0" align="center">
                    &nbsp;
                </td>
            </tr>
            <asp:Repeater ID="rpt_main" runat="server">
                <ItemTemplate>
                    <tr>
                        <td valign="top" height="1" colspan="5">
                            <hr noshade color="#C0C0C0" size="1">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <b><%#Eval("TITLE")%></b><br>
                            <%#Eval("DESCRIPTION")%>
                        </td>
                        <td valign="top" align="center">
                            <%#Eval("VERSION")%>
                        </td>
                        <td valign="top" align="center">
                            <%#Eval("DOC_SIZE")%>
                        </td>
                        <td valign="top" align="center">
                            <%# Eval("DATE_ENTERED", "{0:dd-MMM-yyyy}")%>
                        </td>
                        <td valign="top" align="center">
                            <a href="download.aspx?did=<%#Eval("DOCUMENT_ID")%><%=menuURL %>"><img src="<%= rootDir%>/images/icons/download_icon.png" border="0">&nbsp;Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="4">
                            <br><i><%#Eval("COMMENTS").ToString()%></i><br><br>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>