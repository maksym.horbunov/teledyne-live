Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_SoftwareDownload_PSG_swarchive
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Public standardid As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
        Initial()
    End Sub
    Private Sub Initial()
        'If Request("mseries") Is Nothing And Request("cat") Is Nothing Then
        '    Response.Redirect("default.aspx" + menuURL2)
        'Else
        '    If Not IsNumeric(Request("mseries")) And Not IsNumeric(Request("cat")) Then
        '        Response.Redirect("default.aspx" + menuURL2)
        '    End If
        'End If
        Dim strSQL As String = ""
        Dim ds As DataSet
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Not Request("standardid") Is Nothing Then
            standardid = Request("standardid").ToString()
            If Not IsNumeric(standardid) Then
                Response.Redirect("default.aspx" + menuURL2)
            End If

            'Link To SW Arhive
            strSQL = " SELECT DISTINCT DOCUMENT.DOCUMENT_ID, DOCUMENT.TITLE, DOCUMENT.VERSION, DOCUMENT.SORT_ID, DOCUMENT.PASSWORD_YN, DOCUMENT.DATE_ENTERED " +
            " FROM DOCUMENT INNER JOIN    DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID " +
            " WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'y') and  DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID ORDER BY DOCUMENT.SORT_ID"
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", standardid))
            Dim documents As List(Of Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If (documents.Count > 0) Then
                lblSWLink.Text = String.Format("<ul><li class=""links""><a href='documents.aspx?standardid={0}'> <strong>View active software versions for {1}</strong></a></li></ul>", String.Concat(standardid.ToString(), menuURL), Me.convertToString(Functions.GetProtocolStandardNameOnID(standardid)))
            End If
            strSQL = " Select DISTINCT DOCUMENT.DOCUMENT_ID, Document.Title, Document.Description, Document.Version, Document.SORT_ID, Document.PASSWORD_YN, " +
                    " DOCUMENT.DOC_SIZE, Document.Comments, Document.DATE_ENTERED FROM DOCUMENT INNER JOIN    DOCUMENT_CATEGORY On DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID " +
                    " WHERE (DOCUMENT.DOC_TYPE_ID = 9) And (Document.Post = 'd') and  DOCUMENT_CATEGORY.PROTOCOL_STANDARD_ID=@PROTOCOLSTANDARDID ORDER BY DOCUMENT.SORT_ID"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", standardid))
            lb_Title.Text = "Archived Analysis Software for " + Functions.GetProtocolStandardNameOnID(standardid)
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Archived Analysis Software for " + Functions.GetProtocolStandardNameOnID(standardid)
        Else
            strSQL = " SELECT DISTINCT DOCUMENT.DOCUMENT_ID, DOCUMENT.TITLE,DOCUMENT.DESCRIPTION, DOCUMENT.VERSION, DOCUMENT.SORT_ID, DOCUMENT.PASSWORD_YN, " +
             " DOCUMENT.DOC_SIZE, DOCUMENT.COMMENTS, DOCUMENT.DATE_ENTERED FROM DOCUMENT INNER JOIN    DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID " +
             " WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'd') ORDER BY DOCUMENT.TITLE"
            sqlParameters = New List(Of SqlParameter)
            lb_Title.Text = "Archived Analysis Software"
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Archived Analysis Software"
            lblSWLink.Text = "<ul><li class=""links""><a href='PSG.aspx'><strong>View active software versions</strong></a></li></ul>"

        End If
        'Response.Write(strSQL)
        'Response.End()
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        rpt_main.DataSource = ds
        rpt_main.DataBind()

    End Sub

    Public Function info() As String
        Dim strSQL As String
        info = ""
        strSQL = " SELECT DISTINCT   PROPGROUP_SUBCATEGORY.SUBCAT_ID, PROPGROUP_SUBCATEGORY.SUBCAT_NAME,  PROPGROUP_SUBCATEGORY.SORT_ID  " & _
                   " FROM   PROPGROUP_SUBCATEGORY INNER JOIN    PROPERTYGROUP ON   PROPGROUP_SUBCATEGORY.SUBCAT_ID = PROPERTYGROUP.SUBCAT_ID INNER JOIN " & _
                   " DOWNLOAD_CATEGORY ON PROPERTYGROUP.PROPERTYGROUPID = DOWNLOAD_CATEGORY.PROPERTYGROUPID INNER JOIN  DOWNLOAD ON DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID " & _
          " WHERE PROPGROUP_SUBCATEGORY.CATEGORY_ID = 19 AND DOWNLOAD.DOWN_TOPIC_ID=1 AND DOWNLOAD.POST = 'd'" & _
          " order by PROPGROUP_SUBCATEGORY.SORT_ID"
        ' Response.Write(strSQL & "<br>")
        Dim propGroupSubcategories As List(Of PropGroupSubcategory) = PropGroupSubcategoryRepository.FetchPropGroupSubcategories(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        If (propGroupSubcategories.Count > 0) Then
            Response.Write("<table border=""0"" width=""450"" cellpadding=""2"" cellspacing=""2"">")
            For Each propGroupSubcategory In propGroupSubcategories
                Response.Write("<tr>")
                Response.Write("<td valign=top bgcolor=""#E5ECF9"" colspan=2>")
                Response.Write("<b>")
                Response.Write(propGroupSubcategory.SubcatName)
                Response.Write("</b>")
                Response.Write("</td>")
                Response.Write("</tr>")

                strSQL = " SELECT distinct PROPERTYGROUP.PROPERTYGROUPID,PROPERTYGROUP.PROPERTYGROUPNAME" &
                  " FROM  PROPGROUP_SUBCATEGORY INNER JOIN  PROPERTYGROUP ON " &
                  " PROPGROUP_SUBCATEGORY.SUBCAT_ID = PROPERTYGROUP.SUBCAT_ID INNER JOIN" &
                  " DOWNLOAD_CATEGORY ON PROPERTYGROUP.PROPERTYGROUPID = DOWNLOAD_CATEGORY.PROPERTYGROUPID INNER JOIN" &
                  " DOWNLOAD ON DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID " &
                  "WHERE PROPGROUP_SUBCATEGORY.CATEGORY_ID = 19 AND DOWNLOAD.DOWN_TOPIC_ID=1 AND PROPERTYGROUP.SUBCAT_ID=@SUBCATEGORYID AND DOWNLOAD.POST = 'd' ORDER by PROPERTYGROUP.PROPERTYGROUPNAME"

                'Response.Write strSQL
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SUBCATEGORYID", propGroupSubcategory.SubcatId.ToString()))
                Dim propertyGroups As List(Of PropertyGroup) = PropertyGroupRepository.FetchPropertyGroups(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                For Each pg In propertyGroups
                    Response.Write(String.Format("<tr><td valign=top colspan=3><a name='{0}'><b>{1}</b></td></tr>", pg.PropertyGroupId.ToString(), pg.PropertyGroupName))
                    'Link To SW Arhive
                    strSQL = " SELECT  distinct   DOWNLOAD_CATEGORY.DOWN_CAT_ID, DOWNLOAD_CATEGORY.NAME, DOWNLOAD_CATEGORY.VERSION, " &
                      " DOWNLOAD_CATEGORY.SORT_ID,DOWNLOAD_CATEGORY.PASSWORD_YN, DOWNLOAD_CATEGORY.URL" &
                      " FROM DOWNLOAD_CATEGORY INNER JOIN  DOWNLOAD ON DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID" &
                      " WHERE DOWNLOAD_CATEGORY.PROPERTYGROUPID=@PROPERTYGROUPID and DOWNLOAD.POST='y'" &
                      " AND DOWNLOAD.DOWN_TOPIC_ID=1 ORDER BY DOWNLOAD_CATEGORY.SORT_ID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", pg.PropertyGroupId.ToString()))
                    Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If (ds1.Tables(0).Rows.Count > 0) Then
                        Response.Write(String.Format("<tr><td valign=top width=12>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign=top colspan=2><a href='tracersw.aspx{0}#{1}'><strong>View active software versions for {2}</strong></a></td></tr>", menuURL2, pg.PropertyGroupId.ToString(), pg.PropertyGroupName))
                    End If

                    'List Available SW Downloads
                    strSQL = " SELECT  distinct   DOWNLOAD_CATEGORY.DOWN_CAT_ID, DOWNLOAD_CATEGORY.NAME, DOWNLOAD_CATEGORY.VERSION, " &
                      " DOWNLOAD_CATEGORY.SORT_ID,DOWNLOAD_CATEGORY.PASSWORD_YN, DOWNLOAD_CATEGORY.URL" &
                      " FROM DOWNLOAD_CATEGORY INNER JOIN  DOWNLOAD ON DOWNLOAD_CATEGORY.DOWN_CAT_ID = DOWNLOAD.DOWN_CAT_ID" &
                      " WHERE DOWNLOAD_CATEGORY.PROPERTYGROUPID=@PROPERTYGROUPID and DOWNLOAD.POST='d'" &
                      " AND DOWNLOAD.DOWN_TOPIC_ID=1 ORDER BY DOWNLOAD_CATEGORY.NAME"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", pg.PropertyGroupId.ToString()))
                    Dim ds2 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    For Each dr As DataRow In ds2.Tables(0).Rows
                        Response.Write("<tr><td valign=top width=12><img src=""Images/BlueArrowBox_small.gif"" border=0></td><td valign=top width=""200"">")
                        If dr("URL").ToString() <> "" Then
                            Response.Write(String.Format("<a href='documents_archive.aspx?cat={0}{1}'>", dr("DOWN_CAT_ID"), menuURL))
                        Else
                            Response.Write(String.Format("<a href='{0}'>", dr("URL")))
                        End If
                        Response.Write(String.Format("{0}</a></td><td valign=top> - Version {1}", dr("NAME"), dr("VERSION")))
                        If UCase(CStr(dr("PASSWORD_YN"))) = "Y" Then
                            Response.Write("&nbsp;(requires password)")
                        End If
                        Response.Write(String.Format("<b>&nbsp;{0}</b></td></tr><tr><td valign=top colspan=3><hr></td></tr>", Functions.checkSWisnew(dr("DOWN_CAT_ID"), Session("localeid").ToString())))
                    Next
                Next
                Response.Write("<tr><td valign=top colspan=2>&nbsp;</td></tr>")
            Next
            Response.Write("</table><br><br><br>")
        End If
    End Function
End Class