﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_QPHY" Codebehind="QPHY.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br></p>
                    <p><img src="Images/QualiPHY_logo.jpg" alt="QualiPHY" width="50" height="50" /><br></p>
                    <p align="left">QualiPHY is designed to reduce the time and effort needed to perform compliance testing on a wide array of high-speed serial buses. QualiPHY is the most intuitive and efficient tool available for performing serial data compliance testing.</p>
                    <p align="left"><a href="/serialdata/serialdatastandard.aspx?standardid=240">QualiPHY Datasheet</a></p><br />
                    <p align="left">
                        <a href="download.aspx?did=12343"><h2>Download QualiPHY 9.3.0.3</h2></a><br />
                        <a href="/doc/docview.aspx?id=12344">9.3.0.3 Release Notes</a>
                    </p><br />
                     <p align="left">
                        <strong><i>For 32-bit oscilloscopes locked at 8.1.2.0:</i></strong><br />
                        <a href="download.aspx?did=9762"><h2>Download QualiPHY 8.1.0.0</h2></a><br />
                        <a href="/doc/docview.aspx?id=9761">8.1.0.0 Release Notes</a></p><br />
                    <p align="left">
                        <strong><i>For Win2K SP4 Only:</i></strong><br />
                        If Scope/PC does not have the latest windows update, the following hotfix <strong>MUST</strong> be installed on the machine prior to the firmware installation: <a href="download.aspx?did=11066" target="_blank">Microsoft Windows 2000 Service Pack 4</a>
                    </p>
                    <br />

                    <p align="left"><h2>Matlab Runtime Component Installation Documentation:</h2><a href="/doc/docview.aspx?id=7337">Installing Matlab Runtime Component</a></p><br /><br />
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>