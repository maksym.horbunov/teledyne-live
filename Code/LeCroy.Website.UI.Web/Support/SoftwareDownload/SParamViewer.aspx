﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_SParamViewer" Codebehind="SParamViewer.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left">
                        <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>
                        <p align="left"><h2>SParamViewer</h2><br><strong>Version 2.0</strong></p>
                        <p align="left"><a href="download.aspx?did=5122"><h2>Download</h2></a></p>
                        <img src="/images/sparam_01.jpg" width="285" alt="S Parameter Viewer" align="right" />
                        <p>SParamViewer is a free tool for plotting S-Parameters</p>
                        <p>Features</p>
                        <ul>
                            <li>Reads standard Touchstone format S-Parameter files</li>
                            <li>Zoom in for more detail</li>
                            <li>Copy and Paste S-Parameter data as images, text, or Microsoft Excel&reg; format</li>
                        </ul>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources">
        <table border="0" cellpadding="2" cellspacing="0">
            <tr><td colspan="2" align="left" valign="top"><h3>S-Parameter Information</h3></td></tr>
            <tr><td><div><strong>SPARQ</strong></div></td></tr>
            <tr><td valign="middle">Teledyne LeCroy SPARQ family of TDR based network analyzers are instruments that connect directly to the device-under-test and to PC based software through a single USB connection for quick, mult-port S-parameter measurements.</td></tr>
            <tr><td valign="middle"><a href="/sparq/">More info on SPARQ &raquo;</a></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><b>Virtual Probing and Equalizer Emulation</b></td></tr>
            <tr><td><a href="/serialdata/serialdatastandard.aspx?standardid=226">Eye Doctor</a></td></tr>
            <tr><td><a href="/serialdata/serialdatastandard.aspx?standardid=226">Eye Doctor II</a></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><b>High Bandwidth Oscilloscope Solutions</b></td></tr>
            <tr><td><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=484">WaveExpert Sampling Oscilloscope</a><br />up to 100 GHz</td></tr>
            <tr><td><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=329">WaveMaster 8 Zi-A Oscilloscope</a><br />up to 30 GHz Real-time</td></tr>
        </table>
    </div>
</asp:Content>