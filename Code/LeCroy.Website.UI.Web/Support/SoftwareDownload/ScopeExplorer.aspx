﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_ScopeExplorer" Codebehind="ScopeExplorer.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src='<%= rootDir%>/images/category/headers/hd_download.gif' alt='Software Download'><br></p>
                    <p align="left">
                        <img border="0" src="Images/ScopeXplorerlogo.gif" width="151" height="29"><br>
                        <strong>Version 2.25</strong>
                    </p>
                    Click on the Download button below to start downloading the ScopeExplorer.<br><br>
                    Scope Explorer is a free application for controlling your Teledyne LeCroy oscilloscope from a Windows 2000/XP computer. Use this application to control the oscilloscope via remote control without the need for advanced programming. With it, you can
                    <ul><li>Connect to your scope via GPIB, ETHERNET (Plus RS232 for older scopes)</li><li>Save screen shots</li><li>Save waveforms in a variety of formats</li><li>Save panel setups</li><li>Send remote control commands using a terminal window. (Excellent for learning how to use our remote control language!)</li><li>Update your oscilloscope's firmware*</li></ul>
                    Compatible with almost all Teledyne LeCroy Oscilloscopes!<br><br>
                    * On older scopes, ScopeExplorer can be used to update your scope's firmware over GPIB and TCP/IP connections. Firewalls have been known to block the ability for ScopeExplorer to execute firmware upgrades. Please contact your local Teledyne LeCroy Customer Care center with any questions. This feature is not available on Teledyne LeCroy's Windows-based oscilloscope. See the <a href="FirmwareUpgrade.aspx<%= menuURL2 %>">firmware download</a> page for more information<br /><br />
                    <p align="left"><a href="download.aspx?did=4992"><h2>Download</h2></a></p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>