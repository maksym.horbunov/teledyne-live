﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_VICPPassport" Codebehind="VICPPassport.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br /><br>
                    <h2>Instrument Drivers</h2>
                    <p class="SmallBody" align="left">The following drivers are available for download for Teledyne LeCroy Oscilloscopes:</p>
                    <table border="0" cellpadding="2" width="500" cellspacing="1">
                        <tr>
                            <td height="19" bgcolor="#E7EFFF">
                                <b>Downloads</b>
                            </td>
                        </tr>
                        <tr>
                            <td height="1">
                                <hr noshade color="#C0C0C0" size="1">
                            </td>
                        </tr>
                        <tr>
                            <td height="19">
                                <img border="0" src="images/YellowSquare.gif" width="10" height="10"><b>&nbsp; VICP Passport </b>
                            </td>
                        </tr>
                        <tr>
                            <td height="19">
                                <p>
                                    The Teledyne LeCroy VICP Passport is a plug-in passport for National Instruments' VISA (Windows version only). This passport is recommended for all users who wish to communicate with their Teledyne LeCroy Digital Storage Oscilloscope via TCP/IP using NI-VISA.<br /><br />
                                    This passport is required when using the LCXSDSO <a href="LabView.aspx<%=menuURL2 %>">IVI driver</a> to communicate with Teledyne LeCroy X-Stream scopes via TCP/IP. This passport is NOT required when communicating via GPIB.<br /><br />
                                    After installing the VICP Passport, see the Readme file located in the Start Menu under All Programs - LeCroy - VICP Passport.
                                </p>
                                <p>&gt;&gt;<b>&nbsp; </b><a href="download.aspx?did=9293"><b>Download the Teledyne LeCroy VICP Passport Installer 1.12</b></a></p>
                                <p><img src="/images/icons/icons_pdf.gif" alt="adobe" width="20" height="22" /><b>&nbsp; </b><a href="/doc/docview.aspx?id=1027"><b>Download the Teledyne LeCroy LAB WM827 - Understanding VICP</b></a></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>