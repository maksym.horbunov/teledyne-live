﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_WaveStation_SW" Codebehind="WaveStation.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download" /></p>
        <p><h2>WaveStation Software</h2></p>
        <img src="images/1671CC.gif" width="1" height="1" border="0">
        <table border="0" cellpadding="0" width="100%" cellspacing="0" id="Table3">
            <tr>
                <td valign="top" height="100%" width="20" style="background: #C0C0C0; color: #C0C0C0">
                    <img src="images/clearpixel.gif" width="1" height="1" border="0">
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0">
                    <b>Download</b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b>Version</b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b><nobr>&nbsp;&nbsp;&nbsp;File Size</nobr></b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    <b><nobr>&nbsp;&nbsp;Released</nobr></b>
                </td>
                <td valign="top" height="19" bgcolor="#C0C0C0" align="center">
                    &nbsp;<b>Download</b>
                </td>
            </tr>
            <asp:Repeater ID="rpt_main" runat="server">
                <ItemTemplate>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" height="1" colspan="5">
                            <hr noshade color="#C0C0C0" size="1">
                        </td>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="/images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" height="19">
                            <b><%#Eval("TITLE")%></b><br>
                            <%#Eval("DESCRIPTION")%>
                        </td>
                        <td valign="top" height="19" align="center">
                            <%#Eval("VERSION")%>
                        </td>
                        <td valign="top" height="19" align="center">
                            <%#Eval("DOC_SIZE")%>
                        </td>
                        <td valign="top" height="19" align="center">
                            <%# Eval("DATE_ENTERED", "{0:dd-MMM-yyyy}")%>
                        </td>
                        <td valign="top" height="19" align="center">
                            <p align="center"><a href="download.aspx?mseries=<%=mseries%>&did=<%#Eval("DOCUMENT_ID")%><%=menuURL %>">Download</a></p>
                        </td>
                        <td valign="top" height="100%" width="20">
                            <img src="/images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="100%" width="20">
                            <img src="images/clearpixel.gif" width="1" height="1" border="0">
                        </td>
                        <td valign="top" colspan="5">
                            <br><i><%#Eval("COMMENTS").ToString()%></i><br><br>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="6">
                    <asp:Label ID="lbNoData" runat="server" Text="New Software will be released shortly." Visible="False" Font-Bold="False" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>