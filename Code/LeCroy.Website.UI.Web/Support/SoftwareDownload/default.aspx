<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_software" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"></p>
        <p>
            <table border="0" width="100%" cellspacing="0" cellpadding="0" height="108">
                <tr>
                    <td valign="top" align="left" height="38" width="500">
                        To obtain current product Software, select the desired product from the following list:
                        <p align="left">
                            <a name="Oscilloscope"></a>&nbsp;&nbsp;
                            <!--LCRY start-->
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>Oscilloscope Downloads</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul>
                                            <li class="links"><a href="home.aspx<%=menuURL2 %>"><strong>Software Utilities</strong></a></li>
                                            <li class="links"><strong><a href="firmwareupgrade.aspx?categoryid=1<%=menuURL %>">Firmware Upgrades</a></strong></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--LCRY end-->
                            <!--PSG start-->
                            <a name="PSG"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>Protocol Solutions</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul>
                                            <li class="links"><a href="psgdocuments.aspx<%=menuURL2 %>"><strong>Analysis Software</strong></a></li>
                                            <!--<li class="links"><a href="flexnet.aspx<%=menuURL2 %>"><strong>FlexNet Software Downloads</strong></a></li>
                                            <li class="links"><a href="http://fte.com/products/default.aspx"><strong>Frontline Software Downloads</strong></a></li>
                                            <li class="links"><a href="https://www.quantumdata.com/downloads.html"><strong>Quantum Data Downloads</strong></a></li>-->
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--PSG end-->
                            <!--PeRT3 start-->
                            <a name="Pert"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>PeRT3 Downloads</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul><li class="links"><a href="pert3.aspx<%=menuURL2 %>"><strong>PeRT3 Software</strong></a></li></ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--PeRT3 end-->
                            <!--ArbStudio start-->
                            <a name="ArbStudio"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>Waveform Generator Downloads</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul>
                                            <li class="links"><a href="arbstudio.aspx<%=menuURL2 %>"><strong>Software Utilities</strong></a></li>
                                            <li class="links"><a href="firmwareupgrade.aspx?categoryid=2<%=menuURL %>"><strong>Firmware Upgrades</strong></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--ArbStudio end-->
                            <!--LogicStudio start-->
                            <a name="LogicStudio"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>LogicStudio Downloads</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul><li class="links"><a href="logicstudio.aspx<%=menuURL2 %>"><strong>LogicStudio Software</strong></a></li></ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--LogicStudio end-->
                            <!--SIStudio start-->
                            <a name="sistudio"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>Signal Integrity Studio Downloads</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul><li class="links"><a href="sparq.aspx<%=menuURL2 %>"><strong>Signal Integrity Studio Software</strong></a></li></ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--SIStudio end-->
                             <!--TDR start-->
                            <a name="sistudio"></a>
                            <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                                <tr>
                                    <td><h1>TDR and S-Parameters</h1></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2">
                                        <ul><li class="links"><a href="/resources/details.aspx?doctypeid=25&mseries=570"><strong>Time Domain Reflectometers Software</strong></a></li></ul>
                                    </td>
                                </tr>
                                 <tr>
                                    <td height="1" colspan="2">
                                        <ul><li class="links"><a href="/tdr-and-s-parameters/high-speed-interconnect-analyzer/resources/firmware"><strong>WavePulser Software</strong></a></li></ul>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                            <!--TDR end-->
                        &nbsp;&nbsp;
                    </td>
                    <td valign="top" align="center" height="38" width="10">
                        <p align="left">&nbsp;</p>
                    </td>
                    <td valign="top" align="center" height="38">
                        <p>&nbsp;<p>
                    </td>
                </tr>
            </table>
        </p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>