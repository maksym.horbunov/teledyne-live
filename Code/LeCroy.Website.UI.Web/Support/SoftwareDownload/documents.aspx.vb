﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Support_Documents
    Inherits BasePage

#Region "Variables/Keys"
    Private menuUrl As String = String.Empty
    Private menuUrl2 As String = String.Empty
    Private _documentXrefs As List(Of DocumentXRef) = New List(Of DocumentXRef)
    Private _childDocumentsViaXref As List(Of Document) = New List(Of Document)
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ProcessMenus()
        Dim subCategory As Subcategory = New Subcategory()
        Dim protocolStandardId As Nullable(Of Int32) = Nothing

        If Not (Request("sc") Is Nothing) Then
            Dim subCategoryId As Int32 = ParseSubCategoryQueryString()
            subCategory = GetSubCategory(subCategoryId)
            If (subCategory Is Nothing) Then
                Response.Redirect("~/")
            End If
            If (subCategory.CategoryId.Value = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
                lb_Title.Text = String.Format("Firmware Upgrade File for {0}", subCategory.SubCatName)
                Me.Title = String.Format("{0} - Firmware Upgrade File for {1}", ConfigurationManager.AppSettings("DefaultPageTitle"), subCategory.SubCatName)
            ElseIf (subCategory.CategoryId.Value = CategoryIds.OSCILLOSCOPES) Then
                lb_Title.Text = String.Format("Scope Firmware Upgrade File for {0}", subCategory.SubCatName)
                Me.Title = String.Format("{0} - Scope Firmware Upgrade File for {1}", ConfigurationManager.AppSettings("DefaultPageTitle"), subCategory.SubCatName)
            End If
        Else
            protocolStandardId = ParseProtocolStandardQueryString()
            If (protocolStandardId.HasValue) Then
                HandleProtocolAnalyzer(protocolStandardId.Value)
            Else
                HandleProtocolAnalyzerArchive()
            End If
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Analysis Software"
        End If

        If Not (IsPostBack) Then
            If Not (Request("sc") Is Nothing) Then
                HandleSeriesDrivenData(subCategory)
            Else
                If (protocolStandardId.HasValue) Then
                    HandleProtocolAnalyzer(protocolStandardId.Value)
                Else
                    HandleProtocolAnalyzerArchive()
                End If
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub ddlCompatibility_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCompatibility.SelectedIndexChanged
        rptDownloads.DataSource = Nothing
        rptDownloads.DataBind()
        divProductCompatibility.Visible = False
        If (String.IsNullOrEmpty(ddlCompatibility.SelectedValue)) Then
            Return
        End If
        Dim documents As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = FeDocumentManager.GetDocumentsForSubCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), ParseSubCategoryQueryString(), ParseProductSeriesQueryString())
        Dim partNumbersForSeries As Dictionary(Of Int32, Dictionary(Of Int32, String)) = CType(ViewState("SeriesToPartNumbers"), Dictionary(Of Int32, Dictionary(Of Int32, String)))
        If (partNumbersForSeries.Count > 0) Then
            If (partNumbersForSeries.ContainsKey(Int32.Parse(ddlCompatibility.SelectedValue))) Then
                BindProductCompatibilityDropDown(partNumbersForSeries(Int32.Parse(ddlCompatibility.SelectedValue)))
                divProductCompatibility.Visible = True
                If (documents.Count <= 0) Then Return
            End If
        End If
        If (documents.Count <= 0) Then
            Response.Redirect("default.aspx" + menuUrl2)
        End If

        Dim tempDictionary As Dictionary(Of Int32, List(Of Int32)) = CType(ViewState("SeriesToDocIds"), Dictionary(Of Int32, List(Of Int32)))
        Dim supportedDocuments As List(Of Int32) = tempDictionary(Int32.Parse(ddlCompatibility.SelectedValue.ToString()))
        documents = documents.Where(Function(x) supportedDocuments.Contains(x.DocumentId)).ToList()
        If (documents.Count > 0) Then
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptDownloads.DataSource = documents
            rptDownloads.DataBind()
        End If
    End Sub

    Private Sub ddlProductCompatibility_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProductCompatibility.SelectedIndexChanged
        If (String.IsNullOrEmpty(ddlProductCompatibility.SelectedValue)) Then
            ddlCompatibility_SelectedIndexChanged(Nothing, Nothing)
            Return
        End If
        Dim productsToSeriesIds As Dictionary(Of Int32, List(Of Int32)) = CType(ViewState("ProductToSeriesIds"), Dictionary(Of Int32, List(Of Int32)))
        Dim documents As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = FeDocumentManager.GetDocumentsForSubCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), ParseSubCategoryQueryString(), ParseProductSeriesQueryString())
        If (documents.Count <= 0) Then
            Response.Redirect("default.aspx" + menuUrl2)
        End If
        Dim docIds As List(Of Int32) = CType(ViewState("ProductDocIds"), Dictionary(Of Int32, List(Of Int32)))(Int32.Parse(ddlProductCompatibility.SelectedValue.ToString()))
        documents = documents.Where(Function(x) docIds.Contains(x.DocumentId)).ToList()
        If (documents.Count > 0) Then
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptDownloads.DataSource = documents
            rptDownloads.DataBind()
        End If
    End Sub

    Private Sub rptDownloads_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDownloads.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As LeCroy.Library.Domain.Common.DTOs.Document = CType(e.Item.DataItem, LeCroy.Library.Domain.Common.DTOs.Document)
                Dim litTitle As Literal = CType(e.Item.FindControl("litTitle"), Literal)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)
                Dim litVersion As Literal = CType(e.Item.FindControl("litVersion"), Literal)
                Dim litDocSize As Literal = CType(e.Item.FindControl("litDocSize"), Literal)
                Dim hlkDownload As HyperLink = CType(e.Item.FindControl("hlkDownload"), HyperLink)
                Dim litComments As Literal = CType(e.Item.FindControl("litComments"), Literal)
                Dim litPasswordProtected As Literal = CType(e.Item.FindControl("litPasswordProtected"), Literal)
                Dim trXrefs As HtmlTableRow = CType(e.Item.FindControl("trXrefs"), HtmlTableRow)
                Dim trComments As HtmlTableRow = CType(e.Item.FindControl("trComments"), HtmlTableRow)
                Dim rptXrefs As Repeater = CType(e.Item.FindControl("rptXrefs"), Repeater)

                litTitle.Text = row.Title
                litDescription.Text = IIf(String.IsNullOrEmpty(row.Description), String.Empty, String.Format("{0}<br /><br />", row.Description))
                litVersion.Text = row.Version
                litDocSize.Text = row.DocSize
                hlkDownload.NavigateUrl = String.Format("download.aspx?mseries={0}&did={1}", ParseSubCategoryQueryString(), row.DocumentId)
                If (_documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Count > 0) Then
                    trXrefs.Visible = True
                    AddHandler rptXrefs.ItemDataBound, AddressOf rptXrefs_ItemDataBound
                    Dim docFetch As List(Of Document) = (From c In _childDocumentsViaXref Where _documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).ToList()
                    If (docFetch.Count > 0) Then
                        rptXrefs.DataSource = HackFixSortIdsInCaseOfNulls(docFetch).OrderBy(Function(x) x.SortId)
                        rptXrefs.DataBind()
                    End If
                End If
                litComments.Text = row.Comments
                If (row.DocTypeId = 9) Then
                    litPasswordProtected.Text = row.PasswordYN
                End If
        End Select
    End Sub

    Private Sub rptXrefs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkXRef As HyperLink = CType(e.Item.FindControl("hlkXRef"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkXRef.NavigateUrl = redirectUrl
                    hlkXRef.Text = row.Title
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Function ParseSubCategoryQueryString() As Int32
        Dim retVal As Int32 = 0
        If Not (Request("sc") Is Nothing) Then
            If Not (Int32.TryParse(Request("sc").ToString(), retVal)) Then
                Response.Redirect("default.aspx" + menuUrl2)
            End If
        End If
        Return retVal
    End Function

    Private Function ParseProtocolStandardQueryString() As Nullable(Of Int32)
        Dim retVal As Int32 = 0
        If Not (Request("standardid") Is Nothing) Then
            If Not (Int32.TryParse(Request("standardid").ToString(), retVal)) Then
                Response.Redirect("default.aspx" + menuUrl2)
            Else
                Return retVal
            End If
        Else
            Return Nothing
        End If
    End Function

    Private Function ParseProductSeriesQueryString() As Nullable(Of Int32)
        Dim retVal As Int32 = 0
        If Not (Request("mseries") Is Nothing) Then
            If Not (Int32.TryParse(Request("mseries").ToString(), retVal)) Then
                Response.Redirect("default.aspx" + menuUrl2)
            Else
                Return retVal
            End If
        Else
            Return Nothing
        End If
    End Function

    Private Sub ProcessMenus()
        Dim subMenuID As String = String.Empty
        Dim captionID As String = String.Empty
        Dim menuID As String = String.Empty
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuUrl = "&capid=" + captionID
        menuUrl += "&mid=" + menuID
        menuUrl += "&smid=" + subMenuID
        menuUrl2 = "?capid=" + captionID
        menuUrl2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
        lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False
    End Sub

    Private Function GetSubCategory(ByVal subCategoryId As Int32) As Subcategory
        Dim subCategory As Subcategory = SubcategoryRepository.GetSubcategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), subCategoryId)
        If Not (subCategory Is Nothing) Then
            Return subCategory
        End If
        Return Nothing
    End Function

    Private Sub HandleSeriesDrivenData(ByVal subCategory As Subcategory)
        Dim seriesIdWasSet As Boolean = False
        Dim productSeriesId As Nullable(Of Int32) = ParseProductSeriesQueryString()
        Dim documents As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = FeDocumentManager.GetDocumentsForSubCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), subCategory.SubCatId, productSeriesId)
        If (documents.Count <= 0) Then
            Response.Redirect("default.aspx" + menuUrl2)
        End If

        _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
        If (_documentXrefs.Count > 0) Then
            _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
        End If

        Dim bindSeriesDropDown As Boolean = False
        Dim bindProductDropDown As Boolean = False
        Dim seriesIdsAndNames As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)    ' Storage for the series dropdown
        Dim seriesToDocIds As Dictionary(Of Int32, List(Of Int32)) = New Dictionary(Of Int32, List(Of Int32))   ' Storage for docs associated to a series
        Dim productIdsAndNames As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)   ' Storage for the product dropdown
        Dim productToSeriesIds As Dictionary(Of Int32, List(Of Int32)) = New Dictionary(Of Int32, List(Of Int32))
        Dim productDocIds As Dictionary(Of Int32, List(Of Int32)) = New Dictionary(Of Int32, List(Of Int32))
        Dim seriesToPartNumbers As Dictionary(Of Int32, Dictionary(Of Int32, String)) = New Dictionary(Of Int32, Dictionary(Of Int32, String))

        For Each document As LeCroy.Library.Domain.Common.DTOs.Document In documents
            Dim productSeries As List(Of ProductSeries) = New List(Of ProductSeries)
            If (productSeriesId.HasValue) Then
                seriesIdWasSet = True
                Dim ps As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId.Value)
                If Not (ps Is Nothing) Then
                    productSeries.Add(ps)
                Else
                    productSeries = FeProductManager.GetProductSeriesForDocumentAndSubCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocumentId, subCategory.CategoryId.Value, ParseSubCategoryQueryString())
                End If
            Else
                productSeries = FeProductManager.GetProductSeriesForDocumentAndSubCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocumentId, subCategory.CategoryId.Value, ParseSubCategoryQueryString())
            End If

            If (productSeries.Count <= 0) Then
                Continue For
            End If
            If Not (bindSeriesDropDown) Then bindSeriesDropDown = (productSeries.Count > 0)
            For Each ps As ProductSeries In productSeries
                If Not (seriesIdsAndNames.ContainsKey(ps.ProductSeriesId)) Then
                    seriesIdsAndNames.Add(ps.ProductSeriesId, ps.Name)
                End If
                If Not (seriesToDocIds.ContainsKey(ps.ProductSeriesId)) Then
                    seriesToDocIds.Add(ps.ProductSeriesId, New List(Of Int32)({document.DocumentId}))
                Else
                    Dim tempDocIds As List(Of Int32) = seriesToDocIds(ps.ProductSeriesId)
                    If Not (tempDocIds.Contains(document.DocumentId)) Then
                        tempDocIds.Add(document.DocumentId)
                    End If
                    seriesToDocIds(ps.ProductSeriesId) = tempDocIds
                End If

                Dim documentCount As Int32 = FeDocumentManager.GetDocumentCount(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocumentId, subCategory.CategoryId.Value, ParseSubCategoryQueryString(), ps.ProductSeriesId)
                Dim productSeriesCategoryCount As Int32 = FeProductManager.GetProductSeriesCategoryCount(ConfigurationManager.AppSettings("ConnectionString").ToString(), subCategory.CategoryId.Value, ps.ProductSeriesId)
                If Not (documentCount = productSeriesCategoryCount) Then
                    Dim products As List(Of Product) = FeProductManager.GetProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), subCategory.CategoryId.Value, ps.ProductSeriesId, document.DocumentId, ParseSubCategoryQueryString())
                    If (products.Count <= 0) Then
                        Continue For
                    End If
                    If Not (bindProductDropDown) Then bindProductDropDown = (products.Count > 0)
                    If Not (seriesToPartNumbers.ContainsKey(ps.ProductSeriesId)) Then
                        seriesToPartNumbers.Add(ps.ProductSeriesId, products.ToDictionary(Function(x) x.ProductId, Function(x) x.PartNumber))
                    Else
                        Dim tempSubDictionary As Dictionary(Of Int32, String) = seriesToPartNumbers(ps.ProductSeriesId)
                        For Each p As Product In products
                            If Not (tempSubDictionary.ContainsKey(p.ProductId)) Then
                                tempSubDictionary.Add(p.ProductId, p.PartNumber)
                            End If
                        Next
                    End If
                    For Each p As Product In products
                        If Not (productIdsAndNames.ContainsKey(p.ProductId)) Then
                            productIdsAndNames.Add(p.ProductId, p.PartNumber)
                        End If
                        If Not (productToSeriesIds.ContainsKey(p.ProductId)) Then
                            productToSeriesIds.Add(p.ProductId, New List(Of Int32)({ps.ProductSeriesId}))
                        Else
                            Dim tempList As List(Of Int32) = productToSeriesIds(p.ProductId)
                            If Not (tempList.Contains(p.ProductId)) Then
                                tempList.Add(p.ProductId)
                            End If
                            productToSeriesIds(p.ProductId) = tempList
                        End If
                        If Not (productDocIds.ContainsKey(p.ProductId)) Then
                            productDocIds.Add(p.ProductId, New List(Of Int32)({document.DocumentId}))
                        Else
                            Dim tempList As List(Of Int32) = productDocIds(p.ProductId)
                            If Not (tempList.Contains(document.DocumentId)) Then
                                tempList.Add(document.DocumentId)
                            End If
                            productDocIds(p.ProductId) = tempList
                        End If
                    Next
                End If
            Next
        Next

        If (bindSeriesDropDown) Then
            ddlCompatibility.DataSource = seriesIdsAndNames
            ddlCompatibility.DataValueField = "Key"
            ddlCompatibility.DataTextField = "Value"
            ddlCompatibility.DataBind()
            divSeriesCompatibility.Visible = True
            ViewState("SeriesToDocIds") = seriesToDocIds
            ViewState("SeriesToPartNumbers") = seriesToPartNumbers
            If (bindProductDropDown) Then
                divProductCompatibility.Visible = False
                ViewState("ProductToSeriesIds") = productToSeriesIds
                ViewState("ProductDocIds") = productDocIds
            End If
            If (seriesIdWasSet) Then
                ddlCompatibility.Items.Insert(0, New ListItem("Select...", String.Empty))
                ddlCompatibility.SelectedValue = productSeriesId.Value.ToString()
                ddlCompatibility_SelectedIndexChanged(Nothing, Nothing)
            Else
                If (seriesIdsAndNames.Count = 1) Then
                    ddlCompatibility_SelectedIndexChanged(Nothing, Nothing)
                    ddlCompatibility.Enabled = False
                Else
                    ddlCompatibility.Items.Insert(0, New ListItem("Select...", String.Empty))
                End If
            End If
        ElseIf (bindProductDropDown) Then
            If (productIdsAndNames.Count > 0) Then
                BindProductCompatibilityDropDown(productIdsAndNames)
                divProductCompatibility.Visible = True
            End If
            ViewState("ProductToSeriesIds") = productToSeriesIds
            ViewState("ProductDocIds") = productDocIds
            ddlProductCompatibility_SelectedIndexChanged(Nothing, Nothing)
        Else
            divSeriesCompatibility.Visible = False
            rptDownloads.DataSource = documents
            rptDownloads.DataBind()
        End If
    End Sub

    Private Sub BindProductCompatibilityDropDown(ByVal ds As Dictionary(Of Int32, String))
        ddlProductCompatibility.Items.Clear()
        ddlProductCompatibility.DataSource = ds
        ddlProductCompatibility.DataValueField = "Key"
        ddlProductCompatibility.DataTextField = "Value"
        ddlProductCompatibility.DataBind()
        ddlProductCompatibility.Items.Insert(0, New ListItem("Select...", String.Empty))
    End Sub

    Private Sub HandleProtocolAnalyzer(ByVal protocolStandardId As Int32)
        If (FeProtocolStandardManager.GetArchivedDocumentsForProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), protocolStandardId).Count > 0) Then
            lblArchiveLink.Text = String.Format("<ul><li class=""links""><a href='PSG_swarchive.aspx?standardid={0}{1}'><strong>Archived versions for {2}</strong></a></li></ul>", protocolStandardId.ToString(), menuUrl, ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), protocolStandardId).Name)
        End If
        Dim documents As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = FeProtocolStandardManager.GetDocumentsForProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), protocolStandardId)
        If (documents.Count > 0) Then
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
                documents = FilterOutChildDocumentsInDocumentCategory(documents, _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList())
            End If
            rptDownloads.DataSource = documents
            rptDownloads.DataBind()
        End If
    End Sub

    Private Function FilterOutChildDocumentsInDocumentCategory(ByVal documents As List(Of Document), ByVal childDocumentIds As List(Of Int32)) As List(Of Document)
        Dim retVal As List(Of Document) = New List(Of Document)
        For Each d As Document In documents
            If Not (childDocumentIds.Contains(d.DocumentId)) Then
                retVal.Add(d)
            End If
        Next
        Return retVal
    End Function

    Private Sub HandleProtocolAnalyzerArchive()
        Dim documents As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DOCUMENT.* FROM DOCUMENT INNER JOIN DOCUMENT_CATEGORY ON DOCUMENT.DOCUMENT_ID = DOCUMENT_CATEGORY.DOCUMENT_ID WHERE (DOCUMENT.DOC_TYPE_ID = 9) AND (DOCUMENT.POST = 'y') ORDER BY DOCUMENT.TITLE", New List(Of SqlParameter)().ToArray())
        If (documents.Count > 0) Then
            lb_Title.Text = "Analysis Software"
            lblArchiveLink.Text = "<ul><li class=""links""><a href='PSG_swarchive.aspx'><strong>Click here to go to archive</strong></a></li></ul>"
            _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
            If (_documentXrefs.Count > 0) Then
                _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
            End If
            rptDownloads.DataSource = documents
            rptDownloads.DataBind()
        End If
    End Sub

    Private Function HackFixSortIdsInCaseOfNulls(ByVal documents As List(Of Document)) As List(Of Document)
        If (documents.Count <= 0) Then Return documents
        Dim retVal As List(Of Document) = New List(Of Document)
        For Each document As Document In documents
            Dim matching As DocumentXRef = _documentXrefs.Where(Function(x) x.ChildDocumentFkId = document.DocumentId).FirstOrDefault()
            Dim doc As Document = New Document()
            doc.DocumentId = document.DocumentId
            doc.DocTypeId = document.DocTypeId
            doc.Title = document.Title
            If Not (matching Is Nothing) Then
                doc.SortId = matching.SortOrder
            Else
                If Not (document.SortId.HasValue) Then
                    doc.SortId = Int32.MaxValue
                Else
                    doc.SortId = document.SortId.Value
                End If
            End If
            retVal.Add(doc)
        Next
        Return retVal.OrderBy(Function(x) x.SortId.Value).ToList()
    End Function
#End Region
End Class