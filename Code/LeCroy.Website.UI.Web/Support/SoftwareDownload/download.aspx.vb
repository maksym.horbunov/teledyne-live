﻿Imports System.Net
Imports System.Net.Mail
Imports LeCroy.Library.AmazonServices
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Support_SoftwareDownload_download
    Inherits BasePage

#Region "Variables/Keys"
    Private menuURL As String = String.Empty
    Private menuURL2 As String = String.Empty
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Download Page"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.SOFTWARE_DOWNLOAD_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
        Initial()
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub Initial()
        Dim DocID As String = ""
        Dim mseries As String = ""
        Dim strCRet As String = ""
        Dim RequestID As String = ""
        Dim CampaignID As String = ""
        Dim strBody As String = ""
        Dim PSGdownloadID As String = ""
        Dim redirectURL As String = ""
        Dim EMail As String = ""
        Dim strCC As String = ""
        Dim strTo As String = ""
        Dim agreementAccepted As Boolean = False
        Dim fromAPI As Boolean = False
        Dim strSubject As String
        ValidateUserNoRedir()

        If Request.QueryString("did") Is Nothing Then
            DocID = Session("PSGdownloadID")
        Else
            DocID = Request.QueryString("did")
        End If
        If Not IsNumeric(DocID) Then
            Response.Redirect("default.aspx" + menuURL2)
        End If

        DocID = DocumentReplacementRepository.RetrieveYoungestDocumentReplacementChild(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(DocID))

        Session("PSGdownloadID") = DocID
        If Request.QueryString("accepted") Is Nothing Then
            agreementAccepted = False
        Else
            agreementAccepted = True
        End If

        If Request.QueryString("fromapi") Is Nothing Then
            fromAPI = False
        Else
            fromAPI = True
        End If

        '** mseries
        If Not Request.QueryString("mseries") Is Nothing And Len(Request.QueryString("mseries")) > 0 Then
            If IsNumeric(Request.QueryString("mseries")) Then
                mseries = Request.QueryString("mseries")
            End If
        End If

        redirectURL = rootDir & "/Support/SoftwareDownload/download.aspx?mseries=" & mseries & "&did=" & DocID & menuURL
        If (fromAPI) Then
            redirectURL += "&fromapi=y"
        End If

        Session("RedirectTo") = redirectURL

        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
        strCRet = Chr(13) & Chr(10)

        If Not Session("CampaignID") Is Nothing Then
            CampaignID = Session("CampaignID").ToString()
        End If

        If Not Session("PSGdownloadID") Is Nothing Then
            PSGdownloadID = Session("PSGdownloadID").ToString()
        End If

        If Not agreementAccepted And fromAPI Then
            Response.Redirect("APIagreement.aspx" + menuURL2)
        ElseIf Not agreementAccepted And Functions.CheckDownloadAgreement(Session("PSGdownloadID")) Then
            Response.Redirect("agreement.aspx" + menuURL2)
        End If

        Dim document As Document = DocumentRepository.GetDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(DocID))
        If (document Is Nothing) Then
            Response.Redirect("~/")
        End If
        If (String.Compare(document.Post, "n", True) = 0) Then
            Response.Redirect("~/doc/notavailable.aspx")
        End If

        Dim recordDocumentRequests As Boolean = False
        Boolean.TryParse(ConfigurationManager.AppSettings("RecordDocumentRequests"), recordDocumentRequests)
        If (recordDocumentRequests) Then RequestDocumentRepository.SaveRequestDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), BuildRequestDocumentObject(Int32.Parse(DocID)))

        If (String.Compare(document.PasswordYN, "y", True) = 0) Then
            If (document.DocumentId <= 9358) Then   ' TEMP: 9358 = USB Compliance Suite, last file that will use the password we set; going forward, the zip will be password protected.
                Response.Redirect(String.Format("~/support/softwaredownload/protectedfile.aspx?did={0}", DocID))
            End If
        End If

        If Not Session("ContactId") Is Nothing And Len(Session("ContactId")) > 0 And Len(DocID) > 0 Then
            RequestID = Functions.InsertDownloadByProductRequest(Session("ContactId"), 8, DocID, mseries, 0, "", "", "No Remarks for Download", "", 0, 0, "", CampaignID)
            If Len(Session("Country")) > 0 Then
                If CStr(Session("Country")) = "Japan" Then
                    strBody = strBody & "New Download: " & Functions.GetDOwnloadFilePath(DocID) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    strSubject = "New Download"
                Else
                    strBody = strBody & "New Download: " & Functions.getdownloaddescr(DocID) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    strSubject = "New Download - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
                End If
            Else
                strBody = strBody & "New Download: " & Functions.getdownloaddescr(DocID) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                strSubject = "New Download - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            End If

            If (document.DocTypeId = 13) Then   ' Check for PERT
                strCC = "Roy.Chestnut@teledynelecroy.com"
            End If
            If (document.DocTypeId = 15) Then   ' Check for LogicStudio
                strCC = "james.chan@teledyne.com"
            End If
            If (document.DocTypeId = 9) Then    ' Check for IOTA
                Dim subCategoryId As String = Functions.GetDownloadSubCategory(DocID)
                If Not (String.IsNullOrEmpty(subCategoryId)) Then
                    If (String.Compare(subCategoryId, "18", True) = 0) Then
                        strCC = "Roy.Chestnut@teledynelecroy.com"   ' As per Mickey Romm - 9/8/2015
                    End If
                End If
            End If

            strTo = Functions.GetSalesEmail(Session("Country").ToString, DocID, Session("state").ToString, Session("ContactID").ToString, GetFirstAvailableCategoryIdFromDocument(DocID), 8)   ' Assumes Protocol (catid) only for download (request)
            Dim toOverride As String = ReRouteDistributorProductsForUnitedStates(mseries, DocID)
            If Not (String.IsNullOrEmpty(toOverride)) Then
                strTo = toOverride
            End If

            If Len(strBody) > 0 Then
                Try
                    'Functions.SendEmail(Session("country").ToString, strBody, strTo, "webmaster@teledynelecroy.com", strCC, "", strSubject, "")
                    Me.SendEmail(Session("country").ToString, strBody, strTo, "webmaster@teledynelecroy.com", strCC, "", strSubject, "")
                Catch ex As Exception
                    Functions.ErrorHandlerLog(True, ex.Message)
                End Try
            End If

            ' INC0580869
            HandlePardot(DocID)
        End If
        Session("PSGdownloadID") = Nothing

        litFileName.Text = document.Title
        Dim url As String = String.Format("{0}{1}", ConfigurationManager.AppSettings("AwsPrivateBaseUrl"), document.FileFullPath.ToLower())
        Dim signedUrl As String = AmazonSignedUrl.GetCannedPolicySignedUrl(url, DateTime.UtcNow.AddSeconds(Convert.ToInt32(ConfigurationManager.AppSettings("AwsUtcExpirationRange"))), ConfigurationManager.AppSettings("AwsPemKeyFilePath"), ConfigurationManager.AppSettings("AwsKeyPairId"), False)
        Try
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine("    (function (i, s, o, g, r, a, m) {")
            sb.AppendLine("    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {")
            sb.AppendLine("    (i[r].q = i[r].q || []).push(arguments)")
            sb.AppendLine("    }, i[r].l = 1 * new Date(); a = s.createElement(o),")
            sb.AppendLine("    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)")
            sb.AppendLine("    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');")
            sb.AppendLine(String.Format("    setTimeout('PauseRedirect()', {0});", ConfigurationManager.AppSettings("GoogleAnalyticsTimeoutInterval").ToString()))
            sb.AppendLine("")
            sb.AppendLine("    ga('create', 'UA-13095488-1', 'auto');")
            sb.AppendLine(String.Format("    ga('send', 'pageview', '{0}');", document.FileFullPath.ToLower()))
            'sb.AppendLine("    ga('send', 'pageview');")
            sb.AppendLine("")
            sb.AppendLine("    function PauseRedirect() {")
            sb.AppendLine(String.Format("        window.location = '{0}';", signedUrl))
            sb.AppendLine("    }")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SWDownload", sb.ToString(), True)
        Catch ex As Exception
            Functions.ErrorHandlerLog(True, String.Format("SWDownload: {0} {1} {2}", ex.Message, ex.StackTrace, ex.Source))
            Response.Redirect(signedUrl)
        End Try
    End Sub

    Private Function ReRouteDistributorProductsForUnitedStates(ByVal subCategoryId As String, ByVal documentId As Int32) As String
        ' Override recipient, send notification email to DSM's for US if a distribution product
        If (Session("Country") Is Nothing) Then
            Return String.Empty
        End If
        If (String.Compare(Session("Country").ToString(), "United States", True) <> 0) Then
            Return String.Empty
        End If

        Dim distributorSeriesIdsFromConfig As String = ConfigurationManager.AppSettings("DistScopes")
        If (String.IsNullOrEmpty(distributorSeriesIdsFromConfig)) Then
            Return String.Empty
        End If
        Dim distributorSeriesIds As List(Of Int32) = New List(Of Int32)
        Dim splitString As String() = distributorSeriesIdsFromConfig.Split(",")
        For Each ss In splitString
            If Not (distributorSeriesIds.Contains(Int32.Parse(ss))) Then
                distributorSeriesIds.Add(Int32.Parse(ss))
            End If
        Next

        Dim successRetVal As String = "hilary.lustig@teledyne.com"
        'If (String.IsNullOrEmpty(subCategoryId)) Then
        If (IsDocumentContainedInDistributorProductSeriesListAndNoOtherSeriesList(documentId, distributorSeriesIds)) Then
            Return successRetVal
        End If
        'Return String.Empty
        'End If

        'Dim testVal As Int32 = -1
        'If Not (Int32.TryParse(subCategoryId, testVal)) Then
        '    Return String.Empty
        'End If

        'Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [PRODUCT_SERIES] WHERE [SUBCAT_ID] = @SUBCATEGORYID", New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@SUBCATEGORYID", subCategoryId.ToString())}).ToArray())
        'For Each ps As ProductSeries In productSeries
        '    If (distributorSeriesIds.Contains(ps.ProductSeriesId)) Then
        '        Return successRetVal
        '    End If
        'Next
        Return String.Empty
    End Function

    Private Function IsDocumentContainedInDistributorProductSeriesListAndNoOtherSeriesList(ByVal documentId As Int32, ByVal distributorSeriesIdsFromConfig As List(Of Int32)) As Boolean
        Dim productSeriesIds As List(Of Int32) = DocumentCategoryRepository.GetDocumentCategoriesForDocumentId(ConfigurationManager.AppSettings("ConnectionString").ToString(), documentId).Where(Function(x) x.ProductSeriesId.HasValue).Select(Function(x) x.ProductSeriesId.Value).Distinct().ToList()
        If (productSeriesIds.Count <= 0) Then
            Return False
        End If
        Dim containedInAllowedList As Boolean = False
        For Each configId In distributorSeriesIdsFromConfig
            If (productSeriesIds.Contains(configId)) Then
                containedInAllowedList = True
                Exit For
            End If
        Next
        If Not (containedInAllowedList) Then Return False
        For Each psId In productSeriesIds
            If Not (distributorSeriesIdsFromConfig.Contains(psId)) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub SendEmail(ByVal strCountry As String, ByVal strBody As String, ByVal strTo As String, ByVal strFrom As String, ByVal strCC As String, ByVal strDisplayName As String, ByVal strSubject As String, ByVal AttachmentFile As String)
        If (String.IsNullOrEmpty(strBody) Or String.IsNullOrEmpty(strTo)) Then
            Return
        End If

        'Dim toAddress As MailAddress = New MailAddress(strTo)
        Dim fromAddress As MailAddress = Nothing
        'If Not (String.IsNullOrEmpty(strFrom)) Then
        '    fromAddress = New MailAddress(strFrom, strDisplayName)
        'Else
        fromAddress = New MailAddress("webmaster@teledynelecroy.com", strDisplayName)
        'End If
        Dim mm As MailMessage = New MailMessage() '(fromAddress, toAddress)
        mm.To.Add(strTo)
        'If Not (String.IsNullOrEmpty(strCC)) Then mm.CC.Add(New MailAddress(strCC))
        If Not (String.IsNullOrEmpty(strCC)) Then mm.CC.Add(strCC) ' As per MSDN: Multiple e-mail addresses must be separated with a comma character
        mm.Bcc.Add(New MailAddress("kate.kaplan@teledyne.com"))
        'mm.Bcc.Add(New MailAddress("james.chan@teledyne.com"))
        mm.Subject = strSubject
        mm.Body = strBody
        mm.From = fromAddress
        If Not (String.IsNullOrEmpty(strCountry)) Then
            If CStr(strCountry) = "Japan" Then
                mm.BodyEncoding = Encoding.GetEncoding("shift-JIS")
                mm.SubjectEncoding = Encoding.GetEncoding("shift-JIS")
            ElseIf CStr(strCountry) = "China" Then
                mm.BodyEncoding = Encoding.GetEncoding("GB2312")
                mm.SubjectEncoding = Encoding.GetEncoding("GB2312")
            ElseIf Mid(CStr(strCountry), 1, 5) = "Korea" Then
                mm.BodyEncoding = Encoding.GetEncoding("korean")
                mm.SubjectEncoding = Encoding.GetEncoding("korean")
            End If
        End If

        Try
            Dim smtpClient As SmtpClient = New SmtpClient(ConfigurationManager.AppSettings("EmailHost"), ConfigurationManager.AppSettings("Port"))
            smtpClient.Send(mm)
        Catch ex As Exception
            Functions.ErrorHandlerLog(True, ex.Message + " " + ex.Source)
        End Try
    End Sub

    Private Function GetFirstAvailableCategoryIdFromDocument(ByVal documentId As Int32) As Int32
        Dim documentCategories As List(Of DocumentCategory) = DocumentCategoryRepository.GetDocumentCategoriesForDocumentId(ConfigurationManager.AppSettings("ConnectionString").ToString(), documentId).Where(Function(x) x.CategoryId.HasValue).ToList()
        If (documentCategories.Count > 0) Then
            documentCategories = documentCategories.OrderBy(Function(x) x.CategoryId.Value).ToList()
            Return documentCategories.Select(Function(x) x.CategoryId.Value).FirstOrDefault()
        End If
        Return LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
    End Function

    Private Function BuildRequestDocumentObject(ByVal documentId As Int32) As RequestDocument
        Dim contactFkId As Int32 = 0
        If Not (String.IsNullOrEmpty(Session("ContactID").ToString())) Then
            Int32.TryParse(Session("ContactID").ToString(), contactFkId)
        End If
        Dim retVal As RequestDocument = New RequestDocument()
        retVal.DocumentFkId = documentId
        retVal.SessionFkId = Session.SessionID.ToString()
        retVal.ContactFkId = contactFkId
        retVal.IpAddress = Request.UserHostAddress
        retVal.GeoIpCountry = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        Return retVal
    End Function

    Private Sub HandlePardot(ByVal docID As String)
        Dim url As String = String.Empty
        Dim response As String = String.Empty
        Dim catID As String
        Dim catName As String
        Dim downloadTitle As String = ""

        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            catID = GetFirstAvailableCategoryIdFromDocument(docID)
            If (String.Compare(catID, LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES) = 0) Then
                catName = "Oscilloscope"
            ElseIf (String.Compare(catID, LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS) = 0) Then
                catName = "Protocol"
            Else
                ' Only interested in downloads for Oscilloscopes and Protocol Analyzers at this time
                Return
            End If

            downloadTitle = GetDocumentTitle(docID)

            Dim nvc As New NameValueCollection
            ' Add existing data
            nvc.Add("cfname", Session("FirstName"))
            nvc.Add("clname", Session("LastName"))
            nvc.Add("cphone", Session("Phone"))
            Dim country As String = Session("Country")
            ' Pardot country values differ from the web catalog site
            nvc.Add("ccountry", CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), country).PardotCountry)
            nvc.Add("caddress", Session("Address"))
            nvc.Add("caddress2", Session("Address2"))
            nvc.Add("ccity", Session("City"))
            ' Only supply State/Province for US/Canada
            If (String.Compare(Session("Country"), "United States", True) = 0 Or String.Compare(Session("Country"), "Canada", True) = 0) Then
                nvc.Add("cstate", Session("State"))
            Else
                nvc.Add("cstate", String.Empty)
            End If
            nvc.Add("czip", Session("Zip"))
            nvc.Add("ccompany", Session("Company"))
            nvc.Add("cemail", Session("Email"))
            nvc.Add("comments", "Software downloaded from TeledyneLeCroy.com - " & downloadTitle)
            nvc.Add("leadbu", catName)

            Dim postData As String = String.Empty
            For Each k As String In nvc.Keys
                If (postData.Length > 0) Then
                    postData &= "&"
                End If
                postData &= k & "=" & nvc(k)
            Next
            Dim data As Byte() = Encoding.UTF8.GetBytes(postData)

            url = ConfigurationManager.AppSettings("PardotSoftwareDownload").ToString()
            Dim httpRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpRequest.ContentType = "application/x-www-form-urlencoded"
            httpRequest.Method = "POST"

            httpRequest.ContentLength = data.Length
            Using s As IO.Stream = httpRequest.GetRequestStream()
                s.Write(data, 0, data.Length)
            End Using

            Using httpResponse As HttpWebResponse = CType(httpRequest.GetResponse(), HttpWebResponse)
                response = New IO.StreamReader(httpResponse.GetResponseStream()).ReadToEnd()
            End Using
        Catch ex As Exception
            Dim exM As String = String.Format("{0} -- {1} -- {2} -- {3}", ex.Message, ex.InnerException, ex.StackTrace, ex.Source)
            Dim toAddresses As List(Of String) = New List(Of String)
            toAddresses.Add("james.chan@teledyne.com")
            Utilities.SendEmail(toAddresses, "webmaster@teledynelecroy.com", "Exception in quote request async call", exM + " -------URL: " + url + " -------Response: " + response, New List(Of String), New List(Of String))
        End Try
    End Sub

    Private Function GetDocumentTitle(docID As String) As String
        Dim selectedDoc As Document
        selectedDoc = DocumentRepository.GetDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), docID)
        If (selectedDoc Is Nothing) Then
            Return String.Empty
        End If

        Return selectedDoc.Title
    End Function
#End Region
End Class
