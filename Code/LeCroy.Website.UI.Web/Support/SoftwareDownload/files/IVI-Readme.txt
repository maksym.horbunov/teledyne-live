			***********************
			**** Read Me First ****
			***********************

Version 3.1.0.0
Aug 17, 2009


Introducing the LeCroy IVI-COM/IVI-C Driver for LeCroy oscilloscopes
---------------------------------------------------------------------------
  The instrument driver provides access to the functionality of the
  LeCroyScope through a COM server which also complies
  with the IVI specifications.  This driver works in any development environment which
  supports COM programming including Microsoft� Visual Basic, Microsoft
  Visual C++, Microsoft .NET, Agilent VEE Pro, National Instruments LabVIEW, and others.
  The driver also works in IVI-C environements such as National Instruments LabWindows.  
 

Supported Instruments
---------------------
All Windows-based instruments in the following families:
WaveRunner 6K and WaveRunner Xi series
WavePro 7K and 700Zi series
WaveSurfer 400 and WaveSurfer Xs series
WaveMaster
HornetRunner
HornetSurfer

Installation
-------------
  System Requirements: The driver installation will check for the
  following requirements.  If not found, the installer will either
  abort, warn, or install the required component as appropriate.

  Supported Operating Systems:
    Windows 2000
    Windows XP
    Windows Vista

  VICP Passport
    In order to use the LeCroyScope IVI driver with a TCP/IP connection, you must install the 
    LeCroy VICP Passport for NI-VISA.  You can download this component from the LeCroy website: https://teledynelecroy.com.
    If you are communicating with the instrument via GPIB, installation of the LeCroy passport is not needed.
    
  Shared Components
    Before this driver can be installed, your computer must already
    have several components already installed. The easiest way to
    to install these other components is by running the IVI Shared
    Components installation package available from the IVI Foundation
    as noted below. It installs all the items mentioned below.

    IVI Shared Components version 2.0.0.0 or later.
      The driver installer requires these components. They must be
      installed before the driver installer can install the driver.  

      The IVI Shared Components installer is available from: 
      http://www.ivifoundation.org/shared_components

      To install, open or run the file: IviSharedComponents.msi

      If the driver installer exhibits problems with installing the IVI
      Shared Components, find the program IviSharedComponents.msi and
      Run the program. It is able to install many of the required system
      components, including Windows Installer 2.0.
  
  VISA
    Any compliant implementation is acceptable for GPIB connections.  For using the instrument
    over a TCP/IP connection, only NI-VISA is supported and the LeCroy VICP passport for NI-VISA 
    must also be installed.  You can download the LeCroy VICP passport for NI-VISA at https://teledynelecroy.com.
    

Additional Setup
----------------
  .NET Framework
    The .NET Framework itself is not required by this driver. If you
    plan to use the driver with .NET, Service Pack 2 is required.

  The .NET Framework requires an interop assembly for a COM
  server. A Primary Interop Assembly, along with an XML file for
  IntelliSense is installed with the driver. The driver's PIA, along
  with IVI PIAs are installed, by default, in:
  <drive>:\Program Files\IVI Foundation\IVI\Bin\Primary Interop Assemblies

  The PIA is also installed into the Global Assembly Cache (GAC) if
  you have the .NET framework installed.


Start Menu Shortcuts 
--------------------
   A shortcut to the driver help file is added to the Start Menu,
   Programs, LeCroyScope group.  It contains "Getting
   Started" information on using the driver in a variety of programming
   environments as well as documentation on IVI and instrument specific
   methods and properties.

   You will also see shortcuts to the Readme file and any programming
   examples for this driver.


Help File
---------
  The help file (LeCroyScope.chm) is located in the directory:
   <drive>:\Program Files\IVI Foundation\IVI\Drivers\LeCroyScope

 
MSI Installer
-------------
   The installation package for the driver is distributed as an MSI 2.0
 file. You can install the driver by double-clicking on the file.
 This operation actually runs:
      msiexec /i LeCroyScope.msi

   You can run msiexec from a command prompt and utilize its many
   command line options. There are no public properties which can be
   set from the command line.


Uninstall
---------
  This driver can be uninstalled like any other software from the
  Control Panel using "Add or Remove Programs". 

  To uninstall the IVI Shared Components you must use the IVI Cleanup
  Utility available from the IVI Foundation at:
   http://www.ivifoundation.org/

  Note: All IVI-COM drivers require the IVI Shared Components to
  function. To completely remove IVI components from your computer,
  uninstall all drivers then run the IVI Cleanup Utility. This utility
  does not remove any IVI drivers.


Revision History
----------------
  Version     Date         Notes
  -------   ------------   -----
  0.1.0.0   Dec 19, 2007  Internal release
  0.2.0.0   Jan 20, 2008  Internal release
  1.0.0.0   Feb 03, 2008  First release candidate
  1.0.1.0   Feb 21, 2008  Fixed bug related to simulation mode
                          Changed Channel.Enabled property so that it makes a disabled channel not displayed
  3.0.0.0   Mar 03, 2008  First public beta release
  3.0.1.0   Apr 01, 2008  Minor updates
  3.0.2.0   May 20, 2008  Fixed bug related to max sample rate being divided by the number of active channels
  3.0.3.0   May 27, 2008  Changed Read/FetchWaveformMeasurement functions to return a warning instead of an error
  3.0.4.0   May 28, 2008  Updated Read/FetchWaveformMeasurement to use High/Low/Mid levels for Width, DutyCycle, Frequency, and Period measurements
                          Updated NumberOfPointsMin attribute to not coerce to lock steps
  3.0.5.0   Jul 01, 2008  Added HornetRunner and HornetSurfer as recognized instrument families
                          Added support for math traces, memory traces, and zoom traces to class-compliant implementation
                          Removed instr-specific functions ReadMathWaveform and FetchMathWaveform (now redundant w/ class-compliant)
  3.0.6.0   Jul 02, 2008  Made supported couplings per-channel instead of global
  3.0.7.0   Jul 07, 2008  Fixed bug related to performing measurements on different channels (app.Measure.P1.Source1 set incorrectly)
  3.0.8.0   Oct 01, 2008  Added support for Falcon series of scopes
                          Fixed bug related to vertical offset
                          Added support for AC, LFReject and HFReject trigger coupling
  3.0.9.0   Oct 15, 2008  Fixed bug related to simulation
  3.1.0.0   Aug 17, 2009  Added support for Hawk series of scopes
                          Fixed trigger coupling bug
                          Fixed bug where channel coupling was not getting properly set
                          Fixed trigger slope bug
  
IVI Compliance
--------------
(The following information is required by IVI 3.1 section 5.21.)

IVI-COM/IVI-C IviScope Specific Instrument Driver
IVI Instrument Class: IVI-4.1_IviScope_v3.0
Group Capabilities:                    Supported:
IviScopeAcLineTrigger                     yes
IviScopeAutoSetup                         yes
IviScopeAverageAcquisition                yes
IviScopeContinuousAcquisition             yes
IviScopeGlitchTrigger                     yes
IviScopeInterpolation                     yes
IviScopeMinMaxWaveform                    no
IviScopeProbeSenseAuto                    yes
IviScopeRuntTrigger                       yes
IviScopeTriggerModifier                   yes
IviScopeTVTrigger                         yes
IviScopeSampleMode                        yes
IviScopeWaveformMeas                      yes
IviScopeWidthTrigger                      yes

Optional Features: This driver does not support Interchangeability
Checking, State Caching, or Coercion Recording.

Driver Identification: 
(These three strings are values of properties in the IIviIdentity
interface.)
Vendor: LeCroy. 
Description: IVI-COM/IVI-C driver for Windows-based LeCroy oscilloscopes.
Revision: 3.1.0.0

Component Identifier: LeCroyScope.LeCroyScope
(The component identifier can be used to create an instance of the COM
server.) 

