﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_home" Codebehind="home.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="100%" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" height="38" width="500">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"> </p>
                    <p><h2>Software Utilities</h2></p>
                    <p align="left"><b>Note:&nbsp;<br><br>You may be redirected to a short form if you are not a registered user.</b></p>
                  
                    <table border="0" cellpadding="4" width="500" style="border: 1 solid #1671CC" cellspacing="0">
                        <tr>
                            <td height="19" bgcolor="#1671CC" valign="top" align="left">
                                <b><font size="2" color="#FFFFFF">Download</font></b>
                            </td>
                            <td height="19" bgcolor="#1671CC" valign="top" align="left">
                                <b><font size="2" color="#FFFFFF">Version</font></b>
                            </td>
                            <td height="19" bgcolor="#1671CC" valign="top" align="left">
                                <b><nobr><font size="2" color="#FFFFFF">Help File</font></nobr></b>
                            </td>
                            <td height="19" bgcolor="#1671CC" valign="top" align="left">
                                <font color="#FFFFFF">&nbsp;<b><font size="2">Download</font></b></font>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><span style="color:#0077c0; font-size:1.5rem; font-weight:bold">MAUI Studio</span></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="/support/softwaredownload/mauistudio.aspx"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>Capture the power of Teledyne LeCroy MAUI&trade; oscilloscope software.<br />Work remotely from your oscilloscope and be more productive. </p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><p><img src="Images/QualiPHY_logo.jpg" alt="QualiPHY" width="50" height="50" />&nbsp;<b>QualiPHY</b></p></td>
                            <td height="19" align="center">9.3.0.3</td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="qphy.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>QualiPHY is designed to reduce the time and effort needed to perform compliance testing on a wide array of high-speed serial buses. QualiPHY is the most intuitive and efficient tool available for performing serial data compliance testing.<br /><a href="/serialdata/serialdatastandard.aspx?standardid=240&capid=105&mid=520">QualiPHY Datasheet</a></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><img border="0" src="/images/logos/wavestudiologo.png"></td>
                            <td height="19" align="center">9.0.0.1</td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="wavestudio.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>WaveStudio is a FREE PC based connectivity tool that interfaces a Teledyne LeCroy Digital Oscilloscope to a Windows XP, Vista, 7, or 10 operating system, with support for 32 and 64 bits.</p>
                                    <p>WaveStudio is a fast and easy way to analyze acquired waveforms offline, or remotely control an oscilloscope from your desktop.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><img border="0" src="images/scopexplorerlogo.gif" width="151" height="29"></td>
                            <td height="19" align="center">2.25</td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="scopeexplorer.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>ScopeExplorer is FREE PC based connectivity tool that interfaces a Teledyne LeCroy Digital Oscilloscope to the Windows 2000/XP desktop.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>LabNotebook Windows Plugin</b></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="/support/softwaredownload/download.aspx?did=10222"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>This installer will enable a windows plugin which can be run on any windows PC or oscillscope to enable a easy preview and quick extraction of contents from a LabNotebook (.lnb) file.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                                          <tr>
                            <td height="19"><b>XStreamBrowser</b></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="/support/softwaredownload/download.aspx?did=12290"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>The XStreamBrowser utility enables you to view, copy, and modify the COM object hierarchy of a Windows-based MAUI™ oscilloscope from a remote PC. It is essential for writing Automation programs, as it always shows all the Automation objects on the instrument at the exact current configuration—including those objects belonging to software options. It can also be used as a remote control console, enabling you to directly alter the configuration of Automation control variables. </p>
                                    <p>XStreamBrowser is compatible with 32- and 64-bit Windows PCs. Do NOT install this version if you have already installed the XStreamDSO 64-bit firmware on the same remote PC.</p>
                                    <p>Note: WaveSurfer 3000 users should instead install the <a href="/support/softwaredownload/wavestudio.aspx">WaveStudio(TM)</a> software. The COM object hierarchy of a connected device is shown in the WaveStudio Automation Browser.</p>
                                </blockquote>
                            </td>
                        </tr>
                         <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>EasyScopeX</b></td>
                            <td height="19" align="center"><font size="1">1.01.02.01.20</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="easyscopex.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>The EasyScopeX software utility is provided to connect a Teledyne Test Tool T3DSO1000/1000A and T3DSO2000 oscilloscope to a PC. The EasyScopeX application lets you view waveforms, waveform data, waveform measurements, and bitmap images of the T3DSO1000/1000A and T3DSO2000 display.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>EasyScope</b></td>
                            <td height="19" align="center"><font size="1">3.01.03.01.09</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="easyscope.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>The EasyScope software utility is provided to connect a WaveAce oscilloscope to a PC. The EasyScope application lets you view waveforms, waveform data, waveform measurements, and bitmap images of the WaveAce display.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b> Software Utilities for Windows 10 DCOM Setup</b></td>
                            <td height="19" align="center">&nbsp;</td>
                            <td height="19" align="center">&nbsp;</td>
                            <td height="19" align="center"><p align="center"><a href="<%=rootDir %>/doc/win10dcom-setup-scopes"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>This .zip file contains instructions and scripts required to make a DCOM connection from a remote PC to a Teledyne LeCroy Windows 10 oscilloscope.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>S-Parameter Viewer</b></td>
                            <td height="19" align="center"><font size="1">2.0</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="sparamviewer.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>SParamViewer is a free tool for plotting S-Parameters.  <a href="sparamviewer.aspx<%= menuURL2 %>">Learn More about SParamViewer &raquo;</a></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>LSIB</b></td>
                            <td height="19" align="center"><font size="1">6.0.0</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="lsib.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>LSIB (LeCroy Serial Interface Bus) is a new standard for high-speed data transfer from the oscilloscope with speeds up to 325 MB/s. Teledyne LeCroy&#8217;s exclusive LSIB solution is based on the wired PCI Express standard that uses a x4 (4 lanes) bus for remote data transfer.<br /><a href="/files/misc/lecroy_lsib_datasheet.pdf">LSIB Datasheet</a></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>USB2-GPIB</b></td>
                            <td height="19" align="center"><font size="1"></font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><p align="center"><a href="/support/softwaredownload/download.aspx?did=8138"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="26">
                                <blockquote>
                                    <p>Firmware download for the <a href="/options/productdetails.aspx?modelid=6247&categoryid=11&groupid=26&capid=102&mid=505">USB2-GPIB</a> product.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>ActiveDSO</b></td>
                            <td height="19" align="center">2.36</td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="activedso.aspx<%= menuURL2 %>"><h2>Download</h2></a></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="69">
                                <blockquote>
                                    <p><strong>ActiveDSO</strong> is an <b>ActiveX<sup>TM</sup></b> control that enables Teledyne LeCroy oscilloscopes and LSA-1000 series embedded signal analyzers to be <b>controlled by </b>and <b>exchange data</b> <b>with</b> a variety of Windows applications that support the ActiveX standard.&nbsp; MS Office programs, Internet Explorer, Visual Basic, Visual C++, Visual Java, and Matlab (v5.3) are a few of the many applications that support ActiveX controls.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>DigTraceUtility</b></td>
                            <td height="19" align="center"><font size="1">1.2</font></td>
                            <td height="19" align="center"><span>&nbsp;</span></td>
                            <td height="19" align="center"><a href="digtrace.aspx<%= menuURL2 %>"><h2>Download</h2></a></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="69">
                                <blockquote>
                                    <p>DigTraceUtility is an application that reads in digital waveform files (XMLDig extension) acquired with the MS-250, MS-500 and MS-32 mixed-signal options, and allows users to view, zoom and save the waveform and bus values using several text formats. The application also includes 2 cursors for making timing measurements</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr>
                            <td height="19"><b>EyeTraceUtility</b></td>
                            <td height="19" align="center"><font size="1">1.0</font></td>
                            <td height="19" align="center"><span>&nbsp;</span></td>
                            <td height="19" align="center"><a href="eyetrace.aspx<%= menuURL2 %>"><h2>Download</h2></a></td>
                        </tr>
                        <tr>
                            <td colspan="4" height="69">
                                <blockquote>
                                    <p>EyeTraceUtility is an application that reads in eye diagram waveform files (.XMLPer extension) and allows users to view, zoom and save the waveform in a text format. A script for reading the text output into MATLAB is included.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>LabVIEW&trade;, LabWindows&trade;, and IVI Drivers</b></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="labview.aspx<%= menuURL2 %>"><h2>Download</h2></a></td>
                        </tr>
                        <tr>
                            <td height="16" colspan="4">
                                <blockquote>
                                    <p>LabVIEW, LabWindows/CVI, and IVI Drivers for Teledyne LeCroy Digital Oscilloscopes and Digitizers.</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr>
                            <td height="19"><b>Teledyne LeCroy VICP Passport</b></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="vicppassport.aspx<%= menuURL2 %>"><h2>Download</h2></a></td>
                        </tr>
                        <tr>
                            <td height="16" colspan="4">
                                <blockquote>
                                    <p>The Teledyne LeCroy VICP Passport is a plug-in passport driver for National Intruments' VISA driver (Windows version only).</p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>WaveJet 300A USB Driver</b></td>
                            <td height="19" align="center"><font size="1">1.0</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="download.aspx?did=5078"><h2>Download</h2></a></td> 
                        </tr>
                        <tr>
                            <td height="19" colspan="4">
                                <blockquote>
                                    <p><font size="1"><br><span>The USB interface is standard on each WaveJet 300A oscilloscope.  Note that the front panel USB port (located next to the power button) is for use with USB memory devices for saving waveforms, screen images and setup files.  The rear panel USB port is the remote control interface.  The WaveJet can be connected to a computer with USB A/B cable.  Use of the WaveJet 300A USB hardware requires the installation of a USB driver on the computer.  The WaveJet 300A USB drivers are included on the CD shipped with the WaveJet and are also available for download from the Teledyne LeCroy website.</span></font></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>WaveJet Touch USB Driver</b></td>
                            <td height="19" align="center"><font size="1">1.0</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="download.aspx?did=8531"><h2>Download</h2></a></td> 
                        </tr>
                        <tr>
                            <td height="19" colspan="4">
                                <blockquote>
                                    <p><font size="1"><br><span>The USB interface is standard the WaveJet Touch oscilloscope. Note that the front panel USB port (located next to the power button) is for use with USB memory devices for saving waveforms, screen images, and setup files. The rear panel USB port is the remote control interface. The WaveJet Touch can be connected to a computer with USB A/B cable. Use of the WaveJet Touch USB hardware requires the installation of a USB driver on the computer. The WaveJet Touch are available for download from the Teledyne LeCroy website.</span></font></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>DSO Network Print Gateway</b></td>
                            <td height="19" align="center"><font size="1">1.0</font></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="dsoprintgateway.aspx<%= menuURL2 %>"><h2>Download</h2></a></td> 
                        </tr>
                        <tr>
                            <td height="19" colspan="4">
                                <blockquote>
                                    <p><font size="1"><br><span>The Teledyne LeCroy DSO Network Print Gateway is a software package that facilitates printing to networked printers from an Ethernet-equipped DSO.</span></font></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b>DSOFilter</b></td>
                            <td height="19" align="center">1.0</td>
                            <td height="19" align="center"><a href="/files/misc/dsofilter.hlp">Help File</a></td>
                            <td height="19" align="center"><p align="center"><a href="dsofilter.aspx<%= menuURL2 %>"><h2>Download</h2></a></p></td>
                        </tr>
                        <tr>
                            <td height="19" colspan="4">
                                <blockquote>
                                    <p><br><span>In addition to the seven popular filter types provided with DFP, custom designed filters can be also created and used. The DSOFilter which is an ActiveX control was designed to help to load these filter coefficients into Teledyne LeCroy DSOs.</span></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="1" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><b><img border="0" src="images/maskmakerlogo.gif" width="200" height="55"></b></td>
                            <td height="19" align="center">1.3</td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center">
                            <a href="maskmaker.aspx<%= menuURL2 %>"><h2>Download</h2></a>
                        </tr>
                        <tr>
                            <td height="19" colspan="4">
                                <blockquote>
                                    <p>
                                        MaskMaker is a FREE PC based graphic utility for creating and editing Masks. These Masks are to be later used in Teledyne LeCroy oscilloscopes.<br><br>
                                        <b>New with version 1.3</b><br>
                                        <b>Installer works with Windows 2000 through Windows 10</b>
                                    </p>
                                    <p>
                                        <b>New with version 1.2</b><br>
                                        <b>Colored Circles</b><br>
                                        Colored Circles will highlight points of interest such as failure points. Now you can select the circles color, maximum number and if they will be displayed in or out of the mask.
                                    </p>
                                    <p>
                                        <b>XY Masks</b><br>
                                        Now you can create XY masks for applications such as power measurements.<br></p>
                                </blockquote>
                            </td>
                        </tr>
                        <tr><td height="19" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                        <tr>
                            <td height="19"><img border="0" src="/images/mw_web.gif" width="200" height="55"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"></td>
                            <td height="19" align="center"><a href="matlab.aspx<%= menuURL2 %>"><h2>Download</h2></a>
                        </tr>
                        <tr><td height="19" colspan="4"><blockquote><p>Matlab runtime components are necessary for some QualiPHY scripts.</p></blockquote></td></tr>
                        <tr><td height="19" colspan="4"><hr noshade color="#C0C0C0" size="1"></td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>