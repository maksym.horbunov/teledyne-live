﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_Matlab" CodeBehind="matlab.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left">
                        <img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br>
                        <p align="left"><h2>Matlab Software Downloads</h2><br />
                            <p align="left"><h2>Matlab Compiler Runtime Installer 7.7</h2><a href="download.aspx?did=4886">Download</a></p>
                            <p align="left"><h2>Documentation:</h2><a href="/doc/docview.aspx?id=8360">Installing Matlab Compiler Runtime</a></p>
                        </p>
                    </p>
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    &nbsp;
</asp:Content>