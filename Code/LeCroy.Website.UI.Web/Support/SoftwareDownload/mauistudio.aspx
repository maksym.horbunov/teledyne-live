﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="mauistudio.aspx.vb" Inherits="LeCroy.Website.Support_SoftwareDownload_mauistudio" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">        
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><span style="color:#0077c0; font-size:1.5rem; font-weight:bold">MAUI Studio Software Download</span></p>
                    <p align="left">Capture the power of Teledyne LeCroy MAUI&trade; oscilloscope software.<br />Work remotely from your oscilloscope and be more productive.</p><br />
                    <p align="left"><a href="/doc/maui-studio-install-instructions"><h2>MAUI Studio Installation Instructions</a></h2></p><br />
                    <p align="left">
                        <a href="/doc/maui-studio-software"><h2>Download MAUI Studio software</h2></a>
                    </p><br />
                     
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>