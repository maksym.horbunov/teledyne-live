﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SoftwareDownload_other_scopes" Codebehind="other_scopes.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><b><span style="font-size: 16pt; color: navy;">Firmware Upgrade Path for Obsolete Teledyne LeCroy Oscilloscopes</span></b></p>
        <p><b><u><span style="color: navy;">Flash Upgrade Units:</span></u></b></p>
        <p><span>The following scopes can be upgraded to these versions with emailed files through the floppy drive or Teledyne LeCroy Scope Explorer via GPIB/TCPIP. Press the <b>Show Status</b> or <b>Scope Status</b> button, and then select <b>System</b> to view your model number and current firmware version (sometimes called <b>Soft Version</b>)</span></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><b><u><span style="color: navy;">Model</span></u></b></td>
                <td><b><u><span style="color: navy;">File Name</span></u></b></td>
                <td><b><u><span style="color: navy;">Version</span></u></b></td>
            </tr>
            <tr>
                <td>
                    <span>WP9XX / DDA260 / J260</span>
                </td>
                <td>
                    vxfusion
                </td>
                <td>
                    9.3.0
                </td>
            </tr>
            <tr>
                <td>
                    <span>WR2(LT262,264,372,354,374,584)</span>
                </td>
                <td>
                    LT37X
                </td>
                <td>
                    9.3.0
                </td>
            </tr>
            <tr>
                <td>
                    <span>WR1(LT224,322,342,344,364)</span>
                </td>
                <td>
                    LTXXX
                </td>
                <td>
                    9.3.0
                </td>
            </tr>
            <tr>
                <td>
                    <span>LC684 LC584, LC564 or DDA-110, 120, 125</span>
                </td>
                <td>
                    LC58X
                </td>
                <td>
                    9.3.0
                </td>
            </tr>
            <tr>
                <td>
                    <span>LC334,&nbsp; LC334A,&nbsp; LC534,&nbsp; LC534A,&nbsp; LC574A&nbsp; LC374A</span>
                </td>
                <td>
                    LCXXX
                </td>
                <td>
                    9.3.0
                </td>
            </tr>
            <tr>
                <td>
                    <span>93XXC, 935xA,TM * , 937x, 9384</span>
                </td>
                <td>
                    93XX3
                </td>
                <td>
                    8.2.2
                </td>
            </tr>
            <tr>
                <td>
                    <span>* Some 935xA&rsquo;s have (68030) CPU3 processor with EPROM&rsquo;s</span>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    8.1.0
                </td>
            </tr>
        </table>
        <p><span>These units can be upgraded automatically using the application Scope Explorer either automatically if <a href="scopeexplorer.aspx">ScopeExplorer</a> can access the Internet from your facility, or utilizing a local firmware file.?Upgrades can also be executed using floppy disks. Contact your Teledyne LeCroy Customer Care facility for further upgrade instructions.</span></p>
        <p><span>If firmware does not load, open cover and look on processor for an EPROM chip.These units are not field upgradeable.</span></p>
        <p><b><u><span style="color: navy;">EPROM units: Not Field Upgradeable</span></u></b></p>
        <p><span>93XX and LCXXX scopes at version 7.3.0 or lower are not field upgradeable due to loader incompatible. LCXXX scopes can upgraded at Teledyne LeCroy Service facilities; 93xx scopes are no longer supported.</span></p>
        <p><b><u><span style="color: navy;">Limited to version 7.2.2 due to their 68020 processor:</span></u></b></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <span>935x* non A?(68020)</span>
                </td>
                <td>
                    <span>7.2.2</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>9304A* and non A ** (68020)</span>
                </td>
                <td>
                    <span>7.2.2</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>931xA* and non A ** (68020)</span>
                </td>
                <td>
                    <span>7.2.2</span>
                </td>
            </tr>
            <tr>
                <td>
                    <p><b><u><span style="color: navy;">93xx Specialty Scopes are fixed at the following versions:</span></u></b></p>
                    <span>9324</span>
                </td>
                <td>
                    <span>6.4.7</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>9360, 9362</span>
                </td>
                <td>
                    <span>7.5.1 (68030) some units have EPROM some Flash. Need to check processor board.</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>9361</span>
                </td>
                <td>
                    <span>7.2.2 (68030) some units have EPROM some Flash. Need to check processor board.</span>
                </td>
            </tr>
        </table>
        <p><b><u><span style="color: navy;">Other Obsolete Units with fixed versions:</span></u></b></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <span>94XX</span>
                </td>
                <td>
                    <span>5.2</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>9400, A</span>
                </td>
                <td>
                    <span>2.0.6 (2.0.8 with FFT option.)</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>7200</span>
                </td>
                <td>
                    <span>1.5.0</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>7200A</span>
                </td>
                <td>
                    <span>3.3.1</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>LS140</span>
                </td>
                <td>
                    <span>2.1.1</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span>9210</span>
                </td>
                <td>
                    <span>3.8</span>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>