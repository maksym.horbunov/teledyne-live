﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="protectedfile.aspx.vb" Inherits="LeCroy.Website.Support_SoftwareDownload_protectedfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="padding-bottom: 12px; padding-left: 30px; padding-top: 20px;">
        Please enter the information below as provided by a Teledyne LeCroy representative.<br />
        <b>Note: that the user name and password below are not the same as your website profile user name and password.</b>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
			<td align="right" valign="middle" width="150">
                <asp:Label ID="lblUserName" runat="server" Text="User Name:" />&nbsp;
            </td>
            <td align="left" valign="middle">
                <asp:TextBox ID="txtUserName" runat="server" MaxLength="16" TabIndex="1" ToolTip="This was the user name as provided by your Teledyne LeCroy representative" Width="200" />&nbsp;
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td align="right" valign="middle">
                <asp:Label ID="lblPassword" runat="server" Text="Password:" />&nbsp;
            </td>
            <td align="left" valign="middle">
                <asp:TextBox ID="txtPassword" runat="server" MaxLength="16" TabIndex="2" TextMode="Password" ToolTip="This was the password as provided by your Teledyne LeCroy representative" Width="200" />&nbsp;
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td>&nbsp;</td>
            <td align="left" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" TabIndex="3" Text="Submit" />&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="false" TabIndex="4" Text="Cancel" /><br />
                    <asp:Label ID="lblStatusMessage" runat="server" CssClass="error" />
            </td>
        </tr>
    </table>
</asp:Content>