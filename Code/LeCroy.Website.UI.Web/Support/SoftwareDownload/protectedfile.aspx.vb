﻿Imports System.Net.Mail
Imports LeCroy.Library.AmazonServices
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Support_SoftwareDownload_protectedfile
    Inherits BasePage

#Region "Variables/Keys"
    Private _docId As Int32 = 0
    Private _returnUrl As String = String.Empty

    Private Shared IOTA_UIDPWD As String = ConfigurationManager.AppSettings("ProtectedFileUserPasswords_IOTA").ToString()
    Private Shared IOTA_RETURNURL As String = "~/support/softwaredownload/documents.aspx?standardid=14"
    Private Shared DDR_UIDPWD As String = ConfigurationManager.AppSettings("ProtectedFileUserPasswords_DDR").ToString()
    Private Shared DDR3_RETURNURL As String = "~/support/softwaredownload/documents.aspx?standardid=15"
    Private Shared DDR4_RETURNURL As String = "~/support/softwaredownload/documents.aspx?standardid=13"
    Private Shared UCS_UIDPWD As String = ConfigurationManager.AppSettings("ProtectedFileUserPasswords_UCS").ToString()
    Private Shared UCS_RETURNURL As String = "~/support/softwaredownload/documents.aspx?standardid=4"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ValidateQueryString()
        Session("RedirectTo") = String.Format("~/support/softwaredownload/protectedfile.aspx?did={0}", _docId.ToString())
        If Not (Me.ValidateUser()) Then
            Response.Redirect("~/support/user/")
        End If
        Me.ValidateUserNoRedir()
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        lblStatusMessage.Text = String.Empty
        Page.Validate()
        If (Page.IsValid) Then
            If Not (IsValidUserPasswordCombination()) Then
                lblStatusMessage.Text = "User or password is incorrect. Please verify your entries as provided by your Teledyne LeCroy representative and try again."
                Return
            End If

            Dim document As Document = DocumentRepository.GetDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), _docId)
            If (document Is Nothing) Then
                Response.Redirect("~/")
            End If
            ProcessInternalMailNotification(document.DocTypeId)

            Dim filePath As String = Functions.GetDOwnloadFilePath(_docId)
            If Not (String.IsNullOrEmpty(filePath)) Then
                Dim url As String = String.Format("{0}{1}", ConfigurationManager.AppSettings("AwsPrivateBaseUrl"), filePath.ToLower())
                Dim signedUrl As String = AmazonSignedUrl.GetCannedPolicySignedUrl(url, DateTime.UtcNow.AddSeconds(Convert.ToInt32(ConfigurationManager.AppSettings("AwsUtcExpirationRange"))), ConfigurationManager.AppSettings("AwsPemKeyFilePath"), ConfigurationManager.AppSettings("AwsKeyPairId"), False)
                Response.Redirect(signedUrl)
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        If Not (String.IsNullOrEmpty(_returnUrl)) Then
            Response.Redirect(_returnUrl)
        End If
        HandleBouncedRedirect()
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("did"))) Then
            HandleBouncedRedirect()
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("did"))) Then
            Integer.TryParse(Request.QueryString("did"), _docId)
            If (_docId <= 0) Then
                HandleBouncedRedirect()
            End If
        End If
        If Not (Functions.IsDocumentPasswordProtected(_docId)) Then
            HandleBouncedRedirect()
        End If
    End Sub

    Private Function IsValidUserPasswordCombination() As Boolean
        Dim collectionFromConfig As Dictionary(Of String, String) = ParseWebConfigKey()
        If (collectionFromConfig.Count <= 0) Then
            If Not (String.IsNullOrEmpty(_returnUrl)) Then
                Response.Redirect(_returnUrl)
            End If
            HandleBouncedRedirect()
        End If
        If Not (collectionFromConfig.ContainsKey(txtUserName.Text.Trim().ToLower())) Then
            Return False
        End If
        If Not (String.Compare(txtPassword.Text.Trim(), collectionFromConfig(txtUserName.Text.Trim().ToLower()), False) = 0) Then
            Return False
        End If
        Return True
    End Function

    Private Function ParseWebConfigKey() As Dictionary(Of String, String)
        Dim collectionFromConfig As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim configKey As String = GetProperWebConfigKey()
        If Not (String.IsNullOrEmpty(configKey)) Then
            Dim userPasswords As String() = configKey.Split(",")
            For Each userPassword In userPasswords
                Dim splitUserPassword As String() = userPassword.Split("|")
                If Not (collectionFromConfig.ContainsKey(splitUserPassword(0))) Then
                    collectionFromConfig.Add(splitUserPassword(0).ToLower(), splitUserPassword(1))  ' HACK: Added as a ToLower() to make the uname lookup case insensitive
                End If
            Next
        End If
        Return collectionFromConfig
    End Function

    Private Function GetProperWebConfigKey() As String
        Dim documentCategories As List(Of LeCroy.Library.Domain.Common.DTOs.DocumentCategory) = DocumentCategoryRepository.GetDocumentCategoriesForDocumentId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _docId)
        Dim productSeriesIds As List(Of Int32) = documentCategories.Select(Function(x) x.ProductSeriesId.Value).ToList()
        For Each productSeriesId As Int32 In productSeriesIds
            Select Case productSeriesId
                Case 280
                    _returnUrl = UCS_RETURNURL
                    Return UCS_UIDPWD
                Case 344
                    _returnUrl = DDR3_RETURNURL
                    Return DDR_UIDPWD
                Case 398
                    _returnUrl = DDR4_RETURNURL
                    Return DDR_UIDPWD
                Case 383
                    _returnUrl = IOTA_RETURNURL
                    Return IOTA_UIDPWD
            End Select
        Next
        Return String.Empty
    End Function

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/support/techlib/")
    End Sub

    Private Sub ProcessInternalMailNotification(ByVal docTypeId As Int32)
        Dim CampaignID As String = String.Empty
        Dim RequestID As String = String.Empty
        Dim strBody As String = String.Empty
        Dim strSubject As String = String.Empty
        Dim strCC As String = String.Empty
        Dim strTo As String = String.Empty
        Dim strCRet As String = Chr(13) & Chr(10)
        If Not Session("CampaignID") Is Nothing Then
            CampaignID = Session("CampaignID").ToString()
        End If

        If Not Session("ContactId") Is Nothing And Len(Session("ContactId")) > 0 And Len(_docId) > 0 Then
            RequestID = Functions.InsertDownloadByProductRequest(Session("ContactId"), 8, _docId, 0, 0, "", "", "No Remarks for Download", "", 0, 0, "", CampaignID)    ' NOTE: Mseries should be = 0 as triggered by the psg download pages
            If Len(Session("Country")) > 0 Then
                If CStr(Session("Country")) = "Japan" Then
                    strBody = strBody & "New Download: " & Functions.GetDOwnloadFilePath(_docId) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    strSubject = "New Download"
                Else
                    strBody = strBody & "New Download: " & Functions.getdownloaddescr(_docId) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                    strSubject = "New Download - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
                End If
            Else
                strBody = strBody & "New Download: " & Functions.getdownloaddescr(_docId) & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                strSubject = "New Download - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            End If

            If (docTypeId = 13) Then   ' Check for PERT
                strCC = "Roy.Chestnut@teledynelecroy.com"
            End If
            If (docTypeId = 15) Then   ' Check for LogicStudio
                strCC = "james.chan@teledyne.com"
            End If
            If (docTypeId = 9) Then    ' Check for IOTA
                Dim subCategoryId As String = Functions.GetDownloadSubCategory(_docId)
                If Not (String.IsNullOrEmpty(subCategoryId)) Then
                    If (String.Compare(subCategoryId, "18", True) = 0) Then
                        strCC = "Roy.Chestnut@teledynelecroy.com"   ' As per Mickey Romm - 9/8/2015
                    End If
                End If
            End If

            strTo = Functions.GetSalesEmail(Session("Country").ToString, _docId, Session("state").ToString, Session("ContactID").ToString, LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS, 8)       ' Assumes Protocol (catid) only for download (request)
            If Len(strBody) > 0 Then
                Try
                    Me.SendEmail(Session("country").ToString, strBody, strTo, "webmaster@teledynelecroy.com", strCC, "", strSubject, "")
                Catch ex As Exception
                    Functions.ErrorHandlerLog(True, ex.Message)
                End Try
            End If
        End If
        Session("PSGdownloadID") = Nothing
    End Sub

    'NOTE: Copied from download.aspx.vb
    Private Sub SendEmail(ByVal strCountry As String, ByVal strBody As String, ByVal strTo As String, ByVal strFrom As String, ByVal strCC As String, ByVal strDisplayName As String, ByVal strSubject As String, ByVal AttachmentFile As String)
        If (String.IsNullOrEmpty(strBody) Or String.IsNullOrEmpty(strTo)) Then
            Return
        End If

        Dim fromAddress As MailAddress = Nothing
        fromAddress = New MailAddress("webmaster@teledynelecroy.com", strDisplayName)
        Dim mm As MailMessage = New MailMessage() '(fromAddress, toAddress)
        mm.To.Add(strTo)
        'If Not (String.IsNullOrEmpty(strCC)) Then mm.CC.Add(New MailAddress(strCC))
        If Not (String.IsNullOrEmpty(strCC)) Then mm.CC.Add(strCC)
        mm.Bcc.Add(New MailAddress("kate.kaplan@teledyne.com"))
        mm.Bcc.Add(New MailAddress("james.chan@teledyne.com"))
        mm.Subject = strSubject
        mm.Body = strBody
        mm.From = fromAddress
        If Not (String.IsNullOrEmpty(strCountry)) Then
            If CStr(strCountry) = "Japan" Then
                mm.BodyEncoding = Encoding.GetEncoding("shift-JIS")
                mm.SubjectEncoding = Encoding.GetEncoding("shift-JIS")
            ElseIf CStr(strCountry) = "China" Then
                mm.BodyEncoding = Encoding.GetEncoding("GB2312")
                mm.SubjectEncoding = Encoding.GetEncoding("GB2312")
            ElseIf Mid(CStr(strCountry), 1, 5) = "Korea" Then
                mm.BodyEncoding = Encoding.GetEncoding("korean")
                mm.SubjectEncoding = Encoding.GetEncoding("korean")
            End If
        End If

        Try
            Dim smtpClient As SmtpClient = New SmtpClient(System.Configuration.ConfigurationManager.AppSettings("EmailHost"), System.Configuration.ConfigurationManager.AppSettings("Port"))
            smtpClient.Send(mm)
        Catch ex As Exception
            Functions.ErrorHandlerLog(True, ex.Message + " " + ex.Source)
        End Try
    End Sub
#End Region
End Class