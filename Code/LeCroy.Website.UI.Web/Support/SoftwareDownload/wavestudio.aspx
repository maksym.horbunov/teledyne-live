﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_WaveStudio" Codebehind="wavestudio.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
            <tr>
                <td valign="top" align="left" height="38">
                    <p align="left"><img src="<%= rootDir%>/images/category/headers/hd_download.gif" alt="Software Download"><br></p>
                    <p align="left"><h1>WaveStudio</h1></p>
                    <div class="title2">Features and Benefits</div>
                    <ul><li>Easily capture analog and digital waveforms remotely from Teledyne LeCroy oscilloscopes</li><li>Import, view, and analyze LabNotebook files</li><li>Load saved .trc or WaveML waveform files for offline analysis</li><li>Analyze oscilloscope traces using x and y cursors and 21 built-in measurements</li><li>Annotate waveforms with custom text boxes</li><li>Control oscilloscope remotely using virtual screen and front panel</li><li>Quickly zoom and pan to easily navigate through long waveform records</li><li>Easily connect to the oscilloscope using the LAN, USB, RS-232, or GPIB interface</li><li>Compatible with LabMaster, WaveMaster, WavePro, WaveRunner, HRO, HDO, WaveSurfer, WaveJet, and WaveAce</li></ul>
                    <p>WaveStudio is a fast and easy way to analyze acquired waveforms offline, or remotely control an oscilloscope from your desktop.</p>
                    <div class="title2">Capture</div>
                    <p>Loading a waveform or screenshot into WaveStudio can be performed in one of two ways, either offline or connected to the oscilloscope. When offline, simply open a trace file from a USB stick or e-mail attachment from the oscilloscope's save waveform function.</p>
                    <p>When connected to the oscilloscope, simply click on the display capture option to load a screen shot, or click a channel or channels to automatically load in the trace information.</p>
                    <div class="title2">View</div>
                    <p>WaveStudio presents the trace and screen shot views in tabs or windows. The tab view is ideal when many captures are stored. The window view can tile or cascade up to four widows of data to help correlate related waveforms and provide a time synchronized view.</p>
                    <div class="title2">Analyze</div>
                    <p>WaveStudio provides x and y axis cursors for quick timing and amplitude measurements. For more precise values, use the built in 21 automatic measurements for accurate and quick results.</p>
                    <div class="title2">Document</div>
                    <p>WaveStudio makes it easy to document your work, whether the oscilloscope is next to you or is many miles away. Save LabNotebook traces for future analysis, or save the screenshot as a .bmp, .jpg, .png, or .tif. Traces can be saved in binary or text formats.</p>
                    <div class="title2">Supported Measurements</div>
                    <ul><li>Amplitude</li><li>Max</li><li>Mean</li><li>Min</li><li>Area</li><li>Base</li><li>Top</li><li>RMS</li><li>Pk–Pk</li><li>Overshoot+</li><li>Overshoot-</li><li>Delay</li><li>Duty</li><li>Fall 90–10</li><li>Fall 80–20</li><li>Rise 10–90</li><li>Rise 20–80</li><li>Period</li><li>Frequency</li><li>Width+</li><li>Width-</li></ul>
                    <div class="title2">System Requirements</div>
                    <p>Your oscilloscope firmware must be at least version 6.0.1.x or higher.</p>
                    <p>Your PC must be running Windows&reg; XP SP2, Windows&reg; Vista, Windows&reg; 7, or Windows&reg; 10 with 256 MB of RAM and 2 GB of free HDD space.</p>
                    <div class="title2">Supported Oscilloscopes</div>
                    <ul><li>WaveAce 1000</li><li>WaveAce 2000</li><li>WaveJet 300/300A*/Touch</li><li>WaveSurfer MXs-B/MSO MXs-B/10/3000/3000z/510</li><li>HDO4000/4000A</li><li>WaveRunner 6000/Xi/Xi-A/6 Zi/HRO 6 Zi/8000/9000</li><li>HDO6000/6000A</li><li>HDO8000</li><li>MDA800</li><li>WavePro 7000/7 Zi/Zi-A/HD</li><li>WaveMaster 8000/8 Zi/Zi-A/Zi-B</li><li>LabMaster 9 Zi/Zi-A</li><li>LabMaster 10 Zi</li></ul>
                    <p>* Supports only Display Capture and Terminal Modes</p><br /><br />
                </td>
                <td valign="top" rowspan="2" align="left" height="108">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <img src="/images/sidebar/lft_hd_product_resources.gif" alt="Product Resources" height="23" width="203">
    <div class="resources">
        <ul>
            <li class="pdf"><a href="/doc/docview.aspx?id=8139">Datasheet</a></li>
            <li class="link"><a href="/doc/docview.aspx?id=2979">Manual</a></li>
        </ul>
    </div>
    <div class="divider"></div>
    <div class="resources" style="padding-left: 10px;">
        <a href="download.aspx?did=11655"><h2>Download 64-bit WaveStudio 9.0.0.1</h2></a><br />
        <a href="download.aspx?did=11656"><h2>Download 32-bit WaveStudio 9.0.0.1</h2></a>
    </div>
</asp:Content>