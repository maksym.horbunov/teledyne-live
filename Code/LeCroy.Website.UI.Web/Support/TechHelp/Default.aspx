﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechHelp_Default" Codebehind="Default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <asp:Panel ID="pn_title" runat="server">
            <h1>Technical Help</h1><br />
            <asp:Literal ID="litOne" runat="server" /><br />
            <asp:Label Font-Bold="true" ForeColor="red" ID="lb_errorMsg" runat="server" />
        </asp:Panel>
        <table border="0" cellpadding="0" cellspacing="5">
            <asp:Panel ID="pn_compf" runat="server">
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td><asp:Image ID="img_comp" runat="server" /></td>
                    <td><asp:Label ID="lb_comp" runat="server" /></td>
                    <td><asp:DropDownList ID="ddl_comp" AutoPostBack="true" runat="server" /></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pm_comp" runat="server">
                <tr>
                    <td></td>
                    <td><h2><b><asp:Literal ID="litTwo" runat="server" /></b></h2></td>
                    <td></td>
                </tr>
                <tr>
                    <td><img src="<%=rootDir %>/Images/Icons/BlueArrowBox_small.gif" /></td>
                    <td><h2><b><asp:Literal ID="litThree" runat="server" /></b></h2></td>
                    <td><asp:Button ID="btn_go" runat="server" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><h2><b><asp:Literal ID="litFour" runat="server" /></b></h2></td>
                    <td></td>
                </tr>
                <tr>
                    <td><asp:Image ID="img_categories" runat="server" /></td>
                    <td colspan="2"><asp:Label ID="lb_categories" runat="server" /></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td><asp:DropDownList ID="ddl_categories" AutoPostBack="true" runat="server" /></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pn_model" runat="server">
                <tr>
                    <td valign="top"><asp:Image ID="img_model" runat="server" /></td>
                    <td colspan="2"><asp:Label ID="lb_model" runat="server" /></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td><asp:DropDownList ID="ddl_model" AutoPostBack="true" runat="server" /></td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pn_products" runat="server">
                <tr>
                    <td><img src="<%=rootDir %>/Images/Icons/BlueArrowBox_small.gif" /></td>
                    <td colspan="2"><h2><asp:Label ID="lb_products" runat="server" /></h2></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"><asp:Label ID="lb_msg" runat="server" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <table width="100%">
                            <tr><td colspan="2"><asp:Label ID="lb_check" runat="server" />&nbsp;<asp:CheckBox ID="cb_check" runat="server" /></td></tr>
                            <tr>
                                <td width="50%"><asp:Label ID="lb_cp_y" runat="server" /></td>
                                <td width="50%"><asp:Label ID="lb_cp_n" runat="server" /></td>
                            </tr>
                            <tr>
                                <td width="50%"><asp:ListBox ID="lbx_cp_y" runat="server" SelectionMode="Multiple" Height="100" /></td>
                                <td width="50%"><asp:ListBox ID="lbx_cp_n" runat="server" SelectionMode="Multiple" Height="100" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="pn_input" runat="server">
                <tr>
                    <td><img src="../../Images/Icons/BlueArrowBox_small.gif" width="12" height="12" /></td>
                    <td colspan="2"><h2><b><asp:Label ID="lb_info" runat="server" /></b></h2></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <asp:TextBox ID="tb_info" runat="server" Width="300" TextMode="MultiLine" Rows="8" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_info" ErrorMessage="*" />
                    </td>
                </tr>
                <tr id="trEditProfileDescription" runat="server" visible="false">
                    <td><img src="../../Images/Icons/BlueArrowBox_small.gif" width="12" height="12" /></td>
                    <td colspan="2"><h2><b><asp:Label ID="lb_info2" runat="server" /></b></h2></td>
                </tr>
                <tr id="trEditProfileButton" runat="server" visible="false">
                    <td></td>
                    <td colspan="2"><asp:Button ID="btn_info" runat="server" CausesValidation="false" /></td>
                </tr>
                <tr id="trEditProfileLabel" runat="server" visible="false">
                    <td></td>
                    <td colspan="2"><% Techhelp_showProfile()%></td>
                </tr>
                <tr>
                    <td><img src="../../Images/Icons/BlueArrowBox_small.gif" width="12" height="12" /></td>
                    <td colspan="2"><h2><b><asp:Label ID="lb_info3" runat="server" /></b></h2></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"><asp:Button ID="btn_sub" runat="server" /></td>
                </tr>
            </asp:Panel>
        </table>
        <asp:Label ID="lb_result" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>