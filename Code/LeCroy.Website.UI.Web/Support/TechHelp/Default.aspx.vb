﻿Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class Support_TechHelp_Default
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Software Downloads"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Technical Help"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0594", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()).Replace("/tm/ServiceSupport/default.asp#Info", rootDir & "/support/service.aspx")
        litTwo.Text = Functions.LoadI18N("SLBCA0542", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0540", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0542", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Session("RedirectTo") = rootDir & "/Support/TechHelp/"
        lb_errorMsg.Text = ""
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/support/user/")
        End If
        ValidateUserNoRedir()
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        captionID = AppConstants.SUPPORT_CAPTION_ID

        '** menuID
        menuID = AppConstants.TECH_HELP_MENU
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID
        If Not Me.IsPostBack Then
            Session.Remove("th_comp")
            Session.Remove("th_cat")
            Session.Remove("th_model")
            Session.Remove("tb_info")
            Session.Remove("th_showinfo")
            Initial()
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False

            SetLoggedInTableRowVisibility()
            'SetFormDataFromPreLoginRedirect()
        End If
    End Sub

    Private Sub Initial()
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        Dim li As ListItem
        If Session("localeid") <> "1041" Then
            lb_comp.Text = "<h2><b>" & Functions.LoadI18N("SLBCA0598", localeId) & "</b></h2>"
        Else
            lb_comp.Text = "<font color=""#b22222"">" & Functions.LoadI18N("SLBCA0596", localeId) & "</font>"
        End If

        img_comp.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        img_model.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        img_categories.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        'li = New ListItem("", "0")
        'ddl_comp.Items.Add(li)
        'li = New ListItem(Functions.LoadI18N("SLBCA0595", localeid), "1")  'Analyzers
        'ddl_comp.Items.Add(li)
        'li = New ListItem(Functions.LoadI18N("SLBCA0744", localeid), "3")  'RTG
        'ddl_comp.Items.Add(li)
        'li = New ListItem(Functions.LoadI18N("SLBCA0596", localeid), "2")  'Scopes
        'ddl_comp.Items.Add(li)
        li = New ListItem("", "0")
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0649", localeId), "1")   'Scopes
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0595", localeId), "2")   'Analyzers
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0746", localeId), "3")   'PERT
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0747", localeId), "4")   'Waveform
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0748", localeId), "5")   'Logic
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0749", localeId), "6")   'SPARQ
        ddl_comp.Items.Add(li)
        li = New ListItem(Functions.LoadI18N("SLBCA0750", localeId), "7")   'SIStudio
        ddl_comp.Items.Add(li)
        btn_go.Text = Functions.LoadI18N("SLBCA0539", localeId)
        lb_categories.Text = "<h2><b>" & Functions.LoadI18N("SLBCA0541", localeId) & "</b></h2>"
        lb_model.Text = "<h2><b>" & Functions.LoadI18N("SLBCA0544", localeId) & "</b></h2>"
        lb_msg.Text = Functions.LoadI18N("SLBCA0547", localeId) + "<br>" + Functions.LoadI18N("SLBCA0548", localeId) + "<br>" + Functions.LoadI18N("SLBCA0549", localeId)
        lb_info.Text = Functions.LoadI18N("SLBCA0552", localeId)
        lb_info2.Text = Functions.LoadI18N("SLBCA0553", localeId) & "<BR> " & LoadI18N("SLBCA0554", localeId)
        lb_info3.Text = Functions.LoadI18N("SLBCA0555", localeId)
        btn_sub.Text = Functions.LoadI18N("SLBCA0267", localeId)
        btn_info.Text = LoadI18N("SLBCA0217", localeId)
        pm_comp.Visible = False
        pn_model.Visible = False
        pn_products.Visible = False
        pn_input.Visible = False
        pn_model.Visible = False
        If Not Session("th_comp") Is Nothing And Not Session("th_cat") Is Nothing Then
            Functions.showProductModel(ddl_model, Session("th_comp"), Session("th_cat"), localeId)
            Functions.showCategory(ddl_categories, Session("th_comp"), localeId)
            If Not Session("th_model") Is Nothing Then
                Functions.showProductModel(ddl_model, Session("th_cat"), Session("th_comp"), localeId)
                showProduct(Session("th_comp"), Session("th_cat"), Session("th_comp"))
            End If
            If Not Session("tb_info") Is Nothing Then
                tb_info.Text = Session("tb_info")
            End If
        End If
        'LoadInfo()
    End Sub

    Private Sub LoadInfo()
        Dim th As String = ""
        Dim cat As String = ""
        Dim model As String = ""
        Dim sqlstr As String = ""
        Dim msg As String = ""
        Dim PropertygroupID As String = ""
        Dim counter As String = ""

        If Not Session("th_comp") Is Nothing Then
            th = Session("th_comp").ToString()
        End If

        If Not Session("th_cat") Is Nothing Then
            cat = Session("th_cat").ToString()

        End If

        If Not Session("th_model") Is Nothing Then
            model = Session("th_model").ToString()
        End If

        If Not Session("tb_info") Is Nothing Then
            tb_info.Text = Session("tb_info").ToString()
        End If

        If Not th.Equals("") Then
            ddl_comp.SelectedItem.Selected = False
            If Not ddl_comp.Items.FindByValue(th) Is Nothing Then
                ddl_comp.Items.FindByValue(th).Selected = True
                pn_compf.Visible = True
                pm_comp.Visible = True
            End If
        Else
            pm_comp.Visible = False
            pn_model.Visible = False
            pn_products.Visible = False
            pn_input.Visible = False
        End If

        If Not cat.Equals("") Then
            ddl_categories.SelectedItem.Selected = False

            If Not ddl_categories.Items.FindByValue(cat.ToString()) Is Nothing Then
                ddl_categories.Items.FindByValue(cat).Selected = True
                pn_model.Visible = True
            End If

            ' sqlstr = "select  count(distinct group_id) as counter from PRODUCT_GROUP where category_id = " & cat
            If th.Equals("2") Then
                sqlstr = "Select distinct PRODUCT_SERIES_ID from  PRODUCT_SERIES_CATEGORY  where CATEGORY_ID=19"
                counter = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray()).Count().ToString()
            Else
                If cat.Equals("1") Then
                    sqlstr = "Select distinct PRODUCT_SERIES_ID from  PRODUCT_SERIES_CATEGORY  where CATEGORY_ID=1"
                    counter = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray()).Count().ToString()
                Else
                    sqlstr = "select distinct group_id from PRODUCT_GROUP where category_id = @CATEGORYID"
                    counter = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)({New SqlParameter("@CATEGORYID", cat)}).ToArray()).Count().ToString()
                End If
            End If

            If Not counter.Equals("1") Or cat.Equals("1") Then
                pn_model.Visible = True
                pn_products.Visible = False
            Else
                pn_model.Visible = False
                pn_products.Visible = True
                pn_input.Visible = True
                showProduct(th, cat, model)
            End If

        Else
            pn_input.Visible = False
            pn_model.Visible = False
        End If

        If Not model.Equals("") Then
            ddl_model.SelectedItem.Selected = False
            If Not ddl_model.Items.FindByValue(model) Is Nothing Then
                ddl_model.Items.FindByValue(model).Selected = True

            End If
            showProduct(th, cat, model)
        End If

        If th.Equals("") Then
            img_comp.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        Else
            img_comp.ImageUrl = "/Images/Icons/check.gif"
        End If

        If cat.Equals("") Then
            img_categories.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        Else
            img_categories.ImageUrl = "/Images/Icons/check.gif"
        End If

        If model.Equals("") Then
            img_model.ImageUrl = "/Images/Icons/BlueArrowBox_small.gif"
        Else
            img_model.ImageUrl = "/Images/Icons/check.gif"
        End If

        If Not Session("th_showinfo") Is Nothing Then
            pn_compf.Visible = False
            pm_comp.Visible = False
            pn_model.Visible = False
            pn_products.Visible = False
            pn_input.Visible = True
        End If
    End Sub

    Private Sub showProduct(ByVal compId As String, ByVal cat As String, ByVal model As String)

        If (Not cat.Equals("") And Not model.Equals("")) Or (Not cat.Equals("1") And Not cat.Equals("")) Then

            Dim sqlstr As String = ""
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)()
            Dim li As ListItem
            Dim PropertygroupID As String = ""
            lbx_cp_n.Items.Clear()
            lbx_cp_y.Items.Clear()

            If model.Equals("") Then
                PropertygroupID = Functions.GetGroupIDonCATEGORYID(cat)
            Else
                PropertygroupID = model
            End If

            lb_cp_n.Visible = False
            lb_cp_y.Visible = False
            lbx_cp_n.Visible = False
            lbx_cp_y.Visible = False
            Dim localeId As String = "1033"
            If Not Techhelp_countProducts(Context, cat, PropertygroupID, "n", localeId).Equals("0") Or Not Techhelp_countProducts(Context, cat, PropertygroupID, "y", localeId).Equals("0") Then

                lb_products.Text = LoadI18N("SLBCA0545", localeId) + "&nbsp;Or&nbsp;" + LoadI18N("SLBCA0546", localeId)
                lb_check.Text = "<h3>" + LoadI18N("SLBCA0593", localeId) + "</h3>"
                lb_cp_n.Text = "<h3>" + LoadI18N("SLBCA0550", localeId) + "</h3>"
                lb_cp_y.Text = "<h3>" + LoadI18N("SLBCA0551", localeId) + "</h3>"

                If Not Techhelp_countProducts(Context, cat, PropertygroupID, "n", localeId).Equals("0") Then
                    lbx_cp_n.Visible = True
                    lb_cp_n.Visible = True
                    If compId.Equals("2") Then
                        sqlstr = "Select a.productid, a.partnumber from PRODUCT a INNER JOIN PRODUCT_SERIES_CATEGORY b On a.PRODUCTID=b.PRODUCT_ID " + _
                            " INNER JOIN PRODUCT_SERIES c on b.PRODUCT_SERIES_ID=c.PRODUCT_SERIES_ID where b.PRODUCT_SERIES_ID =@PROPERTYGROUPID and a.eol_yn = 'n' and b.CATEGORY_ID=19 AND c.SUBCAT_ID =@SUBCATID and a.disabled='n' ORDER by a.partnumber"
                        sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()), New SqlParameter("@SUBCATID", cat.ToString())})
                    Else
                        If cat.Equals("1") Then
                            sqlstr = "Select PRODUCT.productid, PRODUCT.partnumber  from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'n' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=1 and   PRODUCT.disabled='n'  ORDER by PRODUCT.partnumber"
                            sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString())})
                        Else
                            sqlstr = "Select PRODUCT.productid, PRODUCT.partnumber  from PRODUCT where  GROUP_ID=@PROPERTYGROUPID and product.eol_yn = 'n' and  PRODUCT.disabled='n' ORDER by PRODUCT.partnumber"
                            sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString())})
                        End If
                    End If
                    Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    For Each product In products
                        li = New ListItem(product.PartNumber, product.ProductId.ToString())
                        lbx_cp_n.Items.Add(li)
                    Next
                Else
                    lbx_cp_n.Visible = False
                End If

                If Not Techhelp_countProducts(Context, cat, PropertygroupID, "y", localeId).Equals("0") Then

                    lbx_cp_y.Visible = True
                    lb_cp_y.Visible = True

                    If compId.Equals("2") Then
                        sqlstr = "Select a.productid, a.partnumber from PRODUCT a INNER JOIN PRODUCT_SERIES_CATEGORY b On a.PRODUCTID=b.PRODUCT_ID " +
                            " INNER JOIN PRODUCT_SERIES c on b.PRODUCT_SERIES_ID=c.PRODUCT_SERIES_ID where b.PRODUCT_SERIES_ID =@PROPERTYGROUPID and a.eol_yn = 'y' and b.CATEGORY_ID=19 AND c.SUBCAT_ID =@CATID and   a.disabled='y'  ORDER by a.partnumber"
                        sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString()), New SqlParameter("@CATID", cat.ToString())})
                    Else
                        If cat.Equals("1") Then
                            sqlstr = "Select PRODUCT.productid, PRODUCT.partnumber  from PRODUCT INNER JOIN PRODUCT_SERIES_CATEGORY On PRODUCT.PRODUCTID=PRODUCT_SERIES_CATEGORY.PRODUCT_ID where PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID =@PROPERTYGROUPID and product.eol_yn = 'y' and PRODUCT_SERIES_CATEGORY.CATEGORY_ID=1 and   PRODUCT.disabled='y'  ORDER by PRODUCT.partnumber"
                            sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString())})
                        Else
                            sqlstr = "Select PRODUCT.productid, PRODUCT.partnumber  from PRODUCT where  GROUP_ID=@PROPERTYGROUPID and product.eol_yn = 'y' and  PRODUCT.disabled='y' ORDER by PRODUCT.partnumber"
                            sqlParameters = New List(Of SqlParameter)({New SqlParameter("@PROPERTYGROUPID", PropertygroupID.ToString())})
                        End If
                    End If
                    Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
                    For Each product In products
                        li = New ListItem(product.PartNumber, product.ProductId.ToString())
                        lbx_cp_y.Items.Add(li)
                    Next
                    lbx_cp_y.Visible = True
                Else
                    lbx_cp_y.Visible = False
                End If
                pn_products.Visible = True
            Else
                pn_products.Visible = False
            End If
            pn_input.Visible = True
        End If
    End Sub

    Protected Sub comp_changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_comp.SelectedIndexChanged
        Session("th_cat") = Nothing
        Session("th_model") = Nothing
        If ddl_comp.SelectedValue <> "0" Then
            If (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                Session("th_comp") = ddl_comp.SelectedValue
                btn_go_click(Nothing, Nothing)
            Else
                Session("th_comp") = ddl_comp.SelectedValue
                Session("th_cat") = Nothing
                Session("th_model") = Nothing
                pm_comp.Visible = False
                pn_model.Visible = False
                pn_products.Visible = False
                pn_input.Visible = False

                Functions.showCategory(ddl_categories, ddl_comp.SelectedValue, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            End If
        Else
            Session("th_comp") = Nothing
            Session("th_cat") = Nothing
            Session("th_model") = Nothing

            pn_model.Visible = False
            pn_products.Visible = False
            pn_input.Visible = False
        End If
        LoadInfo()
    End Sub

    Protected Sub cata_changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_categories.SelectedIndexChanged

        If ddl_categories.SelectedValue <> "0" Then
            Session("th_cat") = ddl_categories.SelectedValue
            Session("th_model") = Nothing
            pn_input.Visible = False
            pn_products.Visible = False

            Functions.showProductModel(ddl_model, ddl_categories.SelectedValue, ddl_comp.SelectedValue, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Else
            pn_model.Visible = False
            pn_products.Visible = False
            pn_input.Visible = False
            Session("th_cat") = Nothing
            Session("th_model") = Nothing
        End If
        LoadInfo()
    End Sub

    Protected Sub model_changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_model.SelectedIndexChanged
        If ddl_model.SelectedValue <> "0" Then

            Session("th_model") = ddl_model.SelectedValue
        Else
            pn_products.Visible = False
            pn_input.Visible = False
            Session("th_model") = Nothing
        End If
        LoadInfo()
    End Sub

    Protected Sub btn_info_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_info.Click
        Session("tb_info") = SQLStringWithOutSingleQuotes(tb_info.Text)
        'SaveFormData()
        Response.Redirect("../User/userprofile.aspx")
    End Sub

    Protected Sub btn_go_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_go.Click
        Session("IsGeneralQuestion") = True
        Session("th_showinfo") = "1"
        LoadInfo()
    End Sub

    Protected Sub btn_sub_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sub.Click
        HandleSubmit()
        'Try
        '    TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID").ToString()))   ' DB record is not needed anymore
        'Catch
        'End Try
        'Session("TECHHELPTEMPSTATEID") = Nothing    ' No need for this anymore
        'Session("IsGeneralQuestion") = Nothing
        'Session.Remove("TECHHELPTEMPSTATEID")
        'Session.Remove("IsGeneralQuestion")

        Dim DiscontinuedProducts As String = ""
        Dim strCRet As String = Chr(13) & Chr(10)
        Dim strContinued As String = ""

        For Each l As ListItem In lbx_cp_n.Items
            If l.Selected Then
                If strContinued.Equals("") Then
                    strContinued = l.Text
                Else
                    strContinued += "<br>" & l.Text
                End If
            End If
        Next

        For Each l As ListItem In lbx_cp_y.Items
            If l.Selected Then
                If DiscontinuedProducts.Equals("") Then
                    DiscontinuedProducts = l.Text
                Else
                    DiscontinuedProducts += "<br>" & l.Text
                End If
            End If
        Next

        If cb_check.Checked And Len(strContinued) > 0 Then
            lb_errorMsg.Text = "Select either the check box or select any of the Current products"
            LoadInfo()
            Return
        End If

        If cb_check.Checked And Len(DiscontinuedProducts) > 0 Then
            lb_errorMsg.Text = "Select either the check box or select the Discontinued products"
            LoadInfo()
            Return
        End If

        Session("th_showinfo") = Nothing
        Session("th_comp") = Nothing
        Session("th_model") = Nothing
        Session("th_cat") = Nothing
        pn_compf.Visible = False
        pm_comp.Visible = False
        pn_model.Visible = False
        pn_products.Visible = False
        pn_input.Visible = False
        pn_title.Visible = False

        Dim strEmailbr As String = ""
        Dim strEmailBody As String = ""
        Dim DisplayString As String = ""
        Dim client As Net.Mail.SmtpClient = New Net.Mail.SmtpClient()
        If Not AppSettings("EmailHost") Is Nothing Or AppSettings("EmailHost").ToString() <> "" Then
            client.Host = AppSettings("EmailHost").ToString()
        End If
        If Not AppSettings("Port") Is Nothing Or AppSettings("Port").ToString() <> "" Then
            client.Port = AppSettings("Port").ToString()
        End If

        ValidateUser()
        'If Not Functions.ValidateUser(Me.Context, rootDir) Then
        '    Response.Redirect(rootDir + "/Support/user/")
        'End If

        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        If (localeId <> 1041) Then
            If Len(Session("FirstName")) > 0 Then
                DisplayString = "Hi" & "&nbsp;" & Session("FirstName") & ", " & "<br>"
            End If
        Else
            If Len(Session("LastName")) > 0 Then
                DisplayString = Session("LastName") & "&nbsp;" & LoadI18N("SLBCA0160", localeId.ToString()) & "<br>"
            End If
        End If
        DisplayString = DisplayString & "<br>" & LoadI18N("SLBCA0562", localeId.ToString()) & "<br>"
        If Not ddl_categories.SelectedValue.ToString().Equals("") And Not ddl_model.SelectedValue.ToString().Equals("") Then
            If ddl_comp.SelectedValue = "2" Then
                DisplayString += "<br>" & LoadI18N("SLBCA0595", localeId.ToString())
                DisplayString += ", " & Functions.GetPSGCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                DisplayString += "<br>" & LoadI18N("SLBCA0746", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "4", True) = 0) Then
                DisplayString += "<br>" & LoadI18N("SLBCA0747", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "5", True) = 0) Then
                DisplayString += "<br>" & LoadI18N("SLBCA0748", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "6", True) = 0) Then
                DisplayString += "<br>" & LoadI18N("SLBCA0749", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "7", True) = 0) Then
                DisplayString += "<br>" & LoadI18N("SLBCA0750", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            Else
                DisplayString += "<br>" & LoadI18N("SLBCA0596", localeId.ToString())
                DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            End If

            If cb_check.Checked Then
                DisplayString = DisplayString & "<br>" & "<br>" & LoadI18N("SLBCA0563", localeId.ToString()) & ": " & Techhelp_getPropertygroupname(ddl_categories.SelectedValue, ddl_model.SelectedValue)
            Else
                DisplayString = DisplayString & "<br>" & "<br>" & LoadI18N("SLBCA0597", localeId.ToString()) & Techhelp_getPropertygroupname(ddl_categories.SelectedValue, ddl_model.SelectedValue)

                If Not strContinued.Equals("") Then
                    DisplayString = DisplayString & "<br>" & "<br>"
                    DisplayString = DisplayString & LoadI18N("SLBCA0550", localeId.ToString())
                    DisplayString = DisplayString & "<br>" & strContinued
                End If

                If Not DiscontinuedProducts.Equals("") Then
                    DisplayString = DisplayString & "<br>" & "<br>"
                    DisplayString = DisplayString & LoadI18N("SLBCA0551", localeId.ToString())
                    DisplayString = DisplayString & "<br>" & DiscontinuedProducts
                End If
            End If
        Else
            If Not ddl_categories.SelectedValue.ToString().Equals("") Then
                If ddl_comp.SelectedValue = "2" Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0595", localeId.ToString())
                    DisplayString += ", " & Functions.GetPSGCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0746", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "4", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0747", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "5", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0748", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "6", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0749", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "7", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0750", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                Else
                    DisplayString += "<br>" & LoadI18N("SLBCA0596", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                End If
            End If
        End If
        DisplayString = DisplayString & "<br>" & "<br>" & LoadI18N("SLBCA0564", localeId.ToString())
        DisplayString = DisplayString & "<br>" & "<br>" & tb_info.Text & "<br>" & "<br>"

        If (localeId <> 1042) Then
            strEmailBody = DisplayString.Replace("<br>", Chr(13) & Chr(10))
            DisplayString = DisplayString.Replace("<nobr>", "")
            DisplayString = DisplayString.Replace("</nobr>", "")
        End If

        If Not (localeId = 1042) Then
            strEmailbr = "<br>"
        Else
            strEmailbr = Chr(13) & Chr(10)
        End If

        strEmailBody = LoadI18N("SLBCA0565", localeId.ToString()) & " " & strEmailbr

        Dim RequestID As String = ""
        Dim CampaignID As Integer
        Dim ContactID As String = ""
        Dim strBody As String = ""
        If Not Session("CampaignID") Is Nothing Then
            CampaignID = CLng(Session("CampaignID"))
        Else
            CampaignID = 0
        End If

        If Not ddl_categories.SelectedValue.ToString().Equals("") And Not ddl_model.SelectedValue.ToString().Equals("") Then
            If Not ddl_comp.SelectedValue.Equals("") And ddl_comp.SelectedValue.Equals("2") Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0595", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetPSGCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0746", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "4", True) = 0) Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0747", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "5", True) = 0) Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0748", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "6", True) = 0) Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0749", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            ElseIf (String.Compare(ddl_comp.SelectedValue, "7", True) = 0) Then
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0750", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            Else
                strEmailBody = strEmailBody & strEmailbr & LoadI18N("SLBCA0596", localeId.ToString())
                strEmailBody = strEmailBody & ", " & GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
            End If
            If cb_check.Checked Then
                strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0566", localeId.ToString()) & "  " & Techhelp_getPropertygroupname(ddl_categories.SelectedValue, ddl_model.SelectedValue)
            Else
                strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0597", localeId.ToString()) & Techhelp_getPropertygroupname(ddl_categories.SelectedValue, ddl_model.SelectedValue)
                If Not strContinued.Equals("") Then
                    strEmailBody = strEmailBody & strEmailbr & strEmailbr & strContinued
                End If
                If Not DiscontinuedProducts.Equals("") Then
                    strEmailBody = strEmailBody & strEmailbr & strEmailbr & DiscontinuedProducts
                End If
            End If
        Else
            If Not ddl_categories.SelectedValue.ToString().Equals("") Then
                If ddl_comp.SelectedValue = "2" Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0595", localeId.ToString())
                    DisplayString += ", " & Functions.GetPSGCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0746", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "4", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0747", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "5", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0748", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "6", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0749", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                ElseIf (String.Compare(ddl_comp.SelectedValue, "7", True) = 0) Then
                    DisplayString += "<br>" & LoadI18N("SLBCA0750", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                Else
                    DisplayString += "<br>" & LoadI18N("SLBCA0596", localeId.ToString())
                    DisplayString += ", " & Functions.GetCategoryNameonID(ddl_categories.SelectedValue) & " " & LoadI18N("SLBCA0567", localeId.ToString())
                End If
            End If
        End If
        strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0568", localeId.ToString())
        strEmailBody = strEmailBody & strEmailbr & SQLStringWithOutSingleQuotes(tb_info.Text) & strEmailbr

        If Len(Session("ContactId")) > 0 Then
            If Len(ddl_comp.SelectedValue) > 0 Then
                Dim fromEmail As String = Session("Email")
                Dim fromName As String = GetContactName(False)
                If (String.IsNullOrEmpty(fromEmail)) Then
                    fromEmail = "webmaster@teledynelecroy.com"
                End If
                If CInt(ddl_comp.SelectedValue) = 2 Then
                    If Len(Session("Country")) > 0 And (CStr(Session("Country")) = "Japan") Then
                        RequestID = InsertRequest(Session("ContactId"), 4, 19, SQLStringWithOutSingleQuotes(strEmailBody), "", 0, CampaignID)
                        strBody = "Technical Support Request:" & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())
                        strBody = strBody & strEmailBody
                        If Len(RequestID) > 0 Then
                            Try
                                Functions.SendEmail(Session("country").ToString, strBody.Replace("<br>", strCRet), "contact.jp@teledynelecroy.com,psgsupport@teledynelecroy.com", fromEmail, "Fumiaki.Sato@teledynelecroy.com", fromName, "Teledyne LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName"), "")
                            Catch ex As Exception

                            End Try
                        End If
                    Else
                        If (localeId = 1042) Then
                            strEmailbr = "<br>"
                        Else
                            strEmailbr = Chr(13) & Chr(10)
                        End If
                        RequestID = InsertRequest(Session("ContactId"), 4, 19, SQLStringWithOutSingleQuotes(strEmailBody), "", 0, CampaignID)
                        strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0650", localeId.ToString()) & " " & RequestID
                        Try
                            Functions.SendEmail(Session("country").ToString, (strEmailBody & strEmailbr & strEmailbr & Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())).Replace("<br>", strEmailbr), "psgsupport@teledynelecroy.com", fromEmail, "", fromName, "Teledyne LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName"), "")
                        Catch ex As Exception

                        End Try
                    End If
                ElseIf (String.Compare(ddl_comp.SelectedValue, "3", True) = 0) Then
                    strEmailbr = Chr(13) & Chr(10)
                    RequestID = InsertRequest(Session("ContactId"), 4, 19, SQLStringWithOutSingleQuotes(strEmailBody), "", 0, CampaignID)
                    strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0650", localeId.ToString()) & " " & RequestID
                    Try
                        Functions.SendEmail(Session("country").ToString, (strEmailBody & strEmailbr & strEmailbr & Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())).Replace("<br>", strEmailbr), "rtgsupport@teledynelecroy.com", fromEmail, "", fromName, "Teledyne LeCroy Technical Support Request - Web - RTG - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName"), "")
                    Catch
                    End Try
                Else
                    If (localeId = 1042) Then
                        strEmailbr = "<br>"
                    Else
                        strEmailbr = Chr(13) & Chr(10)
                    End If
                    RequestID = InsertRequest(Session("ContactId"), 4, 1, SQLStringWithOutSingleQuotes(strEmailBody), "", 0, CampaignID)
                    strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0650", localeId.ToString()) & " " & RequestID
                    If Len(Session("Country")) > 0 And (CStr(Session("Country")) = "Japan") Then
                        strBody = "Technical Support Request:" & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())
                        strBody = strBody & strEmailBody
                        If Len(RequestID) > 0 Then
                            Try
                                Functions.SendEmail(Session("country").ToString, strBody.Replace("<br>", strEmailbr), "contact.jp@teledynelecroy.com,psgsupport@teledynelecroy.com", fromEmail, "Fumiaki.Sato@teledynelecroy.com", fromName, "Teledyne LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName"), "")
                            Catch ex As Exception

                            End Try
                        End If
                    Else
                        SendEmailToTechnicalHelp(strEmailBody)
                    End If

                End If
            Else
                If (localeId = 1042) Then
                    strEmailbr = "<br>"
                Else
                    strEmailbr = Chr(13) & Chr(10)
                End If
                RequestID = InsertRequest(Session("ContactId"), 4, 1, SQLStringWithOutSingleQuotes(strEmailBody), "", 0, CampaignID)
                strEmailBody = strEmailBody & strEmailbr & strEmailbr & LoadI18N("SLBCA0650", localeId.ToString()) & " " & RequestID
                SendEmailToTechnicalHelp(strEmailBody)
            End If
        End If

        If Len(Session("Country")) > 0 And Len(Session("email")) > 0 Then
            If CInt(GetRegionOnCountry(Session("Country"))) = 2 Then
                Try
                    FeMailManager.Send_EmailWithNoReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 12, localeId.ToString(), Session("email").ToString())
                Catch
                End Try
            End If
        End If
        DisplayString += "<br /><strong>" + LoadI18N("SLBCA0650", localeId.ToString()) & " " & RequestID.ToString & "</strong> "
        lb_result.Text = DisplayString
    End Sub

    Sub SendEmailToTechnicalHelp(ByVal EmailBody As String)
        Dim fromEmail As String = Session("Email")
        Dim fromName As String = GetContactName(False)
        If (String.IsNullOrEmpty(fromEmail)) Then
            fromEmail = "webmaster@teledynelecroy.com"
        End If

        Dim strTo As String = ""
        Dim strBCC As String = ""
        Dim strCRet As String = ""
        Dim strSubject As String = ""
        Dim strEBody As String = ""
        Dim strBody1 As String = ""
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        If (localeId <> 1042) Then
            If Len(Session("Country")) > 0 Then
                If UCase(Trim(Session("Country"))) <> UCase("UNITED STATES") Then
                    ' Get salesrep Email
                    If (localeId = 1041) Then
                        strTo = "contact.jp@teledynelecroy.com"
                    Else
                        strTo = Functions.GetTechRepEmail(Session("Country"), GetCategoryIdFromSelection())
                        strBCC = "kate.kaplan@teledyne.com"
                    End If
                Else
                    strTo = "techweb@teledynelecroy.com"
                    strBCC = "kate.kaplan@teledyne.com"
                End If
            Else
                strTo = "techweb@teledynelecroy.com"
                strBCC = "kate.kaplan@teledyne.com"
            End If
            strCRet = Chr(13) & Chr(10)
            strSubject = "Teledyne LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            strEBody = Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())


            If Len(EmailBody) > 0 And Len(strEBody) > 0 Then
                strBody1 = EmailBody & strCRet & strEBody
                Try
                    Functions.SendEmail(Session("country").ToString, strBody1.Replace("<br>", strCRet), strTo, fromEmail, "", fromName, strSubject, "")
                Catch ex As Exception

                End Try
            End If
        Else
            strTo = Functions.GetTechRepEmail(Session("COUNTRY"), GetCategoryIdFromSelection())
            strSubject = "Teledyne LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
            strEBody = Functions.EmailBodyClientData(Session("ContactId"), localeId.ToString())
            strCRet = Chr(13) & Chr(10)
            If Len(EmailBody) > 0 And Len(strEBody) > 0 Then
                strBody1 = EmailBody & strCRet & strEBody
                Try
                    Functions.SendEmail(Session("country").ToString, strBody1.Replace("<br>", strCRet), strTo, fromEmail, "", fromName, strSubject, "")
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub

    Private Function GetCategoryIdFromSelection() As Int32
        Select Case ddl_comp.SelectedValue
            Case "2"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.PROTOCOL_ANALYZERS
            Case "3"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.PERT3_SYSTEMS
            Case "4"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.ARBITRARY_WAVEFORM_GENERATORS
            Case "5"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.LOGIC_ANALYZERS
            Case "6"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.SPARQ
            Case "7"
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.SI_STUDIO
            Case Else
                Return LeCroy.Library.Domain.Common.Constants.CategoryIds.OSCILLOSCOPES
        End Select
    End Function

    Protected Sub Techhelp_showProfile()
        Response.Write("<table cellpadding = 0 cellspacing = 2 border =0>")
        Response.Write("<tr><td valign=top><strong>")
        Response.Write(GetContactName(True))
        Response.Write("</strong></td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Phone") & "</td></tr>")
        Response.Write("<tr><td valign=top>" & Session("Email") & "</td></tr>")
        Response.Write("</table>")
    End Sub

    Private Sub SetLoggedInTableRowVisibility()
        Dim trs As HtmlTableRow() = New HtmlTableRow() {trEditProfileDescription, trEditProfileButton, trEditProfileLabel}
        For Each tr As HtmlTableRow In trs
            tr.Visible = False
        Next
        If Not (Session("ContactId") Is Nothing) Then
            Try
                If (Int32.Parse(Session("ContactID")) = 0) Then
                    Return
                End If
            Catch
            End Try
        End If
        If (ValidateUserNoRedir()) Then
            For Each tr As HtmlTableRow In trs
                tr.Visible = True
            Next
        End If
    End Sub

    'Private Sub SetFormDataFromPreLoginRedirect()
    '    If Not (Session("TECHHELPTEMPSTATEID") Is Nothing) Then
    '        Dim techHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID").ToString()))
    '        If (techHelpTempState Is Nothing) Then
    '            Return
    '        End If
    '        If Not (techHelpTempState.TechHelpTempStateId = Guid.Empty) Then    ' Valid, bind page controls
    '            Dim pnls As Panel() = New Panel() {pn_compf, pm_comp, pn_model, pn_products, pn_input}
    '            For Each p As Panel In pnls
    '                p.Visible = False
    '            Next

    '            If (techHelpTempState.IsGeneralInquiry) Then
    '                pn_input.Visible = True
    '                tb_info.Text = techHelpTempState.EnteredInquiry
    '            Else
    '                ddl_comp.SelectedValue = techHelpTempState.SelectedProductGroupId.ToString()
    '                comp_changed(Nothing, Nothing)
    '                pn_compf.Visible = True
    '                If (techHelpTempState.SelectedProductCategoryId.HasValue) Then
    '                    ddl_categories.SelectedValue = techHelpTempState.SelectedProductCategoryId.Value.ToString()
    '                    cata_changed(Nothing, Nothing)
    '                End If
    '                If (techHelpTempState.SelectedProductModelId.HasValue) Then
    '                    ddl_model.SelectedValue = techHelpTempState.SelectedProductModelId.Value.ToString()
    '                    model_changed(Nothing, Nothing)
    '                End If
    '                If (techHelpTempState.SelectedGeneralQuestion) Then
    '                    cb_check.Checked = True
    '                End If
    '                If Not (String.IsNullOrEmpty(techHelpTempState.SelectedPartNumbersY)) Then
    '                    Dim selectedItems As String() = techHelpTempState.SelectedPartNumbersY.Split(",")
    '                    For Each s As String In selectedItems
    '                        lbx_cp_y.Items.FindByValue(Int32.Parse(s)).Selected = True
    '                    Next
    '                End If
    '                If Not (String.IsNullOrEmpty(techHelpTempState.SelectedPartNumbersN)) Then
    '                    Dim selectedItems As String() = techHelpTempState.SelectedPartNumbersN.Split(",")
    '                    For Each s As String In selectedItems
    '                        lbx_cp_n.Items.FindByValue(Int32.Parse(s)).Selected = True
    '                    Next
    '                End If
    '                tb_info.Text = techHelpTempState.EnteredInquiry
    '                'btn_sub_click(Nothing, Nothing)
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub HandleSubmit()
        'If Not (Session("ContactId") Is Nothing) Then
        '    Try
        '        If (Int32.Parse(Session("ContactID")) = 0) Then
        '            SaveFormData()
        '        End If
        '    Catch
        '    End Try
        'End If

        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
    End Sub

    'Private Sub SaveFormData()
    '    If Not (Session("TECHHELPTEMPSTATEID") Is Nothing) Then ' Do not create another session state if user already has one - look it up and verify
    '        Dim fetchedTechHelpTempState As TechHelpTempState = TechHelpTempStateRepository.GetTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID").ToString()))
    '        If Not (fetchedTechHelpTempState.TechHelpTempStateId = Guid.Empty) Then ' Valid, exit routine
    '            TechHelpTempStateRepository.DeleteTechHelpTempStateRecord(New Guid(Session("TECHHELPTEMPSTATEID").ToString()))
    '        End If
    '    End If

    '    Dim techHelpTempState As TechHelpTempState = New TechHelpTempState()
    '    techHelpTempState.TechHelpTempStateId = Guid.NewGuid()
    '    techHelpTempState.SelectedProductGroupId = Int32.Parse(ddl_comp.SelectedValue)
    '    If (ddl_categories.SelectedIndex > 0) Then
    '        techHelpTempState.SelectedProductCategoryId = Int32.Parse(ddl_categories.SelectedValue)
    '    Else
    '        techHelpTempState.SelectedProductCategoryId = Nothing
    '    End If
    '    If (ddl_model.SelectedIndex > 0) Then
    '        techHelpTempState.SelectedProductModelId = Int32.Parse(ddl_model.SelectedValue)
    '    Else
    '        techHelpTempState.SelectedProductModelId = Nothing
    '    End If
    '    techHelpTempState.SelectedGeneralQuestion = cb_check.Checked
    '    Dim selectedListBoxItemsY As String = BuildCommaDelimitedString(lbx_cp_y)
    '    techHelpTempState.SelectedPartNumbersY = IIf(Not (String.IsNullOrEmpty(selectedListBoxItemsY)), selectedListBoxItemsY, String.Empty)
    '    Dim selectedListBoxItemsN As String = BuildCommaDelimitedString(lbx_cp_n)
    '    techHelpTempState.SelectedPartNumbersN = IIf(Not (String.IsNullOrEmpty(selectedListBoxItemsN)), selectedListBoxItemsN, String.Empty)
    '    techHelpTempState.EnteredInquiry = tb_info.Text.Trim()
    '    Dim isGeneralQuestion As Boolean = False
    '    Boolean.TryParse(Session("IsGeneralQuestion"), isGeneralQuestion)
    '    techHelpTempState.IsGeneralInquiry = isGeneralQuestion
    '    Session("TECHHELPTEMPSTATEID") = techHelpTempState.TechHelpTempStateId.ToString()
    '    TechHelpTempStateRepository.InsertTechHelpTempStateRecord(techHelpTempState)
    'End Sub

    'Private Function BuildCommaDelimitedString(ByVal lb As ListBox) As String
    '    Dim sb As StringBuilder = New StringBuilder()
    '    For Each li As ListItem In lb.Items
    '        If (li.Selected) Then
    '            sb.Append(String.Format("{0},", li.Value))
    '        End If
    '    Next
    '    Dim retVal As String = sb.ToString()
    '    If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 1)
    '    Return retVal
    'End Function

    Private Function GetContactName(ByVal isResponseWrite As Boolean) As String
        Dim spacer As String = " "
        If (isResponseWrite) Then spacer = "&nbsp;"
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        If (localeId <> 1041) Then
            If Len(Session("FirstName")) > 0 Then
                Return String.Format("{0}{1}{2}", Session("FirstName"), spacer, Session("LastName"))
            End If
        Else
            If Len(Session("LastName")) > 0 Then
                Return String.Format("{0}{1}{2}", Session("LastName"), spacer, LoadI18N("SLBCA0160", localeId.ToString()))
            End If
        End If
        Return String.Empty
    End Function
End Class