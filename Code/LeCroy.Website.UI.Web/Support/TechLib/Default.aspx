<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Support_TechLib_TechnicalLibrary" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_techlib.gif" alt="Technical Library"></p>
        <p>The Technical Library holds a vast amount of technical information to help you solve your measurement problems, including, Application Notes, Videos, On-Line Manuals, Frequently Asked Questions, Tutorials and more... We are constantly adding material to this area to keep you up to date with the latest test & measurement technology.</p>
        <p>Select from the left hand side menu to view the literature and material you are interested.</p>
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>