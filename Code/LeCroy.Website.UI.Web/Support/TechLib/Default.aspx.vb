Imports System.Collections
Imports System.Data
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Support_TechLib_TechnicalLibrary
    Inherits BasePage
    Dim sql As String = ""
    Dim ds, dss As DataSet
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        'If Len(Request.QueryString("capid")) = 0 Then
        'captionID = AppConstants.SUPPORT_CAPTION_ID
        'Else
        'If IsNumeric(Request.QueryString("capid")) Then
        'captionID = Request.QueryString("capid")
        'Else
        captionID = AppConstants.SUPPORT_CAPTION_ID
        'End If
        'End If

        '** menuID
        'If Len(Request.QueryString("mid")) = 0 Then
        '    menuID = AppConstants.TECH_LIB_MENU
        'Else
        '    If IsNumeric(Request.QueryString("mid")) Then
        '        menuID = Request.QueryString("mid")
        '    Else
        menuID = AppConstants.TECH_LIB_MENU
        '    End If
        'End If

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, "")
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
    End Sub

End Class
