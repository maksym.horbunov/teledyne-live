Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_ProductManuals
    Inherits BasePage
    Dim strSQL As String = ""
    Dim ds, dss As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID

        typeid = AppConstants.MANUALS_DOC_TYPE_ID

        '** catid
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                catid = Request.QueryString("cat")
            End If
        End If

        strSQL = "select * from document_type"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                'content
                If typeid <> "" And typeid.Equals(dr("DOC_TYPE_ID").ToString()) Then
                    Me.submenulabel.Text += "<ul>"
                    strSQL = "select distinct d.category_id, d.category_name,d.SORT_ID from document a INNER JOIN " +
                    " document_type b ON  a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON " +
                    " a.document_id = c.document_id INNER JOIN  category d  ON c.category_id=d.category_id " +
                    " WHERE a.doc_type_id=b.doc_type_id and a.country_id=208 and a.POST='Y' " +
                    " and b.doc_type_id=@TYPEID ORDER by d.CATEGORY_NAME" 'd.SORT_ID"
                    'Response.Write(strSQL & "<br><br>")
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                    dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        Me.content.Text += "<Table>"
                        For Each drr As DataRow In dss.Tables(0).Rows
                            If drr("category_id").ToString() = catid Then
                                Me.submenulabel.Text += "<li class='current'><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            Else
                                Me.submenulabel.Text += "<li><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            End If

                            Me.content.Text += "<tr><td class='cell' ><h3><a href='?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + drr("category_name").ToString() + "</a></h3></td></tr>"
                            If catid <> "" And catid = drr("category_id").ToString() Then
                                Me.titlelabel.Text = drr("category_name").ToString()
                            End If
                        Next
                        Me.content.Text += "</Table>"
                    End If
                    Me.submenulabel.Text += "</ul>"
                End If
                'content
            Next
        End If

        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(typeid)

        Dim countSeries As Integer = 0
        Dim countGroups As Integer = 0
        Dim countProducts As Integer = 0
        If catid <> Nothing And catid <> "" Then
            Me.Title += " - " + Functions.getTitleForCat(catid)
            'Product Series
            sqlParameters = New List(Of SqlParameter)
            If (catid <> 18) Then
                Me.content.Text = "<div class='accordion'>"
            Else
                Me.content.Text = "<div>"
            End If
            If catid = 19 Then
                strSQL = "SELECT DISTINCT e.PROTOCOL_STANDARD_ID, e.NAME, e.SORTID " +
                        " FROM DOCUMENT AS a INNER JOIN DOCUMENT_CATEGORY AS c ON a.DOCUMENT_ID = c.DOCUMENT_ID " +
                        " INNER JOIN CATEGORY AS d ON c.CATEGORY_ID = d.CATEGORY_ID INNER JOIN " +
                        " DOCUMENT_TYPE AS b ON a.DOC_TYPE_ID = b.DOC_TYPE_ID INNER JOIN " +
                        " PROTOCOL_STANDARD AS e ON c.PROTOCOL_STANDARD_ID = e.PROTOCOL_STANDARD_ID " +
                        " WHERE  b.doc_type_id=@TYPEID and c.category_id=@CATEGORYID and a.country_id=208 and a.POST='Y' order by e.SORTID "
                sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
            Else
                Dim sortOperator As String = "e.FW_SORT_ID"
                If (catid = 12 And typeid = 2) Then sortOperator = "e.NAME, e.FW_SORT_ID" ' Sort Software Options by alpha
                strSQL = String.Format("select DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.FW_SORT_ID " +
                " FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id " +
                " INNER JOIN document_category c ON a.document_id = c.document_id " +
                " INNER JOIN category d On c.category_id=d.category_id " +
                " INNER JOIN product_series e ON c.PRODUCT_SERIES_ID=e.PRODUCT_SERIES_ID " +
                " where b.doc_type_id=@DOCTYPEID and c.category_id=@CATEGORYID and a.country_id=208 and a.POST='Y' order by {0}", sortOperator)
                sqlParameters.Add(New SqlParameter("@DOCTYPEID", typeid))
                sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
            End If

            'Response.Write(strSQL & "<br><br>")
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                countSeries = ds.Tables(0).Rows.Count
                For Each dr As DataRow In ds.Tables(0).Rows
                    Me.content.Text += "<div class='list'>"
                    If catid = 19 Then
                        Me.content.Text += "<a name=" + Me.convertToString(dr("PROTOCOL_STANDARD_ID")) + "></a>"
                    Else
                        Me.content.Text += "<a name=" + Me.convertToString(dr("PRODUCT_SERIES_ID")) + "></a>"
                    End If

                    If (catid <> 18) Then Me.content.Text += Me.convertToString(dr("NAME"))
                    Me.content.Text += "</div>"
                    Me.content.Text += "<div class='faq'>"
                    '------------
                    sqlParameters = New List(Of SqlParameter)
                    If catid = 19 Then
                        strSQL = "select distinct a.document_id, a.title, a.description,a.version,a.sort_id  " +
                            " from document a INNER JOIN document_type b ON  a.doc_type_id=b.doc_type_id " +
                            " INNER JOIN document_category c On a.document_id=c.document_id " +
                            " where c.category_id = @CATEGORYID and c.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID and b.doc_type_id=@TYPEID and a.country_id=208 and a.POST='Y' order by a.title, a.sort_id"
                        sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
                        sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", Me.convertToString(dr("PROTOCOL_STANDARD_ID"))))
                        sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                    Else
                        strSQL = "select distinct a.document_id, a.title, a.description,a.version,a.sort_id  " +
                            " from document a INNER JOIN document_type b ON  a.doc_type_id=b.doc_type_id " +
                            " INNER JOIN document_category c On a.document_id=c.document_id " +
                            " where c.category_id = @CATEGORYID and c.PRODUCT_SERIES_ID = @PRODUCTSERIESID and b.doc_type_id=@TYPEID and a.country_id=208 and a.POST='Y'  order by a.title,a.sort_id"
                        sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
                        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", Me.convertToString(dr("PRODUCT_SERIES_ID"))))
                        sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                    End If
                    'Response.Write(strSQL & "<br><br>")
                    'Response.End()
                    dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        'Me.content.Text += "<ul>"
                        Me.content.Text += "<table width=450>"
                        For Each drr As DataRow In dss.Tables(0).Rows
                            Me.content.Text += "<tr>"
                            Me.content.Text += "<td width=400>"
                            'Me.content.Text += "<li ><B>"
                            Me.content.Text += "<a href='/doc/docview.aspx?id=" + drr("document_id").ToString() + "'>" + drr("title").ToString() + "</a>"
                            Me.content.Text += "</td>"
                            Me.content.Text += "<td width=50>"
                            Me.content.Text += "&nbsp;" + drr("VERSION").ToString()
                            Me.content.Text += "</td>"
                            'Me.content.Text += "</B><BR>"
                            Me.content.Text += "</tr>"
                            Me.content.Text += "<tr>"
                            Me.content.Text += "<td colspan=2>"
                            Me.content.Text += drr("description").ToString()
                            'Me.content.Text += "</li>"
                            Me.content.Text += "</td>"
                            Me.content.Text += "</tr>"
                            Me.content.Text += "<tr>"
                            Me.content.Text += "<td colspan=2><hr>"
                            Me.content.Text += "</td>"
                            Me.content.Text += "</tr>"

                        Next
                        Me.content.Text += "</table>"
                        'Me.content.Text += "</ul>"
                    End If
                    Me.content.Text += "</div>"
                Next
                Me.content.Text += "</div>"
            End If

            ''Category
            If countProducts = 0 And countGroups = 0 And countSeries = 0 Then
                Me.content.Text += "<div class='searchResults'>"
                '------------
                strSQL = "select distinct a.document_id, a.title, a.description,a.VERSION  " +
                      " from document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id " +
                      " INNER JOIN document_category c ON a.document_id=c.document_id " +
                      " where c.category_id = @CATEGORYID and b.doc_type_id=@DOCTYPEID and a.country_id=208 and a.POST='Y'  order by a.title"
                ' Response.Write(strSQL)
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
                sqlParameters.Add(New SqlParameter("@DOCTYPEID", typeid))
                dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                'Response.Write(dss.Tables(0).Rows.Count)
                If dss.Tables(0).Rows.Count > 0 Then

                    'Me.content.Text += "<ul>"
                    Me.content.Text += "<table width=450>"
                    For Each drr As DataRow In dss.Tables(0).Rows
                        Me.content.Text += "<tr>"
                        Me.content.Text += "<td width=400>"
                        'Me.content.Text += "<li ><B>"
                        Me.content.Text += "<a href='/doc/docview.aspx?id=" + drr("document_id").ToString() + "'>" + drr("title").ToString() + "</a>"
                        Me.content.Text += "</td>"
                        Me.content.Text += "<td width=50>"
                        Me.content.Text += "&nbsp;" + drr("VERSION").ToString()
                        Me.content.Text += "</td>"
                        'Me.content.Text += "</B><BR>"
                        Me.content.Text += "</tr>"
                        Me.content.Text += "<tr>"
                        Me.content.Text += "<td colspan=2>"
                        Me.content.Text += drr("description").ToString()
                        'Me.content.Text += "</li>"
                        Me.content.Text += "</td>"
                        Me.content.Text += "</tr>"
                        Me.content.Text += "<tr>"
                        Me.content.Text += "<td colspan=2><hr>"
                        Me.content.Text += "</td>"
                        Me.content.Text += "</tr>"

                    Next
                    Me.content.Text += "</table>"
                End If
                Me.content.Text += "</div></div>"
            End If
        End If
    End Sub
End Class