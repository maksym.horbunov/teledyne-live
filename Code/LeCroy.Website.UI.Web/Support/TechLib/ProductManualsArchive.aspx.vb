Imports System.Data.SqlClient
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_ProductManualsArchive
    Inherits BasePage

#Region "Variables/Keys"
    Private countSeries As Int32 = 0
    Private countGroups As Int32 = 0
    Private countProducts As Int32 = 0
    Private _accordionGroups As Dictionary(Of String, List(Of ArchiveDocumentData)) = New Dictionary(Of String, List(Of ArchiveDocumentData))
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        Dim menuURL As String = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        Dim menuURL2 As String = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID

        Dim documentTypeId As String = AppConstants.MANUALS_ARCIVE_DOC_TYPE_ID
        Dim categoryId As String = String.Empty
        '** catid
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                categoryId = Request.QueryString("cat")
            End If
        End If

        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * from document_type", New List(Of SqlParameter)().ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If (documentTypeId <> "" And documentTypeId.Equals(dr("DOC_TYPE_ID").ToString())) Then
                    Me.submenulabel.Text += "<ul>"
                    Dim sqlString As String = "SELECT DISTINCT d.category_id,d.category_name,d.SORT_ID FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id = c.document_id INNER JOIN category d ON c.category_id=d.category_id WHERE a.doc_type_id=b.doc_type_id and a.country_id=208 and a.POST='d' and b.doc_type_id=@TYPEID ORDER by d.category_name"
                    Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
                    Dim dsSubset As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
                    If dsSubset.Tables(0).Rows.Count > 0 Then
                        Me.content.Text += "<Table>"
                        For Each drr As DataRow In dsSubset.Tables(0).Rows
                            If drr("category_id").ToString() = categoryId Then
                                Me.submenulabel.Text += "<li class='current'><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            Else
                                Me.submenulabel.Text += "<li><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            End If

                            Me.content.Text += "<tr><td class='cell' ><h3><a href='?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + drr("category_name").ToString() + "</a></h3></td></tr>"
                            If (categoryId <> "" And categoryId = drr("category_id").ToString()) Then
                                Me.titlelabel.Text = drr("category_name").ToString()
                            End If
                        Next
                        Me.content.Text += "</Table>"
                    End If
                    Me.submenulabel.Text += "</ul>"
                End If
            Next
        End If

        If Not Me.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(documentTypeId)

        If (categoryId <> Nothing And categoryId <> "") Then
            Me.Title += " - " + Functions.getTitleForCat(categoryId)
            BindDocuments(Int32.Parse(categoryId), Int32.Parse(documentTypeId))
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindDocuments(ByVal categoryId As Int32, ByVal documentTypeId As Int32)
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<div class=""accordion"">")
        LoadDataForProductSeriesOrProtocolStandard(categoryId, documentTypeId)
        LoadDataForProductGroups(categoryId, documentTypeId)
        LoadDataForProducts(categoryId, documentTypeId)

        If (countProducts = 0 And countGroups = 0 And countSeries = 0) Then
            sb.Append("<div class=""searchResults"">")
            sb.Append(BuildCategorySection(categoryId, documentTypeId))
            sb.Append("</div>")
        Else
            Dim ide As IDictionaryEnumerator = _accordionGroups.GetEnumerator()
            While ide.MoveNext()
                sb.AppendFormat("<div class=""list"">{0}</div>", ide.Key)
                sb.Append("<div class=""faq"">")
                sb.Append("<table width=""450"">")
                For i = 0 To ide.Value.Count - 1
                    Dim add As ArchiveDocumentData = ide.Value(i)
                    sb.AppendFormat("<tr><td width=""400""><a href=""/doc/docview.aspx?id={0}"">{1}</a></td><td width=""50"">&nbsp;{2}</td></tr>", add.DocumentId.ToString(), add.Title, add.Version)
                    sb.AppendFormat("<tr><td colspan=""2"">{0}</td></tr><tr><td colspan=""2""><hr /></td></tr>", add.Description)
                Next
                sb.Append("</table>")
                sb.Append("</div>")
            End While
        End If
        sb.Append("</div>")
        Me.content.Text = sb.ToString()
    End Sub

    Private Sub LoadDataForProductSeriesOrProtocolStandard(ByVal categoryId As Int32, ByVal documentTypeId As Int32)
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If (categoryId = CategoryIds.PROTOCOL_ANALYZERS) Then
            sqlString = "SELECT DISTINCT e.PROTOCOL_STANDARD_ID, e.NAME, e.SORTID FROM DOCUMENT AS a INNER JOIN DOCUMENT_CATEGORY AS c ON a.DOCUMENT_ID = c.DOCUMENT_ID INNER JOIN CATEGORY AS d ON c.CATEGORY_ID = d.CATEGORY_ID INNER JOIN DOCUMENT_TYPE AS b ON a.DOC_TYPE_ID = b.DOC_TYPE_ID INNER JOIN PROTOCOL_STANDARD AS e ON c.PROTOCOL_STANDARD_ID = e.PROTOCOL_STANDARD_ID WHERE b.doc_type_id=@TYPEID and c.category_id=@CATEGORYID and a.country_id=208 and a.POST='d' order by e.NAME"
            sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        Else
            sqlString = "SELECT DISTINCT e.PRODUCT_SERIES_ID, e.NAME, e.FW_SORT_ID FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id = c.document_id INNER JOIN category d On c.category_id=d.category_id INNER JOIN product_series e ON c.PRODUCT_SERIES_ID=e.PRODUCT_SERIES_ID where b.doc_type_id=@TYPEID and c.category_id=@CATEGORYID and a.country_id=208 and a.POST='d' order by e.NAME"
            sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        End If
        BindDataToDictionary(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()), categoryId, documentTypeId, SelectionType.ProductSeriesOrProtocolStandard)
    End Sub

    Private Sub LoadDataForProductGroups(ByVal categoryId As Int32, ByVal documentTypeId As Int32)
        Dim sqlString As String = "SELECT DISTINCT e.GROUP_ID,e.GROUP_NAME,e.sortid FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id = c.document_id INNER JOIN category d ON c.category_id=d.category_id INNER JOIN PRODUCT_GROUP e ON c.GROUP_ID=e.GROUP_ID WHERE b.doc_type_id=@TYPEID AND c.category_id=@CATEGORYID AND a.country_id=208 AND a.POST='d' ORDER BY e.sortid"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        BindDataToDictionary(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()), categoryId, documentTypeId, SelectionType.ProductGroup)
    End Sub

    Private Sub LoadDataForProducts(ByVal categoryId As Int32, ByVal documentTypeId As Int32)
        Dim sqlString As String = "SELECT DISTINCT e.PRODUCTID,e.PARTNUMBER,e.sort_id FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id = c.document_id INNER JOIN category d ON c.category_id=d.category_id INNER JOIN PRODUCT e ON c.PRODUCT_ID=e.PRODUCTID WHERE b.doc_type_id=@TYPEID AND c.category_id=@CATEGORYID AND a.country_id=208 AND a.POST='d' ORDER BY e.sort_id"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        BindDataToDictionary(DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray()), categoryId, documentTypeId, SelectionType.Product)
    End Sub

    Private Sub BindDataToDictionary(ByVal topLevelDataSet As DataSet, ByVal categoryId As Int32, ByVal typeId As Int32, ByVal selectionType As SelectionType)
        If (topLevelDataSet.Tables(0).Rows.Count <= 0) Then
            Return
        End If
        For Each dr As DataRow In topLevelDataSet.Tables(0).Rows
            Dim sqlStringModifier As String = String.Empty
            Dim valueOfModifier As String = String.Empty
            Dim name As String = String.Empty
            If (selectionType = Support_TechLib_ProductManualsArchive.SelectionType.Product) Then
                sqlStringModifier = "AND c.product_id = @ID "
                valueOfModifier = dr("PRODUCTID").ToString()
                countProducts = topLevelDataSet.Tables(0).Rows.Count
                name = Me.convertToString(dr("PARTNUMBER"))
            ElseIf (selectionType = Support_TechLib_ProductManualsArchive.SelectionType.ProductGroup) Then
                sqlStringModifier = "AND c.group_id = @ID"
                valueOfModifier = dr("GROUP_ID").ToString()
                countGroups = topLevelDataSet.Tables(0).Rows.Count
                name = Me.convertToString(dr("GROUP_NAME"))
            Else
                If (categoryId = CategoryIds.PROTOCOL_ANALYZERS) Then
                    sqlStringModifier = "AND c.PROTOCOL_STANDARD_ID = @ID"
                    valueOfModifier = Me.convertToString(dr("PROTOCOL_STANDARD_ID"))
                Else
                    sqlStringModifier = "AND c.PRODUCT_SERIES_ID = @ID"
                    valueOfModifier = Me.convertToString(dr("PRODUCT_SERIES_ID"))
                End If
                countSeries = topLevelDataSet.Tables(0).Rows.Count
                name = dr("NAME").ToString()
            End If
            Dim sqlString As String = String.Format("SELECT DISTINCT a.document_id,a.title,a.description,a.version FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id=c.document_id WHERE c.category_id = @CATEGORYID {0} AND b.doc_type_id= @TYPEID AND a.country_id=208 AND a.POST='d' ORDER BY a.title", sqlStringModifier)
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
            sqlParameters.Add(New SqlParameter("@ID", valueOfModifier.ToString()))
            sqlParameters.Add(New SqlParameter("@TYPEID", typeId.ToString()))
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not (_accordionGroups.ContainsKey(name)) Then
                    _accordionGroups.Add(name, CreateListOfDocuments(ds))
                Else
                    _accordionGroups(name) = ModifyDocumentsList(ds, _accordionGroups(name))
                End If
            End If
        Next
    End Sub

    Private Function BuildCategorySection(ByVal categoryId As Int32, ByVal documentTypeId As Int32) As String
        Dim sqlString As String = "SELECT DISTINCT a.document_id,a.title,a.description,a.version FROM document a INNER JOIN document_type b ON a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON a.document_id=c.document_id WHERE c.category_id = @CATEGORYID AND b.doc_type_id= @TYPEID AND a.country_id=208 AND a.POST='d' ORDER BY a.title"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", categoryId.ToString()))
        sqlParameters.Add(New SqlParameter("@TYPEID", documentTypeId.ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        Dim sb As StringBuilder = New StringBuilder
        If (ds.Tables(0).Rows.Count > 0) Then
            sb.Append("<table width=""450"">")
            For Each documentDataSetDataRow As DataRow In ds.Tables(0).Rows
                sb.AppendFormat("<tr><td width=""400""><a href=""/doc/docview.aspx?id={0}"">{1}</a></td><td width=""50"">&nbsp;{2}</td></tr>", documentDataSetDataRow("document_id").ToString(), documentDataSetDataRow("title").ToString(), documentDataSetDataRow("VERSION").ToString())
                sb.AppendFormat("<tr><td colspan=""2"">{0}</td></tr><tr><td colspan=""2""><hr /></td></tr>", documentDataSetDataRow("description").ToString())
            Next
            sb.Append("</table>")
        End If
        Return sb.ToString()
    End Function

    Private Function CreateListOfDocuments(ByVal documentDataSet As DataSet) As List(Of ArchiveDocumentData)
        Dim retVal As List(Of ArchiveDocumentData) = New List(Of ArchiveDocumentData)
        If (documentDataSet.Tables(0).Rows.Count > 0) Then
            For Each documentDataSetDataRow As DataRow In documentDataSet.Tables(0).Rows
                Dim archiveDocumentData As ArchiveDocumentData = New ArchiveDocumentData()
                archiveDocumentData.Description = documentDataSetDataRow("description").ToString()
                archiveDocumentData.DocumentId = Int32.Parse(documentDataSetDataRow("document_id").ToString())
                archiveDocumentData.Title = documentDataSetDataRow("title").ToString()
                archiveDocumentData.Version = documentDataSetDataRow("VERSION").ToString()
                retVal.Add(archiveDocumentData)
            Next
        End If
        Return retVal
    End Function

    Private Function ModifyDocumentsList(ByVal documentDataSet As DataSet, ByVal existingArchiveDocumentData As List(Of ArchiveDocumentData)) As List(Of ArchiveDocumentData)
        If (documentDataSet.Tables(0).Rows.Count <= 0) Then
            Return existingArchiveDocumentData
        End If
        If (existingArchiveDocumentData.Count <= 0) Then
            Return existingArchiveDocumentData
        End If

        For Each documentDataSetDataRow As DataRow In documentDataSet.Tables(0).Rows
            If (existingArchiveDocumentData.Where(Function(x) x.DocumentId = Int32.Parse(documentDataSetDataRow("document_id").ToString())).Count() > 0) Then
                Continue For
            End If

            Dim archiveDocumentData As ArchiveDocumentData = New ArchiveDocumentData()
            archiveDocumentData.Description = documentDataSetDataRow("description").ToString()
            archiveDocumentData.DocumentId = Int32.Parse(documentDataSetDataRow("document_id").ToString())
            archiveDocumentData.Title = documentDataSetDataRow("title").ToString()
            archiveDocumentData.Version = documentDataSetDataRow("VERSION").ToString()
            existingArchiveDocumentData.Add(archiveDocumentData)
        Next
        Return existingArchiveDocumentData
    End Function

    Private Enum SelectionType
        ProductSeriesOrProtocolStandard
        ProductGroup
        Product
    End Enum
#End Region
End Class

Public Class ArchiveDocumentData
    Public DocumentId As Int32
    Public Title As String
    Public Version As String
    Public Description As String
End Class