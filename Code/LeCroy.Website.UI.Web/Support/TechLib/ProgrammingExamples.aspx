﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_ProgrammingExamples" Codebehind="ProgrammingExamples.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <asp:Literal ID="menulabel" runat="server" />
                <asp:Label ID="Label1" runat="server" />
            </ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_techlib.gif" alt="Technical Library"></p>
        <p>Please select the Programming Example of your preference. NOTE: Programming Example attachments require that you are a valid registered user at the Teledyne LeCroy web site.</p>
        <p><h1><%=typeName %></h1></p>
    </div>
    <div class="tabs">
        <div class="bg">
            <div class="tab"><asp:Label ID="resultLB" runat="server" /></div>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <div class="overview">
                    <div class="overviewLeft">
                        <strong><asp:Label ID="result1LB" runat="server" /></strong>
                    </div>
                    <div style="text-align: right">
                        <asp:LinkButton ID="btn_pre" runat="server" Text="<img border='0' src='/images/icons/icons_rgt_nav_arrow.gif'> Previous 10 results" />&nbsp;&nbsp;
                        <asp:LinkButton ID="btn_next" runat="server" Text="next 10 results <img border='0' src='/images/icons/icons_lft_nav_arrow.gif'>" />
                    </div>
                    <div class="searchResults">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="cell">
                                            <h3><a href="ProgrammingExamples.aspx?type=8&docID=<%# Eval("document_id") %>"><%# Eval("title") %></a></h3>
                                            <%# Eval("description") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="plDetail" runat="server" Visible="false">
                <div class="overview">
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><h1><%=doc_title%></h1></td></tr>
                        <tr><td valign="top"><strong><%=doc_description%></strong></td></tr>
                    </table><br /><br />
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr><td valign="top"><%=doc_overview%></td></tr>
                    </table><br /><br />
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td valign="top">
                                <asp:Button ID="btnDownload" runat="server" Text="Download" Visible="False" /><br /><br />
                                <asp:Label ID="lblDownload" runat="server" Text="NOTE: Programming Example attachments require that you are a valid registered user at the Teledyne LeCroy web site." Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table><br /><br />
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr><td class="relatedfaqs" align="center"><img src="/images/icons/icon_small_arrow_left.gif" />&nbsp;<a href="javascript:history.go(-1)"><strong>Back to Programming Examples</strong></a></td></tr>
                    </table><br /><br />
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>