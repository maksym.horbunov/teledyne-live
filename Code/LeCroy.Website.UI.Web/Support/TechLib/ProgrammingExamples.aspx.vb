﻿Imports System.Data.SqlClient
Imports LeCroy.Library.AmazonServices
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.Functions

Partial Class Support_TechLib_ProgrammingExamples
    Inherits BasePage
    Dim strSQL As String = ""
    Public Shared typeName As String = ""

    Public doc_title As String = ""
    Public doc_description As String = ""
    Public doc_overview As String = "'"

    Public productGroup As String = ""
    Public modelNumber As String = ""
    Public category As String = ""
    Public subcategory As String = ""
    Public osciCurrent As String = ""
    Public protocolCurrent As String = ""
    Public menuURL2 As String = ""
    Public menuURL As String = ""
    Dim typeid As String = ""
    Dim catid As String = ""
    Dim docid As String = ""
    Dim ds, dss, dsDoc As DataSet

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If


        '** typeid
        If Len(Request.QueryString("type")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("type")) Then
                typeid = Request.QueryString("type")
            Else
                Response.Redirect("default.aspx")
            End If
        End If

        '** catid
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                catid = Request.QueryString("cat")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(typeid)

        If Not Page.IsPostBack Then
            strSQL = "select * from document_type"
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, New List(Of SqlParameter)().ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    'content
                    If typeid <> "" And typeid = dr("DOC_TYPE_ID").ToString() Then
                        typeName = dr("NAME")
                    End If
                Next
            End If

            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

        End If
        If Len(Request.QueryString("docid")) Then
            If IsNumeric(Request.QueryString("docid")) Then
                docid = Request.QueryString("docid")
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        If typeid <> "" And docid = "" Then
            bind(typeid)
            Panel1.Visible = True
        End If

        If docid <> "" Then

            Dim strProductGroupID As String = ""
            Dim strModelID As String = ""
            Dim strCategoryID As String = ""
            Dim strSubCatID As String = ""
            Dim attachment As String = ""

            plDetail.Visible = True
            'Dim parameters As IDataParameter = New DataList()
            'parameters.ParameterName = "faq_id"
            'parameters.Value = docid
            'DbHelperSQL.RunProcedure("usp_faq", parameters)
            strSQL = "select *  from DOCUMENT where POST='Y' and document_id = @DOCUMENTID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCUMENTID", docid))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                doc_title = dr("title").ToString()
                doc_description = dr("description").ToString()
                attachment = dr("FILE_FULL_PATH").ToString()
                doc_overview = GetOverviewHTML(AppConstants.DOCUMENTS_OVERVIEW_TYPE, docid)

                'attachment
                If attachment.Length > 0 Then
                    btnDownload.Visible = True
                    lblDownload.Visible = True
                End If
            End If

        End If
    End Sub

    Private Sub bind(ByVal typeid As String)
        If Len(typeid) > 0 Then
            Dim count As Integer = 0
            strSQL = "select distinct a.document_id, a.title, a.description,a.date_entered,sort_id " +
                    " from document a inner join document_category c on a.document_id = c.document_id " +
                    " where a.country_id=208 and a.post='y' and a.doc_type_id=@TYPEID order by a.sort_id"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
            dsDoc = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            count = dsDoc.Tables(0).Rows.Count
            Me.resultLB.Text = "There are <b>" +
            count.ToString() + "</b>&nbsp;" + typeName.ToString + "&nbsp;"
            Me.result1LB.Text = ""
            If count > 0 Then
                GridView1.DataSource = dsDoc
                GridView1.Visible = True
                GridView1.DataBind()
                Me.resultLB.Text += "Page <b>" + (Me.GridView1.PageIndex + 1).ToString() +
                "</b> of <b>" + (Me.GridView1.PageCount).ToString() + "</b> pages."
                Me.result1LB.Text = typeName.ToString + "&nbsp;"
                'Response.Write(Me.GridView1.PageIndex)
                If ((Me.GridView1.PageIndex + 1) = (Me.GridView1.PageCount)) Then
                    If ((Me.GridView1.PageIndex * 10) + 1) = (count) Then
                        Me.result1LB.Text += count.ToString()
                    Else
                        Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + count.ToString()
                    End If

                Else
                    Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + (Me.GridView1.PageIndex * 10 + 10).ToString()
                End If

                '** Pager navigation
                If count > 10 Then
                    If GridView1.PageIndex.ToString().Equals("0") Then
                        btn_pre.Visible = False
                    Else
                        btn_pre.Visible = True
                    End If

                    If GridView1.PageIndex.ToString().Equals((GridView1.PageCount - 1).ToString()) Then
                        btn_next.Visible = False
                    Else
                        btn_next.Visible = True
                    End If
                Else
                    btn_next.Visible = False
                    btn_pre.Visible = False
                End If
            Else
                GridView1.Visible = False
                btn_next.Visible = False
                btn_pre.Visible = False
            End If

        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        bind(typeid)
    End Sub

    Protected Sub btn_pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pre.Click
        Dim p As Integer = Me.GridView1.PageIndex - 1
        If p < 0 Then
            Exit Sub
        End If
        Me.GridView1.PageIndex = p
        bind(typeid)
    End Sub

    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_next.Click
        Dim p As Integer = GridView1.PageIndex + 1
        If p > Me.GridView1.PageCount Then
            Exit Sub
        End If
        GridView1.PageIndex = p
        bind(typeid)
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        If Len(docid) > 0 Then
            If Len(docid) > 0 Then
                If Len(Functions.GetPDFFilePath(docid)) > 0 Then
                    Dim url As String = String.Format("{0}{1}", ConfigurationManager.AppSettings("AwsPrivateBaseUrl"), Functions.GetPDFFilePath(docid).ToLower())
                    Dim signedUrl As String = AmazonSignedUrl.GetCannedPolicySignedUrl(url, DateTime.UtcNow.AddSeconds(Convert.ToInt32(ConfigurationManager.AppSettings("AwsUtcExpirationRange"))), ConfigurationManager.AppSettings("AwsPemKeyFilePath"), ConfigurationManager.AppSettings("AwsKeyPairId"), False)
                    Session("RedirectTo") = signedUrl
                    'Session("RedirectTo") = Functions.GetPDFFilePath(docid).ToString 'here is the name of the file itself
                    Dim RequestID As String = ""
                    If Not ValidateUser() Then
                        ' Do not http// for redirect
                        Response.Redirect("/Support/User/")
                    End If

                    ValidateUserNoRedir()
                    If Not Session("ContactID") Is Nothing And Not Session("Country") Is Nothing Then
                        If Len(Session("ContactId")) > 0 Then
                            If Len(Session("CampaignID")) = 0 Or Session("CampaignID") Is Nothing Then
                                Session("CampaignID") = 0
                            End If

                            RequestID = Functions.InsertRequest(Session("ContactId"), 21, docid, "No Remarks for Download", "", 0, Session("CampaignID"))

                        End If
                    End If
                    Response.Redirect(signedUrl)
                Else
                    Response.Redirect("Default.aspx")
                End If
            Else
                Response.Redirect("Default.aspx")
            End If

        End If
    End Sub
End Class