﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_ReleaseNotes" Codebehind="ReleaseNotes.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_techlib.gif" alt="Technical Library"></p>
        <p>The Release Notes section of the Teledyne LeCroy Technical Library lets you download the latest Release Notes for your group"s core operating software.</p>
        <p><img src="<%=rootDir %>/images/icons/icons_pdf.gif" align="left" style="margin-right: 5px;">Release Notes on this site are available in PDF format for easy download.</p>
        <p><img src="<%= rootDir%>/images/category/headers/hd2_releasenotes.gif" alt="Release Notes"></p>
        <p><h3><asp:Label ID="titlelabel" runat="server" /></h3></p>
    </div>
    <div class="tabs">
        <div class="bg">
            <div class="tab">
            </div>
            <div class="overview">
                <div class="searchResults">
                    <table>
                        <asp:Label ID="lbContent" runat="server" Text=""></asp:Label>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>