﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_ReleaseNotes
    Inherits BasePage
    Dim sql As String = ""
    Dim ds, dss As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID


        typeid = AppConstants.RELEASE_NOTES_DOC_TYPE_ID
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                catid = Request.QueryString("cat")
            End If
        End If
        sql = "select * from document_type"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of SqlParameter)().ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows

                Dim content1 As String = ""
                If typeid <> "" And typeid.Equals(dr("DOC_TYPE_ID").ToString()) Then
                    Me.submenulabel.Text += "<ul>"
                    sql = "select distinct d.category_id, d.category_name,d.SORT_ID from document a INNER JOIN " +
                    " document_type b ON  a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON " +
                    " a.document_id = c.document_id INNER JOIN  category d  ON c.category_id=d.category_id " +
                    " WHERE a.doc_type_id=b.doc_type_id and a.country_id=208 and a.POST='Y' " +
                    " and b.doc_type_id=@TYPEID ORDER by d.SORT_ID"
                    Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                    dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                    If dss.Tables(0).Rows.Count > 0 Then
                        Me.lbContent.Text += "<Table>"
                        For Each drr As DataRow In dss.Tables(0).Rows
                            If drr("category_id").ToString() = catid Then
                                Me.submenulabel.Text += "<li class='current'><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            Else
                                Me.submenulabel.Text += "<li><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                            End If

                            Me.lbContent.Text += "<tr><td class='cell'><h3><a href='?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + drr("category_name").ToString() + "</a></h3></td></tr>"
                            If catid <> "" And catid = drr("category_id").ToString() Then
                                Me.titlelabel.Text = drr("category_name").ToString()
                            End If
                        Next
                        Me.lbContent.Text += "</Table>"
                    End If
                    Me.submenulabel.Text += "</ul>"
                End If
            Next
        End If

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If

        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(AppConstants.RELEASE_NOTES_DOC_TYPE_ID)

        'content
        Dim content As String = ""
        If catid <> Nothing And catid <> "" Then
            Me.Title += " - " + Functions.getTitleForCat(catid)

            sql = "select distinct a.document_id, a.title, a.description,a.VERSION,a.SORT_ID  " +
                    " from document a inner join document_type b on a.doc_type_id = b.doc_type_id " +
                    " inner join document_category c on a.document_id = c.document_id " +
                      " WHERE c.category_id = @CATEGORYID and b.doc_type_id=@DOCTYPEID and a.country_id=208 and a.POST='Y' ORDER by a.SORT_ID "
            'Response.Write(sql)
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CATEGORYID", catid))
            sqlParameters.Add(New SqlParameter("@DOCTYPEID", AppConstants.RELEASE_NOTES_DOC_TYPE_ID.ToString()))
            dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            If dss.Tables(0).Rows.Count > 0 Then
                For Each drrr As DataRow In dss.Tables(0).Rows
                    content += "<tr>"
                    content += "<td><a href='/doc/docview.aspx?id=" + drrr("document_id").ToString() + "'>" + "<img src='" + rootDir + "/images/icons/icons_pdf.gif' border=0 vspace=5 hspace=5></a>"
                    content += "<a href='/doc/docview.aspx?id=" + drrr("document_id").ToString() + "'>" + drrr("title").ToString() + "</a> - " + drrr("version").ToString() + "<BR>"
                    content += "</td>"
                    content += "</tr>"
                Next
            End If
        End If
        lbContent.Text = content
    End Sub
End Class