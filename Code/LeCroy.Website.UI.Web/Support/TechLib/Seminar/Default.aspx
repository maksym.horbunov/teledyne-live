﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_Default" Codebehind="Default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <h2><b><asp:Literal ID="litOne" runat="server" /></b></h2><br />
    <img src="<%= rootDir%>/Images/linkarrow.gif" border="0"><a href="#" id="hlkLNA">North America / South America</a><br />
    <img src="<%= rootDir%>/Images/linkarrow.gif" border="0"><a href="#" id="hlkEMEA">Europe</a><br />
    <img src="<%= rootDir%>/Images/linkarrow.gif" border="0"><a href="../../../events/korea.aspx">Korea</a><br />
    <img src="<%= rootDir%>/Images/linkarrow.gif" border="0"><a href="/japan/seminar/default.asp">Japan</a><br /><br />
    <asp:HiddenField ID="hdnRegion" runat="server" Value="1033" />
    <asp:Literal ID="litTwo" runat="server" /><br />
    <asp:Label ID="lblHeader" runat="server" /><br />
    <p>NOTE: All seminars in Germany, Austria and in German-speaking Switzerland will be held in German.</p>


    <script type="text/javascript">
        jQuery(document).ready(function () {
            var jsonURL = "/events/output.aspx";
            jQuery.getJSON(jsonURL, { countryid: 208, eventtypeid: 3 }, processRequest);
        });

        jQuery('#hlkLNA').on('click',function () {
            var jsonURL = "/events/output.aspx";
            jQuery.getJSON(jsonURL, { regionid: 3, eventtypeid: 3 }, processRequest);
        });

        jQuery("#hlkEMEA").on('click',function () {
            var jsonURL = "/events/output.aspx";
            jQuery.getJSON(jsonURL, { regionid: 2, eventtypeid: 3 }, processRequest);
        });

        function processRequest(json) {
            jQuery('#dataTable').empty();
            for (i = 0; i < json.length; i++) {
                var jsonStartDate = json[i].ExhibitionStartDate.replace("/Date(", "").replace("-0400)/", "").replace("-0500)/", "").replace("-0600)/", "");
                var jsonEndDate = json[i].ExhibitionEndDate.replace("/Date(", "").replace("-0400)/", "").replace("-0500)/", "").replace("-0600)/", "");
                var startDate = new Date(jsonStartDate);
                const startMonth = startDate.toLocaleString('default', { month: 'long' });
                var endDate = new Date(jsonEndDate);
                const endMonth = endDate.toLocaleString('default', { month: 'long' });
                var startDateString = (startDate.getUTCDate());
                startDateString = startMonth + "&nbsp;" + startDateString;
                var endDateString = (endDate.getUTCDate());
                endDateString = endMonth + "&nbsp;" + endDateString;
                var jsonTitle = json[i].Title;
                var jsonURL = json[i].Url;
                var jsonDescription = json[i].Description.replace(/(<([^>]+)>)/ig, "");
                var jsonAddress = json[i].Address.replace(/(<([^>]+)>)/ig, " ");
                var jsonCity = json[i].City;
                var jsonState = json[i].State;
                var jsonTimeZone = json[i].TimeZone;
                var location = "";

                jsonTitle = "<strong><a href=\"" + jsonURL + "\">" + jsonTitle + "</a></strong><br />";
                if (jsonDescription.length > 2) { jsonDescription = jsonDescription + "<br />" };
                if (jsonAddress.length > 0) { jsonAddress = jsonAddress + "<br />" };

                if (jsonCity.length > 0) {
                    if (jsonState.length > 0) {
                        location = jsonCity + ", " + jsonState + "<br />";
                    }
                    else if (jsonState.length == 0) {
                        location = jsonCity + "<br />";
                    }
                }
                else if (jsonCity.length == 0) {
                    if (jsonState.length > 0) {
                        location = jsonState + "<br />";
                    }
                    else if (jsonState.length == 0) {
                        location = "";
                    }
                }

                jQuery('#dataTable').append(
                    jsonTitle
                    + jsonDescription
                    + jsonAddress
                    + location
                    + "<strong>Start:</strong> "
                    + startDateString + " " + jsonTimeZone
                    + "<br />"
                    + "<strong>End:</strong> "
                    + endDateString + " " + jsonTimeZone
                    + "<br /><br />");
            }
        }
    </script>
    <div id="dataTable"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"><br /><a href="/events/"><h2>&nbsp;&nbsp;View all<br />&nbsp;&nbsp;Events and Training &raquo;</h2></a></div>
</asp:Content>