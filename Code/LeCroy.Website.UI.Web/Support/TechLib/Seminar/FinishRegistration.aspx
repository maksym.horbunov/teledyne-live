﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Seminar_FinishRegistration" Codebehind="FinishRegistration.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Import Namespace="LeCroy.Website.Functions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td width="7">&nbsp;</td>
                <td valign="top">
                    <br />
                    <h1><asp:Literal ID="litOne" runat="server" /></h1>,&nbsp;<asp:Literal ID="litTwo" runat="server" /><br>
                    <b><asp:Label ID="lb_name" runat="server"></asp:Label></b>.<br><br>
                    <asp:Literal ID="litThree" runat="server" /><br>
                    <img border="0" src="images/icon.jpg" width="29" height="30"><br><br>
                    <strong><asp:Literal ID="litFour" runat="server" />: </strong>
                    <asp:Label ID="lb_name2" runat="server"></asp:Label><br />
                    <strong><asp:Literal ID="litFive" runat="server" />: </strong>
                    <asp:Label ID="lb_desc" runat="server"></asp:Label><br />
                    <strong><asp:Literal ID="litSix" runat="server" />: </strong>
                    <asp:Label ID="lb_date" runat="server"></asp:Label><br />
                    <strong><asp:Literal ID="litSeven" runat="server" />: </strong>
                    <asp:Label ID="lb_time" runat="server"></asp:Label><br />
                    <strong><asp:Literal ID="litEight" runat="server" />: </strong>
                    <asp:Label ID="lb_loc" runat="server"></asp:Label><br /><br>
                    <asp:Button ID="btn_finish" Text="Finish Registration >>" runat="server" />
                    <hr size="1">
                    <h2>
                        Please check your address, if you have moved or your address has changed, please click the <strong>Change Address Button </strong>below:<br><br><br>
                        <strong><% =Session("FirstName") & " " & Session("LastName")%></strong>
                        <br>
                        <% =Session("Company")%>
                        <br>
                        <% = Session("Address")%>
                        <br>
                        <%
                            If Not Session("Address2") Is Nothing Then
                                Response.Write(Session("Address2") & "<br>")
                            End If
                            Response.Write(Session("City"))
                            If Not Session("State") Is Nothing Then
                                Response.Write(", &nbsp; " & Session("State"))
                            End If
                            If Not Session("Zip") Is Nothing Then
                                Response.Write(" ,&nbsp; " & Session("Zip"))
                            End If
                        %>
                        <br>
                        <%=Session("Country")%>
                        &
                        <br>
                        <%  If Not Session("Phone") Is Nothing Then
                                Response.Write(Session("Phone") & "<br>")
                            End If
                        %>
                        <%= Session("Email")%><br>
                    </h2>
                    <asp:Button ID="btn_mod" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>