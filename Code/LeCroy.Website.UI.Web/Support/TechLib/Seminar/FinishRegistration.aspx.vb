﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class Support_Seminar_FinishRegistration
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = LoadI18N("SLBCA0307", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = LoadI18N("SLBCA0316", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = LoadI18N("SLBCA0317", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = LoadI18N("SLBCA0077", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = LoadI18N("SLBCA0312", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = LoadI18N("SLBCA0069", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = LoadI18N("SLBCA0313", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = LoadI18N("SLBCA0314", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Initial()
        End If
    End Sub

    Private Sub Initial()
        If Not Request("sid") Is Nothing Then
            Session("TempSeminarID") = Request.QueryString("sid")
        End If

        If Not Session("TempSeminarID") Is Nothing Then
            Response.Redirect("/Support/TechLib/Seminar/Default.aspx" + menuURL2)
        End If

        ValidateUserNoRedir()

        btn_mod.Text = Functions.LoadI18N("SLBCA0061", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        If Not Session("TempSeminarID") Is Nothing And Session("SeminarAfterReg") Is Nothing Then
            If IsNumeric(Session("TempSeminarID")) Then
                If CInt(Session("TempSeminarID")) = 76 Then
                    Response.Redirect("questions.asp")
                End If
            End If
        End If
        Dim seminar As Seminar = SeminarRepository.GetSeminarSimple(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("TempSeminarID").ToString()))
        If Not (seminar Is Nothing) Then
            lb_name.Text = seminar.Name
        End If
    End Sub

    Protected Sub btn_finish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_finish.Click
        Response.Redirect("RequestRegistration.aspx" + menuURL2)
    End Sub
End Class