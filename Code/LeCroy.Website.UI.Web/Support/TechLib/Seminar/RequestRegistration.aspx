﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_RequestRegistration" Codebehind="RequestRegistration.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
	    <p><asp:Label ID="lb_confirm" runat ="server" /></p>
        <asp:Panel ID="pnlPaymentRequired" runat="server" Visible="false">
            <asp:Button ID="btnCallToPay" runat="server" CausesValidation="false" OnClientClick="ga('send', 'event', 'SeminarRegistration', 'btnCallToPay', 'CallToPay', {'nonInteraction': 1});" Text="I will pay via phone or PO" />&nbsp;&nbsp;
            <asp:Button ID="btnShopifyToPay" runat="server" CausesValidation="false" OnClientClick="ga('send', 'event', 'SeminarRegistration', 'btnShopifyToPay', 'PayViaShopify', {'nonInteraction': 1});" Text="I will pay now" Visible="false" />
        </asp:Panel>
        <asp:Panel ID="pnlCallToComplete" runat="server" Visible="false">
            <br /><asp:Literal ID="litCallInformation" runat="server" />
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>