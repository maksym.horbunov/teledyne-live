﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Seminar_RequestRegistration
    Inherits BasePage

#Region "Variables/Keys"
    Public menuURL As String = ""
    Public menuURL2 As String = ""
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        ValidateUserNoRedir()

        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""
        If Not Len(Session("CampaignID")) > 0 Then
            Session("CampaignID") = 0
        End If
        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Initial()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnCallToPay_Click(sender As Object, e As EventArgs) Handles btnCallToPay.Click
        pnlCallToComplete.Visible = True
        Dim seminarId As Int32 = -1
        If Not (Int32.TryParse(Session("TempSeminarID").ToString(), seminarId)) Then
            Response.Redirect("default.aspx")
        End If

        If Not Session("Email") Is Nothing And Len(Session("Email")) > 0 Then
            Dim seminar As Specialized.SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminarId)
            If (seminar.Seminar Is Nothing) Then
                Response.Redirect("default.aspx")
            End If

            If (String.Compare(seminar.Seminar.EStoreYN, "Y", True) = 0 And seminar.Seminar.ProductFkId.HasValue And seminar.Seminar.Price.HasValue) Then
                Dim productSeriesCategory As ProductSeriesCategory = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [PRODUCT_SERIES_CATEGORY] WHERE [PRODUCT_ID] = @PRODUCTID AND [CATEGORY_ID] <> 26", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", seminar.Seminar.ProductFkId.Value.ToString())}).ToArray()).FirstOrDefault()
                Dim productSeriesId As Int32 = -1
                If Not (productSeriesCategory Is Nothing) Then
                    productSeriesId = productSeriesCategory.ProductSeriesId
                    If (productSeriesCategory.CategoryId = 19) Then
                        litCallInformation.Text = "Please call 1-800-909-7211 to speak to a customer service representative to complete your order."
                    Else
                        litCallInformation.Text = "Please call 1-800-553-2769 #1 or email <a href=""mailto:customersupport@teledynelecroy.com"">customersupport@teledynelecroy.com</a>"
                    End If
                Else
                    litCallInformation.Text = "Please call 1-800-553-2769 #1 or email <a href=""mailto:customersupport@teledynelecroy.com"">customersupport@teledynelecroy.com</a>"
                End If
            End If
        End If

        GenerateEcommerceOrder(Session("ContactID"), Session("CampaignID"), Session.SessionID, 0, Session("PromoCode"))
        btnShopifyToPay.Enabled = False
        btnCallToPay.Enabled = False
        Session("TempSeminarID") = Nothing
    End Sub

    Private Sub btnShopifyToPay_Click(sender As Object, e As EventArgs) Handles btnShopifyToPay.Click
        Dim seminarId As Int32 = -1
        If Not (Int32.TryParse(Session("TempSeminarID").ToString(), seminarId)) Then
            Response.Redirect("default.aspx")
        End If

        Dim seminar As Seminar = SeminarRepository.GetSeminarSimple(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminarId)
        If (seminar Is Nothing) Then
            Response.Redirect("default.aspx")
        End If
        Dim url As String = String.Format("https://store.teledynelecroy.com/cart/{0}:1", seminar.ShopifyVariantId)
        If Not (String.IsNullOrEmpty(url)) Then
            Session("TempSeminarID") = Nothing
            Response.Redirect(url)
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub Initial()
        Session("RedirectTo") = rootDir + "/Support/TechLib/Seminar/RequestRegistration.aspx" + menuURL2
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If

        If (Session("TempSeminarID") Is Nothing) Then
            Response.Redirect("default.aspx")
        End If

        Dim seminarId As Int32 = -1
        If Not (Int32.TryParse(Session("TempSeminarID").ToString(), seminarId)) Then
            Response.Redirect("default.aspx")
        End If

        Dim InsertedOk As Boolean = Functions.InsertRequest(Session("ContactId"), 6, Session("TempSeminarID"), "", "", Session("PageID"), Session("campaignid"))

        If Not Session("Email") Is Nothing And Len(Session("Email")) > 0 Then
            Dim seminar As Specialized.SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminarId)
            If (seminar.Seminar Is Nothing) Then
                Response.Redirect("default.aspx")
            End If

            If (String.Compare(seminar.Seminar.EStoreYN, "Y", True) = 0 And seminar.Seminar.ProductFkId.HasValue And seminar.Seminar.Price.HasValue) Then
                pnlCallToComplete.Visible = False
                pnlPaymentRequired.Visible = True
                Dim productSeriesCategory As ProductSeriesCategory = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [PRODUCT_SERIES_CATEGORY] WHERE [PRODUCT_ID] = @PRODUCTID AND [CATEGORY_ID] <> 26", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", seminar.Seminar.ProductFkId.Value.ToString())}).ToArray()).FirstOrDefault()
                Dim productSeriesId As Int32 = -1
                If Not (productSeriesCategory Is Nothing) Then
                    productSeriesId = productSeriesCategory.ProductSeriesId
                    If (productSeriesCategory.CategoryId = 19) Then
                        litCallInformation.Text = "Please call 1-800-909-7211 to speak to a customer service representative to complete your order."
                    Else
                        litCallInformation.Text = "Please call 1-800-553-2769 #1 or email <a href=""mailto:customersupport@teledynelecroy.com"">customersupport@teledynelecroy.com</a>"
                    End If
                End If
                EShopperSave(Int64.Parse(Session("ContactId")), Session.SessionID, productSeriesId, seminar.Seminar.ProductFkId.Value.ToString(), seminar.Seminar.ProductFkId.Value.ToString(), seminar.Seminar.Price.Value)
                lb_confirm.Text = String.Format("Thank you for your interest in <b>{0}</b>. To complete your registration, please choose from the following methods of payment below:", seminar.Seminar.Name)
            Else
                Try
                    GenerateEmail(seminar)
                Catch ex As Exception
                    Functions.ErrorHandlerLog(True, ex.Message + " Seminars Registration " + ex.Source)
                End Try
                lb_confirm.Text = String.Format("{0}&nbsp;<b>{1}</b>.<br /><br />{2} <strong>{3}</strong>&nbsp;{4}<br /><br />", Functions.LoadI18N("SLBCA0278", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), seminar.Seminar.Name, Functions.LoadI18N("SLBCA0280", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), Session("Email"), Functions.LoadI18N("SLBCA0281", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            End If
        End If
    End Sub

    Private Sub GenerateEmail(ByVal seminar As Specialized.SeminarData)
        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#SeminarShortDescription#>", Seminar.Seminar.ShortDescription)
        replacements.Add("<#SeminarTitle#>", Seminar.Seminar.Name)
        replacements.Add("<#SeminarDate#>", Seminar.Seminar.DateOfSeminar.ToShortDateString())
        replacements.Add("<#SeminarLocation#>", Seminar.Seminar.Location)
        replacements.Add("<#SeminarAgenda#>", Seminar.Seminar.Time)
        replacements.Add("<#SeminarUrl#>", String.Format("{0}/support/techlib/seminar/seminar_info.aspx?seminar_id={1}", ConfigurationManager.AppSettings("DefaultDomain"), seminar.Seminar.SeminarId.ToString()))
        If (seminar.Seminar.ProductFkId.HasValue And seminar.Seminar.Price.HasValue) Then
            'replacements.Add("<#SeminarPaymentRequired#>", String.Format())
        End If

        If Not (String.IsNullOrEmpty(seminar.Seminar.EmailToList)) Then
            Dim bccEmails As List(Of String) = (seminar.Seminar.EmailToList).Replace(";", ",").Split(",").ToList()
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 6, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), String.Empty, New List(Of String)({Session("Email").ToString()}), New List(Of String), bccEmails, replacements)
        Else
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 6, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), Session("Email").ToString(), replacements)
        End If
        Session("TempSeminarID") = Nothing
    End Sub

#Region "Copied from ECOMMERCE"
    Private Function GenerateEcommerceOrder(ByVal ContactID As Long, ByVal CampaignID As Long, ByVal SessionId As String, ByVal QuoteID As Long, ByVal PROMO_CODE As String) As Long
        Dim strSQL As String
        Dim strDate As Date
        Dim ds As DataSet
        Dim dsFromShopper As DataSet
        Dim strOrderID As Long
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)

        If ContactID.ToString.Length > 0 And CampaignID.ToString.Length > 0 And SessionId.ToString.Length Then
            'Create Quote_id for current quote of this customer in Quote table
            strDate = Now()
            strSQL = "Insert into ORDERS (CONTACTID,ORDER_DATE,Campaign_id,SE_CODE,QUOTEID,PROMO_CODE) VALUES (@CONTACTID,@ORDERDATE,@CAMPAIGNID,@SECODE,@QUOTEID,@PROMOCODE)"
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            sqlParameters.Add(New SqlParameter("@ORDERDATE", strDate.ToString()))
            sqlParameters.Add(New SqlParameter("@CAMPAIGNID", CampaignID.ToString()))
            sqlParameters.Add(New SqlParameter("@SECODE", GetSENumber(ContactID)))
            sqlParameters.Add(New SqlParameter("@QUOTEID", QuoteID.ToString()))
            sqlParameters.Add(New SqlParameter("@PROMOCODE", PROMO_CODE))
            DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            'Select Quote_id which was created
            strSQL = "SELECT ORDERID from ORDERS where CONTACTID=@CONTACTID and ORDER_DATE=@ORDERDATE"
            sqlParameters = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            sqlParameters.Add(New SqlParameter("@ORDERDATE", strDate.ToString()))
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    strOrderID = dr("ORDERID")
                Next
                If Len(strOrderID) > 0 Then
                    'Copy Cart Contents to ORDER_DETAILS Table
                    'Select everything from shopper table end insert to ORDER_DETAILS Table with QuoteID and CustomerID
                    strSQL = "SELECT SESSIONID, PRODUCTID, QTY,PRICE, DATE_ENTERED, PROPERTYGROUPID,MASTERPRODUCTID, " +
                    " GROUP_ID, PRODUCT_SERIES_ID,PROMO_CODE,PRICE_SAVED FROM Eshopper where SESSIONID = @SESSIONID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionId))
                    dsFromShopper = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

                    If dsFromShopper.Tables(0).Rows.Count > 0 Then
                        For Each drr As DataRow In dsFromShopper.Tables(0).Rows
                            strSQL = "INSERT into ORDER_DETAILS (ORDERID,SESSIONID,PRODUCTID,QTY,PRICE,DATE_ENTERED,PROPERTYGROUPID,CONTACTID,MASTERPRODUCTID,GROUP_ID, PRODUCT_SERIES_ID,PROMO_CODE,PRICE_SAVED)" +
                                     " values(@ORDERID,@SESSIONID,@PRODUCTID,@QTY,@PRICE,@DATEENTERED,@PROPERTYGROUPID,@CONTACTID,@MASTERPRODUCTID,@GROUPID,@PRODUCTSERIESID,@PROMOCODE,@PRICESAVED)"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@ORDERID", strOrderID.ToString()))
                            sqlParameters.Add(New SqlParameter("@SESSIONID", drr("SESSIONID").ToString()))
                            sqlParameters.Add(New SqlParameter("@PRODUCTID", drr("PRODUCTID").ToString()))
                            sqlParameters.Add(New SqlParameter("@QTY", drr("QTY").ToString()))
                            sqlParameters.Add(New SqlParameter("@PRICE", drr("PRICE").ToString()))
                            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now().ToString()))
                            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", drr("PROPERTYGROUPID").ToString()))
                            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
                            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", drr("MASTERPRODUCTID").ToString()))
                            sqlParameters.Add(New SqlParameter("@GROUPID", drr("group_id").ToString()))
                            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", drr("product_series_id").ToString()))
                            sqlParameters.Add(New SqlParameter("@PROMOCODE", drr("PROMO_CODE").ToString()))
                            sqlParameters.Add(New SqlParameter("@PRICESAVED", drr("PRICE_SAVED").ToString()))
                            'Response.write(strSQL)
                            DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                        Next
                    End If
                    'Clear Database contents for current sessionID
                    strSQL = "DELETE FROM ESHOPPER WHERE SESSIONID = @SESSIONID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@SESSIONID", SessionId))
                    DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    GenerateEcommerceOrder = strOrderID
                End If
            End If
        End If
    End Function

    Private Function GetSENumber(ByVal ContactID As Long) As String
        Dim strSQL As String
        Dim dsCompany As DataSet
        Dim dsCompanyExist As DataSet
        Dim strSENumber As String = ""
        Dim dsZip As DataSet
        Dim strZip As String
        Dim dsSECode As DataSet
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Len(ContactID) > 0 Then
            '****************************************************************
            'Check if the customer company is in the company exceptions list
            '****************************************************************
            'Select company name from contact table
            strSQL = "Select CONTACT.COMPANY ,CONTACT.CITY as CITY,STATE.SHORT_NAME as State,CONTACT.EMAIL as EMAIL from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=@CONTACTID"
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            dsCompany = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If dsCompany.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsCompany.Tables(0).Rows
                    'If the company is in exceptions list
                    'We check if this company for customer's STATE_PROVINCE exist in CCMS_EXCEPTION_COMPANIES
                    strSQL = "Select SECode from CCMS_EXCEPTION_COMPANIES where STATE=@STATE and CompanyName=@COMPANYNAME and CITY=@CITY and SECode in (Select SECode from CCMS_SECSR)"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@STATE", dr("State").ToString()))
                    sqlParameters.Add(New SqlParameter("@COMPANYNAME", Replace(Trim(dr("Company")), "'", "").ToString()))
                    sqlParameters.Add(New SqlParameter("@CITY", Replace(Trim(dr("City")), "'", "").ToString()))
                    dsCompanyExist = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dsCompanyExist.Tables(0).Rows.Count > 0 Then
                        For Each drr As DataRow In dsCompanyExist.Tables(0).Rows
                            strSENumber = drr("SECode")
                        Next
                    End If
                Next
            End If
            '****************************************************************
            'If customer company is not in the company exceptions list
            'Get SENumber by Zip
            '****************************************************************
            If Len(strSENumber) = 0 Then
                'Select ZIP for current customer from table CONTACT
                strSQL = "Select POSTALCODE from CONTACT where CONTACT_ID=@CONTACTID"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
                dsZip = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                If dsZip.Tables(0).Rows.Count > 0 Then
                    For Each drrr As DataRow In dsZip.Tables(0).Rows
                        strZip = Trim(drrr("POSTALCODE"))
                        If Len(strZip) >= 5 Then
                            'If Zip code have more then 5 digits we cut and use first five to find SENumber
                            If Len(strZip) > 5 Then
                                strZip = Mid(strZip, 1, 5)
                            End If
                            'Select SECode for this ZIP CODE from table CCMS_MAP_US
                            If Not IsNumeric(strZip) Then
                                GetSENumber = ""
                                Exit Function
                            End If
                            strSQL = "Select SECode from CCMS_MAP_US where ZipCode=@ZIPCODE"
                            sqlParameters = New List(Of SqlParameter)
                            sqlParameters.Add(New SqlParameter("@ZIPCODE", strZip))
                            dsSECode = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                            If dsSECode.Tables(0).Rows.Count > 0 Then
                                For Each drrrr As DataRow In dsSECode.Tables(0).Rows
                                    strSENumber = Trim(drrrr("SECode"))
                                Next
                            End If
                        End If
                    Next
                End If
            End If
            GetSENumber = strSENumber
        End If
    End Function

    'Private Function buildPaymentLink(ByVal OrderID As Long) As String
    '    Dim strSQL As String = ""
    '    Dim ds As DataSet
    '    Dim dsContact As DataSet
    '    Dim Product_Quantity As Integer = 0
    '    Dim Total_Quantity As Integer = 0
    '    'Dim arrayName(50) As String
    '    Dim b As Integer = 1
    '    ' Dim arrayPrice(50) As String
    '    'Dim ArrayQuantity(50) As String
    '    Dim paymentLink As String = ""
    '    ' *** Optional Variables ***
    '    Dim invoice As String = "invoice=" ' Invoice number for your records
    '    Dim customVariable As String = "custom=" ' Custom variable recorded on the subscription; Invisible to the user
    '    Dim itemNumber As String = "item_number=" ' Item number for your records 

    '    If OrderID > 0 Then

    '        strSQL = "SELECT ORDER_DETAILS.*,product.partnumber FROM ORDER_DETAILS inner join PRODUCT ON ORDER_DETAILS.PRODUCTID=PRODUCT.PRoductid WHERE ORDERID=@ORDERID"
    '        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
    '        sqlParameters.Add(New SqlParameter("@ORDERID", OrderID.ToString()))
    '        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            ' Build the subscription url using all of the consts and variables we have collected
    '            paymentLink = Globals.postUrl
    '            paymentLink += Globals.cmdSinglePayment + Globals.urlVarSeperator
    '            paymentLink += Globals.cmdUpload + Globals.urlVarSeperator
    '            paymentLink += Globals.business + Globals.urlVarSeperator
    '            paymentLink += Globals.reattempt + Globals.urlVarSeperator
    '            paymentLink += Globals.cmdCurrency + Globals.urlVarSeperator
    '            paymentLink += "invoice=" + OrderID.ToString + Globals.urlVarSeperator
    '            For Each dr As DataRow In ds.Tables(0).Rows

    '                paymentLink += "item_name_" + b.ToString + "=" + dr("partnumber").ToString + Globals.urlVarSeperator
    '                paymentLink += "amount_" + b.ToString + "=" + dr("PRICE").ToString + Globals.urlVarSeperator
    '                paymentLink += "quantity_" + b.ToString + "=" + dr("qty").ToString + Globals.urlVarSeperator
    '                b = b + 1
    '            Next
    '            strSQL = "SELECT CONTACT.FIRST_NAME, CONTACT.LAST_NAME, CONTACT.COMPANY, CONTACT.ADDRESS, CONTACT.ADDRESS2, CONTACT.CITY, STATE.SHORT_NAME AS STATE_PROVINCE," +
    '                    " CONTACT.POSTALCODE, CONTACT.PHONE, CONTACT.EMAIL FROM ORDERS INNER JOIN CONTACT ON ORDERS.CONTACTID = CONTACT.CONTACT_ID " +
    '                    " INNER JOIN  STATE ON CONTACT.STATE_PROVINCE = STATE.NAME WHERE ORDERS.ORDERID = @ORDERID"
    '            sqlParameters = New List(Of SqlParameter)
    '            sqlParameters.Add(New SqlParameter("@ORDERID", OrderID.ToString()))
    '            dsContact = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
    '            If dsContact.Tables(0).Rows.Count > 0 Then
    '                paymentLink += "lc=US" + Globals.urlVarSeperator

    '                For Each drr As DataRow In dsContact.Tables(0).Rows
    '                    paymentLink += "email=" + drr("Email").ToString + Globals.urlVarSeperator
    '                    paymentLink += "first_name=" + drr("FIRST_NAME").ToString + Globals.urlVarSeperator
    '                    paymentLink += "last_name=" + drr("LAST_NAME").ToString + Globals.urlVarSeperator
    '                    paymentLink += "city=" + drr("City").ToString + Globals.urlVarSeperator
    '                    paymentLink += "state=" + drr("STATE_PROVINCE").ToString + Globals.urlVarSeperator
    '                    paymentLink += "zip=" + drr("POSTALCODE").ToString + Globals.urlVarSeperator
    '                    paymentLink += "address1=" + drr("Address") + Globals.urlVarSeperator
    '                    paymentLink += "address2=" + drr("Address2").ToString + Globals.urlVarSeperator
    '                    paymentLink += "night_phone_a=" + drr("PHONE").ToString + Globals.urlVarSeperator

    '                Next


    '                ' *** Add Optional Variables to the Subscription Url ***
    '                paymentLink += "shipping_method=pickup" + Globals.urlVarSeperator   ' Force $0.00 shipping
    '                paymentLink += Globals.returnUrl + Globals.urlVarSeperator
    '                paymentLink += Globals.cancelReturnUrl

    '                buildPaymentLink = paymentLink.ToString
    '            End If
    '        End If
    '    End If
    'End Function

    Private Sub EShopperSave(ByVal ContactID As Long, ByVal sessionid As String, ByVal seriesid As String, ByVal masterproductid As String, ByVal productid As String, ByVal priceOverride As Decimal)
        Dim strSQL As String = ""
        Dim PROPERTYGROUPID As String = ""
        Dim GROUPID As String = ""
        Dim strQTY As Long = 0
        Dim dsProductExist As DataSet
        strSQL = "select GROUP_ID from PRODUCT where PRODUCTID=@PRODUCTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
        GROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
        strSQL = "select PROPERTYGROUPID from PRODUCT where PRODUCTID=@PRODUCTID"
        PROPERTYGROUPID = DbHelperSQL.GetSingle(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
        sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
        sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
        strSQL = "select * from ESHOPPER where PRODUCTID=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID and SESSIONID=@SESSIONID"
        dsProductExist = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

        sqlParameters = New List(Of SqlParameter)
        If dsProductExist.Tables(0).Rows.Count > 0 Then
            For Each drqty As DataRow In dsProductExist.Tables(0).Rows
                strQTY = drqty("QTY") + 1
            Next
            strSQL = "update Eshopper set qty=@QTY where PRODUCTID=@PRODUCTID and MASTERPRODUCTID=@MASTERPRODUCTID and SESSIONID=@SESSIONID"
            sqlParameters.Add(New SqlParameter("@QTY", strQTY.ToString()))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        Else
            strSQL = "insert into Eshopper (ContactID,sessionid,productid,qty,price,group_id,PRODUCT_SERIES_ID,MASTERPRODUCTID,Date_entered,PROPERTYGROUPID,PRICE_SAVED)"
            strSQL += " values (@CONTACTID,@SESSIONID,@PRODUCTID,1,@PRICEOVERRIDE,@GROUPID,@PRODUCTSERIESID,@MASTERPRODUCTID,@DATEENTERED,@PROPERTYGROUPID,@PRICESAVED)"
            sqlParameters.Add(New SqlParameter("@CONTACTID", ContactID.ToString()))
            sqlParameters.Add(New SqlParameter("@SESSIONID", sessionid))
            sqlParameters.Add(New SqlParameter("@PRODUCTID", productid))
            sqlParameters.Add(New SqlParameter("@PRICEOVERRIDE", priceOverride.ToString()))
            sqlParameters.Add(New SqlParameter("@GROUPID", GROUPID))
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", seriesid))
            sqlParameters.Add(New SqlParameter("@MASTERPRODUCTID", masterproductid))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            sqlParameters.Add(New SqlParameter("@PROPERTYGROUPID", PROPERTYGROUPID))
            sqlParameters.Add(New SqlParameter("@PRICESAVED", priceOverride.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        End If
    End Sub
#End Region
#End Region
End Class

Public Class Globals
    ' Paypal Subscription Variable Constants
    Public Const urlVarSeperator As String = "&"

    ' Specify the URL that the button will link to
    Public Const postUrl As String = "https://www.paypal.com/cgi-bin/webscr?"

    ' Paypal Sandbox test URL
    Public Const postUrlTEST As String = "https://www.sandbox.paypal.com/cgi-bin/webscr?"

    ' Specify a singley payment or donation
    Public Const cmdSinglePayment As String = "cmd=_cart"
    Public Const cmdUpload As String = "upload=1"
    Public Const cmdCurrency As String = "currency_code=USD"
    ' Specify the Paypal merchant account email address
    Public Const business As String = "business=ecommerce@lecroy.com"

    ' If set to "1," the payment will recur unless your customer cancels the 
    ' subscription before the end of the billing cycle. If omitted, the subscription 
    ' payment will not recur at the end of the billing cycle.
    Public Const recurring As String = "src=1"

    ' If set to "1," and the payment fails, the payment will be reattempted two more times. 
    ' After the third failure, the subscription will be cancelled. If omitted and the payment 
    ' fails, payment will not be reattempted and the subscription will be immediately cancelled.
    Public Const reattempt As String = "sra=1"

    ' The URL to which the customer's browser is returned after completing the payment; 
    ' for example, a URL on your site that displays a "Thank you for your payment" page. 
    ' Default: customer is taken to the PayPal website.
    Public Const returnUrl As String = "return=https://store.teledynelecroy.com"

    ' A URL to which the customer's browser is returned if payment is canceled; for example, 
    ' a URL on your website that displays a "Payment Canceled" page. 
    'Default: Browser is directed to the PayPal website.
    Public Const cancelReturnUrl As String = "cancel_return=https://store.teledynelecroy.com"
End Class