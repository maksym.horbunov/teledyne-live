﻿Partial Class Seminars_confirm
    Inherits BasePage
    Dim strRedir As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim strRedir As String = ""

        If Len(Request.QueryString("reg1")) > 0 Then
            strRedir = "reg1=y"
        End If
        If Len(Request.QueryString("reg2")) > 0 Then
            If Len(strRedir) > 0 Then
                strRedir = strRedir + "&reg2=y"
            Else
                strRedir = "reg2=y"
            End If
        End If
        If Len(Request.QueryString("reg3")) > 0 Then
            If Len(strRedir) > 0 Then
                strRedir = strRedir + "&reg3=y"
            Else
                strRedir = "reg3=y"
            End If
        End If

        If Len(strRedir) > 0 Then
            Session("RedirectTo") = rootDir + "/Support/TechLib/Seminar/confirm.aspx?" + strRedir
            If Not ValidateUser() Then
                Response.Redirect(rootDir + "/Support/user/")
            End If
            ValidateUserNoRedir()
            Initial()
        Else
            Response.Redirect(rootDir + "/Support/TechLib/Seminar/europe.aspx")
        End If
    End Sub

    Private Sub Initial()
        Dim RequestID As String = ""
        Dim strCRet As String = Chr(13) & Chr(10)
        Dim strBody As String = ""


        If Not Session("ContactID") Is Nothing And Not Session("Country") Is Nothing Then
            If Len(Session("ContactId")) > 0 Then
                If Len(Session("CampaignID")) = 0 Or Session("CampaignID") Is Nothing Then
                    Session("CampaignID") = 0
                End If
                If Len(Request.QueryString("reg1")) > 0 Then
                    RequestID = Functions.InsertRequest(Session("ContactId"), 6, 250, " ", "", 0, Session("CampaignID"))
                    strBody = strBody & "Switching Power Supply Seminar Sept-21-2011 Option1 9:00 Uhr: Power Supplies Seminar" & strCRet
                End If
                If Len(Request.QueryString("reg2")) > 0 Then
                    RequestID = Functions.InsertRequest(Session("ContactId"), 6, 251, " ", "", 0, Session("CampaignID"))
                    strBody = strBody & "Switching Power Supply Seminar Sept-21-2011 Option2 13:30 Uhr: High Resolution Oscilloscopes Seminar" & strCRet
                End If
                If Len(Request.QueryString("reg3")) > 0 Then
                    RequestID = Functions.InsertRequest(Session("ContactId"), 6, 252, " ", "", 0, Session("CampaignID"))
                    strBody = strBody & "Switching Power Supply Seminar Sept-21-2011 Option3: All Day Seminar" & strCRet
                End If

                If Len(strBody) > 0 And Len(Functions.EmailBodyClientData(Session("ContactId"), 1033)) > 0 Then
                    strBody = Functions.EmailBodyClientData(Session("ContactId"), 1033) + strCRet + strCRet + strBody
                End If
                Try
                    Functions.SendEmail(Session("country").ToString, strBody, "martin.roggo@teledynelecroy.com", "webmaster@teledynelecroy.com", "", "", "Web - Seminar Registration", "")
                Catch ex As Exception

                End Try

            End If
        End If
    End Sub
End Class
