﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/twocolumn-left.master" CodeBehind="emea-seminar-detail.aspx.vb" Inherits="LeCroy.Website.support_techlib_seminar_emea_seminar_detail" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server"><asp:Literal ID="menulabel" runat="server" /></asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server"><ul><asp:Literal ID="submenulabel" runat="server" /></ul></asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <div><asp:Image ID="imgHeader" runat="server" BorderWidth="0" /></div><br />
        <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
            <asp:Literal ID="litTopContent" runat="server" />
            <div style="font-weight: bold;">
                <asp:Label ID="lblSelect" runat="server" />
            </div><br />
            <div>
                <asp:RadioButtonList ID="rblLocations" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" />
                <asp:CustomValidator ID="cvLocation" runat="server" EnableClientScript="false" ValidationGroup="vgSubmit" />
                <asp:Panel ID="pnlNoEvents" runat="server" Visible="false">
                    <asp:Literal ID="litNoSeminars" runat="server" />
                </asp:Panel>
            </div><br />
			<asp:Literal ID="litBottomContent" runat="server" />
            <div>
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" ValidationGroup="vgSubmit" />&nbsp;&nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="false" />
            </div><br /><br />
        </asp:Panel>
        <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
            <p><asp:Literal ID="litSubmitted" runat="server" /></p><br /><br /><br /><br />
        </asp:Panel>
    </div>
</asp:Content>