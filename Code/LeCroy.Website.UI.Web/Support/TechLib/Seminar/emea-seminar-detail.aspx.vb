﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Public Class support_techlib_seminar_emea_seminar_detail
    Inherits BasePage

#Region "Variables/Keys"
    Private Const EMAIL_ADDRESS_FROM As String = "webmaster@teledynelecroy.com"
    Private Const REDIRECT_BACK_URL As String = "{0}/support/techlib/seminar/emea-seminar-detail.aspx?id={1}&selectedid={2}"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
    Private Const REQUEST_TYPE_ID As Int32 = 6
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindMenu()
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        lblSelect.ForeColor = Drawing.Color.Black
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            If Not (HasValidQueryStringParameters()) Then Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
            SubmitRequest(Int32.Parse(Request.QueryString("id")), Int32.Parse(rblLocations.SelectedValue), rblLocations.SelectedItem.Text)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
    End Sub

    Private Sub cvLocation_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvLocation.ServerValidate
        args.IsValid = True
        If (rblLocations.SelectedIndex < 0) Then
            args.IsValid = False
            lblSelect.ForeColor = Drawing.Color.Red
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindMenu()
        Dim captionId As String = GetMenuCaptionId()
        Dim menuId As String = GetMenuId()
        Me.menulabel.Text = Functions.LeftSubMenu(captionId, rootDir, pn_leftsubmenu, menuId, 658)
        lb_leftmenu.Text = Functions.LeftMenu(captionId, rootDir, pn_leftmenu, menuId)
    End Sub

    Private Function GetMenuCaptionId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("capid")) = 0 Then
            retVal = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                retVal = Request.QueryString("capid")
            Else
                retVal = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If
        Session("menuSelected") = retVal
        Return retVal
    End Function

    Private Function GetMenuId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("mid")) = 0 Then
            retVal = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                retVal = Request.QueryString("mid")
            Else
                retVal = AppConstants.TECH_LIB_MENU
            End If
        End If
        Return retVal
    End Function

    Private Sub BindPage()
        Me.submenulabel.Text = String.Empty
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False

        If Not (HasValidQueryStringParameters()) Then
            Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
        End If

        Dim seminar As Seminar = SeminarRepository.GetSeminarSimple(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Request.QueryString("id")))
        If (seminar Is Nothing) Then
            Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
        End If

        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminar.CmsOverviewXrefFkId.Value, 21)
        If (cmsOverview Is Nothing) Then
            Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
        End If

        Dim data As Dictionary(Of Int32, String) = GetRadioButtonMappings(cmsOverview.Keywords, seminar.LocaleId, seminar.CountryFkId)
        BindRadioButtonList(data, seminar.IsPerpetualSeminar)
        If (IsValidPostbackId(data)) Then
            Dim pageSeminarId As Int32 = Int32.Parse(Request.QueryString("id"))
            Dim selectedSeminarId As Int32 = Int32.Parse(Request.QueryString("selectedid"))
            SubmitRequest(pageSeminarId, selectedSeminarId, data(selectedSeminarId))
        End If

        imgHeader.ImageUrl = seminar.SeminarImagePath
        imgHeader.AlternateText = seminar.Title
        lblSelect.Text = Functions.LoadI18N("SLBCA0942", seminar.LocaleId)
        litNoSeminars.Text = Functions.LoadI18N("SLBCA0941", seminar.LocaleId)
        btnSubmit.Text = Functions.LoadI18N("SLBCA0306", seminar.LocaleId)
        btnCancel.Text = Functions.LoadI18N("SLBCA0944", seminar.LocaleId)
        litSubmitted.Text = Functions.LoadI18N("SLBCA0940", seminar.LocaleId)

        If Not (cmsOverview Is Nothing) Then
            Dim split As String() = cmsOverview.OverviewDetails.Split("|")
            litTopContent.Text = split(0)
            litBottomContent.Text = split(1)
        End If
    End Sub

    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("id") Is Nothing) Then
            Return False
        End If
        Dim testVal As Int32
        If Not (Int32.TryParse(Request.QueryString("id"), testVal)) Then
            Return False
        End If
        If Not (Request.QueryString("selectedid") Is Nothing) Then
            If Not (Int32.TryParse(Request.QueryString("selectedid"), testVal)) Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Function IsValidPostbackId(ByVal data As Dictionary(Of Int32, String)) As Boolean
        Dim testVal As Int32
        If (Int32.TryParse(Request.QueryString("selectedid"), testVal)) Then
            Return data.ContainsKey(testVal)
        End If
        Return False
    End Function

    Private Sub BindRadioButtonList(ByVal data As Dictionary(Of Int32, String), ByVal isPerpetual As Boolean)
        rblLocations.DataSource = data
        rblLocations.DataTextField = "Value"
        rblLocations.DataValueField = "Key"
        rblLocations.DataBind()
        If (isPerpetual) Then
            rblLocations.SelectedIndex = 0
        End If
    End Sub

    Private Sub SubmitRequest(ByVal pageSeminarId As Int32, ByVal selectedSeminarId As Int32, ByVal selectedLocation As String)
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir, pageSeminarId, selectedSeminarId)
        If Not (Me.ValidateUser()) Then
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()

        If Session("ContactID") Is Nothing Or Session("Country") Is Nothing Then
            Return
        End If

        Dim year As Int32 = DateTime.Now.Year
        Dim seminar As Specialized.SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), selectedSeminarId)
        If (seminar Is Nothing) Then Return
        If (seminar.Seminar Is Nothing) Then Return
        year = seminar.Seminar.DateOfSeminar.Year

        Functions.InsertRequest(Session("ContactId"), REQUEST_TYPE_ID, selectedSeminarId, " ", String.Empty, 0, Session("CampaignID"))
        Try
            Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
            replacements.Add("<#SeminarDetails#>", Functions.EmailBodyClientDataHtml(Session("ContactID"), seminar.Seminar.LocaleId))
            replacements.Add("<#SeminarLocation#>", selectedLocation)
            Dim emailTemplateDetail As EmailTemplateDetail = EmailTemplateDetailRepository.GetEmailTemplateDetailForEmailTemplateAndLocale(ConfigurationManager.AppSettings("ConnectionString").ToString(), 30, seminar.Seminar.LocaleId)
            If (emailTemplateDetail Is Nothing) Then
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 30, seminar.Seminar.LocaleId, "james.chan@teledynelecroy.com", replacements)
            End If
            If (emailTemplateDetail.InternalToAddress.Contains(",")) Then
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 30, seminar.Seminar.LocaleId, EMAIL_ADDRESS_FROM, emailTemplateDetail.InternalToAddress.Split(",").ToList(), New List(Of String), New List(Of String), replacements)
            Else
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 30, seminar.Seminar.LocaleId, emailTemplateDetail.InternalToAddress, replacements)
            End If
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = True
            replacements = New Dictionary(Of String, String)
            replacements.Add("<#SeminarYear#>", year.ToString())
            If (seminar.Seminar.IsPerpetualSeminar) Then
                replacements.Add("<#SeminarLocation#>", ".")
            Else
                Dim seminarDate As String = String.Format("{0}.{1}.{2}", UiUtilities.AppendLeadingZeroToInteger(seminar.Seminar.DateOfSeminar.Day, 10), UiUtilities.AppendLeadingZeroToInteger(seminar.Seminar.DateOfSeminar.Month, 10), seminar.Seminar.DateOfSeminar.Year)
                replacements.Add("<#SeminarLocation#>", String.Format(" am {0} in {1}.", seminarDate, seminar.Seminar.Location))
            End If
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminar.Seminar.EmailTemplateFkId.Value, seminar.Seminar.LocaleId, Session("Email").ToString(), replacements)
        Catch ex As Exception
            Dim x As String = ex.Message
        End Try
    End Sub

    Private Function GetRadioButtonMappings(ByVal keywords As String, ByVal localeId As Int32, ByVal countryId As Nullable(Of Int32)) As Dictionary(Of Int32, String)
        Dim dynamicSql As String = String.Empty
        Dim countryFilter As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)({New SqlParameter("@LOCALEID", localeId.ToString())})
        Dim split As String() = keywords.Split("|")
        For i = 0 To split.Length - 1
            dynamicSql += String.Format("[NAME] LIKE '{0}' OR ", split(i))
        Next
        dynamicSql = dynamicSql.Substring(0, dynamicSql.Length - 4)
        If (countryId.HasValue) Then
            countryFilter = String.Format(" AND [CountryFkId] = {0} ", countryId.Value)
        End If
        Dim sqlString As String = String.Format("SELECT * FROM [SEMINAR] WHERE [LOCALE_ID] = @LOCALEID AND [COMPANY_ID] = 1 AND ({0}) AND [DATE_OF_SEMINAR] >= GETDATE() {1} ORDER BY [DATE_OF_SEMINAR]", dynamicSql, countryFilter)
        Dim seminars As List(Of Seminar) = SeminarRepository.FetchSeminars(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (seminars.Count <= 0) Then
            pnlSubmitted.Visible = False
            pnlNoEvents.Visible = True
        End If
        Dim d As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
        For Each seminar As Seminar In seminars
            If Not (d.ContainsKey(seminar.SeminarId)) Then
                Dim seminarDate As String = String.Empty
                If (seminar.IsPerpetualSeminar) Then
                    seminarDate = seminar.Location
                Else
                    If (localeId = 2057) Then
                        seminarDate = String.Format("{0}-{1}-{2} {3}", UiUtilities.AppendLeadingZeroToInteger(seminar.DateOfSeminar.Day, 10), GetMonthString(seminar.DateOfSeminar.Month), seminar.DateOfSeminar.Year, seminar.Location)
                    Else
                        seminarDate = String.Format("{0}.{1}.{2} {3}", UiUtilities.AppendLeadingZeroToInteger(seminar.DateOfSeminar.Day, 10), UiUtilities.AppendLeadingZeroToInteger(seminar.DateOfSeminar.Month, 10), seminar.DateOfSeminar.Year, seminar.Location)
                    End If
                End If
                d.Add(seminar.SeminarId, seminarDate)
            End If
        Next
        Return d
    End Function

    Private Function GetMonthString(ByVal month As Int32) As String
        Select Case month
            Case 1
                Return "Jan"
            Case 2
                Return "Feb"
            Case 3
                Return "Mar"
            Case 4
                Return "Apr"
            Case 5
                Return "May"
            Case 6
                Return "Jun"
            Case 7
                Return "Jul"
            Case 8
                Return "Aug"
            Case 9
                Return "Sep"
            Case 10
                Return "Oct"
            Case 11
                Return "Nov"
            Case 12
                Return "Dec"
        End Select
        Return String.Empty
    End Function
#End Region
End Class