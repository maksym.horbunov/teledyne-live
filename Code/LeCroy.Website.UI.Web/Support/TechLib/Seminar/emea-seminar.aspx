﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/twocolumn-left.master" CodeBehind="emea-seminar.aspx.vb" Inherits="LeCroy.Website.support_techlib_seminar_emea_seminar" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server"><ul><asp:Literal  ID="menulabel" runat="server" /></ul></asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server"><ul><asp:Literal  ID="submenulabel" runat="server" /></ul></asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <h2>Available Seminars</h2>
        <p>NOTE: All seminars in Germany, Austria and in German-speaking Switzerland will be held in German.</p>
        <asp:Repeater ID="rptCountries" runat="server">
            <HeaderTemplate>
                <table border="0" cellpadding="0" cellspacing="3" width="600">
            </HeaderTemplate>
            <ItemTemplate>
                    <tr><td><h3><asp:Literal ID="litCountry" runat="server" /></h3></td></tr>
                    <asp:Repeater ID="rptEventsForCountry" runat="server">
                        <ItemTemplate>
                            <tr><td width="300"><asp:HyperLink ID="hlkEvent" runat="server" /></td></tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr><td><br /><hr /><br /></td></tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            
        </asp:Repeater>
    </div>
</asp:Content>