﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class support_techlib_seminar_emea_seminar
    Inherits BasePage

#Region "Variables/Keys"
    Private _seminarEmeaXrefs As List(Of SeminarEmeaXref) = New List(Of SeminarEmeaXref)
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindMenu()
            BindRepeater()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptCountries_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptCountries.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Country = CType(e.Item.DataItem, Country)
                Dim litCountry As Literal = CType(e.Item.FindControl("litCountry"), Literal)
                Dim rptEventsForCountry As Repeater = CType(e.Item.FindControl("rptEventsForCountry"), Repeater)

                litCountry.Text = row.Name
                AddHandler rptEventsForCountry.ItemDataBound, AddressOf rptEventsForCountry_ItemDataBound
                Dim kvp As List(Of KeyValuePair(Of String, Int32)) = New List(Of KeyValuePair(Of String, Int32))
                Dim seminarName As String = String.Empty
                For Each xref In _seminarEmeaXrefs.ToList().Where(Function(x) x.CountryFkId = row.CountryId).OrderBy(Function(x) x.SeminarTitle).ToList()
                    If (String.Compare(seminarName, xref.SeminarTitle, True) = 0) Then
                        Continue For
                    End If
                    seminarName = xref.SeminarTitle
                    kvp.Add(New KeyValuePair(Of String, Int32)(xref.SeminarTitle, xref.SeminarFkId))
                Next
                rptEventsForCountry.DataSource = kvp
                rptEventsForCountry.DataBind()
        End Select
    End Sub

    Private Sub rptEventsForCountry_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As KeyValuePair(Of String, Int32) = CType(e.Item.DataItem, KeyValuePair(Of String, Int32))
                Dim hlkEvent As HyperLink = CType(e.Item.FindControl("hlkEvent"), HyperLink)

                hlkEvent.Text = row.Key
                hlkEvent.NavigateUrl = String.Format("emea-seminar-detail.aspx?id={0}", row.Value)
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindMenu()
        Dim captionId As String = GetMenuCaptionId()
        Dim menuId As String = GetMenuId()
        Me.menulabel.Text = Functions.LeftSubMenu(captionId, rootDir, pn_leftsubmenu, menuId, 658)
        lb_leftmenu.Text = Functions.LeftMenu(captionId, rootDir, pn_leftmenu, menuId)
        Me.submenulabel.Text = String.Empty
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False
    End Sub

    Private Function GetMenuCaptionId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("capid")) = 0 Then
            retVal = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                retVal = Request.QueryString("capid")
            Else
                retVal = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If
        Session("menuSelected") = retVal
        Return retVal
    End Function

    Private Function GetMenuId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("mid")) = 0 Then
            retVal = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                retVal = Request.QueryString("mid")
            Else
                retVal = AppConstants.TECH_LIB_MENU
            End If
        End If
        Return retVal
    End Function

    Private Sub BindRepeater()
        _seminarEmeaXrefs = SeminarEmeaXrefRepository.FetchSeminarEmeaXrefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [SeminarEmeaXref]", New List(Of SqlParameter)().ToArray()).ToList()
        If (_seminarEmeaXrefs.Count <= 0) Then
            Response.Redirect("~/")
        End If

        Dim countries As List(Of Country) = CountryRepository.GetCountries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _seminarEmeaXrefs.Select(Function(x) x.CountryFkId).Distinct().ToList()).OrderBy(Function(x) x.Name).ToList()
        rptCountries.DataSource = countries
        rptCountries.DataBind()
    End Sub
#End Region
End Class