﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class support_techlib_seminar_europe_en
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim seminars As List(Of Seminar) = SeminarRepository.FetchSeminars(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [SEMINAR] WHERE [CmsOverviewXrefFkId] IN (3) AND [DATE_OF_SEMINAR] >= GETDATE()", New List(Of SqlParameter)().ToArray()).ToList()
        If (seminars.Count > 0) Then
            Response.Redirect(String.Format("~/support/techlib/seminar/emea-seminar-detail.aspx?id={0}", seminars.OrderBy(Function(x) x.DateOfSeminar).Select(Function(x) x.SeminarId).FirstOrDefault()))
        Else
            Response.Redirect("~/support/techlib/seminar/emea-seminar.aspx")
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class