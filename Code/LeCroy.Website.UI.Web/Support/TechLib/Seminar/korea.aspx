﻿<%@ Page Title="LeCroy - Seminars - LeCroy Seminar Series" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MasterPage.master" CodeBehind="korea.aspx.vb" Inherits="LeCroy.Website.Seminar_korea" %>
<%@ MasterType VirtualPath="~/MasterPage/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="myForm" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td width="7" class="contentPadding">
                    &nbsp;
                </td>
                <td valign="top" class="leftNav">
                    <div id="contentLeft">
                        <ul style="margin-top: 96px;">
                            <asp:Literal ID="lb_leftmenu" runat="server"></asp:Literal>
                            <asp:Panel ID="pn_leftmenu" runat="server">
                                <ul>
                                    <asp:Literal ID="menulabel" runat="server" Text=""></asp:Literal>
                                </ul>
                            </asp:Panel>
                            <asp:Panel ID="pn_leftsubmenu" runat="server">
                                <ul>
                                    <asp:Literal ID="submenulabel" runat="server" Text=""></asp:Literal>
                                </ul>
                            </asp:Panel>
                        </ul>
                    </div>
                </td>
                <td valign="top">
                    <div id="contentMiddle">
                        <div class="intro2">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td colspan="3">
                                        <h2>
                                            <b>
                                                <%= LeCroy.Website.Functions.LoadI18N("SLBCA0307", Session("localeID").ToString())%></b></h2>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left">
                                        <table cellspacing="0" cellpadding="0" width="370" border='0'>
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <img src='<%= rootDir%>/Images/linkarrow.gif' border='0'>
                                                </td>
                                                <td widh="120">
                                                    <nobr><asp:LinkButton ID="btnLNA" runat="server"><b>North America / South America</b></asp:LinkButton></nobr>
                                                </td>
                                                <tr>
                                                    <td valign="bottom" align="center">
                                                        <img src='<%= rootDir%>/Images/linkarrow.gif' border='0'>
                                                    </td>
                                                    <td widh="120">
                                                        <nobr><asp:LinkButton ID="btnEMEA" runat="server"><b>Europe</b></asp:LinkButton></nobr>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="bottom" align="center">
                                                        <img src='<%= rootDir%>/Images/linkarrow.gif' border='0'>
                                                    </td>
                                                    <td widh="120">
                                                        <nobr><a href="korea.aspx"><b>Korea</b></a></nobr>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="bottom" align="center">
                                                        <img src='<%= rootDir%>/Images/linkarrow.gif' border='0'>
                                                    </td>
                                                    <td widh="120">
                                                        <nobr><a href='/japan/seminar/default.asp'><b>Japan</b></a></nobr>
                                                    </td>
                                                </tr>
                                        </table>
                                        <br>
                                        <asp:HiddenField ID="hdnRegion" runat="server" Value="1033" />
                                        <%=LeCroy.Website.Functions.LoadI18N("SLBCA0533",localeID)%>
                                        <asp:Label ID="lb_LNA" runat="server"></asp:Label>
                                        <br>
                                    </td>
                                    <td valign="top">
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td valign="top" width="25">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left">
                                        <br>
                                    </td>
                                    <td valign="top">
                                    </td>
                                    <td valign="top" width="25">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" colspan="3">
                                        <asp:Label ID="lb_dbc" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
                <td valign="top" class="contentRight">
                    <div id="contentRight">
                        <div class="greyPadding">
                        </div>
                    </div>
                </td>
                <td width="7" class="contentPadding">
                    &nbsp;
                </td>
            </tr>
        </table>
    </form>
</asp:Content>