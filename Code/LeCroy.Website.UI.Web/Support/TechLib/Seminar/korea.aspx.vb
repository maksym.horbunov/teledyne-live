﻿Imports LeCroy.Library.VBUtilities

Public Class Seminar_korea
    Inherits BasePage

    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Master.SetLocaleId = 1042
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        localeid = "1042"
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
            Initial()
        End If
    End Sub

    Private Sub Initial()
        ShowLNA()
        ShowDBC()
        Dim sqlstr As String = ""
    End Sub

    Private Sub ShowLNA()
        Dim sqlstr As String = ""
        Dim ds As DataSet
        Dim result As String = ""
        sqlstr = "SELECT * FROM SEMINAR WHERE LOCALE_ID=1042 and TYPE='PUB'  and CANCELED_YN='n' and DATE_OF_SEMINAR>='" & FormatDateTime(Now(), vbShortDate) & "' order by DATE_OF_SEMINAR,SEMINAR_ID"
        ds = DbHelperSQL.Query(sqlstr)
        If ds.Tables(0).Rows.Count > 0 Then
            result = "<hr size=""1"" color=""#AAAAAA""><br><b>"
            result += "Korean Seminars</b><br><br>"
            result += "<table cellpadding=0 cellspacing=5 width=""500"" border=0 bgcolor=""#f0f0f0"">"
            For Each dr As DataRow In ds.Tables(0).Rows
                result += "<tr>"
                result += "<td valign=top class=""bodytext1"">"
                result += "<b>"
                Dim dateOfSeminar As DateTime = DateTime.Parse(dr("DATE_OF_SEMINAR"))
                Dim seminarDate As String = String.Format("{0}{1} {2}{3} {4}{5}", dateOfSeminar.Year, Functions.LoadI18N("SLBCA0887", "1042"), dateOfSeminar.Month, Functions.LoadI18N("SLBCA0888", "1042"), dateOfSeminar.Day, Functions.LoadI18N("SLBCA0889", "1042"))
                result += seminarDate
                result += "</b>"
                result += "</td>"
                result += "<td valign=top class=""bodytext1"" width=300>"
                result += "<b>"
                result += "<a href=korea.aspx" & menuURL2 & "#" & dr("SEMINAR_ID").ToString() & ">" & dr("NAME").ToString() & "</a>"
                result += "</b>"
                result += "</td>"
                result += "<td valign=top class=""bodytext1""><nobr>"
                result += "<b>"
                result += dr("SHORT_DESCRIPTION").ToString()
                result += "</b>"
                result += "</nobr></td>"
                result += "</tr>"
                result += "<tr><td valign=top colspan=3><hr></td></tr>"
            Next
            result += "</table>"
        End If
        lb_LNA.Text = result
    End Sub

    Private Sub ShowDBC()
        Dim sqlstr As String
        Dim ds As DataSet
        Dim result As String = ""
        sqlstr = "SELECT * FROM SEMINAR WHERE LOCALE_ID=1042 and TYPE='PUB'  and CANCELED_YN='n'  and DATE_OF_SEMINAR>='" & FormatDateTime(Now(), vbShortDate) & "' order by DATE_OF_SEMINAR,SEMINAR_ID"
        ds = DbHelperSQL.Query(sqlstr)
        On Error Resume Next
        If ds.Tables(0).Rows.Count > 0 Then
            result = "<table cellpadding=0 cellspacing=0 border=0 width='500'><tr><td colspan=2>"
            result += "<hr size=""1"" color=""#AAAAAA""><br><br></td></tr>"
            For Each rs As DataRow In ds.Tables(0).Rows
                result += "<tr border=1 dynamicanimation=""fpAnimformatRolloverFP1"" fprolloverstyle=""background-color: #ffd07b"" onmouseover=""rollIn(this);"" onmouseout=""rollOut(this);"">"
                result += "<td valign=top>"
                result += "<a name=" & rs("SEMINAR_ID") & ">"
                result += "</td>"
                result += "<td valign=top>"
                result += "<b>"
                Dim dateOfSeminar As DateTime = DateTime.Parse(rs("DATE_OF_SEMINAR"))
                Dim seminarDate As String = String.Format("{0}{1} {2}{3} {4}{5}", dateOfSeminar.Year, Functions.LoadI18N("SLBCA0887", "1042"), dateOfSeminar.Month, Functions.LoadI18N("SLBCA0888", "1042"), dateOfSeminar.Day, Functions.LoadI18N("SLBCA0889", "1042"))
                result += seminarDate & " - " & rs("SHORT_DESCRIPTION") & "<br><a href=seminar_info_korea.aspx?seminar_id=" & rs("SEMINAR_ID") & menuURL & ">" & rs("NAME") & "</a>"
                result += "</b>"
                result += "</td>"
                result += "</tr>"
                result += "<tr>"
                result += "<td valign=top>&nbsp;</td>"
                result += "<td valign=top>"
                result += "<b>"
                result += rs("TIME")
                result += "</b>"
                result += "</td>"
                result += "</tr>"
                result += "<tr>"
                result += "<td valign=top>&nbsp;</td>"
                result += "<td valign=top>"
                If CStr(Request.Form("type")) = "PRI" Then
                    result += "<b>"
                    result += rs("COMPANY") & ", " & rs("LOCATION")
                    result += "</b>"
                Else
                    result += "<b>"
                    result += rs("LOCATION")
                    result += "</b>"
                    If Not IsDBNull(rs("URL_LINK")) Then
                        If Len(rs("URL_LINK")) > 0 Then
                            result += " - <a href='" & rs("URL_LINK") & "'>" & LoadI18N("SLBCA0310", localeid) & "</a>"
                        End If
                    End If
                End If
                result += "<hr size=""1"" color=""#AAAAAA""><br><p><br>"
                result += "</td>"
                result += "</tr>"
            Next
            result += "</table>"
        Else
            result += "<b>"
            result += "<br>" & LoadI18N("SLBCA0321", localeid) & "<br><br>"
            result += "</b>"
        End If
        lb_dbc.Text = result
    End Sub
End Class