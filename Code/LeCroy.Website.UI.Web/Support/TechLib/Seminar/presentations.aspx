﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_presentations" Codebehind="presentations.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Import Namespace="LeCroy.Website.Functions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="JavaScript">
        function rollIn(el) {
            var ms = navigator.appVersion.indexOf("MSIE")
            ie4 = (ms > 0) && (parseInt(navigator.appVersion.substring(ms + 5, ms + 6)) >= 4)
            if (ie4) {
                el.initstyle = el.style.cssText; el.style.cssText = el.fprolloverstyle
            }
        }
        function rollOut(el) {
            var ms = navigator.appVersion.indexOf("MSIE")
            ie4 = (ms > 0) && (parseInt(navigator.appVersion.substring(ms + 5, ms + 6)) >= 4)
            if (ie4) {
                el.style.cssText = el.initstyle
            }
        }
    </script>
    <div class="intro">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr><td valign="top"><h2><b><asp:Literal ID="litOne" runat="server" /></b></h2><br /></td></tr>
            <tr><td valign="top">Seminar presentation slides are available for download.</td></tr>
            <tr>
                <td valign="top">
                    <br />Please enter a password provided to you by LeCroy representative.<br />
                    <table border="0" cellpadding="o" cellspacing="0">
                        <tr>
                            <td><asp:TextBox ID="lb_password" runat="server" Width="120" TextMode="Password" /></td>
                            <td valign="top"><nobr><asp:Button ID="btn_go" runat=server Text="Go" CausesValidation="False" /></nobr></td>
                        </tr>
                    </table><br /><br />
                </td>
            </tr>
            <tr><td valign="top" align="left"><asp:Label ID="lb_LNA" runat="server" /><br /></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>