﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Seminar_presentations
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim strSQL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars - Seminar Series"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0307", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        ValidateUserNoRedir()

        Session("RedirectTo") = rootDir + "/Support/TechLib/Seminar/presentations.aspx" 'here is the name of the file itself
        ValidateUserNoRedir()

        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

        End If
        Initial()

    End Sub

    Private Sub Initial()
        ShowLNA()
    End Sub

    Private Sub ShowLNA()
        Dim sqlstr As String = ""
        Dim ds As DataSet
        Dim result As String = ""


        If Len(lb_password.Text) > 0 Then
            strSQL = " SELECT DISTINCT SEMINAR.NAME, SEMINAR_PRESENTATION.URL, SEMINAR.DATE_OF_SEMINAR " +
              " FROM SEMINAR INNER JOIN  SEMINAR_PRESENTATION ON " +
              " SEMINAR.PRESENTATION_ID = SEMINAR_PRESENTATION.PRESENTATION_ID " +
              " WHERE (SEMINAR.PRESENTATION_PASSWORD = @PASSWORD AND POST='y') "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PASSWORD", lb_password.Text.ToString()))
            ' Response.Write(strSQL)
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Session("pwd") = lb_password.Text
                result = "<hr size=""1"" color=""#AAAAAA""><br><b>"
                result += "Seminars Presentations:</b><br><br>"
                result += "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">"
                For Each dr As DataRow In ds.Tables(0).Rows
                    result += "<tr dynamicanimation=""fpAnimformatRolloverFP1"" fprolloverstyle=""background-color: #ffd07b"" onmouseover=""rollIn(this);"" onmouseout=""rollOut(this);"">"
                    result += "<td valign=top width=30><img src=""/images/icons/icons_link.gif""></td>"
                    result += "<td valign=top width=30>"
                    result += "<font face=""Verdana, Arial"" size=2 color=""#008080""><b>"
                    result += dr("DATE_OF_SEMINAR")
                    result += "</b></font>"
                    result += "</td>"
                    result += "<td valign=top width=15>&nbsp;</td>"
                    result += "<td valign=top>"
                    result += "<a href='" & ConfigurationManager.AppSettings("AwsPublicBaseUrl") & dr("URL").ToString().ToLower() & "'>"
                    result += "<font face=""Verdana, Arial"" size=2 color=""#008080""><b>"
                    result += dr("NAME")
                    result += "</b></font></a><br><p><br>"
                    result += "</td>"
                    result += "</tr>"
                Next
                result += "</table>"
            Else
                result = "<strong>Sorry, the slides are not available.</strong>"
            End If
        Else
            result = "<strong>Sorry, the slides are not available.</strong>"
        End If
        lb_LNA.Text = result
    End Sub

    Protected Sub btn_go_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_go.Click

        ShowLNA()

    End Sub
End Class