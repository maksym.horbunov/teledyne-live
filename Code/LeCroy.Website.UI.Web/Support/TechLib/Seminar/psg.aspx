﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_Seminar_PSG" Codebehind="psg.aspx.vb" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
<% Response.Redirect("default.aspx") %>
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <h2 style="color: #0076C0;">Technology Seminar</h2><br />
        <div><b><i>Understand the new challenges faced by moving to higher speed technology.</i></b><br /><br />Reliable, accurate and repeatable measurements are fundamental to improving your designs. Flexibility, processing capabilities, and deep analysis can heighten confidence in your design and speed up your development processes.</div><br />
        <div>Our Seminars are FREE to attend and will focus on the Six Sessions described below. Each session provides a snapshot of individual technology and leveraging of the latest innovative and affordable test equipment technologies.</div><br />
        <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
            <div>
                <div style="padding-left: 10px;">
                    <span style="font-weight: bold;"><span style="color: #0076C0;">Session 1:</span> SAS/SATA</span><br /><ul><li style="list-style-type: square;">What's new, SAS 3.0 and SATA 3.1</li><li style="list-style-type: square;">SAS Protocol Suite</li><li style="list-style-type: square;">ST Automation Test Scripts</li><li style="list-style-type: square;">SATA Device Sleep &#8211; "DevSlp" (Passive DevSlp Emulation &amp; Monitoring)</li></ul><br />
	                <span style="font-weight: bold;"><span style="color: #0076C0;">Session 2:</span> PCI Express</span><br /><ul><li style="list-style-type: square;">PCI Express 3.0 and Dynamic Equalization</li><li style="list-style-type: square;">Storage over PCIe (NVMe, AHCI, PQI, SOP)</li><li style="list-style-type: square;">PCIe Protocol Suite, advanced triggering</li></ul><br />
	                <span style="font-weight: bold;"><span style="color: #0076C0;">Session 3:</span> DDR3/4</span><br /><ul><li style="list-style-type: square;">Introduction to DDR3/4, differences and changes</li><li style="list-style-type: square;">Analysis and Design Optimization</li><li style="list-style-type: square;">Design considerations for DDR3 and DDR4</li><li style="list-style-type: square;">Capturing Traces / Analysis</li></ul><br />
	                <span style="font-weight: bold;"><span style="color: #0076C0;">Session 4:</span> Fibre Channel and FCoE</span><br /><ul><li style="list-style-type: square;">Multi-Protocol Analysis</li><li style="list-style-type: square;">NET Protocol Suite, triggering and search</li><li style="list-style-type: square;">Jammer/Error Injection</li></ul><br />
	                <span style="font-weight: bold;"><span style="color: #0076C0;">Session 5:</span> USB</span><br /><ul><li style="list-style-type: square;">USB3.0 vs USB2.0</li><li style="list-style-type: square;">USB 3.0 Link Layer issues</li><li style="list-style-type: square;">Compliance Testing</li></ul><br />
	                <span style="font-weight: bold;"><span style="color: #0076C0;">Session 6:</span> Multi Protocol Cross Sync</span><br /><ul><li style="list-style-type: square;">CATC Sync Protocol</li><li style="list-style-type: square;">Latencies &amp; Errors</li><li style="list-style-type: square;">CrossSync SW</li></ul>
                </div>
            </div><br />
            <div>
                <span style="color: #0076C0; font-weight: bold;">Location:</span><br />Marriott<br />18000 Von Karman Ave.<br />Irvine, CA 92612-1004
            </div><br />
            <div>
                <span style="color: #0076C0; font-weight: bold;">Date/Time:</span><br />November 7 &amp; 8, 2013<br />10:00 AM to 4:00 PM
            </div><br />
            <div>
                <b>Please indicate below which sessions are of particular interest to you regarding the following Teledyne LeCroy Solutions:</b><br />
                <asp:CheckBox ID="cbxSession1" runat="server" Text="SAS/SATA" /><br />
                <asp:CheckBox ID="cbxSession2" runat="server" Text="PCI Express" /><br />
                <asp:CheckBox ID="cbxSession4" runat="server" Text="DDR3/4" /><br />
                <asp:CheckBox ID="cbxSession8" runat="server" Text="Fibre Channel and FCoE" /><br />
                <asp:CheckBox ID="cbxSession16" runat="server" Text="USB" /><br />
                <asp:CheckBox ID="cbxSession32" runat="server" Text="Multi Protocol Cross Sync" /><br /><br />
                <span style="vertical-align: top;">Special Topics of Interest:&nbsp;<asp:TextBox ID="txtAdditionalTopics" runat="server" Columns="50" MaxLength="500" Rows="4" TextMode="MultiLine" /></span>
            </div><br />
            <div>
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Register" ValidationGroup="vgSubmit" />&nbsp;<asp:CustomValidator ID="cvSelected" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSubmit" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
            <p>Thank you for your registration.<br /><asp:HyperLink runat="server" NavigateUrl="~/" Text="Return to our homepage" /></p><br /><br /><br /><br />
        </asp:Panel>
    </div>
</asp:Content>