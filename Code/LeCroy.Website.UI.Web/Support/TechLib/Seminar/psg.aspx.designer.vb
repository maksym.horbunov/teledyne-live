﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Support_TechLib_Seminar_PSG

    '''<summary>
    '''lb_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lb_leftmenu As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pn_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftmenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''menulabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents menulabel As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pn_leftsubmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftsubmenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''submenulabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents submenulabel As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pnlSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSubmit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cbxSession1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession1 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbxSession2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession2 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbxSession4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession4 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbxSession8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession8 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbxSession16 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession16 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbxSession32 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbxSession32 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtAdditionalTopics control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAdditionalTopics As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''cvSelected control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cvSelected As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''pnlSubmitted control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSubmitted As Global.System.Web.UI.WebControls.Panel
End Class
