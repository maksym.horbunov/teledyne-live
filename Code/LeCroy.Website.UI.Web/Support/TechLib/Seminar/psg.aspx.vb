﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Support_TechLib_Seminar_PSG
    Inherits BasePage

#Region "Variables/Keys"
    Private Const EMAIL_ADDRESS_FROM As String = "webmaster@teledynelecroy.com"
    Private Const EMAIL_ADDRESS_TO As String = "psg.seminars@teledynelecroy.com"
    Private Const EMAIL_BODY As String = "{0}{1}{1}{2}"
    Private Const EMAIL_SUBJECT As String = "Seminar Signup"
    Private Const LOCALE_ID As Int32 = 1033
    Private Const REDIRECT_BACK_URL As String = "{0}/support/techlib/seminar/psg.aspx"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
    Private Const REQUEST_TYPE_ID As Int32 = 6
    Private Const SEMINAR_ID As Int32 = 386
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Dim captionId As String = GetMenuCaptionId()
            Dim menuId As String = GetMenuId()
            Me.menulabel.Text = Functions.LeftSubMenu(captionId, rootDir, pn_leftsubmenu, menuId, 658)
            lb_leftmenu.Text = Functions.LeftMenu(captionId, rootDir, pn_leftmenu, menuId)
            Me.submenulabel.Text = String.Empty
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
            If (HasSessionParameters()) Then
                SubmitRequest()
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvSelected_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvSelected.ServerValidate
        Dim cbxs As List(Of CheckBox) = New List(Of CheckBox)({cbxSession1, cbxSession2, cbxSession4, cbxSession8, cbxSession16, cbxSession32})
        For Each cbx As CheckBox In cbxs
            If (cbx.Checked) Then
                args.IsValid = True
                Return
            End If
        Next

        args.IsValid = False
        cvSelected.ErrorMessage = "*Please select the session(s) you are interested in"
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            Session("PsgSeminarSignup") = BuildSessionVariable()
            SubmitRequest()
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Function GetMenuCaptionId() As String
        Dim retVal As String = String.Empty
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            retVal = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                retVal = Request.QueryString("capid")
            Else
                retVal = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If
        Session("menuSelected") = retVal
        Return retVal
    End Function

    Private Function GetMenuId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("mid")) = 0 Then
            retVal = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                retVal = Request.QueryString("mid")
            Else
                retVal = AppConstants.TECH_LIB_MENU
            End If
        End If
        Return retVal
    End Function

    Private Function HasSessionParameters() As Boolean
        If (Session("PsgSeminarSignup") Is Nothing) Then
            Return False
        End If
        Dim data As String = Session("PsgSeminarSignup").ToString()
        If (String.IsNullOrEmpty(data)) Then
            Return False
        End If
        Dim splitData As String() = data.Split("|")
        Dim testVal As Int32
        If Not (Int32.TryParse(splitData(0), testVal)) Then
            Return False
        End If
        Return True
    End Function

    Private Sub SubmitRequest()
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir)
        If Not (Me.ValidateUser()) Then
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()

        If Session("ContactID") Is Nothing Or Session("Country") Is Nothing Then
            Return
        End If

        Dim data As String() = Session("PsgSeminarSignup").ToString().Split("|")
        Dim bitmaskData As String = ParseBitmask(data(0))
        Dim remarks As String = String.Format("Selected sessions: {0}. Comments: {1}", bitmaskData, data(1))
        Functions.InsertRequest(Session("ContactId"), REQUEST_TYPE_ID, SEMINAR_ID, remarks, String.Empty, 0, Session("CampaignID"))
        Try
            Session.Remove("PsgSeminarSignup")
            Dim emailBody As String = String.Format(EMAIL_BODY, Functions.EmailBodyClientData(Session("ContactID"), LOCALE_ID), Environment.NewLine, remarks)
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = True
            GenerateExternalEmail(bitmaskData)
            Functions.SendEmailHTML(Session("Country").ToString(), emailBody, EMAIL_ADDRESS_TO, EMAIL_ADDRESS_FROM, String.Empty, String.Empty, EMAIL_SUBJECT, String.Empty)
        Catch
        End Try
    End Sub

    Private Function BuildSessionVariable() As String
        Dim cbxCount As Int32 = 0
        If (cbxSession1.Checked) Then cbxCount += 1
        If (cbxSession2.Checked) Then cbxCount += 2
        If (cbxSession4.Checked) Then cbxCount += 4
        If (cbxSession8.Checked) Then cbxCount += 8
        If (cbxSession16.Checked) Then cbxCount += 16
        If (cbxSession32.Checked) Then cbxCount += 32
        Return String.Format("{0}|{1}", cbxCount.ToString(), txtAdditionalTopics.Text)
    End Function

    Private Function ParseBitmask(ByVal selectedValues As Int32) As String
        Dim retVal As String = String.Empty
        If Not ((selectedValues And 32) = 0) Then retVal += "Multi Protocol Cross Sync, "
        If Not ((selectedValues And 16) = 0) Then retVal += "USB, "
        If Not ((selectedValues And 8) = 0) Then retVal += "Fibre Channel and FCoE, "
        If Not ((selectedValues And 4) = 0) Then retVal += "DDR3/4, "
        If Not ((selectedValues And 2) = 0) Then retVal += "PCI Express, "
        If Not ((selectedValues And 1) = 0) Then retVal += "SAS/SATA, "
        If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 2)
        Return retVal
    End Function

    Private Sub GenerateExternalEmail(ByVal bitmaskData As String)
        Dim seminar As Specialized.SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), SEMINAR_ID)
        If (seminar.Seminar Is Nothing) Then
            Response.Redirect("default.aspx")
        End If

        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#SeminarShortDescription#>", seminar.Seminar.ShortDescription)
        replacements.Add("<#SeminarTitle#>", seminar.Seminar.Name)
        replacements.Add("<#SeminarDate#>", seminar.Seminar.DateOfSeminar.ToShortDateString())
        Dim customText As String = String.Format("<strong>YOUR SELECTIONS</strong>: <br />{0}<br /><br />", bitmaskData)
        If Not (String.IsNullOrEmpty(txtAdditionalTopics.Text)) Then
            customText += String.Format("<strong>YOUR COMMENTS</strong>: <br />{0}<br /><br />", txtAdditionalTopics.Text.Trim())
        End If
        replacements.Add("<#SeminarCustomText#>", customText)
        replacements.Add("<#SeminarLocation#>", seminar.Seminar.Location)
        replacements.Add("<#SeminarAgenda#>", seminar.Seminar.Time)
        replacements.Add("<#SeminarUrl#>", String.Format("{0}/support/techlib/seminar/seminar_info.aspx?seminar_id={1}", ConfigurationManager.AppSettings("DefaultDomain"), SEMINAR_ID))

        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 22, LOCALE_ID, Session("Email").ToString(), replacements)
    End Sub
#End Region
End Class