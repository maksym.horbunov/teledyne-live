﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_questions" Codebehind="questions.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td valign="top">
                    <h2>Thank you for registering to attend our seminar. Please take a moment to help us by filling in the following short questionnarie.</h2><br /><br />
                    <table cellpadding="0" cellspacing="0" border="0" id="Table5">
                        <tr>
                            <td valign="top"><b>1.</b></td>
                            <td valign="top">&nbsp;<b>How many hours per day do you typically use an oscilloscope ?</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_1" runat="server">
                                    <asp:ListItem>I don’t use oscilloscopes</asp:ListItem>
                                    <asp:ListItem>Less than 1</asp:ListItem>
                                    <asp:ListItem>1-3</asp:ListItem>
                                    <asp:ListItem>3-5</asp:ListItem>
                                    <asp:ListItem>More than 5</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="12"><td colspan="2"></td></tr>
                        <tr>
                            <td valign="top"><b>2.</b></td>
                            <td valign="top">&nbsp;<b>What bandwidth(s) of oscilloscope(s) do you use ? (check all that apply)</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_2" runat="server">
                                    <asp:ListItem>Less than 199 MHz</asp:ListItem>
                                    <asp:ListItem>200 MHz to 499 MHz</asp:ListItem>
                                    <asp:ListItem>500 MHz to 2.9 GHz</asp:ListItem>
                                    <asp:ListItem>3 GHz to 8 GHz</asp:ListItem>
                                    <asp:ListItem>More than 8 GH</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="12"><td colspan="2"></td></tr>
                        <tr>
                            <td valign="top"><b>3.</b></td>
                            <td valign="top">&nbsp;<b>How often do you (or someone in your work group) acquire a new oscilloscope?</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_3" runat="server">
                                    <asp:ListItem>Usually we acquire one or more new scopes each year</asp:ListItem>
                                    <asp:ListItem>Typically we acquire a new scope each 1-3 years</asp:ListItem>
                                    <asp:ListItem>3-5 years between new scopes</asp:ListItem>
                                    <asp:ListItem>5-8 years between new scopes</asp:ListItem>
                                    <asp:ListItem>It is usually more than 8 years in between getting new oscilloscopes</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="12"><td colspan="2"></td></tr>
                        <tr>
                            <td valign="top"><b>4.</b></td>
                            <td valign="top">&nbsp;<b>Do you currently use MATLAB or Simulink?</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_4" runat="server">
                                    <asp:ListItem>YES</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="12"><td colspan="2"></td></tr>
                        <tr>
                            <td valign="top"><b>5.</b></td>
                            <td valign="top">&nbsp;<b>Do you current use the MathWorks Data Acquisition or Instrument Control Toolboxes?</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_5" runat="server">
                                    <asp:ListItem>YES</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="12"><td colspan="2"></td></tr>
                        <tr>
                            <td valign="top"><b>6.</b></td>
                            <td valign="top">&nbsp;<b>How did you find out about this seminar?</b></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:RadioButtonList ID="rbl_6" runat="server">
                                    <asp:ListItem>Email or Call from LeCroy</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;Email or Call from The MathWorks</asp:ListItem>
                                    <asp:ListItem>Lecroy Website</asp:ListItem>
                                    <asp:ListItem>The MathWorks website</asp:ListItem>
                                    <asp:ListItem>Other</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr><td colspan="2" align="center"><asp:Button ID="btn_go" runat="server" Text="SUBMIT" /></td></tr>
                    </table><br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>