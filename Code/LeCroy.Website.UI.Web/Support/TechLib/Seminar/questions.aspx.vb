﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Seminar_questions
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars - Seminar Series"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Initial()
        End If
    End Sub

    Private Sub Initial()
        If Session("ContactID") Is Nothing Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
    End Sub

    Protected Sub btn_go_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_go.Click
        If rbl_1.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#1');</script>")
            Return
        End If
        If rbl_2.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#2');</script>")
            Return
        End If
        If rbl_3.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#3');</script>")
            Return
        End If
        If rbl_4.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#4');</script>")
            Return
        End If
        If rbl_5.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#5');</script>")
            Return
        End If
        If rbl_6.SelectedIndex = -1 Then
            Response.Write("<script>alert('Please Answer Question#6');</script>")
            Return
        End If
        Dim sqlstr As String = ""
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "1"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_1.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "2"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_2.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "3"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_3.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "4"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_4.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "5"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_5.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())

        sqlstr = "INSERT INTO SEMINAR_SURVEY (QUESTION_ID, ANSWER, SEMINAR_ID, CONTACT_ID, DATE_ENTERED)  VALUES (@QUESTIONID,@ANSWER,@SEMINARID,@CONTACTID,@DATEENTERED)"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@QUESTIONID", "6"))
        sqlParameters.Add(New SqlParameter("@ANSWER", rbl_6.SelectedValue))
        sqlParameters.Add(New SqlParameter("@SEMINARID", "26"))
        sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
        DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, sqlParameters.ToArray())
        Response.Redirect("FinishRegistration.aspx" + menuURL2)
    End Sub
End Class