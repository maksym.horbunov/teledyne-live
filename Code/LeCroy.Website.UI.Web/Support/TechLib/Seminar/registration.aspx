﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_registration" Codebehind="registration.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Import Namespace="LeCroy.Website.Functions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="JavaScript">
        function MM_findObj(n, d) { //v3.0
            var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document); return x;
        }
        function MM_nbGroup(event, grpName) { //v3.0
            var i, img, nbArr, args = MM_nbGroup.arguments;
            if (event == "init" && args.length > 2) {
                if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
                    img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
                    if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
                    nbArr[nbArr.length] = img;
                    for (i = 4; i < args.length - 1; i += 2) if ((img = MM_findObj(args[i])) != null) {
                        if (!img.MM_up) img.MM_up = img.src;
                        img.src = img.MM_dn = args[i + 1];
                        nbArr[nbArr.length] = img;
                    }
                }
            } else if (event == "over") {
                document.MM_nbOver = nbArr = new Array();
                for (i = 1; i < args.length - 1; i += 3) if ((img = MM_findObj(args[i])) != null) {
                    if (!img.MM_up) img.MM_up = img.src;
                    img.src = (img.MM_dn && args[i + 2]) ? args[i + 2] : args[i + 1];
                    nbArr[nbArr.length] = img;
                }
            } else if (event == "out") {
                for (i = 0; i < document.MM_nbOver.length; i++) {
                    img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up;
                }
            } else if (event == "down") {
                if ((nbArr = document[grpName]) != null)
                    for (i = 0; i < nbArr.length; i++) { img = nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
                document[grpName] = nbArr = new Array();
                for (i = 2; i < args.length - 1; i += 2) if ((img = MM_findObj(args[i])) != null) {
                    if (!img.MM_up) img.MM_up = img.src;
                    img.src = img.MM_dn = args[i + 1];
                    nbArr[nbArr.length] = img;
                }
            }
        }
    </script>
    <script language="JavaScript">
        if (document.images) {
            FinishRegistration_1033 = new Image(136, 50); FinishRegistration_1033.src = "Images/FinishRegistration_1033.gif";
            FinishRegistration_f2_1033 = new Image(136, 50); FinishRegistration_f2_1033.src = "Images/FinishRegistration_f2_1033.gif";
            FinishRegistration_f3_1033 = new Image(136, 50); FinishRegistration_f3_1033.src = "Images/FinishRegistration_f3_1033.gif";
        }
    </script>
    <div class="intro">
        <asp:Panel ID="pn_userdata" runat="server">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td valign="top" width="500" align="left">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" id="Table1">
                            <tr><td valign="bottom" colspan="3"><h2><asp:Literal ID="litOne" runat="server" /></h2></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="500" align="left">
                        <asp:Label ID="lb_intro" runat="server" />
                        <asp:Label ID="lb_nameinfo" runat="server" /><br />
                        <strong><asp:Literal ID="litTwo" runat="server" />: </strong>
                        <asp:Label ID="lb_name" runat="server" /><br>
                        <strong><asp:Literal ID="litThree" runat="server" />: </strong>
                        <asp:Label ID="lb_DESCRIPTION" runat="server" /><asp:Label ID="lblCost" runat="server" Visible="false" /><br>
                        <strong><asp:Literal ID="litFour" runat="server" />: </strong>
                        <asp:Label ID="lb_DATE_OF_SEMINAR" runat="server" /><br>
                        <strong><asp:Literal ID="litFive" runat="server" />: </strong>
                        <asp:Label ID="lb_TIME" runat="server" /><br>
                        <strong><asp:Literal ID="litSix" runat="server" />: </strong>
                        <asp:Label ID="lb_loc" runat="server" /><br><br>
                        <asp:Button ID="btn_sub" runat="server" Text="Finish Registration >>" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="500" align="left">
                        <% If Not Session("FirstName") Is Nothing And Len(Session("FirstName")) > 0 Then%>
                        <hr size="1">
                        Please check your address, if you have moved or your address has changed, please click the <strong>Change Address Button </strong>below:<br>
                        <br>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td>
                                    <br><strong><%=Session("FirstName") & " " & Session("LastName")%></strong>
                                    <br>
                                    <% =Session("Company")%>
                                    <br>
                                    <% = Session("Address")%>
                                    <br>
                                    <%
                                        If Not Session("Address2") Is Nothing Then
                                            Response.Write(Session("Address2") & "<br>")
                                        End If
                                        Response.Write(Session("City"))
                                        If Not Session("State") Is Nothing Then
                                            Response.Write(", &nbsp; " & Session("State"))
                                        End If
                                        If Not Session("Zip") Is Nothing Then
                                            Response.Write(" ,&nbsp; " & Session("Zip"))
                                        End If
                                    %>
                                    <br>
                                    <%=Session("Country")%><br>
                                    <%  If Not Session("Phone") Is Nothing Then
                                            Response.Write(Session("Phone") & "<br>")
                                        End If
                                    %>
                                    <%= Session("Email")%><br>
                                    <asp:Button ID="btn_mod" runat="server" Text="" />
                                </td>
                            </tr>
                        </table>
                        <%End If%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>