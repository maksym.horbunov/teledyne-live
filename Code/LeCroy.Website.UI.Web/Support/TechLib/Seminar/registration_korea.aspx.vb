﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities

Partial Class seminar_registration_korea
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim siteMaster As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (siteMaster Is Nothing) Then siteMaster.SetLocaleId = 1042
        btn_sub.Text = Functions.LoadI18N("SLBCA0324", "1042")
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0307", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0077", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0312", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0069", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0313", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0314", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Initial()
        End If
    End Sub

    Private Sub Initial()
        If Not Request.QueryString("sid") Is Nothing Then
            Session("TempSeminarID") = Request.QueryString("sid")
        End If
        If Session("TempSeminarID") Is Nothing Then
            Response.Redirect("~/events/korea.aspx" + menuURL2)
        End If
        Session("RedirectTo") = rootDir + "/support/techlib/seminar/registration_korea.aspx" + menuURL2
        ValidateUserNoRedir()

        'If Len(Session("TempSeminarID")) > 0 And Len(Session("SeminarAfterReg")) = 0 Then
        '    If IsNumeric(Session("TempSeminarID")) Then
        '        If CInt(Session("TempSeminarID")) = 76 Then
        '            Response.Redirect("questions.aspx" + menuURL2)
        '        End If
        '    End If
        'End If
        btn_mod.Text = Functions.LoadI18N("SLBCA0061", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim introText As String = ""
        Dim seminarData As SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("TempSeminarID").ToString()))
        If Not (seminarData Is Nothing) Then
            If (String.Compare(seminarData.Seminar.SeminarFullYN, "Y", True) = 0) Then
                Response.Redirect(String.Format("~/support/techlib/seminar/seminar_info_korea.aspx?seminar_id={0}", Session("TempSeminarID").ToString()))
            End If
            If (String.Compare(seminarData.Seminar.SeminarType, "PRI", True) = 0) Then
                If Not Session("pwd") Is Nothing Then
                    If CStr(seminarData.Seminar.Password) <> CStr(Session("pwd")) Then
                        Response.Redirect("~/events/korea.aspx" + menuURL2)
                    End If
                Else
                    Response.Redirect("~/events/korea.aspx" + menuURL2)
                End If
                lb_loc.Text = seminarData.SeminarCompany.Name & ", " & seminarData.Seminar.Location
            Else
                lb_loc.Text = seminarData.Seminar.Location & "-<a href='" & rootDir & seminarData.Seminar.UrlLink & "'>" & Functions.LoadI18N("SLBCA0310", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "</A>"
            End If
            lb_name.Text = seminarData.Seminar.Name
            Session("SeminarName") = seminarData.Seminar.Name

            lb_DESCRIPTION.Text = seminarData.Seminar.Description
            lb_DATE_OF_SEMINAR.Text = seminarData.Seminar.DateOfSeminar.ToShortDateString()
            lb_TIME.Text = seminarData.Seminar.Time
            If Not Session("FirstName") Is Nothing And Len(Session("FirstName")) > 0 Then
                introText += "<br /><b>" & Session("FirstName") & "</b>,&nbsp;" & LoadI18N("SLBCA0316", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "<br><b>" &
                    seminarData.Seminar.Name & "</b>.<br><br>"
            Else
                introText += "<br />" & LoadI18N("SLBCA0316", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "<br><b>" &
                    seminarData.Seminar.Name & "</b>.<br><br>"
            End If
        End If
        introText += LoadI18N("SLBCA0317", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & "<br><br>"
        If CInt(Session("TempSeminarID")) = 76 Then
            introText += "<br/>By registering for this seminar, you are agreeing to receive future marketing information from Teledyne LeCroy and The Mathworks."
        End If
        introText += "<br/> " & LoadI18N("SLBCA0439", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lb_intro.Text = introText
    End Sub

    Protected Sub btn_sub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sub.Click
        Response.Redirect("requestregistration_korea.aspx?Object6=" & Session("TempSeminarID") + menuURL)
    End Sub

    Protected Sub btn_changeAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_mod.Click
        Response.Redirect(rootDir + "/support/user/userprofile.aspx")
    End Sub
End Class