﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class seminar_requestregistration_korea
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim siteMaster As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (siteMaster Is Nothing) Then siteMaster.SetLocaleId = 1042
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        ValidateUserNoRedir()
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""
        If Not Len(Session("CampaignID")) > 0 Then
            Session("CampaignID") = 0
        End If
        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Session("RedirectTo") = rootDir + "/support/techlib/seminar/requestregistration_korea.aspx" + menuURL2
        If Not ValidateUser() Then
            Response.Redirect(rootDir + "/Support/user/")
        End If
        Dim SeminarType As Boolean = False
        Dim SemRequest As Boolean = False
        Dim SelectionMadeByUser As Boolean = False
        Dim SemMaxAllowedSelection As Integer = 6
        Dim sAgent As String = ""
        Dim confirmationMsg As String = ""

        If Not Session("TempSeminarID") Then
            Dim seminarId As Int32
            If Not (Int32.TryParse(Session("TempSeminarID"), seminarId)) Then
                Response.Redirect("~/events/korea.aspx")
            End If
            Dim seminar As LeCroy.Library.Domain.Common.DTOs.Seminar = LeCroy.Library.DAL.Common.SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminarId).Seminar
            If Not (seminar Is Nothing) Then
                If (String.IsNullOrEmpty(seminar.SeminarFullYN)) Then
                    Response.Redirect(String.Format("~/support/techlib/seminar/seminar_info_korea.aspx?seminar_id={0}", seminarId))
                End If
                If (String.Compare(seminar.SeminarFullYN, "Y", True) = 0) Then
                    Response.Redirect(String.Format("~/support/techlib/seminar/seminar_info_korea.aspx?seminar_id={0}", Session("TempSeminarID").ToString()))
                End If
            End If

            Dim InsertedOk As Boolean = False
            sAgent = Request.ServerVariables("HTTP_USER_AGENT")
            InsertedOk = Functions.InsertRequest(Session("ContactId"), 6, Session("TempSeminarID"), "", "", Session("PageID"), Session("campaignid"))

            If Not Session("Email") Is Nothing And Len(Session("Email")) > 0 Then
                Dim strCRet As String = "<br />" 'Chr(13) & Chr(10)
                Dim strBody As String = ""

                confirmationMsg += Functions.LoadI18N("SLBCA0278", "1042") & "&nbsp;<b>"
                Dim seminarData As SeminarData = SeminarRepository.GetSeminar(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("TempSeminarID").ToString()))
                If Not (seminarData Is Nothing) Then
                    Dim seminarDate As DateTime = seminarData.Seminar.DateOfSeminar
                    Dim dateOf As String = String.Format("{0}{1} {2}{3} {4}{5}", seminarDate.Year, Functions.LoadI18N("SLBCA0887", "1042"), seminarDate.Month, Functions.LoadI18N("SLBCA0888", "1042"), seminarDate.Day, Functions.LoadI18N("SLBCA0889", "1042"))

                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    replacements.Add("<#SeminarLastName#>", Session("LastName").ToString())
                    replacements.Add("<#SeminarFirstName#>", Session("FirstName").ToString())
                    replacements.Add("<#SeminarTitle#>", seminarData.Seminar.Name)
                    replacements.Add("<#SeminarDate#>", dateOf)
                    replacements.Add("<#SeminarLocation#>", seminarData.Seminar.Location)
                    replacements.Add("<#SeminarTime#>", seminarData.Seminar.Time)
                    replacements.Add("<#SeminarYear#>", seminarDate.Year.ToString())
                    replacements.Add("<#SeminarMonth#>", seminarDate.Month.ToString())
                    replacements.Add("<#SeminarDay#>", seminarDate.Day.ToString())
                    replacements.Add("<#SeminarUrl#>", String.Format("{0}/support/techlib/seminar/seminar_info_korea.aspx?seminar_id={1}", ConfigurationManager.AppSettings("DefaultDomain").ToString(), Session("TempSeminarID").ToString()))

                    confirmationMsg += seminarData.Seminar.Name
                    Dim internalEmailBody As StringBuilder = New StringBuilder()
                    internalEmailBody.Append(String.Format("Seminars request for {0} on {1}<br /><br />", seminarData.Seminar.Name, seminarData.Seminar.DateOfSeminar.ToShortDateString()))
                    internalEmailBody.AppendLine(Functions.EmailBodyClientDataHtml(Session("ContactId"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
                    Try
                        'Functions.SendEmailHTML("Korea", strBody, Session("Email").ToString(), "seminars@teledynelecroy.com", "", "", String.Format("{0} {1}", dateOf, Functions.LoadI18N("SLBCA0886", "1042")), "")
                        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 6, 1042, Session("Email").ToString(), replacements)
                        Functions.SendEmailHTML("Korea", internalEmailBody.ToString(), "lecroy.marketing.korea@teledyne.com", "webmaster@teledynelecroy.com", String.Empty, String.Empty, "Seminars request", String.Empty)
                    Catch ex As Exception
                        Functions.ErrorHandlerLog(True, ex.Message + " Seminars Registration " + ex.Source)
                    End Try
                End If
                confirmationMsg += "</b>.<br /><br />" & Functions.LoadI18N("SLBCA0280", "1042") & " <strong> " & Session("Email") & "</strong>&nbsp;" & Functions.LoadI18N("SLBCA0281", "1042") & "<br /><br />"
                lb_confirm.Text = confirmationMsg
            End If
        Else
            Response.Redirect("~/events/korea.aspx")
        End If
    End Sub
End Class