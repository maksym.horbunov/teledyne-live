﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_seminar_info" Codebehind="seminar_info.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="p_cancelled" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="middle"><asp:Button ID="btn_b3" runat="server" Text="Meet our Speakers" /></td>
                <td valign="top" align="left" width="15"></td>
                <td valign="top" width="600">
                    &nbsp;&nbsp;<br /><br /><table width="100%" cellpadding="0" cellspacing="0" border="0" id="Table1">
                        <tr>
                            <td valign="top">
                                <%
                                    If CInt(Session("TempSeminarID")) = 210 Then
                                        Response.Write("<img border=""0"" src=""/images/misc/Logo_LeCroy-TheMathWorks.gif"">")
                                    Else
                                    End If
                                %>
                            </td>
                        </tr>
                        <tr><td valign="top"><h2><%=Title%></h2></td></tr>
                    </table>
                    <asp:Label ID="lblNote" runat="server" Visible="false" />
                    <h2><%=Description%></h2>
                    <asp:Label ID="lblPrice" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="230" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="Table2" style="padding-left: 10px;">
                        <tr>
                            <td valign="bottom" align="left">
                                <b><u><%=strDate%></u></b><br /><br />
                                <b>Agenda:</b><br /><br />
                                <%=strTime%><br />
                                <%If SeminarType = "PRI" Then%>
                                <%=Company%><br /><b><%=Location%></b>
                                <%Else%>
                                    <b><%=Location%></b>
                                <%End If%>
                                <%If Len(MapPath) > 0 Then %>
                                    <b><nobr><asp:HyperLink ID="hlkOne" runat="server" /></nobr></b>
                                <% End If%>
                            </td>
                            <td>&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td width="15">&nbsp;</td>
                <td valign="top" width="600">
                    <%If Len(LeCroy.Website.Functions.GetLongDescription("SEMINAR", "OUTLINE", Session("TempSeminarID"))) > 0 Then%>
                    <br /><b><asp:Literal ID="litOne" runat="server" /></b><br /><br />
                    <%= LeCroy.Website.Functions.GetLongDescription("SEMINAR", "OUTLINE", Session("TempSeminarID"))%><br />
                    <%End If
                        Response.Write("<br/>")
                        If Len(LeCroy.Website.Functions.GetLongDescription("SEMINAR", "Objectives", Session("TempSeminarID"))) Then
                    %>
                    <br /><b><asp:Literal ID="litTwo" runat="server" /></b><br /><br />
                    <%= LeCroy.Website.Functions.GetLongDescription("SEMINAR", "Objectives", Session("TempSeminarID"))%><br /><br />
                    <%End If
                        If Len(LeCroy.Website.Functions.GetLongDescription("SEMINAR", "TARGET_AUDIENCE", Session("TempSeminarID"))) > 0 Then
                    %>
                    <b><b><asp:Literal ID="litThree" runat="server" /></b></b><br /><br />
                    <%= LeCroy.Website.Functions.GetLongDescription("SEMINAR", "TARGET_AUDIENCE ", Session("TempSeminarID"))%><br /><br /><br />
                    <%End If
                        If Len(Info) > 0 Then%>
                    <br /><%=Info%><br /><br />
                    <%End If%>
                    <%  If flgSeminarFull Then
                            Response.Write("")
                        Else%><p><nobr>&nbsp;&nbsp;<asp:Button ID="btn_register" runat="server" Text="Register for Seminar >> " />&nbsp;</nobr></p>
                    <%End If%>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="p_cancelled_y" runat="server">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" align="center"><br /><br /><b>Thank you for your interest in <%=Title%>.<br />Unfortunately it has been cancelled due to scheduling conflicts.<br /></b><br /><br /><br /></td></tr>
    </table>
    </asp:Panel>
</asp:Content>