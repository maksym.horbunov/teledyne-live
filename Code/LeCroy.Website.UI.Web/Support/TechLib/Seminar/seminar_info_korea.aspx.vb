﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Partial Class seminar_seminar_info_korea
    Inherits BasePage
    'Public Shadows Title As String = ""
    Public Description As String = ""
    Public ShortDescription As String = ""
    Public strDayName As String = ""
    Public strDate As String = ""
    Public strTime As String = ""
    Public Location As String = ""
    Public Shadows MapPath As String = ""
    Public SeminarType As String = ""
    Public NumberOfSeats As String = ""
    Public CompanyID As String = ""
    Public Info As String = ""
    Public Company As String = ""
    Public flgSeminarFull As Boolean = False
    Public cancelled As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim siteMaster As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (siteMaster Is Nothing) Then siteMaster.SetLocaleId = 1042
        btn_register.Text = Functions.LoadI18N("SLBCA0235", "1042")
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hlkOne.NavigateUrl = MapPath
        hlkOne.Text = Functions.LoadI18N("SLBCA0310", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litOne.Text = Functions.LoadI18N("SLBCA0325", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0326", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0327", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        Dim captionID As String = Request.QueryString("capid")
        Dim subMenuID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        If Not Page.IsPostBack Then
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim strSQL As String = ""
        If Not Request.QueryString("seminar_id") Is Nothing And Len(Request.QueryString("seminar_id")) > 0 Then
            If Not IsNumeric(Request.QueryString("seminar_id")) Then
                Response.Redirect("~/events/korea.aspx" & menuURL2)
            End If
        Else
            Response.Redirect("~/events/korea.aspx" & menuURL2)
        End If

        If Session("TempSeminarID") Is Nothing Or Len(Session("TempSeminarID")) = 0 Then
            Session("TempSeminarID") = SQLStringWithOutSingleQuotes(Request.QueryString("seminar_id"))
        End If
        If Not Request.QueryString("seminar_id") Is Nothing And Len(Request.QueryString("seminar_id")) > 0 Then
            Session("TempSeminarID") = SQLStringWithOutSingleQuotes(Request.QueryString("seminar_id"))
        End If

        If Session("TempSeminarID") Is Nothing Then
            Session("pwd") = ""
            Response.Redirect("~/events/korea.aspx" & menuURL2)
        End If

        'strSQL = "SELECT a.speaker_id  FROM   seminar_speaker_xref a inner join seminar_speaker b on a.speaker_id=b.speaker_id WHERE a.seminar_id = " & Session("TempSeminarID")
        're = DbHelperSQL.ExecuteReader(strSQL)
        'If re.HasRows() Then
        '    re.Read()
        '    If re("speaker_id").ToString().Equals("") Then
        '        'btn_b3.Visible = False
        '    End If
        'Else
        '    'btn_b3.Visible = False
        'End If
        're.Close()

        strSQL = " SELECT SEMINAR.TITLE, SEMINAR.DESCRIPTION, SEMINAR.SHORT_DESCRIPTION," +
  " SEMINAR.DATE_OF_SEMINAR, SEMINAR.[TIME], SEMINAR.LOCATION, SEMINAR.TYPE,  SEMINAR.NUMBER_OF_SEATS," +
  " SEMINAR.COMPANY_ID, SEMINAR.INFO, SEMINAR.PASSWORD,SEMINAR.URL_LINK," +
  " SEMINAR.CANCELED_YN,SEMINAR_FULL_YN, SEMINAR_NOTE" +
  " FROM SEMINAR INNER JOIN  SEMINAR_COMPANY " +
  " ON SEMINAR.COMPANY_ID = SEMINAR_COMPANY.COMPANY_ID " &
  " where LOCALE_ID=1042 and SEMINAR_ID=@SEMINARID and TYPE='PUB'  and CANCELED_YN='n' and DATE_OF_SEMINAR>=@DATEOFSEMINAR"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@SEMINARID", Session("TempSeminarID").ToString()))
        sqlParameters.Add(New SqlParameter("@DATEOFSEMINAR", FormatDateTime(Now(), vbShortDate).ToString()))
        Dim seminars As List(Of Seminar) = SeminarRepository.FetchSeminars(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If (seminars.Count > 0) Then
            If (String.Compare(seminars.FirstOrDefault().SeminarType, "PRI", True) = 0) Then
                If Not Session("pwd") Is Nothing Then
                    If CStr(seminars.FirstOrDefault().Password) <> CStr(Session("pwd")) Then
                        Response.Redirect("~/events/korea.aspx" & menuURL2)
                    End If
                End If
            End If
            Description = seminars.FirstOrDefault().Description
            ShortDescription = seminars.FirstOrDefault().ShortDescription
            strDayName = seminars.FirstOrDefault().DateOfSeminar.DayOfWeek
            Dim eventDate As DateTime = seminars.FirstOrDefault().DateOfSeminar
            strDate = String.Format("{0}{3} {1}{4} {2}{5}", eventDate.Year, eventDate.Month, eventDate.Day, Functions.LoadI18N("SLBCA0887", "1042"), Functions.LoadI18N("SLBCA0888", "1042"), Functions.LoadI18N("SLBCA0889", "1042")) 'FormatDateTime(re("DATE"), DateFormat.ShortDate).ToString
            strTime = seminars.FirstOrDefault().Time
            Location = seminars.FirstOrDefault().Location
            MapPath = seminars.FirstOrDefault().UrlLink
            hlkOne.NavigateUrl = MapPath
            SeminarType = seminars.FirstOrDefault().SeminarType
            NumberOfSeats = seminars.FirstOrDefault().NumberOfSeats.ToString()
            CompanyID = seminars.FirstOrDefault().CompanyId.ToString()
            Info = seminars.FirstOrDefault().Info
            Company = SeminarCompanyRepository.GetSeminarCompany(ConfigurationManager.AppSettings("ConnectionString").ToString(), seminars.FirstOrDefault().CompanyId).Name
            If (String.Compare(seminars.FirstOrDefault().SeminarFullYN, "y", True) = 0) Then
                flgSeminarFull = True
                lblNote.Visible = True
                lblNote.Text = "<font color='red'><b>" + seminars.FirstOrDefault().SeminarNote + "</b></font>"
            End If
            cancelled = seminars.FirstOrDefault().CanceledYN
            If (String.Compare(cancelled, "n", True) = 0) Then
                p_cancelled.Visible = True
                p_cancelled_y.Visible = False
            Else
                p_cancelled.Visible = False
                p_cancelled_y.Visible = True
            End If
        Else
            Response.Redirect("~/events/korea.aspx" & menuURL2)
        End If
    End Sub

    'Protected Sub btn_b3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_b3.Click
    '    Response.Redirect("speakers.aspx" & menuURL2)
    'End Sub

    Protected Sub btn_register_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_register.Click
        Response.Redirect("registration_korea.aspx" & menuURL2)
    End Sub
End Class