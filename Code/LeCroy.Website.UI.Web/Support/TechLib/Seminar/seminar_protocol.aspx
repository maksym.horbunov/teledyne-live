﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_Seminar_Seminar_Protocol" Codebehind="seminar_protocol.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
            <div><h1 style="color: #ff8e00;">Workshop: Praktische Einf&#252;hrung in die USB 2.0 Protokoll- Analyse</h1></div>
            <p>
                <img src="https://teledynelecroy.com/images/mercury_main.png" align="right" alt="Teledyne LeCroy Mercury T2" border="0" />
                Der Universal Serial Bus - USB - ist die sicherlich am weitesten verbreitete Schnittstelle bei Computern und Peripherieger&#228;ten, dennoch stellt die Implementierung im Embedded-Bereich f&#252;r viele Anwender eine Herausforderung dar.<br /><br />
                Der Workshop vermittelt Einblicke in die Struktur und Analyse des USB Daten&#252;bertragungsprotokolls. Aufbauend auf einer Einf&#252;hrung in die theoretischen Grundlagen des Protokolls werden Sie schrittweise in einem praktischen Training die Aufzeichnung und Analyse der Daten&#252;bertragung erarbeiten.<br /><br />
                Jeder Teilnehmer erh&#228;lt hierzu einen Teledyne LeCroy Mercury T2 USB2.0 Analysator (in der Seminargeb&#252;hr beinhaltet) mit dem der praktische Teil absolviert wird.
            </p>
            <div>
                <span style="color: #0076C0; font-weight: bold;">Ziel des Seminars:</span><br />
                <p>Nach Absolvierung des Seminars sind Sie in der Lage mit den Teledyne LeCroy USB Protokoll-Analyser eigenst&#228;ndig USB &#220;bertragungsprotokolle aufzuzeichnen. Sie haben die wesentlichen Grundlagen der Daten&#252;bertragung auf USB Bussen verstanden und k&#246;nnen mit Hilfe der Software &#8222;USB Protocol Suite&#8220; das Protokoll analysieren.</p>
            </div><br />
            <div>
                <span style="color: #0076C0; font-weight: bold;">Kosten:</span><br />
                <p>
                    Die Teilnahmegeb&#252;hr betr&#228;gt <strong>999.- Euro / CHF 1.250.</strong>- netto zzgl. MWSt.<br />In der Geb&#252;hr enthalten sind die Kosten f&#252;r das Seminar, Mittagessen und Erfrischungsgetr&#228;nke. Zus&#228;tzlich erh&#228;lt jeder Teilnehmer einen Mercury T2 Standard USB 2.0 Protokollanalysator (Listenpreis 1.120.- Euro / CHF 1.400.- netto) sowie ein Teilnehmerzertifikat.<br /><br />
                    Als besonderes Angebot bieten wir Ihnen den Advisor T3 Standard USB 2.0 Protokollanalysator an. In Verbindung mit diesem Gerät betr&#228;gt die Teilnahmegeb&#252;hr 2.195.- Euro netto zzgl. MWSt. (Listenpreis 2.495.- Euro netto).
                </p>
            </div><br />
            <div style="color: #0076C0; font-weight: bold;"><asp:Label ID="lblSelect" runat="server" Text="Anmeldung:" /></div><br />
            <div>
                <asp:RadioButtonList ID="rblLocations" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" />
                <asp:CustomValidator ID="cvLocation" runat="server" EnableClientScript="false" ValidationGroup="vgSubmit" />
                <asp:Panel ID="pnlNoEvents" runat="server" Visible="false">
                    Neue Seminar-Termine in Vorbereitung.
                </asp:Panel>
            </div><br />
            <div>Bitte melden Sie sich rechtzeitig an, die Teilnehmerzahl ist begrenzt.</div><br />
            <div><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Abschicken" ValidationGroup="vgSubmit" /></div>
        </asp:Panel>
        <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
            <p>Vielen Dank f&#252;r Ihre Seminaranmeldung!</p><br /><br /><br /><br />
        </asp:Panel>
    </div>
</asp:Content>