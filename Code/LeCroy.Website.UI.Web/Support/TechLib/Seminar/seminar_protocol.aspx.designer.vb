﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Support_TechLib_Seminar_Seminar_Protocol

    '''<summary>
    '''lb_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lb_leftmenu As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pn_leftmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftmenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''menulabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents menulabel As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pn_leftsubmenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pn_leftsubmenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''submenulabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents submenulabel As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''pnlSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSubmit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblSelect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSelect As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblLocations control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblLocations As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''cvLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cvLocation As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''pnlNoEvents control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNoEvents As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSubmitted control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSubmitted As Global.System.Web.UI.WebControls.Panel
End Class
