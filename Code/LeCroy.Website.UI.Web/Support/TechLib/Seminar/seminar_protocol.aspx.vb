﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.UI.Common
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_Seminar_Seminar_Protocol
    Inherits BasePage

#Region "Variables/Keys"
    Private Const EMAIL_ADDRESS_FROM As String = "webmaster@teledynelecroy.com"
    Private Const EMAIL_ADDRESS_TO As String = "peter.wiegard@teledynelecroy.com"
    Private Const EMAIL_BODY As String = "Termine und Orte: {0}{1}{1}{2}"
    Private Const EMAIL_SUBJECT As String = "Anmeldung für Seminar"
    Private Const LOCALE_ID As Int32 = 1031
    Private Const REDIRECT_BACK_URL As String = "{0}/support/techlib/seminar/seminar_protocol.aspx?id={1}"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
    Private Const REQUEST_TYPE_ID As Int32 = 6
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Dim captionId As String = GetMenuCaptionId()
            Dim menuId As String = GetMenuId()
            Me.menulabel.Text = Functions.LeftSubMenu(captionId, rootDir, pn_leftsubmenu, menuId, 658)
            lb_leftmenu.Text = Functions.LeftMenu(captionId, rootDir, pn_leftmenu, menuId)
            Me.submenulabel.Text = String.Empty
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
            Dim data As Dictionary(Of Int32, String) = GetRadioButtonMappings()
            BindRadioButtonList(data)
            If (HasValidQueryStringParameters() AndAlso IsValidRequestId(data)) Then
                Dim seminarId As Int32 = Int32.Parse(Request.QueryString("id"))
                SubmitRequest(seminarId, data(seminarId))
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        lblSelect.ForeColor = Drawing.Color.Black
        Page.Validate("vgSubmit")
        If (Page.IsValid) Then
            SubmitRequest(Int32.Parse(rblLocations.SelectedValue), rblLocations.SelectedItem.Text)
        End If
    End Sub

    Private Sub cvLocation_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvLocation.ServerValidate
        args.IsValid = True
        If (rblLocations.SelectedIndex < 0) Then
            args.IsValid = False
            lblSelect.ForeColor = Drawing.Color.Red
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Function GetMenuCaptionId() As String
        Dim retVal As String = String.Empty
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            retVal = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                retVal = Request.QueryString("capid")
            Else
                retVal = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If
        Session("menuSelected") = retVal
        Return retVal
    End Function

    Private Function GetMenuId() As String
        Dim retVal As String = String.Empty
        If Len(Request.QueryString("mid")) = 0 Then
            retVal = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                retVal = Request.QueryString("mid")
            Else
                retVal = AppConstants.TECH_LIB_MENU
            End If
        End If
        Return retVal
    End Function

    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("id") Is Nothing) Then
            Return False
        End If
        Dim testVal As Int32
        If Not (Int32.TryParse(Request.QueryString("id"), testVal)) Then
            Return False
        End If
        Return True
    End Function

    Private Function IsValidRequestId(ByVal data As Dictionary(Of Int32, String)) As Boolean
        Dim testVal As Int32
        If (Int32.TryParse(Request.QueryString("id"), testVal)) Then
            Return data.ContainsKey(testVal)
        End If
        Return False
    End Function

    Private Sub BindRadioButtonList(ByVal data As Dictionary(Of Int32, String))
        rblLocations.DataSource = data
        rblLocations.DataTextField = "Value"
        rblLocations.DataValueField = "Key"
        rblLocations.DataBind()
    End Sub

    Private Sub SubmitRequest(ByVal seminarId As Int32, ByVal selectedLocation As String)
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir, seminarId.ToString())
        If Not (Me.ValidateUser()) Then
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()

        If Session("ContactID") Is Nothing Or Session("Country") Is Nothing Then
            Return
        End If

        Functions.InsertRequest(Session("ContactId"), REQUEST_TYPE_ID, seminarId, " ", String.Empty, 0, Session("CampaignID"))
        Try
            Dim emailBody As String = String.Format(EMAIL_BODY, selectedLocation, Environment.NewLine, Functions.EmailBodyClientData(Session("ContactID"), LOCALE_ID))
            Functions.SendEmail(Session("Country").ToString, emailBody, EMAIL_ADDRESS_TO, EMAIL_ADDRESS_FROM, String.Empty, String.Empty, EMAIL_SUBJECT, String.Empty)
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = True
            BuildAndSendConfirmationEmail(selectedLocation)
        Catch
        End Try
    End Sub

    Private Function GetRadioButtonMappings() As Dictionary(Of Int32, String)
        Dim sqlString As String = "SELECT * FROM [SEMINAR] WHERE [LOCALE_ID] = 1031 AND [COMPANY_ID] = 1 AND [NAME] = 'Workshop: Praktische Einf&#252;hrung in die USB 2.0 Protokoll- Analyse' AND [DATE_OF_SEMINAR] >= GETDATE() ORDER BY [DATE_OF_SEMINAR]"
        Dim seminars As List(Of Seminar) = SeminarRepository.FetchSeminars(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of System.Data.SqlClient.SqlParameter)().ToArray())
        If (seminars.Count <= 0) Then
            pnlSubmitted.Visible = False
            pnlNoEvents.Visible = True
            'Response.Redirect("~/events/") ' Do not let the page break, bounce them somewhere useful [seminar#europe bounces them back here]
        End If
        Dim d As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
        For Each seminar As Seminar In seminars
            If Not (d.ContainsKey(seminar.SeminarId)) Then
                Dim seminarDate As String = String.Format("{0}.{1}.{2} {3}", UiUtilities.AppendLeadingZeroToInteger(seminar.DateOfSeminar.Day, 10), UiUtilities.AppendLeadingZeroToInteger(seminar.DateOfSeminar.Month, 10), seminar.DateOfSeminar.Year, seminar.Location)
                d.Add(seminar.SeminarId, seminarDate)
            End If
        Next
        Return d
    End Function

    Private Sub BuildAndSendConfirmationEmail(ByVal selectedLocation As String)
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append("<html><head><title>Teledyne LeCroy</title></head><body><table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">Workshop: Praktische Einf&#252;hrung in die USB 2.0 Protokoll-Analyse</td></tr>")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">&nbsp;</td></tr>")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">Vielen Dank f&#252;r Ihre Seminar Anmeldung!<br /><br />")
        sb.Append("Gerne best&#228;tigen wir Ihnen hiermit Ihre Registrierung f&#252;r folgendes Seminar:<br /><br />")
        sb.Append(String.Format("{0}<br /><br />", selectedLocation))
        sb.Append("Sie erhalten in K&#252;rze weitere Informationen.<br /><br />Wir m&#246;chten Sie darauf hinweisen, dass bei nicht ausreichender Teilnehmerzahl das Seminar eventuell nicht stattfindet.<br /><br />")
        sb.Append("Mit freundlichen Gr&#252;&#223;en,</td></tr><tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">&nbsp;</td></tr>")
        sb.Append("<tr><td style=""color: #0076C0; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px; font-weight: bold;"">PETER WIEGARD</td></tr>")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">Regional Manager EMEA<br />Protocol Solution Group</td></tr>")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;""><a href=""https://teledynelecroy.com/""><img src=""https://teledynelecroy.com/images/tl_weblogo_blkblue_220x60_full.jpg"" alt=""Teledyne LeCroy"" border=""0"" /></a></td></tr>")
        sb.Append("<tr><td style=""color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;"">&nbsp;</td></tr></table></body></html>")
        Functions.SendEmailHTML(Session("Country").ToString(), sb.ToString(), Session("Email").ToString(), EMAIL_ADDRESS_FROM, String.Empty, String.Empty, "Seminar Anmeldebestätigung", String.Empty)
    End Sub
#End Region
End Class