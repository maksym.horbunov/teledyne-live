﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Seminar_speakers" Codebehind="speakers.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" id="Table1">
            <tr>
                <td valign="top" width="600" align="left">
                    <br /><table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr><td valign="top"><br /><b><nobr>Seminar Speakers</nobr></b></td></tr>
                    </table><br />
                </td>
            </tr>
            <tr><td valign="top" width="600" align="left"><asp:Label ID="lb_sp" runat="server" /><br /></td></tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>