﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Seminar_speakers
    Inherits BasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Technical Seminars"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Me.submenulabel.Text = ""

        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim result As String = ""
        Dim dsSpeaker As New DataSet
        Dim strSQL As String = ""
        Dim strAll As String = ""
        Dim SpeakerID As String = ""
        Dim Start As Integer = 0
        If Not Session("TempSeminarID") Is Nothing And Len(Session("TempSeminarID")) > 0 Then
            strSQL = "SELECT DISTINCT a.SPEAKER_ID, b.SPEAKER_NAME, b.PHOTO, b.SPEAKER_BIO " +
                     " FROM SEMINAR_SPEAKER_XREF AS a INNER JOIN SEMINAR_SPEAKER AS b " +
                     " ON a.SPEAKER_ID = b.SPEAKER_ID  WHERE a.seminar_id=@SEMINARID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SEMINARID", Session("TempSeminarID").ToString()))
            dsSpeaker = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())

            If dsSpeaker.Tables(0).Rows.Count > 0 Then
                result += "<table border=""0"" width=""100%"" cellspacing=""0"" cellpadding=""10"" >"
                For Each dr As DataRow In dsSpeaker.Tables(0).Rows
                    result += "<tr>"
                    result += "<td width=""90"" valign=""top"" align=""center"">"
                    result += "<img src=""" & dr("PHOTO").ToString() & """ border=1 width=90 bordercolor=""#000000"">"
                    result += "</td>"
                    result += "<td valign=""top"">"
                    result += "<b>"
                    result += dr("SPEAKER_NAME").ToString()
                    result += "</b><br>"
                    result += ""
                    result += dr("SPEAKER_BIO").ToString()
                    result += ""
                    result += "<hr size=1>"
                    result += "</td>"
                    result += "</tr>"
                Next
            Else
                result += "<tr><td>"
                result += "Speakers information is not available at this time"
                result += "</tr></td>"
            End If
            result += "</table>"
        End If
        lb_sp.Text = result
    End Sub
End Class