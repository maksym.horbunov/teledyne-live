﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_SignalIntegrityFundamentals" Codebehind="SignalIntegrityFundamentals.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <h2>Fundamentals of Signal Integrity</h2>
        <p>Today's world of gigabit per second transfer rates require engineers to combine simulation, modeling and measurement to in order to avoid signal integrity issues in their designs.</p>
        <p>The following videos and articles introduce fundamental signal integrity concepts and methods used to predict where signal integrity issues may become problematic in your designs, and to verify &amp; debug your high-speed serial data circuits.</p>
        <p>Teledyne LeCroy's <a href="/sda/"><strong>Serial Data Analyzers</strong></a>, <a href="/sparq/"><strong>Signal Integrity Network Analyzers</strong></a> and <a href="/sistudio/"><strong>Signal Integrity Studio Software</strong></a> include state-of-the-art acquisition and analysis tools to help you minimize the time required to pinpoint signal integrity issues. For a complete curriculum of video lectures on signal integrity design and analysis, visit the  <a href="http://www.bethesignal.com/" onclick="ga('send', 'event', 'OutboundLink', 'Signal Integrity Page TLSIA Link', 'www.bethesignal.com', {'nonInteraction': 1});"><strong>Teledyne LeCroy Signal Integrity Academy</strong></a> website.</p>
        <p>For a sample of the video lectures at the Teledyne LeCroy Signal Integrity Academy, check out:</p>
        <p><a href=" https://www.bethesignal.com/bogatin/videos-recorded-presentations-webinars-p-1051.html" onclick="ga('send', 'event', 'OutboundLink', 'Signal Integrity Page Sample Video', 'www.bethesignal.com', {'nonInteraction': 1});">Sample Video Lectures</a></p>
    </div>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>