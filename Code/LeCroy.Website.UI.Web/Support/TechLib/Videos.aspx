﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.tm_Library_Videos_Default" Codebehind="Videos.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="javascript" type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $('.click-check').click(function () {
                var idElement = $(this).get(0);
                var idName = $(idElement).attr('name');
                var description = $('#lit' + idName).val();
                var splitDescription = description.split('|');
                $('#<%= txtShareUrl.ClientID %>').text('https://teledynelecroy.com/support/techlib/videos.aspx?capid=106&mid=528&smid=662&docid=' + splitDescription[0]);
                setVideo(idName, splitDescription[1], splitDescription[2]);
            });
            $('#<%= txtShareUrl.ClientID %>').click(function () {
                if (!($('#<%= txtShareUrl.ClientID %>').focus())) {
                    setFocus();
                }
            });
            setFocus();
            $('#divCopyText').text(getOS());
        });

        function getOS() {
            var supportedOS = [{ string: navigator.platform, subString: "Win", identity: "Press Ctrl-C to copy" }, { string: navigator.platform, subString: "Mac", identity: "Press CMD-C to copy"}];
            for (var i = 0; i < supportedOS.length; i++) {
                var dataString = supportedOS[i].string;
                if (dataString) {
                    if (dataString.indexOf(supportedOS[i].subString) != -1)
                        return supportedOS[i].identity;
                }
            }
            return "";
        }

        function setFocus() {
            jQuery('#<%= txtShareUrl.ClientID %>').focus();
            jQuery('#<%= txtShareUrl.ClientID %>').select();
        }

        function setVideo(id, captionText, title) {
            if (id.length == 10) {
                jQuery('#ytapiplayer').empty();
                jQuery('#ytapiplayer').html('<script charset=\"ISO-8859-1\" src=\"\/\/fast.wistia.com\/assets\/external\/E-v1.js\" async><\/script><div class=\"wistia_embed wistia_async_' + id + '\" style=\"height:225px;width:400px;\">&nbsp;</div>');
            }
            else {
                jQuery('#ytapiplayer').empty();
                jQuery('#ytapiplayer').html('<iframe width="400" height="225" src="https://www.youtube.com/embed/' + id + '?rel=0" frameborder="0" allowfullscreen></iframe>');
            }
            jQuery('#captionText').html(captionText);
            jQuery('#docBreadCrumb').html('&nbsp;&gt;&nbsp;' + title);
            setFocus();
        }
    </script>

    <div class="intro2">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr><td colspan="2"><h1>Video Library</h1><a href="./videos.aspx">Videos</a><asp:HyperLink ID="hlkDocGroupBreadCrumb" runat="server" /><span id="docBreadCrumb"></span></td></tr>
            <tr><td colspan="2">&nbsp;</td></tr>
		    <tr>
			    <td width="450">
                    <div id="ytapiplayer" style="height: 225px; width: 400px;"><asp:Literal ID="litPlayerHolder" runat="server" /></div>
			    </td>
                <td>
                    <div id="captionText"></div><br />
                    <div class="clear"></div>
                    <div id="divCopy">
                        <strong>Share this video:</strong><br />
                        <asp:TextBox ID="txtShareUrl" runat="server" ReadOnly="true" Rows="3" TextMode="MultiLine" Width="263" onfocus="this.focus();this.select();" style="border: 0px; color: #008ECE; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px; outline: none; overflow: auto; resize: none;" />
                        <div id="divCopyText" style="vertical-align: top;"></div>
                    </div>
                </td>
		    </tr>
            <tr><td colspan="2"><br /><br /><hr width="100%" /></td></tr>
		    <tr>
			    <td colspan="2">
                    <div id="divDocumentGroup" runat="server" visible="true">
                        <asp:DataList ID="dlDocumentGroup" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table" Width="100%">
                            <ItemTemplate>
                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                    <tr class="sasr">
                                        <td valign="top" width="50"><asp:HyperLink ID="hlkVideoIcon" runat="server"><asp:Image ID="imgVideoIcon" runat="server" AlternateText="" BorderWidth="0" Height="50" Width="50" /></asp:HyperLink></td>
                                        <td valign="top" width="180">
                                            <table cellpadding="0" cellspacing="0" border="0" width="180" style="height:50px;">
                                                <tr><td valign="top"><strong><asp:HyperLink ID="hlkVideoText" runat="server" style="color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px;" /></strong></td></tr>
                                                <tr><td valign="bottom"><asp:HyperLink ID="hlkVideoCount" runat="server" /></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2">&nbsp;</td></tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div id="divVideoLibrary" runat="server" visible="false">
                        <asp:DataList ID="dlVideoLibrary" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table" Width="100%">
                            <ItemStyle Width="33%" />
                            <ItemTemplate>
                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                    <tr class="sasr">
                                        <td rowspan="2" valign="top" width="120"><asp:PlaceHolder ID="phDiv" runat="server" /></td>
                                        <td valign="top" width="110">
                                                <table cellpadding="0" cellspacing="0" border="0" width="110" style="height:90px;">
                                                    <tr><td><asp:HyperLink ID="hlkVideoDescription" runat="server" CssClass="click-check" NavigateUrl="#" style="color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px; cursor: pointer;" /></td></tr>
                                                    <tr><td valign="bottom"><asp:HyperLink ID="hlkVideoLength" runat="server" CssClass="click-check" NavigateUrl="#" style="color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 11px; font-weight:bold; cursor: pointer;" /></td></tr>
                                                </table>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2">&nbsp;</td></tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:Panel ID="pnlVideoDescriptions" runat="server" />
                    </div>
			    </td>
		    </tr>
	    </table>
        <script language="javascript" type="text/javascript"><asp:Literal ID="litJavascript" runat="server" /></script>
    </div><br /><br />
</asp:Content>