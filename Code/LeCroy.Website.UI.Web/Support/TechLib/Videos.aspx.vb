﻿Imports System.IO
Imports System.Net
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports Newtonsoft.Json

Partial Class tm_Library_Videos_Default
    Inherits BasePage

#Region "Variables/Keys"
    Private Const DOC_GROUP_URL_FORMAT As String = "videos.aspx?docgroupid={0}"
    Private Const HIDDEN_DESCRIPTION_FORMAT As String = "<input id=""{0}"" name=""{0}"" type=""hidden"" value=""{1}"" />"
    Private Const SHARE_URL_FORMAT As String = "{0}/support/techlib/videos.aspx?capid=106&mid=528&smid=662&docid={1}"
    Private Const WISTIA_IMAGE_URL_FORMAT As String = "http://fast.wistia.net/oembed?url=http://home.wistia.com/medias/{0}?embedType=async&videoWidth={1}"
    Private Const YOUTUBE_IMAGE_URL_FORMAT As String = "https://i2.ytimg.com/vi/{0}/default.jpg"
#End Region

#Region "Page Properties"
    Private _documentGroupCounts As Dictionary(Of Int32, Int32) = New Dictionary(Of Int32, Int32)
    Private ReadOnly Property DocumentGroupCounts As Dictionary(Of Int32, Int32)
        Get
            If (_documentGroupCounts.Count <= 0) Then
                _documentGroupCounts = FeDocumentManager.GetDocumentGroupVideoCount(ConfigurationManager.AppSettings("ConnectionString").ToString())
            End If
            Return _documentGroupCounts
        End Get
    End Property
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Video Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            SetupPageMenu()
            CheckQueryString()
            SetupVideos()
        End If
    End Sub
#End Region

#Region "Control Events"
    Protected Sub dlDocumentGroup_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlDocumentGroup.ItemDataBound
        Select Case (e.Item.ItemType)
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As DocumentGroup = CType(e.Item.DataItem, DocumentGroup)
                Dim hlkVideoIcon As HyperLink = CType(e.Item.FindControl("hlkVideoIcon"), HyperLink)
                Dim imgVideoIcon As Image = CType(e.Item.FindControl("imgVideoIcon"), Image)
                Dim hlkVideoText As HyperLink = CType(e.Item.FindControl("hlkVideoText"), HyperLink)
                Dim hlkVideoCount As HyperLink = CType(e.Item.FindControl("hlkVideoCount"), HyperLink)

                Dim url As String = String.Format(DOC_GROUP_URL_FORMAT, row.DocGroupId)
                hlkVideoIcon.NavigateUrl = url
                Dim imageId As String = String.Empty '"~/DoNotPublish.png"
                Try
                    ' NOTE: Assumes Wistia video ID length = 10, Youtube video ID length = 11
                    Dim id As String = FeDocumentManager.GetDefaultDocumentForDocumentGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), row.DocGroupId).FileFullPath
                    If (id.Length = 10) Then
                        imageId = GetThumbUrlFromWistia(String.Format(WISTIA_IMAGE_URL_FORMAT, FeDocumentManager.GetDefaultDocumentForDocumentGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), row.DocGroupId).FileFullPath, 400))
                    Else
                        imageId = String.Format(YOUTUBE_IMAGE_URL_FORMAT, FeDocumentManager.GetDefaultDocumentForDocumentGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), row.DocGroupId).FileFullPath)
                    End If
                Catch ex As Exception
                End Try
                imgVideoIcon.ImageUrl = imageId
                hlkVideoText.Text = row.DocGroupName
                hlkVideoText.NavigateUrl = url
                If (DocumentGroupCounts.ContainsKey(row.DocGroupId)) Then
                    Dim videoString As String = "video"
                    If (DocumentGroupCounts(row.DocGroupId) > 1) Then
                        videoString += "s"
                    End If
                    hlkVideoCount.Text = String.Format("{0} {1}", DocumentGroupCounts(row.DocGroupId).ToString(), videoString)
                    hlkVideoCount.NavigateUrl = url
                End If
        End Select
    End Sub

    Protected Sub dlVideoLibrary_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlVideoLibrary.ItemDataBound
        Select Case (e.Item.ItemType)
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim phDiv As PlaceHolder = CType(e.Item.FindControl("phDiv"), PlaceHolder)
                Dim hlkVideoDescription As HyperLink = CType(e.Item.FindControl("hlkVideoDescription"), HyperLink)
                Dim hlkVideoLength As HyperLink = CType(e.Item.FindControl("hlkVideoLength"), HyperLink)

                Dim div As HtmlGenericControl = New HtmlGenericControl()
                div.Attributes.Add("class", "click-check")
                div.ID = String.Format("{0}", row.FileFullPath)
                div.Attributes.Add("name", String.Format("{0}", row.FileFullPath))
                Dim img As Image = New Image()
                If (row.FileFullPath.Length = 10) Then
                    img.ImageUrl = GetThumbUrlFromWistia(String.Format(WISTIA_IMAGE_URL_FORMAT, row.FileFullPath, 120))
                Else
                    img.ImageUrl = String.Format(YOUTUBE_IMAGE_URL_FORMAT, row.FileFullPath)
                End If
                div.Controls.Add(img)
                phDiv.Controls.Add(div)
                hlkVideoDescription.Text = row.Title
                hlkVideoDescription.Attributes.Add("name", String.Format("{0}", row.FileFullPath))
                hlkVideoLength.Text = row.DocSize
                hlkVideoLength.Attributes.Add("name", String.Format("{0}", row.FileFullPath))
                AppendDescriptions(row.FileFullPath, row.DocumentId, row.Description, row.Title)
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub SetupPageMenu()
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("msmidid")
            End If
        End If

        Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
        lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
        pn_leftmenu.Visible = False
        pn_leftsubmenu.Visible = False
    End Sub

    Private Sub CheckQueryString()
        If Not (String.IsNullOrEmpty(Request.QueryString("DocId"))) Then
            Dim docId As Int32 = -1
            Integer.TryParse(Request.QueryString("DocId"), docId)
            If (docId <= 0) Then
                Response.Redirect("~/default.aspx")
            End If
            HandleVideoJS(FeDocumentManager.GetDefaultDocumentForDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), docId, AppConstants.VIDEOS_DOC_TYPE_ID))
        End If
    End Sub

    Private Sub SetupVideos()
        If Not (String.IsNullOrEmpty(Request.QueryString("DocGroupId"))) Then
            Dim docGroupId As Int32 = -1
            Integer.TryParse(Request.QueryString("DocGroupId"), docGroupId)
            Dim documentGroup As DocumentGroup = DocumentGroupRepository.GetDocumentGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), docGroupId)
            If Not (documentGroup Is Nothing) Then
                litPlayerHolder.Text = "Could not load video content"
                divDocumentGroup.Visible = False
                divVideoLibrary.Visible = True
                hlkDocGroupBreadCrumb.Text = String.Format("&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;{0}", documentGroup.DocGroupName)
                hlkDocGroupBreadCrumb.NavigateUrl = String.Format(DOC_GROUP_URL_FORMAT, docGroupId)
                dlVideoLibrary.DataSource = FeDocumentManager.GetDocumentsForGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), docGroupId)
                dlVideoLibrary.DataBind()

                If (String.IsNullOrEmpty(Request.QueryString("DocId"))) Then
                    HandleVideoJS(FeDocumentManager.GetDefaultDocumentForDocumentGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), docGroupId))
                End If
            Else
                Response.Redirect("~/default.aspx")
            End If
        Else
            dlDocumentGroup.DataSource = DocumentGroupRepository.GetDocumentGroupsByActive(ConfigurationManager.AppSettings("ConnectionString").ToString(), "Y")
            dlDocumentGroup.DataBind()

            If (String.IsNullOrEmpty(Request.QueryString("DocId"))) Then
                litPlayerHolder.Text = String.Empty
                Dim defaultDocId As Nullable(Of Int32) = FeDocumentManager.GetDefaultDocumentIdForDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString())
                If (defaultDocId.HasValue) Then
                    HandleVideoJS(FeDocumentManager.GetDefaultDocumentForDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), defaultDocId.Value, AppConstants.VIDEOS_DOC_TYPE_ID))
                End If
            End If
        End If
    End Sub

    Private Sub AppendDescriptions(ByVal videoId As String, ByVal docId As Int32, ByVal videoDescription As String, ByVal title As String)
        Dim controlId As String = String.Format("lit{0}", videoId)
        Dim control As Control = pnlVideoDescriptions.FindControl("controlId")
        If (control Is Nothing) Then
            Dim lit As Literal = New Literal()
            lit.Text = String.Format(HIDDEN_DESCRIPTION_FORMAT, controlId, String.Format("{0}|{1}|{2}", docId.ToString(), videoDescription.Replace("""", String.Empty).Replace("'", String.Empty), title))
            pnlVideoDescriptions.Controls.Add(lit)
        End If
    End Sub

    Private Sub HandleVideoJS(ByVal document As Document)
        If Not (document Is Nothing) Then
            If (document.DocTypeId = AppConstants.VIDEOS_DOC_TYPE_ID) Then
                litJavascript.Text = String.Format("setVideo('{0}', '{1}', '{2}');", document.FileFullPath, document.Description.Replace("""", String.Empty).Replace("'", String.Empty), document.Title)
                txtShareUrl.Text = String.Format(SHARE_URL_FORMAT, ConfigurationManager.AppSettings("DefaultDomain"), document.DocumentId)
                AppendDescriptions(document.FileFullPath, document.DocumentId, document.Description, document.Title)
            End If
        End If
    End Sub

    Private Function GetThumbUrlFromWistia(ByVal url As String) As String
        Try
            Dim httpRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpRequest.ContentType = "application/json"
            httpRequest.PreAuthenticate = False
            httpRequest.Method = "GET"
            Dim jsonData As String = String.Empty
            Using httpResponse As HttpWebResponse = CType(httpRequest.GetResponse(), HttpWebResponse)
                jsonData = New StreamReader(httpResponse.GetResponseStream()).ReadToEnd()
            End Using
            Return JsonConvert.DeserializeObject(Of WistiaObject)(jsonData).thumbnail_url
        Finally
        End Try
        Return String.Empty
    End Function
#End Region
End Class

Public Class WistiaObject
    Public thumbnail_url As String
End Class