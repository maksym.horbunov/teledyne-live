﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/twocolumn-left.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_TechLib_WebCasts" Codebehind="WebCasts.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/twocolumn-left.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro2">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td valign="top"><h1 style="font-weight: bold; font-size: 24px; color: #007ac3;">On Demand Webinars</h1>
        <h4>Browse these recorded webinars presented by Teledyne LeCroy technical experts.</h4></td>
                <td valign="top" align="right">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="right" valign="top">
                                <a href=" /events/?eventtypeid=4"><h1>View Upcoming Webinars &raquo;</h1></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="lbDetails" runat="server" />
    </div>
</asp:Content>