﻿Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_WebCasts
    Inherits BasePage
    Dim sql As String = ""
    Dim ds As DataSet
    Dim typeid As String = ""
    Dim catid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Webcast Archive"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        lbDetails.Text = getEvents()
        If Not Page.IsPostBack Then
            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
    End Sub
    Protected Function getEvents() As String
        Dim content As StringBuilder = New StringBuilder()

        Dim ds1 As DataSet
        Dim sql As String = ""
        sql = "SELECT *, DATEPART([day], EXHIBITIONS.exhibition_startdate) AS STARTDAY," +
             " DATEPART([day], EXHIBITIONS.exhibition_enddate) AS ENDDAY," +
             " COUNTRY.NAME AS COUNTRY FROM EXHIBITIONS INNER JOIN COUNTRY " +
             " ON EXHIBITIONS.country_id = COUNTRY.COUNTRY_ID " +
             " WHERE  (type_id =4) AND archive_yn='y' and archive_URL is not null ORDER BY EXHIBITIONS.exhibition_startdate desc"
        ds1 = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of SqlClient.SqlParameter)().ToArray())
        Dim num1 As Integer = 0
        num1 = ds1.Tables(0).Rows.Count
        If num1 > 0 Then
            content.Append("<table border=""0"" cellpadding=""10"" cellspacing=""0"" width=""100%"" >")

            For i As Integer = 0 To num1 - 1
                Dim dr1 As DataRow = ds1.Tables(0).Rows(i)


                content.Append("<tr height=10>")
                content.Append("<td  colspan=3 >")
                content.Append("<img src=""" + rootDir + "/Images/spacer.gif""  height=3 border=0>")
                content.Append("</td>")
                content.Append("</tr>")
                content.Append("<tr bgcolor=""#E5ECF9"">")
                ' content.Append("<td width=5><img src=""" + rootDir + "/Images/spacer.gif""  width=5 border=0></td>")
                content.Append("<td valign=top width=120 align=left><b>")
                content.Append("<nobr>")

                Dim startDate As DateTime = Convert.ToDateTime(dr1("exhibition_startdate").ToString())


                content.Append("<nobr>")
                content.Append(MonthName(startDate.Month.ToString(), True) + " " + startDate.Day.ToString() + " ," + Year(startDate).ToString)
                content.Append("</nobr>")

                content.Append("</nobr></b>")

                content.Append("</td>")
                ' content.Append("<td width=5><img src=""" + rootDir + "/Images/spacer.gif""  width=5 border=0></td>")
                content.Append("<td valign=top width=90>")
                If dr1("LOGO").ToString().Length > 0 Then
                    If dr1("archive_URL").ToString().Length > 0 Then
                        content.Append("<a href='" + dr1("archive_URL").ToString() + "'>")
                    End If

                    content.Append("<img src='" + rootDir + dr1("LOGO").ToString() + "' border=0 >")

                    If dr1("URL").ToString().Length > 0 Then
                        content.Append("</a>")
                    End If
                Else
                    content.Append("&nbsp;")
                End If
                content.Append("</td>")
                'content.Append("<td width=5><img src=""" + rootDir + "/Images/spacer.gif""  width=5 border=0></td>")
                content.Append("<td valign=top width=350>")
                content.Append("<table border=""0"" cellpadding=""2"" cellspacing=""0"" width=""290"">")
                content.Append("<tr>")
                content.Append("<td valign=top>")
                If dr1("archive_URL").ToString().Length > 0 Then
                    content.Append("<a href='" + dr1("archive_URL") + "'>")
                End If
                content.Append("<b>")

                content.Append(dr1("TITLE").ToString())


                content.Append("</b>")
                If dr1("archive_URL").ToString().Length > 0 Then
                    content.Append("</a>")
                End If
                content.Append("</td>")
                content.Append("</tr>")
                content.Append("<tr>")
                content.Append("<td valign=top>")
                content.Append("")
                content.Append(dr1("description").ToString())
                content.Append("</b>")
                content.Append("</td>")
                content.Append("</tr>")
                content.Append("</table>")
                content.Append("</td>")
            Next
            content.Append("</table><br><br><br>")
        Else
            content.Append("No Data")
        End If

        Return content.ToString()
    End Function
End Class