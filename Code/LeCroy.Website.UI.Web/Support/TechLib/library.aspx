﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="true" Inherits="LeCroy.Website.Support_TechLib_library" Codebehind="library.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
		<asp:Panel ID="pn_leftmenu" runat="server">
            <ul><asp:Literal  ID="menulabel" runat="server" /></ul>
        </asp:Panel>
		<asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal  ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_techlib.gif" alt="Technical Library"></p>
        <p>The <%=typeName %> section of the Teledyne LeCroy Technical Library lets you search for, browse, and print the latest technical documentation. A search aid allows you to filter documents by category or keyword.</p>
        <p><img src="<%=rootDir %>/images/icons/icons_pdf.gif" align="left" style="margin-right: 5px;"><%= typeName %> on this site are available in PDF format for easy download.</p>
        <p><h1><%=typeName %></h1></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td><strong>Search by Category:</strong></td>
                <td><strong>Search by Keyword(s):</strong></td>
            </tr>
            <tr>
                <td>&nbsp;<asp:DropDownList ID="catList" runat="server" AutoPostBack="True" /></td>
                <td>
                    <input name="" type="text" id="searchtext" maxlength="20" runat="server">
                    <input name="" type="submit" value="Search" id="Submit1" runat="server" onclick="return Submit1_onclick()">
                </td>
            </tr>
        </table>
    </div>
    <div class="tabs">
        <div class="bg">
            <div class="tab"><asp:Label ID="resultLB" runat="server" /></div>
            <div class="overview">
                <div class="overviewLeft"><strong><asp:Label ID="result1LB" runat="server" /></strong></div><br />
                <div style="text-align: right">
                    <asp:LinkButton ID="btn_pre" runat="server" Text="<img border='0' src='../../images/icons/icons_rgt_nav_arrow.gif'> Previous 10 results" />&nbsp;&nbsp;
                    <asp:LinkButton ID="btn_next" runat="server" Text="next 10 results <img border='0' src='../../images/icons/icons_lft_nav_arrow.gif'>" />
                </div>
                <div class="searchResults">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                        <PagerSettings Mode="NumericFirstLast" />
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <div class="cell">
                                        <h3><a href="/doc/docview.aspx?id=<%# Eval("document_id") %>"><%# Eval("title") %></a></h3><%# Eval("description") %>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function Submit1_onclick() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>