﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_TechLib_library
    Inherits BasePage
    Dim sql As String = ""
    Dim ds, dss, dsDoc As DataSet
    Public Shared typeName As String = ""
    Dim typeid As String = ""
    Dim catid As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If


        '** typeid
        If Len(Request.QueryString("type")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("type")) Then
                typeid = Request.QueryString("type")
            Else
                Response.Redirect("default.aspx")
            End If
        End If

        '** catid
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                catid = Request.QueryString("cat")
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        If Not Page.IsPostBack Then
            sql = "select * from document_type"
            ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, New List(Of SqlParameter)().ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    'content
                    If typeid <> "" And typeid = dr("DOC_TYPE_ID").ToString() Then
                        typeName = dr("NAME")
                        Me.submenulabel.Text += "<ul>"
                        sql = "select distinct d.category_id, d.category_name,d.SORT_ID from document a INNER JOIN " +
                        " document_type b ON  a.doc_type_id=b.doc_type_id INNER JOIN document_category c ON " +
                        " a.document_id = c.document_id INNER JOIN  category d  ON c.category_id=d.category_id " +
                        " WHERE a.doc_type_id=b.doc_type_id and a.country_id=208 and a.POST='Y' " +
                        " and b.doc_type_id=@TYPEID ORDER by d.SORT_ID"
                        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                        sqlParameters.Add(New SqlParameter("@TYPEID", typeid))
                        dss = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
                        Me.catList.DataSource = dss
                        Me.catList.DataTextField = "category_name"
                        Me.catList.DataValueField = "category_id"
                        Me.catList.DataBind()
                        Me.catList.Items.Insert(0, "ALL")
                        If dss.Tables(0).Rows.Count > 0 Then
                            For Each drr As DataRow In dss.Tables(0).Rows
                                If drr("category_id").ToString() = catid.ToString Then
                                    Me.submenulabel.Text += "<li class='current'><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                                Else
                                    Me.submenulabel.Text += "<li><a href='" + rootDir + dr("url").ToString() + "?type=" + dr("DOC_TYPE_ID").ToString() + "&cat=" + drr("category_id").ToString() + menuURL + "'>" + drr("category_name").ToString() + "</a></li>"
                                End If
                            Next
                        End If
                        Me.submenulabel.Text += "</ul>"
                    End If
                Next
            End If

            Me.menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False

            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(typeid)

            If catid <> Nothing And catid <> "" Then
                Me.catList.SelectedValue = catid
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(typeid) + " - " + Functions.getTitleForCat(catid)
            End If
            bind()
        End If


    End Sub
    Private Sub bind()
        If Len(typeid) > 0 Then
            Dim count As Integer = 0
            sql = "select distinct a.document_id, a.title, a.description,a.date_entered  " +
                    " from document a inner join document_type b on a.doc_type_id = b.doc_type_id " +
                    " inner join document_category c on a.document_id = c.document_id where a.country_id=208 and a.post='y' and b.doc_type_id=@TYPEID "
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@TYPEID", typeid))

            If Me.catList.SelectedIndex <> 0 Then
                sql += " and c.category_id = @CATEGORYID"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", Me.catList.SelectedValue))
            End If
            If Me.searchtext.Value <> "" Then
                Dim key As String = Me.searchtext.Value
                sql += " and (a.title like @TITLELIKE or a.description like @DESCRIPTIONLIKE)"
                sqlParameters.Add(New SqlParameter("@TITLELIKE", String.Format("%{0}%", SQLStringWithOutSingleQuotes(key))))
                sqlParameters.Add(New SqlParameter("@DESCRIPTIONLIKE", String.Format("%{0}%", SQLStringWithOutSingleQuotes(key))))
            End If
            sql += " order by a.date_entered desc"
            'Response.Write(sql)
            'Response.End()
            dsDoc = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sql, sqlParameters.ToArray())
            count = dsDoc.Tables(0).Rows.Count
            Me.resultLB.Text = "<span class='blue'>Search Results:</span> Your search found <b>" + _
            count.ToString() + "</b> matches. "
            Me.result1LB.Text = ""
            If count > 0 Then
                GridView1.DataSource = dsDoc
                GridView1.Visible = True
                GridView1.DataBind()
                Me.resultLB.Text += "Page <b>" + (Me.GridView1.PageIndex + 1).ToString() + _
                "</b> of <b>" + (Me.GridView1.PageCount).ToString() + "</b> pages."

                Me.result1LB.Text = "Results "
                If ((Me.GridView1.PageIndex + 1) = (Me.GridView1.PageCount)) Then
                    Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + count.ToString()
                Else
                    Me.result1LB.Text += ((Me.GridView1.PageIndex * 10) + 1).ToString() + "-" + (Me.GridView1.PageIndex * 10 + 10).ToString()
                End If

                '** Pager navigation
                If count > 10 Then
                    If GridView1.PageIndex.ToString().Equals("0") Then
                        btn_pre.Visible = False
                    Else
                        btn_pre.Visible = True
                    End If

                    If GridView1.PageIndex.ToString().Equals((GridView1.PageCount - 1).ToString()) Then
                        btn_next.Visible = False
                    Else
                        btn_next.Visible = True
                    End If
                Else
                    btn_next.Visible = False
                    btn_pre.Visible = False
                End If
            Else
                GridView1.Visible = False
                btn_next.Visible = False
                btn_pre.Visible = False
            End If

        End If
    End Sub

    Protected Sub catList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles catList.SelectedIndexChanged
        bind()
    End Sub

    Protected Sub Submit1_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Submit1.ServerClick
        bind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_next.Click
        Dim p As Integer = Me.GridView1.PageIndex + 1
        If p > Me.GridView1.PageCount Then
            Exit Sub
        End If
        Me.GridView1.PageIndex = p
        bind()
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        bind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pre.Click

        Dim p As Integer = Me.GridView1.PageIndex - 1

        If p.ToString().Equals("-1") Then
            Exit Sub
        End If
        Me.GridView1.PageIndex = p
        bind()
    End Sub
End Class