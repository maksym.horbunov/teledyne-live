﻿Partial Class Support_TechLib_registerPDF
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(False)
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.Cache.SetNoStore()
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1))
        Response.Cache.SetValidUntilExpires(True)
        Response.AppendHeader("Pragma", "no-cache")
        Response.Redirect(String.Format("~/doc/docview.aspx?id={0}", Request.QueryString("documentid")))
    End Sub
End Class