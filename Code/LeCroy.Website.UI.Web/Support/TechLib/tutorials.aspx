﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="tutorials.aspx.vb" Inherits="LeCroy.Website.Support_TechLib_tutorials" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <asp:Literal ID="menulabel" runat="server" />
                <asp:Label ID="Label1" runat="server" />
            </ul>
        </asp:Panel>
        <asp:Panel ID="pn_leftsubmenu" runat="server">
            <ul><asp:Literal ID="submenulabel" runat="server" /></ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_techlib.gif" alt="Technical Library" /></p>
        <p>Please select the tutorial of your preference.</p>
        <p><h1><%=typeName %></h1></p>
    </div>
    <div class="tabs">
        <div class="bg">
            <div class="tab"><asp:Label ID="resultLB" runat="server" /></div>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <div class="overview">
                    <div class="overviewLeft">
                        <strong><asp:Label ID="result1LB" runat="server" /></strong>
                    </div>
                    <div style="text-align: right">
                        <asp:LinkButton ID="btn_pre" runat="server" Text="<img border='0' src='/images/icons/icons_rgt_nav_arrow.gif'> Previous 10 results" />&nbsp;&nbsp;
                        <asp:LinkButton ID="btn_next" runat="server" Text="next 10 results <img border='0' src='/images/icons/icons_lft_nav_arrow.gif'>" />
                    </div>
                    <div class="searchResults">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="cell">
                                            <h3><asp:HyperLink ID="hlkDocumentTitle" runat="server" Target="_blank" /></h3>
                                            <asp:Literal ID="litDocumentDescription" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>