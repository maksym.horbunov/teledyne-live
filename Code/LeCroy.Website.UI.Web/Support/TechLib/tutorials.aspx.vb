﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Public Class Support_TechLib_tutorials
    Inherits BasePage

#Region "Variables/Keys"
    Public Shared typeName As String = String.Empty

    Dim typeid As String = String.Empty
    Dim docid As String = String.Empty
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim subMenuID As String = ""
        Dim captionID As String = ""
        Dim menuID As String = ""
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.TECH_LIB_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.TECH_LIB_MENU
            End If
        End If

        '** subMenuID
        If Len(Request.QueryString("smid")) > 0 Then
            If IsNumeric(Request.QueryString("smid")) Then
                subMenuID = Request.QueryString("smid")
            End If
        End If

        '** typeid
        If Len(Request.QueryString("type")) = 0 Then
            Response.Redirect("default.aspx")
        Else
            If IsNumeric(Request.QueryString("type")) Then
                typeid = Request.QueryString("type")
            Else
                Response.Redirect("default.aspx")
            End If
        End If

        '** catid
        If Len(Request.QueryString("cat")) > 0 Then
            If IsNumeric(Request.QueryString("cat")) Then
                Dim catid As String = Request.QueryString("cat")
            End If
        End If
        Dim menuURL As String = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL += "&smid=" + subMenuID
        Dim menuURL2 As String = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        menuURL2 += "&smid=" + subMenuID
        Session("menuSelected") = captionID
        Try
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + Functions.getTitleForType(typeid)
        Catch
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
        End Try

        If Not Page.IsPostBack Then
            Dim sqlString As String = "select * from document_type"
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, New List(Of SqlParameter)().ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If typeid <> "" And typeid = dr("DOC_TYPE_ID").ToString() Then
                        typeName = dr("NAME")
                    End If
                Next
            End If
            menulabel.Text = Functions.LeftSubMenu(captionID, rootDir, pn_leftsubmenu, menuID, subMenuID)
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
            pn_leftsubmenu.Visible = False
        End If
        If Len(Request.QueryString("docid")) Then
            If IsNumeric(Request.QueryString("docid")) Then
                docid = Request.QueryString("docid")
            Else
                Response.Redirect("default.aspx")
            End If
        End If
        If typeid <> "" And docid = "" Then
            BindGrid(typeid)
            Panel1.Visible = True
        End If
    End Sub
#End Region

#Region "Control Events"
    Protected Sub btn_pre_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_pre.Click
        Dim p As Integer = GridView1.PageIndex - 1
        If p < 0 Then
            Exit Sub
        End If
        GridView1.PageIndex = p
        BindGrid(typeid)
    End Sub

    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_next.Click
        Dim p As Integer = GridView1.PageIndex + 1
        If p > GridView1.PageCount Then
            Exit Sub
        End If
        GridView1.PageIndex = p
        BindGrid(typeid)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid(typeid)
    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim row As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim hlkDocumentTitle As HyperLink = CType(e.Row.FindControl("hlkDocumentTitle"), HyperLink)
            Dim litDocumentDescription As Literal = CType(e.Row.FindControl("litDocumentDescription"), Literal)
            hlkDocumentTitle.Text = row("Title").ToString()
            hlkDocumentTitle.NavigateUrl = String.Format("~/doc/docview.aspx?id={0}", row("DOCUMENT_ID").ToString())
            litDocumentDescription.Text = row("Description").ToString()
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindGrid(ByVal typeid As String)
        If Len(typeid) > 0 Then
            Dim count As Integer = 0
            Dim sqlString As String = "select distinct a.document_id, a.title, a.description, a.FILE_FULL_PATH, a.date_entered,sort_id " +
                                      " from document a " +
                                      " where a.country_id=208 and a.post='y' and a.doc_type_id=@DOCTYPEID order by a.sort_id" 'inner join document_category c on a.document_id = c.document_id " + _
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@DOCTYPEID", typeid))
            Dim dsDoc As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
            count = dsDoc.Tables(0).Rows.Count
            resultLB.Text = "There are <b>" + _
            count.ToString() + "</b>&nbsp;" + TypeName.ToString + "&nbsp;"
            result1LB.Text = String.Empty
            If count > 0 Then
                GridView1.DataSource = dsDoc
                GridView1.Visible = True
                GridView1.DataBind()
                resultLB.Text += "Page <b>" + (GridView1.PageIndex + 1).ToString() + "</b> of <b>" + (GridView1.PageCount).ToString() + "</b> pages."
                result1LB.Text = TypeName.ToString + "&nbsp;"
                If ((GridView1.PageIndex + 1) = (GridView1.PageCount)) Then
                    If ((GridView1.PageIndex * 10) + 1) = (count) Then
                        result1LB.Text += count.ToString()
                    Else
                        result1LB.Text += ((GridView1.PageIndex * 10) + 1).ToString() + "-" + count.ToString()
                    End If
                Else
                    result1LB.Text += ((GridView1.PageIndex * 10) + 1).ToString() + "-" + (GridView1.PageIndex * 10 + 10).ToString()
                End If

                '** Pager navigation
                If count > 10 Then
                    If GridView1.PageIndex.ToString().Equals("0") Then
                        btn_pre.Visible = False
                    Else
                        btn_pre.Visible = True
                    End If
                    If GridView1.PageIndex.ToString().Equals((GridView1.PageCount - 1).ToString()) Then
                        btn_next.Visible = False
                    Else
                        btn_next.Visible = True
                    End If
                Else
                    btn_next.Visible = False
                    btn_pre.Visible = False
                End If
            Else
                GridView1.Visible = False
                btn_next.Visible = False
                btn_pre.Visible = False
            End If
        End If
    End Sub
#End Region
End Class