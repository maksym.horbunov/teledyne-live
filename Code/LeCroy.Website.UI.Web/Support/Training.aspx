﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_Training" Codebehind="Training.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_training.gif" alt="Training"></p>
                <a href="/protocolanalyzer/austin-labs/" target="_blank"><img src="/images/austinlabs_training_logo.png" border="0" /></a>
        <a href="/protocolanalyzer/austin-labs/" onclick="GaEventPush('OutboundLink', 'Austin Labs Training');" target="_blank"><h1>Austin Labs Testing and Solutions</h1></a>
        <p>Austin Labs is a leading provider of testing and training services. We focus on server, storage, and networking interfaces and protocols. Our engineers and trainers are experts in SAS, SCSI, RAID, iSCSI, SATA, SAS, FC, FCoE, PCIe, and NVMe.</p>
        <p>Our engineers helped develop some of the industry’s key technologies and continue to have a vigorous passion for improving products and sharing their knowledge. This experience and enthusiasm translates into the highest quality testing and training services possible.</p>
        <a href="/protocolanalyzer/austin-labs/" onclick="GaEventPush('OutboundLink', 'Austin Labs Training');" target="_blank">Learn more about Austin Labs Testing and Solutions</a>
        <br /><br />
        <hr />
        <br /><br />
        <a href="http://www.beTheSignal.com/" onclick="GaEventPush('OutboundLink', 'www.beTheSignal.com');" target="_blank"><h1>Teledyne LeCroy Signal Integrity Academy</h1></a>
<p>A complete online curriculum of video lectures with Dr. Eric Bogatin.</p>
<p>Dr. Eric Bogatin, author of the book, Signal and Power Integrity Simplified, and hundreds of other papers and articles, distinguished lecturer with the IEEE EMC Society and invited lecturer worldwide, offers an entire curriculum of recorded video lessons on identifying and fixing signal integrity problems in your next design.</p>
<p>The SI Academy includes topics such as: transmission lines, return current, ground bounce, inductance, impedance profiles, differential impedance, via design, TDR, S-parameters, insertion and return loss, mode conversion, PDN, EMC design and much more. </p>
<p>If you missed one of Eric’s popular public classes, now is your chance to view them all.</p>
<p>Access all the training classes with an individual or corporate level annual subscription. A corporate subscription authorizes every engineer in a company to view all the classes.</p>
<p>View the many free videos and download the slides from many of Eric’s popular presentations and tutorials.</p>
<a href="http://www.beTheSignal.com/" onclick="GaEventPush('OutboundLink', 'www.beTheSignal.com');" target="_blank">Visit www.beTheSignal.com today!</a>
        <br /><br />
       <!-- removed JUL-19 TASK0174114 <hr />
        <br /><br />
        <a href="http://www.mindshare.com/" onclick="GaEventPush('OutboundLink', 'www.mindshare.com');" target="_blank"><img src="images/mindshare_logo.jpg" width="200" height="48" border="0" /></a>
        <p><a href="http://www.mindshare.com" onclick="GaEventPush('OutboundLink', 'www.mindshare.com');" target="_blank">MindShare</a> is a world renowned
            training and publishing company that enables high-tech companies to adopt, implement, and 
            roll-out technologies with greater speed and confidence. Since 1988, MindShare, Inc. has 
            provided innovative courses on a wide variety of subjects to numerous companies around the 
            world. MindShare was founded "out of sheer frustration with boring, dry technical classes 
            that had limited value to anybody". MindShare became the standard that delivers engaging 
            <a href="http://www.mindshare.com/learn/?section=11E3150A0A6B" onclick="GaEventPush('OutboundLink', 'www.mindshare.com/learn/?section=11E3150A0A6B');" target="_blank">classroom 
            training</a> delivered by instructors whose energy, experience and enthusiasm leaves the 
            student with a broader and deeper understanding of the technology. MindShare now goes beyond 
            the classroom to deliver engaging interactive 
            <a href="http://www.mindshare.com/shop/?section=132B1D1E" onclick="GaEventPush('OutboundLink', 'www.mindshare.com/shop/?section=132B1D1E');" target="_blank">eLearning training</a> 
            both in virtual classroom and online module format. MindShare also publishes 
            <a href="http://www.mindshare.com/shop/?section=132B067E" onclick="GaEventPush('OutboundLink', 'www.mindshare.com/shop/?section=132B067E');" target="_blank">books and eBooks</a> 
            on a wide range of technology subjects. MindShare truly lives up to its motto by "Bringing Life 
            to Knowledge".</p>
        <p><a href="http://www.mindshare.com/" onclick="GaEventPush('OutboundLink', 'www.mindshare.com');" target="_blank">MindShare</a> now partners with 
        <a href="https://teledynelecroy.com" target="_blank">Teledyne LeCroy</a> to deliver training in 3 technology areas:</p>
        <ul type="disc">
            <li>PCI Express Architecture</li>
            <li>USB Architecture</li>
            <li>DRAM ARchitecture</li>
        </ul>
        <p><a href="http://www.mindshare.com/shop/?section=07001710" onclick="GaEventPush('OutboundLink', 'www.mindshare.com/shop/?section=07001710');">View or register</a> for public 
        courses in PCI Express, USB or DRAM architecture. To receive a 20% discount on these public courses, please use the coupon code "LeCroy" at checkout.</p>
        <br /><br />
        <hr />
        <br /><br />
        <a href="http://www.dashcourses.com/" onclick="GaEventPush('OutboundLink', 'www.dashcourses.com');" target="_blank"><img src="images/Dashcourses.jpg" width="200" height="48" border="0" /></a>
        <p>
            Dashcourses leads the way in hands-on training for Technology Professionals. Since
            1990, Dashcourses leadership has been actively involved in providing training and
            technology services to globally based companies and government agencies.</p>
        <p>
            Now more then ever, students need training environments that facilitate the learning
            process by providing students with real-time hands-on training environments under
            the guidance of expert instructors with real-world experience. Dashcourses believes
            that students learn best by doing and all courses provide a hands-on learning environment
            where students learn and apply what they have learned all at the same time. With
            the highest commitment to providing the most effective, high quality education for
            Technology Professionals, Dashcourses standards for Instructor Selection and Training
            are key to ensuring that your training dollars produce results.</p>
        <p>
            In the area of Storage and I/O Technology, Dashcourses provides leading edge coverage
            on PCI Express, PCI, PCI-X, USB, Wireless USB, Serial Attached SCSI (SAS), Serial
            ATA (SATA), InfiniBand, and the NEWLY RELEASED series on Virtualization, including
            PCIe IOV. In addition to courses on Storage and I/O technologies, training in the
            same expert hands-on fashion is available on CPU抯, Networking, Operating Systems,
            and Applications such as Oracle, Cisco, WebSphere, and WebLogic, to name a few.</p>
        <p>
            In addition to on-site classes, Dashcourses now offers Public Courses at the Teledyne LeCroy
            facility in Santa Clara. To learn more about the Teledyne LeCroy/Dashcourses Public Course
            Schedule, visit <a href="http://dashcourses.com/public-courses.html" onclick="GaEventPush('OutboundLink', 'dashcourses.com/public-courses.html');">http://dashcourses.com/public-courses.html</a>.
            Courses are also offered in a live and on-demand e-learning format. Live Online
            classes allow customers to have students participate in training from a variety
            of locations and eliminates the training costs that are typically associated with
            on-site instructor-led training. The On-Demand format consists of a pre-recorded
            lecture that is accessible to students from any internet connected PC giving students
            much more flexibility to choose when they learn and what they want to learn.</p>
        <p>
            Dashcourses uses Teledyne LeCroy analyzers in all of its Storage and I/O courses. Students
            break into groups to use the analyzers, analyze traces, and work together on projects
            designed to reinforced the training materials and provide students with the real-world
            situations that will enable them to take their learning and put it into action when
            they return to work. To learn more about Dashcourses training events, visit <a href="http://www.dashcourses.com" onclick="GaEventPush('OutboundLink', 'www.dashcourses.com');">
            www.Dashcourses.com</a> for all your training needs.</p>--><br /><br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources"></div>
    <div class="divider"></div>
    <div class="resources"></div>
</asp:Content>