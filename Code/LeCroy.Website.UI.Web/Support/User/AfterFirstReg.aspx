﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_User_AfterFirstReg" Codebehind="AfterFirstReg.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="default.aspx"><asp:Literal ID="litOne" runat="server" /></a></li>
        <li class="current"><a href="#"><asp:Literal ID="litTwo" runat="server" /></a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <asp:Literal ID="litThree" runat="server" />
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="rblAfterReg" Text="どれか一つ、お選びください。" SetFocusOnError="True" ValidationGroup="vgAfterReg" Font-Bold="True" ForeColor="#CC3300" /><br />
        <asp:RadioButtonList ID="rblAfterReg" runat="server" /><br /><br />
        <asp:Literal ID="litFour" runat="server" /><br />
        <asp:Button ID="btn_next" runat="server" CausesValidation="true" Text="Next" ValidationGroup="vgAfterReg" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources">
        <div class="blue"><asp:Literal ID="litFive" runat="server" /></div>
        <ul>
            <li class="link"><asp:Literal ID="litSix" runat="server" /></li>
            <li class="link"><asp:Literal ID="litSeven" runat="server" /></li>
            <li class="link"><asp:Literal ID="litEight" runat="server" /></li>
            <li class="link"><asp:Literal ID="litNine" runat="server" /></li>
        </ul>
    </div>
</asp:Content>