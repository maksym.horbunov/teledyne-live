﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_User_AfterFirstReg
    Inherits RegBasePage
    Dim strSQL As String
    Dim li As ListItem
    Dim re As SqlDataReader

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Users"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - User Registration Confirmation"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        litOne.Text = Functions.LoadI18N("SLBCA0727", localeId.ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0728", localeId.ToString())
        If (localeId = 1041) Then
            litThree.Text = String.Format("{0}<br><br><h2>{1}</h2>", Functions.LoadI18N("SLBCA0179", localeId), Functions.LoadI18N("SLBCA0466", localeId))
        Else
            litThree.Text = String.Format("{0},&nbsp;{1}.<br><br><h2>{2}</h2>", Functions.LoadI18N("SLBCA0179", localeId), Session("FirstName").ToString(), Functions.LoadI18N("SLBCA0467", localeId))
        End If
        litFour.Text = Functions.LoadI18N("SLBCA0470", localeId)
        litFive.Text = Functions.LoadI18N("SLBCA0609", localeId)
        litSix.Text = Functions.LoadI18N("SLBCA0611", localeId)
        litSeven.Text = Functions.LoadI18N("SLBCA0614", localeId)
        litEight.Text = Functions.LoadI18N("SLBCA0892", localeId)
        litNine.Text = Functions.LoadI18N("SLBCA0657", localeId)

        btn_next.Text = LoadI18N("SLBCA0183", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not Me.IsPostBack Then
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@LOCALEID", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT QUESTIONID,QUESTION  FROM AFTERREGQUESTION where DISABLED='n' and LOCALEID=@LOCALEID ORDER BY SORTID", sqlParameters.ToArray())
        rblAfterReg.DataSource = ds
        rblAfterReg.DataTextField = "QUESTION"
        rblAfterReg.DataValueField = "QUESTIONID"
        rblAfterReg.DataBind()
    End Sub

    Public Function getFirstName() As String
        Dim result As String = ""
        If Not Session("FirstName") Is Nothing Then
            result = Session("FirstName").ToString()
        End If
        Return result
    End Function


    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_next.Click
        Dim strBody As String = ""
        Dim strCRet As String = "<br />"
        If Not Session("ContactID") Is Nothing Then
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            If Len(Session("CampaignID")) > 0 Then
                strSQL = "Insert into AFTERREGANSWER (CONTACTID, QUESTIONID,TEXT,   DATE_ENTERED, CAMPAIGN_ID) values (@CONTACTID,@QUESTIONID,@TEXT,@DATEENTERED,@CAMPAIGNID)"
                sqlParameters.Add(New SqlParameter("@CAMPAIGNID", Session("CampaignID").ToString()))
            Else
                strSQL = "Insert into AFTERREGANSWER (CONTACTID, QUESTIONID, TEXT,  DATE_ENTERED, CAMPAIGN_ID) values (@CONTACTID,@QUESTIONID,@TEXT,@DATEENTERED,@CAMPAIGNID)"
                sqlParameters.Add(New SqlParameter("@CAMPAIGNID", "0"))
            End If
            sqlParameters.Add(New SqlParameter("@CONTACTID", Session("ContactID").ToString()))
            sqlParameters.Add(New SqlParameter("@QUESTIONID", rblAfterReg.SelectedValue.ToString()))
            sqlParameters.Add(New SqlParameter("@TEXT", " "))
            sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            strBody = "How did you hear about LeCroy?"
            strBody = strBody & strCRet & strCRet & CustomerInfo()
            strBody = strBody & strCRet & strCRet
            strBody = strBody & LoadI18N("SLBCA0467", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) & strCRet
            strBody = strBody & "QUESTION: " & rblAfterReg.SelectedItem.ToString & strCRet

            '   If Session("Country") = "Japan" Then
            Dim strTo As String = "contact.jp@teledynelecroy.com"
            Functions.SendEmailHTML("Japan", strBody, strTo, "webmaster@teledynelecroy.com", " ", "", "How did you hear about LeCroy", "")
            'End If
        End If
        Response.Redirect("confirmed.aspx")
    End Sub

    Function CustomerInfo() As String
        Dim strInfo As String = ""
        Dim strCRet As String = "<br />"
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        If CInt(localeId) <> 1041 Then
            ' Client data
            If Len(Session("FirstName")) > 0 And Len(Session("LastName")) > 0 Then

                strInfo = strCRet + strCRet + strInfo & "Client: " & Session("LastName") & " " & LoadI18N("SLBCA0160", localeId) & " " & Session("Firstname") & strCRet
            End If
            If Len(Session("Title")) > 0 Then
                strInfo = strInfo & "Title: " & Session("Title") & strCRet
            End If
            If Len(Session("Company")) > 0 Then
                strInfo = strInfo & "Company: " & Session("Company") & strCRet
            End If
            If Len(Session("Department")) > 0 Then
                strInfo = strInfo & "Department: " & Session("Department") & strCRet
            End If
            If Len(Session("Address")) > 0 Then
                strInfo = strInfo & "Address: " & Session("Address") & " " & Session("Address2") & strCRet
            End If
            If Len(Session("City")) > 0 Then
                strInfo = strInfo & "City: " & Session("City") & strCRet
            End If
            If Len(Session("State")) > 0 Then
                strInfo = strInfo & "State: " & Session("State") & strCRet
            End If
            If Len(Session("Zip")) > 0 Then
                strInfo = strInfo & "Zip: " & Session("Zip") & strCRet
            End If
            If Len(Session("Country")) > 0 Then
                strInfo = strInfo & "Country: " & Session("Country") & strCRet
            End If
            If Len(Session("Phone")) > 0 Then
                strInfo = strInfo & "Phone: " & Session("Phone") & strCRet
            End If
            If Len(Session("Fax")) > 0 Then
                strInfo = strInfo & "Fax: " & Session("Fax") & strCRet
            End If
            If Len(Session("Email")) > 0 Then
                strInfo = strInfo & "Email: " & Session("Email") & strCRet
            End If
            If Len(Session("URL")) > 0 Then
                strInfo = strInfo & "URL: " & Session("URL") & strCRet
            End If
            If Len(Session("Applications")) > 0 Then
                strInfo = strInfo & "Application: " & Session("Applications") & strCRet
            End If
        Else
            ' Client data for Japan
            If Len(Session("Country")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0083", localeId) & ": " & Session("Country") & strCRet
            End If
            If Len(Session("Zip")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0082", localeId) & ": " & Session("Zip") & strCRet
            End If
            If Len(Session("City")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0080", localeId) & ": " & Session("City") & strCRet
            End If
            If Len(Session("Address")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0079", localeId) & ": " & Session("Address") & strCRet
            End If
            If Len(Session("Address2")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0079", localeId) & "2: " & Session("Address2") & strCRet
            End If
            If Len(Session("Company")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0078", localeId) & ": " & Session("Company") & strCRet
            End If
            If Len(Session("Title")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0077", localeId) & ": " & Session("Title") & strCRet
            End If
            If Len(Session("FirstName")) > 0 And Len(Session("LastName")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0076", localeId) & ": " & Session("LastName") & " " & Session("Firstname") & " " & LoadI18N("SLBCA0160", localeId) & strCRet
            End If
            If Len(Session("Phone")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0084", localeId) & ": " & Session("Phone") & strCRet
            End If
            If Len(Session("Fax")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0085", localeId) & ": " & Session("Fax") & strCRet
            End If
            If Len(Session("Email")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0086", localeId) & ": " & Session("Email") & strCRet
            End If
            If Len(Session("URL")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0087", localeId) & ": " & Session("URL") & strCRet
            End If
            If Len(Session("Applications")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0088", localeId) & ": " & Session("Applications") & strCRet
            End If
        End If
        ' Insert empty line 
        strInfo = strInfo & strCRet
        'Response.Write strIntro
        ' Return data 
        CustomerInfo = strInfo

    End Function
End Class