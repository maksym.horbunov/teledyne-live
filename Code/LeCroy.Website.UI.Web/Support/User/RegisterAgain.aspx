﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_User_RegisterAgain" Codebehind="RegisterAgain.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
	<ul>
		<li><a href="default.aspx"><asp:Literal ID="litOne" runat="server" /></a></li>
		<li class="current"><a href="#"><asp:Literal ID="litTwo" runat="server" /></a></li>
	</ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#8000FF"></td>
                <td>&nbsp;</td>
                <td width="100%">
                    <strong><asp:Literal ID="litThree" runat="server" /><br /><asp:Literal ID="litFour" runat="server" /></strong><br /><br />
                    <asp:Literal ID="litFive" runat="server" />
                </td>
            </tr>
            <tr>
                <td bgcolor="#8000FF"></td>
                <td></td>
                <td width="100%"><asp:Button ID="btn_sendtoreg" runat="server" Text="Next" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="divider"></div>
    <div class="resources">
		<div class="blue"><asp:Literal ID="litSix" runat="server" /></div>
        <ul>
            <li class="link"><asp:Literal ID="litSeven" runat="server" /></li>
            <li class="link"><asp:Literal ID="litEight" runat="server" /></li>
            <li class="link"><asp:Literal ID="litNine" runat="server" /></li>
            <li class="link"><asp:Literal ID="litTen" runat="server" /></li>
        </ul>
	</div>
</asp:Content>