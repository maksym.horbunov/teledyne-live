Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Partial Class SupportUser_SetSession
    Inherits BasePage
    Dim strSQL As String

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Add the session information to the database, and redirect to the
        'ASP implementation of SessionTransfer.
        Dim strGuid As String = Request.QueryString("guid")
        If Len(strGuid) = 0 Then
            Response.Redirect("default.aspx")
        Else
            GetSessionFromDatabase(strGuid)
            Session("flgSessionTransfer") = "y"
            If Not Session("redirectto") Is Nothing And Not Session("contactid") Is Nothing Then
                If Len(Session("RedirectTo")) > 0 Then
                    If (Session("RedirectTo") = "/japan/seminar/si-seminar/si-form.asp" Or Session("RedirectTo") = "/japan/web2012/web-form.asp") And Len(Session("contactid")) > 0 Then
                        Response.Redirect("userprofile.aspx")
                    End If
                    If Not (String.IsNullOrEmpty(Session("WebUiDestination"))) Then
                        Response.Redirect(Session("WebUiDestination"))
                    End If
                End If
            End If
            Response.Redirect("default.aspx")
        End If
    End Sub

    'This method adds the session information to the database and returns the GUID
    '  used to identify the data.
    Private Sub GetSessionFromDatabase(ByVal guidIn As String)
        Dim aspSessionStates As List(Of AspSessionState) = AspSessionStateRepository.GetAspSessionState(ConfigurationManager.AppSettings("ConnectionString").ToString(), guidIn)
        If (aspSessionStates.Count > 0) Then
            For Each aspSessionState As AspSessionState In aspSessionStates
                Session(aspSessionState.SessionKey) = aspSessionState.SessionValue
            Next
            AspSessionStateRepository.DeleteAspSessionState(ConfigurationManager.AppSettings("ConnectionString").ToString(), guidIn)
        End If
    End Sub
End Class