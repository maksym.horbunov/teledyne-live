﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class Support_User_Subscription_confirm
    Inherits BasePage
    Dim strSQL As String = ""
    Public menuURL As String = ""
    Public menuURL2 As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim SubID As String = SQLStringWithOutSingleQuotes(Request.QueryString("id"))
        Dim strEmail As String = ""
        If Len(SubID) > 0 Then
            'get email on SubID
            strSQL = "SELECT top 1 EMAIL from SUBSCRIPTION where SUB_ID=@SUBID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@SUBID", SubID))
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    strEmail = dr("EMAIL").ToString
                Next
            Else
                Response.Redirect("default.aspx")
            End If

            'update CONFIRM_YN flag
            sqlParameters = New List(Of SqlParameter)
            strSQL = "Update SUBSCRIPTION set CONFIRM_YN='y' where EMAIL=@EMAIL"
            sqlParameters.Add(New SqlParameter("@EMAIL", strEmail))
            DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            Response.Redirect("default.aspx")
        Else
            Response.Redirect("default.aspx")
        End If
    End Sub
End Class