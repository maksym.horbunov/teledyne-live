<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Subscription" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit">
            <h1>Subscriptions</h1>
            <p>Your Email:</p>
            <div id="divRegistered" runat="server" visible="false">
                <b style="font-size: 10pt;"><asp:Literal ID="litEmail" runat="server" /></b><br /><asp:LinkButton ID="btnThisIsNotMe" runat="server" CausesValidation="false" Font-Size="X-Small" Text="This is not my email address" />
            </div>
            <div id="divUnRegistered" runat="server" visible="false">
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="300" />&nbsp;
                    <asp:CustomValidator ID="cvEmail" runat="server" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSendEmail" />
            </div>
            <p>Will be unsubscribed from all Teledyne LeCroy emails.</p>
            <p>
                If you would like to receive any of the emails listed below, please check the box. Cancel to keep your current subscriptions.<br />
                <asp:CheckBoxList ID="cblSubscriptions" runat="server" />
            </p>
            <p>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="vgSendEmail" />&nbsp;
                <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel" />
            </p>
        </asp:Panel>
        <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
            Your selections have been submitted. Your subscription setting is now:<br />
            <asp:Literal ID="litSubscriptions" runat="server" />
        </asp:Panel>
    </div><br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <div class="resources"></div>
</asp:Content>