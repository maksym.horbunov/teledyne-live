﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Subscription
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Users"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Subscriptions"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnThisIsNotMe_Click(sender As Object, e As EventArgs) Handles btnThisIsNotMe.Click
        DumpUserDataFromSession()
        Session.Remove("SubID")
        Session.Remove("CampaignID")
        Session("SubID") = Nothing
        Session("CampaignID") = Nothing
        If Not (Request.Cookies("LCDATA") Is Nothing) Then
            Response.Cookies("LCDATA").Expires = DateTime.Now.AddDays(-1)
        End If
        BindPage()
    End Sub

    Private Sub cvEmail_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvEmail.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtEmail.Text)) Then
            cvEmail.ErrorMessage = "*required"
            Return
        End If
        If Not (Utilities.checkValidEmail(txtEmail.Text)) Then
            cvEmail.ErrorMessage = "*invalid"
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If (divUnRegistered.Visible) Then
            Page.Validate("vgSendEmail")
            If Not (Page.IsValid) Then
                Return
            End If
        End If

        Dim preferences As List(Of Preference) = GetPreferences()
        Dim subscriptionId As String = UnsubscribeUserFromAllSubscriptions(preferences.Select(Function(x) x.Id).Distinct().ToList())
        Dim selectedPreferences As List(Of Int32) = preferences.Where(Function(x) x.Selected = True).Select(Function(x) x.Id).Distinct().ToList()
        If (selectedPreferences.Count > 0) Then
            subscriptionId = SubscribeUserToOptedInSubscriptions(subscriptionId, selectedPreferences)
        End If
        If Not (String.IsNullOrEmpty(Session("SubID"))) And Not (String.IsNullOrEmpty(Session("CampaignID"))) And Not (String.Compare(Session("CampaignID"), "0", True) = 0) Then
            Dim subscriptionCancel As SubscriptionCancel = New SubscriptionCancel
            subscriptionCancel.SubId = Session("SubID").ToString()
            subscriptionCancel.CampaignId = Int32.Parse(Session("CampaignID").ToString())
            SubscriptionCancelRepository.SaveSubscriptionCancel(ConfigurationManager.AppSettings("ConnectionString").ToString(), subscriptionCancel)
        End If
        PostSubmitCleanup(subscriptionId, preferences)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("~/")
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindPage()
        BindLeftMenu()
        BindSubscriptions()

        If Not (String.IsNullOrEmpty(Session("SubID"))) And Not (String.IsNullOrEmpty(Session("CampaignID"))) And Not (String.Compare(Session("CampaignID"), "0", True) = 0) Then    ' Check Temp, Archive for emails with SubId+CampaignId
            Dim emailsFromSubTempAndSubArchive As List(Of String) = GetEmailAddressesFromSubscriptionTempAndArchive(Session("SubID").ToString(), Int32.Parse(Session("CampaignID").ToString()))
            If (emailsFromSubTempAndSubArchive.Count <= 0) Then     ' None found, require them to enter email address
                divRegistered.Visible = False
                divUnRegistered.Visible = True
                Return
            End If
            divUnRegistered.Visible = False     ' Found, bind literal with first email address
            divRegistered.Visible = True
            litEmail.Text = emailsFromSubTempAndSubArchive.FirstOrDefault()
            ViewState("EmailAddress") = emailsFromSubTempAndSubArchive.FirstOrDefault()
        ElseIf Not (String.IsNullOrEmpty(Session("Email"))) Then                ' Email in session, bind literal with it
            divUnRegistered.Visible = False
            divRegistered.Visible = True
            litEmail.Text = Session("Email").ToString()
            ViewState("EmailAddress") = Session("Email").ToString()
        Else    ' Nothing matches, force them to enter email address
            divRegistered.Visible = False
            divUnRegistered.Visible = True
        End If
    End Sub

    Private Sub BindLeftMenu()
        Dim captionID As String = String.Empty
        Dim menuID As String = String.Empty

        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.PROFILE_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.PROFILE_MENU
            End If
        End If
        Session("menuSelected") = captionID
    End Sub

    Private Sub BindSubscriptions()
        Dim subscriptions As List(Of SubscriptionType) = SubscriptionTypeRepository.GetSubscriptionTypes(ConfigurationManager.AppSettings("ConnectionString").ToString(), True)
        If (subscriptions.Count > 0) Then
            cblSubscriptions.DataSource = subscriptions
            cblSubscriptions.DataTextField = "Name"
            cblSubscriptions.DataValueField = "DocumentId"
            cblSubscriptions.DataBind()
        End If
    End Sub

    Private Function GetPreferences() As List(Of Preference)
        Dim retVal As List(Of Preference) = New List(Of Preference)
        For Each li As ListItem In cblSubscriptions.Items
            Dim pref As Preference = New Preference
            pref.Id = Int32.Parse(li.Value)
            pref.Text = li.Text
            pref.Selected = li.Selected
            retVal.Add(pref)
        Next
        Return retVal
    End Function

    Private Function UnsubscribeUserFromAllSubscriptions(ByVal preferences As List(Of Int32)) As String
        If (divUnRegistered.Visible) Then
            Return GenerateSubscriptionToOptedOut(preferences, txtEmail.Text)
        Else
            Return GenerateSubscriptionToOptedOut(preferences, ViewState("EmailAddress"))
        End If
    End Function

    Private Function GenerateSubscriptionToOptedOut(ByVal preferences As List(Of Int32), ByVal emailAddress As String) As String
        Dim subscriptions As List(Of LeCroy.Library.Domain.Common.DTOs.Subscription) = SubscriptionRepository.GetSubscriptions(ConfigurationManager.AppSettings("ConnectionString").ToString(), emailAddress).ToList()
        If (subscriptions.Count > 0) Then
            SubscriptionRepository.UpdateSubscriptionsToOptOut(ConfigurationManager.AppSettings("ConnectionString").ToString(), subscriptions.Select(Function(x) x.Email).Distinct().ToList())
            Return String.Empty
        Else
            Dim subscriptionId As String = Functions.GenerateUserID().ToString()
            For Each preference As Int32 In preferences
                SubscriptionRepository.InsertSubscriptionsToOptOut(ConfigurationManager.AppSettings("ConnectionString").ToString(), CreateSubscription(subscriptionId, preference, emailAddress, "N"))
            Next
            Return subscriptionId
        End If
    End Function

    Private Function GenerateSubscriptionToOptedIn(ByVal subscriptionId As String, ByVal preference As Int32, ByVal emailAddress As String) As String
        Dim subscriptions As List(Of LeCroy.Library.Domain.Common.DTOs.Subscription) = SubscriptionRepository.GetSubscriptions(ConfigurationManager.AppSettings("ConnectionString").ToString(), emailAddress).ToList()
        If (subscriptions.Count <= 0) Then  ' No Subscription records; insert it
            If (String.IsNullOrEmpty(subscriptionId)) Then subscriptionId = Functions.GenerateUserID().ToString()
            SubscriptionRepository.InsertSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), CreateSubscription(subscriptionId, preference, emailAddress, "Y"))
            Return subscriptionId
        End If

        ' Subscription record(s) exist; check against doc id
        subscriptions = subscriptions.Where(Function(x) x.DocumentId = preference).ToList() ' Should only be one unique record if it exists, otherwise the update will bulk update all based on email+preference
        If (subscriptions.Count > 0) Then   ' Subscription records for email+docid exist; update
            SubscriptionRepository.UpdateSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), New List(Of String)({emailAddress}), New List(Of Int32)({preference}))
            Return subscriptionId   ' Unchanged
        Else    ' No email+docid records exist, insert
            If (String.IsNullOrEmpty(subscriptionId)) Then subscriptionId = Functions.GenerateUserID().ToString()
            SubscriptionRepository.InsertSubscriptionsToOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), CreateSubscription(subscriptionId, preference, emailAddress, "Y"))
            Return subscriptionId
        End If
    End Function

    Private Function CreateSubscription(ByVal subscriptionId As String, ByVal preference As Int32, ByVal emailAddress As String, ByVal enable As String) As LeCroy.Library.Domain.Common.DTOs.Subscription
        Dim subscription As LeCroy.Library.Domain.Common.DTOs.Subscription = New LeCroy.Library.Domain.Common.DTOs.Subscription()
        subscription.SubId = subscriptionId
        subscription.Email = emailAddress
        subscription.DocumentId = preference
        subscription.ContactId = 0
        subscription.Enable = enable
        subscription.CountryId = 208
        subscription.DocumentRead = "N"
        subscription.FormatId = 2
        subscription.ConfirmYn = "N"
        Return subscription
    End Function

    Private Function SubscribeUserToOptedInSubscriptions(ByVal subscriptionId As String, ByVal preferences As List(Of Int32)) As String
        For Each preference As Int32 In preferences
            If (divUnRegistered.Visible) Then
                subscriptionId = GenerateSubscriptionToOptedIn(subscriptionId, preference, txtEmail.Text)
            Else
                subscriptionId = GenerateSubscriptionToOptedIn(subscriptionId, preference, ViewState("EmailAddress").ToString())
            End If
        Next
        Return subscriptionId
    End Function

    Private Sub PostSubmitCleanup(ByVal subscriptionId As String, ByVal preferences As List(Of Preference))
        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#SubscriptionUrl#>", String.Format("{0}/support/user/subscription/confirm.aspx?id={1}", ConfigurationManager.AppSettings("DefaultDomain"), subscriptionId))
        Dim selected = preferences.Where(Function(x) x.Selected = True).Distinct().ToList()
        Dim notSelected = preferences.Where(Function(x) x.Selected = False).Distinct().ToList()
        If (selected.Count() > 0) Then
            replacements.Add("<#Subscribed#>", String.Join("<br />", selected.Select(Function(x) x.Text).Distinct().ToList().ToArray()))
            litSubscriptions.Text = String.Format("<br /><b>Subscribed:</b><br />{0}", String.Join("<br />", selected.Select(Function(x) x.Text).Distinct().ToList().ToArray()))
        Else
            replacements.Add("<#Subscribed#>", "None subscribed")
            litSubscriptions.Text = "<br /><b>Subscribed:</b><br />None subscribed"
        End If
        If (notSelected.Count() > 0) Then
            replacements.Add("<#Unsubscribed#>", String.Join("<br />", notSelected.Select(Function(x) x.Text).Distinct().ToList().ToArray()))
            litSubscriptions.Text += String.Format("<br /><br /><b>Unsubscribed:</b><br />{0}", String.Join("<br />", notSelected.Select(Function(x) x.Text).Distinct().ToList().ToArray()))
        Else
            replacements.Add("<#Unsubscribed#>", "None unsubscribed")
            litSubscriptions.Text += "<br /><br /><b>Unsubscribed:</b><br />None unsubscribed"
        End If
        Try
            If (divUnRegistered.Visible) Then
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 5, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString(), txtEmail.Text, replacements)
            Else
                FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 5, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString(), ViewState("EmailAddress").ToString(), replacements)
            End If
        Catch
        End Try
        Response.Cookies("SUBSCRIBE")("SUB") = subscriptionId
        Response.Cookies("SUBSCRIBE").Expires = (Date.Today()).AddDays(365 * 5)
        Response.Cookies("SUBSCRIBE").HttpOnly = True
        pnlSubmit.Visible = False
        pnlSubmitted.Visible = True
    End Sub

    Private Function GetEmailAddressesFromSubscriptionTempAndArchive(ByVal subscriptionId As String, ByVal campaignId As Int32) As List(Of String)
        Dim retVal As List(Of String) = New List(Of String)
        Dim subscriptionTemps As List(Of SubscriptionTemp) = SubscriptionTempRepository.GetSubscriptionTempsWithCampaignIdAndSubscriptionId(ConfigurationManager.AppSettings("ConnectionString").ToString(), campaignId, subscriptionId).ToList()
        For Each subscriptionTemp As SubscriptionTemp In subscriptionTemps
            If Not (retVal.Contains(subscriptionTemp.Email)) Then
                retVal.Add(subscriptionTemp.Email)
            End If
        Next
        Dim subscriptionArchives As List(Of SubscriptionArchive) = SubscriptionArchiveRepository.GetSubscriptionArchivesWithCampaignIdAndSubscriptionId(ConfigurationManager.AppSettings("ConnectionString").ToString(), campaignId, subscriptionId).ToList()
        For Each subscriptionArchive As SubscriptionArchive In subscriptionArchives
            If Not (retVal.Contains(subscriptionArchive.Email)) Then
                retVal.Add(subscriptionArchive.Email)
            End If
        Next
        Return retVal
    End Function
#End Region
End Class

Public Class Preference
    Public Id As Int32
    Public Text As String
    Public Selected As Boolean
End Class