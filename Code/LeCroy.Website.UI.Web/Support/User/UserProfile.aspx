﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_User_UserProfile" Codebehind="UserProfile.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="default.aspx"><asp:Literal ID="litOne" runat="server" /></a></li>
        <li class="current"><a href="#"><asp:Literal ID="litTwo" runat="server" /></a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="javascript" type="text/javascript">
        var emailCheck = '';
        var passwordCheck = '';
        var cpasswordCheck = '';
        var emailValid = true;
        var passwordValid = true;

        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            StateSelectControl();

            $('#<%= txtEmail.ClientID %>').focus(function () {
                $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                var email = $('#<%= txtEmail.ClientID %>').val();
                if (email != '') {
                    emailCheck = email;
                } else {
                    emailCheck = '';
                    emailValid = false;
                }
            });
            $('#<%= txtEmail.ClientID %>').blur(function () {
                var email = $('#<%= txtEmail.ClientID %>').val();
                var cid = $('#<%= hdnContactId.ClientID %>').val();
                if (email != emailCheck) {
                    if (email != '' && email.length >= 4) {
                        $('#spanEmailValidation').text('Verifying email address...');
                        $.ajax({
                            type: "POST",
                            url: "userprofile.aspx/verifyemailnotused",
                            data: '{"emailAddress": "' + email + '", "id": "' + cid + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                var response = msg.d;
                                if (response.indexOf('success') != -1) {
                                    $('#spanEmailValidation').removeClass('error').addClass('success');
                                    $('#spanEmailValidation').text('Valid');
                                    emailValid = true;
                                }
                                else {
                                    $('#spanEmailValidation').removeClass('success').addClass('error');
                                    $('#spanEmailValidation').text(response);
                                    emailValid = false;
                                }
                                if (emailValid && passwordValid) {
                                    ValidateControls();
                                }
                                else {
                                    $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                                }
                            }
                        });
                    }
                    else {
                        $('#spanEmailValidation').text('');
                        $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                    }
                } else {
                    $('#spanEmailValidation').text('');
                    $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                }
            });

            $('.password').focus(function () {
                var oldPassword = $('#<%= txtOldPassword.ClientID %>').val();
                if (!oldPassword) {
                    $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                    var password = $('#<%= txtPassword.ClientID %>').val();
                    if (password != '') {
                        passwordCheck = password;
                    } else {
                        passwordCheck = '';
                        passwordValid = false;
                    }
                    var cpassword = $('#<%= txtConfirmPassword.ClientID %>').val();
                    if (cpassword != '') {
                        cpasswordCheck = cpassword;
                    } else {
                        cpasswordCheck = '';
                        passwordValid = false;
                    }
                }
            });
            $('.password').blur(function () {
                var cpassword = $('#<%= txtConfirmPassword.ClientID %>').val();
                var password = $('#<%= txtPassword.ClientID %>').val();
                if (password != '' && cpassword != '') {
                    $('#spanPasswordValidation').text('Verifying password...');
                    $.ajax({
                        type: "POST",
                        url: "userprofile.aspx/verifypasswordintegrity",
                        data: '{"password": "' + password + '", "confirmPassword": "' + cpassword + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            var response = msg.d;
                            if (response.indexOf('success') != -1) {
                                $('#spanPasswordValidation').removeClass('error').addClass('success');
                                $('#spanPasswordValidation').text('Valid');
                                passwordValid = true;
                            }
                            else {
                                $('#spanPasswordValidation').removeClass('success').addClass('error');
                                $('#spanPasswordValidation').text(response);
                                passwordValid = false;
                            }
                            if (emailValid && passwordValid) {
                                ValidateControls();
                            }
                            else {
                                $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                            }
                        }
                    });
                }
                else {
                    $('#spanPasswordValidation').text('');
                    $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                }
            });
            
            $('#<%= txtOldPassword.ClientID %>, #<%= txtPassword.ClientID %>, #<%= txtConfirmPassword.ClientID %>').keyup(function () {
                SetPasswordRequiredCss();
                ValidateControls();
            });
            $('#<%= ddlCountry.ClientID %>').change(function () {
                StateSelectControl();
                ValidateControls();
            });
            $('#<%= ddlState.ClientID %>').change(function () {
                ValidateControls();
            });
            $('#<%= ddlCanadaProvince.ClientID %>').change(function () {
                ValidateControls();
            });
            $('#<%= txtFirstName.ClientID %>, #<%= txtLastName.ClientID %>, #<%= txtPhone.ClientID %>, #<%= txtAddress.ClientID %>, #<%= txtCity.ClientID %>, #<%= txtPostalCode.ClientID %>, #<%= txtCompany.ClientID %>').blur(function () {
                ValidateControls();
            });
            $('#<%= txtFirstName.ClientID %>, #<%= txtLastName.ClientID %>, #<%= txtPhone.ClientID %>, #<%= txtAddress.ClientID %>, #<%= txtCity.ClientID %>, #<%= txtPostalCode.ClientID %>, #<%= txtCompany.ClientID %>').focus(function () {
                if (urlParams.get('update') != null && urlParams.get('update').toLowerCase() == 'y') {  // ************** need to fix
                    $('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
                }
            });
        });

        function ValidateControls() {
            var oldPassword = jQuery('#<%= txtOldPassword.ClientID %>').val();
            var fname = jQuery('#<%= txtFirstName.ClientID %>');
            var lname = jQuery('#<%= txtLastName.ClientID %>');
            var phone = jQuery('#<%= txtPhone.ClientID %>');
            var addr = jQuery('#<%= txtAddress.ClientID %>');
            var city = jQuery('#<%= txtCity.ClientID %>');
            var postal = jQuery('#<%= txtPostalCode.ClientID %>');
            var company = jQuery('#<%= txtCompany.ClientID %>');
            var state = jQuery('#<%= ddlState.ClientID %>');
            var country = jQuery('#<%= ddlCountry.ClientID %>');
            var canadaProvince = jQuery('#<%= ddlCanadaProvince.ClientID %>');
            if (fname.val() == null || fname.val() == '' || lname.val() == null || lname.val() == '' || phone.val() == null || phone.val() == '' || addr.val() == null || addr.val() == '' || city.val() == null || city.val() == '' || postal.val() == null || postal.val() == '' || company.val() == null || company.val() == '') {
                jQuery('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
            } else if (country.prop('selectedIndex') <= 0 ) {
                jQuery('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
            } else if ((country.val() == 'United States') && state.prop('selectedIndex') <= 0) {
                jQuery('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
            } else if ((country.val() == 'Canada') && canadaProvince.prop('selectedIndex') <= 0) {
                jQuery('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
            } else if (!passwordValid && (oldPassword == null || oldPassword == '') && !emailValid) {
                jQuery('#<%= btn_next.ClientID %>').attr('disabled', 'disabled');
            } else {
                jQuery('#<%= btn_next.ClientID %>').removeAttr('disabled');
            }
        }

        function StateSelectControl() {
            var selectedCountry = jQuery('#<%= ddlCountry.ClientID %> option:selected').text();
            if (selectedCountry.toUpperCase() == "UNITED STATES") {
                jQuery('#divOtherStateEntry').css('display', 'none');
                jQuery('#divOtherStateLabel').css('display', 'none');
                jQuery('#divStateEntry').css('display', 'block');
                jQuery('#divStateLabel').css('display', 'block');
                jQuery('#divCanadaProvinceEntry').css('display', 'none');
                jQuery('#divCanadaProvinceLabel').css('display', 'none');
            }
            else if (selectedCountry.toUpperCase() == "CANADA") {
                jQuery('#divOtherStateEntry').css('display', 'none');
                jQuery('#divOtherStateLabel').css('display', 'none');
                jQuery('#divStateEntry').css('display', 'none');
                jQuery('#divStateLabel').css('display', 'none');
                jQuery('#divCanadaProvinceEntry').css('display', 'block');
                jQuery('#divCanadaProvinceLabel').css('display', 'block');
            }
            else {
                jQuery('#<%= ddlState.ClientID %>').prop('selectedIndex', 0);
                jQuery('#divStateLabel').css('display', 'none');
                jQuery('#divStateEntry').css('display', 'none');
                jQuery('#divOtherStateLabel').css('display', 'block');
                jQuery('#divOtherStateEntry').css('display', 'block');
                jQuery('#divCanadaProvinceEntry').css('display', 'none');
                jQuery('#divCanadaProvinceLabel').css('display', 'none');
            }
        }

        function SetPasswordRequiredCss() {
            if (jQuery('#<%= txtOldPassword.ClientID %>').val() != '' || jQuery('#<%= txtPassword.ClientID %>').val() != '' || jQuery('#<%= txtConfirmPassword.ClientID %>').val() != '') {
                jQuery('#<%= lblOldPassword.ClientID %>').addClass('required');
                jQuery('#<%= lblPassword.ClientID %>').addClass('required');
                jQuery('#<%= lblConfirmPassword.ClientID %>').addClass('required');
            }
            else {
                jQuery('#<%= lblOldPassword.ClientID %>').removeClass('required');
                jQuery('#<%= lblPassword.ClientID %>').removeClass('required');
                jQuery('#<%= lblConfirmPassword.ClientID %>').removeClass('required');
            }
        }
    </script>

    <asp:HiddenField ID="hdnContactId" runat="server" />
    <asp:Panel ID="pnlEntry" runat="server" Visible="true">
        <div class="intro">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                <tr>
                    <td align="left"><asp:Label ID="lbllanguage" runat="server"><asp:Literal ID="litThree" runat="server" /></asp:Label></td>
                    <td align="left"><asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True" /></td>
                </tr>
            </table><br />
            <asp:Label ID="lb_alert" runat="server" /><br />
            <asp:Label ID="lb_intro1" runat="server" Font-Bold="True"><asp:Literal ID="litFour" runat="server" /></asp:Label><br /><br /><hr /><br />
            <asp:Label ID="lb_4" runat="server"><asp:Literal ID="litFive" runat="server" /></asp:Label><br /><br />
            <asp:Label ID="lb_5" runat="server" ForeColor="Red"><asp:Literal ID="litSix" runat="server" /></asp:Label><br /><br />
            <table border="0" cellpadding="0" cellspacing="0" style="width: 500px;">
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblUserName" runat="server"><asp:Literal ID="litSeven" runat="server" /></asp:Label>&nbsp;
                        <asp:CustomValidator ID="cvUserName" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2"><b><asp:Label ID="lblUserNameReadOnly" runat="server" /></b></td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <span id="spanOldPasswordLabel" runat="server" visible="false">
                            <asp:Label ID="lblOldPassword" runat="server"><asp:Literal ID="litEight" runat="server" />:</asp:Label>&nbsp; <span class="required" id="spanOldPasswordMisMatch" runat="server" visible="false">*<asp:Literal ID="litNine" runat="server" /></span> </span>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <span id="spanOldPasswordTextBox" runat="server" visible="false">
                            <asp:TextBox ID="txtOldPassword" runat="server" CssClass="password" MaxLength="20" TextMode="Password" SkinID="TwoColumnRegistration" />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblPassword" runat="server" CssClass="required"><asp:Literal ID="litTen" runat="server" /></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" CssClass="error" Display="Dynamic" EnableClientScript="false" Enabled="false" ErrorMessage="*" /><br />
                        <asp:Label ID="lblPasswordMessage" runat="server"><asp:Literal ID="litEleven" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="password" MaxLength="20" TextMode="Password" SkinID="TwoColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblConfirmPassword" runat="server" CssClass="required"><asp:Literal ID="litTwelve" runat="server" /></asp:Label>&nbsp;
                        <asp:CustomValidator ID="cvPassword" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="password" MaxLength="20" TextMode="Password" SkinID="TwoColumnRegistration" />&nbsp; <span id="spanPasswordValidation"></span>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblFirstName" runat="server" CssClass="required"><asp:Literal ID="litThirteen" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                    <td align="left">
                        <asp:Label ID="lblLastName" runat="server" CssClass="required"><asp:Literal ID="litFourteen" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtFirstName" runat="server" MaxLength="60" SkinID="TwoColumnRegistration" />
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtLastName" runat="server" MaxLength="30" SkinID="TwoColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblEmail" runat="server" CssClass="required"><asp:Literal ID="litFifteen" runat="server" /></asp:Label>&nbsp;
                        <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="60" SkinID="OneColumnRegistration" />&nbsp; <span id="spanEmailValidation"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblPhone" runat="server" CssClass="required"><asp:Literal ID="litSixteen" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                    <td align="left">
                        <asp:Label ID="lblFax" runat="server"><asp:Literal ID="litSeventeen" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtFax" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblCountry" runat="server" CssClass="required"><asp:Literal ID="litEighteen" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" InitialValue="" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="dropdown" DataTextField="Name" DataValueField="PardotCountry" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblAddress" runat="server" CssClass="required"><asp:Literal ID="litNineteen" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtAddress" runat="server" MaxLength="255" SkinID="OneColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblAddress2" runat="server"><asp:Literal ID="litTwenty" runat="server" /> 2</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="255" SkinID="OneColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblCity" runat="server" CssClass="required"><asp:Literal ID="litTwentyOne" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                    <td align="left">
                        <div id="divStateLabel">
                            <asp:Label ID="lblState" runat="server" CssClass="required"><asp:Literal ID="litTwentyTwo" runat="server" /></asp:Label>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="ddlState" CssClass="error" Display="Dynamic" EnableClientScript="false" Enabled="false" ErrorMessage="*" InitialValue="" />
                        </div>
                        <div id="divCanadaProvinceLabel">
                            <asp:Label ID="lblCanadaProvince" runat="server" CssClass="required"><asp:Literal ID="litTwentyThreeA" runat="server" /></asp:Label>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvCanadaProvince" runat="server" ControlToValidate="ddlCanadaProvince" CssClass="error" Display="Dynamic" EnableClientScript="false" Enabled="false" ErrorMessage="*" InitialValue="" />
                        </div>
                        <div id="divOtherStateLabel">
                            <asp:Label ID="lblOtherState" runat="server"><asp:Literal ID="litTwentyThree" runat="server" /></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                    </td>
                    <td align="left">
                        <div id="divStateEntry">
                            <asp:DropDownList ID="ddlState" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="name" />
                        </div>
                        <div id="divCanadaProvinceEntry">
                            <asp:DropDownList ID="ddlCanadaProvince" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="name" />
                        </div>
                        <div id="divOtherStateEntry">
                            <asp:TextBox ID="txtOtherState" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <div>
                            <asp:Label ID="lblPostalCode" runat="server" CssClass="required"><asp:Literal ID="litTwentyFour" runat="server" /></asp:Label>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvPostalCode" runat="server" ControlToValidate="txtPostalCode" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <div>
                            <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                        </div>
                    </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblJobFunction" runat="server"><asp:Literal ID="litTwentyFive" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:DropDownList ID="ddlJobFunction" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="name" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblJobTitle" runat="server"><asp:Literal ID="litTwentySix" runat="server" /></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblJobDepartment" runat="server"><asp:Literal ID="litTwentySeven" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtJobTitle" runat="server" MaxLength="60" SkinID="TwoColumnRegistration" />
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtJobDepartment" runat="server" MaxLength="100" SkinID="TwoColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblCompany" runat="server" CssClass="required"><asp:Literal ID="litTwentyEight" runat="server" /></asp:Label>&nbsp;
                        <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" />
                    </td>
                    <td align="left">
                        <asp:Label ID="lblCompanyUrl" runat="server"><asp:Literal ID="litTwentyNine" runat="server" />/<asp:Literal ID="litThirty" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCompanyUrl" runat="server" MaxLength="50" SkinID="TwoColumnRegistration" />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblApplication" runat="server"><asp:Literal ID="litThirtyOne" runat="server" /></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtApplication" runat="server" MaxLength="50" SkinID="OneColumnRegistration" />
                    </td>
                </tr>
            </table><br /><hr /><br />
            <asp:Label ID="lb_type" runat="server" ForeColor="RoyalBlue" Font-Bold="True">Opt In</asp:Label><br />
            <span style="font-weight: bold;">
                <asp:CheckBox ID="cbxOptIn" runat="server" Text="I agree" />&nbsp;<br />
                By ticking this box you agree to receive information from Teledyne and our authorized sales representatives and distributors about our latest news, events and products and/or services by email. Please see our privacy policy at <a href="http://www.teledyne.com/privacy-policy" target="_blank">http://www.teledyne.com/privacy-policy</a>
            </span><br /><br />
            <asp:Button ID="btn_next" runat="server" CausesValidation="false" Text="Next" />&nbsp;
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
<div class="greyPadding"></div>
	<div class="resources">
		<div class="blue"><asp:Literal ID="litThirtyThree" runat="server" /></div>
        <ul>
            <li class="link"><asp:Literal ID="litThirtyFour" runat="server" /></li>
            <li class="link"><asp:Literal ID="litThirtyFive" runat="server" /></li>
            <li class="link"><asp:Literal ID="litThirtySix" runat="server" /></li>
            <li class="link"><asp:Literal ID="litThirtySeven" runat="server" /></li>
        </ul>
    </div>
</asp:Content>