﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Net
Imports System.Web.Services
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class Support_User_UserRegisterForm
    Inherits RegBasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim sqlstr As String
    Dim li As ListItem
    Dim ds As DataSet
    Private _fieldFocusSet As Boolean = False
    Private _usCountryNameLookup As String = String.Empty
    Private _canadaProvinceNameLookup As String = String.Empty

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Users"
        lblPendingValidation.Text = Functions.LoadI18N("SLBCA0846", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0727", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0728", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0135", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0157", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0159", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0148", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTen.Text = Functions.LoadI18N("SLBCA0151", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEleven.Text = Functions.LoadI18N("SLBCA0852", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwelve.Text = Functions.LoadI18N("SLBCA0735", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirteen.Text = Functions.LoadI18N("SLBCA0161", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFourteen.Text = Functions.LoadI18N("SLBCA0162", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFifteen.Text = Functions.LoadI18N("SLBCA0086", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSixteen.Text = Functions.LoadI18N("SLBCA0084", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeventeen.Text = Functions.LoadI18N("SLBCA0085", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEighteen.Text = Functions.LoadI18N("SLBCA0083", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNineteen.Text = Functions.LoadI18N("SLBCA0079", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwenty.Text = Functions.LoadI18N("SLBCA0079", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyOne.Text = Functions.LoadI18N("SLBCA0080", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyTwo.Text = Functions.LoadI18N("SLBCA0081", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyThree.Text = Functions.LoadI18N("SLBCA0165", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyThreeA.Text = Functions.LoadI18N("SLBCA0165", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyFour.Text = Functions.LoadI18N("SLBCA0082", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyFive.Text = Functions.LoadI18N("SLBCA0163", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentySix.Text = Functions.LoadI18N("SLBCA0077", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentySeven.Text = Functions.LoadI18N("SLBCA0465", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyEight.Text = Functions.LoadI18N("SLBCA0078", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwentyNine.Text = Functions.LoadI18N("SLBCA0087", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirty.Text = Functions.LoadI18N("SLBCA0168", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyOne.Text = Functions.LoadI18N("SLBCA0088", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyThree.Text = Functions.LoadI18N("SLBCA0609", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyFour.Text = Functions.LoadI18N("SLBCA0611", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtyFive.Text = Functions.LoadI18N("SLBCA0614", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtySix.Text = Functions.LoadI18N("SLBCA0892", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirtySeven.Text = Functions.LoadI18N("SLBCA0657", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        _usCountryNameLookup = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), CountryIds.UNITED_STATES).Name
        ' AFNOTE: Currently do not have access to the LeCroy.Library.Domain.Common.Constants class to add a value for CANADA (35)
        _canadaProvinceNameLookup = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), 35).Name

        If Not Session("ContactID") Is Nothing And Not Request("update") Is Nothing Then
            Response.Redirect("UserProfile.aspx")
        End If
        Dim captionID As String = Request.QueryString("capid")
        Dim menuID As String = Request.QueryString("mid")
        Dim subMenuID As String = Request.QueryString("smid")
        If captionID Is Nothing Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        End If
        If menuID Is Nothing Then
            menuID = AppConstants.PROFILE_MENU
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            'set asp.net controls translations
            btn_next.Text = LoadI18N("SLBCA0436", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            lblPassword.CssClass = "required"
            lblConfirmPassword.CssClass = "required"
            Initial()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
            'GetSubstriptioTypes(localeid.ToString)
        End If
        ' Swap textbox attributes for japanese locale
        If (IsJapaneseLocale()) Then
            txtFirstName.MaxLength = 30
            txtLastName.MaxLength = 60
        Else
            txtFirstName.MaxLength = 60
            txtLastName.MaxLength = 30
        End If


        If Not Me.IsPostBack Then
            ddlState.SelectedIndex = 0
            ddlCanadaProvince.SelectedIndex = 0
            If (ddlCountry.SelectedValue = _usCountryNameLookup) Then
                txtOtherState.Text = ddlState.SelectedValue
            ElseIf (ddlCountry.SelectedValue = _canadaProvinceNameLookup) Then
                txtOtherState.Text = ddlCanadaProvince.SelectedValue
            End If
        End If

    End Sub

    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_next.Click
        ResetFields()
        If Not (IsPageValid()) Then
            Return
        End If

        Dim rtn As Boolean
        Dim msg As String
        Dim enterby As String
        Dim state As String
        Dim guid As String
        Dim pwd As String = ""
        Dim nickName As String = ""
        Dim invalidateUser As Boolean = False

        guid = ""
        state = ""
        enterby = "webuser"
        msg = ""
        rtn = True

        If ddlCountry.SelectedValue = _usCountryNameLookup Then
            state = ddlState.SelectedValue
        ElseIf ddlCountry.SelectedValue = _canadaProvinceNameLookup Then
            state = ddlCanadaProvince.SelectedValue
        Else
            state = txtOtherState.Text
        End If

        If Len(txtFirstName.Text) > 15 Then
            nickName = (txtFirstName.Text).Substring(0, 15)
        Else
            nickName = txtFirstName.Text
        End If

        Dim contact As Contact = New Contact()
        'contact.Honorific = String.Empty
        If (IsJapaneseLocale()) Then
            contact.FirstName = txtLastName.Text
            contact.LastName = txtFirstName.Text
        Else
            contact.FirstName = txtFirstName.Text
            contact.LastName = txtLastName.Text
        End If
        contact.Title = txtJobTitle.Text
        If (ddlJobFunction.SelectedIndex > 0) Then contact.JobFunction = ddlJobFunction.SelectedValue
        contact.Company = txtCompany.Text
        contact.Department = txtJobDepartment.Text
        contact.Address = txtAddress.Text
        contact.Address2 = txtAddress2.Text
        contact.City = txtCity.Text
        contact.StateProvince = state
        contact.PostalCode = txtPostalCode.Text

        Dim sqlString As String = "SELECT [NAME] FROM [COUNTRY] WHERE [PardotCountry] = @PARDOTCOUNTRY"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PARDOTCOUNTRY", ddlCountry.SelectedItem.Value))
        Dim countryNameLookup As String = CountryRepository.FetchCountries(ConfigurationManager.AppSettings("ConnectionString"), sqlString, sqlParameters.ToArray()).FirstOrDefault().Name

        contact.Country = countryNameLookup
        contact.Phone = txtPhone.Text
        contact.Fax = txtFax.Text
        contact.Email = txtEmail.Text
        contact.Url = txtCompanyUrl.Text
        contact.Application = txtApplication.Text
        contact.NickName = nickName
        contact.LastNameUpper = txtLastName.Text.ToUpper()
        contact.EnteredBy = enterby
        contact.Validated = "Y"
        contact.opt_in_flag = cbxOptIn.Checked
        contact.opt_in_date = DateTime.Now
        contact.opt_in_source = 6   'OptinSource
        contact.opt_in_detail = "Filled out website registration on Teledynelecroy.com"

        If (ContactRepository.GetContactsByUserName(ConfigurationManager.AppSettings("ConnectionString").ToString(), Replace(txtUserName.Text, "'", "''")).Count > 0) Then
            lb_alert.Text = "<font color=red>" + LoadI18N("SLBCA0175", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</font>"
            Return
        End If

        guid = Functions.GeneratedGUID()
        contact.ContactWebId = guid.ToString()
        contact.Password = txtPassword.Text
        contact.UserName = txtUserName.Text
        contact.DateEntered = DateTime.Now
        contact.DateLastModified = DateTime.Now
        contact.EnteredBy = enterby

        Dim retVal As InsertReturnValue(Of Int32) = ContactRepository.InsertContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact)
        Dim fetchedContact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), guid)
        If Not (fetchedContact Is Nothing) Then

            ProcessPardotRequest(fetchedContact)

            Session("ContactID") = fetchedContact.ContactId
            Dim newUserValidationRequest As UserValidationRequest = New UserValidationRequest()
            newUserValidationRequest.ContactFkId = fetchedContact.ContactId
            newUserValidationRequest.DateEntered = DateTime.Now
            If Not (String.IsNullOrEmpty(Session("RedirectTo"))) Then
                newUserValidationRequest.ReturningUrl = Session("RedirectTo").ToString()
            End If
            Dim ticketId As Guid = UserValidationRequestRepository.InsertUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), newUserValidationRequest, True)
            If Not (ticketId = System.Guid.Empty) Then
                FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), ticketId, fetchedContact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
            End If

            Session("Honorific") = fetchedContact.Honorific.Trim
            Session("FirstName") = Left(fetchedContact.FirstName, 1) & Mid(fetchedContact.FirstName, 2)
            Session("LastName") = Trim(fetchedContact.LastName)
            Session("UserName") = Trim(fetchedContact.UserName)
            'Session("Password") = Trim(re("PASSWORD").ToString())
            Session("Title") = Trim(fetchedContact.Title)
            Session("Company") = Trim(fetchedContact.Company)
            Session("Address") = Trim(fetchedContact.Address)
            Session("Address2") = Trim(fetchedContact.Address2)
            Session("City") = Trim(fetchedContact.City)
            Session("State") = Trim(fetchedContact.StateProvince)
            Session("Zip") = Trim(fetchedContact.PostalCode)
            Session("Country") = Trim(fetchedContact.Country)
            Session("Phone") = Trim(fetchedContact.Phone)
            Session("Fax") = Trim(fetchedContact.Fax)
            Session("Email") = Trim(fetchedContact.Email)
            Session("URL") = Trim(fetchedContact.Url)
            Session("Applications") = Trim(fetchedContact.Application)
            Session("JobFunction") = Trim(fetchedContact.JobFunction)
            Session("Department") = Trim(fetchedContact.Department)
        End If

        Dim strInfo As String = ""
        Dim strCRet As String = Chr(13) & Chr(10)
        Dim strBody As String = ""
        Dim strTitle As String = ""
        Dim oMailMessage As New CDO.Message
        Dim oConfig As New CDO.Configuration
        Dim oFields As ADODB.Fields
        Dim oField As ADODB.Field

        oConfig = oMailMessage.Configuration
        oFields = oConfig.Fields

        oField = oFields("http://schemas.microsoft.com/cdo/configuration/sendusing")
        oField.Value = 2   ' CDO.CdoConfiguration.cdoSMTPServer 

        oField = oFields("http://schemas.microsoft.com/cdo/configuration/smtpserver")
        oField.Value = Application("GLBLstrMailServer")  ' TODO:

        oField = oFields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")
        oField.Value = Application("GLBLstrMailServerPort")  ' Server port

        oFields.Update()
        oMailMessage.Configuration = oConfig

        strTitle = "New web registration:"
        strBody = "New web registration:" & strCRet & strCRet & CustomerInfo()
        ' This redirect is here just to avoid sending emails
        ' This line should be commented in Production mode
        ' Response.Redirect("confirmed.aspx")

        If Len(Session("Country")) > 0 Then
            If CStr(Session("Country")) = "United States" And Len(Session("Company")) > 0 Then

            ElseIf Mid(Session("Country"), 1, 5) = "Korea" Then
                strCRet = Chr(13) & Chr(10)
                strBody = "New web registration:" & strCRet & strCRet & CustomerInfo()
                ' Set the message properties and send the message.
                oMailMessage.To = Session("email").ToString()
                oMailMessage.From = "webmaster@teledynelecroy.com"
                oMailMessage.BCC = "kate.kaplan@teledyne.com,james.chan@teledyne.com"
                oMailMessage.Subject = strTitle
                oMailMessage.HTMLBody = "<html><body><h1>" + strBody + "</h1></body></html>"
                Try
                    oMailMessage.Send()
                Catch ex As Exception
                End Try
            ElseIf CStr(Session("Country")) = "Japan" Then
                'localeid=1041
                '***************************************************
                'send email about user registration to contact.jp@teledynelecroy.com
                '***************************************************
                strCRet = Chr(13) & Chr(10)

                ' Set the message properties and send the message.
                oMailMessage.To = Session("email").ToString()
                oMailMessage.From = "webmaster@teledynelecroy.com"
                oMailMessage.BCC = "kate.kaplan@teledynelecroy.com"
                oMailMessage.Subject = strTitle
                oMailMessage.HTMLBody = "<html><body><h1>" + strBody + "</h1></body></html>"
                Try
                    oMailMessage.Send()
                Catch ex As Exception
                End Try
            End If
        End If
        If Len(Session("Country")) > 0 Then
            If Session("Country") = "Japan" Then
                Response.Redirect("AfterFirstReg.aspx")
            End If
        End If
        If (invalidateUser) Then    ' email changed
            DumpUserDataFromSession()
            pnlEntry.Visible = False
            pnlValidationRequired.Visible = True
            PopulatePanel(True)
            Return
        End If
        Response.Redirect("confirmed.aspx") ' new user, or profile updated (!email changed); ContactID should be in session
    End Sub

    Private Sub PopulatePanel(ByVal showUpdateTextBox)
        Dim contactId As Int32 = 0
        Int32.TryParse(ViewState("ContactId").ToString(), contactId)
        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
        If Not (contact Is Nothing) Then
            If (contact.ContactId = contactId) Then
                litRegisteredEmailAddress.Text = contact.Email
                If (showUpdateTextBox) Then
                    pnlRevalidated.Visible = False
                    pnlRevalidation.Visible = True
                Else
                    pnlRevalidation.Visible = False
                    pnlRevalidated.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub Initial()

        sqlstr = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' ORDER BY sort_id"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        ddlLanguage.DataSource = ds
        ddlLanguage.DataBind()
        ddlLanguage.SelectedValue = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()

        sqlstr = "Select * from CONTACT_JOB_FUNCTION order by SORT_ID"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        ddlJobFunction.DataSource = ds
        ddlJobFunction.DataBind()
        li = New ListItem(LoadI18N("SLBCA0573", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), "")
        ddlJobFunction.Items.Insert(0, li)

        sqlstr = "Select * from STATE order by NAME"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        ddlState.DataSource = ds
        ddlState.DataBind()

        li = New ListItem(LoadI18N("SLBCA0737", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), "")
        ddlState.Items.Insert(0, li)

        ' AFNOTE: "Select Province" text should be added to translated text in the I18NTEXT table.
        ' Site is expected to have a short remaining life, so Canada Province list is hard coded instead of creating a table and lookup method in the BLL.
        ddlCanadaProvince.Items.Clear()
        li = New ListItem("Select Province", "")
        ddlCanadaProvince.Items.Insert(0, li)
        ddlCanadaProvince.Items.Insert(1, "Alberta")
        ddlCanadaProvince.Items.Insert(2, "British Columbia")
        ddlCanadaProvince.Items.Insert(3, "Manitoba")
        ddlCanadaProvince.Items.Insert(4, "New Brunswick")
        ddlCanadaProvince.Items.Insert(5, "Newfoundland and Labrador")
        ddlCanadaProvince.Items.Insert(6, "Nova Scotia")
        ddlCanadaProvince.Items.Insert(7, "Nunavut")
        ddlCanadaProvince.Items.Insert(8, "Ontario")
        ddlCanadaProvince.Items.Insert(9, "Prince Edward Island")
        ddlCanadaProvince.Items.Insert(10, "Quebec")
        ddlCanadaProvince.Items.Insert(11, "Saskatchewan")
        ddlCanadaProvince.Items.Insert(12, "Yukon Territories")
        ddlCanadaProvince.DataBind()

        sqlstr = "Select * from COUNTRY WHERE ISO_CODE NOT IN (SELECT ISO_CODE From GEOIP_DENIED_COUNTRY  WHERE ACTIVE_YN = 'y') AND [PardotCountry] IS NOT NULL order by [Name]"
        ds = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        ddlCountry.DataSource = ds
        ddlCountry.DataBind()

        li = New ListItem(LoadI18N("SLBCA0736", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()), "")
        ddlCountry.Items.Insert(0, li)

        Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
        If (countryCode > 0) Then
            Dim pardotCountryLookup As String = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), countryCode).PardotCountry
            If Not (String.IsNullOrEmpty(pardotCountryLookup)) Then
                ddlCountry.SelectedValue = pardotCountryLookup
            Else
                ddlCountry.SelectedValue = _usCountryNameLookup
            End If
        ElseIf Len(Session("Country")) > 0 Then
            Dim pardotCountryLookup As String = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), Functions.GetCountryIDOnName(Session("Country"))).PardotCountry
            If Not (String.IsNullOrEmpty(pardotCountryLookup)) Then
                ddlCountry.SelectedValue = pardotCountryLookup
            Else
                ddlCountry.SelectedValue = _usCountryNameLookup
            End If
        Else
            ddlCountry.SelectedValue = _usCountryNameLookup
        End If
    End Sub

    Function CustomerInfo() As String
        Dim localeId As Int32 = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid"))
        Dim strInfo As String = ""
        Dim strCRet As String = Chr(13) & Chr(10)
        If CInt(localeId) <> 1041 Then
            ' Client data
            If Len(Session("FirstName")) > 0 And Len(Session("LastName")) > 0 Then

                strInfo = strCRet + strCRet + strInfo & "Client: " & Session("LastName") & " " & LoadI18N("SLBCA0160", localeId.ToString()) & " " & Session("Firstname") & strCRet
            End If
            If Len(Session("Title")) > 0 Then
                strInfo = strInfo & "Title: " & Session("Title") & strCRet
            End If
            If Len(Session("Company")) > 0 Then
                strInfo = strInfo & "Company: " & Session("Company") & strCRet
            End If
            If Len(Session("Department")) > 0 Then
                strInfo = strInfo & "Department: " & Session("Department") & strCRet
            End If
            If Len(Session("Address")) > 0 Then
                strInfo = strInfo & "Address: " & Session("Address") & " " & Session("Address2") & strCRet
            End If
            If Len(Session("City")) > 0 Then
                strInfo = strInfo & "City: " & Session("City") & strCRet
            End If
            If Len(Session("State")) > 0 Then
                strInfo = strInfo & "State: " & Session("State") & strCRet
            End If
            If Len(Session("Zip")) > 0 Then
                strInfo = strInfo & "Zip: " & Session("Zip") & strCRet
            End If
            If Len(Session("Country")) > 0 Then
                strInfo = strInfo & "Country: " & Session("Country") & strCRet
            End If
            If Len(Session("Phone")) > 0 Then
                strInfo = strInfo & "Phone: " & Session("Phone") & strCRet
            End If
            If Len(Session("Fax")) > 0 Then
                strInfo = strInfo & "Fax: " & Session("Fax") & strCRet
            End If
            If Len(Session("Email")) > 0 Then
                strInfo = strInfo & "Email: " & Session("Email") & strCRet
            End If
            If Len(Session("URL")) > 0 Then
                strInfo = strInfo & "URL: " & Session("URL") & strCRet
            End If
            If Len(Session("Applications")) > 0 Then
                strInfo = strInfo & "Application: " & Session("Applications") & strCRet
            End If
        Else
            ' Client data for Japan
            If Len(Session("Country")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0083", localeId.ToString()) & ": " & Session("Country") & strCRet
            End If
            If Len(Session("Zip")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0082", localeId.ToString()) & ": " & Session("Zip") & strCRet
            End If
            If Len(Session("City")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0080", localeId.ToString()) & ": " & Session("City") & strCRet
            End If
            If Len(Session("Address")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0079", localeId.ToString()) & ": " & Session("Address") & strCRet
            End If
            If Len(Session("Address2")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0079", localeId.ToString()) & "2: " & Session("Address2") & strCRet
            End If
            If Len(Session("Company")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0078", localeId.ToString()) & ": " & Session("Company") & strCRet
            End If
            If Len(Session("Title")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0077", localeId.ToString()) & ": " & Session("Title") & strCRet
            End If
            If Len(Session("FirstName")) > 0 And Len(Session("LastName")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0076", localeId.ToString()) & ": " & Session("LastName") & " " & Session("Firstname") & " " & LoadI18N("SLBCA0160", localeId.ToString()) & strCRet
            End If
            If Len(Session("Phone")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0084", localeId.ToString()) & ": " & Session("Phone") & strCRet
            End If
            If Len(Session("Fax")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0085", localeId.ToString()) & ": " & Session("Fax") & strCRet
            End If
            If Len(Session("Email")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0086", localeId.ToString()) & ": " & Session("Email") & strCRet
            End If
            If Len(Session("URL")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0087", localeId.ToString()) & ": " & Session("URL") & strCRet
            End If
            If Len(Session("Applications")) > 0 Then
                strInfo = strInfo & LoadI18N("SLBCA0088", localeId.ToString()) & ": " & Session("Applications") & strCRet
            End If
        End If
        ' Insert empty line 
        strInfo = strInfo & strCRet
        'Response.Write strIntro
        ' Return data 
        CustomerInfo = strInfo

    End Function

    Private Sub ResetFields()
        Dim txtBoxes As TextBox() = New TextBox() {txtFirstName, txtLastName, txtCompany, txtAddress, txtCity, txtPostalCode, txtPhone, txtEmail, txtUserName, txtPassword, txtConfirmPassword}
        For Each txtBox As TextBox In txtBoxes
            txtBox.BackColor = Color.White
        Next

        Dim ddls As DropDownList() = New DropDownList() {ddlState, ddlCountry, ddlCanadaProvince}
        For Each ddl As DropDownList In ddls
            ddl.BackColor = Color.White
        Next
    End Sub

    Private Sub SetFieldColorAndSetFocus(ByVal control As Control)
        If (String.Compare(control.GetType().ToString(), "System.Web.UI.WebControls.TextBox", True) = 0) Then
            Dim ctl As TextBox = CType(control, TextBox)
            ctl.BackColor = ColorTranslator.FromHtml("#ff8888")
            If Not (_fieldFocusSet) Then
                _fieldFocusSet = True
                ctl.Focus()
            End If
        ElseIf (String.Compare(control.GetType().ToString(), "System.Web.UI.WebControls.DropDownList", True) = 0) Then
            Dim ctl As DropDownList = CType(control, DropDownList)
            ctl.BackColor = ColorTranslator.FromHtml("#ff8888")
            If Not (_fieldFocusSet) Then
                _fieldFocusSet = True
                ctl.Focus()
            End If
        Else
            Dim ctl As CheckBox = CType(control, CheckBox)
            ctl.ForeColor = ColorTranslator.FromHtml("#ff0000")
            If Not (_fieldFocusSet) Then
                _fieldFocusSet = True
                ctl.Focus()
            End If
        End If
    End Sub

    Private Function IsPageValid() As Boolean
        Dim retVal As Boolean = True
        If Not (String.IsNullOrEmpty(ddlCountry.SelectedValue)) Then
            If (ddlCountry.SelectedValue = _usCountryNameLookup) Then rfvState.Enabled = True
            If (ddlCountry.SelectedValue = _canadaProvinceNameLookup) Then rfvCanadaProvince.Enabled = True
        End If
        cvPassword.Enabled = True
        rfvPassword.Enabled = True
        cvUserName.Enabled = True

        Page.Validate()
        If Not (Page.IsValid) Then
            If (String.IsNullOrEmpty(txtFirstName.Text)) Then SetFieldColorAndSetFocus(txtFirstName)
            If (String.IsNullOrEmpty(txtLastName.Text)) Then SetFieldColorAndSetFocus(txtLastName)
            If (String.IsNullOrEmpty(txtCompany.Text)) Then SetFieldColorAndSetFocus(txtCompany)
            If (String.IsNullOrEmpty(txtAddress.Text)) Then SetFieldColorAndSetFocus(txtAddress)
            If (String.IsNullOrEmpty(txtCity.Text)) Then SetFieldColorAndSetFocus(txtCity)
            If (String.IsNullOrEmpty(ddlState.SelectedValue)) Then SetFieldColorAndSetFocus(ddlState)
            If (String.IsNullOrEmpty(txtPostalCode.Text)) Then SetFieldColorAndSetFocus(txtPostalCode)
            If (String.IsNullOrEmpty(ddlCountry.SelectedValue)) Then SetFieldColorAndSetFocus(ddlCountry)
            If (String.IsNullOrEmpty(txtPhone.Text)) Then SetFieldColorAndSetFocus(txtPhone)
            If (String.IsNullOrEmpty(txtEmail.Text)) Then SetFieldColorAndSetFocus(txtEmail)
            If Not (cvEmail.IsValid) Then SetFieldColorAndSetFocus(txtEmail)

            If (String.IsNullOrEmpty(txtUserName.Text)) Then
                SetFieldColorAndSetFocus(txtUserName)
            End If
            SetFieldColorAndSetFocus(txtPassword)
            rfvPassword.IsValid = False
            SetFieldColorAndSetFocus(txtConfirmPassword)
            cvPassword.IsValid = False
            retVal = False    ' Validation already failed, the code within this block is just for visual cues
        End If
        Return retVal
    End Function

    Private Sub cvPassword_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvPassword.ServerValidate
        cvPassword.ErrorMessage = String.Empty
        Dim passwordValidation As InsertReturnValue(Of Boolean) = FeContactManager.ValidatePasswordIntegrity(txtPassword.Text, txtConfirmPassword.Text)
        args.IsValid = passwordValidation.Success
        If (passwordValidation.ErrorMessages.Count > 0) Then
            cvPassword.ErrorMessage = String.Format("{0}", LoadI18N(passwordValidation.ErrorMessages().FirstOrDefault().ErrorMessage, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        End If
    End Sub

    Private Sub cvEmail_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmail.ServerValidate
        Dim validationMessage As String = ValidateEmail(txtEmail.Text)
        If Not (String.IsNullOrEmpty(validationMessage)) Then
            cvEmail.ErrorMessage = validationMessage
            args.IsValid = False
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvCorrectedEmailAddress_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvCorrectedEmailAddress.ServerValidate
        Dim validationMessage As String = ValidateEmail(txtCorrectedEmailAddress.Text)
        If Not (String.IsNullOrEmpty(validationMessage)) Then
            cvCorrectedEmailAddress.ErrorMessage = validationMessage
            args.IsValid = False
            Return
        End If
        args.IsValid = True
    End Sub

    Private Function ValidateEmail(ByVal providedEmail As String) As String
        If (String.IsNullOrEmpty(providedEmail)) Then
            Return "*"
        End If
        If Not (Utilities.checkValidEmail(providedEmail)) Then
            Return "*" + LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If

        If (String.Compare(providedEmail, "olgab4268@yahoo.com", True) = 0) Then    ' HD KFID-9ZDNEX
            Return "*" + LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If

        If (ContactRepository.GetContactsByEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), providedEmail).Count > 0) Then
            Return "*" + LoadI18N("SLBCA0943", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
        Return String.Empty
    End Function

    Private Sub cvUserName_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvUserName.ServerValidate
        args.IsValid = True
        If (String.IsNullOrEmpty(txtUserName.Text)) Then
            cvUserName.ErrorMessage = "*"
            args.IsValid = False
            Return
        End If
        If (txtUserName.Text.Trim().Length < 4) Then
            cvUserName.ErrorMessage = "*" + LoadI18N("SLBCA0741", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
        btn_next.Text = LoadI18N("SLBCA0436", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Initial()
    End Sub

    Private Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("~/support/user/")
    End Sub

    Private Sub btnCorrectedEmailAddressSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCorrectedEmailAddressSubmit.Click
        Page.Validate("vgResubmit")
        If (Page.IsValid) Then
            Dim contactId As Int32 = 0
            Int32.TryParse(ViewState("ContactId").ToString(), contactId)
            If (contactId > 0) Then
                ContactRepository.UpdateContactEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, txtCorrectedEmailAddress.Text.Trim(), String.Empty)
                ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, "N")
                pnlRevalidation.Visible = False
                pnlRevalidated.Visible = True
                Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                ContactUpdatedRepository.InsertContactUpdated(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact)
                Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequestThatHasNotExpired(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                If Not (userValidationRequest Is Nothing) Then
                    If (userValidationRequest.ContactFkId = contactId) Then
                        FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.UserValidationRequestId, contact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
                    End If
                End If
                PopulatePanel(False)
            Else    ' They could have expired, force them to the login page
                Response.Redirect("~/support/user/")
            End If
        End If
    End Sub

    Private Function IsJapaneseLocale() As Boolean
        Return String.Compare(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString(), LocaleIds.JAPANESE.ToString(), True) = 0
    End Function

    Private Sub ProcessPardotRequest(ByVal contact As Contact)
        Dim url As String = String.Empty
        Dim responseString As String = String.Empty

        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Dim nvc As New NameValueCollection
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtFirstName", contact.FirstName)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtLastName", contact.LastName)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtEmail", contact.Email)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtPhone", contact.Phone)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtFax", contact.Fax)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$ddlCountry", CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString"), contact.Country).PardotCountry)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtAddress", contact.Address)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtAddress2", contact.Address2)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtCity", contact.City)
            If (String.Compare(contact.Country, "United States", True) = 0 Or String.Compare(contact.Country, "Canada", True) = 0) Then
                nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$ddlState", contact.StateProvince)
            Else
                nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$ddlState", "")
            End If
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtPostalCode", contact.PostalCode)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$ddlJobFunction", contact.JobFunction)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtJobTitle", contact.Title)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtJobDepartment", contact.Department)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtCompany", contact.Company)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtCompanyUrl", contact.Url)
            nvc.Add("ctl00$ctl00$SiteContent$CenterColumn$txtApplication", contact.Application)

            url = ConfigurationManager.AppSettings("PardotUserRequest").ToString()
            Dim httpRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8"
            httpRequest.Method = "POST"

            Dim postData As String = ""
            For Each k As String In nvc.Keys
                If (postData.Length > 0) Then
                    postData &= "&"
                End If
                postData &= k & "=" & nvc(k)
            Next
            Dim data As Byte() = Encoding.UTF8.GetBytes(postData)
            httpRequest.ContentLength = data.Length
            Using s As IO.Stream = httpRequest.GetRequestStream()
                s.Write(data, 0, data.Length)
            End Using

            Using httpResponse As HttpWebResponse = CType(httpRequest.GetResponse(), HttpWebResponse)
                responseString = New IO.StreamReader(httpResponse.GetResponseStream()).ReadToEnd()
            End Using
        Catch ex As Exception
            Dim exM As String = String.Format("{0} -- {1} -- {2} -- {3}", ex.Message, ex.InnerException, ex.StackTrace, ex.Source)
            Dim toAddresses As List(Of String) = New List(Of String)
            toAddresses.Add("james.chan@teledyne.com")
            Utilities.SendEmail(toAddresses, "webmaster@teledynelecroy.com", "Exception in validation request async call", exM + " -------URL: " + url + " -------Response: " + responseString, New List(Of String), New List(Of String))
        End Try
    End Sub
#Region "WebMethods"
    <WebMethod()>
    Public Shared Function verifyusernameavailability(ByVal userName As String) As String
        If (IsUserNameTaken(userName)) Then    ' Exists in DB, not valid here
            Return "User name taken, please try another"
        Else
            Return "User name is available"
        End If
    End Function

    Private Shared Function IsUserNameTaken(ByVal userName As String) As Boolean
        Dim sqlString As String = "SELECT * FROM [CONTACT] WHERE [USERNAME] = @USERNAME"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@USERNAME", userName))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            Return True
        End If
        Return False
    End Function

    <WebMethod()>
    Public Shared Function verifyemailnotused(ByVal emailAddress As String) As String
        If (IsEmailAlreadyEntered(emailAddress)) Then    ' Exists in DB, not valid here
            Return "Email already exists, please try another"
        Else
            Return "success"
        End If
    End Function

    Private Shared Function IsEmailAlreadyEntered(ByVal emailAddress As String) As Boolean
        Dim sqlString As String = "SELECT * FROM [CONTACT] WHERE [EMAIL] = @EMAIL"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@EMAIL", emailAddress))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            Return True
        End If
        Return False
    End Function

    <WebMethod()>
    Public Shared Function verifypasswordintegrity(ByVal password As String, ByVal confirmPassword As String) As String
        Dim passwordValidation As InsertReturnValue(Of Boolean) = FeContactManager.ValidatePasswordIntegrity(password, confirmPassword)
        If (passwordValidation.Success) Then
            Return "success"
        Else
            Return passwordValidation.ErrorMessages().FirstOrDefault().ErrorMessage
        End If
    End Function
#End Region
End Class