﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_User_confirmed" Codebehind="confirmed.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <li><a href="default.aspx"><asp:Literal ID="litOne" runat="server" /></a></li>
        <li class="current"><a href="#"><asp:Literal ID="litTwo" runat="server" /></a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><asp:Label ID="lb_alert" runat="server" /></p>
        <p><asp:Label ID="lblOpening" runat="server" /></p>
        <p>
            <asp:Literal ID="litThree" runat="server" />
            <asp:Literal ID="litRegisteredEmailAddress" runat="server" /><br />
            <asp:Panel ID="pnlRevalidation" runat="server" DefaultButton="btnCorrectedEmailAddressSubmit" Visible="true">
                <asp:Literal ID="litFour" runat="server" /><br />
                <asp:TextBox ID="txtCorrectedEmailAddress" runat="server" MaxLength="50" />&nbsp;
                <asp:CustomValidator ID="cvCorrectedEmailAddress" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgResubmit" />&nbsp;
                <asp:Button ID="btnCorrectedEmailAddressSubmit" runat="server" CausesValidation="false" Text="Re-submit your email address" />
            </asp:Panel>
            <asp:Panel ID="pnlRevalidated" runat="server" Visible="false">
                <asp:Literal ID="litFive" runat="server" />
            </asp:Panel>
        </p>
        <p><asp:Literal ID="litSix" runat="server" /></p>
        <p><asp:Button ID="btn_continue" runat="server" Text="Continue" /></p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
	<div class="greyPadding"></div>
	<div class="resources">
		<div class="blue"><asp:Literal ID="litSeven" runat="server" /></div>
        <ul>
            <li class="link"><asp:Literal ID="litEight" runat="server" /></li>
            <li class="link"><asp:Literal ID="litNine" runat="server" /></li>
            <li class="link"><asp:Literal ID="litTen" runat="server" /></li>
            <li class="link"><asp:Literal ID="litEleven" runat="server" /></li>
        </ul>
	</div>
</asp:Content>