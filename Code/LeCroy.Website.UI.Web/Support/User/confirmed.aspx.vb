﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Partial Class Support_User_confirmed
    Inherits RegBasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Users"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - User Registration Confirmation"
        btnCorrectedEmailAddressSubmit.Text = Functions.LoadI18N("SLBCA0937", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0727", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0728", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0935", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0936", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0885", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0182", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0609", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = Functions.LoadI18N("SLBCA0611", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNine.Text = Functions.LoadI18N("SLBCA0614", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTen.Text = Functions.LoadI18N("SLBCA0892", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEleven.Text = Functions.LoadI18N("SLBCA0657", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())

        If Not Me.IsPostBack Then
            Initial()
        End If
    End Sub

    Private Sub Initial()
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        btn_continue.Text = LoadI18N("SLBCA0183", localeId)
        If (localeId = 1041) Then
            lblOpening.Text = String.Format("{0} {1}&nbsp;{2}", getFirstName(), Functions.LoadI18N("SLBCA0179", localeId), Functions.LoadI18N("SLBCA0180", localeId))
            btn_continue.Text = LoadI18N("SLBCA0938", localeId)
        Else
            lblOpening.Text = String.Format("{0} {1},&nbsp;{2}", Functions.LoadI18N("SLBCA0179", localeId), getFirstName(), Functions.LoadI18N("SLBCA0180", localeId))
        End If
        PopulatePanel(True)
    End Sub

    Public Function getFirstName() As String
        Dim result As String = ""
        If Not Session("FirstName") Is Nothing Then
            result = Session("FirstName").ToString()
        End If
        Return result
    End Function

    Private Sub PopulatePanel(ByVal showUpdateTextBox)
        Dim contactId As Int32 = 0
        Int32.TryParse(Session("ContactID").ToString(), contactId)
        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
        If Not (contact Is Nothing) Then
            If (contact.ContactId = contactId) Then
                litRegisteredEmailAddress.Text = contact.Email
                If (showUpdateTextBox) Then
                    pnlRevalidated.Visible = False
                    pnlRevalidation.Visible = True
                Else
                    pnlRevalidation.Visible = False
                    pnlRevalidated.Visible = True
                End If
                If (String.Compare(contact.Validated, "Y", True) = 0) Then ' They're validated, don't show any of the panels for revalidation since they can do it on the update page
                    pnlRevalidated.Visible = False
                    pnlRevalidation.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub btn_continue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_continue.Click
        If Session("RedirectTo") Is Nothing Or Session("RedirectTo") = rootDir + "/Support/User/userprofile.aspx" Then
            Session("RedirectTo") = rootDir + "/default.aspx"
        End If
        Response.Redirect(Session("RedirectTo"))
    End Sub

    Private Sub btnCorrectedEmailAddressSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCorrectedEmailAddressSubmit.Click
        Page.Validate("vgResubmit")
        If (Page.IsValid) Then
            Dim contactId As Int32 = 0
            Int32.TryParse(Session("ContactID").ToString(), contactId)
            If (contactId > 0) Then
                ContactRepository.UpdateContactEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, txtCorrectedEmailAddress.Text.Trim(), String.Empty)
                ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, "N")
                pnlRevalidation.Visible = False
                pnlRevalidated.Visible = True
                Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                ContactUpdatedRepository.InsertContactUpdated(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact)
                Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequestThatHasNotExpired(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                If Not (userValidationRequest Is Nothing) Then
                    If (userValidationRequest.ContactFkId = contactId) Then
                        FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.UserValidationRequestId, contact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
                    End If
                End If
                PopulatePanel(False)
            Else    ' Their session could have expired, force them to the login page
                Response.Redirect("~/support/user/")
            End If
        End If
    End Sub

    Private Sub cvCorrectedEmailAddress_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvCorrectedEmailAddress.ServerValidate
        args.IsValid = True
        If (String.IsNullOrEmpty(txtCorrectedEmailAddress.Text)) Then
            cvCorrectedEmailAddress.ErrorMessage = "*"
            args.IsValid = False
            Return
        End If
        If Not (Utilities.checkValidEmail(txtCorrectedEmailAddress.Text)) Then
            cvCorrectedEmailAddress.ErrorMessage = "*" + LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If

        If (String.Compare(txtCorrectedEmailAddress.Text, "olgab4268@yahoo.com", True) = 0) Then    ' HD KFID-9ZDNEX
            cvCorrectedEmailAddress.ErrorMessage = "*" + LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If

        Dim existingContact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Functions.ValidateIntegerFromSession(Session("ContactID").ToString()))
        If Not (existingContact Is Nothing) Then
            If (String.Compare(existingContact.Email.Trim(), txtCorrectedEmailAddress.Text.Trim(), True) = 0) Then
                Return
            End If
            If (ContactRepository.GetContactsByEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), txtCorrectedEmailAddress.Text.Trim()).Count > 0) Then
                cvCorrectedEmailAddress.ErrorMessage = "*" + LoadI18N("SLBCA0943", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                args.IsValid = False
            End If
        Else
            If (ContactRepository.GetContactsByEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), txtCorrectedEmailAddress.Text).Count > 0) Then
                cvCorrectedEmailAddress.ErrorMessage = "*" + LoadI18N("SLBCA0943", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
                args.IsValid = False
            End If
        End If
    End Sub
End Class