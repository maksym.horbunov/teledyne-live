<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false" Inherits="LeCroy.Website.Oscilloscope_Oscilloscope" MaintainScrollPositionOnPostback="true" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
	<ul>
		<li class="current"><a href="#"><asp:Literal ID="litOne" runat="server" />User Login</a></li>
		<li ><a href="UserRegisterForm.aspx"><asp:Literal ID="litTwo" runat="server" />User Register</a></li>
	</ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="pnlDefault" runat="server" Visible="true">
        <asp:Panel ID="p_signin" runat="server">
            <div class="intro">
                <h1><asp:Literal ID="litThree" runat="server" />User Login</h1>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 200px;">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lbllanguage" runat="server"><asp:Literal ID="litFour" runat="server" /></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="dropdown" DataTextField="name" DataValueField="localeid" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <br />
                <h2><asp:Literal ID="litFive" runat="server" /></h2>
                <p><asp:Literal ID="litSix" runat="server" /><br /><asp:Button ID="btnCreateAccount" runat="server" CausesValidation="false" /></p>
                <h2><asp:Literal ID="litSeven" runat="server" /></h2>
                <asp:Label ID="lbl_login_err" runat="server" />
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_signin">
                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                            <td width="21%" align="right" valign="middle"><asp:Literal ID="litEight" runat="server" /></td>
                            <td width="79%" align="left" valign="top">
                                &nbsp;<asp:TextBox ID="tb_username" runat="server" MaxLength="50" Width="150px" ValidationGroup="singin" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_username" ErrorMessage="*" ValidationGroup="singin" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle"><asp:Literal ID="litNine" runat="server" /></td>
                            <td align="left" valign="top">
                                &nbsp;<asp:TextBox ID="tb_password" runat="server" TextMode="Password" MaxLength="20" Width="150px" ValidationGroup="singin" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_password" ErrorMessage="*" ValidationGroup="singin" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle">&nbsp;</td>
                            <td align="left" valign="top"><asp:Button ID="btn_signin" runat="server" Text="Login" ValidationGroup="singin" /></td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel2" runat="server" DefaultButton="btn_send">
                    <strong><asp:Literal ID="litTen" runat="server" /></strong><br />
                    <asp:Literal ID="litEleven" runat="server" /><br /><br />
                    &nbsp;<asp:TextBox ID="txtEmail" runat="server" ValidationGroup="vgSendEmail" />
                    &nbsp;<asp:Button ID="btn_send" runat="server" Text="Send My User Name and Password" ValidationGroup="vgSendEmail" EnableViewState="False" /><br />
                </asp:Panel>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter a valid email address" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgSendEmail" /><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter your email address" ValidationGroup="vgSendEmail" />
                <asp:Label ID="lb_send" runat="server" />
            </div><br />
        </asp:Panel>
        <asp:Panel ID="p_login" runat="server">
            <div class="intro">
                <table border="0" width="480" cellspacing="0" cellpadding="0" height="108" id="Table5">
                    <tr><td valign="top" align="right" height="38"><p class="SmallBody" align="left"><asp:Label ID="lb_name" runat="server" /></p></td></tr>
                    <tr><td valign="top" height="70"><asp:Button ID="btn_next" runat="server" Text="next" /></td></tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="p_logerr" runat="server">
            <div class="intro">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td width="100%"><small><strong><asp:Label ID="lb_err" runat="server" /></strong></small></td></tr>
                    <tr><td width="100%"><asp:Button ID="btn_sendtoreg" runat="server" Text="next" /></td></tr>
                </table>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlValidationRequired" runat="server" Visible="false">
        <div style="background-color: #ffffff; padding-left: 50px; padding-top: 20px;">
            <asp:Label ID="lblPendingValidation" runat="server" /><br /><br />
            <asp:Label ID="lb_type" runat="server" ForeColor="RoyalBlue" Font-Bold="True">Opt In</asp:Label><br />
            <span style="font-weight: bold;">
                <asp:CheckBox ID="cbxOptIn" runat="server" Text="I agree" /><br />
                By ticking this box you agree to receive information from Teledyne and our authorized sales representatives and distributors about our latest news, events and products and/or services by email. Please see our privacy policy at <a href="http://www.teledyne.com/privacy-policy" target="_blank">http://www.teledyne.com/privacy-policy</a>
            </span><br />
            <asp:Button ID="btnAgreeToOptIn" runat="server" CausesValidation="false" Text="Submit" /><br /><br /><br /><br />
            <asp:Panel ID="pnlValidationRequiredSubPanel" runat="server">
                <asp:Literal ID="litTwelve" runat="server" /> <asp:Literal ID="litRegisteredEmailAddress" runat="server" /><br />
                <asp:Panel ID="pnlRevalidation" runat="server" DefaultButton="btnCorrectedEmailAddressSubmit" Visible="true">
                    <asp:Literal ID="litThirteen" runat="server" /><br />
                    <asp:TextBox ID="txtCorrectedEmailAddress" runat="server" MaxLength="50" />&nbsp;
                        <asp:CustomValidator ID="cvCorrectedEmailAddress" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgResubmit" />&nbsp;
                        <asp:Button ID="btnCorrectedEmailAddressSubmit" runat="server" CausesValidation="false" Text="Re-submit your email address" /><br /><br /><br />
                </asp:Panel>
                <asp:Panel ID="pnlRevalidated" runat="server" Visible="false">
                    <asp:Literal ID="litFourteen" runat="server" /><br /><br /><br />
                </asp:Panel>
            </asp:Panel>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlThankYou" runat="server" Visible="false">
        <div style="background-color: #ffffff; font-size: 125%; padding-left: 50px; padding-top: 20px;">
            <span>Thank you for submitting your registration. A message has been sent to the email address provided to validate your address. Please click on the link or copy into your browser to complete your validation. Thank you.</span><br /><br />
            <asp:Button ID="btnHomePage" runat="server" CausesValidation="false" Text="Return to the home page" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
	<div class="resources">
		<div class="blue"><asp:Literal ID="litFifteen" runat="server" /></div>
        <ul>
            <li class="link"><asp:Literal ID="litTwenty" runat="server" /></li>
            <li class="link"><asp:Literal ID="litSixteen" runat="server" /></li>
            <li class="link"><asp:Literal ID="litSeventeen" runat="server" /></li>
            <li class="link"><asp:Literal ID="litEighteen" runat="server" /></li>
            <li class="link"><asp:Literal ID="litNineteen" runat="server" /></li>
        </ul>
    </div>
</asp:Content>