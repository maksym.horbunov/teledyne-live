﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Partial Class UserLogin
    Inherits RegBasePage
    Public menuURL As String = ""
    Public menuURL2 As String = ""
    Dim sqlstr As String
    Dim ds As DataSet

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Users"
        lblPendingValidation.Text = Functions.LoadI18N("SLBCA0846", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnCorrectedEmailAddressSubmit.Text = Functions.LoadI18N("SLBCA0937", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0727", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwo.Text = Functions.LoadI18N("SLBCA0728", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThree.Text = Functions.LoadI18N("SLBCA0727", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFour.Text = Functions.LoadI18N("SLBCA0320", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFive.Text = Functions.LoadI18N("SLBCA0646", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSix.Text = Functions.LoadI18N("SLBCA0729", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeven.Text = Functions.LoadI18N("SLBCA0647", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEight.Text = Functions.LoadI18N("SLBCA0148", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNine.Text = Functions.LoadI18N("SLBCA0151", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTen.Text = Functions.LoadI18N("SLBCA0153", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEleven.Text = Functions.LoadI18N("SLBCA0154", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwelve.Text = Functions.LoadI18N("SLBCA0935", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litThirteen.Text = Functions.LoadI18N("SLBCA0936", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFourteen.Text = Functions.LoadI18N("SLBCA0885", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litFifteen.Text = Functions.LoadI18N("SLBCA0609", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSixteen.Text = Functions.LoadI18N("SLBCA0611", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litSeventeen.Text = Functions.LoadI18N("SLBCA0614", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litEighteen.Text = Functions.LoadI18N("SLBCA0892", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litNineteen.Text = Functions.LoadI18N("SLBCA0657", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        litTwenty.Text = Functions.LoadI18N("SLBCA0949", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Dim captionID As String = ""
        Dim menuID As String = ""

        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SUPPORT_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SUPPORT_CAPTION_ID
            End If
        End If

        '** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = AppConstants.PROFILE_MENU
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = AppConstants.PROFILE_MENU
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        menuURL2 = "?capid=" + captionID
        menuURL2 += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
            Initial()
        Else
            Session("localeid") = ddlLanguage.SelectedValue
        End If
    End Sub

    Protected Sub Initial()
        'set asp.net controls translations
        LoadButtons()

        sqlstr = "SELECT LOCALEID, NAME  From localeid WHERE disabled = 'n' ORDER BY sort_id"
        ds= DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlstr, New List(Of SqlParameter)().ToArray())
        ddlLanguage.DataSource = ds
        ddlLanguage.DataBind()
        ddlLanguage.SelectedValue = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()

        p_signin.Visible = True
        p_login.Visible = False
        p_logerr.Visible = False

    End Sub

    Protected Sub btn_signin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_signin.Click
        lbl_login_err.Text = String.Empty
        Dim contact As Contact = ContactRepository.Login(ConfigurationManager.AppSettings("ConnectionString").ToString(), tb_username.Text, SQLStringWithOutSingleQuotes(tb_password.Text))
        If Not (contact Is Nothing) Then
            If (contact.ContactId > 0) Then
                ViewState("ContactId") = contact.ContactId ' HACK: Need contact ID ONLY for this page in case they resubmit their email address; don't add it to session
                If Not (String.Compare(contact.Validated, "Y", True) = 0) Then
                    'Dim userValidationRequest As UserValidationRequest = New UserValidationRequest()
                    'userValidationRequest.ContactFkId = contact.ContactId
                    'userValidationRequest.DateEntered = DateTime.Now
                    pnlDefault.Visible = False
                    pnlValidationRequired.Visible = True
                    PopulatePanel(contact, True)
                    'Dim ticketId As Guid = UserValidationRequestRepository.InsertUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest, False)
                    'If Not (ticketId = Guid.Empty) Then
                    '    FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), ticketId, contact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
                    'End If
                    Return
                End If

                If Not (contact.opt_in_flag) Then
                    pnlDefault.Visible = False
                    lblPendingValidation.Visible = False
                    pnlValidationRequiredSubPanel.Visible = False
                    pnlValidationRequired.Visible = True
                    Return
                End If

                Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                If CLng(contact.ContactId) > 0 Then
                    p_signin.Visible = False
                    p_login.Visible = True
                    SetUserDataIfValid(contact.ContactId, contact.ContactWebId)
                    Me.Title = "Welcome Back"
                    If Not (String.Compare(localeId, "1041", True) = 0) Then
                        lb_name.Text = String.Format("{0}, {1}! {2}", Session("FirstName"), Functions.LoadI18N("SLBCA0244", localeId), Functions.LoadI18N("SLBCA0245", localeId))
                    Else
                        lb_name.Text = String.Format("{0}! {1}", Functions.LoadI18N("SLBCA0244", localeId), Functions.LoadI18N("SLBCA0245", localeId)) 'Session("LastName")
                    End If
                Else
                    p_signin.Visible = False
                    p_logerr.Visible = True
                    lb_err.Text = "<small><strong>" + Functions.LoadI18N("SLBCA0251", localeId) + "<br>" + Functions.LoadI18N("SLBCA0252", localeId) + "</strong><br>" + Functions.LoadI18N("SLBCA0253", localeId) + "</small>"
                End If
            Else
                lbl_login_err.Text = "<br /><B>Invalid UserName and/or Password.  Please try again</B>"
            End If
        Else
            lbl_login_err.Text = "<br /><B>Invalid UserName and/or Password.  Please try again</B>"
        End If
    End Sub

    Private Sub btnAgreeToOptIn_Click(sender As Object, e As EventArgs) Handles btnAgreeToOptIn.Click
        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(ViewState("ContactId")))
        If Not (contact Is Nothing) Then
            If (contact.ContactId > 0) Then
                If Not (String.Compare(contact.Validated, "Y", True) = 0) Then
                    Dim userValidationRequest As UserValidationRequest = New UserValidationRequest()
                    userValidationRequest.ContactFkId = contact.ContactId
                    userValidationRequest.DateEntered = DateTime.Now
                    If Not (String.IsNullOrEmpty(Session("RedirectTo"))) Then
                        userValidationRequest.ReturningUrl = Session("RedirectTo").ToString()
                    End If
                    Dim ticketId As Guid = UserValidationRequestRepository.InsertUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest, False)
                    If Not (ticketId = Guid.Empty) Then
                        FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), ticketId, contact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
                    End If
                    ContactRepository.UpdateContactOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, True, 6, "Filled out website registration on Teledynelecroy.com")
                    pnlDefault.Visible = False
                    pnlRevalidated.Visible = False
                    pnlRevalidation.Visible = False
                    pnlValidationRequired.Visible = False
                    pnlThankYou.Visible = True
                Else
                    ContactRepository.UpdateContactOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, True, 6, "Filled out website registration on Teledynelecroy.com")
                    Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
                    If CLng(contact.ContactId) > 0 Then
                        SetUserDataIfValid(contact.ContactId, contact.ContactWebId)
                        If Not (String.IsNullOrEmpty(Session("RedirectTo").ToString())) Then
                            Response.Redirect(Session("RedirectTo").ToString())
                        End If
                        pnlThankYou.Visible = True
                    Else
                        p_signin.Visible = False
                        p_logerr.Visible = True
                        lb_err.Text = "<small><strong>" + Functions.LoadI18N("SLBCA0251", localeId) + "<br>" + Functions.LoadI18N("SLBCA0252", localeId) + "</strong><br>" + Functions.LoadI18N("SLBCA0253", localeId) + "</small>"
                    End If
                End If
            Else
                lbl_login_err.Text = "<br /><B>Invalid UserName and/or Password.  Please try again</B>"
            End If
        Else
            lbl_login_err.Text = "<br /><B>Invalid UserName and/or Password.  Please try again</B>"
        End If
    End Sub

    Protected Sub btn_sendtoreg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_sendtoreg.Click
        Response.Redirect("userregisterform.aspx")
    End Sub

    Protected Sub btn_next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_next.Click
        If Session("RedirectTo") Is Nothing Then
            Session("RedirectTo") = rootDir + "/default.aspx"
        End If
        Response.Redirect(Session("RedirectTo").ToString())
    End Sub

    Protected Sub btnHomePage_Click(sender As Object, e As System.EventArgs) Handles btnHomePage.Click
        Response.Redirect("~/")
    End Sub

    Private Function GetServerName() As String
        Dim serverName As String = Request.ServerVariables("SERVER_NAME")
        Dim internalServers As List(Of String) = New List(Of String)(New String() {"ny-wwwtest", "US1-VTSQL-GEN01", "US1-VTSQL-GEN01.tdy.teledyne.com", "localhost"})
        If Not (String.IsNullOrEmpty(ConfigurationManager.AppSettings("DefaultDomain"))) Then internalServers.Add(ConfigurationManager.AppSettings("DefaultDomain").Replace("http://", String.Empty).Replace("https://", String.Empty))
        If (internalServers.Contains(serverName)) Then
            Return serverName
        End If
        Return "teledynelecroy.com"
    End Function

    Protected Sub btn_send_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_send.Click
        Dim EMail As String = Me.txtEmail.Text
        Dim strBody As String = ""
        Dim strCRet As String = "<br>"
        If Not Utilities.checkValidEmail(EMail) Then
            lb_send.Text = String.Format("<font color='red'>{0}</font>", Functions.LoadI18N("SLBCA0848", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If

        Dim CONTACT_ID, CONTACT_WEB_ID As String
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@EMAIL", SQLStringWithOutSingleQuotes(EMail)))
        Dim contacts As List(Of Contact) = ContactRepository.FetchContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT top 1 USERNAME, PASSWORD,CONTACT_ID,CONTACT_WEB_ID  FROM V_CONTACT WHERE EMAIL = @EMAIL ORDER BY DATE_LAST_MODIFIED DESC", sqlParameters.ToArray())
        If (contacts.Count > 0) Then
            CONTACT_ID = contacts.FirstOrDefault().ContactId.ToString()
            CONTACT_WEB_ID = contacts.FirstOrDefault().ContactWebId.ToString()
        Else
            lb_send.Text = String.Format("<font color='red'>{0}</font>", Functions.LoadI18N("SLBCA0848", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If

        Dim userValidationRequest As UserValidationRequest = New UserValidationRequest()
        userValidationRequest.ContactFkId = Int32.Parse(CONTACT_ID)
        userValidationRequest.DateEntered = DateTime.Now
        If Not (String.IsNullOrEmpty(Session("RedirectTo"))) Then
            userValidationRequest.ReturningUrl = Session("RedirectTo").ToString()
        End If
        Dim ticketId As Guid = UserValidationRequestRepository.InsertUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest, False)
        If (ticketId = Guid.Empty) Then
            lb_send.Text = String.Format("<font color='red'>{0}</font>", Functions.LoadI18N("SLBCA0850", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
            Return
        End If

        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#ConfirmationUrl#>", String.Format("{0}/support/user/passwordreset.aspx?ticketid={1}", ConfigurationManager.AppSettings("DefaultDomain"), ticketId.ToString()))
        replacements.Add("<#ConfirmationCancelUrl#>", String.Format("{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true", ConfigurationManager.AppSettings("DefaultDomain"), ticketId.ToString()))
        Try
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 3, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")), EMail, replacements)
            lb_send.Text = String.Format("<font color='red'>{0}</font>", Functions.LoadI18N("SLBCA0849", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        Catch ex As Exception
            Functions.ErrorHandlerLog(True, ex.Message)
        End Try
    End Sub

    Protected Sub ddlLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLanguage.SelectedIndexChanged
        Session("localeid") = ddlLanguage.SelectedValue
        Session("flgSessionTransfer") = "true"
        LoadButtons()
    End Sub

    Sub LoadButtons()
        Dim localeId As String = Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()
        btn_signin.Text = LoadI18N("SLBCA0730", localeId)
        btn_send.Text = LoadI18N("SLBCA0731", localeId)
        RegularExpressionValidator1.Text = LoadI18N("SLBCA0732", localeId)
        RequiredFieldValidator3.Text = LoadI18N("SLBCA0733", localeId)
        btn_next.Text = LoadI18N("SLBCA0170", localeId)
        btn_sendtoreg.Text = LoadI18N("SLBCA0170", localeId)
        btnCreateAccount.Text = Functions.LoadI18N("SLBCA0751", localeId)
    End Sub

    Private Sub btnCreateAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateAccount.Click
        Response.Redirect("userregisterform.aspx")
    End Sub

    Private Function GetCountryForLocaleId(ByVal localeId As Int32) As String
        Select Case localeId
            Case 1031
                Return "Germany"
            Case 1033
                Return "United States"
            Case 1036
                Return "France"
            Case 1040
                Return "Italy"
            Case 1041
                Return "Japan"
            Case 1042
                Return "Korea"
            Case Else
                Return "United States"
        End Select
    End Function

    Private Function GetLocalizedEmailBody(ByVal localeId As Int32, ByVal ticketId As String) As String
        Dim serverName As String = GetServerName()
        Dim retVal As StringBuilder = New StringBuilder()
        Select Case localeId
            Case 1031
                retVal.Append(String.Format("Teledyne LeCroy Benutzer Profil Center{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Sie haben Ihren Benutzernamen & Password angefragt{0}", Environment.NewLine))
                retVal.AppendLine("Bitte klicken Sie auf den Link unten, um Ihre Information zu sehen:")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("Ihr Benutzername & Password erlauben Ihnen den Zugang zu speziellen Bereichen der Teledyne LeCroy Website und kostenlose Software.{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Falls Sie diese Anfrage nicht gestellt haben, oder diese abbrechen möchten, so klicken Sie bitte hier: https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("Mit freundlichen Grüßen,")
                retVal.AppendLine("Teledyne LeCroy Benutzer Profil Center")
            Case 1036
                retVal.Append(String.Format("Centre de profil utilisateur de Teledyne LeCroy{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Vous avez demandé votre nom d'utilisateur et votre mot de passe{0}", Environment.NewLine))
                retVal.AppendLine("Veuillez cliquer sur le lien ci-dessous pour voir vos informations:")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("Vos nom d'utilisateur et mot de passe vous donnent accès à des sections spécifiques du site Teledyne LeCroy et à des logiciels gratuits Teledyne LeCroy.{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Si telle n''etait pas votre demande, veuillez cliquer ici pour annuler votre demande: https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("Bien cordialement,")
                retVal.AppendLine("Teledyne LeCroy User Profile Center")
            Case 1040
                retVal.Append(String.Format("Centro dei profili-utente Teledyne LeCroy{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Avete richiesto il vostro NomeUtente & Password.{0}", Environment.NewLine))
                retVal.AppendLine("Cliccate sul link sottostante per visualizzare le vostre informazioni:")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("Il vostro NomeUtente & Password vi danno accesso a specifiche sezioni del sito web Teledyne LeCroy ed ai software GRATUITI Teledyne LeCroy.{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("Se non avete inoltrato questa richiesta o se desiderate concellarla cliccate gentilmente qui: https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("Cordiali saluti,")
                retVal.AppendLine("Centro dei profili-utente Teledyne LeCroy")
            Case 1041
                retVal.Append(String.Format("テレダインレクロイのユーザー·プロファイル·センター{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("あなたが要求したユーザー名とパスワード​。{0}", Environment.NewLine))
                retVal.AppendLine("あなたの情報を表示するには、以下のリンクをクリックしてください：")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("ユーザー名とパスワードは、テレダインレクロイのサイトとテレダインレクロイのフリーソフトウェアの特定のセクションへのアクセスを提供します。{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("キャンセルしたい場合は、ここをクリックしてください。 https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("敬具")
                retVal.AppendLine("テレダインレクロイ ユーザープロファイルセンター")
            Case 1042
                retVal.Append(String.Format("Teledyne LeCroy's User Profile Center{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("사용자 이름과 비밀번호를 요청하셨습니다.{0}", Environment.NewLine))
                retVal.AppendLine("귀하의 정보를 보시려면 아래 링크를 클릭하십시오: ")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("사용자 이름과 비밀번호는 텔레다인 르크로이 사이트의 특정 섹션과 무료 소프트웨어를 이용할 수 있도록 허용합니다.{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("이 작업을 원치 않거나 취소하려면 여기를 클릭하십시오 https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("Sincerely,")
                retVal.AppendLine("Teledyne LeCroy User Profile Center")
            Case Else
                retVal.Append(String.Format("Teledyne LeCroy's User Profile Center{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("You requested your UserName & Password.{0}", Environment.NewLine))
                retVal.AppendLine("Please click the link below to view your information:")
                retVal.AppendLine(String.Format("https://{0}/support/user/passwordreset.aspx?ticketid={1}{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine(String.Format("Your UserName & Password provide you with access to specific sections of Teledyne LeCroy's Site and Teledyne LeCroy's FREE Software.{0}", Environment.NewLine))
                retVal.AppendLine(String.Format("If you did not make this request or wish to cancel it, please click here: https://{0}/support/user/passwordreset.aspx?ticketid={1}&cancel=true{2}{2}", serverName, ticketId, Environment.NewLine))
                retVal.AppendLine("Sincerely,")
                retVal.AppendLine("Teledyne LeCroy User Profile Center")
        End Select
        Return retVal.ToString()
    End Function

    Private Sub PopulatePanel(ByVal contact As Contact, ByVal showUpdateTextBox As Boolean)
        If Not (contact Is Nothing) Then
            litRegisteredEmailAddress.Text = contact.Email
            If (showUpdateTextBox) Then
                pnlRevalidated.Visible = False
                pnlRevalidation.Visible = True
            Else
                pnlRevalidation.Visible = False
                pnlRevalidated.Visible = True
            End If
        End If
    End Sub

    Private Sub btnCorrectedEmailAddressSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCorrectedEmailAddressSubmit.Click
        Page.Validate("vgResubmit")
        If (Page.IsValid) Then
            Dim contactId As Int32 = 0
            Int32.TryParse(ViewState("ContactId").ToString(), contactId)
            If (contactId > 0) Then
                ContactRepository.UpdateContactEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, txtCorrectedEmailAddress.Text.Trim(), String.Empty)
                ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId, "N")
                pnlRevalidation.Visible = False
                pnlRevalidated.Visible = True
                Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                ContactUpdatedRepository.InsertContactUpdated(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact)
                Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequestThatHasNotExpired(ConfigurationManager.AppSettings("ConnectionString").ToString(), contactId)
                If Not (userValidationRequest Is Nothing) Then
                    If (userValidationRequest.ContactFkId = contactId) Then
                        FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.UserValidationRequestId, contact.Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
                    End If
                End If
                PopulatePanel(contact, False)
            Else    ' They could have expired, force them to the login page
                Response.Redirect("~/support/user/")
            End If
        End If
    End Sub

    Private Sub cvCorrectedEmailAddress_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvCorrectedEmailAddress.ServerValidate
        args.IsValid = True
        If (String.IsNullOrEmpty(txtCorrectedEmailAddress.Text)) Then
            cvCorrectedEmailAddress.ErrorMessage = "*"
            args.IsValid = False
            Return
        End If
        If Not (Utilities.checkValidEmail(txtCorrectedEmailAddress.Text)) Then
            cvCorrectedEmailAddress.ErrorMessage = "*" + LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If
    End Sub
End Class