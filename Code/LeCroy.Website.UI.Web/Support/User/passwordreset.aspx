﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="passwordreset.aspx.vb" Inherits="LeCroy.Website.support_user_passwordreset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel runat="server" DefaultButton="btnSubmit">
        <div style="background-color: #ffffff; padding-left: 50px; padding-top: 20px;">
            <div style="clear: both; padding-top: 5px;">
                <div style="float: left; text-align: right; width: 20%;">
                    <asp:Label ID="lblUserName" runat="server" Text="Username" />:
                </div>
                <div style="float: left; margin-left: 15px;">
                    <asp:Label ID="lblDatabaseUserName" runat="server" />
                </div>
            </div>
            <div style="clear: both; padding-top: 5px;">
                <div style="float: left; text-align: right; width: 20%;">
                    <asp:Label ID="lblEmailAddress" runat="server" Text="Confirm Your Email Address:" />
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="60" Width="200" />&nbsp;
                        <asp:CustomValidator ID="cvEmail" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="VerifyPassword" />
                </div>
            </div>
            <div style="clear: both; padding-top: 5px;">
                <div style="float: left; text-align: right; width: 20%;">
                    <asp:Label ID="lblNewPassword" runat="server" Text="New Password:" />
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" TextMode="Password" Width="200" />&nbsp;
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" CssClass="error" Display="Dynamic" EnableClientScript="false" ErrorMessage="*" ValidationGroup="VerifyPassword" />
                </div>
            </div>
            <div style="clear: both;">
                <div style="padding-left: 70px; text-align: left; width: 100%;">
                    <asp:Label ID="lblPasswordMessage" runat="server" CssClass="error"><asp:Literal ID="litOne" runat="server" /></asp:Label>
                </div>
            </div>
            <div style="clear: both; padding-top: 10px;">
                <div style="float: left; text-align: right; width: 20%;">
                    <asp:Label ID="lblConfirmNewPassword" runat="server" Text="Confirm New Password:" />
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="20" TextMode="Password" Width="200" />&nbsp;
                        <asp:CustomValidator ID="cvPassword" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="VerifyPassword" />
                </div>
            </div><br /><br />
            <div style="clear: both; padding-top: 5px;">
                <div style="float: left; text-align: right; width: 20%;">
                    &nbsp;
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit" ValidationGroup="VerifyPassword" />&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel" />
                </div>
            </div><br /><br />
        </div>
    </asp:Panel>
</asp:Content>