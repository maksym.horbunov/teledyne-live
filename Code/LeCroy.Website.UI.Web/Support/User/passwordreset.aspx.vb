﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class support_user_passwordreset
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        btnSubmit.Text = Functions.LoadI18N("SLBCA0306", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        btnCancel.Text = Functions.LoadI18N("SLBCA0830", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblUserName.Text = Functions.LoadI18N("SLBCA0148", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblEmailAddress.Text = Functions.LoadI18N("SLBCA0838", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblNewPassword.Text = Functions.LoadI18N("SLBCA0839", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        lblConfirmNewPassword.Text = Functions.LoadI18N("SLBCA0840", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litOne.Text = Functions.LoadI18N("SLBCA0852", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        If Not (HasValidQueryStringParameters()) Then
            NavigateUserAwayFromHere()
        End If
        ValidateUserRequest()
        If Not (IsPostBack) Then
            CheckIfIsCancelRequest()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvEmail_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvEmail.ServerValidate
        args.IsValid = True
        If (String.IsNullOrEmpty(txtEmailAddress.Text)) Then
            cvEmail.ErrorMessage = "*" + Functions.LoadI18N("SLBCA0838", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If
        If Not (Utilities.checkValidEmail(txtEmailAddress.Text)) Then
            cvEmail.ErrorMessage = "*" + Functions.LoadI18N("SLBCA0740", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            args.IsValid = False
            Return
        End If
    End Sub

    Private Sub cvPassword_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvPassword.ServerValidate
        cvPassword.ErrorMessage = String.Empty
        Dim passwordValidation As InsertReturnValue(Of Boolean) = FeContactManager.ValidatePasswordIntegrity(txtPassword.Text, txtConfirmPassword.Text)
        args.IsValid = passwordValidation.Success
        If (passwordValidation.ErrorMessages.Count > 0) Then
            cvPassword.ErrorMessage = String.Format("{0}", Functions.LoadI18N(passwordValidation.ErrorMessages().FirstOrDefault().ErrorMessage, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()))
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Page.Validate()
        If (Page.IsValid) Then
            Dim contact As Contact = ContactRepository.GetContactsByEmail(ConfigurationManager.AppSettings("ConnectionString").ToString(), txtEmailAddress.Text).OrderByDescending(Function(x) x.DateLastModified).FirstOrDefault()
            If (contact Is Nothing) Then
                NavigateUserAwayFromHere()
            End If

            If (String.Compare(contact.Email, "olgab4268@yahoo.com", True) = 0) Then    ' HD KFID-9ZDNEX
                ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, "N")
                NavigateUserAwayFromHere()
            End If

            Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), New Guid(Request.QueryString("ticketid")))
            If (userValidationRequest Is Nothing) Then
                NavigateUserAwayFromHere()
            End If
            If Not (userValidationRequest.ContactFkId = contact.ContactId) Then
                NavigateUserAwayFromHere()
            End If
            ContactRepository.UpdateContactPassword(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, txtPassword.Text)
            UserValidationRequestRepository.DeleteOldUserValidationRequestTickets(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId)

            If (String.Compare(contact.Validated, "N", True) = 0) Then
                ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, "Y")   ' We have already checked their email address; they're valid
            End If
            SetUserDataIfValid(contact.ContactId, contact.ContactWebId)
            Response.Redirect("~/")
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        UserValidationRequestRepository.DeleteOldUserValidationRequestTickets(ConfigurationManager.AppSettings("ConnectionString").ToString(), New Guid(Request.QueryString("ticketid")))
        NavigateUserAwayFromHere()
    End Sub
#End Region

#Region "Page Methods"
    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("ticketid") Is Nothing) Then
            Return False
        End If
        Dim testVal As Guid
        If Not (Guid.TryParse(Request.QueryString("ticketid"), testVal)) Then
            Return False
        End If
        If Not (Request.QueryString("cancel") Is Nothing) Then
            Dim cancelTestVal As Boolean
            If Not (Boolean.TryParse(Request.QueryString("cancel"), cancelTestVal)) Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub NavigateUserAwayFromHere()
        Response.Redirect("~/support/user/default.aspx")
    End Sub

    Private Sub ValidateUserRequest()
        Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), New Guid(Request.QueryString("ticketid")))
        If Not (userValidationRequest Is Nothing) Then
            Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId)
            If Not (contact Is Nothing) Then
                lblDatabaseUserName.Text = contact.UserName
                Return
            End If
        End If
        NavigateUserAwayFromHere()
    End Sub

    Private Sub CheckIfIsCancelRequest()
        If Not (Request.QueryString("cancel") Is Nothing) Then
            If (Boolean.Parse(Request.QueryString("cancel"))) Then
                UserValidationRequestRepository.DeleteOldUserValidationRequestTickets(ConfigurationManager.AppSettings("ConnectionString").ToString(), New Guid(Request.QueryString("ticketid")))
                NavigateUserAwayFromHere()
            End If
        End If
    End Sub
#End Region
End Class