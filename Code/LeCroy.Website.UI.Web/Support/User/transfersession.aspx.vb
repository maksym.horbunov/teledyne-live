﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class Support_User_transfersession
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        WriteSessionToDatabase()
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub WriteSessionToDatabase()
        Dim uniqueSessionId As Boolean = False
        Dim sessionId As Guid = Guid.NewGuid()
        Do While Not uniqueSessionId
            If (AspSessionStateRepository.DoesGuidExist(ConfigurationManager.AppSettings("ConnectionString").ToString(), sessionId.ToString())) Then
                sessionId = Guid.NewGuid()
                Continue Do
            End If
            uniqueSessionId = True
        Loop
        For Each s As String In Session.Keys
            If (String.Compare(s, "WebUiDestination", True) = 0 Or String.Compare(s, "RedirectTo", True) = 0) Then  ' Neither of these keys are needed in the subweb
                Continue For
            End If
            Dim aspSessionState As AspSessionState = New AspSessionState()
            aspSessionState.Guid = sessionId.ToString()
            aspSessionState.SessionKey = s
            aspSessionState.SessionValue = Session(s).ToString()
            AspSessionStateRepository.InsertAspSessionState(ConfigurationManager.AppSettings("ConnectionString").ToString(), aspSessionState)
        Next
        Response.Redirect(String.Format(Session("SubWebUiDestination").ToString(), sessionId.ToString()))   ' Subweb should have supplied this as a string format
    End Sub
#End Region
End Class