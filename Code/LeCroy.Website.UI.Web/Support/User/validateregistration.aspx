﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="validateregistration.aspx.vb" Inherits="LeCroy.Website.support_user_validateregistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; padding-left: 50px; padding-top: 20px;">
        <asp:Panel ID="pnlOptInRequired" runat="server" DefaultButton="" Visible="false">
            <asp:Label ID="lb_type" runat="server" ForeColor="RoyalBlue" Font-Bold="True">Opt In</asp:Label><br />
            <span style="font-weight: bold;">
                <asp:CheckBox ID="cbxOptIn" runat="server" Text="I agree" /><br />
                By ticking this box you agree to receive information from Teledyne and our authorized sales representatives and distributors about our latest news, events and products and/or services by email. Please see our privacy policy at <a href="http://www.teledyne.com/privacy-policy" target="_blank">http://www.teledyne.com/privacy-policy</a>
            </span><br />
            <asp:Button ID="btnAgreeToOptIn" runat="server" CausesValidation="false" Text="Submit" /><br /><br /><br /><br />
        </asp:Panel>
        <asp:Panel ID="pnlSuccess" runat="server" DefaultButton="btnContinue" Visible="false">
            <asp:Label ID="lblSuccessMessage" runat="server" Text="Your account has been successfully validated." /><br />
            <asp:LinkButton ID="btnContinue" runat="server" CausesValidation="false" Text="Continue" /><br /><br />
        </asp:Panel>
        <asp:Panel ID="pnlFailure" runat="server" Visible="true">
            <asp:Label ID="lblFailureMessage1" runat="server" Text="There appears to be a problem validating your request. Please confirm this is your email address:" />&nbsp;
                <b><asp:Literal ID="litEmailAddress" runat="server" /></b><br />
            <asp:Label ID="lblFailureMessage2" runat="server" Text="If that is your email address, we will " />
                <asp:LinkButton ID="btnRequestNewTicket" runat="server" CausesValidation="false" Text="send you a new ticket" />.<br /><br />
            <asp:Label ID="lblFailureMessage3" runat="server" Text="If this problem persists, please contact the webmaster." /><br /><br />
        </asp:Panel>
    </div>
</asp:Content>