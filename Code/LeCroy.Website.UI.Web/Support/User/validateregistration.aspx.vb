﻿Imports System.Net
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class support_user_validateregistration
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (HasValidQueryStringParameters()) Then
            NavigateUserAwayFromHere()
        End If
        If Not (IsPostBack) Then
            ValidateUserRequest(New Guid(Request.QueryString("ticketid")))
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinue.Click
        If Not (String.IsNullOrEmpty(Session("RedirectTo").ToString())) Then
            Response.Redirect(Session("RedirectTo").ToString())
        End If
        Response.Redirect("~/")
    End Sub

    Private Sub btnRequestNewTicket_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRequestNewTicket.Click
        Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), New Guid(Request.QueryString("ticketid")))
        If Not (userValidationRequest Is Nothing) Then
            Dim newUserValidationRequest As UserValidationRequest = New UserValidationRequest()
            newUserValidationRequest.ContactFkId = userValidationRequest.ContactFkId
            newUserValidationRequest.DateEntered = DateTime.Now
            newUserValidationRequest.ReturningUrl = userValidationRequest.ReturningUrl
            Dim newTicketId As Guid = UserValidationRequestRepository.InsertUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), newUserValidationRequest, True)
            If Not (newTicketId = Guid.Empty) Then
                FeContactManager.GenerateUserValidationRequestEmailAsPlainText(ConfigurationManager.AppSettings("ConnectionString").ToString(), newTicketId, ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId).Email, ConfigurationManager.AppSettings("DefaultDomain"), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")))
            End If
        End If
        NavigateUserAwayFromHere()
    End Sub

    Private Sub btnAgreeToOptIn_Click(sender As Object, e As EventArgs) Handles btnAgreeToOptIn.Click
        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(ViewState("ContactId")))
        If Not (contact Is Nothing) Then
            If (contact.ContactId > 0) Then
                ContactRepository.UpdateContactOptIn(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, True, 6, "Filled out website registration on Teledynelecroy.com")
                Dim testVal As Guid
                If Not (Guid.TryParse(ViewState("TicketId").ToString(), testVal)) Then
                    FailingPanel()
                    Return
                End If
                ValidateUserRequest(testVal)
            Else
                FailingPanel()
                Return
            End If
        Else
            FailingPanel()
            Return
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("ticketid") Is Nothing) Then
            Return False
        End If
        Dim testVal As Guid
        If Not (Guid.TryParse(Request.QueryString("ticketid"), testVal)) Then
            Return False
        End If
        Return True
    End Function

    Private Sub NavigateUserAwayFromHere()
        Response.Redirect("~/support/user/default.aspx")
    End Sub

    Private Sub ValidateUserRequest(ByVal ticketId As Guid)
        Dim url As String = String.Empty
        Dim responseString As String = String.Empty
        Dim userValidationRequest As UserValidationRequest = UserValidationRequestRepository.GetUserValidationRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), ticketId)
        If (userValidationRequest Is Nothing) Then
            NavigateUserAwayFromHere()
        End If

        Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId)
        If (contact Is Nothing) Then
            NavigateUserAwayFromHere()
        End If

        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            url = String.Format("https://go.teledynelecroy.com/l/48392/2018-10-23/76hmn7?email={0}", contact.Email.ToString())
            Dim httpRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpRequest.ContentType = "application/x-www-form-urlencoded"
            httpRequest.Method = "POST"

            Dim postData As String = "" 'String.Format("email={0}", contact.Email.ToString())
            Dim data As Byte() = Encoding.ASCII.GetBytes(postData)
            httpRequest.ContentLength = data.Length
            Using s As IO.Stream = httpRequest.GetRequestStream()
                s.Write(data, 0, data.Length)
            End Using

            Using httpResponse As HttpWebResponse = CType(httpRequest.GetResponse(), HttpWebResponse)
                responseString = New IO.StreamReader(httpResponse.GetResponseStream()).ReadToEnd()
            End Using
        Catch ex As Exception
            Dim exM As String = String.Format("{0} -- {1} -- {2} -- {3}", ex.Message, ex.InnerException, ex.StackTrace, ex.Source)
            Dim toAddresses As List(Of String) = New List(Of String)
            toAddresses.Add("james.chan@teledyne.com")
            Utilities.SendEmail(toAddresses, "webmaster@teledynelecroy.com", "Exception in validation request async call", exM + " -------URL: " + url + " -------Response: " + responseString, New List(Of String), New List(Of String))
        End Try

        If (EmailDomainBlackListRepository.IsEmailDomainBlackListed(ConfigurationManager.AppSettings("ConnectionString"), contact.Email)) Then
            NavigateUserAwayFromHere()
        End If

        ViewState("ContactId") = contact.ContactId
        ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId, "Y")
        contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), userValidationRequest.ContactFkId)
        If (contact Is Nothing) Then
            NavigateUserAwayFromHere()
        End If

        If (String.Compare(contact.Email, "olgab4268@yahoo.com", True) = 0) Then    ' HD KFID-9ZDNEX
            ContactRepository.UpdateContactValidation(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId, "N")
            NavigateUserAwayFromHere()
        End If

        If Not (String.IsNullOrEmpty(userValidationRequest.ReturningUrl)) Then
            Session("RedirectTo") = userValidationRequest.ReturningUrl
        End If

        Dim correctedLocaleId As Int32 = 1033
        Dim country As Country = CountryRepository.GetCountry(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.Country)
        If Not (country Is Nothing) Then
            If (country.LocaleId.HasValue) Then
                Session("localeid") = country.LocaleId.Value.ToString()    ' Override based on country
                correctedLocaleId = country.LocaleId.Value.ToString()
            End If
        End If
        BindLabels(correctedLocaleId)

        If (String.Compare(contact.Validated, "Y", True) = 0) Then
            UserValidationRequestRepository.DeleteOldUserValidationRequestTickets(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId)
            pnlFailure.Visible = False
            pnlSuccess.Visible = True
            SetUserDataIfValid(contact.ContactId, contact.ContactWebId)

            If Not (String.IsNullOrEmpty(userValidationRequest.ReturningUrl)) Then
                Response.Redirect(userValidationRequest.ReturningUrl)
            End If
        Else
            pnlSuccess.Visible = False
            pnlFailure.Visible = True
            litEmailAddress.Text = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), contact.ContactId).Email
        End If
    End Sub

    Private Sub BindLabels(ByVal correctedLocaleId As String)
        lblSuccessMessage.Text = Functions.LoadI18N("SLBCA0841", correctedLocaleId)
        btnContinue.Text = Functions.LoadI18N("SLBCA0183", correctedLocaleId)
        lblFailureMessage1.Text = Functions.LoadI18N("SLBCA0842", correctedLocaleId)
        lblFailureMessage2.Text = Functions.LoadI18N("SLBCA0843", correctedLocaleId)
        lblFailureMessage3.Text = Functions.LoadI18N("SLBCA0845", correctedLocaleId)
        btnRequestNewTicket.Text = Functions.LoadI18N("SLBCA0844", correctedLocaleId)
    End Sub

    Private Sub FailingPanel()
        pnlOptInRequired.Visible = False
        pnlSuccess.Visible = False
        pnlFailure.Visible = True
    End Sub
#End Region
End Class