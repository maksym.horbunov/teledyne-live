<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.Support_SupportHome" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <asp:Literal ID="lb_leftmenu" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div class="intro">
        <p><img src="<%= rootDir%>/images/category/headers/hd_support_overview.gif" alt="Support Overview"></p>
        <p>Building long-term relationships with our customers is a deeply-held commitment at LeCroy. This commitment applies to our product development, manufacturing, sales, and support processes. It is reflected in the unprecedented level of customization that we build into our products, enabling the companies who use our equipment to easily tailor solutions for their specific applications.</p>
        <p>At LeCroy, we believe an important way that we can provide significant added value for our customers is by extending the usable life of their LeCroy equipment. We believe our service practices are the best in the industry.</p>
        <p>Please use the links on left hand side menu to get support for specific topic.</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
</asp:Content>