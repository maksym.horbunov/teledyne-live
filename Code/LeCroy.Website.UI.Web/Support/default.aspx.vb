Imports LeCroy.Library.VBUtilities

Partial Class Support_SupportHome
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Technical Library"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Support Overview"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = ""
        Dim menuID As String = ""
        captionID = AppConstants.SUPPORT_CAPTION_ID
        menuID = getDefaultMenu(captionID)
        Session("menuSelected") = captionID
        lb_leftmenu.Text = Functions.TopMenu(captionID, rootDir)
    End Sub
End Class