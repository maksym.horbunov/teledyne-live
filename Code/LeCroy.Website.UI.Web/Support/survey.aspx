﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="survey.aspx.vb" Inherits="LeCroy.Website.support_survey" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="pnlSurveysExist" runat="server">
        <div style="padding-left: 30px; padding-top: 20px;"><h1>Please select your language to continue:</h1></div>
        <asp:Panel ID="pnlEnglishUk" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkEnglishUk" runat="server" Text="English" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlGerman" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkGerman" runat="server" Text="Deutsch" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlFrench" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkFrench" runat="server" Text="Fran&#231;ais" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlItalian" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkItalian" runat="server" Text="Italiano" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlKorean" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkKorean" runat="server" Text="한국어" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlJapanese" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkJapanese" runat="server" Text="日本語" /></b></div>
        </asp:Panel><br />
        <asp:Panel ID="pnlChinese" runat="server" Visible="false">
            <div style="padding-left: 50px;"><b><asp:HyperLink ID="hlkChinese" runat="server" Text="中文" /></b></div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlNoSurveysExist" runat="server" Visible="false">
        <div style="padding-left: 50px;">There are currently no active surveys.</div>
    </asp:Panel>
    <br /><br />
</asp:Content>