﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs

Public Class support_survey
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim meta As HtmlMeta = New HtmlMeta()
        meta.Name = "robots"
        meta.Content = "noindex"
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(meta)
        Me.Master.PageContentHeader.TopText = "Support"
        Me.Master.PageContentHeader.BottomText = "Service Customer Satisfaction Survey"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindPage()
        Dim surveys As List(Of Survey) = SurveyRepository.GetActiveSurveysForDate(ConfigurationManager.AppSettings("ConnectionString").ToString())
        If (surveys.Count <= 0) Then
            pnlSurveysExist.Visible = False
            pnlNoSurveysExist.Visible = True
            Return
        End If

        pnlNoSurveysExist.Visible = False
        pnlSurveysExist.Visible = True
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.ENGLISH_UK).Count > 0) Then
            BindControls(surveys, LocaleIds.ENGLISH_UK, pnlEnglishUk, hlkEnglishUk)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.GERMAN).Count > 0) Then
            BindControls(surveys, LocaleIds.GERMAN, pnlGerman, hlkGerman)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.FRENCH).Count > 0) Then
            BindControls(surveys, LocaleIds.FRENCH, pnlFrench, hlkFrench)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.ITALIAN).Count > 0) Then
            BindControls(surveys, LocaleIds.ITALIAN, pnlItalian, hlkItalian)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.KOREAN).Count > 0) Then
            BindControls(surveys, LocaleIds.KOREAN, pnlKorean, hlkKorean)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.JAPANESE).Count > 0) Then
            BindControls(surveys, LocaleIds.JAPANESE, pnlJapanese, hlkJapanese)
        End If
        If (surveys.Where(Function(x) x.LocaleFkId = LocaleIds.CHINESE_PRC).Count > 0) Then
            BindControls(surveys, LocaleIds.CHINESE_PRC, pnlChinese, hlkChinese)
        End If
    End Sub

    Private Sub BindControls(ByVal surveys As List(Of Survey), ByVal localeId As Int32, ByVal pnl As Panel, ByVal hlk As HyperLink)
        Dim survey As Survey = surveys.Where(Function(x) x.LocaleFkId = localeId).FirstOrDefault()
        If Not (survey Is Nothing) Then
            pnl.Visible = True
            If (survey.SubscriptionLinkFkId.HasValue) Then
                hlk.NavigateUrl = String.Format("{0}/doc.aspx?d={1}", ConfigurationManager.AppSettings("DefaultDomain"), survey.SubscriptionLinkFkId.Value)
            Else
                hlk.Attributes.Add("onclick", String.Format("GaEventPush('OutboundLink', '{0}');", survey.Url))
                hlk.NavigateUrl = survey.Url
            End If
        End If
    End Sub
#End Region
End Class