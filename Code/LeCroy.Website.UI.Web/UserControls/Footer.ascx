﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Footer.ascx.vb" Inherits="LeCroy.Website.UserControls_Footer" %>
<ul id="ulHeaderQuickLinks" runat="server" visible="false">
    <asp:Repeater ID="rptHeaders" runat="server">
        <ItemTemplate>
            <li>
                <div class="h3"><asp:Literal ID="litHeader" runat="server" /></div>
                <ul>
                    <asp:Repeater ID="rptElements" runat="server">
                        <ItemTemplate>
                            <li><asp:HyperLink ID="hlkElement" runat="server" /></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<div class="copyright">
    <p><a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px" /></a>&nbsp;&nbsp;<a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px" /></a>&nbsp;&nbsp;<a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px" /></a>&nbsp;&nbsp;<a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px" /></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px" /></a></p>
    <p>&#169; <asp:Literal ID="litCopyrightYear" runat="server" />&nbsp;Teledyne LeCroy</p>
    <asp:Repeater ID="rptLinks" runat="server">
        <ItemTemplate>
            <asp:HyperLink ID="hlkLink" runat="server" /><asp:Literal ID="litSplitter" runat="server" Text=" | " Visible="false" />
        </ItemTemplate>
    </asp:Repeater>
    <p><a href="http://teledyne.com/" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a></p>
</div>