﻿Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class UserControls_Footer
    Inherits UserControl

#Region "Variables/Keys"
    Public ShowQuickLinks As Boolean = False
    Private _copyrightLinkCount As Int32 = 0
    Private _copyrightLinksCounted As Int32 = 0
#End Region

#Region "Page Events"
#End Region

#Region "Control Events"
    Private Sub rptHeaders_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptHeaders.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedMenu = CType(e.Item.DataItem, ExtendedMenu)
                Dim litHeader As Literal = CType(e.Item.FindControl("litHeader"), Literal)
                Dim rptElements As Repeater = CType(e.Item.FindControl("rptElements"), Repeater)

                litHeader.Text = row.MenuName
                If (row.SubMenu.Count() > 0) Then
                    AddHandler rptElements.ItemDataBound, AddressOf rptElements_ItemDataBound
                    rptElements.DataSource = row.SubMenu
                    rptElements.DataBind()
                End If
        End Select
    End Sub

    Private Sub rptElements_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As SubMenu = CType(e.Item.DataItem, SubMenu)
                Dim hlkElement As HyperLink = CType(e.Item.FindControl("hlkElement"), HyperLink)

                hlkElement.NavigateUrl = row.Url
                hlkElement.Text = row.SubMenuName
        End Select
    End Sub

    Private Sub rptLinks_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptLinks.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedMenu = CType(e.Item.DataItem, ExtendedMenu)
                Dim hlkLink As HyperLink = CType(e.Item.FindControl("hlkLink"), HyperLink)
                Dim litSplitter As Literal = CType(e.Item.FindControl("litSplitter"), Literal)

                _copyrightLinksCounted += 1
                hlkLink.NavigateUrl = row.Url
                hlkLink.Text = row.MenuName
                If (_copyrightLinksCounted < _copyrightLinkCount) Then
                    litSplitter.Visible = True
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Public Sub BindControl(ByVal extendedMenu As List(Of ExtendedMenu))
        If (ShowQuickLinks) Then
            ulHeaderQuickLinks.Visible = True
            BindLinks(extendedMenu.Where(Function(x) x.IsExpandable).OrderBy(Function(x) x.SortId).ToList())
        End If
        BindCopyright(extendedMenu.Where(Function(x) Not (x.IsExpandable)).OrderBy(Function(x) x.SortId).ToList())
    End Sub

    Private Sub BindLinks(ByVal extendedMenu As List(Of ExtendedMenu))
        If (extendedMenu.Count() > 0) Then
            rptHeaders.DataSource = extendedMenu
            rptHeaders.DataBind()
        End If
    End Sub

    Private Sub BindCopyright(ByVal extendedMenu As List(Of ExtendedMenu))
        _copyrightLinkCount = extendedMenu.Count()
        litCopyrightYear.Text = DateTime.Now.Year.ToString()
        If (extendedMenu.Count() > 0) Then
            rptLinks.DataSource = extendedMenu
            rptLinks.DataBind()
        End If
    End Sub
#End Region
End Class