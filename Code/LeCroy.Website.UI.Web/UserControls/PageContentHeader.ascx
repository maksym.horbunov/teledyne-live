﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PageContentHeader.ascx.vb" Inherits="LeCroy.Website.UserControls_PageContentHeader" %>
<div class="content-header" id="divContentHeader" runat="server">
    <div class="content-header-text">
        <div class="text16">
            <asp:Label ID="lblTopText" runat="server" />
        </div>
        <div class="text16c">
            <asp:Label ID="lblBottomText" runat="server" />
        </div>
    </div>
</div>