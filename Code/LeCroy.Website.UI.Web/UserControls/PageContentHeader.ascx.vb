﻿Public Class UserControls_PageContentHeader
    Inherits UserControl

#Region "Variables/Keys"
    Public Property TopText As String = String.Empty
    Public Property BottomText As String = String.Empty
    Public Property IsRightMaster As Boolean = False
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (String.IsNullOrEmpty(TopText)) Then lblTopText.Text = TopText
        If Not (String.IsNullOrEmpty(BottomText)) Then lblBottomText.Text = BottomText
        If (IsRightMaster) Then
            divContentHeader.Attributes.Remove("class")
            divContentHeader.Attributes.Add("class", "content-header-right")
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class