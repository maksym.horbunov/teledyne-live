﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Resources.ascx.vb" Inherits="LeCroy.Website.UserControls_Resources" %>
<asp:Panel ID="pnlProductResources" runat="server" Visible="true">
    <asp:HyperLink ID="hlkProductResources" runat="server"><img src="/images/sidebar/lft_hd_product_resources.gif" alt="Product Resources" border="0" height="23" width="203" /></asp:HyperLink>
    <div class="resources">
	    <ul id="ulResourcesGroup1" runat="server" /><asp:Literal ID="litGroup1To2Break" runat="server" />
        <ul id="ulResourcesGroup2" runat="server" /><asp:Literal ID="litGroup2To3Break" runat="server" />
        <ul id="ulResourcesGroup3" runat="server" />
    </div>
</asp:Panel>