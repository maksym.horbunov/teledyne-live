﻿Imports System.Data.SqlClient
Imports LeCroy.Website.BLL
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities

Public Class UserControls_Resources
    Inherits UserControl

#Region "Variables/Keys"
    Public Property CategoryId As Nullable(Of Int32)
    Public Property ProductGroupId As Nullable(Of Int32)
    Public Property ProductSeriesId As Nullable(Of Int32)
    Public Property ProtocolStandardId As Nullable(Of Int32)
    Public Property ProductId As Nullable(Of Int32)
    Public Property IsOptionsSubPage As Boolean = False
    Public Property ShowBuy As Boolean = False
    Public Property ShowRequestDemo As Boolean = False
    Public Property ShowRequestQuote As Boolean = False
    Public Property ShowWhereToBuy As Boolean = False

    Private Const DEFAULT_DOWNLOAD_IMAGE_TEMPLATE As String = "/images/icons/download_icon{0}x{1}.png"
    Private Const DEFAULT_LINKICON_IMAGE_TEMPLATE As String = "/images/icons/link_icon{0}x{1}.png"
    Private Const DEFAULT_WHERETOBUY_IMAGE_TEMPLATE As String = "/images/icons/wheretobuy_icon{0}x{1}.png"

    Private STORE_URL_BASE As String = ConfigurationManager.AppSettings("StoreBaseUrl")

    Private _categorySeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _protocolSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _seriesSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _productSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _docTypeIds As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (ProductGroupId.HasValue) Then IsOptionsSubPage = False
        BindControl()
        If (ulResourcesGroup1.Controls.Count > 0 And (ulResourcesGroup2.Controls.Count > 0 Or ulResourcesGroup3.Controls.Count > 0)) Then
            litGroup1To2Break.Text = "<br />"
        End If
        If (ulResourcesGroup2.Controls.Count > 0 And ulResourcesGroup3.Controls.Count > 0) Then
            litGroup2To3Break.Text = "<br />"
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindLookups()
        Dim categories As List(Of Category) = CategoryRepository.FetchCategories(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [CATEGORY_ID],[SeoValue] FROM [CATEGORY] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each c As Category In categories
            _categorySeos.Add(c.CategoryId, c.SeoValue)
        Next
        Dim protocols As List(Of ProtocolStandard) = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PROTOCOL_STANDARD_ID],[SeoValue] FROM [PROTOCOL_STANDARD] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As ProtocolStandard In protocols
            _protocolSeos.Add(p.ProtocolStandardId, p.SeoValue)
        Next
        Dim productSeries = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCT_SERIES_ID],[SeoValue] FROM [PRODUCT_SERIES] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each ps As ProductSeries In productSeries
            _seriesSeos.Add(ps.ProductSeriesId, ps.SeoValue)
        Next
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCTID],[SeoValue] FROM [PRODUCT] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As Product In products
            _productSeos.Add(p.ProductId, p.SeoValue)
        Next
        Dim docTypes As List(Of DocumentType) = DocumentTypeRepository.FetchDocumentTypes(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [DOC_TYPE_ID],[SeoValue] FROM [DOCUMENT_TYPE] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each dt As DocumentType In docTypes
            _docTypeIds.Add(dt.DocTypeId, dt.SeoValue)
        Next
    End Sub

    Private Sub BindControl()
        BindLookups()
        Dim documentObjects As List(Of DocumentObject) = GetData()
        hlkProductResources.NavigateUrl = GetResourceDefaultPageUrl()
        If (documentObjects.Count > 0) Then
            Dim dataSheets As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.DATASHEETS).ToList()
            If (dataSheets.Count > 0) Then
                CommonGroup1LinkBuilder(dataSheets, DocumentTypeIds.DATASHEETS, "Datasheets", ulResourcesGroup1)
            End If
            Dim competitiveComparisons As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = 29).ToList()
            If (competitiveComparisons.Count > 0) Then
                CommonGroup1LinkBuilder(competitiveComparisons, 29, "Competitive Comparisons", ulResourcesGroup1)
            End If
            Dim factSheets As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.FACT_SHEET).ToList()
            If (factSheets.Count > 0) Then
                CommonGroup1LinkBuilder(factSheets, DocumentTypeIds.FACT_SHEET, "Fact Sheets", ulResourcesGroup1)
            End If

            AddPromotionsLink()
            If (ShowRequestDemo) Then AddRequestDemoLink()
            If (ShowWhereToBuy) Then AddWhereToBuyLink()

            Dim analysisSoftware As List(Of DocumentObject) = (From docObjs In documentObjects Where docObjs.DocTypeId = DocumentTypeIds.SOFTWARE).ToList()
            If (analysisSoftware.Count > 0) Then
                AddAnalysisSoftwareLink(analysisSoftware)
            End If
            Dim appNotes As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.APP_NOTES).ToList()
            If (appNotes.Count > 0) Then
                CommonGroup2LinkBuilder(appNotes, DocumentTypeIds.APP_NOTES, "Application Notes", ulResourcesGroup3, True)
            End If
            Dim confProceedings As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.CONFERENCE_PROCEEDINGS).ToList()
            If (confProceedings.Count > 0) Then
                CommonGroup2LinkBuilder(confProceedings, DocumentTypeIds.CONFERENCE_PROCEEDINGS, "Conference Proceedings", ulResourcesGroup3, True)
            End If
            Dim firmware As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.FIRMWARE).ToList()
            If (firmware.Count > 0) Then
                CommonGroup2LinkBuilder(firmware, DocumentTypeIds.FIRMWARE, "Firmware", ulResourcesGroup3, False)
            End If
            ' HACK:
            Dim wavefirmware As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = 25).ToList()
            If (wavefirmware.Count > 0) Then
                CommonGroup2LinkBuilder(wavefirmware, 25, "Firmware Upgrades", ulResourcesGroup3, False)
            End If
            Dim manuals As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.PRODUCT_MANUALS).ToList()
            If (manuals.Count > 0) Then
                AddManualsLink(manuals)
            End If
            Dim programmingExamples As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.PROGRAMMING_EXAMPLES).ToList()
            If (programmingExamples.Count > 0) Then
                CommonGroup2LinkBuilder(programmingExamples, DocumentTypeIds.PROGRAMMING_EXAMPLES, "Programming Examples", ulResourcesGroup3, True)
            End If
            Dim releaseNotes As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.RELEASE_NOTES).ToList()
            If (releaseNotes.Count > 0) Then
                CommonGroup1LinkBuilder(releaseNotes, DocumentTypeIds.RELEASE_NOTES, "Release Notes", ulResourcesGroup3)
            End If
            Dim specSheets As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.SPECIFICATIONS).ToList()
            If (specSheets.Count > 0) Then
                CommonGroup2LinkBuilder(specSheets, DocumentTypeIds.SPECIFICATIONS, "Specifications", ulResourcesGroup3, False)
            End If
            Dim tutorials As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.TUTORIALS).ToList()
            If (tutorials.Count > 0) Then
                CommonGroup1LinkBuilder(tutorials, DocumentTypeIds.TUTORIALS, "Tutorials", ulResourcesGroup3)
            End If
            Dim utilitiesCatIds As List(Of Int32) = New List(Of Int32)({CategoryIds.OSCILLOSCOPES, CategoryIds.ARBITRARY_WAVEFORM_GENERATORS, CategoryIds.LOGIC_ANALYZERS, CategoryIds.PERT3_SYSTEMS, CategoryIds.SI_STUDIO, CategoryIds.SPARQ})
            Dim utilities As List(Of DocumentObject) = (From docObjs In documentObjects
                                                        Where docObjs.DocTypeId = DocumentTypeIds.UTILITIES Or docObjs.DocTypeId = DocumentTypeIds.SOFTWARE Or docObjs.DocTypeId = DocumentTypeIds.ARBSTUDIO Or docObjs.DocTypeId = DocumentTypeIds.LOGICSTUDIO Or docObjs.DocTypeId = DocumentTypeIds.PERT3 Or docObjs.DocTypeId = DocumentTypeIds.SISTUDIO Or docObjs.DocTypeId = DocumentTypeIds.SPARQ Or docObjs.DocTypeId = DocumentTypeIds.WAVESTATION _
                                                        AndAlso utilitiesCatIds.Contains(docObjs.CategoryId)).ToList()
            If (utilities.Count > 0) Then
                AddUtilitiesLink(utilities)
            End If
            Dim videos As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.VIDEOS).ToList()
            If (videos.Count > 0) Then
                AddVideosLink(videos)
            End If
            Dim whitePapers As List(Of DocumentObject) = documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.WHITE_PAPERS).ToList()
            If (whitePapers.Count > 0) Then
                CommonGroup2LinkBuilder(whitePapers, DocumentTypeIds.WHITE_PAPERS, "White Papers", ulResourcesGroup3, True)
            End If
        Else
            AddPromotionsLink()
            If (ShowRequestDemo) Then AddRequestDemoLink()
            If (ShowWhereToBuy) Then AddWhereToBuyLink()
        End If

        If (ShowRequestQuote) Then AddRequestQuoteLink()
        If (ShowBuy) Then
            If Not (String.IsNullOrEmpty(ConfigurationManager.AppSettings("BuyButtonAllowedCountryIds"))) Then
                Dim allowedCountryIds As List(Of Int32) = ConfigurationManager.AppSettings("BuyButtonAllowedCountryIds").ToString().Split(",").[Select](Function(x) Int32.Parse(x)).ToList()
                Dim countryId As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
                If (allowedCountryIds.Contains(countryId)) Then
                    AddBuyLink()
                End If
            End If
        End If

        If (documentObjects.Count <= 0 And Not (ShowRequestDemo) And Not (ShowRequestQuote) And Not (ShowBuy) And Not (ShowWhereToBuy)) Then
            pnlProductResources.Visible = False
        End If
    End Sub

    Private Function GetResourceDefaultPageUrl() As String
        If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
            If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        Return String.Format("~/{0}/{1}/{2}/resources", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value))
                    Else
                        Return String.Format("~/{0}/{1}/resources", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value))
                    End If
                Else
                    ' Fall through to old handlers
                End If
            ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                    Return String.Format("~/{0}/{1}/resources", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value))
                Else
                    ' Fall through to old handlers
                End If
            End If
        End If

        If (CategoryId.HasValue) Then
            If (CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                Return String.Format("~/resources/default.aspx?mseries={0}&categoryid={1}", ProductSeriesId.Value, CategoryId.Value)
            End If
        End If

        If (CategoryId.HasValue And ProductGroupId.HasValue) Then
            If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                If (ProductSeriesId.HasValue) Then
                    If (ProductGroupId.HasValue) Then
                        Return String.Format("~/resources/default.aspx?mseries={0}&groupid={1}", ProductSeriesId.Value, ProductGroupId.Value)
                    End If
                    Return String.Format("~/resources/default.aspx?mseries={0}", ProductSeriesId.Value)
                End If
                Return String.Format("~/resources/default.aspx?groupid={0}", ProductGroupId.Value)
            End If
        End If  ' Fall through

        If (ProductId.HasValue) Then
            Return String.Format("~/resources/default.aspx?modelid={0}", ProductId.Value)
        ElseIf (ProductSeriesId.HasValue) Then
            Return String.Format("~/resources/default.aspx?mseries={0}", ProductSeriesId.Value)
        ElseIf (ProductGroupId.HasValue) Then
            Return String.Format("~/resources/default.aspx?groupid={0}", ProductGroupId.Value)
        ElseIf (ProtocolStandardId.HasValue) Then
            Return String.Format("~/resources/default.aspx?standardid={0}", ProtocolStandardId.Value)
        ElseIf (CategoryId.HasValue) Then
            Return String.Format("~/resources/default.aspx?categoryid={0}", CategoryId.Value)
        Else
            Return String.Empty
        End If
    End Function

    Private Sub CommonGroup1LinkBuilder(ByVal documentObjects As List(Of DocumentObject), ByVal docTypeId As Int32, ByVal docType As String, ByVal ul As HtmlGenericControl)
        Dim li As HtmlGenericControl = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
        If (documentObjects.Where(Function(x) x.DocTypeId = docTypeId).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
            If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                        If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">{4}</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(docTypeId), docType)
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">{3}</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(docTypeId), docType)
                        End If
                        ul.Controls.Add(li)
                        Return
                    End If
                ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">{4}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(docTypeId), docType)
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">{3}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(docTypeId), docType)
                        End If
                        ul.Controls.Add(li)
                        Return
                    End If
                End If
            End If

            If (CategoryId.HasValue) Then
                If (CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                    li.InnerHtml = String.Format("~/resources/default.aspx?mseries={0}&categoryid={1}", ProductSeriesId.Value, CategoryId.Value)
                    ul.Controls.Add(li)
                    Return
                End If
            End If

            If (IsOptionsSubPage And CategoryId.HasValue And Not ProductGroupId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">{2}</a>", docTypeId, CategoryId.Value, docType)
            Else
                If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                    If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                        If (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">{2}</a>", docTypeId, ProductSeriesId.Value, docType)
                            ul.Controls.Add(li)
                            Return
                        End If
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">{2}</a>", docTypeId, ProductGroupId.Value, docType)
                        ul.Controls.Add(li)
                        Return
                    End If
                End If  ' Fall through

                If (ProductSeriesId.HasValue) Then
                    If (ProductId.HasValue) Then    ' NOTE: Only probes falls under this branch
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">{2}</a>", docTypeId, ProductId.Value, docType)
                    Else
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">{2}</a>", docTypeId, ProductSeriesId.Value, docType)
                    End If
                ElseIf (ProductGroupId.HasValue) Then
                    If (ProductId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">{2}</a>", docTypeId, ProductId.Value, docType)
                    Else
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">{2}</a>", docTypeId, ProductGroupId.Value, docType)
                    End If
                ElseIf (ProtocolStandardId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">{2}</a>", docTypeId, ProtocolStandardId.Value, docType)
                Else
                    If Not (String.IsNullOrEmpty(documentObjects.FirstOrDefault().SeoUrl)) Then
                        li.InnerHtml = String.Format("<a href=""/doc/{0}#pdf"">{1}</a>", documentObjects.FirstOrDefault().SeoUrl, docType.Substring(0, docType.Length - 1))
                    Else
                        li.InnerHtml = String.Format("<a href=""/doc/docview.aspx?id={0}"">{1}</a>", documentObjects.FirstOrDefault().DocumentId, docType.Substring(0, docType.Length - 1))
                    End If
                End If
            End If
        Else
            If (CategoryId.Value = CategoryIds.PROBES) Then
                If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                    If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">{4}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(docTypeId), docType)
                    Else
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">{3}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(docTypeId), docType)
                    End If
                    ul.Controls.Add(li)
                    Return
                End If
            End If

            If Not (String.IsNullOrEmpty(documentObjects.FirstOrDefault().SeoUrl)) Then
                li.InnerHtml = String.Format("<a href=""/doc/{0}#pdf"">{1}</a>", documentObjects.FirstOrDefault().SeoUrl, docType.Substring(0, docType.Length - 1))
            Else
                li.InnerHtml = String.Format("<a href=""/doc/docview.aspx?id={0}"">{1}</a>", documentObjects.FirstOrDefault().DocumentId, docType.Substring(0, docType.Length - 1))   'String.Format("<a href=""{0}"" onclick=""_gaq.push(['_trackPageview', '{1}']);"">Datasheet</a>", String.Concat(ConfigurationManager.AppSettings("AwsPublicBaseUrl"), temps.FirstOrDefault().FileFullPath.ToLower()), temps.FirstOrDefault().FileFullPath.ToLower())
            End If
        End If
        ul.Controls.Add(li)
    End Sub

    Private Sub CommonGroup2LinkBuilder(ByVal documentObjects As List(Of DocumentObject), ByVal docTypeId As Int32, ByVal docType As String, ByVal ul As HtmlGenericControl, ByVal truncateType As Boolean)
        Dim li As HtmlGenericControl = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
        If (documentObjects.Where(Function(x) x.DocTypeId = docTypeId).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
            If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                        If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">{4}</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(docTypeId), docType)
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">{3}</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(docTypeId), docType)
                        End If
                        ul.Controls.Add(li)
                        Return
                    End If
                ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">{4}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(docTypeId), docType)
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">{3}</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(docTypeId), docType)
                        End If
                        ul.Controls.Add(li)
                        Return
                    End If
                End If
            End If

            If (CategoryId.HasValue) Then
                If (CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                    li.InnerHtml = String.Format("~/resources/default.aspx?mseries={0}&categoryid={1}", ProductSeriesId.Value, CategoryId.Value)
                    ul.Controls.Add(li)
                    Return
                End If
            End If

            If (IsOptionsSubPage And CategoryId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">{2}</a>", docTypeId, CategoryId.Value, docType)
            Else
                If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                    If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                        If (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">{2}</a>", docTypeId, ProductSeriesId.Value, docType)
                            ul.Controls.Add(li)
                            Return
                        End If
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">{2}</a>", docTypeId, ProductGroupId.Value, docType)
                        ul.Controls.Add(li)
                        Return
                    End If
                End If  ' Fall through

                If (ProductSeriesId.HasValue) Then
                    If (docTypeId = DocumentTypeIds.FIRMWARE) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Firmware</a>", DocumentTypeIds.FIRMWARE, ProductSeriesId.Value)
                    ElseIf (docTypeId = 25) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Firmware Upgrades</a>", 25, ProductSeriesId.Value)
                    Else
                        If (ProductId.HasValue) Then    ' NOTE: Only probes falls under this branch
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">{2}</a>", docTypeId, ProductId.Value, docType)
                        Else
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">{2}</a>", docTypeId, ProductSeriesId.Value, docType)
                        End If
                    End If
                ElseIf (docTypeId = DocumentTypeIds.PROGRAMMING_EXAMPLES) Then
                    li.InnerHtml = "<a href=""/support/techlib/programmingexamples.aspx?type=8"">Programming Examples</a>"
                ElseIf (docTypeId = DocumentTypeIds.FIRMWARE) Then
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Firmware</a>", documentObjects.FirstOrDefault().DocumentId)
                ElseIf (docTypeId = 25) Then
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Firmware Upgrades</a>", documentObjects.FirstOrDefault().DocumentId)
                ElseIf (ProductGroupId.HasValue And docTypeId = DocumentTypeIds.SPECIFICATIONS) Then  ' Original spec branch had this, for the sake of common code, added this branch to unite the methods
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">Specifications</a>", DocumentTypeIds.SPECIFICATIONS, ProductGroupId.Value)
                ElseIf (ProtocolStandardId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">{2}</a>", docTypeId, ProtocolStandardId.Value, docType)
                ElseIf (CategoryId.HasValue And docTypeId <> DocumentTypeIds.SPECIFICATIONS) Then  ' Original spec branch did not have this, for the sake of common code, limited this branch to exclude specs to unite the methods
                    li.InnerHtml = String.Format("<a href=""/support/techlib/library.aspx?type=1&cat={0}"">{1}</a>", CategoryId.Value, IIf(truncateType, docType.Substring(0, docType.Length - 1), docType))
                Else
                    If (docTypeId = DocumentTypeIds.SPECIFICATIONS) Then  ' Original spec branch had this, for the sake of common code, added this branch to unite the methods
                        If (ProductId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">Specification</a>", DocumentTypeIds.SPECIFICATIONS, ProductId.Value)
                        Else
                            If Not (String.IsNullOrEmpty(documentObjects.FirstOrDefault().SeoUrl)) Then
                                li.InnerHtml = String.Format("<a href=""/doc/{0}#pdf"">Specification</a>", documentObjects.FirstOrDefault().SeoUrl)
                            Else
                                li.InnerHtml = String.Format("<a href=""/doc/docview.aspx?id={0}"">Specification</a>", documentObjects.FirstOrDefault().DocumentId)
                            End If
                        End If
                    End If
                End If
            End If
        Else
            If (docTypeId = DocumentTypeIds.PROGRAMMING_EXAMPLES) Then
                li.InnerHtml = "<a href=""/support/techlib/programmingexamples.aspx?type=8"">Programming Examples</a>"
            ElseIf (docTypeId = DocumentTypeIds.FIRMWARE) Then
                If (ProductSeriesId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?mseries={0}&did={1}"">Firmware</a>", ProductSeriesId.Value, documentObjects.FirstOrDefault().DocumentId)
                Else
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Firmware</a>", documentObjects.FirstOrDefault().DocumentId)
                End If
            ElseIf (docTypeId = 25) Then
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Firmware Upgrades</a>", documentObjects.FirstOrDefault().DocumentId)
            Else
                If Not (String.IsNullOrEmpty(documentObjects.FirstOrDefault().SeoUrl)) Then
                    li.InnerHtml = String.Format("<a href=""/doc/{0}"">{1}</a>", documentObjects.FirstOrDefault().SeoUrl, IIf(truncateType, docType.Substring(0, docType.Length - 1), docType))
                Else
                    li.InnerHtml = String.Format("<a href=""/doc/docview.aspx?id={0}"">{1}</a>", documentObjects.FirstOrDefault().DocumentId, IIf(truncateType, docType.Substring(0, docType.Length - 1), docType))
                End If
            End If
        End If
        ul.Controls.Add(li)
    End Sub

    Private Sub AddVideosLink(ByVal documentObjects As List(Of DocumentObject))
        Dim li As HtmlGenericControl = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
        If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
            If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Videos &amp; Demos</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.VIDEOS))
                    Else
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Videos &amp; Demos</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(DocumentTypeIds.VIDEOS))
                    End If
                    ulResourcesGroup3.Controls.Add(li)
                    Return
                End If
            ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                    If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Videos &amp; Demos</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.VIDEOS))
                    Else
                        li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Videos &amp; Demos</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.VIDEOS))
                    End If
                    ulResourcesGroup3.Controls.Add(li)
                    Return
                End If
            End If
        End If

        If (ProductSeriesId.HasValue) Then
            If (ProductId.HasValue) Then    ' NOTE: Only probes falls under this branch
                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">Videos &amp; Demos</a>", DocumentTypeIds.VIDEOS, ProductId.Value)
            Else
                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Videos &amp; Demos</a>", DocumentTypeIds.VIDEOS, ProductSeriesId.Value)
            End If
        ElseIf (ProtocolStandardId.HasValue) Then
            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Videos &amp; Demos</a>", DocumentTypeIds.VIDEOS, ProtocolStandardId.Value)
        Else
            li.InnerHtml = "<a href=""/support/techlib/videos.aspx"">Videos &amp; Demos</a>"
        End If
        ulResourcesGroup3.Controls.Add(li)
    End Sub

    Private Sub AddAnalysisSoftwareLink(ByVal documentObjects As List(Of DocumentObject))
        Dim li As HtmlGenericControl = New HtmlGenericControl()
        If (documentObjects.FirstOrDefault().CategoryId = CategoryIds.SOFTWARE_OPTIONS) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.SOFTWARE).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, CategoryId.Value)
                Else
                    If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                        If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                            If (ProductSeriesId.HasValue) Then
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                                ulResourcesGroup3.Controls.Add(li)
                                Return
                            End If
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductGroupId.Value)
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    End If  ' Fall through

                    If (ProtocolStandardId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProtocolStandardId.Value)
                        ulResourcesGroup3.Controls.Add(li)
                    Else
                        If (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                            ulResourcesGroup3.Controls.Add(li)
                        End If
                    End If
                End If
            Else
                If (String.Compare(documentObjects.FirstOrDefault().PasswordYn, "Y", True) = 0) Then
                    If (IsOptionsSubPage And CategoryId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, CategoryId.Value)
                    Else
                        If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                            If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                                If (ProductSeriesId.HasValue) Then
                                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                                    ulResourcesGroup3.Controls.Add(li)
                                    Return
                                End If
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductGroupId.Value)
                                ulResourcesGroup3.Controls.Add(li)
                                Return
                            End If
                        End If  ' Fall through

                        If (ProtocolStandardId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProtocolStandardId.Value)
                        ElseIf (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                        Else
                            li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
                        End If
                    End If
                Else
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
                End If
                ulResourcesGroup3.Controls.Add(li)
            End If
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.HARDWARE_OPTIONS) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (String.Compare(documentObjects.FirstOrDefault().PasswordYn, "Y", True) = 0) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, CategoryId.Value)
                Else
                    If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                        If (CategoryId.Value = CategoryIds.HARDWARE_OPTIONS) Then
                            If (ProductSeriesId.HasValue) Then
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                                ulResourcesGroup3.Controls.Add(li)
                                Return
                            End If
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductGroupId.Value)
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    End If  ' Fall through

                    If (ProtocolStandardId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProtocolStandardId.Value)
                    ElseIf (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        Else
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.SOFTWARE).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                    If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                        If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                            If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            Else
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            End If
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                        If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                            If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            Else
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            End If
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    End If
                End If

                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, CategoryId.Value)
                Else
                    If (ProtocolStandardId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProtocolStandardId.Value)
                        ulResourcesGroup3.Controls.Add(li)
                    Else
                        If (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                            ulResourcesGroup3.Controls.Add(li)
                        End If
                    End If
                End If
            Else
                If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                    If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                        If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                            If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            Else
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            End If
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                        If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                            If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            Else
                                li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Analysis Software Download</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.SOFTWARE))
                            End If
                            ulResourcesGroup3.Controls.Add(li)
                            Return
                        End If
                    End If
                End If

                If (String.Compare(documentObjects.FirstOrDefault().PasswordYn, "Y", True) = 0) Then
                    If (IsOptionsSubPage And CategoryId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, CategoryId.Value)
                    Else
                        If (ProtocolStandardId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProtocolStandardId.Value)
                        ElseIf (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Analysis Software Download</a>", DocumentTypeIds.SOFTWARE, ProductSeriesId.Value)
                        Else
                            li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
                        End If
                    End If
                Else
                    li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Analysis Software Download</a>", documentObjects.FirstOrDefault().DocumentId)
                End If
                ulResourcesGroup3.Controls.Add(li)
            End If
        End If
    End Sub

    Private Sub AddUtilitiesLink(ByVal documentObjects As List(Of DocumentObject))
        Dim li As HtmlGenericControl = New HtmlGenericControl()
        If (documentObjects.FirstOrDefault().CategoryId = CategoryIds.OSCILLOSCOPES) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)

            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.UTILITIES).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Software &amp; Utilities</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.UTILITIES))
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Software &amp; Utilities</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.UTILITIES))
                        End If
                        ulResourcesGroup3.Controls.Add(li)
                        Return
                    End If
                End If

                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.UTILITIES, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.UTILITIES, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.ARBSTUDIO).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.ARBSTUDIO, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.ARBSTUDIO, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            ElseIf (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.WAVESTATION).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.WAVESTATION, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.WAVESTATION, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.LOGIC_ANALYZERS) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.LOGICSTUDIO).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.LOGICSTUDIO, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.LOGICSTUDIO, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.PERT3_SYSTEMS) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.PERT3).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.PERT3, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.PERT3, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.SI_STUDIO) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.SPARQ).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.SPARQ, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.SPARQ, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.SPARQ) Then
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.SPARQ).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.SPARQ, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.SPARQ, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        ElseIf (documentObjects.FirstOrDefault().CategoryId = CategoryIds.PROTOCOL_ANALYZERS) Then
            Return
        Else
            li = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
            If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.UTILITIES).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
                If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Software &amp; Utilities</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.UTILITIES))
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Software &amp; Utilities</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.UTILITIES))
                        End If
                        ulResourcesGroup3.Controls.Add(li)
                        Return
                    End If
                End If

                If (IsOptionsSubPage And CategoryId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Software &amp; Utilities</a>", DocumentTypeIds.UTILITIES, CategoryId.Value)
                Else
                    If (ProductSeriesId.HasValue) Then
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Software &amp; Utilities</a>", DocumentTypeIds.UTILITIES, ProductSeriesId.Value)
                    Else
                        li.InnerHtml = "<a href=""/support/softwaredownload/default.aspx?group=1"">Software &amp; Utilities</a>"
                    End If
                End If
            Else
                ' TODO: Should anything be here?
                li.InnerHtml = String.Format("<a href=""/support/softwaredownload/download.aspx?did={0}"">Software &amp; Utilities</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        End If
    End Sub

    Private Sub AddManualsLink(ByVal documentObjects As List(Of DocumentObject))
        Dim li As HtmlGenericControl = GetBaseListItem(documentObjects.FirstOrDefault().ImagePathTemplate)
        If (documentObjects.Where(Function(x) x.DocTypeId = DocumentTypeIds.PRODUCT_MANUALS).Select(Function(x) x.DocumentId).Distinct().ToList().Count > 1) Then
            If (CategoryId.HasValue AndAlso _categorySeos.ContainsKey(CategoryId.Value)) Then
                If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (ProtocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(ProtocolStandardId.Value)) Then
                        If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Manuals</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.PRODUCT_MANUALS))
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Manuals</a>", _categorySeos(CategoryId.Value), _protocolSeos(ProtocolStandardId.Value), _docTypeIds(DocumentTypeIds.PRODUCT_MANUALS))
                        End If
                        ulResourcesGroup3.Controls.Add(li)
                        Return
                    End If
                ElseIf (CategoryId.Value = CategoryIds.OSCILLOSCOPES Or CategoryId.Value = CategoryIds.PROBES Or CategoryId.Value = 35 Or CategoryId.Value = 36) Then
                    If (ProductSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(ProductSeriesId.Value)) Then
                        If (ProductId.HasValue AndAlso _productSeos.ContainsKey(ProductId.Value)) Then
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/{2}/resources/{3}"">Manuals</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _productSeos(ProductId.Value), _docTypeIds(DocumentTypeIds.PRODUCT_MANUALS))
                        Else
                            li.InnerHtml = String.Format("<a href=""/{0}/{1}/resources/{2}"">Manuals</a>", _categorySeos(CategoryId.Value), _seriesSeos(ProductSeriesId.Value), _docTypeIds(DocumentTypeIds.PRODUCT_MANUALS))
                        End If
                        ulResourcesGroup3.Controls.Add(li)
                        Return
                    End If
                End If
            End If

            If (CategoryId.HasValue) Then
                If (IsOptionsSubPage) Then
                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&categoryid={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, CategoryId.Value)
                Else
                    If (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                        If (ProtocolStandardId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&standardid={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProtocolStandardId.Value)
                        ElseIf (ProductSeriesId.HasValue) Then
                            li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductSeriesId.Value)
                        Else
                            li.InnerHtml = String.Format("<a href=""/support/techlib/productmanuals.aspx?type=2&cat={0}"">Manuals</a>", CategoryIds.PROTOCOL_ANALYZERS)
                        End If
                    Else
                        If (CategoryId.HasValue And ProductGroupId.HasValue) Then
                            If (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                                If (ProductSeriesId.HasValue) Then
                                    li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductSeriesId.Value)
                                    ulResourcesGroup3.Controls.Add(li)
                                    Return
                                End If
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&groupid={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductGroupId.Value)
                                ulResourcesGroup3.Controls.Add(li)
                                Return
                            End If
                        End If  ' Fall through

                        If (ProductSeriesId.HasValue) Then
                            If (ProductId.HasValue) Then    ' NOTE: Only probes falls under this branch
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductId.Value)
                            Else
                                li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductSeriesId.Value)
                            End If
                        Else
                            li.InnerHtml = String.Format("<a href=""/support/techlib/productmanuals.aspx?type=2&cat={0}"">Manuals</a>", CategoryId.Value)
                        End If
                    End If
                End If
                ulResourcesGroup3.Controls.Add(li)
            Else
                If (ProductSeriesId.HasValue) Then
                    If (ProductId.HasValue) Then    ' NOTE: Only probes falls under this branch
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&modelid={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductId.Value)
                    Else
                        li.InnerHtml = String.Format("<a href=""/resources/details.aspx?doctypeid={0}&mseries={1}"">Manuals</a>", DocumentTypeIds.PRODUCT_MANUALS, ProductSeriesId.Value)
                    End If
                    ulResourcesGroup3.Controls.Add(li)
                End If
            End If
        Else
            If Not (String.IsNullOrEmpty(documentObjects.FirstOrDefault().SeoUrl)) Then
                li.InnerHtml = String.Format("<a href=""/doc/{0}#pdf"">Manuals</a>", documentObjects.FirstOrDefault().SeoUrl)
            Else
                li.InnerHtml = String.Format("<a href=""/doc/docview.aspx?id={0}"">Manuals</a>", documentObjects.FirstOrDefault().DocumentId)
            End If
            ulResourcesGroup3.Controls.Add(li)
        End If
    End Sub

    Private Sub AddBuyLink()
        Dim li As HtmlGenericControl = GetBaseListItem(DEFAULT_LINKICON_IMAGE_TEMPLATE)
        If (ProductId.HasValue) Then
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductId.Value)
            If (product Is Nothing) Then Return
            If (String.IsNullOrEmpty(product.ShopifyHandle)) Then Return
            'If (String.Compare(product.EStoreYN, "N", True) = 0) Then Return

            Dim categoryIds As List(Of Int32) = ProductSeriesCategoryRepository.GetCategoriesAssignedToProductId(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductId.Value).ToList()
            li.InnerHtml = String.Format("<a href=""{0}{1}"">Buy Now</a>", STORE_URL_BASE, product.ShopifyHandle)
            ulResourcesGroup2.Controls.Add(li)
        ElseIf (ProductSeriesId.HasValue) Then
            If (ProductGroupId.HasValue) Then
                Dim pg As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductGroupId.Value)
                If (pg Is Nothing) Then Return
                If Not (pg.CategoryId.HasValue) Then Return
                If (pg.CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                    'li.InnerHtml = String.Format("<a href=""/shopper/storeinterstitial.aspx?seriesid={0}&groupid={1}"">Buy Now</a>", ProductSeriesId.Value, ProductGroupId.Value)
                    'ulResourcesGroup2.Controls.Add(li)
                    'Else
                    'Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductId.Value)
                    'If (product Is Nothing) Then Return
                    'If (String.IsNullOrEmpty(product.ShopifyHandle)) Then Return
                    'If (String.Compare(product.EStoreYN, "N", True) = 0) Then Return

                    'li.InnerHtml = String.Format("<a href=""{0}{1}"">Buy Now</a>", STORE_URL_BASE, product.ShopifyHandle)
                    'ulResourcesGroup2.Controls.Add(li)
                End If
            Else
                If (ProductSeriesId.Value = 280 Or ProductSeriesId.Value = 414 Or ProductSeriesId = 491) Then
                    li.InnerHtml = String.Format("<a href=""/shopper/buynow.aspx?mseries={0}"">Buy Now</a>", ProductSeriesId.Value)
                    ulResourcesGroup2.Controls.Add(li)
                Else
                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductSeriesId.Value)
                    If (productSeries Is Nothing) Then Return
                    If (String.IsNullOrEmpty(productSeries.ShopifyCollectionId)) Then Return

                    Dim categoryId As Int32 = ProductSeriesCategoryRepository.GetCategoryAssociatedToProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductSeriesId.Value)
                    If (categoryId > 0) Then
                        li.InnerHtml = String.Format("<a href=""{0}{1}"">Buy Now</a>", STORE_URL_BASE, productSeries.ShopifyCollectionId)
                        ulResourcesGroup2.Controls.Add(li)
                    End If
                End If
            End If
        ElseIf (ProductGroupId.HasValue) Then
            'If (ProductGroupId.HasValue) Then
            '        If Not (String.IsNullOrEmpty(Session("contactid"))) Then
            '            li.InnerHtml = String.Format("<a href=""https: //store.teledynelecroy.com/cart_step2.aspx?seriesid={0}"">Buy Now</a>", ProductGroupId.Value)
            '        Else
            '            li.InnerHtml = String.Format("<a href=""https://store.teledynelecroy.com/cart_step1.aspx?catid={0}"">Buy Now</a>", CategoryId.Value)
            '        End If
            '    Else
            '        li.InnerHtml = String.Format("<a href=""https://store.teledynelecroy.com/cart_step1.aspx?catid={0}"">Buy Now</a>", CategoryId.Value)
            '    End If
        ElseIf (CategoryId.HasValue) Then
            li.InnerHtml = String.Format("<a href=""{0}"">Buy Now</a>", STORE_URL_BASE)
            ulResourcesGroup2.Controls.Add(li)
        End If
    End Sub

    Private Sub AddWhereToBuyLink()
        If Not (ProductSeriesId.HasValue) Then
            Return
        End If
        If Not (FeContactManager.IsProductSeriesIdInSuppliedString(ProductSeriesId.Value, ConfigurationManager.AppSettings("AllowWhereToBuyForTheseSeries"))) Then
            Return
        End If
        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        Dim contactOffices As List(Of ContactOffice) = FeContactManager.GetContactOfficesForProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProductSeriesId.Value, "D")
        contactOffices = contactOffices.Where(Function(x) x.CountryIdWeb.Value = countryCode).ToList()
        If (contactOffices.Count > 0) Then
            Dim li As HtmlGenericControl = GetBaseListItem(DEFAULT_WHERETOBUY_IMAGE_TEMPLATE)
            li.InnerHtml = String.Format("<a href=""/support/contact/wheretobuy.aspx?mseries={0}"">Where To Buy</a>", ProductSeriesId.Value)
            ulResourcesGroup2.Controls.Add(li)
        End If
    End Sub

    Private Sub AddRequestDemoLink()
        If (CategoryId.HasValue) Then
            Dim li As HtmlGenericControl = GetBaseListItem(DEFAULT_LINKICON_IMAGE_TEMPLATE)
            If (CategoryId.Value = CategoryIds.OSCILLOSCOPES) Then
                If (ProductSeriesId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/oscilloscope/demo.aspx?mseries={0}"">Request Demo</a>", ProductSeriesId.Value)
                    ulResourcesGroup2.Controls.Add(li)
                Else
                    'TODO: ? Response.Redirect("~/oscilloscope/")
                End If
            ElseIf (CategoryId.Value = CategoryIds.SPARQ) Then
                li.InnerHtml = "<a href=""/support/requestinfo/default.aspx"">Request Demo</a>"
                ulResourcesGroup2.Controls.Add(li)
            End If
        End If
    End Sub

    Private Sub AddRequestQuoteLink()
        If Not (CategoryId.HasValue) Then
            Return
        End If
        Dim li As HtmlGenericControl = GetBaseListItem(DEFAULT_LINKICON_IMAGE_TEMPLATE)
        If (CategoryId.Value = CategoryIds.OSCILLOSCOPES) Then
            If (ProductGroupId.HasValue And ProductSeriesId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/select_scope.aspx?seriesid={0}&mseries={1}"">Request Quote</a>", ProductGroupId.Value, ProductSeriesId.Value)
            Else
                li.InnerHtml = "<a href=""/oscilloscope/configure/configure_step2.aspx?from=overview"">Request Quote</a>"
            End If
        ElseIf (CategoryId.Value = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
            If (ProductGroupId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductGroupId.Value)
            ElseIf (ProductSeriesId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductSeriesId.Value)
            Else
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)
            End If
        ElseIf (CategoryId.Value = CategoryIds.PROBES) Then
            If (ProductGroupId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductGroupId.Value)
                'li.InnerHtml = String.Format("<a href=""/shopper/requestquote/select_scope.aspx?seriesid={0}"">Request Quote</a>", ProductGroupId.Value)
            ElseIf (ProductSeriesId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductSeriesId.Value)
            Else
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)
            End If
        ElseIf (CategoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
            If (ProductGroupId.HasValue) Then
                If (ProductSeriesId.HasValue) Then
                    li.InnerHtml = String.Format("<a href=""/shopper/requestquote/select_scope.aspx?seriesid={0}&mseries={1}"">Request Quote</a>", ProductGroupId.Value, ProductSeriesId.Value)
                Else
                    li.InnerHtml = String.Format("<a href=""/shopper/requestquote/select_scope.aspx?seriesid={0}"">Request Quote</a>", ProductGroupId.Value)
                End If
            Else
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)  'Production was pointed here
                'li.InnerHtml = "<a href=""/shopper/requestquote/"">Request Quote</a>"
            End If
        ElseIf (CategoryId.Value = CategoryIds.HARDWARE_OPTIONS Or CategoryId.Value = CategoryIds.MIXED_SIGNAL_TEST_SOLUTIONS Or CategoryId.Value = CategoryIds.ACCESSORIES) Then
            If (ProductGroupId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductGroupId.Value)
            Else
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)
            End If
        ElseIf (CategoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
            If (ProductSeriesId.HasValue) Then
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step2.aspx?seriesid={0}"">Request Quote</a>", ProductSeriesId.Value)
            Else
                li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)
            End If
        Else
            li.InnerHtml = String.Format("<a href=""/shopper/requestquote/configure_small_step1.aspx?catid={0}"">Request Quote</a>", CategoryId.Value)
        End If
        ulResourcesGroup2.Controls.Add(li)
    End Sub

    Private Sub AddPromotionsLink()
        Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
        If (countryCode < 0) Then Return
        Dim promotionCountries As List(Of PromotionCountry) = PromotionCountryRepository.GetAllPromotionsForCountryId(ConfigurationManager.AppSettings("ConnectionString").ToString(), countryCode.ToString())
        If (promotionCountries.Count <= 0) Then Return
        Dim promotions As List(Of Promotion) = PromotionRepository.GetSelectedPromotions(ConfigurationManager.AppSettings("ConnectionString").ToString(), promotionCountries.Select(Function(x) x.PromoId).Distinct().ToList())
        If (promotions.Count <= 0) Then Return
        promotions = (From p In promotions
                      Where ((p.PromoStartDate <= DateTime.Now() And p.PromoEndDate >= DateTime.Now()) Or
                             (p.PromoStartDate Is Nothing And p.PromoEndDate Is Nothing) Or
                             (p.PromoStartDate Is Nothing And p.PromoEndDate >= DateTime.Now()) Or
                             (p.PromoStartDate <= DateTime.Now() And p.PromoEndDate Is Nothing)) And p.PostYN.ToUpper() = "Y"
                      Select p).ToList()
        If (promotions.Count <= 0) Then Return
        Dim promotionCategories As List(Of PromotionCategory) = PromotionCategoryRepository.GetSelectedPromotionCategoriesForPromotionIds(ConfigurationManager.AppSettings("ConnectionString").ToString(), promotions.Select(Function(x) x.PromoId).Distinct().ToList())
        If (promotionCategories.Count <= 0) Then Return
        Dim newPcCollection As List(Of PromotionCategory) = New List(Of PromotionCategory)
        If (ProductSeriesId.HasValue) Then
            If (ProductId.HasValue) Then
                For Each pc As PromotionCategory In promotionCategories
                    If (pc.ProductSeriesId.HasValue And pc.ProductId.HasValue) Then
                        If (pc.ProductSeriesId.Value = ProductSeriesId.Value And pc.ProductId.Value = ProductId.Value) Then newPcCollection.Add(pc)
                    End If
                Next
            Else
                For Each pc As PromotionCategory In promotionCategories
                    If (pc.ProductSeriesId.HasValue) Then
                        If (pc.ProductSeriesId.Value = ProductSeriesId.Value) Then newPcCollection.Add(pc)
                    End If
                Next
            End If
        ElseIf (ProductId.HasValue) Then
            For Each pc As PromotionCategory In promotionCategories
                If (pc.ProductId.HasValue) Then
                    If (pc.ProductId.Value = ProductId.Value) Then newPcCollection.Add(pc)
                End If
            Next
        End If

        If (newPcCollection.Count <= 0) Then Return
        Dim li As HtmlGenericControl = GetBaseListItem(DEFAULT_LINKICON_IMAGE_TEMPLATE)
        If (newPcCollection.Count = 1) Then
            li.InnerHtml = String.Format("<a href=""/promotions/promotionoverview.aspx?promoid={0}"">Promotions</a>", newPcCollection.FirstOrDefault().PromoId)
        Else
            li.InnerHtml = "<a href=""https://teledynelecroy.com/promotions/"">Promotions</a>"
        End If
        ulResourcesGroup2.Controls.Add(li)
    End Sub

    Private Function GetBaseListItem(ByVal backgroundImage As String) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        If Not (String.IsNullOrEmpty(backgroundImage)) Then
            li.Style.Add("background-image", String.Format("url('{0}')", String.Format(backgroundImage, "16", "16")))
            li.Style.Add("background-position", "0 5px")
            li.Style.Add("background-repeat", "no-repeat")
            li.Style.Add("margin", "0")
            li.Style.Add("padding", "5px 0 9px 28px")
        End If
        Return li
    End Function
#End Region

    ' TODO: Move me outta here
    Private Function GetData() As IList(Of DocumentObject)
        If (CategoryId.HasValue) Then
            If (IsOptionsSubPage) Then
                If (ProductGroupId.HasValue) Then
                    Return UnifiedQuery(QueryType.Group)
                Else
                    Return UnifiedQuery(QueryType.Category)
                End If
            Else
                If (CategoryId.Value = 19) Then
                    If (ProductId.HasValue) Then
                        Return UnifiedQuery(QueryType.Product)
                    ElseIf (ProtocolStandardId.HasValue) Then
                        Return UnifiedQuery(QueryType.Standard)
                    ElseIf (ProductSeriesId.HasValue) Then
                        Return UnifiedQuery(QueryType.Series)
                    ElseIf (ProductGroupId.HasValue) Then
                        Return UnifiedQuery(QueryType.Group)
                    Else
                        Return UnifiedQuery(QueryType.Category)
                    End If
                ElseIf (CategoryId.Value = 12 And ProductGroupId.HasValue) Then
                    Return HackQuery()
                Else
                    If (ProductId.HasValue) Then
                        Return UnifiedQuery(QueryType.Product)
                    ElseIf (ProductSeriesId.HasValue) Then
                        Return UnifiedQuery(QueryType.Series)
                    ElseIf (ProductGroupId.HasValue) Then
                        Return UnifiedQuery(QueryType.Group)
                    Else
                        Return UnifiedQuery(QueryType.Category)
                    End If
                End If
            End If
        Else
            If (ProductId.HasValue) Then
                Return UnifiedQuery(QueryType.Product)
            ElseIf (ProductSeriesId.HasValue) Then
                Return UnifiedQuery(QueryType.Series)
            Else
                Response.Redirect("/support/techlib")
                Return Nothing
            End If
        End If
    End Function

    Private Function UnifiedQuery(ByVal queryType As QueryType) As IList(Of DocumentObject)
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        Select Case CType(queryType, Int32)
            Case CType(UserControls_Resources.QueryType.Category, Int32)
                sqlString = "select distinct dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl from DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt where dc.DOCUMENT_ID = d.DOCUMENT_ID And d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND d.POST = 'Y' and dc.CATEGORY_ID = @CATEGORYID"
                sqlParameters.Add(New SqlParameter("@CATEGORYID", CategoryId.Value.ToString()))
            Case CType(UserControls_Resources.QueryType.Series, Int32)
                sqlString = "select distinct dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl from DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt where dc.DOCUMENT_ID = d.DOCUMENT_ID And d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND d.POST = 'Y' and dc.PRODUCT_SERIES_ID = @PRODUCTSERIESID"
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", ProductSeriesId.Value.ToString()))
            Case CType(UserControls_Resources.QueryType.Group, Int32)
                sqlString = "select distinct dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl from DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt where dc.DOCUMENT_ID = d.DOCUMENT_ID And d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND d.POST = 'Y' and dc.GROUP_ID = @PRODUCTGROUPID"
                sqlParameters.Add(New SqlParameter("@PRODUCTGROUPID", ProductGroupId.Value.ToString()))
            Case CType(UserControls_Resources.QueryType.Standard, Int32)
                sqlString = "select distinct dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl from DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt where dc.DOCUMENT_ID = d.DOCUMENT_ID And d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND d.POST = 'Y' and dc.PROTOCOL_STANDARD_ID = @PROTOCOLSTANDARDID"
                sqlParameters.Add(New SqlParameter("@PROTOCOLSTANDARDID", ProtocolStandardId.Value.ToString()))
            Case CType(UserControls_Resources.QueryType.Product, Int32)
                sqlString = "select distinct dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl from DOCUMENT_CATEGORY dc, DOCUMENT d, DOCUMENT_TYPE dt where dc.DOCUMENT_ID = d.DOCUMENT_ID And d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND d.POST = 'Y' and dc.PRODUCT_ID = @PRODUCTID"
                sqlParameters.Add(New SqlParameter("@PRODUCTID", ProductId.Value.ToString()))
        End Select
        Return FetchData(sqlString, sqlParameters.ToArray()).ToList()
    End Function

    Private Function HackQuery() As IList(Of DocumentObject)
        Dim sqlString As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If (ProductSeriesId.HasValue) Then
            sqlString = "SELECT DISTINCT dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl FROM DOCUMENT_CATEGORY dc, DOCUMENT d , DOCUMENT_TYPE dt WHERE dc.DOCUMENT_ID = d.DOCUMENT_ID AND d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND dc.CATEGORY_ID = @CATEGORYID AND d.POST = 'Y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx WHERE psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPFKID AND psc.PRODUCT_SERIES_ID = @PRODUCTSERIESID)"
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", ProductSeriesId.Value.ToString()))
        Else
            sqlString = "SELECT DISTINCT dc.document_id, dc.CATEGORY_ID, d.FILE_FULL_PATH, dt.DOC_TYPE_ID, dt.ImagePathTemplate, d.PASSWORD_YN, d.SeoUrl FROM DOCUMENT_CATEGORY dc, DOCUMENT d , DOCUMENT_TYPE dt WHERE dc.DOCUMENT_ID = d.DOCUMENT_ID AND d.DOC_TYPE_ID = dt.DOC_TYPE_ID AND dc.CATEGORY_ID = @CATEGORYID AND d.POST = 'Y' AND dc.PRODUCT_SERIES_ID IN (SELECT psc.PRODUCT_SERIES_ID FROM PRODUCT_SERIES_CATEGORY psc, PRODUCT p, PRODUCT_GROUP_XREF pgx WHERE psc.CATEGORY_ID = @CATEGORYID AND psc.PRODUCT_ID = p.PRODUCTID AND p.PRODUCTID = pgx.ProductFkId AND pgx.ProductGroupFkId = @PRODUCTGROUPFKID)"
        End If
        sqlParameters.Add(New SqlParameter("@CATEGORYID", "12"))
        sqlParameters.Add(New SqlParameter("@PRODUCTGROUPFKID", ProductGroupId.Value.ToString()))
        Return FetchData(sqlString, sqlParameters.ToArray())
    End Function

    Private Function FetchData(ByVal sqlString As String, ByVal sqlParameters As SqlParameter()) As IList(Of DocumentObject)
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters)
        Dim retVal As List(Of DocumentObject) = New List(Of DocumentObject)
        If (ds.Tables.Count > 0) Then
            For Each dr As DataRow In ds.Tables(0).Rows
                retVal.Add(ConvertDataRowToDocumentObject(dr))
            Next
        End If
        Return retVal
    End Function

    Private Function ConvertDataRowToDocumentObject(ByVal dr As DataRow) As DocumentObject
        Dim retVal As DocumentObject = New DocumentObject
        retVal.DocumentId = Int32.Parse(dr("DOCUMENT_ID").ToString())
        If Not (dr("CATEGORY_ID") Is DBNull.Value) Then retVal.CategoryId = Int32.Parse(dr("CATEGORY_ID").ToString())
        retVal.FileFullPath = dr("FILE_FULL_PATH").ToString()
        If Not (dr("DOC_TYPE_ID") Is DBNull.Value) Then retVal.DocTypeId = Int32.Parse(dr("DOC_TYPE_ID").ToString())
        retVal.ImagePathTemplate = dr("IMAGEPATHTEMPLATE").ToString()
        retVal.PasswordYn = dr("PASSWORD_YN").ToString()
        If Not (dr("SeoUrl") Is DBNull.Value) Then retVal.SeoUrl = dr("SeoUrl").ToString()
        Return retVal
    End Function

    Public Enum QueryType As Int32
        Unset = 0
        Category = 1
        Series = 2
        Group = 3
        Standard = 4
        Product = 5
    End Enum
End Class

Public Class DocumentObject
    Public DocumentId As Int32
    Public CategoryId As Int32
    Public FileFullPath As String
    Public DocTypeId As Int32
    Public ImagePathTemplate As String
    Public PasswordYn As String
    Public SeoUrl As String
End Class