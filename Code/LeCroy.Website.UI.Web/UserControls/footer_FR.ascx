﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="footer_FR.ascx.vb" Inherits="LeCroy.Website.footer_FR" %>
				<div class="container">
					<div class="inner-footer-content acc-footer">
						<div class="item-footer">
							<h6 class="title opener">Solutions</h6>
							<ul class="slide">
								<li><a href="/scopes/">Oscilloscopes</a></li>
								<li><a href="/protocolanalyzer/">Protocol Solutions</a></li>
								<li><a href="https://www.spdevices.com/products/hardware">Modular Data Acquisition</a></li>
								<li><a href="http://teledyne-ts.com/overview.html">Sensors and Calibrators</a></li>
								<li><a href="/testtools/">Test Equipment and Tools</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">Buy</h6>
							<ul class="slide">
								<li><a href="/shopper/requestquote/">Request a Quote</a></li>
								<li><a href="/support/contact/">Contact our Sales Experts</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">Support</h6>
							<ul class="slide">
								<li><a href="/support/techlib/">Document Library</a></li>
								<li><a href="/support/techlib/videos.aspx">Video Library</a></li>
								<li><a href="/support/softwaredownload/">Téléchargement de logiciel</a></li>
								<li><a href="/support/softwaredownload/firmwareupgrade.aspx">Firmware oscilloscope</a></li>
								<li><a href="/support/softwaredownload/psg.aspx">Logiciel analyse de protocole</a></li>
								<li><a href="/support/user/">Contactez support technique</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">Explore</h6>
							<ul class="slide">
								<li><a href="/events/">Formations et évènement</a></li>
								<li><a href="/support/techlib/seminar/">Séminaires</a></li>
								<li><a href="/pressreleases/">Communiqués de presse</a></li>
								<li><a href="/corporate/jobs/">Carrières</a></li>
								<li><a href="/corporate/">Profile de société</a></li>
							</ul>
						</div>
					</div>
					<div class="row holder-social">
						<div class="col"><span>Follow Teledyne LeCroy</span>
							<ul class="social-list">
								<li><a href="https://www.facebook.com/TeledyneLecroy/" target="_blank"><i class="icon-fb"></i></a></li>
								<li><a href="https://twitter.com/teledynelecroy" target="_blank"><i class="icon-tw"></i></a></li>
								<li><a href="https://www.youtube.com/user/LeCroyCorp" target="_blank"><i class="icon-yt"></i></a></li>
								<li><a href="https://blog.teledynelecroy.com/" target="_blank"><i class="icon-blogger"></i></a></li>
								<li><a href="https://www.linkedin.com/company/teledyne-lecroy" target="_blank"><i class="icon-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="sub-footer">
						<div class="row flex-lg-row-reverse">
							<div class="col-lg-3">
								<div class="dropdown-languages dropdown-item holder-languages"><a href="#"><i class="icon-world"></i><span>English | United States</span></a>
									<div class="drop">
										<ul class="list-languages d-flex flex-column-reverse">
											<li class="active"><a href="/">English (United States)</a></li>
											<li><a href="/europe/english.aspx">English (Europe)</a></li>
											<li><a href="/france/">Français</a></li>
											<li><a href="/germany/">Deutsch</a></li>
											<li><a href="/it/">Italiano</a></li>
											<li><a href="/japan/">日本語</a></li>
											<li><a href="/korea/">한국어</a></li>
											<li><a href="http://www.teledynelecroy.com.cn/">中文</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-9">
								<ul class="d-sm-flex">
									<li><a href="/support/securitypolicy.aspx">Respect des données personnelles</a></li>
									<li><a href="/terms/">Terms and Conditions</a></li>
									<li><a href="/sitemap/">Carte du site</a></li>
									<li><a href="http://www.teledyne.com/">Teledyne Technologies</a></li>
									<li><span class="copyright">Copyright &copy; 2020</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
