﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="footer_KO.ascx.vb" Inherits="LeCroy.Website.footer_KO" %>
				<div class="container">
					<p style="margin-top: 1rem; margin-bottom: 1rem; background-color:aqua;"><asp:PlaceHolder ID="phSocialMediaLinks" runat="server" /></p>
					<div class="inner-footer-content acc-footer">
						<div class="item-footer">
							<h6 class="title opener">Solutions</h6>
							<ul class="slide">
								<li><a href="/oscilloscope/">Oscilloscopes</a></li>
								<li><a href="/protocolanalyzer/">Protocol Solutions</a></li>
								<li><a href="/testtools/">Electronic Test Equipment</a></li>
								<li><a href="https://www.spdevices.com/products/hardware">Modular Data Acquisition</a></li>
								<li><a href="https://teledyne-ts.com/overview.html">Sensors and Calibrators</a></li>
								<li><a href="/services/">Testing and Training Services</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">구매</h6>
							<ul class="slide">
								<li><a href="/shopper/requestquote/">견적 요청</a></li>
								<li><a href="/support/contact/">연락처</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">Support</h6>
							<ul class="slide">
								<li><a href="/support/techlib/">Document Library</a></li>
								<li><a href="/support/techlib/videos.aspx">Video Library</a></li>
								<li><a href="/support/softwaredownload/">소프트웨어 다운로드</a></li>
								<li><a href="/support/softwaredownload/firmwareupgrade.aspx">오실로스코프 펌웨어</a></li>
								<li><a href="/support/softwaredownload/psgdocuments.aspx">프로토콜분석기 SW</a></li>
								<li><a href="/support/user/">Contact Technical Support</a></li>
							</ul>
						</div>
						<div class="item-footer">
							<h6 class="title opener">Explore</h6>
							<ul class="slide">
								<li><a href="/events/korea.aspx">이벤트 & 교육</a></li>
								<li><a href="/pressreleases/">보도자료</a></li>
								<li><a href="/corporate/jobs/">채용 정보</a></li>
								<li><a href="/corporate/">About Teledyne LeCroy</a></li>
							</ul>
						</div>
					</div>
					<div class="row holder-social">
						<div class="col"><span>Follow Teledyne LeCroy</span>
							<ul class="social-list">
								<li><a href="https://www.facebook.com/TeledyneLeCroyKorea/" target="_blank"><i class="icon-fb"></i></a></li>
                                <li><a href="https://twitter.com/teledynelecroy" target="_blank"><i class="icon-tw"></i></a></li>
								<li><a href="https://www.youtube.com/TeledyneLeCroyKorea" target="_blank"><i class="icon-yt"></i></a></li>
								<li><a href="https://blog.naver.com/lecroykorea" target="_blank"><i class="icon-blogger"></i></a></li>
								<li><a href="https://www.linkedin.com/company/teledyne-lecroy" target="_blank"><i class="icon-in"></i></a></li>
							</ul>
						</div>
					</div></div>
					<div class="sub-footer"><div class="container">
						<div class="row flex-lg-row-reverse">
							<div class="col-lg-3">
								<div class="dropdown-languages dropdown-item holder-languages"><a href="#"><i class="icon-world"></i><span>English | United States</span></a>
									<div class="drop">
										<ul class="list-languages d-flex flex-column-reverse">
											<li><a href="/">English (United States)</a></li>
											<li><a href="/europe/english.aspx">English (Europe)</a></li>
											<li><a href="/france/">Français</a></li>
											<li><a href="/germany/">Deutsch</a></li>
											<li><a href="/it/">Italiano</a></li>
											<li><a href="/japan/">日本語</a></li>
											<li class="active"><a href="/korea/">한국어</a></li>
											<li><a href="http://www.teledynelecroy.com.cn/">中文</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-9">
								<ul class="d-sm-flex">
									<li><a href="/support/securitypolicy.aspx">개인정보보호 정책</a></li>
									<li><a href="/terms/">Terms and Conditions</a></li>
									<li><a href="/sitemap/">사이트맵</a></li>
									<li><a href="http://www.teledyne.com/">Teledyne Technologies</a></li>
									<li><span class="copyright">Copyright &copy; <script type="text/javascript">document.write(new Date().getFullYear());</script></span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
