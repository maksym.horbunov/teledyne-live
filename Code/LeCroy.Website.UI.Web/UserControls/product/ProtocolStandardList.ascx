﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProtocolStandardList.ascx.vb" Inherits="LeCroy.Website.UserControls_Product_ProtocolStandardList" %>
<script language="javascript" type="text/javascript">
    jQuery.noConflict();
    jQuery(document).ready(function ($) {
        $('.no-click').click(function (e) {
            e.stopPropagation();
        });
    });
</script>
<asp:Repeater ID="rptProtocolStandards" runat="server">
    <HeaderTemplate>
        <table border="0" cellpadding="0" cellspacing="0" style="background-color: #ffffff;" width="833">
    </HeaderTemplate>
    <ItemTemplate>
            <tr>
                <td align="left" valign="top">
                    <div class="accordion" style="width: 730px;" id="divAccordion" runat="server">
                        <div class="list" style="background-color: #ffffff;" id="divList" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="font-weight: normal;" valign="top" width="200">
                                        <asp:HyperLink ID="hlkProtocolStandard" runat="server" CssClass="no-click">
                                            <asp:Image ID="imgProtocolStandard" runat="server" BorderStyle="None" /><br />
                                            <asp:Literal ID="litProtocolStandardName" runat="server" />
                                        </asp:HyperLink>
                                    </td>
                                    <td>
                                        <asp:Image ID="imgProtocolStandardFeatured" runat="server" BorderStyle="None" />
                                    </td>
                                    <td align="left" style="font-weight: normal;" valign="top" width="450">
                                        <span>
                                            <asp:Literal ID="litProtocolStandardDescription" runat="server" />
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="faq" style="display: none; font-weight: normal;">
                            <asp:Repeater ID="rptProductSeries" runat="server">
                                <HeaderTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr>
                                            <td rowspan="2" width="200">&nbsp;</td>
                                            <td align="center" valign="top" width="100">
                                                <div class="pacatpadtop"><asp:HyperLink ID="hlkProductSeriesImage" runat="server"><asp:Image ID="imgProductSeries" runat="server" BorderStyle="None" /></asp:HyperLink></div>
                                            </td>
                                            <td align="left" valign="top">
                                                <div class="pacatheadline"><asp:HyperLink ID="hlkProductSeriesName" runat="server" /></div>
                                                <div class="pacatdescription"><asp:Literal ID="litProductSeriesDescription" runat="server" /></div>
                                            </td>
                                        </tr>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                </SeparatorTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </td>
            </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>