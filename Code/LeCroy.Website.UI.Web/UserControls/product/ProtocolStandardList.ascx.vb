﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class UserControls_Product_ProtocolStandardList
    Inherits UserControl

#Region "Variables/Keys"
    Private _currentProtocolStandardId As Int32
    Private _currentProtocolStandardSeoValue As String

    Public Property ProtocolStandards As List(Of ProtocolStandard) = New List(Of ProtocolStandard)
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        BindRepeater()
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptProtocolStandards_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptProtocolStandards.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ProtocolStandard = CType(e.Item.DataItem, ProtocolStandard)
                Dim hlkProtocolStandard As HyperLink = CType(e.Item.FindControl("hlkProtocolStandard"), HyperLink)
                Dim imgProtocolStandard As Image = CType(e.Item.FindControl("imgProtocolStandard"), Image)
                Dim litProtocolStandardName As Literal = CType(e.Item.FindControl("litProtocolStandardName"), Literal)
                Dim imgProtocolStandardFeatured As Image = CType(e.Item.FindControl("imgProtocolStandardFeatured"), Image)
                Dim litProtocolStandardDescription As Literal = CType(e.Item.FindControl("litProtocolStandardDescription"), Literal)
                Dim rptProductSeries As Repeater = CType(e.Item.FindControl("rptProductSeries"), Repeater)
                Dim divAccordion As HtmlGenericControl = CType(e.Item.FindControl("divAccordion"), HtmlGenericControl)
                Dim divList As HtmlGenericControl = CType(e.Item.FindControl("divList"), HtmlGenericControl)

                imgProtocolStandard.AlternateText = row.Name
                imgProtocolStandard.ImageUrl = String.Format("{0}/images/category/{1}", ConfigurationManager.AppSettings("RootDir"), row.ProductImage)
                litProtocolStandardName.Text = row.Name
                If Not (String.IsNullOrEmpty(row.PrimaryStandardImage)) Then
                    imgProtocolStandardFeatured.AlternateText = row.Name
                    imgProtocolStandardFeatured.ImageUrl = String.Format("{0}/images/category/{1}", ConfigurationManager.AppSettings("RootDir"), row.PrimaryStandardImage)
                Else
                    imgProtocolStandardFeatured.Visible = False
                End If
                litProtocolStandardDescription.Text = row.Description

                If Not (String.IsNullOrEmpty(row.UrlB)) Then
                    hlkProtocolStandard.NavigateUrl = row.UrlB
                    divAccordion.Attributes("class") = String.Empty
                    divAccordion.Style.Add("background-color", "#fff;")
                    divAccordion.Style.Add("border", "1px solid #dfdfdf;")
                    divAccordion.Style.Add("margin", "0 auto;")
                    divAccordion.Style.Add("width", "730px;")
                    divList.Style.Clear()
                Else
                    If Not (String.IsNullOrEmpty(row.SeoValue)) Then
                        hlkProtocolStandard.NavigateUrl = String.Format("~/protocolanalyzer/{0}", row.SeoValue)
                        _currentProtocolStandardSeoValue = row.SeoValue
                    Else
                        hlkProtocolStandard.NavigateUrl = String.Format("~/protocolanalyzer/protocolstandard.aspx?standardid={0}&capid=103&mid=511", row.ProtocolStandardId)
                    End If

                    _currentProtocolStandardId = row.ProtocolStandardId
                    AddHandler rptProductSeries.ItemDataBound, AddressOf rptProductSeries_ItemDataBound
                    rptProductSeries.DataSource = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), ProtocolStandardSeriesRepository.GetProtocolStandardSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), row.ProtocolStandardId).Select(Function(x) x.ProductSeriesId).ToList()).Where(Function(x) x.Disabled.ToUpper() = "N").OrderBy(Function(x) x.SortId.Value).ToList()
                    rptProductSeries.DataBind()
                End If
        End Select
    End Sub

    Private Sub rptProductSeries_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ProductSeries = CType(e.Item.DataItem, ProductSeries)
                Dim hlkProductSeriesImage As HyperLink = CType(e.Item.FindControl("hlkProductSeriesImage"), HyperLink)
                Dim imgProductSeries As Image = CType(e.Item.FindControl("imgProductSeries"), Image)
                Dim hlkProductSeriesName As HyperLink = CType(e.Item.FindControl("hlkProductSeriesName"), HyperLink)
                Dim litProductSeriesDescription As Literal = CType(e.Item.FindControl("litProductSeriesDescription"), Literal)

                'HACK
                If (row.ProductSeriesId = 561) Then
                    _currentProtocolStandardSeoValue = "ethernet"
                End If
                If Not (String.IsNullOrEmpty(row.SeoValue)) Then
                    If Not (String.IsNullOrEmpty(_currentProtocolStandardSeoValue)) Then
                        hlkProductSeriesName.NavigateUrl = String.Format("~/protocolanalyzer/{0}/{1}", _currentProtocolStandardSeoValue, row.SeoValue)
                    Else
                        hlkProductSeriesName.NavigateUrl = String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", row.ProductSeriesId)
                    End If
                Else
                    hlkProductSeriesName.NavigateUrl = String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", row.ProductSeriesId)
                End If
                hlkProductSeriesName.Text = row.Name
                litProductSeriesDescription.Text = row.Description
                If Not (String.IsNullOrEmpty(row.SeriesImg)) Then
                    imgProductSeries.AlternateText = row.Name
                    imgProductSeries.ImageUrl = row.SeriesImg
                    hlkProductSeriesImage.NavigateUrl = hlkProductSeriesName.NavigateUrl
                End If
        End Select
    End Sub

    Public Sub rptProductSeries_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        If (String.Compare(e.CommandName, "ShowSeries", True) = 0) Then
            Dim c As LinkButton = CType(e.Item.FindControl("btnProductSeriesName"), LinkButton)
            If Not (String.IsNullOrEmpty(c.CommandArgument)) Then
                Dim split As String() = c.CommandArgument.ToString().Split("|")
                If Not (String.IsNullOrEmpty(split(0))) AndAlso Not (String.IsNullOrEmpty(split(1))) Then
                    Response.Redirect(String.Format("~/protocolanalyzer/{0}/{1}", split(1), split(0)))
                Else
                    Response.Redirect(String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", split(2)))
                End If
            End If
        ElseIf (String.Compare(e.CommandName, "ShowSeriesImage", True) = 0) Then
            Dim c As LinkButton = CType(e.Item.FindControl("btnProductSeriesImage"), LinkButton)
            If Not (String.IsNullOrEmpty(c.CommandArgument)) Then
                Dim split As String() = c.CommandArgument.ToString().Split("|")
                If Not (String.IsNullOrEmpty(split(0))) AndAlso Not (String.IsNullOrEmpty(split(1))) Then
                    Response.Redirect(String.Format("~/protocolanalyzer/{0}/{1}", split(1), split(0)))
                Else
                    Response.Redirect(String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", split(2)))
                End If
            End If
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindRepeater()
        rptProtocolStandards.DataSource = ProtocolStandards
        rptProtocolStandards.DataBind()
    End Sub
#End Region
End Class