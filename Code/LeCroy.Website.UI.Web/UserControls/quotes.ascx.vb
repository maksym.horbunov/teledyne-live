﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities
Imports System.Data
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Website.Functions

Public Class UserControls_Quotes_Head
    Inherits UserControl
    Dim strProfile As String = ""
    Dim area As String = ""
    Dim hasHardware As Boolean = False
    Dim hasSoftware As Boolean = False
    Dim hasProbes As Boolean = False
    Dim hasAccessories As Boolean = False
    Dim hasWarranty As Boolean = False
    Public Property selectedmodel As String
    Dim strPageURL As String = ""
    Dim ds As DataSet
    Public Event QuoteLanguageChangeEvent As System.EventHandler
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("flgQuotes") = "true"
        If Not Request.Url.AbsoluteUri Is Nothing Then
            If Len(Request.Url.AbsoluteUri.ToString()) > 0 Then
                strPageURL = LCase(Request.Url.AbsoluteUri.ToString())
            End If
        End If
        Initial()
    End Sub
    Protected Sub Initial()
        LoadQuoteHeader()
    End Sub
    Protected Sub LoadQuoteHeader()
        If Len(selectedmodel) > 0 Then
            hasHardware = hasOptions(selectedmodel, AppConstants.CAT_ID_HARDWARE_OPTIONS, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            hasSoftware = hasOptions(selectedmodel, AppConstants.CAT_ID_SOFTWARE_OPTIONS, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            hasProbes = hasOptions(selectedmodel, AppConstants.CAT_ID_PROBES, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            hasAccessories = hasOptions(selectedmodel, AppConstants.CAT_ID_ACCESSORIES, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
            hasWarranty = hasOptions(selectedmodel, AppConstants.GROUP_ID_WARRANTY, Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        End If
        If Len(strPageURL) > 0 Then
            If InStr(strPageURL, "/shopper/requestquote/configure_small_step1.aspx") > 0 Then
                area = "<div class='white'></div><div class='white'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteEnd'></div>"
                area += "<div class='blue'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='blueEnd'></div>"
                area += "<div class='blue'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/shopper/requestquote/configure_small_step2.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='Configure_Small_Step1.aspx?catid=" + Session("cataid").ToString() + "'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'><a href='configure_small_step2.aspx?SeriesID=" + Session("SeriesID").ToString() + "'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='whiteEnd'></div>"
                area += "<div class='blue'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/shopper/requestquote/configure_small_step3.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='Configure_Small_Step1.aspx?catid=" + Session("cataid").ToString() + "'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                If Not Session("PrefixScopeSeries") Is Nothing And Len(Session("PrefixScopeSeries")) > 0 Then
                    area += "<div class='grey'>" + LoadI18N("SLBCA0777", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                    area += "<div class='greyEndGrey'></div>"
                    area += "<div class='grey'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                Else
                    area += "<div class='grey'><a href='configure_small_step2.aspx?SeriesID=" + Session("SeriesID").ToString() + "'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                End If
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div><div class='whiteFinish'></div>"
            ElseIf InStr(strPageURL, "/shopper/requestquote/select_model.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='Configure_Small_Step1.aspx?catid=" + Session("cataid").ToString() + "'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'>" + LoadI18N("SLBCA0777", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteEnd'></div>"
                area += "<div class='blue'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div><div class='blueFinish'></div>"
                Me.lbArea.Text = area
                lbArea.Visible = True
            ElseIf InStr(strPageURL, "/shopper/requestquote/select_scope.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='Configure_Small_Step1.aspx?catid=" + Session("cataid").ToString() + "'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0777", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteEnd'></div>"
                area += "<div class='blue'><a href='configure_small_step2.aspx?SeriesID=" + Session("SeriesID").ToString() + "'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='blueEnd'></div>"
                area += "<div class='blue'>" + LoadI18N("SLBCA0763", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/default.aspx") > 0 Then
                area = "<div class='white'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='whiteFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step2.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                If Len(selectedmodel) > 0 Then
                    area += "<div class='whiteEnd'></div>"
                    If hasHardware = True Then
                        area += "<div class='blue'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                    End If
                    If hasSoftware = True Then
                        area += "<div class='blue'><a href='configure_step4.aspx'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                    End If
                    If hasProbes = True Then
                        area += "<div class='blue'><a href='configure_step5.aspx'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                    End If
                    If hasAccessories = True Then
                        area += "<div class='blue'><a href='configure_step6.aspx'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                    End If
                    If hasWarranty = True Then
                        area += "<div class='blue'><a href='configure_step7.aspx'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                    End If
                    area += "<div class='blue'><a href='configure_step8.aspx'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
                End If
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step3.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'><a href='javascript:step3()'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteEnd'></div>"
                If hasSoftware = True Then
                    area += "<div class='blue'><a href='javascript:step4()'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasProbes = True Then
                    area += "<div class='blue'><a href='javascript:step5()'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasAccessories = True Then
                    area += "<div class='blue'><a href='javascript:step6()'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasWarranty = True Then
                    area += "<div class='blue'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step4.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                If hasHardware = True Then
                    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                    area += "<div class='greyEnd'></div>"
                Else
                    area += "<div class='greyEnd'></div>"
                End If
                area += "<div class='white'><a href='javascript:step4()'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteEnd'></div>"
                If hasProbes = True Then
                    area += "<div class='blue'><a href='javascript:step5()'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasAccessories = True Then
                    area += "<div class='blue'><a href='javascript:step6()'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasWarranty = True Then
                    area += "<div class='blue'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step5.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                If hasHardware = True Then
                    area += "<div class='greyEndGrey'></div>"
                    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                End If
                If hasSoftware = True Then
                    area += "<div class='greyEndGrey'></div>"
                    area += "<div class='grey'><a href='configure_step4.aspx''>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEnd'></div>"
                Else
                    area += "<div class='greyEnd'></div>"
                End If
                area += "<div class='white'><a href='javascript:step5()'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteEnd'></div>"
                If hasAccessories = True Then
                    area += "<div class='blue'><a href='javascript:step6()'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                If hasWarranty = True Then
                    area += "<div class='blue'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step6.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                If hasHardware = True Then
                    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                    area += "<div class='greyEndGrey'></div>"
                End If
                If hasSoftware = True Then
                    area += "<div class='grey'><a href='configure_step4.aspx'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEndGrey'></div>"
                End If
                If hasProbes = True Then
                    area += "<div class='grey'><a href='configure_step5.aspx'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEnd'></div>"
                Else
                    area += "<div class='greyEnd'></div>"
                End If
                area += "<div class='white'><a href='javascript:step6()'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteEnd'></div>"
                If hasWarranty = True Then
                    area += "<div class='blue'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueEnd'></div>"
                End If
                area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step7.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                If hasHardware = True Then
                    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                    area += "<div class='greyEndGrey'></div>"
                End If
                If hasSoftware = True Then
                    area += "<div class='grey'><a href='configure_step4.aspx'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEndGrey'></div>"
                End If
                If hasProbes = True Then
                    area += "<div class='grey'><a href='configure_step5.aspx'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEndGrey'></div>"
                End If
                If hasAccessories = True Then
                    area += "<div class='grey'><a href='configure_step6.aspx'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEnd'></div>"
                End If
                area += "<div class='white'><a href='javascript:step7()'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteEnd'></div>"
                area += "<div class='blue'><a href='javascript:step8()'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='blueFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/configure_step8.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                area += "<div class='grey'><a href='configure_step2.aspx'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEndGrey'></div>"
                If hasHardware = True Then
                    area += "<div class='grey'><a href='configure_step3.aspx'>" + LoadI18N("SLBCA0757", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                    area += "<div class='greyEndGrey'></div>"
                End If
                If hasSoftware = True Then
                    area += "<div class='grey'><a href='configure_step4.aspx'>" + LoadI18N("SLBCA0758", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEndGrey'></div>"
                End If
                If hasProbes = True Then
                    area += "<div class='grey'><a href='configure_step5.aspx'>" + LoadI18N("SLBCA0759", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEndGrey'></div>"
                End If
                If hasAccessories = True Then
                    area += "<div class='grey'><a href='configure_step6.aspx'>" + LoadI18N("SLBCA0760", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                End If
                If hasWarranty = True Then
                    area += "<div class='greyEndGrey'></div>"
                    area += "<div class='grey'><a href='configure_step7.aspx'>" + LoadI18N("SLBCA0761", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='greyEnd'></div>"
                Else
                    area += "<div class='greyEnd'></div>"
                End If
                area += "<div class='white'><a href='#'>" + LoadI18N("SLBCA0762", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div><div class='whiteFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/modular/default.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='/oscilloscope/configure/'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/modular/step1.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteFinish'></div>"
            ElseIf InStr(strPageURL, "/oscilloscope/configure/modular/step2.aspx") > 0 Then
                area = "<div class='greyStart'></div><div class='grey'><a href='default.aspx'>" + LoadI18N("SLBCA0755", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</a></div>"
                area += "<div class='greyEnd'></div>"
                area += "<div class='white'>" + LoadI18N("SLBCA0756", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString()) + "</div>"
                area += "<div class='whiteFinish'></div>"
            End If
            If Len(area) > 0 Then
                Me.lbArea.Text = area
                lbArea.Visible = True
            End If
        End If
    End Sub
End Class