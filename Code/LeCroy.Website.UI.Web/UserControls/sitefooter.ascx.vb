﻿Public Class UserControls_sitefooter
    Inherits UserControl

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        litCopyrightYear.Text = DateTime.Now.Year.ToString()
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class