﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="siteheader.ascx.vb" Inherits="LeCroy.Website.UserControls_siteheader" %>
<div class="navbars navbar-fixed-top">
    <div class="navbar-inner noshadow">
        <div class="container">
            <a class="brand" href="/">
                <img class="logo screen" src="../images/logo_white.png" width="189" height="30" border="0" />
            </a>
        </div>
        <img class="logo print" src="../images/lecroy.jpg" width="240" height="38" border="0" />
    </div>
</div>