﻿Public Class UserControls_viewer_information
    Inherits UserControl

#Region "Variables/Keys"
    Public Property DocumentDate As DateTime = DateTime.MinValue
    Public Property Author As String = String.Empty
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ulInformation.Controls.Add(GetListItem("nav-header flat_grey screen", "Document Information", False))
        If Not (DocumentDate = DateTime.MinValue) Then ulInformation.Controls.Add(GetListItem("screen", String.Format("<i class=""icon-calendar ""></i>{0}", DocumentDate.ToShortDateString()), True))
        If Not (String.IsNullOrEmpty(Author)) Then ulInformation.Controls.Add(GetListItem("screen", String.Format("<i class=""icon-user ""></i>{0}", Author), True))
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Function GetListItem(ByVal cssClass As String, ByVal itemText As String, ByVal isHtmlContent As Boolean) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        If Not (String.IsNullOrEmpty(cssClass)) Then
            li.Attributes.Add("class", cssClass)
        End If
        If (isHtmlContent) Then
            li.InnerHtml = itemText
        Else
            li.InnerText = itemText
        End If
        Return li
    End Function
#End Region
End Class