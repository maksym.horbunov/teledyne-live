﻿Imports System.Collections

Public Class UserControls_viewer_options
    Inherits UserControl

#Region "Variables/Keys"
    Public Property SeoUrl As String
    Public Property SupportedFormats As OrderedDictionary = New OrderedDictionary()
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ulOptions.Controls.Add(GetListItem("nav-header flat_grey", "Additional Formats", False))
        Dim formats As List(Of HtmlGenericControl) = GetListItems(SupportedFormats)
        For Each li As HtmlGenericControl In formats
            ulOptions.Controls.Add(li)
        Next
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Function GetListItem(ByVal cssClass As String, ByVal itemText As String, ByVal isHtmlContent As Boolean) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        If Not (String.IsNullOrEmpty(cssClass)) Then
            li.Attributes.Add("class", cssClass)
        End If
        If (isHtmlContent) Then
            li.InnerHtml = itemText
        Else
            li.InnerText = itemText
        End If
        Return li
    End Function

    Private Function GetListItems(ByVal formats As OrderedDictionary) As List(Of HtmlGenericControl)
        Dim retVal As List(Of HtmlGenericControl) = New List(Of HtmlGenericControl)
        If (formats.Count > 0) Then
            Dim ide As IDictionaryEnumerator = formats.GetEnumerator()
            While (ide.MoveNext())
                Dim classes As String() = ide.Value.Split("|")
                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                li.InnerHtml = String.Format("<i class=""{0}"" style=""""></i>", classes(0))
                Dim btn As LinkButton = New LinkButton()
                If Not (String.IsNullOrEmpty(classes(1))) Then btn.Attributes.Add("class", classes(1))
                If Not (String.IsNullOrEmpty(SeoUrl)) Then
                    Dim url As String = SeoUrl
                    If (SeoUrl.Contains("#")) Then
                        url = SeoUrl.Split("#")(0)
                    End If
                    btn.PostBackUrl = String.Format("~/doc/{0}#{1}", url, classes(2))
                    btn.Text = ide.Key
                End If
                li.Controls.Add(btn)
                retVal.Add(li)
            End While
        End If
        Return retVal
    End Function
#End Region
End Class