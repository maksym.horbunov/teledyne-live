﻿Public Class UserControls_viewer_summary
    Inherits UserControl

#Region "Variables/Keys"
    Public Property Description As String = String.Empty
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ulSummary.Controls.Add(GetListItem("nav-header flat_grey screen", "Summary"))
        If Not (String.IsNullOrEmpty(Description)) Then ulSummary.Controls.Add(GetListItem("screen-indent", Description))
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Function GetListItem(ByVal cssClass As String, ByVal itemText As String) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        li.Attributes.Add("class", cssClass)
        li.InnerText = itemText
        Return li
    End Function
#End Region
End Class