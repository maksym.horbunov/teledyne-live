<%'@CODEPAGE=65001 %>
<!--#INCLUDE virtual="/cgi-bin/profile/allfunctions.asp" -->
<%Session("localeid")=1041

 Session("RedirectTo") = "/webdoc_japan/default.asp?wp=" & Request.QueryString("wp") 'here is the name of the file itself
If Not ValidateUser Then
	' Do not http// for redirect
	Response.redirect "/cgi-bin/profile/SentToReg.asp"
End If
ValidateUserNoRedir
%>

<html>
<head>

</head><body></body>
<% 
sAgent = Request.ServerVariables("HTTP_USER_AGENT")%>
<!-- #INCLUDE virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy White Papers for Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	if len(Session("localeid"))=0 then Session("localeid")=1033
	localeid=Session("localeid")
'**********************************************************************************
if len (Request.QueryString("wp"))>0 then
	if len(getpdf(Request.QueryString("wp")))>0 then
		if clng(Session("ContactId"))>0 then
			RequestId=InsertRequest (Session("ContactId"), 12, Request.QueryString("wp"), "No Remarks for Download", "",0)
			'****************send email***********************
			strCRet=chr(13)&chr(10)
			ValidateUserNoRedir
			if len(EmailBodyClientData)>0 then
				strBody = strBody & "New PDF download:"  & strCRet & strCRet 
				strBody = strBody &  EmailBodyClientData & strCRet 
				strBody = strBody & getdescription(Request.QueryString("wp")) & strCRet &  getpdf(Request.QueryString("wp")) & strCRet & strCRet 
				If len(RequestID)> 0  Then 
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "contact.jp@teledynelecroy.com"
							.BCC= "kate.kaplan@teledynelecroy.com"
							.From = "webmaster@teledynelecroy.com"
							.Subject = "New Download"
							.TextBody = strBody
							.textBodyPart.Charset = "shift-JIS"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing
				End If
			end if
		end if
		Response.Redirect getpdf(Request.QueryString("wp"))
	else
		Response.Redirect "/japan/pdf/lab/default.asp"
	end if
end if
Response.Redirect "/japan/pdf/lab/default.asp"
function getpdf(docid)
	if len(docid)>0 then
		if isnumeric(docid) then
			set rsGetPDF=server.CreateObject("ADODB.RECORDSET")
			strSQL="select * from WEB_DOCUMENTS where DOCUMENT_ID=" & docid
			'Response.Write strSQL
			rsGetPDF.Open strSQL, dbconnstr()
			if not rsGetPDF.EOF then
				getpdf=trim(rsGetPDF("HYPERLINK"))
			else
				getpdf=""
			end if
		else
			getpdf=""
		end if
	else
		getpdf=""
	end if
end function
function getdescription(docid)
	if len(docid)>0 then
		if isnumeric(docid) then
			set rsGetDesc=server.CreateObject("ADODB.RECORDSET")
			strSQL="select * from WEB_DOCUMENTS where DOCUMENT_ID=" & docid
			'Response.Write strSQL
			rsGetDesc.Open strSQL, dbconnstr()
			if not rsGetDesc.EOF then
				getdescription=trim(rsGetDesc("TITLE"))
			else
				getdescription=""
			end if
		else
			getdescription=""
		end if
	else
		getdescription=""
	end if
end function
%>

</html>