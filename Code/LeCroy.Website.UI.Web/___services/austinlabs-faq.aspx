﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="austinlabs-faq.aspx.vb" Inherits="LeCroy.Website.austinlabs_faq" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<main id="main" role="main" class="skew-style v1 header-transparented">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-2.jpg"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md col-lg-9 col-xl-7 px-sm-0 px-md-3">
									<h1>A full line of advanced Protocol Training classes</h1>
									<div class="row">
										<div class="col-xl-9">
											<p class="mb-0">Instructor-led classes guiding you through the protocol specifications with hands on labs including trace analysis</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

                <section class="section-anchors init-slide-anchors">
					<div class="container"><a class="opener d-lg-none"><span>Austin Labs FAQ</span><i class="icon-arrow-down"></i></a>
						<div class="slide">
							<div class="container">
								<ul class="anchor-nav">
									<li><a href="#WhyTrain">Why Do I need Training?</a></li>
									<li><a href="#WhatIsTraining">What Is Training?</a></li>
									<li><a href="#WhatGainTraining">What will I gain from training?</a></li>
									<li><a href="#WhatIsOnMyQuote">What is the item on my quote?</a></li>
									<li><a href="#HowDoISignUp">How do I sign up for class?</a></li>
									<li><a href="#WhatIfIHaveQuestions">What if I have questions?</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>


				<section id="WhyTrain" class="section-provide section-training-slider section-white">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Why Do I Need Training?</h2>
								</div>
							</div>
						</div>
                        <p>

                            One of the services offered by Teledyne LeCroy’s Austin Labs is access to our catalog of in-depth protocol training classes. In these classes, students are presented with guided insight into the history, structure, functional capability, use cases for the protocol under discussion. As an integral part of the discussion, interwoven with the various aspects of the protocol, we also educate students on how to use a hardware protocol analyzer to physically assess, verify, and debug the protocol interactions under discussion. With a mix of presentation, discussion, and hands-on labs we can consistently and reliably assure that every student leaves the class with an operational knowledge of the protocol and the core skills needed to identify these functional aspects in an analyzer trace.
                        </p>
                        <p>
                            With each topic, we start at the beginning of the life of communication…How does the protocol build the communication path? Is there a link to initialize? Are there connections that must be built? Any steps required to create the underlying infrastructure for the passing of information are logically presented and then reinforced by analyzing a relevant trace that displays those actions. Once the highways and byways of transportation for the protocol exist, we move on to the aspects of discovery and introduction. How does a device know who it is or where it resides in the system topology? What kind of credentials are needed to participate in this new, undiscovered world? How big is the world? What other devices or communication partners are in the topology? Are they open and available for interaction? After a discussion of the protocol’s characteristics and capabilities in these areas the students are given the opportunity to step through the actual processes and transmissions encompassed in a protocol trace capture analysis lab. Once the infrastructure is in place and introductions have been made, we branch out to ancillary but vital functions such as flow-control and prioritization. What keeps one device from talking over another? What if a device has something immediate and important to say? All protocols deal with these concepts, but each protocol has its own unique implementation. After acquiring a theoretical understanding of the functional topics our curriculum reinforces that learning with protocol trace analysis lab exercises that exemplify the communication and interaction. Finally, after analyzing the construction of the world we will be living in, we examine the movement of actual data…across the links and connections…to the discovered and introduced devices…being moderated, modulated, and prioritized by the ancillary functions. We take a look at the protocol performing the function it was designed to perform. In watching the reads and writes from one device to another the students can view and understand the end result function that they are engineering and testing to achieve.
                        </p>
                        <p>
                            While protocols may differ in their communication and capability, our approach to teaching these protocols is always the same. Each student will walk away with a thorough understanding of the environment in question as well as functional skills that they can apply to their role upon completion of the class.
                        </p>
					</div>
				</section>
                <section id="WhatIsTraining" class="section-provide section-training-slider section-gray">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">What is Training?</h2>
								</div>
							</div>
						</div>
                        <p>
                            Each class is a 4-day, instructor led, face-to-face or virtual training that has specifically been designed around the protocols and architectures being discussed. The content for each class is drawn from both the specifications written and ratified by the appropriate standards bodies as well as our years of experience in the industry providing Subject Matter Expert (SME), testing, and training services. Every class contains both presentation and discussion of the materials as well as protocol trace capture analysis lab exercises at various milestones throughout the training material. The training is NOT a product demo or sales pitch. While we are affiliated with Teledyne LeCroy and use their analyzer hardware and software to prepare and deliver the labs, our time and effort in the class is solely directed at teaching the relevant protocol and architecture specifications.
                        </p>

					</div>
				</section>
                <section id="WhatGainTraining" class="section-provide section-training-slider section-white">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">What will I gain from training?</h2>
								</div>
							</div>
						</div>
                        <p>
                            As a result of attending one of our in-depth protocol classes, a student can expect to leave with a more complete, holistic, view of the protocol and its parts. As a result of the class labs and hands-on exercises, students will acquire and hone their trace analysis skills while applying newly acquired protocol functional knowledge to approach a problem or question. This, in turn, will make them much more efficient at debugging issues, resolving architectural compatibility conflicts, and generally being able to locate bad behavior in the communication between devices. Having this improved knowledge and capability set can only serve to encourage a sense of confidence and assuredness in the student’s day to day activities when dealing with the subject matter at hand. Many companies also use our protocol classes to satisfy their employees’ Major Business Objectives (MBO) pertaining to training, continued education, and career growth requirements.
                        </p>

					</div>
				</section>				
                
                <section id="WhatIsOnMyQuote" class="section-provide section-training-slider section-gray">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">What is the item on my quote?</h2>
								</div>
							</div>
						</div>
                        <p>
                            If you see the part number “TECH-TRN-XXX-01” on your quote, this represents the purchase of three (3) seats in any of our public, 4-day, virtual or onsite, protocol training classes. These seats are valid in any class and do not have to correspond with the protocol capabilities of the newly purchased hardware. Also, the seats may be used individually or as a group (i.e. sending three people to one class or one person to three classes). It is also important to note that their listed price on your quote represents a 25% discount off of the standard pricing model. If you would like to capitalize on this discount and purchase additional seats at the reduced price, please speak to your sales representative. Understand, this pricing is only available when combined with a new hardware purchase platform and unused training seats will expire 1 year after the date of purchase.
                        </p>

					</div>
				</section>	              
                <section id="HowDoISignUp" class="section-provide section-training-slider section-white">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">How do I sign up for class?</h2>
								</div>
							</div>
						</div>
                        <p>
                            Once the purchase process is completed and you are ready to register for class, you can go to the <a class="text-blue" href="https://teledynelecroy.com/events/?eventtypeid=5" target="_self">Upcoming Trainings</a> page to find out what classes 
                                are currently scheduled. From there, simply select the class that interests you and on the resulting description page for that class, locate and select 
                                the registration link. Once you are registered, you will be notified in follow-up emails of how to attend and retrieve the student materials for that 
                                class. If you do not see the class that you would like to attend, feel free to contact us at <a class="text-blue" href="mailto:Austin_Labs_Training@Teledyne.com">Austin_Labs_Training@Teledyne.com</a>.                        </p>

					</div>
				</section>	

				<section id="WhatIfIHaveQuestions" class="section-explore section-gray">
					<div class="container small">                                            
						<div class="justify-content-center section-heading text-center">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Got a question? Ask us about trainings</h2>
							<div class="sub-title">We’re here to help and answer any question you might have. We look forward to hearing from you</div>
						</div>
						<div class="explore-content">
 <iframe src="https://go.teledynelecroy.com/l/48392/2020-07-24/7zhn6w" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
						</div>
					</div>
				</section>
				<section class="section-explore">
					<div class="container">    
						<div class="card-line horizontale p-0 d-flex">
							<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/explore-2.jpg"></span></div>
							<div class="inner-content">
								<h4 class="title">Explore physical and protocol layer training solutions</h4>
								<div class="card-text">
									<p>Austin Labs is the premier third-party test and validation center for servers, storage, and network devices across multiple technologies. The lab provides customized services to help our customers deliver fully tested products to market on time and within budget.</p>
								</div>
								<div class="footer-slide">                                              <a class="btn btn-default" href="austinlabs-training.aspx">Visit Training Solutions Page</a></div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
