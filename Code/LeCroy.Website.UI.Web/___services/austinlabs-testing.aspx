﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="austinlabs-testing.aspx.vb" Inherits="LeCroy.Website.austinlabs_testing" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<link href="../../css/styles.services.css" rel="stylesheet" />
	<link href="../../css/style_v0121.css" rel="stylesheet" />
	<script src="../../js/jquery.services.js"></script>
	<script src="../../js/jquery.main_v0121.js"></script>

			<main id="main" role="main" class="has-header-transparented skew-style v1 reset">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container"> 
							<div class="row">
								<div class="col-md-12"><span class="page">Austin Labs</span>
									<h1>Physical and Protocol Layer Testing Solutions</h1>
									<p>Building Technical Expertise, Workplace Efficiency and Product Quality</p>
								</div>
							</div>
						</div>
					</div>
				</section>
<section class="section-training-solutions section-angle">
					<div class="container">          
						<div class="section-heading text-center">             
							<h2>Our testing services</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-11 col-md-11 col-lg-10 col-xl-7">
									<p>Testing ensures expected functionality, increases stability, improves reliability, and provides a higher quality product to you customers</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="slider-block">
								<div class="slider-block-content"> 
									<div class="slider-item">
										<div class="slider-item-img"><a href="/files/pdf/AL_Testing_ZNS_testing.pdf" target="_blank"><img src="https://assets.lcry.net/images/labs-slider-1.png" alt="description"></a></div>
										<div class="slider-item-title"><a class="link-arrow" href="/files/pdf/AL_Testing_ZNS_testing.pdf" target="_blank"><span>Zoned Namespace ZNS Testing Services </span><i class="icon-btn-right-01"> </i></a></div>
									</div>
									<div class="slider-item">
										<div class="slider-item-img"><a href="/files/pdf/AL_Testing_OCP_NVMe_Cloud_SSD_testing.pdf" target="_blank"><img src="https://assets.lcry.net/images/labs-slider-2.png" alt="description"></a></div>
										<div class="slider-item-title"><a class="link-arrow" href="/files/pdf/AL_Testing_OCP_NVMe_Cloud_SSD_testing.pdf" target="_blank"><span>Explore Open Compute Project OCP SSD Cloud Testing Services</span><i class="icon-btn-right-01"> </i></a></div>
									</div>
									<div class="slider-item">
										<div class="slider-item-img"><a href="/files/pdf/AL_Testing_PCIe_protocol_compliance_testing.pdf" target="_blank"><img src="https://assets.lcry.net/images/labs-slider-3.png" alt="description"></a></div>
										<div class="slider-item-title"><a class="link-arrow" href="/files/pdf/AL_Testing_PCIe_protocol_compliance_testing.pdf" target="_blank"><span>PCI Express Protocol Compliance Testing Services</span><i class="icon-btn-right-01"> </i></a></div>
									</div>
									<div class="slider-item">
										<div class="slider-item-img"><a href="/files/pdf/AL_Testing_PCIe_electrical_compliance_testing.pdf" target="_blank"><img src="https://assets.lcry.net/images/labs-slider-4.png" alt="description"></a></div>
										<div class="slider-item-title"><a class="link-arrow" href="/files/pdf/AL_Testing_PCIe_electrical_compliance_testing.pdf" target="_blank"><span>PCI Express Electrical Compliance Testing Services </span><i class="icon-btn-right-01"> </i></a></div>
									</div>
								</div>
								<div class="slider-block-action"> 
									<div class="slider-block-info"><span class="current"></span> / <span class="length"> </span></div>
								</div>
							</div>
							<h3 class="text-center mb-32">What do we test?</h3>
							<div class="card-line mb-40">
								<div class="inner-content">
									<ul class="row list-dots two ml-0">
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Data Integrity</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Certification</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Open Compute</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Virtualization</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Conformance</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Storage </li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Servers</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Arrays</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Test Planning</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Fabrics	</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Signal Integrity</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Electrical Analysis</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Storage Compliance</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Kubernetes</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Interoperability</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Network Devices	</li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Software Defined </li>
										<li class="col-lg-2 col-md-4 col-sm-6 col-6">Performance</li>
									</ul>
								</div>
							</div>
							<h3 class="mb-40 text-center">Teledyne LeCroy Austin Labs can test using the following protocols            </h3>
							<div class="logos-list-wrap mb-40">
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/pci-express">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-1.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/nvm-express">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-2.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/usb">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-3.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/serial-attached-scsi">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-4.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/serial-ata">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-5.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/cxl">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-6.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/ethernet">
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-7.png" alt="logo"></div>
										</div></a></div>
								<div class="logos-item"><a href="https://teledynelecroy.com/protocolanalyzer/fibre-channel"> 
										<div class="content">
											<div class="visual"><img src="https://assets.lcry.net/images/labs-logo-8.png" alt="logo"></div>
										</div></a></div>
							</div>
							
							<h3 class="mb-40 text-center">[Table heading]     </h3>
							<div class="table-responsive"> 
								<table class="table-third">
									<thead>
										<tr> 
											<th> </th>
											<th>   </th>
											<th> <b>IP/Silicon</b><br>Fundamental technology blocks</th>
											<th> <b>Device</b><br>Discrete, application-specific product</th>
											<th> <b>System – Embedded</b><br>Product of components interacting in one board-level design</th>
											<th> <b>System – Integrated</b><br>System built by integrating multiple off-the shelf and/or custom devices</th>
										</tr>
									</thead>
									<tbody>
										<tr class="group1">
											<td class="first-icon" rowspan="3"> <span>Oscilloscopes</span></td>
											<td> <b>Base Electrical Test</b></td>
											<td>To verify perfomance relative to the Base specification</td>
											<td>To verify component when integrated into device</td>
											<td>To verify component when integrated into system</td>
											<td>Verified upstream        </td>
										</tr>
										<tr class="group1">
											<td> <b>Electrical Debug</b></td>
											<td>To find root cause of problems at the component/package level</td>
											<td>To find root cause of problems at the board/module level</td>
											<td>For dealing with interoperability issues</td>
											<td>For dealing with interoperability issues</td>
										</tr>
										<tr class="group1">
											<td> <b>Electrical Compliance Test</b></td>
											<td>To verify that component can enable compliant devices</td>
											<td>For providing compliance to standard or specification</td>
											<td>For providing compliance to standard or specification</td>
											<td>To qualify system level building blocks </td>
										</tr>
										<tr class="group2"> 
											<td class="first-icon" rowspan="3"> <span>Protocol Analyzers</span></td>
											<td> <b>Protocol Stimulus</b></td>
											<td>For early componrnt development when ecosystem isn’t yet copmlete </td>
											<td>For error injection, perfomance, corner case and regression testing</td>
											<td>For error injection testing and component emulation</td>
											<td>For testing error handling, also provides Host and Device emulation  </td>
										</tr>
										<tr class="group2">
											<td> <b>Protocol Analysis</b></td>
											<td>Provides ability to identify, understand, and debug communication issues</td>
											<td>Ensures error-free operation while allowing for debugging and perfomance tuning</td>
											<td>Ensures error-free operation while allowing for debugging and performance tuning</td>
											<td>For resolving interoperabulity issues and providing perfomance analysis</td>
										</tr>
										<tr class="group2">
											<td> <b>Protocol Compliance</b></td>
											<td>Validate link and logical communication layers are functioning within specification</td>
											<td>Provides for full compliance testing for link and transaction layers</td>
											<td>For ensuring system operation aligns with specification requirements</td>
											<td>For ensuring interoperability with various compliant devices</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
				<section class=" section-angle-before section-padding section-gray angle-small">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Enterprise Solutions Testing</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-11 col-md-11 col-lg-10 col-xl-11">
									<p>Test and validation center for servers, storage, and network devices</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-lg-4 col-md-6 mb-32 mb-md-0"> 
									<div class="card-line p-0">
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/austin-labs-testing-1.jpg"></span></div>
										<div class="inner-content">
											<h4 class="title">Attitude                                          </h4>
											<ul class="list-check">
												<li><i class="icon-chek"></i><span>We will find serious flaws and bugs with our testing</span></li>
												<li><i class="icon-chek"></i><span>We will help you understand how to isolate and fix these problems</span></li>
												<li><i class="icon-chek"></i><span>We are your partners</span></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 mb-md-0"> 
									<div class="card-line p-0">
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/austin-labs-testing-2.jpg"></span></div>
										<div class="inner-content">
											<h4 class="title">Approach                                           </h4>
											<ul class="list-check">
												<li><i class="icon-chek"></i><span>We focus on the entire product from documentation to critical issues</span></li>
												<li><i class="icon-chek"></i><span>We specialize in data corruption, data loss, disruptions</span></li>
												<li><i class="icon-chek"></i><span>We are protocol experts and have the tools to test protocol compliance                   </span></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 mb-md-0"> 
									<div class="card-line p-0">
										<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/austin-labs-testing-3.jpg"></span></div>
										<div class="inner-content">
											<h4 class="title">Quality                                          </h4>
											<ul class="list-check">
												<li><i class="icon-chek"></i><span>We use Teledyne LeCroy test and analysis tools along with industry tools</span></li>
												<li><i class="icon-chek"></i><span>We supplement internal testing and provide an external validation</span></li>
												<li><i class="icon-chek"></i><span>Our customers always come back for more   </span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="section-lifecycle section-angle" id="lifecycle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12 col-md-9 col-lg-8">
								<div class="section-heading text-center mx-auto">
									<h2 class="px-0 mx-sm-5 mx-md-0">Testing Services in Product Lifecyle</h2>
									<div class="row">
										<div class="col mx-auto">
											<p class="pb-md-1 pb-lg-0">Austin Labs has the right service offering for every stage of the product lifecycle and for every layer, from the
                                                 physical to the application.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row tabs-area">
							<div class="col-md-4 col-xl-12 acc-pci tabs-pci">
								<div class="lc-item active"><a class="opener" href="#" data-href="#tab-pci-1"><i class="icon-training"></i><span>Training</span></a>
									<div class="slide-content">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, 
                                                test and debug the relevant specification.  Austin Labs offers in-depth classes across many different protocols to help you become a protocol expert! 
											</p>
											<div class="wrap-btn"><a class="btn btn-default" href="austinlabs-training.aspx">Explore Austin Labs Training Solutions</a></div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-2"><i class="icon-prototype"></i><span>Prototype</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Prototype</h3>
												<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s Austin Labs helps quickly and economically identify layout and interconnect issues through the use of High Definition Oscilloscopes and power rail probes sniff out power integrity problems.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-3"><i class="icon-power-on"></i><span>Power On</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Power On</h3>
												<p>At first power our customers are looking for a seasoned partner to provide multi-lane signal integrity testing.  Austin Labs provides the expertise to help capture, analyze and debug with a complete lab of test equipment including High-Bandwidth Oscilloscopes.  </p>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-4"><i class="icon-bring-up"></i><span>Bring Up</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<div class="text">
												<h3 class="title">Bring Up</h3>
												<p>Critical testing to verify signal integrity and overall capabilities is a necessary across multiple phases of the product lifecycle.  Austin Labs offers pre-compliance testing for many different protocols including PCIe Gen5 and USB4.  Moving into the protocol we are able to test power-on behaviors for next-gen storage technologies before host controllers are commercially available. Offering complete control of timing and link states, the PCIe, NVMe, SAS, and SATA exerciser systems allow users to verify low-level protocol behaviors and deliver faster time-to-market. </p>
											</div>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-5"><i class="icon-validation"></i><span>Validation</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Validation</h3>
											<p>The heart of testing is in validating a product’s overall performance and capabilities.  Austin Labs can work with customer test plans to provide additional testing resources or create and deliver test plans for feature and specification validation.  We work very closely with our customer to define the needs and can provide a full customized solution that includes, but not limited to data integrity, stress, performance analysis, protocol analysis, error injection/recovery, and software solutions.  </p>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-6"><i class="icon-debug"></i><span>Debug</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Debug</h3>
											<p>Critical to testing is the ability to provide live debug and analysis of issues as they are discovered.  Austin Labs provides complete and detailed issues lists for issue reproduction as well as protocol traces that are captured specific to each configuration.  Add a picture – person troubleshooting or debugging with test equipment.</p>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-7"><i class="icon-compliance"></i><span>Compliance</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Compliance</h3>
											<p>Austin Labs is directly involved with many of the industry standard committees for network, storage, and IO.  We offer testing for compliance across many of the standard specifications.  Austin Labs also creates custom conformance programs for many of our partners to help them address gaps in testing coverage.  These compliance capabilities include full automation of tests suites to allow for standardized pass/fail testing.</p>
    									</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-8"><i class="icon-resolution"></i><span>Problem Resolution</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Problem Resolution</h3>
											<p>Many times there is finger pointing on who is responsible for a bug.  Austin Labs can help quickly identify violations that cause issues and help determine the root cause of the failing configuration.  By using fast and efficient debug tools and test equipment Austin Labs helps our customers save money and time.  </p>
										</div>
									</div>
								</div>
								<div class="lc-item"><a class="opener" href="#" data-href="#tab-pci-9"><i class="icon-testing"></i><span>Regression Testing</span></a>
									<div class="slide-content row">
										<div class="col-xl-6">
											<h3 class="title">Regression Testing</h3>
											<p>As part of any required testing, the ability to quickly validate and reproduce bug fixes is important.  Austin Labs can perform full or partial execution of previously attempted tests and fully document issue resolution, inadequate fixes, or newly introduced issues with details for reproduction and captures traces of the offending behavior.  This allows for quick and confident issue regression.  Regression testing is a required step in validation and the product life cycle.  </p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-xl-12 tab-holder">
								<div class="slide-content row flex-xl-row-reverse" id="tab-pci-1">
									<div class="col-xl-6">
										<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/img-lifecycle-01.png, https://assets.lcry.net/images/img-lifecycle-01@2x.png 2x"/><img src="https://assets.lcry.net/images/img-lifecycle-01.png" alt="image description"/>
											</picture>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Training</h3>
											<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, 
                                                test and debug the relevant specification.  Austin Labs offers in-depth classes across many different protocols to help you become a protocol expert! </p>
											<div class="wrap-btn pt-lg-0"><a class="btn btn-default" href="austinlabs-training.aspx">Get know Explore Austin Labs Training Solutions</a></div>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-2">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Prototype</h3>
											<p>The first board turn provides the foundation for your design. Teledyne LeCroy’s Austin Labs helps quickly and economically identify layout and interconnect issues through the use of High Definition Oscilloscopes and power rail probes sniff out power integrity problems.  Add picture that includes eye pattern and Wavepro HD.</p>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-3">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Power On</h3>
											<p>At first power our customers are looking for a seasoned partner to provide multi-lane signal integrity testing.  Austin Labs provides the expertise to help capture, analyze and debug with a complete lab of test equipment including High-Bandwidth Oscilloscopes.</p>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-4">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Bring Up</h3>
											<p>Critical testing to verify signal integrity and overall capabilities is a necessary across multiple phases of the product lifecycle.  Austin Labs offers pre-compliance testing for many different protocols including PCIe Gen5 and USB4.  Moving into the protocol we are able to test power-on behaviors for next-gen storage technologies before host controllers are commercially available. Offering complete control of timing and link states, the PCIe, NVMe, SAS, and SATA exerciser systems allow users to verify low-level protocol behaviors and deliver faster time-to-market.  </p>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-5">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Validation</h3>
											<p>The heart of testing is in validating a product’s overall performance and capabilities.  Austin Labs can work with customer test plans to provide additional testing resources or create and deliver test plans for feature and specification validation.  We work very closely with our customer to define the needs and can provide a full customized solution that includes, but not limited to data integrity, stress, performance analysis, protocol analysis, error injection/recovery, and software solutions.  </p>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-6">
									<div class="col-xl-6">
										<div class="text">
											<h3 class="title">Debug</h3>
											<p>Critical testing to verify signal integrity and overall capabilities is a necessary across multiple phases of the product lifecycle.  Austin Labs offers pre-compliance testing for many different protocols including PCIe Gen5 and USB4.  Moving into the protocol we are able to test power-on behaviors for next-gen storage technologies before host controllers are commercially available. Offering complete control of timing and link states, the PCIe, NVMe, SAS, and SATA exerciser systems allow users to verify low-level protocol behaviors and deliver faster time-to-market.  </p>
										</div>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-7">
									<div class="col-xl-6">
										<h3 class="title">Compliance</h3>
										<p>Austin Labs is directly involved with many of the industry standard committees for network, storage, and IO.  We offer testing for compliance across many of the standard specifications.  Austin Labs also creates custom conformance programs for many of our partners to help them address gaps in testing coverage.  These compliance capabilities include full automation of tests suites to allow for standardized pass/fail testing.</p>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-8">
									<div class="col-xl-6">
										<h3 class="title">Problem Resolution</h3>
										<p>Many times there is finger pointing on who is responsible for a bug.  Austin Labs can help quickly identify violations that cause issues and help determine the root cause of the failing configuration.  By using fast and efficient debug tools and test equipment Austin Labs helps our customers save money and time.  </p>
									</div>
								</div>
								<div class="slide-content row" id="tab-pci-9">
									<div class="col-xl-6">
										<h3 class="title">Regression Testing </h3>
										<p>As part of any required testing, the ability to quickly validate and reproduce bug fixes is important.  Austin Labs can perform full or partial execution of previously attempted tests and fully document issue resolution, inadequate fixes, or newly introduced issues with details for reproduction and captures traces of the offending behavior.  This allows for quick and confident issue regression.  Regression testing is a required step in validation and the product life cycle.  </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

<section class="section-external section-gray section-angle-before angle-small">
					<div class="container">
						<div class="justify-content-center">                        
							<div class="section-heading text-center mx-auto">
								<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">How Does External Validation Work?</h2>
							</div>
						</div>
						<div class="external-content">                                                                                            
							<div class="row">
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">This is a step-by-step overview on how we do the External Validation    </div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">1. Create Test Plan</div>
											<p>Jointly develop a test plan to meet your needs. Sample test plans are available                                     </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">2. Define Testing Scope</div>
											<p>Agree on a scope of work that needs to be covered in testing </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">3. Device Testing</div>
											<p>Austin Labs begins testing     </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">4. Preliminary Reporting</div>
											<p>Issues are reported with complete details for reproduction and trace capture and analysis provided                                    </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">5. Review and Adjust</div>
											<p>Interaction between customer and lab to determine next best steps     </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">6. Regression Testing</div>
											<p>Issues regression happens throughout testing through updates                </p>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-32">
									<div class="card-line no-bg">
										<div class="inner-content">
											<div class="number">7. Final Reporting</div>
											<p>Test results compiled and built into a final report </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-validation section-angle angle-small">
					<div class="container">
						<div class="justify-content-center">                        
							<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
								<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Why Third-Party Validation?</h2>
							</div>
						</div>
						<div class="external-content">                                                                                            
							<div class="row">
								<div class="col-lg-4 col-md-6 col-12 mb-24">
									<div class="card-line">
										<div class="inner-content">
											<h4 class="title d-flex"> 
												<div class="icons"><svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="26" cy="26" r="20" stroke="#0076C0" stroke-width="2"/>
<circle cx="26" cy="26" r="16" stroke="#0076C0" stroke-width="2"/>
<path d="M27.998 29.2891C27.998 28.7357 27.8223 28.2799 27.4707 27.9219C27.1191 27.5573 26.5332 27.2318 25.7129 26.9453C24.8926 26.6523 24.2546 26.3822 23.7988 26.1348C22.2819 25.321 21.5234 24.1165 21.5234 22.5215C21.5234 21.4408 21.8522 20.5521 22.5098 19.8555C23.1673 19.1589 24.0592 18.7454 25.1855 18.6152V16.4863H26.748V18.6348C27.8809 18.7975 28.7565 19.2793 29.375 20.0801C29.9935 20.8743 30.3027 21.9095 30.3027 23.1855H27.9395C27.9395 22.3652 27.7539 21.7207 27.3828 21.252C27.0182 20.7767 26.5202 20.5391 25.8887 20.5391C25.2637 20.5391 24.7754 20.7083 24.4238 21.0469C24.0723 21.3854 23.8965 21.8704 23.8965 22.502C23.8965 23.0684 24.069 23.5241 24.4141 23.8691C24.7656 24.2077 25.3581 24.5299 26.1914 24.8359C27.0247 25.1419 27.679 25.4251 28.1543 25.6855C28.6296 25.946 29.0299 26.2454 29.3555 26.584C29.681 26.916 29.9316 27.3001 30.1074 27.7363C30.2832 28.1725 30.3711 28.6836 30.3711 29.2695C30.3711 30.3698 30.0326 31.2617 29.3555 31.9453C28.6849 32.6289 27.7474 33.0326 26.543 33.1562V35.0605H24.9902V33.166C23.7012 33.0228 22.7083 32.5605 22.0117 31.7793C21.3216 30.998 20.9766 29.9629 20.9766 28.6738H23.3496C23.3496 29.4941 23.5547 30.1289 23.9648 30.5781C24.3815 31.0273 24.9674 31.252 25.7227 31.252C26.4648 31.252 27.028 31.0729 27.4121 30.7148C27.8027 30.3568 27.998 29.8815 27.998 29.2891Z" fill="#0076C0"/>
</svg>
												</div>Cost Savings
											</h4>
											<p>Third-party validation provides a defined solution with known expenses                                 </p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-12 mb-24">
									<div class="card-line">
										<div class="inner-content">
											<h4 class="title d-flex"> 
												<div class="icons"><svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M10.8022 10.8033C6.61191 14.9936 6.61191 21.8111 10.8022 26.0014L16.6853 31.8845C17.2072 32.4064 17.7827 32.8746 18.3954 33.2757C18.4516 33.3128 18.5136 33.3292 18.5746 33.3481C19.005 34.0468 19.5121 34.7111 20.1171 35.3161L26.0002 41.1992C30.1905 45.3895 37.0081 45.3895 41.1983 41.1992C45.3886 37.009 45.3886 30.1914 41.1983 26.0011L35.3152 20.118C34.7411 19.5439 34.1021 19.035 33.4164 18.6051C33.4085 18.6 33.3993 18.6002 33.3912 18.5955C32.9682 17.9183 32.4717 17.2743 31.8835 16.6861L26.0005 10.8031C21.81 6.61305 14.9924 6.61305 10.8022 10.8033ZM40.2178 26.9819C43.8674 30.6315 43.8674 36.5693 40.2178 40.2189C36.5682 43.8686 30.6304 43.8686 26.9808 40.2189L21.0976 34.3358C17.448 30.6862 17.448 24.7484 21.0976 21.0988C22.8547 19.3417 25.19 18.3753 27.68 18.3662L28.9418 19.628C29.0242 19.7104 29.0964 19.7987 29.1733 19.8843C26.6315 19.4216 23.9436 20.2139 22.0782 22.0793C18.9695 25.1879 18.9695 30.2467 22.0782 33.3553L27.9613 39.2384C31.0699 42.347 36.1287 42.347 39.2373 39.2384C42.3459 36.1298 42.3459 31.071 39.2373 27.9624L35.0701 23.7952C35.0495 23.7746 35.0228 23.7663 35.0005 23.7489C34.9609 22.9528 34.8392 22.1612 34.6241 21.3882L40.2178 26.9819ZM26.3811 17.0672C24.0149 17.3581 21.827 18.4084 20.1171 20.1183C18.3615 21.8739 17.3518 24.0918 17.0672 26.3833L13.7437 23.0598C11.1756 20.4917 11.1756 16.313 13.7437 13.7449C16.3118 11.1768 20.4906 11.1768 23.0587 13.7449L26.3811 17.0672ZM25.6198 34.9359C27.9858 34.6449 30.1746 33.5933 31.8834 31.8845C33.639 30.1289 34.6487 27.911 34.9333 25.6195L38.2568 28.9429C40.8249 31.5111 40.8249 35.6898 38.2568 38.2579C35.6887 40.826 31.5099 40.826 28.9418 38.2579L25.6198 34.9359ZM30.9029 17.667C34.5525 21.3166 34.5525 27.2544 30.9029 30.904C29.1469 32.66 26.811 33.629 24.3226 33.6387L23.0587 32.3748C20.4906 29.8067 20.4906 25.6279 23.0587 23.0598C24.9496 21.1689 27.8636 20.6138 30.3234 21.6697C31.3622 24.0764 30.9053 26.9795 28.9418 28.943C27.8746 30.0101 26.4599 30.6819 24.957 30.8346C24.5754 30.8735 24.3 31.2138 24.3374 31.5949C24.3761 31.9756 24.7165 32.2527 25.0977 32.2145C26.9165 32.0291 28.63 31.2158 29.9222 29.9236C33.0308 26.815 33.0308 21.7562 29.9222 18.6476L24.0391 12.7645C20.9305 9.65585 15.8717 9.65585 12.7631 12.7645C9.65447 15.8731 9.65447 20.9318 12.7631 24.0405L16.9303 28.2077C16.9509 28.2283 16.9776 28.2366 16.9999 28.254C17.0395 29.0501 17.1612 29.8417 17.3763 30.6147L11.7826 25.021C8.13294 21.3714 8.13294 15.4336 11.7826 11.7839C15.4322 8.13432 21.37 8.13433 25.0196 11.7839L30.9027 17.6671L30.9029 17.667Z" fill="#0076C0"/>
</svg>
												</div>Resource Constraints    
											</h4>
											<p>Doing more with less means having to find new ways to test         </p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-12 mb-24">
									<div class="card-line">
										<div class="inner-content">
											<h4 class="title d-flex"> 
												<div class="icons"><svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="25.5" y="38.5" width="1.5" height="7" fill="#0076C0"/>
<rect x="25.5" y="6.5" width="1.5" height="7" fill="#0076C0"/>
<rect x="30" y="6" width="4" height="1.5" fill="#0076C0"/>
<rect x="30" y="7.5" width="1.5" height="6" fill="#0076C0"/>
<rect width="4" height="1.5" transform="matrix(1 0 0 -1 30 46)" fill="#0076C0"/>
<rect width="1.5" height="6" transform="matrix(1 0 0 -1 30 44.5)" fill="#0076C0"/>
<rect width="4" height="1.5" transform="matrix(-1 0 0 1 22.5 6)" fill="#0076C0"/>
<rect width="1.5" height="6" transform="matrix(-1 0 0 1 22.5 7.5)" fill="#0076C0"/>
<rect x="22.5" y="46" width="4" height="1.5" transform="rotate(-180 22.5 46)" fill="#0076C0"/>
<rect x="22.5" y="44.5" width="1.5" height="6" transform="rotate(-180 22.5 44.5)" fill="#0076C0"/>
<rect x="44.5" y="30" width="1.5" height="4" fill="#0076C0"/>
<rect x="38.5" y="30" width="6" height="1.5" fill="#0076C0"/>
<rect width="1.5" height="4" transform="matrix(-1 0 0 1 7.5 30)" fill="#0076C0"/>
<rect width="6" height="1.5" transform="matrix(-1 0 0 1 13.5 30)" fill="#0076C0"/>
<rect width="1.5" height="4" transform="matrix(1 0 0 -1 44.5 22.5)" fill="#0076C0"/>
<rect width="6" height="1.5" transform="matrix(1 0 0 -1 38.5 22.5)" fill="#0076C0"/>
<rect x="7.5" y="22.5" width="1.5" height="4" transform="rotate(-180 7.5 22.5)" fill="#0076C0"/>
<rect x="13.5" y="22.5" width="6" height="1.5" transform="rotate(-180 13.5 22.5)" fill="#0076C0"/>
<rect x="6.5" y="25.5" width="7" height="1.5" fill="#0076C0"/>
<rect x="38.5" y="25.5" width="7" height="1.5" fill="#0076C0"/>
<rect x="12.75" y="12.75" width="26.5" height="26.5" fill="white" stroke="#0076C0" stroke-width="1.5"/>
<rect x="17.75" y="17.75" width="16.5" height="16.5" stroke="#0076C0" stroke-width="1.5"/>
</svg>
												</div>Hardware Needs
											</h4>
											<p>Independent labs provide access to a wide range of equipment  </p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-12 mb-24">
									<div class="card-line">
										<div class="inner-content">
											<h4 class="title d-flex"> 
												<div class="icons"><svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="25" y="29" width="2" height="10" transform="rotate(-45.1886 25 29)" fill="#0076C0"/>
<rect x="25" y="15" width="2" height="14" fill="#0076C0"/>
<circle cx="26" cy="26" r="20" stroke="#0076C0" stroke-width="2"/>
<circle cx="26" cy="26" r="16" stroke="#0076C0" stroke-width="2"/>
</svg>
												</div>Time
											</h4>
											<p>Delivery schedules are always shortened – beat the market with a partner                                    </p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-12 mb-24">
									<div class="card-line">
										<div class="inner-content">
											<h4 class="title d-flex"> 
												<div class="icons"><svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="25.9854" cy="26.1992" r="3" stroke="#0076C0" stroke-width="2"/>
<mask id="path-2-inside-1" fill="white">
<path fill-rule="evenodd" clip-rule="evenodd" d="M36.4854 25.4701C33.7113 22.8045 30.0252 21.1797 25.9799 21.1797C21.9399 21.1797 18.2582 22.8003 15.4854 25.4597L16.883 26.8916C19.3095 24.5757 22.5114 23.1797 25.9799 23.1797C29.4537 23.1797 32.66 24.5799 35.0877 26.9019L36.4854 25.4701Z"/>
</mask>
<path fill-rule="evenodd" clip-rule="evenodd" d="M36.4854 25.4701C33.7113 22.8045 30.0252 21.1797 25.9799 21.1797C21.9399 21.1797 18.2582 22.8003 15.4854 25.4597L16.883 26.8916C19.3095 24.5757 22.5114 23.1797 25.9799 23.1797C29.4537 23.1797 32.66 24.5799 35.0877 26.9019L36.4854 25.4701Z" fill="#C4C4C4"/>
<path d="M36.4854 25.4701L37.9165 26.8671L39.3246 25.4247L37.8711 24.028L36.4854 25.4701ZM15.4854 25.4597L14.101 24.0163L12.6448 25.4129L14.0542 26.8567L15.4854 25.4597ZM16.883 26.8916L15.4518 28.2886L16.8332 29.7038L18.2639 28.3384L16.883 26.8916ZM35.0877 26.9019L33.7053 28.3473L35.136 29.7157L36.5189 28.299L35.0877 26.9019ZM37.8711 24.028C34.7502 21.0291 30.5764 19.1797 25.9799 19.1797V23.1797C29.4741 23.1797 32.6724 24.5799 35.0996 26.9122L37.8711 24.028ZM25.9799 19.1797C21.3895 19.1797 17.2206 21.0242 14.101 24.0163L16.8697 26.9031C19.2958 24.5763 22.4903 23.1797 25.9799 23.1797V19.1797ZM18.3142 25.4945L16.9165 24.0627L14.0542 26.8567L15.4518 28.2886L18.3142 25.4945ZM25.9799 21.1797C21.9615 21.1797 18.2741 22.7993 15.5022 25.4448L18.2639 28.3384C20.3449 26.3522 23.0613 25.1797 25.9799 25.1797V21.1797ZM36.4701 25.4566C33.6968 22.8041 30.0044 21.1797 25.9799 21.1797V25.1797C28.9029 25.1797 31.6231 26.3557 33.7053 28.3473L36.4701 25.4566ZM35.0542 24.0731L33.6565 25.5049L36.5189 28.299L37.9165 26.8671L35.0542 24.0731Z" fill="#0076C0" mask="url(#path-2-inside-1)"/>
<mask id="path-4-inside-2" fill="white">
<path fill-rule="evenodd" clip-rule="evenodd" d="M17.0165 24.1855L25.9904 33.3789L34.9556 24.1946C35.4912 24.5927 36.0021 25.0254 36.4854 25.4898L27.3875 34.8101L25.9904 36.2413L24.5934 34.8101L15.4854 25.4794C15.969 25.0155 16.4804 24.5831 17.0165 24.1855Z"/>
</mask>
<path fill-rule="evenodd" clip-rule="evenodd" d="M17.0165 24.1855L25.9904 33.3789L34.9556 24.1946C35.4912 24.5927 36.0021 25.0254 36.4854 25.4898L27.3875 34.8101L25.9904 36.2413L24.5934 34.8101L15.4854 25.4794C15.969 25.0155 16.4804 24.5831 17.0165 24.1855Z" fill="#C4C4C4"/>
<path d="M17.0165 24.1855L18.4477 22.7885L17.2278 21.5388L15.8251 22.5792L17.0165 24.1855ZM25.9904 33.3789L24.5592 34.776L25.9904 36.2422L27.4216 34.776L25.9904 33.3789ZM34.9556 24.1946L36.1485 22.5893L34.7455 21.5466L33.5244 22.7975L34.9556 24.1946ZM36.4854 25.4898L37.9165 26.8868L39.3246 25.4443L37.8711 24.0476L36.4854 25.4898ZM27.3875 34.8101L28.8186 36.2072H28.8186L27.3875 34.8101ZM25.9904 36.2413L24.5592 37.6384L25.9904 39.1046L27.4216 37.6384L25.9904 36.2413ZM24.5934 34.8101L23.1622 36.2072H23.1622L24.5934 34.8101ZM15.4854 25.4794L14.101 24.0359L12.6448 25.4326L14.0542 26.8764L15.4854 25.4794ZM15.5853 25.5826L24.5592 34.776L27.4216 31.9819L18.4477 22.7885L15.5853 25.5826ZM27.4216 34.776L36.3868 25.5916L33.5244 22.7975L24.5592 31.9819L27.4216 34.776ZM33.7626 25.7998C34.23 26.1472 34.6765 26.5254 35.0996 26.9319L37.8711 24.0476C37.3277 23.5255 36.7525 23.0382 36.1485 22.5893L33.7626 25.7998ZM28.8186 36.2072L37.9165 26.8868L35.0542 24.0927L25.9563 33.4131L28.8186 36.2072ZM27.4216 37.6384L28.8186 36.2072L25.9563 33.4131L24.5592 34.8443L27.4216 37.6384ZM23.1622 36.2072L24.5592 37.6384L27.4216 34.8443L26.0246 33.4131L23.1622 36.2072ZM14.0542 26.8764L23.1622 36.2072L26.0246 33.4131L16.9165 24.0823L14.0542 26.8764ZM16.8697 26.9228C17.2932 26.5166 17.7402 26.1388 18.2079 25.7919L15.8251 22.5792C15.2206 23.0274 14.6449 23.5143 14.101 24.0359L16.8697 26.9228Z" fill="#0076C0" mask="url(#path-4-inside-2)"/>
<rect x="24.9854" y="17" width="2" height="4.2" fill="#0076C0"/>
<rect x="12.7002" y="22.5703" width="2" height="6" transform="rotate(-43.8492 12.7002 22.5703)" fill="#0076C0"/>
<rect x="16.5884" y="19.5996" width="2" height="5.93987" transform="rotate(-27.2877 16.5884 19.5996)" fill="#0076C0"/>
<rect width="2" height="5.93987" transform="matrix(-0.888716 -0.458458 -0.458458 0.888716 35.4854 19.5996)" fill="#0076C0"/>
<rect x="20.8164" y="17.7461" width="2" height="5.60301" transform="rotate(-13.4782 20.8164 17.7461)" fill="#0076C0"/>
<rect width="2" height="5.60301" transform="matrix(-0.972458 -0.233076 -0.233076 0.972458 31.1562 17.7461)" fill="#0076C0"/>
<rect width="2" height="6" transform="matrix(-0.716331 -0.69776 -0.69776 0.716331 39.2979 22.5918)" fill="#0076C0"/>
<circle cx="26" cy="26" r="20" stroke="#0076C0" stroke-width="2"/>
</svg>
												</div>Perspective
											</h4>
											<p>It is beneficial to have someone external to the project test with a new view       </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="section-explore section-gray">
					<div class="container small">                                            
						<div class="justify-content-center section-heading text-center">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Got a question? Ask us about testing or trainings</h2>
							<div class="sub-title">We’re here to help and answer any question you might have. We look forward to hearing from you</div>
						</div>
						<div class="explore-content">
							<iframe src="https://go.teledynelecroy.com/l/48392/2020-07-24/7zhn6w" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
						</div>
					</div>
				</section>

				<section class="section-explore">
					<div class="container">    
						<div class="card-line horizontale p-0 d-flex">
							<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/explore-3.jpg"></span></div>
							<div class="inner-content">
								<h4 class="title">Explore physical and protocol layer training solutions</h4>
								<div class="card-text">
									<p>Teledyne LeCroy’s Austin Labs is the premier third-party test and validation center for servers, storage, and network devices. With decades of testing experience, the lab provides customized services to help our customers deliver fully tested products to market on time and within budget. Experience the best test equipment available including Oscilloscopes, Protocol Analyzers, Jammers, Exercisers, BERT’s, and protocol compliance test suites. 
</p>
								</div>
								<div class="footer-card">                                              <a class="btn btn-default" href="austinlabs-training.aspx">Visit Protocol Training Page</a></div>
							</div>
						</div>
					</div>
				</section>
			</main>

</asp:Content>
