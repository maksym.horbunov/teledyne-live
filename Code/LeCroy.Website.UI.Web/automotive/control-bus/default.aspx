﻿<!DOCTYPE html>
<html lang="en">
<head>
<!-- META -->
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Control Bus | Automotive Solutions Group | Teledyne LeCroy</title>
  <meta name="description" content="Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analyzers have the right tools, right measurements.">
<!-- OG META -->
  <meta property="og:title" content="Teledyne LeCroy Automotive Solutions Group - A Measured Approach to the Connected Car."/>
  <meta property="og:image" content="http://teledynelecroy.com/automotive/img/og-controlbus.jpg"/>
  <meta property="og:url" content="http://teledynelecroy.com/automotive/control-bus/"/>
  <meta property="og:description" content="As the amount of data being transferred in-vehicle is rapidly increasing, control busses are evolving to meet the new demands. We provide unparalleled insight into serial data anomalies."/>

<!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
</head>

<body>

<!-- Navigation -->  
<div class="container">
  <nav>
    <div class="nav-wrapper white">
      <a href="/" class="brand-logo"><img src="../img/tl_weblogo_blkblue_189x30.png"></a>
      <ul class="right hide-on-med-and-down">
        <li>
          <div class="valign-wrapper">
            <a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true"><img src="../img/car-thumb.png" height="43px" class="responsive-img" alt="Automotive Solutions Group"></a>
          </div>
        </li>
      </ul>
      <ul class="left hide-on-med-and-down" style="margin-left: 200px; margin-top:10px;">
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdownp" data-beloworigin="true">Products</a></li>
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdowns" data-beloworigin="true">Serial Data</a></li>
        <li class="dropdown"><a class="dropdown-button" href="/support/" data-activates="dropdownu" data-beloworigin="true">Support</a></li>
        <li><a href="/support/">Buy</a></li>
      </ul>
    </div>
  </nav>
</div>

<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="http://www.fte.com">Frontline</a></li>
    <li><a href="http://teledynelecroy.com/protocolanalyzer/">Protocol Solutions Group</a></li>
    <li><a href="http://www.quantumdata.com/">quantumdata</a></li>
    <li><a href="http://teledynelecroy.com/oscilloscope/">Oscilloscopes</a></li>
    <li><a href="http://www.teledyne-ts.com">Torque Test</a></li>
</ul>
<ul id="dropdownp" class="dropdown-content">
  <li><a href="/oscilloscope/">Oscilloscopes</a></li>
  <li><a href="/probes/">Oscilloscope Probes</a></li>
  <li><a href="/protocolanalyzer/">Protocol Analyzers</a></li>
  <li><a href="/pert3/pertphoenix.aspx">PeRT</a></li>
  <li><a href="/waveformgenerators/">Waveform Generators</a></li>
  <li><a href="/logicstudio/">Logic Analyzers</a></li>
  <li><a href="/optical-modulation-analyzer/">Optical Modulation Analyzer</a></li>
  <li><a href="/sistudio/">Signal Integrity Studio</a></li>
</ul>
<ul id="dropdowns" class="dropdown-content">
  <li><a href="/serialdata/">Serial Data Standards</a></li>
  <li><a href="/sdaiii/">Serial Data Analysis</a></li>
</ul>
<ul id="dropdownu" class="dropdown-content">
  <li><a href="/support/techlib/">Tech Library</a></li>
  <li><a href="/support/service.aspx">Instrument Service</a></li>
  <li><a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a></li>
  <li><a href="/support/dsosecurity.aspx">Oscilloscope Security</a></li>
  <li><a href="/support/register/">Product Registration</a></li>
  <li><a href="/support/softwaredownload/">Software Downloads</a></li>
  <li><a href="/support/techhelp/">Technical Help</a></li>
  <li><a href="/support/requestinfo/">Request</a></li>
  <li><a href="/support/rohs.aspx">RoHS &amp; WEEE</a></li>
  <li><a href="/events/">Events &amp; Training</a></li>
  <li><a href="/support/training.aspx">Training</a></li>
  <li><a href="/support/user/userregisterform.aspx?update=y">Update Profile</a></li>
  <li><a href="/support/contact/">Contact Us</a></li>
</ul>
<!--end of dropwdowns -->
<!-- End Navigation -->

<!-- Main section -->
<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m12 l12">
        <h1>Control Busses</h1>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l6">
        <p class="flow-text">Automotive control busses enable communication between ECUs, sensors, actuators, etc. using well defined protocols. These protocols are deployed using electrical, optical, or wireless signaling. As the amount of data being transferred in-vehicle is rapidly increasing, control busses are evolving to meet the new demands.</p>
        <p class="flow-text">Today’s vehicles utilize an array of different protocols for in-vehicle communication.
          <ul>
            <li><strong>CAN</strong> – in-vehicle networks controlling window/seat operation, engine management, brake control, etc.</li>
            <li><strong>CAN FD</strong> – extends the bit rate of CAN via a flexible data rate</li>
            <li><strong>LIN</strong> – communication between components in vehicles, provides a cheaper alternative to CAN</li>
            <li><strong>FlexRay</strong> – utilizes two independent data channels for mission critical data transmission</li>
            <li><strong>SENT</strong> – point-to-point scheme for communication with sensors</li>
            <li><strong>MOST</strong> – electrically or optically based ring topology to transport data for infotainment</li>
            <li><strong>Automotive Ethernet</strong> – provides Ethernet connectivity in vehicles; used for infotainment, ADAS and can serve as Ethernet backbone</li>
          </ul>
        </p>
        <p class="flow-text">Common Control Bus Issues:
          <ul>
            <li>Serial bus protocol errors (ie: error frames or CRC errors)</li>
            <li>Reflections caused from improper termination</li>
            <li>Slow rise times causing timing errors in cause-effect relationships</li>
            <li>Physical layer abnormalities – runts, glitches, etc.</li>
          </ul>
        </p>
      </div>
      <div class="col s12 m12 l6">
          <img class="responsive-img" src="../img/control-busses-main.png">
      </div>
    </div>
  </div>
</div>
<!-- End Main section -->

<!-- Secondary section -->
<div class="wrap3">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
           <h3>The Teledyne LeCroy Solution</h3>
        </div>
      </div>
      <div class="row">
        <div class="col s12 m12 l6">
          <p class="flow-text">Teledyne LeCroy oscilloscopes can be equipped with a suite of protocol-specific measurement and eye diagram packages to provide unparalleled insight into serial data anomalies.</p>
          <p class="flow-text">These unique packages complement the industry’s most powerful trigger/decode software providing the ability to extract decoded data and plot over time, perform bus timing measurements, and create eye diagrams for testing against standard or custom masks.</p>
        </div>
        <div class="col s12 m12 l6">
          <p class="flow-text">Use Cases
            <ol>
              <li>Digital data extraction and graphing of CAN wheel speed information</li>
              <li>Detect a rogue message on a bus that is outside a list of specified message IDs</li>
              <li>Isolate signal integrity issues coming from a specific node on the bus</li>
              <li>Automated compliance testing of PHYs for Automotive Ethernet, USB, MOST, etc.</li>
              <li>Margin testing of different cable lengths and network topologies using eye diagrams and mask testing</li>
              <li>Corner-case testing of timing relationships between two specified events using long acquisitions and automated measurements (ie: message to analog, analog to message, message to message)</li>
            </ol>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Secondary section -->

<!-- Address Block -->
<div class="clearfix"></div>
<div class="wrap">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l6">
          <div class="google-maps" ><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2942.057740667409!2d-83.4350577845396!3d42.490325079178334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824afcff87b0001%3A0xe6e14e2b36e8e3a7!2s27007+Hills+Tech+Ct%2C+Farmington+Hills%2C+MI+48331!5e0!3m2!1sen!2sus!4v1476976339141" width="600" height="350" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
            <!-- <img src="img/tl-auto-solutions-brochure.jpg" class="responsive-img" style="padding-top:27px;" /> -->
        </div>
        <div class="col s12 m12 l6">
          <h3 class="maui-blue center"><b>Automotive Technology Center</b></h3>
          <p> The Teledyne LeCroy Automotive Technology Center is the premier Bluetooth device testing and services partner, with an enormous device library comprising 3000+ devices including coverage for every major car model.</p>
          <div class="row">
            <div class="col s12 m8">
              <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                  <p>Teledyne LeCroy Automotive Technology Center<br />
                  27007 Hills Tech Court<br />
                  Farmington Hills, Michigan 48331</p>
                  <ul class="collapsible" data-collapsible="accordion">
                    <li>
                    <div class="collapsible-header light-blue accent-4 white-text"><i class="material-icons left">email</i>Contact Us</div>
                    <div class="collapsible-body"><iframe src="http://go.teledynelecroy.com/l/48392/2016-10-19/4dsgck" height="350" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe></div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Address -->




<!-- Simple footer -->
<footer class="page-footer white">
  <div class="container center social">
    <p>
      <a href="http://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'http://facebook.com/teledynelecroy');">
        <img src="http://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'http://twitter.com/teledynelecroy');">
        <img src="http://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'http://youtube.com/lecroycorp');">
        <img src="http://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://blog.teledynelecroy.com/');">
        <img src="http://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'http://www.linkedin.com/company/10007');">
        <img src="http://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2017&nbsp;Teledyne LeCroy</p>
    <a target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |
    <a target="_blank" href="/sitemap/">Site Map</a>
    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
  </div>
</footer>
<!--End Footer -->

<!--  Scripts-->
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/init.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-13095488-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
<script src="http://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>