﻿<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Automotive Solutions Group | Teledyne LeCroy</title>
  <meta name="description" content="Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analyzers have the right tools, right measurements.">
  <meta property="og:title" content="Teledyne LeCroy Automotive Solutions Group - A Measured Approach to the Connected Car."/>
  <meta property="og:image" content="https://teledynelecroy.com/automotive/img/automotive_og.jpg"/>
  <meta property="og:url" content="https://teledynelecroy.com/automotive/"/>
  <meta property="og:description" content="Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analysis of wired and wireless systems have the right tools, right measurements, and fastest time to insight of any solution in the industry."/>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
</head>

<body>
<div class="container">
  <nav>
    <div class="nav-wrapper white">
      <a href="/" class="brand-logo"><img src="img/tl_weblogo_blkblue_189x30.png"></a>
      <ul class="right hide-on-med-and-down">
        <li>
          <div class="valign-wrapper">
            <a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true"><img src="img/car-thumb.png" height="43px" class="responsive-img" alt="Automotive Solutions Group"></a>
          </div>
        </li>
      </ul>
      <ul class="left hide-on-med-and-down" style="margin-left: 200px; margin-top:10px;">
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdownp" data-beloworigin="true">Products</a></li>
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdowns" data-beloworigin="true">Serial Data</a></li>
        <li class="dropdown"><a class="dropdown-button" href="/support/" data-activates="dropdownu" data-beloworigin="true">Support</a></li>
        <li><a href="/support/">Buy</a></li>
      </ul>
    </div>
  </nav>
</div>

<!-- Dropdown Structure -->

<ul id="dropdown1" class="dropdown-content">
    <li><a href="http://www.fte.com">Frontline</a></li>
    <li><a href="https://teledynelecroy.com/protocolanalyzer/">Protocol Solutions Group</a></li>
    <li><a href="http://www.quantumdata.com/">quantumdata</a></li>
    <li><a href="https://teledynelecroy.com/oscilloscope/">Oscilloscopes</a></li>
    <li><a href="http://www.teledyne-ts.com">Torque Test</a></li>
</ul>
<ul id="dropdownp" class="dropdown-content">
  <li><a href="/oscilloscope/">Oscilloscopes</a></li>
  <li><a href="/probes/">Oscilloscope Probes</a></li>
  <li><a href="/protocolanalyzer/">Protocol Analyzers</a></li>
  <li><a href="/pert3/pertphoenix.aspx">PeRT</a></li>
  <li><a href="/waveform-function-generators/">Waveform Generators</a></li>
  <li><a href="/logicanalyzers/">Logic Analyzers</a></li>
  <li>
      <a href="/tdr-and-s-parameters/">TDR and S-Parameters</a>
    </li>
      <li><a href="/spectrum-analyzers/">Spectrum Analyzers</a></li>
<li><a href="/power-supplies/">Power Supplies</a></li>
 <li><a href="/digital-multimeters/">Digital Multimeters</a></li>
</ul>
<ul id="dropdowns" class="dropdown-content">
  <li><a href="/serialdata/">Serial Data Standards</a></li>
  <li><a href="/sdaiii/">Serial Data Analysis</a></li>
</ul>
<ul id="dropdownu" class="dropdown-content">
  <li><a href="/support/techlib/">Tech Library</a></li>
  <li><a href="/support/service.aspx">Instrument Service</a></li>
  <li><a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a></li>
  <li><a href="/support/dsosecurity.aspx">Oscilloscope Security</a></li>
  <li><a href="/support/register/">Product Registration</a></li>
  <li><a href="/support/softwaredownload/">Software Downloads</a></li>
  <li><a href="/support/techhelp/">Technical Help</a></li>
  <li><a href="/support/requestinfo/">Request</a></li>
  <li><a href="/support/rohs.aspx">RoHS &amp; WEEE</a></li>
  <li><a href="/events/">Events &amp; Training</a></li>
  <li><a href="/support/training.aspx">Training</a></li>
  <li><a href="/support/user/userprofile.aspx">Update Profile</a></li>
  <li><a href="/support/contact/">Contact Us</a></li>
</ul>
<!--end of dropwdowns -->



<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m12 l12 xl12">
          <div class="slider">
            <ul class="slides">
              <li>
                <img src="img/lte_banner.jpg" class="responsive-img">
                <div class="caption right-align">
                  <h3 class="hide-on-med-and-up">Virtual Test Track</h3>
                  <h1 class="hide-on-small-only">Virtual Test Track</h1>
                  <h4 class="light grey-text text-lighten-3">Cellular Services Testing Solution</h4>
                  <a href="lte/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
                </div>
              </li>
              <li>
                <img src="img/infotainment_banner.jpg" class="responsive-img">
                <div class="caption right-align">
                  <h3 class="hide-on-med-and-up">Infotainment</h3>
                    <h1 class="hide-on-small-only">Infotainment</h1>
                  <h4 class="light grey-text text-lighten-3">Ensuring interoperability and performance</h4>
                  <a href="infotainment/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
                </div>
              </li>
              <li>
                <img src="img/adas_banner.jpg" class="responsive-img">
                <div class="caption left-align">
                  <h3 class="hide-on-med-and-up">ADAS</h3>
                    <h1 class="hide-on-small-only">ADAS</h1>
                  <h4 class="light grey-text text-lighten-3">Optimizing imaging and sensor data systems</h4>
                  <a href="adas/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
                </div>
              </li>
              <li>
                <img src="img/cybersecurity_banner.jpg" class="responsive-img">
                <div class="caption left-align">
                  <h3 class="hide-on-med-and-up">CyberSecurity</h3>
                    <h1 class="hide-on-small-only">CyberSecurity</h1>
                  <h4 class="light grey-text text-lighten-3">Exposing security vunerabilities and threats</h4>
                  <a href="cybersecurity/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
                </div>
              </li>
            </ul>
          </div>
      </div>
      <div class="col s12 m12 l12">
        <h1 style="font-weight:700; font-size: 35px;">A Measured Approach to the Connected Car</h1>
      </div>
      <div class="col s12 m8 l10">
        <p class="flow-text">The Teledyne LeCroy Automotive Solutions Group understands the challenging design environment facing today's automotive engineer. The connected car evolution combines complex embedded and wireless system design challenges with user experience, security and reliability in a way no other industry has experiences. Teledyne LeCroy's testing solutions for electrical embedded systems, torque measurements, and protocol analysis of wired and wireless systems have the right tools, right measurements, and fastest time-to-insight of any solution in the industry.</p>
      </div>
      <div class="col s12 m2 l2">
        <div class="row">
          <div class="col s12 m12 l12"><a href="/doc/docview.aspx?id=10054"><img src="img/automotive-solutions-group-brochure.png" class="responsive-img" border="0"></a></div>
          <div class="col s12 m12 l12 center-align"><a href="/doc/docview.aspx?id=10054" class="waves-effect waves-light btn #0288d1 light-blue darken-2">Download</a></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Subcategory Cards *****There are 3 sections for 3 media sizes Be sure to change all three!! -->

<!-- MED cards for MED screen sizes -->
<div class="clearfix"></div>
<div class="wrap hide-on-small-only hide-on-large-only">
  <div class="container">
    <div class="section">
    <!--Row 1 -->
      <div class="row">
        <div class="col s4">
          <div class="card">
            <div class="card-image ">
              <a href="adas/"><img class="activator" src="img/adas_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">ADAS<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">ADAS<i class="material-icons right">close</i></span>
              <p>ADAS systems rely heavily on image and sensor data. Our tools and services can help developers debug, validate, and performance tune ADAS systems with ease.</p>
            </div>
          </div>    
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="control-bus/"><img class="activator" src="img/control_bus_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Control Bus<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Control Bus<i class="material-icons right">close</i></span>
              <p>As the amount of data being transferred in-vehicle is rapidly increasing, control busses are evolving to meet the new demands. We provide unparalleled insight into serial data anomalies.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="cybersecurity/"><img class="activator" src="img/cybersecurity_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Cybersecurity<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Cybersecurity<i class="material-icons right">close</i></span>
              <p>We ensure that communications security protocols have been implemented correctly to limit exposure, and that targeted components are able to ward off hostile attacks.</p>
            </div>
          </div>   
        </div>
      </div>
      <!-- Row 2 -->
      <div class="row">        
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="infotainment/"><img class="activator" src="img/infotainment_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Infotainment<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Infotainment<i class="material-icons right">close</i></span>
              <p>Our tools let you test audio, video, and protocols of any type of digital video service, and help ensure that data storage, transfer and wired connectivity are reliable and efficient.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="motor-drive/"><img class="activator" src="img/motordrive_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Motor Drive<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Motor Drive<i class="material-icons right">close</i></span>
              <p>Our tools and services provide comprehensive and complete testing capability for all motor drive control, inverter subsection, and system level operations.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="powertrain/"><img class="activator" src="img/powertrain_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Powertrain<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Powertrain<i class="material-icons right">close</i></span>
              <p>Using noncontact, wireless telemetry technology, we provide custom torque measurement solutions for customer applications not often addressed by available alternative solutions.</p>
            </div>
          </div>   
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Small Single row cards for tablet / phone screen sizes -->
<div class="clearfix"></div>
<div class="wrap hide-on-med-only hide-on-large-only">
  <div class="container">
    <div class="section">
      <!--Row 1 -->
      <div class="row">
        <div class="col s12">

          <div class="card">
            <div class="card-image ">
              <a href="adas/"><img class="activator" src="img/adas_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">ADAS<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">ADAS<i class="material-icons right">close</i></span>
              <p>ADAS systems rely heavily on image and sensor data. Our tools and services can help developers debug, validate, and performance tune ADAS systems with ease.</p>
            </div>
          </div>    

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="control-bus/"><img class="activator" src="img/control_bus_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Control Bus<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Control Bus<i class="material-icons right">close</i></span>
              <p>As the amount of data being transferred in-vehicle is rapidly increasing, control busses are evolving to meet the new demands. We provide unparalleled insight into serial data anomalies.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="cybersecurity/"><img class="activator" src="img/cybersecurity_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Cybersecurity<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Cybersecurity<i class="material-icons right">close</i></span>
              <p>We ensure that communications security protocols have been implemented correctly to limit exposure, and that targeted components are able to ward off hostile attacks.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="infotainment/"><img class="activator" src="img/infotainment_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Infotainment<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Infotainment<i class="material-icons right">close</i></span>
              <p>Our tools let you test audio, video, and protocols of any type of digital video service, and help ensure that data storage, transfer and wired connectivity are reliable and efficient.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="motor-drive/"><img class="activator" src="img/motordrive_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Motor Drive<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Motor Drive<i class="material-icons right">close</i></span>
              <p>Our tools and services provide comprehensive and complete testing capability for all motor drive control, inverter subsection, and system level operations.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="powertrain/"><img class="activator" src="img/powertrain_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Powertrain<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Powertrain<i class="material-icons right">close</i></span>
              <p>Using noncontact, wireless telemetry technology, we provide custom torque measurement solutions for customer applications not often addressed by available alternative solutions.</p>
            </div>
          </div>   
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Stripey Section for LARGE screen sizes-->
<div class="hide-on-med-and-down">

  <div class="clearfix section"></div>
  <div class="wrap2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/adas_menu.jpg">
        </div>
        <div class="col s7 top">
          <h1 class="noPad">ADAS</h1>
          <p class="flow-text">ADAS systems rely heavily on image and sensor data. Our tools and services can help developers debug, validate, and performance tune ADAS systems with ease.</p>
          <a class="waves-effect waves-light btn #0288d1 light-blue darken-2" href="adas/"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>
 
  <div class="clearfix section"></div>
  <div class="none2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/control_bus_menu.jpg">
        </div>
        <div class="col s7">
          <h1 class="noPad">Control Bus</h1>
          <p class="flow-text">As the amount of data being transferred in-vehicle is rapidly increasing, control busses are evolving to meet the new demands. We provide unparalleled insight into serial data anomalies.</p>
          <a href="control-bus/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="clearfix section"></div>
  <div class="wrap2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/cybersecurity_menu.jpg">
        </div>
        <div class="col s7">
          <h1 class="noPad">Cybersecurity</h1>
          <p class="flow-text">We ensure that communications security protocols have been implemented correctly to limit exposure, and that targeted components are able to ward off hostile attacks.</p>
          <a href="cybersecurity/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="clearfix section"></div>
  <div class="none2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/infotainment_menu.jpg">
        </div>
        <div class="col s7">
          <h1 class="noPad">Infotainment</h1>
          <p class="flow-text">Our tools let you test audio, video, and protocols of any type of digital video service, and help ensure that data storage, transfer and wired connectivity are reliable and efficient.</p>
          <a href="infotainment/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="clearfix section"></div>
  <div class="wrap2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/motordrive_menu.jpg">
        </div>
        <div class="col s7">
          <h1 class="noPad">Motor Drive</h1>
          <p class="flow-text">Our tools and services provide comprehensive and complete testing capability for all motor drive control, inverter subsection, and system level operations.</p>
          <a href="motor-drive/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="clearfix section"></div>
  <div class="none2">
    <div class="container">
      <div class="row">
        <div class="col s5">
          <img class="responsive-img" src="img/powertrain_menu.jpg">
        </div>
        <div class="col s7">
          <h1 class="noPad">Powertrain</h1>
          <p class="flow-text">Using noncontact, wireless telemetry technology, we provide custom torque measurement solutions for customer applications not often addressed by available alternative solutions.</p>
          <a href="powertrain/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- End Stripey -->

<!-- End Cards Section -->

<!-- Address Block -->
<div class="clearfix"></div>
<div class="wrap">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l6">
          <div class="google-maps" ><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2942.057740667409!2d-83.4350577845396!3d42.490325079178334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824afcff87b0001%3A0xe6e14e2b36e8e3a7!2s27007+Hills+Tech+Ct%2C+Farmington+Hills%2C+MI+48331!5e0!3m2!1sen!2sus!4v1476976339141" width="600" height="350" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
            <!-- <img src="img/tl-auto-solutions-brochure.jpg" class="responsive-img" style="padding-top:27px;" /> -->
        </div>
        <div class="col s12 m12 l6">
          <h3 class="maui-blue center"><b>Automotive Technology Center</b></h3>
          <p> The Teledyne LeCroy Automotive Technology Center is the premier Bluetooth device testing and services partner, with an enormous device library comprising 3000+ devices including coverage for every major car model.</p>
          <div class="row">
            <div class="col s12 m8">
              <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                  <p>Teledyne LeCroy Automotive Technology Center<br />
                  27007 Hills Tech Court<br />
                  Farmington Hills, Michigan 48331</p>
                  <ul class="collapsible" data-collapsible="accordion">
                    <li>
                    <div class="collapsible-header light-blue accent-4 white-text"><i class="material-icons left">email</i>Contact Us</div>
                    <div class="collapsible-body"><iframe src="https://go.teledynelecroy.com/l/48392/2016-10-19/4dsgck" height="350" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe></div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Address -->




<!-- Simple footer -->
<footer class="page-footer white">
  <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2017&nbsp;Teledyne LeCroy</p>
    <a target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |
    <a target="_blank" href="/sitemap/">Site Map</a>
    <p><a href="https://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://teledyne.com');">Teledyne Technologies</a>
    </p>
  </div>
</footer>
<!--End Footer -->

<!--  Scripts-->
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/init.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-13095488-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
<script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>