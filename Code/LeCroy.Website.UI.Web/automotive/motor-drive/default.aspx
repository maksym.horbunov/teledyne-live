﻿<!DOCTYPE html>
<html lang="en">
<head>
<!-- META -->
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Motor Drive | Automotive Solutions Group | Teledyne LeCroy</title>
  <meta name="description" content="Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analyzers have the right tools, right measurements.">
<!-- OG META -->
  <meta property="og:title" content="Teledyne LeCroy Automotive Solutions Group - A Measured Approach to the Connected Car."/>
  <meta property="og:image" content="http://teledynelecroy.com/automotive/img/og-motor-drive.jpg"/>
  <meta property="og:url" content="http://teledynelecroy.com/automotive/motor-drive/"/>
  <meta property="og:description" content="Our tools and services provide comprehensive and complete testing capability for all motor drive control, inverter subsection, and system level operations."/>

<!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
</head>

<body>

<!-- Navigation -->  
<div class="container">
  <nav>
    <div class="nav-wrapper white">
      <a href="/" class="brand-logo"><img src="../img/tl_weblogo_blkblue_189x30.png"></a>
      <ul class="right hide-on-med-and-down">
        <li>
          <div class="valign-wrapper">
            <a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true"><img src="../img/car-thumb.png" height="43px" class="responsive-img" alt="Automotive Solutions Group"></a>
          </div>
        </li>
      </ul>
      <ul class="left hide-on-med-and-down" style="margin-left: 200px; margin-top:10px;">
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdownp" data-beloworigin="true">Products</a></li>
        <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdowns" data-beloworigin="true">Serial Data</a></li>
        <li class="dropdown"><a class="dropdown-button" href="/support/" data-activates="dropdownu" data-beloworigin="true">Support</a></li>
        <li><a href="/support/">Buy</a></li>
      </ul>
    </div>
  </nav>
</div>

<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="http://www.fte.com">Frontline</a></li>
    <li><a href="http://teledynelecroy.com/protocolanalyzer/">Protocol Solutions Group</a></li>
    <li><a href="http://www.quantumdata.com/">quantumdata</a></li>
    <li><a href="http://teledynelecroy.com/oscilloscope/">Oscilloscopes</a></li>
    <li><a href="http://www.teledyne-ts.com">Torque Test</a></li>
</ul>
<ul id="dropdownp" class="dropdown-content">
  <li><a href="/oscilloscope/">Oscilloscopes</a></li>
  <li><a href="/probes/">Oscilloscope Probes</a></li>
  <li><a href="/protocolanalyzer/">Protocol Analyzers</a></li>
  <li><a href="/pert3/pertphoenix.aspx">PeRT</a></li>
  <li><a href="/waveformgenerators/">Waveform Generators</a></li>
  <li><a href="/logicstudio/">Logic Analyzers</a></li>
  <li><a href="/optical-modulation-analyzer/">Optical Modulation Analyzer</a></li>
  <li><a href="/sistudio/">Signal Integrity Studio</a></li>
</ul>
<ul id="dropdowns" class="dropdown-content">
  <li><a href="/serialdata/">Serial Data Standards</a></li>
  <li><a href="/sdaiii/">Serial Data Analysis</a></li>
</ul>
<ul id="dropdownu" class="dropdown-content">
  <li><a href="/support/techlib/">Tech Library</a></li>
  <li><a href="/support/service.aspx">Instrument Service</a></li>
  <li><a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a></li>
  <li><a href="/support/dsosecurity.aspx">Oscilloscope Security</a></li>
  <li><a href="/support/register/">Product Registration</a></li>
  <li><a href="/support/softwaredownload/">Software Downloads</a></li>
  <li><a href="/support/techhelp/">Technical Help</a></li>
  <li><a href="/support/requestinfo/">Request</a></li>
  <li><a href="/support/rohs.aspx">RoHS &amp; WEEE</a></li>
  <li><a href="/events/">Events &amp; Training</a></li>
  <li><a href="/support/training.aspx">Training</a></li>
  <li><a href="/support/user/userregisterform.aspx?update=y">Update Profile</a></li>
  <li><a href="/support/contact/">Contact Us</a></li>
</ul>
<!--end of dropwdowns -->
<!-- End Navigation -->

<!-- Main section -->
<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m12 l12">
        <h1>Motor Drive</h1>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l6">
        <p class="flow-text">Electric/Hybrid propulsion systems and electrification of other non-propulsion vehicle motors are requiring large investments. Automotive OEMs and suppliers seek to achieve optimized efficiency and control characteristics for all vehicle motors. Therefore, power electronics converters, inverters and drives are used in new vehicles.</p>
        <p class="flow-text">Power electronics systems utilize power semiconductors in various half-, full (H)-, or cascaded H-bridge designs to provide optimized control and efficiency of propulsion and other motors. Complex Vector FOC or direct torque control systems, including those that monitor and control power flow based on analysis of a single device switching period, are used. Motor speed and torque feedback signals provide important information for control and power flow. All inverter subsection, control system, motor feedback, and system level devices need extensive validation, often under complex dynamic conditions.</p>
      </div>
      <div class="col s12 m12 l6">
          <img class="responsive-img" src="../img/motordrive-main.png">
      </div>
    </div>
  </div>
</div>
<!-- End Main section -->
<!-- Secondary section -->
<div class="wrap3">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l6">
           <h3>Common Motor Drive Issues:</h3>
           <p class="flow-text">
            <ul>
              <li style="list-style-type:circle;margin-left:20px;">Dynamic motor/drive operation not well understood</li>
              <li style="list-style-type:circle;margin-left:20px;">Control system operation not well correlated to drive behaviors</li>
              <li style="list-style-type:circle;margin-left:20px;">System level debug and validation taking too much time</li>
              <li style="list-style-type:circle;margin-left:20px;">Inability of R+D engineers to validate system operation at their bench</li>
              <li style="list-style-type:circle;margin-left:20px;">“Static” power analysis providing an incomplete picture of “dynamic” operations</li>
            </ul>
          </p>
        </div>

        <div class="col s12 m12 l6 z-depth-4 blue-grey darken-1" style="padding:10px;">
          <h3>Common Use Cases</h3>
          <ul>
            <li style="list-style-type:circle;margin-left:20px;">Peak delivered per-cycle power needed to be understood for a windshield wiper motor drive application for better motor/drive design to real-world snow loads and frozen to glass conditions.</li>
            <li style="list-style-type:circle;margin-left:20px;">A headlight system needed serial data control packets correlated to headlight movement and position.</li>
            <li style="list-style-type:circle;margin-left:20px;">An electric steering system needed to understand dynamic drive behavior and correlate activity to a KMZ60 angle sensor</li>
            <li style="list-style-type:circle;margin-left:20px;">Electric motor turbocharger boost system test</li>
            <li style="list-style-type:circle;margin-left:20px;">Propulsion system dynamic power flow during regenerative braking conditions.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Secondary section -->

<!-- Tertiary section -->
<div class="wrap2">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
           <h3>The Teledyne LeCroy Solution</h3>
        </div>
      </div>
      <div class="row">
        <div class="col s12 m12 l6">
          <p class="flow-text">The Motor Drive Analyzer (MDA) provides comprehensive and complete test capability for all control, inverter subsection, and system level operations. The MDA is based on the Teledyne LeCroy’s 8 channel, 12 bit, 1 GHz oscilloscope platform, and also incorporates single and three-phase electrical and mechanical power analysis. It uniquely measures and graphically displays dynamic power behaviors.</p>
          <p class="flow-text">It is one test instrument to correlate all control, inverter subsection and motor mechanical behaviors in order to better understand, validate, and debug complex dynamic behaviors.</p>
        </div>
        <div class="col s12 m12 l6">
          <img class="responsive-img" src="../img/motordrive-mda-sub.png">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Tertiary section -->

<!-- Address Block -->
<div class="clearfix"></div>
<div class="wrap">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l6">
          <div class="google-maps" ><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2942.057740667409!2d-83.4350577845396!3d42.490325079178334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824afcff87b0001%3A0xe6e14e2b36e8e3a7!2s27007+Hills+Tech+Ct%2C+Farmington+Hills%2C+MI+48331!5e0!3m2!1sen!2sus!4v1476976339141" width="600" height="350" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
            <!-- <img src="img/tl-auto-solutions-brochure.jpg" class="responsive-img" style="padding-top:27px;" /> -->
        </div>
        <div class="col s12 m12 l6">
          <h3 class="maui-blue center"><b>Automotive Technology Center</b></h3>
          <p> The Teledyne LeCroy Automotive Technology Center is the premier Bluetooth device testing and services partner, with an enormous device library comprising 3000+ devices including coverage for every major car model.</p>
          <div class="row">
            <div class="col s12 m8">
              <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                  <p>Teledyne LeCroy Automotive Technology Center<br />
                  27007 Hills Tech Court<br />
                  Farmington Hills, Michigan 48331</p>
                  <ul class="collapsible" data-collapsible="accordion">
                    <li>
                    <div class="collapsible-header light-blue accent-4 white-text"><i class="material-icons left">email</i>Contact Us</div>
                    <div class="collapsible-body"><iframe src="http://go.teledynelecroy.com/l/48392/2016-10-19/4dsgck" height="350" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe></div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Address -->




<!-- Simple footer -->
<footer class="page-footer white">
  <div class="container center social">
    <p>
      <a href="http://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'http://facebook.com/teledynelecroy');">
        <img src="http://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'http://twitter.com/teledynelecroy');">
        <img src="http://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'http://youtube.com/lecroycorp');">
        <img src="http://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://blog.teledynelecroy.com/');">
        <img src="http://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="http://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'http://www.linkedin.com/company/10007');">
        <img src="http://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2017&nbsp;Teledyne LeCroy</p>
    <a target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |
    <a target="_blank" href="/sitemap/">Site Map</a>
    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
  </div>
</footer>
<!--End Footer -->

<!--  Scripts-->
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/init.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-13095488-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
<script src="http://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>