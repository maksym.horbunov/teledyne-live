﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.certified_pre_owned_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div>
        <h1 style="font-size:30px;font-weight:bold;margin:10px;">Certified Pre-Owned Equipment</h1>
    </div>
    <div id="categoryBottom">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                <a href="https://stores.ebay.com/teledynelecroycertifiedpreowned/"><img src="/images/certified-pre-owned.jpg" border="0" width="400px" /></a>
                    <a href="https://stores.ebay.com/teledynelecroycertifiedpreowned/"><h2>Teledyne LeCroy Certified Pre-Owned</h2></a>
                    <p>Great prices on factory refurbished equipment direct from Teledyne LeCroy.</p>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top" id="tdLiberty" runat="server" visible="false">
                    <a href="https://libertytest.com/teledyne-lecroy"><img src="/images/liberty-test-logo.png" border="0" width="250px" /></a>
                    <a href="https://libertytest.com/teledyne-lecroy"><h2>Certified Pre-Owned from Liberty Test Equipment</h2></a>
                    <p>Teledyne LeCroy's only certified pre-owned reseller.</p>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>