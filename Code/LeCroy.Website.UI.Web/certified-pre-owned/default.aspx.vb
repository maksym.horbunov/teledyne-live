﻿Imports System
Imports LeCroy.Library.VBUtilities

Partial Class certified_pre_owned_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Certified Pre-Owned Oscilloscopes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.PRODUCT_CAPTION_ID
        Dim menuID As String

        '** MenuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        Dim menuURL As String = "&capid=" + captionID
        menuURL += "&mid=" + menuID
        Session("menuSelected") = captionID

        If Not (IsPostBack) Then
            LocalizeContent()
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub LocalizeContent()
        Dim countryCode As Int32 = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        If (countryCode = 208) Then
            tdLiberty.Visible = True
        Else
            tdLiberty.Visible = False
        End If
    End Sub
#End Region
End Class