<!--#include virtual="/include/global.asp" -->
<!-- #INCLUDE FILE="allfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

if len(Session("localeid"))=0 then Session("localeid")=1033
%>
<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body>
<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->
<table width="100%" cellpadding="0" cellspacing="0" border=0>
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2" >
			  <%if cint(Session ("localeid"))=1041 then%>
				  <!--#include virtual="/japan/menus/navi/pro.asp" --> 
			  <%end if%>
			  </td>
			  <td valign="top" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			  <form action="afterfirstregregistration.asp" method="post" id=form1 name=form1>
			<tr>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td width="90%" valign="top">
				<%'Whats New Table %>
				<table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
					<tr>
						<td valign="top" align="left" height="38">
					<%
					if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042 then
						Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
					else
						Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
					end if
					%>
                       <p>
                        <font face="Verdana,Arial,Helvetica" size=2>
                       <%if Session("localeid")=1041 then%>
							<%=LoadI18N("SLBCA0179",Session("localeid"))%>
							<br><br><%=LoadI18N("SLBCA0466",Session("localeid"))%>
						<%else%>	
							<%=LoadI18N("SLBCA0179",Session("localeid"))%>,&nbsp;<% = Session("FirstName")%>.
							<br><br><%=LoadI18N("SLBCA0466",Session("localeid"))%>
						<%end if%>
						</font>
						<br>
						<!--<font face="Verdana"><small>Thank you for your registration,&nbsp;</small></font>
						<font face="Verdana"><small><% = Session("FirstName")%>.<br></small>Now you've gained full access to all our services. When you
						return to LeCroy's site in the future you will not be required to fill out a registration
						form as long as you keep LeCroy's cookie in your computer.</font><br>-->
						<%
						
						'check if user was redirected to this page from question registaration page
						othertext=Request.QueryString("othertext")
						questionid=Request.QueryString("questionid")
												
						set rsQuestion=server.CreateObject("adodb.recordset")
						select case Session("localeid")
						case 1041
							strSQL = "SELECT * FROM AFTERREGQUESTION where DISABLED='n' and LOCALEID=1041 ORDER BY SORTID"
						case 1042
							strSQL = "SELECT * FROM AFTERREGQUESTION where DISABLED='n' and LOCALEID=1042 ORDER BY SORTID desc"
						case else
							strSQL = "SELECT * FROM AFTERREGQUESTION where DISABLED='n' and LOCALEID=1033 ORDER BY SORTID desc"	
						end select
						rsQuestion.Open strSQL,dbconnstr()
						if not rsQuestion.EOF then
							Response.Write "<br><font face=""Verdana,Arial,Helvetica"" size=2 color=DodgerBlue><b>"
							Response.write LoadI18N("SLBCA0467",Session("localeid"))
							Response.Write "</b></font>"
							'show message that user have to select answer
							if len(questionid)>0 and len(othertext)=0 then
								Response.Write "&nbsp;&nbsp;&nbsp;&nbsp;<font face=""Verdana"" size=1 color=red><b>" & LoadI18N("SLBCA0468",Session("localeid")) & "</b></font>"
							end if
							Response.Write "<br><br><table border=0 cellpadding=4 cellspacing=0 width=""40"">"
							while not rsQuestion.eof
								'Response.Write "<tr>"
								'Response.Write "<td width=20 colspan=3>&nbsp;</td>"
								'Response.Write "<td valign=""top""><nobr>"
								'if len(rsQuestion("REMARKS"))>0 then
								'Response.Write "<font face=""Verdana"" size=""1"" color=""#000000"">"
								'Response.Write  rsQuestion("REMARKS")
								'Response.Write "</font></nobr>"
								'else 
							'		Response.Write "&nbsp;"
							'	end if
							'	Response.Write "</td>"
							'	Response.Write "</tr>"
								Response.Write "<tr>"
								Response.Write "<td width=20>&nbsp;</td>"
								Response.Write "<td valign=""middle"">"
								strChecked=""
								if len(questionid)>0 then
									if isnumeric(questionid) then
										if cint(questionid)= cint(rsQuestion("QUESTIONID")) then
											strChecked="checked"
										end if
									end if
								end if
								'Response.Write "<font color=green>" & rsQuestion("QUESTIONID") & "</font>"
								Response.Write "<nobr><input type=radio name='afterreg' id='afterreg' " & strChecked & " value='" & rsQuestion("QUESTIONID") & "'>" 
								Response.Write "</td>"
								Response.Write "<td valign=""middle""><nobr>"
								Response.Write "<b><font face=""Verdana"" size=""2"" color=""#00008b"">"
								Response.Write  rsQuestion("QUESTION")
								Response.Write "</font></b>"
								Response.Write "</nobr><br>"
								Response.Write "</td>"
								Response.Write "<td valign=""top"">"
								Response.Write "<nobr><font face=""Verdana"" size=""1"" color=""#000000"">"
								Response.Write  rsQuestion("REMARKS")
								Response.Write "</font></nobr><br>"

								Response.Write "<nobr>"
								'text field
								if len(rsQuestion("TEXTBOX_YN"))>0 then
									if lcase(cstr(rsQuestion("TEXTBOX_YN")))="y" then
										Response.Write "<input type=text size=35 maxlength=100 name='q" & rsQuestion("QUESTIONID") & "' id='q" & rsQuestion("QUESTIONID") & "' style=""font-family: Arial; font-size: 8pt; font-weight: bold; border: 1 outset #6082FF"">"
										if len(questionid)>0 and len(othertext)>0 then
											if isnumeric(questionid) then
												if cint(questionid)= cint(rsQuestion("QUESTIONID")) then
													Response.Write "&nbsp;<font face=""Verdana"" size=1 color=red><b>" & LoadI18N("SLBCA0469",Session("localeid")) & "</b></font>"
												end if
											end if
										end if
									else
										Response.Write "&nbsp;"
									end if
								else
									Response.Write "<br>"
								end if
								Response.Write "</nobr></td>"
								Response.Write "</tr>"

								rsQuestion.movenext
							wend
							Response.Write "</table><br>"
							'Session("AfterRegCount")=count
							'Session("AfterReg") = AfterReg

						end if
						%>
						<br><font face="Verdana,Arial,Helvetica" size=2 color="#000000"><%=LoadI18N("SLBCA0470",Session("localeid"))%></font><br><br>
  
						  <br><input type="submit" value="<%=LoadI18N("SLBCA0183",Session("localeid"))%>" id=submit1 name=submit1><br><br>
   
                            

                            </p>
   
                            

						</td>
					</tr>
					 </form>
				</table>
			  </td>
			</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html>
<script language=Javascript>
function validate()
{/*
var rLength =form1.afterreg.value;
alert("A total of " + rLength + " radiobuttons");
for(i = 0;i &lt; rLength;i++)
   {
   if(f.r1[i].checked)
     {
      alert(f.r1[i].value); 
      break;
     }
*/
}

</script>