<%@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

	if len(Session("localeid"))=0 then Session("localeid")=1033
%>
<%
Session("flgQuestion")=true
Session("sendnewreg")=true
questionid=Request.Form("afterreg")
'Response.Write Request.Form
if len(questionid)>0 then
	textfield="q" & questionid
	for each item in request.Form
		if cstr(item)=cstr(textfield) then
			if getcommentrequired(questionid) then
				flgText=true
			end if
			othertext=Request.Form(textfield)
			othertext=Replace(othertext,"'","''")
			othertext=Replace(othertext,"%","")
		end if
	next
	'Response.Write flgText
	'Response.end
	if cint(questionid)<>17 then
		if len(othertext)=0 and flgText then
			Response.Redirect "afterfirstreg.asp?questionid=" & questionid & "&othertext='null'"
		end if
	end if
else
	Response.Redirect "afterfirstreg.asp?questionid='null'"
end if
'Response.end
if len(Session("ContactID"))>0 then
	'insert data into AFTERREGANSWER table (database webdb32)
	
	set rsInsertQuestion=server.CreateObject("ADODB.Recordset")
	if len(Session("CampaignID"))>0 then
		strSQL=	"Insert into AFTERREGANSWER (CONTACTID, QUESTIONID, TEXT, DATE_ENTERED, CAMPAIGN_ID) values (" & Session("ContactID") &_
				"," & questionid & ",N'" &  othertext & "','" & Now() & "'," & Session("CampaignID")& ")"
	else
		strSQL=	"Insert into AFTERREGANSWER (CONTACTID, QUESTIONID, TEXT, DATE_ENTERED, CAMPAIGN_ID) values (" & Session("ContactID") &_
				"," & questionid & ",N'" &  othertext & "','" & Now() & "',0)"
	end if
	'Response.Write strSQL
	'Response.end
	on error resume next
	rsInsertQuestion.Open strSQL, dbconnstr()
	
	'send email
			'Create E-Mail Body
		strCRet = chr(13) & chr(10)
		
		' Client data
		strBody= "How did you hear about LeCroy?"
		strBody = strBody & strCRet & strCRet & CustomerInfo	 
		strBody = strBody & strCRet & strCRet
		strBody = strBody & LoadI18N("SLBCA0467",Session("localeid")) & strCRet 
		strBody = strBody & "QUESTION: " & getquestion(questionid) & strCRet 
		strBody = strBody & "TEXT: "
		if questionid=0 then
			strBody=strBody & "Other (" & othertext & ")"
		else
			strBody=strBody & othertext
			Session("Source")=othertext
		end if
		if len(Session("Country"))>0 then
			if cstr(Session("Country"))="Japan" then
				'strCRet=chr(13)&chr(10)
				'strBody = "How did you hear about LeCroy?"  & strCRet & strCRet &  EmailBodyClientData
				'strBody = strBody & "Click the link below to view detail:"& strCRet& strCRet
				'strBody = strBody & ">> http://ny-wwwtest/ja.asp?c=" & Session("ContactID")
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "contact.jp@teledynelecroy.com"
							.BCC="kate.kaplan@teledynelecroy.com"
							.From = "webmaster@teledynelecroy.com"
							.Subject = "How did you hear about LeCroy"
							.TextBody = strBody
							.textBodyPart.Charset = "shift-JIS"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing
								
				elseif mid(Session("Country"),1,5)="Korea" then
				'strCRet=chr(13)&chr(10)
				'strBody = "How did you hear about LeCroy?"  & strCRet & strCRet &  EmailBodyClientData
				'strBody = strBody & "Click the link below to view detail:"& strCRet& strCRet
				'strBody = strBody & ">> http://ny-wwwtest/ja.asp?c=" & Session("ContactID")
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "lecroy.marketing.korea@teledyne.com"
							.BCC="kate.kaplan@teledyne.com"
							.From = "webmaster@teledynelecroy.com"
							.Subject = "How did you hear about LeCroy"
							.TextBody = strBody
							.textBodyPart.Charset = "korean"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing
								
			end if
		end if

end if
if len(Session("redirectTo"))>0 then
	Response.Redirect Session("redirectTo")
else
	if Session("localeid") = 1041 then  
		if len(Session("regcampaign"))>0 then
			Response.Redirect "/cgi-bin/profile/JapanGifts.asp"
		else
			Response.Redirect "/japan/default.asp"
		end if 
	elseif Session("localeid") = 1042 then  
		Response.Redirect "/korea/default.asp"
	else
		Response.Redirect "/default.asp"
	end if
end if


function getanswer(questionid)
if len(Session("ContactID"))>0 then
	set rsQuestion=server.CreateObject("ADODB.recordset")
	strSQL="select TEXT from AFTERREGQUESTION where QUESTIONID=" & questionid & " CONTACTID=" & Session("ContactID")
	rsQuestion.open strSQL, dbconnstr()
	if not rsQuestion.EOF then
		getanswer=rsQuestion("TEXT")
	end if
end if
end function
function getcommentrequired(questionid)
	set rsComment=server.CreateObject("ADODB.recordset")
	strSQL="select * from AFTERREGQUESTION where QUESTIONID=" & questionid
	Response.Write strSQL
	rsComment.open strSQL, dbconnstr()
	if not rsComment.EOF then
		if len(rsComment("TEXTBOX_YN"))>0 then
			if cstr(rsComment("TEXTBOX_YN"))="y" then
				getcommentrequired=true
			else
				getcommentrequired=false
			end if
		else
			getcommentrequired=false
		end if
	else
		getcommentrequired=false
	end if
end function
function getquestion(questionid)
	set rsComment=server.CreateObject("ADODB.recordset")
	strSQL="select * from AFTERREGQUESTION where QUESTIONID=" & questionid
	rsComment.open strSQL, dbconnstr()
	if not rsComment.EOF then
		if len(rsComment("QUESTION"))>0 then
				getquestion=rsComment("QUESTION")
		end if
	end if
end function
%>

