<!-- #INCLUDE FILE="regFormVariables.asp" -->
<!-- #INCLUDE FILE="allfunctions.asp" -->
<%
MissingFields = CheckAllReqFields()
'Check if there are both State and OtherState -- IS NOT ALLOWED
'If (Len(Trim(State))>0) and (Len(Trim(OtherState))>0) then
'<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" --> 
'<strong><small>Uh-oh! You entered both an US state and another state. Please go back and change your selection.</small></strong><br>
'<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> %>

<% ' Check state in US and Country US
'Elseif (Len(Trim(State)) and (Country) <> "United States")  then 
'<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" --> 
'<strong><small>Uh-oh! You entered an US state but your country is different than USA. Please go back and modify.</small></strong><br>
'<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> %>

<% ' Check state not US and Country US
'Elseif (Len(OtherState) and (Country) = "United States")  then 
'<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" --> 
'<strong><small>Uh-oh! You entered United States as the country but the state is different than a US state. Please go back and modify.</small></strong><br>
'<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> %>

<% ' Check for any Missing fields
if Len (CheckAllReqFields) > 0 then %>
<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" --> 
<% if Session("UserFrom") = "Japan" then %>
<strong><small>&#12362;&#12387;&#12392;�! &#24517;&#35201;&#12394;&#12501;&#12451;&#12540;&#12523;&#12489;&#12408;&#12398;&#35352;&#20837;&#12364;&#23436;&#20102;&#12375;&#12390;&#12356;&#12414;&#12379;&#12435;&#12290;
&#20197;&#19979;&#12398;&#35352;&#20837;&#28431;&#12428;&#12398;&#12501;&#12451;&#12540;&#12523;&#12489;&#12395;&#12372;&#35352;&#20837;&#12367;&#12384;&#12373;&#12356;&#12290;</small></strong><br>
<font color = "Red"><% = MissingFields %>.</font>
<% else %>
<strong><small>
Uh-oh! You skipped some fields back there. Please complete the following missing fields: 
<font color = "Red"><% = MissingFields %>.</font></small></strong><br>
<% end if %>
<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> 

<% 'Check if Password entered is correct
Elseif Password <> ConfirmPassword then %>
	<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" -->
<strong><small><%=LoadI18N("SLBCA0173",Session("localeid"))%></small></strong><br>
<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> 
<% 
Elseif Len(Password) < 5 or Len(Password) > 15  then %>
	<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" -->
<strong><small><%=LoadI18N("SLBCA0174",Session("localeid"))%></small></strong><br>
<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> 
<%
Else
	'Verify Uniqueness of Username & Password
	UIDandPWD = CheckPWDNotExistance(Username, Password)
	If Ucase(UIDandPWD) = "FALSE" then %>
	<!-- #INCLUDE FILE="ConfirmationFirstPart.asp" -->
<% if Session("UserFrom") = "Japan" then %>
<strong><small>&#12362;&#12387;&#12392;�! &#20182;&#12398;&#12518;&#12540;&#12470;&#12540;&#21517;&#12434;&#12362;&#36984;&#12403;&#12367;&#12384;&#12373;&#12356;&#12290;c</small></strong><br>
<% else %>
<strong><small><%=LoadI18N("SLBCA0175",Session("localeid"))%></small></strong><br>
<% end if %>
<!-- #INCLUDE FILE="ConfirmationSecondPart.asp" --> 
<% Else
		'Everything is O.K, go and save the contact information
		' If there is from other country
		If Len(Trim(State)) = 0 Then
			State = OtherState
		End If	
		GUID = GeneratedGUID()
		Session("GUID") = GUID
	
	'send new registration notification email if customer's company
	'doesn't exist in company lookup table

	'	if len(Session("Country"))>0 then
			'if cstr(Session("Country"))="United States" and len(Session("Company"))>0 then
				'check if company isn't in tblCompanylookup
			'	if not checkcompanyexists(trim(Session("Company"))) then
					'strBody= "New registration:"
					'strBody = strBody & strCRet & strCRet & CustomerInfo 
					'strBody = strBody & strCRet & strCRet
					'Set oMail = Server.CreateObject("CDO.Message")
					'oMail.To ="WebReg@teledynelecroy.com"
					'oMail.From = "webmaster@teledynelecroy.com"
					'oMail.BCC = "kate.kaplan@teledynelecroy.com"
					'oMail.Subject =Session("Company")
					'oMail.TextBody = strBody
					'oMail.Send 
					'Set oMail = Nothing ' Release the object
				'end if
			'end if
		'end if
		'+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'if contac info had been transfered from www.catc.com
		'update flag on the CONTACT_PSG table
		if len(Session("PSGProfile"))>0 then
			if cstr(Session("PSGProfile"))="y" then
				SetTransferTime(email)
			end if
		end if
		
		'+++++++++++++++++++++++++++++++++++++++++++++++++++++++
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		'if contac info had been transfered from www.lecroy.co.kr
		'update flag on the CONTACT_PSG table
		if len(Session("KRProfile"))>0 then
			if cstr(Session("KRProfile"))="y" then
				SetTransferTimeKr(email)
			end if
		end if
		
		'+++++++++++++++++++++++++++++++++++++++++++++++++++++++

		Session("ContactID") = InsertContact(GUID, Honorific, FirstName, LastName, Title, JobFunction, Company,Department, Address, Address2, City, State, Zip, Country, Phone, Fax, Email, URL, Applications, Username, Password)
		Response.Cookies("LCDATA")("GUID") = GUID
		Response.Cookies("LCDATA").Expires = Date + 365 * 5 
		Response.Redirect "Confirmed.asp"
	End if
End if %>
