<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy Web Site Registration" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="registration, web registration, register, profile, LeCroy registration, LeCroy, Digital Oscilloscopes, LeCroy, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

'**************Added by YK 1/2/2002	*************
if len(Session("localeid"))=o then Session("localeid")=1033
localeid=Session("localeid")
'************************************************
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<a NAME="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2" >
			  <%if cint(Session ("localeid"))=1041 then%>
				  <!--#include virtual="/japan/menus/navi/pro.asp" --> 
			  <%end if%>
			  </td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top"><br>
					
			  </td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td align="right" valign="top">
					<br>
			  </td>
			</tr>
			<tr>
			  <td valign="top"></td>
			  <td width="90%" valign="top">
				<%'Whats New Table 
				Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" align=""left"" height=""38"">"
				if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042  then
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """ >"
				else
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """ >"
				end if
                %>    
                 <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="1">
                            