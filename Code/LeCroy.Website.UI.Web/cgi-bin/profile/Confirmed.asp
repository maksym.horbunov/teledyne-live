<!-- #INCLUDE FILE="allfunctions.asp" -->
<!-- #INCLUDE FILE="newfunctions.asp" -->
<%'check if user's subscribed to receive via email about LeCroy's products and sevices
ValidateUser()
if cint(Session("SelectedSubCount"))>0 then
	call InsertSubscription()
end if
if CustomerFromCampaign(Session("CampaignID")) then
	Response.Redirect "/goto/CANbus/confirmation.asp"
end if

strCRet=chr(13)& chr(10)
if len(Session("localeid"))=0 then Session("localeid")=1033
%>
<%'send new registration notification email if customer's company
	'doesn't exist in company lookup table
		if len(Session("Country"))>0 then
			if cstr(Session("Country"))="United States" and len(Session("Company"))>0 then
				'check if company isn't in tblCompanylookup
				if not checkcompanyexists(trim(Session("Company"))) then
					'strBody= "New registration:"
					'strBody = strBody & strCRet & strCRet & CustomerInfo 
					'strBody = strBody & strCRet & strCRet
					'Set oMail = Server.CreateObject("CDO.Message")
					'oMail.To ="WebReg@teledynelecroy.com"
					'oMail.From = "webmaster@teledynelecroy.com"
					'oMail.BCC = "kate.kaplan@teledynelecroy.com"
					'oMail.Subject =Session("Company")
					'oMail.TextBody = strBody
					'oMail.Send 
					'Set oMail = Nothing ' Release the object
				end if
			elseif mid(Session("Country"),1,5)="Korea"  then
				strCRet=chr(13)&chr(10)
				strBody = "New web registration:"  & strCRet & strCRet &  CustomerInfo
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "lecroy.marketing.korea@teledyne.com"
							.BCC="kate.kaplan@teledyne.com"
							.From = "webmaster@teledynelecroy.com"
							.Subject = "New web registration"
							.TextBody = strBody
							.textBodyPart.Charset = "korean"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing		
			elseif cstr(Session("Country"))="Japan" then
				'Session("localeid")=1041
				'***************************************************
				'send email about user registration to contact.jp@teledynelecroy.com
				'***************************************************
				strCRet=chr(13)&chr(10)
				strBody = "New web registration:"  & strCRet & strCRet &  CustomerInfo
				'strBody = strBody & "Click the link below to view detail:"& strCRet& strCRet
				'strBody = strBody & ">> http://ny-wwwtest/ja.asp?creg=" & Session("ContactID")
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "contact.jp@teledynelecroy.com"
							.BCC="kate.kaplan@teledynelecroy.com"
							.From = "webmaster@teledynelecroy.com"
							.Subject = "New web registration"
							.TextBody = strBody
							.textBodyPart.Charset = "shift-JIS"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing				
				'***************************************************
				'send confirmation email about the registration to the customer
				'***************************************************					
				strBody=  Session("LastName")&  " " &  Session("Firstname") & " " & LoadI18N("SLBCA0160",Session("localeid"))& strCRet & strCRet
				strBody = strBody & LoadI18N("SLBCA0448",Session("localeid"))& strCRet& strCRet
				strBody = strBody & Session("Country")& strCRet
				strBody = strBody & Session("Zip")& strCRet
				strBody = strBody & Session("City")&  strCRet
				strBody = strBody & Session("Address")& strCRet
				If Len(Session("Address2")) > 0 then
					strBody = strBody & Session("Address2") & strCRet
				end if
				strBody = strBody & Session("Company") & strCRet
				If Len(Session("Title")) > 0 then
					strBody = strBody & Session("Title") & strCRet
				end if
				strBody = strBody & Session("LastName") & " " &  Session("Firstname")& " " &LoadI18N("SLBCA0160",Session("localeid")) & strCRet
				strBody = strBody & Session("Phone")& strCRet
				If Len(Session("Fax")) > 0 then
					strBody = strBody & Session("Fax") & strCRet
				end if		
				strBody = strBody & Session("Email")& strCRet
				If Len(Session("URL")) > 0 then
					strBody = strBody & Session("URL") & strCRet
				end if	
				If Len(Session("Application")) > 0 then
					strBody = strBody & Session("Application") & strCRet
				end if		
				strBody = strBody & strCRet & strCRet
				strBody = strBody &"**********************************"& strCRet
				strBody = strBody &LoadI18N("SLBCA0449",Session("localeid"))& strCRet
				strBody = strBody &LoadI18N("SLBCA0450",Session("localeid"))& strCRet
				strBody = strBody &"Tel: 03-6861-9400 / Fax: 03-6861-9586"& strCRet
				strBody = strBody &"https://teledynelecroy.com/japan/"& strCRet
				strBody = strBody &"E-mail: contact.jp@teledynelecroy.com"& strCRet
				strBody = strBody &"**********************************"& strCRet& strCRet
				'Response.Write strBOdy
				Set oNewReg = Server.CreateObject("CDO.Message")
				oNewReg.To =Session("email")
				oNewReg.From ="webmaster@teledynelecroy.com"
				oNewReg.BCC = "kate.kaplan@teledynelecroy.com"
				oNewReg.Subject ="LeCroy Website Registration Confirmation"
				oNewReg.TextBody = strBody
				oNewReg.textBodyPart.Charset = "shift-JIS"
				oNewReg.Send 
				Set oNewReg = Nothing ' Release the object				
			end if
		end if
'Redirect to "How did you hear about LeCroy page

if not Session("sendnewreg") and len(Session("TempSeminarID"))=0 then
	Response.Redirect "AfterFirstReg.asp"
end if
If Len (session("RedirectTo")) = 0 Then
	if Session("localeid")=1041 then
		session("RedirectTo")="/japan/default.asp"
	elseif Session("localeid")=1042 then
		session("RedirectTo")="/Korea/default.asp" 
	elseif Session("localeid")=1040 then
		session("RedirectTo")="/italy/default.asp"
	elseif Session("localeid")=1036 then
		session("RedirectTo")="/france/default.asp" 
	elseif Session("localeid")=1031 then
		session("RedirectTo")="/germany/default.asp" 
	else
		session("RedirectTo")="/default.asp" 
	end if
end if
If Len (session("RedirectTo")) > 0  Then%>
<!--#include file="ConfirmationFirstPart.asp" -->
<%if cint(Session("localeid"))=1041 then%>
<font face="Verdana,arial" size="2">
	<%=LoadI18N("SLBCA0179",Session("localeid"))%>
</font><font face="Verdana,arial" size="2">
	<% = Session("LastName")%>
	&nbsp;<% = LoadI18N("SLBCA0160",Session("localeid"))%>&nbsp;<%=LoadI18N("SLBCA0180",Session("localeid"))%>&nbsp;<%=LoadI18N("SLBCA0181",Session("localeid"))%>&nbsp;</font>
<p><font face="Verdana,arial" size="2">
		<%=LoadI18N("SLBCA0182",Session("localeid"))%>
	</font>
	<%else%>
	<font face="Verdana"><small>
			<%=LoadI18N("SLBCA0179",Session("localeid"))%>
		</small></font><font face="Verdana"><small>
			<% = Session("FirstName")%>
			,
			<%=LoadI18N("SLBCA0180",Session("localeid"))%>
			.
			<%=LoadI18N("SLBCA0181",Session("localeid"))%>
			.</small></font>
<p><font face="Verdana"><small><%=LoadI18N("SLBCA0182",Session("localeid"))%>.</small></font>
	<%end if%>
	<form action="<% = Session("RedirectTo")%>" method="post" id=form1 name=form1>
		<p><input type="submit" value="<%=LoadI18N("SLBCA0183",Session("localeid"))%>" id=submit1 name=submit1></p>
	</form>
	<!--#include file="ConfirmationSecondPart.asp" -->
	<%Else%>
	<!--#include file="ConfirmationFirstPart.asp" -->
	<!--#INCLUDE FILE="CookieNotAccepted.asp" -->
	<form action="senttoreg.asp" method="POST" id="form2" name="form2">
		<p><input type="submit" value="<%=LoadI18N("SLBCA0170",Session("localeid"))%>" id=submit2 name=submit2></p>
	</form>
	<!--#include file="ConfirmationSecondPart.asp" -->
	<% End If%>