<%'@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="cgi-bin/profile/allfunctions.asp"-->
<%Session("PropProdCategory")=0%>

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	contry_id=208
	topic_id=1
	division_id=1
	'**************Added by YK 3/1/2006	*************
	localeid=Request.QueryString("lid")
	Session("localeid")=localeid
	if len(Session("localeid"))=0 then Session("localeid")=1033
	localeid=Session("localeid")
	'************************************************
%>

<html>
<head>
<script LANGUAGE="JavaScript">
		function isReady(form) {
			if (form.EMail.value!="") return true;
			else {
			alert("Please type your E-Mail Address");
			form.EMail.focus( );
			return false;
			}
		}	
	</script>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>

<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->
<table width="100%" cellpadding="0" cellspacing="0" border=0 ID="Table1">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table2">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2"  >
				  <%if Session("localeid")=1041 then%>
		  					<!--#include virtual="/japan/menus/navi/pro.asp" --> 
					<%end if%>
			  </td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top">&nbsp; </td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td width="10" valign="top">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td width="90%" valign="top">
				<%'Whats New Table %>
				<table border="0" width="480" cellspacing="0" cellpadding="0" height="108" ID="Table3">
					<tr>
						<td valign="top" height="38">
				<%		
				if Session("localeid")=1033 then
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
				else
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
				end if
				%>      
				<p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="2">
				<% 
				LinkCookie = Request.QueryString("c")
				LinkContactID=Request.QueryString("cid")
				flgValid=false
				if len(LinkCookie)>0 and len(LinkContactID)>0 then
					ContactIdTemp = GetContactIDOnWebId(LinkCookie)
						If isnumeric(ContactIdTemp) and isnumeric(LinkContactID) Then
							if clng(ContactIdTemp)=clng(LinkContactID) then
								flgValid=true
							end if
						end if
					else
				end if
				if flgValid then
					GetUserData(LinkCookie)
					Response.CookiesRequest.Cookies("LCDATA")("GUID") = LinkCookie
					Response.Cookies("LCDATA").Expires = Date + 365 * 5
					sAgent = Request.ServerVariables("HTTP_USER_AGENT")
					Set rs = Server.CreateObject("ADODB.Recordset")
					set cmdSendPassword = createobject("ADODB.Command")
					cmdSendPassword.ActiveConnection = dbconnstr()
					cmdSendPassword.CommandText = "sp_emailusernamepassword"
					cmdSendPassword.CommandType =4	' 4 = stored procedure
					set ParamEmail=cmdSendPassword.CreateParameter("@EMAIL",200,1,120)
					cmdSendPassword.Parameters.Append ParamEmail
					cmdSendPassword.Parameters("@EMAIL")= Session("email")
					set rs=	cmdSendPassword.Execute
					Response.Write LoadI18N("SLBCA0236",localeID) & "<br><br>"
					Response.Write LoadI18N("SLBCA0238",localeID) & " " & rs("USERNAME")& "<br><br>"
					Response.Write LoadI18N("SLBCA0239",localeID) & " " & rs("PASSWORD")& "<br><br>"
					%>
					<form action="senttoreg.asp" method="post" id=form1 name=form1>
						<p><input type="submit" value="<%=LoadI18N("SLBCA0235",Session("localeid"))%>" id=submit1 name=submit1></p>
					</form>
	
				<%	
				else
					Response.Redirect "RegisterAgain.asp"
				end if
				%>
                  </font></p>
                <p class="SmallBody" align="left">&nbsp;</p>
				</td>
					</tr>
					<tr>
						<td valign="top" height="70">
						</td>
					</tr>
				</table>
			  </td>
			  <td>&nbsp;</td>
			  <td valign="top" align="right">&nbsp;</td>
			</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html>


