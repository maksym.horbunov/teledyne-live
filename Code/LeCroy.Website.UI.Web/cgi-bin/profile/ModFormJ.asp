<%@ Language=VBScript %>
<!-- #INCLUDE FILE="allfunctions.asp" -->
<!-- #INCLUDE FILE="regFormVariables.asp" -->


<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="E-profile" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="ja" ' Sets the language of the current page
	country_id=100
	division_id=1
	topic_id=1
	
'**************Added by KK 4/7/2003	*************
 Session("localeid")=1041
 Session("Country")="Japan"
localeid=Session("localeid")
'************************************************

if len(Request.QueryString("si")) > 0 then
    Session("RedirectTo") = "/japan/seminar/si-seminar/si-form.asp"
end if
if len(Request.QueryString("w12")) > 0 then
    Session("RedirectTo") = "/japan/web2012/web-form.asp"
end if

Response.Redirect "transfersession.asp"	
%>


