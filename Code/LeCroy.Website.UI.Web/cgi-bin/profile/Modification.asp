<!--#INCLUDE FILE="allfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<% srcfrom = Request.Form("srcfrom") %>

<%

if len(Request.Form("Honorific"))>0 then
	Honorific = Request.Form("Honorific")
else
	Honorific = ""
end if
 FirstName = Request.Form("FirstName")
 LastName = Request.Form("LastName")
 Title = Request.Form("Title")
 Company = Request.Form("Company")
 Address = Request.Form("Address")
 Address2 = Request.Form("Address2")
 City = Request.Form("City")
 if len(Request.Form("State"))>0 then
	State = Request.Form("State")
 else
	State =""
 end if
 if len(Request.Form("OtherState"))>0 then
	OtherState = Request.Form("OtherState")
 else
	OtherState = ""
 end if
 Zip = Request.Form("Zip")
 Country = Request.Form("Country")
 Phone = Request.Form("Phone")
 Fax = Request.Form("Fax")
 Email = Request.Form("Email")
 URL = Request.Form("URL")
 Applications = Request.Form("Applications") 
 Department=Request.Form("Department") 
 Comments=Request.Form("Comments") 
 SubFormat=Request.Form("subformat")
 if len(SubFormat)>0 then
    Session("subformat")=SubFormat
 else
    Session("subformat")=2
 end if
JobFunction = Request.Form("Job_Function")
%>
 
<% MissingFields = CheckAllReqFieldsForModification()%>
	<% ' Check for any Missing fields
	if Len (MissingFields) > 0 then %>
	<!-- #INCLUDE FILE="ModificationFirstPart.asp" --> 
	<strong><small>
	Uh-oh! You skipped some fields back there. Please complete the following missing fields:<br> 
	<font color = "Red"><% = MissingFields %>.</font></small></strong><br>
	<!-- #INCLUDE FILE="ModificationSecondPart.asp" --> 
	<% Else
			'Everything is O.K, go and update the contact information
			' If there is from other country
			If Len(State) = 0 Then
				State = OtherState
			End If %>	
			<% IDContact = UpdateContact(Honorific, FirstName, LastName, Title, JobFunction, Company, Department,Address, Address2, City, State, Zip, Country, Phone, Fax, Email, URL, Applications, Session("ContactId"))
			'Assign new data to the Session variables
			' Redirect to:
			Session("Honorific") = Honorific
			Session("FirstName") = FirstName
			Session("LastName") = LastName
			Session("Title") = Title
			Session("Company") = Company
			Session("Address") = Address
			Session("Address2") = Address2
			Session("City") = City
			Session("State") = State
			Session("Zip") = Zip
			Session("Country") = Country
			Session("Phone") = Phone
			Session("Fax") = Fax
			Session("Email") = Email
			Session("URL") = URL
			Session("Applications") = Applications
			Session("Signature")=Request.form("sign")
			Session("subformat")=SubFormat
			Session("department")=department
			Session("comments")=comments
			Session("JobFunction")=JobFunction
 
			'YK 5/3/2002 Update members table (forum db)
			'set rsUpdateSign=server.CreateObject("adodb.Recordset")
			'strSQL="Update members set M_sig='" & Session("signature") & "' where M_Contact_ID=" & Session("ContactID")
			'rsUpdateSign.LockType=3
			'rsUpdateSign.Open strSQL, dbconnstr("forum")
			
			'check which subscriptions were selected
			sub_all=Session("SubTypeID")
			for i=0 to (Session("SubCount")-1)
																				
				'Response.Write "<br>" & sub_all(i) & "<br>" & Request.form(cstr("subscription" & sub_all(i))) & "<br>"

				if len(Request.form(cstr("subscription" & sub_all(i))))>0 then
					call UpdateSubscription(checksubscriptionexist(sub_all(i)),"on",sub_all(i))
				else
					call UpdateSubscription(checksubscriptionexist(sub_all(i)),"",sub_all(i))
				end if
			next
			
			
			if srcfrom="Japan" then
				Response.Redirect "Modified.asp?srcfrom=Japan"
			else
				Response.Redirect "Modified.asp"
			end if
		End if %>
