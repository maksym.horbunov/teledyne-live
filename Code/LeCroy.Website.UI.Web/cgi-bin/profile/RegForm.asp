<%@ Language=VBScript %>
<!--#include virtual="cgi-bin/profile/allfunctions.asp"-->
<!--#INCLUDE FILE="regFormVariables.asp" -->
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->

<%	'*********>*****Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy Registration Page" ' Title of Web Page
	PageDescription="In this page you can register to become a LeCroy Registered User." 'Description of Page
	PageKeywords="Registration Page, registration" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="E-profile" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
'***************Select Language******************
'**************Added by KK 7/27/2004*************
	Session("LanguagePage")="/cgi-bin/profile/regform.asp"
	'check language has been changed by user
	strLang=Request.QueryString("localeid")
	if len(strLang)>0 then
		Session("localeid")=cint(strLang)
	end if
'************************************************
'**************Added by KK 8/2/2001	*************
if len(Session("localeid"))=0 then Session("localeid")=1033
if Session("localeid")=1041 then 
	Response.Redirect "/cgi-bin/profile/regformj.asp"
end if	
localeid=Session("localeid")
Response.Redirect "TransferSession.asp"	
'************************************************
%>
<html>
<head>
<script language="JavaScript">
function validate()
//this function redirect user to the next page if all fields of
//registration form are correct
{

    var msg;
    msg ="";
    var flg=false;
	if(replace(document.regform.FirstName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0161",localeid)%>" + "\n";
      flg=true;
      document.regform.FirstName.focus();
     }
    if(replace(document.regform.LastName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0162",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.LastName.focus();
	  }
     }
  
     if(document.regform.Job_function.value == "")
     {
     msg += "<%=LoadI18N("SLBCA0163",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Job_function.focus();
	  }
     }
    if (replace(document.regform.Company.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0078",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Company.focus();
	  }
    }
    if(replace(document.regform.Address.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0079",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Address.focus();
	  }	
    }
    if(replace(document.regform.City.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0080",localeid)%>" + "\n";
     if (flg!=true)
       {
        flg=true;
		document.regform.City.focus();
	   }	
    }
    if ((document.regform.State[document.regform.State.selectedIndex].value == "") &&(document.regform.Country[document.regform.Country.selectedIndex].value=="United States"))
	{
	 msg += "<%=LoadI18N("SLBCA0081",localeid)%>" + "\n";
     if (flg!=true)
		 {
		  flg=true;
		  document.regform.State.focus();
		 }
	}
	if(replace(document.regform.Zip.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0082",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Zip.focus();
		}	
	}
	if(document.regform.Country[document.regform.Country.selectedIndex].value == "")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Country.focus();
		}	
	}
    if(replace(document.regform.Phone.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0084",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Phone.focus();
		}	
   }
    if(replace(document.regform.Email.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0086",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Email.focus();
		}	
   }
   if(replace(document.regform.Username.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0148",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Username.focus();
		}	
	}
	if(replace(document.regform.Password.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0151",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Password.focus();
		}	
	}
	if(replace(document.regform.ConfirmPassword.value," ","")== "")
	{
	 msg += "<%=LoadI18N("SLBCA0169",localeid)%>" + " " + "<%=LoadI18N("SLBCA0151",localeid)%>" + "\n";
	if (flg!=true)
		{
		    flg=true;
			document.regform.ConfirmPassword.focus();
		}		
	}
		
    if(msg != "")
    {
      alert("<%=LoadI18N("SLBCA0172",localeid)%>" + ":"+ "\n" + "\n" + msg);
      return false;
    }
    document.regform.submit();
}
    function openTermWin(URL)
	{
		aWindow = window.open(URL,"thewindow","width=575,height=400 bgcolor=white,scrollbars=yes,resizable=yes");
	}
	function replace(string,text,by)
	 {
	// Replaces text with by in string
	    var strLength = string.length;
	    var txtLength = text.length;
	    if ((strLength == 0) || (txtLength == 0)) return string;

	    var i = string.indexOf(text);
	    if ((!i)&&(text != string.substring(0,txtLength))) return string;
	    if (i == -1) return string;

	    var newstr = string.substring(0,i) + by;

	    if (i+txtLength < strLength)
	        newstr +=replace(string.substring(i+txtLength,strLength),text,by);

	    return newstr;
	}
	</script>
<meta name="Title" content="<% = PageTitle %>">
<meta name="Description" content="<% = PageDescription %>">
<meta name="keywords" content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="/nav/images/<% = bgimage %>" <% end if %>leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<a name="TopofPage"></a>
<%if CustomerFromCampaign(Session("CampaignID")) then%>
    <table border="0" width="800" height="147" cellspacing="0" cellpadding="0" id="Table1">
      <tr>
        <td colspan="2" height="30">
        <img src="/goto/WS_Spin/images/WS_Drive_Header_T.jpg" border =0>
        </td>
      </tr>
    </table>
<%else%>
<!--#include virtual="/include/Header.asp" -->
<%end if%>
<table width="100%" cellpadding="0" cellspacing="0" border=0>
  <tr>
    <td valign="top" align=left>
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<%if CustomerFromCampaign(Session("CampaignID")) then%>
			  <td valign="top" rowspan="2">&nbsp;</td>
			  <td valign="top">&nbsp;</td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<%else%>
			  <td width="100" valign="top" rowspan="2" >
					<!--#include virtual="/shopper/selectlanguage.asp"-->
			  </td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			 <%end if%>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td width="90%" valign="top" align=left>
				<%'Whats New Table 
				Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" align=""left"" height=""38"">"
				if cint(Session("TempSeminarID"))=76 then
					Response.Write "<img border=""0"" src=""/seminars/images/Logo_LeCroy-TheMathWorks.gif"">"
				else
					if not CustomerFromCampaign(Session("CampaignID")) then
						if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042  then
							Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
						else
							Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
						end if
					end if
				end if
                %>
                            
             
     <table border="0" width="500" cellspacing="0" cellpadding="0">
	<%if len(Session("PSGProfile"))>0 then%>
     	<tr>
		<td width="500"><font face="Verdana,Arial"><small><strong>
		<%
		if cstr(Session("PSGProfile"))="y" then
			Response.Write Session("FirstName") & ", " & LoadI18N("SLBCA0607",localeid)& "<br><br>"
			Response.Write LoadI18N("SLBCA0608",localeid)
		elseif cstr(Session("KRProfile"))="y" then
			Response.Write Session("FirstName") & ", " & LoadI18N("SLBCA052",localeid)& "<br><br>"
			Response.Write LoadI18N("SLBCA0608",localeid)

		else
			Response.Write LoadI18N("SLBCA0605",localeid) & "<br><br>"
			Response.Write LoadI18N("SLBCA0606",localeid)
		end if
		%>
		<br></strong></small></font><br>
		</td>
		</tr>
     <%else%>
		<tr>
		<td width="500"><font face="Verdana,Arial"><small><strong><%=LoadI18N("SLBCA0135",localeid)%>
		<br></strong></small></font><br>
		<hr size="1" color="RoyalBlue">
		</td>
		</tr>
	<%end if%>
	</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr>
		<td>


		<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
		<td>
			<br><font face="Verdana"><small><%=LoadI18N("SLBCA0157",localeid)%> <%=LoadI18N("SLBCA0158",localeid)%></small></font>
			<br><br>
			<small><font color="red" face="Verdana"><%=LoadI18N("SLBCA0159",localeid)%></font></small><br>
		</td>
	</tr>
	<tr>
		<td width="100%">
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<form method="POST" action="confirmation.asp" id="regform" name="regform">
					<tr>
					  <td width="200" align="left"><small><font face="Verdana" color="#000000"><strong><%=LoadI18N("SLBCA0160",localeid)%>:</strong></font></small>
					  </td>
					  <td width="200"><input type="text" name="Honorific" value="<% = Session("Honorific") %>" size="5" maxlength=5>
					  </td>
					</tr>
					<tr>
					  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0161",localeid)%>:</strong></font></small>
					  </td>
					  <td width="200">
						  <input type="text" name="FirstName" value="<% = Session("FirstName") %>" size="30" maxlength="30">
					  </td>
					</tr>
					<tr>
					  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0162",localeid)%>:</strong></font></small>
					  </td>
					  <td width="200"><input type="text" name="LastName" size="30" value="<% = Session("LastName") %>" maxlength="30" >
					  </td>
					</tr>
					<tr>
					  <td width="200" align="left"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0077",localeid)%>:</strong></font></small>
					  </td>
					   <td width="200"><input type="text" name="Title" size="30" maxlength="60" value="<% = Session("Title") %>" >
					  </td>
					</tr>
					<%'Job Function Field - added by KK 3/11/2004%>
					<tr>
							<td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0163",localeid)%>:</strong></font></small>
						</td>
						<td width="200">
						<select name=Job_function>
							<% If isnull(Session("JobFunction")) or len(Session("JobFunction"))=0 Then %>
								<option value=''><%=LoadI18N("SLBCA0573",localeid)%>
                                <%end if
							set rsJobFunction=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from CONTACT_JOB_FUNCTION order by SORT_ID"
							'Response.Write strSQL
							rsJobFunction.Open strSQL, dbconnstr()
							if not rsJobFunction.EOF then
								while not rsJobFunction.EOF
								If not isnull(Session("JobFunction")) or len(Session("JobFunction"))>0 Then 
									if cstr(Session("JobFunction"))=cstr(translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))) then
										Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """ SELECTED>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									else
										Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									end if
								else
									Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
								end if
								rsJobFunction.MoveNext
								wend
							end if
							 %>		 
								</select>
							</td>
						</tr>
						<tr>	
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0078",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Company" size="30" maxlength="50" value="<% = Session("Company") %>" >
						  </td>
						</tr>
						<tr>	
						  <td width="200" align="left"><small><font face="Verdana" color="#000000"><strong><%=LoadI18N("SLBCA0465",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Department" size="30" maxlength="100" value="<% = Session("Department") %>" id="Text1">
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0079",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Address" size="40" maxlength="255" value="<% = Session("Address") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0079",localeid)%>2:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Address2" size="40" maxlength="255" value="<% = Session("Address2") %>">
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0080",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="City" size="30" maxlength="50" value="<% = Session("City") %>" >
						  </td>
						</tr>
						<tr>
							<td width="200" align="left"><small><% if localeid = 1033 then %><font face="Verdana" color="red">
							<% else%><font face="Verdana" color="black">
							<%end if%>
							<strong><%=LoadI18N("SLBCA0081",localeid)%> (<%=LoadI18N("SLBCA0164",localeid)%>):</strong></font></small>
							</td>
							<td width="200">
							<select name="State"  size="1">
	    								<% If Len (Session("State"))= 0 Then %>
											<option value=''>Select State	
										<% end if 	
							set rsState=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from STATE order by NAME"
							rsState.Open strSQL, dbconnstr()
							if not rsState.EOF then
								while not rsState.EOF
								If Len (Session("State"))>0 Then 
									if cstr(Session("State"))=cstr(rsState("NAME")) then
										Response.Write "<option VALUE=""" & rsState("NAME") & """ SELECTED>" & rsState("NAME")
									else
										Response.Write "<option VALUE=""" & rsState("NAME") & """>" & rsState("NAME")
									end if
								else
									Response.Write "<option VALUE=""" & rsState("NAME") & """>" & rsState("NAME")
								end if
								
								rsState.MoveNext
								wend
							end if%>	
								</select>
							</td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="#000000"><nobr><strong><%=LoadI18N("SLBCA0165",localeid)%> (<%=LoadI18N("SLBCA0166",localeid)%></strong></nobr></font></small><font face="Verdana" color="#000000">):</small>
						  </td>
						  <td width="200"><input type="text" name="OtherState" size="30" maxlength="50" value="<% = Session("OtherState") %>">
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0082",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Zip" size="20" maxlength="20" value="<% = Session("Zip") %>">
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0083",localeid)%>:</strong></font></small></td>
						  <td width="200">
						  <select name="Country"  size="1">
							<% If Len (Session("Country"))=0 Then %>
								<option value=''><%=LoadI18N("SLBCA0171",localeid)%>&nbsp;<%=LoadI18N("SLBCA0083",localeid)%>
                                <%end if
							set rsCountry=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from COUNTRY order by NAME"
							rsCountry.Open strSQL, dbconnstr()
							if not rsCountry.EOF then
								while not rsCountry.EOF
								If Len (Session("Country"))>0 Then 
									if cstr(Session("Country"))=cstr(rsCountry("NAME")) then
										Response.Write "<option VALUE=""" & rsCountry("NAME") & """ SELECTED>" & rsCountry("NAME")
									else
										Response.Write "<option VALUE=""" & rsCountry("NAME") & """>" & rsCountry("NAME")
									end if
								else
									Response.Write "<option VALUE=""" & rsCountry("NAME") & """>" & rsCountry("NAME")
								end if
								
								rsCountry.MoveNext
								wend
							end if
							 %>		 
								</select></td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0084",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><input type="text" name="Phone" size="30" maxlength="30" value="<% = Session("Phone") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0085",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="text" name="Fax" size="30" maxlength="30" value="<% = Session("Fax") %>" ></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0086",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="text" name="Email" size="30" maxlength="60" value="<% = Session("Email") %>" ></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0087",localeid)%>/<%=LoadI18N("SLBCA0168",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="text" name="URL" size="30" maxlength="50" value="<% = Session("URL") %>" ></font>
						  </td>
						</tr>
						<tr>
						 <td width="200" align="left"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0088",localeid)%>:</strong></font></small>
						 </td>
						  <td width="200"><font face="Verdana"><input type="text" name="Applications" size="30" maxlength="50" value="<% = Session("Applications") %>"></font>
						  </td>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0148",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="text" name="Username" size="15" maxlength="15" value="<% = Username %>"></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0151",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="password" name="Password" size="15" maxlength="15" value="<% = Password %>"></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="left"><small><font face="Verdana" color="Red"><strong><%=LoadI18N("SLBCA0169",localeid)%>&nbsp;<%=LoadI18N("SLBCA0151",localeid)%>:</strong></font></small>
						  </td>
						  <td width="200"><font face="Verdana"><input type="password" name="ConfirmPassword" size="15" maxlength="15" value="<% = ConfirmPassword %>"></font>
						  </td>
						</tr>
						<%
						'*****YK 5/3/2002
						'if user came from "/Forum/Chat/Post.asp" allow him to create
						'a signature
						
						if Session("RedirectTo") = "/Forum/Chat/Post.asp"  then
							Response.Write "<tr>"
						    Response.Write "<td width=""200""  align=""right"" valign=""top"">"
							Response.Write "<small><font face=""Verdana,Arial"" size=2 color=""#000000""><nobr><b>" & "Signature" & "</b></nobr></font></small>"
							Response.Write "</td>"
						    Response.Write "<td width=""200""  valign=""top"">"
							Response.Write "<textarea rows=3 cols=30 name=sign id=sign>" & Session("signature") & "</textarea>"
							Response.Write "</td>"
							Response.Write "</tr>"
						end if
						%>
						
						<%
						'select all enabled subscription type 
						'and show checked checkboxes
						set rsSubType=server.CreateObject("ADODB.Recordset")
						if session("localeID")=1042 then
							strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y' or DOCUMENT_ID=37"
						else
							strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y'"
						end if
						rsSubType.Open strSQL,dbconnstr()
						if not rsSubType.EOF then
							Response.Write "<tr>"
							Response.Write "<td  colspan=2 align=""left"" valign=""top""><br>"
							Response.Write "<hr size=""1""	 color=""#4169E1"" width=""80%"">"
							Response.Write "</td>"
							Response.Write "</tr>"
							Response.Write "<tr>"
							Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
							Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0382",localeid) & "</b></font>"
							Response.Write "</td>"
							Response.Write "</tr>"
							Response.Write "<tr>"
							Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
							Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
						
							while not rsSubType.EOF
								subcount=subcount+1
								Response.Write "<tr>"
								Response.Write "<td valign=""top"" width=""30"">"
								Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription" & rsSubType("DOCUMENT_ID") & "' checked>"
								Response.Write "</td>"
								Response.Write "<td valign=""middle"">"
								Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  translation_webdb32("SUBSCRIPTION_TYPE","NAME","DOCUMENT_ID",rsSubType("DOCUMENT_ID"))  & "</b></nobr></font>"
								Response.Write "</td>"
								Response.Write "</tr>"
								redim preserve SubTypeID(subcount)				
								SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")
								
							rsSubType.MoveNext
							wend
							
							Response.Write "</table>"
							Response.Write "</td>"
							Response.Write "</tr>"
							'ask customer in which format he prefer to recive an email
							'Removed by KK on 06/17/2009 - defaulted format to HTML
						'	set rsSubFormat=server.CreateObject("ADODB.Recordset")
						'	strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y'"
						'	rsSubFormat.Open strSQL,dbconnstr()
						'	if not rsSubFormat.EOF then
						'		Response.Write "<tr>"
						'		Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
						'		Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font>"
						'		Response.Write "</td>"
						'		Response.Write "</tr>"
						'		Response.Write "<tr>"
						'		Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
						'		Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
						'	
						'		while not rsSubFormat.EOF
						'			if cstr(rsSubFormat("NAME"))="Text" then
						'				strSelected="CHECKED"
						'			else
						'				strSelected=""
						'			end if
						'			Response.Write "<tr>"
						'			Response.Write "<td valign=""top"" width=""30"">"
						'			Response.Write "<input type=radio name='subformat' id='subformat' value=""" & rsSubFormat("FORMAT_ID")  & """ " & strSelected & ">"
						'			Response.Write "</td>"
						'			Response.Write "<td valign=""middle"">"
						'			Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
						'			Response.Write "</td>"
						'			Response.Write "</tr>"
						'		rsSubFormat.MoveNext
						'		wend
						'		
						'		Response.Write "</table>"
						'		Response.Write "</td>"
						'		Response.Write "</tr>"
						'	end if
						end if
						
						
							Session("SubCount")=subcount
							Session("SubTypeID") = SubTypeID						
						
						%>
						
							<tr>
							<td align="left" colspan=2>
								<%
								'if localeid="1036" then
								%>
                                <br>
								<%=LoadI18N("SLBCA0442",localeid)%>
								<br />
								<br />
								<!--<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="document.regform.submit();">-->
								<%'else%>
								<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="validate()">
								<%'end if%>
							</td>
						</tr>
						</form>
					</table>
				</td>
		</tr>
</table>
</td>
</tr>
</table>
                            <%' End of RegForm Code %>

						</td>

					</tr>
				</table>
			  </td>
			</tr>
<%if CustomerFromCampaign(Session("CampaignID"))then%>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><img border="0" name="holder" src="/nav/images/spacer.gif" ></td>
</tr>
</table>
</td>
</tr>
</table>
<%else%>
		<tr>
			<td >&nbsp;</td>
			<td>&nbsp;</td>
			<td><nobr><img border="0" name="holder" src="/nav/images/spacer.gif" width="300" height="25" align="absbottom"></nobr></td>
		</tr>
    </table>
</td>
</tr>
<tr>
<td>
<!--#include virtual="/images/Footer/footer.asp" -->
</td>
</tr>
</table>
<%end if%>
</body>
</html>






