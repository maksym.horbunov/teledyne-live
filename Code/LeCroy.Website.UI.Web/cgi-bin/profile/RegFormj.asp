<%@ Language=VBScript %>
<!--#include virtual="/cgi-bin/profile/allfunctions.asp"-->
<!--#INCLUDE FILE="regFormVariables.asp" -->

<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->


<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy Registration Page" ' Title of Web Page
	PageDescription="In this page you can register to become a LeCroy Registered User." 'Description of Page
	PageKeywords="Registration Page, registration" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="E-profile" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
'**************Added by KK 4/7/2003	*************
 Session("localeid")=1041
 Session("Country")="Japan"
localeid=Session("localeid")
'************************************************
Response.Redirect "TransferSession.asp"	
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>

<body <% if bgimage="" then %><% else %>background="/nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<a NAME="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0" ID="Table1">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table2">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2" bgcolor="#0099FF">
			<!--#include virtual="/japan/menus/navi/pro.asp" --> 
			  </td>
			  <td valign="top">&nbsp&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top"><br>
			  </td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td align="right" valign="top">
					<br>
			  </td>
			</tr>
			<tr>
			  <td valign="top"></td>
			  <td width="90%" valign="top">
				<%'Whats New Table 
				Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" align=""left"" height=""38"">"
				'if Session("localeid")=1033 then
				Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
				'else
				'	Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
				'end if
                %>
                            <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="1">
                            
                            <%'Start of RegForm Code%>
                             
                            <table border="0" width="500" cellspacing="0" cellpadding="0" ID="Table3">
	<tr>
		
		
		<td width="500"><font face="Verdana,Arial" size=2><b>
		<%
		if len(Session("PSGProfile"))>0 then
			if cstr(Session("PSGProfile"))="y" then
				Response.Write LoadI18N("SLBCA0607",localeid)& "<br><br>"
				Response.Write LoadI18N("SLBCA0608",localeid)
			else
				Response.Write LoadI18N("SLBCA0135",localeid) 
			end if
		else
			Response.Write LoadI18N("SLBCA0135",localeid) 
		end if
		%>
		</b>
		<br><br>
		<hr size="1" color="RoyalBlue">
			<br><%=LoadI18N("SLBCA0157",localeid)%> <%=LoadI18N("SLBCA0158",localeid)%>
			<br><br>
			<font color="red"><%=LoadI18N("SLBCA0159",localeid)%></font><br></font>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<form method="POST" action="confirmation.asp" id="regform" name="regform">
					  <input type="hidden" name="Honorific" value="" size="5" ID="Honorific">
  					  <input type="hidden" name="OtherState" value="" size="5" ID="OtherState">
				<table border="0" cellspacing="0" cellpadding="3" width="100%" ID="Table4">
					<tr>
					  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0162",localeid)%>:</nobr></font>
					  </td>
					  <td width="200"><input type="text" name="LastName" id="LastName"size="30" value="<% = Session("LastName") %>" maxlength="30" ID="Text2">
					  </td>
					</tr>
					<tr>
					  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0161",localeid)%>:</nobr></font>
					  </td>
					  <td width="200"><input type="text" name="FirstName" id="FirstName"value="<% = Session("FirstName") %>" size="30" maxlength="30" ID="Text1">
					  </td>
					</tr>

					<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0078",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="Company" size="30" maxlength="50" value="<% = Session("Company") %>" >
						  </td>
					</tr>
					<tr>
					  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0077",localeid)%></nobr></font>
					  </td>
					  <td width="200" ><input type="text" name="Title" size="30" maxlength="30" value="<% = Session("Title") %>" >
					  </td>
					</tr>
					<%'Job Function Field - added by KK 5/5/2004%>
					<tr>
							<td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0163",localeid)%>:</nobr></font>
						</td>
						<td width="200">
						<select name="Job_function" ID="Select1">
							<% If isnull(Session("JobFunction")) or len(Session("JobFunction"))=0 Then %>
								<option VALUE=''><%=LoadI18N("SLBCA0573",localeid)%>
                                <%end if
							set rsJobFunction=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from CONTACT_JOB_FUNCTION order by SORT_ID"
							rsJobFunction.Open strSQL, dbconnstr()
							if not rsJobFunction.EOF then
								while not rsJobFunction.EOF
								If not isnull(Session("JobFunction")) or len(Session("JobFunction"))>0 Then 
									if cstr(Session("JobFunction"))=cstr(translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))) then
										Response.Write "<option VALUE='" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & "' SELECTED>" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									else
										Response.Write "<option VALUE='" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & "'>" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									end if
								else
									Response.Write "<option VALUE='" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & "'>" & translation_global("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
								end if
								
								rsJobFunction.MoveNext
								wend
							end if
							 %>		 
								</select>
							</td>
						</tr>					
					<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0083",localeid)%>:</nobr></font></td>
						  <td width="200">
						  <select name="Country" maxlength="50" value size="1" ID="Select2">
							<% If Len (Session("Country"))=0 Then %>
								<option VALUE><%=LoadI18N("SLBCA0171",localeid)%>&nbsp;<%=LoadI18N("SLBCA0083",localeid)%>
							<%end if
							set rsCountry=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from COUNTRY order by NAME"
							rsCountry.Open strSQL, dbconnstr()
							if not rsCountry.EOF then
								while not rsCountry.EOF
								If Len (Session("Country"))>0 Then 
									if cstr(Session("Country"))=cstr(rsCountry("NAME")) then
										Response.Write "<option VALUE='" & rsCountry("NAME") & "' SELECTED>" & rsCountry("NAME")
									else
										Response.Write "<option VALUE='" & rsCountry("NAME") & "'>" & rsCountry("NAME")
									end if
								else
									Response.Write "<option VALUE='" & rsCountry("NAME") & "'>" & rsCountry("NAME")
								end if
								
								rsCountry.MoveNext
								wend
							end if
							 %>		 
								</select></td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0082",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="Zip" size="20" maxlength="20" value="<% = Session("Zip") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0080",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="City" size="30" maxlength="50" value="<% = Session("City") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0079",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="Address" size="40" maxlength="255" value="<% = Session("Address") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2><nobr><%=LoadI18N("SLBCA0079",localeid)%>2:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="Address2" size="40" maxlength="255" value="<% = Session("Address2") %>" ID="Text7">
						  </td>
						</tr>



						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0084",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><input type="text" name="Phone" size="30" maxlength="30" value="<% = Session("Phone") %>" >
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2><nobr><%=LoadI18N("SLBCA0085",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="text" name="Fax" size="30" maxlength="30" value="<% = Session("Fax") %>" ></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0086",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="text" name="Email" size="30" maxlength="50" value="<% = Session("Email") %>" ></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2><nobr><%=LoadI18N("SLBCA0087",localeid)%>/<%=LoadI18N("SLBCA0168",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="text" name="URL" size="30" maxlength="50" value="<% = Session("URL") %>" ></font>
						  </td>
						</tr>
						<tr>
						 <td width="200" align="right"><font face="Verdana,arial" size=2><nobr><%=LoadI18N("SLBCA0088",localeid)%>:</nobr></font>
						 </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="text" name="Applications" size="30" maxlength="50" value="<% = Session("Applications") %>" ID="Text15"></font>
						  </td>
						<tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0148",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="text" name="Username" size="15" maxlength="15" value="<% = Username %>" ID="Text16"></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="red"><nobr><%=LoadI18N("SLBCA0151",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="password" name="Password" size="15" maxlength="15" value="<% = Password %>" ID="Password1"></font>
						  </td>
						</tr>
						<tr>
						  <td width="200" align="right"><font face="Verdana,arial" size=2 color="Red"><nobr><%=LoadI18N("SLBCA0169",localeid)%>&nbsp;<%=LoadI18N("SLBCA0151",localeid)%>:</nobr></font>
						  </td>
						  <td width="200"><font face="Verdana,arial" size=2><input type="password" name="ConfirmPassword" size="15" maxlength="15" value="<% = ConfirmPassword %>" ID="Password2"></font>
						  </td>
						</tr>
						<%
						'*****YK 5/3/2002
						'if user came from "/Forum/Chat/Post.asp" allow him to create
						'a signature
						
						if Session("RedirectTo") = "/Forum/Chat/Post.asp"  then
							Response.Write "<tr>"
						    Response.Write "<td width=""200""  align=""right"" valign=""top"">"
							Response.Write "<font face=""Verdana,Arial"" size=2 color=""#000000""><nobr><b>" & "Signature" & "</b></nobr></font>"
							Response.Write "</td>"
						    Response.Write "<td width=""200""  valign=""top"">"
							Response.Write "<textarea rows=3 cols=30 name=sign id=sign>" & Session("signature") & "</textarea>"
							Response.Write "</td>"
							Response.Write "</tr>"
						end if
						%>
						
						<%
						'select all enabled subscription type 
						'and show checked checkboxes
						set rsSubType=server.CreateObject("ADODB.Recordset")
						strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y' or document_id=64 order by name"
						rsSubType.Open strSQL,dbconnstr()
						if not rsSubType.EOF then
							Response.Write "<tr>"
						    Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
							Response.Write "<br><br><font face=""Verdana,Arial"" size=2 color=""DodgerBlue""><b>" & LoadI18N("SLBCA0382",localeid) & "</b></font>"
							Response.Write "</td>"
							Response.Write "</tr>"
							Response.Write "<tr>"
							Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
							Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
						
							while not rsSubType.EOF
								subcount=subcount+1
								Response.Write "<tr>"
								Response.Write "<td valign=""top"" width=""30"">"
								Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription" & rsSubType("DOCUMENT_ID") & "' checked>"
								Response.Write "</td>"
								Response.Write "<td valign=""middle"">"
								Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubType("NAME") &  "</b></nobr></font>"
								Response.Write "</td>"
								Response.Write "</tr>"
								redim preserve SubTypeID(subcount)				
								SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")
								
							rsSubType.MoveNext
							wend
							
							Response.Write "</table>"
							Response.Write "</td>"
							Response.Write "</tr>"
							'ask customer in which format he prefer to recive an email
							set rsSubFormat=server.CreateObject("ADODB.Recordset")
							strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y' "
							rsSubFormat.Open strSQL,dbconnstr()
							if not rsSubFormat.EOF then
								Response.Write "<tr>"
								Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
								Response.Write "<br><br><font face=""Verdana,Arial"" size=2 color=""DodgerBlue""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font>"
								Response.Write "</td>"
								Response.Write "</tr>"
								Response.Write "<tr>"
								Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
								Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
							
								while not rsSubFormat.EOF
									if cstr(rsSubFormat("NAME"))="Text" then
										strSelected="CHECKED"
									else
										strSelected=""
									end if
									Response.Write "<tr>"
									Response.Write "<td valign=""top"" width=""30"">"
									Response.Write "<input type=radio name='subformat' id='subformat' value='" & rsSubFormat("FORMAT_ID")  & "'" & " " & strSelected & ">"
									Response.Write "</td>"
									Response.Write "<td valign=""middle"">"
									Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
									Response.Write "</td>"
									Response.Write "</tr>"
								rsSubFormat.MoveNext
								wend
								
								Response.Write "</table>"
								Response.Write "</td>"
								Response.Write "</tr>"
								end if
						end if
						
						
							Session("SubCount")=subcount
							Session("SubTypeID") = SubTypeID						
						
						%>
						
						<tr>
							<td align=left colspan=2><br>
							<font face="verdana,arial,helvetica" size="2"><%=LoadI18N("SLBCA0604",localeID)%></font>
							</td>
						</tr>
							<tr>
							<td>
							</td>
							<td align="right">
								<hr size="1" color="RoyalBlue">
								<%
								if localeid="1036" then
								%>
								<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="document.regform.submit();" ID="Button1">
								<%else%>
								<input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="validate()" ID="Button2">
								<%end if%>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
</table>
                            
                            
                            <%' End of RegForm Code %>
                            
                            
                            </font>
                            <p class="SmallBody" align="left">&nbsp;</p>
						</td>
						<td valign="top" rowspan="2" align="left" height="108">
							
						</td>
					</tr>
				</table>
			  </td>
			  <td></td>
			  <td valign="top" align="right">
			  <br>
			  </td>
			</tr>
		<tr>
			<td bgcolor="#0099FF">&nbsp;</td>
			<td></td>
			<td><nobr><img border="0" name="holder" src="/nav/images/spacer.gif" alt width="300" height="25" align="absbottom"></nobr></td>
			<td></td>
			<td></td>
		</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html>

<script LANGUAGE="JavaScript">
function validate()
//this function redirect user to the next page if all fields of
//registration form are correct
{

    var msg;
    msg ="";
    var flg=false;
    if(replace(document.regform.LastName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0162",localeid)%>" + "\n";
      flg=true;
	  document.regform.LastName.focus();
	}
    if(replace(document.regform.FirstName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0161",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.FirstName.focus();
	  }
     }

    if (replace(document.regform.Company.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0078",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Company.focus();
	  }
    }
    
    if (replace(document.regform.Title.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0077",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Title.focus();
	  }
    }
     if(document.regform.Job_function.value == "")
     {
      msg += "<%=LoadI18N("SLBCA0163",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Job_function.focus();
	  }
     }
	if(document.regform.Country[document.regform.Country.selectedIndex].value == "")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Country.focus();
		}	
	}
	if(replace(document.regform.Zip.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0082",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Zip.focus();
		}	
	}
    if(replace(document.regform.City.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0080",localeid)%>" + "\n";
     if (flg!=true)
       {
        flg=true;
		document.regform.City.focus();
	   }	
    }
	if(replace(document.regform.Address.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0079",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.regform.Address.focus();
	  }	
    }
    if(replace(document.regform.Phone.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0084",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Phone.focus();
		}	
   }
    if(replace(document.regform.Email.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0086",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Email.focus();
		}	
   }
   	if(replace(document.regform.Username.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0148",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Username.focus();
		}	
	}
	if(replace(document.regform.Password.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0151",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.regform.Password.focus();
		}	
	}
	if(replace(document.regform.ConfirmPassword.value," ","") == "")
	{
	 msg += "<%=LoadI18N("SLBCA0169",localeid)%>" + " " + "<%=LoadI18N("SLBCA0151",localeid)%>" + "\n";
	if (flg!=true)
		{
		    flg=true;
			document.regform.ConfirmPassword.focus();
		}		
	}

    if(msg != "")
    {
      alert("<%=LoadI18N("SLBCA0172",localeid)%>" + ":"+ "\n" + "\n" + msg);
      return false;
    }
    document.regform.submit();
}
    function openTermWin(URL)
	{
		aWindow = window.open(URL,"thewindow","width=575,height=400 bgcolor=white,scrollbars=yes,resizable=yes");
	}
function replace(string,text,by)
	 {
	// Replaces text with by in string
	    var strLength = string.length;
	    var txtLength = text.length;
	    if ((strLength == 0) || (txtLength == 0)) return string;

	    var i = string.indexOf(text);
	    if ((!i)&&(text != string.substring(0,txtLength))) return string;
	    if (i == -1) return string;

	    var newstr = string.substring(0,i) + by;

	    if (i+txtLength < strLength)
	        newstr +=replace(string.substring(i+txtLength,strLength),text,by);

	    return newstr;
	}
</script>


