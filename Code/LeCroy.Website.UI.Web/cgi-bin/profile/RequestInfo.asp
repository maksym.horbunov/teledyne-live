<%@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->
<!--#INCLUDE virtual="/cgi-bin/profile/allfunctions.asp" -->
<% Session("RedirectTo") = "RequestInfo.asp" 'here is the name of the file itself
If Not ValidateUser Then
	' Do not http// for redirect
	Response.redirect "SentToReg.asp"
End If
%>
<% 
Session("PageID")=1
Session("PropProdCategory")=0%>
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy Corporation -Request Information Page" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

'**************Added by YK 8/15/2001	*************
if len(Session("localeid"))=0 then Session("localeid")=1033
if Session("localeid")=1041 then 
	Response.Redirect "/cgi-bin/profile/requestinfoj.asp"
end if
localeid=Session("localeid")
Response.Redirect "TransferSession.asp"	
'************************************************
%>

<html>

<head>
<meta name="Title" content="<% = PageTitle %>">
<meta name="Description" content="<% = PageDescription %>">
<meta name="keywords" content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->


<title><% = PageTitle %></title>
</head>

<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>

<a name="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" ID="Table1">
  <tr>
    <td valign="top">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table2">
         <tr>
          <td valign="top">&nbsp;</td>
          <td width="90%" valign="top">
				<%'Whats New Table %>
            <table border="0" width="480" cellspacing="0" cellpadding="0" height="108" ID="Table3">
              <tr>
                <td valign="top" align="left" height="38"><img border="0" src="/images/Page_Titles/TITLE_RequestInfo.gif" WIDTH="400" HEIGHT="50"><br><br>
                 <!--#include file="RequestInfoForm.asp" -->
                  </td>
              </tr>
              <tr>
                <td valign="top" height="70">&nbsp;</td>
              </tr>
            </table>
          </td>
          <td valign="top" align="right">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<a name="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->

</body>

</html>
