<%'@ Language=VBScript %>
<% 'You will enter the 5 lines bellow on each page%>
<!-- #INCLUDE FILE="allfunctions.asp" -->
<% Session("RedirectTo") = "RequestInfoCD.asp" 'here is the name of the file itself
If Not ValidateUser Then
	Response.redirect "SentToReg.asp"
End If
%>

<% 
Session("PageID")=1
sAgent = Request.ServerVariables("HTTP_USER_AGENT")%>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%Session("PropProdCategory")=0%>

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

'******************adde by YK 1/2/2002**********************
if len(Session("localeid"))=0 then Session("localeid")=1033
localeid=Session("localeid")
'***********************************************************
%>

<html>

<head>
<meta name="Title" content="<% = PageTitle %>">
<meta name="Description" content="<% = PageDescription %>">
<meta name="keywords" content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->


<title><% = PageTitle %></title>
</head>

<script LANGUAGE="JavaScript">
function validate()
//this function redirect user to the next page if all fields of
//registration form are correct
{
    var msg;
    msg ="";

    document.form2.submit();
}
</script>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>

<a name="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td valign="top">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr height="10">
          <td width="100" valign="top" rowspan="2" bgcolor="#0099FF"> 
            <!--#include virtual="/shopper/menu/prodpropcategory.asp" -->
          </td>
          <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td valign="top"><br>
          </td>
          <td width="10" valign="top">&nbsp;</td>
          <td align="right" valign="top"><br>
          </td>
        </tr>
        <tr>
          <td valign="top"></td>
          <td width="90%" valign="top"> 
            <%'Start of CD Request Table %><font face="verdana" size="2">
            <p><br>
            <strong> 
              <% = Session("FirstName") %>
            , <%=LoadI18N("SLBCA0254",localeid)%>.</strong><br>
            <br>
              <%=LoadI18N("SLBCA0387",localeid)%></p>
            <br>
            <hr size="1">
            <br>
            <!--<p><font face="verdana" size="2"><strong><%=LoadI18N("SLBCA0388",localeid)%></strong></font><br>
            <font size="1"><%=LoadI18N("SLBCA0389",localeid)%></font></p>-->
            <!--<p><img src="../images/JitterCDROM.jpg" WIDTH="150" HEIGHT="150"><br></p>-->
            <form method="get" action="RequestRegistration.asp" id="form2" name="form2">
              <p> 
              <%
							
				Response.Write "<table border=""0"" cellpadding=""0"" callspacing=""0"">"

								set rsLit=server.CreateObject("ADODB.Recordset")
								strSQL = "SELECT LITERATURE_ID, NAME FROM V_ALL_LITERATURES WHERE COUNTRY_ID = 208 and LITCATID=4 ORDER BY NAME"
							
								rsLit.Open strSQL,dbconnstr()
								Response.Write "<table border=""0"" cellpadding=""0""  cellspacing=""0"">"
								while not rsLit.EOF
									Response.Write "<tr>"
									count=count+1
									iLitLooper = iLitLooper + 1
									Redim Preserve LitIdAndName(1,iLitLooper)
									LitIdAndName(0,iLitLooper-1)  = rsLit("LITERATURE_ID")
									LitIdAndName(1,iLitLooper-1)  = rsLit("NAME")
									Response.Write "<td width=20>&nbsp;</td>"
									Response.Write "<td valign=""top"">"
									if not Session("literature")="" then
										if  checkliteratureselected(rsLit("LITERATURE_ID")) then
											Response.Write "<nobr><input type=checkbox name=Lit" & count & " value='" & rsLit("LITERATURE_ID") & "' checked>" 
										else
											Response.Write "<nobr><input type=checkbox name=Lit" & count & " value='" & rsLit("LITERATURE_ID") & "'>" 
										end if
									else
										Response.Write "<nobr><input type=checkbox name=Lit" & count & " value='" & rsLit("LITERATURE_ID") & "'>" 
									end if
									Response.Write "</td>"
									Response.Write "<td>"
									Response.Write "<font face=""Verdana"" size=""1"" color=""#000000"">"
									Response.Write  rsLit("NAME")
									Response.Write "</font></nobr><br>"
									Response.Write "</td>"
									Response.Write "</tr>"
									rsLit.MoveNext
								wend
				

				
							Session("LitCount")=iLitLooper
							Session("StoredLitIdAndName") = LitIdAndName
							%>
              </p>
              <p><font face="Verdana" size="2"> 
              <%'Make sure that the Maximum Requests Asked = the total number of possibilities to request %>
              <input type="hidden" name="MaxRequestsAsked" value="4"></font></p>
            </font>
          </tr>
          <tr>
            <td width="100" valign="top">&nbsp;</td>
            <td valign="top"></td>
            <td><br>
              <br>
              <input type="button" value="<%=LoadI18N("SLBCA0267",localeid)%>" name="B1" style="background-color: #6082FF; color: #FFFFFF; font-weight: bold; border-style: outset" onclick="validate()"></td>
          </tr>
		  </form>
          <tr>
            <td width="100" valign="top">&nbsp;</td>
            <td valign="top"></td>
            <td width="90%" valign="top">
              <hr size="1">
              <font face="verdana" size="2"><%=LoadI18N("SLBCA0386",localeid)%><br>
              <br>
              <strong> 
              <% = Session("FirstName") %>
              &nbsp;&nbsp;&nbsp; 
              <% = Session("LastName") %>
              </strong><br>
              <% = Session("Company") %>
              <br>
              <% = Session("Address") %>
              <br>
              <%If Len(Session("Address2")) > 0 then 
			Response.Write 	Session("Address2") %>
              <br>
              <% end if %>
              <% = Session("City") %>
              <br>
              <% = Session("State") %>
              <br>
              <% = Session("Zip") %>
              <br>
              <% = Session("Country") %>
              <br>
              <%=LoadI18N("SLBCA0086",localeid)%>: 
              <% = Session("Email") %>
              <br>
              <%=LoadI18N("SLBCA0084",localeid)%>: 
              <% = Session("Phone") %>
              <br>
              <br>
              </font><font size="2"><%=LoadI18N("SLBCA0390",localeid)%>.</font>
              <form action="/cgi-bin/profile/modform.asp" method="get" id="form1" name="form1">
                <% Session("RedirectToFirst") = "RequestInfoCD.asp" %>
                <input type="submit" value="<%=LoadI18N("SLBCA0061",localeid)%>" id="submit1" name="submit1">
              </form>
              <font face="Verdana" size="2"></font>
          </tr>
        </table>
  </table>
  <!--#include virtual="/images/Footer/footer.asp" -->

</body>

</html>
