<%@ LANGUAGE="VBSCRIPT" %>

<!--#INCLUDE virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/shopper/functions.asp" -->
<!--#INCLUDE virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->
<!--#include virtual="/include/global.asp" -->
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	sAgent = Request.ServerVariables("HTTP_USER_AGENT")
	PageTitle="Request info confirmation" ' Title of Web Page
	PageDescription="LeCroy Corporation's Web Site" 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes,Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	division_id=1 'Sets the Division ID
	topic_id=1
	session("language") = "us"
	country_id=208
'**************Added by YK 8/15/2001	*************
if len(Session("localeid"))=0 then Session("localeid")=1033
localeid=Session("localeid")
'************************************************
%><html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" bgcolor="#ffffff">
<a NAME="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->

<%
Session("DemoBody")=""
Session("SECall")=""
Session("litBody")=""
session("litremarks")=""
If Not ValidateUser Then
	' Do not http// for redirect
	Response.redirect "/cgi-bin/profile/SentToReg.asp"
End If
if (Session("localeid")=1041 or Session("COuntry")="Japan") or Session("country")="United States" then
	strSpace=Chr(13) & Chr(10)
else
	strSpace="<br>"
end if
  
'Set all the selected information
'response.Write request.QueryString 
'DEMO MODELS**************
If Len(Request.QueryString("DemoModels"))> 0 Then 
	'create array of all DEMO (YK 8/10/01)
	demo_all=Session("StoredCurrModelsIdAndName")
	'create string of requested DEMO
	for each item in Request.QueryString ("DemoModels")
		if len(item)>0 then 
			for i=0 to Session("DemoCount")
				if cint(demo_all(0,i))=cint(item) then
					'if lcase(mid(cstr(demo_all(1,i)),1,10))="wavesurfer" then
					'	WSFlag="true"
					'elseif lcase(mid(cstr(demo_all(1,i)),1,7))="wavejet" then
					'	WJFlag="true"
					'end if 
					'if lcase(mid(cstr(demo_all(1,i)),1,10))="wavesurfer" or lcase(mid(cstr(demo_all(1,i)),1,7))="wavejet" then
					'	if len(strDemoName)=0 then
					'		strDemoName="<nobr>" & demo_all(1,i)& "</nobr>"
					'		strWSDemoBody= demo_all(1,i)& strSpace
					'	else
					'		strDemoName=strDemoName &  "<br><nobr>" & demo_all(1,i)  & "</nobr>"
					'		strWSDemoBody=strWSDemoBody &  demo_all(1,i)& strSpace
					'	end if
					'else
						if len(strDemoName)=0 then
							strDemoName="<nobr>" & demo_all(1,i)& "</nobr>"
							strDemoBody= demo_all(1,i)& strSpace
						else
							strDemoName=strDemoName &  "<br><nobr>" & demo_all(1,i)  & "</nobr>"
							strDemoBody=strDemoBody &  demo_all(1,i)& strSpace
						end if
					'end if
				end if
			next
		'create array of selected DEMO
			democount=democount+1
			redim preserve demo(democount)
			demo(democount-1)=item
		end if
	next
	'Check if WS Distributor Exists for US
	if Session("COuntry")="United States" then
		Session("DemoBody")=strDemoBody
		Session("WSDemoBody")=strWSDemoBody
	else
		Session("DemoBody")=strDemoBody & strWSDemoBody
	end if
	Session("selecteddemocount") =democount 
	Session("demo")=demo
	DemoRequest = True
else
	Session("demo")=""
End If
'END OF DEMO MODELS******************** 

'SETOCALL**************************************
if len(request.QueryString("SEcallckbx")) > 0 then
	
	If Len(Request.QueryString("SEcallmodels"))> 0 Then 
		setocall_all =Session("StoredSEtocallModels")
		'create string of requested DEMO
		for each item in Request.QueryString ("SEcallmodels")
			if len(item)>0 then
				for i=0 to Session("SECallCount")
					if cint(setocall_all(0,i))=cint(item) then
						if len(strSetocallname)=0 then
							strSetocallname="<nobr>" & setocall_all(1,i)& "</nobr>"
							strSetocallBody=setocall_all(1,i) & strSpace
						else
							strSetocallname=strSetocallname &  "<br><nobr>" & setocall_all(1,i)  & "</nobr>"
							strSetocallBody=strSetocallBody & setocall_all(1,i) & strSpace
						end if
					end if
				next
			'create array of selected DEMO
				Setocallnamecount=Setocallnamecount+1
				redim preserve SeTocall(Setocallnamecount)
				SeTocall(Setocallnamecount-1)=item
			end if
		next 
		Session("SEtocall") = SeTocall
		Session("SEtocallBody")=strSetocallBody
		Session("SEtocallselectedcount") = Setocallnamecount
		SEtocallRequest = True
	else
		SEtocallRequest = True
		Session("SEnoModels") = true
		Session("SEtocallBody")=LoadI18N("SLBCA0645",localeid)
	End If
	
	if len(request.QueryString("SEcallques")) > 0 then 
		SEtocallRequest = True
		Session("SEcallques") = replace(request.QueryString("SEcallques"),"'","''")
		strSetocallBody=strSetocallBody & LoadI18N("SLBCA0630",localeid) & "<br>"  & request.QueryString("SEcallques") & "<br>" 
		strSetocallname=strSetocallname &  "<br><br><nobr>" & "<font size=""2"" face=""verdana,arial"" color=""DodgerBlue""><b>"& LoadI18N("SLBCA0630",localeid) &"</b></font><BR>" & "<br>"  & request.QueryString("SEcallques") &   "</nobr>"
	end if 
	
else
	Session("SEtocallBody")=""
	SEtocallRequest = false
	Session("SEcallques")=""
end if 
If len(Request.QueryString("SEcallckbx"))> 0 Then 
	Session("SECall")="true"
else
	Session("SECall")=""
end if 
'END OF SETOCALL SELECTION********************   
 
'LIT Remarks
if len(Request.QueryString("RemarksLit"))>0 then
    set rsLitModels=server.CreateObject("ADODB.Recordset")
    strSQL = "SELECT MODEL_ID, NAME FROM V_CUR_MODELS_JAPAN where  MODEL_ID=" & request.querystring("RemarksLit")& " ORDER BY SORT_ID"
    rsLitModels.Open strSQL,dbconnstr()
    if not rsLitModels.EOF then
         session("litremarks")=rsLitModels("Name")
    end if
end if
'END OF LIT Remarks*******************   
 
'LITERATURE***************************** 
lit_all=Session("StoredLitIdAndName")
for each item in Request.QueryString 
	if mid(item,1,3)="Lit" and item<>"LitRemarks" then
		if len(Request.QueryString(item))>0 then
		'create string of literatrures,which have been selected
			for i=0 to Session("LitCount")
				if cint(lit_all(0,i))=cint(Request.QueryString(item)) then
					if len(strLitName)=0 then
						strLitName="<nobr>" & lit_all(1,i) & "</nobr>"
					else
						strLitName=strLitName & "<br>" & "<nobr>" & lit_all(1,i)& "</nobr>"  
					end if
					if Request.QueryString(item)=257 then
					    strLitName=strLitName & " (" &  session("litremarks") & ")"
					end if
				end if
			next
			litcount=litcount+1
		'create array of selected literature
			redim preserve literature(litcount)
			literature(litcount-1)=Request.QueryString(item)
			if len(strLit)>0 then
				strLit=strLit & "," & replace(Request.QueryString(item),"<br>"," ")
			else
				strLit=replace(Request.QueryString(item),"<br>"," ")
			end if
		end if
	end if
next
Session("literature")=literature
Session("selectedlitcount")=litcount
'create part of email body for literature
if len(strLit)>0 then
	LitRequest= true
	set rsLit=server.CreateObject("ADODB.Recordset")
	if Session("localeid")<>1041 then
		strSQL = "SELECT LITERATURE_ID, NAME FROM V_ALL_LITERATURES WHERE COUNTRY_ID = 208 and  LITERATURE_ID in (" & strLit & ") ORDER BY NAME"
	else
		strSQL = "SELECT LITERATURE_ID, NAME FROM V_ALL_LITERATURES WHERE COUNTRY_ID = 100 and  LITERATURE_ID in (" & strLit & ") ORDER BY NAME"
	end if
	rsLit.Open strSQL,dbconnstr()
	While not rsLit.EOF 
		if len(litBody)=0 then
			litBody=replace(rsLit("Name"),"<br>",strSpace) 
		else
			litBody= litBody &  replace(rsLit("Name"),"<br>",strSpace) 
		end if
		if rsLit("LITERATURE_ID")=257 then
		    litBody=litBody & " (" &  session("litremarks") & ")"
		end if
		litBody= litBody & strSpace
		rslit.MoveNext
	wend
	Session("litBody")=litBody & strSpace
else
	Session("litBody")=""
end if
'END OF LIT SELECTION**************************



'change in address was clicked

if len(request.QueryString ("changeadd"))>0 then 
	If Session("localeid")<>1041 then
		response.Redirect "/cgi-bin/profile/ModForm.asp?" 
	else
		response.Redirect "/cgi-bin/profile/ModFormj.asp?" 
	end if
end if 
'end of code - change in address

'START OF EMAIL BODY AND SEND EMAIL************
'if not CheckcustomerfromLNA then
	if (Session("localeid")=1041 or Session("COuntry")="Japan") or Session("country")="United States"  then
		strEmailBodyAll = CreateEmailBody(LitRequest, DemoRequest, false, false, SEtocallRequest)
	else
		strEmailBodyAll = CreateEmailBodyHTML(LitRequest, DemoRequest, false, false, SEtocallRequest)
	end if
'else
	'if len(Session("Lastname")) > 0 and len(Session("Company"))> 0 then
	'	if WJFlag and WSFlag then
	'		strSubject = "WaveSurfer and WaveJet Demo Request" & " - " & Session("Company") & " - " & Session("LastName")
	'	else
	'		if WSFlag then
	'			strSubject = "WaveSurfer Demo Request" & " - " & Session("Company") & " - " & Session("LastName")
	'		elseif WJFlag then
	'			strSubject = "WaveJet Demo Request" & " - " & Session("Company") & " - " & Session("LastName")
	'		end if 
	'	end if 
	'end if 
	
		
	
'	strEmailBodyAll = CreateEmailBody(LitRequest, DemoRequest, false, false, SEtocallRequest)
	'if cstr(Session("country"))="United States" then
	'	if len(strWSDemoBody)>0 then
	'		if CheckDistExist(Session("ContactID"))  then
	'			'send demo to the distributor
	'			strTo = "kate.kaplan@teledynelecroy.com"'GetWSDistEmail(Session("ContactID"))
	'			strFrom = "webmaster@teledynelecroy.com"
	'			strCC = "kate.kaplan@teledynelecroy.com"'GetWSDistEmailCC(session("contactid")) & "," & "Barry.Kitaen@teledynelecroy.com"
	'			strBCC ="kate.kaplan@teledynelecroy.com"
	'			'strSubject= strSubject1
	'			strBody = CreateWSDemoEmailBody
	'			'call SendQuoteEmail(GetWSDistEmail(Session("ContactID")) , "webquote@teledynelecroy.com",GetWSDistEmailCC(session("contactid")) & "," & "Barry.Kitaen@teledynelecroy.com","kate.kaplan@teledynelecroy.com","WaveSurfer Demo Request",CreateWSDemoEmailBody)
	'			'call SendQuoteEmail("kate.kaplan@teledynelecroy.com", "webmaster@teledynelecroy.com","","","WaveSurfer Demo Request" & " - " & GetWSDistName(Session("ContactID")) & " " & GetWSDistEmail(Session("ContactID")) & " " & GetWSDistEmailCC(session("contactid")),CreateWSDemoEmailBody)
	'		else
	'			strTo = "kate.kaplan@teledynelecroy.com"'"Barry.Kitaen@teledynelecroy.com"
	'			strFrom = "webmaster@teledynelecroy.com"
	'			strCC = ""'GetWSDistEmailCC(session("contactid")) & "," & "Barry.Kitaen@teledynelecroy.com"
	'			strBCC ="kate.kaplan@teledynelecroy.com"
	'			'strSubject= strSubject1
	'			strBody = CreateWSDemoEmailBody
	'			'call SendQuoteEmail("Barry.Kitaen@teledynelecroy.com" ,"webmaster@teledynelecroy.com","","kate.kaplan@teledynelecroy.com",strSubject,CreateWSDemoEmailBody)
	'			'call SendQuoteEmail("kate.kaplan@teledynelecroy.com" ,"webmaster@teledynelecroy.com","","","WaveSurfer Demo Request",CreateWSDemoEmailBody)
	'		end if
	'			Set oMailDistMsg = Server.CreateObject("CDO.Message")
	'			oMailDistMsg.To = strTo
	'			oMailDistMsg.From = strFrom
	'			if len(strCC)>0 then
	'				oMailDistMsg.cc=strCC
	'			end if
	'			if len(strBCC)>0 then
	'				oMailDistMsg.Bcc = strBCC
	'			end if
	'			oMailDistMsg.Subject = strSubject
	'			oMailDistMsg.HTMLBody = strBody
	'			oMailDistMsg.Send
	'			Set oMailDistMsg = Nothing ' Release the object
	'	end if
	'end if
'end if
If DemoRequest Then
	blDemoAsked = True
Else
	blDemoAsked = False
End If

'Email to the appropriate person
'if not CheckcustomerfromLNA then'and Session("localeid")<>1041 and Session("country")<>"Japan" then
	If len(strEmailBodyAll)<> 0 and (LitRequest or DemoRequest or QuoteRequest or  SEtocallRequest) Then  
		Call SendEmailToSalesRep(strEmailBodyAll, blDemoAsked)
	End If
'else
'	'8/1/2005 ***** do not sent email to telesales
'	'1/4/2007 ***** Send all requests to telesales and data entry
'	If len(strEmailBodyAll)<> 0 and (LitRequest or DemoRequest or QuoteRequest or  SEtocallRequest) Then 
'		Call SendEmailToSalesRep(strEmailBodyAll, blDemoAsked)
'	End If
'end if
 
'Email to customer
strCustomerEmailBody=CreateCustomerEmailBody(LitRequest, DemoRequest, false, false, SEtocallRequest)

if len(strCustomerEmailBody)>0 then
	call SendEmailToCustomer(strCustomerEmailBody)
end if

'END OF EMAILS*****************************

'START DISPLAY BODY AND DISPLAY TEXT************

If cint(Session("selectedlitcount")) <= 100 Then
	
	MaxRequests = Request.QueryString("MaxRequestsAsked")
	InsertedOk = InsertAllCustomerRequests(MaxRequests)
	
	'If len(InsertedOk)>0 Then
		 
		response.Write "<table border=0 cellspacing=0 cellpadding=0 width=100% ID=Table1>"
		response.Write "<tr><td valign=top>"
		response.Write "<table width=500 border=0 cellspacing=0 cellpadding=0 height=100% ID=Table2>"
		response.Write "<tr><td width=10 valign=top>&nbsp;</td><td align=right valign=top>&nbsp;</td></tr>"
		'Light Grey Line
		Response.Write "<tr height=""1"">"
		Response.Write "<td height=""1"" bgcolor=""#dcdcdc"" colspan=2 valign=""top"">"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"

		Response.Write "<td width=""10"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		Response.write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
		If LitRequest or DemoRequest or SEtocallRequest Then
			Response.Write "<tr>"
			Response.Write "<td colspan=2 valign=""top"">" 
			if Session("localeid")<>1041 then
				Response.Write "<font face=""verdana,arial"" size=""2""><b>" & Session("FirstName") & ", " & LoadI18N("SLBCA0270",localeid) & "</b></font><br><br>"
			else
				Response.Write "<font face=""verdana,arial"" size=""2""><b>" & Session("LastName")&"&nbsp;"& Session("FirstName") & "&nbsp;" & LoadI18N("SLBCA0160",Session("localeid"))& "<br>" & LoadI18N("SLBCA0270",localeid) & "</b></font><br><br>"
			end if
			Response.Write "</td>"
			Response.Write "</tr>"
		end if
		Response.Write "<tr>"
		Response.Write "<td width=""15"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		if LitRequest then
			Response.Write "<font size=""2"" face=""verdana,arial"" color=""DodgerBlue""><b>" & LoadI18N("SLBCA0271",localeid) & "</b></font><br>"
			Response.Write "<font size=""2"" face=""verdana,arial"">" & strLitName & "</font><br><br>"
		end if
		If DemoRequest Then 
			Response.Write "<font face=""verdana,arial"" size=""2"" color=""DodgerBlue""><b>" & LoadI18N("SLBCA0272",localeid) & "</b></font><br>"
			Response.Write "<font face=""verdana,arial"" size=""2"">" & strDemoName & "</font><br><br>"
		end if
		If HelpRequest then
			Response.Write "<font size=""2"" face=""verdana,arial"" color=""DodgerBlue""><b>" & LoadI18N("SLBCA0273",localeid) & "</b></font><br>"
			Response.Write "<font size=""2"" face=""verdana,arial"">" & strHelpModel & "</font><br><br>"
		end if
		If SEtocallRequest  then
			if Len(Request.QueryString("SEcallmodels"))> 0 then
				Response.Write "<font size=""2"" face=""verdana,arial"" color=""DodgerBlue""><b>"& LoadI18N("SLBCA0629",localeid) &"</b></font><br>"
			end if 
			Response.Write "<font size=""2"" face=""verdana,arial"">" & strSetocallname & "</font><br><br>"
		end if
		
		Response.Write "</td></tr>"
		Response.Write "</table><br>"
		Response.Write "</td>"
		Response.Write "</tr>"

		if LitRequest then
		' Write here for literature
		Response.Write "<tr>"
		Response.Write "<td width=""10"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		Response.Write "<FONT face=""verdana,arial"" size=""2"">" & LoadI18N("SLBCA0274",localeid) & "</font><br>"
		if Session("localeid")<> 1041 then
			Response.Write "<br><FONT face=""verdana,arial"" size=""1""><b>" &  Session("FirstName") & "&nbsp;" &  Session("LastName")& " </b><br>"
			if len(Session("Company"))>0 then
				Response.Write  Session("Company") & "<br>"
			end if
			if len(Session("Address"))>0 then
				Response.Write Session("Address") & "<br>"
			end if
			If Len(Session("Address2")) > 0 then 
				Response.Write Session("Address2") & "<br>"
			end if
			if len(Session("City"))>0 then
				Response.Write Session("City") & "<br>"
			end if
			if len(Session("State"))>0 then
				Response.Write Session("State") & "<br>"
			end if
			if len(Session("Zip"))>0 then
				Response.Write Session("Zip") & "<br>"
			end if
			if len(Session("Country"))>0 then
				Response.Write Session("Country")& "</font>"
			end if
		else
			Response.Write "<br><FONT face=""verdana,arial"" size=""2"">"
			if len(Session("Zip"))>0 then
				Response.Write Session("Zip") & "<br>"
			end if
			if len(Session("City"))>0 then
				Response.Write Session("City") & "<br>"
			end if
			if len(Session("Address"))>0 then
				Response.Write Session("Address") & "<br>"
			end if
			If Len(Session("Address2")) > 0 then 
				Response.Write Session("Address2") & "<br>"
			end if
			if len(Session("Company"))>0 then
				Response.Write  Session("Company") & "<br>"
			end if
			if len(Session("title"))>0 then
				Response.Write  Session("Title") & "<br>"
			end if
			Response.Write "</font><br><FONT face=""verdana,arial"" size=""2""><b>" &  Session("LastName")& " " & Session("Firstname") & " " & LoadI18N("SLBCA0160",Session("localeid"))& "</b><br>"
		end if
		Response.Write "<br><br><br></td>"
		Response.Write "</tr>"
		'end if
		'Light Grey Line
		Response.Write "<tr height=""1"">"
		Response.Write "<td height=""1"" bgcolor=""#dcdcdc"" colspan=2>"
		Response.Write "</td>"
		Response.Write "</tr>"

		'End if%> 
		<%If Request.QueryString("SEcallckbx")= 1 Then %>
			<tr>
			<td width="10">&nbsp;</td>
			<td valign="top" valign="top">
			<br><font face="verdana" size="2">

					<%=LoadI18N("SLBCA0276",localeid)%><br>
					<% = Session("Phone") %>
				</td>
			</tr> 

		<%
			Session("SECall")="true"
		else
			Session("SECall")=""
		End if
	end if  
	response.Write "</table></td></tr></table>"
end if
'end of Display body**************************
%>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html> 

