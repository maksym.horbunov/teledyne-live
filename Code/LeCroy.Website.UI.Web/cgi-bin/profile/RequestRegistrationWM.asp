<%@ LANGUAGE="VBSCRIPT" %>
<% SeminarName = Request.QueryString("SeminarName") %>
<!-- #INCLUDE virtual="/cgi-bin/profile/allfunctions.asp" -->
<!-- #INCLUDE virtual="/cgi-bin/profile/newfunctions.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->
<%
If Not ValidateUser Then
	' Do not http// for redirect
	Response.redirect "/cgi-bin/profile/SentToReg.asp"
End If
Session("TempEmail")=Session("Email")

'*****************check subscription***************
'*****************added 11/30/2001 YK
'check which subscriptions were selected
	sub_all=Session("SubTypeID")
	for i=0 to (Session("SubCount")-1)
			
		if len(Request.QueryString(cstr("subscription" & sub_all(i))))>0 then
			call UpdateSubscription(checksubscriptionexist(sub_all(i)),"on",sub_all(i))
		else
			call UpdateSubscription(checksubscriptionexist(sub_all(i)),"",sub_all(i))
		end if
	next
	Session("TempEmail")=""
'*************************************************

LiteratureMaxAllowedSelections = 15
DemoMaxAllowedSelections = 15
QuoteMaxAllowedSelections = 15
HelpMaxAllowedSelections = 15
' Added March 30 by Adrian to Accomodate Seminars
SemMaxAllowedSelection = 6
' End of Added Code 3/30/99 Adrian

LitSelectionNumberOK = "TRUE"
DemoSelectionNumberOK = "TRUE"
QuoteSelectionNumberOK = "TRUE"
HelpSelectionNumberOK = "TRUE"
' End of Added Code 3/30/99 Adrian
SelectionMadeByUser = False
' Added Jan 04 99 to accomodate new series
If Session("ComeFrom") = "WAVERUNNER" then
	Session("ComeFrom") = ""
	WaveRunner = True
Else

	'Create string of Literature's id and array of literatures ids (YK 8/9/01)
	lit_all=Session("StoredLitIdAndName")
	for each item in Request.QueryString 
		if mid(item,1,3)="Lit" then
			if len(Request.QueryString(item))>0 then
			'create string of literatrures,which have been selected
				for i=0 to (Session("LitCount")-1)
					if cint(lit_all(0,i))=cint(Request.QueryString(item)) then
						if len(strLitName)=0 then
							strLitName="<nobr>" & lit_all(1,i) & "</nobr>"
						else
							strLitName=strLitName & "<br>" & "<nobr>" & lit_all(1,i)& "</nobr>"  
						end if
					end if
				next
				litcount=litcount+1
			'create array of selected literature
				redim preserve literature(litcount)
				literature(litcount-1)=Request.QueryString(item)
				if len(strLit)>0 then
					strLit=strLit & "," & Request.QueryString(item)	
				else
					strLit=Request.QueryString(item)
				end if
			end if
		end if
	next
	Session("literature")=literature
	Session("selectedlitcount")=litcount
	'create part of email body for literature
	if len(strLit)>0 then
		set rsLit=server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT LITERATURE_ID, NAME FROM LITERATURE WHERE COUNTRY_ID = 208 and  LITERATURE_ID in (" & strLit & ") ORDER BY NAME"
		rsLit.Open strSQL,dbconnstr()
		While not rsLit.EOF 
			if len(litBody)=0 then
				litBody=rsLit("Name") & strCret
			else
				litBody=litBody & rsLit("Name") & strCret
			end if
			rslit.MoveNext
		wend
		Session("litBody")=litBody & strCret
	
		end if
	' Check if max allowed was not passed

	If Len(strLit)> 0 Then 
		LitRequest = True
		LitSelectionNumberOK = Trim(CheckMaxNumberOfSelections(strLit,LiteratureMaxAllowedSelections))
	End If
	
	'Demo
	demo_all=Session("StoredCurrModelsIdAndName")
	for each item in Request.QueryString 
		if mid(item,1,4)="Demo" then
			if len(Request.QueryString(item))>0 then
			'create string of literatrures,which have been selected
				for i=0 to (Session("DemoCount")-1)
					if cint(demo_all(0,i))=cint(Request.QueryString(item)) then
						if len(strDemoName)=0 then
							strDemoName="<nobr>" & demo_all(1,i) & "</nobr>"
						else
							strDemoName=strDemoName & "<br>" & "<nobr>" & demo_all(1,i)& "</nobr>"  
						end if
					end if
				next
				democount=democount+1
			'create array of selected literature
				redim preserve demo(democount)
				demo(democount-1)=Request.QueryString(item)
				if len(strDemo)>0 then
					strDemo=strDemo & "," & Request.QueryString(item)	
				else
					strDemo=Request.QueryString(item)
				end if
			end if
		end if
	next
	Session("demo")=demo
	Session("selecteddemocount")=democount

	'create part of email body for demo
	if len(strDemo)>0 then
		Session("demomodel")=true
		set rsDemo=server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT MODEL_ID, NAME FROM MODEL WHERE   MODEL_ID in (" & strDemo & ") ORDER BY NAME"
		rsDemo.Open strSQL,dbconnstr()
		While not rsDemo.EOF 
			if len(DemoBody)=0 then
				DemoBody=rsDemo("Name") & strCret
			else
				DemoBody=DemoBody & rsDemo("Name") & strCret
			end if
			rsDemo.MoveNext
		wend
		Session("demoBody")=DemoBody & strCret
		end if
		
	' Check if max allowed was not passed

	If Len(strDemo)> 0 Then 
		DemoRequest = True
		DemoSelectionNumberOK = Trim(CheckMaxNumberOfSelections(Request.QueryString("Object2"),DemoMaxAllowedSelections))
	End If	
	
	If Len(Request.QueryString("Object4"))> 0 Then 

		'create array of all MODELS in Technical Help (YK 8/10/01)
		helpmodel_all=Session("StoredAllModelsIdAndName")
		'create string of models requested for Technical Help
		for each item in Request.QueryString ("Object4")
			if len(item)>0 then
				for i=0 to (Session("HelpModelCount")-1)
					if cint(helpmodel_all(0,i))=cint(item) then
						if len(strHelpModel)=0 then
							strHelpModel="<nobr>" & helpmodel_all(1,i) & "</nobr>"
						else
							strHelpModel=strHelpModel &  "<br><nobr>" & helpmodel_all(1,i) & "</nobr>" 
						end if
					end if
				next
			'create array of selected Models for Technical Help
				helpcount=helpcount+1
				redim preserve helpmodel(helpcount)
				helpmodel(helpcount-1)=item
			end if
		next
		Session("helpmodel")=helpmodel
		
		HelpRequest = True
		HelpSelectionNumberOK = Trim(CheckMaxNumberOfSelections(Request.QueryString("Object4"),HelpMaxAllowedSelections))
	else
		Session("helpmodel")=""
	End If

	If (Request.QueryString("Object5")= 1)  Then 
		If Len(Session("Phone"))>0 then 
			SalesCall = True
		End If
	End If
	
	If (Request.QueryString("Object6")= 1)  Then 
		SemRequest = True
	End If

	' Modified Statement to add Seminar Requests Validation 3/30/99 Adrian
	If (LitRequest  = True) Or (DemoRequest = True) Or (HelpRequest = True) Or (SalesCall = True) Or (SemRequest = True) Then
		SelectionMadeByUser = True
	End If
	if len(Request.QueryString("Remarks4"))>0 then
		Session("Remark")=server.HTMLEncode(trim(Request.QueryString("Remarks4")))
	else
		Session("Remark")=""
	end if
End If
If (SelectionMadeByUser And (LitSelectionNumberOK  = "TRUE") And (DemoSelectionNumberOK = "TRUE") And (QuoteSelectionNumberOK = "TRUE") And (HelpSelectionNumberOK = "TRUE")) or WaveRunner = TRUE OR SemRequest = True Then
	MaxRequests = Request.QueryString("MaxRequestsAsked")
	InsertedOk = InsertAllCustomerRequests(MaxRequests)
	'InsertedOk = True
	If InsertedOk Then
		sAgent = Request.ServerVariables("HTTP_USER_AGENT")%>
		
		
		<!--#include virtual="/include/global.asp" -->
		<%	'**************Every Page must include the header.asp file******************
			'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
			PageTitle="LeCroy Corporation's Web Site" ' Title of Web Page
			PageDescription="LeCroy Corporation's Web Site" 'Description of Page
			PageKeywords="LeCroy, Digital Oscilloscopes,Analog Oscilloscopes" ' List of Keywords for Search Engines
			banner="testmeasurement" 'other available - see banners.txt for more details
			topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
			bgimage="" 'sets the background image for the document
			language="us" ' Sets the language of the current page
			division_id=1 'Sets the Division ID
			topic_id=1
			session("language") = "us"
			country_id=208
		%>

		<html>
		<head>
		<meta NAME="Title" Content="<% = PageTitle %>">
		<meta NAME="Description" Content="<% = PageDescription %>">
		<meta NAME="keywords" Content="<% = PageKeywords %>">
		<%'Check for Browser Version and Display Appropriate StyleSheet %>
		<% if bIsie then %>
			<!--#include virtual="/include/ie.asp" -->
		<% elseif bIsNS then %>
			<!--#include virtual="/include/ns.asp" -->
		<% elseif bIsMac then %>
			<!--#include virtual="/include/mac.asp" -->
		<% end if %>
		<title><% = PageTitle %></title>
		</head>
<body leftmargin="0" topmargin="0" bgcolor="#ffffff">
<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->
  
	<%' Surrounding Table %>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td width="75" valign="top">
			<table width="500" border="0" cellspacing="0" cellpadding="0" height="100%">
			<tr>
				<td width="10" valign="top">&nbsp;</td>
				<td align="right" valign="top">
				&nbsp;
					<!--<a href="/cgi-bin/profile/requestinfo.asp"><img src="/images/BacktoRqPg.gif" border="0" WIDTH="148" HEIGHT="64"></a>-->
				</td>
			</tr>
	<%
	'Light Grey Line
		Response.Write "<tr height=""1"">"
		Response.Write "<td height=""1"" bgcolor=""#dcdcdc"" colspan=2 valign=""top"">"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		
		Response.Write "<td width=""10"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		Response.write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
		If LitRequest or DemoRequest or HelpRequest Then
			Response.Write "<tr>"
			Response.Write "<td colspan=2 valign=""top"">"
			Response.Write "<font face=""verdana,arial"" size=""2""><b>" & Session("FirstName") & ", thank you for your request:</b></font><br><br>"
			Response.Write "</td>"
			Response.Write "</tr>"
		end if
		Response.Write "<tr>"
		Response.Write "<td width=""15"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		if LitRequest then
			Response.Write "<font size=""2"" color=""DodgerBlue""><b>Literature:</b></font><br>"
			Response.Write "<font size=""2"">" & strLitName & "</font><br><br>"
		end if
		If DemoRequest Then 
			Response.Write "<font size=""2"" color=""DodgerBlue""><b>Demo:</b></font><br>"
			Response.Write "<font size=""2"">" & strDemoName & "</font><br><br>"
		end if
		If HelpRequest then
			Response.Write "<font size=""2"" color=""DodgerBlue""><b>Technical Help:</b></font><br>"
			Response.Write "<font size=""2"">" & strHelpModel & "</font><br><br>"
		end if
		Response.Write "</td></tr>"
		Response.Write "</table><br>"
		Response.Write "</td>"
		Response.Write "</tr>"
		'Light Grey Line
		'Response.Write "<tr height=""1"">"
		'Response.Write "<td height=""1"" bgcolor=""#dcdcdc"" colspan=2>"
		'Response.Write "</td>"
		'Response.Write "</tr>"
		if LitRequest then
		' Write here for literature
		Response.Write "<tr>"
		Response.Write "<td width=""10"">&nbsp;</td>"
		Response.Write "<td valign=""top"">"
		Response.Write "<font face=""verdana"" size=""2"">The literature that you have requested <br>will be shipped to the following address:</font><br>"
		Response.Write "<br><font size=""2""><b><strong>" &  Session("FirstName") & "&nbsp;" &  Session("LastName")& " </b></strong><br>"
		if len(Session("Company"))>0 then
			Response.Write  Session("Company") & "<br>"
		end if
		if len(Session("Address"))>0 then
			Response.Write Session("Address") & "<br>"
		end if
		'Response.Write "<font size=""2"">" 
		If Len(Session("Address2")) > 0 then 
			Response.Write Session("Address2") & "<br>"
		end if
		if len(Session("City"))>0 then
			Response.Write Session("City") & "<br>"
		end if
		if len(Session("State"))>0 then
			Response.Write Session("State") & "<br>"
		end if
		if len(Session("Zip"))>0 then
			Response.Write Session("Zip") & "<br>"
		end if
		if len(Session("Country"))>0 then
			Response.Write Session("Country")& "</font>"
		end if
		Response.Write "<br><br><br></td>"
		Response.Write "</tr>"
		end if

		if Len(Session("EMail"))>0 then 
			Response.Write "<tr>"
			Response.Write "<td width=""10"">&nbsp;</td>"
			Response.Write "<td valign=""top"">"
		%>

			<font size="2">You will be contacted at the following E-Mail address:</font><font size="2"> <strong>&quot;<% = Session("EMail") %>&quot;</font></strong>
		<%
			Response.Write "</td>"
			Response.Write "</tr>"

			'Light Grey Line
			Response.Write "<tr height=""1"">"
			Response.Write "<td height=""1"" bgcolor=""#dcdcdc"" colspan=2>"
			Response.Write "</td>"
			Response.Write "</tr>"

		End if%> 
		
	<%'End if%> 
	<%If Request.QueryString("Object5")= 1 Then %>
		<tr>
		<td width="10">&nbsp;</td>
		<td valign="top" valign="top">
		<br><font face="verdana" size="2">

				You will be contacted by a salesperson at the following number:<br>
				<% = Session("Phone") %>
			</td>
		</tr>

	<%
		Session("SECall")="true"
	else
		Session("SECall")=""
	End if %>
	
	<%'Added by Adrian 3/3/0/99 to accomodate Seminar Requests %>
	<%If Request.QueryString("Object6")= 1 Then %>
		<tr>
		<td width="10">&nbsp;</td>
		<td valign="top" valign="top">
				Thank you for registering for the <% = SeminarName %> Seminar(s). You will be contacted at <strong><% = Session("Phone") %></strong> to confirm your reservation.<br><br>
				<a href="/seminars/">Click here to view other open seminars.</a>

		</td>
		</tr>

	<% End if %>
	<%'End of Code Added  by Adrian 3/30/99 %>


		<%If WaveRunner = True then%>
		<tr>
		<td width="10">&nbsp;</td>
		<td valign="top">	
			Your request(s) have been registered. You will be contacted shortly.<br>
		</td>
		</tr>

		<%end if%>
	</table>
	<% ' Added jan 04 99 Nick Benes to accomodate the new products
	If WaveRunner = True then
		strEmailBodyAll = CompleteWaveRunnerEmailBody
	Else
		' Added dec 07 98 Nick Benes to send e-mail to sales reps	
		strEmailBodyAll = CreateEmailBody2(LitRequest, DemoRequest, QuoteRequest, HelpRequest, SalesCall)
	End If
	
	If WaveRunner or QuoteRequest or DemoRequest Then
		blDemoAsked = True
	Else
		blDemoAsked = False
	End If
	
	If len(strEmailBodyAll)<> 0 Then 
		'Call SendEmailToSalesRep(strEmailBodyAll, blDemoAsked)
	End If
	
	If HelpRequest then
		'Call SendEmailToTechnicalHelp
	End If
	
	If SemRequest then
		'Call SendEmailtoSeminarRequest
	End If
	%>
		</font>
		</td>
	<td valign="top" align="right">
		<!--include virtual="/include/feedback.asp" -->
	</td>
	<td width="50">&nbsp;</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>	
	</table>
	<% 'Not Inserted
	Else %>
		<html>
			<head>
				<title>Request Registration Error</title>
			</head>
		
		<body leftmargin="0" topmargin="0">
  
			<%' Surrounding Table %>
			<table border="0" cellspacing="0" cellpadding="0" height="100%">
				<tr>
					<td width="75">
						<font face="verdana" size="2">
						There was an error in registering your request.<br>
						Please contact LeCroy.<br>
						Thank you.
					</td>
					</small>
				</tr>
			</table>
			<%End if%>
		

<%
'There is no selection or max number has been over passed
Else %>
	<html>
	<head>
	<title>Processing Request Error</title>
	</head>
	<body leftmargin="0" topmargin="0">
  
	<%' Surrounding Table %>
	<table border="0" cellspacing="0" cellpadding="0" height="100%">
		<tr>
			<td width="75">
				<table width="500" border="0" cellspacing="0" cellpadding="0" height="100%">
					<tr>
						<td><font face="verdana" size="2">Registration Error
						<%' Enter Text Here %></font>
			            </td>
			        </tr>
			    </table>
			</td>
		</tr>
		<tr>
    		<td valign="top" width="100%" height="100%">
	<font face="Verdana,Arial,Helvetica" size="2">
	<%If Not SelectionMadeByUser Then%>
		No selection has been made.<br>
	<%End If%>
	
	<%If LitSelectionNumberOK = "TOO MANY" Then%>
	We're sorry, the maximum number of requests for <strong>Literature</strong> is <strong><% = LiteratureMaxAllowedSelections %></strong><br>
	<%End If%>
	
	<%If DemoSelectionNumberOK = "TOO MANY" Then%>
	We're sorry, the maximum number of requests for <strong>Demo</strong> is <strong><% = DemoMaxAllowedSelections %></strong><br>
	<%End If%>

	<%If QuoteSelectionNumberOK = "TOO MANY" Then%>
	We're sorry, the maximum number of requests for <strong>Quote</strong> is <strong><% = QuoteMaxAllowedSelections %></strong><br>
	<%End If%>

	<%If HelpSelectionNumberOK = "TOO MANY" Then%>
	We're sorry, the maximum number of requests for <strong>Help</strong> is <strong><% = HelpMaxAllowedSelections %></strong><br>
	<%End If%><br>
	<%If (Request.QueryString("Object5")= 1) AND LEN(Session("Phone")) = 0 Then  %>
		You have not provided us with your phone number, please click on <b>Change Address</b> and add your phone number.<br>
	<%End if%>
	<%'Added by Adrian 3/30/99 to accomodate Seminars %>
	<%If (Request.QueryString("Object6")= 1) AND LEN(Session("Phone")) = 0 Then  %>
		You have not provided us with your phone number, please click on <b>Change Address</b> and add your phone number.<br>
	<%End if%>
	<%'End of Code Added %>
	
	Please go back and check your selection.
	</font>
	<form action="RequestInfoWM.asp" method="get" id=form1 name=form1>
	<input type="submit" value="Go Back" id=submit1 name=submit1>
	</form>
	</td>
	</tr>
	</table>
	<!--#include virtual="/images/Footer/footer.asp" -->
	</body>
	</html>	
<%End If%>





