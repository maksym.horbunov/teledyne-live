<%'@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%Session("PropProdCategory")=0%>

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="LeCroy Web Site Visitor Private Policy" ' Title of Web Page
	PageDescription="LeCroy strongly believes in protecting your privacy, and any personal information you share with us. We request information from you on our website only for the purpose of serving you better. If you choose to provide information, we will not sell that information to anyone else." 'Description of Page
	PageKeywords="LeCroy, privacy policy, visitor private policy, private, policy, private policy, web, website, unsubscribe, security, cookies, cookie, site, registration, product registration, web registration, acceptable use policy, use policy, " ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">

<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2" &nbsp;</td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top">&nbsp;</td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td align="right" valign="top">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top"></td>
			  <td width="90%" valign="top">
				<%'Whats New Table %>
				<table border="0" width="500" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" align="left" height="38">
                            <font size="2" face="Verdana"><a href="javascript:window.history.go(-1);">Previous Page<br>
                            </a>&nbsp;</font>
                            <table border="0" width="100%" cellpadding="0">
                              <tr>
                                <td width="58">
                                </td>
                                <td><font face="verdana,arial,helvetica" size="4" color="#3366FF">LeCroy Website Visitor Privacy Protection and Acceptable Use Policy<br>
                                  </font>
                                  <font face="verdana,arial,helvetica" size="2" color="#000000">(effective
                                  December 2004)</font><font face="verdana,arial,helvetica" size="4" color="#3366FF">&nbsp;&nbsp;</font>
                                </td>
                              </tr>
                            </table>
            <p><font face="verdana,arial,helvetica" size="2">By visiting this web site, you accept the provisions of these Privacy and Acceptable Use Statements.</font> </p>
            <p><strong><font face="verdana,arial,helvetica" size="2">LeCroy's Privacy Pledge:</font></strong><strong><font face="verdana,arial,helvetica" size="2"><br></font> </strong><font face="verdana,arial,helvetica" size="2">LeCroy strongly believes in protecting your privacy, and any personal information you share with us. We request information from you on our website only for the purpose of serving you better. If you choose to provide information, we will not sell that information to anyone else.&nbsp;</font> </p>
            <p><strong><font face="verdana,arial,helvetica" size="2">Unsubscribe Pledge:</font></strong><font face="verdana,arial,helvetica" size="2"><br> 
            If you choose not to receive electronic communications from LeCroy ('opt-out'), or choose not to receive a specific category of communication ('unsubscribe'), your choice will be honored promptly.<br>
            <br>
            Our commitment to your privacy goes beyond regulatory requirements. We will not provide any of your information, including your opt-out choices, to anyone else, unless compelled by law.�<br>
            <br>
            Note that you may receive information about LeCroy from other sources, such as trade journals, if you have not opted-out of their communication streams. 
            </font></p>
            <p><strong><font face="verdana,arial,helvetica" size="2">Information Security:<br>
 </font></strong><font face="verdana,arial,helvetica" size="2">We have implemented a variety of technical, physical and administrative security measures to protect your personal information from improper use or unauthorized access. LeCroy enforces strict information security policies to insure the security of our customer's DSOs. On our website we make sure no one else uses your identity through a process of password authentication. The minimum number of characters required for this purpose is 5. The maximum is 15.&nbsp;</font></p>
            <p><strong><font face="verdana,arial,helvetica" size="2">Cookies:&nbsp;<br> 
            </font></strong><font face="verdana,arial,helvetica" size="2">To help us recognize you as a return visitor to our site, we place a simple line of recognition text on your hard drive. When you come back to request a demo or to download software, after verifying your username and password, the registration forms will automatically be completed for you.�<br>
            <br>
            Cookied visitors can download materials, get instant quotes, and request literature and CDs without filling out long forms. To learn more about cookies we encourage you to visit
            <a href="http://www.cookiecentral.com" onclick="GaEventPush('OutboundLink', 'www.cookiecentral.com');">www.cookiecentral.com</a> .<br>
            <br>
            LeCroy's cookie is completely harmless ... it contains no executable code.&nbsp;</font></p>
            <p><strong><font face="verdana,arial,helvetica" size="2">Contact Information:&nbsp; <br> 
            </font></strong><font face="verdana,arial,helvetica" size="2">If you request LeCroy contact you for a demo, quote, or a salesperson's call, please make sure you've filled in either a phone number or email where we can reach you.�<br>
            <br>
            If you have questions, wish to send us comments about this Privacy Policy, or would like a copy of your personal information on file, please send an e-mail with your questions or comments to
            <a href="mailto:Webmaster@teledynelecroy.com"> Webmaster@teledynelecroy.com</a> or write us at:&nbsp;<br>
            <br>
            LeCroy Corporation<br>
            Attention:      Webmaster<br>
            700 Chestnut Ridge Road<br>
            Chestnut Ridge, New York 10977.</font></p>
                            <p class="SmallBody" align="left"><strong><font face="verdana,arial,helvetica" size="2">Personally Identifiable Data:&nbsp;&nbsp; <br> 
            </font></strong><font face="verdana,arial,helvetica" size="2">LeCroy will not request personally identifiable data from you online (such as social security number, driver�s license number, bank account or credit card number or password).�<br>
                            <br>
                            On our website we request only data that enables us to provide you with information helpful in solving your test and measurement challenges; name and address, job title and function, email, website and fax details, and your test, measurement, or analysis application.</font></p>
                            <p class="SmallBody" align="left"><strong><font face="verdana,arial,helvetica" size="2">Privacy Policy Changes:&nbsp; <br> 
            </font></strong><font face="verdana,arial,helvetica" size="2">All material changes to LeCroy�s Privacy Protection Policy will be posted on this page. The revision date shown at the top of the page will be updated accordingly.</font></p>
                            <p class="SmallBody" align="left"><strong><font face="verdana,arial,helvetica" size="2">Acceptable Use:&nbsp; <br> 
            </font></strong><font face="verdana,arial,helvetica" size="2">LeCroy's website is provided for the use of the Test and Measurement community in the furtherance of the field.  Those who visit our site out of interest in measurement equipment, and those considering an investment in, or employment by, LeCroy, are welcome.  Visits to the site for other purposes without the consent of LeCroy are prohibited.  Prohibited uses include any form of unsolicited testing, changes to site contents or links, malicious acts, and any acts which have the effect of denying access to this site to other users.  Intentional damage to the site or its users will be prosecuted.&nbsp;<br>
                            <br>
                            Nothing contained in this web site may be reproduced without LeCroy's written permission.</font></p>
                            <p class="SmallBody" align="left"><font face="verdana,arial,helvetica" size="2">The content of this web site is provided "as is", without warranty.</font></p>
                            <p class="SmallBody" align="left"><font face="verdana,arial,helvetica" size="2">&nbsp;</font></p>
						</td>
						<td valign="top" rowspan="2" align="left">
						</td>
					</tr>
					<tr>
						<td valign="top" height="70">
						</td>
					</tr>
				</table>
			  </td>
			  <td></td>
			  <td valign="top" align="left">
			  <br>
			  </td>
			</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html>
