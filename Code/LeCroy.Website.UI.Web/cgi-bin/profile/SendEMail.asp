<%'@ Language=VBScript %>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<% ServerName = Request.ServerVariables("SERVER_NAME")
if len(ServerName)>0 then
	if cstr(ServerName)="ny-www1" then
		ServerName="teledynelecroy.com"
	end if
end if %>
<%'***************DO NOT CHANGE SERVER NAME ********************%>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="cgi-bin/profile/allfunctions.asp"-->
<%Session("PropProdCategory")=0%>

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	contry_id=208
	topic_id=1
	division_id=1
	'**************Added by YK 4/10/2003	*************
	if len(Session("localeid"))=0 then Session("localeid")=1033
	localeid=Session("localeid")
	'************************************************
%>

<html>
<head>
<script LANGUAGE="JavaScript">
		function isReady(form) {
			if (form.EMail.value!="") return true;
			else {
			alert("Please type your E-Mail Address");
			form.EMail.focus( );
			return false;
			}
		}	
	</script>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>

<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->
<table width="100%" cellpadding="0" cellspacing="0" border=0>
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2"  >
				  <%if Session("localeid")=1041 then%>
		  					<!--#include virtual="/japan/menus/navi/pro.asp" --> 
					<%end if%>
			  </td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top">&nbsp; </td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td width="10" valign="top">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td width="90%" valign="top">
				<%'Whats New Table %>
				<table border="0" width="480" cellspacing="0" cellpadding="0" height="108">
					<tr>
						<td valign="top" height="38">
	<%		
	if Session("localeid")=1033 then
		Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
	else
		Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
	end if
	%>      
	<p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="2">
	<% 
	if len(trim(Request.form("Email"))) >0 then
		EMail=trim(Request.form("Email"))
	else
		EMail=trim(Request.QueryString("Email"))
	end if
    if len(EMail)>0 then                      
		Set rs = Server.CreateObject("ADODB.Recordset")
		set cmdSendPassword = createobject("ADODB.Command")
		cmdSendPassword.ActiveConnection = dbconnstr()
		cmdSendPassword.CommandText = "sp_emailusernamepassword"
		cmdSendPassword.CommandType =4	' 4 = stored procedure
		set ParamEmail=cmdSendPassword.CreateParameter("@EMAIL",200,1,120)
		cmdSendPassword.Parameters.Append ParamEmail
		cmdSendPassword.Parameters("@EMAIL")= replace(EMail,"'","''")
		set rs=	cmdSendPassword.Execute
		if rs.EOF or BOF then
			Response.Write "<form action=regform.asp id=form1 name=form1><input type=submit value='" & LoadI18N("SLBCA0436",localeID) & "' id=submit1 name=submit1></form>"
			Response.Write LoadI18N("SLBCA0438",localeID)
		%>
		<form action="SendEMail.asp" onSubmit="return isReady(this)" method="get" id="form1" name="form1">
		<input type="text" name="EMail" maxlength="100">
		<input type="submit" value="<%=LoadI18N("SLBCA0437",localeID)%>" id="submit1" name="submit1">
		</form>
		<%
		else
			Response.Write LoadI18N("SLBCA0444",localeID)
			if Session("localeid")<>1041 then
				Response.Write "<form action=senttoreg.asp method=post id=form1 name=form1>"
				Response.Write "<font size=""2"" face=""verdana,arila,helvetica"">Once you have registered with the LeCroy website, if you would like to change your Username and Password, go to your user profile and look for the reset buttons.</font>"	
			else
				Response.Write "<form action=""/japan/"" method=post id=form1 name=form1>"
			end if
			Response.Write "<p><input type=submit value='" & LoadI18N("SLBCA0235",localeID) & "' id=submit1 name=submit1>"
			Response.Write "</p></form>"
		'Create E-Mail Body
			strCRet = "<br>"
			if Session("localeid")<>1041 then
				strBody = strBody & "<font face=""Verdana, Arial"" size=2>LeCroy's User Profile Center" & strCret & strCret
				strBody = strBody & "You requested your UserName & Password." & strCret & strCret
				strBody = strBody & "Please click the link below to view your information:" & strCret& strCret
				strBody = strBody & "<font face=""Verdana, Arial"" size=2><a href=""http://" & ServerName & "/cgi.asp?cid=" & rs("CONTACT_ID") & "&c=" & rs("CONTACT_WEB_ID") &  "&lid=" & localeID & """>http://" & ServerName & "/cgi.asp?cid=" & rs("CONTACT_ID") & "&c=" & rs("CONTACT_WEB_ID") &  "&lid=" & localeID &  "</a></font>" & strCret & strCret 
				strBody = strBody & "Your UserName & Password provide you with access to specific sections of LeCroy's Site and LeCroy's FREE Software." & strCret& strCret
				strBody = strBody & "Sincerely," & strCret & strCret
				strBody = strBody & "LeCroy User Profile Center</font>" 
			else
				strBody = strBody & "<img src=""" & "http://" & ServerName & "/images/misc/Pwd_request_ja1.gif"">" & strCret & strCret
				strBody = strBody & "<font face=""Verdana, Arial"" size=2><a href=""http://" & ServerName & "/cgi.asp?cid=" & rs("CONTACT_ID") & "&c=" & rs("CONTACT_WEB_ID") &  "&lid=" & localeID & """>http://" & ServerName & "/cgi.asp?cid=" & rs("CONTACT_ID") & "&c=" & rs("CONTACT_WEB_ID") &  "&lid=" & localeID &  "</a></font>" & strCret & strCret 
				strBody = strBody & "<img src=""" & "http://" & ServerName & "/images/misc/Pwd_request_ja2.gif"">" & strCret & strCret
			end if
			Set oMailMessage = Server.CreateObject("CDO.Message")
			oMailMessage.To = EMail
			oMailMessage.From = "webmaster@teledynelecroy.com"
			oMailMessage.BCC = "kate.kaplan@teledynelecroy.com"
			oMailMessage.Subject ="LeCroy Registration"
			oMailMessage.HTMLBody = "<html><body><h1>" + strBody +"</h1></body></html>"
			oMailMessage.Send 
			Set oMailMessage= Nothing	
	end if
else
			Response.Write "<form action=regform.asp id=form1 name=form1><input type=submit value='" & LoadI18N("SLBCA0436",localeID) & "' id=submit1 name=submit1></form>"
			Response.Write LoadI18N("SLBCA0444",localeID)
		%>
		<form action="SendEMail.asp" onSubmit="return isReady(this)" method="get" id="form1" name="form1">
		<input type="text" name="EMail" maxlength="100">
		<input type="submit" value="<%=LoadI18N("SLBCA0437",localeID)%>" id="submit1" name="submit1">
		</form>
<%end if%>	


                            
                            </font></p>
                            <p class="SmallBody" align="left">&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td valign="top" height="70">
						</td>
					</tr>
				</table>
			  </td>
			  <td>&nbsp;</td>
			  <td valign="top" align="right">&nbsp;</td>
			</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
<!--#include virtual="/images/Footer/footer.asp" -->
</body>
</html>


