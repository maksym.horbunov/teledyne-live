<%@ LANGUAGE="VBSCRIPT" %>
<!--#include virtual="/include/dbconnstr.asp"-->
<!--#include virtual="cgi-bin/profile/allfunctions.asp"-->
<%
			'**************Added by YK 5/20/2003	*************
			if len(Session("localeid"))=0 then Session("localeid")=1033
			localeid=Session("localeid")
			'************************************************
' If we have both username and password

If (Len(Request.Form("UserName")) > 0) And (Len(Request.Form("Password")) >0) Then
	ContactIdTemp = GetContactIDOnUserNameAndPwd(Request.Form("UserName"), Request.Form("Password"))

     If isnumeric(ContactIdTemp)  Then
    
		if clng(ContactIdTemp)>0 then
			' If he accepted the cookie
			GUIDFromDB = GetGUIDOnContactId(ContactIdTemp)
			Session("ContactId") = ContactIdTemp
			GetUserData(GUIDFromDB)
			Response.Cookies("LCDATA")("GUID") = GUIDFromDB
			Response.Cookies("LCDATA").Expires = Date + 365 * 5
			sAgent = Request.ServerVariables("HTTP_USER_AGENT")
			If Len(Session("RedirectTo")) = 0  Then 
				if Session("localeid")=1041 then
					Response.Redirect "/japan/default.asp"
				else
					Response.Redirect "/default.asp"
				end if
			end if

			%>
			
			
			<!--#include virtual="/include/global.asp" -->

			<%	'**************Every Page must include the header.asp file******************
				'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
				PageTitle="Welcome Back" ' Title of Web Page
				PageDescription="LeCroy User Registration Form" 'Description of Page
				PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
				banner="testmeasurement" 'other available - see banners.txt for more details
				topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
				bgimage="" 'sets the background image for the document
				language="us" ' Sets the language of the current page
				country_id=208
				division_id=1
				topic_id=1
			%>
			<%'Start of First Page HTML %>
			<html>
			<head>
			<meta NAME="Title" Content="<% = PageTitle %>">
			<meta NAME="Description" Content="<% = PageDescription %>">
			<meta NAME="keywords" Content="<% = PageKeywords %>">
			<!--#include virtual="/include/meta_info.asp" -->
			<title><% = PageTitle %></title>
					<meta NAME="Title" Content="<% = PageTitle %>">
					<meta NAME="Description" Content="<% = PageDescription %>">
					<meta NAME="keywords" Content="<% = PageKeywords %>">
					<!--#include virtual="/include/meta_info.asp" -->
					<title><% = PageTitle %></title>
			</head>
			<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
			<a NAME="TopofPage"></a>	

			<%'Start of We Found You Page %>
			<a NAME="TopofPage"></a>
			<!--#include virtual="/include/Header.asp" -->
			<table width="100%" cellpadding="0" cellspacing="0" ID="Table1">
			<tr>
				<td valign="top">
					<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table2">
						<tr height="10">
						<td width="100" valign="top" rowspan="2" >
					  <%if cint(Session("localeid"))=1041 then%>
		  					<!--#include virtual="/japan/menus/navi/pro.asp" --> 
					<%end if%>
						</td>
						<td valign="top">&nbsp&nbsp;&nbsp;&nbsp;</td>
						<td valign="top"><br>
								
						</td>
						<td width="10" valign="top">&nbsp;</td>
						<td align="right" valign="top">
								<br>
						</td>
						</tr>
						<tr>
						<td valign="top"></td>
						<td width="90%" valign="top">
							<%'Whats New Table 
							
							Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
							Response.Write "<tr>"
							Response.Write "<td valign=""top"" align=""left"" height=""38"">"
 
							if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042  then
								Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
							else
								Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
							end if
	
							
							if cint(Session("localeid"))<>1041 then%>
							    <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="3"><% = Session("FirstName")%>, <%=LoadI18N("SLBCA0244",Session("localeid"))%>! <%=LoadI18N("SLBCA0245",Session("localeid"))%><br>
							<%else%>
							   <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="3"><% = Session("LastName")%>&nbsp;<% =  Session("Firstname")%>&nbsp;<% = LoadI18N("SLBCA0160",Session("localeid"))%>&nbsp;<%=LoadI18N("SLBCA0244",Session("localeid"))%><br><%=LoadI18N("SLBCA0245",Session("localeid"))%><br>
							<%
							end if
							'End of We Found You Page %>
							<form action="<% = Session("RedirectTo")%>" method="post" id="form1" name="form1">
			<%else
			
				Response.Redirect "RegisterAgain.asp"
			end if
		 ' He didn't accept the cookie
		Else
		%>
						
			
			<!--#include virtual="/include/global.asp" -->

			<%	'**************Every Page must include the header.asp file******************
				'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
				PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
				PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
				PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
				banner="testmeasurement" 'other available - see banners.txt for more details
				topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
				bgimage="" 'sets the background image for the document
				language="us" ' Sets the language of the current page
			%>
                               </font></font>
                        </table>
            </table>
					<a NAME="TopofPage"></a>
					<!--#include virtual="/include/header.asp"-->
					<table width="100%" cellpadding="0" cellspacing="0" ID="Table3">
					<tr>
						<td valign="top">
							<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table4">
								<tr height="10">
								<td width="175" valign="top" rowspan="2" >
							  <%if cint(Session("localeid"))=1041 then%>
		  							<!--#include virtual="/japan/menus/navi/pro.asp" --> 
							<%else%>
									<!--#include virtual="/shopper/menu/prodpropcategory.asp" -->
							<%end if%>
								</td>
								<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td valign="top"><br>
										
								</td>
								<td width="10" valign="top">&nbsp;</td>
								<td align="right" valign="top">
										<br>
								</td>
								</tr>
								<tr>
								<td valign="top"></td>
								<td width="90%" valign="top">
									<%'Whats New Table %>
									<table border="0" width="480" cellspacing="0" cellpadding="0" height="108" ID="Table5">
										<tr>
											<td valign="top" align="right" height="38">
											<%
											if Session("localeid")=1033 then
												Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """ WIDTH=""500"" HEIGHT=""37"">"
											else
												Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """ WIDTH=""500"" HEIGHT=""37"">"
											end if
											%>
											<p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="1">
											<!-- #INCLUDE FILE="CookieNotAccepted.asp" -->
											<form action="senttoreg.asp" method="post" id="form2" name="form2">
						<%End If%>
				<input type="submit" value="<%=LoadI18N("SLBCA0170",Session("localeid"))%>" id="submit1" name="submit1"></p>
				</form>
				</font></p>
				<p class="SmallBody" align="left">&nbsp;</p>
				</td>
				</tr>
				<tr>
				<td valign="top" height="70">
				</td>
				</tr>
				</table>
				</td>
				<td></td>
				<td valign="top" align="right"><br></td></tr>
				</table>
				</td>
				</tr>
				</table>
				<a NAME="BottomofPage"></a>
				<!--#include virtual="/images/Footer/footer.asp" -->
				</body>
				</html>
<%'End of Page We Found the User Code %>

<%'We did not find the user
	'Else
		Session("TriedToGetContactIdbyUsrAndPwd") = True
		'If Session("UserFrom") = "Japan" then
		'	Response.Redirect "https://teledynelecroy.com/cgi-bin/Profile/SentToRegj.asp"
		'else
		'	Response.Redirect "/cgi-bin/Profile/SentToReg.asp"
		'end if
	'End If
'Incomplete request username or password or both missing
 Else
	Session("TriedToGetContactIdbyUsrAndPwd") = True
	Response.Redirect "SentToReg.asp" %>
<%End If%>
