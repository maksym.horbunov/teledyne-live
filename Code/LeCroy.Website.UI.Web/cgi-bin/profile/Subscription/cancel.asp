<%'@ Language=VBScript %>
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%validateusernoredir()%>
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	country_id=208
if len(Session("localeid"))=0 then
	Session("localeid")=1033
end if
localeid=Session("localeid")
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">



<title><% = PageTitle %></title>
</head>
<script LANGUAGE="JavaScript">
<!--
//Check E-Mail address : look for [@] and [.]
function isEmail(elm){
	if (elm.value.indexOf("@") != "-1" &&
		elm.value.indexOf(".") != "-1" )
	{
	
	var elm=elm.value;
	
	elm=replace(elm,"@","");
	elm=replace(elm,".","");
	elm=replace(elm," ","");
	if (elm!="")
	{
	return true;
	}
	else
	{ return false;}
	}
	else return false;
}

//Check for null and for empty
function isFilled(elm) {
	if (elm.value =="" ||
		elm.value ==null)
	return false;
	else return true;
}

function isReady(form) {
	if (isEmail(form.EMAIL) == false){
	alert("Please enter your e-mail address");
	form.EMAIL.focus();
	return false;
	}
return true
}
function uncheck_all()
{for (i=0; i<document.form.subscription.length; i++){
document.form.subscription[i].checked=false;}
 }
function replace(string,text,by)
 {
// Replaces text with by in string
    var strLength = string.length;
    var txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return string;

    var i = string.indexOf(text);
    if ((!i)&&(text != string.substring(0,txtLength))) return string;
    if (i == -1) return string;

    var newstr = string.substring(0,i) + by;

    if (i+txtLength < strLength)
        newstr +=replace(string.substring(i+txtLength,strLength),text,by);

    return newstr;
}

//-->
</script>
<body>
<%
Response.Write Request.Form ("loadsubs")

%>

<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->	
		<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table1">
			<tr>
			  <td valign="top" width=15>&nbsp;&nbsp;</td>
			  <td width="70%" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" ID="Table2">
					<tr>
						<td valign="top" align="left" >
						<%Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"%>
						<font face="Verdana,Arial,Helvetica" size="2">
                            <table border="0" cellpadding="4" cellspacing="0" width="100%" ID="Table3">
                              <tr>
                                <td valign="top">
								<%if len(session("ContactId"))>0 then%>
									<strong><% = Session("FirstName") %>, <%=LoadI18N("SLBCA0244",localeID)%>.<br><br>  
									<hr size="1" color="RoyalBlue">
									</font><br>
									<%end if
									Response.Write "<form action=""save.asp""  method=""post"" id=""form"" name=""form"" onSubmit=""return isReady(this)"";>"
									'Response.Write "<input type=""hidden"" name=""REMOVE"" value=""yes"" ID=""Hidden5"">"
									'Response.Write "To unsubscribe, please select &nbsp;<input type=""submit"" value=""unsubscribe"" id=""Submit15"" name=""submit15"" >"
									%>
									<!--<input type="hidden" name="EMAIL" maxlength="100" size="36" value="<%=Session("Email")%>" ID="Text3">
									<input type="hidden" name="SUB_ID" value="<%=GenerateUserID%>" ID="Hidden10">
									<input type="hidden" name="DATE_ENTERED" value="<% = date %>" ID="Hidden11">
									<input type="hidden" name="CONTACT_ID" value="<% = session("ContactId") %>" ID="Hidden12">
									<input type="hidden" name="COUNTRY_ID" value="<% = country_id %>" ID="Hidden13">-->
									<%	
									Response.Write "<font face=""Verdana,Arial"" size=2 color=""#000000""><b>To unsubscribe, please make your selections below and save changes.</b></font><br><br>"
								'else
									'Response.Write "To unsubscribe, please enter your email address below.  A confirmation email will be sent to you shortly.<br><br>"
								'	Response.Write "To unsubscribe, please make your selections below and save changes.<br><br>"
								'end if%>
                            </font><br>
                           <font face="Verdana,Arial,Helvetica" size="2">

                           <%'Email form to subscribe to email information %>
                          
	
						<%
						'if len(session("ContactId"))>0 then
							set rsSubType=server.CreateObject("ADODB.Recordset")
							strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y' and COUNTRY_ID=" & country_id 
							rsSubType.Open strSQL,dbconnstr()
							if not rsSubType.EOF then
								'if len(session("ContactId"))>0 then
									Response.Write "<font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>Yes, I would like to receive:</b></font><br>"
								'else
								'	Response.Write "<font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0382",localeid) & "</b></font>"
								'end if
								Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
							
								while not rsSubType.EOF
								count=count+1
									if len(Session("Email"))>0 then
									
										if checksubscriptionexist(rsSubType("DOCUMENT_ID")) then
										'check what value we have in DISABLE column
											if checksubscriptiondisable(rsSubType("DOCUMENT_ID")) then
												strChecked=" checked "
											else
												strChecked=""
											end if
										else
											if UCASE(rsSubType("DISPLAY_YN"))="Y" then
												strChecked=" checked "
											else
												strChecked=""
											end if
										end if
									else
										if UCASE(rsSubType("DISPLAY_YN"))="Y" then
											strChecked=" checked "
										else
											strChecked=""
										end if
									end if
									subcount=subcount+1
									Response.Write "<tr>"
									Response.Write "<td valign=""top"" width=""30"">"
									Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription' " & strChecked & ">"
									Response.Write "</td>"
									Response.Write "<td valign=""middle"" align=left>"
									Response.Write "<font face=""Verdana,Arial"" size=2 color=""#000000""><nobr><b>" &  rsSubType("NAME") &  "</b></nobr></font>"
									Response.Write "</td>"
									Response.Write "</tr>"
									
									redim preserve SubTypeID(subcount)				
									SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")

								rsSubType.MoveNext
								wend
									Response.Write "<tr>"
									Response.Write "<td valign=""top"" colspan=2>"
									if cint(count)>1 then
										Response.Write "<input type=button value='Clear All' onclick=uncheck_all();>"
									else
										Response.Write "<input type=button value='Clear All' onclick=document.form.subscription.checked=false;>"
									end if
									Response.Write "</td>"
									Response.Write "</tr>"
								Response.Write "</table><br>"
							'end if

				'ask customer in which format he prefer to recive an email
				set rsSubFormat=server.CreateObject("ADODB.Recordset")
				strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y' order by FORMAT_ID desc"
				rsSubFormat.Open strSQL,dbconnstr()
				if not rsSubFormat.EOF then
					Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
					Response.Write "<tr>"
					Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
					Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font><br>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td  colspan=2 valign=""top""  align=""left"">"
					Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
					while not rsSubFormat.EOF
						if cint(getsubscriptionformat(Session("email")))=cint(rsSubFormat("FORMAT_ID")) then
							strSelected="CHECKED"
						else
							if cstr(rsSubFormat("NAME"))="HTML" then
								strSelected="CHECKED"
							else
								strSelected=""
							end if
						end if
						Response.Write "<tr>"
						Response.Write "<td valign=""top"" width=""30"">"
						Response.Write "<input type=radio name='subformat' id='subformat' value=""" & rsSubFormat("FORMAT_ID")  & """ " & " " & strSelected & ">"
						Response.Write "</td>"
						Response.Write "<td valign=""middle"">"
						Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
						Response.Write "</td>"
						Response.Write "</tr>"
						rsSubFormat.MoveNext
					wend
							
					Response.Write "</table>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "</table><br><br>"
				end if	

							
			Session("SelectedSubCount")	=subcount		
			Session("SubCount")=subcount
			Session("SubTypeID") = SubTypeID	
		
						Response.write "<font color=""#4169E1""><b>Email:</b></font><br>"%>
						<input type="text" name="EMAIL" maxlength="100" size="36" value="<%=Session("Email")%>" ID="Text2">
						<input type="hidden" name="SUB_ID" value="<%=GenerateUserID%>" ID="Hidden1">
						<input type="hidden" name="DATE_ENTERED" value="<% = date %>" ID="Hidden2">
						<input type="hidden" name="CONTACT_ID" value="<% = session("ContactId") %>" ID="Hidden3">
						<input type="hidden" name="COUNTRY_ID" value="<% = country_id %>" ID="Hidden4">
						<input type="submit" value="Save Changes" id="submit1" name="submit1">
        <%else
						Response.write "<font color=""#4169E1""><b>Email:</b></font><br>"%>
						<input type="text" name="EMAIL" maxlength="100" size="36" value="<%=Session("Email")%>" ID="Text1">
						<input type="hidden" name="SUB_ID" value="<%=GenerateUserID%>" ID="Hidden5">
						<input type="hidden" name="DATE_ENTERED" value="<% = date %>" ID="Hidden6">
						<input type="hidden" name="CONTACT_ID" value="<% = session("ContactId") %>" ID="Hidden7">
						<input type="hidden" name="COUNTRY_ID" value="<% = country_id %>" ID="Hidden8">
						<input type="hidden" name="REMOVE" value="yes" ID="Hidden9">
						<input type="submit" value="Unsubscribe" id="Submit2" name="submit1">
        <%end if%>
                          </form>
                           
                            </font></td>
                                  </tr>
                            </table>
                            <font face="Verdana,Arial,Helvetica" size="2">
                            
                           <%'End of Form %>
                            
                            </font>
  
						</td>

					</tr>
	
				</table>
			  </td>
			<td>&nbsp;&nbsp;</td>
			</tr>
		
    </table>

<!--#include virtual="/images/footer/footer.asp" -->
</body>
</html>
