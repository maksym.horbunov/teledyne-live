<%'@ Language=VBScript %>
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%
validateusernoredir()%>
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	country_id=208
if len(Session("localeid"))=0 then
	Session("localeid")=1033
end if
localeid=Session("localeid")
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">



<title><% = PageTitle %></title>
</head>
<script LANGUAGE="JavaScript">
<!--
//Check E-Mail address : look for [@] and [.]
function isEmail(elm){
	if (elm.value.indexOf("@") != "-1" &&
		elm.value.indexOf(".") != "-1" )
	{
	
	var elm=elm.value;
	
	elm=replace(elm,"@","");
	elm=replace(elm,".","");
	elm=replace(elm," ","");
	if (elm!="")
	{
	return true;
	}
	else
	{ return false;}
	}
	else return false;
}

//Check for null and for empty
function isFilled(elm) {
	if (elm.value =="" ||
		elm.value ==null)
	return false;
	else return true;
}

function isReady(form) {
	if (isEmail(form.EMAIL) == false){
	alert("Please enter your e-mail address");
	form.EMAIL.focus();
	return false;
	}
return true
}

function replace(string,text,by)
 {
// Replaces text with by in string
    var strLength = string.length;
    var txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return string;

    var i = string.indexOf(text);
    if ((!i)&&(text != string.substring(0,txtLength))) return string;
    if (i == -1) return string;

    var newstr = string.substring(0,i) + by;

    if (i+txtLength < strLength)
        newstr +=replace(string.substring(i+txtLength,strLength),text,by);

    return newstr;
}

//-->
</script>
<body>

<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->	
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
			  <td valign="top" width=15>&nbsp;&nbsp;</td>
			  <td width="70%" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<td valign="top" align="left" >
						<%Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"%>
						<font face="Verdana,Arial,Helvetica" size="2">
                            <table border="0" cellpadding="4" cellspacing="0" width="100%">
                              <tr>
                                <td valign="top">
								<%if len(session("ContactId"))>0 then%>
									<strong><%=LoadI18N("SLBCA0244",localeID)%>&nbsp;<% = Session("FirstName") %></strong><br><br>  
									<hr size="1" color="RoyalBlue">
									</font><br>
									<strong><font color="#4169E1">Update profile</font></strong><br><br>
									<%
									
									'Response.Write "<form action=""/cgi-bin/profile/modform.asp""  method=""get"" id=""Form6"" name=""form6"">"
									'Response.Write "<input type=""submit"" value=""Update profile"" id=""Submit7"" name=""submit7"">"
									'Response.Write "</form><br>"
									%>
									Please review your current subscriptions below.<br>
								<%else
								Session("RedirectTo") = "/cgi-bin/Profile/Subscription/default.asp" 'here is the name of the file itself
								Response.Write "<form action=""/cgi-bin/profile/senttoreg.asp#reg""  method=""get"" id=""Form4"" name=""form4"">"
								Response.Write "Registered User?<br>"
								Response.Write "Click here to sign in&nbsp;<input type=""submit"" value=""Sign In"" id=""Submit6"" name=""submit6"">"
								Response.Write "</form><br>"
								Response.Write "<form action=""/cgi-bin/profile/senttoreg.asp#newreg""  method=""get"" id=""Form5"" name=""form5"">"
								Response.Write "Not registered?<br>"
								Response.Write "Register Here&nbsp;<input type=""submit"" value=""" & LoadI18N("SLBCA0144",localeID) & """ id=""Submit5"" name=""submit5"">"
								Response.Write "</form><br>"
								Response.Write "If you prefer not to register with LeCroy.com but would like to receive email from LeCroy please enter your subscription choices and email address below.<br><br>"

								end if%>
                            </font><br>
                           <font face="Verdana,Arial,Helvetica" size="2">

                           <%'Email form to subscribe to email information %>
                          
                        <form action="save.asp" name="form" method="post" onSubmit="return isReady(this)">
						<%
						set rsSubType=server.CreateObject("ADODB.Recordset")
						strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y' and COUNTRY_ID=" & country_id 
						rsSubType.Open strSQL,dbconnstr()
						if not rsSubType.EOF then
							if len(session("ContactId"))>0 then
								Response.Write "<font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0435",localeid) & "</b></font><br>"
							else
								Response.Write "<font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>Yes, I would like to receive:</b></font>"
							end if
							Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
						
							while not rsSubType.EOF
							'Response.Write len(Session("Email"))
								if len(Session("Email"))>0 then
								
									if checksubscriptionexist(rsSubType("DOCUMENT_ID")) then
									'check what value we have in DISABLE column
										if checksubscriptiondisable(rsSubType("DOCUMENT_ID")) then
											strChecked=" checked "
										else
											strChecked=""
										end if
									else
										if UCASE(rsSubType("DISPLAY_YN"))="Y" then
											strChecked=" checked "
										else
											strChecked=""
										end if
									end if
								else
									if UCASE(rsSubType("DISPLAY_YN"))="Y" then
										strChecked=" checked "
									else
										strChecked=""
									end if
								end if
								subcount=subcount+1
								Response.Write "<tr>"
								Response.Write "<td valign=""top"" width=""30"">"
								Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription" & rsSubType("DOCUMENT_ID") & "' " & strChecked & ">"
								Response.Write "</td>"
								Response.Write "<td valign=""middle"">"
								Response.Write "<font face=""Verdana,Arial"" size=2 color=""#000000""><nobr><b>" &  rsSubType("NAME") &  "</b></nobr></font>"
								Response.Write "</td>"
								Response.Write "</tr>"
								
								redim preserve SubTypeID(subcount)				
								SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")

							rsSubType.MoveNext
							wend
							
							Response.Write "</table><br><br>"
						end if

			'ask customer in which format he prefer to recive an email
			set rsSubFormat=server.CreateObject("ADODB.Recordset")
			strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y' order by FORMAT_ID desc"
			rsSubFormat.Open strSQL,dbconnstr()
			if not rsSubFormat.EOF then
				Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
				Response.Write "<tr>"
				Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
				Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font><br>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
				Response.Write "<td  colspan=2 valign=""top""  align=""left"">"
				Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
				while not rsSubFormat.EOF
					if cint(getsubscriptionformat(Session("email")))=cint(rsSubFormat("FORMAT_ID")) then
						strSelected="CHECKED"
					else
						if cstr(rsSubFormat("NAME"))="HTML" then
							strSelected="CHECKED"
						else
							strSelected=""
						end if
					end if
					Response.Write "<tr>"
					Response.Write "<td valign=""top"" width=""30"">"
					Response.Write "<input type=radio name='subformat' id='subformat' value=""" & rsSubFormat("FORMAT_ID")  & """ " & " " & strSelected & ">"
					Response.Write "</td>"
					Response.Write "<td valign=""middle"">"
					Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
					Response.Write "</td>"
					Response.Write "</tr>"
					rsSubFormat.MoveNext
				wend
						
				Response.Write "</table>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "</table><br><br>"
			end if	

						
		Session("SelectedSubCount")	=subcount		
		Session("SubCount")=subcount
		Session("SubTypeID") = SubTypeID	
		                        
						Response.write "<font color=""#4169E1""><b>Email:</b></font><br>"%>
       
						<input type="text" name="EMAIL" maxlength="100" size="36" value="<%=Session("Email")%>">
						<input type="hidden" name="SUB_ID" value="<%=GenerateUserID%>">
						<input type="hidden" name="DATE_ENTERED" value="<% = date %>">
						<input type="hidden" name="CONTACT_ID" value="<% = session("ContactId") %>">
						<input type="hidden" name="COUNTRY_ID" value="<% = country_id %>">
						<input type="submit" value="Submit" id="submit1" name="submit1">
                          </form>
                           
                            </font></td>
                                  </tr>
                            </table>
                            <font face="Verdana,Arial,Helvetica" size="2">
                            
                           <%'End of Form %>
                            
                            </font>
  
						</td>

					</tr>
	
				</table>
			  </td>
			<td>&nbsp;&nbsp;</td>
			</tr>
		
    </table>

<!--#include virtual="/images/footer/footer.asp" -->
</body>
</html>
