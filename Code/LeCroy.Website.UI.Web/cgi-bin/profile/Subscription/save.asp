<%'***************DO NOT CHANGE SERVER NAME ********************%>
<% ServerName = Request.ServerVariables("SERVER_NAME")
if len(ServerName)>0 then
	if cstr(ServerName)<>"ny-wwwtest" then
		ServerName="teledynelecroy.com"
	end if
end if %>
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<%validateusernoredir()%>
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	country_id=208
if len(Session("localeid"))=0 then
	Session("localeid")=1033
end if
localeid=Session("localeid")
strCRet = chr(13) & chr(10)
'Response.Write request.Form
'Response.end
%>
<body>
<a NAME="TopofPage"></a>
<!--#include virtual="/include/header.asp"-->	
		<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table1">
			<tr>
			  <td valign="top" width=15>&nbsp;&nbsp;</td>
			  <td width="70%" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" ID="Table2">
					<tr>
						<td valign="top" align="left" >
						<%Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"%>
  <font face="Verdana,Arial,Helvetica" size="2">
                            <br><table border="0" cellpadding="4" cellspacing="0" width="100%" ID="Table3">
                              <tr>
                                <td valign="top">
<%

if len( Request.Form("subformat") )>0 then
	Session("subformat")= Request.Form("subformat")
end if
if len(Session("COntactID"))=0 then
'check if the email exists in our db
	set rsCheckContactID=server.CreateObject("ADODB.Recordset")
	strSQL="Select * from SUBSCRIPTION where EMAIL='" & Request.Form("EMail") & "' and CONFIRM_YN='y'"
	rsCheckContactID.Open strSQL, dbconnstr()
	if not rsCheckContactID.EOF then
		flgNew=False
		strContact=rsCheckContactID("CONTACT_ID")
	else
		flgNew=True
		strContact=0
	end if
else
	flgNew=False
	'check if the email was changed
	if lcase(trim(cstr(Request.Form("EMail"))))<>lcase(trim(cstr(Session("Email")))) then
		'update contact record with new email 
		set rsUpdate1=server.CreateObject("ADODB.Recordset")
		strSQL=	"Update CONTACT set EMAIL='" & replace(Request.Form("EMail"),"'","''") & "' where EMAIL='" & Session("Email") & "'"
		'Response.Write strSQL & "<br>"
		rsUpdate1.LockType=3
		rsUpdate1.Open strSQL, dbconnstr()
		'update subscription table with new email
		set rsUpdate2=server.CreateObject("ADODB.Recordset")
		strSQL=	"Update SUBSCRIPTION set EMAIL='" & replace(Request.Form("EMail"),"'","''") & "', FORMAT_ID=" & Request.Form("subformat") & " where EMAIL='" & Session("Email") & "'"
		'Response.Write strSQL & "<br>"
		rsUpdate2.LockType=3
		rsUpdate2.Open strSQL, dbconnstr()
		Session("Email")=Request.Form("EMail")
	end if
	
	strContact=Session("COntactID")
end if

if len(country_id)=0 then
	CountryID=0
else
	CountryID=country_id
end if


		if len (Session("FirstName") )>0 then
			Response.Write Session("FirstName") & ", your subscription has been modified.<br><br>"
		else
			Response.Write "Your subscription has been modified.<br><br>"
		end if
	'end if
	strSubscriptions= "The following changes have been made to <b>" & Request.Form("EMail")& "</b>: <br><br>"
		
	'check which subscriptions were selected

	set rsSubType=server.CreateObject("ADODB.Recordset")
	strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y'"
	rsSubType.Open strSQL,dbconnstr()
	if not rsSubType.EOF then
		if flgNew then
			strConfirmed="n"
		else
			strConfirmed="y"
		end if
		while not rsSubType.EOF
			'check if subscription for this email address
			'exist in SUBSCRIBTION table
			'Response.Write rsSubType("DOCUMENT_ID") & "<br>"
			
			strChecked=Request.Form(cstr("subscription" & rsSubType("DOCUMENT_ID")))
			if len(strChecked)>0 then
				strChecked="y"	
			else
				strChecked="n"
			end if
			
			
			if cstr(strChecked)="y" then
				if len(strlist1)>0 then
					strEmailList1=strEmailList1 & ", " & strCRet & rsSubType("NAME")
					strlist1=strlist1 & ", " & "<br>" & rsSubType("NAME")
				else
					strlist1=  "<b>Subscribed:</b><br>" &rsSubType("NAME")
					strEmailList1= "Subscribed:"  & strCRet &rsSubType("NAME")
				end if
			else
				if len(strlist2)>0 then
					strEmailList2=strEmailList2 & ", " & strCRet & rsSubType("NAME")
					strlist2=strlist2 & "<br>" & rsSubType("NAME")
				else
					strlist2=  "<b>Unsubscribed:</b><br>" & rsSubType("NAME")
					strEmailList2= "Unsubscribed:" & strCRet & rsSubType("NAME")
				end if
			end if
				set rsCheckSubExist=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from SUBSCRIPTION where EMAIL='" & Request.Form("EMail") & "' and DOCUMENT_ID=" & rsSubType("DOCUMENT_ID")
				rsCheckSubExist.Open strSQL, dbconnstr()

				if not rsCheckSubExist.EOF then
				'if subscription exist update it
					set rsUpdateSub=server.CreateObject("ADODB.Recordset")
					strSQL=	"Update SUBSCRIPTION set SUB_ID='" & Request.Form("SUB_ID") & "',DATE_MODIFIED='" & Now() & "',ENABLE='" & strChecked & "'," & _
							"CONTACT_ID=" & strContact & ",CONFIRM_YN='" & strConfirmed & "',FORMAT_ID=" & Request.Form("subformat") & " where EMAIL='" & Request.Form("EMail") & "' and DOCUMENT_ID=" & rsSubType("DOCUMENT_ID")
							
					'Response.Write strSQL & "<br>"
					rsUpdateSub.LockType=3
					rsUpdateSub.Open strSQL, dbconnstr()
				
				else
				'if not insert new record into SUBSCRIPTION table
									
					set rsInsertSub=server.CreateObject("ADODB.Recordset")
					strSQL=	"Insert into SUBSCRIPTION (SUB_ID,DATE_ENTERED,EMAIL,DOCUMENT_ID," & _
							"CONTACT_ID,ENABLE,COUNTRY_ID,DOCUMENT_READ,FORMAT_ID, CONFIRM_YN) values('" & _
							Request.Form("SUB_ID") & "','" & Now() & "','" & _
							Request.Form("EMail") & "'," & rsSubType("DOCUMENT_ID") & "," & strContact & ",'" & strChecked & "'," & CountryID & ",'n'," & Request.Form("subformat") & ",'" &  strConfirmed & "')"
					'Response.Write strSQL & "<br>"
					rsInsertSub.LockType=3
					rsInsertSub.Open strSQL, dbconnstr()
				end if	
		
		rsSubType.MoveNext
		wend
	'send email to the customer
		'if flgNew then
			strBody=""
			strBody = strBody & "Your subscription has been modified."& strCRet & strCRet 
		if len(strEmailList1)>0 or len(strEmailList2)>0 then
			strBody = strBody & "The following changes have been made to " & Request.Form("EMAIL") & ":"& strCRet& strCRet  
			if len(strEmailList1)>0 then
				strBody = strBody & strEmailList1& strCRet & strCRet 
			end if
			strBody = strBody & strEmailList2
		else
			strBody = strBody & "You did not select any subscriptions."
		end if
		strBody = strBody & strCRet & strCRet

			strBody=strBody&"*************************************************************"&strCret&strCret
			strBody = strBody & "If this change has been made in error or you wish to modify your change, please follow the link below:"
			strBody = strBody & strCRet & strCRet
			strBody = strBody & "http://" & ServerName & "/cgi-bin/Profile/Subscription/confirm.asp?id=" & Request.Form("SUB_ID")
			if len(Session("COntactID"))=0 then
				strBody = strBody & strCRet & strCRet
				strBody=strBody&"*************************************************************"&strCret&strCret
				strBody = strBody & "If you would like to complete full registation on LeCroy website follow the link below:"
				strBody = strBody & strCRet & strCRet
				strBody = strBody & "http://" & ServerName & "/cgi-bin/Profile/RegForm.asp"
			end if
			set objMessage =Server.CreateObject("CDO.Message")
			set objConfig = Server.CreateObject("CDO.Configuration")
			' Set the message properties and send the message.
				With objMessage
					Set .Configuration = objConfig
					.MimeFormatted = True
					.Fields.Update
					.To = Request.Form("EMAIL")
					.BCC= "kate.kaplan@teledynelecroy.com"
					.From = "webmaster@teledynelecroy.com"
					.Subject = "Teledyne LeCroy Subscription Confirmation"
					.TextBody = strBody
					.Send
				End With

			Set objMessage = Nothing
			Set objConfig = Nothing		
		'end if
	'send cookie to customer	
	'Cookie is equal SUB_ID
	'if len(Request.Cookies("SUBSCRIBE")("SUB"))=0 then
		Response.Cookies("SUBSCRIBE")("SUB") =Request.Form("SUB_ID")
		Response.Cookies("SUBSCRIBE").Expires = Date + 365 * 5
	'end if


	Response.Write 	strSubscriptions 
	if len(strlist1)>0 then
	  Response.Write strlist1 & "<br><br>" 
	end if
	Response.Write strlist2
	Response.Write "<br><br>You will receive an email to confirm your subscription."
end if
'Response.Write "<a href 					
%>

                            </font></td>
                                  </tr>
                            </table><br><br><br><br>

						</td>

					</tr>
	
				</table>
			  </td>
			<td>&nbsp;&nbsp;</td>
			</tr>
		
    </table>

<!--#include virtual="/images/footer/footer.asp" -->
</body>
</html>