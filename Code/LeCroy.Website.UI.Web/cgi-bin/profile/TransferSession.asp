<%@ LANGUAGE="VBScript" %>
<%%>
<!--#include virtual="/include/dbconnstr.asp" -->
<%

'***********************************
'Functions
'***********************************

'Function returns a valid Globally Unique ID (GUID) for identifying a session.
Function GetGuid()
	Set TypeLib = Server.CreateObject("Scriptlet.TypeLib")

	'A true GUID contains a unicode null termination, that needs to be stripped to behave like 
	'  a string.
	guid_temp = TypeLib.Guid
	GetGuid = Left(guid_temp, Len(guid_temp) - 2)
		
	Set TypeLib = Nothing
End Function

'This function adds all Session information to the database and returns the GUID used to 
'  identify the Session information.
Function AddSessionToDatabase()
   
	 
   'Iterate through all Session variables and add them to the database with the 
	'  same GUID as an identifier.
	guidTemp = GetGuid()
	i=1
    Do While (i <= Session.Contents.Count)
        Set rsGuid = Server.CreateObject("ADODB.Recordset")
		strSQL = "INSERT INTO ASPSessionState (GUID, SessionKey, SessionValue) VALUES ('" & guidTemp & "', '" & Session.Contents.Key(i) & "', '" & Session.Contents.Item(i) & "')"
            'response.write strSQL & "<br>"
		rsGuid.open strSQL,dbconnstr
	 i=i+1
	loop
 
	'Return the GUID used to identify the Session information
	AddSessionToDatabase = guidTemp

	'Clean up database objects
	 
End Function

'***********************************
'Main code execution
'***********************************
        'Add the session information to the database, and redirect to the
        'ASPX implementation of SessionTransfer.
       ' localeid=request.QueryString("localeid")
     '   Session("localeid")=localeid
        guidSave = AddSessionToDatabase()
        'response.write guidSave
        Response.Redirect("/support/user/setsession.aspx?guid=" + guidSave)
%>