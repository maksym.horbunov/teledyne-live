<%
Set cnn = Server.CreateObject("ADODB.Connection")
Set rs = Server.CreateObject("ADODB.Recordset")

Function CheckMaxNumberOfSelections(strObjectId, maxAllowed)
	' Author:           Nick Benes
    ' Date:             July 04, 1998
    ' Description:      Count the number of selections asn returns
    ' Returns:          TRUE if the number is within the limits
	'					FALSE if over passed
	' Revisions:        0

	Dim iSeparatorLoc
	Dim iLooper

	CheckMaxNumberOfSelections = "TOO MANY"

	iSeparatorLoc = Instr(strObjectId,",")
	' Only one selection
	If iSeparatorLoc = 0 Then
		CheckMaxNumberOfSelections = "TRUE"
		Exit Function
	Else
		
		Do While (iSeparatorLoc > 0)
		iSeparatorLoc = Instr(strObjectId,",")	
			iLooper = iLooper + 1
			strObjectId = Trim(Mid(strObjectId, iSeparatorLoc + 1))
		Loop
	End If		
	
	If iLooper <= maxAllowed Then
		CheckMaxNumberOfSelections = "TRUE"
	Else
		CheckMaxNumberOfSelections = "TOO MANY"
	End If

End Function

Function InsertAllCustomerRequests(maxRequests)
    ' Author:           Kate Kaplan
    ' Date:             March 3, 2006
    ' Description:      Inserts all customer requests
    ' Returns:          TRUE or FALSE
    ' Revisions:        1 Added Application Type 
	'					2 Japanese email added

if cint(Session("selectedlitcount"))>0 then
		lit=Session("literature")
		for i=0 to  Session("selectedlitcount")-1
			strObjectId=lit(i)
	
			strRequestShortName="LIT"
			RequestTypeId = GetRequestTypeId(strRequestShortName)
			if cstr(strObjectId)="257" then
			    strRemarks=session("litremarks")
			else
			    strRemarks = "No remarks for Literature"
			end if
			If len(Session("PageID")) = 0 then
				Session("PageID") = 0
			end if
			InsertOk = InsertRequest(Session("ContactId"), RequestTypeId, strObjectId, strRemarks,"",Session("PageID"))
			'call SendRequestInfo(InsertOk)
		next
	end if

	'demo
	
	if cint(Session("selecteddemocount"))>0 then'and Session("demomodel")=true then
		demomodel=Session("demo")
		for i=0 to  Session("selecteddemocount")-1
			strObjectId=demomodel(i)
			strRequestShortName="DEM"
			RequestTypeId = GetRequestTypeId(strRequestShortName)
			strRemarks = "No Remarks for Demo"
			If Len(Trim(Request.QueryString("ApplicationOther"))) >0 then
				strApplicationType = "" & Trim(Request.QueryString("ApplicationOther"))
			ElseIf Len(Trim(Request.QueryString("Applications"))) >0 then
				strApplicationType = "" & Trim(Request.QueryString("Applications"))
			Else
				strApplicationType = ""
			End If 
			 

			If len(Session("PageID")) = 0 then
				Session("PageID") = 0
			end if
			InsertOk = InsertRequest(Session("ContactId"), RequestTypeId, strObjectId, strRemarks,strApplicationType,Session("PageID"))
			'call SendRequestInfo(InsertOk)
		next
	end if
	'response.Write Session("SEtocallselectedcount")
		
	'4/7/05 new code added SS - SETocall
	if len(Session("SEtocallselectedcount"))>0  then
		'response.Write "test1"
		SECallmodels=Session("SeTocall")
		'insert the request in the table and the get the requestID of that request
		If len(Session("PageID")) = 0 then
			Session("PageID") = 0
		end if
		strRemarks= Session("SEcallques")
		if len(strRemarks) = 0 then
			strRemarks =""
		end if
		RequestID = InsertRequest(Session("ContactId"), 3, 1, strRemarks,"",0)
		'call SendRequestInfo(RequestID)
		for i=0 to  Session("SEtocallselectedcount")-1
			'RESPONSE.Write SECallmodels(i)
			set rsInsertReqDetails=server.CreateObject("ADODB.Recordset")
			strSQL=	"Insert INTO REQUEST_DETAILS "&_
					"(REQUEST_ID, TABLE_ID, IDINTABLE)" &_
					" VALUES (" & RequestID & ",30," & SECallmodels(i) & " )"
			'Response.Write strSQL
			'Response.end
			rsInsertReqDetails.Open strSQL, dbconnstr()	
		
			
		next
		if len(requestid) > 0 then
			InsertedOk= true
		end if 
	end if 
	'4/7/05 end of new code
	For iLooper = 1 to maxRequests
		' If we have a request of that object
		strObjectId = Request.QueryString("Object"& iLooper)
		If Len (strObjectId) > 0 Then
			' Get the request type short name
			strRequestShortName = Request.QueryString("RequestType"& iLooper)
			' Get the request_type_id
			RequestTypeId = GetRequestTypeId(strRequestShortName)
			' Get the remarks
			strRemarks = "" & Request.QueryString("Remarks"& iLooper)
			' Get the application
			If Len(Trim(Request.QueryString("ApplicationOther"))) >0 then
				strApplicationType = "" & Trim(Request.QueryString("ApplicationOther"))
			ElseIf Len(Trim(Request.QueryString("Applications"))) >0 then
				strApplicationType = "" & Trim(Request.QueryString("Applications"))
			Else
				strApplicationType = ""
			End If 
			' See if we do not have multiple selection
			iSeparatorLoc = Instr(strObjectId,",")
			If iSeparatorLoc = 0 Then
				' Start inserting the request
				'Set Session("PageID") to 0 if none was entered
				'This is used to track Marketing Campaigns - refer to Campaign Table for more info.
				If Session("PageID") = "" then
					Session("PageID") = 0
				end if
				
				InsertOk = InsertRequest(Session("ContactId"), RequestTypeId, strObjectId, strRemarks,strApplicationType,Session("PageID"))
				'call SendRequestInfo(InsertOk)
				If Len(InsertOk) = 0 or InsertOk = 0 Then
					InsertAllCustomerRequests = False
					Exit Function
				End If		
			' If we have multiple selection
			Else
				Do 
					iSeparatorLoc = Instr(strObjectId,",")
					strObjectIdUnique = Trim(Left(strObjectId,iSeparatorLoc - 1 ))
					' if user didn't choose the NULL value
					If Len(strObjectIdUnique)>0 then
						'Insert Request
						InsertOk = InsertRequest(Session("ContactId"), RequestTypeId, strObjectIdUnique, strRemarks, strApplicationType,Session("PageID"))
						'call SendRequestInfo(InsertOk)
						' Check Insertion
						If Len(InsertOk) = 0 or InsertOk = 0 Then
							InsertAllCustomerRequests = False
							Exit Function
						End If					
					End If
					' Get rid of the previous part
					strObjectId = Trim(Mid(strObjectId, iSeparatorLoc + 1))
					If Instr(strObjectId,",") = 0 Then 
						Exit Do
					End If	 
					
				Loop While (iSeparatorLoc > 0) or Len(iSeparatorLoc) > 0
				' if user didn't choose the NULL value
				If Len(strObjectIdUnique)>0 then
					' Now insert the last Object
					InsertOk = InsertRequest(Session("ContactId"), RequestTypeId, strObjectId, strRemarks, strApplicationType,Session("PageID"))
					'call SendRequestInfo(InsertOk)
					If Len(InsertOk) = 0 or InsertOk = 0 Then
						InsertAllCustomerRequests = False
						Exit Function
					End If					
				End If
			End If
		End If
	Next
	InsertAllCustomerRequests = True

End Function

Function ValidateUser()
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Validate a user
    ' Returns:          TRUE or FALSE
    ' Revisions:        2
    '1-modified by Kate Kaplan 5/17/2004
    '2-modified by Shraddha Shah 5/18/2004
    ' Get the user contact id
  If len(Session("ContactId")) > 0 Then
    If clng(Session("ContactId")) > 0 Then
    GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
    GetUserData(GUIDFromUSER)
        ValidateUser = True
        Exit Function
    Else
        ' Check the cookie
        GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
        ' If Cookie is there
        If Len(GUIDFromUSER) > 0 Then
            ContactIdTemp = GetContactIDOnWebId(GUIDFromUSER)
            ' If we found the id in the database
            If isnumeric(ContactIdTemp)  Then
				if clng(ContactIdTemp)>0 then
					Session("ContactId") = ContactIdTemp
					Session("GUID") = GUIDFromUSER
					GetUserData(GUIDFromUSER)
					ValidateUser = True
					Exit Function
                else
					'There is a problem:
					'We have the GUID but somewhat we don't have the
					' contactID
					ValidateUser = False
					if Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
				elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
                else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
					Exit Function
               end if 
            ' If not found
            Else
                'There is a problem:
                'We have the GUID but somewhat we don't have the
                ' contactID
                ValidateUser = False
                if Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
				elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
                else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
                Exit Function
            End If
        Else
        ' If we do not have the GUID
        ' There are 2 possibilities
        ' He refused the GUID but he is with username and password
        ' or he is first time here
        ' so:
		response.Write Session("userfrom")
		
If Session("UserFrom") = "Japan" then
					Response.Redirect "/japan/cgi-bin/senttoreg.asp"
				elseif Session("UserFrom") = "Korea" then
					Response.Redirect "/korea/cgi-bin/senttoreg.asp"
				elseif Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
				elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
                else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
            Exit Function
        End If
    End If
else
	'There is a problem:
	'We have the GUID but somewhat we don't have the
	' contactID
	' Check the cookie
        GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
        ' If Cookie is there
        If Len(GUIDFromUSER) > 0 Then
            ContactIdTemp = GetContactIDOnWebId(GUIDFromUSER)
            ' If we found the id in the database
            If isnumeric(ContactIdTemp)  Then
				if clng(ContactIdTemp)>0 then
					Session("ContactId") = ContactIdTemp
					Session("GUID") = GUIDFromUSER
					GetUserData(GUIDFromUSER)
					ValidateUser = True
					Exit Function
                else
					'There is a problem:
					'We have the GUID but somewhat we don't have the
					' contactID
					ValidateUser = False
					if Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
				elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
                else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
					Exit Function
               end if 
            ' If not found
            Else
                'There is a problem:
                'We have the GUID but somewhat we don't have the
                ' contactID
                ValidateUser = False
                if Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
				elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
                else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
                Exit Function
            End If
        Else
			ValidateUser = False
			if Session("UserFrom") = "er" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?er=y"
				elseif Session("UserFrom") = "eg" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?eg=y"
               elseif Session("UserFrom") = "si" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?si=y"
                elseif Session("UserFrom") = "w12" then
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?w12=y" 
				else
					Response.Redirect "/cgi-bin/profile/senttoreg.asp?d=d"
				end if
			Exit Function
		end if 
end if 


End Function

Function ValidateUserNoRedir()
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Validate a user
    ' Returns:          TRUE or FALSE
    ' Revisions:        0
    
    ' Get the user contact id
    
    If Session("ContactId") > 0 Then
        ValidateUserNoRedir = True
        Exit Function
    Else
        ' Check the cookie
        GUIDFromUSER = Request.Cookies("LCDATA")("GUID")
        ' If Cookie is there
        If Len(GUIDFromUSER) > 0 Then
            ContactIdTemp = GetContactIDOnWebId(GUIDFromUSER)
            ' If we found the id in the database
            If ContactIdTemp > 0 Then
                Session("ContactId") = ContactIdTemp
				Session("GUID") = GUIDFromUSER
				GetUserData(GUIDFromUSER)
                ValidateUserNoRedir = True
                Exit Function
            ' If not found
            Else
                'There is a problem:
                'We have the GUID but somewhat we don't have the
                ' contactID
                ValidateUserNoRedir = False
                Exit Function
            End If
        Else
        ' If we do not have the GUID
        ' There are 2 possibilities
        ' He refused the GUID but he is with username and password
        ' or he is first time here
        ' so:
			ValidateUserNoRedir = False
            Exit Function
        End If
    End If
End Function

Sub GetUserData(strGUIDFromUser)

    ' Author:           Adrian Cake
    ' Date:             June 23, 1998
    ' Description:      Gets the the User Data
    ' Returns:          
    ' Revisions:        0
    
	' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT * FROM V_CONTACT WHERE CONTACT_WEB_ID = '" & Trim(strGUIDFromUser) & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.BOF And Not rs.EOF Then
		Session("Honorific") = rs.fields("HONORIFIC").Value
		'Change Inital Character to Upper Case - DOES NOT WORK FOR JAPAN CHARACTERS
		Session("FirstName") = Left(rs.Fields("FIRST_NAME").Value,1) & Mid(rs.Fields("FIRST_NAME").Value,2)
		Session("LastName") = trim(rs.fields("LAST_NAME").Value)
		Session("UserName") = trim(rs.fields("USERNAME").value)
		Session("Password") = trim(rs.fields("PASSWORD").value)
		Session("Title") = trim(rs.fields("TITLE").value)
		Session("Company") = trim(rs.fields("COMPANY").Value)
		Session("Address") = trim(rs.fields("ADDRESS").Value)
		Session("Address2") = trim(rs.fields("ADDRESS2").Value)
		Session("City") = trim(rs.fields("CITY").Value)
		Session("State") = trim(rs.fields("STATE_PROVINCE").Value)
		Session("Zip") = trim(rs.fields("POSTALCODE").Value)
		Session("Country") = trim(rs.fields("COUNTRY").Value)
		Session("Phone") = trim(rs.fields("PHONE").Value)
		Session("Fax") = trim(rs.fields("FAX").Value)
		Session("Email") = trim(rs.fields("EMAIL").Value)
		Session("URL") = trim(rs.fields("URL").Value)
		Session("Applications") = trim(rs.fields("APPLICATION").Value)
		Session("JobFunction")=trim(rs.fields("JOB_FUNCTION").Value)
		Session("Department")=trim(rs.fields("DEPARTMENT").Value)
		'Added Contact_Web_Id 11/1/99 by Adrian
		'Session("Contact_Web_Id") = rs.Fields("CONTACT_WEB_ID").Value
		'Session("Nick_Name") = rs.fields("NICK_NAME").Value
	
'Else
'		Session("FirstName") = "Unknown"
	End If
	rs.Close

End Sub

Function OpenConnection()
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Opens the ADO db connection
    ' Returns:          TRUE or FALSE
    ' Revisions:        0
    
    Dim strCnn
   
    ' Check if connection is openned
    If (Not cnn Is Nothing) Then
        If cnn <> "" Then
            OpenConnection = True
            Exit Function
        End If
    End If
'***********	
	strCnn = "Provider=MSDASQL;Driver={SQL Server};Server=SQLWEB;Database=webcatalog;UID=webuser;PWD=mendelssohn$405"
    'strCnn = "Provider=MSDASQL;Driver={SQL Server};Server=ny-sqlwebtest;Database=webcatalog;UID=kate;PWD=kate"

'**********
    Set cnn = Server.CreateObject("ADODB.Connection")
    cnn.Open strCnn
    cnn.CursorLocation = 3  'adUseClient
    OpenConnection = True

End Function

Sub CloseConnection()
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Closes the ADO db connection
    ' Revisions:        0
    
    ' Check if connection is openned
    If (Not cnn Is Nothing) Then
        If cnn <> "" Then
            ' Close connection
            cnn.Close
            Set cnn = Nothing
        End If
    End If
End Sub

Sub DestroyRecordset(rs)
    ' Author:           Nick Benes
    ' Date:             July 02, 1998
    ' Description:      Close a recordset
    ' Revisions:        0
     If (Not rs Is Nothing) Then
      ' Close it
        rs.Close
        Set rs = Nothing
     End If
End Sub

Public Function GeneratedGUID()
    ' Author:           Nick Benes
    ' Date:             May 22, 1998
    ' Description:      Generate a new GUID
    ' Returns:          the GUID
    ' Revisions:        1

    Dim strTemp
    Dim bl
    Dim strGUIDTemp
    Dim strGIUDFinal

    bl = False
    Do While Not bl
        Do Until Len(strGIUDFinal) > 14 ' number of the strlen -1
            Randomize
            ' Get a character
            strGUIDTemp = Chr(Int(44 * Rnd) + 48)
            
            ' Check if character is in the 0-9 and A-Z range
            If Asc(strGUIDTemp) < 58 Or (Asc(strGUIDTemp) > 64 And Asc(strGUIDTemp) < 91) Then
                strGIUDFinal = strGIUDFinal & strGUIDTemp
            End If
        Loop
        If UCase(CheckWebIDNotExistance(strGIUDFinal)) = "TRUE" Then
            bl = True
        Else
            strGIUDFinal = ""
            bl = False
        End If
    Loop
    GeneratedGUID = strGIUDFinal
End Function

Function CheckAllReqFields()
    'Author:           Nick Benes
    ' Date:             May 22, 1998
    ' Description:      Checks if user entered all required fields
    ' Returns:          a string with the missing fields or empty string if OK
    ' Revisions:        1
    
    ' Ubound must match the number of required fieds
    Dim strFieldMissing(10)
    Dim strFields
    Dim iLooper

        '1 st field
    If Len(Request("FirstName")) = 0 Then
        strFieldMissing(0) = "First Name"
    End If

    '2nd field
    If Len(Request("LastName")) = 0 Then
        strFieldMissing(1) = "Last Name"
    End If

    '3rd field
    If Len(Request("Company")) = 0 Then
        strFieldMissing(2) = "Company"
    End If

    '4th field
    If Len(Request("Address")) = 0 Then
        strFieldMissing(3) = "Address"
    End If

    '5th field
    If Len(Request("City")) = 0 Then
        strFieldMissing(4) = "City"
    End If

    '6th field
    'If Len(Request("State")) = 0 Then
	'	If Len(Request("OtherState")) = 0 Then
	'		strFieldMissing(5) = "State (Other State)"
	'	End If
    'End If

    '7th field
    If Len(Request("Zip")) = 0 Then
        strFieldMissing(6) = "Zip"
    End If

    '8th field
    If Len(Request("Country")) = 0 Then
        strFieldMissing(7) = "Country"
    End If

    '9th field
    If Len(Request("Username")) = 0 Then
        strFieldMissing(8) = "Username"
    End If

    '10th field
    If Len(Request("Password")) = 0 Then
        strFieldMissing(9) = "Password"
    End If

    '11th field
    If Len(Request("ConfirmPassword")) = 0 Then
        strFieldMissing(10) = "Confirm Password"
    End If
    
    '12 Field Added by Adrian 11/1/99
	'12th field
    If Len(Request("Email")) = 0 Then
        strFieldMissing(10) = "E-Mail"
    End If

    For iLooper = 0 To UBound(strFieldMissing)
        If Len(strFieldMissing(iLooper)) <> 0 Then
            strFields = strFields & ", " & strFieldMissing(iLooper)
        End If
    Next
    strFields = Mid(strFields, 3)
    CheckAllReqFields = strFields

End Function

Function CheckAllReqFieldsForModification()
    ' Author:           Nick Benes
    ' Date:            Sept 03, 1998
    ' Description:     Checks if user entered all required fields for modification
    ' Returns:         a string with the missing fields or empty string if OK
    ' Revisions:       0
    
    ' Ubound must match the number of required fieds
    Dim strFieldMissing(7)
    Dim strFields
    Dim iLooper

    '1 st field
    If Len(Request("FirstName")) = 0 Then
        strFieldMissing(0) = "First Name"
    End If

    '2nd field
    If Len(Request("LastName")) = 0 Then
        strFieldMissing(1) = "Last Name"
    End If

    '3rd field
    If Len(Request("Company")) = 0 Then
        strFieldMissing(2) = "Company"
    End If

    '4th field
    If Len(Request("Address")) = 0 Then
        strFieldMissing(3) = "Address"
    End If

    '5th field
    If Len(Request("City")) = 0 Then
        strFieldMissing(4) = "City"
    End If

    '6th field
	
	'If Len(Request("State")) = 0 and Session("Country")="United States" Then
	'	If Len(Request("OtherState")) = 0 Then
	'		strFieldMissing(5) = "State (Other State)"
	'	End If
	'End If

    '7th field
    If Len(Request("Zip")) = 0 Then
        strFieldMissing(6) = "Zip"
    End If

    '8th field
    If Len(Request("Country")) = 0 Then
        strFieldMissing(7) = "Country"
    End If

    For iLooper = 0 To UBound(strFieldMissing)
        If Len(strFieldMissing(iLooper)) <> 0 Then
            strFields = strFields & ", " & strFieldMissing(iLooper)
        End If
    Next
    strFields = Mid(strFields, 3)
    CheckAllReqFieldsForModification = strFields

End Function

'******************************************
'Get GetContactIDOnUserNameAndPwd 
'Modified Kate Kaplan 1/6/2004
'arguments-strUsername (varchar(30)), strPassword (varchar(30))
'Returns long integer
'**********************************************
Function GetContactIDOnUserNameAndPwd(strUsername, strPassword)
    If Not OpenConnection Then
		Response.Redirect "/sitemap/error.asp"
        Exit Function
    End If
    'Set rsSignIn = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT CONTACT_ID FROM V_CONTACT WHERE UPPER(USERNAME) = N'" & replace(Trim(UCase(strUsername)),"'","''") & "' AND UPPER(PASSWORD) = N'" & replace(Trim(Ucase(strPassword)),"'","''") & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
      'response.Write strSQL & "<br>"
	'set cmdSignIn = createobject("ADODB.Command")
	'cmdSignIn.ActiveConnection = cnn
	'cmdSignIn.CommandText = "sp_registration_signin"
	'cmdSignIn.CommandType =4	' 4 = stored procedure
	'set ParamUsername=cmdSignIn.CreateParameter("@USERNAME",200,1,50)
	'cmdSignIn.Parameters.Append ParamUsername
	'cmdSignIn.Parameters("@USERNAME")= replace(strUsername,"'","''")
	'set ParamPassword=cmdSignIn.CreateParameter("@PASSWORD",200,1,50)
	'cmdSignIn.Parameters.Append ParamPassword
	'cmdSignIn.Parameters("@PASSWORD")= replace(strPassword,"'","''")
	'set rsSignIn=	cmdSignIn.Execute
   ' rs.ActiveConnection = dbconnstr()
   ' rs.Open strSQL	
  
    'response.Write  rs.EOF  
   ' response.end
   
    If Not rs.EOF Then
		GetContactIDOnUserNameAndPwd = CLng(rs("CONTACT_ID"))
	Else
		GetContactIDOnUserNameAndPwd = 0
	End If
	rs.Close
	
	'set cmdSignIn=nothing
End Function

'*******************************************
'function GetContactIDOnWebId
'modified by KK 1/22/2004 (store procedure)
'*******************************************
Function GetContactIDOnWebId(strWebID)
	if len(strWebID)>0 then
		Set cmdGetContactID=server.CreateObject("ADODB.Command")
		cmdGetContactID.CommandText="sp_getcontactidonwebid"
		cmdGetContactID.ActiveConnection=dbconnstr()
		cmdGetContactID.CommandType=4'STORE PROCEDURE
		'create Paremeter
		set ParamCONTACT_WEB_ID=cmdGetContactID.CreateParameter("@CONTACT_WEB_ID",200,1,15)'200 adVarchar, 1 adParamInput, 15 -size
		cmdGetContactID.Parameters.Append ParamCONTACT_WEB_ID
		cmdGetContactID.Parameters("@CONTACT_WEB_ID")= Trim(Ucase(strWebID))
		set rs=cmdGetContactID.execute
		If Not rs.EOF Then
			if len(rs.fields("CONTACT_ID"))>0  then
				GetContactIDOnWebId = CLng(rs("CONTACT_ID"))
			else
				GetContactIDOnWebId = 0
			end if
		Else
			GetContactIDOnWebId = 0
		End If
		rs.Close
	else
	GetContactIDOnWebId = 0
	end if
End Function

Function GetContactIDOnEmail(strEmail)
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT CONTACT_ID FROM V_CONTACT WHERE UPPER(EMAIL) = '" & Trim(Ucase(strEmail)) & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetContactIDOnEmail = CLng(rs.fields("CONTACT_ID").Value)
	Else
		GetContactIDOnEmail = 0
	End If
	rs.Close
End Function

Function GetGUIDOnContactId(IContactId)
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT CONTACT_WEB_ID FROM V_CONTACT WHERE CONTACT_ID = " & IContactID
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetGUIDOnContactId = rs.fields("CONTACT_WEB_ID").Value
	Else
		GetGUIDOnContactId = 0
	End If
	rs.Close

End Function

Function CheckWebIDNotExistance(strWebID)
	' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the ContactID based on Contact_web_Id
    ' Returns:          String "TRUE" if not found, or
    '                   String "FALSE" if found
    ' Revisions:        0
    
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT CONTACT_ID FROM V_CONTACT WHERE UPPER(CONTACT_WEB_ID) = '"  & Trim(UCase(strWebID)) & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		CheckWebIDNotExistance = "FALSE"
	Else
		CheckWebIDNotExistance = "TRUE"
	End If
	rs.Close

End Function
    

Function CheckPWDNotExistance(strUsername, strPassword)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Checks if the combination Username and password is unique
    ' Returns:          String "TRUE" if not found
    '                   String "FALSE" if found
    ' Revisions:        0
     If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT CONTACT_ID FROM V_CONTACT WHERE UPPER(USERNAME) = N'" & replace(Trim(UCase(strUsername)),"'","''") & "' AND UPPER(PASSWORD) = N'" & replace(Trim(Ucase(strPassword)),"'","''") & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		CheckPWDNotExistance = "FALSE"
	Else
		CheckPWDNotExistance = "TRUE"
	End If
	rs.Close
End Function

Function InsertContact(strWebID, strHonorific, strFirstName, strLastName, strTitle,strJobFunction, strCompany,strDepartment, strAddress, strAddress2, strCity, strState, strPostalCode, strCountry, strPhone, strFax, strEmail, strURL, strApplication, strUsername, strPassword)
    ' Author:           Nick Benes
    ' Modified by Kate Kaplan 3/15/2004
    ' Modified by Kate Kaplan 9/29/2003
    ' Date:             June 25, 1998
    ' Description:      Insert a new contact
    ' Returns:          The contactId just inserted
    ' Revisions:        0
	'					1 (9/29/2003)
	'					2 (3/15/2004)
    
   ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
            InsertContact = 0
        Exit Function
    End If
    
    set rsInsertContact=server.CreateObject("ADODB.Recordset")
    strSQL="INSERT INTO CONTACT (CONTACT_WEB_ID, HONORIFIC, FIRST_NAME, LAST_NAME, TITLE,JOB_FUNCTION,COMPANY,DEPARTMENT, ADDRESS, ADDRESS2, CITY, STATE_PROVINCE, POSTALCODE, COUNTRY, PHONE, FAX, EMAIL, URL,APPLICATION, USERNAME, PASSWORD, DATE_ENTERED, ENTERED_BY, LAST_NAME_UPPER)" &_
			"VALUES " &_
			"('" & strWebID & "',N'" & replace(strHonorific,"'","''") & "',N'" &  replace(strFirstName,"'","''") & "',N'" &_
			replace(strLastName,"'","''") & "',N'" &  replace(strTitle,"'","''")& "',N'" &  replace(strJobFunction,"'","''") & _
			"',N'" & replace(strCompany,"'","''") & "',N'" & replace(strDepartment,"'","''") & "',N'"&_
			replace(strAddress,"'","''") & "',N'" & replace(strAddress2,"'","''") & "',N'" & replace(strCity,"'","''") & "',N'" &_
			replace(strState,"'","''") & "',N'" & replace(strPostalCode,"'","''") & "',N'" &  replace(strCountry,"'","''") &_
			"',N'" &  replace(strPhone,"'","''") & "',N'" &_
			replace(strFax,"'","''") & "',N'" & replace(strEmail,"'","''") & "',N'" & replace(strURL,"'","''") & "',N'" & replace(strApplication,"'","''") & "',N'" &_
			replace(strUsername,"'","''") & "',N'" & replace(strPassword,"'","''") & "','" & now() & "','webuser',N'" & UCase(replace(strLastName,"'","''")) & "')"
   'Response.Write strSQL
   ' Response.end
    'on error resume next
    rsInsertContact.Open strSQL,dbconnstr()
 	if len(GetContactIDOnWebId(strWebID))>0 then
		InsertContact  = GetContactIDOnWebId(strWebID)
    else
		 Response.Redirect "/cgi-bin/profile/RegisterAgain.asp"
    end if
    
    
     ' Open command object
   ' Set cmd = Server.CreateObject("ADODB.Command")
   ' Set paramIn1 = CreateObject("ADODB.Parameter")
   ' Set paramIn2 = CreateObject("ADODB.Parameter")
   ' Set paramIn3 = CreateObject("ADODB.Parameter")
   ' Set paramIn4 = CreateObject("ADODB.Parameter")
   ' Set paramIn5 = CreateObject("ADODB.Parameter")
   ' Set paramIn6 = CreateObject("ADODB.Parameter")
   ' Set paramIn7 = CreateObject("ADODB.Parameter")
   ' Set paramIn8 = CreateObject("ADODB.Parameter")
    'Set paramIn9 = CreateObject("ADODB.Parameter")
    'Set paramIn10 = CreateObject("ADODB.Parameter")
    'Set paramIn11 = CreateObject("ADODB.Parameter")
    'Set paramIn12 = CreateObject("ADODB.Parameter")
    'Set paramIn13 = CreateObject("ADODB.Parameter")
    'Set paramIn14 = CreateObject("ADODB.Parameter")
    'Set paramIn15 = CreateObject("ADODB.Parameter")
    'Set paramIn16 = CreateObject("ADODB.Parameter")
    'Set paramIn17 = CreateObject("ADODB.Parameter")
    'Set paramIn18 = CreateObject("ADODB.Parameter")
    'Set paramIn19 = CreateObject("ADODB.Parameter")
    'Set paramOut = CreateObject("ADODB.Parameter")
  
    'cmd.CommandText = "{call ZP_INSERT_CONTACT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    ' adParamOutput =2

    'Set paramIn1 = cmd.CreateParameter("", 200, 1, 15)     'webid(GUID)
    'Set paramIn2 = cmd.CreateParameter("", 200, 1, 5)      'Honorific
    'Set paramIn3 = cmd.CreateParameter("", 200, 1, 30)     'FirstName
    'Set paramIn4 = cmd.CreateParameter("", 200, 1, 30)     'LastName
    'Set paramIn5 = cmd.CreateParameter("", 200, 1, 60)     'Title
    'Set paramIn6 = cmd.CreateParameter("", 200, 1, 50)     'Company
    'Set paramIn7 = cmd.CreateParameter("", 200, 1, 255)    'Address
    'Set paramIn8 = cmd.CreateParameter("", 200, 1, 255)    'Address2
    'Set paramIn9 = cmd.CreateParameter("", 200, 1, 50)     'City
    'Set paramIn10 = cmd.CreateParameter("", 200, 1, 50)    'State_Province
    'Set paramIn11 = cmd.CreateParameter("", 200, 1, 20)    'Postal Code
    'Set paramIn12 = cmd.CreateParameter("", 129, 1, 50)    'Country
    'Set paramIn13 = cmd.CreateParameter("", 129, 1, 30)    'Phone
    'Set paramIn14 = cmd.CreateParameter("", 129, 1, 30)    'Fax
    'Set paramIn15 = cmd.CreateParameter("", 129, 1, 50)    'E-mail
    'Set paramIn16 = cmd.CreateParameter("", 129, 1, 50)    'URL
    'Set paramIn17 = cmd.CreateParameter("", 129, 1, 50)    'Application
    'Set paramIn18 = cmd.CreateParameter("", 129, 1, 15)    'Username
    'Set paramIn19 = cmd.CreateParameter("", 129, 1, 15)    'Password
    'Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value contactId
    
	' Assign the values to the parameters
   ' paramIn1.Value = strWebID
    'paramIn2.Value = strHonorific
    'paramIn3.Value = strFirstName
    'paramIn4.Value = strLastName
    'paramIn5.Value = strTitle
    'paramIn6.Value = strCompany
    'paramIn7.Value = strAddress
    'paramIn8.Value = strAddress2
    'paramIn9.Value = strCity
    'paramIn10.Value = strState
    'paramIn11.Value = strPostalCode
    'paramIn12.Value = strCountry
    'paramIn13.Value = strPhone
    'paramIn14.Value = strFax
    'paramIn15.Value = strEmail
    'paramIn16.Value = strURL
    'paramIn17.Value = strApplication
    'paramIn18.Value = strUsername
    'paramIn19.Value = strPassword
    
   ' cmd.Parameters.Append paramIn1
    'cmd.Parameters.Append paramIn2
    'cmd.Parameters.Append paramIn3
    'cmd.Parameters.Append paramIn4
    'cmd.Parameters.Append paramIn5
    'cmd.Parameters.Append paramIn6
    'cmd.Parameters.Append paramIn7
    'cmd.Parameters.Append paramIn8
    'cmd.Parameters.Append paramIn9
    'cmd.Parameters.Append paramIn10
    'cmd.Parameters.Append paramIn11
    'cmd.Parameters.Append paramIn12
    'cmd.Parameters.Append paramIn13
    'cmd.Parameters.Append paramIn14
    'cmd.Parameters.Append paramIn15
    'cmd.Parameters.Append paramIn16
    'cmd.Parameters.Append paramIn17
    'cmd.Parameters.Append paramIn18
    'cmd.Parameters.Append paramIn19
    'cmd.Parameters.Append paramOut
    '****************************************
    
    ' Execute the command.
    'Set cmd.ActiveConnection = cnn

    'cmd.Execute
    ' Assign contact id
    
   
    'Set cmd = Nothing

End Function

Function UpdateContact(strHonorific, strFirstName, strLastName, strTitle, strJobFunction, strCompany, strDepartment, strAddress, strAddress2, strCity, strState, strPostalCode, strCountry, strPhone, strFax, strEmail, strURL, strApplication, strContactId)
    
    ' Author:           Nick Benes
    ' Date:             Sept 03, 1998
    ' Description:      Update a contact
    ' Returns:          The contactId just updated
    ' Revisions:        0
       
   ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
            UpdateContact = 0
        Exit Function
    End If
    'REsponse.Write strDepartment
    'Response.end
    '****************Insert old data into CONTACT_UPDATED table
    if len(strContactId)>0 then
    set rsSaveContactData=server.CreateObject("ADODB.Recordset")
    strSQL="INSERT INTO CONTACT_UPDATED   (CONTACT_ID, CONTACT_WEB_ID, HONORIFIC, FIRST_NAME, LAST_NAME, PASSWORD, USERNAME, TITLE, JOB_FUNCTION, COMPANY, DEPARTMENT, ADDRESS, ADDRESS2, CITY, STATE_PROVINCE, POSTALCODE, COUNTRY, PHONE, FAX, EMAIL, URL, DATE_ENTERED, ENTERED_BY,  DATE_LAST_MODIFIED, MODIFIED_BY, APPLICATION, NICK_NAME, LAST_NAME_UPPER, APPROVED_FLAG, APPROVED_DATE)  SELECT CONTACT_ID, CONTACT_WEB_ID, HONORIFIC, FIRST_NAME, LAST_NAME, PASSWORD, USERNAME, TITLE, JOB_FUNCTION, COMPANY, DEPARTMENT, ADDRESS, ADDRESS2, CITY, STATE_PROVINCE, POSTALCODE, COUNTRY, PHONE, FAX, EMAIL, URL, DATE_ENTERED, ENTERED_BY,  DATE_LAST_MODIFIED, MODIFIED_BY, APPLICATION, NICK_NAME, LAST_NAME_UPPER, APPROVED_FLAG, APPROVED_DATE FROM CONTACT WHERE CONTACT_ID = " & strContactId
    rsSaveContactData.Open strSQL,dbconnstr()
    
    '****************Update changed customer onfo in contact table
    'Session.CodePage=1252 
    set rsUpdateContact=server.CreateObject("ADODB.Recordset")
   ' strSQL="select * from CONTACT where CONTACT_ID = " & strContactId
    'rsUpdateContact.LockType=3
    'rsUpdateContact.Open strSQL,dbconnstr()
    'rsUpdateContact.Fields("FIRST_NAME")=replace(strFirstName,"'","''") 
    'rsUpdateContact.Update 
    strSQL=	"UPDATE CONTACT SET  HONORIFIC=N'" & replace(strHonorific,"'","''") &_
		"',FIRST_NAME =N'" & replace(strFirstName,"'","''") & "',LAST_NAME=N'" &_
			replace(strLastName,"'","''") & "',TITLE=N'" & replace(strTitle,"'","''") & _
			"', JOB_FUNCTION=N'" & replace(strJobFunction,"'","''")&_
			"',COMPANY=N'" & replace(strCompany,"'","''") & "',ADDRESS=N'" &_
			replace(strAddress,"'","''") & "',ADDRESS2=N'" & replace(strAddress2,"'","''") &_
			"',CITY=N'" & replace(strCity,"'","''") & "',STATE_PROVINCE=N'" & _
			replace(strState,"'","''") & "',POSTALCODE=N'" &replace(strPostalCode,"'","''") &_
			"',COUNTRY=N'" &replace(strCountry,"'","''") & "',PHONE=N'" &_
			replace(strPhone,"'","''") & "',FAX=N'" & replace(strFax,"'","''") &_
			"',EMAIL=N'" & replace(strEmail,"'","''") & "',URL=N'" & replace(strURL,"'","''") &_
			"',APPLICATION=N'" & replace(strApplication,"'","''") &_
			"',DEPARTMENT=N'" & replace(strDepartment,"'","''") & _
			"',DATE_LAST_MODIFIED = GETDATE(),MODIFIED_BY = 'webuser' " &_
		    "WHERE CONTACT_ID = " & strContactId
    'on error resume next
   ' Response.Write strSQL
    'REsponse.end
    rsUpdateContact.Open strSQL,dbconnstr()
	'if len(GetContactIDOnWebId(strWebID))>0 then
	UpdateContact  = strContactId
    'else
	'	 Response.Redirect "/cgi-bin/profile/RegisterAgain.asp"
    'end if
     ' Open command object
   ' Set cmd = Server.CreateObject("ADODB.Command")
   ' Set paramIn1 = CreateObject("ADODB.Parameter")
   ' Set paramIn2 = CreateObject("ADODB.Parameter")
   ' Set paramIn3 = CreateObject("ADODB.Parameter")
   ' Set paramIn4 = CreateObject("ADODB.Parameter")
   ' Set paramIn5 = CreateObject("ADODB.Parameter")
   ' Set paramIn6 = CreateObject("ADODB.Parameter")
   ' Set paramIn7 = CreateObject("ADODB.Parameter")
   ' Set paramIn8 = CreateObject("ADODB.Parameter")
   ' Set paramIn9 = CreateObject("ADODB.Parameter")
   ' Set paramIn10 = CreateObject("ADODB.Parameter")
   ' Set paramIn11 = CreateObject("ADODB.Parameter")
   ' Set paramIn12 = CreateObject("ADODB.Parameter")
   ' Set paramIn13 = CreateObject("ADODB.Parameter")
   ' Set paramIn14 = CreateObject("ADODB.Parameter")
   ' Set paramIn15 = CreateObject("ADODB.Parameter")
   ' Set paramIn16 = CreateObject("ADODB.Parameter")
   ' Set paramIn17 = CreateObject("ADODB.Parameter")
   ' Set paramOut = CreateObject("ADODB.Parameter")
	
	'cmd.CommandText = "{call ZP_UPDATE_CONTACT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
    'Set paramIn1 = cmd.CreateParameter("", 129, 1, 15)    'Nick_name
	'Set paramIn1 = cmd.CreateParameter("", 200, 1, 5)      'Honorific
    'Set paramIn2 = cmd.CreateParameter("", 200, 1, 30)     'FirstName
    'Set paramIn3 = cmd.CreateParameter("", 200, 1, 30)     'LastName
    'Set paramIn4 = cmd.CreateParameter("", 200, 1, 30)     'Title
    'Set paramIn5 = cmd.CreateParameter("", 200, 1, 50)     'Company
    'Set paramIn6 = cmd.CreateParameter("", 200, 1, 255)    'Address
    'Set paramIn7 = cmd.CreateParameter("", 200, 1, 255)    'Address2
    'Set paramIn8 = cmd.CreateParameter("", 200, 1, 50)     'City
    'Set paramIn9 = cmd.CreateParameter("", 200, 1, 50)    'State_Province
    'Set paramIn10 = cmd.CreateParameter("", 200, 1, 20)    'Postal Code
    'Set paramIn11 = cmd.CreateParameter("", 129, 1, 50)    'Country
    'Set paramIn12 = cmd.CreateParameter("", 129, 1, 30)    'Phone
    'Set paramIn13 = cmd.CreateParameter("", 129, 1, 30)    'Fax
    'Set paramIn14 = cmd.CreateParameter("", 129, 1, 50)    'E-mail
    'Set paramIn15 = cmd.CreateParameter("", 129, 1, 50)    'URL
    'Set paramIn16 = cmd.CreateParameter("", 200, 1, 50)    'Application
    'Set paramIn17 = cmd.CreateParameter("", 200, 1, 50)    'strContact_id
    'Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value contactId
    
	' Assign the values to the parameters
    'paramIn1.Value = strHonorific
    'paramIn2.Value = strFirstName
    'paramIn3.Value = strLastName
    'paramIn4.Value = strTitle
    'paramIn5.Value = strCompany
    'paramIn6.Value = strAddress
    'paramIn7.Value = strAddress2
    'paramIn8.Value = strCity
    'paramIn9.Value = strState
    'paramIn10.Value = strPostalCode
    'paramIn11.Value = strCountry
    'paramIn12.Value = strPhone
    'paramIn13.Value = strFax
    'paramIn14.Value = strEmail
    'paramIn15.Value = strURL
    'paramIn16.Value = strApplication
    'paramIn17.Value = strContactId
    
    'cmd.Parameters.Append paramIn1
    'cmd.Parameters.Append paramIn2
    'cmd.Parameters.Append paramIn3
    'cmd.Parameters.Append paramIn4
    'cmd.Parameters.Append paramIn5
    'cmd.Parameters.Append paramIn6
    'cmd.Parameters.Append paramIn7
    'cmd.Parameters.Append paramIn8
    'cmd.Parameters.Append paramIn9
    'cmd.Parameters.Append paramIn10
    'cmd.Parameters.Append paramIn11
    'cmd.Parameters.Append paramIn12
    'cmd.Parameters.Append paramIn13
    'cmd.Parameters.Append paramIn14
    'cmd.Parameters.Append paramIn15
    'cmd.Parameters.Append paramIn16
    'cmd.Parameters.Append paramIn17
    'cmd.Parameters.Append paramOut
    ' Execute the command.
    'Set cmd.ActiveConnection = cnn
    'cmd.Execute
    ' If OK
    'UpdateContact = paramOut.Value
    'Set cmd = Nothing
  end if
End Function

Function UpdateContactNickName(strNickName, strContactId)
    
    ' Author:           Nick Benes
    ' Date:             April 28, 1999
    ' Description:      Update a contact adding the nick name for auction
    ' Returns:          The contactId just updated
    ' Revisions:        0
    
	Dim cmd
    Dim paramIn1
    Dim paramIn2
    Dim paramOut
    
   ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
            UpdateContactNickName = 0
        Exit Function
    End If
    
     ' Open command object
    Set cmd = Server.CreateObject("ADODB.Command")
    Set paramIn1 = CreateObject("ADODB.Parameter")
    Set paramIn2 = CreateObject("ADODB.Parameter")
    Set paramOut = CreateObject("ADODB.Parameter")
	
	cmd.CommandText = "{call ZP_UPDATE_CONTACT_NICK_NAME(?,?,?)}"
    Set paramIn1 = cmd.CreateParameter("", 200, 1, 15)    'Nick_Name
	Set paramIn2 = cmd.CreateParameter("", 200, 1, 50)    'strContact_id
    Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value contactId
    
	' Assign the values to the parameters
    paramIn1.Value = strNickName
    paramIn2.Value = strContactId
    
    cmd.Parameters.Append paramIn1
    cmd.Parameters.Append paramIn2
    cmd.Parameters.Append paramOut
    ' Execute the command.
    Set cmd.ActiveConnection = cnn
    cmd.Execute
    ' If OK
    UpdateContactNickName = paramOut.Value
    Set cmd = Nothing
  
End Function

'******************************************
' Author:           Nick Benes
' Date:             June 25, 1998
' Insert requests
' Modified by Kate Kaplan	4/1/2003 
' Description:      Insert a new request
' Returns:          The Request_Id just inserted
'**********************************************
Function InsertRequest(lgContactId, lgRequestTypeId, lgObjectId, strRemarks, strApplication,lgPageID)
    ' Author:           Kate Kaplan
    ' Date:             June 25, 1998
    ' Description:      Insert a new request
    ' Returns:          The Request_Id just inserted
    ' Revisions:        1 to accomodate the application type
    '					2 to support unicodes - 04/12/2006

    
   ' Check connection
    If Not OpenConnection Then
       MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        InsertRequest = 0
        Exit Function
    End If
    
     ' Open command object
   ' Set cmdRequest= Server.CreateObject("ADODB.Command")
   set rsInsertRequest =Server.CreateObject("ADODB.Recordset")
   ' Set paramIn1 = CreateObject("ADODB.Parameter")
   ' Set paramIn2 = CreateObject("ADODB.Parameter")
   ' Set paramIn3 = CreateObject("ADODB.Parameter")
   ' Set paramIn4 = CreateObject("ADODB.Parameter")
	'Set paramIn5 = CreateObject("ADODB.Parameter")
	'Set paramIn6 = CreateObject("ADODB.Parameter")
	'Set paramIn7 = CreateObject("ADODB.Parameter")
    'Set paramOut = CreateObject("ADODB.Parameter")

     ' This is used for the new MSORCL32.dll which doesn't accept the statements like above
    ' cmd.CommandText="{call ZP_INSERT_REQUEST(?,?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    ' adInteger = 3
    ' adDouble  = 5  , adParamOutput =2
   ' Set paramIn1 = cmd.CreateParameter("", 200, 1, 12)     'ContactId
   ' Set paramIn2 = cmd.CreateParameter("", 200, 1, 5)      'RequestTypeId
    'Set paramIn3 = cmd.CreateParameter("", 200, 1, 6)      'ObjectID
    'Set paramIn4 = cmd.CreateParameter("", 201, 1, 4000)   'Remarks
	'Set paramIn5 = cmd.CreateParameter("", 200, 1, 50)	   'Application
	'Set paramIn6 = cmd.CreateParameter("", 3, 1)		  'PageID
	'Set paramIn7 = cmd.CreateParameter("", 3, 1)		  'Campaign ID
    'Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value Request_Id
    ' Assign the values to the parameters
   ' paramIn1.Value = lgContactId
    'paramIn2.Value = lgRequestTypeId
    'paramIn3.Value = lgObjectId
    'paramIn4.Value = strRemarks
    'paramIn5.Value = strApplication
    'paramIn6.Value = lgPageID
    if len(Session("CampaignID"))=0 then Session("CampaignID")=0
   ' paramIn7.Value = Session("CampaignID")
   ' cmd.Parameters.Append paramIn1
   ' cmd.Parameters.Append paramIn2
   ' cmd.Parameters.Append paramIn3
   ' cmd.Parameters.Append paramIn4
'	cmd.Parameters.Append paramIn5
'	cmd.Parameters.Append paramIn6
'	cmd.Parameters.Append paramIn7
 '   cmd.Parameters.Append paramOut
 strDate= NOW()
   strSQL = " INSERT INTO REQUEST (CONTACT_ID, REQUEST_TYPE_ID, OBJECT_ID, REMARKS, ENTERED_BY, DATE_ENTERED, DATE_RESPONDED, RESPONDED_BY,PAGEID,CAMPAIGN_ID) " &_
						" VALUES(" & lgContactId & "," & lgRequestTypeId & "," & lgObjectId & ",N'" & strRemarks & "','" & "webuser" & "','" & strDate & "','" &  strDate & "','" & "webuser" & "'," & lgPageID & "," & Session("CampaignID") & ")"
 ' cmdRequest.CommandType=1'adCmdText

 rsInsertRequest.LockType=3
 rsInsertRequest.Open strSQL, cnn
  '****************************************
    
    ' Execute the command.
    'Set cmdRequest.ActiveConnection = cnn
    'cmdRequest.Execute
	'Select Request_id which was created
	set rsREQUESTID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT REQUEST_ID from REQUEST where CONTACT_ID=" & Session("ContactID") & " and OBJECT_ID='" & lgObjectId & "' and DATE_ENTERED='" & strDate & "' and REQUEST_TYPE_ID=" & lgRequestTypeId
	
	rsREQUESTID.open strSQL, cnn
	if not rsREQUESTID.eof then
		InsertRequest=rsREQUESTID("REQUEST_ID")
	end if
	 'response.Write rsREQUESTID("REQUEST_ID")
 'response.End 
    'InsertRequest = paramOut.Value
	'Set cmd = Nothing

End Function

Function InsertSurveyResponse(lgContactId, lgSurveyTopicId, lgSurveyMasterQId, lgSurveySubQId, strSurveyValue, strRemarks)
    ' Author:           Nick Benes
    ' Date:             June 25, 1998
    ' Description:      Insert a new survey response
    ' Returns:          The Survey_Response_Id just inserted
    ' Revisions:        0
    
    Dim cmd
    Dim paramIn1
    Dim paramIn2
    Dim paramIn3
    Dim paramIn4
    Dim paramIn5
    Dim paramIn6
	
    Dim paramOut
    
   ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
     ' Open command object
    Set cmd = Server.CreateObject("ADODB.Command")
    Set paramIn1 = CreateObject("ADODB.Parameter")
    Set paramIn2 = CreateObject("ADODB.Parameter")
    Set paramIn3 = CreateObject("ADODB.Parameter")
    Set paramIn4 = CreateObject("ADODB.Parameter")
    Set paramIn5 = CreateObject("ADODB.Parameter")
    Set paramIn6 = CreateObject("ADODB.Parameter")
    Set paramOut = CreateObject("ADODB.Parameter")

     '****************************************
     ' This is used for the new MSORCL32.dll which doesn't accept the statements like above
    cmd.CommandText = "{call ZP_INSERT_SURVEY_RESPONSE(?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    'adDouble  = 5 , adParamOutput =2

    Set paramIn1 = cmd.CreateParameter("", 200, 1, 12)     'ContactId
    Set paramIn2 = cmd.CreateParameter("", 200, 1, 6)      'lgSurveyTopicId
    Set paramIn3 = cmd.CreateParameter("", 200, 1, 6)      'lgSurveyMasterQId
    Set paramIn4 = cmd.CreateParameter("", 200, 1, 6)      'lgSurveySubQId
    Set paramIn5 = cmd.CreateParameter("", 200, 1, 10)     'strSurveyValue
    Set paramIn6 = cmd.CreateParameter("", 200, 1, 2000)   'Remarks
    Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value Survey_Response_Id
    ' Assign the values to the parameters
    paramIn1.Value = lgContactId
    paramIn2.Value = lgSurveyTopicId
    paramIn3.Value = lgSurveyMasterQId
    paramIn4.Value = lgSurveySubQId
    paramIn5.Value = strSurveyValue
    paramIn6.Value = strRemarks
    
    cmd.Parameters.Append paramIn1
    cmd.Parameters.Append paramIn2
    cmd.Parameters.Append paramIn3
    cmd.Parameters.Append paramIn4
    cmd.Parameters.Append paramIn5
    cmd.Parameters.Append paramIn6
    cmd.Parameters.Append paramOut
    '****************************************
    
    ' Execute the command.
    Set cmd.ActiveConnection = cnn
    cmd.Execute
    ' Assign the survey_response id
    InsertSurveyResponse = paramOut.Value
	Set cmd = Nothing

End Function

Function GetDownloadId(strDownloadShortName)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the DownloadId based on Download_ShortName
    ' Returns:          The Id
    ' Revisions:        0
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT DOWNLOAD_ID FROM V_DOWNLOAD WHERE UPPER(SHORT_NAME) = '" & Trim(Ucase(strDownloadShortName)) & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetDownloadId = CLng(rs.fields("DOWNLOAD_ID").Value)
	Else
		GetDownloadId = 0
	End If
	rs.Close
End Function

Function GetRequestTypeId(strRequestTypeShortName)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the RequestType_Id based on RequestTypeShortName
    ' Returns:          RequestType_Id
    ' Revisions:        0
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT REQUEST_TYPE_ID FROM V_REQUEST_TYPE WHERE UPPER(SHORT_NAME) = '" & Trim(Ucase(strRequestTypeShortName)) & "'"
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetRequestTypeId = CInt(rs.fields("REQUEST_TYPE_ID").Value)
	Else
		GetRequestTypeId = 0
	End If
	rs.Close
End Function

Public Function GetStates(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the US states in alpha order
    ' Revisions:        0
    
    Dim strSQL
    
    ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT NAME FROM V_STATE ORDER BY NAME"
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Function

Sub GetCountries(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the US countries in alpha order
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT NAME FROM V_COUNTRY ORDER BY NAME"
    rs.ActiveConnection = cnn
    rs.Open strSQL
End Sub

Sub GetApplications(rs)
    ' Author:           Nick Benes
    ' Date:             July 17, 1998
    ' Description:      Gets the Applications Type
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "SELECT NAME FROM V_APPLICATION ORDER BY NAME"
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Sub GetModels(rs, blAll)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    '****** Modified by Kate Kaplan - 9/27/2004 (excluded WS models)
    ' Description:      Gets the models in alpha order
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    
    Dim strSQL
    Dim strTemp
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS"
    Else
        strTemp = "V_CUR_MODELS"
    End If
    
    'if len(Session("Campaignid"))>0 then
	'	if cint(Session("CampaignID"))=7 then
	'		strSQL = "SELECT MODEL_ID, NAME FROM MODEL where FAMILY_MAILER_FLAG='y' and PRIVATE_FLAG<>'y' ORDER BY NAME"
	'	else
	'		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " ORDER BY NAME"
	'	end if
	'else
		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " where MODEL_ID NOT IN (171,172,173) AND (DEMO_YN = 'y') ORDER BY SORT_ID"
	'end if
	    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetRegModels(rs, blAll)
    ' Author:           Kate Kaplan - 9/27/2004 
    ' Description:      Gets the models in alpha order
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    
    Dim strSQL
    Dim strTemp
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS"
    Else
        strTemp = "V_CUR_MODELS"
    End If
    
    'if len(Session("Campaignid"))>0 then
	'	if cint(Session("CampaignID"))=7 then
	'		strSQL = "SELECT MODEL_ID, NAME FROM MODEL where FAMILY_MAILER_FLAG='y' and PRIVATE_FLAG<>'y' ORDER BY NAME"
	'	else
	'		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " ORDER BY NAME"
	'	end if
	'else
		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " where  PSG_YN='n' ORDER BY SORT_ID"
	'end if
	'Response.Write strSQL
	    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetRegModelsJapan(rs, blAll)
    ' Author:           Kate Kaplan - 6/14/2005
    ' Description:      Gets the models in alpha order
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS_JAPAN"
    Else
        strTemp = "V_CUR_MODELS_JAPAN"
    End If
    
    'if len(Session("Campaignid"))>0 then
	'	if cint(Session("CampaignID"))=7 then
	'		strSQL = "SELECT MODEL_ID, NAME FROM MODEL where FAMILY_MAILER_FLAG='y' and PRIVATE_FLAG<>'y' ORDER BY NAME"
	'	else
	'		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " ORDER BY NAME"
	'	end if
	'else
		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " where (MODEL_ID NOT IN (153,154,155,156,157,158,171,172,173)and PSG_YN='n') or (PSG_YN='y' and DEMO_YN='n') ORDER BY SORT_ID"
	'end if
	'Response.Write strSQL
	    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetRegProtocols(rs, blAll)
    ' Author:           Kate Kaplan - 6/14/2004 
    ' Description:      Gets the models in alpha order
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    
    Dim strSQL
    Dim strTemp
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS"
    Else
        strTemp = "V_CUR_MODELS"
    End If
    
    'if len(Session("Campaignid"))>0 then
	'	if cint(Session("CampaignID"))=7 then
	'		strSQL = "SELECT MODEL_ID, NAME FROM MODEL where FAMILY_MAILER_FLAG='y' and PRIVATE_FLAG<>'y' ORDER BY NAME"
	'	else
	'		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " ORDER BY NAME"
	'	end if
	'else
		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " where PSG_YN='y' and DEMO_YN='n' ORDER BY SORT_ID"
	'end if
	'Response.Write strSQL
	    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetWSModels(rs, blAll)
    ' Author:           Kate Kaplan
    ' Date:             Sept 27, 2004
    '****** Modified by Kate Kaplan - 9/27/2004 (excluded WS models)
    ' Description:      Gets the models in alpha order
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    
    Dim strSQL
    Dim strTemp
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS"
    Else
        strTemp = "V_CUR_MODELS"
    End If
    
    'if len(Session("Campaignid"))>0 then
	'	if cint(Session("CampaignID"))=7 then
	'		strSQL = "SELECT MODEL_ID, NAME FROM MODEL where FAMILY_MAILER_FLAG='y' and PRIVATE_FLAG<>'y' ORDER BY NAME"
	'	else
	'		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " ORDER BY NAME"
	'	end if
	'else
		strSQL = "SELECT MODEL_ID, NAME FROM " & strTemp & " where MODEL_ID  IN (153,154,155,156,157,158,171,172,173)ORDER BY SORT_ID"
	'end if
	    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetModelsJapan(rs, blAll)
    ' Author:           Kate Kaplan
    ' Date:             May 22 2003
    ' Description:      Gets the models in alpha order for Japan
    '                   If blAll = TRUE - returns all Models
    '                   Else Current Models only
    ' Revisions:        0
    
    Dim strSQL
    Dim strTemp
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    ' Get the view to use depending of the blAll
    If blAll Then
        'strTemp = "V_NOT_NEW_MODELS"
        strTemp = "V_CUR_MODELS_JAPAN"
    Else
        strTemp = "V_CUR_MODELS_JAPAN"
    End If
	strSQL = "SELECT MODEL_ID, NAME FROM V_CUR_MODELS_JAPAN where  (DEMO_YN = 'y') ORDER BY SORT_ID"
    'Response.Write strSQL
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub
Sub GetLiteraturesCategory(rs, country)

    ' Author:           Nick Benes
    'Modified by YK 8/8/2001
    ' Date:             June 23, 1998
    ' Description:      Gets the literature in alpha order
    ' Revisions:        0
   
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
   
    if len(Session("CampaignID"))>0 then
		if cint(Session("CampaignID"))=7 then
			strSQL =	"SELECT DISTINCT LITERATURE_CATEGORY.LITCATID, LITERATURE_CATEGORY.LITCATEGORY, LITERATURE_CATEGORY.SORTID "&_
					 	"FROM LITERATURE_CATEGORY INNER JOIN LITERATURE ON LITERATURE_CATEGORY.LITCATID = LITERATURE.LITCATID "&_
						"WHERE (LITERATURE.FAMILY_MAILER_FLAG = 'y') and LITERATURE.COUNTRY_ID = " & country & " and LITERATURE.PRIVATE_FLAG<>'y'order by LITERATURE_CATEGORY.SORTID"
		else
			strSQL = "SELECT DISTINCT LITCATID, LITCATEGORY,SORTID FROM V_ALL_LITERATURES WHERE COUNTRY_ID = " & country & " ORDER BY SORTID"
		end if
    else
		strSQL = "SELECT DISTINCT LITCATID, LITCATEGORY,SORTID FROM V_ALL_LITERATURES WHERE COUNTRY_ID = " & country & " ORDER BY SORTID"
    end if
   ' Response.write strSQL
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Sub GetDownloads(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the literature in alpha order
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT DOWNLOAD_ID, NAME FROM V_ALL_DOWNLOADS ORDER BY NAME"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Sub GetRequestTypes(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the all request_type
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT REQUEST_TYPE_ID, NAME FROM V_ALL_REQUEST_TYPES ORDER BY NAME"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Sub GetSeminars(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the all seminars
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT SEMINAR_ID, NAME FROM V_ALL_SEMINARS ORDER BY NAME"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Sub GetSurveyTopic(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the all seminars
    ' Revisions:        0
    
    Dim strSQL
    
    ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT SURVEY_TOPIC_ID, DESCRIPTION FROM V_ALL_SEMINARS ORDER BY DESCRIPTION"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Public Sub GetSurveyMasterQuestions(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the all seminars
    ' Revisions:        0
    
    Dim strSQL
    
    ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT SUR_MASTER_QUESTION_ID, DESCRIPTION FROM V_ALL_MASTER_QUESTIONS ORDER BY DESCRIPTION"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL

End Sub

Public Sub GetSurveySubQuestions(rs)
    ' Author:           Nick Benes
    ' Date:             June 23, 1998
    ' Description:      Gets the all seminars
    ' Revisions:        0
    
    Dim strSQL
    
     ' Check connection
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Sub
    End If
    
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    
    strSQL = "SELECT SUR_SUB_QUESTION_ID, DESCRIPTION FROM V_ALL_SUB_QUESTIONS ORDER BY DESCRIPTION"
    
    rs.ActiveConnection = cnn
    rs.Open strSQL, cnn, adOpenForwardOnly, adLockReadOnly, adCmdText

End Sub

'*************************************
'Check if Literature has been selected
'YK 8/10/01
'arguments (litid)
'Returns Boolean
'*************************************
function checkliteratureselected(litid)
'check if array Session("literature") exists
	lit=Session("literature")

	if UBound(lit)>0 then
		for i=0 to (UBound(lit)-1)
			if cint(litid)=cint(lit(i)) then
				checkliteratureselected=true
				exit function
			else
				checkliteratureselected=false
			end if
		next
	else
		checkliteratureselected=false
	end if
end function

'*************************************
'Check if DEMO has been selected
'YK 8/10/01
'arguments (demoid)
'Returns Boolean
'*************************************
function checkdemoselected(demoid)
'check if array Session("demo") exists
	demo=Session("demo")

	if UBound(demo)>0 then
		for i=0 to (UBound(demo)-1)
			
			if cint(demoid)=cint(demo(i)) then
				checkdemoselected=true
				exit function
			else
				checkdemoselected=false
			end if
		next
	else
		checkdemoselected=false
	end if
end function



'*************************************
'Check if MODELS for Technical Help has been selected
'YK 8/10/01
'arguments (modelid)
'Returns Boolean
'*************************************
function checkhelpmodelselected(modelid)
'check if array Session("helpmodel") exists
	helpmodel=Session("helpmodel")

	if UBound(helpmodel)>0 then
		for i=0 to (UBound(helpmodel)-1)
			
			if cint(modelid)=cint(helpmodel(i)) then
				checkhelpmodelselected=true
				exit function
			else
				checkhelpmodelselected=false
			end if
		next
	else
		checkhelpmodelselected=false
	end if
end function




'*************************************
'Check if SEMINAR has been selected
'YK 8/15/01
'arguments (seminarid)
'Returns Boolean
'*************************************
function checkseminarselected(seminarid)
'check if array Session("helpmodel") exists
	seminar=Session("seminar")

	if UBound(seminar)>0 then
		for i=0 to (UBound(seminar)-1)
			
			if cint(seminarid)=cint(seminar(i)) then
				checkseminarselected=true
				exit function
			else
				checkseminarselected=false
			end if
		next
	else
		checkseminarselected=false
	end if
end function
'*************************************
'Insert new subscription (table "SUBSCRIPTION" db )
'rev1 -KK 11/2/01
'rev2 -KK 10/18/2006
'rev3 -KK 9/24/2008 (registration Bug?? fix)
'arguments (strForm)
'*************************************
sub InsertSubscription()
strCRet = chr(13) & chr(10)
'If this is a new user
'he came from registration form
sub_selected=Session("SelectedSub")
if len(Session("subformat"))=0 then Session("subformat")=2
'Response.Write Session("SelectedSubCount")
for i=0 to (Session("SelectedSubCount")-1)
	if Session("Contactid")>0 and len(Session("email"))> 0  and OpenConnection and len(sub_selected(i))>0 then 
'check if we have his email address in SUBSCRUPTION table
		set rsExist=server.CreateObject("ADODB.Recordset")
		strSQL="Select * from  SUBSCRIPTION where EMAIL='" & Session("email")& "' and DOCUMENT_ID=" & sub_selected(i)
		'Response.Write strSQL
		rsExist.Open strSQL,cnn
		if rsExist.EOF then
		'We don't have subscription for this user.Insert new subscription 
			
			set rsSubsription=server.CreateObject("ADODB.Recordset")
			strSQL=	"Insert into SUBSCRIPTION (SUB_ID,EMAIL,DOCUMENT_ID,DATE_ENTERED,CONTACT_ID,ENABLE,COUNTRY_ID,FORMAT_ID) values ('" & GenerateUserID & "','" & Session("Email") & "'," & sub_selected(i) & ",'" & now() & "'," & Session("Contactid") & ",'y'," & getcountryoncountyname(Session("Country"))& "," & Session("subformat")& ")"
			'Response.Write strSQL
			'rsSubsription.LockType=3
			'rsSubsription.Open strSQL,cnn
			'call sendsubnotificationemail(getsubnameondocid(cint(sub_selected(i))))
		else
			set rsSubUpdate=server.CreateObject("ADODB.Recordset")
			strSQL=	"Update SUBSCRIPTION set CONTACT_ID=" & Session("ContactID") & ",ENABLE='y',COUNTRY_ID=" & getcountryoncountyname(Session("Country")) & _
				    ",DATE_MODIFIED='" & Now() & "',FORMAT_ID=" & Session("subformat") & " where EMAIl='" & Session("email") & "' and DOCUMENT_ID=" & sub_selected(i) 
			'Response.Write strSQL
			'rsSubUpdate.LockType=3
			'rsSubUpdate.open strSQL,cnn
		end if
		'**************************************************
	   ' set objMessage =Server.CreateObject("CDO.Message")
	   ' set objConfig = Server.CreateObject("CDO.Configuration")
	   ' ' Set the message properties and send the message.
		'    With objMessage
		'	    Set .Configuration = objConfig
		'	    .MimeFormatted = True
		'	    .Fields.Update
		'	    .To = "kate.kaplan@teledynelecroy.com"
		'	    .From = "webmaster@teledynelecroy.com"
		'	    .Subject = "LeCroy Registration Confirmation"
		'	    .TextBody = strSQL
		'	    .Send
		'    End With

	    'Set objMessage = Nothing
	    'Set objConfig = Nothing		
	    '**************************************************	
        on error resume next
		rsSubUpdate.open strSQL,cnn
		
		if len(strEmailList)>0 then
			strEmailList=strEmailList & ", " & strCRet & getsubnameondocid(cint(sub_selected(i))) 
		else
			strEmailList= getsubnameondocid(cint(sub_selected(i))) 
		end if
	end if
next
'response.write strEmail
'Response.end
'Send Subscription Updates Emails to customer
'if session("localeid") <>1041 and Session("Country")<>"Japan" then
'	strBody = "Your subscription profile had been modified."& strCRet & strCRet 
'	if len(strEmailList)>0 then
'		strBody = strBody & Session("email")& " has requested the following:"& strCRet& strCRet  
'		strBody = strBody & strEmailList
'	else
'		strBody = strBody & "You did select any subscriptions."
'	end if

'end if

end sub


'*************************************
'update ENABLE column in subscription table
'for current user 
'rev1 -KK 11/5/01
'rev2 - KK 10/18/2006
'arguments -none
'*************************************

sub UpdateSubscription(flgExist,strDisable,docid)
	
	if len(strDisable)>0 then 
		strDisable="y"
	else
		strDisable="n"
	end if
'User exist
	if len (Session("subformat"))=0 then Session("subformat")=2 
	if  len(Session("Email"))>0 then'Session("Contactid")>0 and
		'if record for this user exist then update it
		
		if flgExist then
			if OpenConnection then
				set rsUpdateSubsription=server.CreateObject("ADODB.Recordset")
				strSQL=	"Update SUBSCRIPTION set EMAIL='"  & Session("Email") & _
				        "',enable='" & strDisable & "', COUNTRY_ID= " & _
				        getcountryoncountyname(Session("Country")) & _
				        ",DATE_MODIFIED='" &  Now() & "',CONTACT_ID=" & Session("ContactID") & ",format_id=" & Session("subformat") & " where EMAIL='" & Session("TempEmail")& "' and DOCUMENT_ID=" & docid 
				'Response.Write strSQL
				rsUpdateSubsription.LockType=3
			    rsUpdateSubsription.Open strSQL,cnn
			end if
		'or insert a new record
		else
			if OpenConnection then
				set rsInsertSubsription=server.CreateObject("ADODB.Recordset")
				strSQL=	"Insert into SUBSCRIPTION (SUB_ID,EMAIL,DOCUMENT_ID,DATE_ENTERED,CONTACT_ID,ENABLE,COUNTRY_ID,FORMAT_ID) " & _
				"values ('" & GenerateUserID & "','" & Session("Email") & "'," & docid & ",'" & now() & "'," & Session("Contactid") & ",'" & strDisable & "'," & getcountryoncountyname(Session("Country")) & "," & Session("subformat") & ")"
				'Response.Write strSQL
				'rsInsertSubsription.LockType=3
			    rsInsertSubsription.Open strSQL,cnn
    			'call sendsubnotificationemail(getsubnameondocid(cint(docid)))
			end if
	
		end if
	end if

end sub





'*************************************
'Check if we have record in subscription table
'for current user 
'rev 1	YK 11/5/01
'rev 2	KK 10/18/06
'arguments -none
'Returns Boolean
'*************************************
function checksubscriptionexist(docid)
	checksubscriptionexist=false
	if OpenConnection and len(docid)>0 then
		if len(Session("TempEmail"))=0 then 
			if len(Session("Email"))>0 then
				Session("TempEmail")=Session("Email")
			end if
		end if
		if len(Session("TempEmail"))>0 then
			set rsCheckSubscription = server.CreateObject("ADODB.Recordset")
			strSQL=	"SELECT DISTINCT * FROM SUBSCRIPTION WHERE EMAIL='" & Session("TempEmail") & "' and DOCUMENT_ID=" & docid ' and CONTACT_ID=" & Session("Contactid") 
			'Response.Write strSQL
			rsCheckSubscription.open strSQL,cnn
			if not rsCheckSubscription.EOF then
				checksubscriptionexist=true
			end if
		end if
	end if
end function
'*************************************
'Check if subscription is enable or disable in subscription table
'for current user 
'YK 11/5/01
'arguments -none
'Returns Boolean
'*************************************

function checksubscriptiondisable(docid)
	
	if OpenConnection then
		set rsSubscriptionStatus = server.CreateObject("ADODB.Recordset")
		strSQL=	"SELECT * FROM SUBSCRIPTION WHERE EMAIL='" & Session("TempEmail") & "' and DOCUMENT_ID=" & docid '"' and CONTACT_ID=" & Session("Contactid") 
	
	    rsSubscriptionStatus.open strSQL,cnn
	    if not rsSubscriptionStatus.EOF then
			if rsSubscriptionStatus("ENABLE")="y" then
				checksubscriptiondisable=true
			else
				checksubscriptiondisable=false
			end if
	    else
			checksubscriptiondisable=false
	    end if
	end if
end function


'*************************************
'Generate user_id for subsribtion table
'YK 11/08/01
'arguments -none
'Returns string
'*************************************

 Function GenerateUserID()
    bl = False
    Do While Not bl
        Do Until Len(strUID1) > 9 ' number of the strlen -1
            Randomize
            ' Get a character
            strUIDTemp1 = Chr(Int(44 * Rnd) + 48)
            
            ' Check if character is in the 0-9 and A-Z range
            If Asc(strUIDTemp1) < 58 Or (Asc(strUIDTemp1) > 64 And Asc(strUIDTemp1) < 91) Then
                strUID1 = strUID1 & strUIDTemp1
            End If
        Loop
        
        Do Until Len(strUID2) > 9 ' number of the strlen -1
            Randomize
            ' Get a character
            strUIDTemp2 = Chr(Int(44 * Rnd) + 48)
            
            ' Check if character is in the 0-9 and A-Z range
            If Asc(strUIDTemp2) < 58 Or (Asc(strUIDTemp2) > 64 And Asc(strUIDTemp2) < 91) Then
                strUID2 = strUID2 & strUIDTemp2
            End If
        Loop
        
        If UCase(CheckUserIDNotExist(strUID1,strUID2)) = "TRUE" Then
            bl = True
        Else
            strUID1 = ""
            strUID2 = ""
            bl = False
        End If
    Loop
    GenerateUserID=strUID1&strUID2
End Function


'*************************************
'Checks if user_id is not already exist in subsribtion table
'YK 11/08/01
'arguments -strUID
'Returns string
'*************************************

Function CheckUserIDNotExist(str1, str2)
   if OpenConnection then
		set rsSubExist= Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM SUBSCRIPTION WHERE SUBSTRING(SUB_ID,1,10)='" & str1 & "' and SUBSTRING(SUB_ID,11,10)='" & str2 & "'"
		rsSubExist.open strSQL,cnn

		If Not rsSubExist.EOF Then
			CheckUserIDNotExist= "FALSE"
		Else
			CheckUserIDNotExist= "TRUE"
		End If
	end if

End Function

'*************************************
'Get Country_id on Country_name
'YK 11/09/01
'arguments -CountryName
'Returns integer
'*************************************

Function getcountryoncountyname(CountryName)
   if len(CountryName)>0 and OpenConnection then
		set rsCountryID= Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM COUNTRY WHERE NAME = '"  & CountryName & "'"
	
		rsCountryID.open strSQL,cnn

		If Not rsCountryID.EOF Then
			
			getcountryoncountyname= rsCountryID("COUNTRY_ID")
			
		Else
			getcountryoncountyname= 0
		End If
	else
		getcountryoncountyname= 0
	end if

End Function

'*************************************
'Get Subscription type's name on document_id
'YK 11/13/01
'arguments -DOCID
'Returns string
'*************************************

Function getsubnameondocid(DOCID)
   if len(DOCID)>0 and OpenConnection then
		set rsSubName= Server.CreateObject("ADODB.Recordset")
		strSQL = "SELECT * FROM SUBSCRIPTION_TYPE WHERE DOCUMENT_ID=" & DOCID
		'Response.Write strSQL & "<br>"
		rsSubName.open strSQL,cnn

		If Not rsSubName.EOF Then
			'Response.Write rsSubName("NAME")
			getsubnameondocid = rsSubName("NAME")
		Else
			getsubnameonsubid=""
		End If
	else
		getsubnameonsubid= ""
	end if

End Function

'*************************************
'Sends new subscription notification email
'YK 11/13/01
'arguments -SUBNAME
'Returns string
'*************************************

Sub sendsubnotificationemail(SUBNAME)
	if len(SUBNAME)>0 and len(Session("EMAIL"))>0 then
		'Create E-Mail Body
		strCRet = "<br>"
		' Client data
		strBody = strBody & Session("EMAIL") & " has requested a subscription to the '" & SUBNAME & "'"
		strBody = strBody & strCRet & strCRet
		strBody = strBody & "Click on the hyperlink below view all subscribers"
		strBody = strBody & strCRet & strCRet
		strBody = strBody & "http://ny-wwwtest.lecroy.net/admin/subscription/confirm.asp"	
		strBody = strBody & CustomerInfoHTML
		'Response.Write strBody
		Set oMailSub = Server.CreateObject("CDO.Message")
		oMailSub.To ="kate.kaplan@teledynelecroy.com"
		oMailSub.From = "webmaster@teledynelecroy.com"
		oMailSub.Subject ="New Subscription:" & Session("EMAIL")
		oMailSub.HTMLBody = strBody
		on error resume next
		oMailSub.Send 
		Set oMailSub = Nothing ' Release the object
	end if

End Sub
'*************************************
'Get customer's info
'YK 11/13/01
'arguments -none
'Returns string
'*************************************
Function CustomerInfo()
	strInfo=""
	strCRet = chr(13) & chr(10)
	if cint(Session("localeid"))<>1041 then	
		' Client data
		if len(Session("FirstName")) >0 and  len(Session("LastName"))>0 then
			
			strInfo = strCRet & strCRet& strInfo & "Client: " & Session("LastName")& " " & LoadI18N("SLBCA0160",Session("localeid"))& " " &  Session("Firstname") & strCRet
		end if
		if len(Session("Title"))>0 then
			strInfo = strInfo & "Title: " & Session("Title")& strCRet
		end if
		if len(Session("Company"))>0 then
			strInfo = strInfo & "Company: " & Session("Company")& strCRet
		end if
		if len(Session("Department"))>0 then
			strInfo = strInfo & "Department: " & Session("Department")& strCRet
		end if
		if len(Session("Address"))>0 then
		strInfo = strInfo & "Address: " & Session("Address")& " " & Session("Address2") & strCRet
		end if
		if len(Session("City"))>0 then
			strInfo = strInfo & "City: " & Session("City")& strCRet
		end if
		if len(Session("State"))>0 then
			strInfo = strInfo & "State: " & Session("State")& strCRet
		end if
		if len(Session("Zip"))>0 then
			strInfo = strInfo & "Zip: " & Session("Zip")& strCRet
		end if
		if len(Session("Country"))>0 then
			strInfo = strInfo & "Country: " & Session("Country")& strCRet
		end if
		if len(Session("Phone"))>0 then
			strInfo = strInfo & "Phone: " & Session("Phone")& strCRet
		end if
		if len(Session("Fax"))>0 then
			strInfo = strInfo & "Fax: " & Session("Fax")& strCRet
		end if
		if len(Session("Email"))>0 then
			strInfo = strInfo & "Email: " & Session("Email")& strCRet
		end if
		if len(Session("URL"))>0 then
			strInfo = strInfo & "URL: " & Session("URL")& strCRet
		end if
		if len(Session("Applications"))>0 then
			strInfo = strInfo & "Application: " & Session("Applications")& strCRet
		end if
	else
	' Client data for Japan
	    if len(Session("Country"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0083",Session("localeid")) & ": " & Session("Country")& strCret
		end if
		if len(Session("Zip"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0082",Session("localeid")) & ": " & Session("Zip")& strCret
		end if
		if len(Session("City"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0080",Session("localeid")) & ": " & Session("City")& strCret
		end if
		if len(Session("Address"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0079",Session("localeid")) & ": " & Session("Address")& strCret
		end if
		if len(Session("Address2"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0079",Session("localeid")) & "2: " & Session("Address2")& strCret
		end if
		if len(Session("Company"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0078",Session("localeid")) & ": " & Session("Company")& strCret
		end if
		if len(Session("Title"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0077",Session("localeid")) & ": " & Session("Title")& strCret
		end if
		if len(Session("FirstName"))>0 and  len(Session("LastName"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0076",Session("localeid")) & ": "  &  Session("LastName")& " " & Session("Firstname") & " " & LoadI18N("SLBCA0160",Session("localeid"))& strCret
		end if
		if len(Session("Phone"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0084",Session("localeid")) & ": " & Session("Phone")& strCret
		end if
		if len(Session("Fax"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0085",Session("localeid")) & ": " & Session("Fax")& strCret
		end if
		if len(Session("Email"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0086",Session("localeid")) & ": " & Session("Email")& strCret
		end if
		if len(Session("URL"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0087",Session("localeid")) & ": " & Session("URL")& strCret
		end if
		if len(Session("Applications"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0088",Session("localeid")) & ": " & Session("Applications")& strCret
		end if
	end if
	' Insert empty line 
	strInfo = strInfo & strCRet
	'Response.Write strIntro
	' Return data 
	CustomerInfo = strInfo
	
End Function
'*************************************
'Load label for current localeid
'YK 8/15/01
'Arguments - Label,LocaleID
'Returns string
'*************************************
Function LoadI18N(Label,LocaleID)
    if len(Label)>0 and LocaleID>0 then
		if LocaleID=1031 or LocaleID=1040 or LocaleID=1036 then
			 LocaleID=1033
		end if
		Set rsLabels=server.CreateObject("ADODB.Recordset")
		strSQL="Select TEXT from I18NTEXT where LOCALEID=" & LocaleID & " and LABEL='" & Label & "'"
		'Response.Write strSQL&"<br>"
		Set cmdLabels=server.CreateObject("ADODB.COmmand")
		cmdLabels.CommandText="sp_LoadLabels"'strSQL
		cmdLabels.ActiveConnection=dbconnstr()
		'cmdLabels.CommandType=1'TEXT
		cmdLabels.CommandType=4'Store Procedure
		set ParamLOCALEID=cmdLabels.CreateParameter("@LOCALEID",3,1)'3 adInteger, 1 adParamInput
		cmdLabels.Parameters.Append ParamLOCALEID
		cmdLabels.Parameters("@LOCALEID")= LocaleID
		set ParamLabel=cmdLabels.CreateParameter("@Label",200,1,9)'200 varchar, 1 adParamInput
		cmdLabels.Parameters.Append ParamLabel
		cmdLabels.Parameters("@Label")= Label
		set rsLabels=cmdLabels.Execute
		'rsLabels.Open strSQL, dbconnstr()
		if not rsLabels.EOF then
			LoadI18N=trim(rsLabels("Text"))
		else  
			'LoadI18N=" Label " & Label & " "
			set rsLabelsUS=server.CreateObject("ADODB.Recordset")
			strSQL="Select TEXT from I18NTEXT where LOCALEID=1033 and LABEL='" & trim(Label) & "'"
			rsLabelsUS.Open strSQL, dbconnstr()
			'Response.Write strSQL&"<br>"
			if not rsLabelsUS.EOF then
				LoadI18N=trim(rsLabelsUS("Text"))
			else  
				LoadI18N=" Label " & Label & " "
			end if
		end if
	'else
	   'Response.Write "Label doesn't exist"
	end if
	'rsLabel.Close
	'set rsLabel=nothing
End Function


'******************************************
'Check if company exist in tblCompanyLookup table in CCMS database
'YK 8/1/2002
'arguments-companyname
'Returns boolean
'**********************************************

function checkcompanyexists(companyname)
	if len(companyname)>0 then
		'check if company exists in CONTACT table
		set rsContactCompany=server.CreateObject("ADODB.Recordset")
		strSQL="select * from CONTACT where COMPANY='" & replace(trim(companyname),"'","''") & "'"
		'Response.Write strSQL
		rsContactCompany.open strSQL, dbconnstr()
		if not rsContactCompany.EOF then
			checkcompanyexists=true
		else
			'check if company exists in tblCompanyLookup table
			set rsCompany=server.CreateObject("ADODB.Recordset")
			strSQL="Select * from tblCompanyLookup where CompanyName ='" & replace(trim(companyname),"'","''") & "'"
			rsCompany.Open strSQL, dbconnstr()
			'Response.Write "<br>" & strSQL
			if not rsCompany.EOF then
				checkcompanyexists=true
			else
				
				checkcompanyexists=false
			end if
		end if
	else
		checkcompanyexists=false
	end if
end function
'******************************************
'Get sunscription format by the email
'Kate Kaplan 4/3/2003
'arguments-email
'Returns integer
'**********************************************
function getsubscriptionformat(email)
if len(email)>0 then
	set rsGetSubFormat=server.CreateObject("ADODB.Recordset")
	strSQL=	" SELECT top 1 SUBSCRIPTION.FORMAT_ID as FORMAT_ID FROM SUBSCRIPTION_FORMAT INNER JOIN "&_
			" SUBSCRIPTION ON SUBSCRIPTION_FORMAT.FORMAT_ID = SUBSCRIPTION.FORMAT_ID "&_
			" WHERE (SUBSCRIPTION_FORMAT.ENABLE = 'y') AND (SUBSCRIPTION.EMAIL = '" & replace(email,"'","''") & "')"
	rsGetSubFormat.Open strSQL, dbconnstr()		
	if not rsGetSubFormat.EOF then
		getsubscriptionformat=rsGetSubFormat("FORMAT_ID")
	else
		getsubscriptionformat=0
	end if
else
	getsubscriptionformat=0
end if
end function

'************************************************************
'Author			Kate Kaplan
'Revision		1.0		6/6/2003	
'	check if customer registered for seminar happened in the past 
'	seminar also should have available presentation
'Revision		2.0	KK	12/18/2006	
'	check if there are presentations availbale for the password entered
'arguments		none
'returns		boolean
'************************************************************
Function presentation_approved()
'check if customer registered on the site
presentation_approved=false
ValidateUserNoredir 
if len(Session("ContactID"))>0 then
	'check if customer registered for the seminars in the past
	'and seminar has presentation
	set rsPresentationApproved=server.CreateObject("ADODB.Recordset")
		strSQL=	" SELECT DISTINCT SEMINAR.NAME, SEMINAR_PRESENTATION.URL, SEMINAR.DATE_OF_SEMINAR "&_
			" FROM SEMINAR INNER JOIN  SEMINAR_PRESENTATION ON "&_
			" SEMINAR.PRESENTATION_ID = SEMINAR_PRESENTATION.PRESENTATION_ID where SEMINAR_PRESENTATION.POST='y'"
			
	'strSQL=	"SELECT DISTINCT SEMINAR.NAME, SEMINAR_PRESENTATION.URL, SEMINAR.DATE_OF_SEMINAR "&_
	'		"FROM REQUEST INNER JOIN SEMINAR ON REQUEST.OBJECT_ID = SEMINAR.SEMINAR_ID "&_
	'		"INNER JOIN SEMINAR_PRESENTATION ON SEMINAR.PRESENTATION_ID = "&_
	'		"SEMINAR_PRESENTATION.PRESENTATION_ID AND  (SEMINAR.DATE_OF_SEMINAR < GETDATE()) " &_
	'		"and REQUEST.CONTACT_ID=" & Session("ContactID") 
	'Response.Write strSQL
	rsPresentationApproved.Open strSQL, dbconnstr()
	if not rsPresentationApproved.EOF then
		presentation_approved=true
	end if
end if
end function



'*************************************
'Shraddha Shah
'6/18/2004
'arguments (demoid)
'Returns Boolean
'*************************************
function checkContinuedProductselected(prodid)
'check if array Session("demo") exists
    'checkContinuedProductselected = false
	Conti_Products = Session("SelectedContinued")
	if isArray(Session("SelectedContinued")) then
		for i=0 to (UBound(Conti_Products)-1)
			
			if cint(prodid)=cint(Conti_Products(i)) then
				checkContinuedProductselected=true
				exit function
			else
				'checkContinuedProductselected=false
			end if
		next
	else
		checkContinuedProductselected=false
	end if
end function
'*************************************

'arguments (demoid)
'Returns Boolean
'*************************************
function checkDiscontinuedProductselected(prodid)
'check if array Session("demo") exists
	'checkDiscontinuedProductselected=false
	disc_Conti_Products = Session("SelectedDiscontinued")
	'Response.Write UBound(DiscProducts)
	'if UBound(DiscontinuedProducts)>0 then
	if isArray(Session("SelectedDiscontinued")) then 
		for i=0 to (UBound(disc_Conti_Products))
			if cint(prodid)=cint(disc_Conti_Products(i)) then
				checkDiscontinuedProductselected=true
				exit function
			else
				'checkDiscontinuedProductselected=false
			end if
		next
	else
		checkDiscontinuedProductselected=false
	end if
end function

'radio button
function checkRadioButton(buttonid)
	RadioSelect = Session("RadioSelection")
	
	if(len (RadioSelect) >0) then 
		if(RadioSelect = buttonid)then 
			checkRadioButton = true
			exit function
		else 
			checkRadioButton = false
		end if
	else 
		checkRadioButton = false
	end if 
end function

'***********************************************
'Receives the Category name based on CategoryID
'AC 1/30/00
'Modified by Shraddha Shah 6/18/2004
'arguments (categoryid)
'Returns String
'***********************************************
function GetCategoryNameonID(catid)
	Set rsCat = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT CATEGORY_NAME FROM PROPGROUPCATEGORY WHERE CATEGORY_ID = " & catid 
	rsCat.Open strSQL, dbconnstr()
		if not rsCat.EOF then
			GetCategoryNameonID = rsCat("CATEGORY_NAME")
		else
			GetCategoryNameonID = ""
		end if
	rsCat.Close
	Set rsCat = Nothing	
end function
'***********************************************
'Gets PArtnumber on ProductID
'Shraddha Shah 6/18/2004
'arguments (categoryid)
'Returns String
'***********************************************

function GetProductNameonID(prodid)
	Set rsCat = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT partnumber FROM  product WHERE productID = " & prodid 
	rsCat.Open strSQL, dbconnstr()
		if not rsCat.EOF then
			GetProductNameonID = rsCat("partnumber")
		else
			GetProductNameonID = ""
		end if
	rsCat.Close
	Set rsCat = Nothing	
end function

'******************************************
' Author:           Kate Kaplan
' Date:             March 18,2004
'Modified by Kate Kaplan 9/28/2004 - added PRESENT_ID Parameter
'Modified by Kate Kaplan 5/24/2006 - removed store procedure
' Insert requests with Serial Number and ScopeId(Not requered)
' Description:      Insert a new request with scope info
' Returns:          The Request_Id just inserted
'**********************************************
Function InsertRequestWithScopeInfo(lgContactId, lgRequestTypeId, lgObjectId, strSerialNum, strScopeID, strRemarks, strApplication,lgPageID, PresentID)
    if len(Session("CampaignID"))=0 then Session("CampaignID")=0
    strDate= NOW()

   set rsInsertScopeInfo=server.CreateObject("ADODB.Recordset")
   strSQL=	"	INSERT INTO REQUEST (CONTACT_ID, REQUEST_TYPE_ID, OBJECT_ID, SERIAL_NUM, SCOPEID, REMARKS, ENTERED_BY, DATE_ENTERED,PAGEID,CAMPAIGN_ID,PRESENT_ID) " &_
			"	VALUES(" & lgContactId & "," & lgRequestTypeId & "," & lgObjectId & ",'" & strSerialNum & "','" & strScopeID & "',N'" & strRemarks & "','webuser','" & strDate & "'," & lgPageID & "," & Session("Campaignid") & "," & PresentID & ")"
   'Response.Write strsql
   'Response.end
   rsInsertScopeInfo.Open strSQL, dbconnstr()
   
   
     ' Open command object
   ' Set cmd = Server.CreateObject("ADODB.Command")
    'Set paramIn1 = CreateObject("ADODB.Parameter")
    'Set paramIn2 = CreateObject("ADODB.Parameter")
    'Set paramIn3 = CreateObject("ADODB.Parameter")
    'Set paramIn4 = CreateObject("ADODB.Parameter")
	'Set paramIn5 = CreateObject("ADODB.Parameter")
	'Set paramIn6 = CreateObject("ADODB.Parameter")
	'Set paramIn7 = CreateObject("ADODB.Parameter")
	'Set paramIn8 = CreateObject("ADODB.Parameter")
	'Set paramIn9 = CreateObject("ADODB.Parameter")
	'Set paramIn10 = CreateObject("ADODB.Parameter")
    'Set paramOut = CreateObject("ADODB.Parameter")

    'cmd.ActiveConnection = dbconnstr()
    'cmd.CommandText = "{call ZP_INSERT_REQUEST_SCOPEINFO(?,?,?,?,?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    ' adInteger = 3
    ' adDouble  = 5  , adParamOutput =2
    'Set paramIn1 = cmd.CreateParameter("", 200, 1, 12)     'ContactId
    'Set paramIn2 = cmd.CreateParameter("", 200, 1, 5)      'RequestTypeId
    'Set paramIn3 = cmd.CreateParameter("", 200, 1, 6)      'ObjectID
    'Set paramIn4 = cmd.CreateParameter("", 200, 1, 50)	   'Serial_num
    'Set paramIn5 = cmd.CreateParameter("", 200, 1, 20)      'ScopeID
    'Set paramIn6 = cmd.CreateParameter("", 200, 1, 8000)   'Remarks
	'Set paramIn7 = cmd.CreateParameter("", 200, 1, 50)	   'Application
	'Set paramIn8 = cmd.CreateParameter("", 3, 1)		  'PageID
	'Set paramIn9 = cmd.CreateParameter("", 3, 1)		  'Campaign ID
	'Set paramIn10 = cmd.CreateParameter("", 3, 1)		  'Present ID
    'Set paramOut = cmd.CreateParameter("", 5, 2)     'Return value Request_Id
    ' Assign the values to the parameters
    'paramIn1.Value = lgContactId
    'paramIn2.Value = lgRequestTypeId
    'paramIn3.Value = lgObjectId
    'paramIn4.Value = strSerialNum
    'paramIn5.Value = strScopeID
    'paramIn6.Value = strRemarks
    'paramIn7.Value = strApplication
    'paramIn8.Value = 0'lgPageID
   
    'paramIn9.Value = Session("CampaignID")
    'paramIn10.Value = PresentID
    'cmd.Parameters.Append paramIn1
    'cmd.Parameters.Append paramIn2
    'cmd.Parameters.Append paramIn3
    'cmd.Parameters.Append paramIn4
	'cmd.Parameters.Append paramIn5
	'cmd.Parameters.Append paramIn6
	'cmd.Parameters.Append paramIn7
	'cmd.Parameters.Append paramIn8
	'cmd.Parameters.Append paramIn9
	'cmd.Parameters.Append paramIn10
    'cmd.Parameters.Append paramOut
    '****************************************
    
    ' Execute the command.
    'cmd.Execute
   	'Select Request_id which was created
	set rsREQUESTID=Server.CreateObject("ADODB.Recordset")
	strSQL="SELECT REQUEST_ID from REQUEST where CONTACT_ID=" & lgContactId & " and OBJECT_ID='" & lgObjectId & "' and DATE_ENTERED='" & strDate & "' and REQUEST_TYPE_ID=" & lgRequestTypeId
	rsREQUESTID.open strSQL, dbconnstr()
	if not rsREQUESTID.eof then
		InsertRequestWithScopeInfo=rsREQUESTID("REQUEST_ID")
	end if
    'InsertRequestWithScopeInfo =paramOut.Value
	'Set cmd = Nothing
End Function
'******************************************
' Author:           Kate Kaplan
' Date:             June 17,2004
' Check if Customer Came from Campaign
' Description:      Insert a new request with scope info
' Returns:			Boolean
'**********************************************
function CustomerFromCampaign(CampaignID)
	CustomerFromCampaign=false
	if len(CampaignID)>0 then
		if isnumeric(CampaignID) then
			if cint(CampaignID)=226  then
				CustomerFromCampaign=true
			end if
		end if
	end if
end function

'*************************************
'Select translation
'YK 7/27/01
'Arguments -tablename,columnname, id
'Returns string
'*************************************

function translation_webdb32(tablename,columnname,columnid,id)

if len(Session("localeid"))=0 then Session("localeid")=1033
localeid=Session("localeid")

if Session("localeid")=1031 or Session("localeid")=1040 or Session("localeid")=1036 then
	localeid=1033
end if
if cint(localeid)=1033 then
	set rsUsTranslation=CreateObject("ADODB.Recordset")
	strSQL="Select " & columnname & " as VALUE from " & tablename &  " where " & columnid & "=" & id
	Set cmdUsTranslation=server.CreateObject("ADODB.COmmand")
	cmdUsTranslation.CommandText=strSQL
	cmdUsTranslation.ActiveConnection=dbconnstr()
	cmdUsTranslation.CommandType=1'TEXT
	set rsUsTranslation=cmdUsTranslation.Execute
	'rsUsTranslation.Open strSQL, dbconnstr()
	if not rsUsTranslation.EOF then
		translation_webdb32=rsUsTranslation("VALUE")
	else
		translation_webdb32=""
	end if
	rsUsTranslation.Close
	set rsUsTranslation=nothing
	set cmdUsTranslation=nothing
else
	'select tableid from table "TABLE_ID" (ecatalog)
	set rsTable=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Table_id From Table_id where Table_name='" & tablename & "'"
	'Response.Write strSQL
	rsTable.Open strSQL, dbconnstr()
	if not rsTable.EOF then
		TableID=rsTable("Table_ID")
	end if
	'Response.write TableID & "<br>"
	'close recordset rsTable
	rsTable.Close
	set rsTable=nothing

	'select columnid from table"COLUMN_ID" (ecatalog)
	set rsColumn=server.CreateObject("ADODB.Recordset")
	strSQL="SELECT Column_id From Column_id where Column_name='" & columnname & "'"
	rsColumn.Open strSQL, dbconnstr()
	if not rsColumn.EOF then
		ColID=rsColumn("Column_ID")
	end if
	'Response.Write ColumnID & "<br>"
	'Response.Write ID
	'close recordset  rsColumn
	 rsColumn.Close
	set  rsColumn=nothing

	'Select value from table "TABLESBYLANGUAGE" (ecatalog)
	'for TableID,COlumnID,ID and LOCALEID
	set rsTranslation=server.CreateObject("ADODB.Recordset")
	strSQL=	"Select VALUE from TABLESBYLANGUAGE where TABLE_ID=" & TableID & _
			" and COLUMN_ID=" & ColID & " and IDINTABLE=" & ID  & _
			" and LOCALEID=" & localeid
			
			'Response.Write strSQL
	rsTranslation.Open strSQL, dbconnstr()
	if not rsTranslation.EOF then
		translation_webdb32=rsTranslation("Value")
	else
		'translation="No Translation exists"
			set rsUsTranslation1033=server.CreateObject("ADODB.Recordset")
			strSQL="Select " & columnname & " as VALUE from " & tablename &  " where " & columnid & "=" & id
			'Response.Write strSQL
			rsUsTranslation1033.Open strSQL, dbconnstr()
			if not rsUsTranslation1033.EOF then
				translation_webdb32=trim(rsUsTranslation1033("VALUE"))
			else
				translation_webdb32=""
			end if
	end if
	rsTranslation.Close
	set rsTranslation=nothing
end if
end function
'******************************************
' Author:           Kate Kaplan
' Date:             March 1,2005
' Insert SW Download Request
' Description:      Insert a new SW Download request with product info and date purchased
' Returns:          The Request_Id just inserted
'**********************************************
Function InsertSWDownloadRequest(lgContactId, lgRequestTypeId, lgObjectId, strSerialNum, strScopeID, strRemarks, strApplication,lgPageID, PresentID,DatePurchased)
   ' Open command object
    Set cmdSW = Server.CreateObject("ADODB.Command")
    Set paramIn1 = CreateObject("ADODB.Parameter")
    Set paramIn2 = CreateObject("ADODB.Parameter")
    Set paramIn3 = CreateObject("ADODB.Parameter")
    Set paramIn4 = CreateObject("ADODB.Parameter")
	Set paramIn5 = CreateObject("ADODB.Parameter")
	Set paramIn6 = CreateObject("ADODB.Parameter")
	Set paramIn7 = CreateObject("ADODB.Parameter")
	Set paramIn8 = CreateObject("ADODB.Parameter")
	Set paramIn9 = CreateObject("ADODB.Parameter")
	Set paramIn10 = CreateObject("ADODB.Parameter")
	Set paramIn11 = CreateObject("ADODB.Parameter")
    Set paramOut = CreateObject("ADODB.Parameter")

    cmdSW.ActiveConnection = dbconnstr()
    cmdSW.CommandText = "{call ZP_INSERT_SWDOWNLOAD_REQUEST(?,?,?,?,?,?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    ' adInteger = 3
    ' adDouble  = 5  , adParamOutput =2
    'Const adDate = 7
    Set paramIn1 = cmdSW.CreateParameter("", 200, 1, 12)     'ContactId
    Set paramIn2 = cmdSW.CreateParameter("", 200, 1, 5)      'RequestTypeId
    Set paramIn3 = cmdSW.CreateParameter("", 200, 1, 6)      'ObjectID
    Set paramIn4 = cmdSW.CreateParameter("", 200, 1, 50)	   'Serial_num
    Set paramIn5 = cmdSW.CreateParameter("", 200, 1, 20)      'ScopeID
    Set paramIn6 = cmdSW.CreateParameter("", 200, 1, 8000)   'Remarks
	Set paramIn7 = cmdSW.CreateParameter("", 200, 1, 50)	   'Application
	Set paramIn8 = cmdSW.CreateParameter("", 3, 1)		  'PageID
	Set paramIn9 = cmdSW.CreateParameter("", 3, 1)		  'Campaign ID
	Set paramIn10 = cmdSW.CreateParameter("", 3, 1)		  'Present ID
	Set paramIn11 = cmdSW.CreateParameter("",200,1,11)		  'DatePurchased
	Set paramOut = cmdSW.CreateParameter("", 5, 2)     'Return value Request_Id
    ' Assign the values to the parameters
    paramIn1.Value = lgContactId
    paramIn2.Value = lgRequestTypeId
    paramIn3.Value = lgObjectId
    paramIn4.Value = strSerialNum
    paramIn5.Value = strScopeID
    paramIn6.Value = strRemarks
    paramIn7.Value = strApplication
    paramIn8.Value = 0'lgPageID
    if len(Session("CampaignID"))=0 then Session("CampaignID")=0
    paramIn9.Value = Session("CampaignID")
    paramIn10.Value = PresentID
    paramIn11.Value = DatePurchased

    cmdSW.Parameters.Append paramIn1
    cmdSW.Parameters.Append paramIn2
    cmdSW.Parameters.Append paramIn3
    cmdSW.Parameters.Append paramIn4
	cmdSW.Parameters.Append paramIn5
	cmdSW.Parameters.Append paramIn6
	cmdSW.Parameters.Append paramIn7
	cmdSW.Parameters.Append paramIn8
	cmdSW.Parameters.Append paramIn9
	cmdSW.Parameters.Append paramIn10
	cmdSW.Parameters.Append paramIn11
    cmdSW.Parameters.Append paramOut
    '****************************************
    
    ' Execute the command.
    cmdSW.Execute
    ' Assign the request id
    InsertSWDownloadRequest = paramOut.Value
	Set cmd = Nothing
End Function

'ADded by SS for cATC /PSG products	 
function GetPSGCategoryNameonID(catid)
	Set rsCat = Server.CreateObject("ADODB.Recordset")
	strSQL =  "select subcat_name as category_name from PROPGROUP_SUBCATEGORY where subcat_id=" & catid
	rsCat.Open strSQL, dbconnstr()
	'response.Write rsCat("CATEGORY_NAME")
	'response.End 
	if not rsCat.EOF then
		GetPSGCategoryNameonID = rsCat("CATEGORY_NAME")
	else
		GetPSGCategoryNameonID = ""
	end if
	rsCat.Close
	Set rsCat = Nothing	
end function	
'Added by KK - 4/5/2005
'set PSG Profile transfer time
function SetTransferTime (EMail)
if len(email)>0 then
	set rsSetTransferTime=Server.CreateObject("ADODB.Recordset")
	strSQL="Update CONTACT_PSG set DATE_TRANSFERED='" & now()& "'where email='" & EMail & "'"
	
	rsSetTransferTime.Open strSQL, dbconnstr()
end if
end function

'*************************************
'Check if SEto call models has been selected
'SS 5/17/05
'arguments (secallid)
'Returns Boolean
'*************************************
function checkSetocallselected(secallid)
'check if array Session("literature") exists
	secall=Session("SEtocall")

	if UBound(secall)>0 then
		for i=0 to (UBound(secall)-1)
			if cint(secallid)=cint(secall(i)) then
				checkSetocallselected=true
				exit function
			else
				checkSetocallselected=false
			end if
		next
	else
		checkSetocallselected=false
	end if
end function
'Added by KK - 1/15/2006
'set Korean Profile transfer time
function SetTransferTimekr (EMail)
if len(email)>0 then
	set rsSetTransferTimekr=Server.CreateObject("ADODB.Recordset")
	strSQL="Update CONTACT_Kr set DATE_TRANSFERED='" & now()& "'where email='" & EMail & "'"
	
	rsSetTransferTimekr.Open strSQL, dbconnstr()
end if
end function


'*************************************
'Get customer's info in HTML
'KK 1/24/2006
'arguments -none
'Returns string
'*************************************
Function CustomerInfoHTML()
	strInfo=""
	strCRet = "<br>"
	if cint(Session("localeid"))<>1041 then	
		' Client data
		if len(Session("FirstName")) >0 and  len(Session("LastName"))>0 then
			
			strInfo = strCRet & strCRet& strInfo & "Client: " & Session("LastName")& " " & LoadI18N("SLBCA0160",Session("localeid"))& " " &  Session("Firstname") & strCRet
		end if
		if len(Session("Title"))>0 then
			strInfo = strInfo & "Title: " & Session("Title")& strCRet
		end if
		if len(Session("Company"))>0 then
			strInfo = strInfo & "Company: " & Session("Company")& strCRet
		end if
		if len(Session("Department"))>0 then
			strInfo = strInfo & "Department: " & Session("Department")& strCRet
		end if
		if len(Session("Address"))>0 then
		strInfo = strInfo & "Address: " & Session("Address")& " " & Session("Address2") & strCRet
		end if
		if len(Session("City"))>0 then
			strInfo = strInfo & "City: " & Session("City")& strCRet
		end if
		if len(Session("State"))>0 then
			strInfo = strInfo & "State: " & Session("State")& strCRet
		end if
		if len(Session("Zip"))>0 then
			strInfo = strInfo & "Zip: " & Session("Zip")& strCRet
		end if
		if len(Session("Country"))>0 then
			strInfo = strInfo & "Country: " & Session("Country")& strCRet
		end if
		if len(Session("Phone"))>0 then
			strInfo = strInfo & "Phone: " & Session("Phone")& strCRet
		end if
		if len(Session("Fax"))>0 then
			strInfo = strInfo & "Fax: " & Session("Fax")& strCRet
		end if
		if len(Session("Email"))>0 then
			strInfo = strInfo & "Email: " & Session("Email")& strCRet
		end if
		if len(Session("URL"))>0 then
			strInfo = strInfo & "URL: " & Session("URL")& strCRet
		end if
		if len(Session("Applications"))>0 then
			strInfo = strInfo & "Application: " & Session("Applications")& strCRet
		end if
	else
	' Client data for Japan
	    if len(Session("Country"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0083",Session("localeid")) & ": " & Session("Country")& strCret
		end if
		if len(Session("Zip"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0082",Session("localeid")) & ": " & Session("Zip")& strCret
		end if
		if len(Session("City"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0080",Session("localeid")) & ": " & Session("City")& strCret
		end if
		if len(Session("Address"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0079",Session("localeid")) & ": " & Session("Address")& strCret
		end if
		if len(Session("Address2"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0079",Session("localeid")) & "2: " & Session("Address2")& strCret
		end if
		if len(Session("Company"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0078",Session("localeid")) & ": " & Session("Company")& strCret
		end if
		if len(Session("Title"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0077",Session("localeid")) & ": " & Session("Title")& strCret
		end if
		if len(Session("FirstName"))>0 and  len(Session("LastName"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0076",Session("localeid")) & ": "  &  Session("LastName")& " " & Session("Firstname") & " " & LoadI18N("SLBCA0160",Session("localeid"))& strCret
		end if
		if len(Session("Phone"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0084",Session("localeid")) & ": " & Session("Phone")& strCret
		end if
		if len(Session("Fax"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0085",Session("localeid")) & ": " & Session("Fax")& strCret
		end if
		if len(Session("Email"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0086",Session("localeid")) & ": " & Session("Email")& strCret
		end if
		if len(Session("URL"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0087",Session("localeid")) & ": " & Session("URL")& strCret
		end if
		if len(Session("Applications"))>0 then
			strInfo = strInfo & LoadI18N("SLBCA0088",Session("localeid")) & ": " & Session("Applications")& strCret
		end if
	end if
	' Insert empty line 
	strInfo = strInfo & strCRet
	'Response.Write strIntro
	' Return data 
	CustomerInfoHTML = strInfo
	
End Function

'******************************************
' Author:           Kate Kaplan
' Date:             September 26,2006
' Insert Download Request with Model Series and ProductID
' Returns:          The Request_Id just inserted
'**********************************************
Function InsertDownloadByProductRequest(lgContactId, lgRequestTypeId, lgObjectId, lgModelSeries,lgProductID,strSerialNum, strScopeID, strRemarks, strApplication,lgPageID, PresentID,DatePurchased)
  
   ' Open command object
    Set cmdDownloadByProduct = Server.CreateObject("ADODB.Command")
    Set paramIn1 = CreateObject("ADODB.Parameter")
    Set paramIn2 = CreateObject("ADODB.Parameter")
    Set paramIn3 = CreateObject("ADODB.Parameter")
    Set paramIn4 = CreateObject("ADODB.Parameter")
	Set paramIn5 = CreateObject("ADODB.Parameter")
	Set paramIn6 = CreateObject("ADODB.Parameter")
	Set paramIn7 = CreateObject("ADODB.Parameter")
	Set paramIn8 = CreateObject("ADODB.Parameter")
	Set paramIn9 = CreateObject("ADODB.Parameter")
	Set paramIn10 = CreateObject("ADODB.Parameter")
	Set paramIn11 = CreateObject("ADODB.Parameter")
	Set paramIn12 = CreateObject("ADODB.Parameter")
	Set paramIn13 = CreateObject("ADODB.Parameter")
    Set paramOut = CreateObject("ADODB.Parameter")
    cmdDownloadByProduct.ActiveConnection = dbconnstr()
    cmdDownloadByProduct.CommandText = "{call ZP_InsertDownloadByProductRequest(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
    ' adChar =129 adParamInput = 1
    ' adVarChar = 200
    ' adInteger = 3
    ' adDouble  = 5  , adParamOutput =2
    'Const adDate = 7
    Set paramIn1 = cmdDownloadByProduct.CreateParameter("", 3,1)			'ContactId
    Set paramIn2 = cmdDownloadByProduct.CreateParameter("", 3,1)			'RequestTypeId
    Set paramIn3 = cmdDownloadByProduct.CreateParameter("", 3,1)			'ObjectID
    Set paramIn4 = cmdDownloadByProduct.CreateParameter("", 3,1)			'Model_Series
    Set paramIn5 = cmdDownloadByProduct.CreateParameter("", 3,1)			'ProductID
    Set paramIn6 = cmdDownloadByProduct.CreateParameter("", 200, 1, 50)	'Serial_num
    Set paramIn7 = cmdDownloadByProduct.CreateParameter("", 200, 1, 20)    'ScopeID
    Set paramIn8 = cmdDownloadByProduct.CreateParameter("", 200, 1, 8000)  'Remarks
	Set paramIn9 = cmdDownloadByProduct.CreateParameter("", 200, 1, 50)	'Application
	Set paramIn10 = cmdDownloadByProduct.CreateParameter("", 3, 1)			'PageID
	Set paramIn11 = cmdDownloadByProduct.CreateParameter("", 3, 1)			'Campaign ID
	Set paramIn12 = cmdDownloadByProduct.CreateParameter("", 3, 1)			'Present ID
	Set paramIn13 = cmdDownloadByProduct.CreateParameter("",200,1,11)		'DatePurchased
	Set paramOut = cmdDownloadByProduct.CreateParameter("", 5, 2)			'Return value Request_Id
    ' Assign the values to the parameters
    paramIn1.Value = lgContactId
    paramIn2.Value = lgRequestTypeId
    paramIn3.Value = lgObjectId
    paramIn4.Value = lgModelSeries
    paramIn5.Value = lgProductID
    paramIn6.Value = strSerialNum
    paramIn7.Value = strScopeID
    paramIn8.Value = strRemarks
    paramIn9.Value = strApplication
    paramIn10.Value = 0'lgPageID
    if len(Session("CampaignID"))=0 then Session("CampaignID")=0
    paramIn11.Value = Session("CampaignID")
    paramIn12.Value = PresentID
    paramIn13.Value = DatePurchased

    cmdDownloadByProduct.Parameters.Append paramIn1
    cmdDownloadByProduct.Parameters.Append paramIn2
    cmdDownloadByProduct.Parameters.Append paramIn3
    cmdDownloadByProduct.Parameters.Append paramIn4
	cmdDownloadByProduct.Parameters.Append paramIn5
	cmdDownloadByProduct.Parameters.Append paramIn6
	cmdDownloadByProduct.Parameters.Append paramIn7
	cmdDownloadByProduct.Parameters.Append paramIn8
	cmdDownloadByProduct.Parameters.Append paramIn9
	cmdDownloadByProduct.Parameters.Append paramIn10
	cmdDownloadByProduct.Parameters.Append paramIn11
	cmdDownloadByProduct.Parameters.Append paramIn12
	cmdDownloadByProduct.Parameters.Append paramIn13
    cmdDownloadByProduct.Parameters.Append paramOut
    '****************************************
    
    ' Execute the command.
    cmdDownloadByProduct.Execute
    ' Assign the request id
    InsertDownloadByProductRequest = paramOut.Value
	Set cmdDownloadByProduct = Nothing
End Function
'*********LeCroy Direct Campaigns (10/22/2007)
function GetCRMFMasterID(CampaignID, SUB_ID)
	if len(CampaignID)>0 and len(SUB_ID)>0 then
		set rsGetMasterID=server.CreateObject("ADODB.REcordset")
		strSQL="Select * from SUBSCRIPTION_ARCHIVE where campaign_id=" & CampaignID & " and SUB_ID='" & SUB_ID& "'"
		'Response.Write strSQL
		'Response.end
		rsGetMasterID.Open strSQL,dbconnstr()
		if not rsGetMasterID.EOF then
			GetCRMFMasterID=rsGetMasterID("MASTER_ID")
		end if
	end if
end function

function GetPhoneByMasterID(MASTER_ID)
	if len(MASTER_ID)>0 then
		set rsGetPhoneByMasterID=server.CreateObject("ADODB.REcordset")
		strSQL="Select PHONE from CONTACTS where MASTER_ID=" & MASTER_ID 
		'Response.Write strSQL
		'Response.end
		rsGetPhoneByMasterID.Open strSQL,dbconnstr("LeCroyDW")
		if not rsGetPhoneByMasterID.EOF then
			GetPhoneByMasterID=rsGetPhoneByMasterID("PHONE")
		end if
	end if
end function

function GetCRMFEmail(CampaignID, SUB_ID)
	if len(CampaignID)>0 and len(SUB_ID)>0 then
		set rsGetCRMFEmail=server.CreateObject("ADODB.REcordset")
		strSQL="Select * from SUBSCRIPTION_ARCHIVE where campaign_id=" & CampaignID & " and SUB_ID='" & SUB_ID& "'"
		rsGetCRMFEmail.Open strSQL,dbconnstr()
		if not rsGetCRMFEmail.EOF then
			GetCRMFEmail=rsGetCRMFEmail("EMAIL")
		end if
	end if
end function

Function EmailBodyCRMFClientDataHTML(MasterID)
	' Author:          Kate Kaplan
    ' Date:            Oct.22, 2007
    ' Description:     Returns an HTML string with Client Data
    ' Revisions:       0
    if len(MasterID)>0 then
	strCRet = "<br>"
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
	set rsCRMFContactInfo=server.CreateObject("ADODB.REcordset")
	strSQL="Select * from CONTACTS where MASTER_ID=" & MasterID
	rsCRMFContactInfo.Open strSQL,dbconnstr("LeCroyDW")
	' Client data
		if len(rsCRMFContactInfo("First_Name"))>0 and  len(rsCRMFContactInfo("Last_Name"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0076",localeid) & ": " & rsCRMFContactInfo("Honorific") & " " & rsCRMFContactInfo("First_Name") & " " & rsCRMFContactInfo("Last_Name") & strCRet
		end if
		if len(rsCRMFContactInfo("Title"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0077",localeid) & ": " & rsCRMFContactInfo("Title")& strCRet
		end if
		if len(rsCRMFContactInfo("Company"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0078",localeid) & ": " & rsCRMFContactInfo("Company")& strCRet
		end if
		if len(rsCRMFContactInfo("ADDRESS1"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0079",localeid) & ": " & rsCRMFContactInfo("ADDRESS1")& " " & rsCRMFContactInfo("Address2") & strCRet
		end if
		if len(rsCRMFContactInfo("CITY1"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0080",localeid) & ": " & rsCRMFContactInfo("CITY1")& strCRet
		end if
		if len(rsCRMFContactInfo("STATE_PROVINCE"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0081",localeid) & ": " & rsCRMFContactInfo("STATE_PROVINCE")& strCRet
		end if
		if len(rsCRMFContactInfo("POSTAL_CODE"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0082",localeid) & ": " & rsCRMFContactInfo("POSTAL_CODE")& strCRet
		end if
		if len(rsCRMFContactInfo("Country"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0083",localeid) & ": " & rsCRMFContactInfo("Country")& strCRet
		end if
		if len(rsCRMFContactInfo("Phone"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0084",localeid) & ": " & rsCRMFContactInfo("Phone")& strCRet
		end if
		if len(rsCRMFContactInfo("Fax"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0085",localeid) & ": " & rsCRMFContactInfo("Fax")& strCRet
		end if
		if len(rsCRMFContactInfo("Email"))>0 then
			EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & LoadI18N("SLBCA0086",localeid) & ": mailto:" & rsCRMFContactInfo("Email")& strCRet
		end if
		' Insert empty line 
		EmailBodyCRMFClientDataHTML = EmailBodyCRMFClientDataHTML & strCRet
	end if
End Function
%>




