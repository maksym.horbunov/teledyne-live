<%@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp"-->
<!--#INCLUDE FILE="allfunctions.asp" -->
<% 'here is the name of the file itself
If Not ValidateUser Then
	Response.redirect "SentToReg.asp"
End If
validateusernoredir()
%>

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="E-profile" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	country_id=208
	division_id=1
	topic_id=1
'***************Select Language******************
'**************Added by KK 7/27/2004*************
	Session("LanguagePage")="/cgi-bin/profile/modform.asp"
	'check language has been changed by user
	strLang=Request.QueryString("localeid")
	if len(strLang)>0 then
		Session("localeid")=cint(strLang)
	end if
'************************************************
'**************Added by YK 8/3/2001	*************
if len(Session("localeid"))=0 then Session("localeid")=1033
if Session("localeid")=1041 then 
	Response.Redirect "/cgi-bin/profile/modformj.asp"
end if	
localeid=Session("localeid")
Response.Redirect "TransferSession.asp"	
'************************************************

'************Added by AC 10/27/2008**************
'check if user came from a .net session and put redirection url into session
if len(Request.QueryString("redir")) > 0 then
    Session("RedirectTo") = Request.QueryString("redir")
end if

%>
<%
'check if user didn't have phone and enter it 
if len(Request.QueryString("newphone"))>0 then 
	Session("Phone")= Request.QueryString("newphone")
end if
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->
<title><% = PageTitle %></title>
</head>
<script LANGUAGE="JavaScript">
function validate()
//this function redirect user to the next page if all fields of
//registration form are correct
{
    var msg;
    msg ="";
    var flg=false;

	if(replace(document.modform.FirstName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0161",localeid)%>" + "\n";
      flg=true;
      document.modform.FirstName.focus();
     }
    if(replace(document.modform.LastName.value," ","") == "")
     {
      msg += "<%=LoadI18N("SLBCA0162",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.modform.LastName.focus();
	  }
     }
     if(document.modform.Job_function.value == "")
     {
      msg += "<%=LoadI18N("SLBCA0163",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.modform.Job_function.focus();
	  }
     }
     if (replace(document.modform.Company.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0078",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.modform.Company.focus();
	  }
    }
    if(replace(document.modform.Address.value," ","") == "")
    {
      msg += "<%=LoadI18N("SLBCA0079",localeid)%>" + "\n";
      if (flg!=true)
      {
        flg=true;
		document.modform.Address.focus();
	  }	
    }
    if(replace(document.modform.City.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0080",localeid)%>" + "\n";
     if (flg!=true)
       {
        flg=true;
		document.modform.City.focus();
	   }	
    }
  	
  	if(document.modform.Country.value == "Select Country")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.modform.Country.focus();
		}	
	}

    if (document.modform.Country.value =="United States")
    {
		if (document.modform.State.value == "") 
		{
		msg += "<%=LoadI18N("SLBCA0081",localeid)%>" + "\n";
		 if (flg!=true)
		 {
		  flg=true;
		  document.modform.State.focus();
		 }
		}
	}
	if(replace(document.modform.Zip.value," ","")== "")
	{
	 msg += "<%=LoadI18N("SLBCA0082",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.modform.Zip.focus();
		}	
	}
	if(document.modform.Country.value == "")
	{
	 msg += "<%=LoadI18N("SLBCA0083",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.modform.Country.focus();
		}	
	}
	if(replace(document.modform.Phone.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0084",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.modform.Phone.focus();
		}	
   }

    if(replace(document.modform.Email.value," ","") == "")
    {
     msg += "<%=LoadI18N("SLBCA0086",localeid)%>" + "\n";
     if (flg!=true)
		{
		  flg=true;
		  document.modform.Email.focus();
		}	
   }

    if(msg != "")
    {
      alert("<%=LoadI18N("SLBCA0172",localeid)%>" + ":"+  "\n" + "\n" + msg);
      return;
    }
    document.modform.submit();
}

function changecountry()
{
    if((document.modform.sessioncountry.value)!=(document.modform.Country.options[document.modform.Country.selectedIndex].value))
    {
    var countryname=document.modform.Country.options[document.modform.Country.selectedIndex].value;
 
    document.location.href="changecountry.asp?country="+countryname;
    }
}
    function openTermWin(URL)
	{
		aWindow = window.open(URL,"thewindow","width=575,height=400 bgcolor=white,scrollbars=yes,resizable=yes");
	}
function replace(string,text,by)
	 {
	// Replaces text with by in string
	    var strLength = string.length;
	    var txtLength = text.length;
	    if ((strLength == 0) || (txtLength == 0)) return string;

	    var i = string.indexOf(text);
	    if ((!i)&&(text != string.substring(0,txtLength))) return string;
	    if (i == -1) return string;

	    var newstr = string.substring(0,i) + by;

	    if (i+txtLength < strLength)
	        newstr +=replace(string.substring(i+txtLength,strLength),text,by);

	    return newstr;
	}
</script>

<body LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<a NAME="TopofPage"></a>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0" border=0 ID="Table1">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" ID="Table2">
			<tr>
			  <td width="100" valign="top" rowspan="2" >
				<!--#include virtual="/shopper/selectlanguage.asp"-->
			  </td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td width="90%" valign="top">
			<%'Whats New Table 
				Response.Write "<a name=""focus"">&nbsp;</a>"
				Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" align=""figth"" height=""38"">"
				if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042  then
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
				else
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",localeID) & """ >"
				end if
                %>
	                        <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="1">
                            
                            <table border="0" width="400" cellspacing="0" cellpadding="0" ID="Table3">
  <tr>
    <td width="500"><font face="Verdana"><small><strong><% = Session("FirstName") %>, <%=LoadI18N("SLBCA0184",localeID)%>.<br></strong></small><br>  
		<hr size="1" color="RoyalBlue">
      </font>
		</td>
		</tr>
		</table>

		<table border="0" cellspacing="0" cellpadding="3" width="100%" ID="Table4">
		<tr>
		<td>


		<table border="0" cellspacing="0" cellpadding="0" width="100%" ID="Table5">
        </font>
		<tr>
		<td colspan=2>
			<br>
			<small><font color="red"><%=LoadI18N("SLBCA0159",localeid)%></font></small><br></font></font>
		</td>
	</tr>
	  <tr>
    <td>&nbsp;</td>
    <td width="100%"><font face="Verdana,Arial,Helvetica" size="1"><form method="POST" action="modification.asp" id="modform" name="modform">
      <input type="hidden" name="sessioncountry" id="sessioncountry" value="<%=Session("Country")%> ">
      <table border="0" cellspacing="0" cellpadding="3" width="500" ID="Table6">
	
        <tr>
          <td width="195"><small><font face="Verdana" color="#000000"><nobr><strong><%=LoadI18N("SLBCA0160",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="Honorific" value="<% = Session("Honorific")%>" size="5" ID="Text1"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0161",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="FirstName" value="<% = Session("FirstName") %>" size="30" maxlength="30" ID="Text2"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0162",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="LastName" size="30" value="<% = Session("LastName") %>" maxlength="30" ID="Text3"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana"><nobr><strong><%=LoadI18N("SLBCA0077",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="Title" size="30" maxlength="30" value="<% = Session("Title") %>" ID="Text4"></td>
        </tr>
					<%'Job Function Field - added by KK 3/11/2004%>
					<tr>
						<td width="195"><small><font face="Verdana" color="red"><strong><nobr><%=LoadI18N("SLBCA0163",localeid)%>:</strong></nobr></font></small>
						</td>
						<td width="1935"><font face="Verdana">
						<select name="Job_function" ID="Select1">
							<%If isnull(Session("JobFunction")) or len(Session("JobFunction"))=0  Then %>   
								<option VALUE=''><%=LoadI18N("SLBCA0573",localeid)%>
                                <%end if
							set rsJobFunction=server.CreateObject("ADODB.Recordset")
							strSQL="Select * from CONTACT_JOB_FUNCTION order by SORT_ID"
							rsJobFunction.Open strSQL, dbconnstr()
							if not rsJobFunction.EOF then
								while not rsJobFunction.EOF
								If not isnull(Session("JobFunction")) or len(Session("JobFunction"))>0 Then 
									if cstr(Session("JobFunction"))=cstr(translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))) then
										Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """ SELECTED>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									else
										Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
									end if
								else
									Response.Write "<option VALUE=""" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID")) & """>" & translation_webdb32("CONTACT_JOB_FUNCTION","NAME","JOB_FUNCTION_ID",rsJobFunction("JOB_FUNCTION_ID"))
								end if
								
								rsJobFunction.MoveNext
								wend
							end if
							 %>		 
	</select>
    </font>
	</td>
	</tr>
		<tr>
          <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0078",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="Company" size="30" maxlength="50" value="<% = Session("Company") %>" ID="Text5"></td>
        </tr>
		<tr>	
			<td width="195"><small><font face="Verdana" color="#000000"><strong><%=LoadI18N("SLBCA0465",localeid)%>:</strong></font></small>
			</td>
			<td width="1935"><input type="text" name="Department" size="30" maxlength="100" value="<% = Session("Department")%>" ID="Text6">
			</td>
		</tr>  
		 <tr>
		<td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0079",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="Address" size="30" maxlength="255" value="<% =  Session("Address") %>" ID="Text7"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana"><nobr><strong><%=LoadI18N("SLBCA0079",localeid)%>2:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="Address2" size="30" maxlength="255" value="<% = Session("Address2") %>" ID="Text8"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0080",localeid)%>:</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="City" size="30" maxlength="50" value="<% = Session("City") %>" ID="Text9"></td>
        </tr>
        
          <%
          if Session("Country")="United States" then%>
          <tr>
		  <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0112",localeid)%>&nbsp;<%=LoadI18N("SLBCA0081",localeid)%> (<%=LoadI18N("SLBCA0185",localeid)%>):</strong></nobr></font></small></td>   

          <td width="1935"><select name="State" maxlength="50" value="<% = State %>" size="1" ID="Select2">
			<%  'Insert the states
			If Session("Country") = "United States" and Session("State")="" Then
				Response.Write "<option VALUE=""" & Session("State") & """ SELECTED>Select State"
			End If

			GetStates(rs) 
			rs.MoveFirst 
			Do While Not rs.BOF And Not rs.EOF 
			If rs.Fields(0).value = Session("State") then 
            	Response.Write "<option VALUE=""" & rs.Fields(0).Value & """ SELECTED>" & rs.Fields(0).Value
			Else
            	Response.Write "<option VALUE=""" & rs.Fields(0).Value & """>" & rs.Fields(0).Value
			End If
			rs.MoveNext
			Loop
			rs.Close
		%>
		
		</select> </td>
        </tr>
        <%else
			Response.Write "<tr>"
		    Response.Write "<td width=""195""><small><font face=""Verdana"" color=""red""><nobr><strong>" & LoadI18N("SLBCA0081",localeid) & "(" & LoadI18N("SLBCA0164",localeid)& "):</strong></nobr></font></small></td>"
	        Response.write "<td width=""1935""><input type=""Text"" name=""State"" size=""30"" maxlength=""50""></td>"
			Response.Write "</tr>" 
        end if%>
        <tr>
          <td width="195"><small><font face="Verdana" color="#000000"><nobr><strong><%=LoadI18N("SLBCA0165",localeid)%> (<%=LoadI18N("SLBCA0166",localeid)%>):</strong></nobr></font></small></td>
          <td width="1935"><input type="text" name="OtherState" size="30" maxlength="50" value="<%= Session("OtherState")%>" ID="Text10"> <% 
			'If Session("Country") <> "United States" Then
				'Response.Write "value=""" & Session("State") & """>"
			'Else
			'	Response.Write "value=""" & Session("OtherState") & """>"
			'End If
		  %> 
		  </td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><strong><%=LoadI18N("SLBCA0082",localeid)%>:</strong></font></small></td>
          <td width="1935"><input type="text" name="Zip" size="30" maxlength="30" value="<% = Session("Zip") %>" ID="Text11"></td>
        </tr>
        <tr>
		  <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0112",localeid)%>&nbsp;<%=LoadI18N("SLBCA0083",localeid)%> (<%=LoadI18N("SLBCA0185",localeid)%>):</strong></nobr></font></small></td>   
          <td width="1935"><select name="Country" id="Country" maxlength="50" value="<% = Country %>" size="1" onchange="changecountry()">
		<% 'Insert the countries%>
			<option VALUE="<% = Session("Country") %>" SELECTED><% = Session("Country") %>
		<%if not Session("Country")="United States" then%>
			<option VALUE="United States">United States
		<%end if%>
		<% GetCountries(rs) 
			
				rs.MoveFirst 
				Do While Not rs.BOF And Not rs.EOF %> 
				<%If rs.Fields(0).value = Country then %>
				<option VALUE="<% = rs.Fields(0).Value%>" SELECTED>
				<% = rs.Fields(0).Value%><%Else%>
				<option VALUE="<% = rs.Fields(0).Value%>"><% = rs.Fields(0).Value%><%End If%>
				<% rs.MoveNext
				Loop
				rs.Close %>
				
				</select></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><nobr><strong><%=LoadI18N("SLBCA0084",localeid)%>:</strong></nobr></font></small></td>
          <td width="195"><input type="text" name="Phone" size="30" maxlength="30" value="<% = Session("Phone") %>" ID="Text12"></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana"><nobr><strong><%=LoadI18N("SLBCA0085",localeid)%>:</strong></nobr></font></small></td>
          <td width="195"><font face="Verdana"><input type="text" name="Fax" size="20" maxlength="30" value="<% = Session("Fax") %>" ID="Text13"></font></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana" color="red"><strong><nobr><%=LoadI18N("SLBCA0086",localeid)%>:</strong></nobr></font></small></td>
          <td width="195"><font face="Verdana"><input type="text" name="Email" size="30" maxlength="50" value="<% = Session("Email") %>" ID="Text14"></font></td>
        </tr>
        <tr>
          <td width="195"><small><font face="Verdana"><nobr><strong><%=LoadI18N("SLBCA0087",localeid)%>/<%=LoadI18N("SLBCA0168",localeid)%>:</strong></font></small></td>
          <td width="195"><font face="Verdana"><input type="text" name="URL" size="20" maxlength="50" value="<% = Session("URL") %>" ID="Text15"></font></td>
        </tr>
		<tr>
          <td width="195"><small><font face="Verdana"><strong><%=LoadI18N("SLBCA0088",localeid)%>:</strong></font></small></td>
          <td width="195"><font face="Verdana"><input type="text" name="Applications" size="20" maxlength="50" value="<% = Session("Applications") %>" ID="Text16"></font></td>
        </tr>
		<%
			'*****YK 5/3/2002
			'if user exist in member table allow him to change
			'his signature
			if len(Session("MemberID"))>0 then
				Response.Write "<tr>"
			    Response.Write "<td width=""195""  valign=""top"">"
				Response.Write "<small><font face=""Verdana,Arial"" size=2 color=""#000000""><nobr><strong>" & "Signature" & "</strong></nobr></font></small>"
				Response.Write "</td>"
			    Response.Write "<td width=""195""  valign=""top"">"
				Response.Write "<textarea rows=3 cols=30 name=sign id=sign>" & Session("signature") & "</textarea>"
				Response.Write "</td>"
				Response.Write "</tr>"
			end if
		%>	
		  <%
		 'Check if we have record in subscription table for this user

		Session("TempEmail")=Session("Email")
		
				
		'select all enabled subscription type 
		'and show checked checkboxes
		set rsSubType=server.CreateObject("ADODB.Recordset")
		if session("localeID")=1042 then
			strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y' or DOCUMENT_ID=37"
		else
			strSQL="select * from SUBSCRIPTION_TYPE where ENABLE='y'"
		end if
		rsSubType.Open strSQL,dbconnstr()
		if not rsSubType.EOF then
			Response.Write "<tr>"
			Response.Write "<td  colspan=2 align=""left"" valign=""top""><br>"
			Response.Write "<hr size=""1""	 color=""#4169E1"" width=""80%"">"
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
		    Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
			Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0435",localeid) & "</b></font>"
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
			Response.Write "<td width=""195"" colspan=2 valign=""top""  align=""left"">"
			Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
					
			while not rsSubType.EOF
				subcount=subcount+1
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" width=""30"">"
	            if checksubscriptionexist(rsSubType("DOCUMENT_ID")) then
			'check what value we have in DISABLE column

					if checksubscriptiondisable(rsSubType("DOCUMENT_ID")) then
						strChecked=" checked "
					else
						strChecked=""
					end if
				else
					strChecked=""
				end if

				'Response.Write  rsSubType("DOCUMENT_ID")
				Response.Write "<input type=checkbox name='subscription" & rsSubType("DOCUMENT_ID") & "' id='subscription" & rsSubType("DOCUMENT_ID") & "' " & strChecked & ">"
				Response.Write "</td>"
				Response.Write "<td valign=""middle"">"
				'Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubType("NAME") &  "</b></nobr></font>"
				Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  translation_webdb32("SUBSCRIPTION_TYPE","NAME","DOCUMENT_ID",rsSubType("DOCUMENT_ID"))  & "</b></nobr></font>"
				
				Response.Write "</td>"
				Response.Write "</tr>"
				redim preserve SubTypeID(subcount)				
				SubTypeID(subcount-1)  = rsSubType("DOCUMENT_ID")
									
				rsSubType.MoveNext
			wend
								
			Response.Write "</table>"
			Response.Write "</td>"
			Response.Write "</tr>"
			'ask customer in which format he prefer to recive an email
			'Removed by KK on 06/17/2009 - defaulted format to HTML

			'set rsSubFormat=server.CreateObject("ADODB.Recordset")
			'strSQL="select * from SUBSCRIPTION_FORMAT where ENABLE='y'"
			'rsSubFormat.Open strSQL,dbconnstr()
			'if not rsSubFormat.EOF then
			'	Response.Write "<tr>"
			'	Response.Write "<td  colspan=2 align=""left"" valign=""top"">"
			'	Response.Write "<br><font face=""Verdana,Arial"" size=2 color=""#4169E1""><b>" & LoadI18N("SLBCA0434",localeid) & "</b></font>"
			'	Response.Write "</td>"
			'	Response.Write "</tr>"
			'	Response.Write "<tr>"
			'	Response.Write "<td width=""200"" colspan=2 valign=""top""  align=""left"">"
			'	Response.Write "<table border=""0"" cellpadding=""0"" cellspacing=""0"">"
			'	while not rsSubFormat.EOF
			'		if cint(getsubscriptionformat(Session("email")))=cint(rsSubFormat("FORMAT_ID")) then
			'			strSelected="CHECKED"
			'		else
			'			strSelected=""
			'		end if
			'		Response.Write "<tr>"
			'		Response.Write "<td valign=""top"" width=""30"">"
			'		Response.Write "<input type=radio name='subformat' id='subformat' value=""" & rsSubFormat("FORMAT_ID")  & """ " & " " & strSelected & ">"
			'		Response.Write "</td>"
			'		Response.Write "<td valign=""middle"">"
			'		Response.Write "<font face=""Verdana,Arial"" size=2><nobr><b>" &  rsSubFormat("NAME") &  "</b></nobr></font>"
			'		Response.Write "</td>"
			'		Response.Write "</tr>"
			'		rsSubFormat.MoveNext
			'	wend
			'			
			'	Response.Write "</table>"
			'	Response.Write "</td>"
			'	Response.Write "</tr>"
			'end if	
		end if
						
		Session("SelectedSubCount")	=subcount		
		Session("SubCount")=subcount
		Session("SubTypeID") = SubTypeID						
						
					
		 
		  %>
		  <!--<input type="checkbox" name="subscription" <%=strChecked%>>
		  </td>
		  <td width="200"  valign="top"><font face="Verdana" size=1><nobr><b>Yes, I would like to receive other information about LeCroy's products and services via email</b></nobr></font>
		  </td>
		</tr>-->
      </table>
           <table ID="Table7">
        <tr>
          <td><small><font face="Verdana"><input type="button" value="<%=LoadI18N("SLBCA0170",localeid)%>" name="B1" onclick="validate()" ID="Button1"> </font></small></td>
		<!--<td><input type="Reset" value="<%=LoadI18N("SLBCA0186",localeid)%>" name="B2"> </td>-->
      </table>
    </form></font>
    </td>
  </tr>
</table>

  		</td>
		</tr>
		</table>
		</td>
		</tr>
</table>
 </td>
	</tr>
		<tr>
			<td >&nbsp;</td>
			<td>&nbsp;</td>
			<td><nobr><img border="0" name="holder" src="/nav/images/spacer.gif" width="300" height="25" align="absbottom"></nobr></td>
		</tr>
   </table>
</td>
</tr>
<tr>
<td>
<!--#include virtual="/images/Footer/footer.asp" -->
</td>
</tr>
</table>
</body>
</html>

