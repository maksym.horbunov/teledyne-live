<%'@ Language=VBScript %>
<!--#include virtual="/include/global.asp" -->
<!--#include virtual="/include/dbconnstr.asp" -->
<!--#include virtual="/cgi-bin/profile/allfunctions.asp" -->
<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Software & Hardware Options for LeCroy Digital Oscilloscopes" ' Title of Web Page
	PageDescription="LeCroy produces high quality, high performance Digital Oscilloscopes & Accessories. You'll find all the scopes and accessories right at your finger tips, and plenty of technical documentation to help you get started using a LeCroy scope." 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes, Scopes, Signal Sources, Probes, Accessories, Diff Amps, Differential, Measurements, Power, Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page

'**************Added by YK 8/3/2001	*************
if len(Session("localeid"))=0 then Session("localeid")=1033
localeid=Session("localeid")

'************************************************

%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<!--#include virtual="/include/meta_info.asp" -->


<title><% = PageTitle %></title>
</head>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<a NAME="TopofPage"></a>
<%
	'call UpdateSubscription(checksubscriptionexist,Session("Subscription"))
	Session("TempEmail")=""
%>
<!--#include virtual="/include/Header.asp" -->
<table width="100%" cellpadding="0" cellspacing="0" border=0>
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
			  
<%if Session("localeid")=1041 then%>
<td width="100" valign="top" rowspan="2" >
<!--#include virtual="/japan/menus/navi/pro.asp" --> 
</td>
<%else%>
<td rowspan="2">&nbsp;</td>
<%end if%>
			  <td valign="top">&nbsp;</td>
			  <td valign="top">&nbsp;</td>
			  <td width="10" valign="top">&nbsp;</td>
			  <td align="right" valign="top">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td width="90%" valign="top">
				<%
				if len(Session("Country"))>0 then
					if cstr(Session("Country"))="Japan" then
						Session("localeid")=1041
						'***************************************************
						'send email about user registration to contact.jp@teledynelecroy.com
						'***************************************************
						strCRet=chr(13)&chr(10)
						strBody = "Modified web registration:"  & strCRet & strCRet &  CustomerInfo
						'strBody = strBody & "Click the link below to view detail:"& strCRet& strCRet
						'strBody = strBody & ">> http://ny-wwwtest/ja.asp?creg=" & Session("ContactID")
							set objMessage =Server.CreateObject("CDO.Message")
							set objConfig = Server.CreateObject("CDO.Configuration")
							' Set the message properties and send the message.
								With objMessage
									Set .Configuration = objConfig
									.MimeFormatted = True
									.Fields.Update
									.To = "contact.jp@teledynelecroy.com"
									.BCC="kate.kaplan@teledynelecroy.com"
									.From = "webmaster@teledynelecroy.com"
									.Subject = "Modified web registration"
									.TextBody = strBody
									.textBodyPart.Charset = "shift-JIS"
									.Send
								End With

							Set objMessage = Nothing
							Set objConfig = Nothing							
					elseif mid(Session("Country"),1,5)="Korea"  then
					strCRet=chr(13)&chr(10)
					strBody = "Modified web registration"  & strCRet & strCRet &  CustomerInfo
						set objMessage =Server.CreateObject("CDO.Message")
						set objConfig = Server.CreateObject("CDO.Configuration")
						' Set the message properties and send the message.
							With objMessage
								Set .Configuration = objConfig
								.MimeFormatted = True
								.Fields.Update
								.To = "kate.kaplan@teledynelecroy.com"
								.BCC="kate.kaplan@teledynelecroy.com"
								.From = "webmaster@teledynelecroy.com"
								.Subject = "Modified web registration"
								.TextBody = strBody
								.textBodyPart.Charset = "korean"
								.Send
							End With

						Set objMessage = Nothing
						Set objConfig = Nothing		
					end if
				end if
				'Whats New Table 
				Response.Write "<table border=""0"" width=""480"" cellspacing=""0"" cellpadding=""0"" height=""108"">"
				Response.Write "<tr>"
				Response.Write "<td valign=""top"" align=""left"" height=""38"">"
				if Session("localeid")=1033 or Session("localeid")=1041 or Session("localeid")=1042  then
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header.gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
				else
					Response.Write "<img SRC=""/Nav/Images/Profile/UserRegistration_Header_" & Session("localeid") & ".gif"" alt=""" & LoadI18N("SLBCA0136",Session("localeid")) & """>"
				end if
                %>      
                        <p class="SmallBody" align="left"><font face="Verdana,Arial,Helvetica" size="1">
                		<table border="0" width="100%" cellspacing="0" cellpadding="0">
							  <tr>
							    <td>&nbsp;</td>
							    <td><img src='/images/clearpixel.gif' width='20' height='1'></td>
							    <td width="100%"><font face="Verdana,Arial" size=2>
							    <%if Session("localeid")=1041 then%>
									<% = Session("LastName")%>&nbsp;<% = LoadI18N("SLBCA0160",Session("localeid"))%>&nbsp;<%=LoadI18N("SLBCA0187",Session("localeid"))%><br>
							    <%else%>
									<% = Session("FirstName")%>,&nbsp;<%=LoadI18N("SLBCA0187",Session("localeid"))%>.<br>
							    <%end if%>
							    <%=LoadI18N("SLBCA0188",Session("localeid"))%></font><br>
							    </td>
							  </tr>
							  <tr>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td width="100%">&nbsp;</td>
							  </tr>
							  <tr>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
								<%If Len (Session("RedirectToFirst")) > 0  Then %>
									<td width="100%"><form action="<% = Session("RedirectToFirst")%>" method="post" id="form1" name="form1">
								<%ElseIf Len (Session("RedirectTo")) > 0  Then %>
							    <td width="100%"><form action="<% = Session("RedirectTo")%>" method="post" id="form2" name="form2">
							    <%ElseIf Request.QueryString("srcfrom")="Japan" Then %>
							    <td width="100%"><form action="/cgi-bin/requestinfoj.asp" method="post" id="form3" name="form3">
							    <%Else%>
								<td >
								<%if Session("localeid")=1041 then%>
									<form action="/japan/default.asp" method="post" id="Form5" name="form4">
								<%elseif Session("localeid")=1042 then%>
									<form action="/Korea/default.asp" method="post" id="Form6" name="form4">
								<%elseif Session("localeid")=1040 then%>
									<form action="/italy/default.asp" method="post" id="Form7" name="form4">
								<%elseif Session("localeid")=1036 then%>
									<form action="/france/default.asp" method="post" id="Form8" name="form4">
								<%elseif Session("localeid")=1031 then%>
									<form action="/germany/default.asp" method="post" id="Form9" name="form4">
								<% else%>
									<form action="/default.asp" method="post" id="form4" name="form4">
								<%end if%>
								<% End If%>
								<br>
								 <input type="submit" value="<%=LoadI18N("SLBCA0183",Session("localeid"))%>"> 
							    </form>
							    </td>
							  </tr>
							</table>
                           </font>
						</td>
					</tr>
					<tr>
						<td valign="top" height="70">&nbsp;</td>
					</tr>
				</table>
			  </td>
			  <td>&nbsp;</td>
			  <td valign="top" align="right">&nbsp;</td>
			</tr>

    </table>
    </td>
	</tr>
    </table>

<!--#include virtual="/images/footer/footer.asp" -->
</body>
</html>
