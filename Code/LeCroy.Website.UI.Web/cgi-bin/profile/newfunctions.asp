<%

Function GetSalesRepEmail()
    ' Author:           Brett Kuehner
    ' Date:             Sep. 27th, 2000
    ' Description:      Gets the sales reprezentative email based on the country
    ' Returns:          sales_rep_email
    ' Revisions:        0
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
   	set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "select V_SALES_REP_EMAIL.EMAIL from V_COUNTRY, V_SALES_REP_EMAIL "
	strSQL = strSQL + "where UPPER(V_COUNTRY.NAME) = '" & Session("Country") & "' and "
	strSQL = strSQL + "V_SALES_REP_EMAIL.SALES_REP_ID = V_COUNTRY.SALES_REP"
    rs.ActiveConnection = cnn
    rs.Open strSQL
    'Response.Write strSQL
	If Not rs.EOF Then
		GetSalesRepEmail = rs("EMAIL")
	Else
		GetSalesRepEmail = ""
	End If
	rs.Close
End Function

Function GetPSGSalesRepEmail()
    ' Author:           Kate Kaplan
    ' Date:             Feb 17 2005
    ' Description:      Gets the PSG sales reprezentative
    '					email based on the country (not for US or Canada)
    ' Returns:          sales_rep_email for PSG
    ' Revisions:        0
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
   	set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "select SALES_REP_PSG.EMAIL from  V_COUNTRY INNER JOIN "
    strSQL = strSQL + " SALES_REP_PSG ON V_COUNTRY.SALES_REP_PSG = SALES_REP_PSG.SALES_REP_ID"
	strSQL = strSQL + " where V_COUNTRY.NAME = '" & Session("Country") & "'"
	'response.Write strSQL
	'response.end
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetPSGSalesRepEmail = rs.fields("EMAIL").Value
	Else
		GetPSGSalesRepEmail = ""
	End If
	rs.Close
End Function

Sub SendEmailToSalesRep(strEmailBody, blDemoAskedInUSA)
	' Author:           Kate Kaplan
    ' Date:             Jan 24, 2006
    ' Description:      Sends email to the sales rep
	strSubject = "New request(s)"
	If Ucase(Trim(Session("Country")))<> Ucase("United States") Then
		' Get salesrep Email
		strTo =  GetSalesRepEmail()
		Session("SalesRepEmail") = strTo
		' Create the email
		
		If Session("SalesRepEmail") <> "0" Then
			If Ucase(Trim(Session("Country")))= Ucase("JAPAN") Then
					set objMessage =Server.CreateObject("CDO.Message")
					set objConfig = Server.CreateObject("CDO.Configuration")
					' Set the message properties and send the message.
						With objMessage
							Set .Configuration = objConfig
							.MimeFormatted = True
							.Fields.Update
							.To = "contact.jp@teledynelecroy.com"
							.Cc="ai.yamamoto@teledynelecroy.com"
							.BCC="kate.kaplan@teledynelecroy.com"
							.From = "webmaster@teledynelecroy.com"
							'if cint(rsGetType("REQUEST_TYPE_ID"))=8 then
							'	.Subject = "New Download"
							'else
								.Subject = "New request(s)"
							'end if
							.TextBody = strEmailBody
							.textBodyPart.Charset = "shift-JIS"
							.Send
						End With

					Set objMessage = Nothing
					Set objConfig = Nothing		
			else
				strTo =Session("SalesRepEmail")
				strBCC="kate.kaplan@teledynelecroy.com"
				Set oNewRequest = Server.CreateObject("CDO.Message")
				oNewRequest.To =strTo
				if len(strCC)>0 then
					'oNewRequest.CC =strCC
				end if
				oNewRequest.From = "webmaster@teledynelecroy.com"
				oNewRequest.BCC = strBCC
				oNewRequest.Subject = strSubject
				oNewRequest.htmlBody = strEmailBody
				if Session("localeid")=1042 or mid(Session("country"),1,5)="Korea" then
					oNewRequest.htmlBodyPart.Charset = "korean"
				end if
				oNewRequest.Send 
				Set oNewRequest= Nothing
			end if
		End If
	Else
		strSubject=strSubject & " - "
		if len(Session("litBody"))> 0 then
			strSubject=strSubject & " Lit "
		end if		
		if len(Session("DemoBody"))> 0 then
			strSubject=strSubject & " Demo "
		end if
		if len(Session("SECall"))> 0 then
			strSubject=strSubject & " Call "
		end if
		strSubject = strSubject & " - " & Session("Company") & " - " & Session("lastname")
		if len(Session("campaignid"))>0 then
			if len(GetOfferInfo(Session("CampaignID"),0))>0 then
				strSubject=strSubject & " - " & GetOfferInfo(Session("CampaignID"),0)
			end if
		end if
		strTo = "customercare@teledynelecroy.com,Hilary.Lustig@teledynelecroy.com,dataentry@teledynelecroy.com"
		strBCC="kate.kaplan@teledynelecroy.com"
		Set oNewRequest = Server.CreateObject("CDO.Message")
		oNewRequest.To =strTo
		if len(strCC)>0 then
			'oNewRequest.CC =strCC
		end if
		oNewRequest.From = "webmaster@teledynelecroy.com"
		oNewRequest.BCC = strBCC
		oNewRequest.Subject = strSubject 
		oNewRequest.TextBody = strEmailBody
		oNewRequest.Send 
		Set oNewRequest= Nothing
	End If

End Sub

Sub SendEmailToTechnicalHelp(EmailBody)
	' Author:           Kate Kaplan
    ' Date:             Jan. 16, 2003
    ' Modified			Jan. 18 2006
    ' Description:      Sends email to the Technical Help

	    if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
    'response.Write Session("localeid")
    
	if Session("localeid")<>1042 then
		if len(Session("Country"))>0 then
			If Ucase(Trim(Session("Country")))<> Ucase("UNITED STATES") Then
				' Get salesrep Email
				if Session("localeid")=1041 then
					'strTo ="contact.jp@teledynelecroy.com"
				else
					strTo =  GetTechRepEmail()
					strBCC = "kate.kaplan@teledynelecroy.com"
				end if
			else
				strTo = "techweb@teledynelecroy.com"
				strBCC = "kate.kaplan@teledynelecroy.com"
			end if
		else
			strTo = "techweb@teledynelecroy.com"
			strBCC = "kate.kaplan@teledynelecroy.com"
		end if
		strCRet=chr(13) & chr(10)
		strSubject = "LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
		strEBody = EmailBodyClientData
	
		
		if len (EmailBody)>0 and len (strEBody)>0 then 
			strBody1 =  EmailBody & strCRet & strEBody  
			Set oTechHelp1 = Server.CreateObject("CDO.Message")
			oTechHelp1.To =strTo
			oTechHelp1.From = "webmaster@teledynelecroy.com"
			if len(strCC)>0 then
				oTechHelp1.cc=strCC
			end if
			oTechHelp1.Bcc = strBCC
			oTechHelp1.Subject =strSubject
			oTechHelp1.TextBody = strBody1
			oTechHelp1.Send 
			Set oTechHelp1 = Nothing

		end if 
	else
		strTo =  GetTechRepEmail()
		'strCC= "kate.kaplan@teledynelecroy.com"
		strBCC = "kate.kaplan@teledynelecroy.com"
		strSubject = "LeCroy Technical Support Request - Web - " & Session("COUNTRY") & ", " & Session("Company") & ", " & Session("FirstName") & " " & Session("LastName")
		strEBody = EmailBodyClientDataHTML
		strCRet = "<br>"
		if len (EmailBody)>0 and len (strEBody)>0 then 
			strBody2 =  EmailBody & strCRet & strEBody  
			Set oTechHelp2 = Server.CreateObject("CDO.Message")
			oTechHelp2.To =strTo
			oTechHelp2.From = "webmaster@teledynelecroy.com"
			oTechHelp2.cc=strCC
			oTechHelp2.Bcc = strBCC
			oTechHelp2.Subject =strSubject
			oTechHelp2.HTMLBody = strBody2
			oTechHelp2.Send 
			Set oTechHelp2 = Nothing

		end if		
	end if
End Sub

'*****************************************************
' Author:           Nick Benes
' Date:             Jan. 05, 1999
' Description:      Returns a string with Client Data
' Revisions:        0
'***********************************************************
'Modified by Kate Kaplan 5/25/2001 
'Modified by Kate Kaplan 01/14/2004
'Modified by Kate Kaplan 01/05/2010
'***********************************************************

Function EmailBodyClientData()
    Dim strBody
	Dim strCRet	
	if len(emaillocaleID)=0 then emaillocaleID=1033
	strCRet = chr(13) & chr(10)
    if len(Session("contactid"))>0 then
        set rs=Server.CreateObject("ADODB.Recordset")
        strSQL="Select  * from CONTACT where CONTACT_ID=" & Session("contactid")
        'response.Write strSQL
        rs.Open strSQL, dbconnstr()
        if not rs.eof then

                if emaillocaleID<>1041 then	
            	
	                ' Client data
	                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": " & rs("Honorific") & " " & rs("First_Name") & " " & rs("Last_Name") & strCRet
	                if len((rs("Title")))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleid) & ": " & rs("Title")& strCRet
	                end if
	                if len(rs("Department"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0465",emaillocaleid) & ": " & rs("Department")& strCRet
	                end if
	                if len(Session("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleid) & ": " & rs("Company")& strCRet
	                end if
	                if len(rs("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleid) & ": " & rs("Address")& " " & rs("Address2") & strCRet
	                end if
	                if len(rs("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleid) & ": " & rs("City")& strCRet
	                end if
	                if len(rs("STATE_PROVINCE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0081",emaillocaleid) & ": " & rs("STATE_PROVINCE")& strCRet
	                end if
	                if len(rs("POSTALCODE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleid) & ": " & rs("POSTALCODE")& strCRet
	                end if
	                if len( rs("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleid) & ": " & rs("Country")& strCRet
	                end if
	                if len(rs("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleid) & ": " & rs("Phone")& strCRet
	                end if
	                if len(rs("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleid) & ": " & rs("Fax")& strCRet
	                end if
	                if len(rs("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleid) & ": mailto:" & rs("Email")& strCRet
	                end if
	                if len(rs("URL"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0087",emaillocaleid) & ": " & rs("URL")& strCRet
	                end if
	                if len(rs("APPLICATION"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0088",emaillocaleid) & ": " & rs("APPLICATION")& strCRet
	                end if

	                ' Added Technical Support Question 2-3-99 Adrian Cake
	                if len(Request.QueryString("Remarks4"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0089",emaillocaleid) & ": " & Request.QueryString("Remarks4")& strCRet
	                end if
                else
	                ' Client data for Japan
	                'response.Write "test" & len(rs("Country"))
                    if len(rs("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleID) & ": " & rs("Country")& strCRet
	                end if
	                	

	                if len(rs("POSTALCODE"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleID) & ": " & rs("POSTALCODE")& strCRet
	                end if
	                	

	                if len(rs("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleID) & ": " & rs("City")& strCRet
	                end if
	                	

	                if len(rs("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleID) & ": " & rs("Address")& strCRet
	                end if
	                	

	                if len(rs("Address2"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleID) & "2: " & rs("Address2")& strCRet
	                end if
	                	

	                if len(rs("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleID) & ": " & rs("Company")& strCRet
	                end if
	                	

	                if len(rs("Title"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleID) & ": " & rs("Title")& strCRet
	                end if
	                	

	                if len(rs("FIRST_NAME"))>0 and  len(rs("Last_Name"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": "  &  rs("Last_Name")& " " & rs("FIRST_NAME") & " " & LoadI18N("SLBCA0160",emaillocaleID)& strCRet
	                end if
	                	

	                if len(rs("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleID) & ": " & rs("Phone")& strCRet
	                end if
	                
	                if len(rs("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleID) & ": " & rs("Fax")& strCRet
	                end if
	                
	                if len(rs("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleID) & ": " & rs("Email")& strCRet
	                end if
	                
	                if len(rs("URL"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0087",emaillocaleID) & ": " & rs("URL")& strCRet
	                end if
	                
	                if len(rs("APPLICATION"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0088",emaillocaleID) & ": " & rs("APPLICATION")& strCRet
	                end if
	                
	                ' Added Technical Support Question
	                if len(Request.QueryString("Remarks4"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0089",emaillocaleID) & ": " & Request.QueryString("Remarks4")& strCRet
	                end if
	                
                end if
        else
        	                ' Client data
	                strBody = strBody & LoadI18N("SLBCA0076",emaillocaleID) & ": " & Session("Honorific") & " " & Session("FirstName") & " " & rSessions("LastName") & strCRet
	                if len((Session("Title")))>0 then
		                strBody = strBody & LoadI18N("SLBCA0077",emaillocaleid) & ": " & Session("Title")& strCRet
	                end if
	                if len(Session("Department"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0465",emaillocaleid) & ": " & Session("Department")& strCRet
	                end if
	                if len(Session("Company"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0078",emaillocaleid) & ": " & Session("Company")& strCRet
	                end if
	                if len(Session("Address"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0079",emaillocaleid) & ": " & Session("Address")& " " & Session("Address2") & strCRet
	                end if
	                if len(Session("City"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0080",emaillocaleid) & ": " & Session("City")& strCRet
	                end if
	                if len(Session("State"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0081",emaillocaleid) & ": " & Session("Stage")& strCRet
	                end if
	                if len(Session("Zip"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0082",emaillocaleid) & ": " & Session("Zip")& strCRet
	                end if
	                if len( Session("Country"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0083",emaillocaleid) & ": " & Session("Country")& strCRet
	                end if
	                if len(Session("Phone"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0084",emaillocaleid) & ": " & Session("Phone")& strCRet
	                end if
	                if len(Session("Fax"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0085",emaillocaleid) & ": " & Session("Fax")& strCRet
	                end if
	                if len(Session("Email"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0086",emaillocaleid) & ": mailto:" & Session("Email")& strCRet
	                end if
	                if len(Session("URL"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0087",emaillocaleid) & ": " & Session("URL")& strCRet
	                end if
	                if len(Session("APPLICATION"))>0 then
		                strBody = strBody & LoadI18N("SLBCA0088",emaillocaleid) & ": " & Session("APPLICATION")& strCRet
	                end if
	    end if
	end if
	' Insert empty line 
	strBody = strBody & strCRet
	' Return data 
	EmailBodyClientData = strBody
End Function
Function CreateEmailBody2(LitRequest, DemoRequest, QuoteRequest, HelpRequest, SalesCall)
	' Author:           Nick Benes
    ' Date:             Dec. 07, 1998
    ' Description:      Creates the email body to be send to the rep
    ' Revisions:        0

	Dim strBody
	Dim strCRet	
	Dim strObjectId
	Dim strLitId
	
	strCRet = chr(13) & chr(10)
	strBody = "New request(s)"
	strCRet = chr(13) & chr(10)
	if len(Session("Source"))>0 then
			strBody = strBody & " (from " & Session("Source") & ")"
	end if
		
	' Client data
	strBody = strBody & ":"  & strCRet & strCRet & EmailBodyClientData	

	
	' Requests data
	strBody = strBody & "Request(s) for: " & strCRet
	
	' Literature
	' Literature
	If	LitRequest = True Then
		strBody = strBody & strCRet &  "Literature:"  & strCRet & strCRet
		strBody = strBody & Session("litBody")& strCRet & strCRet
	End If 	
	
	' Demo on current models
	If	DemoRequest = True Then
		strBody = strBody & strCRet & "Demo for the model(s):" & strCRet& strCRet
		strBody = strBody &  Session("DemoBody") & strCRet & strCRet
	End If
	
	' Quotation on current models
	If	QuoteRequest = True Then
		strBody = strBody & strCRet & "Quotation for the model(s):" & strCRet
		strBody = strBody & ParseArray(Request.QueryString("Object3"), "CURRMODEL")
	End If
	 	
	' Help on all models
	If	HelpRequest = True Then
		strBody = strBody & strCRet & "Technical Help for the model(s):" & strCRet& strCRet
		strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
	End If 	
	
	' Sales Call
	If	SalesCall = True Then
		strBody = strBody & strCRet & "Sales Person Call"
	End If 	 	
	
	CreateEmailBody2 = strBody
End Function


Function CreateEmailBody(LitRequest, DemoRequest, QuoteRequest, HelpRequest, SalesCall)
	' Author:           Nick Benes
	' modified  by YK 9/14/2001(added labels for translations)
    ' Date:             Dec. 07, 1998
    ' Description:      Creates the email body to be send to the rep
    ' Revisions:        1-9/14/2001- Kate Kaplan-added labels for translations
	'					1-5/30/2003- Kate Kaplan-changes for Japan (Session("localeid")=1041
	Dim strBody
	Dim strCRet	
	Dim strObjectId
	Dim strLitId
	
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")

	strBody = "New request(s)"
	strCRet = chr(13) & chr(10)
	if len(Session("Source"))>0 then
			if len(Session("campaignid"))>0 then
				if cint(Session("CampaignID"))=7 then
					strBody = strBody & " (from DSO Family Mailer Campaign)"
				else
					strBody = strBody & " (from " & Session("Source") & ")"
				end if
			else
				strBody = strBody & " (from " & Session("Source") & ")"
			end if
	end if
	if localeid<>1041 then	
		' Client data
		strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientData	
		' Requests data
		strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
		
		' Literature
		If	LitRequest = True Then
			strBody = strBody & strCRet &  LoadI18N("SLBCA0339",localeid)  & strCRet
			strBody = strBody & Session("litBody") & strCRet & strCRet
		End If 	
		
		' Demo on current models
		If	DemoRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
			strBody = strBody & Session("DemoBody") & strCRet & strCRet
		End If

		 	
		' Help on all models
		If	HelpRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0341",localeid) & strCRet
			strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
		End If 	
		
		' Sales Call
		'SS new code start 4/7/05
		if len(Session("SEtocallBody")) > 0 and len(Session("SEnoModels")) = 0 then 
			strBody = strBody & strCRet & LoadI18N("SLBCA0632",localeid) & strCRet
			strBody = strBody & Session("SEtocallBody") & strCRet 
		else
			strBody = strBody & Session("SEtocallBody") & strCRet
		end if 
		if len(Session("SEcallques")) > 0 then 
			strBody=strBody & strCRet & LoadI18N("SLBCA0631",localeid) & strCRet & Session("SEcallques") & strCRet&  strCRet
		end if 
		'SS end of new code 4/7/05
	else
		' Client data
		strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientData	
		' Requests data
		strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
		
		' Literature
		If	LitRequest = True Then
			strBody = strBody & strCRet &  LoadI18N("SLBCA0339",localeid)  & strCRet
			strBody = strBody & Session("litBody") & strCRet & strCRet
		End If 	
		
		' Demo on current models
		If	DemoRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
			strBody = strBody & Session("DemoBody") & strCRet & strCRet
		End If

		 	
		' Help on all models
		If	HelpRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0341",localeid) & strCRet
			strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
		End If 	
		
		' Sales Call
		'SS new code start 4/7/05
		if len(Session("SEtocallBody")) > 0 and len(Session("SEnoModels")) = 0 then 
			strBody = strBody & strCRet & LoadI18N("SLBCA0632",localeid) & strCRet
			strBody = strBody & Session("SEtocallBody") & strCRet 
		else
			strBody = strBody & Session("SEtocallBody") & strCRet
		end if 
		if len(Session("SEcallques")) > 0 then 
			strBody=strBody & strCRet & LoadI18N("SLBCA0631",localeid) & strCRet & Session("SEcallques") & strCRet&  strCRet
		end if 
		'SS end of new code 4/7/05
		' Sales Call
		'If	SalesCall = True Then
		'	strBody = strBody & strCRet & LoadI18N("SLBCA0342",localeid) 
		'End If
	end if
	CreateEmailBody = strBody
End Function

Function CreateWaveRunnerEmailBody()
	' Author:           Nick Benes
    ' Date:             Jan. 04, 1998
    ' Description:      Creates the email body to be send to the rep
    ' Revisions:        0

	Dim strBody
	Dim strCRet	
	Dim strObjectId
	Dim strLitId
	
	strCRet = chr(13) & chr(10)
	strBody = "New request(s) from:" & strCRet & strCRet
		
	' Client data
	strBody = strBody & EmailBodyClientData
		
	' Requests data
	strBody = strBody & "Request(s) for: " & strCRet
	
	'1
	If	Len(Request.QueryString("Object1"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT342"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object2"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT342"  & strCRet
	End If
	'2
	If	Len(Request.QueryString("Object3"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT342L"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object4"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT342L"  & strCRet
	End If
	
	'3
	If	Len(Request.QueryString("Object5"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT344"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object6"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT344"  & strCRet
	End If
	'4
	If	Len(Request.QueryString("Object7"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT344L"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object8"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT344L"  & strCRet
	End If
	
	'5
	If	Len(Request.QueryString("Object9"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT224"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object10"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT224"  & strCRet
	End If
	
	'6
	If	Len(Request.QueryString("Object11"))> 0 Then
		strBody = strBody & strCRet &  "Demo for LT322"  & strCRet
	End If 	
	
	If	Len(Request.QueryString("Object12"))> 0 Then
		strBody = strBody & strCRet &  "Quote for LT322"  & strCRet
	End If
	CreateWaveRunnerEmailBody = strBody
End Function

Function CompleteWaveRunnerEmailBody()
	' Author:           Nick Benes
    ' Date:             Jan. 04, 1998
    ' Description:      Completes the email body with the probes
    ' Revisions:        0

		Dim strEmailBody
		Dim strProbeText
		Dim strCRet
		
		strCRet = chr(13) & chr(10)
		' First part - clent data and models asked
		strEmailBody = CreateWaveRunnerEmailBody
		strEmailBody = strEmailBody & strCRet & "and the following probes/accessories:" & strCRet
		
		For iLooper = 1 to 23 'max number of probes
		' If we have a request of that type
		strProbeText = Request.QueryString("Probe"& iLooper)
			If Len (strProbeText) > 0 Then
				strEmailBody = strEmailBody & chr(13) & chr(10) & strProbeText
			End If
		Next
		CompleteWaveRunnerEmailBody = strEmailBody
End Function		

Function ParseArray(strToParse, LitAllModelOrCurrModel)
	' Author:           Nick Benes
    ' Date:             Dec. 07, 1998
    ' Description:      Parses  an array if multiple selections
    ' Revisions:        0

	Dim strLocalId
	Dim strTemp
	Dim strCRet	
	Dim strLocalArray ' from which to parse
	Dim iSeparatorLoc
	Dim iLooper
	Dim iObjId
	Dim strObjName
	'strLocalId = ""
	'strTemp = ""
	'iSeparatorLoc = ""
	'iLooper =""
	'iObjId = ""
	'strObjName = ""
	 
	strCRet = chr(13) & chr(10)
	
	If LitAllModelOrCurrModel = "LIT" Then
		strLocalArray = Session("StoredLitIdAndName")
	ElseIf LitAllModelOrCurrModel = "ALLMODEL" Then
		strLocalArray = Session("StoredAllModelsIdAndName")
	ElseIf LitAllModelOrCurrModel = "CURRMODEL" Then
		strLocalArray = Session("StoredCurrModelsIdAndName")
	End if

	' If is multiple selection
	If Instr(strToParse, ",") <> 0 Then
		Do
			iSeparatorLoc = Instr(strToParse, ",")
			strLocalId = Trim(Left(strToParse,iSeparatorLoc - 1 ))
			For iLooper = 1 to Ubound(strLocalArray,2)
				If Trim(strLocalId) = Trim(strLocalArray(0,iLooper)) Then
					strTemp = strTemp & strLocalArray(1,iLooper) & ";" & strCRet
					Exit For
				End If	
			Next
			strToParse = Trim(Mid(strToParse, iSeparatorLoc + 1))
			If Instr(strToParse,",") = 0 Then 
				Exit Do
			End If	 
		Loop While (iSeparatorLoc > 0) or Len(iSeparatorLoc) > 0
		' Loop for the last item request
		For iLooper = 1 to Ubound(strLocalArray,2)
			If Trim(strToParse) = Trim(strLocalArray(0,iLooper)) Then
				strTemp = strTemp & "and"  & strCRet & strLocalArray(1,iLooper) & "." & strCRet
				Exit For
			End If
		Next	
	
	Else
		For iLooper = 1 to Ubound(strLocalArray,2)
			If  Trim(strToParse) = Trim(strLocalArray(0,iLooper)) Then
				strTemp = strLocalArray(1,iLooper) & "." & strCRet
				Exit For
			End If
		Next
		
	End if
	ParseArray = strTemp
End Function
Sub SendEmailtoSeminarRequest()'added YK 8/15/2001
	' Author:           
    ' Date:             Jan. 27, 1999
    ' Description:      Sends email to the Technical Help
    ' Revisions:        1 Do not send Adrian, or my emails to the guys
	
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
	blIsNotATest  = True
	strTo = session("Email")
	strSubject = LoadI18N("SLBCA0343",localeid)
	strBody = EmailBodyClientDataSeminar
	CR = chr(13) & chr(10)
	strCRet = chr(13) & chr(10)

	' Create the email with seminar confirmation
	Set oNewMail = Server.CreateObject("CDO.Message")
	oNewMail.To =strTo
	oNewMail.From = "webmaster@teledynelecroy.com"
	oNewMail.BCC = "kate.kaplan@teledynelecroy.com"
	oNewMail.Subject ="New strSubject"
	oNewMail.TextBody = strBody
	'oNewMail.Send 
	Set oNewMail= Nothing
		
End Sub

Function EmailBodyClientDataSeminar()'added from semenars/newfunctions YK 8/15/2001
	' Author:           Kate Kaplan
    ' Date:             April 15, 2001
    ' modified			May 9, 2005 - KK
    ' modified			July 27, 2005 - KK
    ' Description:      Returns a string about Seminar Registration
    ' Revisions:        0
    
    Dim strBody
	Dim strCRet	
	
	strCRet = chr(13) & chr(10)

	' Client data
	'strBody = strBody & Session("FirstName") & ", " & LoadI18N("SLBCA0328",localeid)& "." & strCRet & strCRet
	'strBody = strBody & LoadI18N("SLBCA0332",localeid) & strCRet 
	'strBody = strBody & LoadI18N("SLBCA0333",localeid)& " " & Request.QueryString("Remarks6") & " " & LoadI18N("SLBCA0334",localeid) & " " & replace(replace(Session("SeminarName"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)) &  "." & strCRet & strCRet
	'strBody = strBody & replace(replace(Request.QueryString("sem_desc"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)) & strCRet& strCRet
	'strBody = strBody & replace(replace(Request.QueryString("SeminarDate"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10))& strCRet
	'strBody = strBody & replace(replace(Request.QueryString("sem_time"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10))& strCRet 
	
	strBody = strBody & Session("FirstName") & ", " & strCRet & strCRet
	strBody = strBody & LoadI18N("SLBCA0621",localeid)& strCRet & strCRet
	strBody = strBody & replace(replace(Session("SeminarName"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)) &  "." & strCRet & strCRet
	strBody = strBody & LoadI18N("SLBCA0069",localeid) & ": " & replace(replace(replace(Request.QueryString("SeminarDate"),"<br />",chr(13)&chr(10)),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10))& strCRet 
	strBody = strBody & LoadI18N("SLBCA0314",localeid) & ": " & replace(replace(replace(replace(Request.QueryString("sem_location"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)),"<Br>", chr(13)&chr(10)),"<br />",chr(13)&chr(10)) & strCRet 
	strBody = strBody & LoadI18N("SLBCA0622",localeid)& strCRet & strCRet
	'strBody = strBody & replace(replace(Request.QueryString("sem_desc"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)) & strCRet& strCRet
	'strBody = strBody & replace(replace(Request.QueryString("SeminarDate"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10))& strCRet
	strBody = strBody & replace(replace(replace(Request.QueryString("sem_time"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)),"<br />",chr(13)&chr(10))& strCRet 

	'if  SeminarType>0 then
	'	strBody = strBody & strCRet
	'else
	'	strBody = strBody & replace(replace(replace(Request.QueryString("sem_location"),"<br>",chr(13)&chr(10)),"<BR>",chr(13)&chr(10)),"<Br>", chr(13)&chr(10)) & strCRet& strCRet  
	'	strBody = strBody & LoadI18N("SLBCA0331",localeid) & " : " & strCRet& strCRet  
		strBody = strBody & LoadI18N("SLBCA0623",localeid)  & strCRet& strCRet  
		strBody = strBody & "https://teledynelecroy.com/seminars/default.asp" & strCRet & strCRet
	'end if
	strBody = strBody & LoadI18N("SLBCA0329",localeid) & strCRet
	'if cint(request.QueryString("Object6"))=27 then
	'	strBody = strBody & "LeCroy Corporation, CATC and Apcon" & strCRet & strCRet
	'else
		strBody = strBody & LoadI18N("SLBCA0330",localeid) & strCRet & strCRet
	'end if
	' Insert empty line 
	strBody = strBody & strCRet
	' Return data 
	EmailBodyClientDataSeminar = strBody
	
End Function

Function EmailSEDataSeminar()'added from semenars/newfunctions YK 8/15/2001
' Author:           Kate Kaplan
    ' Date:            9/6/2001
    ' Description:      Returns a string 
    ' Revisions:        0
    
    Dim strBody
	Dim strCRet	
	
	strCRet = chr(13) & chr(10)
	
	' Client data
	strBody =  Session("FirstName") & " " & LoadI18N("SLBCA0335",localeid) & " " & Request.QueryString("Remarks6") & " " & LoadI18N("SLBCA0336",localeid)& " " & Session("SeminarName") & " (" & Request.QueryString("SeminarDate") & ")." & strCRet & strCRet

	' Insert empty line 
	strBody = strBody & strCRet & EmailBodyClientData
	' Return data 
	EmailSEDataSeminar = strBody
	
End Function

'*************************************
'Load label for current localeid
'YK 8/15/01
'Arguments - Label,LocaleID
'Returns string
'*************************************
Function LoadI18N(Label,LocaleID)
    if len(Label)>0 and LocaleID>0 then
		Set rsLabels=server.CreateObject("ADODB.Recordset")
		strSQL="Select TEXT from I18NTEXT where LOCALEID=" & LocaleID & " and LABEL='" & Label & "'"
		'Response.Write strSQL&"<br>"
		rsLabels.Open strSQL, dbconnstr()
		if not rsLabels.EOF then
			LoadI18N=trim(rsLabels("Text"))
		else  
			'LoadI18N=" Label " & Label & " "
			set rsLabelsUS=server.CreateObject("ADODB.Recordset")
			strSQL="Select TEXT from I18NTEXT where LOCALEID=1033 and LABEL='" & trim(Label) & "'"
			rsLabelsUS.Open strSQL, dbconnstr()
			'Response.Write strSQL&"<br>"
			if not rsLabelsUS.EOF then
				LoadI18N=trim(rsLabelsUS("Text"))
			else  
				LoadI18N=" Label " & Label & " "
			end if
		end if
	else
	  'Response.Write "Label doesn't exist"
	end if
	'rsLabel.Close
	'set rsLabel=nothing
End Function

'*************************************
'gets email of sales rep
'YK 6/11/02
'Arguments - none
'Returns string
'*************************************

function getsalesmanemail()
'Check if this is US customer
if Session("COUNTRY")="United States"  then
'If yes
'Select company name from contact
	set rsCompany=server.CreateObject("ADODB.Recordset")
	strSQL="Select CONTACT.COMPANY as Company,STATE.SHORT_NAME as State,CONTACT.EMAIL as EMAIL from CONTACT inner join STATE on CONTACT.STATE_PROVINCE=STATE.NAME where CONTACT.CONTACT_ID=" & Session("ContactID")
	'Response.write strSQL
	'Response.end

	rsCompany.Open strSQL, cnn

	if not rsCompany.EOF then
	'Customer EMail
		strCustomerEmail=trim(rsCompany("EMAIL"))
		strCompany=trim(rsCompany("Company"))
		SpacePosition=instr(1,strCompany," ")
		if not SpacePosition=0 then
			strCompanyName=mid(strCompany,1,SpacePosition-1)
		else
			strCompanyName=strCompany
		end if
	'If company name exist in CONTACT table 
	'We check if this company for customer's STATE_PROVINCE exist in tblCOMPANYLOOKUP 
		set rsExistCompany=server.CreateObject("ADODB.Recordset")
		strSQL="Select * from tblCOMPANYLOOKUP where STATE='"  & rsCompany("State") & "' and CompanyName like'" & strCompanyName & "%'"
		
		'Response.write strSQL
		'Response.end
		rsExistCompany.Open strSQL,cnn

		if not rsExistCompany.EOF then
			rsExistCompany.MoveFirst
			'second checking
			if len(rsExistCompany("CompanyName"))>len(strCompany) then
				strFirstWord=mid(rsExistCompany("CompanyName"),1,(instr(1,rsExistCompany("CompanyName")," ")-1))
			
				if cstr(strFirstWord)=cstr(strCompany) then
					strSENumber=trim(rsExistCompany("SENumber"))
				end if
			else
				strSENumber=trim(rsExistCompany("SENumber"))
			end if
		end if
	end if
	
	if len(strSENumber) =0 then
'Select ZIP for current customer from table CONTACT

		set rsZip=server.CreateObject("ADODB.Recordset")
		strSQL="Select POSTALCODE,EMAIL from CONTACT where CONTACT_ID=" & Session("ContactID")
		rsZip.Open strSQL, cnn
		if not rsZip.EOF then

			rsZip.MoveFirst
			strCustomerEmail=trim(rsZip("EMAIL"))
			strZip=trim(rsZip("POSTALCODE"))
			if len(strZip)>=5 then
'If Zip code have more then 5 digits we cut and use first five to find SENumber
				if len(strZip)>5 then 
					strZip=mid(strZip,1,5)
				end if

'Select SENumber for this ZIP CODE from table tblAllZips
				if not isnumeric(strZip) then
					'Response.Write "not numeric"
					Response.Redirect "error.asp"
				end if
				set rsSENumber=	server.CreateObject("ADODB.Recordset")
				strSQL="Select SENumber from tblAllZips where ZipCode=" & strZip
				rsSENumber.Open strSQL, cnn
		
				if not rsSENumber.EOF then
					rsSENumber.MoveFirst

					strSENumber=trim(rsSENumber("SENumber"))
				end if	
			
			'Close recordset rsSENumber
				rsSENumber.Close
				set rsSENumber=nothing
					end if
				end if	
			'Close recordset rsZip
				rsZip.Close
				set rsZip=nothing
			end if		
'Select SEName and SEEmail	from table tblSECSR with this SENumber	
				set rsSE=server.CreateObject("ADODB.Recordset")
				strSQL="Select SEName, SEEmail,SEPager from tblSECSR where SENumber=" & strSENumber
				
				rsSE.Open strSQL, cnn
				if not rsSE.EOF then
					rsSE.MoveFirst
					strSEName=trim(rsSE("SEName"))
					strSEEmail=trim(rsSE("SEEmail"))
				end if

	end if
	getsalesmanemail= strSEEmail
end function

Function CreateDemoEmailBody(DemoRequest,SalesCall)
	' Author:           Kate Kaplan
    ' Date:            6/12/2002
    ' Description:      Creates the email body to be send demo(s) request to the salesrep
    ' Revisions:        0

	Dim strBody
	Dim strCRet	
	Dim strObjectId
	Dim strLitId
	
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")

	strBody = "New request(s)"
	strCRet = chr(13) & chr(10)
	if len(Session("Source"))>0 then
			if len(Session("campaignid"))>0 then
				if cint(Session("CampaignID"))=7 then
					strBody = strBody & " (from DSO Family Mailer Campaign)"
				else
					'strBody = strBody & " (from " & Session("Source") & ")"
				end if
			else
				'strBody = strBody & " (from " & Session("Source") & ")"
			end if
	end if
		
	' Client data
	strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientData	
	' Demo on current models
	If	DemoRequest = True Then
		strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
		strBody = strBody & Session("DemoBody") & strCRet & strCRet
	End If
	' Sales Call
	If	SalesCall = True Then
		strBody = strBody & strCRet & LoadI18N("SLBCA0342",localeid) 
	End If 	 	
	if len(Session("campaignid"))>0 and len(strBody)>0 and DemoRequest then
		if cint(Session("CampaignID"))=7 then
			strBody = strBody & "Customer should recieve a FREE color-combo pen and stylus"
		end if
	end if
	
	CreateDemoEmailBody = strBody
End Function

sub SendDemoEmailToSalesman(emailbody)

	if len(getsalesmanemail)>0 and len(trim(emailbody))>0 then
		Set objMailSalesmen = Server.CreateObject("CDO.Message")
		objMailSalesmen.To ="kate.kaplan@teledynelecroy.com"'getsalesmanemail
		objMailSalesmen.From = "webmaster@teledynelecroy.com"
		objMailSalesmen.BCC = "kate.kaplan@teledynelecroy.com"
		objMailSalesmen.Subject ="Web request(s):"
		objMailSalesmen.TextBody = emailbody
		'objMailSalesmen.Send 
		Set objMailSalesmen= Nothing
	end if
end sub

sub SendEmailToCustomer(emailbody)
	' Author:			YK 
    ' Date:				6/13/2002
    'modified KK		1/24/2006
    ' Description:     Sends the emailto the customer
	if len(emailbody)>0 then
		Set oMail = Server.CreateObject("CDO.Message")
		oMail.To =Session("email")
		oMail.From = "webmaster@teledynelecroy.com"
		oMail.BCC = "kate.kaplan@teledynelecroy.com"
		oMail.Subject ="LeCroy Information Request Confirmation"
		oMail.TextBody = emailbody
		oMail.Send 
		Set oMail= Nothing		
	end if
end sub

Function CreateCustomerEmailBody(LitRequest, DemoRequest, QuoteRequest, HelpRequest, SalesCall)
	' Author:			YK 
    ' Date:				6/13/2002
    ' Modified :		SS - 10/8/04 for sending European customers confirmation email	
    ' Modified :		KK - 8/1/2005 - send WS demo request from US to the Distributor	

    ' Description:      Creates the email body to be send a confirmation
    '					email to the customer
if Session("Country")="United States" and (len(Session("DemoBody"))>0 or len(Session("litBody"))>0) then	
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
	strCRet = chr(13) & chr(10)
	strBody = "Dear " & ucase(mid(session("FirstName"),1,1)) & lcase(mid(session("FirstName"),2))& "," & strCRet & strCRet  
	
		
	' Client data
	strBody = strBody   & "Thank you for requesting information from LeCroy!" & strCRet & strCRet
	' Requests data
	'strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
	
	' Demo on current models
	If	DemoRequest  and len(Session("DemoBody"))>0 Then
		strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet & strCRet
		strBody = strBody & Session("DemoBody") 
		if len(Session("WSDemoBody"))>0 then
			strBody = strBody & Session("WSDemoBody") 
		
		end if
		strBody=strBody & strCRet & strCRet
	End If

	 ' Help on all models
	If	HelpRequest  Then
		strBody = strBody & strCRet & LoadI18N("SLBCA0341",localeid) & strCRet & strCRet
		strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
	End If 	
	
	If	SalesCall = True Then 
		if len(Session("SEtocallBody")) > 0 then 
			strBody = strBody & strCRet & "SE call Models:"& strCRet& strCRet
			strBody = strBody & Session("SEtocallBody") & strCRet & strCRet
		end if 
		if len(Session("SEcallques")) > 0 then 
			strBody=strBody & strCRet & "Your comments are:" & strCRet& strCRet  & Session("SEcallques") & strCRet&  strCRet
		end if 
	End If
	
	' Literature
	If	LitRequest  Then
		strBody = strBody & strCRet &  LoadI18N("SLBCA0339",localeid)  & strCRet & strCRet
		strBody = strBody & Session("litBody") & strCRet & strCRet
	End If 
	'Sales Call
	'If	SalesCall  Then
	'	strBody = strBody & strCRet & LoadI18N("SLBCA0342",localeid) 
	'End If  
	
	if LitRequest then
		' Write here for literature
		strBody=strBody & strCret & "-------------------------------------------" & strCret
		strBody = strBody & strCRet & strCRet & LoadI18N("SLBCA0274",localeid) 
		strBody = strBody & strCRet &  Session("FirstName") & " " &  Session("LastName")
		if len(Session("Company"))>0 then
			strBody = strBody & strCRet &  Session("Company") 
		end if
		if len(Session("Address"))>0 then
			strBody = strBody & strCRet &  Session("Address") 
		end if
		'Response.Write "<font size=""2"">" 
		If Len(Session("Address2")) > 0 then 
			strBody = strBody & strCRet &  Session("Address2") 
		end if
		if len(Session("City"))>0 then
			strBody = strBody & strCRet &  Session("City")
		end if
		if len(Session("State"))>0 then
			strBody = strBody & strCRet &  Session("State") 
		end if
		if len(Session("Zip"))>0 then
			strBody = strBody & strCRet &  Session("Zip") 
		end if
		if len(Session("Country"))>0 then
			strBody = strBody & strCRet &  Session("Country")& strCRet  & strCRet
		end if
	end if
		
	If	SalesCall or DemoRequest and len(Session("Phone"))>0 and len(Session("DemoBody"))>0 Then

		strBody = strBody & strCRet & LoadI18N("SLBCA0276",localeid)
		strBody = strBody & strCRet & Session("Phone") 
		
	End if
else

	'Added by SS 10/8/04 for sending European customers confirmation email	
	if len(Session("Country") ) > 0 then
		if(GetRegionOnCountry(Session("Country"))) = 2 then
			'if all 3 requested
			if LitRequest and DemoRequest and SalesCall then
				strBody = "Thank you for your interest in LeCroy products. The requested product literature will be send to you shortly and a sales engineer in your region will contact you to arrange a demo."
			end if
			'only 2
			if LitRequest and DemoRequest and not SalesCall then
				strBody = "Thank you for your interest in LeCroy products. The requested product literature will be send to you and a sales engineer in your region will contact you shortly to arrange a demo."
			end if
			if LitRequest and  SalesCall  and not DemoRequest then
				strBody = "Thank you for your interest in LeCroy products. The requested product literature will be send to you and a sales engineer in your region will contact you shortly."
			end if
			if  SalesCall  and  DemoRequest  and not LitRequest then
				strBody = "Thank you for your interest in LeCroy products. You will be contacted shortly by a sales engineer in your region to arrange a demo."
			end if
			
			if DemoRequest and  not SalesCall  and not LitRequest then
				strBody = "Thank you for your interest in LeCroy products. You will be contacted shortly by your local LeCroy representative to arrange a demo."
			end if
			if SalesCall  and not DemoRequest   and not LitRequest then
				strBody = "Thank you for your interest in LeCroy products. You will be contacted shortly by your local LeCroy SE."
			end if
			if  LitRequest and not DemoRequest and  not SalesCall  then
				strBody = "Thank you for your interest in LeCroy products. The requested product literature will be send to you shortly by your local LeCroy representative."
			end if
		end if
	end if 
	'end of new code SS 10/8/04
end if
	CreateCustomerEmailBody = strBody
End Function

Function GetTechRepEmail()
    ' Author:           Kate Kaplan
    ' Date:             June 10 2003
    ' Description:      Gets the email to send technical help request
    ' Returns:          tech_rep_email
    ' Revisions:        0
    If Not OpenConnection Then
        MsgBox "Cannot connect to the database" & vbCrLf & _
                "Please try later"
        Exit Function
    End If
   	set rs=server.CreateObject("ADODB.Recordset")
    rs.CursorType = 0   'adOpenForwardOnly
    rs.LockType = 1 'adLockReadOnly
    strSQL = "select V_TECH_REP_EMAIL.EMAIL from V_COUNTRY, V_TECH_REP_EMAIL "
	strSQL = strSQL + "where UPPER(V_COUNTRY.NAME) = '" & Session("Country") & "' and "
	strSQL = strSQL + "V_TECH_REP_EMAIL.TECH_REP_ID = V_COUNTRY.TECH_REP_ID"
	'Response.Write strSQL
	
    rs.ActiveConnection = cnn
    rs.Open strSQL
	If Not rs.EOF Then
		GetTechRepEmail = rs("EMAIL")
	Else
		GetTechRepEmail = GetSalesRepEmail
	End If
End Function
function GetPSGSalesEmail()
    ' Author:           Kate Kaplan
    ' Date:             Feb 17 2005
    ' Description:      Gets the email to send  request to PSG sales rep
    ' Returns:          Sales_rep_email
    ' Revisions:        0

if len(session("Country"))>0 then
	if Session("COUNTRY")<>"United States" and Session("COUNTRY")<>"Canada" then
		GetPSGSalesEmail=GetPSGSalesRepEmail
	else
		if Session("COUNTRY")="Canada" then
			strSQL=	" SELECT SALES_REP_PSG.EMAIL FROM SALES_REP_PSG"&_
					" INNER JOIN  SALES_PSG ON SALES_REP_PSG.SALES_REP_ID = SALES_PSG.SALES_REP_PSG"&_
					" WHERE SALES_PSG.SUBCAT_ID = " & GetDownloadSubCategory(Session("PSGdownloadID")) &_
					" AND SALES_PSG.COUNTRY_ID = 35"
		else
			strSQL=	" SELECT SALES_REP_PSG.EMAIL FROM SALES_REP_PSG"&_
					" INNER JOIN  SALES_PSG ON SALES_REP_PSG.SALES_REP_ID = SALES_PSG.SALES_REP_PSG"&_
					" INNER JOIN  STATE ON SALES_PSG.STATE = STATE.SHORT_NAME"&_
					" WHERE SALES_PSG.SUBCAT_ID = " & GetDownloadSubCategory(Session("PSGdownloadID")) &_
					" AND SALES_PSG.COUNTRY_ID = 208 and STATE.NAME='" & Session ("STATE") & "'"
		end if
		'response.Write strSQL
		'Response.end
			set rsEmail=server.CreateObject("ADODB.Recordset")
			rsEmail.Open strSQL, dbconnstr()
			if not rsEmail.EOF then
				GetPSGSalesEmail=rsEmail("EMAIL")
			end if
	end if
end if

end function

Function CreateWSDemoEmailBody()
    ' Author:           Kate Kaplan
    ' Date:             8/1/2005
	
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")

	strBody = "New request(s)"
	strCRet="<br>"


	' Client data
	strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientDataHTML	
	' Requests data
	strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
	
	
	' Demo on current models
	If	DemoRequest = True Then
		strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
		strBody = strBody & Session("WSDemoBody") & strCRet & strCRet
	End If

	CreateWSDemoEmailBody= strBody
End Function

Function EmailBodyClientDataHTML()
	' Author:          Kate Kaplan
    ' Date:             Jan. 18, 2006
    ' Description:      Returns an HTML string with Client Data
    ' Revisions:        0
	strCRet = "<br>"
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
	
	if localeid<>1041 then	
	' Client data
		if len(Session("FirstName"))>0 and  len(Session("LastName"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0076",localeid) & ": " & Session("Honorific") & " " & Session("FirstName") & " " & Session("LastName") & strCRet
		end if
		if len(Session("Title"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0077",localeid) & ": " & Session("Title")& strCRet
		end if
		if len(Session("Company"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0078",localeid) & ": " & Session("Company")& strCRet
		end if
		if len(Session("Address"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0079",localeid) & ": " & Session("Address")& " " & Session("Address2") & strCRet
		end if
		if len(Session("City"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0080",localeid) & ": " & Session("City")& strCRet
		end if
		if len(Session("State"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0081",localeid) & ": " & Session("State")& strCRet
		end if
		if len(Session("Zip"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0082",localeid) & ": " & Session("Zip")& strCRet
		end if
		if len(Session("Country"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0083",localeid) & ": " & Session("Country")& strCRet
		end if
		if len(Session("Phone"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0084",localeid) & ": " & Session("Phone")& strCRet
		end if
		if len(Session("Fax"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0085",localeid) & ": " & Session("Fax")& strCRet
		end if
		if len(Session("Email"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0086",localeid) & ": mailto:" & Session("Email")& strCRet
		end if
		if len(Session("URL"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0087",localeid) & ": " & Session("URL")& strCRet
		end if
		if len(Session("Applications"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0088",localeid) & ": " & Session("Applications")& strCRet
		end if
		' Added Technical Support Question
		if len(Request.QueryString("Remarks4"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0089",localeid) & ": " & Request.QueryString("Remarks4")& strCRet
		end if
		if len(Request.QueryString("comments"))> 0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0346",localeid) & ": " & Request.QueryString("comments")& strCRet
		end if
		' Insert empty line 
		EmailBodyClientDataHTML = EmailBodyClientDataHTML & strCRet
	else
	' Client data for Japan
	    if len(Session("Country"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0083",localeid) & ": " & Session("Country")& strCRet
		end if
		if len(Session("Zip"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0082",localeid) & ": " & Session("Zip")& strCRet
		end if
		if len(Session("City"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0080",localeid) & ": " & Session("City")& strCRet
		end if
		if len(Session("Address"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0079",localeid) & ": " & Session("Address")& strCRet
		end if
		if len(Session("Address2"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0079",localeid) & "2: " & Session("Address2")& strCRet
		end if
		if len(Session("Company"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0078",localeid) & ": " & Session("Company")& strCRet
		end if
		if len(Session("Title"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0077",localeid) & ": " & Session("Title")& strCRet
		end if
		if len(Session("FirstName"))>0 and  len(Session("LastName"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0076",localeid) & ": "  &  Session("LastName")& " " & Session("Firstname") & " " & LoadI18N("SLBCA0160",Session("localeid"))& strCRet
		end if
		if len(Session("Phone"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0084",localeid) & ": " & Session("Phone")& strCRet
		end if
		if len(Session("Fax"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0085",localeid) & ": " & Session("Fax")& strCRet
		end if
		if len(Session("Email"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0086",localeid) & ": mailto:" & Session("Email")& strCRet
		end if
		if len(Session("URL"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0087",localeid) & ": " & Session("URL")& strCRet
		end if
		if len(Session("Applications"))>0 then
			EmailBodyClientDataHTML = EmailBodyClientDataHTML & LoadI18N("SLBCA0088",localeid) & ": " & Session("Applications")& strCRet
		end if
	end if
	
End Function

'************************
Function CreateEmailBodyHTML(LitRequest, DemoRequest, QuoteRequest, HelpRequest, SalesCall)
	' Author:          Kate Kaplan
    ' Date:            Jan 24, 2006
    ' Description:      Creates the email body to be send to the rep in HTML
	if len(Session("localeid"))=0 then Session("localeid")=1033	
	localeid=Session("localeid")
	strBody = "New request(s)"
	strCRet = "<br>"
	if len(Session("Source"))>0 then
			if len(Session("campaignid"))>0 then
				if cint(Session("CampaignID"))=7 then
					strBody = strBody & " (from DSO Family Mailer Campaign)"
				else
					strBody = strBody & " (from " & Session("Source") & ")"
				end if
			else
				strBody = strBody & " (from " & Session("Source") & ")"
			end if
	end if
	if localeid<>1041 then	
		' Client data
		strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientDataHTML	
		' Requests data
		strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
		
		' Literature
		If	LitRequest = True Then
			strBody = strBody & strCRet &  LoadI18N("SLBCA0339",localeid)  & strCRet
			strBody = strBody & Session("litBody") & strCRet & strCRet
		End If 	
		
		' Demo on current models
		If	DemoRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
			strBody = strBody & Session("DemoBody") & strCRet & strCRet
		End If

		 	
		' Help on all models
		If	HelpRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0341",localeid) & strCRet
			strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
		End If 	
		
		' Sales Call
		'SS new code start 4/7/05
		if len(Session("SEtocallBody")) > 0 and len(Session("SEnoModels")) = 0 then 
			strBody = strBody & strCRet & LoadI18N("SLBCA0632",localeid) & strCRet
			strBody = strBody & Session("SEtocallBody") & strCRet 
		else
			strBody = strBody & Session("SEtocallBody") & strCRet
		end if 
		if len(Session("SEcallques")) > 0 then 
			strBody=strBody & strCRet & LoadI18N("SLBCA0631",localeid) & strCRet & Session("SEcallques") & strCRet&  strCRet
		end if 
		'SS end of new code 4/7/05
	else
		' Client data
		strBody = strBody & ":"  & strCRet & strCRet &EmailBodyClientDataHTML	
		' Requests data
		strBody = strBody & LoadI18N("SLBCA0338",localeid) & strCRet
		
		' Literature
		If	LitRequest = True Then
			strBody = strBody & strCRet &  LoadI18N("SLBCA0339",localeid)  & strCRet
			strBody = strBody & Session("litBody") & strCRet & strCRet
		End If 	
		
		' Demo on current models
		If	DemoRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0340",localeid) & strCRet
			strBody = strBody & Session("DemoBody") & strCRet & strCRet
		End If

		 	
		' Help on all models
		If	HelpRequest = True Then
			strBody = strBody & strCRet & LoadI18N("SLBCA0341",localeid) & strCRet
			strBody = strBody & ParseArray(Request.QueryString("Object4"), "ALLMODEL")
		End If 	
		
		' Sales Call
		'SS new code start 4/7/05
		if len(Session("SEtocallBody")) > 0 and len(Session("SEnoModels")) = 0 then 
			strBody = strBody & strCRet & LoadI18N("SLBCA0632",localeid) & strCRet
			strBody = strBody & Session("SEtocallBody") & strCRet
		else
			strBody = strBody & Session("SEtocallBody") & strCRet
		end if 
		if len(Session("SEcallques")) > 0 then 
			strBody=strBody & strCRet & LoadI18N("SLBCA0631",localeid) & strCRet & Session("SEcallques") & strCRet&  strCRet
		end if 
		'SS end of new code 4/7/05
		' Sales Call
		'If	SalesCall = True Then
		'	strBody = strBody & strCRet & LoadI18N("SLBCA0342",localeid) 
		'End If
	end if
	CreateEmailBodyHTML = strBody
End Function
%>