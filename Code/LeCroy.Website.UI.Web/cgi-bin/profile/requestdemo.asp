<%@ LANGUAGE="VBSCRIPT"%>
<% 'You will enter the 5 lines bellow on each page%>
<!-- #INCLUDE FILE="allfunctions.asp" -->
<% Session("RedirectTo") = "RequestDemo.asp" 'here is the name of the file itself
If Not ValidateUser Then
	Response.redirect "SentToReg.asp"
End If
%>
<html>
<head>

<title>LeCroy Literature Request Form</title>
<meta name="Title" Content="LeCroy Information Request Form">
<meta name="Description" Content="This page enables users to request information, such as, Demos, Quotes, Literature &amp; Technical Help">

</head>
<body topmargin=0 leftmargin=0>
  
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#8000FF" width="5"></td>
		<td><img src="/images/spacer.gif" width="20" height="10">
		<%'Column before form starts - Do not enter anything here %></td>
		<td width="25">
	
<table width="500" border="0" cellspacing="0" cellpadding="0">
	<tr><font face="verdana" size="2">
		<br><% = Session("FirstName") %>, please let us know how we can help you.<br>
		<br>On this page you can request Product Quotes, Demos, and Technical Help.
		<br>
		

		<form method="GET" action="RequestRegistration.asp" id=form2 name=form2>
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td valign="top"><small><font face="Verdana" color="#000000">

						<%  'Insert Literature %>
						<input type="hidden" name="RequestType1" value="LIT">
						<input type="hidden" name="Remarks1" value="No Remarks for Literature">
						<input type="checkbox" name="Object1" value="52">
						
		<br>
		</small></font>
			</td>
					<br><td valign="top"><small><font face="Verdana" color="#000000">Please send me a copy of the NEW LC584AXL Brochure:</font></small></td>
					
		</tr>
		<tr>
			<td valign="top" width="25"><small><font face="Verdana" color="#000000">

				<%  'Insert Model %>
				<input type="hidden" name="RequestType2" value="DEM">
				<input type="hidden" name="Remarks2" value="No Remarks for Demo">
				<input type="checkbox" name="Object2" value="32"></td>
			<td valign="top" width="100"><small><font face="Verdana" color="#000000">I would like a demo of the 'AXL':</font></small></td>
			
				
		<%'Make Sure that the First Line in case there is no model includes Value="" %>
		<td valign="top"><small><font face="Verdana" color="#000000">

				<%  'Insert Model %>
				<input type="hidden" name="RequestType3" value="QOT">
				<input type="hidden" name="Remarks3" value="No Remarks for Quote">
				<input type="checkbox" name="Object3" value="32"></td>
		<td valign="top" width="100"><small><font face="Verdana" color="#000000">I would like to receive a quote on the 'AXL':</font></small></td>
		
					
				
		
		<br><br><br>
		</small></font>
		<tr>
		<td valign="top" width="25"><small><font face="Verdana" color="#000000">
				<%  'Insert Model %>
				<input type="hidden" name="RequestType4" value="TCH">
				<input type="hidden" name="Remarks4" value="No Remarks for Technical Help">
				<input type="checkbox" name="Object4" value="32">
				
		<%'Make Sure that the First Line in case there is no model includes Value="" %></td>
		<td valign="top" width="100"><small><font face="Verdana" color="#000000">I would like technical help:</font></small></td>
		
		
		
	<td valign="top" width="25">
			<input type="hidden" name="RequestType5" value="SCL">
			<input type="hidden" name="Remarks5" value="No Remarks for Sales Call">
			<input type="checkbox" value="1" name="Object5">
	</td>
	<td valign="top">
			<font face="verdana" size="2">Please have a salesperson call me:</font>
			</td>
	
		
		<tr>
			<td valign="top" width="25">
			<small><font face="Verdana">
			<%'Make sure that the Maximum Requests Asked = the total number of possibilities to request %>
			<input type="hidden" name="MaxRequestsAsked" value="5">	
			
			</td>
			<td valign="top">
			<nobr><input type="submit" value="Submit Request" name="B1">&nbsp<input type="reset" value="Reset" name="B2"></font></small>
			</td>
		</form>
	<td>
	</td>
	<td valign="top" colspan="2">
	
	</td>	
	</tr>
	</table>
	
<table width="200">
	<tr>
		<td>
			<hr size=1><font face="verdana" size="2">Please check your address:<br>
	<br><strong><% = Session("FirstName") %>&nbsp;<% = Session("LastName") %></strong><br>
	<% = Session("Company") %><br>
	<% = Session("Address") %><br>
	<%If Len(Session("Address2")) > 0 then 
	Session("Address2") %><br>
	<% end if %>
	<% = Session("City") %><br>
	<% = Session("State") %><br>
	<% = Session("Zip") %><br>
	<% = Session("Country") %><br>
	E-Mail: <% = Session("Email") %><br>
	Phone: <% = Session("Phone") %><br>
	<br>
	<font size="2">If any of this shipping information is incorrect or your address has changed, please click the button below.</font>
	<form action="https://teledynelecroy.com/cgi-bin/modform.asp" method="get" id="form1" name="form1">
	<% Session("RedirectToFirst") = "Test1.asp" %>
	<input type="submit" value="Change Address" id="submit1" name="submit1">
	</form>
	<hr size="1">
	</td>
</tr>
</table>

</body>
