﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.ddr_default" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Teledyne LeCroy offers a full line of DDR test solutions for system bring-up, debug, performance analysis, and compliance. Teledyne LeCroy’s DDR test solutions ensure the right solution for every stage of development.">
    <meta name="author" content="Teledyne LeCroy">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>DDR Test Suite | Teledyne LeCroy</title>

    <!-- Bootstrap core CSS -->
    <link href="https://teledynelecroy.com/newwave/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="https://teledynelecroy.com/newwave/css/carousel.css" rel="stylesheet">
    <link href="https://teledynelecroy.com/newwave/css/newwave.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
 
      <style type="text/css">
      .tab-pane
      {
          margin-top:25px;
          vertical-align:middle;
      }
      </style>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="https://teledynelecroy.com/" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="/">Products</a></li>
                    <li><a href="/serialdata/">Serial Data</a></li>
                    <li><a href="/support/">Support</a></li>
                    <li><a href="/support/">Buy</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading middle">Comprehensive DDR Test Suite</h2>
          <p class="lead">Teledyne LeCroy offers a full line of DDR test solutions for system bring-up, debug, performance analysis, and compliance. Teledyne LeCroy’s DDR test solutions ensure the right solution for every stage of development.</p>
          

          
        </div>

        <div class="col-md-6">
          <img class="featurette-image img-responsive" alt="Comprehensive DDR Test Suite" style="max-height:500px;" src="/images/dh-probes-02.jpg">
        </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Physical Layer DDR Toolkit</h2>
          <p class="lead">The <a href="/options/productseries.aspx?mseries=475&groupid=140">DDR Debug Toolkit</a> provides test, debug, and analysis tools for the entire DDR design cycle. Unique DDR analysis capabilities provide automatic Read and Write burst separation, bursted data jitter analysis, and DDR-specific measurement parameters. All this DDR analysis can be performed simultaneously over four different measurement views.
</p>
        </div>
        <div class="col-md-7">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#ddr1" role="tab" data-toggle="tab">Effortless Burst Separation</a></li>
  <li><a href="#ddr2" role="tab" data-toggle="tab">Eye Diagram Analysis</a></li>
  <li><a href="#ddr3" role="tab" data-toggle="tab">DDR-Specific Parameters</a></li>
  </ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="ddr1"><a href="/images/ddr-suite-02.png" rel="shadowbox"><img src="/images/ddr-suite-02.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>Automatic separation of Read and Write bursts eliminates the time-consuming process of manual burst identification and simplifies the analysis of DDR systems. Bursts can be separated based on DQ-DQS skew or based on the command bus when used in conjunction with the HDA125.  The HDA125 additionally enables a unique "bus view", tabulating the Command Bus activity and placing color-coded overlays and annotations on top of the physical layer waveform in an intuitive manner.</p>
</div>
  <div class="tab-pane" id="ddr2"><a href="/images/ddr-suite-03.png" rel="shadowbox"><img src="/images/ddr-suite-03.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>Any DQ, DQS or command/address signal can be tested against a standard or a custom defined mask, with up to 10 eye diagrams viewed simultaneously. Enabling mask failure indicators will automatically identify and locate the specific UI where any mask violation occurred. Built-in measurements such as eye height, eye width, and eye opening are critical to gaining a quantitative understanding of the system performance. With simultaneous eye measurements it is easy to compare performance across multiple testing views.</p>
</p></div>
  <div class="tab-pane" id="ddr3"><a href="/images/ddr-suite-04.png" rel="shadowbox"><img src="/images/ddr-suite-04.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>With a toolbox of parameters specific to DDR it is simple to quickly configure insightful measurements for validation, characterization, and debug. Up to 12 configurable measurements can be displayed and analyzed simultaneously across all active measurement views. For each measurement, advanced statistics such as min, max, mean, and number of measurement instances can be displayed and easily located with the searchable zoom feature.</p>
</div>
  </div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Physical Layer Compliance</h2>
          <p class="lead">The QualiPHY DDR packages perform all of the clock, electrical and timing tests per the JEDEC standards. Upon completion of each test run a report is generated which contains pass/fail results as well as fully annotated screenshots of the worst case measurement</p>
            <a href="/options/productseries.aspx?mseries=460&groupid=140" type="button" class="btn btn-default">QPHY-DDR4/LPDDR4<br />Datasheet</a>&nbsp;
            <a href="/options/productseries.aspx?mseries=269&groupid=140" type="button" class="btn btn-default">QPHY-DDR3/LPDDR3<br />Datasheet</a><br />
            <a href="/options/productseries.aspx?mseries=231&groupid=140" type="button" class="btn btn-default" style="margin-top:10px;">QPHY-DDR2 Datasheet</a>&nbsp;
            <a href="/options/productseries.aspx?mseries=355&groupid=140" type="button" class="btn btn-default" style="margin-top:10px;">QPHY-LPDDR2 Datasheet</a>
        </div>
        <div class="col-md-7" style="text-align:left;">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#ddr4" role="tab" data-toggle="tab">Complete JEDEC Test Coverage</a></li>
  <li><a href="#ddr5" role="tab" data-toggle="tab">Fully Annotated Screenshots</a></li>
  <li><a href="#ddr6" role="tab" data-toggle="tab">Automatic Report Generation</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="ddr4"><a href="/images/ddr-suite-05.png" rel="shadowbox"><img src="/images/ddr-suite-05.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>QualiPHY DDR packages preform all clock, electrical, and timing tests to conform the JEDEC specification. Additionally, eye diagrams are created to assist in debugging and to provide a high-level system overview. A limit set is included for each standard speed grade and can be fully customized.</p></div>
  <div class="tab-pane" id="ddr5"><a href="/images/ddr-suite-06.png" rel="shadowbox"><img src="/images/ddr-suite-06.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>In addition to the measured value and the pass/fail status for each test, QualiPHY reports contain screenshots of the worst case measurement for each test. Each screenshot is fully annotated including trace labels and pertinent voltage levels.</p></div>
  <div class="tab-pane" id="ddr6"><a href="/images/ddr-suite-07.png" rel="shadowbox"><img src="/images/ddr-suite-07.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>Upon completion of each test run the QualiPHY software generates a compliance reports contain pass/fail results, all of the tested values, the specific test limits and fully annotated screenshots of the worst case measurement. Compliance Reports can be created as HTML, PDF or XML.</p></div>
</div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
          <div class="col-md-5">
            <h2 class="featurette-heading top">High-speed Digital Analyzer</h2>
            <p class="lead">The <a href="/Options/ProductDetails.aspx?modelid=9703&categoryid=18&groupid=54">HDA125</a> turns your Teledyne LeCroy oscilloscope into the highest-performance, most flexible mixed-signal solution for DDR debug and evaluation. With 12.5 GS/s digital sampling rate on 18 input channels and the revolutionary QuickLink probing solution, validation of DDR interfaces has never been simpler or more comprehensive.</p>
          </div>
          <div class="col-md-7" style="text-align:left;">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified" role="tablist">
              <li class="active"><a href="#ddr8" role="tab" data-toggle="tab">Command Bus Capture for Full Interface Visibility</a></li>
              <li><a href="#ddr9" role="tab" data-toggle="tab">Analyze Bus Activity</a></li>
              <li><a href="#ddr10" role="tab" data-toggle="tab">Trigger on DDR Commands</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="ddr8"><a href="/images/ddr-suite-08.png" rel="shadowbox"><img src="/images/ddr-suite-08.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>Basic debugging and validation of embedded DDR interfaces typically involves analysis of the analog properties of the clock, data (DQ) and strobe (DQS) signals but when validation tasks become more complex and problems require deeper insight, the ability to trigger on, acquire and visualize the state of the DDR command bus is invaluable. The HDA125 brings command bus acquisition to Teledyne LeCroy’s already comprehensive toolset, providing the ultimate in memory bus analysis capability.</p>
</div>
                <div class="tab-pane" id="ddr9"><a href="/images/ddr-suite-09.png" rel="shadowbox"><img src="/images/ddr-suite-09.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>The HDA125 enables the unique “bus view” feature of the DDR Debug software option, which brings Teledyne LeCroy’s advanced bus analysis feature set to bear on DDR analysis. View bus activity in tabular form, and move time-correlated views to a desired event with the touch of a button. Search for specific events and bus states within the acquired record. Intuitive color overlays and annotations make it easy to identify areas of interest in the acquired analog waveforms.</p>
</div>
                <div class="tab-pane" id="ddr10"><a href="/images/ddr-suite-10.png" rel="shadowbox"><img src="/images/ddr-suite-10.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>The ability to trigger on specific states of the command bus becomes an invaluable tool for quick understanding of DDR signal quality. The HDA125’s logic triggering combines with the DDR Debug toolkit's intuitive setup and intelligent software cross-triggering to provide the ultimate DDR triggering system. Persistence maps of read and write bursts provide an easy and fast means of identifying subtle signal-quality problems for further investigation.</p>
</div>
            <!-- End NAV TABS -->
            </div>
        </div>
      </div>

      <hr class="featurette-divider">
<div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Unmatched probing versatility</h2>
          <p class="lead">The QuickLink probe tip system was designed from the ground up to be compatible with both the HDA125 and with Teledyne LeCroy’s high-bandwidth differential analog probes. This cross-connection ability allows you to equip your system under test with QuickLink tips at all desired test points, and swap connections between digital and analog acquisition systems as needed. Industry-leading tip impedance ensures superior signal fidelity and low loading.</p>

          <a href="/probes/dh-series-differential-probes" type="button" class="btn btn-default">DH Series Datasheet</a>

        </div>
        <div class="col-md-7" style="text-align:left;">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#logic4" role="tab" data-toggle="tab">Superior analog and digital signal fidelity</a></li>
  <li><a href="#logic5" role="tab" data-toggle="tab">Simple and cost-effective</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="logic4"><a href="/images/dh-probes-07.jpg" rel="shadowbox"><img src="/images/dh-probes-07.jpg" width="300px" border="0" align="left" hspace="10px" style="margin-bottom: 35px;" /></a><p style="text-align:left;">When connected to a WaveLink analog probe, QuickLink tips provide 8 GHz of bandwidth and a flat, well-controlled frequency response. When used for digital acquisitions with the HDA125, they support 3 GHz bandwidth with industry-leading sensitivity. In both cases, high input impedance ensures minimal loading of the system under test.</p></div>
  <div class="tab-pane" id="logic5"><a href="/images/dh-probes-04.jpg" rel="shadowbox"><img src="/images/dh-probes-04.jpg" width="300px" border="0" align="left" hspace="10px"  style="margin-bottom: 45px;" /></a><p style="text-align:left;">QuickLink solder-in tips are low-cost, making it easy to equip multiple test points and DUTs, and eliminating time-consuming re-soldering of connectors. The integral 9-inch lead effectively relocates your test point to a more convenient location, and makes testing more reliable by eliminating torque and other forces on the solder joints.</p></div>
  


</div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2020&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://teledynelecroy.com/newwave/js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
        Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
