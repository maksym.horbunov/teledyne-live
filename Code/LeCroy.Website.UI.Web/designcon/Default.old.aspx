<%@ Page Language="VB" MasterPageFile="~/MasterPage/MasterPage.master" AutoEventWireup="false" Inherits="LeCroy.Website.DesignCon" title="LeCroy - DesignCon 2012" Codebehind="Default.old.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 
  <form runat="server" id="clockDataForm">

<script>
    jQuery.noConflict();
  jQuery(document).ready(function ($) {
    if ($('.plusColumn').length) {
        $('.plusColumn').each(function () {
            var $image = $(this).find('.plusImage');
            var $imagePosition = $image.position();
            var $plusHoverPositionX = $imagePosition.left + $image.outerWidth() - $(this).find('div.plusHover').outerWidth();
            var $plusHoverPositionY = $imagePosition.top + $image.outerHeight(true) - $(this).find('div.plusHover').outerHeight();
            $(this).find('div.plusHover').css('left', $plusHoverPositionX).css('top', $plusHoverPositionY);
            $(this).find('div.plusHover').show();
            $(this).hover(function () {
                $(this).find('div.plusHover').addClass('plusHover-over');
                            $(this).css('cursor', 'pointer');
            }, function () {
                $(this).find('div.plusHover').removeClass('plusHover-over');
            });
                $(this).click(function () {
                    if($(this).hasClass('noUrl')) { }
                    else {
                        window.location = $(this).children('a').attr('href');
                    }
            });
        });
    }

    });
</script>

  <style>

/* STYLE FOR THE USP BOXES */

div.USP { min-height:330px; }

div.USPBoxes { display:none; }



/* FOR THE IMAGE ROLLOVER */

div.plusHover { position:absolute; width: 27px; height:27px; background-image:url('/Global/Images/Icons/plusImageHover.png'); background-repeat:no-repeat; }

div.plusHover-over { background-image:url('/Global/Images/Icons/plusImageHover-over.png'); }



.USPBoxes div p img {border:1px solid #00A1E2;}

</style>



<script>
    /* SCRIPT FOR THE USP BOXES */
    jQuery.noConflict();
    jQuery().ready(function ($) {
        $("div.USP").click(function (e) {
            if ($(this).hasClass("selected")) { return false; }
            else {
                $(".USPBoxes:visible").hide(200);
                $(this).addClass('selected');
                $("div.USP").not(this).removeClass('selected');
                $("#" + $(this).attr("id") + "BOX").show(1000);
                return false;
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }
        });
    });

</script>


     <link href='http://fonts.googleapis.com/css?family=Gruppo' rel='stylesheet' type='text/css'>
        <div id="categoryTop" style="background:url(/images/oscilloscope/designcon_background.png) no-repeat; background-color:#cdd0d1;">
            <div class="content">
                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="235" valign="top">
                           <img src="<%=rootDir %>/images/spacer.gif" width="100" height="115" alt="Be the Labmaster | DesignCon 2012" />  
                          <p style="font-size:16px; line-height:26px;">Every year LeCroy takes its show on the<br />road to DesignCon, the premiere event<br />for signal integrity engineers, in Silicon Valley.<br />Come see us at Booth 101.</p>                            
                      </td>
                        <td width="248" valign="top">&nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="categoryBottom">
            <div class="content">
            <a href="/labmaster/labmaster10Zi/"><img src="/images/oscilloscope/labmasterbanner.jpg" alt="LabMaster 10 Zi"  border=0></a><br /><br />
                
                <p><a href="http://2012.designcon.com/lecroy" onclick="GaEventPush('OutboundLink', '2012.designcon.com/lecroy');" target="_blank">Register</a> for your complimentary Expo Pass, courtesy of LeCroy</p>
<p>Meet the LeCroy engineers at booth 101 for demos and discussions on:</p>
<ul type="disc">
  <li>LabMaster: the world's highest bandwidth - 60 GHz - oscilloscope</li>
  <li>8 HP SiGe 36 GHz chipset and patented Digital Bandwidth Interleave (DBI) technology</li>
  <li>12-port S-parameter analysis with SPARQ Signal Integrity Network Analyzer</li>
  <li>LeCroy's new 12-bit HRO high resolution oscilloscope</li>
  <li>End-to-end serial data test solutions including transmitter, receiver and protocol-enabled test</li>
  <li>PCI Express and DDR protocol analysis</li>
</ul>
<p><strong>Highlights of the technical conference include:</strong></p>
<p><strong><a href="http://schedule.designcon.com/session/6410" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6410');" target="_blank">Delivering on Time-to-Answer: Meeting Designers Needs in Test & Measurement</a></strong><br>
  Industry Panel: David Graef (LeCroy Corporation), Greg Peters (Agilent Technologies), Kevin Ilcisin (Tektronix),  and Eric Starkloff (National Instruments) Moderator: Patrick Mannion (UBM Electronics)</p>
<p><strong><a href="http://schedule.designcon.com/session/6325" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6325');" target="_blank">De-Embedding in High Speed Design</a></strong><br>
  Tutorial Presented by: Kaviyesh Doshi (LeCroy), Peter Pupalaikis (LeCroy), Don DeGroot (CCN), and David Dunham (Molex Inc) </p>
<p><strong><a href="http://schedule.designcon.com/session/6385" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6385');" target="_blank">The Case of the Closing Eye: De-Mystifying the Measurement Complexity</a></strong><br>
  Industry Panel:  Martin Miller (LeCroy Corporation), Chris Loberg (Tektronix - Danaher), Ransom Stephens (Teraspeed Consulting, LLC), Mike Peng Li (Altera), Greg Le Cheminant (Agilent Technologies), Eric Kvamme (LSI Corp.), Mark Marlett (Analog Bits) and Pavel Zivny (Tektronix, Inc.)</p>
<p><strong><a href="http://schedule.designcon.com/session/6412" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6412');" target="_blank">How Return Loss Gets Its Ripples</a></strong><br>
  Speed Training Special Event with Eric Bogatin (Bogatin Enterprises)</p>
<p><strong><a href="http://schedule.designcon.com/session/6304" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6304');" target="_blank">A Robust Method for Addressing 12 Gbps Interoperability for High-Loss and Crosstalk-Aggressed Channels</a></strong><br>
  Technical Paper Presentation by: Alan Blankman (LeCroy Corporation), Eric Bogatin (Bogatin Enterprises), James Bell (Wild River Technology LLC), Alfred Neves (Wild River Technology LLC), George Noh (Vitesse Semiconductor),  and Marty Spadaro (Vitesse Semiconductor Corporation)</p>
<p><strong><a href="http://schedule.designcon.com/session/6303" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6303');" target="_blank">A Practical Approach for Using Circuit Board Qualification Test Results to Accurately Simulate High Speed Serial Link Performance</a></strong><br>
  Technical Paper Presentation by: Alan Blankman (LeCroy Corporation), Eric Bogatin (Bogatin Enterprises), and Don DeGroot (CCN)</p>
<p><strong><a href="http://schedule.designcon.com/session/6415" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6415');" target="_blank">Ask The Experts... Anything Goes!</a></strong><br>
  Panel Forum Led by: Eric Bogatin (Bogatin Enterprises)</p>
<p><strong><a href="http://schedule.designcon.com/session/6387" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6387');" target="_blank">The Relationship Between Discrete Frequency S-Parameters and Continuous Frequency Responses</a></strong><br>
  Technical Paper Presentation by: Peter Pupalaikis (LeCroy)</p>
<p><strong><a href="http://schedule.designcon.com/session/6344" onclick="GaEventPush('OutboundLink', 'schedule.designcon.com/session/6344');" target="_blank">Fast and Optimal Algorithms for Enforcing Reciprocity, Passivity and Causality in S-Parameters</a></strong><br>
  Technical Paper Presentation by: Kaviyesh Doshi (LeCroy), Peter Pupalaikis (LeCroy) and Anirudh Sureka (LeCroy)</p>
<br />

                
                
            </div>
        </div>
  </form>
</asp:Content>
