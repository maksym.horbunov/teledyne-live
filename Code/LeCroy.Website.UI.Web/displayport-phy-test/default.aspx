﻿<%@ Page Title="Teledyne LeCroy - DisplayPort PHY Testing" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.displayport_phy_test_default" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main" class="has-header-transparented skew-style reset v1">
							<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
		<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
				<section class="section-banner-slide section-white section-angle-before angle-small small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/oscilloscopes/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>Complete DisplayPort™ PHY Test Solution</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-text">
										<p>Teledyne LeCroy provides the fastest and most complete DisplayPort 2.0 PHY<br />Test Solution</p>
										<ul class="list-dots mb-3">
											<li>The fastest way to DisplayPort 2.0 and 1.4 Source compliance</li>
											<li>Simple, repeatable Sink calibration and compliance testing</li>
											<li>DisplayPort-AUX and USB Type-C<sup>&reg;</sup> ‘Alt Mode’ debug</li>
										</ul>
									</div>
									<div class="page-button anchor-active"><a class="btn btn-default btn-blue-outline" href="/doc/displayport-phy-datasheet">Datasheet</a></div>
								</div>
							</div>
							<div class="col-md-6"><a href="/options/productseries.aspx?mseries=654&groupid=140"><img src="https://assets.lcry.net/images/displayport-phy-solution_collage.png" alt="DisplayPort PHY Solution"></a></div> 
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xl-4"> 
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery30" href="https://assets.lcry.net/images/003_dp20_source_lm_2ln.png"><img src="https://assets.lcry.net/images/003_dp20_source_lm_2ln.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Fastest Source Testing</h3>
										</div>
										<div class="card-content">        
											<ul class="list-dots mb-3">
												<li>Multi-Lane UHBR20, UHBR13.5, and UHBR10 testing using the <a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10Zi A</a> oscilloscope</li>
												<li>Easy setup and execution using <a href="/options/productseries.aspx?mseries=654&groupid=140">QPHY-DisplayPort2.0-Source</a> software</li>
												<li>Debug compliance failures using <a href="/sdaiii/">SDA-III-CompleteLinQ</a> software  </li>
												<li>Includes <a href="/options/productseries.aspx?mseries=241&groupid=140">QPHY-DisplayPort1.4-Source</a> (HBR3, HBR2, HBR, and RBR) compliance test software</li>
											</ul><a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=654&groupid=140">Explore DP 2.0 Source Compliance</a>
											<a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=241&groupid=140">Explore DP 1.4 Source Compliance</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery31" href="https://assets.lcry.net/images/023_dp20_sink_cal_tp3_lm.png"><img src="https://assets.lcry.net/images/023_dp20_sink_cal_tp3_lm.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Simple, Repeatable Sink Testing</h3>
										</div>
										<div class="card-content">        
											<ul class="list-dots mb-3">
												<li><a href="/options/productseries.aspx?mseries=654&groupid=140">QPHY-DisplayPort2.0-Sink</a> fully automates sink test calibration and execution with the Anritsu SQA-R MP1900A stressed signal generator</li>
												<li>ISI Channel calibration using the <a href="/tdr-and-s-parameters/series.aspx?mseries=592">WavePulser 40iX</a> High-speed Interconnect Analyzer</li>
												<li>Enjoy a single QPHY automation experience for both Source and Sink testing when testing Docks or Hubs</li>
											</ul>
											<a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=654&groupid=140">Explore DP 2.0 Sink Compliance</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery32" href="https://assets.lcry.net/images/dp-aux dme with pd msg-msg.png"><img style="max-height:210px;padding-left: 14px;" src="https://assets.lcry.net/images/dp-aux dme with pd msg-msg.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>DP-AUX and USB-C Debug</h3>
										</div>
										<div class="card-content mb-24">        
											<ul class="list-dots mb-3">
												<li><a href="/options/productseries.aspx?mseries=655&groupid=88">DisplayPort-AUX DME</a> provides AUX channel Decode, Measurements, and Eye testing for DP link training debug</li>
												<li>Time correlation with USB Power Delivery using <a href="/options/productseries.aspx?mseries=642&groupid=88">USB-PD TDME</a> for DisplayPort Alt Mode over USB Type-C debug</li>
											</ul>
											<a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=655&groupid=88">Explore DP-AUX DME</a>
									<a class="large-link text-blue font-weight-medium pb-0" href="/options/productseries.aspx?mseries=642&groupid=88">Explore USB-PD TDME</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
