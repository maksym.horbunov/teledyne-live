﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Partial Class doc
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Dim urchinCampaign As String = GetUrchinQueryStringValue("utmccn")
            Dim urchinSource As String = GetUrchinQueryStringValue("utmcsr")
            Dim urchinMedium As String = GetUrchinQueryStringValue("utmcmd")
            Dim urchinKeyword As String = GetUrchinQueryStringValue("utmctr")
            Dim urchinData As List(Of String) = New List(Of String)
            urchinData.Add(urchinCampaign)
            urchinData.Add(urchinSource)
            urchinData.Add(urchinMedium)
            urchinData.Add(urchinKeyword)
            Functions.SendRequestToGoogleAnalytics("DocRedirect", String.Format("{0}{1}", Request.AppRelativeCurrentExecutionFilePath, IIf(Not (String.IsNullOrEmpty(Request.QueryString.ToString())), String.Concat("?", Request.QueryString.ToString()), String.Empty)), Request.UrlReferrer, Request.UserAgent, HttpContext.Current.Request.ServerVariables("REMOTE_HOST"), Request.ContentEncoding.EncodingName, urchinData.ToArray())
        Catch
        End Try

        Dim d As String = ""
        Dim SubID As String = ""
        Dim strSQL As String = ""
        Dim dsRedirect As DataSet
        Dim strRedirect As String = ""
        Dim strQueryString As String = ""
        Dim strIP As String = ""
        'get Contact Id for registered user
        ValidateUserNoRedir()
        d = Request.QueryString("d")

        If Not Session("CONTACTID") Is Nothing Then
            If Len(Session("CONTACTID").ToString) = 0 Then
                Session("CONTACTID") = 0
            End If
        Else
            Session("CONTACTID") = 0
        End If
        If Len(Request.QueryString("id")) > 0 Then
            SubID = SQLStringWithOutSingleQuotes(Request.QueryString("id").ToString)
            Session("SubID") = SubID
        Else
            Session("SubID") = ""
        End If
        If Len(Request.QueryString("cid")) > 0 Then
            If IsNumeric(Request.QueryString("cid")) Then
                If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), ",") Then
                    Session("CampaignID") = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString), ",") + 1)
                Else
                    Session("CampaignID") = SQLStringWithOutSingleQuotes(Request.QueryString("cid").ToString)
                End If
            Else
                Session("CampaignID") = 0
            End If
        Else
            If Len(Session("CampaignID")) = 0 Then
                Session("CampaignID") = 0
            End If
        End If
        If Not Request.ServerVariables("REMOTE_ADDR") Is Nothing Then
            strIP = Request.ServerVariables("REMOTE_ADDR").ToString
        End If
        If Not d Is Nothing And Not Session("CampaignID") Is Nothing Then
            If Len(d) > 0 And Len(Session("CampaignID")) > 0 Then
                d = Replace(d, ".", "")
                If IsNumeric(d) Then
                    'inserting the clicked links into the database
                    '  If SubID Is Nothing And Len(SubID) > 0 Then
                    Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@DOC_ID", d))
                    sqlParameters.Add(New SqlParameter("@CONTACT_ID", Session("CONTACTID").ToString()))
                    sqlParameters.Add(New SqlParameter("@CAMPAIGN_ID", Session("CampaignID").ToString()))
                    sqlParameters.Add(New SqlParameter("@IP_ADDRESS", strIP.ToString()))
                    sqlParameters.Add(New SqlParameter("@SUB_ID", SQLStringWithOutSingleQuotes(SubID.ToString())))
                    Dim rowsAffected As Int32 = 0
                    DbHelperSQL.RunProcedure(ConfigurationManager.AppSettings("ConnectionString").ToString(), "sp_INSERT_REQUEST_LINKS", sqlParameters.ToArray(), rowsAffected)
                    ' strSQL = "INSERT INTO REQUEST_LINKS (DOC_ID,CONTACT_ID,CAMPAIGN_ID,DATE_ENTERED,IP_ADDRESS, SUB_ID) " + _
                    '            " VALUES (" & d & "," & Session("CONTACTID") & " ," & Session("CampaignID") & ",'" & Now() & "','" + _
                    '            Request.ServerVariables("REMOTE_ADDR") & "','" & SQLStringWithOutSingleQuotes(SubID) & "')"
                    'response.Write strsql
                    'response.End 
                    'DbHelperSQL.ExecuteSql(strSQL)
                    'End If

                    strSQL = "Select * from SUBSCRIPTION_LINKS where DOC_ID=@DOCID"
                    sqlParameters = New List(Of SqlParameter)
                    sqlParameters.Add(New SqlParameter("@DOCID", d))
                    dsRedirect = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                    If dsRedirect.Tables(0).Rows.Count > 0 Then
                        Dim dr As DataRow = dsRedirect.Tables(0).Rows(0)
                        strRedirect = Trim(dr("URL"))
                        If Len(Request.QueryString("rid")) > 0 Then
                            If Mid(strRedirect, 1, 4) = "http" Then
                                Response.Redirect(strRedirect)
                            End If
                            strQueryString = "rid=" & Request.QueryString("rid")
                        Else
                            If Mid(strRedirect, 1, 4) = "http" Then
                                Response.Redirect(strRedirect)
                            End If
                            If Len(SubID) > 0 Then
                                strQueryString = "subid=" & SubID
                            End If
                            If Len(Session("CampaignID")) > 0 Then
                                If Len(strQueryString) > 0 Then
                                    strQueryString = strQueryString & "&cid=" & Session("CampaignID")
                                Else
                                    strQueryString = "cid=" & Session("CampaignID")
                                End If
                            End If
                        End If
                        If Len(strQueryString) > 0 Then
                            If InStr(1, strRedirect, "?") > 0 Then
                                strRedirect = strRedirect & "&" & strQueryString
                            Else
                                strRedirect = strRedirect & "?" & strQueryString
                            End If
                        End If
                        Response.Redirect(strRedirect)
                    Else
                        Response.Redirect("default.aspx")
                    End If
                Else
                    Response.Redirect("default.aspx")
                End If
            Else
                Response.Redirect("default.aspx")
            End If
        Else
            Response.Redirect("default.aspx")
        End If
    End Sub

    Private Function GetUrchinQueryStringValue(ByVal qsParameter As String) As String
        Try
            If Not (String.IsNullOrEmpty(Request.QueryString(qsParameter))) Then
                Return Request.QueryString(qsParameter)
            End If
        Catch
        End Try
        Return String.Empty
    End Function
End Class