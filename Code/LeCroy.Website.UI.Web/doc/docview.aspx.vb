﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.Domain.Common.DTOs.Specialized

Public Class doc_docview
    Inherits BasePage

#Region "Variables/Keys"
    Private _documentFormat As String
    Private _documentId As Int32
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            ParseDocumentIdInQueryString()
            Dim documentFormats As List(Of DocumentFormat) = DocumentFormatRepository.GetDocumentFormats(ConfigurationManager.AppSettings("ConnectionString").ToString(), True)
            ParseDocumentFormatInQueryString(documentFormats)
            Dim recordDocumentRequests As Boolean = False
            Boolean.TryParse(ConfigurationManager.AppSettings("RecordDocumentRequests"), recordDocumentRequests)
            If (recordDocumentRequests) Then RequestDocumentRepository.SaveRequestDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), BuildRequestDocumentObject(_documentId))
            InterceptRequest()
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub ParseDocumentIdInQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("id"))) Then
            Response.Redirect("~/sitemap/default.aspx")
        End If
        Dim testVal As Int32 = 0
        Int32.TryParse(Request.QueryString("id"), testVal)
        If (testVal <= 0) Then
            Response.Redirect("~/sitemap/default.aspx")
        End If
        _documentId = DocumentReplacementRepository.RetrieveYoungestDocumentReplacementChild(ConfigurationManager.AppSettings("ConnectionString").ToString(), testVal)
    End Sub

    Private Sub ParseDocumentFormatInQueryString(ByVal documentFormats As List(Of DocumentFormat))
        If (String.IsNullOrEmpty(Request.QueryString("format"))) Then
            Return
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("format"))) Then
            If (documentFormats.Where(Function(x) x.FormatType.ToLower() = Request.QueryString("format").ToString().Trim().ToLower()).Count > 0) Then
                _documentFormat = Request.QueryString("format").ToString().Trim()
            End If
        End If
    End Sub

    Private Sub InterceptRequest()
        Dim documentFormatContainer As DocumentFormatContainer = DocumentFormatXRefRepository.GetDocumentFormatDataForDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentId)
        If (documentFormatContainer.DocumentFormatXRefs.Count > 0) Then
            If (String.IsNullOrEmpty(_documentFormat)) Then     ' A document format was not specified
                Dim documentFormatXRef As DocumentFormatXRef = documentFormatContainer.DocumentFormatXRefs.Where(Function(x) x.IsPrimary = True And x.DocumentFormatFkId = 5).FirstOrDefault()
                If Not (documentFormatXRef Is Nothing) Then
                    If Not (String.IsNullOrEmpty(documentFormatXRef.BaseFilePath)) Then
                        If (String.Compare(documentFormatXRef.BaseFilePath, "path", True) <> 0) Then
                            Response.Redirect(String.Format("~/doc/{0}", documentFormatXRef.BaseFilePath))
                        End If
                    Else
                        Response.Redirect(String.Format("~/doc/pageviewer.aspx?id={0}", _documentId))
                    End If
                End If
            End If
        End If
        LoadDocument()
    End Sub

    Private Sub LoadDocument()
        HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(False)
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.Cache.SetNoStore()
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1))
        Response.Cache.SetValidUntilExpires(True)
        Response.AppendHeader("Pragma", "no-cache")

        Dim RequestID As String = ""
        Dim strCRet As String = ""
        Dim strBody As String = ""

        Dim document As Document = DocumentRepository.GetDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentId)
        If (document Is Nothing) Then
            Response.Redirect("/sitemap/default.aspx")
        End If
        If (String.IsNullOrEmpty(document.FileFullPath)) Then
            Response.Redirect("/sitemap/default.aspx")
        End If

        Dim documentType As DocumentType = DocumentTypeRepository.GetDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocTypeId)
        If (documentType Is Nothing) Then
            Response.Redirect("/sitemap/default.aspx")
        End If

        Session("RedirectTo") = String.Format("/doc/docview.aspx?id={0}", _documentId) 'here is the name of the file itself
        If (String.Compare(document.Registration_YN, "Y", True) = 0) Then
            If Not ValidateUser() Then
                ' Do not http// for redirect
                Response.Redirect("/support/user/")
            End If
        End If

        If (String.Compare(document.Post, "n", True) = 0) Then
            If Not (String.Compare(document.Registration_YN, "Y", True) = 0) Then
                Response.Redirect("~/doc/notavailable.aspx")
            End If
        End If

        ValidateUserNoRedir()
        If Not Session("ContactID") Is Nothing And Not Session("Country") Is Nothing Then
            If Len(Session("ContactId")) > 0 Then
                If Len(Session("CampaignID")) = 0 Or Session("CampaignID") Is Nothing Then
                    Session("CampaignID") = 0
                End If

                RequestID = Functions.InsertRequest(Session("ContactId"), 21, _documentId, "No Remarks for Download", "", 0, Session("CampaignID"))
                If Len(Session("Country")) > 0 Then
                    If CStr(Session("Country")) = "Japan" Then
                        strCRet = Chr(13) & Chr(10)
                        strBody = strBody & "New PDF download:" & strCRet & strCRet & Functions.EmailBodyClientData(Session("ContactId"), 1041)
                        strBody = strBody & document.FileFullPath.ToLower() & strCRet
                        If Len(RequestID) > 0 And Len(strBody) > 0 Then
                            Try
                                Functions.SendEmail(Session("country"), strBody, "contact.jp@teledynelecroy.com", "webmaster@teledynelecroy.com", "", "", "New Download", "")
                            Catch
                            End Try
                        End If
                    End If
                End If
            End If
        End If

        HACK_HandleIERedirects(document.FileFullPath.ToLower())
        Try
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine("   piAId = '49392';")
            sb.AppendLine("   piCId = '1896';")
            sb.AppendLine("   piHostname = 'pi.pardot.com';")
            sb.AppendLine("   (function() {")
            sb.AppendLine("                  function async_load(){")
            sb.AppendLine("                       var s = document.createElement('script'); s.type = 'text/javascript';")
            sb.AppendLine("                       s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';")
            'sb.AppendLine("                       alert(s.src);")
            sb.AppendLine("                       var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);")
            sb.AppendLine("                  };")
            sb.AppendLine("                  async_load();")
            sb.AppendLine("   })();")


            sb.AppendLine("    (function (i, s, o, g, r, a, m) {")
            sb.AppendLine("    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {")
            sb.AppendLine("    (i[r].q = i[r].q || []).push(arguments)")
            sb.AppendLine("    }, i[r].l = 1 * new Date(); a = s.createElement(o),")
            sb.AppendLine("    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)")
            sb.AppendLine("    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');")
            sb.AppendLine("")
            sb.AppendLine("    ga('create', 'UA-13095488-1', 'auto');")
            sb.AppendLine(String.Format("    ga('send', 'pageview', '{0}');", document.FileFullPath.ToLower()))
            'sb.AppendLine("    ga('send', 'pageview');")
            'sb.AppendLine(String.Format("alert('{0}');", document.FileFullPath.ToLower()))
            sb.AppendLine(String.Format("    window.location.href = '{0}';", document.FileFullPath.ToLower()))
            HACK_SetSessionForIE(document.FileFullPath.ToLower())
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "RegisterPdf", sb.ToString(), True)
        Catch ex As Exception
            Functions.ErrorHandlerLog(True, String.Format("RegisterPDF: {0} {1} {2}", ex.Message, ex.StackTrace, ex.Source))
            HACK_SetSessionForIE(document.FileFullPath.ToLower())
            Response.Redirect(String.Format("{0}{1}", ConfigurationManager.AppSettings("AwsPublicBaseUrl").ToString(), document.FileFullPath.ToLower()))
        End Try
    End Sub

    Private Sub HACK_SetSessionForIE(ByVal pdfFilePath As String)
        If (Request.Browser.Type.ToUpper().Contains("IE") Or Request.Browser.Type.Contains("InternetExplorer")) Then
            Session("IEHack") = pdfFilePath
            Dim referrer As Uri = Request.UrlReferrer
            If (referrer Is Nothing) Then
                Return
            End If
            If (String.IsNullOrEmpty(referrer.ToString())) Then
                Return
            End If
            Session("IEHackPreviousPage") = referrer.ToString()
        End If
    End Sub

    Private Sub HACK_HandleIERedirects(ByVal pdfFilePath As String)
        If (Request.Browser.Type.ToUpper().Contains("IE") Or Request.Browser.Type.Contains("InternetExplorer")) Then
            Try
                If Not (Session("IEHack") Is Nothing) Then
                    If Not (String.IsNullOrEmpty(Session("IEHack").ToString())) Then
                        If (String.Compare(Session("IEHack").ToString(), pdfFilePath, True) = 0) Then
                            Session.Remove("IEHack")
                            Response.Redirect(Request.UrlReferrer.ToString())
                        End If
                    End If
                End If
            Catch
            End Try
        End If
    End Sub

    Private Function BuildRequestDocumentObject(ByVal documentId As Int32) As RequestDocument
        Dim contactFkId As Int32 = 0
        If Not (String.IsNullOrEmpty(Session("ContactID").ToString())) Then
            Int32.TryParse(Session("ContactID").ToString(), contactFkId)
        End If
        Dim retVal As RequestDocument = New RequestDocument()
        retVal.DocumentFkId = documentId
        retVal.SessionFkId = Session.SessionID.ToString()
        retVal.ContactFkId = contactFkId
        retVal.IpAddress = Request.UserHostAddress
        retVal.GeoIpCountry = Functions.ValidateIntegerAndGetDefaultCountryCodeIfNeededFromSession(Session("CountryCode"))
        Return retVal
    End Function
#End Region
End Class