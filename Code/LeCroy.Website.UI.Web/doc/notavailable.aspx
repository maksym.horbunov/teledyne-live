﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="notavailable.aspx.vb" Inherits="LeCroy.Website.doc_notavailable" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; padding-bottom: 20px; padding-top: 30px; text-align: center;">
        We're sorry, but the document you have requested could not be found.<br />
        Return to our <a href="../">homepage</a>.
    </div>
</asp:Content>