﻿<%@ Page Language="VB" AutoEventWireup="false" Codebehind="pageviewer.aspx.vb" MasterPageFile="~/MasterPage/contentviewer.master" Inherits="LeCroy.Website.doc_pageviewer" %>
<%@ Register TagPrefix="LeCroy" TagName="Summary" Src="~/usercontrols/viewer/summary.ascx" %>
<%@ Register TagPrefix="LeCroy" TagName="DocInfo" Src="~/usercontrols/viewer/information.ascx" %>
<%@ Register TagPrefix="LeCroy" TagName="Options" Src="~/usercontrols/viewer/options.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--title section-->
    <div class="page-header">
        <asp:Literal ID="lit" runat="server" />
        <h1><asp:Literal ID="litContentTitle" runat="server" /></h1>
        <span class="tagline"><asp:Literal ID="litContentReleaseDate" runat="server" /></span>
    </div>
    <!--/title section-->
    <!--content section-->
    <div class="row main-content">
        <!--fixed left boxes-->
        <div class="span3 fixedspan3">
            <LeCroy:Summary ID="ucSummary" runat="server" Visible="false" />
            <LeCroy:DocInfo ID="ucDocInfo" runat="server" Visible="false" />
            <LeCroy:Options ID="ucOptions" runat="server" Visible="false" />
        </div>
        <!--/fixed left boxes-->
        <!--placeholder-->
        <div class="span3"></div>
        <!--/placeholder-->
        <!--middle content-->
        <div class="span9 content-mid">
            <asp:Literal ID="litContent" runat="server" />
        </div>
        <!--/middle content-->                          
    </div>
    <!--/content section-->
    <div id="div" runat="server"></div>

    <asp:ScriptManager runat="server" />
    <script type="text/javascript" language="javascript">
        Sys.Application.add_init(function () {
            if (window.location.hash) {
                $.ajax({
                    type: "POST",
                    url: "pageviewer.aspx/getcdnurl",
                    data: '{"seoUrl": "' + window.location.href.substr(window.location.href.lastIndexOf('/') + 1) + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var response = msg.d;
                        if (response) {
                            window.location.href = response;
                        }
                    }
                });
            }
        });
    </script>
</asp:Content>