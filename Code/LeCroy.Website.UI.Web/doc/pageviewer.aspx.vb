﻿Imports System.Web.Services
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.Domain.Common.DTOs.Specialized
Imports LeCroy.Website.BLL

Public Class doc_pageviewer
    Inherits BasePage

#Region "Variables/Keys"
    Private _documentId As Nullable(Of Int32)
    Private _documentTitle As String
    Private _seoUrl As String
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            If (Page.RouteData.Values.Count() > 0) Then
                ProcessUrlRoutingData()
            Else
                ParseDocumentIdInQueryString()
            End If

            Dim documentFormats As List(Of DocumentFormat) = DocumentFormatRepository.GetDocumentFormats(ConfigurationManager.AppSettings("ConnectionString").ToString(), True)
            BindPage(documentFormats)
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub ProcessUrlRoutingData()
        Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
        If Not (param1.Key Is Nothing) Then
            Dim xParam As Document = DocumentRepository.GetDocumentBySeoUrl(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value.ToString())
            If Not (xParam Is Nothing) Then
                _seoUrl = param1.Value.ToString()
                _documentId = xParam.DocumentId
                Return
            End If
        End If
        HandleBouncedRedirect()
    End Sub

    Private Sub ParseDocumentIdInQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("id"))) Then
            Return
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("id"))) Then
            Dim testVal As Int32 = 0
            Int32.TryParse(Request.QueryString("id"), testVal)
            If (testVal <= 0) Then
                Return
            End If
            _documentId = DocumentReplacementRepository.RetrieveYoungestDocumentReplacementChild(ConfigurationManager.AppSettings("ConnectionString").ToString(), testVal)
        End If
    End Sub

    Private Sub BindPage(ByVal documentFormats As List(Of DocumentFormat))
        If Not (_documentId.HasValue) Then          ' Could not determine document id, bounce
            HandleBouncedRedirect()
        End If

        Dim document As Document = DocumentRepository.GetDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentId)
        If (document Is Nothing) Then               ' Could not retrieve a document association, bounce
            HandleBouncedRedirect()
        End If

        If (String.Compare(document.Post, "n", True) = 0) Then
            If Not (String.Compare(document.Registration_YN, "Y", True) = 0) Then
                Response.Redirect("~/doc/notavailable.aspx")
            End If
        End If

        Dim documentType As DocumentType = DocumentTypeRepository.GetDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocTypeId)
        If (documentType Is Nothing) Then           ' Could not find a valid document type XREF, bounce
            HandleBouncedRedirect()
        End If

        If Not (documentType.IsHtmlAllowed) Then    ' HTML is not allowed, follow standard document display procedure
            Response.Redirect(FeDocumentManager.GetDocumentRedirectUrl(_documentId, document.DocTypeId))
        End If

        If (documentFormats.Where(Function(x) x.CanRenderInBrowser = True).Count > 0) Then  ' Fall through, display HTML version of document
            RenderDocument(document)
            Return
        End If
        HandleBouncedRedirect() ' Something completely failed, bounce
    End Sub

    Private Sub RenderDocument(ByVal document As Document)
        Dim documentFormatContainer As DocumentFormatContainer = DocumentFormatXRefRepository.GetDocumentFormatDataForDocument(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentId)
        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), document.DocumentId, 19)
        If Not (cmsOverview Is Nothing) Then
            litContent.Text = Server.HtmlDecode(Server.HtmlEncode(cmsOverview.OverviewDetails))
            litContentTitle.Text = document.Title
            litContentReleaseDate.Text = document.DateEntered.ToString("MMMM d, yyyy")
            Dim docIdMetaTag As HtmlMeta = New HtmlMeta()
            docIdMetaTag.Name = "document-id"
            docIdMetaTag.Content = document.DocumentId.ToString()
            Header.Controls.Add(docIdMetaTag)
            If Not (String.IsNullOrEmpty(document.Description)) Then
                ucSummary.Description = document.Description
                ucSummary.Visible = True
            End If

            If (document.DateCreated.HasValue Or Not (String.IsNullOrEmpty(document.Author))) Then
                ucDocInfo.DocumentDate = document.DateCreated.Value.ToShortDateString()
                ucDocInfo.Author = document.Author
                ucDocInfo.Visible = True
            End If

            If (documentFormatContainer.DocumentFormats.Count > 0) Then
                Dim formats As OrderedDictionary = New OrderedDictionary()
                For Each format As DocumentFormatXRef In documentFormatContainer.DocumentFormatXRefs.OrderBy(Function(x) x.SortId).ToList()
                    Dim docFormat As DocumentFormat = documentFormatContainer.DocumentFormats.Where(Function(x) x.DocumentFormatPkId = format.DocumentFormatFkId).FirstOrDefault()
                    If (docFormat.DocumentFormatPkId = 5) Then  ' Suppress HTML from showing on this page since it's rendering as HTML
                        Continue For
                    End If
                    formats.Add(docFormat.FormatType, String.Format("{0}|{1}|{2}", docFormat.IconCssClass, docFormat.HrefCssClass, docFormat.FormatType.ToLower()))
                Next
                If (formats.Count > 0) Then
                    ucOptions.SeoUrl = _seoUrl
                    ucOptions.SupportedFormats = formats
                    ucOptions.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/")
    End Sub
#End Region

#Region "WebMethods"
    <WebMethod()> _
    Public Shared Function getcdnurl(ByVal seoUrl As String) As String
        If (seoUrl.Contains("#")) Then
            Dim splitUrl As String() = seoUrl.Split("#")
            Select Case splitUrl(1).ToLower()
                Case "pdf"
                    Dim document As Document = DocumentRepository.GetDocumentBySeoUrl(ConfigurationManager.AppSettings("ConnectionString").ToString(), splitUrl(0))
                    If Not (document Is Nothing) Then
                        Return document.FileFullPath
                    End If
            End Select
        End If
        Return String.Empty
    End Function
#End Region
End Class