<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.electronic_loads_default" Codebehind="default.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server">
            <ul>
                <li class="current">
                    <asp:Label ID="lbSeries1" runat="server" />
                    <ul><asp:Label ID="lbProduct1" runat="server" /></ul>
                </li>
            </ul>
        </asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <div class="intro">
                    <h1><asp:Label ID="lbSeriesTileTop" runat="server" /></h1>
                    <p><asp:Label ID="lbSeriesDesc" runat="server" /></p>
                    <asp:Label ID="lbSeriesTileBelow" runat="server" CssClass="lecroyblue" />
                </div>
            </td>
            <td valign="top"><asp:Label ID="lbSeriesImage" runat="server" /></td>
        </tr>
    </table>
    <div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Literal ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line"><asp:Label ID="lbQuickSpecs" runat="server" /></div>
            <div id="overview"><asp:Label ID="lblOverview" runat="server" /></div>
            <div id="product_details"><asp:Label ID="lblProductDetail" runat="server" /></div>
            <div id="spec"><asp:Literal ID="lblSpecs" runat="server" /></div>
            <div id="listprice"><asp:Literal ID="lblListPrice" runat="server" /></div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="lbPromotion" runat="server" />
    <div class="greyPadding"></div>
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="39" ProductGroupId="237" ProductSeriesId="605" ShowBuy="false" ShowRequestDemo="false" ShowRequestQuote="true" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>