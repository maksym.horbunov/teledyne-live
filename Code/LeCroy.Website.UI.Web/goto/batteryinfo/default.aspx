﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_batteryinfo_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="padding-left: 100px; padding-right: 100px; padding-top: 10px;" valign="top">
                <p align="left">
                    <font face="verdana,arial,helvetica" size="2"><font color="#1372CF"><b>Transport of Lithium-ion Batteries</b></font><br /><br />
                        In accordance with UN Recommendations on the transport of Lithium Ion batteries (ST/SG/AC.10/27/Add.2), the 44th Edition of Dangerous Goods Regulation issued by the International Air Transport Association (IATA) includes changes to the Lithium-ion battery handling provisions. Several national regulatory agencies (e.g. US. D.O.T.) have also incorporated the UN recommendations into their regulations.<br /><br />
                        The battery has an aggregate Lithium-equivalent greater than 8 grams. The WaveSurfer battery (WS-BATT) falls into the regulatory category of Hazardous Materials class 9 – miscellaneous dangerous goods. Packaging used to transport the WS-BATT must meet the Class-9 Dangerous Goods transport requirements and relevant packing instructions. Before shipping the WS-BATT, consult with your transportation company on specific measures required for shipment of this class of product.<br /><br />
                        Please refer to the transportation documents provided for more information.<br /><br />
                        <font color="#1372CF"><b>Transportation Documents</b></font><br /><br /><a href="/files/pdf/msds_mh63-2738.pdf">Materials Safety Data Sheet (MSDS)</a>
                    </font>
                    <font face="verdana,arial,helvetica" size="1">-file in PDF format - size 51 KB</font>
                    <font face="verdana,arial,helvetica" size="2"><br /><br /><a href="/files/pdf/un_tests.pdf">UN Test Documentation</a></font>
                    <font face="verdana,arial,helvetica" size="1">- file in PDF format - size 255 KB</font>
                </p>
            </td>
        </tr>
    </table>
</asp:Content>