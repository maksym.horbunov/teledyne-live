﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_download_getmore_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel runat="server" DefaultButton="btnSubmit">
        <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 100px; margin-right: 100px; margin-top: 10px;">
            <tr>
                <td colspan="2" valign="top">
                    <span style="color: #0000ff; font-face: Verdana; font-size: 26px; font-weight: bold;">Get More out of Your Digital Oscilloscope</span>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <p><font face="verdana,arial" size="2">Digital oscilloscopes have become increasingly capable of a larger and more sophisticated set of measurements but many users do not always take advantage of all these capabilities.<strong></strong></font></p>
                    <p><font face="Verdana" size="2"><strong>The booklet</strong>: Getting More out of Your Digital Oscilloscope outlines several ways in which everyday use of a digital oscilloscope can be improved. Methods discussed can be applied to a wide variety of products and applications.</font></p>
                </td>
                <td style="padding-left: 30px; padding-right: 120px;">
                    <img src="/images/gettingmore.jpg" alt="" border="0" height="200" width="129" />
                </td>
            </tr>
            <tr>
                <td colspan="2"><span style="color: #000000; font-family: Verdana; font-size: 14px; font-weight: bold;">Register and download the booklet "Getting More out of Your Digital Oscilloscope" and learn more about: </span></td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <ul style="padding-left: 40px;">
                        <li><font face="Verdana" size="2">&nbsp;Improved techniques for signal capture and viewing </font></li>
                        <li><font face="Verdana" size="2">&nbsp;Capturing and viewing signals using persistence mode and exclusion trigger </font></li>
                        <li><font face="Verdana" size="2">&nbsp;Improved techniques for signal measurement and analysis </font></li>
                        <li><font face="Verdana" size="2">&nbsp;Using signal and parameter histograms </font></li>
                        <li><font face="Verdana" size="2">&nbsp;Using FFT to examine frequency content</font></li>
                    </ul>
                    <p style="padding-left: 40px;"><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Download Booklet" /></p>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>