﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class goto_download_getmore_default
    Inherits BasePage

#Region "Variables/Keys"
    Private Const REDIRECT_BACK_URL As String = "{0}/goto/download/getmore/default.aspx?rnd={1}"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            If Not (IsPostBack) Then
                If (HasValidQueryStringParameters()) Then
                    MakeActivationRequest()
                End If
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        MakeActivationRequest()
    End Sub
#End Region

#Region "Page Methods"
    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("rnd") Is Nothing) Then
            Return False
        End If
        If (Session("Randomizer_Download_GetMore") Is Nothing) Then
            Return False
        End If
        If Not (String.Compare(Request.QueryString("rnd"), Session("Randomizer_Download_GetMore").ToString(), True) = 0) Then
            Return False
        End If
        Return True
    End Function

    Private Sub MakeActivationRequest()
        Session("Randomizer_Download_GetMore") = Functions.GeneratedGUID()
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir, Session("Randomizer_Download_GetMore").ToString())
        If Not (Me.ValidateUser()) Then
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()
        Session("Randomizer_Download_GetMore") = Nothing
        Session.Remove("Randomizer_Download_GetMore")

        Dim request As Request = New Request()
        request.ContactId = Int32.Parse(Session("ContactId").ToString())
        request.RequestTypeId = 12
        request.ObjectId = 916
        request.Remarks = "No Remarks for Download"
        request.PageId = 0

        Dim retVal As InsertReturnValue(Of Int32) = RequestRepository.InsertRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), request)
        If (retVal.Success) Then
            GenerateInternalEmail()
            Response.Redirect("/files/pdf/getmoreoutofyourscope.pdf")
        End If
    End Sub

    Private Sub GenerateInternalEmail()
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(String.Format("Download: {0}{0}Getting more out of your digital oscilloscope{0}{0}{1}", Environment.NewLine, Functions.EmailBodyClientData(Int32.Parse(Session("ContactId").ToString()), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())))
        Utilities.SendEmail(New List(Of String)({"kate.kaplan@teledynelecroy.com"}), "webmaster@teledynelecroy.com", "Getting more out of your digital oscilloscope", sb.ToString(), New List(Of String), New List(Of String)({"kate.kaplan@teledynelecroy.com"}))
    End Sub
#End Region
End Class