<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_eyedr_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding-left: 100px; padding-right: 100px; padding-top: 10px;" valign="top">
                    <div align="center">
                        <h1>Eye Doctor Equalized Receiver Emulation<br />and Virtual Probe Software Trial<br /></h1>
            <table border="0" cellpadding="0">
                <tr><td colspan="2"><font face="Verdana,Arial"><small><b>Scope Information</b></small></font><br /></td></tr>
                <tr id="trModels" runat="server" visible="false">
                    <td width="75">
                        <nobr><small><strong><font face="Verdana,Arial" color="#FF0000">*</font></strong><font face="Verdana,Arial">Scope Model</font></small></nobr>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlModel" runat="server" /><asp:CustomValidator ID="cvModel" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" /><asp:HiddenField ID="hdnSwOptions" runat="server" Value="1479" />
                    </td>
                </tr>
                <tr>
                    <td><nobr><small><strong><font face="Verdana,Arial" color="#ff0000">*</font></strong><font face="Verdana,Arial">Serial #</font></small></nobr></td>
                    <td><asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="14" Width="100" />&nbsp;<asp:CustomValidator ID="cvSerialNumber" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" /></td>
                </tr>
                <tr>
                    <td><small><strong><font face="Verdana,Arial" color="#ff0000">*</font></strong><font face="Verdana,Arial">ScopeID</font></small></td>
                    <td><asp:TextBox ID="txtScopeId" runat="server" MaxLength="9" Width="100" />&nbsp;<asp:CustomValidator ID="cvScopeId" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit Your Request" /><asp:HiddenField ID="sw" runat="server" Value="1479" /></td>
                </tr>
            </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlExists" runat="server" Visible="false">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div align="center">
                        <br /><br /><br /><b><font face="Verdana" size="2">The request for Eye Doctor software for that model and serial number had been already placed in the past. You can request only one demo key.<br /></font></b>
                        <br /><b><font face="Verdana" size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="request.asp">Click here to go to the demo request page</a><br /></font></b>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div align="center">
                        <br /><br /><br /><b><font face="Verdana" size="2">This promotion is exclusive to United States customers only<br /></font></b>
                        <br /><b><font face="Verdana" size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/default.asp">Click here to go to Teledyne LeCroy's home page</a><br /></font></b>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div align="center">
                        <br /><br /><br /><font face="verdana,arial,helvetica" size="2"><b>Eye Doctor Demo Request Confirmation.</b></font>
                        <font face="verdana,arial,helvetica" size="2">Thank you for your request</font><br /><br />
                        <font face="verdana,arial,helvetica" size="2">You will receive a confirmation email to the address provided on your registration within 48 hours.</font>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>