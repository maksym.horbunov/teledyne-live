﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class goto_eyedr_default
    Inherits BasePage

#Region "Variables/Keys"
    Private Const REDIRECT_BACK_URL As String = "{0}/goto/eyedr/default.aspx?rnd={1}"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Eye Doctor Demo"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindModelDropDown()

            If (HasValidQueryStringParameters()) Then
                MakeRequest()
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvModel_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvModel.ServerValidate
        args.IsValid = False
        If (ddlModel.SelectedIndex > 0) Then
            args.IsValid = True
        End If
    End Sub

    Private Sub cvSerialNumber_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs) Handles cvSerialNumber.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtSerialNumber.Text)) Then
            cvSerialNumber.ErrorMessage = "*required"
            Return
        End If
        If (txtSerialNumber.Text.Length <> 14) Then
            cvSerialNumber.ErrorMessage = "*invalid serial number"
            Return
        End If
        Dim regex As Regex = New Regex("[L,l][C,c][R,r][Y,y][0-9]{4}[A-Za-z]{1}[0-9]{5}$")
        If Not (regex.IsMatch(txtSerialNumber.Text)) Then
            cvSerialNumber.ErrorMessage = "*invalid serial number"
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvScopeId_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvScopeId.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtScopeId.Text)) Then
            cvScopeId.ErrorMessage = "*required"
            Return
        End If
        If (txtScopeId.Text.Length <> 9) Then
            cvScopeId.ErrorMessage = "*invalid scope id"
            Return
        End If
        Dim regex As Regex = New Regex("(\w{6}-\w{2})|[W,w][M,m][0][0][0][0][0][1]")
        If Not (regex.IsMatch(txtScopeId.Text)) Then
            cvScopeId.ErrorMessage = "*invalid scope id"
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        MakeRequest()
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindModelDropDown()
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [PRODUCT] WHERE [MODEL_SERIES] = 10 AND [DISABLED] = 'n' ORDER BY [SORT_ID]", New List(Of System.Data.SqlClient.SqlParameter)().ToArray()).ToList()
        If (products.Count > 0) Then
            For Each product As Product In products
                ddlModel.Items.Add(New ListItem(product.PartNumber, product.ProductId.ToString()))
            Next
            ddlModel.Items.Insert(0, New ListItem("Select model", String.Empty))
            trModels.Visible = True
        End If
    End Sub

    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("rnd") Is Nothing) Then
            Return False
        End If
        If (Session("Randomizer_EyeDr") Is Nothing) Then
            Return False
        End If
        If Not (String.Compare(Request.QueryString("rnd"), Session("Randomizer_EyeDr").ToString(), True) = 0) Then
            Return False
        End If
        Return True
    End Function

    Private Function ConvertCommaSeparatedStringToDictionary() As Dictionary(Of Int32, String)
        Dim retVal As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
        If Not (String.IsNullOrEmpty(hdnSwOptions.Value)) Then
            Dim options As String() = hdnSwOptions.Value.ToString().Split(",")
            For Each s As String In options
                If Not (retVal.ContainsKey(Int32.Parse(s))) Then
                    Dim partNumber As String = String.Empty
                    Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(s))
                    If Not (product Is Nothing) Then partNumber = product.PartNumber
                    retVal.Add(Int32.Parse(s), partNumber)
                End If
            Next
        End If
        Return retVal
    End Function

    Private Sub MakeRequest()
        Session("Randomizer_EyeDr") = Functions.GeneratedGUID()
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir, Session("Randomizer_EyeDr").ToString())
        If Not (Me.ValidateUser()) Then
            If (trModels.Visible) Then Session("SWKEY_Model") = ddlModel.SelectedValue
            Session("SWKEY_SN") = SQLStringWithOutSingleQuotes(txtSerialNumber.Text)
            Session("SWKEY_SCOPEID") = SQLStringWithOutSingleQuotes(txtScopeId.Text)
            Session("SWKEY_ARRAY") = ConvertCommaSeparatedStringToDictionary()
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()
        Session("Randomizer_EyeDr") = Nothing
        Session.Remove("Randomizer_EyeDr")

        If Not (Functions.CheckcustomerfromLNA(Session("Country"))) Then
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = False
            pnlExists.Visible = False
            pnlError.Visible = True
            Return
        End If

        Dim requests As List(Of Request) = RequestRepository.FetchRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT * FROM [REQUEST] WHERE [REQUEST_TYPE_ID] = 17 AND [OBJECT_ID] = @MODEL AND [SERIAL_NUM] = @SERIALNUMBER",
                                                                          New List(Of System.Data.SqlClient.SqlParameter)({New System.Data.SqlClient.SqlParameter("@MODEL", ""), New System.Data.SqlClient.SqlParameter("@SERIALNUMBER", "")}).ToArray())
        If (requests.Count > 0) Then
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = False
            pnlError.Visible = False
            pnlExists.Visible = True
        End If

        Dim request As Request = New Request()
        request.ContactId = Int32.Parse(Session("ContactId").ToString())
        request.RequestTypeId = 17
        If (trModels.Visible) Then
            request.ObjectId = Int32.Parse(ddlModel.SelectedValue)
        Else
            request.ObjectId = 0
        End If
        request.SerialNum = SQLStringWithOutSingleQuotes(txtSerialNumber.Text)
        request.ScopeId = SQLStringWithOutSingleQuotes(txtScopeId.Text)
        request.Remarks = "Eye Doctor Software Trial Offer"
        request.PageId = 0

        Dim retVal As InsertReturnValue(Of Int32) = RequestRepository.InsertRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), request)
        If (retVal.Success) Then
            Dim swOptions As Dictionary(Of Int32, String) = ConvertCommaSeparatedStringToDictionary()
            If (swOptions.Count > 0) Then
                Dim ide As IDictionaryEnumerator = swOptions.GetEnumerator()
                While (ide.MoveNext())
                    Dim requestDetail As RequestDetails = New RequestDetails()
                    requestDetail.RequestId = retVal.PrimaryKeyId
                    requestDetail.TableId = 15
                    requestDetail.IdInTable = Int32.Parse(ide.Key)
                    RequestDetailRepository.InsertRequestDetail(ConfigurationManager.AppSettings("ConnectionString").ToString(), requestDetail)
                End While
                GenerateInternalEmail(GetEmailBody(swOptions.Select(Function(x) x.Value).ToList()))
            End If
            pnlSubmit.Visible = False
            pnlError.Visible = False
            pnlExists.Visible = False
            pnlSubmitted.Visible = True
        End If
    End Sub

    Private Function GetEmailBody(ByVal swOptions As List(Of String)) As String
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(String.Format("Eye Doctor Demo Request: {0}{0}{1}", Environment.NewLine, Functions.EmailBodyClientData(Int32.Parse(Session("ContactID")), Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())))
        If (trModels.Visible) Then
            Dim partNumber As String = String.Empty
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(ddlModel.SelectedValue))
            If Not (product Is Nothing) Then partNumber = product.PartNumber
            sb.Append(String.Format("Scope Model: {0}{1}", partNumber, Environment.NewLine))
        End If
        sb.Append(String.Format("Serial Number: {0}{1}", Session("SWKEY_SN"), Environment.NewLine))
        sb.Append(String.Format("ScopeID: {0}{1}{1}", Session("SWKEY_SCOPEID"), Environment.NewLine))
        sb.Append(String.Format("Requested options:{0}", Environment.NewLine))
        sb.Append(String.Join(Environment.NewLine, swOptions))
        Return sb.ToString()
    End Function

    Private Sub GenerateInternalEmail(ByVal emailBody As String)
        Utilities.SendEmail(New List(Of String)({"hilary.lustig@teledynelecroy.com", "liz.toronto@teledynelecroy.com"}), "webmaster@teledynelecroy.com", "Eye Doctor Software Trial", emailBody, New List(Of String), New List(Of String)({"kate.kaplan@teledynelecroy.com"}))
    End Sub
#End Region
End Class