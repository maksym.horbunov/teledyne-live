<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_sparameterviewer_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit" Visible="true">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td width="135px" valign="top">&nbsp;</td>
                <td width="650px" valign="top">
                    <img src="/images/sparam_01.jpg" alt="S Parameter Viewer" align="right" border="0" title="S Parameter Viewer" hspace="10px" />
                    <h1>S-Parameter Viewer Activation</h1>
                    <p>Thank you for downloading SParamViewer from Teledyne LeCroy. Please click on the button below to complete your software activation. You will be directed to complete a registration form if you do not already have a teledynelecroy.com account.</p>
                    <p>The activation code will be sent to the email address you provide.</p>
                    <p><asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Activate SParamViewer" /></p>
                    <p>More S-Parameter Viewer information can be found here: <a href="/support/softwaredownload/sparamviewer.aspx?capid=106&mid=533&smid=">SParamViewer</a>&nbsp;<img src="/images/icons/icons_lft_nav_arrow.gif" /></p>
                </td>
                <td width="135px" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" valign="top">&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td width="135px" valign="top">&nbsp;</td>
                <td width="650px" valign="top">
                    <img src="/images/sparam_activation.gif" width="369" height="195" alt="SParamViewer Activation" align="right" />
                    <h1>S-Parameter Viewer Activation</h1>
                    <p>Thank you for your Request!</p>
                    <p>The activation code has been sent to the email address you provided.</p>
                    <p>More S-Parameter Viewer information can be found here: <a href="/support/softwaredownload/sparamviewer.aspx?capid=106&mid=533&smid=">SParamViewer</a>&nbsp;<img src="/images/icons/icons_lft_nav_arrow.gif" /></p>
                </td>
                <td width="135px" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" valign="top">&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>