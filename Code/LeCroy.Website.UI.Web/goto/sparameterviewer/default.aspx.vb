﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class goto_sparameterviewer_default
    Inherits BasePage

#Region "Variables/Keys"
    Private Const REDIRECT_BACK_URL As String = "{0}/goto/sparameterviewer/default.aspx?rnd={1}"
    Private Const REDIRECT_REGISTRATION_URL As String = "{0}/support/user/"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - S-Parameter Viewer"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            pnlSubmitted.Visible = False
            pnlSubmit.Visible = True

            If (HasValidQueryStringParameters()) Then
                MakeActivationRequest()
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        MakeActivationRequest()
    End Sub
#End Region

#Region "Page Methods"
    Private Function HasValidQueryStringParameters() As Boolean
        If (Request.QueryString("rnd") Is Nothing) Then
            Return False
        End If
        If (Session("Randomizer_SParam") Is Nothing) Then
            Return False
        End If
        If Not (String.Compare(Request.QueryString("rnd"), Session("Randomizer_SParam").ToString(), True) = 0) Then
            Return False
        End If
        Return True
    End Function

    Private Sub MakeActivationRequest()
        Session("Randomizer_SParam") = Functions.GeneratedGUID()
        Session("RedirectTo") = String.Format(REDIRECT_BACK_URL, rootDir, Session("Randomizer_SParam").ToString())
        If Not (Me.ValidateUser()) Then
            Response.Redirect(String.Format(REDIRECT_REGISTRATION_URL, rootDir))
        End If
        Me.ValidateUserNoRedir()
        Session("Randomizer_SParam") = Nothing
        Session.Remove("Randomizer_SParam")

        Dim request As Request = New Request()
        request.ContactId = Int32.Parse(Session("ContactId").ToString())
        request.RequestTypeId = 8
        request.ObjectId = 648
        request.Remarks = "No Remarks for Download"
        request.PageId = 0

        Dim retVal As InsertReturnValue(Of Int32) = RequestRepository.InsertRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), request)
        If (retVal.Success) Then
            FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 11, 1033, Session("Email").ToString(), New Dictionary(Of String, String) From {{"<#SparamUrl#>", String.Format("{0}/support/softwaredownload/sparamviewer.aspx?capid=106&mid=533&smid=", ConfigurationManager.AppSettings("DefaultDomain"))}})
            pnlSubmit.Visible = False
            pnlSubmitted.Visible = True
        End If
    End Sub
#End Region
End Class