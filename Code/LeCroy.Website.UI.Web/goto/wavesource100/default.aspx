﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_wavesource100_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <script language="JavaScript">
        function openWinScreen(URL) {
            aWindow = window.open(URL, "thewindow", "alywaysRaised=yes,width=800,height=600,topmargin=0,leftmargin=0,bgcolor=white,scrollbars=yes,resizable=yes");
        }
    </script>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td style="padding-left: 100px; padding-right: 100px; padding-top: 10px;" valign="top">
                <span style="color: #0000ff; font-face: Verdana; font-size: 26px; font-weight: bold;">WaveSource 100</span>
                <hr size="1px" color="#000000" width="100%" />
                <p align="center"><a href="Javascript:openWinScreen('/images/wavesource100_lg.jpg');"><img src="/images/wavesource100_sm.jpg" border="0" /></a></p>
                <span style="color: #0000ff; font-face: Verdana; font-size: 20px; font-weight: bold;">WaveSource 100</span>
                <p>An oscilloscope demonstration and evaluation signal source. Generates the following signals:<br />Communication Protocols:</p>
                <ul style="padding-left: 40px;">
                    <li>I<sup>2</sup>C</li>
                    <li>SPI</li>
                    <li>UART/RS232</li>
                </ul>
                <p>Troublesome signal anomalies that oscilloscopes should be able to capture and analyze:</p>
                <ul style="padding-left: 40px;">
                    <li>Runts</li>
                    <li>Glitches - Beyond needing to know details of expected glitches for trigger circuits, oscilloscope tools should quickly and easily scan for, and provide insight to, unexpected signal events</li>
                    <li>Jitter</li>
                    <li>Slow Rising Edge - Symbolic of circuit signal reflections</li>
                    <li>Non Monotonic Edges</li>
                    <li>Deterministic Edge Widths – Oscilloscopes tools should provide insight to not only each of their values but provide insight to how frequent and their occurrence in time</li>
                    <li>Signal Bursts Separated by Long Delay</li>
                </ul>
                <p><img src="/images/Icons/IconPDF.gif" border="0" /><a href="/files/pdf/wavesource_100_quick_ref_guide.pdf">WaveSource 100 Quick Ref Guide</a></p>
                <p align="center"><a href="Javascript:openWinScreen('/images/wavesource100_drawing_lg.jpg');"><img src="/images/wavesource100_drawing_sm.jpg" border="0" /></a></p>
                <p align="center">WaveSource 100 Connection Diagram</p>
            </td>
        </tr>
    </table>
</asp:Content>