<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_wrxi_welcome_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
<div style="overflow: inherit;">
    <a name="TopofPage"></a>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td width="100%" valign="top" align="left">
                <table align="center" border="0" cellspacing="0" cellpadding="8">
                    <tr>
                        <td><a href="default.aspx#1">English</a></td>
                        <td><a href="default.aspx#8">Japanese</a></td>
                        <td><a href="default.aspx#2">Chinese-simp</a></td>
                        <td><a href="default.aspx#3">German</a></td>
                        <td><a href="default.aspx#4">Italian</a></td>
                        <td><a href="default.aspx#5">French</a></td>
                        <td><a href="default.aspx#6">Russian</a></td>
                        <td><a href="default.aspx#7">Chinese-trad</a></td>
                        <td><a href="default.aspx#9">Korean</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <hr noshade size="1" color="#1671CC" />
                <a name="1"></a><img src="/images/wrxi_welcome.jpg" width="58" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Thank you for purchasing a WaveRunner Xi series oscilloscope. We know you'll be delighted to work with the first oscilloscope to offer great performance (5 GS/s sample rate and 2 Mpts of memory) with a big, bright 10.4" display and a small footprint. What you need without compromises!
                    <br /><br />Multi-Language Capable: You can easily set your WaveRunner Xi oscilloscope's user interface to display in your local language. Touch the icon during bootup, or select a language in Utilities/Preferences.
                    <br /><br />Product Registration: We urge you to register your new scope. Registering will allow you to receive essential product software, instruction manual and service updates, and more. To register please visit <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Contacting Teledyne LeCroy: Have any comments, questions or concerns about your new oscilloscope? Feel free to contact our Product Manager directly at <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a>
                    <br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="8"></a><img src="/images/wrxi_welcome_ja.jpg" width="48" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><img src="/images/wrxi_text_ja_1.jpg">
                    <br /><a href="https://teledynelecroy.com/japan/support/registerscope/default.asp">https://teledynelecroy.com/japan/support/registerscope/default.asp</a>
                    <br /><br /><img src="/images/wrxi_text_ja_2.jpg"><br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="2"></a><img src="/images/wrxi_welcome_ch_s.jpg" width="37" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><img src="/images/wrxi_text_ch_s_1.jpg">
                    <br /><a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br /><img src="/images/wrxi_text_ch_s_2.jpg">
                    <br /><a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="3"></a><img src="/images/wrxi_welcome_de.jpg" width="63" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Vielen Dank, dass Sie sich f&#252;r ein Oszilloskop der WaveRunner Xi Serie entschieden haben. Sie werden begeistert sein von der Kombination aus Leistungsf&#228;higkeit (5 GS/s Abtastrate/Kanal und 2 M-Punkte Speicher/Kanal standard), dem grossen kontrastreichen 26,4cm Display und der kleinen Stellfl&#228;che. Endlich ein Oszilloskop ohne Kompromisse!
                    <br /><br />WaveRunner - das Sprachtalent: Sie k&#246;nnen die Bedienoberfl&#228;che des WaveRunner Xi in Ihrer Landessprache anzeigen lassen. Dr&#252;cken Sie auf den ICON beim Hochfahren oder w&#228;hlen Sie die entsprechende Sprache unter Utilities/Einstellung.
                    <br /><br />Produkt Registrierung: Wir empfehlen, Ihr Produkt registrieren zu lassen. Dadurch erhalten Sie automatisch u.a. Software Aktualisierungen, Bedienhinweise und Service Updates. Um Ihr Ger&#228;t zu registrieren, besuchen Sie <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Kontakt zu Teledyne LeCroy: Haben Sie Kommentare, Anregungen oder Fragen &#252;ber Ihr neues Oszilloskop? Kontaktieren Sie unseren Produkt Manager in Englisch direkt &#252;ber <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="4"></a><img src="/images/wrxi_welcome_it.jpg" width="57" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Grazie per aver acquistato un oscilloscopio della serie Waverunner Xi. Sappiamo che sar&#224; entusiasta di lavorare con il primo oscilloscopio che offre prestazioni elevate (campionamento 5 GS/s e memoria 2Mpunti) con un ampio e brilante display da 10,4" ed un ingombro minimo. Tutto ci&#242; che le serve senza alcun compromesso!
                    <br /><br />Multi lingue: pu&#242; facilmente impostare l'interfaccia utente del suo Waverunner Xi nella sua lingua. Selezioni l'icona relativa durante il "Boot" oppure utilizzi la voce "Utilit&#224;/Preferenze" del men&#249;.
                    <br /><br />Registrazione: le consigliamo di registrare il suo nuovo oscilloscopio. Questo le consentir&#224; di ricevere aggiornamenti relativi al software, al manuale d'uso, all'assistenza e altro. Visiti quindi il sito <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Contatti Teledyne LeCroy: per qualsiasi necessit&#224; o richiesta di informazioni, relative al suo nuovo strumento, contatti liberamente il nostro Responsabile di Prodotto direttamente su <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="6"></a><img src="/images/wrxi_welcome_fr.jpg" width="54" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Merci pour votre achat de cet oscilloscope de la s&#233;rie WaveRunner Xi. Nous savons que vous serez enchant&#233; de travailler avec le premier oscilloscope offrant une telle performance (Echantillonnage de 5GEch/s et 2Mpts de m&#233;moire) avec un grand &#233;cran de 10.4" et un encombrement r&#233;duit. Tout ce dont vous avez besoin sans compromis!
                    <br /><br />Multi-Langue: vous pouvez facilement choisir l'interface utilisateur de votre oscilloscope WaveRunner Xi pour un affichage dans votre langue. Touchez l'ic&#244;ne pendant la mise en route, ou choisissez une langue dans Utilities/Preferences.
                    <br /><br />Enregistrement de produit: nous vous invitons &#224; enregistrer votre nouvel oscilloscope. L'enregistrement vous permettra de recevoir les mises &#224; jour de logiciel, de manuel d'instruction et de service, et autre. Pour s'enregistrer, visitez <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Entrer en contact avec Teledyne LeCroy: avez-vous des commentaires, questions ou soucis concernant votre nouvel oscilloscope? Veuillez contacter notre chef de produit directement &#224; <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="7"></a><img src="/images/wrxi_welcome_ch_t.jpg" width="35" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><img src="/images/wrxi_text_ch_t_1.jpg">
                    <br /><a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br /><img src="/images/wrxi_text_ch_t_2.jpg">
                    <br /><br />
                </font>
                <hr noshade size="1" color="#1671CC" />
                <a name="9"></a><img src="/images/wrxi_welcome_kr.jpg" width="35" height="18">
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><img src="/images/wrxi_text_kr_1.jpg">
                    <br /><a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br /><img src="/images/wrxi_text_kr_2.jpg"><br /><br />
                </font>
            </td>
        </tr>
    </table>
</div>
</asp:Content>