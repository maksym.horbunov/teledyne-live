<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/masterpage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.goto_wsxs_welcome_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <a name="TopofPage"></a>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td align="left" valign="top" width="100%">
                <table align="center" border="0" cellspacing="0" cellpadding="8">
                    <tr>
                        <td><a href="default.aspx#1">English</a></td>
                        <td><a href="default.aspx#8">Japanese</a></td>
                        <td><a href="default.aspx#2">Chinese-simp</a></td>
                        <td><a href="default.aspx#3">German</a></td>
                        <td><a href="default.aspx#7">Chinese-trad</a></td>
                        <td><a href="default.aspx#10">Spanish</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <hr noshade color="#1671CC" size="1" />
                <a name="1"></a><img src="/images/wsxs_welcome.jpg" width=58 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Thank you for your purchase of this WaveSurfer Xs series oscilloscope offering great value and performance with big display and small footprint.
                    <br /><br />Multi-Language: Touch the icon during bootup, or select a language in Utilities/Preferences.
                    <br /><br />Product Registration: We urge you to register your new scope.  Registering will allow you to receive essential product software, instruction manual and service updates, and more. To register please visit <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Contacting Teledyne LeCroy: Have any comments, questions or concerns about your new oscilloscope?  Feel free to contact our Product Manager directly at <a href="mailto:WaveSurferXs@teledynelecroy.com">WaveSurferXs@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade color="#1671CC" size="1" />
                <a name="8"></a><img src="/images/wsxs_welcome_ja.jpg" width=48 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><IMG SRC="/images/wsxs_text_ja_1.jpg">
                    <br /><a href="https://teledynelecroy.com/japan/support/registerscope/default.asp">https://teledynelecroy.com/japan/support/registerscope/default.asp</a>
                    <br /><br /><IMG SRC="/images/wsxs_text_ja_2.jpg"><br /><br />
                </font>
                <hr noshade color="#1671CC" size="1" />
                <a name="2"></a><img src="/images/wsxs_welcome_ch_s.jpg" width=37 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><IMG SRC="/images/wsxs_text_ch_s_1.jpg">
                    <br /><a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br /><IMG SRC="/images/wsxs_text_ch_s_2.jpg">
                    <br /><a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a>
                    <br /><br /><IMG SRC="/images/wsxs_text_ch_s_3.jpg"><br /><br />
                </font>
                <hr noshade color="#1671CC" size="1" />
                <a name="3"></a><img src="/images/wsxs_welcome_de.jpg" width=63 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Vielen Dank, da&#223; Sie sich f&#252;r einen WaveSurfer Xs entschieden haben, der Ihnen neben gro&#223;em Bildschirm und kleiner Stellfl&#228;che ein hervorragendes Preis-/Leistungsverh&#228;ltnis bietet.
                    <br /><br />Mehrsprachige Bedienung: Dr&#252;cken Sie den Icon beim Hochfahren, oder w&#228;hlen Sie die Sprache Ihrer Wahl unter Utilities/Preferences.
                    <br /><br />Produktregistrierung: Um Ihr Ger&#228;t zu registrieren, besuchen Sie <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />Haben Sie Fragen oder Anregungen zu Ihrem Oszilloskop? Bitte wenden Sie sich direkt an den Produkt Manager &#252;ber <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade color="#1671CC" size="1" />
                <a name="10"></a><img src="/images/wsxs_welcome_sp.jpg" width=63 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br />Gracias por adquirir este osciloscopio de la serie WaveSurfer Xs de altas prestaciones y rendimiento, con gran pantalla y tama&#241;o peque&#241;o.
                    <br /><br />Multi-idioma: Toque el icono durante el arranque o seleccione el idioma en Utilidades/Preferencias.
                    <br /><br />Registro del equipo: Por favor, para registrarlo vaya a <a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br />&#191;Tiene alg&#250;n comentario, pregunta o duda acerca de su Nuevo osciloscopio? Por favor, contacte libremente con nuestro Jefe de Producto en <a href="mailto:WaveRunnerXi@teledynelecroy.com">WaveRunnerXi@teledynelecroy.com </a><br /><br />
                </font>
                <hr noshade color="#1671CC" size="1" />
                <a name="7"></a><img src="/images/wsxs_welcome_ch_t.jpg" width=35 height=18>
                <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <br /><br /><IMG SRC="/images/wsxs_text_ch_t_1.jpg">
                    <br /><a href="https://teledynelecroy.com/register">https://teledynelecroy.com/register</a>
                    <br /><br /><IMG SRC="/images/wsxs_text_ch_t_2.jpg"><br /><br />
                </font>
            </td>
        </tr>
    </table>
</asp:Content>