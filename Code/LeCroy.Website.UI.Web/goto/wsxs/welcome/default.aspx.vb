﻿Public Class goto_wsxs_welcome_default
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - WaveSurfer Xs Series Oscilloscope"
    End Sub
End Class