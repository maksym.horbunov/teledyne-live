
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../img/favicon.ico">

    <title>Teledyne LeCroy &ndash; 12-Bit High Definition Technology</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="../css/carousel.css" rel="stylesheet">
    <link href="../css/theme.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script>
    </script>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="../" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="../12bit/">12-bit High Definition Technology</a></li>
                <li><a href="../hdo/">High Definition Oscilloscopes</a></li>
                <li><a href="../videos/">Videos &amp;Tech Briefs</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading top">HD4096. <span class="text-muted">High Definition Technology.</span></h2>
          <p class="lead">HD4096 high definition technology consists of high 
sample rate 12-bit ADCs, high signal-to-noise ratio 
amplifiers and a low-noise system architecture. This 
technology enables high definition oscilloscopes to 
capture and display signals of up to 1 GHz with high 
sample rate and 16 times more resolution than other 
oscilloscopes.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" alt="500x500" src="../img/why12bit_0.png">
        </div>
      </div>
      <hr class="featurette-divider">
      <div class="row featurette">
        <div class="col-md-12 horizontal">
          <h2 class="featurette-heading top">16x More Resolution. <span class="text-muted">16x Closer to Perfect.</span></h2>
          <p class="lead">12-bits of vertical resolution provides sixteen times more resolution than 8-bits. The 4096 discrete 
levels reduce the quantization error. Signals captured with lower resolution oscilloscopes have a higher 
level of quantization error resulting in less accurate waveforms on the display. Signals captured on an 
oscilloscope with 12-bit HD4096 technology are accurately displayed with minimal quantization error.</p>
        </div>
        <div class="horizontal-group">
          <div class="col-md-5">
            <img class="featurette-image img-responsive" alt="500x500" src="../img/why12bit_1.png">
          </div>
          <div class="col-md-5">
            <img class="featurette-image img-responsive" alt="500x500" src="../img/why12bit_2.png">
          </div>
          <div class="col-md-2">
            <img class="featurette-image img-responsive key" alt="500x500" src="../img/why12bit_3.png">
          </div>
        </div>
      </div>
      <hr class="featurette-divider">
      <div class="row featurette">
        <div class="col-md-3 horizontal">
          <table border="0">
              <tr>
                  <td>
                      <div class="circle_green"><p>A</p>
                         </div>
                  </td>
              </tr>
          </table>
          <h2 class=" top">Clean, Crisp Waveforms</h2>
          <p class="lead">Waveforms captured and displayed on oscilloscopes with HD4096 technology are cleaner and crisper.</p>
        </div>
        <div class="col-md-3 col-md-offset-1 horizontal">
          <table border="0">
              <tr>
                  <td>
                      <div class="circle_green"><p>B</p>
                         </div>
                  </td>
              </tr>
          </table>
          <h2 class=" top">More Signal Details</h2>
          <p class="lead">Signal details often lost in the noise are clearly visible and easy to distinguish.</p>
        </div>
        <div class="col-md-3 col-md-offset-1 horizontal">
          <table border="0">
              <tr>
                  <td>
                      <div class="circle_green"><p>C</p>
                         </div>
                  </td>
              </tr>
          </table>
          <h2 class=" top">Unmatched Measurement Precision</h2>
          <p class="lead">HD4096 enables oscilloscopes to deliver unmatched measurement precision for improved debug and analysis.</p>
        </div>
        <div class="horizontal-group">
          <div class="col-md-6">
            <img class="featurette-image img-responsive" alt="500x500" src="../img/why12bit_4.png">
          </div>
          <div class="col-md-6">
            <img class="featurette-image img-responsive" alt="500x500" src="../img/why12bit_5.png">
          </div>
        </div>
      </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2014&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
      Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
