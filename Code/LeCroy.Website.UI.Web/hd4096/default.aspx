﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_default" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>HD4096 High Definition Technology | Teledyne LeCroy</title>

 <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
  <style type="text/css">
    table.centered tbody tr td{padding: 0; line-height: 38px;}
  </style>
</head>

<body>
  <div class="container">
    <nav>
      <div class="nav-wrapper white">
        <a href="/" class="brand-logo"><img src="/images/tl_weblogo_blkblue_189x30.png"></a>
          <ul class="right hide-on-med-and-down">
             <li><a class="dropdown-button" href="/hdo/" data-alignment="right" data-activates="dropdown1" data-beloworigin="true">Explore HDO Family <img src="/images/hdo-family-2019.png" style="vertical-align:middle;"></a></li>
          </ul>
          <ul class="left hide-on-med-and-down" style="margin-left: 200px;">
            <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdownp" data-beloworigin="true">Products</a></li>
            <li class="dropdown"><a class="dropdown-button" href="#!" data-activates="dropdowns" data-beloworigin="true">Serial Data</a></li>
            <li class="dropdown"><a class="dropdown-button" href="/support/" data-activates="dropdownu" data-beloworigin="true">Support</a></li>
          </ul>
      </div>
    </nav>
  </div>

<!-- Dropdown Structure -->
<div id="dropdown1" class="dropdown-content dropdown-content-table">
  <table class="centered responsive-table striped">
    <tr style="background-color: rgba(92, 193, 81, 0.05)">
      <td>&nbsp;</td>
      <td><a href="/oscilloscope/wavesurfer-4000hd-oscilloscopes"><img src="/images/ws4000hd-table.png" width="100" height="100"></a></td>
      <td><a href="/oscilloscope/hdo6000a-high-definition-oscilloscopes"><img src="/images/hdo6000a-table.png" width="100" height="100"></a></td>
      <td><a href="/oscilloscope/waverunner-8000hd-oscilloscopes"><img src="/images/wr8000hd-table.png" width="100" height="100"></a></td>
      <td><a href="/oscilloscope/wavepro-hd-oscilloscope"><img src="/images/wphd-table.png" width="100" height="100"></a></td>
    </tr>
    <tr style="background-color: rgba(92, 193, 81, 1); color: white; border-bottom-color: white;">
      <td>&nbsp;</td>
      <td>WaveSurfer 4000HD</td>
      <td>HDO6000A</td>
      <td>WaveRunner 8000HD</td>
      <td>WavePro HD</td>
    </tr>
    <tr style="background-color: rgba(92, 193, 81, 0.15)">
      <td><b>HD Technology</b></td>
      <td>HD4096</td>
      <td>HD4096</td>
      <td>HD4096</td>
      <td>HD4096</td>
    </tr>
    <tr style="background-color: rgba(92, 193, 81, 0.05)">
      <td><b>Bandwidth</b></td>
      <td>200 MHz - 1 GHz</td>
      <td>350 MHz - 1 GHz</td>
      <td>350 MHz - 2 GHz</td>
      <td>2.5 GHz - 8 GHz</td>
    </tr>
    <tr style="background-color: rgba(92, 193, 81, 0.15)">
      <td><b>Input Channels</b></td>
      <td>4</td>
      <td>4</td>
      <td>8</td>
      <td>4</td>
    </tr>
    <tr style="background-color: rgba(92, 193, 81, 0.05)">
      <td></td>
      <td><br /><a href="/oscilloscope/wavesurfer-4000hd-oscilloscopes" class="waves-effect waves-light btn green accent-4 white-text">WaveSurfer 4000HD &raquo;</a><br />&nbsp;</td>
      <td><br /><a href="/oscilloscope/hdo6000a-high-definition-oscilloscopes" class="waves-effect waves-light btn green accent-4 white-text">HDO6000A &raquo;</a><br />&nbsp;</td>
      <td><br /><a href="/oscilloscope/waverunner-8000hd-oscilloscopes" class="waves-effect waves-light btn green accent-4 white-text">WaveRunner 8000HD &raquo;</a><br />&nbsp;</td>
      <td><br /><a href="/oscilloscope/wavepro-hd-oscilloscope" class="waves-effect waves-light btn green accent-4 white-text">WavePro HD &raquo;</a><br />&nbsp;</td>
    </tr>
  </table>
</div>

<ul id="dropdownp" class="dropdown-content">
  <li><a href="/oscilloscope/">Oscilloscopes</a></li>
  <li><a href="/probes/">Oscilloscope Probes</a></li>
  <li><a href="/protocolanalyzer/">Protocol Analyzers</a></li>
  <li><a href="/pert3/pertphoenix.aspx">PeRT</a></li>
  <li><a href="/waveformgenerators/">Waveform Generators</a></li>
  <li>
      <a href="/tdr-and-s-parameters/">TDR and S-Parameters</a>
    </li>
      <li><a href="/spectrum-analyzers/">Spectrum Analyzers</a></li>
<li><a href="/power-supplies/">Power Supplies</a></li>
 <li><a href="/digital-multimeters/">Digital Multimeters</a></li>

</ul>
  <ul id="dropdowns" class="dropdown-content">
  <li><a href="/serialdata/">Serial Data Standards</a></li>
  <li><a href="/sdaiii/">Serial Data Analysis</a></li>
</ul>
<ul id="dropdownu" class="dropdown-content">
  <li><a href="/support/techlib/">Tech Library</a></li>
  <li><a href="/support/service.aspx">Instrument Service</a></li>
  <li><a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a></li>
  <li><a href="/support/dsosecurity.aspx">Oscilloscope Security</a></li>
  <li><a href="/support/register/">Product Registration</a></li>
  <li><a href="/support/softwaredownload/">Software Downloads</a></li>
  <li><a href="/support/techhelp/">Technical Help</a></li>
  <li><a href="/support/requestinfo/">Request Info</a></li>
  <li><a href="/support/rohs.aspx">RoHS &amp; WEEE</a></li>
  <li><a href="/events/">Events &amp; Training</a></li>
  <li><a href="/support/training.aspx">Training</a></li>
  <li><a href="/support/user/userprofile.aspx">Update Profile</a></li>
  <li><a href="/support/contact/">Contact Us</a></li>
</ul>
<!--end of dropwdowns -->

    
      <div id="index-banner" class="container">
      <div class="section">
      <div class="row">
        <div class="col l6 m8 s12"><p class="h1 green-text text-accent-4">HD4096<br />High Definition Technology.</p>
            <p class="h3 black-text" style="margin-right: 15px;">HD4096 high definition technology consists of high sample rate 12-bit ADCs, high signal-to-noise ratio amplifiers and a low-noise system architecture. This technology enables <a href="/hdo/">high definition oscilloscopes</a> to capture and display signals of up to 8 GHz with high sample rate and 16 times more resolution than other oscilloscopes.</p>
        <p><span class="wistia_embed wistia_async_pdj1cnubp8 popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:183px;position:relative;width:325px">&nbsp;</span></p>
        </div>
        <div class="col l6 m8 s12"><img class="right responsive-img" src="/images/hd4096-itransport.png" alt="HD4096 High Definition Technology"></div>
      </div>
      </div>
    </div>



<div class="clearfix"></div>
  <div class="wrap">
<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 center">
            <h3 class="green-text text-accent-4">16x More Resolution. <span class="green-text text-accent-4">16x Closer to Perfect.</span></h3>
<p>HD4096 technology provides 12 bits of vertical resolution with 16x more resolution compared to conventional 8-bit oscilloscopes. The 4096 discrete vertical levels reduce the quantization error compared to 256 vertical levels. This improves the accuracy and precision of the signal capture and increases measurement confidence.</p>
</div>  
    <div class="center"><img class="materialboxed responsive-img" style="display:inline-block; width: 50%;" src="/images/hdo-graphic-key.jpg"></div>  
    </div>
    </div>
      
  </div>
</div>



</div>
<div class="clearfix"></div>
<div class="container">
  <div class="section">
    <div class="row">
    <div class="col s4"><div class="card-panel"><h5><button class="btn-floating btn green">A</button> Clean, Crisp Waveforms</h5><p>Waveforms captured and displayed on oscilloscopes with HD4096 technology are cleaner and crisper.</p></div></div>
    <div class="col s4"><div class="card-panel"><h5><button class="btn-floating btn green">B</button> More Signal Details</h5><p>Signal details often lost in the noise are clearly visible and easy to distinguish.</p></div></div>
    <div class="col s4"><div class="card-panel"><h5><button class="btn-floating btn green">C</button> Unmatched Measurement Precision</h5><p>HD4096 enables oscilloscopes to deliver unmatched measurement precision for improved debug and analysis.</p></div></div>
    </div>
        <div class="row">
    <div class="col s6"><img class="materialboxed responsive-img" src="https://teledynelecroy.com/hd4096/img/why12bit_4.png"></div>
    <div class="col s6"><img class="materialboxed responsive-img" src="https://teledynelecroy.com/hd4096/img/why12bit_5.png"></div>
    </div>
  </div>
</div>




  






<footer class="page-footer white">
      <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2019&nbsp;Teledyne LeCroy</p>

    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
      </div>
  </footer>


  <!--  Scripts-->
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/init.js"></script>
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
      <script>
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>

  </body>
</html>
