<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_hdo_default" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../img/favicon.ico">

    <title>Teledyne LeCroy &ndash; High Definition Oscilloscopes</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="../css/carousel.css" rel="stylesheet">
    <link href="../css/theme.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <!--<script>
    $(document).ready(function(){

    $(".slidingDiv").hide();
    $(".show_hide").show();

    $('.show_hide').toggle(function(){
         $("#plus").text("Hide Price Comparisons");
         $(".slidingDiv").slideDown(200, function() {
            $('html, body').delay('200').animate({
            scrollTop: $(this).offset().top
            }, 200);
          });

           },function(){
               $("#plus").text("Show Price Comparisons");
               $(".slidingDiv").slideUp(200, function() {
                  $('html, body').delay('200').animate({
                  scrollTop: $(this).offset().top - 111
                  }, 200);
          });
       });

      });
    </script>-->
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="../" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="../12bit/">12-bit High Definition Technology</a></li>
                <li><a href="../hdo/">High Definition Oscilloscopes</a></li>
                <li><a href="../videos/">Videos &amp;Tech Briefs</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading middle">Debug in High Definition <!--  HD4096 --></h2>
          <p class="lead">Oscilloscopes with HD4096 have a variety of benefits that allow the user to debug in high definition. Waveforms displayed by high definition oscilloscopes are cleaner and crisper. More signal details can be seen and measured; these measurements are made with unmatched precision resulting in better test results and shorter debug time.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" alt="Debug in High Definition with HD4096" style="max-height:275px;" src="../img/why12bit_hdo.png">
        </div>
      </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered comparison">
            <tr>
              <td valign="bottom">&nbsp;</td>
              <td colspan="2" valign="bottom"><img src="../img/hdo4000-table.png" class="img-responsive" alt="" style="max-height:175px;"/></td>
              <td colspan="2" valign="bottom"><img src="../img/hdo6000-table.png" class="img-responsive" alt="" style="max-height:175px;"/></td>
              <td colspan="2" valign="bottom"><img src="../img/hdo8000-table.png" class="img-responsive" alt="" style="max-height:175px;"/></td>
            </tr>
            <tr class="centered">
              <td>&nbsp;</td>
              <td colspan="2"><strong>HDO4000</strong></td>
              <td colspan="2"><strong>HDO6000</strong></td>
              <td colspan="2"><strong>HDO8000</strong></td>
            </tr>
            <tr>
              <td>Bandwidth</td>
              <td colspan="2">200 MHz - 1 GHz</td>
              <td colspan="2">350 MHz - 1 GHz</td>
              <td colspan="2">350 MHz - 1 GHz</td>
            </tr>
            <tr>
              <td>Channels</td>
              <td colspan="2">2, 4 Channels</td>
              <td colspan="2">4 Channels</td>
              <td colspan="2">8 Channels</td>
            </tr>
            <tr>
              <td>Resolution</td>
              <td colspan="2">12 bit resolution</td>
              <td colspan="2">12 bit resolution</td>
              <td colspan="2">12 bit resolution</td>
            </tr>
            <tr>
              <td>Sample Rate and Memory</td>
              <td colspan="2">2.5 GS/s, 12.5 Mpts/ch<br>50 Mpts max</td>
              <td colspan="2">2.5 GS/s, 50 Mpts/ch<br>250 Mpts max</td>
              <td colspan="2">2.5 GS/s, 50 Mpts/ch<br>250 Mpts max</td>
            </tr>
            <tr>
              <td>Display</td>
              <td colspan="2">12.1&quot; Touch Screen Display</td>
              <td colspan="2">12.1&quot; Touch Screen Display</td>
              <td colspan="2">12.1&quot; Touch Screen Display</td>
            </tr>
            <tr>
              <td>Mixed Signal</td>
              <td colspan="2">
                MSO - 16 Channels, 1.25 Gb/s                
              </td>
              <td colspan="2">
                MSO - 16 Channels, 1.25 Gb/s                
              </td>
              <td colspan="2">
                MSO - 16 Channels, 1.25 Gb/s                
              </td>
            </tr>
            <tr>
              <td>CPU</td>
              <td colspan="2">1.6 GHz Celeron with 2 GB RAM</td>
              <td colspan="2">2.5 GHz i5 with 8 GB RAM</td>
              <td colspan="2">2.9 GHz i5 with 8 GB RAM</td>
            </tr>
          
            <tr class="centered">
              <td width="13%"></td>
              <td colspan="2" width="29%"><strong>HDO4000</strong></td>
              <td colspan="2" width="29%"><strong>HDO6000</strong></td>
              <td colspan="2" width="29%"><strong>HDO8000</strong></td>
            </tr>
            <tr>
              <td>Grids Selection</td>
              <td colspan="2">3</td>
              <td colspan="2">Many</td>
              <td colspan="2">Many</td>
            </tr>
            <tr>
              <td># of Math Traces</td>
              <td colspan="2">2</td>
              <td colspan="2">8</td>
              <td colspan="2">8</td>
            </tr>
            <tr>
              <td># of Measurement Parameters, Tools</td>
              <td colspan="2">8 on screen with stats, trends and histicons</td>
              <td colspan="2">8 on screen with stats, trends, histicons, tracks and histograms</td>
              <td colspan="2">8 on screen with stats, trends, histicons, tracks and histograms</td>
            </tr>
            <tr>
              <td># of Supported Math Functions</td>
              <td colspan="2">20</td>
              <td colspan="2">39</td>
              <td colspan="2">39</td>
            </tr>
            <tr>
              <td># of Supported Measurements</td>
              <td colspan="2">32</td>
              <td colspan="2">72</td>
              <td colspan="2">72</td>
            </tr>
            <tr>
              <td>Built-in Tools</td>
              <td colspan="2">WaveScan, LabNotebook, Sequence, History</td>
              <td colspan="2">Spectrum, TriggerScan, WaveScan, LabNotebook, Sequence, History</td>
              <td colspan="2">Spectrum, TriggerScan, WaveScan, LabNotebook, Sequence, History</td>
            </tr>
            <tr>
              <td>Option Packages</td>
              <td colspan="2">Spectrum, Power, Serial Trigger and Decode</td>
              <td colspan="2">Power, JitKit, Serial Data Mask, Serial Trigger and/or Decode options, Advanced Customization, EMC, Digital Filtering, Serial Data Debug Toolkit</td>
              <td colspan="2">Power, JitKit, Serial Data Mask, Serial Trigger and/or Decode options, Advanced Customization, EMC, Digital Filtering, Serial Data Debug Toolkit</td>
            </tr>
            <asp:PlaceHolder ID="phPricingData" runat="server" />
            <tr>
              <td>&nbsp;</td>
              <td colspan="2" align="center"><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=419" onclick="ga('send', 'event', 'HD4096', 'Button', 'Quote HDO4000', {'nonInteraction': 1});"><img src="../img/button-configure-quote.png"></a></td>
              <td colspan="2" align="center"><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=422" onclick="ga('send', 'event', 'HD4096', 'Button', 'Quote HDO6000', {'nonInteraction': 1});"><img src="../img/button-configure-quote.png"></a></td>
              <td colspan="2" align="center"><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=458" onclick="ga('send', 'event', 'HD4096', 'Button', 'Quote HDO8000', {'nonInteraction': 1});"><img src="../img/button-configure-quote.png"></a></td>
            </tr>
          </table>
        </div>
      </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2014&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
        Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
