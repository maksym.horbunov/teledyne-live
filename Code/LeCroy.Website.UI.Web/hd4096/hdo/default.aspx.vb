﻿Public Class hd4096_hdo_default
    Inherits BasePage

#Region "Variables/Keys"
    Private Const HDO8000MS_PRICE_ADJUSTER As Int32 = 2800
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
            If (countryCode >= 0) Then
                If (String.Compare(countryCode.ToString(), "208", True) = 0) Then
                    BindPricingData()
                End If
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub BindPricingData()
        phPricingData.Controls.Add(GetHeaderRow(New List(Of String)({String.Empty, "HDO4000", "HDO4000-MS", "HDO6000", "HDO6000-MS", "HDO8000", "HDO8000-MS"})))
        For Each productHack In GetStaticData()
            Dim tr As HtmlTableRow = New HtmlTableRow()
            tr.Controls.Add(GetTableCell(False, False, productHack.Description))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo4000Price > 0, productHack.Hdo4000Price.ToString("C0"), "-")))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo4000MSPrice > 0, productHack.Hdo4000MSPrice.ToString("C0"), "-")))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo6000Price > 0, productHack.Hdo6000Price.ToString("C0"), "-")))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo6000MSPrice > 0, productHack.Hdo6000MSPrice.ToString("C0"), "-")))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo8000Price > 0, productHack.Hdo8000Price.ToString("C0"), "-")))
            tr.Controls.Add(GetTableCell(False, True, IIf(productHack.Hdo8000MSPrice > 0, productHack.Hdo8000MSPrice.ToString("C0"), "-")))
            phPricingData.Controls.Add(tr)
        Next
    End Sub

    Private Function GetHeaderRow(ByVal headerItems As List(Of String)) As HtmlTableRow
        Dim retVal As HtmlTableRow = New HtmlTableRow()
        retVal.Attributes.Add("class", "centered")
        For Each headerItem In headerItems
            retVal.Controls.Add(GetTableCell(True, False, headerItem))
        Next
        Return retVal
    End Function

    Private Function GetTableCell(ByVal isBold As Boolean, ByVal isCentered As Boolean, ByVal cellValue As String) As HtmlTableCell
        Dim retVal As HtmlTableCell = New HtmlTableCell()
        If (isCentered) Then
            retVal.Align = "center"
        End If
        If (isBold) Then
            retVal.InnerHtml = String.Format("<strong>{0}</strong>", cellValue)
        Else
            retVal.InnerHtml = cellValue
        End If
        Return retVal
    End Function

    Private Function GetStaticData() As List(Of ProductHack)
        Dim retVal As List(Of ProductHack) = New List(Of ProductHack)
        retVal.Add(New ProductHack("200 MHz 2 channel", Functions.GetProductListPriceonProductID(7121), Functions.GetProductListPriceonProductID(8294), 0, 0, 0, 0))          ' HDO4000
        retVal.Add(New ProductHack("200 MHz 4 channel", Functions.GetProductListPriceonProductID(7122), Functions.GetProductListPriceonProductID(8295), 0, 0, 0, 0))          ' HDO4000-MS
        retVal.Add(New ProductHack("350 MHz 2 channel", Functions.GetProductListPriceonProductID(7123), Functions.GetProductListPriceonProductID(8296), 0, 0, 0, 0))          ' HDO6000
        retVal.Add(New ProductHack("350 MHz 4 channel", Functions.GetProductListPriceonProductID(7124), Functions.GetProductListPriceonProductID(8297), Functions.GetProductListPriceonProductID(7168), Functions.GetProductListPriceonProductID(8291), Functions.GetProductListPriceonProductID(8361), (Functions.GetProductListPriceonProductID(8361) + HDO8000MS_PRICE_ADJUSTER)))          ' HDO6000-MS
        retVal.Add(New ProductHack("500 MHz 4 channel", Functions.GetProductListPriceonProductID(7125), Functions.GetProductListPriceonProductID(8298), Functions.GetProductListPriceonProductID(7169), Functions.GetProductListPriceonProductID(8292), Functions.GetProductListPriceonProductID(8362), (Functions.GetProductListPriceonProductID(8362) + HDO8000MS_PRICE_ADJUSTER)))          ' HDO8000
        retVal.Add(New ProductHack("1 GHz 4 channel", Functions.GetProductListPriceonProductID(7126), Functions.GetProductListPriceonProductID(8299), Functions.GetProductListPriceonProductID(7170), Functions.GetProductListPriceonProductID(8293), Functions.GetProductListPriceonProductID(8363), (Functions.GetProductListPriceonProductID(8363) + HDO8000MS_PRICE_ADJUSTER)))           ' HDO8000-MS
        Return retVal
    End Function
#End Region
End Class

Public Class ProductHack
    Private _description As String
    Private _hdo4000Price As Decimal
    Private _hdo4000MSPrice As Decimal
    Private _hdo6000Price As Decimal
    Private _hdo6000MSPrice As Decimal
    Private _hdo8000Price As Decimal
    Private _hdo8000MSPrice As Decimal

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(value As String)
            _description = value
        End Set
    End Property

    Public Property Hdo4000Price() As Decimal
        Get
            Return _hdo4000Price
        End Get
        Set(value As Decimal)
            _hdo4000Price = value
        End Set
    End Property

    Public Property Hdo4000MSPrice() As Decimal
        Get
            Return _hdo4000MSPrice
        End Get
        Set(value As Decimal)
            _hdo4000MSPrice = value
        End Set
    End Property

    Public Property Hdo6000Price() As Decimal
        Get
            Return _hdo6000Price
        End Get
        Set(value As Decimal)
            _hdo6000Price = value
        End Set
    End Property

    Public Property Hdo6000MSPrice() As Decimal
        Get
            Return _hdo6000MSPrice
        End Get
        Set(value As Decimal)
            _hdo6000MSPrice = value
        End Set
    End Property

    Public Property Hdo8000Price() As Decimal
        Get
            Return _hdo8000Price
        End Get
        Set(value As Decimal)
            _hdo8000Price = value
        End Set
    End Property

    Public Property Hdo8000MSPrice() As Decimal
        Get
            Return _hdo8000MSPrice
        End Get
        Set(value As Decimal)
            _hdo8000MSPrice = value
        End Set
    End Property

    Public Sub New(ByVal description As String, ByVal hdo4000Price As Decimal, ByVal hdo4000MSPrice As Decimal, ByVal hdo6000Price As Decimal, ByVal hdo6000MSPrice As Decimal, ByVal hdo8000Price As Decimal, ByVal hdo8000MSPrice As Decimal)
        _description = description
        _hdo4000Price = hdo4000Price
        _hdo4000MSPrice = hdo4000MSPrice
        _hdo6000Price = hdo6000Price
        _hdo6000MSPrice = hdo6000MSPrice
        _hdo8000Price = hdo8000Price
        _hdo8000MSPrice = hdo8000MSPrice
    End Sub
End Class