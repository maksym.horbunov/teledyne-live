﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_history_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>History Mode in HDO Oscilloscopes </h1><small>History Mode gives the High Definition Oscilloscope line a powerful debug tool, providing waveform playback that takes users back in time to effectively isolate anomalies in signals.</small>
          </div>

          
<h3 class="hdogreen">Introduction</h3> <p>Today’s oscilloscopes are extremely powerful tools with the ability to unearth minutely fine signal details. In no case is this truer than with the HDO4000 and HDO6000 High Definition Oscilloscopes from Teledyne LeCroy. The HDO models are endowed with a number of debug and analysis tools, such as History Mode, which allows users to scroll back in time to isolate anomalies or other waveform events, measure them with parameters or cursors, and quickly find the sources of problems. </p> <p>Often, when viewing waveforms on an oscilloscope display, unexpected events may occur and there is no way for the user to stop the trigger and view anomalies. History Mode changes this and shortens the amount of time it takes to identify problems by automatically stopping the trigger and displaying a list of previously captured waveforms. In this way, unexpected anomalies can be viewed, quantified, and analyzed.</p> <h3 class="hdogreen"><b>Invoking History Mode</b></h3> <p>For this demonstration, a 25-MHz clock signal was input to Channel 1 on the oscilloscope. The initial oscilloscope setup is shown in Figure 1.</p> <a href="/images/hdo_techbrief_history_01.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_history_01.png" alt=""><div class="caption"><h4>Figure 1:</h4> <p>This is the initial setup with a 25-MHz clock waveform on Channel 1 of the HDO oscilloscope. </p></div></a> <p>History Mode is best accessed directly from the front panel; pressing the History button stops the trigger and quickly displays a list of previously captured waveforms alongside the waveform grid as shown in Figure 3. The History Mode dialog box is shown in Figure 2.</p> <a href="/images/hdo_techbrief_history_02.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_history_02.png" alt=""><div class="caption"><h4>Figure 2:</h4> <p>Upon invoking the History Mode option in the Timebase pulldown menu, users are presented with the controls dialog at the bottom of the screen. </p> </div></a> <a href="/images/hdo_techbrief_history_03.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_history_03.png" alt=""><div class="caption"><h4>Figure 3:</h4> <p>Checking both View History and View Table brings up the History Mode table seen at top left. History Mode saves and shows all acquisitions automatically with the time at which they were acquired. </p> </div></a> <h3 class="hdogreen">Navigation within History Mode</h3> <p>The History Mode table is interactive in that tapping any line within the table automatically brings up that acquisition. Thus, by scrolling up or down the table, users have direct access to any acquisition. This can be accomplished either by manipulating the front-panel adjustment knob, or by using the Waveform History Navigation section of the History Mode dialog box.</p> <p>The Waveform History Navigation controls provide a flexible means of traveling backward and forward through the mass of acquisitions stored in memory. Shown in Figure 4, the Waveform History Navigation controls offers slow scrolling, fast scrolling, single stepping, and a second scroll bar. </p> <a href="/images/hdo_techbrief_history_04.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_history_04.png" alt=""><div class="caption"><h4>Figure 4:</h4> <p>Within the Waveform History Navigation section of the History Mode dialog box is a full range of controls to move backward and forward through the acquisition memory. </p> </div></a> <p>The top row of controls regulates scrolling, with the two outer buttons initiating a fast scroll and the two inner buttons a slower scroll. On the bottom row, the outer buttons jump to the oldest and newest acquisitions, while the inner two step through one at a time. Tapping the Index box allows for direct numeric entry of the desired history item. At bottom is the manual scroll bar, which duplicates that seen to the right of the History Mode table in Figure 3. Also note that the Relative Times checkbox in the History Table section of the dialog box toggles the table between display of the absolute time of each acquisition and the relative time from the last acquisition.</p><p>Note that it is possible to save any displayed acquisition in History Mode by opening the File pulldown menu and selecting Save Waveform. Also, users may apply measurements or math operations to any selected History Mode acquisition as shown in Figure 5. </p> <a href="/images/hdo_techbrief_history_05.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_history_05.png" alt=""><div class="caption"><h4>Figure 5:</h4> <p>Measurement and/or math operations may be performed on any acquired waveform in History Mode. In this example, one of a group of single-shot acquisitions is measured for width, frequency, rise time (20-80%), duty cycle, and overshoot. Note that the measurement statistics show the 4.49-ns glitch</p> </div></a> <h3 class="hdogreen">Conclusion</h3> <p>The History Mode function in the HDO4000 and HDO6000 High Definition Oscilloscopes provides a myriad of powerful analysis and debug options. History Mode automatically saves all acquisitions and delivers convenient means of navigating through a tabular list. Moreover, measurements and/or math operations may be performed on any historical acquisition.</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=7522#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'History Mode PDF', {'nonInteraction': 1});"><i class="icon-file"></i>History Mode</a></li>
              <li><a href="https://www.youtube.com/embed/F6xDA3lUZyc?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="History Mode"><i class="icon-film"></i>History Mode (6:18)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

      
      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
