﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_logic_gate_emulation_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Logic Gate Emulation in HDO-MS Oscilloscopes</h1>
          </div>

          <h3 class="hdogreen">Introduction</h3><p>A useful debugging tool is logic-gate emulation, which allows users to simulate and debug complete digital designs. The Mixed Signal High Definition Oscilloscopes enable accomplishing this task within the instrument itself. Combined with the powerful Processor Web Editor utility, many logic gates may be daisy-chained together to create complex digital designs. </p>

<h3 class="hdogreen">Procedure</h3>
<p>For purposes of this demonstration, digital lines D0-D4 of the 16-channel digital lead set were connected to clock pins of varying speeds. Next, press the Dig (for Digital) button in the Vertical section of the front panel. This will activate the digital channels.</p>

<p>Logic gate emulation is accessed through the oscilloscope’s Math menu. Select the Math pull-down menu and choose F1 Setup (Figure 1).</p>

<a href="/images/hdo_techbrief_logic_gate_01.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_01.png" alt=""><div class="caption"><h4>Figure 1:</h4><p>  The F1 setup dialog box</p></div></a>

<p>Press the Operator1 button. In the Select Math Operator dialog box, specify the category as Digital. Under choices, select DigitalAND (Figure 2).</p>

<a href="/images/hdo_techbrief_logic_gate_02.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_02.png" alt=""><div class="caption"><h4>Figure 2:</h4><p>  The Select Math Operator dialog box</p></div></a>
<p>Press the Source 1 button. In the Select Source dialog box, select Digital Lines as the category and set Source1 to D0. Similarly, set Source2 as D1.</p>

<a href="/images/hdo_techbrief_logic_gate_03.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_03.png" alt=""><div class="caption"><h4>Figure 3:</h4><p>  The Select Source dialog box</p></div></a>


<p>Turn the F1 trace on to confirm that the result is the logical AND of the D0 and D1 signals (Figure 4).</p>


<a href="/images/hdo_techbrief_logic_gate_04.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_04.png" alt=""><div class="caption"><h4>Figure 4:</h4>  <p>The magenta traces are digital lines D0 and D1; the yellow trace is that of the digitalAND operation that combines them in emulation</p></div></a>


<p>Using logic gate emulation in combination with the Processing Web Editor allows combining many digital gates together in one math function to emulate complex designs (Figure 5). AND, OR, NAND, NOR, XOR, NOT and D Flip Flop gates are all available as math operators to enable debugging complete digital systems. The Processing Web Editor enables a graphical approach to problem solving by allowing users to develop math and measurement processing chains in a familiar block diagram/flowchart format.</p>

<a href="/images/hdo_techbrief_logic_gate_05.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_05.png" alt=""><div class="caption"><h4>Figure 5:</h4> <p>The Web Editor function enables users to combine many digital gates in a single complex math function to emulate complex designs </p></div></a>

<p>The Processing Web Editor is accessed by touching the Math drop-down menu, followed by Math Setup. Next, open the F1 tab and press the web edit button on the left side. Check the Trace On check box. Press the Show Processing Web button to open the Processing Web Editor.</p>

<p>Within the Web Editor are buttons for adding math, measurements, and parameter math functions (Figure 5, again). Another button labeled Add Preview opens viewing windows that show the waveform or measured value at the output of any operation.</p>

<p>The Math tab in the Web Editor dialog enables users to set the function of any math traces to single operator, dual operator, historgram, or web edit (Figure 6). By default, F1 is set as the output from the processing web, which is indicated by the F1 terminal on the right side of the Web Editor.</p>

<a href="/images/hdo_techbrief_logic_gate_06.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_06.png" alt=""><div class="caption"><h4>Figure 6:</h4> <p>The Math tab within the Processing Web Editor dialog enables users to set the function of math traces.</p></div></a>

<p>The Measure tab in the Web Editor allows users to set the function of any of the measurement parameters to be a simple measurement, parameter math, or the Web Editor (Figure 7).</p>

<a href="/images/hdo_techbrief_logic_gate_07.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_07.png" alt=""><div class="caption"><h4>Figure 7:</h4> <p>The Measure tab allows users to set the function of any measurement parameters</p></div></a>

<p>Touching the Add Math (or Add Measure, or Add Param Math) button opens the Add Math Processor, which allows selection from a large range of basic math functions, custom functions, filters, frequency analysis, and more (Figure 8).</p>

<a href="/images/hdo_techbrief_logic_gate_08.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_logic_gate_08.png" alt=""><div class="caption"><h4>Figure 8:</h4> <p>The Add Math Processor dialog box</p></div></a>

<p>Once math components, measurements, and/or parameter math functions have been added to the Web Editor grid, simply touch and drag them into position. Then, interconnect them to channel inputs/outputs and to each other in the desired configurations by drawing interconnect lines with a fingertip or the stylus.</p>

<h3 class="hdogreen">Conclusion</h3>
<p>Teledyne LeCroy’s Mixed Signal High Definition Oscilloscopes offer digital input channels, which combined with the Processing Web Editor utility, comprise a powerful debugging tool enabling users to emulate logic gate designs in a large range of configurations.</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=8135#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Logic Gate Emulation PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Logic Gate Emulation</a></li>
              <li><a href="https://www.youtube.com/embed/1dqjn2CDfWE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Logic Gate Emulation"><i class="icon-film"></i>Logic Gate Emulation (1:18)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

     


      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
