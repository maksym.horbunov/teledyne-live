﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_parallel_pattern_search_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscopes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Searching for Parallel Digital Patterns in HDO-MS Oscilloscopes</h1>
          </div>

          <h3 class="hdogreen">Introduction</h3>

<p>The combination of Teledyne LeCroy’s Mixed Signal High Definition Oscilloscopes and their 16 digital signal inputs with the instruments’ WaveScan technology makes it a simple matter to identify matching patterns across many digital lines. Identified patterns are presented in a table with timestamp information and enable quick searching for each pattern occurrence. Here, we will examine an example of how to set up such a debugging scenario.
</p>
<h3 class="hdogreen">Procedure</h3>

<p>For purposes of this demonstration, digital lines D0-D4 of the 16-channel digital lead set were connected to clock pins of varying speeds. Next, press the Dig (for Digital) button in the Vertical section of the front panel. This will activate the digital channels (Figure 1).
</p>
<a href="/images/digital-measuremnts-hdo6000_03.png" rel="lightbox" class="thumbnail"><img src="/images/digital-measuremnts-hdo6000_03.png" alt=""><div class="caption"><h4>Figure 1:</h4>  <p>The Digital Channels dialog box, where users may indicate how many, and which, of 16 available digital input lines are to be shown on the display</p></div></a>


<p>WaveScan may be enabled by either accessing it through the Analysis pull-down menu on the touch screen or by simply pressing the WaveScan button on the front panel. Next, check the Enable box in the WaveScan dialog (Figure 2).
</p>

<a href="/images/digital-measuremnts-hdo6000_03.png" rel="lightbox" class="thumbnail"><img src="/images/digital-measuremnts-hdo6000_03.png" alt=""><div class="caption"><h4>Figure 2:</h4>  <p>The WaveScan dialog box</p></div></a>

<p>Now set the WaveScan mode to Bus Pattern. Touch the Source1 button and select Digital Buses under Category. Under Source, select the group of digital lines of interest. The Digital Channels dialog contains four tabs for groups Digital1 through Digital4.
</p>
<p>At the right side of the WaveScan dialog box is a tab labeled Pattern Time. There, users may select either Binary or Hex viewing modes, specify the pattern for search, and indicate how many instances of the pattern WaveScan should report (Figure 3).
</p>

<a href="/images/digital-measuremnts-hdo6000_03.png" rel="lightbox" class="thumbnail"><img src="/images/digital-measuremnts-hdo6000_03.png" alt=""><div class="caption"><h4>Figure 3:</h4>  <p>The Binary Virtual Keypad, found in the WaveScan dialog, enables users to specify the binary or hex pattern to be searched for in the acquired waveform(s)</p></div></a>

<p>Next, select an action for WaveScan to perform upon discovery of the specified pattern. This is found in the WaveScan dialog under Action on Features Found. For our example, we will have WaveScan stop acquisition. Other options include saving the waveform(s), saving as a LabNotebook documentation and report generation entry, setup of a pulse AUX output, printing the screen to a file or hard copy, or simply emitting a beep (Figure 4).
</p>
        
<a href="/images/digital-measuremnts-hdo6000_03.png" rel="lightbox" class="thumbnail"><img src="/images/digital-measuremnts-hdo6000_03.png" alt=""><div class="caption"><h4>Figure 4:</h4>  <p>Shown are the various actions WaveScan may be set to take upon discovery of the specified bit pattern</p></div></a>

<p>In this instance, we have indicated that WaveScan is to report 10 instances of the specified pattern. These are seen in a table at the upper left corner of the display. Enable the table by checking the Table box in the WaveScan dialog. Note that WaveScan will have stopped the trigger. Touching any entry in the table automatically zooms to that instance in the acquired waveform (Figure 5).
</p>

<a href="/images/digital-measuremnts-hdo6000_03.png" rel="lightbox" class="thumbnail"><img src="/images/digital-measuremnts-hdo6000_03.png" alt=""><div class="caption"><h4>Figure 5:</h4>  <p>In this full screen capture, note the WaveScan table at upper left with instance 7 of 10 of the specified bit pattern highlighted. The zoom trace automatically displays the highlighted instance</p></div></a>


<h3 class="hdogreen">Conclusion</h3>

<p>Teledyne LeCroy’s Mixed Signal High Definition Oscilloscopes provide powerful tools for measurement and analysis of digital signals. All of these powerful tools, such as parallel pattern search using WaveScan, make debugging of digital designs quick and painless. 
</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=8071#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Searching for Parallel Digital Patterns in HDO-MS Oscilloscopes PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Searching for Parallel Digital Patterns in HDO-MS Oscilloscopes</a></li>
              <li><a href="https://www.youtube.com/embed/ETWqRCzWHwY?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Searching for Parallel Digital Patterns in HDO-MS Oscilloscopes"><i class="icon-film"></i>Searching for Parallel Digital Patterns in HDO-MS Oscilloscopes (1:28)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

      


      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
