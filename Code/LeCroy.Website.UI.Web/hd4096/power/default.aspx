﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_power_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Using the Teledyne LeCroy HDO Oscilloscopes with Power Analyzer Option</h1>
          </div>

          <h3 class="hdogreen">Introduction </h3> <p>Power Analyzer allows all aspects of switched mode power devices to be measured in a single, highly integrated software tool as shown in Figure 1. </p> <a href="/images/hdo_techbrief_power_02.png" rel="lightbox" class="thumbnail"> <img src="/images/hdo_techbrief_power_02.png" alt=""> <div class="caption"> <h4>Figure 1: </h4> <p>An example of the Power Analyzer option with a choice of Device, Control Loop, and Line Power Analysis </p> </div></a> <p>Device analysis such as power loss, saturation voltage, high side gate drive, dynamic-on resistance, safe operating area, and others are easily performed. Control Loop (Modulation) analysis provides tools for easy understanding of control loop response, such as soft start performance or step response to line and load changes. </p> <p>Line Power Analysis allows simple and quick pre-compliance testing to EN 61000-3-2. </p> <p>Power Analysis is a complete solution with a wide range of measurement and analysis tools.  It features: <ul> <li>Automatic setup and display of relevant waveforms and parameters </li> <li>Power device performance analyzed in-circuit </li> <li>Waveforms scaled and displayed in Volts, Amps, Watts, Joules, Ohms, etc. </li> <li>Measurement and viewing of the time domain response of the entire control loop </li> <li>Line Analysis including line harmonics </li> </ul> </p> <h3 class="hdogreen">Device Analysis </h3> <p>Device Analysis includes loss measurement, Safe Operating Area (SOA), Dynamic On Resistance (RD <sub>s </sub>[On]), and DV/dt and Di/dt. </p> <p>Loss measurement, as shown in Figure 1, includes automatic identification of zones using color specific overlays to mark conducting and off state regions of device operation. Losses associated with the switching, conduction, and off state are measured independently and displayed along with the sum of selected loss types.  Power analysis includes its own measurement display table allowing both conventional and power analysis measurements simultaneously. </p> <p>Using an oscilloscope with a 12 bit dynamic range makes it easier to characterize saturation region measurements.  The saturation region is typically a very low level, on the order of a Volt, that alternates with the off-state where the voltage may rise to several hundred Volts. </p> <p>Safe Operating Area is another aspect of device characterization and an example is shown in Figure 2. </p> <a href="/images/hdo_techbrief_power_04.png" rel="lightbox" class="thumbnail"> <img src="/images/hdo_techbrief_power_04.png" alt=""> <div class="caption"> <h4>Figure 2: </h4> <p>An example of a safe operating area plot </p> </div></a> <p>The safe operating area display plots device voltage as a function of current.  It shows the in-circuit behavior of the device under test revealing any transitions near peak voltage, current, and power limits. </p> <p>The loop dynamics of a switched mode power device can be evaluated using the Control Loop analysis of the Power Analysis software as shown in Figure 3 </p> <a href="/images/hdo_techbrief_power_06.png" rel="lightbox" class="thumbnail"> <img src="/images/hdo_techbrief_power_06.png" alt=""> <div class="caption"> <h4>Figure 3: </h4> <p>Closed loop analysis of the gate drive waveform for a switched mode power supply shows the variation in gate drive pulse width in response to a change in power supply loading </p> </div></a> <p>Control loop analysis studies the variation in the parameters of the switched mode power feedback loop.  It plots the variation of pulse width, duty cycle, period or frequency.  In Figure 3 the device under test is a power supply using pulse width modulation as the control method.  The closed loop (CL) plot shows the variation of PWM controller width due to a 7.5 Hz square wave load change.  The plot shows the dynamics of the width variation on a cycle by cycle basis.  The Width parameter shows a variation of from 100 ns (min) to 5 µs (max).  The duty cycle also shows a similar variation.  </p> <p>An import aspect of this type of analysis is the length of the oscilloscope memory.  Most power supply variations take a long time.  For example, startup may take several seconds.  The HDO family of oscilloscopes support up to 250 Mpts of acquisition memory.  In Figure 3 the capture time is 200 ms of time the oscilloscope is still acquiring at a 100 MS/s sampling rate.  The final analysis mode of the Power Analyzer is Line Power.  Here, line harmonics can be investigated to see if they are in compliance with international standards like EN 61000-3-2.  </p> <p>Line harmonic analysis is shown in Figure 4.  Here, the in addition to the line voltage and current in channels C1 and C2 respectively, the first forty harmonics of the 60 Hz line current are displayed.  The blue overlay shows the compliance levels for EN 61000-3-2.  The table in the lower right of the display lists each of these harmonics and includes level, frequency, and conformance to the standard. </p> <a href="/images/hdo_techbrief_power_08.png" rel="lightbox" class="thumbnail"> <img src="/images/hdo_techbrief_power_08.png" alt=""> <div class="caption"> <h4>Figure 4: </h4> <p>Line power analysis showing the harmonics of line current.  The table in the lower right lists each of 40 harmonics including it’s frequency and level </p> </div></a> <h3 class="hdogreen">Conclusion </h3> <p>The Power Analyzer option for the HDO4000 and HDO6000 high definition oscilloscopes simplifies how power measurements are made.  Power device performance while the device is operating in circuit can be analyzed without requiring specially designed test fixtures. The long memory of the HDO enables capture times in the 100s of milliseconds at high sample rates for finding unusual events during measurements. </p>
            


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=7518#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Power Mode PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Power</a></li>
              <li><a href="https://www.youtube.com/embed/rZFUU8AOEJI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Power"><i class="icon-film"></i>Power Mode (1:31)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

  

      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
