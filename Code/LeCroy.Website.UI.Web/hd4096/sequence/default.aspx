﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_sequence_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Sequence Mode in HDO Oscilloscopes </h1>
          </div>

          <h3 class="hdogreen">Introduction</h3> <p>There are often instances in which an oscilloscope user may need to capture a large number of fast pulses in quick succession, or, conversely, a small number of events separated by long periods of time. Either scenario can be challenging with typical oscilloscope acquisition modes. Sequence Mode, a standard acquisition mode found in the HDO4000 and HDO6000 High Definition Oscilloscopes, enables capture of fine details of complex event sequences occurring over long time intervals, while ignoring the intervals between events. </p> <h3 class="hdogreen">Invoking Sequence Mode</h3> <p>For this demonstration, the input signal on channel 1 is from an echo ranging system.  A transmitted pulse is followed by a reflected pulse whose position and amplitude vary with distance.  The measurement repeats at a 10 kHz (100 µs) rate. The initial oscilloscope setup is shown in Figure 1.</p> <a href="/images/hdo_techbrief_sequence_02.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_sequence_02.png" alt=""><div class="caption"><h4>Figure 1:</h4> <p>This is the initial setup with a waveform on Channel 1 from the HDO oscilloscope’s Aux Output</p> </div> </a> <p>Sequence Mode can be accessed by tapping the Timebase descriptor box or by opening the Timebase pulldown menu and clicking Horizontal Setup. On the Timebase tab, select Sequence under Sampling Mode. This will create a new tab in the Timebase dialog box labeled Sequence.</p> <a href="/images/hdo_techbrief_sequence_04.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_sequence_04.png" alt=""><div class="caption"><h4>Figure 2:</h4> <p>Upon invoking the Sequence Mode option in the Timebase tab, users are presented with the controls dialog at the bottom of the screen</p></div></a> <p>The first section on the left side of the Sequence tab is Acquisition Settings. Touching the Number of Segments button twice brings up a dialog box that allows setting of how many acquisitions (or segments) the oscilloscope will make in Sequence mode. This can range from a minimum of two to a maximum of 10,000 with the HDO4000 and 65,000 with the HDO6000; the default number is 10. Regardless of what number is entered, the oscilloscope will acquire data until either that number is reached, the timeout (described next) is reached, or the user presses the front-panel Stop button. Next to the Acquisition Settings section is the Sequence Timeout section. Here, the user may instruct the oscilloscope to stop acquiring data if a preset time period passes without a trigger event.</p> <p>When using the HDO4000, segments are shown using an adjacent display, with each segment shown next to the previous segment. For long captures of many segments, the Zoom function can be used to show individual segments. The HDO6000 offers a variety of segment-viewing techniques. The Display Settings section has all the display modes, which include adjacent, overlay, waterfall, perspective, and mosaic. The adjacent mode is the default as seen in Figure 2. Another option, Perspective mode, is shown in Figure 3.</p> <a href="/images/hdo_techbrief_sequence_06.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_sequence_06.png" alt=""><div class="caption"><h4>Figure 3:</h4> <p>The Display Mode setting provides various ways of viewing the memory segments acquired in Sequence Mode. Shown is the perspective display mode. Other options include adjacent (the default mode), overlay, waterfall, and mosaic</p></div></a> <h3 class="hdogreen">Displaying More Information</h3> <p>Also within the Display Settings section of the Sequence tab is the option to Show Sequence Trigger Times. Tapping this button brings up an entirely new Trigger Times dialog box shown in Figure 4.</p> <a href="/images/hdo_techbrief_sequence_08.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_sequence_08.png" alt=""><div class="caption"><h4>Figure 4:</h4> <p>Figure 4: Tapping the Show Sequence Trigger Times button opens this dialog box</p></div></a> <p>The first column under the Time section is the segment number, followed by an absolute time stamp (1s resolution). The third column shows the time since Segment #1 and the fourth shows the time between segments (both in nanosecond resolution). </p> <p>Each segment can be viewed individually or as groups of segments by pushing the front-panel Zoom button or by using the touch screen, as shown in Figure 5. The horizontal delay knob then permits scrolling through the segments. In the Zoom dialog box, users can set up zoomed views of multiple segments for comparison purposes.</p> <a href="/images/hdo_techbrief_sequence_10.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_sequence_10.png" alt=""><div class="caption"><h4>Figure 5:</h4> <p>Multiple segments can be zoomed for comparison purposes</p></div></a> <h3 class="hdogreen">Conclusion</h3> <p>The Sequence Mode acquisition mode found in the HDO4000 and HDO6000 is a fast trigger mode that makes the most of the instruments’ acquisition memory. With each memory segment storing a single acquisition, Sequence Mode minimizes dead time between trigger events and enables capture of many fast pulses in rapid succession.</p>

</div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=7520#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Sequence Mode PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Sequence Mode</a></li>
              <li><a href="https://www.youtube.com/embed/qeISsxGzH0c?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Sequence Mode"><i class="icon-film"></i>Sequence Mode (1:24)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->



    
      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
