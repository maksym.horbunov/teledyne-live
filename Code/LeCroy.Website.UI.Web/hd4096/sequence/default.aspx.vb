﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports LeCroy.Library.VBUtilities

Public Class hd4096_sequence_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region

#Region "WebMethods"
    <WebMethod()> _
    Public Shared Function submitrequest(ByVal emailaddress As String) As Boolean
        If (String.IsNullOrEmpty(emailaddress)) Then
            Return False
        End If
        If (Not BLL.Utilities.checkValidEmail(emailaddress)) Then
            Return False
        End If

        Dim strEmail As String = SQLStringWithOutSingleQuotes_LOCAL(emailaddress)
        Dim DOCUMENT_ID As Integer = 75 'hd4096 email list
        Dim strSubID As String = Functions.GenerateUserID.ToString
        Dim strContactId As String = "0"
        If Not HttpContext.Current.Session("ContactID") Is Nothing And Len(HttpContext.Current.Session("ContactID")) > 0 Then
            strContactId = HttpContext.Current.Session("ContactID")
        End If
        If Len(strEmail) > 0 Then
            If Not Functions.checksubscriptionexist(DOCUMENT_ID, strEmail.ToString) Then
                Dim sqlString As String = "Insert into SUBSCRIPTION (SUB_ID,DATE_ENTERED,EMAIL,DOCUMENT_ID,CONTACT_ID,ENABLE,COUNTRY_ID,DOCUMENT_READ,FORMAT_ID, CONFIRM_YN) VALUES (@SUBID, @DATEENTERED, @EMAIL, @DOCUMENTID, @CONTACTID, @ENABLE, @COUNTRYID, @DOCUMENTREAD, @FORMATID, @CONFIRMYN)"
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@SUBID", strSubID))
                sqlParameters.Add(New SqlParameter("@DATEENTERED", DateTime.Now.ToString()))
                sqlParameters.Add(New SqlParameter("@EMAIL", strEmail))
                sqlParameters.Add(New SqlParameter("@DOCUMENTID", DOCUMENT_ID.ToString()))
                sqlParameters.Add(New SqlParameter("@CONTACTID", strContactId.ToString()))
                sqlParameters.Add(New SqlParameter("@ENABLE", "N"))
                sqlParameters.Add(New SqlParameter("@COUNTRYID", "208"))
                sqlParameters.Add(New SqlParameter("@DOCUMENTREAD", "N"))
                sqlParameters.Add(New SqlParameter("@FORMATID", "2"))
                sqlParameters.Add(New SqlParameter("@CONFIRMYN", "N"))
                DbHelperSQL.ExecuteSql(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
                Return True
            End If
        End If
        Return False
    End Function

    Private Shared Function SQLStringWithOutSingleQuotes_LOCAL(ByVal strinput As String) As String
        Dim badChars As String() = New String() {"SELECT", "DROP", "select", "drop", "--", _
                                                "INSERT", "insert", "DELETE", "delete", "_XP", "xp_", "1=1", "/* ... */", "Char("}
        If Not strinput Is Nothing Then
            If strinput.Length > 0 Then
                strinput = strinput.Trim()
                strinput = strinput.Replace("'", "''")
                Dim i As Integer
                For i = 0 To badChars.Length - 1 Step i + 1
                    strinput = strinput.Replace(badChars(i), "")
                Next
            Else
                strinput = String.Empty
            End If
        Else
            strinput = String.Empty
        End If
        Return strinput
    End Function
#End Region
End Class