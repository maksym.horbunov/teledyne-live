﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_setting_up_channels_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Digital Channel Setup in <nobr>HDO-MS</nobr> Oscilloscopes</h1>
          </div>

          <h3 class="hdogreen">Introduction</h3>
          <p>Teledyne LeCroy’s Mixed Signal High Definition Oscilloscopes provide 16 digital signal inputs that are easily set up, labeled, and grouped in a myriad of ways. Taking a little time to customize your setup at the outset of a debugging session will help to make the debug process quick and intuitive.</p>
          <h3 class="hdogreen">Procedure</h3>
          <p>For purposes of this demonstration, digital lines D0-D4 of the 16-channel digital lead set were connected to clock pins of varying speeds. Next, press the Dig (for Digital) button in the Vertical section of the front panel, or access the Digital Channels dialog using the Vertical pull-down menu on the touch screen. Either will activate the digital channels and also open the Digital Channels dialog (Figure 1).</p>
          <a href="/images/setting_channels_01.png" rel="lightbox" class="thumbnail"><img src="/images/setting_channels_01.png" alt=""><div class="caption"><h4>Figure 1:</h4><p>The Digital Channels dialog box, where users may indicate how many, and which, of 16 available digital input lines are to be shown on the display</p></div></a>
          <p>Across the bottom center of the Digital Channels dialog are the Line Activity indicators, which provide a quick means of determining the logical state of all digital lines at once (Figure 1, again). The oscilloscope need not be triggered to observe line activity with the indicators. The indicators return three states: High, Low, and Transitioning. </p>
          <p>On the right side of the Digital Channels dialog are controls to adjust the Vertical Position and Group Height. The Vertical Position button determines the relative position of the digital signals on the screen. A value of 4.00 divisions will position the top of the top-most trace in the Digital Group very near to the top of the display. Group Height simply determines how many vertical divisions the Digital Group consumes on the display. Both settings may be done using the front panel knobs.</p>
          <p>Also on the right is quick access to a zoom view. The touch screen Zoom button zooms in on only the digital channels, while the Zoom button in the center of the front panel turns on zoom traces for all channels. Similarly, the Decode button duplicates the function of the front-panel Decode button; both open the Serial Decode dialog for further insight into bus traffic. The Store button in the Digital Channels dialog stores all traces in the Digital Group in memory.</p>
          <p>On the left in the View column, check the Group box to display lines in group mode. Below that, the Display Mode menu permits a choice of displaying Lines, Bus, or Lines & Bus. The Labels button permits selection of channel names by Data, Address, or Custom. Under each channel check box is a name box; these permit easy swapping of channel names. The Custom labels may be used to give each channel a completely custom name. The Display D0-D7 and D8-D15 shortcut buttons are available to quickly turn multiple channels on or off at the same time.</p>
          <p>The four color-coded tabs allow the digital input channels to be specified in four groups labeled Digital1 through Digital4. Any given channel can be in any of the groups at the same time, allowing users to set up differing groupings of the same input channels for measurement and/or analysis.</p>
          <p>The Logic Setup tab (Figure 2) permits selection of seven standard logic families (TTL, ECL, CMOS 5V, CMOS 3.3V, CMOS 2.5V, PECL 5V, LVDS, and user defined). There, users select thresholds and hysteresis settings if applicable.</p>
          <a href="/images/setting_channels_02.png" rel="lightbox" class="thumbnail"><img src="/images/setting_channels_02.png" alt=""><div class="caption"><h4>Figure 2:</h4><p>The Logic Setup tab allows selection of logic families, thresholds, and hysteresis values</p></div></a>
          <h3 class="hdogreen">Conclusion</h3>
          <p>Teledyne LeCroy’s Mixed Signal High Definition Oscilloscopes provide a powerful and highly flexible user interface for display and organization of digital input channels. The interface lends an intuitive feel to the debugging of digital designs.</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=8072#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Digital Channel Setup in HDO-MS Oscilloscopes PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Digital Channel Setup in <nobr>HDO-MS</nobr> Oscilloscopes</a></li>
              <li><a href="https://www.youtube.com/embed/cfR6eAmYvGE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Digital Channel Setup in HDO-MS Oscilloscopes"><i class="icon-film"></i>Digital Channel Setup in <nobr>HDO-MS</nobr> Oscilloscopes (1:35)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

      


      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
