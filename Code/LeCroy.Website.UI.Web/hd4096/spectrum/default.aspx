﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_spectrum_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>Spectrum Analyzer Mode in HDO Oscilloscopes</h1>
          </div>

     <h3 class="hdogreen">Simplifies Frequency Analysis</h3> <p>An oscilloscope provides powerful capability to debug frequency-related effects. Fast Fourier Transforms (FFTs) have long been part of oscilloscope toolboxes. Now, a spectrum analyzer like capability is available to simplify setup and use of the oscilloscope for analyzing frequency-dependent effects. It allows users who are familiar with RF spectrum analyzers to start using the FFT with little or no concern about the details of setting up an FFT. Setup is simple - select a center frequency, span, and resolution bandwidth and the necessary sample rate and time-domain acquisition length of the oscilloscope are automatically determined behind the scenes.  Settings for normal or averaged FFTs as well as reference levels and scales are also available.  Then, select an operational mode (Normal, Average, or Max Hold) for the spectrum display. Other settings for reference level and scale may also be made. With the Spectrum Analyzer mode the user is freed from the need to translate scope sample rate, memory, and acquisition length settings into frequency domain relevant units.</p> <p>The automatic peak detect function identifies the highest peaks in the spectrum, labels them on the display and builds an interactive table of all peaks.  Simply touch the table to order peaks by frequency or amplitude, selected peaks can be quickly moved to the center of the display. The table also makes apparent frequency peaks that may not have otherwise obvious.</p> <p>In addition to the spectrum display the Spectrum Analyzer mode includes a spectrogram display, available in either a 2 D or a 3D, which shows a history of how spectra have changed over time.</p> <h3 class="hdogreen">Using the Spectrum Analyzer</h3> <p>Turn on the Spectrum Analyzer mode by pressing the front panel button. </p> <p>The Spectrum Analyzer dialog box, shown in Figure 1, contains all the controls easy frequency domain analysis.</p> <a href="/images/hdo_techbrief_spectrum_02.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_02.png" alt=""><div class="caption"><h4>Figure 1:</h4> <p>The Spectrum Analyzer dialog box showing the principal controls of the Spectrum Analyzer</p></div></a> <p>The user can select the source trace from any input channel, math, memory, or, zoom trace.  </p> <p>The main controls for the Spectrum Analyzer option are the center frequency and span, just like an RF Spectrum Analyzer.  Center and Span are similar to adjusting the position of the FFT trace. Values for each setting are easily entered through the on-screen keypad.  The user interface reports the maximum frequency that can be observed, which is one half of the current oscilloscope sampling.  </p> <a href="/images/hdo_techbrief_spectrum_" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_04.png" alt=""><div class="caption"><h4>Figure 2:</h4> <p>Basic view of the spectrum directly below the time domain signal</p></div></a> <p>Alternately, Start Stop provides another way to adjust the position of the FFT trace. Start and Stop frequencies can be specified with the keypad, similar to center and span. </p> <p>Resolution Bandwidth is equivalent to changing the Timebase setting to increase or decrease memory in FFT mode. Reducing the Resolution Bandwidth equals more memory. The Spectrum Analyzer reports back adapted values for the resolution bandwidth if the value entered is not achievable.  The default is to set the resolution bandwidth automatically as confirmed by the Auto check box being checked.</p> <p>The main vertical scale controls are Output (type), Scale, and Reference Level.  The Output type can be dBm, dBVrms, dBmV, dBµV, Vrms, and Arms(if a current probe is the source). </p> <p>Reference Level sets the amplitude of the top of the screen in units matching the output type. </p> <p>Scale is the same as adjusting the Vertical Gain knob in FFT mode and sets the scale in dB per division or Volts/div as is appropriate for the output type selection.</p> <p>There are three operating modes Normal, Average, and Max Hold.  Normal mode displays the power spectrum of the source trace.  In Averaging mode, the user can enter the number of spectra to be averaged. Averaging is effective in reducing the noise of the signal to see more of the harmonic or carrier detail. Max (Peak) Hold mode is useful for swept frequency measurements where it shows the history of peak values across the frequency axis. Max Hold shows the maximum level the signal reaches.  It is also useful for finding infrequent spurs.</p> <p>The Persistence On check box gives control of display persistence on the spectrum display.  With persistence on, a history of multiple spectra similar to the display on an RF spectrum analyzer is shown.</p> <p>The Show Source and Show Zoom check boxes enable viewing the time domain source trace and a zoom of the source trace respectively.  In Figure 1 the Show Source box is checked and the channel 1 display is shown.  The source trace and zoom traces will each appear in their own grids.</p> <p>Window allows the user to specify the weighting window to be used for the FFT.  The choices are Von Hann (Hanning), Hamming, Flat Top, and Blackman Harris. Figure 3 shows a comparison of the spectral shape of each of the available weighting functions </p> <a href="/images/hdo_techbrief_spectrum_" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_06.png" alt=""><div class="caption"><h4>Figure 3:</h4> <p>A comparison of the spectral shape of each of the four weighting functions</p></div></a> <h3 class="hdogreen">Advantages of High Definition 12 bit Oscilloscopes in the Frequency Domain</h3> <p>Spectrum analyzers inherently show a wide dynamic range.  Most 8 bit oscilloscopes can only provide 48 dB of dynamic range on a spectrum that can show 80 or more dB of dynamic range.  A 12 bit oscilloscope allows up to 72 dB of dynamic range.  An example is shown in Figure 4.  Here the harmonics of the signal are better than 68 dB below full scale.  With an 8 bit scope they would be invisible without averaging.</p> <a href="/images/hdo_techbrief_spectrum_08.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_08.png" alt=""><div class="caption"><h4>Figure 4:</h4> <p>An example of a signal with greater than 12 bit resolution.  An 8 bit scope would have a noise floor around 50 db below full scale obscuring the harmonics</p></div></a> <h3 class="hdogreen">Peak Search</h3> <p>The Peaks/Markers tab, shown in Figure 5 provides an easy way to find and label up to 100 peaks in the spectrum as well as set up to 20 markers.</p> <p>Pressing the Peaks button on the Peaks/Markers tab allows turns on automatic peak detection and labeling as shown in Figure 5.  A maximum of 100 peaks can be tracked with this feature.  The Show Table check box will, if checked, open a table showing the number of peaks requested in the Max Peaks numeric entry field.  Peaks in the table can be sorted by Frequency or Amplitude as selected in the Sort By field.  Peaks are normally listed by the peak number as shown in the table.  Checking the Show Freq box will add frequency readout to each peak.</p> <a href="/images/hdo_techbrief_spectrum_10.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_10.png" alt=""><div class="caption"><h4>Figure 5:</h4> <p>Setting up the automatic identification of peaks</p></div></a> <p>In the Action area of the Peaks/Marker dialog box any peak can be selected and moved to the center of the screen by pressing apply.</p> <h3 class="hdogreen">Markers</h3> <p>The spectrum Analyzer option has the ability to add up to 20 user set markers.  The Marker controls are accessed by pressing the Markers button in the Peak/Markers dialog box as shown in Figure 6.  Markers are shown as blue, inverted triangles on the spectrum display.  The markers frequency locations and amplitudes are summarized in the accompanying table.  </p> <p>The marker view shortcut buttons provide an easy way to view markers in common locations such as on peaks, harmonics, or five default markers spaced evenly in frequency over the spectrum</p> <p>Marker control allows the location of each, of up to 20 available markers, to be set manually.  The table is linked to the marker and selecting an entry sets the Marker number in the Marker Control Marker field.  Scroll buttons allows the selected marker to be set to the next peak location (left-right scan) or the next amplitude value (up-down scan).  The center frequency of the spectrum display can be set to the selected marker by pressing the Set Center Frequency to Marker button.  Likewise, the reference level can be set to the selected marker amplitude by pressing the Set Ref. Level to Marker.</p> <a href="/images/hdo_techbrief_spectrum_12.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_12.png" alt=""><div class="caption"><h4>Figure 6:</h4> <p>Setting up to 20 markers on the spectrum display</p></div></a> <p>The Marker Measurements control group allows you to select the values included in the peak table.  Checking Show Delta Values from Ref. Marker will add the Delta Frequency and Amplitude columns to the marker table. The Show Abs Values of All Markers causes the absolute frequency and amplitude to be displayed.  These two buttons are interlocked so that there will always be either absolute values, differential values, or both shown.</p> <p>The Track All Markers to Ref Marker locks the locations of the markers to the reference marker.  They will follow any changes in the reference marker location. </p> <h3 class="hdogreen">Spectrogram Display</h3> <p>The spectrogram display shows a history of spectral changes in a separate display grid.  Up to 256 spectra are displayed in a vertically stacked display as shown in Figure 7.</p> <a href="/images/hdo_techbrief_spectrum_14.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_14.png" alt=""><div class="caption"><h4>Figure 7:</h4> <p>A view of the 2D spectrogram display</p></div></a> <p>Controls in the Spectrogram field of the Spectrum Analyzer dialog box are used to turn on and configure the spectrogram display.  The View box turns the display on or off.  The Type entry allows selection of either a two dimensional (2D) or three dimensional (3D) display.  Figure 7 is the monochrome, 2D view where the spectral amplitude is proportional to the display intensity.  By unchecking the Monochrome check box the spectrogram will indicate spectral amplitude by color.  The slider control in the Spectrogram field controls the mapping of both the intensity and color to the amplitude of the spectrum.</p> <p>The 3D rendering of the spectrogram is shown in Figure 8.  The 3D rendering shows the spectral amplitude as a vertical display in addition to the intensity or color grading.</p> <a href="/images/hdo_techbrief_spectrum_16.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_spectrum_16.png" alt=""><div class="caption"><h4>Figure 8:</h4> <p>The 3D rendering of the spectrogram using color graded persistence</p></div></a> <h3 class="hdogreen">Conclusion</h3> <p>The Spectrum Analyzer mode offers the power of FFT analysis with a streamlined user interface similar to RF spectrum analyzers, dedicated to frequency domain analysis.</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=7519#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'Spectrum Mode PDF', {'nonInteraction': 1});"><i class="icon-file"></i>Spectrum Mode</a></li>
              <li><a href="https://www.youtube.com/embed/ZQUrCAEfPfI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Spectrum Mode"><i class="icon-film"></i>Spectrum Mode (1:32)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

   

      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
