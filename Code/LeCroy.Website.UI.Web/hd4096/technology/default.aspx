﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_technology_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
      /*.container{
        background:url('/images/background4096.png') no-repeat;width:960px;height:100%;
      }*/
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
    <body>
<div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="/"><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a> 
        </div>
  </div>
    </div>
<div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <img src="/images/hd4096_badge_skew.png" border="0" />
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->


        <div class="span7">
          <div class="page-header">
            <h1>HD4096 High Definition Technology</h1>
          </div>
                <p>HD4096 high definition technology consists of high sample rate 12-bit ADCs, high signal-to-noise ratio amplifiers and a low-noise system architecture.  This technology enables high definition oscilloscopes to capture and display signals of up to 1 GHz with high sample rate and 16 times more resolution than other oscilloscopes.</p><p>Oscilloscopes with HD4096 technology have higher resolution and measurement precision than 8-bit alternatives. The high sample rate 12-bit ADCs provide high resolution sampling at up to 2.5 GS/s. The high performance input amplifiers deliver phenomenal signal fidelity with a 55 dB signal-to-noise ratio and provide a pristine signal to the ADC to be digitized. The low-noise signal architecture ensures that nothing interferes with the captured signal and the oscilloscope displays a waveform that accurately represents the signals from the device under test.</p>
                <a href="/images/hdo_quantization.png" rel="shadowbox" class="thumbnail" role="thumbnail"  title="16x Closer to Perfect"><img src="/images/hdo_quantization.png" border="0" /><div class="caption"><h4 class="hdogreen">16x More Resolution</h4><p>12-bits of vertical resolution provides sixteen times more resolution than 8-bits. The 4096 discrete levels reduce the quantization error. Signals captured with lower resolution oscilloscopes have a higher level of quantization error resulting in less accurate waveforms on the display. Signals captured on an oscilloscope with 12-bit HD4096 technology are accurately displayed with minimal quantization error.</p></div></a>

                <p>Oscilloscopes with HD4096 have a variety of benefits that allow the user to debug in high definition. Waveforms displayed by high definition oscilloscopes are cleaner and crisper. More signal details can be seen and measured; these measurements are made with unmatched precision resulting in better test results and shorter debug time.</p>

<h4>Clean, Crisp Waveforms</h4>
<p>When compared to waveforms captured and displayed by 8-bit oscilloscopes, waveforms captured with HD4096 technology are dramatically crisper and cleaner.  Oscilloscopes with HD4096 acquire waveforms at high resolution, high sample rate and low noise to display the most accurate waveforms.</p>

<h4>More Signal Details</h4>
<p>Signal details often lost in the noise are clearly visible and easy to distinguish when captured on
oscilloscopes with HD4096. Details which were previously difficult to even see can now be easily seen and measured. Using the oscilloscope zoom capabilities gives an even closer look at the details for unparalleled insight to the signals on screen.</p>

<h4>Unmatched Measurement Precision</h4>
<p>Precise measurements are critical for effective debug and analysis. HD4096 enables  oscilloscopes to deliver unmatched measurement precision to improve testing capabilities and provide better results.</p>

<div class="thumbnail noborder"><img src="/images/hdo_benefits.png" border="0" /><div class="caption"><ol type="A"><li><strong>Clean, Crisp Waveforms</strong><br />Thin traces show the actual waveform with minimal noise interference<br /></li><li><strong>More Signal Details</strong><br />Waveform details lost on an 8-bit oscilloscope can now be clearly seen<br /></li><li><strong>Unmatched Measurement Precision</strong><br />Measurements are more precise and not affected by quantization noise</li></ol></div></div>


            </div>

          
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

     

      <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>
