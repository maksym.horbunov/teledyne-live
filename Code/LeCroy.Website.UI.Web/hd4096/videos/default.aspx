
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../img/favicon.ico">

    <title>Teledyne LeCroy &ndash; Videos &amp; Tech Briefs</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="../css/carousel.css" rel="stylesheet">
    <link href="../css/theme.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script>
        $(function () {
            $("#tabs2").tabs({
                collapsible: true,
                active: 0,
                show: {
                    effect: "fadeIn",
                    duration: 500,
                    delay: 0
                },
                hide: {
                    effect: "fadeOut",
                    duration: 500,
                    delay: 0
                }
            });
            $("#tabs2 li a.tabbed").click(function () {
                $('#tabs2 li').removeClass("active");
                $(this).parent().addClass("active");
                var tabs_offset = $("#tabs2").offset();
                $('html, body').animate({ scrollTop: tabs_offset.top }, 500);
            });
        });
    </script>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="../" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="../12bit/">12-bit High Definition Technology</a></li>
                <li><a href="../hdo/">High Definition Oscilloscopes</a></li>
                <li><a href="../videos/">Videos &amp;Tech Briefs</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div id="tabs">
          <div class="clearfix"></div>
           <h2 class="featurette-heading top">HD4096. <span class="text-muted">High Definition Technology.</span></h2>
          <!--<p class="lead">HD4096 high definition technology consists of high sample rate 12-bit ADCs, high signal-to-noise ratio amplifiers and a low-noise system architecture. This technology enables high definition oscilloscopes to capture and display signals of up to 1 GHz with high sample rate and 16 times more resolution than other oscilloscopes.</p> -->
          <div id="tabs-3" style="margin-top:40px;">
            <div id="tabs2">
              <ul style="list-style:none; padding:0;" class="tabs2">
                <li class="active"><a href="#video-container" class="tdygreen tabbed"><h4 class="tdygreen">Videos</h4></a></li>
                <li><a href="#briefs-container" class="tdygreen tabbed"><h4 class="tdygreen">Tech Briefs</h4></a></li>
              </ul>
              <div id="video-container">
                <div class="col-sm-5 col-lg-5 col-md-5 focus-video padded visible-lg">
                  <a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HDO Overview" onclick="ga('send', 'event', 'HD4096', 'Video', 'HDO Overview', {'nonInteraction': 1});"><p style="position:absolute; top:0; margin-top:15px; padding-left:10px;"><span class="tdygreen" style="font-size:16px; font-weight: 500;">HDO Overview</span></p></a>
                  <a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HDO Overview" onclick="ga('send', 'event', 'HD4096', 'Video', 'HDO Overview', {'nonInteraction': 1});"><img style="z-index:1023; border:1px solid #e2e2e2;"src="../img/hdo-video.jpg" class="img-responsive" alt=""/></a>
                  <p style="position:absolute; bottom:0; padding:10px;padding-right:25px;">High Definition Oscilloscopes combine Teledyne LeCroy&#39;s HD4096 high definition technology with long memory, powerful debug tools, and mixed signal capability in a compact form factor with a 12.1&quot; wide touch screen display.</p>
                </div>
                <div class="col-sm-7 col-lg-7 col-md-7 padded video-container">
                  <ul class="videos">
                  <li class="video-item clearfix hidden-lg">
                    <div class="thumbnail">
                      <a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HDO Overview" onclick="ga('send', 'event', 'HD4096', 'Video', 'HDO Overview', {'nonInteraction': 1});" class="thumbnail noborder"><img src="https://teledynelecroy.com/images/hd4096_vid_01.png" width="120px" alt=""/></a>
                    </div>
                    <div class="content">
                      <a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HDO Overview" onclick="ga('send', 'event', 'HD4096', 'Video', 'HDO Overview', {'nonInteraction': 1});" class="tdygreen underline"><h4>HDO Overview</h4></a>
                      <p>LeCroy&#39;s HD4096 high definition technology with long memory, powerful debug tools, and mixed... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=CIE_vZD7h3A" alt="">View More</a></p>
                    </div>
                  </li>
                  <li class="video-item clearfix">
                    <div class="thumbnail">
                      <a href="https://www.youtube.com/embed/F6xDA3lUZyc?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="History Mode" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'History Mode', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_02.png" width="120px" alt=""/></a>
                    </div>
                    <div class="content">
                      <a href="https://www.youtube.com/embed/F6xDA3lUZyc?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="History Mode" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'History Mode', {'nonInteraction': 1});"><h4>History Mode</h4></a>
                      <p>History mode is a powerful debug feature that provides the ability to scroll back in time to... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=F6xDA3lUZyc" alt="">View More</a></p>
                    </div>
                  </li>
                  <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/qeISsxGzH0c?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Sequence Mode" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Sequence Mode', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_03.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/qeISsxGzH0c?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Sequence Mode" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Sequence Mode', {'nonInteraction': 1});"><h4>Sequence Mode</h4></a>
                        <p>Sequence mode is a powerful acquisition mode, standard in the HDO High Definition oscilloscopes... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=qeISsxGzH0c" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/ZQUrCAEfPfI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Spectrum Analyzer" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Spectrum Analyzer', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_04.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/ZQUrCAEfPfI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Spectrum Analyzer" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Spectrum Analyzer', {'nonInteraction': 1});"><h4>Spectrum Analyzer</h4></a>
                        <p>In comparison to an oscilloscope's time domain analysis tools, the frequency domain provides... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=ZQUrCAEfPfI" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/pFYvgAVOq2A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="WaveScan" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'WaveScan', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_05.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/pFYvgAVOq2A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="WaveScan" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'WaveScan', {'nonInteraction': 1});"><h4>WaveScan</h4></a>
                        <p>WaveScan provides a powerful and fast way to scan waveforms for runts, glitches, or other anomalies... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=pFYvgAVOq2A" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/rZFUU8AOEJI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Power Analyzer" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Power Analyzer', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_06.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/rZFUU8AOEJI?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Power Analyzer" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Power Analyzer', {'nonInteraction': 1});"><h4>Power Analyzer</h4></a>
                        <p>Confirming how a switched-mode power circuit is performing, improving the efficiency or determining... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=rZFUU8AOEJI" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/1dqjn2CDfWE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Logic Gate Emulation" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Logic Gate Emulation', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_07.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/1dqjn2CDfWE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Logic Gate Emulation" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Logic Gate Emulation', {'nonInteraction': 1});"><h4>Logic Gate Emulation</h4></a>
                        <p>Logic Gate Emulation is a powerful debug feature of the HDO-MS Mixed Signal High Definition... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=1dqjn2CDfWE" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/2pCIF1s9R8M?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Measurements on Digital HDO4000" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Measurements on HDO4000', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_09.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/2pCIF1s9R8M?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Measurements on Digital HDO4000" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Measurements on HDO4000', {'nonInteraction': 1});"><h4>Measurements on Digital HDO4000</h4></a>
                        <p>Learn how to use the many timing parameters that are available on the HDO4000-MS Mixed Signal... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=2pCIF1s9R8M" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/7s2Jp6GkGe0?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Measurements on Digital HDO6000" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Measurements on HDO6000', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_10.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/7s2Jp6GkGe0?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Measurements on Digital HDO6000" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Measurements on HDO6000', {'nonInteraction': 1});"><h4>Measurements on Digital HDO6000</h4></a>
                        <p>Learn how to use the many timing parameters that are available on the HDO6000-MS Mixed Signal... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=7s2Jp6GkGe0" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/ETWqRCzWHwY?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Parallel Pattern Search" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Parallel Pattern Search', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_11.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/ETWqRCzWHwY?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Parallel Pattern Search" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Parallel Pattern Search', {'nonInteraction': 1});"><h4>Parallel Pattern Search</h4></a>
                        <p>See how the parallel pattern search capability within WaveScan allows the HDO-MS Mixed Signal... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=ETWqRCzWHwY" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://www.youtube.com/embed/cfR6eAmYvGE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Setting Up Channels" class="thumbnail noborder" onclick="ga('send', 'event', 'HD4096', 'Video', 'Setting Up Channels', {'nonInteraction': 1});"><img src="https://teledynelecroy.com/images/hd4096_vid_12.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://www.youtube.com/embed/cfR6eAmYvGE?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="Setting Up Channels" class="tdygreen underline" onclick="ga('send', 'event', 'HD4096', 'Video', 'Setting Up Channels', {'nonInteraction': 1});"><h4>Setting Up Channels</h4></a>
                        <p>earn how easy it is to set up digital channels on the HDO-MS Mixed Signal High Definition Oscilloscopes... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://www.youtube.com/watch?v=cfR6eAmYvGE" alt="">View More</a></p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div id="briefs-container">
                <div class="col-sm-12 col-lg-12 col-md-12 padded" style="min-height:300px;">
                  <ul class="techbriefs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/history/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_history_02.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/history/" alt="" class="tdygreen underline"><h4>History Mode</h4></a>
                        <p>History mode is a powerful debug feature that provides the ability to scroll back in time to effectively isolate... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/history/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/sequence/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_sequence_04.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/sequence/" alt="" class="tdygreen underline"><h4>Sequence Mode</h4></a>
                        <p>Sequence Mode enables capture of fine details of complex event sequences occurring over long time intervals, while... .<a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/sequence/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/spectrum/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_spectrum_02.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/spectrum/" alt="" class="tdygreen underline"><h4>Spectrum Analyzer</h4></a>
                        <p>A Spectrum Analyzer allows users who are familiar with RF spectrum analyzers to start using the FFT with little or no... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/spectrum/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/wavescan/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_power_02.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/wavescan/" alt="" class="tdygreen underline"><h4>WaveScan</h4></a>
                        <p>WaveScan is an advanced search and analysis tool that provides the ability to quickly locate and... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/wavescan/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/power/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_power_02.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/power/" alt="" class="tdygreen underline"><h4>Power Analyzer</h4></a>
                        <p>Power Analyzer allows all aspects of switched mode power devices to be measured in a single, highly... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/power/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/logic-gate-emulation/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/hdo_techbrief_logic_gate_05.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/logic-gate-emulation/" alt="" class="tdygreen underline"><h4>Logic Gate Emulation</h4></a>
                        <p>Logic-gate emulation is a useful debugging tool which allows users to simulate and debug complete... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/logic-gate-emulation/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/digital-measurements-hdo4000/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/digital-measuremnts-hdo4000_03.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/digital-measurements-hdo4000/" alt="" class="tdygreen underline"><h4>Measurements on Digital HDO4000</h4></a>
                        <p>With the 16 digital channels available on Teledyne LeCroy&#39;s HDO4000 Mixed Signal High Definition... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/digital-measurements-hdo4000/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/digital-measurements-hdo6000/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/digital-measuremnts-hdo6000_03.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/digital-measurements-hdo6000/" alt="" class="tdygreen underline"><h4>Measurements on Digital HDO6000</h4></a>
                        <p>With the 16 digital channels available on Teledyne LeCroy&#39;s HDO4000 Mixed Signal High Definition... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/digital-measurements-hdo6000/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/parallel-pattern-search/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/digital-measuremnts-hdo6000_03.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/parallel-pattern-search/" alt="" class="tdygreen underline"><h4>Parallel Pattern Search</h4></a>
                        <p>The combination of Teledyne LeCroy&#39;s Mixed Signal High Definition Oscilloscopes and their 16... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/parallel-pattern-search/" alt="">View More</a></p>
                      </div>
                    </li>
                    <hr class="featurette-divider visible-xs">
                    <li class="video-item clearfix">
                      <div class="thumbnail">
                        <a href="https://teledynelecroy.com/hd4096/setting-up-channels/" alt=""><span class="glyphicon glyphicon-file document"></span><img style="display:none;" src="https://teledynelecroy.com/images/setting_channels_02.png" width="120px" alt=""/></a>
                      </div>
                      <div class="content">
                        <a href="https://teledynelecroy.com/hd4096/setting-up-channels/" alt="" class="tdygreen underline"><h4>Setting Up Channels</h4></a>
                        <p>Teledyne LeCroy&#39;s Mixed Signal High Definition Oscilloscopes provide 16 digital signal inputs... <a class="tdygreen pull-right" style="margin-right:15px;" href="https://teledynelecroy.com/hd4096/setting-up-channels/" alt="">View More</a></p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
      </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2014&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
        Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
