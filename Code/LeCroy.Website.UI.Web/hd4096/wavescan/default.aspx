﻿<%@ Page Title="HD4096 - Teledyne LeCroy" Language="vb" AutoEventWireup="true" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.hd4096_wavescan_default" %>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>HDO | High Definition Oscilloscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDO High Definition Oscilloscope. 12-bit Oscilloscopes">
    <meta name="author" content="Teledyne LeCroy">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,700' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
body {
  padding-top: 60px;
  padding-bottom: 40px;
}
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--  icons -->
    <link rel="shortcut icon" href="https://teledynelecroy.com/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/shadowbox/shadowbox.css">
    </head>
  <body>

    <div class="navbar navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/"><img src="/images/tl_weblogo_blkblue_189x30.png" width="189" height="30" border="0" /></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span3">
        <div class="sidebar-nav-fixed" >
          <a href="/hd4096/"><img src="/images/hd4096_badge_skew.png" border="0" /></a>
            <ul class="nav nav-list well small">
                <li class="nav-header">Learn what<br />the HDO can do</li>
                <li><a href="https://www.youtube.com/embed/CIE_vZD7h3A?rel=0&amp;showinfo=0"  rel="shadowbox;width=853;height=480;" title="HD4096"><i class="icon-film"></i>&nbsp;&nbsp;HDO Overview</a></li>
                <li><a href="/hd4096/history/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;History Mode</a></li>
                <li><a href="/hd4096/sequence/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Sequence Mode</a></li>
                <li><a href="/hd4096/spectrum/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Spectrum Analyzer</a></li>
                <li><a href="/hd4096/wavescan/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;WaveScan</a></li>
                <li><a href="/hd4096/power/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Power Analyzer Option</a></li>
                <li><a href="/hd4096/logic-gate-emulation/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Logic Gate Emulation</a></li>
                <li><a href="/hd4096/digital-measurements-hdo4000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO4000</a></li>
                <li><a href="/hd4096/digital-measurements-hdo6000/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Measurements on Digital HDO6000</a></li>
                <li><a href="/hd4096/parallel-pattern-search/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Parallel Pattern Search</a></li>
                <li><a href="/hd4096/setting-up-channels/"><i class="icon-file"></i><i class="icon-film"></i>&nbsp;&nbsp;Setting Up Channels</a></li>
            </ul>
        </div>
        </div><!--/span-->



        <div class="span7">
          <div class="page-header">
            <h1>WaveScan Mode in HDO Oscilloscopes </h1>
          </div>

          <h3 class="hdogreen">Introduction</h3> <p>Teledyne LeCroy’s HDO4000 and HDO6000 oscilloscopes offer a large complement of powerful hardware triggers, but even the most advanced hardware triggers carry limitations in that they can find an event predetermined by the user. Thus, the HDO4000 and HDO6000 oscilloscopes are augmented with WaveScan Search and Find technology. WaveScan is an advanced search and analysis tool that provides the ability to quickly locate and characterize unusual events. WaveScan can detect anomalies within a single capture and can also operate in continuous scanning mode, constantly monitoring incoming waveforms. There are many search criteria to choose from, including non-monotonic edge detection, runt mode, and a large number of parametric measurement criteria, including rise time, fall time, frequency, pulse width, duty cycle, and many others.</p> <h3 class="hdogreen">Getting Started with WaveScan</h3> <p>The WaveScan feature can be accessed from the front panel or through the Analysis pull-down menu. Using Teledyne LeCroy’s WaveSource 100 evaluation source to input a signal with a slow rise time, the screen will look similar to Figure 1. </p> <a href="/images/hdo_techbrief_wavescan_02.png" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_wavescan_02.png" alt=""><div class="caption"><h4>Figure 1:</h4> <p>The initial setup screen for WaveScan is the starting point for deep signal exploration</p></div></a> <p>In Figure 1, WaveScan is still in its default state, which is edge mode, and edge settings are for a positive slope where the edge threshold is 50% of the signal amplitude. Note that all such edges in the input signal are highlighted in red. The first edge in the signal trace is brighter, indicating highlighting, and that highlighted section appears in the WaveScan zoom view below.</p> <h3 class="hdogreen">Dig Deeper Into Waveforms</h3> <p>When using WaveScan, note that the zoomed area of the signal trace may be manipulated using the HDO zoom controls. In addition, WaveScan’s powerful controls enable searching for a wide range of anomalies within the signal. Opening the Mode menu within the WaveScan dialog reveals numerous options, including searching for edges, runts, non-monotonic edges, serial or bus patterns, and measurements. Each mode can be further customized to specify the precise nature of the anomaly. </p> <p>With the Table option enabled, WaveScan will display all occurrences of the condition being searched for. In the example of Figure 2, the WaveScan table shows the times at which each of the edges in the trace occurred relative to the cursor. Tapping any of the lines in the table changes the zoom view to the corresponding event in the signal trace. In this case, the zoom view is of the sixth edge shown in Channel 1.</p> <a href="/images/hdo_techbrief_wavescan_" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_wavescan_04.png" alt=""><div class="caption"><h4>Figure 2:</h4> <p>Tapping any line in the WaveScan table changes the zoom view to the corresponding event</p></div></a> <h3 class="hdogreen">Measurement Filters</h3> <p>To further refine searches for signal anomalies, WaveScan’s measurement mode provides a measurement filter. This allows for measurement criteria to include attributes such as less than, greater than, within or outside of a limit, plus or minus a delta, plus or minus a percentage, or the rarest events. Users may also invoke a filter wizard, which can establish limits based on statistical distributions of the data set (such as ±1 sigma). As shown in Figure 3, one may, for example, direct WaveScan to find the rarest events in the signal trace, and define what</p> <p>event is being sought. In this example, WaveScan has been charged with finding the edges with the shortest and longest rise times. Clicking on that result in the table highlights the corresponding edge. WaveScan can also direct the HDO to take specific actions upon finding the sought-after event, including stopping acquisitions, saving waveform(s), printing the screen, or simply beeping, among others. Note also in Figure 3 that statistics have been turned on in the Measurement Setup menu, as have histicons and the start/stop times for the edges. </p> <a href="/images/hdo_techbrief_wavescan_" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_wavescan_06.png" alt=""><div class="caption"><h4>Figure 3:</h4> <p>Shown is WaveScan in Measurement mode and filtering for the smallest and largest rise times in the signal trace</p></div></a> <h3 class="hdogreen">Histograms</h3> <p>Another valuable tool available within WaveScan is statistical analysis in the form of histograms, which can help characterize random events. Histograms can be accessed from the WaveScan dialog box by checking the Histogram box. Users have complete manual control of the nature of the random events to be analyzed, or a Filter Wizard may be invoked to automate the process as shown in Figure 4. In this case, the Filter Wizard has been configured to Find Rare Events that fall outside a delta. </p> <a href="/images/hdo_techbrief_wavescan_" rel="lightbox" class="thumbnail"><img src="/images/hdo_techbrief_wavescan_08.png" alt=""><div class="caption"><h4>Figure 4:</h4> <p>WaveScan’s Histogram function enables HDO6000 oscilloscopes to create a persistence display of only the results that meet the users’ predefined criteria. This is shown in the blue trace in the center grid</p></div></a> <h3 class="hdogreen">Scan Overlay</h3> <p>In the HDO6000, the scan overlay function may be activated from the WaveScan menu. Unlike a standard persistence display, WaveScan evaluates the data first, then includes only those pulses in the scan overlay persistent display which have met the user-defined criteria, as shown in Figure 4. Scan Histo is a special type of measurement histogram which includes only the results meeting the user defined criteria. </p> <p>For example, in scanning for rise time anomalies on a system clock, the Scan Histo can be configured to record only those rise times with values greater than a specified limit. WaveScan builds on the Teledyne LeCroy legacy of fast data processing and will quickly scan millions of events, looking for unusual occurrences and display the results with unprecedented speed.</p> <p>In continuous scanning mode, software trigger actions can be set to take place when an event is found. Some of the choices include stopping the acquisition, saving waveforms, saving the screen image, creating a LabNotebook entry, beeping to get user’s attention, generating an output pulse that can be used by other instruments, or automatically sending an email. </p> <p>WaveScan capability is not limited to input channels. Scans can also be performed on memory traces, math functions, and chains of math or measurement operators, allowing second or third level analysis to be performed. For example, the first derivative of the waveform edge is computed, and WaveScan is scanning for non-monotonic edges. However, the input source is not channel one; it’s the first derivative math operator. Therefore, WaveScan is verifying the linearity of the rate of change.</p> <h3 class="hdogreen">Conclusion</h3> <p>WaveScan adds considerable functionality to the HDO4000 and HDO6000 beyond what is offered by any hardware trigger. While a hardware trigger only finds one event per acquisition, WaveScan identifies all events which meet the criteria. Also, the types of waveform activity that can be found by WaveScan are more in-depth than that offered by hardware triggers. This allows users to accumulate a data set of unusual events that are separated by hours, minutes, or even days, enabling comprehensive debugging.</p>


          </div>
        <!--/span-->
         <div class="span2">
          <div class="well well-small sidebar-nav-fixed">
            <ul class="nav nav-list">
              <li class="nav-header">Additional Material</li>
              <li><a href="<%=rootDir %>/doc/docview.aspx?id=7516#pdf" onClick="ga('send', 'event', 'HDO', 'Side Link', 'WaveScan PDF', {'nonInteraction': 1});"><i class="icon-file"></i>WaveScan</a></li>
              <li><a href="https://www.youtube.com/embed/pFYvgAVOq2A?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="WaveScan"><i class="icon-film"></i>WaveScan (1:10)</a></li>
              
              <li class="divider"></li>
              <li class="nav-header">Webinar</li>
              <li><a href="https://www.youtube.com/embed/GlEJpDDQW2I?rel=0&amp;showinfo=0" role="thumbnail" rel="shadowbox;width=853;height=480;" title="HD4096 Webinar"><i class="icon-film"></i>HD4096 Technology and High Definition Oscilloscopes</a></li>
              <li class="divider"></li>
              <li><a href="<%=rootDir %>/hdo/" onClick="ga('send', 'event', 'HDO', 'Side Link', '/HDO', {'nonInteraction': 1});"><i class="icon-info-sign"></i>HDO Product Details</a></li>
            </ul>
          </div>
        </div>


       </div> <!--/row-->

    


     <footer>
    <div class="container">
    <hr>
      <p>
        &copy; 2013 Teledyne LeCroy&nbsp;&nbsp;&nbsp;
        <a href="https://facebook.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://twitter.com/teledynelecroy" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://youtube.com/lecroycorp" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://blog.teledynelecroy.com/" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" width="25px" height="25px" /></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/10007" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" width="25px" height="25px" /></a>
      </p>
    </div>
  </footer>
    </div>
<!-- /container --> 

<!--  javascript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/assets/js/jquery.js"></script> 
<script src="/assets/js/bootstrap-transition.js"></script> 
<script src="/assets/js/bootstrap-alert.js"></script> 
<script src="/assets/js/bootstrap-modal.js"></script> 
<script src="/assets/js/bootstrap-dropdown.js"></script> 
<script src="/assets/js/bootstrap-scrollspy.js"></script> 
<script src="/assets/js/bootstrap-tab.js"></script> 
<script src="/assets/js/bootstrap-tooltip.js"></script> 
<script src="/assets/js/bootstrap-popover.js"></script> 
<script src="/assets/js/bootstrap-button.js"></script> 
<script src="/assets/js/bootstrap-collapse.js"></script> 
<script src="/assets/js/bootstrap-carousel.js"></script> 
<script src="/assets/js/bootstrap-typeahead.js"></script> 
<script type="text/javascript" src="/assets/plugins/shadowbox/shadowbox.js"></script> 
<script type="text/javascript">
    Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
