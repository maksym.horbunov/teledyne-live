﻿<%@ Page Language="VB"  AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.HDO_default" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width,initial-scale=1"/>
  <title>High Definition Oscilloscopes | Teledyne LeCroy</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
 table.centered tbody tr td{padding: 0; line-height: 38px;}

  </style>
<meta property="og:title" content="Teledyne LeCroy High Definition Oscilloscopes"/>
<meta property="og:description" content="High Definition Oscilloscopes (HDO) with HD4096 Technology capture and display signals from 200 MHz up to 8 GHz with high sample rate and 12 bits of resolution all the time."/> 
<meta property="og:url" content="https://teledynelecroy.com/hdo/" /> 
<meta property="og:image" content="https://assets.lcry.net/images/hdo-og-image.png"/>
<meta property="og:type" content="website" />
</head>
<body>
  <div class="container">
      <div class="nav-wrapper white">
        <a href="/" class="brand-logo"><img src="/images/tl_weblogo_blkblue_189x30.png"></a>
      </div>
  </div>
<div id="index-banner" class="container">
      <div class="section">
      <div class="row">
        <div class="col l6 m8 s12"><h1 class="h1 green-text accent-4">High Definition Oscilloscopes</h1>
            <p class="black-text" style="font-size: 1.3rem;">High Definition Oscilloscopes (HDO) use <a href="#hd4096" class="green-text accent-4">HD4096 Technology</a> to provide <strong>12 bits of resolution all the time</strong> from 
200 MHz up to 8 GHz.</p>
            <ul class="h5 black-text" style="padding-left:20px;font-size: 1.3rem;"><li style="list-style-type:square">No tradeoff of resolution, sample rate, bandwidth</li>
                <li style="list-style-type:square">Clean, crisp waveforms</li>
                <li style="list-style-type:square">More signal details</li>
                <li style="list-style-type:square">Unmatched measurement precision</li>
            </ul>
            <p class="black-text"style="font-size: 1.3rem;">Once you use a Teledyne LeCroy HDO, you’ll never want to go back.</p>
            <p>
                <a style="margin: 5px 0;" class="waves-effect waves-light btn-large  blue darken-2  dropdown-button" data-activates="datasheet-dropdown" href="/doc/docview.aspx?id=9455"><i class="material-icons left">info</i>Datasheet</a>&nbsp;

                <a style="margin: 5px 0;"  class="waves-effect waves-light btn-large green accent-4 dropdown-button" data-activates="configure-dropdown" href="/oscilloscope/configure/configure_step2.aspx?seriesid=511"><i class="material-icons left">settings</i>Configure</a>&nbsp;<a style="margin: 5px 0;"  class="waves-effect waves-light btn-large green accent-4" href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x"><i class="material-icons left">file_download</i>Download White Paper</a></p>
<!-- Dropdown Structure -->
                    <ul id='datasheet-dropdown' class='dropdown-content white green-text text-accent-4'>
                        <li><a href="/doc/wavesurfer-4000hd-datasheet">WaveSurfer 4000HD Datasheet</a></li>
                        <li><a href="/doc/docview.aspx?id=10018">HDO6000A Datasheet</a></li>
                        <li><a href="/doc/waverunner-8000hd-datasheet">WaveRunner 8000HD Datasheet</a></li>
                        <li><a href="/doc/waveprohd-datasheet">WavePro HD Datasheet</a></li>
                    </ul>
                    <ul id='configure-dropdown' class='dropdown-content white'>
                        <li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=609">WaveSurfer 4000HD Configure</a></li>
                        <li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=551">HDO6000A Configure</a></li>
                        <li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=607">WaveRunner 8000HD Configure</a></li>
                        <li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=566">WavePro HD Configure</a></li>
                    </ul>

        </div>
        <div class="col l6 m8 s12">
            <a href="#chart"> <img src="https://assets.lcry.net/images/hdo-group-shot-slider.png"></a>
        </div>
      </div>
      </div>
    </div>

<div class="clearfix"></div>
  <div class="wrap"><div class="container">
    <div class="section" id="videos">
            <div class="row">
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_pdj1cnubp8 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_jlen8kljmq popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_2kced34urr popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
            </div>
            </div></div></div>
<div class="clearfix"></div>

    <div class="container">
    <div class="section"
         id="hd4096">
      <div class="row">
        <div class="col m8 s12">
          <h3 class="green-text accent-4">HD4096 Technology<br />12 bits <strong>all the time</strong></h3>
          <p>Teledyne LeCroy High Definition Oscilloscopes use a systems design approach to achieve high resolution and low noise all the time. Other manufacturers use software post-processing, a single high-resolution component, or a combination of both with reduced overall performance. Only Teledyne LeCroy gives you 12 bits all the time.</p>
        </div>
        <div class="col m4 s12 center"><img class="responsive-img"
             src="/images/wr8khd_02.png"
             width="230" alt="hd4096 badge"><br /><a class="waves-effect waves-light btn-large green accent-4" href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x">Download White Paper</a></div>
      </div>
        <div class="row">
    <div class="col s12"><img class="materialboxed responsive-img" src="/images/hdo12.png" alt="high definition oscilloscope hd4096"></div>
    </div>

        <div class="divider"></div>
        <div class="row">
        <div class="col m8 s12">
          <h3 class="green-text accent-4">HD4096 Technology<br />Systems Approach</h3>
          <p>HD4096 High Definition Technology consists of high sample rate 12-bit ADCs, high signal-to-noise ratio amplifiers and a low-noise system architecture. This total system approach provides an ideal signal path to ensure that signal details are delivered accurately to the oscilloscope display. </p>
        </div>
        <div class="col m4 s12 center"><img class="responsive-img"
             src="/images/hd4096-itransport.png"
             width="230" alt="hd4096 itransport"></div></div>
      <div class="row">
    <div class="col m4 s12"><div class="card-panel small"><p style="font-size:1.3rem;"><button class="btn-floating btn green">A</button>Clean, Crisp Waveforms</p>
        <p>Waveforms captured and displayed on oscilloscopes with HD4096 technology are cleaner and crisper.</p></div></div>
    <div class="col m4 s12"><div class="card-panel small"><p style="font-size:1.3rem;"><button class="btn-floating btn green">B</button> More Signal Details</p>
        <p>Signal details often lost in the noise are clearly visible and easy to distinguish.</p></div></div>
    <div class="col m4 s12"><div class="card-panel small"><p style="font-size:1.3rem;"><button class="btn-floating btn green">C</button> Unmatched Precision</p>
        <p>HD4096 enables oscilloscopes to deliver unmatched measurement precision for improved debug and analysis.</p></div></div>
    </div>
        
        <div class="row">
    <div class="col s6"><img class="materialboxed responsive-img" src="https://teledynelecroy.com/hd4096/img/why12bit_4.png" alt="high definition oscilloscope 8-bit"></div>
    <div class="col s6"><img class="materialboxed responsive-img" src="https://teledynelecroy.com/hd4096/img/why12bit_5.png" alt="high definition oscilloscope 12-bit"></div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
  <div class="wrap">
<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 center">
<p>The 4096 discrete vertical levels reduce the quantization error compared to 256 vertical levels.<br />This improves the accuracy and precision of the signal capture and increases measurement confidence.</p>
</div>  
    <div class="center"><img class="materialboxed responsive-img" style="display:inline-block; width: 50%;" src="/images/hdo-graphic-key.jpg"></div>  
    </div>
    </div>      
  </div>
</div>
<div class="clearfix"></div>
<div class="container">
  <div class="section" id="chart">
    <div class="row">
      <div class="col s18 m12 center">
      <h3>Compare High Definition Oscilloscopes</h3>
      <p class="center-align"><img class="responsive-img" src="/images/hdo-family-large.png" style="margin: auto;"></p>
      <table class="striped">
        <thead>
          <tr style="background-color: rgba(92, 193, 81, 1); color: white; border-bottom-color: white;">
              <th>&nbsp;</th>
              <th>WaveSurfer 4000HD</th>
              <th>HDO6000A</th>
              <th>WaveRunner 8000HD</th>
              <th>WavePro HD</th>
          </tr>
        </thead>

        <tbody>
          <tr style="background-color: rgba(92, 193, 81, 0.15)">
            <td><b>HD Technology</b></td>
            <td>HD4096<br>12 bits</td>
            <td>HD4096<br>12 bits</td>
            <td>HD4096<br>12 bits</td>
            <td>HD4096<br>12 bits</td>            
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.05)">
            <td><b>Bandwidth</b></td>
            <td>200 MHz - 1 GHz</td>
            <td>350 MHz - 1 GHz</td>
            <td>350 MHz - 2 GHz</td>
            <td>2.5 GHz - 8 GHz</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.15)">
            <td><b>Input Channels</b></td>
            <td>4</td>
            <td>4</td>
            <td>8</td>
            <td>4</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.05)">
           <td><b>Sample Rate</b></td>
            <td>5 GS/s</td>
            <td>10 GS/s</td>
            <td>10 GS/s</td>
            <td>20 GS/s</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.05)">
           <td><b>Max. Acquisition Memory</b></td>
            <td>25 Mpts</td>
            <td>250 Mpts</td>
            <td>5 Gpts</td>
            <td>5 Gpts</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.15)">
          <td><b>Analysis Capability</b></td>
            <td>Basic</td>
            <td>Advanced</td>
            <td>Advanced</td>
            <td>Advanced</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.05)">
            <td><b>Serial Data Tools</b></td>
            <td>TD</td>
            <td>TDME, QPHY</td>
            <td>TDME, SDAIII, QPHY</td>
            <td>TDME, SDAIII, QPHY</td>
          </tr>
          <tr style="background-color: rgba(92, 193, 81, 0.15)">
          <td><b>User Experience</b></td>
            <td>MAUI with OneTouch</td>
            <td>MAUI with OneTouch</td>
            <td>MAUI with OneTouch</td>
            <td>MAUI with OneTouch</td>
          </tr>

          <tr style="background-color: rgba(92, 193, 81, 0.05)">
            <td></td>
            <td><a href="/oscilloscope/wavesurfer-4000hd-oscilloscopes" class="waves-effect waves-light btn green accent-4">WaveSurfer 4000HD &raquo;</a></td>
            <td><a href="/oscilloscope/hdo6000a-high-definition-oscilloscopes" class="waves-effect waves-light btn green accent-4">HDO6000A &raquo;</a></td>
            <td><a href="/oscilloscope/waverunner-8000hd-oscilloscopes" class="waves-effect waves-light btn green accent-4">WaveRunner 8000HD &raquo;</a></td>
            <td><a href="/oscilloscope/wavepro-hd-oscilloscope" class="waves-effect waves-light btn green accent-4">WavePro HD &raquo;</a></td>
          </tr>

        </tbody>
      </table>
      </div>     

    </div>
  </div>
</div>
  






<footer class="page-footer white">
      <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2019&nbsp;Teledyne LeCroy</p>

    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
      </div>
  </footer>


  <!--  Scripts-->
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/init.js"></script>
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
      <script>
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>

  </body>
</html>
