﻿<%@ Page Language="VB"  AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.HDO_default" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>HDO Family Oscilloscopes | Teledyne LeCroy</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
  <style type="text/css">
    .orange2 {
    background-color:#ff8e00;
    }
    .maui-blue{
    color:#0076c0;
    }
    table.centered tbody tr td{padding: 0; line-height: 38px;}
  </style>
</head>
<body>

<div class="container">
  <nav>
    <div class="nav-wrapper white">
      <a href="/" class="brand-logo"><img src="/images/tl_weblogo_blkblue_189x30.png" alt="Teledyne LeCroy logo"></a>
       
    </div>
  </nav>
</div>

<div id="index-banner" class="container">
  <div class="section">
    <div class="row">
      <div class="col l6 m8 s12"><p class="h1 green-text accent-4">HDO8000 / MDA800 High Definition Oscilloscopes</p>
        <p>HDO-A oscilloscopes feature Enhanced Sample Rate technology (10x oversampling) that automatically ensures optimal display of acquired waveforms to the instruments' full rated bandwidth. Additionally, the next-generation MAUI® with OneTouch user interface makes measurement setup insanely easy and provides users dramatically faster time to insight when debugging complex signal abnormalities. Lastly, the updated PC systems with faster processors, more CPU memory, and solid-state drives add even more value.<br /><br />With the HDO-A introduction, the world's highest definition oscilloscopes become even more powerful and productive with 10x oversampling, the most efficient and intuitive user interface, and a supercharged PC system.</p>
        <a class="waves-effect waves-light btn-large  blue darken-2"  href="/doc/docview.aspx?id=10020"><i class="material-icons left">info</i>HDO8000A Datasheet</a>
        &nbsp;
        <a class="waves-effect waves-light btn-large  blue darken-2"  href="/doc/docview.aspx?id=10024"><i class="material-icons left">info</i>MDA800A Datasheet</a>
      </p>
      </div>
      <div class="col l6 m8 s12"><img class="right responsive-img" src="/images/hdo8000a-1000x1000.png" alt="HDO8000A Oscilloscope"></div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="wrap">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s6">
          <div class="card horizontal">
            <div class="card-stacked">
              <div class="card-content" style="height:395px">
                <h4>"A" Model upgrades include:</h4>
                <ul>
                  <li style="list-style:disc;margin-left:15px;font-size:1.25em">Upgraded CPU</li>
                  <li style="list-style:disc;margin-left:15px;font-size:1.25em">Upgraded CPU RAM</li>
                  <li style="list-style:disc;margin-left:15px;font-size:1.25em">240 GB solid-state drive (SSD) to replace the hard-disk drive (HDD)</li>
                  <li style="list-style:disc;margin-left:15px;font-size:1.25em">10 GS/s Enhanced Sample Rate mode</li>
                  <li style="list-style:disc;margin-left:15px;font-size:1.25em">New MAUI with OneTouch user interface</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col s6">
          <div class="card horizontal">
            <div class="card-stacked">
              <div class="card-content">
                <iframe src="https://go.teledynelecroy.com/l/48392/2017-09-07/5mfy3n" width="100%" height="350" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="page-footer white">
  <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"><img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px"></a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"><img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px"></a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"><img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px"></a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"><img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px"></a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"><img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px"></a>
    </p>
    <p>&copy; 2017&nbsp;Teledyne LeCroy</p>
    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |
    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>
    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a></p>
  </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/init.js"></script>
  <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
      m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
  </script>
  <script src="https://teledynelecroy.com/js/pardot.js"></script>

</body>
</html>