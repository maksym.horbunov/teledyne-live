<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.HRO_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop" style="background: url(/images/hro/hro_background.png) no-repeat; background-color: #cdd0d1;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="370" valign="top">
                    <img src="<%=rootDir %>/images/spacer.gif" width="302" height="115" alt="WaveRunner HRO - High Resolution Oscilloscope" />  
                    <p style="width: 370px;">With <strong>16 times</strong> more resolution, unmatched accuracy and impressive signal fidelity, Teledyne LeCroy <strong>High Resolution Oscilloscopes</strong> are a new breed of instrument delivering up to 600 MHz bandwidth, 2 GS/s sample rate and <strong>true 12 bit vertical resolution</strong>.</p>
                    <table border="0"><tr><td><a href="https://www1.gotomeeting.com/register/750193401" onClick="ga('send', 'event', 'HRO', 'Button', 'Webinar', {'nonInteraction': 1});"><img src="/images/hro/view_hro.png" border="0" /></a></td><td>&nbsp;</td><td><a href="/oscilloscope/demo.aspx?mseries=388" onClick="ga('send', 'event', 'HRO', 'Button', 'Webinar', {'nonInteraction': 1});"><img src="/images/hro/request_hro.png" border="0" /></a></td></tr></table>                            
                </td>
                <td width="208" valign="top"></td>
                <td width="203">
                    <div class="subNav">
                        <ul>
                            <li><a href="<%=rootDir %>/oscilloscope/oscilloscopeseries.aspx?mseries=388" onClick="ga('send', 'event', 'HRO', 'Side Link', 'Explore WaveRunner HRO', {'nonInteraction': 1});">Explore HRO Oscilloscopes</a></li>
                            <li><a href="<%=rootDir %>/files/pdf/hro-12bit_datasheet.pdf" onClick="ga('send', 'event', 'HRO', 'Side Link', 'HRO Datasheet', {'nonInteraction': 1});">HRO Datasheet</a></li>
                            <li><a href="/oscilloscope/configure/configure_step2.aspx?seriesid=388" onClick="ga('send', 'event', 'HRO', 'Side Link', 'Request Quote', {'nonInteraction': 1});">Request Quote</a></li>
                        </ul>
					</div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <h2>16x More Vertical Resolution</h2>
                    <p class="pad15">The true 12 bit ADC digitizes signals using 4096 levels, 16 times more levels than an 8 bit ADC with only 256.</p>
                    <h2>Unmatched Accuracy</h2>
                    <p class="pad15">Higher resolution means the most accurate waveforms, most precise measurements and most reliable results.</p>
                    <h2>Impressive Signal Fidelity</h2>
                    <p class="pad15">The pristine signal path produces high signal-to-noise performance and impressive waveform clarity.</p>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top">
                    <h1>Experience the true 12-bit Advantage.</h1>
                    <iframe width="400" height="225" src="https://www.youtube.com/embed/WJ9fF6j3HM8" frameborder="0" allowfullscreen></iframe>
                </td>
            </tr>
        </table>
        <div class="rounded2">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left" valign="top"><img src="/images/hro/hrotall.png" alt="HRO" border="0" /></td>
                    <td width="40">&nbsp;</td>
                    <td align="left" class="right" valign="middle">
                        <br /><br /><br /><br /><h1>HRO 12-bit Oscilloscopes</h1>
                        <p>The <strong>Teledyne LeCroy HRO Oscilloscopes</strong> offer high resolution and a powerful feature set that includes a wide range of application packages, advanced triggering to isolate events, a wide range of probing options, and lightning-fast performance.</p>
                        <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=388" onclick="ga('send', 'event', 'HRO', 'Button', 'WaveRunner HRO', {'nonInteraction': 1});"><img src="/images/hro/explore_hro.png" border="0" /></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>