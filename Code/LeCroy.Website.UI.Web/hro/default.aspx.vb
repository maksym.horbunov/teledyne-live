Imports System
Imports LeCroy.Library.VBUtilities

Partial Class HRO_default
    Inherits BasePage
    Public Shared menuURL As String = ""

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - HRO - High Resolution Oscilloscope"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.PRODUCT_CAPTION_ID
        Dim menuID As String

        '** MenuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
        End If
    End Sub
End Class