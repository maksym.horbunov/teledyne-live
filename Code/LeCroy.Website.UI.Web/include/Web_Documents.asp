<%'This document retrieves all the current available application notes from the DB %>
<% topic=Request.QueryString("Topic")
   menuid=Request.QueryString("menuid")
   flgConfig=false %>

<% 'lastselected = topic %>
<% Set rsTopic = Server.CreateObject("ADODB.Recordset")
	strSQL = "SELECT * FROM TOPIC WHERE TOPIC_ID=" & topic
	
	on error resume next
	rsTopic.Open strSQL, dbconnstr()
	if Err.number <> 0 then
		Response.Write ""
	else
		TopicName=rsTopic("NAME")
	end if
	
	if len(TopicName)=0 then
		if len(menuid)>0 then
			if isnumeric(menuid)then
				set rsMenuname=server.CreateObject("ADODB.Recordset")
				strSQL="Select * from MENU_MAIN where MENU_ID=" & menuid
				
				rsMenuname.open strSQL, dbconnstr()
				if not rsMenuname.EOF then
				  TopicName=rsMenuname("MENU_NAME")
				  if UCASE(cstr(rsMenuname("CONFIGURABLE")))="Y"  then
					flgConfig=true
				  end if		
				else
				 Response.Write ""
				end if
			else
				TopicName="All Topics"
			end if
		else
			TopicName="All Topics"
		end if
	end if
%>

<% Set rsDocument = Server.CreateObject("ADODB.Recordset")
	if topic=0 then
		if len(menuid)>0 then
			if isnumeric(menuid) then 
				strSQL = "SELECT * FROM WEB_DOCUMENTS WHERE COUNTRY_ID = '" & country_id & "' AND POST = 'Y' AND TOPIC_ID in (select TOPIC_ID from TOPIC WHERE MENU_ID=" & menuid & ") ORDER BY DATE_ADDED DESC"
			else
				strSQL = "SELECT * FROM WEB_DOCUMENTS WHERE COUNTRY_ID = '" & country_id & "' AND POST = 'Y' AND (TOPIC_ID <> 16)AND (TOPIC_ID <> 18)AND (TOPIC_ID <> 19) AND (TOPIC_ID <> 20)AND (TOPIC_ID <> 21) ORDER BY DATE_ADDED DESC"
			end if
		else
			strSQL = "SELECT * FROM WEB_DOCUMENTS WHERE COUNTRY_ID = '" & country_id & "' AND POST = 'Y' AND (TOPIC_ID <> 16)AND (TOPIC_ID <> 18)AND (TOPIC_ID <> 19) AND (TOPIC_ID <> 20)AND (TOPIC_ID <> 21) ORDER BY DATE_ADDED DESC"
		end if
	else
		strSQL = "SELECT * FROM WEB_DOCUMENTS WHERE COUNTRY_ID = '" & country_id & "' AND POST = 'Y' and TOPIC_ID = '" & topic & "' ORDER BY DATE_ADDED DESC"
	end if
	'Response.Write strSQL
	on error resume next
	rsDocument.Open strSQL, dbconnstr()
	if Err.number <> 0 then
	Response.Write "Being Updated"
	else
%>

<%'Count Total Records
	rsDocument.MoveFirst
	While Not rsDocument.EOF
		DocumentCount = DocumentCount + 1
		rsDocument.MoveNext
	wend
%>
<p align="left"><font face="Verdana,Arial,Helvetica" size="3" color="blue"><strong><% if DocumentCount < 1 then %>Sorry, no document were found matching your criteria.<% else %><% = DocumentCount %></strong>&nbsp;Documents Found for "<b><%=TopicName%></b>".</font><% end if %></p>
<table width="100%" border=0>
<%if cint(request.QueryString("menuid")) = cint(2) then%>
	<tr>
	<td  valign="top"><img src="/nav/images/linkarrow.gif" border="0" WIDTH="14" HEIGHT="14">
<%
		response.Write "<font face=""Verdana,Arial,Helvetica"" size=""2"">&nbsp;<a href =/tm/library/registerHTML.asp?wp=221>LAB List Update</a>"
		response.write "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;List of all available LAB Briefs and application Briefs posted on the LeCroy Website</font> "
%>
	<br><br></td></tr>
<%	
	end if
%>

<% rsDocumentCount = 0
if not flgConfig then
	rsDocument.MoveFirst
	While not rsDocument.EOF
	rsDocumentCount = rsDocumentCount + 1
	%>

		<tr>
			<td width="15" valign="top"><img src="/nav/images/linkarrow.gif" border="0" WIDTH="14" HEIGHT="14"></td>
			<td class="AWN" width="500">
			<%'Create and Instance of the file system object
			Set MyNewFileObject=Server.CreateObject("Scripting.FileSystemObject")
			'Create and instance of the file object
			Set aFile=MyNewFileObject.GetFile(rsDocument("FILE_FULL_PATH"))
			ON ERROR RESUME NEXT %>
				<%	
					if DateDiff("d",rsDocument("DATE_ADDED"),Now) < 14 then
						Response.Write "<FONT SIZE=1 color=red><b>New</b></FONT>"
					end if
				%>
				<a class="AWN" href="<% = "/tm/library/registerHTML.asp?wp=" & rsDocument("DOCUMENT_ID") %>"><% = rsDocument("TITLE") %></a> - 
			</td>
			<td align="left" width="30%">
				<%
				 if len(rsDocument("DOC_NUMBER"))>0 then 
					'if cstr(rsDocument("DOC_NUMBER"))= "18"  or cstr(rsDocument("DOC_NUMBER"))= "19" or cstr(rsDocument("DOC_NUMBER"))= "20" or cstr(rsDocument("DOC_NUMBER"))= "21" then
						Response.Write "<a href=""/tm/library/registerPDF.asp?wp=" & rsDocument("DOCUMENT_ID")  & """><img src=""/nav/images/icons/IconPDF.gif"" hspace=""3"" border=""0"" WIDTH=""20"" HEIGHT=""22""><font face=""Verdana,Arial,Helvetica"" size=""1"">View PDF</font></a>"
					'else
					'	Response.Write "<a href=""/tm/library/registerPDF.asp?wp=" & rsDocument("DOC_NUMBER") & ".pdf""><img src=""/nav/images/icons/IconPDF.gif"" hspace=""3"" border=""0"" WIDTH=""20"" HEIGHT=""22""><font face=""Verdana,Arial,Helvetica"" size=""1"">View PDF</font></a>"
					'end if
				end if
				'Create and Instance of the file system object
						Set DocumentLastModified=Server.CreateObject("Scripting.FileSystemObject")
						'Create and instance of the file object
						if DocumentLastModified.GetFile(rsDocument.Fields(8).value) = "" then
							Response.Write ""
						else
							Set FileLastModified=DocumentLastModified.GetFile(rsDocument.Fields(8).value)
							Response.Write "<font style='color:gray' class=LastModified><font face=Verdana Size=1 color=Gray>" & LEFT(FileLastModified.DateLastModified,8) & "</font></font><br>"
							on error resume next
						end if
				%>
				
			</td>
		</tr>
		<tr>
			<td valign="top">
			</td>
			<td class="SmallBody">
				<% = left(rsDocument("DESCRIPTION"),250) %>...
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td valign="top"></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td valign="top"></td>
			<td class="SmallBody" colspan="2" align="right">
			<nobr><%
				if (rsDocumentCount mod 5)=0 then 
					Response.write "<font face=""Verdana,Arial"" size=""1"" class=""SmallBody"">"
					Response.write "<a class=""SmallBody"" style=""color:""" & MainBodyColor & """ href=""#TopofPage"">"
					Response.write "<font color=""blue"">Top of Page</font></a></font>"
				else 
					Response.Write "&nbsp;"
				end if %>
			</nobr></td>
		</tr>


	<% rsDocument.MoveNext
	wend
else
	Response.Write "<tr>"
	REsponse.Write "<td>"
	set rsConfig=server.CreateObject("ADODB.Recordset")
	strSQL="Select TOPIC_ID,NAME from TOPIC WHERE MENU_ID=" & menuid & " order by SORT_ID"
	rsConfig.Open strSQL, dbconnstr()
	'Response.Write strSQL & "<br>"
	if not rsConfig.EOF then
	rsDocumentCount = 0
	counter =0
		while not rsConfig.EOF 
		
			set rsDocs=server.CreateObject("ADODB.Recordset")
			strSQL = "SELECT * FROM WEB_DOCUMENTS WHERE COUNTRY_ID = '" & country_id & "' AND POST = 'Y' AND TOPIC_ID =" & rsConfig("TOPIC_ID")& " and document_id <> 221 ORDER BY DATE_ADDED DESC"
			'Response.Write strSQL & "<br>"
			rsDocs.Open strSQL, dbconnstr()
			if not rsDocs.EOF then
				Response.Write "<table cellpadding=0 cellspacing=0 width=""100%"" border=0>"
				Response.Write "<tr>"
				Response.Write "<td valign=0><nobr>"
				counter = counter + 1
				Response.Write "<a name=" & counter & "></a>"
				Response.Write "<font face=""Verdana, Arial"" size=3 color=""#008080""><b>"
				Response.Write rsConfig("NAME") 
				Response.Write "</b></font></nobr><br>"
				Response.Write "<br><table cellpadding=0 cellspacing=0 width=""100%"" border=0>"
				while not rsDocs.EOF
				rsDocumentCount = rsDocumentCount +1
					Response.Write "<tr>"
					Response.Write "<td valign=top width=""15""><nobr>"
					Response.Write "<img src=""/nav/images/linkarrow.gif"" border=""0"" WIDTH=""14"" HEIGHT=""14"">&nbsp;"
					Response.Write "</nobr></td>"
					Response.Write "<td class=AWN width=""500"" valign=top>"
			        'Create and Instance of the file system object
			        Set MyNewFileObject=Server.CreateObject("Scripting.FileSystemObject")
					'Create and instance of the file object
					Set aFile=MyNewFileObject.GetFile(rsDocs("FILE_FULL_PATH"))
					ON ERROR RESUME NEXT 
					if DateDiff("d",rsDocs("DATE_ADDED"),Now) < 14 then
						Response.Write "<FONT SIZE=1 color=red face=""Verdana, Arial""><b>New&nbsp;</b></FONT>"
					end if
					'if cstr(rsDocs("DOC_NUMBER"))= "18"  or cstr(rsDocs("DOC_NUMBER"))= "19"  or cstr(rsDocs("DOC_NUMBER"))= "20"  or cstr(rsDocs("DOC_NUMBER"))= "21" then
						Response.Write "<a class=AWN href=""/tm/library/registerHTML.asp?wp=" & rsDocs("DOCUMENT_ID") & """>" & rsDocs("TITLE") & "</a> - "
					'else
					'	Response.Write "<a class=AWN href=""" & rsDocs("FILE_FULL_PATH") & """>" & rsDocs("TITLE") & "</a> - "
					'end if
					Response.Write "</td>"
					Response.Write "<td valign=top width=""30%"" align=left>"
					'if not rsDocs("DOC_NUMBER") = "" then 
					'	if cstr(rsDocs("DOC_NUMBER"))= "18"  or cstr(rsDocs("DOC_NUMBER"))= "19" or cstr(rsDocs("DOC_NUMBER"))= "20"  or cstr(rsDocs("DOC_NUMBER"))= "21" then
							'Response.Write rsDocs("DOCUMENT_ID")
							'Response.Write "<a href=""" & rsDocs("HYPERLINK") & """><img src=""/nav/images/icons/IconPDF.gif"" hspace=""3"" border=""0"" WIDTH=""20"" HEIGHT=""22""></a>"
							Response.Write  "<a href=""/tm/library/registerPDF.asp?wp=" & rsDocs("DOCUMENT_ID") & """><img src=""/nav/images/icons/IconPDF.gif"" hspace=""3"" border=""0"" WIDTH=""20"" HEIGHT=""22""></a>"

					'	else
					'		Response.Write "<a href=""/tm/library/LABs/PDF/LAB" & rsDocs("DOC_NUMBER") & ".pdf""><img src=""/nav/images/icons/IconPDF.gif"" hspace=""3"" border=""0"" WIDTH=""20"" HEIGHT=""22""></a>"
					'	end if
					'end if
					Response.Write "<font face=""Verdana,Arial,Helvetica"" size=""1"">View PDF</font>"
					Response.Write "</a>"
				'Create and Instance of the file system object
						Set DocumentLastModified=Server.CreateObject("Scripting.FileSystemObject")
						'Create and instance of the file object
						if DocumentLastModified.GetFile(rsDocs.Fields(8).value) = "" then
							Response.Write ""
						else
							Set FileLastModified=DocumentLastModified.GetFile(rsDocs.Fields(8).value)
							Response.Write "<font style='color:gray' class=LastModified><font face=Verdana Size=1 color=Gray>" & LEFT(FileLastModified.DateLastModified,8) & "</font></font><br>"
							on error resume next
						end if
						Response.Write "</td>"
						Response.Write "</tr>"
						Response.Write "<tr>"
						Response.Write "<td>&nbsp;</td>"
						Response.Write "<td class=SmallBody>"
						Response.Write left(rsDocs("DESCRIPTION"),250) & "..."
						Response.Write "</td>"
						Response.Write "<td>&nbsp;</td>"
						Response.Write "</tr>"
						Response.Write "<tr>"
						Response.Write "<td colspan=3>&nbsp;</td>"
						Response.Write "</tr>"
						Response.Write "<tr>"
						Response.Write "<td>&nbsp;</td>"
						Response.Write "<td class=SmallBody colspan=2 align=right><nobr>"
					if (rsDocumentCount mod 4)=0 then 
						Response.write "<font face=""Verdana,Arial"" size=""1"" class=""SmallBody"">"
						Response.write "<a class=""SmallBody"" style=""color:""" & MainBodyColor & """ href=""#TopofPage"">"
						Response.write "<font color=""blue"">Top of Page</font></a></font>"
					else 
						Response.Write "&nbsp;"
					end if 
					Response.Write "</nobr></td>"
					Response.Write "</tr>"
				rsDocs.MoveNext
				wend
				response.Write "</table>"
			end if
				Response.Write "</td>"
				Response.Write "</tr>"
				'Response.Write "</table><br>"
		rsConfig.MoveNext
		wend
		
	end if
end if
%>
<% end if %>
</table>
