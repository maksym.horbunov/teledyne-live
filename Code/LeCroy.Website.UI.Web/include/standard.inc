<%
	pagewidth="100%" 'sets the page width
	bannerbgcolor="" 'sets the banner background color - behind the logo
	menubgcolor="#0099ff" 'sets the top menu color
	bgcolor="#ffffff" 'sets the background color for the document
	menufontcolor="#ffffff" 'sets the top menu font color
	menufontcolorhighlight="yellow" 'sets the rollover color of the menu text
	linkcolor="blue" 'sets the top menu link color
	tocbgcolor="Blue" 'Sets the Table of Contents Menu Color
	tocbgcolorlight="#0099ff" ' Sets the Table of Contents Highlight Color
	tocbgcolordark="DarkBlue" ' Sets the Table of Contents Shadow Color
	tocbgrollover="DarkBlue" 'Sets the Table of Contents Rollover Color
	BNavBGColor="Blue" 'Sets the background color behind the bottom navigation
	Tier2BGColor="#6699FF" ' Sets the background color of the second tier in the contents menu
	NSTier2BGColor="DarkBlue" 'Sets the background color of the Contents Menu for Netscape ONLY.
	QuickLink="#d4de6b" 'Sets the Background Color behind the QuickLink dropdown
	CountryID=208
	SideNavTier2="#ffffff"
	MainBodyColor="#000000"
%>

