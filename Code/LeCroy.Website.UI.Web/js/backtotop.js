$.kScroll = function(options) {
    var defaults = {
        x: 0,
        y: 0,
        speed: 1000,
        callback: function() {}
    };
    options = options || {};
    options = $.extend({}, defaults, options);
    $('html,body').animate({
        scrollTop: options.y,
        scrollLeft: options.x,
    }, options.speed).promise().done(options.callback);
};
var menu = $('#menu'),
    items = menu.find('a');
items.each(function(i,a) {
    $(a).on('click', function() {
        var id = '#' + this.href.split('#')[1];
        var header = $(".page-header");
        $.kScroll({
            y: ($(id).position().top | 0) - header.outerHeight() - 61
        });
        return false;
    });
});

$(document).ready(function(){
	$("#backtotop").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#backtotop').fadeIn();
			} else {
				$('#backtotop').fadeOut();
			}
		});
		$('.backtotop_container a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 700);
			return false;
		});
	});

});
window.addEventListener("load",function() {
    setTimeout(function(){
        window.scrollTo(0, 1);
    }, 0);
});

