jQuery.noConflict();

//variable to hold which region is selected
var selectedRegion = "";
var selectedOffice = "S";

jQuery(function () {
    jQuery('#AmericasHotspot')
	    .hover(
		    function () {
		        jQuery('#Americas').addClass('highlighted');
		    },
		    function () {
		        if (selectedRegion != 'Americas') jQuery('#Americas').removeClass('highlighted');
		    }
	    )
	    .click(function () {
	        HandleMapClick("3", jQuery('#ddlCountryLA').val());
	    });
    jQuery('#AsiaPacHotspot')
	    .hover(
		    function () {
		        jQuery('#AsiaPac').addClass('highlighted');
		    },
		    function () {
		        if (selectedRegion != 'AsiaPac') jQuery('#AsiaPac').removeClass('highlighted');
		    }
	    )
	    .click(function () {
	        HandleMapClick("4", jQuery('#ddlCountryLAP').val());
	    });
    jQuery('#EuropeHotspot')
	    .hover(
		    function () {
		        jQuery('#Europe').addClass('highlighted');
		    },
		    function () {
		        if (selectedRegion != 'Europe') jQuery('#Europe').removeClass('highlighted');
		    }
	    )
	    .click(function () {
	        HandleMapClick("2", jQuery('#ddlCountryEMEA').val());
	    });
    jQuery('#ddlCountryEMEA').change(function () {
        var countryId = jQuery(this).val();
        if (countryId.length > 0) {
            jQuery('#hdnCountryId').val(countryId);
            HandleMapClick("2", countryId);
        }
    });
    jQuery('#ddlCountryLA').change(function () {
        var countryId = jQuery(this).val();
        if (countryId.length > 0) {
            jQuery('#hdnCountryId').val(countryId);
            HandleMapClick("3", countryId);
        }
    });
    jQuery('#ddlCountryLAP').change(function () {
        var countryId = jQuery(this).val();
        if (countryId.length > 0) {
            jQuery('#hdnCountryId').val(countryId);
            HandleMapClick("4", countryId);
        }
    });
    jQuery('#liOfficeTab').click(function () {
        jQuery('#spanHeader').html('To contact a Teledyne LeCroy office in your area, select a region from the map.');
        jQuery('#liDistroTab').css('font-weight', 'normal');
        jQuery('#liOfficeTab').removeClass('first');
        jQuery('#liOfficeTab').addClass('first on');
        jQuery('#liOfficeTabClose').removeClass('close');
        jQuery('#liOfficeTabClose').addClass('close close-on');
        jQuery('#liDistroTab').removeClass('on');
        jQuery('#liDistroTabClose').removeClass('close close-on');
        jQuery('#liDistroTabClose').addClass('close');
        jQuery(this).css('font-weight', 'bold');
        selectedOffice = "S";
        displayRegionOffices(jQuery('#hdnRegionId').val(), jQuery('#hdnCountryId').val());
    });
    jQuery('#liDistroTab').click(function () {
        HandleDistroTab();
    });
    jQuery('.hlkDistroLink').live('click', function () {
        HandleDistroTab();
    });
    jQuery('.hlkSalesOfficeLink').live('click', function () {
        HandleSalesOfficeTab();
    });

    HandleMapClick(jQuery('#hdnRegionId').val(), jQuery('#hdnCountryId').val());
});

// displays region office results when a user clicks a region on the map.
function displayRegionOffices(region, countryId) {
    var output = '';
    if (regions[region] == null || region <= 0) {
        HandleNoResults();
        return;
    }
    var officeCount = 0;
    var countries = regions[region].countries;
    for (countryKey in countries) {
        if (countryId > 0 && countryKey != countryId) continue;
        var country = countries[countryKey];
        var offices = country.offices;
        var leftSide = 500;
        var rightSide = 10;
        var innerOfficeCount = 0;
        for (i in offices) {
            officeCount++;
            var office = offices[i];
            if (office.type.toUpperCase() != selectedOffice) continue;
            var extraClasses = '';
            if (office.type.toUpperCase() == 'D' || (office.type.toUpperCase() == 'S' && office.isDefault.toUpperCase() == 'TRUE')) {
                leftSide = 365;
                rightSide = 280;
                output += '<div class="result' + extraClasses + '" id="' + i + '">' + GetLeftSide(leftSide, office, office.addressInfo) + '<div class="rightside" style="width: ' + rightSide + 'px;">' + GetRightSide(office, office.addressInfo) + '</div><div class="clear"></div></div>';
            }
            else {
                output += '<div class="result' + extraClasses + '" id="' + i + '">' + GetLeftSide(leftSide, office, office.addressInfo) + '<div class="clear"></div></div>';
            }
            innerOfficeCount++;
        }
        if (innerOfficeCount <= 0) {
            output += GetNoDOffices();
        }
    }
    jQuery('#RegionResults').html(output);
    if (officeCount <= 0) {
        HandleNoResults();
    }
    normalizeHeights();
    zebraStripes();
}

// sets the selected region; also handles unselecting a region if another region is selected. 
function setSelectedRegion(regionName) {
    if (selectedRegion != "" && selectedRegion != regionName) {
        var curRegion = jQuery("#" + selectedRegion);
        if (curRegion.length > 0) curRegion.removeClass("highlighted");
    }
    selectedRegion = regionName;
}

// Makes left side and right side of each result the same height
function normalizeHeights() {
    jQuery('.result').each(function () {
        var leftHeight = jQuery(this).find(".leftside").height();
        var rightHeight = jQuery(this).find(".rightside").height();
        if (leftHeight > rightHeight) {
            jQuery(this).find(".rightside").height(leftHeight);
        }
        else {
            jQuery(this).find(".leftside").height(rightHeight);
        }
    });
}

// Applies Zebra Stripe pattern to all visible results
function zebraStripes() {
    var visibleResults = jQuery("#RegionResults .result").filter(":visible");
    visibleResults.filter(":odd").addClass("odd");
    visibleResults.filter(":even").removeClass("odd");
}

function HandleDistroTab() {
    jQuery('#spanHeader').html('To find a business partner in your area, select a region from the map.');
    jQuery('#liOfficeTab').css('font-weight', 'normal');
    jQuery('#liOfficeTab').removeClass('first on');
    jQuery('#liOfficeTab').addClass('first');
    jQuery('#liOfficeTabClose').removeClass('close close-on');
    jQuery('#liOfficeTabClose').addClass('close');
    jQuery('#liDistroTab').addClass('on');
    jQuery('#liDistroTabClose').removeClass('close');
    jQuery('#liDistroTabClose').addClass('close close-on');
    jQuery('#liDistroTab').css('font-weight', 'bold');
    selectedOffice = "D";
    displayRegionOffices(jQuery('#hdnRegionId').val(), jQuery('#hdnCountryId').val());
}

function HandleSalesOfficeTab() {
    jQuery('#spanHeader').html('To contact a Teledyne LeCroy office in your area, select a region from the map.');
    jQuery('#liDistroTab').css('font-weight', 'normal');
    jQuery('#liOfficeTab').removeClass('first');
    jQuery('#liOfficeTab').addClass('first on');
    jQuery('#liOfficeTabClose').removeClass('close');
    jQuery('#liOfficeTabClose').addClass('close close-on');
    jQuery('#liDistroTab').removeClass('on');
    jQuery('#liDistroTabClose').removeClass('close close-on');
    jQuery('#liDistroTabClose').addClass('close');
    jQuery('#liOfficeTab').css('font-weight', 'bold');
    selectedOffice = "S";
    displayRegionOffices(jQuery('#hdnRegionId').val(), jQuery('#hdnCountryId').val());
}

function HandleNoResults() {
    jQuery('#RegionResults').html('<div>We\'re sorry, but no results were found.</div>');
}

function HandleMapClick(regionId, countryId) {
    jQuery('#hdnRegionId').val(regionId);
    jQuery('#ddlCountryLA').hide();
    jQuery('#ddlCountryLAP').hide();
    jQuery('#ddlCountryEMEA').hide();
    jQuery('#Americas').removeClass('highlighted');
    jQuery('#Europe').removeClass('highlighted');
    jQuery('#AsiaPac').removeClass('highlighted');
    if (regionId == 2) {
        setSelectedRegion('Europe');
        jQuery('#ddlCountryEMEA').show();
        jQuery('#Europe').addClass('highlighted');
        jQuery('#hdnCountryId').val(jQuery('#ddlCountryEMEA').val());
    }
    else if (regionId == 3) {
        setSelectedRegion('Americas');
        jQuery('#ddlCountryLA').show();
        jQuery('#Americas').addClass('highlighted');
        jQuery('#hdnCountryId').val(jQuery('#ddlCountryLA').val());
    }
    else if (regionId == 4) {
        setSelectedRegion('AsiaPac');
        jQuery('#ddlCountryLAP').show();
        jQuery('#AsiaPac').addClass('highlighted');
        jQuery('#hdnCountryId').val(jQuery('#ddlCountryLAP').val());
    }
    displayRegionOffices(regionId, countryId);
}

function GetLeftSide(leftSideWidth, office, addresses) {
    var retVal = '';
    for (i in addresses) {
        retVal += '<p>';
        if (office.addressInfo[i].address1 != null && office.addressInfo[i].address1) retVal += office.addressInfo[i].address1 + '<br />';
        if (office.addressInfo[i].address2 != null && office.addressInfo[i].address2) retVal += office.addressInfo[i].address2 + '<br />';
        if (office.addressInfo[i].address3 != null && office.addressInfo[i].address3) retVal += office.addressInfo[i].address3 + '<br />';
        if (office.addressInfo[i].address4 != null && office.addressInfo[i].address4) retVal += office.addressInfo[i].address4 + '<br />';
        if (office.addressInfo[i].address5 != null && office.addressInfo[i].address5) retVal += office.addressInfo[i].address5 + '<br />';
        if (office.addressInfo[i].phone != null && office.addressInfo[i].phone) retVal += '<b>Phone:</b> ' + office.addressInfo[i].phone + '<br />';
        if (office.addressInfo[i].fax != null && office.addressInfo[i].fax) retVal += '<b>Fax:</b> ' + office.addressInfo[i].fax + '<br />';
        if (office.addressInfo[i].faxSales != null && office.addressInfo[i].faxSales) retVal += '<b>Fax Sales:</b> ' + office.addressInfo[i].faxSales + '<br />';
        if (office.addressInfo[i].faxService != null && office.addressInfo[i].faxService) retVal += '<b>Fax Service:</b> ' + office.addressInfo[i].faxService + '<br />';
        if (office.addressInfo[i].email != null && office.addressInfo[i].email) {
            var emailString = GetEmailString(office.addressInfo[i].email);
            retVal += '<b>Email Sales:</b> ' + emailString + '<br />';
        }
        if (office.addressInfo[i].emailService != null && office.addressInfo[i].emailService) {
            var emailString = GetEmailString(office.addressInfo[i].emailService);
            retVal += '<b>Email Service:</b> ' + emailString + '<br />';
        }
        if (office.addressInfo[i].emailSupport != null && office.addressInfo[i].emailSupport) {
            var emailString = GetEmailString(office.addressInfo[i].emailSupport);
            retVal += '<b>Email Support:</b> ' + emailString + '<br />';
        }
        if (office.addressInfo[i].website != null && office.addressInfo[i].website) {
            retVal += '<b>Web Site:</b> <a href="' + office.addressInfo[i].website + '" onclick="GaEventPush(\'OutboundLink\', \'' + office.addressInfo[i].website.replace('https://', '').replace('http://', '') + '\');">' + office.addressInfo[i].website + '</a><br />';
        }
        if (office.addressInfo[i].phoneService != null && office.addressInfo[i].phoneService) retVal += '<b>Phone Service:</b> ' + office.addressInfo[i].phoneService + '<br />';
        if (office.addressInfo[i].phoneSupport != null && office.addressInfo[i].phoneSupport) retVal += '<b>Phone Support:</b> ' + office.addressInfo[i].phoneSupport;
        retVal += '</p>';
    }
    //if (office.type.toUpperCase() == 'D') {
        if (office.nameSubHead != null) {
            return '<div class="leftside" style="width: ' + leftSideWidth + 'px;"><h3>' + office.name + '</h3><h4 style="font-weight: bold;">' + office.nameSubHead + '</h4>' + retVal + '</div>';
        }
        else {
            return '<div class="leftside" style="width: ' + leftSideWidth + 'px;"><h3>' + office.name + '</h3>' + retVal + '</div>';
        }
    //}
    //else {
    //    return '<div class="leftside" style="width: ' + leftSideWidth + 'px;"><h3>' + office.name + '</h3>' + retVal + '</div>';
    //}
}

function GetEmailString(emailAddress) {
    if (emailAddress.indexOf('(') != -1 && emailAddress.indexOf(')') != -1) {
        if (emailAddress.indexOf(';') != -1) {
            var emails = emailAddress.split(';');
            var retVal = '';
            for (z = 0; z < emails.length; z++) {
                if (z == emails.length - 1) {
                    retVal += '<a href="mailto:' + emails[z].substring(0, emails[z].indexOf('(')).replace(' ', '') + '">' + emails[z] + '</a>';
                }
                else {
                    retVal += '<a href="mailto:' + emails[z].substring(0, emails[z].indexOf('(')).replace(' ', '') + '">' + emails[z] + '</a>, ';
                }
            }
            return retVal;
        }
        else {
            return '<a href="mailto:' + emailAddress.substring(0, emailAddress.indexOf('(')).replace(' ', '') + '">' + emailAddress + '</a>'
        }
    }
    else {
        if (emailAddress.indexOf(';') != -1) {
            var emails = emailAddress.split(';');
            var retVal = '';
            for (y = 0; y < emails.length; y++) {
                if (y == emails.length - 1) {
                    retVal += '<a href="mailto:' + emails[y].replace(' ', '') + '">' + emails[y] + '</a>';
                }
                else {
                    retVal += '<a href="mailto:' + emails[y].replace(' ', '') + '">' + emails[y] + '</a>, ';
                }
            }
            return retVal;
        }
        else {
            return '<a href="mailto:' + emailAddress.replace(' ', '') + '">' + emailAddress + '</a>'
        }
    }
}

function GetRightSide(office, addresses) {
    var retVal = '';
    for (i in addresses) {
        if (office.type.toUpperCase() == 'D') {
            var prefix = '';
            var suffix = '<br />';
            var counter = 0;
            var currentCategory = '';
            var seeMore = false;
            for (product in office.addressInfo[i].products) {
                var splitProduct = product.split('|');
                if (currentCategory != splitProduct[0]) {
                    if (seeMore) {
                        retVal = retVal.substr(0, retVal.length - 6);
                        retVal += "</span><span class=\"description-heading-less\" style=\"cursor:pointer;\">&nbsp;</span></div>";
                        suffix = '<br />';
                    }
                    counter = 0;
                    if (counter == 0 && suffix == '') suffix = '<br />';
                    currentCategory = splitProduct[0];
                    retVal += prefix + '<b>' + splitProduct[0] + '</b>' + suffix;
                    seeMore = false;
                    prefix = '<br />';
                    suffix = '';
                }
                if (counter >= 10) {
                    if (!seeMore) {
                        retVal += "<div class=\"product-container\"><span class=\"description-heading-more\" style=\"color: #008ecd; font-weight: bold; text-decoration: underline; cursor:pointer;\">Show All " + currentCategory + "</span><span class=\"description-content\" style=\"display: none;\">";
                        seeMore = true;
                    }
                }

                if (currentCategory.toLowerCase() == 'protocol analyzers') {
                    if (splitProduct[2] !== '-1') {
                        retVal += '<a href="../../protocolanalyzer/protocolstandard.aspx?standardid=' + splitProduct[2] + '" style="color: #000000;">' + splitProduct[1] + '</a><br />';
                    }
                    else {
                        retVal += splitProduct[1] + '<br />';
                    }
                }
                else {
                    if (splitProduct[2] !== '-1') {
                        if (splitProduct[3] == '2') {
                            if (splitProduct[2] == '276') {
                                retVal += '<a href="../../arbstudio/arbstudio.aspx" style="color: #000000;">' + splitProduct[1] + '</a><br />';
                            }
                            else {
                                retVal += '<a href="../../wavestation/" style="color: #000000;">' + splitProduct[1] + '</a><br />';
                            }
                        }
                        else if (splitProduct[3] == '31') {
                            retVal += '<a href="../../logicstudio/" style="color: #000000;">' + splitProduct[1] + '</a><br />';
                        }
                        else if (splitProduct[3] == '1') {
                            retVal += '<a href="../../oscilloscope/oscilloscopeseries.aspx?mseries=' + splitProduct[2] + '" style="color: #000000;">' + splitProduct[1] + '</a><br />';
                        }
                        else {
                            retVal += splitProduct[1] + '<br />';
                        }
                    }
                    else {
                        retVal += splitProduct[1] + '<br />';
                    }
                }
                counter++;
            }
            if (seeMore) {
                retVal = retVal.substr(0, retVal.length - 6);
                retVal += "</span><span class=\"description-heading-less\" style=\"cursor:pointer;\">&nbsp;</span></div>";
            }
        }
    }
    if (office.type.toUpperCase() == 'S' && office.isDefault.toUpperCase() == 'TRUE') {
        retVal += '<div class="distroLink"><a href="#" class="hlkDistroLink">Click Sales &amp; Distribution Partners tab to view a list of local Teledyne LeCroy Sales Partners</a></div>';
    }
    normalizeHeights();
    return retVal;
}

function GetNoDOffices() {
    return '<div class="result"><a href="#" class="hlkSalesOfficeLink">We do not currently list any distributors for this country. Please check the Teledyne LeCroy Offices tab for the nearest office</a></div>';
}