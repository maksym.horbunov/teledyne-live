$(document).ready(function() {
	$('a.font_arial').click(function(){
		$('.content-mid').css('font-family', "Arial");
		$('.fixedspan3').css('font-family', "Arial");
	});
	$('a.font_open').click(function(){
		$('.content-mid').css('font-family', "Open Sans");
		$('.fixedspan3').css('font-family', "Open Sans");
	});
	$('a.font_roboto').click(function(){
		$('.content-mid').css('font-family', "Roboto");
		$('.fixedspan3').css('font-family', "Roboto");
	});
	$('a.font_cabin').click(function(){
		$('.content-mid').css('font-family', "Cabin");
		$('.fixedspan3').css('font-family', "Cabin");
	});
	$('a.font_droid').click(function(){
		$('.content-mid').css('font-family', "Droid Sans");
		$('.fixedspan3').css('font-family', "Droid Sans");
	});
	$('a.font_noto').click(function(){
		$('.content-mid').css('font-family', "Noto Sans");
		$('.fixedspan3').css('font-family', "Noto Sans");
	});
});