jQuery(function() {
    //- initCustomHover();
    clearShowedClassOfMenu();
    getDropMenu();
    //- initFocusSearch();
    initPopups();
    initMobileNav();
    initTouchNav();
    initDropDownClasses();
    initMenuTabs();
    initMenuAccordion();
});

//- add classes on hover/touch
//- function initCustomHover() {
//-     jQuery('.dropdown-item').touchHover({});
//- }

function clearShowedClassOfMenu() {
    // console.log('this: ', this);
    var listItems = document.querySelectorAll('.menu li');
    var arr = document.querySelectorAll('.nav-opener');
    Array.prototype.forEach.call(arr, function(elem) {
        elem.addEventListener('click', function() {
            Array.prototype.forEach.call(listItems, function(li) {
                if (li.classList.contains('show')) {
                    li.classList.remove('show')
                }
            });
        });
    });
}

// for animation of main menu
function getDropMenu() {
    var arr = document.querySelectorAll('.dropdown-item');
    Array.prototype.forEach.call(arr, function(elem) {
        var drop = elem.querySelector('.drop');
        var arrTabs = elem.querySelectorAll('.tabs-menu > li');
        var arrTabsContent = elem.querySelectorAll('.tab-holder > div');

        elem.addEventListener('mouseenter', function(event) {
            event.preventDefault();
            drop.style.maxHeight = drop.scrollHeight + 'px';
        });

        elem.addEventListener('mouseleave', function() {
            drop.style.maxHeight = 1 + 'px';
            setTimeout(function () {
                Array.prototype.forEach.call(arrTabs, function(elTab, i) {
                    elTab.classList.remove('active');
                    if (i === 0) {
                        elTab.classList.add('active');
                    }
                });
                Array.prototype.forEach.call(arrTabsContent, function(elTabContent, i) {
                    elTabContent.classList.add('tab-hidden');
                    if (i === 0) {
                        elTabContent.classList.remove('tab-hidden');
                    }
                });
            }, 400)
        });

        if (arrTabs.length && arrTabsContent) {
            Array.prototype.forEach.call(arrTabs, function(elTab) {
                elTab.addEventListener('click', function() {
                    Array.prototype.forEach.call(arrTabsContent, function(elTabContent) {
                        if (!elTabContent.classList.contains('tab-hidden')) {
                            drop.style.maxHeight =
                                elTabContent.parentElement.scrollHeight + 56 + 'px';
                        }
                    });
                    return;
                });
            });
        }
    });
}

//- // autofocus search input
//- function initFocusSearch() {
//-     var el = document.getElementById('open-search');
//-     el.addEventListener('click', function(){
//-         // document.getElementById("search").focus();
//-     });
//- }

// popups init
function initPopups() {
    //- var isIE = document.body.style.msTouchAction !== undefined;

    //- if (isIE) {
    //-     jQuery('#search').attr('placeholder', '')
    //- }

    jQuery('.popup-holder').contentPopup({
        mode: 'click',
        btnOpen: '.open-search',
        hideOnClickLink: 'popup-active'
    }).bind('keydown', function(e) {
        if (e.which === 27) {
            jQuery(this).removeClass('popup-active');
            jQuery(this).find('.popup').css({
                display: 'none'
            });
            jQuery('#search').val('');
        }
    });
}

// mobile menu init
function initMobileNav() {
    ResponsiveHelper.addRange({
        '..991': {
            on: function() {
                jQuery('body').mobileNav({
                    menuActiveClass: 'nav-active',
                    menuOpener: '.nav-opener',
                    hideOnClickOutside: true,
                    menuDrop: '.holder-nav'
                });
            },
            off: function() {
                jQuery('body').mobileNav('destroy');
            }
        }
    });
}

// handle dropdowns on mobile devices
function initTouchNav() {
    var $nav = jQuery('#nav');
    var hoverClass = 'show';

    function onBackBtn(e) {
        var $btn = jQuery(e.currentTarget);
        $btn.closest('.' + hoverClass).removeClass(hoverClass);
    }

    $nav.on('click', '.nav-back', onBackBtn);
}

// add classes if item has dropdown
function initDropDownClasses() {
    jQuery('#nav li').each(function() {
        var item = jQuery(this);
        var drop = item.find('ul');
        var link = item.find('a').eq(0);
        if (drop.length) {
            item.addClass('has-drop-down');
            if (link.length) link.addClass('has-drop-down-a');
        }
    });
}

// init Tabs
function initMenuTabs() {
    ResponsiveHelper.addRange({
        '992..': {
            on: function() {
                jQuery('.tabs-menu').tabs({
                    tabItems: 'li',
                    tabLink: 'a',
                    attrb: 'data-href'
                });
            },
            off: function() {
                jQuery('.tabs-menu').tabs('destroy');
            }
        }
    });
}

// accordion menu init
function initMenuAccordion() {
    ResponsiveHelper.addRange({
        '..991': {
            on: function() {
                jQuery('.main-menu').slideAccordion({
                    opener: '>a.opener',
                    slider: '>div.drop',
                    animSpeed: 0,
                    activeClass: 'show',
                    collapsible: true,
                });
            },
            off: function() {
                jQuery('.main-menu').slideAccordion('destroy');
            }
        }
    });
}


/*
 * Responsive Layout helper
 */
window.ResponsiveHelper = (function($){
    // init variables
    var handlers = [],
        prevWinWidth,
        win = $(window),
        nativeMatchMedia = false;

    // detect match media support
    if(window.matchMedia) {
        if(window.Window && window.matchMedia === Window.prototype.matchMedia) {
            nativeMatchMedia = true;
        } else if(window.matchMedia.toString().indexOf('native') > -1) {
            nativeMatchMedia = true;
        }
    }

    // prepare resize handler
    function resizeHandler() {
        var winWidth = win.width();
        if(winWidth !== prevWinWidth) {
            prevWinWidth = winWidth;

            // loop through range groups
            $.each(handlers, function(index, rangeObject){
                // disable current active area if needed
                $.each(rangeObject.data, function(property, item) {
                    if(item.currentActive && !matchRange(item.range[0], item.range[1])) {
                        item.currentActive = false;
                        if(typeof item.disableCallback === 'function') {
                            item.disableCallback();
                        }
                    }
                });

                // enable areas that match current width
                $.each(rangeObject.data, function(property, item) {
                    if(!item.currentActive && matchRange(item.range[0], item.range[1])) {
                        // make callback
                        item.currentActive = true;
                        if(typeof item.enableCallback === 'function') {
                            item.enableCallback();
                        }
                    }
                });
            });
        }
    }
    win.bind('load resize orientationchange', resizeHandler);

    // test range
    function matchRange(r1, r2) {
        var mediaQueryString = '';
        if(r1 > 0) {
            mediaQueryString += '(min-width: ' + r1 + 'px)';
        }
        if(r2 < Infinity) {
            mediaQueryString += (mediaQueryString ? ' and ' : '') + '(max-width: ' + r2 + 'px)';
        }
        return matchQuery(mediaQueryString, r1, r2);
    }

    // media query function
    function matchQuery(query, r1, r2) {
        if(window.matchMedia && nativeMatchMedia) {
            return matchMedia(query).matches;
        } else if(window.styleMedia) {
            return styleMedia.matchMedium(query);
        } else if(window.media) {
            return media.matchMedium(query);
        } else {
            return prevWinWidth >= r1 && prevWinWidth <= r2;
        }
    }

    // range parser
    function parseRange(rangeStr) {
        var rangeData = rangeStr.split('..');
        var x1 = parseInt(rangeData[0], 10) || -Infinity;
        var x2 = parseInt(rangeData[1], 10) || Infinity;
        return [x1, x2].sort(function(a, b){
            return a - b;
        });
    }

    // export public functions
    return {
        addRange: function(ranges) {
            // parse data and add items to collection
            var result = {data:{}};
            $.each(ranges, function(property, data){
                result.data[property] = {
                    range: parseRange(property),
                    enableCallback: data.on,
                    disableCallback: data.off
                };
            });
            handlers.push(result);

            // call resizeHandler to recalculate all events
            prevWinWidth = null;
            resizeHandler();
        }
    };
}(jQuery));

/*
 * Popups plugin
 */
;(function($) {
    function ContentPopup(opt) {
        this.options = $.extend({
            holder: null,
            popup: '.popup',
            btnOpen: '.open',
            btnClose: '.close',
            openClass: 'popup-active',
            clickEvent: 'click',
            mode: 'click',
            hideOnClickLink: true,
            hideOnClickOutside: true,
            delay: 50
        }, opt);
        if (this.options.holder) {
            this.holder = $(this.options.holder);
            this.init();
        }
    }
    ContentPopup.prototype = {
        init: function() {
            this.findElements();
            this.attachEvents();
        },
        findElements: function() {
            this.popup = this.holder.find(this.options.popup);
            this.btnOpen = this.holder.find(this.options.btnOpen);
            this.btnClose = this.holder.find(this.options.btnClose);
        },
        attachEvents: function() {
            // handle popup openers
            var self = this;
            this.clickMode = isTouchDevice || (self.options.mode === self.options.clickEvent);

            if (this.clickMode) {
                // handle click mode
                this.btnOpen.bind(self.options.clickEvent + '.popup', function(e) {
                    if (self.holder.hasClass(self.options.openClass)) {
                        if (self.options.hideOnClickLink) {
                            self.hidePopup();
                        }
                    } else {
                        self.showPopup();
                    }
                    e.preventDefault();
                });

                // prepare outside click handler
                this.outsideClickHandler = this.bind(this.outsideClickHandler, this);
            } else {
                // handle hover mode
                var timer, delayedFunc = function(func) {
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        func.call(self);
                    }, self.options.delay);
                };
                this.btnOpen.on('mouseover.popup', function() {
                    delayedFunc(self.showPopup);
                }).on('mouseout.popup', function() {
                    delayedFunc(self.hidePopup);
                });
                this.popup.on('mouseover.popup', function() {
                    delayedFunc(self.showPopup);
                }).on('mouseout.popup', function() {
                    delayedFunc(self.hidePopup);
                });
            }

            // handle close buttons
            this.btnClose.on(self.options.clickEvent + '.popup', function(e) {
                self.hidePopup();
                e.preventDefault();
            });
        },
        outsideClickHandler: function(e) {
            // hide popup if clicked outside
            var targetNode = $((e.changedTouches ? e.changedTouches[0] : e).target);
            if (!targetNode.closest(this.popup).length && !targetNode.closest(this.btnOpen).length) {
                this.hidePopup();
            }
        },
        showPopup: function() {
            // reveal popup
            this.holder.addClass(this.options.openClass);
            this.popup.css({
                display: 'block'
            });

            // outside click handler
            if (this.clickMode && this.options.hideOnClickOutside && !this.outsideHandlerActive) {
                this.outsideHandlerActive = true;
                $(document).on('click touchstart', this.outsideClickHandler);
            }
            document.getElementById("search").focus();
        },
        hidePopup: function() {
            // hide popup
            jQuery('#searchform')[0].reset();
            this.holder.removeClass(this.options.openClass);
            this.popup.css({
                display: 'none'
            });

            // outside click handler
            if (this.clickMode && this.options.hideOnClickOutside && this.outsideHandlerActive) {
                this.outsideHandlerActive = false;
                $(document).off('click touchstart', this.outsideClickHandler);
            }
            document.getElementById("search").blur();
        },
        bind: function(f, scope, forceArgs) {
            return function() {
                return f.apply(scope, forceArgs ? [forceArgs] : arguments);
            };
        },
        destroy: function() {
            this.popup.removeAttr('style');
            this.holder.removeClass(this.options.openClass);
            this.btnOpen.add(this.btnClose).add(this.popup).off('.popup');
            $(document).off('click touchstart', this.outsideClickHandler);
        }
    };

    // detect touch devices
    var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

    // jQuery plugin interface
    $.fn.contentPopup = function(opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        return this.each(function() {
            var $holder = jQuery(this);
            var instance = $holder.data('ContentPopup');

            if (typeof opt === 'object' || typeof opt === 'undefined') {
                $holder.data('ContentPopup', new ContentPopup($.extend({
                    holder: this
                }, opt)));
            } else if (typeof method === 'string' && instance) {
                if (typeof instance[method] === 'function') {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };
}(jQuery));

/*
 * jQuery Accordion plugin new
 */
;(function(root, factory) {

    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    } else {
        root.SlideAccordion = factory(jQuery);
    }
}(this, function($) {

    'use strict';
    var accHiddenClass = 'js-acc-hidden';

    function SlideAccordion(options) {
        this.options = $.extend(true, {
            allowClickWhenExpanded: false,
            activeClass:'active',
            opener:'.opener',
            slider:'.slide',
            animSpeed: 300,
            collapsible:true,
            event: 'click',
            scrollToActiveItem: {
                enable: false,
                breakpoint: 767, // max-width
                animSpeed: 600,
                extraOffset: null
            }
        }, options);
        this.init();
    }

    SlideAccordion.prototype = {
        init: function() {
            if (this.options.holder) {
                this.findElements();
                this.setStateOnInit();
                this.attachEvents();
                this.makeCallback('onInit');
            }
        },

        findElements: function() {
            this.$holder = $(this.options.holder).data('SlideAccordion', this);
            this.$items = this.$holder.find(':has(' + this.options.slider + ')');
        },

        setStateOnInit: function() {
            var self = this;

            this.$items.each(function() {
                if (!$(this).hasClass(self.options.activeClass)) {
                    $(this).find(self.options.slider).addClass(accHiddenClass);
                }
            });
        },

        attachEvents: function() {
            var self = this;

            this.accordionToggle = function(e) {
                var $item = jQuery(this).closest(self.$items);
                var $actiItem = self.getActiveItem($item);

                if (!self.options.allowClickWhenExpanded || !$item.hasClass(self.options.activeClass)) {
                    e.preventDefault();
                    self.toggle($item, $actiItem);
                }
            };

            this.$items.on(this.options.event, this.options.opener, this.accordionToggle);
        },

        toggle: function($item, $prevItem) {
            if (!$item.hasClass(this.options.activeClass)) {
                this.show($item);
            } else if (this.options.collapsible) {
                this.hide($item);
            }

            if (!$item.is($prevItem) && $prevItem.length) {
                this.hide($prevItem);
            }

            this.makeCallback('beforeToggle');
        },

        show: function($item) {
            var $slider = $item.find(this.options.slider);

            $item.addClass(this.options.activeClass);
            $slider.stop().hide().removeClass(accHiddenClass).slideDown({
                duration: this.options.animSpeed,
                complete: function() {
                    $slider.removeAttr('style');
                    if (
                        this.options.scrollToActiveItem.enable &&
                        window.innerWidth <= this.options.scrollToActiveItem.breakpoint
                    ) {
                        this.goToItem($item);
                    }
                    this.makeCallback('onShow', $item);
                }.bind(this)
            });

            this.makeCallback('beforeShow', $item);
        },

        hide: function($item) {
            var $slider = $item.find(this.options.slider);

            $item.removeClass(this.options.activeClass);
            $slider.stop().show().slideUp({
                duration: this.options.animSpeed,
                complete: function() {
                    $slider.addClass(accHiddenClass);
                    $slider.removeAttr('style');
                    this.makeCallback('onHide', $item);
                }.bind(this)
            });

            this.makeCallback('beforeHide', $item);
        },

        goToItem: function($item) {
            var itemOffset = $item.offset().top;

            if (itemOffset < $(window).scrollTop()) {
                // handle extra offset
                if (typeof this.options.scrollToActiveItem.extraOffset === 'number') {
                    itemOffset -= this.options.scrollToActiveItem.extraOffset;
                } else if (typeof this.options.scrollToActiveItem.extraOffset === 'function') {
                    itemOffset -= this.options.scrollToActiveItem.extraOffset();
                }

                $('body, html').animate({
                    scrollTop: itemOffset
                }, this.options.scrollToActiveItem.animSpeed);
            }
        },

        getActiveItem: function($item) {
            return $item.siblings().filter('.' + this.options.activeClass);
        },

        makeCallback: function(name) {
            if (typeof this.options[name] === 'function') {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },

        destroy: function() {
            this.$holder.removeData('SlideAccordion');
            this.$items.off(this.options.event, this.options.opener, this.accordionToggle);
            this.$items.removeClass(this.options.activeClass).each(function(i, item) {
                $(item).find(this.options.slider).removeAttr('style').removeClass(accHiddenClass);
            }.bind(this));
            this.makeCallback('onDestroy');
        }
    };

    $.fn.slideAccordion = function(opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        return this.each(function() {
            var $holder = jQuery(this);
            var instance = $holder.data('SlideAccordion');

            if (typeof opt === 'object' || typeof opt === 'undefined') {
                new SlideAccordion($.extend(true, {
                    holder: this
                }, opt));
            } else if (typeof method === 'string' && instance) {
                if(typeof instance[method] === 'function') {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };

    (function() {
        var tabStyleSheet = $('<style type="text/css">')[0];
        var tabStyleRule = '.' + accHiddenClass;
        tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important; width: 100% !important;}';
        if (tabStyleSheet.styleSheet) {
            tabStyleSheet.styleSheet.cssText = tabStyleRule;
        } else {
            tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
        }
        $('head').append(tabStyleSheet);
    }());

    return SlideAccordion;
}));


/*
 * jQuery tab content plugin
 */
;(function($, window, exportName) {
    'use strict';

    var Tabs = function(options) {
        this.options = $.extend({}, Tabs.DEFAULTS, options);
        this.init();
    };

    var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch || /Windows Phone/.test(navigator.userAgent);

    // default options
    Tabs.DEFAULTS = {
        tabItems: 'li',
        tabLink: 'a',
        attrb: 'href',
        activeClass: 'active',
        hiddenClass: 'tab-hidden',
        eventMode: 'click',
        collapsedTab: false,
        checkHash: false,

        // callbacks
        onInit: null,
        onChange: null,
        onShow: null,
        onHide: null,
        onDestroy: null
    };

    Tabs.prototype = {
        init: function() {
            if (this.options.tabset) {
                this.initStructure();
                this.attachEvents();
                this.makeCallback('onInit', this);
            }
        },
        initStructure: function() {
            this.tabset = $(this.options.tabset);
            this.tabs = $();
            this.tabItems = this.tabset.children(this.options.tabItems);
            this.tabLinks = this.tabItems.children(this.options.tabLink);
            this.currentIndex = 0;
            this.eventMode = isTouchDevice ? 'click' : this.options.eventMode;
            this.handleLocationHash();
            this.groupmentTabs();
        },
        attachEvents: function() {
            var self = this;

            // event handler
            this.onSwitchTab = function(event) {
                var link = $(this);
                var item = link.closest(self.options.tabItems);
                event.preventDefault();
                self.switchTab(item);
            };

            this.tabLinks.on(this.options.eventMode, this.onSwitchTab);
        },
        switchTab: function(item) {
            var itemActive = this.getActiveItem();

            if (this.isActiveItem(item)) {
                if (this.options.collapsedTab) {
                    this.hideTab(item);
                }
            } else {
                this.showTab(item);
                this.hideTab(itemActive);
            }

            this.makeCallback('onChange', this, item);
        },
        showTab: function(item) {
            item.addClass(this.options.activeClass);
            this.getTargetTab(item).removeClass(this.options.hiddenClass);
            this.makeCallback('onShow', this, item);
        },
        hideTab: function(item) {
            item.removeClass(this.options.activeClass);
            this.getTargetTab(item).addClass(this.options.hiddenClass);
            this.makeCallback('onHide', this, item);
        },
        nextTab: function() {
            var index = this.getActiveItem().index();
            this.currentIndex = index < this.tabItems.length - 1 ? index + 1 : 0;
            this.switchTab(this.tabItems.eq(this.currentIndex));
        },
        prevTab: function() {
            var index = this.getActiveItem().index();
            this.currentIndex = index > 0 ? index - 1 : this.tabItems.length - 1;
            this.switchTab(this.tabItems.eq(this.currentIndex));
        },
        numTab: function(num) {
            if (this.currentIndex !== num && num <= this.tabItems.length - 1) {
                this.currentIndex = num;
                this.currentItem = this.tabItems.eq(this.currentIndex);
                this.switchTab(this.currentItem);
            }
        },
        groupmentTabs: function() {
            var self = this;

            this.tabLinks.each(function() {
                var link = $(this);
                var item = link.closest(self.options.tabItems);
                var href = link.attr(self.options.attrb);
                var tab;
                href = href.substr(href.lastIndexOf('#'));
                tab = $(href).addClass(self.options.hiddenClass);
                self.tabs = self.tabs.add(tab);
                self.visibleActiveTab(item, tab, href);
                console.log(href);
            });
        },
        visibleActiveTab: function(item, tab, href) {
            if (item.hasClass(this.options.activeClass) || (this.options.checkHash && location.hash === href)){
                this.showTab(item);
            }
        },
        handleLocationHash: function() {
            console.log(this.options.checkHash);
            console.log(this.tabLinks.filter('[' + this.options.attrb + '="' + location.hash + '"]').length);

            if (this.options.checkHash && this.tabLinks.filter('[' + this.options.attrb + '="' + location.hash + '"]').length) {
                this.tabItems.removeClass(this.options.activeClass);
                setTimeout(function() {
                    window.scrollTo(0, 0);
                }, 1);
            }
        },
        isActiveItem: function(item) {
            return item.hasClass(this.options.activeClass);
        },
        getActiveItem: function() {
            return this.tabItems.siblings('.' + this.options.activeClass);
        },
        getTargetTab: function(item) {
            var link = item.find(this.options.tabLink);
            return $(link.attr(this.options.attrb));
        },
        destroy: function() {
            var self = this;

            this.makeCallback('onDestroy', this);
            this.tabset.removeData('Tabs');
            this.tabItems.removeClass(this.options.activeClass);
            this.tabLinks.off(this.options.eventMode, this.onSwitchTab);
            this.tabLinks.each(function() {
                var link = $(this);
                var tab = $(link.attr(self.options.attrb));

                tab.removeClass(self.options.hiddenClass);
            });
        },
        makeCallback: function(name) {
            var args;

            if ($.isFunction(this.options[name])) {
                args = Array.prototype.slice.call(arguments);
                args.splice(0, 1);
                this.options[name].apply(this, args);
            }
        }
    };

    // jQuery plugin interface
    $.fn.tabs = function(options) {
        return this.each(function() {
            var elements = $(this);
            var instance = elements.data('Tabs');
            var settings;

            if (!instance) {
                settings = $.extend({}, options, { tabset: this });
                elements.data('Tabs', new Tabs(settings));
            }
        });
    };

    // export module
    if (typeof module !== 'undefined' && module.exports) {
        module.exports = Tabs;
    } else {
        window[exportName] = Tabs;
    };
}(jQuery, window, 'Tabs'));



/*
 * Simple Mobile Navigation
 */
;(function($) {
    function MobileNav(options) {
        this.options = $.extend({
            container: null,
            hideOnClickOutside: false,
            menuActiveClass: 'nav-active',
            menuOpener: '.nav-opener',
            menuDrop: '.nav-drop',
            toggleEvent: 'click',
            outsideClickEvent: 'click touchstart pointerdown MSPointerDown'
        }, options);
        this.initStructure();
        this.attachEvents();
    }
    MobileNav.prototype = {
        initStructure: function() {
            this.page = $('html');
            this.container = $(this.options.container);
            this.opener = this.container.find(this.options.menuOpener);
            this.drop = this.container.find(this.options.menuDrop);
        },
        attachEvents: function() {
            var self = this;

            if(activateResizeHandler) {
                activateResizeHandler();
                activateResizeHandler = null;
            }

            this.outsideClickHandler = function(e) {
                if(self.isOpened()) {
                    var target = $(e.target);
                    if(!target.closest(self.opener).length && !target.closest(self.drop).length) {
                        self.hide();
                    }
                }
            };

            this.openerClickHandler = function(e) {
                e.preventDefault();
                self.toggle();
            };

            this.opener.on(this.options.toggleEvent, this.openerClickHandler);
        },
        isOpened: function() {
            return this.container.hasClass(this.options.menuActiveClass);
        },
        show: function() {
            this.container.addClass(this.options.menuActiveClass);
            if(this.options.hideOnClickOutside) {
                this.page.on(this.options.outsideClickEvent, this.outsideClickHandler);
            }
        },
        hide: function() {
            this.container.removeClass(this.options.menuActiveClass);
            if(this.options.hideOnClickOutside) {
                this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
            }
        },
        toggle: function() {
            if(this.isOpened()) {
                this.hide();
            } else {
                this.show();
            }
        },
        destroy: function() {
            this.container.removeClass(this.options.menuActiveClass);
            this.opener.off(this.options.toggleEvent, this.clickHandler);
            this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
        }
    };

    var activateResizeHandler = function() {
        var win = $(window),
            doc = $('html'),
            resizeClass = 'resize-active',
            flag, timer;
        var removeClassHandler = function() {
            flag = false;
            doc.removeClass(resizeClass);
        };
        var resizeHandler = function() {
            if(!flag) {
                flag = true;
                doc.addClass(resizeClass);
            }
            clearTimeout(timer);
            timer = setTimeout(removeClassHandler, 500);
        };
        win.on('resize orientationchange', resizeHandler);
    };

    $.fn.mobileNav = function(opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        return this.each(function() {
            var $container = jQuery(this);
            var instance = $container.data('MobileNav');

            if (typeof opt === 'object' || typeof opt === 'undefined') {
                $container.data('MobileNav', new MobileNav($.extend({
                    container: this
                }, opt)));
            } else if (typeof method === 'string' && instance) {
                if (typeof instance[method] === 'function') {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };
}(jQuery));

// navigation accesibility module
function TouchNav(opt) {
    this.options = {
        hoverClass: 'hover',
        menuItems: 'li',
        menuOpener: 'a',
        menuDrop: 'ul',
        navBlock: null,
        destroy: null
    };
    for (var p in opt) {
        if (opt.hasOwnProperty(p)) {
            this.options[p] = opt[p];
        }
    }
    // console.log(this)
    this.init();
}


TouchNav.isActiveOn = function (elem) {
    return elem && elem.touchNavActive;
};

TouchNav.prototype = {
    init: function () {
        if (typeof this.options.navBlock === 'string') {
            this.menu = document.getElementById(this.options.navBlock);
        } else if (typeof this.options.navBlock === 'object') {
            this.menu = this.options.navBlock;
        }
        if (this.menu) {
            this.hanldeEvents();
        }
    },
    hanldeEvents: function () {
        // attach event handlers
        var self = this;
        var touchEvent = (navigator.pointerEnabled && 'pointerdown') || (navigator.msPointerEnabled && 'MSPointerDown') || (this.isTouchDevice && 'touchstart');
        this.menuItems = lib.queryElementsBySelector(this.options.menuItems, this.menu);

        var initMenuItem = function (item) {
            var currentDrop = lib.queryElementsBySelector(self.options.menuDrop, item)[0],
                currentOpener = lib.queryElementsBySelector(self.options.menuOpener, item)[0];

            // only for touch input devices
            if (currentDrop && currentOpener && (self.isTouchDevice || self.isPointerDevice)) {
                if (!self.options.destroy) {
                    lib.event.add(currentOpener, 'click', lib.bind(self.clickHandler, self));
                    lib.event.add(currentOpener, 'mousedown', lib.bind(self.mousedownHandler, self));
                    lib.event.add(currentOpener, touchEvent, function (e) {
                        if (!self.isTouchPointerEvent(e)) {
                            self.preventCurrentClick = false;
                            return;
                        }
                        self.touchFlag = true;
                        self.currentItem = item;
                        self.currentLink = currentOpener;
                        self.pressHandler.apply(self, arguments);
                    });
                } else {
                    lib.event.remove(currentOpener, 'click');
                    lib.event.remove(currentOpener, 'mousedown');
                    lib.event.remove(currentOpener, touchEvent);
                }
            }
            if (!self.options.destroy) {
                // for desktop computers and touch devices
                jQuery(item).bind('mouseenter', function () {
                    if (!self.touchFlag) {
                        self.currentItem = item;
                        self.mouseoverHandler();
                    }
                });
                jQuery(item).bind('mouseleave', function () {
                    if (!self.touchFlag) {
                        self.currentItem = item;
                        self.mouseoutHandler();
                    }
                });
                item.touchNavActive = true;

            } else {
                jQuery(item).unbind('mouseenter');
                jQuery(item).unbind('mouseleave');

                item.touchNavActive = false;

            }

        };

        // addd handlers for all menu items
        for (var i = 0; i < this.menuItems.length; i++) {
            initMenuItem(self.menuItems[i]);
        }

        // hide dropdowns when clicking outside navigation
        if (this.isTouchDevice || this.isPointerDevice) {
            lib.event.add(document.documentElement, 'mousedown', lib.bind(this.clickOutsideHandler, this));
            lib.event.add(document.documentElement, touchEvent, lib.bind(this.clickOutsideHandler, this));
        }
    },
    mousedownHandler: function (e) {
        if (this.touchFlag) {
            e.preventDefault();
            this.touchFlag = false;
            this.preventCurrentClick = false;
        }
    },
    mouseoverHandler: function () {
        lib.addClass(this.currentItem, this.options.hoverClass);
        /**/
        // jQuery(this.currentItem).trigger('itemhover');
        /**/
    },
    mouseoutHandler: function () {
        lib.removeClass(this.currentItem, this.options.hoverClass);
        /**/
        // jQuery(this.currentItem).trigger('itemleave');
        /**/
    },
    hideActiveDropdown: function () {
        for (var i = 0; i < this.menuItems.length; i++) {
            if (lib.hasClass(this.menuItems[i], this.options.hoverClass)) {
                lib.removeClass(this.menuItems[i], this.options.hoverClass);
                /**/
                // jQuery(this.menuItems[i]).trigger('itemleave');
                /**/
            }
        }
        this.activeParent = null;
    },
    pressHandler: function (e) {
        // hide previous drop (if active)
        if (this.currentItem !== this.activeParent) {
            if (this.activeParent && this.currentItem.parentNode === this.activeParent.parentNode) {
                lib.removeClass(this.activeParent, this.options.hoverClass);
            } else if (!this.isParent(this.activeParent, this.currentLink)) {
                this.hideActiveDropdown();
            }
        }
        // handle current drop
        this.activeParent = this.currentItem;
        if (lib.hasClass(this.currentItem, this.options.hoverClass)) {
            this.preventCurrentClick = false;
        } else {
            e.preventDefault();
            this.preventCurrentClick = true;
            lib.addClass(this.currentItem, this.options.hoverClass);
            /**/
            // jQuery(this.currentItem).trigger('itemhover');
            /**/
        }
    },
    clickHandler: function (e) {
        // prevent first click on link
        if (this.preventCurrentClick) {
            e.preventDefault();
        }
    },
    clickOutsideHandler: function (event) {
        var e = event.changedTouches ? event.changedTouches[0] : event;
        if (this.activeParent && !this.isParent(this.menu, e.target)) {
            this.hideActiveDropdown();
            this.touchFlag = false;
        }
    },
    isParent: function (parent, child) {
        while (child.parentNode) {
            if (child.parentNode == parent) {
                return true;
            }
            child = child.parentNode;
        }
        return false;
    },

    isTouchPointerEvent: function (e) {
        return (e.type.indexOf('touch') > -1) ||
            (navigator.pointerEnabled && e.pointerType === 'touch') ||
            (navigator.msPointerEnabled && e.pointerType == e.MSPOINTER_TYPE_TOUCH);
    },
    isPointerDevice: (function () {
        return !!(navigator.pointerEnabled || navigator.msPointerEnabled);
    }()),
    isTouchDevice: (function () {
        return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
    }())
};

/*
 * Utility module
 */
lib = {
    hasClass: function(el,cls) {
        return el && el.className ? el.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)')) : false;
    },
    addClass: function(el,cls) {
        if (el && !this.hasClass(el,cls)) el.className += " "+cls;
    },
    removeClass: function(el,cls) {
        if (el && this.hasClass(el,cls)) {el.className=el.className.replace(new RegExp('(\\s|^)'+cls+'(\\s|$)'),' ');}
    },
    extend: function(obj) {
        for(var i = 1; i < arguments.length; i++) {
            for(var p in arguments[i]) {
                if(arguments[i].hasOwnProperty(p)) {
                    obj[p] = arguments[i][p];
                }
            }
        }
        return obj;
    },
    each: function(obj, callback) {
        var property, len;
        if(typeof obj.length === 'number') {
            for(property = 0, len = obj.length; property < len; property++) {
                if(callback.call(obj[property], property, obj[property]) === false) {
                    break;
                }
            }
        } else {
            for(property in obj) {
                if(obj.hasOwnProperty(property)) {
                    if(callback.call(obj[property], property, obj[property]) === false) {
                        break;
                    }
                }
            }
        }
    },
    event: (function() {
        var fixEvent = function(e) {
            e = e || window.event;
            if(e.isFixed) return e; else e.isFixed = true;
            if(!e.target) e.target = e.srcElement;
            e.preventDefault = e.preventDefault || function() {this.returnValue = false;};
            e.stopPropagation = e.stopPropagation || function() {this.cancelBubble = true;};
            return e;
        };
        return {
            add: function(elem, event, handler) {
                if(!elem.events) {
                    elem.events = {};
                    elem.handle = function(e) {
                        var ret, handlers = elem.events[e.type];
                        e = fixEvent(e);
                        for(var i = 0, len = handlers.length; i < len; i++) {
                            if(handlers[i]) {
                                ret = handlers[i].call(elem, e);
                                if(ret === false) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            }
                        }
                    };
                }
                if(!elem.events[event]) {
                    elem.events[event] = [];
                    if(elem.addEventListener) elem.addEventListener(event, elem.handle, false);
                    else if(elem.attachEvent) elem.attachEvent('on'+event, elem.handle);
                }
                elem.events[event].push(handler);
            },
            remove: function(elem, event, handler) {
                var handlers = elem.events[event];
                for(var i = handlers.length - 1; i >= 0; i--) {
                    if(handlers[i] === handler) {
                        handlers.splice(i,1);
                    }
                }
                if(!handlers.length) {
                    delete elem.events[event];
                    if(elem.removeEventListener) elem.removeEventListener(event, elem.handle, false);
                    else if(elem.detachEvent) elem.detachEvent('on'+event, elem.handle);
                }
            }
        };
    }()),
    queryElementsBySelector: function(selector, scope) {
        scope = scope || document;
        if(!selector) return [];
        if(selector === '>*') return scope.children;
        if(typeof document.querySelectorAll === 'function') {
            return scope.querySelectorAll(selector);
        }
        var selectors = selector.split(',');
        var resultList = [];
        for(var s = 0; s < selectors.length; s++) {
            var currentContext = [scope || document];
            var tokens = selectors[s].replace(/^\s+/,'').replace(/\s+$/,'').split(' ');
            for (var i = 0; i < tokens.length; i++) {
                token = tokens[i].replace(/^\s+/,'').replace(/\s+$/,'');
                if (token.indexOf('#') > -1) {
                    var bits = token.split('#'), tagName = bits[0], id = bits[1];
                    var element = document.getElementById(id);
                    if (element && tagName && element.nodeName.toLowerCase() != tagName) {
                        return [];
                    }
                    currentContext = element ? [element] : [];
                    continue;
                }
                if (token.indexOf('.') > -1) {
                    var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; j < elements.length; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (found[k].className && found[k].className.match(new RegExp('(\\s|^)'+className+'(\\s|$)'))) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
                    var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
                    if(attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
                        attrName = 'htmlFor';
                    }
                    var found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; elements[j]; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0, checkFunction;
                    switch (attrOperator) {
                        case '=': checkFunction = function(e) { return (e.getAttribute(attrName) == attrValue) }; break;
                        case '~': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)'+attrValue+'(\\s|$)'))) }; break;
                        case '|': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('^'+attrValue+'-?'))) }; break;
                        case '^': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
                        case '$': checkFunction = function(e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
                        case '*': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
                        default : checkFunction = function(e) { return e.getAttribute(attrName) };
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (checkFunction(found[k])) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                tagName = token;
                var found = [], foundCount = 0;
                for (var h = 0; h < currentContext.length; h++) {
                    var elements = currentContext[h].getElementsByTagName(tagName);
                    for (var j = 0; j < elements.length; j++) {
                        found[foundCount++] = elements[j];
                    }
                }
                currentContext = found;
            }
            resultList = [].concat(resultList,currentContext);
        }
        return resultList;
    },
    trim: function (str) {
        return str.replace(/^\s+/, '').replace(/\s+$/, '');
    },
    bind: function(f, scope, forceArgs){
        return function() {return f.apply(scope, typeof forceArgs !== 'undefined' ? [forceArgs] : arguments);};
    }
};

/*
 * Add class plugin
 */
jQuery.fn.clickClass = function(opt) {
    var options = jQuery.extend({
        classAdd: 'add-class',
        addToParent: false,
        event: 'click'
    }, opt);

    return this.each(function() {
        var classItem = jQuery(this);
        if(options.addToParent) {
            if(typeof options.addToParent === 'boolean') {
                classItem = classItem.parent();
            } else {
                classItem = classItem.parents('.' + options.addToParent);
            }
        }
        jQuery(this).bind(options.event, function(e) {
            classItem.toggleClass(options.classAdd);
            e.preventDefault();
        });
    });
};


/*
 * Mobile hover plugin
 */
;(function($){

    var doc = jQuery(document);
    // detect device type
    var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
        isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);

    // define events
    var eventOn = (isTouchDevice && 'touchstart') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerdown') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerDown') || 'mouseenter',
        eventOff = (isTouchDevice && 'touchend') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerup') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerUp') || 'mouseleave';

    isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

    function toggleClass(opt, e) {
        var el = jQuery(e.currentTarget);

        if (!isTouchDevice) {
            el.one(eventOff, function(){
                jQuery(this).removeClass(opt.hoverClass);
            })
        } else {
            if (!el.hasClass(opt.hoverClass)) {
                jQuery(this).off('click.hover', function(e){e.preventDefault()});
                setTimeout(function() {
                    doc.on(eventOn + '.hover', function(e){
                        var target = jQuery(e.target);
                        if (!target.is(el) && !target.closest(el).length) {
                            el.removeClass(opt.hoverClass);
                            jQuery(this).off('click.hover');
                            doc.off(eventOn + '.hover')
                        }
                    })
                })
            }
        }

        if (!el.hasClass(opt.hoverClass)){
            if (e) e.preventDefault();
            el.addClass(opt.hoverClass);
        }
    }

    // jQuery plugin
    $.fn.touchHover = function(opt) {
        var options = $.extend({
            hoverClass: 'hover'
        }, opt);

        this.on(eventOn, toggleClass.bind(this, options));
        return this;
    };
}(jQuery));
