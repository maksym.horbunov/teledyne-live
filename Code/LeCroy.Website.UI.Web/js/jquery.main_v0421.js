﻿jQuery(function () {
    initAnchors();
    initStickyScrollBlock();
    initHideText();
    goToPage();
    initTabs();
    initRetinaCover();
    initSlickCarousel();
    initAccordion();
    initOpenClose();
    // initTogglerBtn();
    initBackgroundVideo();
    initInViewport();
    Responsive(); //function for mobile version added jan2021
    SectionAnimate();
    BannerSlider();
    HasTransparented();
    SliderBlock();
    rentSliderInfo();
    Dropdown();
    ContacUs()

    Tooltip();
});


function Tooltip() {
    jQuery('[data-toggle="tooltip"]').tooltip();
    jQuery('a[data-trigger="click"]').on("click", function (e) {
        e.preventDefault();
    })
}

function Dropdown() {
    jQuery(".datasheet-dropdown-menu").on("click", function () {
        jQuery(this).toggleClass("open");
    });
}

function ContacUs() {
    jQuery(".open-form").on("click", function () {
        jQuery(this).toggleClass("open");
    });
}


function SliderBlock() {
    jQuery(".slider-block-content").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: ".slider-block-action",
        fade: true,
        autoplay: 3000,

        prevArrow:
            '<div class="slick-prev"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 13L1 7L7 1" stroke="#0076C0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></div>',
        nextArrow:
            '<div class="slick-next"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 13L7 7L1 1" stroke="#0076C0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></div>',
    });

    jQuery(".slider-block-content").on("afterChange", function () {
        rentSliderInfo();
    });
}

function rentSliderInfo() {
    let searchWrap = jQuery(".slider-block"),
        searchInfo = searchWrap.find(".slider-block-info"),
        sliderItemLength = searchWrap.find(".slick-slide").length,
        sliderItemCurrent = searchWrap.find(".slick-current").index() + 1;
    searchInfo.find(".current").html(sliderItemCurrent);
    searchInfo.find(".length").html(sliderItemLength);
}

function HasTransparented() {
    if (jQuery(".has-header-transparented").length) {
        jQuery("body").toggleClass("header-transparented");
    }

    if (jQuery("main.reset").length) {
        jQuery("body").addClass("reset skew-style");
    }
}

function Responsive() {
    jQuery(".content-header .content-header-text").prepend("<span class='mobi-menu'></span>");
    jQuery(".mobi-menu").click(function () {
        jQuery("body").toggleClass("opener");
    });

    jQuery('#SiteContent_ucPageContentHeader_divContentHeader + .content-container .column-container .column-left-container .column-left, #SiteContent_ucPageContentHeader_divContentHeader + .content-container .column-container .column-left-container .column-right').wrapAll('<div class="responsive-sidebar"></div>');

    jQuery(".responsive-sidebar").prepend("<span class='mobi-menu-close'></span>");

    jQuery(".mobi-menu-close").click(function () {
        jQuery("body").toggleClass("opener");
    });
}


function BannerSlider() {
    jQuery(".banner-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true,
        arrows: false,
        dots: true,
    });
}

function SectionAnimate() {
    jQuery(window).on("load resize scroll", function () {
        let h = jQuery(window).height();
        jQuery(".section-automotive-list").each(function () {
            if ($(window).scrollTop() + h >= jQuery(this).offset().top) {
                jQuery(this).addClass("view");
            }
        });
    });
}

// in view port init
function initInViewport() {
    jQuery(".viewport-section.card-line").itemInViewport({
        visibleMode: 60,
    });
}

// background video init
function initBackgroundVideo() {
    jQuery(".bg-video").backgroundVideo({
        activeClass: "video-active",
    });
}

// initialize smooth anchor links
function initAnchors() {
    new SmoothScroll({
        anchorLinks: 'a[href^="#"]:not([href="#"])',
        extraOffset: 0, //48
        activeClasses: "parent",
        wheelBehavior: "none",
        closeDropList: closeDropList(),
    });

    function closeDropList() {
        var arr = document.querySelectorAll(".section-anchors .slide a");
        Array.prototype.forEach.call(arr, function (elem) {
            elem.addEventListener("click", function () {
                var listItems = document.querySelector(".section-anchors");
                var slide = document.querySelector(".section-anchors .slide");

                if (listItems.classList.contains("open")) {
                    listItems.classList.remove("open");
                    slide.classList.add("js-slide-hidden");
                }
            });
        });
    }
}

// initialize fixed blocks on scroll
function initStickyScrollBlock() {
    jQuery(".section-anchors").stickyScrollBlock({
        // setBoxHeight: true,
        activeClass: "fixed-position",
        positionType: "fixed",
    });
}

//plugin for hiding text
function initHideText() {
    jQuery.fn.liTextLength = function (options) {
        var o = jQuery.extend(
            {
                length: 150,
                afterLength: "...",
                fullText: true,
                moreText: "Read more...",
                lessText: "Hide",
            },
            options
        );
        return this.each(function () {
            var $el = $(this),
                elText = $.trim($el.text()),
                elLength = elText.length;
            if (elLength > o.length) {
                var textSlice = $.trim(elText.substr(0, o.length)),
                    textSliced = $.trim(elText.substr(o.length));
                if (textSlice.length < o.length) {
                    var textVisible = textSlice,
                        textHidden = $.trim(elText.substr(o.length));
                } else {
                    var arrSlice = textSlice.split(" "),
                        popped = arrSlice.pop(),
                        textVisible = arrSlice.join(" ") + " ",
                        textHidden = popped + textSliced + " ";
                }
                var $elTextHidden = $("<span>")
                    .addClass("elTextHidden")
                    .html(textHidden),
                    $afterLength = $("<span>")
                        .addClass("afterLength")
                        .html(o.afterLength + " "),
                    $more = $("<span>").addClass("text-link text-blue").html(o.moreText);
                $el.text(textVisible).append($afterLength).append($elTextHidden);
                var displayStyle = $elTextHidden.css("display");
                $elTextHidden.hide();
                if (o.fullText) {
                    $el.closest("div").append($more);
                    $more.click(function () {
                        // console.log('$el.closest("div"): ', $el.closest("div"));
                        if ($elTextHidden.is(":hidden")) {
                            $elTextHidden.css({ display: displayStyle });
                            $more.html(o.lessText);
                            $afterLength.hide();
                        } else {
                            $elTextHidden.hide();
                            $more.html(o.moreText);
                            $afterLength.show();
                        }
                        return false;
                    });
                } else {
                    $elTextHidden.remove();
                }
            }
        });
    };

    if (document.body.clientWidth < 768) {
        //init
        jQuery(".pr1").liTextLength({
            length: 180,
            afterLength: "...",
            fullText: true,
        });
        jQuery(".pr2").liTextLength({
            length: 170,
            afterLength: "...",
            fullText: true,
        });
        jQuery(".pr3").liTextLength({
            length: 170,
            afterLength: "...",
            fullText: true,
            // moreText: '<br>Read more...',
            // lessText: '<br>hide'
        });
    }
}

// init Selection
function goToPage() {
    jQuery("[data-js-href]").on("click", function () {
        var attr = jQuery(this).attr("data-js-href");

        if (this.getSelection) {
            var text = window.getSelection();
        } else if (document.getSelection) {
            var text = document.getSelection();
        } else if (document.selection) {
            var text = document.selection.createRange();
        }

        if (!text.toString()) {
            document.location.href = attr;
        }
    });
}

// init Tabs
function initTabs() {
    ResponsiveHelper.addRange({
        "768..": {
            on: function () {
                jQuery(".tabs-pci").tabs({
                    tabItems: ".lc-item",
                    attrb: "data-href",
                    checkHash: true,
                    // onChange: function(instance, currentItem) {
                    //     var dataHref = currentItem[0].childNodes[0].attributes[2].value;
                    //     var hash = location.hash.substr(1);
                    //     if (instance.options.checkHash) {
                    //         window.location.hash = dataHref;
                    //     }
                    // }
                });
            },
            off: function () {
                jQuery(".tabs-pci").tabs("destroy");
            },
        },
    });
    ResponsiveHelper.addRange({
        "768..": {
            on: function () {
                jQuery(".tabs-gen").tabs({
                    tabItems: ".gen-item",
                    tabLink: "a",
                    activeClass: "active-tab",
                    hiddenClass: "tab-hidden-md",
                    attrb: "data-href",
                });
            },
            off: function () {
                jQuery(".tabs-gen").tabs("destroy");
            },
        },
    });
}

function initRetinaCover() {
    jQuery(".bg-retina").retinaCover();
}

// slick init

function initSlickCarousel() {
    jQuery(".slider-releases").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        mobileFirst: true,
        prevArrow: '<a href="#" class="slick-prev">Previous</a>',
        nextArrow: '<a href="#" class="slick-next">Next</a>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                },
            },
        ],
    });

    ResponsiveHelper.addRange({
        "..767": {
            on: function () {
                jQuery(".tech-list-cards").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    rows: 2,
                    slidesPerRow: 2,
                    dots: true,
                    fade: false,
                    infinite: false,
                    arrows: false,
                    mobileFirst: true,
                });
            },
            off: function () {
                jQuery(".tech-list-cards").slick("unslick");
            },
        },
    });
}

// accordion menu init
function initAccordion() {
    ResponsiveHelper.addRange({
        "..767": {
            on: function () {
                jQuery(".acc-pci").slideAccordion({
                    opener: ".opener",
                    slider: ".slide-content",
                    activeClass: "show",
                    animSpeed: 300,
                    scrollToActiveItem: {
                        enable: true,
                        animSpeed: 0,
                        extraOffset: 75,
                    },
                });
            },
            off: function () {
                jQuery(".acc-pci").slideAccordion("destroy");
            },
        },
    });
    ResponsiveHelper.addRange({
        "..767": {
            on: function () {
                jQuery(".acc-gen").slideAccordion({
                    opener: ".opener-gen",
                    slider: ".slide-gen",
                    activeClass: "active",
                    animSpeed: 300,
                    scrollToActiveItem: {
                        enable: true,
                        animSpeed: 0,
                        extraOffset: 75,
                    },
                });
            },
            off: function () {
                jQuery(".acc-gen").slideAccordion("destroy");
            },
        },
    });
    ResponsiveHelper.addRange({
        "..575": {
            on: function () {
                jQuery(".acc-footer").slideAccordion({
                    opener: ".opener",
                    slider: ".slide",
                    animSpeed: 0,
                });
            },
            off: function () {
                jQuery(".acc-footer").slideAccordion("destroy");
            },
        },
    });
}

// open-close init
function initOpenClose() {
    ResponsiveHelper.addRange({
        "768..": {
            on: function () {
                jQuery(".open-close").openClose({
                    activeClass: "show",
                    opener: ".opener",
                    slider: ".slide",
                    animSpeed: 0,
                    hideOnClickOutside: true,
                    event: "hover",
                    effect: "none",
                });
            },
            off: function () {
                jQuery(".open-close").openClose("destroy");
            },
        },
    });

    jQuery(".init-more").openClose({
        opener: ".opener",
        activeClass: "open",
        slider: ".slide",
        effect: "none",
    });

    ResponsiveHelper.addRange({
        "..991": {
            on: function () {
                jQuery(".init-slide-anchors").openClose({
                    opener: ".opener",
                    activeClass: "open",
                    slider: ".slide",
                    hideOnClickOutside: true,
                    effect: "none",
                });
            },
            off: function () {
                jQuery(".init-slide-anchors").openClose("destroy");
            },
        },
    });
}

// function initTogglerBtn() {
//
//     $('.second-button').on('click', function () {
//         $('.animated-icon2').toggleClass('open');
//     });
//
//     $('.collapse').on('show.bs.collapse hide.bs.collapse', function(e) {
//         e.preventDefault();
//     });
//     $('[data-toggle="collapse"]').on('click', function(e) {
//         e.preventDefault();
//         $($(this).data('target')).toggleClass('show');
//     });
// }

/*
 * jQuery Open/Close plugin
 */
(function ($) {
    function OpenClose(options) {
        this.options = $.extend(
            {
                addClassBeforeAnimation: true,
                hideOnClickOutside: false,
                activeClass: "active",
                opener: ".opener",
                slider: ".slide",
                animSpeed: 400,
                effect: "fade",
                event: "click",
            },
            options
        );
        this.init();
    }
    OpenClose.prototype = {
        init: function () {
            if (this.options.holder) {
                this.findElements();
                this.attachEvents();
                this.makeCallback("onInit", this);
            }
        },
        findElements: function () {
            this.holder = $(this.options.holder);
            this.opener = this.holder.find(this.options.opener);
            this.slider = this.holder.find(this.options.slider);
        },
        attachEvents: function () {
            // add handler
            var self = this;
            this.eventHandler = function (e) {
                e.preventDefault();
                if (self.slider.hasClass(slideHiddenClass)) {
                    self.showSlide();
                } else {
                    self.hideSlide();
                }
            };
            self.opener.on(self.options.event, this.eventHandler);

            // hover mode handler
            if (self.options.event === "hover") {
                self.opener.on("mouseenter", function () {
                    if (!self.holder.hasClass(self.options.activeClass)) {
                        self.showSlide();
                    }
                });
                self.holder.on("mouseleave", function () {
                    self.hideSlide();
                });
            }

            // outside click handler
            self.outsideClickHandler = function (e) {
                if (self.options.hideOnClickOutside) {
                    var target = $(e.target);
                    if (!target.is(self.holder) && !target.closest(self.holder).length) {
                        self.hideSlide();
                    }
                }
            };

            // set initial styles
            if (this.holder.hasClass(this.options.activeClass)) {
                $(document).on("click touchstart", self.outsideClickHandler);
            } else {
                this.slider.addClass(slideHiddenClass);
            }
        },
        showSlide: function () {
            var self = this;
            if (self.options.addClassBeforeAnimation) {
                self.holder.addClass(self.options.activeClass);
            }
            self.slider.removeClass(slideHiddenClass);
            $(document).on("click touchstart", self.outsideClickHandler);

            self.makeCallback("animStart", true);
            toggleEffects[self.options.effect].show({
                box: self.slider,
                speed: self.options.animSpeed,
                complete: function () {
                    if (!self.options.addClassBeforeAnimation) {
                        self.holder.addClass(self.options.activeClass);
                    }
                    self.makeCallback("animEnd", true);
                },
            });
        },
        hideSlide: function () {
            var self = this;
            if (self.options.addClassBeforeAnimation) {
                self.holder.removeClass(self.options.activeClass);
            }
            $(document).off("click touchstart", self.outsideClickHandler);

            self.makeCallback("animStart", false);
            toggleEffects[self.options.effect].hide({
                box: self.slider,
                speed: self.options.animSpeed,
                complete: function () {
                    if (!self.options.addClassBeforeAnimation) {
                        self.holder.removeClass(self.options.activeClass);
                    }
                    self.slider.addClass(slideHiddenClass);
                    self.makeCallback("animEnd", false);
                },
            });
        },
        destroy: function () {
            this.slider.removeClass(slideHiddenClass).css({
                display: "",
            });
            this.opener
                .off(this.options.event, this.eventHandler)
                .removeClass(this.options.activeClass);
            this.holder.removeClass(this.options.activeClass).removeData("OpenClose");
            $(document).off("click touchstart", this.outsideClickHandler);
        },
        makeCallback: function (name) {
            if (typeof this.options[name] === "function") {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },
    };

    // add stylesheet for slide on DOMReady
    var slideHiddenClass = "js-slide-hidden";
    (function () {
        var tabStyleSheet = $('<style type="text/css">')[0];
        var tabStyleRule = "." + slideHiddenClass;
        tabStyleRule +=
            "{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}";
        if (tabStyleSheet.styleSheet) {
            tabStyleSheet.styleSheet.cssText = tabStyleRule;
        } else {
            tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
        }
        $("head").append(tabStyleSheet);
    })();

    // animation effects
    var toggleEffects = {
        slide: {
            show: function (o) {
                o.box.stop(true).hide().slideDown(o.speed, o.complete);
            },
            hide: function (o) {
                o.box.stop(true).slideUp(o.speed, o.complete);
            },
        },
        fade: {
            show: function (o) {
                o.box.stop(true).hide().fadeIn(o.speed, o.complete);
            },
            hide: function (o) {
                o.box.stop(true).fadeOut(o.speed, o.complete);
            },
        },
        none: {
            show: function (o) {
                o.box.hide().show(0, o.complete);
            },
            hide: function (o) {
                o.box.hide(0, o.complete);
            },
        },
    };

    // jQuery plugin interface
    $.fn.openClose = function (opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        return this.each(function () {
            var $holder = jQuery(this);
            var instance = $holder.data("OpenClose");

            if (typeof opt === "object" || typeof opt === "undefined") {
                $holder.data(
                    "OpenClose",
                    new OpenClose(
                        $.extend(
                            {
                                holder: this,
                            },
                            opt
                        )
                    )
                );
            } else if (typeof method === "string" && instance) {
                if (typeof instance[method] === "function") {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };
})(jQuery);

/*
 * Responsive Layout helper
 */
window.ResponsiveHelper = (function ($) {
    // init variables
    var handlers = [],
        prevWinWidth,
        win = $(window),
        nativeMatchMedia = false;

    // detect match media support
    if (window.matchMedia) {
        if (window.Window && window.matchMedia === Window.prototype.matchMedia) {
            nativeMatchMedia = true;
        } else if (window.matchMedia.toString().indexOf("native") > -1) {
            nativeMatchMedia = true;
        }
    }

    // prepare resize handler
    function resizeHandler() {
        var winWidth = win.width();
        if (winWidth !== prevWinWidth) {
            prevWinWidth = winWidth;

            // loop through range groups
            $.each(handlers, function (index, rangeObject) {
                // disable current active area if needed
                $.each(rangeObject.data, function (property, item) {
                    if (item.currentActive && !matchRange(item.range[0], item.range[1])) {
                        item.currentActive = false;
                        if (typeof item.disableCallback === "function") {
                            item.disableCallback();
                        }
                    }
                });

                // enable areas that match current width
                $.each(rangeObject.data, function (property, item) {
                    if (!item.currentActive && matchRange(item.range[0], item.range[1])) {
                        // make callback
                        item.currentActive = true;
                        if (typeof item.enableCallback === "function") {
                            item.enableCallback();
                        }
                    }
                });
            });
        }
    }
    win.bind("load resize orientationchange", resizeHandler);

    // test range
    function matchRange(r1, r2) {
        var mediaQueryString = "";
        if (r1 > 0) {
            mediaQueryString += "(min-width: " + r1 + "px)";
        }
        if (r2 < Infinity) {
            mediaQueryString +=
                (mediaQueryString ? " and " : "") + "(max-width: " + r2 + "px)";
        }
        return matchQuery(mediaQueryString, r1, r2);
    }

    // media query function
    function matchQuery(query, r1, r2) {
        if (window.matchMedia && nativeMatchMedia) {
            return matchMedia(query).matches;
        } else if (window.styleMedia) {
            return styleMedia.matchMedium(query);
        } else if (window.media) {
            return media.matchMedium(query);
        } else {
            return prevWinWidth >= r1 && prevWinWidth <= r2;
        }
    }

    // range parser
    function parseRange(rangeStr) {
        var rangeData = rangeStr.split("..");
        var x1 = parseInt(rangeData[0], 10) || -Infinity;
        var x2 = parseInt(rangeData[1], 10) || Infinity;
        return [x1, x2].sort(function (a, b) {
            return a - b;
        });
    }

    // export public functions
    return {
        addRange: function (ranges) {
            // parse data and add items to collection
            var result = { data: {} };
            $.each(ranges, function (property, data) {
                result.data[property] = {
                    range: parseRange(property),
                    enableCallback: data.on,
                    disableCallback: data.off,
                };
            });
            handlers.push(result);

            // call resizeHandler to recalculate all events
            prevWinWidth = null;
            resizeHandler();
        },
    };
})(jQuery);

/*
 * jQuery Accordion plugin new
 */
(function (root, factory) {
    "use strict";
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("jquery"));
    } else {
        root.SlideAccordion = factory(jQuery);
    }
})(this, function ($) {
    "use strict";
    var accHiddenClass = "js-acc-hidden";

    function SlideAccordion(options) {
        this.options = $.extend(
            true,
            {
                allowClickWhenExpanded: false,
                activeClass: "active",
                opener: ".opener",
                slider: ".slide",
                animSpeed: 300,
                collapsible: true,
                event: "click",
                scrollToActiveItem: {
                    enable: false,
                    breakpoint: 767, // max-width
                    animSpeed: 600,
                    extraOffset: null,
                },
            },
            options
        );
        this.init();
    }

    SlideAccordion.prototype = {
        init: function () {
            if (this.options.holder) {
                this.findElements();
                this.setStateOnInit();
                this.attachEvents();
                this.makeCallback("onInit");
            }
        },

        findElements: function () {
            this.$holder = $(this.options.holder).data("SlideAccordion", this);
            this.$items = this.$holder.find(":has(" + this.options.slider + ")");
        },

        setStateOnInit: function () {
            var self = this;

            this.$items.each(function () {
                if (!$(this).hasClass(self.options.activeClass)) {
                    $(this).find(self.options.slider).addClass(accHiddenClass);
                }
            });
        },

        attachEvents: function () {
            var self = this;

            this.accordionToggle = function (e) {
                var $item = jQuery(this).closest(self.$items);
                var $actiItem = self.getActiveItem($item);

                if (
                    !self.options.allowClickWhenExpanded ||
                    !$item.hasClass(self.options.activeClass)
                ) {
                    e.preventDefault();
                    self.toggle($item, $actiItem);
                }
            };

            this.$items.on(
                this.options.event,
                this.options.opener,
                this.accordionToggle
            );
        },

        toggle: function ($item, $prevItem) {
            if (!$item.hasClass(this.options.activeClass)) {
                this.show($item);
            } else if (this.options.collapsible) {
                this.hide($item);
            }

            if (!$item.is($prevItem) && $prevItem.length) {
                this.hide($prevItem);
            }

            this.makeCallback("beforeToggle");
        },

        show: function ($item) {
            var $slider = $item.find(this.options.slider);

            $item.addClass(this.options.activeClass);
            $slider
                .stop()
                .hide()
                .removeClass(accHiddenClass)
                .slideDown({
                    duration: this.options.animSpeed,
                    complete: function () {
                        $slider.removeAttr("style");
                        if (
                            this.options.scrollToActiveItem.enable &&
                            window.innerWidth <= this.options.scrollToActiveItem.breakpoint
                        ) {
                            this.goToItem($item);
                        }
                        this.makeCallback("onShow", $item);
                    }.bind(this),
                });

            this.makeCallback("beforeShow", $item);
        },

        hide: function ($item) {
            var $slider = $item.find(this.options.slider);

            $item.removeClass(this.options.activeClass);
            $slider
                .stop()
                .show()
                .slideUp({
                    duration: this.options.animSpeed,
                    complete: function () {
                        $slider.addClass(accHiddenClass);
                        $slider.removeAttr("style");
                        this.makeCallback("onHide", $item);
                    }.bind(this),
                });

            this.makeCallback("beforeHide", $item);
        },

        goToItem: function ($item) {
            var itemOffset = $item.offset().top;

            if (itemOffset < $(window).scrollTop()) {
                // handle extra offset
                if (typeof this.options.scrollToActiveItem.extraOffset === "number") {
                    itemOffset -= this.options.scrollToActiveItem.extraOffset;
                } else if (
                    typeof this.options.scrollToActiveItem.extraOffset === "function"
                ) {
                    itemOffset -= this.options.scrollToActiveItem.extraOffset();
                }

                $("body, html").animate(
                    {
                        scrollTop: itemOffset,
                    },
                    this.options.scrollToActiveItem.animSpeed
                );
            }
        },

        getActiveItem: function ($item) {
            return $item.siblings().filter("." + this.options.activeClass);
        },

        makeCallback: function (name) {
            if (typeof this.options[name] === "function") {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },

        destroy: function () {
            this.$holder.removeData("SlideAccordion");
            this.$items.off(
                this.options.event,
                this.options.opener,
                this.accordionToggle
            );
            this.$items.removeClass(this.options.activeClass).each(
                function (i, item) {
                    $(item)
                        .find(this.options.slider)
                        .removeAttr("style")
                        .removeClass(accHiddenClass);
                }.bind(this)
            );
            this.makeCallback("onDestroy");
        },
    };

    $.fn.slideAccordion = function (opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        return this.each(function () {
            var $holder = jQuery(this);
            var instance = $holder.data("SlideAccordion");

            if (typeof opt === "object" || typeof opt === "undefined") {
                new SlideAccordion(
                    $.extend(
                        true,
                        {
                            holder: this,
                        },
                        opt
                    )
                );
            } else if (typeof method === "string" && instance) {
                if (typeof instance[method] === "function") {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };

    (function () {
        var tabStyleSheet = $('<style type="text/css">')[0];
        var tabStyleRule = "." + accHiddenClass;
        tabStyleRule +=
            "{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important; width: 100% !important;}";
        if (tabStyleSheet.styleSheet) {
            tabStyleSheet.styleSheet.cssText = tabStyleRule;
        } else {
            tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
        }
        $("head").append(tabStyleSheet);
    })();

    return SlideAccordion;
});

/*
 * jQuery retina cover plugin
 */
(function ($) {
    "use strict";

    var styleRules = {};
    var templates = {
        "2x": [
            "(-webkit-min-device-pixel-ratio: 1.5)",
            "(min-resolution: 192dpi)",
            "(min-device-pixel-ratio: 1.5)",
            "(min-resolution: 1.5dppx)",
        ],
        "3x": [
            "(-webkit-min-device-pixel-ratio: 3)",
            "(min-resolution: 384dpi)",
            "(min-device-pixel-ratio: 3)",
            "(min-resolution: 3dppx)",
        ],
    };

    function addSimple(imageSrc, media, id) {
        var style = buildRule(id, imageSrc);

        addRule(media, style);
    }

    function addRetina(imageData, media, id) {
        var currentRules = templates[imageData[1]].slice();
        var patchedRules = currentRules;
        var style = buildRule(id, imageData[0]);

        if (media !== "default") {
            patchedRules = $.map(currentRules, function (ele, i) {
                return ele + " and " + media;
            });
        }

        media = patchedRules.join(",");

        addRule(media, style);
    }

    function buildRule(id, src) {
        return "#" + id + '{background-image: url("' + src + '");}';
    }

    function addRule(media, rule) {
        var $styleTag = styleRules[media];
        var styleTagData;
        var rules = "";

        if (media === "default") {
            rules = rule + " ";
        } else {
            rules = "@media " + media + "{" + rule + "}";
        }

        if (!$styleTag) {
            styleRules[media] = $("<style>").text(rules).appendTo("head");
        } else {
            styleTagData = $styleTag.text();
            styleTagData =
                styleTagData.substring(0, styleTagData.length - 2) + " }" + rule + "}";
            $styleTag.text(styleTagData);
        }
    }

    $.fn.retinaCover = function () {
        return this.each(function () {
            var $block = $(this);
            var $items = $block.children("[data-srcset]");
            var id = "bg-stretch" + Date.now() + (Math.random() * 1000).toFixed(0);

            if ($items.length) {
                $block.attr("id", id);

                $items.each(function () {
                    var $item = $(this);
                    var data = $item.data("srcset").split(", ");
                    var media = $item.data("media") || "default";
                    var dataLength = data.length;
                    var itemData;
                    var i;

                    for (i = 0; i < dataLength; i++) {
                        itemData = data[i].split(" ");

                        if (itemData.length === 1) {
                            addSimple(itemData[0], media, id);
                        } else {
                            addRetina(itemData, media, id);
                        }
                    }
                });
            }

            $items.detach();
        });
    };
})(jQuery);

/*
 * jQuery tab content plugin
 */
(function ($, window, exportName) {
    "use strict";

    var Tabs = function (options) {
        this.options = $.extend({}, Tabs.DEFAULTS, options);
        this.init();
    };

    var isTouchDevice =
        "ontouchstart" in window ||
        (window.DocumentTouch && document instanceof window.DocumentTouch) ||
        /Windows Phone/.test(navigator.userAgent);

    // default options
    Tabs.DEFAULTS = {
        tabItems: "li",
        tabLink: "a",
        attrb: "href",
        activeClass: "active",
        hiddenClass: "tab-hidden",
        eventMode: "click",
        collapsedTab: false,
        checkHash: false,

        // callbacks
        onInit: null,
        onChange: null,
        onShow: null,
        onHide: null,
        onDestroy: null,
    };

    Tabs.prototype = {
        init: function () {
            if (this.options.tabset) {
                this.initStructure();
                this.attachEvents();
                this.makeCallback("onInit", this);
            }
        },
        initStructure: function () {
            this.tabset = $(this.options.tabset);
            this.tabs = $();
            this.tabItems = this.tabset.children(this.options.tabItems);
            this.tabLinks = this.tabItems.children(this.options.tabLink);
            this.currentIndex = 0;
            this.eventMode = isTouchDevice ? "click" : this.options.eventMode;
            this.handleLocationHash();
            this.groupmentTabs();
        },
        attachEvents: function () {
            var self = this;

            // event handler
            this.onSwitchTab = function (event) {
                var link = $(this);
                var item = link.closest(self.options.tabItems);
                event.preventDefault();
                self.switchTab(item);
            };

            this.tabLinks.on(this.options.eventMode, this.onSwitchTab);
        },
        switchTab: function (item) {
            var itemActive = this.getActiveItem();

            if (this.isActiveItem(item)) {
                if (this.options.collapsedTab) {
                    this.hideTab(item);
                }
            } else {
                this.showTab(item);
                this.hideTab(itemActive);
            }

            this.makeCallback("onChange", this, item);
        },
        showTab: function (item) {
            item.addClass(this.options.activeClass);
            this.getTargetTab(item).removeClass(this.options.hiddenClass);
            this.makeCallback("onShow", this, item);
        },
        hideTab: function (item) {
            item.removeClass(this.options.activeClass);
            this.getTargetTab(item).addClass(this.options.hiddenClass);
            this.makeCallback("onHide", this, item);
        },
        nextTab: function () {
            var index = this.getActiveItem().index();
            this.currentIndex = index < this.tabItems.length - 1 ? index + 1 : 0;
            this.switchTab(this.tabItems.eq(this.currentIndex));
        },
        prevTab: function () {
            var index = this.getActiveItem().index();
            this.currentIndex = index > 0 ? index - 1 : this.tabItems.length - 1;
            this.switchTab(this.tabItems.eq(this.currentIndex));
        },
        numTab: function (num) {
            if (this.currentIndex !== num && num <= this.tabItems.length - 1) {
                this.currentIndex = num;
                this.currentItem = this.tabItems.eq(this.currentIndex);
                this.switchTab(this.currentItem);
            }
        },
        groupmentTabs: function () {
            var self = this;

            this.tabLinks.each(function () {
                var link = $(this);
                var item = link.closest(self.options.tabItems);
                var href = link.attr(self.options.attrb);
                var tab;
                href = href.substr(href.lastIndexOf("#"));
                tab = $(href).addClass(self.options.hiddenClass);
                self.tabs = self.tabs.add(tab);
                self.visibleActiveTab(item, tab, href);
                // console.log(href);
            });
        },
        visibleActiveTab: function (item, tab, href) {
            if (
                item.hasClass(this.options.activeClass) ||
                (this.options.checkHash && location.hash === href)
            ) {
                this.showTab(item);
            }
        },
        handleLocationHash: function () {
            // console.log(this.options.checkHash);
            // console.log(this.tabLinks.filter('[' + this.options.attrb + '="' + location.hash + '"]').length);

            if (
                this.options.checkHash &&
                this.tabLinks.filter(
                    "[" + this.options.attrb + '="' + location.hash + '"]'
                ).length
            ) {
                this.tabItems.removeClass(this.options.activeClass);
                setTimeout(function () {
                    window.scrollTo(0, 0);
                }, 1);
            }
        },
        isActiveItem: function (item) {
            return item.hasClass(this.options.activeClass);
        },
        getActiveItem: function () {
            return this.tabItems.siblings("." + this.options.activeClass);
        },
        getTargetTab: function (item) {
            var link = item.find(this.options.tabLink);
            return $(link.attr(this.options.attrb));
        },
        destroy: function () {
            var self = this;

            this.makeCallback("onDestroy", this);
            this.tabset.removeData("Tabs");
            this.tabItems.removeClass(this.options.activeClass);
            this.tabLinks.off(this.options.eventMode, this.onSwitchTab);
            this.tabLinks.each(function () {
                var link = $(this);
                var tab = $(link.attr(self.options.attrb));

                tab.removeClass(self.options.hiddenClass);
            });
        },
        makeCallback: function (name) {
            var args;

            if ($.isFunction(this.options[name])) {
                args = Array.prototype.slice.call(arguments);
                args.splice(0, 1);
                this.options[name].apply(this, args);
            }
        },
    };

    // jQuery plugin interface
    $.fn.tabs = function (options) {
        return this.each(function () {
            var elements = $(this);
            var instance = elements.data("Tabs");
            var settings;

            if (!instance) {
                settings = $.extend({}, options, { tabset: this });
                elements.data("Tabs", new Tabs(settings));
            }
        });
    };

    // export module
    if (typeof module !== "undefined" && module.exports) {
        module.exports = Tabs;
    } else {
        window[exportName] = Tabs;
    }
})(jQuery, window, "Tabs");

/*
 * Utility module
 */
lib = {
    hasClass: function (el, cls) {
        return el && el.className
            ? el.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"))
            : false;
    },
    addClass: function (el, cls) {
        if (el && !this.hasClass(el, cls)) el.className += " " + cls;
    },
    removeClass: function (el, cls) {
        if (el && this.hasClass(el, cls)) {
            el.className = el.className.replace(
                new RegExp("(\\s|^)" + cls + "(\\s|$)"),
                " "
            );
        }
    },
    extend: function (obj) {
        for (var i = 1; i < arguments.length; i++) {
            for (var p in arguments[i]) {
                if (arguments[i].hasOwnProperty(p)) {
                    obj[p] = arguments[i][p];
                }
            }
        }
        return obj;
    },
    each: function (obj, callback) {
        var property, len;
        if (typeof obj.length === "number") {
            for (property = 0, len = obj.length; property < len; property++) {
                if (callback.call(obj[property], property, obj[property]) === false) {
                    break;
                }
            }
        } else {
            for (property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (callback.call(obj[property], property, obj[property]) === false) {
                        break;
                    }
                }
            }
        }
    },
    event: (function () {
        var fixEvent = function (e) {
            e = e || window.event;
            if (e.isFixed) return e;
            else e.isFixed = true;
            if (!e.target) e.target = e.srcElement;
            e.preventDefault =
                e.preventDefault ||
                function () {
                    this.returnValue = false;
                };
            e.stopPropagation =
                e.stopPropagation ||
                function () {
                    this.cancelBubble = true;
                };
            return e;
        };
        return {
            add: function (elem, event, handler) {
                if (!elem.events) {
                    elem.events = {};
                    elem.handle = function (e) {
                        var ret,
                            handlers = elem.events[e.type];
                        e = fixEvent(e);
                        for (var i = 0, len = handlers.length; i < len; i++) {
                            if (handlers[i]) {
                                ret = handlers[i].call(elem, e);
                                if (ret === false) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            }
                        }
                    };
                }
                if (!elem.events[event]) {
                    elem.events[event] = [];
                    if (elem.addEventListener)
                        elem.addEventListener(event, elem.handle, false);
                    else if (elem.attachEvent)
                        elem.attachEvent("on" + event, elem.handle);
                }
                elem.events[event].push(handler);
            },
            remove: function (elem, event, handler) {
                var handlers = elem.events[event];
                for (var i = handlers.length - 1; i >= 0; i--) {
                    if (handlers[i] === handler) {
                        handlers.splice(i, 1);
                    }
                }
                if (!handlers.length) {
                    delete elem.events[event];
                    if (elem.removeEventListener)
                        elem.removeEventListener(event, elem.handle, false);
                    else if (elem.detachEvent)
                        elem.detachEvent("on" + event, elem.handle);
                }
            },
        };
    })(),
    queryElementsBySelector: function (selector, scope) {
        scope = scope || document;
        if (!selector) return [];
        if (selector === ">*") return scope.children;
        if (typeof document.querySelectorAll === "function") {
            return scope.querySelectorAll(selector);
        }
        var selectors = selector.split(",");
        var resultList = [];
        for (var s = 0; s < selectors.length; s++) {
            var currentContext = [scope || document];
            var tokens = selectors[s]
                .replace(/^\s+/, "")
                .replace(/\s+$/, "")
                .split(" ");
            for (var i = 0; i < tokens.length; i++) {
                token = tokens[i].replace(/^\s+/, "").replace(/\s+$/, "");
                if (token.indexOf("#") > -1) {
                    var bits = token.split("#"),
                        tagName = bits[0],
                        id = bits[1];
                    var element = document.getElementById(id);
                    if (element && tagName && element.nodeName.toLowerCase() != tagName) {
                        return [];
                    }
                    currentContext = element ? [element] : [];
                    continue;
                }
                if (token.indexOf(".") > -1) {
                    var bits = token.split("."),
                        tagName = bits[0] || "*",
                        className = bits[1],
                        found = [],
                        foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == "*") {
                            elements = currentContext[h].getElementsByTagName("*");
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; j < elements.length; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (
                            found[k].className &&
                            found[k].className.match(
                                new RegExp("(\\s|^)" + className + "(\\s|$)")
                            )
                        ) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
                    var tagName = RegExp.$1 || "*",
                        attrName = RegExp.$2,
                        attrOperator = RegExp.$3,
                        attrValue = RegExp.$4;
                    if (
                        attrName.toLowerCase() == "for" &&
                        this.browser.msie &&
                        this.browser.version < 8
                    ) {
                        attrName = "htmlFor";
                    }
                    var found = [],
                        foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == "*") {
                            elements = currentContext[h].getElementsByTagName("*");
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; elements[j]; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0,
                        checkFunction;
                    switch (attrOperator) {
                        case "=":
                            checkFunction = function (e) {
                                return e.getAttribute(attrName) == attrValue;
                            };
                            break;
                        case "~":
                            checkFunction = function (e) {
                                return e
                                    .getAttribute(attrName)
                                    .match(new RegExp("(\\s|^)" + attrValue + "(\\s|$)"));
                            };
                            break;
                        case "|":
                            checkFunction = function (e) {
                                return e
                                    .getAttribute(attrName)
                                    .match(new RegExp("^" + attrValue + "-?"));
                            };
                            break;
                        case "^":
                            checkFunction = function (e) {
                                return e.getAttribute(attrName).indexOf(attrValue) == 0;
                            };
                            break;
                        case "$":
                            checkFunction = function (e) {
                                return (
                                    e.getAttribute(attrName).lastIndexOf(attrValue) ==
                                    e.getAttribute(attrName).length - attrValue.length
                                );
                            };
                            break;
                        case "*":
                            checkFunction = function (e) {
                                return e.getAttribute(attrName).indexOf(attrValue) > -1;
                            };
                            break;
                        default:
                            checkFunction = function (e) {
                                return e.getAttribute(attrName);
                            };
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (checkFunction(found[k])) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                tagName = token;
                var found = [],
                    foundCount = 0;
                for (var h = 0; h < currentContext.length; h++) {
                    var elements = currentContext[h].getElementsByTagName(tagName);
                    for (var j = 0; j < elements.length; j++) {
                        found[foundCount++] = elements[j];
                    }
                }
                currentContext = found;
            }
            resultList = [].concat(resultList, currentContext);
        }
        return resultList;
    },
    trim: function (str) {
        return str.replace(/^\s+/, "").replace(/\s+$/, "");
    },
    bind: function (f, scope, forceArgs) {
        return function () {
            return f.apply(
                scope,
                typeof forceArgs !== "undefined" ? [forceArgs] : arguments
            );
        };
    },
};

/*
 * Add class plugin
 */
jQuery.fn.clickClass = function (opt) {
    var options = jQuery.extend(
        {
            classAdd: "add-class",
            addToParent: false,
            event: "click",
        },
        opt
    );

    return this.each(function () {
        var classItem = jQuery(this);
        if (options.addToParent) {
            if (typeof options.addToParent === "boolean") {
                classItem = classItem.parent();
            } else {
                classItem = classItem.parents("." + options.addToParent);
            }
        }
        jQuery(this).bind(options.event, function (e) {
            classItem.toggleClass(options.classAdd);
            e.preventDefault();
        });
    });
};

/*!
 * SmoothScroll module
 */
(function ($, exports) {
    // private variables
    var page,
        win = $(window),
        activeBlock,
        activeWheelHandler,
        wheelEvents =
            "onwheel" in document || document.documentMode >= 9
                ? "wheel"
                : "mousewheel DOMMouseScroll";

    // animation handlers
    function scrollTo(offset, options, callback) {
        // initialize variables
        var scrollBlock;
        if (document.body) {
            if (typeof options === "number") {
                options = {
                    duration: options,
                };
            } else {
                options = options || {};
            }
            page = page || $("html, body");
            scrollBlock = options.container || page;
        } else {
            return;
        }

        // treat single number as scrollTop
        if (typeof offset === "number") {
            offset = {
                top: offset,
            };
        }

        // handle mousewheel/trackpad while animation is active
        if (activeBlock && activeWheelHandler) {
            activeBlock.off(wheelEvents, activeWheelHandler);
        }
        if (options.wheelBehavior && options.wheelBehavior !== "none") {
            activeWheelHandler = function (e) {
                if (options.wheelBehavior === "stop") {
                    scrollBlock.off(wheelEvents, activeWheelHandler);
                    scrollBlock.stop();
                } else if (options.wheelBehavior === "ignore") {
                    e.preventDefault();
                }
            };
            activeBlock = scrollBlock.on(wheelEvents, activeWheelHandler);
        }

        // start scrolling animation
        scrollBlock.stop().animate(
            {
                scrollLeft: offset.left,
                scrollTop: offset.top,
            },
            options.duration,
            function () {
                if (activeWheelHandler) {
                    scrollBlock.off(wheelEvents, activeWheelHandler);
                }
                if ($.isFunction(callback)) {
                    callback();
                }
            }
        );
    }

    // smooth scroll contstructor
    function SmoothScroll(options) {
        this.options = $.extend(
            {
                anchorLinks: 'a[href^="#"]', // selector or jQuery object
                container: null, // specify container for scrolling (default - whole page)
                extraOffset: null, // function or fixed number
                activeClasses: null, // null, "link", "parent"
                easing: "swing", // easing of scrolling
                animMode: "duration", // or "speed" mode
                animDuration: 800, // total duration for scroll (any distance)
                animSpeed: 1500, // pixels per second
                anchorActiveClass: "anchor-active",
                sectionActiveClass: "section-active",
                wheelBehavior: "stop", // "stop", "ignore" or "none"
                useNativeAnchorScrolling: false, // do not handle click in devices with native smooth scrolling
            },
            options
        );
        this.init();
    }
    SmoothScroll.prototype = {
        init: function () {
            this.initStructure();
            this.attachEvents();
            this.isInit = true;
        },
        initStructure: function () {
            var self = this;

            this.container = this.options.container
                ? $(this.options.container)
                : $("html,body");
            this.scrollContainer = this.options.container ? this.container : win;
            this.anchorLinks = jQuery(this.options.anchorLinks).filter(function () {
                return jQuery(self.getAnchorTarget(jQuery(this))).length;
            });
        },
        getId: function (str) {
            try {
                return "#" + str.replace(/^.*?(#|$)/, "");
            } catch (err) {
                return null;
            }
        },
        getAnchorTarget: function (link) {
            // get target block from link href
            var targetId = this.getId($(link).attr("href"));
            return $(targetId.length > 1 ? targetId : "html");
        },
        getTargetOffset: function (block) {
            // get target offset
            var blockOffset = block.offset().top;
            if (this.options.container) {
                blockOffset -=
                    this.container.offset().top - this.container.prop("scrollTop");
            }

            // handle extra offset
            if (typeof this.options.extraOffset === "number") {
                blockOffset -= this.options.extraOffset;
            } else if (typeof this.options.extraOffset === "function") {
                blockOffset -= this.options.extraOffset(block);
            }
            return {
                top: blockOffset,
            };
        },
        attachEvents: function () {
            var self = this;

            // handle active classes
            if (this.options.activeClasses && this.anchorLinks.length) {
                // cache structure
                this.anchorData = [];

                for (var i = 0; i < this.anchorLinks.length; i++) {
                    var link = jQuery(this.anchorLinks[i]),
                        targetBlock = self.getAnchorTarget(link),
                        anchorDataItem = null;

                    $.each(self.anchorData, function (index, item) {
                        if (item.block[0] === targetBlock[0]) {
                            anchorDataItem = item;
                        }
                    });

                    if (anchorDataItem) {
                        anchorDataItem.link = anchorDataItem.link.add(link);
                    } else {
                        self.anchorData.push({
                            link: link,
                            block: targetBlock,
                        });
                    }
                }

                // add additional event handlers
                this.resizeHandler = function () {
                    if (!self.isInit) return;
                    self.recalculateOffsets();
                };
                this.scrollHandler = function () {
                    self.refreshActiveClass();
                };

                this.recalculateOffsets();
                this.scrollContainer.on("scroll", this.scrollHandler);
                win.on(
                    "resize.SmoothScroll load.SmoothScroll orientationchange.SmoothScroll refreshAnchor.SmoothScroll",
                    this.resizeHandler
                );
            }

            // handle click event
            this.clickHandler = function (e) {
                self.onClick(e);
            };
            if (!this.options.useNativeAnchorScrolling) {
                this.anchorLinks.on("click", this.clickHandler);
            }
        },
        recalculateOffsets: function () {
            var self = this;
            $.each(this.anchorData, function (index, data) {
                data.offset = self.getTargetOffset(data.block);
                data.height = data.block.outerHeight();
            });
            this.refreshActiveClass();
        },
        toggleActiveClass: function (anchor, block, state) {
            anchor.toggleClass(this.options.anchorActiveClass, state);
            block.toggleClass(this.options.sectionActiveClass, state);
        },
        refreshActiveClass: function () {
            var self = this,
                foundFlag = false,
                containerHeight = this.container.prop("scrollHeight"),
                viewPortHeight = this.scrollContainer.height(),
                scrollTop = this.options.container
                    ? this.container.prop("scrollTop")
                    : win.scrollTop();

            // user function instead of default handler
            if (this.options.customScrollHandler) {
                this.options.customScrollHandler.call(this, scrollTop, this.anchorData);
                return;
            }

            // sort anchor data by offsets
            this.anchorData.sort(function (a, b) {
                return a.offset.top - b.offset.top;
            });

            // default active class handler
            $.each(this.anchorData, function (index) {
                var reverseIndex = self.anchorData.length - index - 1,
                    data = self.anchorData[reverseIndex],
                    anchorElement =
                        self.options.activeClasses === "parent"
                            ? data.link.parent()
                            : data.link;

                if (scrollTop >= containerHeight - viewPortHeight) {
                    // handle last section
                    if (reverseIndex === self.anchorData.length - 1) {
                        self.toggleActiveClass(anchorElement, data.block, true);
                    } else {
                        self.toggleActiveClass(anchorElement, data.block, false);
                    }
                } else {
                    // handle other sections
                    if (
                        !foundFlag &&
                        (scrollTop >= data.offset.top - 1 || reverseIndex === 0)
                    ) {
                        foundFlag = true;
                        self.toggleActiveClass(anchorElement, data.block, true);
                    } else {
                        self.toggleActiveClass(anchorElement, data.block, false);
                    }
                }
            });
        },
        calculateScrollDuration: function (offset) {
            var distance;
            if (this.options.animMode === "speed") {
                distance = Math.abs(this.scrollContainer.scrollTop() - offset.top);
                return (distance / this.options.animSpeed) * 1000;
            } else {
                return this.options.animDuration;
            }
        },
        onClick: function (e) {
            var targetBlock = this.getAnchorTarget(e.currentTarget),
                targetOffset = this.getTargetOffset(targetBlock);

            e.preventDefault();
            scrollTo(targetOffset, {
                container: this.container,
                wheelBehavior: this.options.wheelBehavior,
                duration: this.calculateScrollDuration(targetOffset),
            });
            this.makeCallback("onBeforeScroll", e.currentTarget);
        },
        makeCallback: function (name) {
            if (typeof this.options[name] === "function") {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },
        destroy: function () {
            var self = this;

            this.isInit = false;
            if (this.options.activeClasses) {
                win.off(
                    "resize.SmoothScroll load.SmoothScroll orientationchange.SmoothScroll refreshAnchor.SmoothScroll",
                    this.resizeHandler
                );
                this.scrollContainer.off("scroll", this.scrollHandler);
                $.each(this.anchorData, function (index) {
                    var reverseIndex = self.anchorData.length - index - 1,
                        data = self.anchorData[reverseIndex],
                        anchorElement =
                            self.options.activeClasses === "parent"
                                ? data.link.parent()
                                : data.link;

                    self.toggleActiveClass(anchorElement, data.block, false);
                });
            }
            this.anchorLinks.off("click", this.clickHandler);
        },
    };

    // public API
    $.extend(SmoothScroll, {
        scrollTo: function (blockOrOffset, durationOrOptions, callback) {
            scrollTo(blockOrOffset, durationOrOptions, callback);
        },
    });

    // export module
    exports.SmoothScroll = SmoothScroll;
})(jQuery, this);

/*
 * jQuery sticky box plugin
 */
(function ($, $win) {
    "use strict";

    function StickyScrollBlock($stickyBox, options) {
        this.options = options;
        this.$stickyBox = $stickyBox;
        this.init();
    }

    var StickyScrollBlockPrototype = {
        init: function () {
            this.findElements();
            this.attachEvents();
            this.makeCallback("onInit");
        },

        findElements: function () {
            // find parent container in which will be box move
            this.$container = this.$stickyBox.closest(this.options.container);
            // define box wrap flag
            this.isWrap =
                this.options.positionType === "fixed" && this.options.setBoxHeight;
            // define box move flag
            this.moveInContainer = !!this.$container.length;
            // wrapping box to set place in content
            if (this.isWrap) {
                this.$stickyBoxWrap = this.$stickyBox
                    .wrap('<div class="' + this.getWrapClass() + '"/>')
                    .parent();
            }
            //define block to add active class
            this.parentForActive = this.getParentForActive();
            this.isInit = true;
        },

        attachEvents: function () {
            var self = this;

            // bind events
            this.onResize = function () {
                if (!self.isInit) return;
                self.resetState();
                self.recalculateOffsets();
                self.checkStickyPermission();
                self.scrollHandler();
            };

            this.onScroll = function () {
                self.scrollHandler();
            };

            // initial handler call
            this.onResize();

            // handle events
            $win
                .on("load resize orientationchange", this.onResize)
                .on("scroll", this.onScroll);
        },

        defineExtraTop: function () {
            // define box's extra top dimension
            var extraTop;

            if (typeof this.options.extraTop === "number") {
                extraTop = this.options.extraTop;
            } else if (typeof this.options.extraTop === "function") {
                extraTop = this.options.extraTop();
            }

            this.extraTop =
                this.options.positionType === "absolute"
                    ? extraTop
                    : Math.min(this.winParams.height - this.data.boxFullHeight, extraTop);
        },

        checkStickyPermission: function () {
            // check the permission to set sticky
            this.isStickyEnabled = this.moveInContainer
                ? this.data.containerOffsetTop + this.data.containerHeight >
                this.data.boxFullHeight +
                this.data.boxOffsetTop +
                this.options.extraBottom
                : true;
        },

        getParentForActive: function () {
            if (this.isWrap) {
                return this.$stickyBoxWrap;
            }

            if (this.$container.length) {
                return this.$container;
            }

            return this.$stickyBox;
        },

        getWrapClass: function () {
            // get set of container classes
            try {
                return this.$stickyBox
                    .attr("class")
                    .split(" ")
                    .map(function (name) {
                        return "sticky-wrap-" + name;
                    })
                    .join(" ");
            } catch (err) {
                return "sticky-wrap";
            }
        },

        resetState: function () {
            // reset dimensions and state
            this.stickyFlag = false;
            this.$stickyBox
                .css({
                    "-webkit-transition": "",
                    "-webkit-transform": "",
                    transition: "",
                    transform: "",
                    position: "",
                    width: "",
                    left: "",
                    top: "",
                })
                .removeClass(this.options.activeClass);

            if (this.isWrap) {
                this.$stickyBoxWrap
                    .removeClass(this.options.activeClass)
                    .removeAttr("style");
            }

            if (this.moveInContainer) {
                this.$container.removeClass(this.options.activeClass);
            }
        },

        recalculateOffsets: function () {
            // define box and container dimensions
            this.winParams = this.getWindowParams();

            this.data = $.extend(this.getBoxOffsets(), this.getContainerOffsets());

            this.defineExtraTop();
        },

        getBoxOffsets: function () {
            function offetTop(obj) {
                obj.top = 0;
                return obj;
            }
            var boxOffset =
                this.$stickyBox.css("position") === "fixed"
                    ? offetTop(this.$stickyBox.offset())
                    : this.$stickyBox.offset();
            var boxPosition = this.$stickyBox.position();

            return {
                // sticky box offsets
                boxOffsetLeft: boxOffset.left,
                boxOffsetTop: boxOffset.top,
                // sticky box positions
                boxTopPosition: boxPosition.top,
                boxLeftPosition: boxPosition.left,
                // sticky box width/height
                boxFullHeight: this.$stickyBox.outerHeight(true),
                boxHeight: this.$stickyBox.outerHeight(),
                boxWidth: this.$stickyBox.outerWidth(),
            };
        },

        getContainerOffsets: function () {
            var containerOffset = this.moveInContainer
                ? this.$container.offset()
                : null;

            return containerOffset
                ? {
                    // container offsets
                    containerOffsetLeft: containerOffset.left,
                    containerOffsetTop: containerOffset.top,
                    // container height
                    containerHeight: this.$container.outerHeight(),
                }
                : {};
        },

        getWindowParams: function () {
            return {
                height: window.innerHeight || document.documentElement.clientHeight,
            };
        },

        makeCallback: function (name) {
            if (typeof this.options[name] === "function") {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },

        destroy: function () {
            this.isInit = false;
            // remove event handlers and styles
            $win
                .off("load resize orientationchange", this.onResize)
                .off("scroll", this.onScroll);
            this.resetState();
            this.$stickyBox.removeData("StickyScrollBlock");
            if (this.isWrap) {
                this.$stickyBox.unwrap();
            }
            this.makeCallback("onDestroy");
        },
    };

    var stickyMethods = {
        fixed: {
            scrollHandler: function () {
                this.winScrollTop = $win.scrollTop();
                var isActiveSticky =
                    this.winScrollTop -
                    (this.options.showAfterScrolled ? this.extraTop : 0) -
                    (this.options.showAfterScrolled
                        ? this.data.boxHeight + this.extraTop
                        : 0) >
                    this.data.boxOffsetTop - this.extraTop;

                if (isActiveSticky) {
                    this.isStickyEnabled && this.stickyOn();
                } else {
                    this.stickyOff();
                }
            },

            stickyOn: function () {
                if (!this.stickyFlag) {
                    this.stickyFlag = true;
                    this.parentForActive.addClass(this.options.activeClass);
                    this.$stickyBox.css({
                        width: this.data.boxWidth,
                        position: this.options.positionType,
                    });
                    if (this.isWrap) {
                        this.$stickyBoxWrap.css({
                            height: this.data.boxFullHeight,
                        });
                    }
                    this.makeCallback("fixedOn");
                }
                this.setDynamicPosition();
            },

            stickyOff: function () {
                if (this.stickyFlag) {
                    this.stickyFlag = false;
                    this.resetState();
                    this.makeCallback("fixedOff");
                }
            },

            setDynamicPosition: function () {
                this.$stickyBox.css({
                    top: this.getTopPosition(),
                    left: this.data.boxOffsetLeft - $win.scrollLeft(),
                });
            },

            getTopPosition: function () {
                if (this.moveInContainer) {
                    var currScrollTop =
                        this.winScrollTop + this.data.boxHeight + this.options.extraBottom;

                    return Math.min(
                        this.extraTop,
                        this.data.containerHeight +
                        this.data.containerOffsetTop -
                        currScrollTop
                    );
                } else {
                    return this.extraTop;
                }
            },
        },
        absolute: {
            scrollHandler: function () {
                this.winScrollTop = $win.scrollTop();
                var isActiveSticky =
                    this.winScrollTop > this.data.boxOffsetTop - this.extraTop;

                if (isActiveSticky) {
                    this.isStickyEnabled && this.stickyOn();
                } else {
                    this.stickyOff();
                }
            },

            stickyOn: function () {
                if (!this.stickyFlag) {
                    this.stickyFlag = true;
                    this.parentForActive.addClass(this.options.activeClass);
                    this.$stickyBox.css({
                        width: this.data.boxWidth,
                        transition: "transform " + this.options.animSpeed + "s ease",
                        "-webkit-transition":
                            "transform " + this.options.animSpeed + "s ease",
                    });

                    if (this.isWrap) {
                        this.$stickyBoxWrap.css({
                            height: this.data.boxFullHeight,
                        });
                    }

                    this.makeCallback("fixedOn");
                }

                this.clearTimer();
                this.timer = setTimeout(
                    function () {
                        this.setDynamicPosition();
                    }.bind(this),
                    this.options.animDelay * 1000
                );
            },

            stickyOff: function () {
                if (this.stickyFlag) {
                    this.clearTimer();
                    this.stickyFlag = false;

                    this.timer = setTimeout(
                        function () {
                            this.setDynamicPosition();
                            setTimeout(
                                function () {
                                    this.resetState();
                                }.bind(this),
                                this.options.animSpeed * 1000
                            );
                        }.bind(this),
                        this.options.animDelay * 1000
                    );
                    this.makeCallback("fixedOff");
                }
            },

            clearTimer: function () {
                clearTimeout(this.timer);
            },

            setDynamicPosition: function () {
                var topPosition = Math.max(0, this.getTopPosition());

                this.$stickyBox.css({
                    transform: "translateY(" + topPosition + "px)",
                    "-webkit-transform": "translateY(" + topPosition + "px)",
                });
            },

            getTopPosition: function () {
                var currTopPosition =
                    this.winScrollTop - this.data.boxOffsetTop + this.extraTop;

                if (this.moveInContainer) {
                    var currScrollTop =
                        this.winScrollTop + this.data.boxHeight + this.options.extraBottom;
                    var diffOffset = Math.abs(
                        Math.min(
                            0,
                            this.data.containerHeight +
                            this.data.containerOffsetTop -
                            currScrollTop -
                            this.extraTop
                        )
                    );

                    return currTopPosition - diffOffset;
                } else {
                    return currTopPosition;
                }
            },
        },
    };

    // jQuery plugin interface
    $.fn.stickyScrollBlock = function (opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];

        var options = $.extend(
            {
                container: null,
                positionType: "fixed", // 'fixed' or 'absolute'
                activeClass: "fixed-position",
                setBoxHeight: true,
                showAfterScrolled: false,
                extraTop: 0,
                extraBottom: 0,
                animDelay: 0.1,
                animSpeed: 0.2,
            },
            opt
        );

        return this.each(function () {
            var $stickyBox = jQuery(this);
            var instance = $stickyBox.data("StickyScrollBlock");

            if (typeof opt === "object" || typeof opt === "undefined") {
                StickyScrollBlock.prototype = $.extend(
                    stickyMethods[options.positionType],
                    StickyScrollBlockPrototype
                );
                $stickyBox.data(
                    "StickyScrollBlock",
                    new StickyScrollBlock($stickyBox, options)
                );
            } else if (typeof method === "string" && instance) {
                if (typeof instance[method] === "function") {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };

    // module exports
    window.StickyScrollBlock = StickyScrollBlock;
})(jQuery, jQuery(window));

/*
 * jQuery video background plugin
 */
(function ($, $win) {
    var BgVideoController = (function () {
        var videos = [];

        return {
            init: function () {
                $win.on(
                    "load.bgVideo resize.bgVideo orientationchange.bgVideo",
                    this.resizeHandler.bind(this)
                );
            },

            resizeHandler: function () {
                if (this.isInit) {
                    $.each(videos, this.resizeVideo.bind(this));
                }
            },

            buildPoster: function ($video, $holder) {
                $holder.css({
                    "background-image": "url(" + $video.attr("poster") + ")",
                    "background-repeat": "no-repeat",
                    "background-size": "cover",
                });
            },

            resizeVideo: function (i) {
                var item = videos[i];
                var styles = this.getDimensions({
                    videoRatio: item.ratio,
                    maskWidth: item.$holder.outerWidth(),
                    maskHeight: item.$holder.outerHeight(),
                });

                item.$video.css({
                    width: styles.width,
                    height: styles.height,
                    marginTop: styles.top,
                    marginLeft: styles.left,
                });
            },

            getRatio: function ($video) {
                return (
                    $video[0].videoWidth / $video[0].videoHeight ||
                    $video.attr("width") / $video.attr("height") ||
                    $video.width() / $video.height()
                );
            },

            getDimensions: function (data) {
                var ratio = data.videoRatio,
                    slideWidth = data.maskWidth,
                    slideHeight = slideWidth / ratio;

                if (slideHeight < data.maskHeight) {
                    slideHeight = data.maskHeight;
                    slideWidth = slideHeight * ratio;
                }
                return {
                    width: slideWidth,
                    height: slideHeight,
                    top: (data.maskHeight - slideHeight) / 2,
                    left: (data.maskWidth - slideWidth) / 2,
                };
            },

            add: function ($video, options) {
                var $holder = options.videoHolder
                    ? $video.closest(options.videoHolder)
                    : $video.parent();
                var item = {
                    $video: $video,
                    $holder: $holder,
                    options: options,
                };

                if ($video.attr("poster")) {
                    this.buildPoster($video, $holder);
                }

                if ($video[0].readyState) {
                    this.onVideoReady(item);
                    if ($video[0].paused) {
                        $video[0].play();
                    } else {
                        $holder.addClass(options.activeClass);
                    }
                } else {
                    $video.one(
                        "loadedmetadata",
                        function () {
                            if ($video[0].paused) {
                                $video[0].play();
                            } else {
                                $holder.addClass(options.activeClass);
                            }
                            this.onVideoReady(item);
                        }.bind(this)
                    );
                }

                $video.one(
                    "play",
                    function () {
                        $holder.addClass(options.activeClass);
                        this.makeCallback.apply($.extend(true, {}, this, item), ["onPlay"]);
                    }.bind(this)
                );

                this.makeCallback.apply($.extend(true, {}, this, item), ["onInit"]);
                return this;
            },

            onVideoReady: function (item) {
                if (!this.isInit) {
                    this.isInit = true;
                    this.init();
                }
                videos.push(
                    $.extend(item, {
                        ratio: this.getRatio(item.$video),
                    })
                );
                this.resizeVideo(videos.length - 1);
            },

            destroy: function ($video) {
                if (!$video) {
                    videos = videos.filter(this.destroySingle);
                } else {
                    videos = videos.filter(
                        function (item) {
                            var removeFlag = item.$video.is($video);

                            removeFlag && this.destroySingle(item);

                            return !removeFlag;
                        }.bind(this)
                    );
                }

                if (!videos.length) {
                    this.isInit = false;
                    $win.off(".bgVideo");
                }
            },

            destroySingle: function (item) {
                item.$video
                    .removeAttr("style")
                    .removeData("BackgroundVideo")[0]
                    .pause();
                item.$holder.removeClass(item.options.activeClass);
            },

            makeCallback: function (name) {
                if (typeof this.options[name] === "function") {
                    var args = Array.prototype.slice.call(arguments);
                    args.shift();
                    this.options[name].apply(this, args);
                }
            },
        };
    })();

    $.fn.backgroundVideo = function (opt) {
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];
        var options = $.extend(
            {
                activeClass: "video-active",
                videoHolder: null,
            },
            opt
        );

        return this.each(function () {
            var $video = jQuery(this);
            var instance = $video.data("BackgroundVideo");

            if (typeof opt === "object" || typeof opt === "undefined") {
                $video.data("BackgroundVideo", BgVideoController.add($video, options));
            } else if (typeof method === "string" && instance) {
                if (typeof instance[method] === "function") {
                    args.shift();
                    instance[method].apply(instance, args);
                }
            }
        });
    };

    window.BgVideoController = BgVideoController;

    return BgVideoController;
})(jQuery, jQuery(window));

/*
 * jQuery In Viewport plugin
 */
(function ($, $win) {
    "use strict";

    var ScrollDetector = (function () {
        var data = {};

        return {
            init: function () {
                var self = this;

                this.addHolder("win", $win);

                $win.on(
                    "load.blockInViewport resize.blockInViewport orientationchange.blockInViewport",
                    function () {
                        $.each(data, function (holderKey, holderData) {
                            self.calcHolderSize(holderData);

                            $.each(holderData.items, function (itemKey, itemData) {
                                self.calcItemSize(itemKey, itemData);
                            });
                        });
                    }
                );
            },

            addHolder: function (holderKey, $holder) {
                var self = this;
                var holderData = {
                    holder: $holder,
                    items: {},
                    props: {
                        height: 0,
                        scroll: 0,
                    },
                };

                data[holderKey] = holderData;

                $holder.on("scroll.blockInViewport", function () {
                    self.calcHolderScroll(holderData);

                    $.each(holderData.items, function (itemKey, itemData) {
                        self.calcItemScroll(itemKey, itemData);
                    });
                });

                this.calcHolderSize(data[holderKey]);
            },

            calcHolderSize: function (holderData) {
                var holderOffset =
                    window.self !== holderData.holder[0] ? holderData.holder.offset() : 0;

                holderData.props.height =
                    holderData.holder.get(0) === window
                        ? window.innerHeight || document.documentElement.clientHeight
                        : holderData.holder.outerHeight();
                holderData.props.offset = holderOffset ? holderOffset.top : 0;

                this.calcHolderScroll(holderData);
            },

            calcItemSize: function (itemKey, itemData) {
                itemData.offset =
                    itemData.$el.offset().top - itemData.holderProps.props.offset;
                itemData.height = itemData.$el.outerHeight();

                this.calcItemScroll(itemKey, itemData);
            },

            calcHolderScroll: function (holderData) {
                holderData.props.scroll = holderData.holder.scrollTop();
            },

            calcItemScroll: function (itemKey, itemData) {
                var itemInViewPortFromUp;
                var itemInViewPortFromDown;
                var itemOutViewPort;
                var holderProps = itemData.holderProps.props;

                switch (itemData.options.visibleMode) {
                    case 1:
                        itemInViewPortFromDown =
                            itemData.offset < holderProps.scroll + holderProps.height / 2 ||
                            itemData.offset + itemData.height <
                            holderProps.scroll + holderProps.height;
                        itemInViewPortFromUp =
                            itemData.offset > holderProps.scroll ||
                            itemData.offset + itemData.height >
                            holderProps.scroll + holderProps.height / 2;
                        break;

                    case 2:
                        itemInViewPortFromDown =
                            itemInViewPortFromDown ||
                            itemData.offset < holderProps.scroll + holderProps.height / 2 ||
                            itemData.offset + itemData.height / 2 <
                            holderProps.scroll + holderProps.height;
                        itemInViewPortFromUp =
                            itemInViewPortFromUp ||
                            itemData.offset + itemData.height / 2 > holderProps.scroll ||
                            itemData.offset + itemData.height >
                            holderProps.scroll + holderProps.height / 2;
                        break;

                    case 3:
                        itemInViewPortFromDown =
                            itemInViewPortFromDown ||
                            itemData.offset < holderProps.scroll + holderProps.height / 2 ||
                            itemData.offset < holderProps.scroll + holderProps.height;
                        itemInViewPortFromUp =
                            itemInViewPortFromUp ||
                            itemData.offset + itemData.height > holderProps.scroll ||
                            itemData.offset + itemData.height >
                            holderProps.scroll + holderProps.height / 2;
                        break;

                    default:
                        itemInViewPortFromDown =
                            itemInViewPortFromDown ||
                            itemData.offset < holderProps.scroll + holderProps.height / 2 ||
                            itemData.offset +
                            Math.min(itemData.options.visibleMode, itemData.height) <
                            holderProps.scroll + holderProps.height;
                        itemInViewPortFromUp =
                            itemInViewPortFromUp ||
                            itemData.offset +
                            itemData.height -
                            Math.min(itemData.options.visibleMode, itemData.height) >
                            holderProps.scroll ||
                            itemData.offset + itemData.height >
                            holderProps.scroll + holderProps.height / 2;
                        break;
                }

                if (itemInViewPortFromUp && itemInViewPortFromDown) {
                    if (!itemData.state) {
                        itemData.state = true;
                        itemData.$el
                            .addClass(itemData.options.activeClass)
                            .trigger("in-viewport", true);

                        if (
                            itemData.options.once ||
                            ($.isFunction(itemData.options.onShow) &&
                                itemData.options.onShow(itemData))
                        ) {
                            delete itemData.holderProps.items[itemKey];
                        }
                    }
                } else {
                    itemOutViewPort =
                        itemData.offset < holderProps.scroll + holderProps.height &&
                        itemData.offset + itemData.height > holderProps.scroll;

                    if ((itemData.state || isNaN(itemData.state)) && !itemOutViewPort) {
                        itemData.state = false;
                        itemData.$el
                            .removeClass(itemData.options.activeClass)
                            .trigger("in-viewport", false);
                    }
                }
            },

            addItem: function (el, options) {
                var itemKey = "item" + this.getRandomValue();
                var newItem = {
                    $el: $(el),
                    options: options,
                };
                var holderKeyDataName = "in-viewport-holder";

                var $holder = newItem.$el.closest(options.holder);
                var holderKey = $holder.data(holderKeyDataName);

                if (!$holder.length) {
                    holderKey = "win";
                } else if (!holderKey) {
                    holderKey = "holder" + this.getRandomValue();
                    $holder.data(holderKeyDataName, holderKey);

                    this.addHolder(holderKey, $holder);
                }

                newItem.holderProps = data[holderKey];

                data[holderKey].items[itemKey] = newItem;

                this.calcItemSize(itemKey, newItem);
            },

            getRandomValue: function () {
                return (Math.random() * 100000).toFixed(0);
            },

            destroy: function () {
                $win.off(".blockInViewport");

                $.each(data, function (key, value) {
                    value.holder.off(".blockInViewport");

                    $.each(value.items, function (key, value) {
                        value.$el.removeClass(value.options.activeClass);
                        value.$el.get(0).itemInViewportAdded = null;
                    });
                });

                data = {};
            },
        };
    })();

    ScrollDetector.init();

    $.fn.itemInViewport = function (options) {
        options = $.extend(
            {
                activeClass: "in-viewport",
                once: true,
                holder: "",
                visibleMode: 1, // 1 - full block, 2 - half block, 3 - immediate, 4... - custom
            },
            options
        );

        return this.each(function () {
            if (this.itemInViewportAdded) {
                return;
            }

            this.itemInViewportAdded = true;

            ScrollDetector.addItem(this, options);
        });
    };
})(jQuery, jQuery(window));

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/
 Version: 1.9.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues
 */
(function (i) {
    "use strict";
    "function" == typeof define && define.amd
        ? define(["jquery"], i)
        : "undefined" != typeof exports
            ? (module.exports = i(require("jquery")))
            : i(jQuery);
})(function (i) {
    "use strict";
    var e = window.Slick || {};
    (e = (function () {
        function e(e, o) {
            var s,
                n = this;
            (n.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: i(e),
                appendDots: i(e),
                arrows: !0,
                asNavFor: null,
                prevArrow:
                    '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow:
                    '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function (e, t) {
                    return i('<button type="button" />').text(t + 1);
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: 0.35,
                fade: !1,
                focusOnSelect: !1,
                focusOnChange: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3,
            }),
                (n.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    scrolling: !1,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    swiping: !1,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1,
                    unslicked: !1,
                }),
                i.extend(n, n.initials),
                (n.activeBreakpoint = null),
                (n.animType = null),
                (n.animProp = null),
                (n.breakpoints = []),
                (n.breakpointSettings = []),
                (n.cssTransitions = !1),
                (n.focussed = !1),
                (n.interrupted = !1),
                (n.hidden = "hidden"),
                (n.paused = !0),
                (n.positionProp = null),
                (n.respondTo = null),
                (n.rowCount = 1),
                (n.shouldClick = !0),
                (n.$slider = i(e)),
                (n.$slidesCache = null),
                (n.transformType = null),
                (n.transitionType = null),
                (n.visibilityChange = "visibilitychange"),
                (n.windowWidth = 0),
                (n.windowTimer = null),
                (s = i(e).data("slick") || {}),
                (n.options = i.extend({}, n.defaults, o, s)),
                (n.currentSlide = n.options.initialSlide),
                (n.originalSettings = n.options),
                "undefined" != typeof document.mozHidden
                    ? ((n.hidden = "mozHidden"),
                        (n.visibilityChange = "mozvisibilitychange"))
                    : "undefined" != typeof document.webkitHidden &&
                    ((n.hidden = "webkitHidden"),
                        (n.visibilityChange = "webkitvisibilitychange")),
                (n.autoPlay = i.proxy(n.autoPlay, n)),
                (n.autoPlayClear = i.proxy(n.autoPlayClear, n)),
                (n.autoPlayIterator = i.proxy(n.autoPlayIterator, n)),
                (n.changeSlide = i.proxy(n.changeSlide, n)),
                (n.clickHandler = i.proxy(n.clickHandler, n)),
                (n.selectHandler = i.proxy(n.selectHandler, n)),
                (n.setPosition = i.proxy(n.setPosition, n)),
                (n.swipeHandler = i.proxy(n.swipeHandler, n)),
                (n.dragHandler = i.proxy(n.dragHandler, n)),
                (n.keyHandler = i.proxy(n.keyHandler, n)),
                (n.instanceUid = t++),
                (n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/),
                n.registerBreakpoints(),
                n.init(!0);
        }
        var t = 0;
        return e;
    })()),
        (e.prototype.activateADA = function () {
            var i = this;
            i.$slideTrack
                .find(".slick-active")
                .attr({ "aria-hidden": "false" })
                .find("a, input, button, select")
                .attr({ tabindex: "0" });
        }),
        (e.prototype.addSlide = e.prototype.slickAdd = function (e, t, o) {
            var s = this;
            if ("boolean" == typeof t) (o = t), (t = null);
            else if (t < 0 || t >= s.slideCount) return !1;
            s.unload(),
                "number" == typeof t
                    ? 0 === t && 0 === s.$slides.length
                        ? i(e).appendTo(s.$slideTrack)
                        : o
                            ? i(e).insertBefore(s.$slides.eq(t))
                            : i(e).insertAfter(s.$slides.eq(t))
                    : o === !0
                        ? i(e).prependTo(s.$slideTrack)
                        : i(e).appendTo(s.$slideTrack),
                (s.$slides = s.$slideTrack.children(this.options.slide)),
                s.$slideTrack.children(this.options.slide).detach(),
                s.$slideTrack.append(s.$slides),
                s.$slides.each(function (e, t) {
                    i(t).attr("data-slick-index", e);
                }),
                (s.$slidesCache = s.$slides),
                s.reinit();
        }),
        (e.prototype.animateHeight = function () {
            var i = this;
            if (
                1 === i.options.slidesToShow &&
                i.options.adaptiveHeight === !0 &&
                i.options.vertical === !1
            ) {
                var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
                i.$list.animate({ height: e }, i.options.speed);
            }
        }),
        (e.prototype.animateSlide = function (e, t) {
            var o = {},
                s = this;
            s.animateHeight(),
                s.options.rtl === !0 && s.options.vertical === !1 && (e = -e),
                s.transformsEnabled === !1
                    ? s.options.vertical === !1
                        ? s.$slideTrack.animate(
                            { left: e },
                            s.options.speed,
                            s.options.easing,
                            t
                        )
                        : s.$slideTrack.animate(
                            { top: e },
                            s.options.speed,
                            s.options.easing,
                            t
                        )
                    : s.cssTransitions === !1
                        ? (s.options.rtl === !0 && (s.currentLeft = -s.currentLeft),
                            i({ animStart: s.currentLeft }).animate(
                                { animStart: e },
                                {
                                    duration: s.options.speed,
                                    easing: s.options.easing,
                                    step: function (i) {
                                        (i = Math.ceil(i)),
                                            s.options.vertical === !1
                                                ? ((o[s.animType] = "translate(" + i + "px, 0px)"),
                                                    s.$slideTrack.css(o))
                                                : ((o[s.animType] = "translate(0px," + i + "px)"),
                                                    s.$slideTrack.css(o));
                                    },
                                    complete: function () {
                                        t && t.call();
                                    },
                                }
                            ))
                        : (s.applyTransition(),
                            (e = Math.ceil(e)),
                            s.options.vertical === !1
                                ? (o[s.animType] = "translate3d(" + e + "px, 0px, 0px)")
                                : (o[s.animType] = "translate3d(0px," + e + "px, 0px)"),
                            s.$slideTrack.css(o),
                            t &&
                            setTimeout(function () {
                                s.disableTransition(), t.call();
                            }, s.options.speed));
        }),
        (e.prototype.getNavTarget = function () {
            var e = this,
                t = e.options.asNavFor;
            return t && null !== t && (t = i(t).not(e.$slider)), t;
        }),
        (e.prototype.asNavFor = function (e) {
            var t = this,
                o = t.getNavTarget();
            null !== o &&
                "object" == typeof o &&
                o.each(function () {
                    var t = i(this).slick("getSlick");
                    t.unslicked || t.slideHandler(e, !0);
                });
        }),
        (e.prototype.applyTransition = function (i) {
            var e = this,
                t = {};
            e.options.fade === !1
                ? (t[e.transitionType] =
                    e.transformType + " " + e.options.speed + "ms " + e.options.cssEase)
                : (t[e.transitionType] =
                    "opacity " + e.options.speed + "ms " + e.options.cssEase),
                e.options.fade === !1 ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
        }),
        (e.prototype.autoPlay = function () {
            var i = this;
            i.autoPlayClear(),
                i.slideCount > i.options.slidesToShow &&
                (i.autoPlayTimer = setInterval(
                    i.autoPlayIterator,
                    i.options.autoplaySpeed
                ));
        }),
        (e.prototype.autoPlayClear = function () {
            var i = this;
            i.autoPlayTimer && clearInterval(i.autoPlayTimer);
        }),
        (e.prototype.autoPlayIterator = function () {
            var i = this,
                e = i.currentSlide + i.options.slidesToScroll;
            i.paused ||
                i.interrupted ||
                i.focussed ||
                (i.options.infinite === !1 &&
                    (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1
                        ? (i.direction = 0)
                        : 0 === i.direction &&
                        ((e = i.currentSlide - i.options.slidesToScroll),
                            i.currentSlide - 1 === 0 && (i.direction = 1))),
                    i.slideHandler(e));
        }),
        (e.prototype.buildArrows = function () {
            var e = this;
            e.options.arrows === !0 &&
                ((e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow")),
                    (e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow")),
                    e.slideCount > e.options.slidesToShow
                        ? (e.$prevArrow
                            .removeClass("slick-hidden")
                            .removeAttr("aria-hidden tabindex"),
                            e.$nextArrow
                                .removeClass("slick-hidden")
                                .removeAttr("aria-hidden tabindex"),
                            e.htmlExpr.test(e.options.prevArrow) &&
                            e.$prevArrow.prependTo(e.options.appendArrows),
                            e.htmlExpr.test(e.options.nextArrow) &&
                            e.$nextArrow.appendTo(e.options.appendArrows),
                            e.options.infinite !== !0 &&
                            e.$prevArrow
                                .addClass("slick-disabled")
                                .attr("aria-disabled", "true"))
                        : e.$prevArrow
                            .add(e.$nextArrow)
                            .addClass("slick-hidden")
                            .attr({ "aria-disabled": "true", tabindex: "-1" }));
        }),
        (e.prototype.buildDots = function () {
            var e,
                t,
                o = this;
            if (o.options.dots === !0 && o.slideCount > o.options.slidesToShow) {
                for (
                    o.$slider.addClass("slick-dotted"),
                    t = i("<ul />").addClass(o.options.dotsClass),
                    e = 0;
                    e <= o.getDotCount();
                    e += 1
                )
                    t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));
                (o.$dots = t.appendTo(o.options.appendDots)),
                    o.$dots.find("li").first().addClass("slick-active");
            }
        }),
        (e.prototype.buildOut = function () {
            var e = this;
            (e.$slides = e.$slider
                .children(e.options.slide + ":not(.slick-cloned)")
                .addClass("slick-slide")),
                (e.slideCount = e.$slides.length),
                e.$slides.each(function (e, t) {
                    i(t)
                        .attr("data-slick-index", e)
                        .data("originalStyling", i(t).attr("style") || "");
                }),
                e.$slider.addClass("slick-slider"),
                (e.$slideTrack =
                    0 === e.slideCount
                        ? i('<div class="slick-track"/>').appendTo(e.$slider)
                        : e.$slides.wrapAll('<div class="slick-track"/>').parent()),
                (e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent()),
                e.$slideTrack.css("opacity", 0),
                (e.options.centerMode !== !0 && e.options.swipeToSlide !== !0) ||
                (e.options.slidesToScroll = 1),
                i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"),
                e.setupInfinite(),
                e.buildArrows(),
                e.buildDots(),
                e.updateDots(),
                e.setSlideClasses(
                    "number" == typeof e.currentSlide ? e.currentSlide : 0
                ),
                e.options.draggable === !0 && e.$list.addClass("draggable");
        }),
        (e.prototype.buildRows = function () {
            var i,
                e,
                t,
                o,
                s,
                n,
                r,
                l = this;
            if (
                ((o = document.createDocumentFragment()),
                    (n = l.$slider.children()),
                    l.options.rows > 0)
            ) {
                for (
                    r = l.options.slidesPerRow * l.options.rows,
                    s = Math.ceil(n.length / r),
                    i = 0;
                    i < s;
                    i++
                ) {
                    var d = document.createElement("div");
                    for (e = 0; e < l.options.rows; e++) {
                        var a = document.createElement("div");
                        for (t = 0; t < l.options.slidesPerRow; t++) {
                            var c = i * r + (e * l.options.slidesPerRow + t);
                            n.get(c) && a.appendChild(n.get(c));
                        }
                        d.appendChild(a);
                    }
                    o.appendChild(d);
                }
                l.$slider.empty().append(o),
                    l.$slider
                        .children()
                        .children()
                        .children()
                        .css({
                            width: 100 / l.options.slidesPerRow + "%",
                            display: "inline-block",
                        });
            }
        }),
        (e.prototype.checkResponsive = function (e, t) {
            var o,
                s,
                n,
                r = this,
                l = !1,
                d = r.$slider.width(),
                a = window.innerWidth || i(window).width();
            if (
                ("window" === r.respondTo
                    ? (n = a)
                    : "slider" === r.respondTo
                        ? (n = d)
                        : "min" === r.respondTo && (n = Math.min(a, d)),
                    r.options.responsive &&
                    r.options.responsive.length &&
                    null !== r.options.responsive)
            ) {
                s = null;
                for (o in r.breakpoints)
                    r.breakpoints.hasOwnProperty(o) &&
                        (r.originalSettings.mobileFirst === !1
                            ? n < r.breakpoints[o] && (s = r.breakpoints[o])
                            : n > r.breakpoints[o] && (s = r.breakpoints[o]));
                null !== s
                    ? null !== r.activeBreakpoint
                        ? (s !== r.activeBreakpoint || t) &&
                        ((r.activeBreakpoint = s),
                            "unslick" === r.breakpointSettings[s]
                                ? r.unslick(s)
                                : ((r.options = i.extend(
                                    {},
                                    r.originalSettings,
                                    r.breakpointSettings[s]
                                )),
                                    e === !0 && (r.currentSlide = r.options.initialSlide),
                                    r.refresh(e)),
                            (l = s))
                        : ((r.activeBreakpoint = s),
                            "unslick" === r.breakpointSettings[s]
                                ? r.unslick(s)
                                : ((r.options = i.extend(
                                    {},
                                    r.originalSettings,
                                    r.breakpointSettings[s]
                                )),
                                    e === !0 && (r.currentSlide = r.options.initialSlide),
                                    r.refresh(e)),
                            (l = s))
                    : null !== r.activeBreakpoint &&
                    ((r.activeBreakpoint = null),
                        (r.options = r.originalSettings),
                        e === !0 && (r.currentSlide = r.options.initialSlide),
                        r.refresh(e),
                        (l = s)),
                    e || l === !1 || r.$slider.trigger("breakpoint", [r, l]);
            }
        }),
        (e.prototype.changeSlide = function (e, t) {
            var o,
                s,
                n,
                r = this,
                l = i(e.currentTarget);
            switch (
            (l.is("a") && e.preventDefault(),
                l.is("li") || (l = l.closest("li")),
                (n = r.slideCount % r.options.slidesToScroll !== 0),
                (o = n
                    ? 0
                    : (r.slideCount - r.currentSlide) % r.options.slidesToScroll),
                e.data.message)
            ) {
                case "previous":
                    (s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o),
                        r.slideCount > r.options.slidesToShow &&
                        r.slideHandler(r.currentSlide - s, !1, t);
                    break;
                case "next":
                    (s = 0 === o ? r.options.slidesToScroll : o),
                        r.slideCount > r.options.slidesToShow &&
                        r.slideHandler(r.currentSlide + s, !1, t);
                    break;
                case "index":
                    var d =
                        0 === e.data.index
                            ? 0
                            : e.data.index || l.index() * r.options.slidesToScroll;
                    r.slideHandler(r.checkNavigable(d), !1, t),
                        l.children().trigger("focus");
                    break;
                default:
                    return;
            }
        }),
        (e.prototype.checkNavigable = function (i) {
            var e,
                t,
                o = this;
            if (((e = o.getNavigableIndexes()), (t = 0), i > e[e.length - 1]))
                i = e[e.length - 1];
            else
                for (var s in e) {
                    if (i < e[s]) {
                        i = t;
                        break;
                    }
                    t = e[s];
                }
            return i;
        }),
        (e.prototype.cleanUpEvents = function () {
            var e = this;
            e.options.dots &&
                null !== e.$dots &&
                (i("li", e.$dots)
                    .off("click.slick", e.changeSlide)
                    .off("mouseenter.slick", i.proxy(e.interrupt, e, !0))
                    .off("mouseleave.slick", i.proxy(e.interrupt, e, !1)),
                    e.options.accessibility === !0 &&
                    e.$dots.off("keydown.slick", e.keyHandler)),
                e.$slider.off("focus.slick blur.slick"),
                e.options.arrows === !0 &&
                e.slideCount > e.options.slidesToShow &&
                (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide),
                    e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide),
                    e.options.accessibility === !0 &&
                    (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler),
                        e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))),
                e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler),
                e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler),
                e.$list.off("touchend.slick mouseup.slick", e.swipeHandler),
                e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler),
                e.$list.off("click.slick", e.clickHandler),
                i(document).off(e.visibilityChange, e.visibility),
                e.cleanUpSlideEvents(),
                e.options.accessibility === !0 &&
                e.$list.off("keydown.slick", e.keyHandler),
                e.options.focusOnSelect === !0 &&
                i(e.$slideTrack).children().off("click.slick", e.selectHandler),
                i(window).off(
                    "orientationchange.slick.slick-" + e.instanceUid,
                    e.orientationChange
                ),
                i(window).off("resize.slick.slick-" + e.instanceUid, e.resize),
                i("[draggable!=true]", e.$slideTrack).off(
                    "dragstart",
                    e.preventDefault
                ),
                i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition);
        }),
        (e.prototype.cleanUpSlideEvents = function () {
            var e = this;
            e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)),
                e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1));
        }),
        (e.prototype.cleanUpRows = function () {
            var i,
                e = this;
            e.options.rows > 0 &&
                ((i = e.$slides.children().children()),
                    i.removeAttr("style"),
                    e.$slider.empty().append(i));
        }),
        (e.prototype.clickHandler = function (i) {
            var e = this;
            e.shouldClick === !1 &&
                (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault());
        }),
        (e.prototype.destroy = function (e) {
            var t = this;
            t.autoPlayClear(),
                (t.touchObject = {}),
                t.cleanUpEvents(),
                i(".slick-cloned", t.$slider).detach(),
                t.$dots && t.$dots.remove(),
                t.$prevArrow &&
                t.$prevArrow.length &&
                (t.$prevArrow
                    .removeClass("slick-disabled slick-arrow slick-hidden")
                    .removeAttr("aria-hidden aria-disabled tabindex")
                    .css("display", ""),
                    t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()),
                t.$nextArrow &&
                t.$nextArrow.length &&
                (t.$nextArrow
                    .removeClass("slick-disabled slick-arrow slick-hidden")
                    .removeAttr("aria-hidden aria-disabled tabindex")
                    .css("display", ""),
                    t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()),
                t.$slides &&
                (t.$slides
                    .removeClass(
                        "slick-slide slick-active slick-center slick-visible slick-current"
                    )
                    .removeAttr("aria-hidden")
                    .removeAttr("data-slick-index")
                    .each(function () {
                        i(this).attr("style", i(this).data("originalStyling"));
                    }),
                    t.$slideTrack.children(this.options.slide).detach(),
                    t.$slideTrack.detach(),
                    t.$list.detach(),
                    t.$slider.append(t.$slides)),
                t.cleanUpRows(),
                t.$slider.removeClass("slick-slider"),
                t.$slider.removeClass("slick-initialized"),
                t.$slider.removeClass("slick-dotted"),
                (t.unslicked = !0),
                e || t.$slider.trigger("destroy", [t]);
        }),
        (e.prototype.disableTransition = function (i) {
            var e = this,
                t = {};
            (t[e.transitionType] = ""),
                e.options.fade === !1 ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
        }),
        (e.prototype.fadeSlide = function (i, e) {
            var t = this;
            t.cssTransitions === !1
                ? (t.$slides.eq(i).css({ zIndex: t.options.zIndex }),
                    t.$slides
                        .eq(i)
                        .animate({ opacity: 1 }, t.options.speed, t.options.easing, e))
                : (t.applyTransition(i),
                    t.$slides.eq(i).css({ opacity: 1, zIndex: t.options.zIndex }),
                    e &&
                    setTimeout(function () {
                        t.disableTransition(i), e.call();
                    }, t.options.speed));
        }),
        (e.prototype.fadeSlideOut = function (i) {
            var e = this;
            e.cssTransitions === !1
                ? e.$slides
                    .eq(i)
                    .animate(
                        { opacity: 0, zIndex: e.options.zIndex - 2 },
                        e.options.speed,
                        e.options.easing
                    )
                : (e.applyTransition(i),
                    e.$slides.eq(i).css({ opacity: 0, zIndex: e.options.zIndex - 2 }));
        }),
        (e.prototype.filterSlides = e.prototype.slickFilter = function (i) {
            var e = this;
            null !== i &&
                ((e.$slidesCache = e.$slides),
                    e.unload(),
                    e.$slideTrack.children(this.options.slide).detach(),
                    e.$slidesCache.filter(i).appendTo(e.$slideTrack),
                    e.reinit());
        }),
        (e.prototype.focusHandler = function () {
            var e = this;
            e.$slider
                .off("focus.slick blur.slick")
                .on("focus.slick", "*", function (t) {
                    var o = i(this);
                    setTimeout(function () {
                        e.options.pauseOnFocus &&
                            o.is(":focus") &&
                            ((e.focussed = !0), e.autoPlay());
                    }, 0);
                })
                .on("blur.slick", "*", function (t) {
                    i(this);
                    e.options.pauseOnFocus && ((e.focussed = !1), e.autoPlay());
                });
        }),
        (e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
            var i = this;
            return i.currentSlide;
        }),
        (e.prototype.getDotCount = function () {
            var i = this,
                e = 0,
                t = 0,
                o = 0;
            if (i.options.infinite === !0)
                if (i.slideCount <= i.options.slidesToShow) ++o;
                else
                    for (; e < i.slideCount;)
                        ++o,
                            (e = t + i.options.slidesToScroll),
                            (t +=
                                i.options.slidesToScroll <= i.options.slidesToShow
                                    ? i.options.slidesToScroll
                                    : i.options.slidesToShow);
            else if (i.options.centerMode === !0) o = i.slideCount;
            else if (i.options.asNavFor)
                for (; e < i.slideCount;)
                    ++o,
                        (e = t + i.options.slidesToScroll),
                        (t +=
                            i.options.slidesToScroll <= i.options.slidesToShow
                                ? i.options.slidesToScroll
                                : i.options.slidesToShow);
            else
                o =
                    1 +
                    Math.ceil(
                        (i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll
                    );
            return o - 1;
        }),
        (e.prototype.getLeft = function (i) {
            var e,
                t,
                o,
                s,
                n = this,
                r = 0;
            return (
                (n.slideOffset = 0),
                (t = n.$slides.first().outerHeight(!0)),
                n.options.infinite === !0
                    ? (n.slideCount > n.options.slidesToShow &&
                        ((n.slideOffset = n.slideWidth * n.options.slidesToShow * -1),
                            (s = -1),
                            n.options.vertical === !0 &&
                            n.options.centerMode === !0 &&
                            (2 === n.options.slidesToShow
                                ? (s = -1.5)
                                : 1 === n.options.slidesToShow && (s = -2)),
                            (r = t * n.options.slidesToShow * s)),
                        n.slideCount % n.options.slidesToScroll !== 0 &&
                        i + n.options.slidesToScroll > n.slideCount &&
                        n.slideCount > n.options.slidesToShow &&
                        (i > n.slideCount
                            ? ((n.slideOffset =
                                (n.options.slidesToShow - (i - n.slideCount)) *
                                n.slideWidth *
                                -1),
                                (r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1))
                            : ((n.slideOffset =
                                (n.slideCount % n.options.slidesToScroll) *
                                n.slideWidth *
                                -1),
                                (r = (n.slideCount % n.options.slidesToScroll) * t * -1))))
                    : i + n.options.slidesToShow > n.slideCount &&
                    ((n.slideOffset =
                        (i + n.options.slidesToShow - n.slideCount) * n.slideWidth),
                        (r = (i + n.options.slidesToShow - n.slideCount) * t)),
                n.slideCount <= n.options.slidesToShow &&
                ((n.slideOffset = 0), (r = 0)),
                n.options.centerMode === !0 && n.slideCount <= n.options.slidesToShow
                    ? (n.slideOffset =
                        (n.slideWidth * Math.floor(n.options.slidesToShow)) / 2 -
                        (n.slideWidth * n.slideCount) / 2)
                    : n.options.centerMode === !0 && n.options.infinite === !0
                        ? (n.slideOffset +=
                            n.slideWidth * Math.floor(n.options.slidesToShow / 2) -
                            n.slideWidth)
                        : n.options.centerMode === !0 &&
                        ((n.slideOffset = 0),
                            (n.slideOffset +=
                                n.slideWidth * Math.floor(n.options.slidesToShow / 2))),
                (e =
                    n.options.vertical === !1
                        ? i * n.slideWidth * -1 + n.slideOffset
                        : i * t * -1 + r),
                n.options.variableWidth === !0 &&
                ((o =
                    n.slideCount <= n.options.slidesToShow || n.options.infinite === !1
                        ? n.$slideTrack.children(".slick-slide").eq(i)
                        : n.$slideTrack
                            .children(".slick-slide")
                            .eq(i + n.options.slidesToShow)),
                    (e =
                        n.options.rtl === !0
                            ? o[0]
                                ? (n.$slideTrack.width() - o[0].offsetLeft - o.width()) * -1
                                : 0
                            : o[0]
                                ? o[0].offsetLeft * -1
                                : 0),
                    n.options.centerMode === !0 &&
                    ((o =
                        n.slideCount <= n.options.slidesToShow ||
                            n.options.infinite === !1
                            ? n.$slideTrack.children(".slick-slide").eq(i)
                            : n.$slideTrack
                                .children(".slick-slide")
                                .eq(i + n.options.slidesToShow + 1)),
                        (e =
                            n.options.rtl === !0
                                ? o[0]
                                    ? (n.$slideTrack.width() - o[0].offsetLeft - o.width()) * -1
                                    : 0
                                : o[0]
                                    ? o[0].offsetLeft * -1
                                    : 0),
                        (e += (n.$list.width() - o.outerWidth()) / 2))),
                e
            );
        }),
        (e.prototype.getOption = e.prototype.slickGetOption = function (i) {
            var e = this;
            return e.options[i];
        }),
        (e.prototype.getNavigableIndexes = function () {
            var i,
                e = this,
                t = 0,
                o = 0,
                s = [];
            for (
                e.options.infinite === !1
                    ? (i = e.slideCount)
                    : ((t = e.options.slidesToScroll * -1),
                        (o = e.options.slidesToScroll * -1),
                        (i = 2 * e.slideCount));
                t < i;

            )
                s.push(t),
                    (t = o + e.options.slidesToScroll),
                    (o +=
                        e.options.slidesToScroll <= e.options.slidesToShow
                            ? e.options.slidesToScroll
                            : e.options.slidesToShow);
            return s;
        }),
        (e.prototype.getSlick = function () {
            return this;
        }),
        (e.prototype.getSlideCount = function () {
            var e,
                t,
                o,
                s,
                n = this;
            return (
                (s = n.options.centerMode === !0 ? Math.floor(n.$list.width() / 2) : 0),
                (o = n.swipeLeft * -1 + s),
                n.options.swipeToSlide === !0
                    ? (n.$slideTrack.find(".slick-slide").each(function (e, s) {
                        var r, l, d;
                        if (
                            ((r = i(s).outerWidth()),
                                (l = s.offsetLeft),
                                n.options.centerMode !== !0 && (l += r / 2),
                                (d = l + r),
                                o < d)
                        )
                            return (t = s), !1;
                    }),
                        (e = Math.abs(i(t).attr("data-slick-index") - n.currentSlide) || 1))
                    : n.options.slidesToScroll
            );
        }),
        (e.prototype.goTo = e.prototype.slickGoTo = function (i, e) {
            var t = this;
            t.changeSlide({ data: { message: "index", index: parseInt(i) } }, e);
        }),
        (e.prototype.init = function (e) {
            var t = this;
            i(t.$slider).hasClass("slick-initialized") ||
                (i(t.$slider).addClass("slick-initialized"),
                    t.buildRows(),
                    t.buildOut(),
                    t.setProps(),
                    t.startLoad(),
                    t.loadSlider(),
                    t.initializeEvents(),
                    t.updateArrows(),
                    t.updateDots(),
                    t.checkResponsive(!0),
                    t.focusHandler()),
                e && t.$slider.trigger("init", [t]),
                t.options.accessibility === !0 && t.initADA(),
                t.options.autoplay && ((t.paused = !1), t.autoPlay());
        }),
        (e.prototype.initADA = function () {
            var e = this,
                t = Math.ceil(e.slideCount / e.options.slidesToShow),
                o = e.getNavigableIndexes().filter(function (i) {
                    return i >= 0 && i < e.slideCount;
                });
            e.$slides
                .add(e.$slideTrack.find(".slick-cloned"))
                .attr({ "aria-hidden": "true", tabindex: "-1" })
                .find("a, input, button, select")
                .attr({ tabindex: "-1" }),
                null !== e.$dots &&
                (e.$slides
                    .not(e.$slideTrack.find(".slick-cloned"))
                    .each(function (t) {
                        var s = o.indexOf(t);
                        if (
                            (i(this).attr({
                                role: "tabpanel",
                                id: "slick-slide" + e.instanceUid + t,
                                tabindex: -1,
                            }),
                                s !== -1)
                        ) {
                            var n = "slick-slide-control" + e.instanceUid + s;
                            i("#" + n).length && i(this).attr({ "aria-describedby": n });
                        }
                    }),
                    e.$dots
                        .attr("role", "tablist")
                        .find("li")
                        .each(function (s) {
                            var n = o[s];
                            i(this).attr({ role: "presentation" }),
                                i(this)
                                    .find("button")
                                    .first()
                                    .attr({
                                        role: "tab",
                                        id: "slick-slide-control" + e.instanceUid + s,
                                        "aria-controls": "slick-slide" + e.instanceUid + n,
                                        "aria-label": s + 1 + " of " + t,
                                        "aria-selected": null,
                                        tabindex: "-1",
                                    });
                        })
                        .eq(e.currentSlide)
                        .find("button")
                        .attr({ "aria-selected": "true", tabindex: "0" })
                        .end());
            for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++)
                e.options.focusOnChange
                    ? e.$slides.eq(s).attr({ tabindex: "0" })
                    : e.$slides.eq(s).removeAttr("tabindex");
            e.activateADA();
        }),
        (e.prototype.initArrowEvents = function () {
            var i = this;
            i.options.arrows === !0 &&
                i.slideCount > i.options.slidesToShow &&
                (i.$prevArrow
                    .off("click.slick")
                    .on("click.slick", { message: "previous" }, i.changeSlide),
                    i.$nextArrow
                        .off("click.slick")
                        .on("click.slick", { message: "next" }, i.changeSlide),
                    i.options.accessibility === !0 &&
                    (i.$prevArrow.on("keydown.slick", i.keyHandler),
                        i.$nextArrow.on("keydown.slick", i.keyHandler)));
        }),
        (e.prototype.initDotEvents = function () {
            var e = this;
            e.options.dots === !0 &&
                e.slideCount > e.options.slidesToShow &&
                (i("li", e.$dots).on(
                    "click.slick",
                    { message: "index" },
                    e.changeSlide
                ),
                    e.options.accessibility === !0 &&
                    e.$dots.on("keydown.slick", e.keyHandler)),
                e.options.dots === !0 &&
                e.options.pauseOnDotsHover === !0 &&
                e.slideCount > e.options.slidesToShow &&
                i("li", e.$dots)
                    .on("mouseenter.slick", i.proxy(e.interrupt, e, !0))
                    .on("mouseleave.slick", i.proxy(e.interrupt, e, !1));
        }),
        (e.prototype.initSlideEvents = function () {
            var e = this;
            e.options.pauseOnHover &&
                (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)),
                    e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)));
        }),
        (e.prototype.initializeEvents = function () {
            var e = this;
            e.initArrowEvents(),
                e.initDotEvents(),
                e.initSlideEvents(),
                e.$list.on(
                    "touchstart.slick mousedown.slick",
                    { action: "start" },
                    e.swipeHandler
                ),
                e.$list.on(
                    "touchmove.slick mousemove.slick",
                    { action: "move" },
                    e.swipeHandler
                ),
                e.$list.on(
                    "touchend.slick mouseup.slick",
                    { action: "end" },
                    e.swipeHandler
                ),
                e.$list.on(
                    "touchcancel.slick mouseleave.slick",
                    { action: "end" },
                    e.swipeHandler
                ),
                e.$list.on("click.slick", e.clickHandler),
                i(document).on(e.visibilityChange, i.proxy(e.visibility, e)),
                e.options.accessibility === !0 &&
                e.$list.on("keydown.slick", e.keyHandler),
                e.options.focusOnSelect === !0 &&
                i(e.$slideTrack).children().on("click.slick", e.selectHandler),
                i(window).on(
                    "orientationchange.slick.slick-" + e.instanceUid,
                    i.proxy(e.orientationChange, e)
                ),
                i(window).on(
                    "resize.slick.slick-" + e.instanceUid,
                    i.proxy(e.resize, e)
                ),
                i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault),
                i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition),
                i(e.setPosition);
        }),
        (e.prototype.initUI = function () {
            var i = this;
            i.options.arrows === !0 &&
                i.slideCount > i.options.slidesToShow &&
                (i.$prevArrow.show(), i.$nextArrow.show()),
                i.options.dots === !0 &&
                i.slideCount > i.options.slidesToShow &&
                i.$dots.show();
        }),
        (e.prototype.keyHandler = function (i) {
            var e = this;
            i.target.tagName.match("TEXTAREA|INPUT|SELECT") ||
                (37 === i.keyCode && e.options.accessibility === !0
                    ? e.changeSlide({
                        data: { message: e.options.rtl === !0 ? "next" : "previous" },
                    })
                    : 39 === i.keyCode &&
                    e.options.accessibility === !0 &&
                    e.changeSlide({
                        data: { message: e.options.rtl === !0 ? "previous" : "next" },
                    }));
        }),
        (e.prototype.lazyLoad = function () {
            function e(e) {
                i("img[data-lazy]", e).each(function () {
                    var e = i(this),
                        t = i(this).attr("data-lazy"),
                        o = i(this).attr("data-srcset"),
                        s = i(this).attr("data-sizes") || r.$slider.attr("data-sizes"),
                        n = document.createElement("img");
                    (n.onload = function () {
                        e.animate({ opacity: 0 }, 100, function () {
                            o && (e.attr("srcset", o), s && e.attr("sizes", s)),
                                e.attr("src", t).animate({ opacity: 1 }, 200, function () {
                                    e.removeAttr("data-lazy data-srcset data-sizes").removeClass(
                                        "slick-loading"
                                    );
                                }),
                                r.$slider.trigger("lazyLoaded", [r, e, t]);
                        });
                    }),
                        (n.onerror = function () {
                            e
                                .removeAttr("data-lazy")
                                .removeClass("slick-loading")
                                .addClass("slick-lazyload-error"),
                                r.$slider.trigger("lazyLoadError", [r, e, t]);
                        }),
                        (n.src = t);
                });
            }
            var t,
                o,
                s,
                n,
                r = this;
            if (
                (r.options.centerMode === !0
                    ? r.options.infinite === !0
                        ? ((s = r.currentSlide + (r.options.slidesToShow / 2 + 1)),
                            (n = s + r.options.slidesToShow + 2))
                        : ((s = Math.max(
                            0,
                            r.currentSlide - (r.options.slidesToShow / 2 + 1)
                        )),
                            (n = 2 + (r.options.slidesToShow / 2 + 1) + r.currentSlide))
                    : ((s = r.options.infinite
                        ? r.options.slidesToShow + r.currentSlide
                        : r.currentSlide),
                        (n = Math.ceil(s + r.options.slidesToShow)),
                        r.options.fade === !0 && (s > 0 && s--, n <= r.slideCount && n++)),
                    (t = r.$slider.find(".slick-slide").slice(s, n)),
                    "anticipated" === r.options.lazyLoad)
            )
                for (
                    var l = s - 1, d = n, a = r.$slider.find(".slick-slide"), c = 0;
                    c < r.options.slidesToScroll;
                    c++
                )
                    l < 0 && (l = r.slideCount - 1),
                        (t = t.add(a.eq(l))),
                        (t = t.add(a.eq(d))),
                        l--,
                        d++;
            e(t),
                r.slideCount <= r.options.slidesToShow
                    ? ((o = r.$slider.find(".slick-slide")), e(o))
                    : r.currentSlide >= r.slideCount - r.options.slidesToShow
                        ? ((o = r.$slider
                            .find(".slick-cloned")
                            .slice(0, r.options.slidesToShow)),
                            e(o))
                        : 0 === r.currentSlide &&
                        ((o = r.$slider
                            .find(".slick-cloned")
                            .slice(r.options.slidesToShow * -1)),
                            e(o));
        }),
        (e.prototype.loadSlider = function () {
            var i = this;
            i.setPosition(),
                i.$slideTrack.css({ opacity: 1 }),
                i.$slider.removeClass("slick-loading"),
                i.initUI(),
                "progressive" === i.options.lazyLoad && i.progressiveLazyLoad();
        }),
        (e.prototype.next = e.prototype.slickNext = function () {
            var i = this;
            i.changeSlide({ data: { message: "next" } });
        }),
        (e.prototype.orientationChange = function () {
            var i = this;
            i.checkResponsive(), i.setPosition();
        }),
        (e.prototype.pause = e.prototype.slickPause = function () {
            var i = this;
            i.autoPlayClear(), (i.paused = !0);
        }),
        (e.prototype.play = e.prototype.slickPlay = function () {
            var i = this;
            i.autoPlay(),
                (i.options.autoplay = !0),
                (i.paused = !1),
                (i.focussed = !1),
                (i.interrupted = !1);
        }),
        (e.prototype.postSlide = function (e) {
            var t = this;
            if (
                !t.unslicked &&
                (t.$slider.trigger("afterChange", [t, e]),
                    (t.animating = !1),
                    t.slideCount > t.options.slidesToShow && t.setPosition(),
                    (t.swipeLeft = null),
                    t.options.autoplay && t.autoPlay(),
                    t.options.accessibility === !0 &&
                    (t.initADA(), t.options.focusOnChange))
            ) {
                var o = i(t.$slides.get(t.currentSlide));
                o.attr("tabindex", 0).focus();
            }
        }),
        (e.prototype.prev = e.prototype.slickPrev = function () {
            var i = this;
            i.changeSlide({ data: { message: "previous" } });
        }),
        (e.prototype.preventDefault = function (i) {
            i.preventDefault();
        }),
        (e.prototype.progressiveLazyLoad = function (e) {
            e = e || 1;
            var t,
                o,
                s,
                n,
                r,
                l = this,
                d = i("img[data-lazy]", l.$slider);
            d.length
                ? ((t = d.first()),
                    (o = t.attr("data-lazy")),
                    (s = t.attr("data-srcset")),
                    (n = t.attr("data-sizes") || l.$slider.attr("data-sizes")),
                    (r = document.createElement("img")),
                    (r.onload = function () {
                        s && (t.attr("srcset", s), n && t.attr("sizes", n)),
                            t
                                .attr("src", o)
                                .removeAttr("data-lazy data-srcset data-sizes")
                                .removeClass("slick-loading"),
                            l.options.adaptiveHeight === !0 && l.setPosition(),
                            l.$slider.trigger("lazyLoaded", [l, t, o]),
                            l.progressiveLazyLoad();
                    }),
                    (r.onerror = function () {
                        e < 3
                            ? setTimeout(function () {
                                l.progressiveLazyLoad(e + 1);
                            }, 500)
                            : (t
                                .removeAttr("data-lazy")
                                .removeClass("slick-loading")
                                .addClass("slick-lazyload-error"),
                                l.$slider.trigger("lazyLoadError", [l, t, o]),
                                l.progressiveLazyLoad());
                    }),
                    (r.src = o))
                : l.$slider.trigger("allImagesLoaded", [l]);
        }),
        (e.prototype.refresh = function (e) {
            var t,
                o,
                s = this;
            (o = s.slideCount - s.options.slidesToShow),
                !s.options.infinite && s.currentSlide > o && (s.currentSlide = o),
                s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0),
                (t = s.currentSlide),
                s.destroy(!0),
                i.extend(s, s.initials, { currentSlide: t }),
                s.init(),
                e || s.changeSlide({ data: { message: "index", index: t } }, !1);
        }),
        (e.prototype.registerBreakpoints = function () {
            var e,
                t,
                o,
                s = this,
                n = s.options.responsive || null;
            if ("array" === i.type(n) && n.length) {
                s.respondTo = s.options.respondTo || "window";
                for (e in n)
                    if (((o = s.breakpoints.length - 1), n.hasOwnProperty(e))) {
                        for (t = n[e].breakpoint; o >= 0;)
                            s.breakpoints[o] &&
                                s.breakpoints[o] === t &&
                                s.breakpoints.splice(o, 1),
                                o--;
                        s.breakpoints.push(t), (s.breakpointSettings[t] = n[e].settings);
                    }
                s.breakpoints.sort(function (i, e) {
                    return s.options.mobileFirst ? i - e : e - i;
                });
            }
        }),
        (e.prototype.reinit = function () {
            var e = this;
            (e.$slides = e.$slideTrack
                .children(e.options.slide)
                .addClass("slick-slide")),
                (e.slideCount = e.$slides.length),
                e.currentSlide >= e.slideCount &&
                0 !== e.currentSlide &&
                (e.currentSlide = e.currentSlide - e.options.slidesToScroll),
                e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0),
                e.registerBreakpoints(),
                e.setProps(),
                e.setupInfinite(),
                e.buildArrows(),
                e.updateArrows(),
                e.initArrowEvents(),
                e.buildDots(),
                e.updateDots(),
                e.initDotEvents(),
                e.cleanUpSlideEvents(),
                e.initSlideEvents(),
                e.checkResponsive(!1, !0),
                e.options.focusOnSelect === !0 &&
                i(e.$slideTrack).children().on("click.slick", e.selectHandler),
                e.setSlideClasses(
                    "number" == typeof e.currentSlide ? e.currentSlide : 0
                ),
                e.setPosition(),
                e.focusHandler(),
                (e.paused = !e.options.autoplay),
                e.autoPlay(),
                e.$slider.trigger("reInit", [e]);
        }),
        (e.prototype.resize = function () {
            var e = this;
            i(window).width() !== e.windowWidth &&
                (clearTimeout(e.windowDelay),
                    (e.windowDelay = window.setTimeout(function () {
                        (e.windowWidth = i(window).width()),
                            e.checkResponsive(),
                            e.unslicked || e.setPosition();
                    }, 50)));
        }),
        (e.prototype.removeSlide = e.prototype.slickRemove = function (i, e, t) {
            var o = this;
            return (
                "boolean" == typeof i
                    ? ((e = i), (i = e === !0 ? 0 : o.slideCount - 1))
                    : (i = e === !0 ? --i : i),
                !(o.slideCount < 1 || i < 0 || i > o.slideCount - 1) &&
                (o.unload(),
                    t === !0
                        ? o.$slideTrack.children().remove()
                        : o.$slideTrack.children(this.options.slide).eq(i).remove(),
                    (o.$slides = o.$slideTrack.children(this.options.slide)),
                    o.$slideTrack.children(this.options.slide).detach(),
                    o.$slideTrack.append(o.$slides),
                    (o.$slidesCache = o.$slides),
                    void o.reinit())
            );
        }),
        (e.prototype.setCSS = function (i) {
            var e,
                t,
                o = this,
                s = {};
            o.options.rtl === !0 && (i = -i),
                (e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px"),
                (t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px"),
                (s[o.positionProp] = i),
                o.transformsEnabled === !1
                    ? o.$slideTrack.css(s)
                    : ((s = {}),
                        o.cssTransitions === !1
                            ? ((s[o.animType] = "translate(" + e + ", " + t + ")"),
                                o.$slideTrack.css(s))
                            : ((s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)"),
                                o.$slideTrack.css(s)));
        }),
        (e.prototype.setDimensions = function () {
            var i = this;
            i.options.vertical === !1
                ? i.options.centerMode === !0 &&
                i.$list.css({ padding: "0px " + i.options.centerPadding })
                : (i.$list.height(
                    i.$slides.first().outerHeight(!0) * i.options.slidesToShow
                ),
                    i.options.centerMode === !0 &&
                    i.$list.css({ padding: i.options.centerPadding + " 0px" })),
                (i.listWidth = i.$list.width()),
                (i.listHeight = i.$list.height()),
                i.options.vertical === !1 && i.options.variableWidth === !1
                    ? ((i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow)),
                        i.$slideTrack.width(
                            Math.ceil(
                                i.slideWidth * i.$slideTrack.children(".slick-slide").length
                            )
                        ))
                    : i.options.variableWidth === !0
                        ? i.$slideTrack.width(5e3 * i.slideCount)
                        : ((i.slideWidth = Math.ceil(i.listWidth)),
                            i.$slideTrack.height(
                                Math.ceil(
                                    i.$slides.first().outerHeight(!0) *
                                    i.$slideTrack.children(".slick-slide").length
                                )
                            ));
            var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();
            i.options.variableWidth === !1 &&
                i.$slideTrack.children(".slick-slide").width(i.slideWidth - e);
        }),
        (e.prototype.setFade = function () {
            var e,
                t = this;
            t.$slides.each(function (o, s) {
                (e = t.slideWidth * o * -1),
                    t.options.rtl === !0
                        ? i(s).css({
                            position: "relative",
                            right: e,
                            top: 0,
                            zIndex: t.options.zIndex - 2,
                            opacity: 0,
                        })
                        : i(s).css({
                            position: "relative",
                            left: e,
                            top: 0,
                            zIndex: t.options.zIndex - 2,
                            opacity: 0,
                        });
            }),
                t.$slides
                    .eq(t.currentSlide)
                    .css({ zIndex: t.options.zIndex - 1, opacity: 1 });
        }),
        (e.prototype.setHeight = function () {
            var i = this;
            if (
                1 === i.options.slidesToShow &&
                i.options.adaptiveHeight === !0 &&
                i.options.vertical === !1
            ) {
                var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
                i.$list.css("height", e);
            }
        }),
        (e.prototype.setOption = e.prototype.slickSetOption = function () {
            var e,
                t,
                o,
                s,
                n,
                r = this,
                l = !1;
            if (
                ("object" === i.type(arguments[0])
                    ? ((o = arguments[0]), (l = arguments[1]), (n = "multiple"))
                    : "string" === i.type(arguments[0]) &&
                    ((o = arguments[0]),
                        (s = arguments[1]),
                        (l = arguments[2]),
                        "responsive" === arguments[0] && "array" === i.type(arguments[1])
                            ? (n = "responsive")
                            : "undefined" != typeof arguments[1] && (n = "single")),
                    "single" === n)
            )
                r.options[o] = s;
            else if ("multiple" === n)
                i.each(o, function (i, e) {
                    r.options[i] = e;
                });
            else if ("responsive" === n)
                for (t in s)
                    if ("array" !== i.type(r.options.responsive))
                        r.options.responsive = [s[t]];
                    else {
                        for (e = r.options.responsive.length - 1; e >= 0;)
                            r.options.responsive[e].breakpoint === s[t].breakpoint &&
                                r.options.responsive.splice(e, 1),
                                e--;
                        r.options.responsive.push(s[t]);
                    }
            l && (r.unload(), r.reinit());
        }),
        (e.prototype.setPosition = function () {
            var i = this;
            i.setDimensions(),
                i.setHeight(),
                i.options.fade === !1
                    ? i.setCSS(i.getLeft(i.currentSlide))
                    : i.setFade(),
                i.$slider.trigger("setPosition", [i]);
        }),
        (e.prototype.setProps = function () {
            var i = this,
                e = document.body.style;
            (i.positionProp = i.options.vertical === !0 ? "top" : "left"),
                "top" === i.positionProp
                    ? i.$slider.addClass("slick-vertical")
                    : i.$slider.removeClass("slick-vertical"),
                (void 0 === e.WebkitTransition &&
                    void 0 === e.MozTransition &&
                    void 0 === e.msTransition) ||
                (i.options.useCSS === !0 && (i.cssTransitions = !0)),
                i.options.fade &&
                ("number" == typeof i.options.zIndex
                    ? i.options.zIndex < 3 && (i.options.zIndex = 3)
                    : (i.options.zIndex = i.defaults.zIndex)),
                void 0 !== e.OTransform &&
                ((i.animType = "OTransform"),
                    (i.transformType = "-o-transform"),
                    (i.transitionType = "OTransition"),
                    void 0 === e.perspectiveProperty &&
                    void 0 === e.webkitPerspective &&
                    (i.animType = !1)),
                void 0 !== e.MozTransform &&
                ((i.animType = "MozTransform"),
                    (i.transformType = "-moz-transform"),
                    (i.transitionType = "MozTransition"),
                    void 0 === e.perspectiveProperty &&
                    void 0 === e.MozPerspective &&
                    (i.animType = !1)),
                void 0 !== e.webkitTransform &&
                ((i.animType = "webkitTransform"),
                    (i.transformType = "-webkit-transform"),
                    (i.transitionType = "webkitTransition"),
                    void 0 === e.perspectiveProperty &&
                    void 0 === e.webkitPerspective &&
                    (i.animType = !1)),
                void 0 !== e.msTransform &&
                ((i.animType = "msTransform"),
                    (i.transformType = "-ms-transform"),
                    (i.transitionType = "msTransition"),
                    void 0 === e.msTransform && (i.animType = !1)),
                void 0 !== e.transform &&
                i.animType !== !1 &&
                ((i.animType = "transform"),
                    (i.transformType = "transform"),
                    (i.transitionType = "transition")),
                (i.transformsEnabled =
                    i.options.useTransform && null !== i.animType && i.animType !== !1);
        }),
        (e.prototype.setSlideClasses = function (i) {
            var e,
                t,
                o,
                s,
                n = this;
            if (
                ((t = n.$slider
                    .find(".slick-slide")
                    .removeClass("slick-active slick-center slick-current")
                    .attr("aria-hidden", "true")),
                    n.$slides.eq(i).addClass("slick-current"),
                    n.options.centerMode === !0)
            ) {
                var r = n.options.slidesToShow % 2 === 0 ? 1 : 0;
                (e = Math.floor(n.options.slidesToShow / 2)),
                    n.options.infinite === !0 &&
                    (i >= e && i <= n.slideCount - 1 - e
                        ? n.$slides
                            .slice(i - e + r, i + e + 1)
                            .addClass("slick-active")
                            .attr("aria-hidden", "false")
                        : ((o = n.options.slidesToShow + i),
                            t
                                .slice(o - e + 1 + r, o + e + 2)
                                .addClass("slick-active")
                                .attr("aria-hidden", "false")),
                        0 === i
                            ? t
                                .eq(t.length - 1 - n.options.slidesToShow)
                                .addClass("slick-center")
                            : i === n.slideCount - 1 &&
                            t.eq(n.options.slidesToShow).addClass("slick-center")),
                    n.$slides.eq(i).addClass("slick-center");
            } else
                i >= 0 && i <= n.slideCount - n.options.slidesToShow
                    ? n.$slides
                        .slice(i, i + n.options.slidesToShow)
                        .addClass("slick-active")
                        .attr("aria-hidden", "false")
                    : t.length <= n.options.slidesToShow
                        ? t.addClass("slick-active").attr("aria-hidden", "false")
                        : ((s = n.slideCount % n.options.slidesToShow),
                            (o = n.options.infinite === !0 ? n.options.slidesToShow + i : i),
                            n.options.slidesToShow == n.options.slidesToScroll &&
                                n.slideCount - i < n.options.slidesToShow
                                ? t
                                    .slice(o - (n.options.slidesToShow - s), o + s)
                                    .addClass("slick-active")
                                    .attr("aria-hidden", "false")
                                : t
                                    .slice(o, o + n.options.slidesToShow)
                                    .addClass("slick-active")
                                    .attr("aria-hidden", "false"));
            ("ondemand" !== n.options.lazyLoad &&
                "anticipated" !== n.options.lazyLoad) ||
                n.lazyLoad();
        }),
        (e.prototype.setupInfinite = function () {
            var e,
                t,
                o,
                s = this;
            if (
                (s.options.fade === !0 && (s.options.centerMode = !1),
                    s.options.infinite === !0 &&
                    s.options.fade === !1 &&
                    ((t = null), s.slideCount > s.options.slidesToShow))
            ) {
                for (
                    o =
                    s.options.centerMode === !0
                        ? s.options.slidesToShow + 1
                        : s.options.slidesToShow,
                    e = s.slideCount;
                    e > s.slideCount - o;
                    e -= 1
                )
                    (t = e - 1),
                        i(s.$slides[t])
                            .clone(!0)
                            .attr("id", "")
                            .attr("data-slick-index", t - s.slideCount)
                            .prependTo(s.$slideTrack)
                            .addClass("slick-cloned");
                for (e = 0; e < o + s.slideCount; e += 1)
                    (t = e),
                        i(s.$slides[t])
                            .clone(!0)
                            .attr("id", "")
                            .attr("data-slick-index", t + s.slideCount)
                            .appendTo(s.$slideTrack)
                            .addClass("slick-cloned");
                s.$slideTrack
                    .find(".slick-cloned")
                    .find("[id]")
                    .each(function () {
                        i(this).attr("id", "");
                    });
            }
        }),
        (e.prototype.interrupt = function (i) {
            var e = this;
            i || e.autoPlay(), (e.interrupted = i);
        }),
        (e.prototype.selectHandler = function (e) {
            var t = this,
                o = i(e.target).is(".slick-slide")
                    ? i(e.target)
                    : i(e.target).parents(".slick-slide"),
                s = parseInt(o.attr("data-slick-index"));
            return (
                s || (s = 0),
                t.slideCount <= t.options.slidesToShow
                    ? void t.slideHandler(s, !1, !0)
                    : void t.slideHandler(s)
            );
        }),
        (e.prototype.slideHandler = function (i, e, t) {
            var o,
                s,
                n,
                r,
                l,
                d = null,
                a = this;
            if (
                ((e = e || !1),
                    !(
                        (a.animating === !0 && a.options.waitForAnimate === !0) ||
                        (a.options.fade === !0 && a.currentSlide === i)
                    ))
            )
                return (
                    e === !1 && a.asNavFor(i),
                    (o = i),
                    (d = a.getLeft(o)),
                    (r = a.getLeft(a.currentSlide)),
                    (a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft),
                    a.options.infinite === !1 &&
                        a.options.centerMode === !1 &&
                        (i < 0 || i > a.getDotCount() * a.options.slidesToScroll)
                        ? void (
                            a.options.fade === !1 &&
                            ((o = a.currentSlide),
                                t !== !0 && a.slideCount > a.options.slidesToShow
                                    ? a.animateSlide(r, function () {
                                        a.postSlide(o);
                                    })
                                    : a.postSlide(o))
                        )
                        : a.options.infinite === !1 &&
                            a.options.centerMode === !0 &&
                            (i < 0 || i > a.slideCount - a.options.slidesToScroll)
                            ? void (
                                a.options.fade === !1 &&
                                ((o = a.currentSlide),
                                    t !== !0 && a.slideCount > a.options.slidesToShow
                                        ? a.animateSlide(r, function () {
                                            a.postSlide(o);
                                        })
                                        : a.postSlide(o))
                            )
                            : (a.options.autoplay && clearInterval(a.autoPlayTimer),
                                (s =
                                    o < 0
                                        ? a.slideCount % a.options.slidesToScroll !== 0
                                            ? a.slideCount - (a.slideCount % a.options.slidesToScroll)
                                            : a.slideCount + o
                                        : o >= a.slideCount
                                            ? a.slideCount % a.options.slidesToScroll !== 0
                                                ? 0
                                                : o - a.slideCount
                                            : o),
                                (a.animating = !0),
                                a.$slider.trigger("beforeChange", [a, a.currentSlide, s]),
                                (n = a.currentSlide),
                                (a.currentSlide = s),
                                a.setSlideClasses(a.currentSlide),
                                a.options.asNavFor &&
                                ((l = a.getNavTarget()),
                                    (l = l.slick("getSlick")),
                                    l.slideCount <= l.options.slidesToShow &&
                                    l.setSlideClasses(a.currentSlide)),
                                a.updateDots(),
                                a.updateArrows(),
                                a.options.fade === !0
                                    ? (t !== !0
                                        ? (a.fadeSlideOut(n),
                                            a.fadeSlide(s, function () {
                                                a.postSlide(s);
                                            }))
                                        : a.postSlide(s),
                                        void a.animateHeight())
                                    : void (t !== !0 && a.slideCount > a.options.slidesToShow
                                        ? a.animateSlide(d, function () {
                                            a.postSlide(s);
                                        })
                                        : a.postSlide(s)))
                );
        }),
        (e.prototype.startLoad = function () {
            var i = this;
            i.options.arrows === !0 &&
                i.slideCount > i.options.slidesToShow &&
                (i.$prevArrow.hide(), i.$nextArrow.hide()),
                i.options.dots === !0 &&
                i.slideCount > i.options.slidesToShow &&
                i.$dots.hide(),
                i.$slider.addClass("slick-loading");
        }),
        (e.prototype.swipeDirection = function () {
            var i,
                e,
                t,
                o,
                s = this;
            return (
                (i = s.touchObject.startX - s.touchObject.curX),
                (e = s.touchObject.startY - s.touchObject.curY),
                (t = Math.atan2(e, i)),
                (o = Math.round((180 * t) / Math.PI)),
                o < 0 && (o = 360 - Math.abs(o)),
                o <= 45 && o >= 0
                    ? s.options.rtl === !1
                        ? "left"
                        : "right"
                    : o <= 360 && o >= 315
                        ? s.options.rtl === !1
                            ? "left"
                            : "right"
                        : o >= 135 && o <= 225
                            ? s.options.rtl === !1
                                ? "right"
                                : "left"
                            : s.options.verticalSwiping === !0
                                ? o >= 35 && o <= 135
                                    ? "down"
                                    : "up"
                                : "vertical"
            );
        }),
        (e.prototype.swipeEnd = function (i) {
            var e,
                t,
                o = this;
            if (((o.dragging = !1), (o.swiping = !1), o.scrolling))
                return (o.scrolling = !1), !1;
            if (
                ((o.interrupted = !1),
                    (o.shouldClick = !(o.touchObject.swipeLength > 10)),
                    void 0 === o.touchObject.curX)
            )
                return !1;
            if (
                (o.touchObject.edgeHit === !0 &&
                    o.$slider.trigger("edge", [o, o.swipeDirection()]),
                    o.touchObject.swipeLength >= o.touchObject.minSwipe)
            ) {
                switch ((t = o.swipeDirection())) {
                    case "left":
                    case "down":
                        (e = o.options.swipeToSlide
                            ? o.checkNavigable(o.currentSlide + o.getSlideCount())
                            : o.currentSlide + o.getSlideCount()),
                            (o.currentDirection = 0);
                        break;
                    case "right":
                    case "up":
                        (e = o.options.swipeToSlide
                            ? o.checkNavigable(o.currentSlide - o.getSlideCount())
                            : o.currentSlide - o.getSlideCount()),
                            (o.currentDirection = 1);
                }
                "vertical" != t &&
                    (o.slideHandler(e),
                        (o.touchObject = {}),
                        o.$slider.trigger("swipe", [o, t]));
            } else
                o.touchObject.startX !== o.touchObject.curX &&
                    (o.slideHandler(o.currentSlide), (o.touchObject = {}));
        }),
        (e.prototype.swipeHandler = function (i) {
            var e = this;
            if (
                !(
                    e.options.swipe === !1 ||
                    ("ontouchend" in document && e.options.swipe === !1) ||
                    (e.options.draggable === !1 && i.type.indexOf("mouse") !== -1)
                )
            )
                switch (
                ((e.touchObject.fingerCount =
                    i.originalEvent && void 0 !== i.originalEvent.touches
                        ? i.originalEvent.touches.length
                        : 1),
                    (e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold),
                    e.options.verticalSwiping === !0 &&
                    (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold),
                    i.data.action)
                ) {
                    case "start":
                        e.swipeStart(i);
                        break;
                    case "move":
                        e.swipeMove(i);
                        break;
                    case "end":
                        e.swipeEnd(i);
                }
        }),
        (e.prototype.swipeMove = function (i) {
            var e,
                t,
                o,
                s,
                n,
                r,
                l = this;
            return (
                (n = void 0 !== i.originalEvent ? i.originalEvent.touches : null),
                !(!l.dragging || l.scrolling || (n && 1 !== n.length)) &&
                ((e = l.getLeft(l.currentSlide)),
                    (l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX),
                    (l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY),
                    (l.touchObject.swipeLength = Math.round(
                        Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))
                    )),
                    (r = Math.round(
                        Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))
                    )),
                    !l.options.verticalSwiping && !l.swiping && r > 4
                        ? ((l.scrolling = !0), !1)
                        : (l.options.verticalSwiping === !0 &&
                            (l.touchObject.swipeLength = r),
                            (t = l.swipeDirection()),
                            void 0 !== i.originalEvent &&
                            l.touchObject.swipeLength > 4 &&
                            ((l.swiping = !0), i.preventDefault()),
                            (s =
                                (l.options.rtl === !1 ? 1 : -1) *
                                (l.touchObject.curX > l.touchObject.startX ? 1 : -1)),
                            l.options.verticalSwiping === !0 &&
                            (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1),
                            (o = l.touchObject.swipeLength),
                            (l.touchObject.edgeHit = !1),
                            l.options.infinite === !1 &&
                            ((0 === l.currentSlide && "right" === t) ||
                                (l.currentSlide >= l.getDotCount() && "left" === t)) &&
                            ((o = l.touchObject.swipeLength * l.options.edgeFriction),
                                (l.touchObject.edgeHit = !0)),
                            l.options.vertical === !1
                                ? (l.swipeLeft = e + o * s)
                                : (l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s),
                            l.options.verticalSwiping === !0 && (l.swipeLeft = e + o * s),
                            l.options.fade !== !0 &&
                            l.options.touchMove !== !1 &&
                            (l.animating === !0
                                ? ((l.swipeLeft = null), !1)
                                : void l.setCSS(l.swipeLeft))))
            );
        }),
        (e.prototype.swipeStart = function (i) {
            var e,
                t = this;
            return (
                (t.interrupted = !0),
                1 !== t.touchObject.fingerCount ||
                    t.slideCount <= t.options.slidesToShow
                    ? ((t.touchObject = {}), !1)
                    : (void 0 !== i.originalEvent &&
                        void 0 !== i.originalEvent.touches &&
                        (e = i.originalEvent.touches[0]),
                        (t.touchObject.startX = t.touchObject.curX =
                            void 0 !== e ? e.pageX : i.clientX),
                        (t.touchObject.startY = t.touchObject.curY =
                            void 0 !== e ? e.pageY : i.clientY),
                        void (t.dragging = !0))
            );
        }),
        (e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
            var i = this;
            null !== i.$slidesCache &&
                (i.unload(),
                    i.$slideTrack.children(this.options.slide).detach(),
                    i.$slidesCache.appendTo(i.$slideTrack),
                    i.reinit());
        }),
        (e.prototype.unload = function () {
            var e = this;
            i(".slick-cloned", e.$slider).remove(),
                e.$dots && e.$dots.remove(),
                e.$prevArrow &&
                e.htmlExpr.test(e.options.prevArrow) &&
                e.$prevArrow.remove(),
                e.$nextArrow &&
                e.htmlExpr.test(e.options.nextArrow) &&
                e.$nextArrow.remove(),
                e.$slides
                    .removeClass("slick-slide slick-active slick-visible slick-current")
                    .attr("aria-hidden", "true")
                    .css("width", "");
        }),
        (e.prototype.unslick = function (i) {
            var e = this;
            e.$slider.trigger("unslick", [e, i]), e.destroy();
        }),
        (e.prototype.updateArrows = function () {
            var i,
                e = this;
            (i = Math.floor(e.options.slidesToShow / 2)),
                e.options.arrows === !0 &&
                e.slideCount > e.options.slidesToShow &&
                !e.options.infinite &&
                (e.$prevArrow
                    .removeClass("slick-disabled")
                    .attr("aria-disabled", "false"),
                    e.$nextArrow
                        .removeClass("slick-disabled")
                        .attr("aria-disabled", "false"),
                    0 === e.currentSlide
                        ? (e.$prevArrow
                            .addClass("slick-disabled")
                            .attr("aria-disabled", "true"),
                            e.$nextArrow
                                .removeClass("slick-disabled")
                                .attr("aria-disabled", "false"))
                        : e.currentSlide >= e.slideCount - e.options.slidesToShow &&
                            e.options.centerMode === !1
                            ? (e.$nextArrow
                                .addClass("slick-disabled")
                                .attr("aria-disabled", "true"),
                                e.$prevArrow
                                    .removeClass("slick-disabled")
                                    .attr("aria-disabled", "false"))
                            : e.currentSlide >= e.slideCount - 1 &&
                            e.options.centerMode === !0 &&
                            (e.$nextArrow
                                .addClass("slick-disabled")
                                .attr("aria-disabled", "true"),
                                e.$prevArrow
                                    .removeClass("slick-disabled")
                                    .attr("aria-disabled", "false")));
        }),
        (e.prototype.updateDots = function () {
            var i = this;
            null !== i.$dots &&
                (i.$dots.find("li").removeClass("slick-active").end(),
                    i.$dots
                        .find("li")
                        .eq(Math.floor(i.currentSlide / i.options.slidesToScroll))
                        .addClass("slick-active"));
        }),
        (e.prototype.visibility = function () {
            var i = this;
            i.options.autoplay &&
                (document[i.hidden] ? (i.interrupted = !0) : (i.interrupted = !1));
        }),
        (i.fn.slick = function () {
            var i,
                t,
                o = this,
                s = arguments[0],
                n = Array.prototype.slice.call(arguments, 1),
                r = o.length;
            for (i = 0; i < r; i++)
                if (
                    ("object" == typeof s || "undefined" == typeof s
                        ? (o[i].slick = new e(o[i], s))
                        : (t = o[i].slick[s].apply(o[i].slick, n)),
                        "undefined" != typeof t)
                )
                    return t;
            return o;
        });
});

/*! Picturefill - v2.3.1 - 2015-04-09
 * http://scottjehl.github.io/picturefill
 * Copyright (c) 2015 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT */
document.createElement("picture");
window.matchMedia ||
    (window.matchMedia = (function () {
        "use strict";
        var a = window.styleMedia || window.media;
        if (!a) {
            var b = document.createElement("style"),
                c = document.getElementsByTagName("script")[0],
                d = null;
            (b.type = "text/css"),
                (b.id = "matchmediajs-test"),
                c.parentNode.insertBefore(b, c),
                (d =
                    ("getComputedStyle" in window && window.getComputedStyle(b, null)) ||
                    b.currentStyle),
                (a = {
                    matchMedium: function (a) {
                        var c = "@media " + a + "{ #matchmediajs-test { width: 1px; } }";
                        return (
                            b.styleSheet ? (b.styleSheet.cssText = c) : (b.textContent = c),
                            "1px" === d.width
                        );
                    },
                });
        }
        return function (b) {
            return { matches: a.matchMedium(b || "all"), media: b || "all" };
        };
    })()),
    (function (a, b, c) {
        "use strict";
        function d(b) {
            "object" == typeof module && "object" == typeof module.exports
                ? (module.exports = b)
                : "function" == typeof define &&
                define.amd &&
                define("picturefill", function () {
                    return b;
                }),
                "object" == typeof a && (a.picturefill = b);
        }
        function e(a) {
            var b,
                c,
                d,
                e,
                f,
                i = a || {};
            b = i.elements || g.getAllElements();
            for (var j = 0, k = b.length; k > j; j++)
                if (
                    ((c = b[j]),
                        (d = c.parentNode),
                        (e = void 0),
                        (f = void 0),
                        "IMG" === c.nodeName.toUpperCase() &&
                        (c[g.ns] || (c[g.ns] = {}), i.reevaluate || !c[g.ns].evaluated))
                ) {
                    if (d && "PICTURE" === d.nodeName.toUpperCase()) {
                        if ((g.removeVideoShim(d), (e = g.getMatch(c, d)), e === !1))
                            continue;
                    } else e = void 0;
                    ((d && "PICTURE" === d.nodeName.toUpperCase()) ||
                        (!g.sizesSupported && c.srcset && h.test(c.srcset))) &&
                        g.dodgeSrcset(c),
                        e
                            ? ((f = g.processSourceSet(e)), g.applyBestCandidate(f, c))
                            : ((f = g.processSourceSet(c)),
                                (void 0 === c.srcset || c[g.ns].srcset) &&
                                g.applyBestCandidate(f, c)),
                        (c[g.ns].evaluated = !0);
                }
        }
        function f() {
            function c() {
                clearTimeout(d), (d = setTimeout(h, 60));
            }
            g.initTypeDetects(), e();
            var d,
                f = setInterval(function () {
                    return (
                        e(),
                        /^loaded|^i|^c/.test(b.readyState) ? void clearInterval(f) : void 0
                    );
                }, 250),
                h = function () {
                    e({ reevaluate: !0 });
                };
            a.addEventListener
                ? a.addEventListener("resize", c, !1)
                : a.attachEvent && a.attachEvent("onresize", c);
        }
        if (a.HTMLPictureElement) return void d(function () { });
        b.createElement("picture");
        var g = a.picturefill || {},
            h = /\s+\+?\d+(e\d+)?w/;
        (g.ns = "picturefill"),
            (function () {
                (g.srcsetSupported = "srcset" in c),
                    (g.sizesSupported = "sizes" in c),
                    (g.curSrcSupported = "currentSrc" in c);
            })(),
            (g.trim = function (a) {
                return a.trim ? a.trim() : a.replace(/^\s+|\s+$/g, "");
            }),
            (g.makeUrl = (function () {
                var a = b.createElement("a");
                return function (b) {
                    return (a.href = b), a.href;
                };
            })()),
            (g.restrictsMixedContent = function () {
                return "https:" === a.location.protocol;
            }),
            (g.matchesMedia = function (b) {
                return a.matchMedia && a.matchMedia(b).matches;
            }),
            (g.getDpr = function () {
                return a.devicePixelRatio || 1;
            }),
            (g.getWidthFromLength = function (a) {
                var c;
                if (
                    !a ||
                    a.indexOf("%") > -1 != !1 ||
                    !(parseFloat(a) > 0 || a.indexOf("calc(") > -1)
                )
                    return !1;
                (a = a.replace("vw", "%")),
                    g.lengthEl ||
                    ((g.lengthEl = b.createElement("div")),
                        (g.lengthEl.style.cssText =
                            "border:0;display:block;font-size:1em;left:0;margin:0;padding:0;position:absolute;visibility:hidden"),
                        (g.lengthEl.className = "helper-from-picturefill-js")),
                    (g.lengthEl.style.width = "0px");
                try {
                    g.lengthEl.style.width = a;
                } catch (d) { }
                return (
                    b.body.appendChild(g.lengthEl),
                    (c = g.lengthEl.offsetWidth),
                    0 >= c && (c = !1),
                    b.body.removeChild(g.lengthEl),
                    c
                );
            }),
            (g.detectTypeSupport = function (b, c) {
                var d = new a.Image();
                return (
                    (d.onerror = function () {
                        (g.types[b] = !1), e();
                    }),
                    (d.onload = function () {
                        (g.types[b] = 1 === d.width), e();
                    }),
                    (d.src = c),
                    "pending"
                );
            }),
            (g.types = g.types || {}),
            (g.initTypeDetects = function () {
                (g.types["image/jpeg"] = !0),
                    (g.types["image/gif"] = !0),
                    (g.types["image/png"] = !0),
                    (g.types["image/svg+xml"] = b.implementation.hasFeature(
                        "http://www.w3.org/TR/SVG11/feature#Image",
                        "1.1"
                    )),
                    (g.types["image/webp"] = g.detectTypeSupport(
                        "image/webp",
                        "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA="
                    ));
            }),
            (g.verifyTypeSupport = function (a) {
                var b = a.getAttribute("type");
                if (null === b || "" === b) return !0;
                var c = g.types[b];
                return "string" == typeof c && "pending" !== c
                    ? ((g.types[b] = g.detectTypeSupport(b, c)), "pending")
                    : "function" == typeof c
                        ? (c(), "pending")
                        : c;
            }),
            (g.parseSize = function (a) {
                var b = /(\([^)]+\))?\s*(.+)/g.exec(a);
                return { media: b && b[1], length: b && b[2] };
            }),
            (g.findWidthFromSourceSize = function (c) {
                for (
                    var d, e = g.trim(c).split(/\s*,\s*/), f = 0, h = e.length;
                    h > f;
                    f++
                ) {
                    var i = e[f],
                        j = g.parseSize(i),
                        k = j.length,
                        l = j.media;
                    if (k && (!l || g.matchesMedia(l)) && (d = g.getWidthFromLength(k)))
                        break;
                }
                return d || Math.max(a.innerWidth || 0, b.documentElement.clientWidth);
            }),
            (g.parseSrcset = function (a) {
                for (var b = []; "" !== a;) {
                    a = a.replace(/^\s+/g, "");
                    var c,
                        d = a.search(/\s/g),
                        e = null;
                    if (-1 !== d) {
                        c = a.slice(0, d);
                        var f = c.slice(-1);
                        if (
                            (("," === f || "" === c) &&
                                ((c = c.replace(/,+$/, "")), (e = "")),
                                (a = a.slice(d + 1)),
                                null === e)
                        ) {
                            var g = a.indexOf(",");
                            -1 !== g
                                ? ((e = a.slice(0, g)), (a = a.slice(g + 1)))
                                : ((e = a), (a = ""));
                        }
                    } else (c = a), (a = "");
                    (c || e) && b.push({ url: c, descriptor: e });
                }
                return b;
            }),
            (g.parseDescriptor = function (a, b) {
                var c,
                    d = b || "100vw",
                    e = a && a.replace(/(^\s+|\s+$)/g, ""),
                    f = g.findWidthFromSourceSize(d);
                if (e)
                    for (var h = e.split(" "), i = h.length - 1; i >= 0; i--) {
                        var j = h[i],
                            k = j && j.slice(j.length - 1);
                        if (("h" !== k && "w" !== k) || g.sizesSupported) {
                            if ("x" === k) {
                                var l = j && parseFloat(j, 10);
                                c = l && !isNaN(l) ? l : 1;
                            }
                        } else c = parseFloat(parseInt(j, 10) / f);
                    }
                return c || 1;
            }),
            (g.getCandidatesFromSourceSet = function (a, b) {
                for (
                    var c = g.parseSrcset(a), d = [], e = 0, f = c.length;
                    f > e;
                    e++
                ) {
                    var h = c[e];
                    d.push({
                        url: h.url,
                        resolution: g.parseDescriptor(h.descriptor, b),
                    });
                }
                return d;
            }),
            (g.dodgeSrcset = function (a) {
                a.srcset &&
                    ((a[g.ns].srcset = a.srcset),
                        (a.srcset = ""),
                        a.setAttribute("data-pfsrcset", a[g.ns].srcset));
            }),
            (g.processSourceSet = function (a) {
                var b = a.getAttribute("srcset"),
                    c = a.getAttribute("sizes"),
                    d = [];
                return (
                    "IMG" === a.nodeName.toUpperCase() &&
                    a[g.ns] &&
                    a[g.ns].srcset &&
                    (b = a[g.ns].srcset),
                    b && (d = g.getCandidatesFromSourceSet(b, c)),
                    d
                );
            }),
            (g.backfaceVisibilityFix = function (a) {
                var b = a.style || {},
                    c = "webkitBackfaceVisibility" in b,
                    d = b.zoom;
                c && ((b.zoom = ".999"), (c = a.offsetWidth), (b.zoom = d));
            }),
            (g.setIntrinsicSize = (function () {
                var c = {},
                    d = function (a, b, c) {
                        b && a.setAttribute("width", parseInt(b / c, 10));
                    };
                return function (e, f) {
                    var h;
                    e[g.ns] &&
                        !a.pfStopIntrinsicSize &&
                        (void 0 === e[g.ns].dims &&
                            (e[g.ns].dims =
                                e.getAttribute("width") || e.getAttribute("height")),
                            e[g.ns].dims ||
                            (f.url in c
                                ? d(e, c[f.url], f.resolution)
                                : ((h = b.createElement("img")),
                                    (h.onload = function () {
                                        if (((c[f.url] = h.width), !c[f.url]))
                                            try {
                                                b.body.appendChild(h),
                                                    (c[f.url] = h.width || h.offsetWidth),
                                                    b.body.removeChild(h);
                                            } catch (a) { }
                                        e.src === f.url && d(e, c[f.url], f.resolution),
                                            (e = null),
                                            (h.onload = null),
                                            (h = null);
                                    }),
                                    (h.src = f.url))));
                };
            })()),
            (g.applyBestCandidate = function (a, b) {
                var c, d, e;
                a.sort(g.ascendingSort), (d = a.length), (e = a[d - 1]);
                for (var f = 0; d > f; f++)
                    if (((c = a[f]), c.resolution >= g.getDpr())) {
                        e = c;
                        break;
                    }
                e &&
                    ((e.url = g.makeUrl(e.url)),
                        b.src !== e.url &&
                        (g.restrictsMixedContent() &&
                            "http:" === e.url.substr(0, "http:".length).toLowerCase()
                            ? void 0 !== window.console &&
                            console.warn("Blocked mixed content image " + e.url)
                            : ((b.src = e.url),
                                g.curSrcSupported || (b.currentSrc = b.src),
                                g.backfaceVisibilityFix(b))),
                        g.setIntrinsicSize(b, e));
            }),
            (g.ascendingSort = function (a, b) {
                return a.resolution - b.resolution;
            }),
            (g.removeVideoShim = function (a) {
                var b = a.getElementsByTagName("video");
                if (b.length) {
                    for (var c = b[0], d = c.getElementsByTagName("source"); d.length;)
                        a.insertBefore(d[0], c);
                    c.parentNode.removeChild(c);
                }
            }),
            (g.getAllElements = function () {
                for (
                    var a = [], c = b.getElementsByTagName("img"), d = 0, e = c.length;
                    e > d;
                    d++
                ) {
                    var f = c[d];
                    ("PICTURE" === f.parentNode.nodeName.toUpperCase() ||
                        null !== f.getAttribute("srcset") ||
                        (f[g.ns] && null !== f[g.ns].srcset)) &&
                        a.push(f);
                }
                return a;
            }),
            (g.getMatch = function (a, b) {
                for (var c, d = b.childNodes, e = 0, f = d.length; f > e; e++) {
                    var h = d[e];
                    if (1 === h.nodeType) {
                        if (h === a) return c;
                        if ("SOURCE" === h.nodeName.toUpperCase()) {
                            null !== h.getAttribute("src") &&
                                void 0 !== typeof console &&
                                console.warn(
                                    "The `src` attribute is invalid on `picture` `source` element; instead, use `srcset`."
                                );
                            var i = h.getAttribute("media");
                            if (h.getAttribute("srcset") && (!i || g.matchesMedia(i))) {
                                var j = g.verifyTypeSupport(h);
                                if (j === !0) {
                                    c = h;
                                    break;
                                }
                                if ("pending" === j) return !1;
                            }
                        }
                    }
                }
                return c;
            }),
            f(),
            (e._ = g),
            d(e);
    })(window, window.document, new window.Image());


   

