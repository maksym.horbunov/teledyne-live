﻿
// Vue
new Vue({
    el: '#app',
    data() {
        return {

            info: [],
            loading: true,
            errored: false,
            filterSearch: "",
            filterDate: "upcoming",

            filterRegion: [],
            filterRegionValue: "",
            filterType: [],
            filterTypeValue: "",
            filterCategory: [],
            filterCategoryValue: "",
            date: new Date(),

        };
    },



    methods: {
        refreshTab(){
            this.filterRegionValue = this.filterRegion[0],
                this.filterTypeValue = this.filterType[1],
                this.filterCategoryValue = this.filterCategory[2],
                this.filterSearch = ""

        },
        formatDate2: d => d.toLocaleString('ru-RU').replace(',', '').slice(0, -3),
    },

    async mounted() {
        axios

            // .get('json/jsonName.json')
            .get('https://teledynelecroy.com/events/output.aspx')
            .then(response => {
                this.info = response.data;

                // country
                let CountryList =  this.info.map(({ Region }) => Region);
                let CountryListNew = CountryList.filter(function(elem, pos) {
                    return CountryList.indexOf(elem) == pos;
                });
                this.filterRegion = CountryListNew;
                this.filterRegion.unshift("All");
                this.filterRegionValue = this.filterRegion[0];
                // type
                let TypeList =  this.info.map(({ TypeName }) => TypeName);
                let TypeListNew = TypeList.filter(function(elem, pos) {
                    return TypeList.indexOf(elem) == pos;
                });
                this.filterType = TypeListNew;
                this.filterType.unshift("All");
                this.filterTypeValue = this.filterType[1];



                // category
               let CategoryList =  this.info.map(({ EventCategories }) => EventCategories);

                let CategoryListNew = CategoryList.filter(function(elem, pos) {
                    return CategoryList.indexOf(elem) == pos;
                });
                this.filterCategory = CategoryListNew;
                 this.filterCategory.unshift("All");
                this.filterCategoryValue = this.filterCategory[2];

            })
            .catch(error => {
                console.log(error);
                this.errored = true;
            })
            .finally(() => (this.loading = false));
    },

    computed: {
        computedFilter(){

            if(this.filterDate == "upcoming"){
                if(this.filterRegionValue !== "All" && this.filterTypeValue !== "All" && this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && t.TypeName == this.filterTypeValue && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }

                if(this.filterRegionValue == "All" && this.filterTypeValue !== "All" && this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.TypeName == this.filterTypeValue && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }
                if(this.filterRegionValue == "All" && this.filterTypeValue == "All" && this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }

                if(this.filterRegionValue !== "All" && this.filterTypeValue == "All" && this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }
                if(this.filterRegionValue !== "All" && this.filterTypeValue == "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && new Date(t.ExhibitionStartDate) >= new Date()});
                }

                if(this.filterRegionValue !== "All" && this.filterTypeValue !== "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && t.TypeName == this.filterTypeValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }
                if(this.filterRegionValue == "All" && this.filterTypeValue !== "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.TypeName == this.filterTypeValue && new Date(t.ExhibitionStartDate) >= new Date()});
                }


                if(this.filterRegionValue == "All" && this.filterTypeValue == "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && new Date(t.ExhibitionStartDate) >= new Date()});
                }

            }


            if(this.filterDate == "on-demand"){

                if(this.filterRegionValue !== "All" && this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && t.TypeName == this.filterTypeValue && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) < new Date()});
                }

                if(this.filterRegionValue == "All" &&  this.filterCategoryValue !== "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.TypeName == this.filterTypeValue && t.EventCategories == this.filterCategoryValue && new Date(t.ExhibitionStartDate) < new Date()});
                }

                if(this.filterRegionValue !== "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && t.Region == this.filterRegionValue  && t.TypeName == this.filterTypeValue && new Date(t.ExhibitionStartDate) < new Date()});
                }

                if(this.filterRegionValue == "All" && this.filterCategoryValue == "All"){
                    return this.info.filter(t => {return t.Title.includes(this.filterSearch) && new Date(t.ExhibitionStartDate) <= new Date()});
                }

            }

        },

        getTranslateURL() {
            if (this.info.join_translation_url !== '') {
                return this.info.join_translation_url
            } else return false
        },
        emptyDeveloper() {
            if (this.info.developer !== '') {
                return this.info.developer
            } else return false
        },
        emptyPublisher() {
            if (this.info.publisher !== '') {
                return this.info.publisher
            } else return false
        }
    },


});




