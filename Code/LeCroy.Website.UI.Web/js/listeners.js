$(document).ready(function() {
  function minHeight() {
    var box = $(".formats");
    var offset = box.offset();
    var totalnewheight = offset.top + $(".formats").outerHeight() - 170;
    $('.content-mid').css('min-height', totalnewheight);
  }
  var $window = $(window);
  var $pane = $('#pane1');
  function checkWidth() {
      var div = $(".page-header");
      var height= div.outerHeight()-80;
      $('.content-mid').css('margin-top', height-5);
      $('.fixedspan3').css('margin-top', height);
      var windowsize = $window.width();
      if (windowsize < 970) {
          $('.content-mid').css('margin-top', "20px");
          $('.fixedspan3').css('margin-top', "20px");
      }
  }
  minHeight();
  checkWidth();
  $(window).resize(checkWidth);
});
$(document).ready(function() {
  function measureSize() {
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
      myWidth = window.innerWidth;
      myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
      myWidth = document.documentElement.clientWidth;
      myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
      myWidth = document.body.clientWidth;
      myHeight = document.body.clientHeight;
    }
    var nav =$(".navbar");
    var header = $(".page-header");
    var fixed = $(".fixedspan3");
    var total = nav.outerHeight() + header.outerHeight() + fixed.outerHeight();

    if (myHeight < (total + 160)) {
      $('.fixedspan3').css('position', "relative");
    }
  }
  measureSize();
  $(window).resize(measureSize);
  $(window).scroll(measureSize);
});