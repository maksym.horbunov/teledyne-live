      $(document).ready(function() {
        $('.image-popup-vertical-fit').magnificPopup({
          type: 'image',
          midClick: true,
          removalDelay: 50,
          closeOnContentClick: true,
          mainClass: 'mfp-img-mobile my-mfp-zoom-in',
          image: {
            verticalFit: true
          }     
        });
       });

      $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps, .popup-link').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          removalDelay: 160,
          preloader: false,

          fixedContentPos: false
        });
      });