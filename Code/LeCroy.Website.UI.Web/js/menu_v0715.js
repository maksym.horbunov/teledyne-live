/*
* Superfish v1.4.1 - jQuery menu widget
* Copyright (c) 2008 Joel Birch
*
* Dual licensed under the MIT and GPL licenses:
* 	http://www.opensource.org/licenses/mit-license.php
* 	http://www.gnu.org/licenses/gpl.html
*
* CHANGELOG: http://users.tpg.com.au/j_birch/plugins/superfish/changelog.txt
*/

jQuery.noConflict();

// DYNAMIC TABBING
function getHash(obj) {
    var hash = obj;
    var tabContainers = jQuery('div.tabs .bg > div');
    tabContainers.hide().filter(hash).fadeIn();
    jQuery("#product_line").attr({ src: "/images/tabs/tabs_product_line_off.gif" })
    jQuery("#overview").attr({ src: "/images/tabs/tabs_overview_off.gif" })
    jQuery("#product_details").attr({ src: "/images/tabs/tabs_product_details_off.gif" })
    jQuery("#spec").attr({ src: "/images/tabs/tabs_specs_off.gif" })
    jQuery("#options").attr({ src: "/images/tabs/tabs_options_off.gif" })
    jQuery("#probes").attr({ src: "/images/tabs/tabs_probes_off.gif" })
    jQuery("#listprice").attr({ src: "/images/tabs/tabs_list_off.gif" })
    jQuery("#rec_config").attr({ src: "/images/tabs/tabs_rec_config_off.gif" })
    jQuery("#compatability").attr({ src: "/images/tabs/compatability_off.gif" })
    jQuery("#accessories").attr({ src: "/images/tabs/accessories_off.gif" })
    jQuery("#jammer").attr({ src: "/images/tabs/tabs-infusion-off.gif" })
    jQuery("#poweranalyzer").attr({ src: "/images/tabs/tabs-poweranalyzer-off.gif" })
    jQuery("#analyzers").attr({ src: "/images/tabs/tabs_analyzers_off.gif" })
    jQuery("#exerciser").attr({ src: "/images/tabs/tabs_exerciser_off.gif" })
    jQuery("#compliance").attr({ src: "/images/tabs/tabs_compliance_off.gif" })
    if (hash == '#product_line') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_product_line_on.gif" })
    } else if (hash == '#overview') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_overview_on.gif" })
    } else if (hash == '#product_details') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_product_details_on.gif" })
    } else if (hash == '#spec') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_specs_on.gif" })
    } else if (hash == '#options') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_options_on.gif" })
    } else if (hash == '#probes') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_probes_on.gif" })
    } else if (hash == '#listprice') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_list_on.gif" })
    } else if (hash == '#rec_config') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_rec_config_on.gif" })
    } else if (hash == '#key_features') {
        jQuery(hash).attr({ src: "/images/tabs/key_features_on.gif" })
    } else if (hash == '#compatability') {
        jQuery(hash).attr({ src: "/images/tabs/compatability_on.gif" })
    } else if (hash == '#accessories') {
        jQuery(hash).attr({ src: "/images/tabs/accessories_on.gif" })
    } else if (hash == '#jammer') {
        jQuery(hash).attr({ src: "/images/tabs/tabs-infusion-on.gif" })
    } else if (hash == '#poweranalyzer') {
        jQuery(hash).attr({ src: "/images/tabs/tabs-poweranalyzer-on.gif" })
    } else if (hash == '#analyzers') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_analyzers_on.gif" })
    } else if (hash == '#exerciser') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_exerciser_on.gif" })
    } else if (hash == '#compliance') {
        jQuery(hash).attr({ src: "/images/tabs/tabs_compliance_on.gif" })
    } else {
        tabContainers.hide().filter('#product_line').fadeIn();
        jQuery('#product_line').attr({ src: "/images/tabs/tabs_product_line_on.gif" })
    }
    return false;
}
//END DYNAMIC TABBING

jQuery(document).ready(function ($) {
    $(".nav").superfish({
        animation: { opacity: "show", height: "show" }
    });

   // $(document).pngFix();

    //hide the all of the element with class msg_body
    $("#specs").hide();
    //toggle the componenet with class msg_body
    $(".title").click(function () {
        $(this).next("#specs").slideToggle(600);
        $(this).toggleClass("active");
    });


    //ACCORDION	
    //$(".accordion .list:first").addClass("active");
    $(".accordion .faq").hide();

    $(".accordion .list").click(function () {
        $(this).next("div").slideToggle("slow")
		.siblings(".faq:visible").slideUp("slow");
        $(this).toggleClass("active");
        $(this).siblings(".list").removeClass("active");
    });
    $(".accordion .vidlist").click(function () {
        $(this).next("div").slideToggle("slow")
		.siblings(".faq:visible").slideUp("slow");
        $(this).toggleClass("active");
        $(this).siblings(".vidlist").removeClass("active");
    });
    //TABS

    $(function () {
        var tabContainers = $('div.tabs .bg > div');
        $('div.tabs .bg ul.tabNavigation a').click(function () {
            tabContainers.hide().filter(this.hash).fadeIn();
            $("#product_line").attr({ src: "/images/tabs/tabs_product_line_off.gif" })
            $("#overview").attr({ src: "/images/tabs/tabs_overview_off.gif" })
            $("#product_details").attr({ src: "/images/tabs/tabs_product_details_off.gif" })
            $("#spec").attr({ src: "/images/tabs/tabs_specs_off.gif" })
            $("#options").attr({ src: "/images/tabs/tabs_options_off.gif" })
            $("#probes").attr({ src: "/images/tabs/tabs_probes_off.gif" })
            $("#listprice").attr({ src: "/images/tabs/tabs_list_off.gif" })
            $("#rec_config").attr({ src: "/images/tabs/tabs_rec_config_off.gif" })
            $("#key_features").attr({ src: "/images/tabs/key_features_off.gif" })
            $("#compatability").attr({ src: "/images/tabs/compatability_off.gif" })
            $("#accessories").attr({ src: "/images/tabs/accessories_off.gif" })
            $("#jammer").attr({ src: "/images/tabs/tabs-infusion-off.gif" })
            $("#poweranalyzer").attr({ src: "/images/tabs/tabs-poweranalyzer-off.gif" })
            $("#analyzers").attr({ src: "/images/tabs/tabs_analyzers_off.gif" })
            $("#exerciser").attr({ src: "/images/tabs/tabs_exerciser_off.gif" })
            $("#compliance").attr({ src: "/images/tabs/tabs_compliance_off.gif" })
            if (this.hash == '#product_line') {
                $(this.hash).attr({ src: "/images/tabs/tabs_product_line_on.gif" })
            } else if (this.hash == '#overview') {
                $(this.hash).attr({ src: "/images/tabs/tabs_overview_on.gif" })
            } else if (this.hash == '#product_details') {
                $(this.hash).attr({ src: "/images/tabs/tabs_product_details_on.gif" })
            } else if (this.hash == '#spec') {
                $(this.hash).attr({ src: "/images/tabs/tabs_specs_on.gif" })
            } else if (this.hash == '#options') {
                $(this.hash).attr({ src: "/images/tabs/tabs_options_on.gif" })
            } else if (this.hash == '#probes') {
                $(this.hash).attr({ src: "/images/tabs/tabs_probes_on.gif" })
            } else if (this.hash == '#listprice') {
                $(this.hash).attr({ src: "/images/tabs/tabs_list_on.gif" })
            } else if (this.hash == '#rec_config') {
                $(this.hash).attr({ src: "/images/tabs/tabs_rec_config_on.gif" })
            } else if (this.hash == '#key_features') {
                $(this.hash).attr({ src: "/images/tabs/key_features_on.gif" })
            } else if (this.hash == '#compatability') {
                $(this.hash).attr({ src: "/images/tabs/compatability_on.gif" })
            } else if (this.hash == '#accessories') {
                $(this.hash).attr({ src: "/images/tabs/accessories_on.gif" })
            } else if (this.hash == '#jammer') {
                $(this.hash).attr({ src: "/images/tabs/tabs-infusion-on.gif" })
            } else if (this.hash == '#poweranalyzer') {
                $(this.hash).attr({ src: "/images/tabs/tabs-poweranalyzer-on.gif" })
            } else if (this.hash == '#analyzers') {
                $(this.hash).attr({ src: "/images/tabs/tabs_analyzers_on.gif" })
            } else if (this.hash == '#exerciser') {
                $(this.hash).attr({ src: "/images/tabs/tabs_exerciser_on.gif" })
            } else if (this.hash == '#compliance') {
                $(this.hash).attr({ src: "/images/tabs/tabs_compliance_on.gif" })
            }

            return false;
        });

        if (document.location.hash != "") {

            var hash = document.location.hash;
            var tabContainers = $('div.tabs .bg > div');
            tabContainers.hide().filter(hash).fadeIn();
            $("#product_line").attr({ src: "/images/tabs/tabs_product_line_off.gif" })
            $("#overview").attr({ src: "/images/tabs/tabs_overview_off.gif" })
            $("#product_details").attr({ src: "/images/tabs/tabs_product_details_off.gif" })
            $("#spec").attr({ src: "/images/tabs/tabs_specs_off.gif" })
            $("#options").attr({ src: "/images/tabs/tabs_options_off.gif" })
            $("#probes").attr({ src: "/images/tabs/tabs_probes_off.gif" })
            $("#listprice").attr({ src: "/images/tabs/tabs_list_off.gif" })
            $("#rec_config").attr({ src: "/images/tabs/tabs_rec_config_off.gif" })
            $("#listprice").attr({ src: "/images/tabs/tabs_list_off.gif" })
            $("#key_features").attr({ src: "/images/tabs/key_features_off.gif" })
            $("#compatability").attr({ src: "/images/tabs/compatability_off.gif" })
            $("#accessories").attr({ src: "/images/tabs/accessories_off.gif" })
            $("#jammer").attr({ src: "/images/tabs/tabs-infusion-off.gif" })
            $("#poweranalyzer").attr({ src: "/images/tabs/tabs-poweranalyzer-off.gif" })
            $("#analyzers").attr({ src: "/images/tabs/tabs_analyzers_off.gif" })
            $("#exerciser").attr({ src: "/images/tabs/tabs_exerciser_off.gif" })
            $("#compliance").attr({ src: "/images/tabs/tabs_compliance_off.gif" })
            if (hash == '#product_line') {
                $(hash).attr({ src: "/images/tabs/tabs_product_line_on.gif" })
            } else if (hash == '#overview') {
                $(hash).attr({ src: "/images/tabs/tabs_overview_on.gif" })
            } else if (hash == '#product_details') {
                $(hash).attr({ src: "/images/tabs/tabs_product_details_on.gif" })
            } else if (hash == '#spec') {
                $(hash).attr({ src: "/images/tabs/tabs_specs_on.gif" })
            } else if (hash == '#options') {
                $(hash).attr({ src: "/images/tabs/tabs_options_on.gif" })
            } else if (hash == '#probes') {
                $(hash).attr({ src: "/images/tabs/tabs_probes_on.gif" })
            } else if (hash == '#listprice') {
                $(hash).attr({ src: "/images/tabs/tabs_list_on.gif" })
            } else if (hash == '#rec_config') {
                $(hash).attr({ src: "/images/tabs/tabs_rec_config_on.gif" })
            } else if (hash == '#key_features') {
                $(hash).attr({ src: "/images/tabs/key_features_on.gif" })
            } else if (hash == '#compatability') {
                $(hash).attr({ src: "/images/tabs/compatability_on.gif" })
            } else if (hash == '#accessories') {
                $(hash).attr({ src: "/images/tabs/accessories_on.gif" })
            } else if (hash == '#jammer') {
                $(hash).attr({ src: "/images/tabs/tabs-infusion-on.gif" })
            } else if (hash == '#poweranalyzer') {
                $(hash).attr({ src: "/images/tabs/tabs-poweranalyzer-on.gif" })
            } else if (hash == '#analyzers') {
                $(hash).attr({ src: "/images/tabs/tabs_analyzers_on.gif" })
            } else if (hash == '#exerciser') {
                $(hash).attr({ src: "/images/tabs/tabs_exerciser_on.gif" })
            } else if (hash == '#compliance') {
                $(hash).attr({ src: "/images/tabs/tabs_compliance_on.gif" })
            } else {
                tabContainers.hide().filter('#product_line').fadeIn();
                $('#product_line').attr({ src: "/images/tabs/tabs_product_line_on.gif" })
            }
            return false;
        }
    });

    //BROWSER DETECTIONS
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName = '';
    var fullVersion = 0;
    var majorVersion = 0;

    // In Internet Explorer, the true version is after "MSIE" in userAgent
    if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = parseFloat(nAgt.substring(verOffset + 5));
        majorVersion = parseInt('' + fullVersion);
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -158, leftOffset: -186});
    }

    // In Opera, the true version is after "Opera" 
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
        fullVersion = parseFloat(nAgt.substring(verOffset + 6));
        majorVersion = parseInt('' + fullVersion);
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -158, leftOffset: -185});
    }

    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
        fullVersion = parseFloat(nAgt.substring(verOffset + 8));
        majorVersion = parseInt('' + fullVersion);
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -245, leftOffset: -186});
    }

    // In Firefox, the true version is after "Safari" 
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
        fullVersion = parseFloat(nAgt.substring(verOffset + 8));
        majorVersion = parseInt('' + fullVersion);
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -240, leftOffset: -10});
        $('#content #contentMiddle .tabs .tab').css({ height: "25px" });
        $('#content #contentMiddle .tabs .bg').css({ backgroundPosition: "0px 25px" });
    }

    // In most other browsers, "name/version" is at the end of userAgent 
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = parseFloat(nAgt.substring(verOffset + 1));
        if (!isNaN(fullVersion)) majorVersion = parseInt('' + fullVersion);
        else { fullVersion = 0; majorVersion = 0; }
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -158, leftOffset: -185});
    }

    // Finally, if no name and/or no version detected from userAgent...
    if (browserName.toLowerCase() == browserName.toUpperCase()
	 || fullVersion == 0 || majorVersion == 0) {
        browserName = navigator.appName;
        fullVersion = parseFloat(nVer);
        majorVersion = parseInt(nVer);
        //	 $('a.tips').cluetip({cluetipClass: 'rounded', dropShadow: false, waitImage: false, positionBy: 'fixed', ajaxCache: false, width: 162, topOffset: -158, leftOffset: -185});
    }

});

(function ($) {
    $.superfish = {};
    $.superfish.o = [];
    $.superfish.op = {};
    $.superfish.defaults = {
        hoverClass: 'sfHover',
        pathClass: 'overideThisToUse',
        delay: 250,
        animation: { opacity: 'show' },
        speed: 'normal',
        oldJquery: false, /* set to true if using jQuery version below 1.2 */
        disableHI: false, /* set to true to disable hoverIntent usage */
        // callback functions:
        onInit: function () { },
        onBeforeShow: function () { },
        onShow: function () { }, /* note this name changed ('onshow' to 'onShow') from version 1.4 onward */
        onHide: function () { }
    };
    $.fn.superfish = function (op) {
        var bcClass = 'sfbreadcrumb',
			over = function () {
			    var $$ = $(this), menu = getMenu($$);
			    getOpts(menu, true);
			    clearTimeout(menu.sfTimer);
			    $$.showSuperfishUl().siblings().hideSuperfishUl();
			},
			out = function () {
			    var $$ = $(this), menu = getMenu($$);
			    var o = getOpts(menu, true);
			    clearTimeout(menu.sfTimer);
			    if (!$$.is('.' + bcClass)) {
			        menu.sfTimer = setTimeout(function () {
			            $$.hideSuperfishUl();
			            if (o.$path.length) { over.call(o.$path); }
			        }, o.delay);
			    }
			},
			getMenu = function ($el) { return $el.parents('ul.superfish:first')[0]; },
			getOpts = function (el, menuFound) { el = menuFound ? el : getMenu(el); return $.superfish.op = $.superfish.o[el.serial]; },
			hasUl = function () { return $.superfish.op.oldJquery ? 'li[ul]' : 'li:has(ul)'; };

        return this.each(function () {
            var s = this.serial = $.superfish.o.length;
            var o = $.extend({}, $.superfish.defaults, op);
            o.$path = $('li.' + o.pathClass, this).each(function () {
                $(this).addClass(o.hoverClass + ' ' + bcClass)
					.filter(hasUl()).removeClass(o.pathClass);
            });
            $.superfish.o[s] = $.superfish.op = o;

            $(hasUl(), this)[($.fn.hoverIntent && !o.disableHI) ? 'hoverIntent' : 'hover'](over, out)
			.not('.' + bcClass)
				.hideSuperfishUl();

            var $a = $('a', this);
            $a.each(function (i) {
                var $li = $a.eq(i).parents('li');
                $a.eq(i).focus(function () { over.call($li); }).blur(function () { out.call($li); });
            });

            o.onInit.call(this);

        }).addClass('superfish');
    };

    $.fn.extend({
        hideSuperfishUl: function () {
            var o = $.superfish.op,
				$ul = $('li.' + o.hoverClass, this).add(this).removeClass(o.hoverClass)
					.find('>ul').hide().css('visibility', 'hidden');
            o.onHide.call($ul);
            return this;
        },
        showSuperfishUl: function () {
            var o = $.superfish.op,
				$ul = this.addClass(o.hoverClass)
					.find('>ul:hidden').css('visibility', 'visible');
            o.onBeforeShow.call($ul);
            $ul.animate(o.animation, o.speed, function () { o.onShow.call(this); });
            return this;
        }
    });

    $(window).on('beforeunload', function() {
        $('ul.superfish').each(function () {
            $('li', this).unbind('mouseover', 'mouseout', 'mouseenter', 'mouseleave');
        });
    });
})(jQuery);