﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.logicanalyzers_default" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Teledyne LeCroy Modular Logic Analysis for superior mixed-signal capability. ">
    <meta name="author" content="Teledyne LeCroy">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>Logic Analyzers | Teledyne LeCroy</title>

    <!-- Bootstrap core CSS -->
    <link href="https://teledynelecroy.com/newwave/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="https://teledynelecroy.com/newwave/css/carousel.css" rel="stylesheet">
    <link href="https://teledynelecroy.com/newwave/css/newwave.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
 
      <style type="text/css">
      .tab-pane
      {
          margin-top:25px;
          vertical-align:middle;
      }
      #sb-body,#sb-loading{background-color:#fff;}
      </style>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="https://teledynelecroy.com/" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="/">Products</a></li>
                    <li><a href="/serialdata/">Serial Data</a></li>
                    <li><a href="/support/">Support</a></li>
                    <li><a href="/support/contact/">Buy</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading middle">Modular Logic Analysis for superior mixed-signal capability</h2>
          <p class="lead">With the HDA125 High-Speed Digital Analyzer, Teledyne LeCroy has defined a totally new class of instrument – a high-performance logic analyzer module that can be combined with existing high-speed oscilloscopes to provide unparalleled mixed-signal measurement and analysis.</p>


          
        </div>

        <div class="col-md-6">
          <img class="featurette-image img-responsive" alt="Modular Logic Analysis" style="max-height:500px;" src="/images/logic-analyzers-01.png">
        </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Flexibility and performance in a single platform</h2>
          <p class="lead">Competing mixed-signal solutions require an investment in an entirely new oscilloscope. The HDA125 offers superior acquisition performance with the added benefits of mobility between existing oscilloscopes.</p>

          <a href="/options/productdetails.aspx?modelid=9703&categoryid=18&groupid=54&capid=102&mid=566" type="button" class="btn btn-default btn-primary">See More: HDA125 &raquo;</a>
        </div>
        <div class="col-md-7">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#logic1" role="tab" data-toggle="tab">Transform your oscilloscope with high-speed digital channels</a></li>
  <li><a href="#logic2" role="tab" data-toggle="tab">Superior sensitivity and precise timing</a></li>
  </ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="logic1"><a href="/images/logic-analyzers-02.png" rel="shadowbox"><img src="/images/logic-analyzers-02.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">The HDA125 can be added to any Teledyne LeCroy oscilloscope with an LBUS or SYNC connector – from the 10-bit, 4 GHz HDO9000 family to the modular LabMaster 10Zi-A family with up to 100 GHz of analog bandwidth!</p></div>
  <div class="tab-pane" id="logic2"><a href="/images/logic-analyzers-03.png" rel="shadowbox"><img src="/images/logic-analyzers-03.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">This screenshot shows a 6 Gb/s signal captured in both digital and analog domains. The HDA125 samples at 12.5 GS/s, for 80ps timing accuracy to capture the fastest signals. But in challenging signal environments, vertical performance is also critical – the HDA125 has class-leading sensitivity and ultra-low probe loading to ensure pristine digital signal fidelity.</p></div>

  </div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Unmatched probing versatility</h2>
          <p class="lead">The QuickLink probe tip system was designed from the ground up to be compatible with both the HDA125 and with Teledyne LeCroy’s WaveLink series of differential analog probes. This cross-connection ability allows you to equip your system under test with QuickLink tips at all desired test points, and swap connections between digital and analog acquisition systems as needed. Industry-leading tip impedance (110 k&Omega;, 0.12pF differential) ensures superior signal fidelity and low loading.</p>

          <a href="/options/productdetails.aspx?modelid=9703&categoryid=18&groupid=54&capid=102&mid=566" type="button" class="btn btn-default btn-primary">See More: HDA125 Probing &raquo;</a>

        </div>
        <div class="col-md-7" style="text-align:left;">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#logic4" role="tab" data-toggle="tab">Superior analog and digital signal fidelity</a></li>
  <li><a href="#logic5" role="tab" data-toggle="tab">Simple and cost-effective</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="logic4"><a href="/images/logic-analyzers-04.png" rel="shadowbox"><img src="/images/logic-analyzers-04.png" width="300px" border="0" align="left" hspace="10px" style="margin-bottom: 35px;" /></a><p style="text-align:left;">When connected to a WaveLink analog probe, QuickLink tips provide 8 GHz of bandwidth and a flat, well-controlled frequency response. When used for digital acquisitions with the HDA125, they support 3 GHz bandwidth with industry-leading sensitivity. In both cases, high input impedance ensures minimal loading of the system under test.</p></div>
  <div class="tab-pane" id="logic5"><a href="/images/logic-analyzers-05.png" rel="shadowbox"><img src="/images/logic-analyzers-05.png" width="300px" border="0" align="left" hspace="10px"  style="margin-bottom: 45px;" /></a><p style="text-align:left;">QuickLink solder-in tips are low-cost, making it easy to equip multiple test points and DUTs, and eliminating time-consuming re-soldering of connectors. The integral 9-inch lead effectively relocates your test point to a more convenient location, and makes testing more reliable by eliminating torque and other forces on the solder joints.</p></div>
  


</div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
<div class="row">
        <div class="row featurette">
          <div class="col-md-5">
            <h2 class="featurette-heading top">Enhanced DDR debug</h2>
            <p class="lead">The HDA125 is ideally suited to the acquisition of DDR command bus signals, adding significant functionality to Teledyne LeCroy’s unique DDR Debug Toolkit.</p>
                      <a href="/ddr/" type="button" class="btn btn-default btn-primary">See More: DDR Debug &raquo;</a>
          

          </div>
          <div class="col-md-7" style="text-align:left;">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified" role="tablist">
              <li class="active"><a href="#logic8" role="tab" data-toggle="tab">Command Bus Capture for Full Interface Visibility</a></li>
              <li><a href="#logic9" role="tab" data-toggle="tab">Analyze Bus Activity</a></li>
              <li><a href="#logic10" role="tab" data-toggle="tab">Trigger on DDR Commands</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="logic8"><a href="/images/ddr-suite-08.png" rel="shadowbox"><img src="/images/ddr-suite-08.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>Basic debugging and validation of embedded DDR interfaces typically involves analysis of the analog properties of the clock, data (DQ) and strobe (DQS) signals but when validation tasks become more complex and problems require deeper insight, the ability to trigger on, acquire and visualize the state of the DDR command bus is invaluable. The HDA125 brings command bus acquisition to Teledyne LeCroy’s already comprehensive toolset, providing the ultimate in memory bus analysis capability.</p>
                </div>
                <div class="tab-pane" id="logic9"><a href="/images/ddr-suite-09.png" rel="shadowbox"><img src="/images/ddr-suite-09.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>The HDA125 enables the unique “bus view” feature of the DDR Debug software option, which brings Teledyne LeCroy’s advanced bus analysis feature set to bear on DDR analysis. View bus activity in tabular form, and move time-correlated views to a desired event with the touch of a button. Search for specific events and bus states within the acquired record. Intuitive color overlays and annotations make it easy to identify areas of interest in the acquired analog waveforms.</p>
                </div>
                <div class="tab-pane" id="logic10"><a href="/images/ddr-suite-10.png" rel="shadowbox"><img src="/images/ddr-suite-10.png" width="300px" border="0" align="left" hspace="20px" vspace="25px"></a><p>The ability to trigger on specific states of the command bus becomes an invaluable tool for quick understanding of DDR signal quality. The HDA125’s logic triggering combines with the DDR Debug toolkit's intuitive setup and intelligent software cross-triggering to provide the ultimate DDR triggering system. Persistence maps of read and write bursts provide an easy and fast means of identifying subtle signal-quality problems for further investigation.</p>
                </div>
            <!-- End NAV TABS -->
            </div>
        </div>
      </div>
</div>
      <hr class="featurette-divider">
      
<!-- LOGICSTUDIO EOL <div class="row">
        <div class="row featurette">
          <div class="col-md-5">
            <h2 class="featurette-heading top">LogicStudio</h2>
            <p class="lead">The LogicStudio is a powerful solution for high-speed test and debug. For lower-speed signals, LogicStudio delivers a powerful feature set, high performance hardware and an intuitive point and click user-interface – all for under $1,000.</p>
            <a href="/logicstudio/" type="button" class="btn btn-default btn-primary">See More: LogicStudio &raquo;</a>
          </div>
          <div class="col-md-7" style="text-align:left;">

            <ul class="nav nav-pills nav-justified" role="tablist">
              <li class="active"><a href="#logic22" role="tab" data-toggle="tab">Powerful feature set with simple operation</a></li>
              <li><a href="#logic23" role="tab" data-toggle="tab">Digital, Serial, Analog</a></li>
            </ul>

            <div class="tab-content">
              <div class="tab-pane active" id="logic22"><a href="/images/logic-analyzers-06.png" rel="shadowbox"><img src="/images/logic-analyzers-06.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">Timing cursors, history mode and serial data protocol decoding help debug the most complicated problems. Simple mouse operations control every aspect of the user-interface from panning and zooming waveforms to configuring the trigger.</p></div>
              <div class="tab-pane" id="logic23"><a href="/images/logic-analyzers-07.png" rel="shadowbox"><img src="/images/logic-analyzers-07.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">Turn your PC in to a MSO by connecting LogicStudio to any of ten popular oscilloscopes from Teledyne LeCroy, Tektronix and Keysight. See Compatible Oscilloscopes</p></div>
            </div>
        </div>
      </div>
      </div>-->

      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2020&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://teledynelecroy.com/newwave/js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
        Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
