﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.maui_default" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>MAUI – Most Advanced User Interface | Teledyne LeCroy</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
  .available-on{
    min-height:300px;
  }
  </style>
<!--Adobe Edge Runtime-->
    <script type="text/javascript" charset="utf-8" src="edge_includes/edge.6.0.0.min.js"></script>
    <style>
        .edgeLoad-copy-setup { visibility:hidden; }
    </style>
<script>
   AdobeEdge.loadComposition('copy-setup', 'copy-setup', {
    scaleToFit: "both",
    centerStage: "both",
    minW: "0px",
    maxW: "400px",
    width: "400px",
    height: "300px"
}, {"style":{"${symbolSelector}":{"isStage":"true","rect":["undefined","undefined","400px","300px"],"fill":["rgba(255,255,255,1)"]}},"dom":[{"rect":["140","90","120px","120px","auto","auto"],"id":"preloader4","fill":["rgba(0,0,0,0)","images/preloader4.gif","0px","0px"],"type":"image","tag":"img"}]}, {"style":{"${symbolSelector}":{"isStage":"true","rect":["undefined","undefined","400px","300px"],"fill":["rgba(255,255,255,1)"]}},"dom":[{"rect":["0","0","400px","300px","auto","auto"],"id":"Poster","fill":["rgba(0,0,0,0)","images/Poster.png","0px","0px"],"type":"image","tag":"img"}]});
</script>
<!--Adobe Edge Runtime End-->
</head>
<body>
  <div class="container">
    <a id="logo-container" href="../" alt=""><img src="/images/tl_weblogo_blkblue_189x30.png" class="brand-logo" alt="Teledyne LeCroy"/></a>
  </div>


  <div id="index-banner">
    <div class="section">
      <div class="container">
      <div class="row">
      <div class="col l6 m8 s12"><p class="h1 maui-blue">MAUI – Most Advanced User Interface</p>
          <p class="h3 black-text">MAUI was developed to put all the <b>power and capabilities</b> of the modern <a href="/oscilloscope/">oscilloscope</a> right at your fingertips. MAUI is <b>designed for touch</b>, <b>built for simplicity</b>, and <b>made to solve</b>.  The addition of OneTouch to MAUI delivers a unique set of touchscreen gestures that <b>simplifies measurement setup</b> and brings <b>unsurpassed efficiency and intuitiveness</b> to <a href="/oscilloscope/">oscilloscope</a> operation.</p>
          <div class="col m8" style="margin-left:-10px;">
                <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                    <span class="wistia_embed wistia_async_ssrlz8cpls popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>
            </div>

        </div>

      <div class="col l6 m8 s12">
      <img class="right responsive-img" src="/images/maui-itransport.png" alt="MAUI - Most Advanced User Interface"></div>
      </div>
      </div>
    </div>
  </div>

<div class="clearfix"></div>
  <div class="wrap">
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
            <div class="row">
        <div class="col s12 m4">
          <div class="card hoverable available-on">
            <div class="card-image">
              <img class="materialboxed responsive-img" src="/images/waverunner8000-touchscreen-2288.png">
            </div>
            <div class="card-content">
            <h5 class="center maui-blue darken-2">Designed for Touch</h5>

            <p class="light">MAUI is designed for touch. Operate the <a href="/oscilloscope/">oscilloscope</a> just like a phone or tablet with the most unique touch screen features on any oscilloscope. All important controls are always one touch away. Touch the waveform to position or zoom in for more details using intuitive actions.</p>
            </div>

          </div>
        </div>
        <div class="col s12 m4">
          <div class="card hoverable available-on">
            <div class="card-image">
              <div id="Stage" class="copy-setup">
                <div id="Stage_SCREEN-2-01" class="edgeLoad-copy-setup"></div>
                <div id="Stage_SCREEN-2-02" class="edgeLoad-copy-setup"></div>
                <div id="Stage_HIGHLIGHT-2" class="edgeLoad-copy-setup"></div>
                <div id="Stage_SCREEN-2-03-descriptor" class="edgeLoad-copy-setup"></div>
                <div id="Stage_HAND-2" class="edgeLoad-copy-setup"></div>
              </div> 
            </div>
            <div class="card-content"><h5 class="center maui-blue darken-2">Built for Simplicity</h5>

            <p class="light">MAUI is built for simplicity. Basic waveform viewing and <a href="/tools/">measurement tools</a> as well as advanced math and analysis capabilities are seamlessly integrated in a single user interface. Time saving shortcuts and intuitive dialogs simplify setup and shorten debug time.<br />&nbsp</p>
            </div>

          </div>
        </div>
                <div class="col s12 m4">
          <div class="card hoverable available-on">
            <div class="card-image">
<img class="materialboxed responsive-img" src="/images/wr8k-tdme-m.png">            </div>
            <div class="card-content"><br />&nbsp;<h5 class="center maui-blue darken-2">Made to Solve</h5>

            <p class="light">MAUI is made to solve. A deep set of integrated debug and <a href="/tools/">analysis tools</a> help identify problems and find solutions quickly. Unsurpassed integration provides critical flexibility when debugging. Solve problems fast with powerful analysis tools.<br />&nbsp</p>
            </div>

          </div>
        </div>
      </div>
      

    </div>
  </div>
</div>


  <div class="container">
    <div class="section">
<div class="row center-align">
<h2 class="orange-text text-darken-2">MAUI with OneTouch</h2>
                      <p class="h4">MAUI with OneTouch sets the standard for <a href="/oscilloscope/">oscilloscope</a> user experience by providing the most unique touch features on any oscilloscope. Common gestures are used to instinctively interact with the oscilloscope and dramatically reduce setup time. Convenience and efficiency are optimized - all common operations can be performed with one touch and do not require opening and closing of pop-up dialogs or menus.</p>
</div>
      <div class="row">
        <div class="col s12 m8">
          <div class="card">
            <div class="card-image">

              <img class="materialboxed" src="/images/maui-w-onetouch-callouts.png" class="responsive-img" width="600px">
<br/>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;<span style=" display:inline-block; vertical-align:top;">MAUI</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;<span style=" display:inline-block; vertical-align:top;">OneTouch</span>
            </div>
          </div>
        </div>

  <div class="col l4 m4 s12 callout2">
  <ul class="collection z-depth-1">
    <li class="collection-item avatar">
      <i class="material-icons circle blue">A</i>
      <p>Channel, timebase, and trigger descriptors provide easy access to controls without navigating menus.</p>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle blue">B</i>
      <p>Configure parameters by touching measurement results. </p>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle blue">C</i>
      <p>Shortcuts to commonly used functions are displayed at the bottom of the channel, math and memory menus.</p>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle" style="background-color:#f26631;">D</i>
      <p>Use the &quot;Add New&quot; button for one-touch trace creation.</p>
    </li>
        <li class="collection-item avatar">
      <i class="material-icons circle" style="background-color:#f26631;">E</i>
      <p>Drag to change source, copy setup, turn on new trace, or move waveform location.</p>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle" style="background-color:#f26631;">F</i>
      <p>Drag to copy measurement parameters to streamline setup process.</p>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle" style="background-color:#f26631;">G</i>
      <p>Drag to quickly position cursors on a trace.</p>
    </li>
  </ul>
        </div></div>

  
    </div>
  </div>



<div class="container">
  <div class="section">
    <div class="row">
      <h3>Available on:</h3>
      <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;<span style=" display:inline-block; vertical-align:top;">MAUI</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;<span style=" display:inline-block; vertical-align:top;">OneTouch</span></p>
      <div class="col s12 m2 l2">

        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/wavesurfer-3000z-oscilloscopes"><img class="responsive-img" src="/images/wavesurfer-3000z-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i></p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveSurfer 3000z Oscilloscopes</p>
          </div>
        </div>
      </div>
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=549"><img class="responsive-img" src="/images/wavesurfer510-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveSurfer 510 Oscilloscope</p>
            
          </div>
        </div>
      </div>
    
      
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/wavesurfer-4000hd-oscilloscopes"><img class="responsive-img" src="/images/ws4000hd-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveSurfer 4000HD High Definition Oscilloscopes</p>
            
          </div>
        </div>
      </div>
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=591"><img class="responsive-img" src="/images/waverunner9000-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveRunner 9000 Oscilloscopes</p>

          </div>
        </div>
      </div>




      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=551"><img class="responsive-img" src="/images/hdo6000a-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">HDO6000A High Definition Oscilloscopes</p>
            
          </div>
        </div>
      </div>
    
      
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=547"><img class="responsive-img" src="/images/hdo8000a-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">HDO8000A High Definition Oscilloscopes</p>
            
          </div>
        </div>
      </div>
        <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=607"><img class="responsive-img" src="/images/wr8000hd-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveRunner 8000HD Oscilloscopes</p>
            
          </div>
        </div>
      </div>
        <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=606"><img class="responsive-img" src="/images/mda8000hd-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">MDA8000HD Motor Drive Analyzers</p>
            
          </div>
        </div>
      </div>
    
      
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=548"><img class="responsive-img" src="/images/mda800a-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">MDA800A Motor Drive Analyzers</p>
            
          </div>
        </div>
      </div>
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/wavepro-hd-oscilloscope"><img class="responsive-img" src="/images/wphd-selection.png"></a>
          </div>
          <div class="card-content">
             <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WavePro HD Oscilloscopes</p>
            
          </div>
        </div>
      </div>
    
      
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=480"><img class="responsive-img" src="/images/wavemaster8zi-b-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">WaveMaster / SDA / DDA 8 Zi-B Oscilloscopes</p>
            
          </div>
        </div>
      </div>
    
      
     
      
      <div class="col s12 m2 l2">
        <div class="card hoverable available-on">
          <div class="card-image">
            <a href="/oscilloscope/oscilloscopeseries.aspx?mseries=484"><img class="responsive-img" src="/images/labmaster10zia-selection.png"></a>
          </div>
          <div class="card-content">
            <p>&nbsp;&nbsp;<i class="material-icons circle blue">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="material-icons circle" style="background-color:#f26631;">&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;</p>
            <p style="font-size:15px;font-weight:300;" class="center maui-blue darken-2">LabMaster 10 Zi-A Oscilloscopes</p>
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
</div>



<footer class="page-footer white">
      <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2019&nbsp;Teledyne LeCroy</p>

    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
      </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/init.js"></script>
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
      <script>
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>

  </body>
</html>
