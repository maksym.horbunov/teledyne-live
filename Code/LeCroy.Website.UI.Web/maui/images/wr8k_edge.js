/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'wr8k-splash-scene3-01',
                            display: 'none',
                            type: 'image',
                            rect: ['110px', '-228px', '1280px', '800px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%203/wr8k-splash-scene3-01.jpg",'0px','0px']
                        },
                        {
                            id: 'wr8k-splash-scene3-02',
                            display: 'none',
                            type: 'image',
                            rect: ['110px', '-228px', '1280px', '800px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%203/wr8k-splash-scene3-02.jpg",'0px','0px']
                        },
                        {
                            id: 'wr8k-splash-scene1-02',
                            display: 'none',
                            type: 'image',
                            rect: ['110px', '-228px', '1280px', '800px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%201/wr8k-splash-scene1-02.jpg",'0px','0px']
                        },
                        {
                            id: 'wr8k-splash-scene1-01',
                            display: 'block',
                            type: 'image',
                            rect: ['110px', '-228px', '1280px', '800px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%201/wr8k-splash-scene1-01.jpg",'0px','0px']
                        },
                        {
                            id: 'Scene_3_Highlight',
                            display: 'none',
                            type: 'rect',
                            rect: ['214px', '503px', '98px', '49px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,0)"],
                            stroke: [1,"rgb(255, 255, 255)","solid"]
                        },
                        {
                            id: 'Scene_1_Highlight',
                            display: 'none',
                            type: 'rect',
                            rect: ['312px', '452px', '94px', '47px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,0)"],
                            stroke: [2,"rgb(255, 255, 255)","solid"]
                        },
                        {
                            id: 'wr8k-splash-scene3-03-descriptor',
                            display: 'none',
                            type: 'image',
                            rect: ['114px', '505px', '97px', '49px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%203/wr8k-splash-scene3-03-descriptor.jpg",'0px','0px']
                        },
                        {
                            id: 'wr8k-splash-scene1-03-descriptor',
                            display: 'none',
                            type: 'image',
                            rect: ['213px', '503px', '99px', '50px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Scene%201/wr8k-splash-scene1-03-descriptor.png",'0px','0px']
                        },
                        {
                            id: 'Blackout_Rectangle',
                            type: 'rect',
                            rect: ['-118px', '-575px', '1472px', '852px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,1.00)"],
                            stroke: [10,"rgba(255,255,255,0.00)","dotted"]
                        },
                        {
                            id: 'wr8k-splash-mockup',
                            type: 'image',
                            rect: ['8px', '8px', '1250px', '750px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"wr8k-splash-mockup.png",'0px','0px']
                        },
                        {
                            id: 'Scene_3_Text',
                            display: 'none',
                            type: 'text',
                            rect: ['106px', '217px', '565px', '89px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-weight: 700; font-size: 30px;\">Add New</span></p><p style=\"margin: 0px;\">Drag the Channel 1 descriptor box to the Add New box.</p><p style=\"margin: 0px;\">A new Channel 2 descriptor box is now turned on.</p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\"></span></p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\"></span></p><p style=\"margin: 0px;\"></p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(255,255,255,1.00)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "24px", "", "none"]
                        },
                        {
                            id: 'Scene_1_Text',
                            display: 'block',
                            type: 'text',
                            rect: ['106px', '217px', '460px', '89px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255); font-size: 30px; font-weight: 700;\">Change Source</span></p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\">Drag the Channel 2 descriptor box to the measurements table.</span></p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\">The source on P1:ampl is now set to Channel 2.</span></p><p style=\"margin: 0px;\"></p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(219,25,25,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "24px", "", "none"]
                        },
                        {
                            id: 'Scene_3_Point',
                            display: 'none',
                            type: 'image',
                            rect: ['192px', '531px', '185px', '185px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Hands/right-point.png",'0px','0px']
                        },
                        {
                            id: 'Scene_1_Right_Hand_Index_Point',
                            display: 'none',
                            type: 'image',
                            rect: ['343px', '468px', '185px', '185px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Hands/right-point.png",'0px','0px']
                        },
                        {
                            id: 'see-onetouch-help-button',
                            type: 'image',
                            rect: ['106', '697px', '392px', '40px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"see-onetouch-help-button.png",'0px','0px']
                        },
                        {
                            id: 'Loading_TEXT',
                            display: 'none',
                            type: 'text',
                            rect: ['849px', '383px', '104px', '27px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255); font-size: 18px;\">Loading...</span></p>",
                            align: "left",
                            font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Loading_Circle_Animation',
                            display: 'block',
                            type: 'ellipse',
                            rect: ['841px', '294px', '61px', '61px', 'auto', 'auto'],
                            borderRadius: ["50%", "50%", "50%", "50%"],
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [10,"rgb(255, 255, 255)","dotted"],
                            transform: [[],['4320']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1250px', '750px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(0,0,0,1)",[270,[['rgba(255,255,255,0.00)',0],['rgba(255,255,255,0.00)',100]]]]
                        }
                    }
                },
                timeline: {
                    duration: 60000,
                    autoPlay: true,
                    labels: {
                        "label1": 0,
                        "label2": 20000
                    },
                    data: [
                        [
                            "eid160",
                            "height",
                            22000,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            '185px',
                            '185px'
                        ],
                        [
                            "eid215",
                            "color",
                            20000,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            'rgba(255,255,255,1.00)',
                            'rgba(255,255,255,1.00)'
                        ],
                        [
                            "eid201",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Scene_1_Highlight}",
                            'none',
                            'none'
                        ],
                        [
                            "eid200",
                            "display",
                            4500,
                            0,
                            "linear",
                            "${Scene_1_Highlight}",
                            'none',
                            'block'
                        ],
                        [
                            "eid203",
                            "display",
                            7000,
                            0,
                            "linear",
                            "${Scene_1_Highlight}",
                            'block',
                            'none'
                        ],
                        [
                            "eid120",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Loading_TEXT}",
                            'none',
                            'block'
                        ],
                        [
                            "eid181",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Scene_3_Highlight}",
                            'none',
                            'none'
                        ],
                        [
                            "eid179",
                            "display",
                            24250,
                            0,
                            "linear",
                            "${Scene_3_Highlight}",
                            'none',
                            'block'
                        ],
                        [
                            "eid180",
                            "display",
                            27000,
                            0,
                            "linear",
                            "${Scene_3_Highlight}",
                            'block',
                            'none'
                        ],
                        [
                            "eid175",
                            "left",
                            24000,
                            2000,
                            "linear",
                            "${wr8k-splash-scene3-03-descriptor}",
                            '114px',
                            '219px'
                        ],
                        [
                            "eid107",
                            "rotateZ",
                            0,
                            60000,
                            "linear",
                            "${Loading_Circle_Animation}",
                            '0deg',
                            '4320deg'
                        ],
                        [
                            "eid161",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-03-descriptor}",
                            'none',
                            'none'
                        ],
                        [
                            "eid168",
                            "display",
                            24000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-03-descriptor}",
                            'none',
                            'block'
                        ],
                        [
                            "eid169",
                            "display",
                            27000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-03-descriptor}",
                            'block',
                            'none'
                        ],
                        [
                            "eid178",
                            "top",
                            13000,
                            0,
                            "linear",
                            "${Scene_1_Text}",
                            '217px',
                            '217px'
                        ],
                        [
                            "eid214",
                            "width",
                            20000,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            '565px',
                            '565px'
                        ],
                        [
                            "eid154",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Scene_1_Text}",
                            'block',
                            'block'
                        ],
                        [
                            "eid155",
                            "display",
                            19000,
                            0,
                            "linear",
                            "${Scene_1_Text}",
                            'block',
                            'none'
                        ],
                        [
                            "eid171",
                            "top",
                            24000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-03-descriptor}",
                            '505px',
                            '505px'
                        ],
                        [
                            "eid153",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Loading_Circle_Animation}",
                            'block',
                            'block'
                        ],
                        [
                            "eid197",
                            "left",
                            21000,
                            2000,
                            "linear",
                            "${Scene_3_Point}",
                            '-306px',
                            '93px'
                        ],
                        [
                            "eid192",
                            "left",
                            24000,
                            2000,
                            "linear",
                            "${Scene_3_Point}",
                            '93px',
                            '192px'
                        ],
                        [
                            "eid186",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            'none',
                            'none'
                        ],
                        [
                            "eid185",
                            "display",
                            20000,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            'none',
                            'none'
                        ],
                        [
                            "eid187",
                            "display",
                            21000,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            'none',
                            'block'
                        ],
                        [
                            "eid248",
                            "display",
                            27000,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            'block',
                            'none'
                        ],
                        [
                            "eid166",
                            "width",
                            22000,
                            0,
                            "linear",
                            "${Scene_3_Point}",
                            '185px',
                            '185px'
                        ],
                        [
                            "eid198",
                            "top",
                            21000,
                            2000,
                            "linear",
                            "${Scene_3_Point}",
                            '569px',
                            '532px'
                        ],
                        [
                            "eid193",
                            "top",
                            24000,
                            2000,
                            "linear",
                            "${Scene_3_Point}",
                            '532px',
                            '531px'
                        ],
                        [
                            "eid40",
                            "left",
                            1000,
                            2000,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            '-297px',
                            '209px'
                        ],
                        [
                            "eid44",
                            "left",
                            4000,
                            2000,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            '209px',
                            '343px'
                        ],
                        [
                            "eid172",
                            "left",
                            4000,
                            2000,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            '213px',
                            '343px'
                        ],
                        [
                            "eid196",
                            "left",
                            6000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            '343px',
                            '343px'
                        ],
                        [
                            "eid157",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-01}",
                            'block',
                            'block'
                        ],
                        [
                            "eid165",
                            "display",
                            6000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-01}",
                            'block',
                            'none'
                        ],
                        [
                            "eid159",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-01}",
                            'none',
                            'none'
                        ],
                        [
                            "eid152",
                            "display",
                            20000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-01}",
                            'none',
                            'block'
                        ],
                        [
                            "eid150",
                            "display",
                            27000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-01}",
                            'block',
                            'none'
                        ],
                        [
                            "eid41",
                            "top",
                            1000,
                            2000,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            '724px',
                            '528px'
                        ],
                        [
                            "eid45",
                            "top",
                            4000,
                            2000,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            '528px',
                            '468px'
                        ],
                        [
                            "eid173",
                            "top",
                            4000,
                            2000,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            '503px',
                            '443px'
                        ],
                        [
                            "eid156",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            'none',
                            'none'
                        ],
                        [
                            "eid167",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            'none',
                            'block'
                        ],
                        [
                            "eid174",
                            "display",
                            7000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            'block',
                            'none'
                        ],
                        [
                            "eid163",
                            "display",
                            19000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-03-descriptor}",
                            'none',
                            'none'
                        ],
                        [
                            "eid204",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-02}",
                            'none',
                            'none'
                        ],
                        [
                            "eid149",
                            "display",
                            27000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-02}",
                            'none',
                            'block'
                        ],
                        [
                            "eid151",
                            "display",
                            60000,
                            0,
                            "linear",
                            "${wr8k-splash-scene3-02}",
                            'block',
                            'none'
                        ],
                        [
                            "eid210",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            'none',
                            'none'
                        ],
                        [
                            "eid211",
                            "display",
                            20000,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            'none',
                            'block'
                        ],
                        [
                            "eid212",
                            "display",
                            60000,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            'block',
                            'none'
                        ],
                        [
                            "eid83",
                            "display",
                            1000,
                            0,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            'none',
                            'block'
                        ],
                        [
                            "eid82",
                            "display",
                            7000,
                            0,
                            "linear",
                            "${Scene_1_Right_Hand_Index_Point}",
                            'block',
                            'none'
                        ],
                        [
                            "eid213",
                            "top",
                            33000,
                            0,
                            "linear",
                            "${Scene_3_Text}",
                            '217px',
                            '217px'
                        ],
                        [
                            "eid158",
                            "display",
                            0,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-02}",
                            'none',
                            'none'
                        ],
                        [
                            "eid247",
                            "display",
                            6000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-02}",
                            'none',
                            'block'
                        ],
                        [
                            "eid162",
                            "display",
                            19000,
                            0,
                            "linear",
                            "${wr8k-splash-scene1-02}",
                            'block',
                            'none'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("wr8k_edgeActions.js");
})("wr8k");
