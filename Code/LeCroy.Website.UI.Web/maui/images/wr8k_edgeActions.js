/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // insert code to be run when the composition is fully loaded here
         $("body").css("background-color","#000000");
         var rand = Math.floor((Math.random() * 2) + 1);
         //alert (rand);
         sym.play('label'+rand);

      });
      //Edge binding end

      Symbol.bindTimelineAction(compId, symbolName, "Default Timeline", "play", function(sym, e) {
         // insert code to be run at timeline play here
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${see-onetouch-help-button}", "click", function(sym, e) {
         // insert code for mouse click here
         window.open("../../HTML5-CSH/WR8K-OLH/Content/A_Getting Started/OneTouch Help.htm");
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "wr8k");