﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.mauistudio_default" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner-slide section-white section-angle-before angle-small small">
					<div class="banner-slider"> 
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>
											 MAUI Studio</h1>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<div class="page-content mb-4">
									<p>Unleash the power of a Teledyne LeCroy oscilloscope anywhere using a PC with MAUI Studio Pro. Work remotely from your oscilloscope and be more productive.</p>
									<ul class="list-dots">
										<li> <b>Flexibility  </b>– work remotely, simulate lab setups, portable software options</li>
										<li> <b>Productivity </b>– easily share data, better collaboration, move faster</li>
										<li> <b>Power </b>– unbelievable analytical capability at your fingertips</li>
									</ul>
									<p>Download and register <a href="/doc/maui-studio-software">here</a> for a free 30-day trial of MAUI Studio Pro.</p>
								</div><div class="page-button"><a class="btn btn-default btn-blue-outline" href="/support/softwaredownload/mauistudio.aspx">Download Software</a><a class="btn btn-default btn-blue-outline" href="/mauistudioregister/">Register Software</a><a class="btn btn-default btn-blue-outline" href="/doc/maui-studio-pro-datasheet/">Datasheet</a></div>
							</div>
							<div class="col-md-4">
								<a href="/options/productseries.aspx?mseries=648&groupid=144"><img src="https://assets.lcry.net/images/maui-studio-top.png" alt="img-description"></a>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="pcreq">
					<div class="container">

						<div class="section-content">      

									<div class="page-content">
									
										<div class="table-list mb-2">
											<div class="table-list-column col-md-6 p-0 first">
												<div class="table-column-item">

												</div>
												<div class="table-column-item">
													Number of Math Functions

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Math function uses an input source to output a new math trace."> ? </a>
												</div>
												<div class="table-column-item">
													Number of Measurements

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Measurements such as Amplitude, Area, Frequency, Maximum, Mean, Period, RMS and many more."> ? </a>
												</div>
												<div class="table-column-item">
													Number of Pass/Fail

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Compare an acquired waveform to a mask template."> ? </a>
												</div>
												<div class="table-column-item">
													Number of Grid Options

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="The number of grids displayed at once."> ? </a>
												</div>
												<div class="table-column-item">
													Waveform Save/Recall

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Save waveform data either to internal memory or a file, and recall file to internal memory."> ? </a>
												</div>
												<div class="table-column-item">
													Waveform Import

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Import waveform files from: Teledyne LeCroy (.trc), Tektronix(.wfm), Keysight (.bin), Rohde & Schwarz (.bin), and Yokogawa (.wvf)."> ? </a>
												</div>
												<div class="table-column-item">
													Simulate Basic Waveforms (Sine, Square, Triangle, etc.)

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Waveform functions such as Sine, Square, Pulse, Triangle, Ramp, Exponential, and DC."> ? </a>
												</div>

												<div class="table-column-item">
													Simulate Advanced Waveforms (Define jitter and noise)

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Create waveforms using mathematical expressions. 9 additional data type signals and can add jitter to these signals."> ? </a>
												</div>
												<div class="table-column-item">
													Remote Control

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Connect to a Teledyne LeCroy oscilloscope via an Ethernet connection."> ? </a>
												</div>
												<div class="table-column-item">
													Copy Options from Remote Scope

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Remote connection will copy the oscilloscope's software options to MAUI Studio Pro."> ? </a>
												</div>
												<div class="table-column-item">
													Setup Save/Recall

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Save oscilloscope’s configuration to a file and recall to both MAUI Studio and MAUI Studio Pro."> ? </a>
												</div>
												<div class="table-column-item">
													LabNotebook Save/Recall

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="LabNotebook is a composite file containing a screen capture and all waveform and setup data."> ? </a>
												</div>
												<div class="table-column-item">
													LabNotebook Model/Option Recall

													<a tabindex="0" class="tooltip" data-toggle="tooltip" data-placement="right" data-trigger="focus" data-html="true" 
													data-title="Recall a LabNotebook file to copy the oscilloscope's software options to MAUI Studio Pro."> ? </a>
												</div>
											</div>	

											<div class="table-list-column col-md-3 p-0 text-center">
												<div class="table-column-item">
													MAUI Studio
												</div>
												<div class="table-column-item">
													2
												</div>
												<div class="table-column-item">
													8
												</div>
												<div class="table-column-item">
													1
												</div>
												<div class="table-column-item">
													10
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													-
												</div>
												<div class="table-column-item">
													-
												</div>
												<div class="table-column-item">
													-
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													-
												</div>
											</div>

											<div class="table-list-column active col-md-3 p-0 text-center">
												<div class="table-column-item">
													MAUI Studio Pro
												</div>
												<div class="table-column-item">
													12
												</div>
												<div class="table-column-item">
													12
												</div>
												<div class="table-column-item">
													12
												</div>
												<div class="table-column-item">
													20
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
												<div class="table-column-item">
													<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M19.3749 0.453125H17.7366C17.5069 0.453125 17.289 0.558594 17.1483 0.739062L7.48505 12.9805L2.85145 7.10938C2.78136 7.02038 2.69201 6.94842 2.59013 6.8989C2.48824 6.84937 2.37646 6.82357 2.26317 6.82344H0.624891C0.46786 6.82344 0.381141 7.00391 0.477235 7.12578L6.89677 15.2586C7.19677 15.6383 7.77333 15.6383 8.07567 15.2586L19.5225 0.753125C19.6186 0.633594 19.5319 0.453125 19.3749 0.453125Z" fill="#0076C0"/>
													</svg>
												</div>
											</div>
										</div>
										<!--<div class="table-list-description ml-3">File formats supported: LeCroy (.trc), Tektronix(.wfm), Keysight (.bin), Rohde & Schwarz (.bin), and Yokogawa (.wvf)</div>-->
									</div>
								
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="videos">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Videos </h2>
						</div>
						<div class="section-content">   
							<div class="row">
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_700j741di0 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-700j741di0-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_40.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_231.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_246.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_246.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_231.thumbnail_img" alt="Wistia video thumbnail" src="https://embed-fastly.wistia.com/deliveries/70bba3ddb2d6f256ea63245bb35dce83.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_vbefw7epwi wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-vbefw7epwi-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_50.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_256.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_271.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_271.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_256.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/38fee05831f390ea081f73e0ca83e9bb.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_tq86dyx296 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-tq86dyx296-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_60.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_281.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_296.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_296.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_281.thumbnail_img" alt="Wistia video thumbnail" src="https://embed-fastly.wistia.com/deliveries/ee59f81acee492545c6f42ddd513cc91.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_oom2w0d75f wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-oom2w0d75f-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_70.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_205.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_221.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_221.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_205.thumbnail_img" alt="Wistia video thumbnail" src="https://embed-fastly.wistia.com/deliveries/f8f3db3d9a19cada9a4fd9f1f050c71b.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_5uwswdgmpd wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-5uwswdgmpd-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_80.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_306.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_321.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_321.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_306.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/d144cb448e5c61e98c835c08da611549.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page technical-docs" id="briefs">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Technical Docs</h2>
						</div>
						<div class="section-content"><div class="row">
							<ul class="collection technical-docs-ul"><li><a href="/doc/docview.aspx?id=12818">
												<h5>MAUI Studio Pro AFG</h5>
												<div class="description" title="This application note provides examples of commonly used waveforms and equations.">This application note provides examples of commonly used waveforms and equations.</div></a></li>
									<li><a href="/doc/docview.aspx?id=12828">
												<h5>MAUI Studio Pro LabNotebook</h5>
												<div class="description" title=" This application note describes how MAUI Studio Pro uses the enhanced features in a LabNotebook."> This application note describes how MAUI Studio Pro uses the enhanced features in a LabNotebook.</div></a></li>
									<li><a href="/doc/docview.aspx?id=12829">
												<h5>MAUI Studio Pro Recall Various Waveform Formats</h5>
												<div class="description" title="This application note discusses methods for recalling waveform data from other vendors' oscilloscopes into MAUI Studio.">This application note discusses methods for recalling waveform data from other vendors' oscilloscopes into MAUI Studio.</div></a></li>
									<li><a href="/doc/docview.aspx?id=12830">
												<h5>MAUI Studio Pro Remote Control</h5>
												<div class="description" title="This application note will explore remote control from initial connection, to triggering, and to transferring files between the oscilloscope and MAUI Studio Pro.">This application note will explore remote control from initial connection, to triggering, and to transferring files between the oscilloscope and MAUI Studio Pro.</div></a></li>
								                      </ul></div>
							</div></div></section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Explore More Learning Opportunities from Teledyne LeCroy</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="card-line"> 
										<div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/maui-studio-02.png"></span></div>
										<div class="inner-content mb-24">  
											<div class="card-title">                                      
												<h3>Live Events                                         </h3>
											</div>
											<div class="card-text">
												<p>Attend live webinars or events hosted by industry and application experts to gain expertise and learn how to solve problems faster.         </p>
											</div>
										</div>
										<div class="inner-visible text-center mb-40">                                               <a class="btn btn-default" href="/events/">Explore</a></div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/maui-studio-03.png"></span></div>
										<div class="inner-content mb-24">  
											<div class="card-title">                                      
												<h3>On-demand Webinars                                   </h3>
											</div>
											<div class="card-text">
												<p>Access our archive of on-demand webinars to get up-to-speed quickly on the topics that matter to you most.   </p>
											</div>
										</div>
										<div class="inner-visible text-center mb-40">                                               <a class="btn btn-default" href="https://teledynelecroy.com/support/techlib/webcasts.aspx">Explore    </a></div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-img jumbotron bg-retina big"><span data-srcset="https://assets.lcry.net/images/maui-studio-04.png"></span></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Application Notes                                 </h3>
											</div>
											<div class="card-text">
												<p>Access our archive of application notes to further your understanding of how to apply Teledyne LeCroy oscilloscopes, protocol analyzers and other test tools to your unique challenges. </p>
											</div>
										</div>
										<div class="inner-visible text-center mb-40">                                         <a class="btn btn-default" href="/support/techlib/library.aspx?type=1">Explore                         </a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
	</asp:Content>