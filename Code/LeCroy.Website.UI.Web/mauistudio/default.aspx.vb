﻿Public Class mauistudio_default
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.Title = "MAUI Studio - Teledyne LeCroy"
        Me.MetaDescription = "MAUI Studio Software Download"

        Master.MetaOgTitle = "MAUI Studio - Teledyne LeCroy"
        Master.MetaOgImage = "https://assets.lcry.net/images/maui-studio-laptop.png"
        Master.MetaOgDescription = "MAUI Studio Software Download"
        Master.MetaOgUrl = "https://teledynelecroy.com/mauistudio/"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub
End Class