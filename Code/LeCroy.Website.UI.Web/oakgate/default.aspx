<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.oakgate_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="400" valign="top">
                    <h1 style="padding-top:20px;font-size:22px;color:#0076c0;">Teledyne LeCroy Inc. announced that it has acquired OakGate Technology</h1>
                    <p style="font-size:16px;">Teledyne LeCroy has acquired OakGate Technology, the market leader in Validation and Performance Analysis for Solid State Storage with support for all popular storage protocols including PCI Express, SAS, SATA & Fibre Channel.</p>
                </td>
                <td width="325" valign="top"><a href="https://oakgatetech.com/"><img src="/images/oakgate-logo.png" border="0" style="margin:40px;" /></a></td>
                <td width="250">
                    <div class="subNav">
                        <ul>
                            <li><a href="/pressreleases/document.aspx?news_id=2092">Press Release</a></li>
                            <li><a href="https://oakgatetech.com/products/overview.html">Products</a></li>
                            <li><a href="https://oakgatetech.com/company/contact_us.html">Contact OakGate Technology</a></li>
                        </ul>
			        </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
       <h1 style="text-align:center;font-size:28pt;font-weight:400;">#1 Validation and Analytics Solution for SSDs</h1>
           <div style="background:rgba(30,136,229,0.6);float:left;width:50%;text-align:center; margin: 20px 0">
               <a href="https://www.oakgatetech.com/">
               <img style="width: 85%" src="https://www.oakgatetech.com/img/applications/analytics/enduro_front_screenshot_500.png" alt="OakRidge Technology Applications">
               <h2 style="color: white;font-size:16pt;font-weight:400;">Enduro with SVF Pro</h2>
                   <p style="color: white;font-size:12pt;font-weight:400; padding: 20px;">Enabling a complete validation solution of SSDs with robust and feature-rich software</p></a></div>
           <div style="background:rgba(142,36,170,0.6);float:left;width:50%;text-align:center;color: white; margin: 20px 0">
               <a href="https://www.oakgatetech.com/">
               <img style="width: 85%" src="https://www.oakgatetech.com/img/applications/analytics/analytics_green_screenshot_500.png" alt="OakRidge Technology Analytics">
                   <h2 style="color: white;font-size:16pt;font-weight:400;">Workload<i>Intelligence</i>�</h2>
                   <p style="color: white;font-size:12pt;font-weight:400; padding: 20px;">Providing deep insight into production workloads with Analytics and Replay software</p></a></div>
    </div>
</asp:Content>