﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.patents_default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; height: 100%;">
        <div style="padding-bottom: 12px; padding-left: 30px; padding-right: 30px; padding-top: 20px;">
            <h1>Teledyne LeCroy Patents Notice</h1>
            <p>See the patent notice document for the applicable patents for Teledyne LeCroy products</p>
            <p><a href="./teledyne_lecroy_hdo_patents_924940-4.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> HDO Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_labmaster_patents_923810-b.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> LabMaster Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_wavemaster_patents_924891-2.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WaveMaster Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_wavepro_patents_924890-2.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WavePro Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_wavepro_hd_patents_930183-2.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WavePro HD Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_wavepulser_sparq_patents_925144-3.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WavePulser and SPARQ Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_waverunner_patents_924499-3.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WaveRunner Patent Notice</a></p>
            <p><a href="./teledyne_lecroy_wavesurfer_patents_924636-2.pdf" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> WaveSurfer Patent Notice</a></p>
        </div>
    </div>
</asp:Content>