﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.pcieelectricaltest_default" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner-slide section-white section-angle-before angle-small small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>Complete PCI Express® Electrical Test Solutions</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page"> 
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-text">
										<p>Teledyne LeCroy's PCI Express electrical test solutions combine superior instruments with sophisticated software for:</p>
										<ul class="list-dots">
											<li>Automated Tx, Rx and LEQ testing up to PCIe 5.0 32 GT/s</li>
											<li>Visibility from physical layer through protocol operations</li>
											<li>Expanded capabilities to support PAM4 signaling for PCIe 6.0 64 GT/s</li>
										</ul>
									</div>
									<div class="page-button"><a class="btn btn-default btn-blue-outline" href="/doc/pcie-test-solutions-brochure">Brochure</a><span class="btn btn-default datasheet-dropdown-menu btn-blue-outline">Datasheet
											<ul class="datasheet-dropdown">
												<li><a href="/doc/pcie5-test-solution-datasheet">QPHY-PCIE5-TX-RX Datasheet</a></li>
												<li><a href="/doc/qphy-pcie4-tx-rx-ds">QPHY-PCIE4-TX-RX Datasheet</a></li>
											</ul></span><a class="btn btn-default btn-blue-outline" href="#videos">Videos</a><a class="btn btn-default btn-blue-outline" href="#webinar">Webinars</a></div>
									<div class="page-video">
										<div class="row">
											
											<div class="col-md-8">
												<div class="visual"><span class="wistia_embed wistia_async_9h464x50vi wistia_embed_initialized popover=true popoverAnimateThumbnail=true" id="wistia-9h464x50vi-1" style="display:inline-block;height:183px;position:relative;width:325px">
														<div class="wistia_click_to_play" id="wistia_44.thumb_container" style="position: relative; height: 183px; width: 325px;">
															<div id="wistia_190.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 183px; overflow: hidden; outline: none; position: relative; width: 325px;">
																<div id="wistia_205.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 133px; top: 73px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
																<div id="wistia_205.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 133px; top: 73px;"></div><img id="wistia_190.thumbnail_img" alt="Introduction to PCI Express Testing" src="https://embedwistia-a.akamaihd.net/deliveries/2e6576040369897fd4e5d73a85116017.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 183px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 325px; left: 0px; top: 0px;">
															</div>
														</div></span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6"><img src="https://assets.lcry.net/images/pcie-electrical-1.png" alt="img-description"></div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery20" href="https://assets.lcry.net/images/pcie-electrical-2.png"><img src="https://assets.lcry.net/images/pcie-electrical-2.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Superior PCIe Test Solutions</h3>
										</div>
										<ul class="list-dots">
											<li>Approved PCI-SIG gold suite solution for all PCIe electrical compliance tests</li>
											<li>High accuracy and repeatability due to superior signal quality</li>
											<li>Fastest receiver test calibration</li>
											<li>Complete DUT automation</li>
										</ul>
									</div>
									<div class="inner-visible p-40 pt-0 text-center"><a class="btn btn-default" href="#one">Explore</a></div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery21" href="https://assets.lcry.net/images/pcie-electrical-3.png"><img src="https://assets.lcry.net/images/pcie-electrical-3.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Visibility from Physical Layer Through Protocol Operations</h3>
										</div>
										<ul class="list-dots">
											<li>LTSSM logging and state-machine triggering</li>
											<li>ProtoSync integrates industry-standard protocol display and physical-layer analysis</li>
											<li>Go directly from Link Equalization compliance tests to deep debug</li>
										</ul>
									</div>
									<div class="inner-visible p-40 pt-0 text-center"><a class="btn btn-default" href="#two">Explore</a></div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery22" href="https://assets.lcry.net/images/pcie-electrical-4.png"><img src="https://assets.lcry.net/images/pcie-electrical-4.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Versatile and "Future Proof"</h3>
										</div>
										<ul class="list-dots">
											<li>Fully automated PCIe 5.0 electrical compliance test solution</li>
											<li>Instruments are PAM4-capable for investment protection into PCIe 6.0 development</li>
											<li>One set of instruments for all PCIe electrical Tx/Rx testing</li>
										</ul>
									</div>
									<div class="inner-visible p-40 pt-0 text-center"><a class="btn btn-default" href="#three">Explore</a></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="videos">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Videos </h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_awdr4t1sca wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-awdr4t1sca-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_57.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_126.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_142.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_142.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_126.thumbnail_img" alt="Fastest PCIe Receiver Test Calibration" src="https://embed-fastly.wistia.com/deliveries/bb13e14f76eccaaf0b71970e70dbe021.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_be67dze7ak wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-be67dze7ak-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_93.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_217.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_232.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_232.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_217.thumbnail_img" alt="PCIe Link Equalization Testing" src="https://embed-fastly.wistia.com/deliveries/5db643b6700693685b4544e1513c755f.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_wg2qton0b6 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-wg2qton0b6-1" style="display:inline-block;height:100%;position:relative;width:100%">
													<div class="wistia_click_to_play" id="wistia_181.thumb_container" style="position: relative; height: 195.094px; width: 346.859px;">
														<div id="wistia_270.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 195.094px; overflow: hidden; outline: none; position: relative; width: 346.844px;">
															<div id="wistia_285.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 143px; top: 79px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_285.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 143px; top: 79px;"></div><img id="wistia_270.thumbnail_img" alt="PCIe Transmitter Characterization and Compliance Testing" src="https://embed-fastly.wistia.com/deliveries/c2e52e33f82f9994476f48245109addb.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 195.094px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 346.844px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="one">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-title">
										<h2>Superior PCIe Electrical Test Solutions</h2>
									</div>
									<div class="page-text">
										<p>Teledyne LeCroy's LabMaster 10 Zi-A oscilloscopes and Anritsu’s Signal Quality Analyzer-R (SQA-R) MP1900A are certified for all PCIe compliance tests. Superior electrical performance and tighter integration mean the best repeatability, with faster test times.</p>
									</div>
								</div>
							</div>
							<div class="col-md-6"><a data-fancybox="gallery" href="https://assets.lcry.net/images/pcie-electrical-5.png"><img src="https://assets.lcry.net/images/pcie-electrical-5.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Approved solution for all PCI-SIG electrical compliance tests</h3>
										</div>
										<div class="card-content">
											<p>LabMaster 10Zi-A and the SQA-R MP1900A are approved for all official Gen1, Gen2, Gen3, Gen4, and Gen5 PCI-SIG electrical tests:</p>
											<ul class="list-dots">
												<li>PCIe 3.0 Receiver test</li>
												<li>PCIe 3.0 Transmitter tests</li>
												<li>PCIe 3.0 Link Equalization tests</li>
												<li>PCIe 4.0 Receiver test</li>
												<li>PCIe 4.0 Transmitter tests</li>
												<li>PCIe 4.0 Link Equalization tests</li>
												<li>PCIe 5.0 Receiver tests</li>
												<li>PCIe 5.0 Transmitter tests</li>
												<li>PCIe 5.0 Link Equalization tests</li>
												<li>PCIe 5.0 Reference Clock Jitter Test</li>
												<li>PLL Bandwidth test</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">
										<div class="card-title">                                      
											<h3>Highest accuracy and repeatability</h3>
										</div>
										<div class="card-content">
											<p> Teledyne LeCroy QualiPHY software for PCIe 3.0, 4.0 and 5.0 fully automates the oscilloscope and MP1900A for calibration and testing. This simplifies complex processes and eliminates confusion and setup errors.</p><p>With time and temperature jitter drift of less than +/- 2% (2 days, +/- 5 deg), the MP1900A has the highest signal quality for the most repeatable results.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">
										<div class="card-title">                                      
											<h3>Fastest receiver test calibration</h3>
										</div>
										<div class="card-content">
											<p>A typical PCIe receiver test calibration requires hundreds of SigTest analyses. Competitive scopes only have 4- or 8-core processors with 16 or 32 GB of RAM. The LabMaster 10Zi-A has a 20-core processor and up to 192 GB of RAM more parallel analysis means faster receiver test calibration. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="two">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-title">
										<h2>Visibility from Electrical Through Transaction Layer</h2>
									</div>
									<div class="page-text">
										<p>Automated characterization and compliance testing are vital to bringing a product to market, but when failures occur, it falls to the rest of the instruments’ toolbox to help locate and debug problems. Teledyne LeCroy and Anritsu provide visibility into the most layers of the PCI Express interface, to get to the root-cause of issues faster.</p>
									</div>
								</div>
							</div>
							<div class="col-md-6"><a data-fancybox="gallery2" href="https://assets.lcry.net/images/pcie-electrical-6.png"><img src="https://assets.lcry.net/images/pcie-electrical-6.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>LTSSM logging and state-machine triggering</h3>
										</div>
										<div class="card-content">
											<p>  After a training operation, the MP1900A displays the actual Training State transition logs, so the state transition path (route) and transition times can be analyzed in detail.</p><p>State transitions can be selected as the oscilloscope acquisition trigger, allowing the link training operation to be analyzed in depth using ProtoSync on the oscilloscope.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Integrated protocol and physical-layer analysis</h3>
										</div>
										<div class="card-content">
											<p> PCIEBus Decode and ProtoSync protocol analysis software provide views of the complete protocol stack, from physical layer to transaction layer. Physical layer and protocol views are linked directly to each other at all times touch a packet on either software interface, and the oscilloscope will highlight and zoom to that packet in both views.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Debug During LEQ Testing</h3>
										</div>
										<div class="card-content">
											<p>QualiPHY PCIe software leverages Teledyne LeCroy’s ProtoSync, allowing you to switch seamlessly from electrical to protocol views during the LEQ tests—a feature unique to our solutions. If a failure or issue is detected, the oscilloscope, BERT and Device Under Test are already configured to provide unmatched visibility into the entire link training operation.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="three">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-title">
										<h2>Versatile and "Future Proof" Instrumentation</h2>
									</div>
									<div class="page-text">
										<p>One set of test equipment covers all generations of PCIe electrical Tx/Rx testing, including PCIe 6.0 with PAM4 signaling. The combination of Teledyne LeCroy LabMaster 10 Zi-A and Anritsu SQA-R MP1900A provides versatility in the lab.</p>
									</div>
								</div>
							</div>
							<div class="col-md-6 mb-3"><a data-fancybox="gallery3" href="https://assets.lcry.net/images/pcie-electrical-7.png"><img src="https://assets.lcry.net/images/pcie-electrical-7.png" alt="img-description"></a></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">     
										<div class="card-title">                                      
											<h3>PCIe 5.0 Tx/Rx compliance testing</h3>
										</div>
										<div class="card-content">
											<p><a href="/options/productseries.aspx?mseries=588&amp;groupid=140">QPHY-PCIE5-TX-RX</a> is a fully automated PCIe 5.0 test solution that meets all the CEM PHY specification requirements for both transmitter and receiver compliance testing.<a href="/options/productseries.aspx?mseries=588&amp;groupid=140">QPHY-PCIE5-TX-RX</a> also has a set of base specification transmitter and receiver measurements for characterizing and testing PCIe 5.0 devices.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">  
										<div class="card-title">
											<h3>One set of equipment for all PCIe testing</h3>
										</div>
										<div class="card-content">
											<p>The following instrument configuration is fully capable of PCIe 5.0 transmitter and receiver testing:</p>
											<ul class="list-dots mb-2">
												<li>MP1900A with 32Gb/s capability</li>
												<li>LabMaster 10-50Zi-A 50 GHz oscilloscope</li>
												<li>QPHY-PCIE5-TX-RX software for transmitter characterization and receiver test calibration for PCIe 5.0, 4.0, 3.0, 2.0, and 1.0.</li>
											</ul>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-content h-auto">     
										<div class="card-title">                                      
											<h3>PAM4 capable for PCIe 6.0 and beyond</h3>
										</div>
										<div class="card-content">
											<p>  Many future serial data technologies are expected to leverage multi-level signaling to increase data throughput without increasing analog bandwidth requirements.</p><p>PCI Express 6.0 will be based on 32 Gbaud PAM4 – Teledyne LeCroy and Anritsu are fully prepared for this new test challenge. The LabMaster 10 Zi-A has PAM4 analysis capability to 64 Gbaud and beyond. The MP1900A has PAM4 PPG modules up to 64&nbsp;Gbaud and PAM4 error detection to 58&nbsp;Gbaud.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row mb-4">
							<div class="col-md-12">
								<div class="page-content">
									<div class="page-title">
										<h2>Experience, Leadership, Support</h2>
									</div>
									<div class="page-text">
										<p>
											Combining the Teledyne LeCroy LabMaster 10 Zi-A oscilloscope and QualiPHY automated compliance test software with the Anritsu SQA-R MP1900A BERT results in the most powerful characterization, compliance and debug solution available. First-line support for the solution is available directly from the industry’s most experienced PCI Express field support team at Teledyne LeCroy.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery10" href="https://assets.lcry.net/images/pcie-electrical-8.png"><img src="https://assets.lcry.net/images/pcie-electrical-8.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>LabMaster 10 Zi-A</h3>
										</div>
										<div class="card-content">
											<p> <a href="/oscilloscope/labmaster-10-zi-a-oscilloscopes">LabMaster 10 Zi-A oscilloscopes </a>a platform for all generations of PCIe testing, at all required bandwidths:</p>
											<ul class="list-dots mb-2">
												<li>50 GHz for PCI Express 5.0</li>
												<li>25 GHz for PCI Express 4.0</li>
												<li>13 GHz for PCI Express 3.0</li>
											</ul>
											<p>
												     The LabMaster 10 Zi-A has a 20-core processor and up to 192 GB of RAM for significantly faster receiver test calibration and transmitter testing.   </p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery11" href="https://assets.lcry.net/images/pcie-electrical-9.png"><img src="https://assets.lcry.net/images/pcie-electrical-9.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>QualiPHY</h3>
										</div>
										<div class="card-content">
											<p>
												 QualiPHY software options <a href="/options/productseries.aspx?mseries=356&amp;groupid=175"> QPHY-PCIE3-TX-RX</a>, <a href="/options/productseries.aspx?mseries=563&amp;groupid=140"> QPHY-PCIE4-TX-RX</a>, and <a href="/options/productseries.aspx?mseries=588&amp;groupid=140"> QPHY-PCIE5-TX-RX </a>fully automate all aspects of PCIe compliance testing:</p>
											<ul class="list-dots mb-2">
												<li>Transmitter testing</li>
												<li>Receiver test calibration and execution</li>
												<li>Link Equalization testing</li>
												<li>PLL Bandwidth testing</li>
												<li>Report generation</li>
											</ul>
											<p>Teledyne LeCroy provides full application support for all integrated electrical test solutions.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery12" href="https://assets.lcry.net/images/pcie-electrical-10.png"><img src="https://assets.lcry.net/images/pcie-electrical-10.png" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>Anritsu SQA-R MP1900A</h3>
										</div>
										<div class="card-content">
											<p>
												 The <a href="https://info.goanritsu.com/l/492921/2020-01-20/b6pdn"> Anritsu SQA-R MP1900A is </a>a high-speed BERT solution. In one box, it combines:</p>
											<ul class="list-dots mb-2">
												<li>Jitter and noise generation</li>
												<li>High quality waveform PPG with integrated 10-tap emphasis</li>
												<li>High input sensitivity ED with clock recovery</li>
												<li>Variable CTLE and link negotiation functions required for PCIe Rx tests</li>
											</ul>
											<p>With time and temperature jitter drift of less than +/- 2% (2 days, +/- 5 deg), the MP1900A has the highest signal quality for the most repeatable results.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="webinar">
					<div class="container">
						<div class="section-heading text-center">
							<h2>On-Demand Webinars</h2>
						</div>
						<div class="section-content">
							<div class="block-view view-list">
								<div class="tabs-content">
									<div class="row mb-24">
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2020-04-16/7xtd3q">PCI Express® 5.0 and 4.0 Electrical Testing</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2020-04-16/7xtd3q">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2021-02-05/84pf3s">PCI Express® 5.0 Compliance Test Overview Part 1: Protocol</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2021-02-05/84pf3s">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2021-02-05/84plg8">PCI Express® 5.0 Compliance Test Overview Part 2: Physical Layer</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2021-02-05/84plg8">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-07-01/7qxs4d">PCI Express 4.0 Compliance Test Overview Part 1: Protocol Testing</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-07-01/7qxs4d">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-07-01/7qxs58">PCI Express 4.0 Compliance Test Overview Part 2: Physical-Layer Testing</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-07-01/7qxs58">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2021-02-23/8512dg">Debug PCIe® Faster with New Protocol and Physical Layer Test</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2021-02-23/8512dg">Watch Now</a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2021-04-22/86n1wj">How to Debug PCI Express® Power Management and Dynamic Link</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2021-04-22/86n1wj">Watch Now</a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
