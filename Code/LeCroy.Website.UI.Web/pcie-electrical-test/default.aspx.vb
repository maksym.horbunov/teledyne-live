﻿Public Class pcieelectricaltest_default
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.Title = "Teledyne LeCroy - PCI Express Electrical Test Solutions"
        Me.MetaDescription = "PCI Express Electrical Test Solutions"

        Master.MetaOgTitle = "PCI Express Electrical Test Solutions"
        Master.MetaOgImage = "https://assets.lcry.net/images/maui-studio-laptop.png"
        Master.MetaOgDescription = "PCI Express Electrical Test Solutions"
        Master.MetaOgUrl = "https://assets.lcry.net/images/pcie-electrical-01.png"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub
End Class