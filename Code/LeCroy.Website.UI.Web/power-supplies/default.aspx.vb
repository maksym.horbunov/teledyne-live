Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class powersupplies_default
    Inherits BasePage
    Dim strSQL As String = ""
    Public mseries As String = ""
    Dim standardid As String = ""
    Dim firstmenuid As String = ""
    Dim dataSheetURL As String = ""
    Dim specificationURL As String = ""
    Dim promoImages As String = ""
    Dim seriesName As String = ""
    Dim seriesExplorerText As String = ""
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Dim hasOptions As Boolean = False
    Dim menuURL As String = ""
    Dim tabsToShow As StringBuilder = New StringBuilder()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Power Supplies"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim imageIndex As Integer = 0
        Dim captionID As String
        Dim menuID As String = "1155"
        '** caption_id
        captionID = AppConstants.PRODUCT_CAPTION_ID

        '** mseries	
        mseries = 575
        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), mseries, 37)) Then
            Response.Redirect("~/")
        End If

        Session("cataid") = 37
        Session("menuSelected") = captionID
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        If Not Me.IsPostBack Then
            getProductLine()
            getSeriesOverview()
            getSpecs()
            getOptions()
            If Not Session("CountryCode") Is Nothing Then
                If Len(Session("CountryCode").ToString) > 0 Then
                    If Session("CountryCode").ToString = "208" Then
                        getListPrice()
                    End If
                End If
            End If
            lblTabsToShow.Text = tabsToShow.ToString()

            If hasDetail Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
            ElseIf hasOverview Then
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
            Else
                lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
            End If

            Functions.CreateMetaTags(Me.Header, mseries, AppConstants.SERIES_OVERVIEW_TYPE)

            strSQL = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_ID=@PRODUCTSERIESID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                seriesName = dr("NAME").ToString()
                dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
                specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
                promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
                seriesExplorerText = Me.convertToString(ds.Tables(0).Rows(0)("SHORT_NAME"))
                lbSeriesTileTop.Text = seriesName
                lbSeriesTileBelow.Text = "Explore " + seriesExplorerText + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + seriesExplorerText + "' >"
                lbSeriesImage.Text = "<img src='" + rootDir + ds.Tables(0).Rows(0)("TITLE_IMAGE").ToString() + "' width='260' />"
                lbSeriesDesc.Text = ds.Tables(0).Rows(0)("DESCRIPTION").ToString()
                Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - T3PS Series - Power Supplies"
            End If
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - T3PS Series - Power Supplies"
            If Not promoImages = Nothing And Len(promoImages) > 0 Then
                lbPromotion.Text = promoImages
            End If
            'Additional Resources
            If Len(lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString).ToString) > 0 Then
                lblAdditionalResources.Text = Functions.GetOverviewHTML(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString, mseries.ToString)
            End If
            lb_leftmenu.Text = Functions.LeftMenu(captionID, rootDir, pn_leftmenu, menuID)
            pn_leftmenu.Visible = False
        End If
    End Sub

    Private Sub getProductDetail()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            If Len(dr("OVER_VIEW_DETAILS").ToString()) > 0 Then
                Me.lblProductDetail.Text = dr("OVER_VIEW_DETAILS").ToString()
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub
    Private Sub getSpecs()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM [CMS_OVERVIEW] where ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.PROTOCOL_SPECS))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblSpecs.Text = dr("OVER_VIEW_DETAILS").ToString()
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
    End Sub

    Private Sub getSeriesOverview()
        strSQL = "SELECT OVER_VIEW_DETAILS FROM CMS_OVERVIEW WHERE ID = @ID AND OVERVIEW_TYPE_ID=@OVERVIEWTYPEID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@ID", mseries))
        sqlParameters.Add(New SqlParameter("@OVERVIEWTYPEID", AppConstants.SERIES_OVERVIEW_TYPE))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Me.lblOverview.Text = dr("OVER_VIEW_DETAILS").ToString()
            hasOverview = True
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If

    End Sub

    Private Sub getOptions()
        Dim sb As StringBuilder = New StringBuilder
        Dim sqlString As String = "SELECT DISTINCT b.group_id, b.GROUP_NAME,b.SORTID FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND b.CATEGORY_ID = @CATEGORYID AND c.disabled='n' AND e.disabled='n' ORDER BY b.SORTID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        sqlParameters.Add(New SqlParameter("@CATEGORYID", 37))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count > 0) Then
            For Each dr As DataRow In ds.Tables(0).Rows
                sb.AppendFormat("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='cell'><h3>{0}</h3></td></tr>", dr("GROUP_NAME").ToString())
                Dim groupDetailsSqlString As String = "SELECT DISTINCT c.productid, c.partnumber, c.productdescription,c.sort_id,c.INTRODUCTION,b.CATEGORY_ID,b.GROUP_ID, f.PRODUCT_SERIES_ID FROM CONFIG a INNER JOIN PRODUCT c ON a.OPTIONID = c.PRODUCTID INNER JOIN PRODUCT_GROUP b ON c.GROUP_ID = b.GROUP_ID INNER JOIN PRODUCT_SERIES_CATEGORY d ON a.PRODUCTID = d.PRODUCT_ID INNER JOIN PRODUCT e ON e.PRODUCTID = d.PRODUCT_ID LEFT OUTER JOIN PRODUCT_SERIES_CATEGORY AS f ON b.CATEGORY_ID = f.CATEGORY_ID AND c.PRODUCTID = f.PRODUCT_ID WHERE d.PRODUCT_SERIES_ID = @PRODUCTSERIESID and b.group_id = @GROUPID AND c.disabled='n' Order by c.sort_id"
                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
                sqlParameters.Add(New SqlParameter("@GROUPID", dr("GROUP_ID").ToString()))
                Dim groupDetails As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), groupDetailsSqlString, sqlParameters.ToArray())
                If (groupDetails.Tables.Count > 0) Then
                    For Each groupDr As DataRow In groupDetails.Tables(0).Rows
                        sb.AppendFormat("<tr><td class='cell'><strong><u><a href='{0}'>{1}</a></u></strong><br />{2}</td></tr>", String.Format("detail.aspx?modelid={0}", groupDr("PRODUCTID").ToString()), groupDr("PARTNUMBER").ToString(), Functions.GetProdDescriptionOnProdID(groupDr("PRODUCTID").ToString()).ToString())
                    Next
                End If
            Next
            If (ds.Tables(0).Rows.Count > 0) Then sb.Append("</table>")
        End If
        If (sb.ToString().Length > 0) Then
            tabsToShow.AppendFormat("<li><a href=""#options""><img src='{0}/images/tabs/tabs_options_off.gif' id='options' border='0' /></a></li>", rootDir)
            lblOptions.Text = sb.ToString()
        End If
    End Sub

    Private Sub getProductLine()
        '*left menu for series
        Dim sqlGetSeries As String
        sqlGetSeries = "SELECT * FROM PRODUCT_SERIES WHERE PRODUCT_SERIES_id = @PRODUCTSERIESID"
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables(0).Rows.Count > 0) Then
            seriesName = Me.convertToString(ds.Tables(0).Rows(0)("NAME"))
            dataSheetURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_DATASHEET"))
            specificationURL = Me.convertToString(ds.Tables(0).Rows(0)("URL_SPEC"))
            promoImages = Functions.GetPromoImagesForSeries(mseries, rootDir)
        End If

        If Not promoImages = Nothing And Len(promoImages) > 0 Then
            lbPromotion.Text = promoImages
        End If

        '*left menu for products
        Dim i As Integer
        Dim products As String = ""
        Dim productLine As String = ""

        strSQL = "SELECT DISTINCT a.*" +
                         " FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE " +
                         " a.PRODUCTID = c.PRODUCT_ID " +
                         " AND b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID AND c.CATEGORY_ID=@CATEGORYID AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND a.disabled='n' order by a.sort_id"
        sqlParameters = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@CATEGORYID", 37))
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
        Dim ds1 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        If ds1.Tables(0).Rows.Count Then
            productLine += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>"
            For Each dr As DataRow In ds1.Tables(0).Rows
                Dim modelID As String = ds.Tables(0).Rows(i)("NAME").ToString()
                productLine = productLine + String.Format("<tr><td class='cell'><strong><u><a href='detail.aspx?modelid={0}'>{1}</a></u></strong><br />{2}</td></tr>", dr("PRODUCTID").ToString(), dr("PARTNUMBER").ToString(), Functions.GetProdDescriptionOnProdID(dr("PRODUCTID").ToString()).ToString())
            Next
            productLine = productLine + "</table> "
            lbProduct1.Text = products
            lbQuickSpecs.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub
    Private Sub getListPrice()
        Dim pNum As Integer
        Dim j As Integer
        Dim strListPrice As String = ""

        If Functions.hasListPrice(mseries) Then
            strSQL = " SELECT distinct PRODUCT.PRODUCTID, PRODUCT.PARTNUMBER,PRODUCT.SORT_ID FROM  PRODUCT_SERIES_CATEGORY INNER JOIN  PRODUCT ON PRODUCT_SERIES_CATEGORY.PRODUCT_ID = PRODUCT.PRODUCTID " +
                     " WHERE PRODUCT.DISABLED = 'n' AND PRODUCT_SERIES_CATEGORY.PRODUCT_SERIES_ID = @PRODUCTSERIESID ORDER BY PRODUCT.SORT_ID"
            Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
            sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", mseries))
            Dim DsList As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
            pNum = DsList.Tables(0).Rows.Count()
            If pNum > 0 Then

                strListPrice = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>"
                strListPrice += "<tr>"
                strListPrice += "<td width='96' align='left' valign='top' class='title2'>Product</td>"
                strListPrice += "<td width='61' align='left' valign='top' class='title2'>Price*</td>"
                strListPrice += "<td width='176' align='left' valign='top' class='title2'>Description</td>"
                strListPrice += "<td width='115' align='left' valign='top'></td>"
                strListPrice += "</tr>"
                For j = 0 To pNum - 1
                    Dim dr As DataRow = DsList.Tables(0).Rows(j)
                    strListPrice += "<tr>"
                    strListPrice += "<td width='96' align='left' valign='top' class='title2'>" + dr("PARTNUMBER").ToString + "</td>"
                    strListPrice += "<td width='61' align='left' valign='top' class='cell'>" + FormatCurrency(Functions.GetProductListPriceonProductID(dr("PRODUCTID")).ToString(), 2) + "</td>"
                    strListPrice += "<td width='176' align='left' valign='top' class='cell'>" + Functions.GetProdDescriptionOnProdID(dr("productid").ToString).ToString + "</td>"
                    strListPrice += "<td width='115' align='left' valign='top'><a href='/oscilloscope/configure/configure_step2.aspx?seriesid=" + mseries.ToString + "&modelid=" + dr("PRODUCTID").ToString() + "'><img src='/images/icons/icons_link.gif' align='absmiddle' border='0' />Request Quote</a></td>"
                    strListPrice += "</tr>"
                Next
                strListPrice += "</table>"
                strListPrice += "<p class='smaller'>* Prices Shown Are US Domestic Prices, MSRP, and are exclusive of <strong>local and state taxes, shipping, levies, fees, duties, exportation/importation costs, service and warranties outside the United States</strong>, and assume standard 30 day payment terms.</p>"
                strListPrice += "<p class='smaller'>Information and/or pricing may be changed or updated without notice. Teledyne LeCroy may also make improvements and/or changes in the products and/or the programs, as well as associated pricing, described in this information at any time without notice.</p>"
                strListPrice += "<p class='smaller'>Because international information is provided at this Web Site, not all products, programs or pricing mentioned will be available in your country. Please contact your local sales representative for information as to products and services available in your country.</p>"

                tabsToShow.Append("<li><a href='#listprice'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_list_off.gif' id='listprice' border='0'></a></li>")
                Me.lblListPrice.Text = strListPrice

            End If
        End If
    End Sub
End Class