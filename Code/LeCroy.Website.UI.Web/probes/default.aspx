﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.probes_default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
			    <td valign="top" width="370">
                    <h1 style="color:#0076c0;font-family:Verdana, Helvetica, Sans;font-weight:400;font-size:28px;padding-top:25px;">Oscilloscope Probes</h1>
                    <h2 style="color:#000;font-family:Verdana, Helvetica,Sans;font-weight:bold;font-size:14px">Oscilloscope Probes and Probe Accessories</h2>
				    <p>Teledyne LeCroy has a wide variety of world class probes and amplifiers to complement its product line. From the ZS high impedance active probes to the WaveLink differential probing system which offers bandwidths up to 25 GHz, Teledyne LeCroy probes and probe accessories provide optimum mechanical connections for signal measurement.</p>
                    <p><a href="/doc/oscilloscope-probe-catalog" ><img src="/images/probe_catalog_cover.png" border="0" hspace="10px" style="vertical-align:middle" />Oscilloscope Probes and Probe Accessories Catalog</a></p>
                    <h3 style="color:#0076c0;font-size:12px;">Explore Oscilloscope Probes<img src="/images/icons/icon_small_arrow_down.gif" border="0" width="11" height="11" hspace="10px" /></h3>
                </td>
			    <td valign="top" width="248"><img src="/images/probe-category.png" border="0" height="278" width="242" /></td>
            </tr>
        </table>
    </div>
    <div style="margin: 0 auto;">
        <div class="products-table">
            <div class="sub-content">
                <asp:Repeater ID="rptProduct" runat="server">
            <HeaderTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="910">
                    <tr id="trHeaders" runat="server">
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <td class="spacer last"></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr id="trRowData" runat="server" class="sasr">
                        <td class="grey">
                            <div style="text-align:left;">
                                <div style="float:left;display:inline-block;padding:5px;width:125px;">
                                    <asp:Image ID="imgProduct" runat="server" Height="64" Width="115" />
                                </div>
                                <div style="float:left;display:inline-block;padding:5px;width:200px;">
                                    <asp:HyperLink ID="hlkProduct" runat="server" CssClass="products" />
                                </div>
                                <div style="float:left;display:inline-block;padding:5px;width:520px;">
                                    <asp:Literal ID="litDescription" runat="server" />
                                </div>
                            </div>
                        </td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr id="trAlternateRowData" runat="server" class="sasr">
                        <td>
                            <div style="text-align:left;">
                                <div style="float:left;display:inline-block;padding:5px;width:125px;">
                                    <asp:Image ID="imgProduct" runat="server" Height="64" Width="115" />
                                </div>
                                <div style="float:left;display:inline-block;padding:5px;width:200px;">
                                    <asp:HyperLink ID="hlkProduct" runat="server" CssClass="products" />
                                </div>
                                <div style="float:left;display:inline-block;padding:5px;width:520px;">
                                    <asp:Literal ID="litDescription" runat="server" />
                                </div>
                            </div>
                        </td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>  
            </FooterTemplate>
        </asp:Repeater>
            </div>
        </div>
	    <div class="clear"></div>
    </div>
</asp:Content>