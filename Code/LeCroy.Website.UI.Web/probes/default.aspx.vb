﻿Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.DTOs

Public Class probes_default
    Inherits BasePage

#Region "Variables/Keys"
    Private _seriesProperties As List(Of SeriesProperty) = New List(Of SeriesProperty)
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscope Probes"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Oscilloscope Probes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindSeries()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptProduct_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptProduct.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ProductSeries = CType(e.Item.DataItem, ProductSeries)
                Dim imgProduct As Image = CType(e.Item.FindControl("imgProduct"), Image)
                Dim hlkProduct As HyperLink = CType(e.Item.FindControl("hlkProduct"), HyperLink)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)

                imgProduct.ImageUrl = String.Format("{0}{1}", ConfigurationManager.AppSettings("RootDir"), row.ProductImage)
                imgProduct.AlternateText = row.Name
                If Not (String.IsNullOrEmpty(row.SeoValue)) Then
                    hlkProduct.NavigateUrl = String.Format("~/probes/{0}{1}", row.SeoValue, IIf(row.SeoValue.Contains("."), "/", String.Empty))
                Else
                    hlkProduct.NavigateUrl = String.Format("probeseries.aspx?mseries={0}", row.ProductSeriesId)
                End If
                hlkProduct.Text = row.Name
                litDescription.Text = row.Description
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindSeries()
        Dim series As List(Of ProductSeries) = ProductSeriesRepository.GetActiveProductSeriesForCategoryId(ConfigurationManager.AppSettings("ConnectionString").ToString(), 3, New List(Of Int32)())
        If (series.Count > 0) Then
            rptProduct.DataSource = AdjustSeriesSortIds(series).OrderBy(Function(x) x.SortId.Value)
            rptProduct.DataBind()
        End If
    End Sub

    Private Function AdjustSeriesSortIds(ByVal productSeries As List(Of ProductSeries)) As List(Of ProductSeries)
        Dim retVal As List(Of ProductSeries) = New List(Of ProductSeries)
        For Each ps As ProductSeries In productSeries
            Dim newPs As ProductSeries = ps
            If Not (ps.SortId.HasValue) Then
                newPs.SortId = 99
            End If
            retVal.Add(newPs)
        Next
        Return retVal
    End Function
#End Region
End Class