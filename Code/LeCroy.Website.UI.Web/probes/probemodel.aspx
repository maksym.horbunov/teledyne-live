﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="probemodel.aspx.vb" Inherits="LeCroy.Website.probes_probemodel" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server"><asp:Literal ID="menulabel" runat="server" /></asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tr>
            <td valign="top">
                <div id="contentMiddle">
                    <asp:Label ID="lbModelImage" runat="server" />
                    <div class="content2">
                        <h1><asp:Literal ID="landTitle" runat="server" /></h1>
                        <p><asp:Literal ID="landContent" runat="server" /></p>
                    </div>
                    <br />
                    <div class="tabs">
                        <div class="bg">
                            <ul class="tabNavigation"><asp:Literal ID="lblTabsToShow" runat="server" /></ul>
                            <div id="product_line"><asp:Label ID="lb_productLine" runat="server" /></div>
                            <div id="overview"><asp:Label ID="lb_overview" runat="server" /></div>
                            <div id="product_details"><asp:Label ID="lblProductDetail" runat="server" /></div>
                            <div id="spec"><asp:Label ID="lblSpecs" runat="server" /></div>
                            <div id="accessories">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td valign="top">
                                            <h3><asp:Literal ID="litModelName" runat="server" /></h3>
                                            <asp:DataList ID="dl_accessories" runat="server" RepeatColumns="1" RepeatDirection="Horizontal" Style="border: 1px solid #dedede;" GridLines="Both" BorderStyle="Solid">
                                                <ItemTemplate>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top" align="left" width="174">
                                                                <a href="<%= ConfigurationManager.AppSettings("AwsPublicBaseUrl") %><%#Eval("FILEFULLPATH").ToString().ToLower()%>" target="_blank">
                                                                    <img src="<%=rootDir %><%#Eval("IMAGE").ToString()%>" border="0" width="174" height="97"></a>
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <a href="<%= ConfigurationManager.AppSettings("AwsPublicBaseUrl") %><%#Eval("FILEFULLPATH").ToString().ToLower()%>" target="_blank">
                                                                    <%# Eval("TITLE")%></a><br />
                                                                <%# Eval("DESCRIPTION")%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="compatability">
                                <table width="450" border="0" cellspacing="0" cellpadding="0" class="selectionGuide">
                                    <tr>
                                        <td valign="top" width="450">
                                            <asp:DataList ID="dl_series" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Style="border: 1px solid #dedede;" GridLines="Both" BorderStyle="Solid">
                                                <ItemTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="item" valign="top" id="1" align="center">
                                                                <div id="info">
                                                                    <asp:Literal ID="litName" runat="server" /><br /><br />
                                                                    <asp:HyperLink ID="hlkLink" runat="server" CssClass="info" Target="_blank">
                                                                        <asp:Image ID="imgProductImage" runat="server" BorderWidth="0" /><span><asp:Literal ID="litProductImageDescription" runat="server" /></span>
                                                                    </asp:HyperLink><br />&nbsp;&nbsp;
                                                                    <asp:HyperLink ID="hlkLink2" runat="server" Target="_blank">
                                                                        <img id="imgProductImage2" runat="server" alt="Learn More" border="0" width="74" height="15" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="litPromotion" runat="server" />
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="3" ShowBuy="false" ShowRequestDemo="true" ShowRequestQuote="false" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>