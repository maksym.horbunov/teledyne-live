﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL
Imports LeCroy.Library.VBUtilities

Public Class probes_probemodel
    Inherits BasePage

#Region "Variables/Keys"
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Dim hasOverview As Boolean = False
    Dim hasDetail As Boolean = False
    Public menuURL As String = String.Empty
    Private Const captionId As Int32 = 102
    Private Const categoryId As Int32 = 3
    Private Const menuId As Int32 = 508
    Private _productSeriesId As Int32 = 0
    Private _productId As Int32 = 0
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscope Probes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        HandleRoute()
        ValidateProductSeriesIdQueryString()
        ValidateProductIdQueryString()

        If Not (FeProductManager.CanProductShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId, CategoryIds.PROBES, _productSeriesId)) Then
            Response.Redirect("~/")
        End If

        If Not (IsPostBack) Then
            Session("cataid") = categoryId
            Session("SeriesID") = _productSeriesId
            Session("menuSelected") = captionId
            menuURL = String.Format("&capid={0}&mid={1}", captionId, menuId)
            BindPage()
            ucResources.CategoryId = categoryId
            ucResources.ProductSeriesId = _productSeriesId
            ucResources.ProductId = _productId
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub dl_series_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dl_series.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim row As ProductSeries = CType(e.Item.DataItem, ProductSeries)
            Dim litName As Literal = CType(e.Item.FindControl("litName"), Literal)
            Dim hlkLink As HyperLink = CType(e.Item.FindControl("hlkLink"), HyperLink)
            Dim imgProductImage As Image = CType(e.Item.FindControl("imgProductImage"), Image)
            Dim litProductImageDescription As Literal = CType(e.Item.FindControl("litProductImageDescription"), Literal)
            Dim hlkLink2 As HyperLink = CType(e.Item.FindControl("hlkLink2"), HyperLink)
            Dim imgProductImage2 As HtmlControls.HtmlImage = CType(e.Item.FindControl("imgProductImage2"), HtmlControls.HtmlImage)

            litName.Text = row.Name
            hlkLink.NavigateUrl = String.Format("/Oscilloscope/OscilloscopeSeries.aspx?mseries={0}{1}", row.ProductSeriesId, menuURL)
            imgProductImage.ImageUrl = String.Format("{0}{1}", ConfigurationManager.AppSettings("rootdir"), row.ProductImage)
            litProductImageDescription.Text = row.ProductImageDescription
            hlkLink2.NavigateUrl = String.Format("/Oscilloscope/OscilloscopeSeries.aspx?mseries={0}{1}", row.ProductSeriesId, menuURL)
            hlkLink2.Attributes.Add("onmouseover", String.Format("MM_swapImage('ID_{0}','','{1}/images/category/btn_learn_more_on.gif',0)", row.ProductSeriesId, ConfigurationManager.AppSettings("rootdir")))
            hlkLink2.Attributes.Add("onmouseout", "MM_swapImgRestore()")
            imgProductImage2.Src = String.Format("{0}/images/category/btn_learn_more_off.gif", ConfigurationManager.AppSettings("rootdir"))
            imgProductImage2.Attributes.Add("name", String.Format("ID_{0}", row.ProductSeriesId))
        End If
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateProductIdQueryString()
        If (_productId <= 0) Then
            If Len(Request.QueryString("modelid")) = 0 Then
                Response.Redirect("default.aspx")
            Else
                If IsNumeric(Request.QueryString("modelid")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ", ") Then
                        _productId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("modelid")), ", ") + 1)
                    Else
                        _productId = SQLStringWithOutSingleQuotes(Request.QueryString("modelid"))
                    End If

                    Dim psSeoValue As String = String.Empty
                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
                    If Not (productSeries Is Nothing) Then
                        If Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                            psSeoValue = productSeries.SeoValue
                        End If
                    End If

                    Dim productSeriesCategory As List(Of ProductSeriesCategory) = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
                    If (productSeriesCategory.Count > 0) Then

                    End If

                    Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId)
                    If Not (product Is Nothing) Then
                        If (String.IsNullOrEmpty(product.SeoValue) Or String.IsNullOrEmpty(psSeoValue)) Then
                            ' Fall through
                        Else
                            CreateMeta(psSeoValue, product.SeoValue)
                            Response.Redirect(String.Format("~/probes/{0}/{1}", psSeoValue, product.SeoValue))
                        End If
                    End If
                Else
                    Response.Redirect("Default.aspx")
                End If
            End If
        End If
    End Sub

    Private Sub ValidateProductSeriesIdQueryString()
        If (_productSeriesId <= 0) Then
            If Len(Request.QueryString("mseries")) = 0 Then
                Response.Redirect("Default.aspx")
            Else
                If IsNumeric(Request.QueryString("mseries")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ", ") Then
                        _productSeriesId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ", ") + 1)
                    Else
                        _productSeriesId = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                    End If
                    If Not Functions.CheckSeriesExists(_productSeriesId, 3) Then
                        Response.Redirect("Default.aspx")
                    End If
                    'Else
                    '    Response.Redirect("Default.aspx")
                End If
            End If
        End If
    End Sub

    Private Sub BindPage()
        Dim productSeries As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "Select distinct a.product_series_id, a.name, a.sortid, a.SeoValue from product_series a  INNER JOIN product_series_category psc on a.product_series_id = psc.product_series_id inner join product b ON psc.product_id = b.productid WHERE  b.disabled='n' and psc.category_id= 3 order by a.sortid", New List(Of SqlParameter)().ToArray())
        If (productSeries.Count > 0) Then
            Me.menulabel.Text += "<ul>"
            For Each ps In productSeries
                If (_productSeriesId = ps.ProductSeriesId) Then
                    If Not (String.IsNullOrEmpty(ps.SeoValue)) Then
                        Me.menulabel.Text += String.Format("<li class='current'><a href='/probes/{0}{2}'>{1}</a></li>", ps.SeoValue, ps.Name, IIf(ps.SeoValue.Contains("."), "/", String.Empty))
                    Else
                        Me.menulabel.Text += String.Format("<li class='current'><a href='/probes/probeseries.aspx?mseries={0}'>{1}</a></li>", ps.ProductSeriesId.ToString(), ps.Name)
                    End If
                Else
                    If Not (String.IsNullOrEmpty(ps.SeoValue)) Then
                        Me.menulabel.Text += String.Format("<li><a href='/probes/{0}{2}'>{1}</a></li>", ps.SeoValue, ps.Name, IIf(ps.SeoValue.Contains("."), "/", String.Empty))
                    Else
                        Me.menulabel.Text += String.Format("<li><a href='/probes/probeseries.aspx?mseries={0}'>{1}</a></li>", ps.ProductSeriesId.ToString(), ps.Name)
                    End If
                End If
            Next
            Me.menulabel.Text += "</ul>"
        End If

        Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId)
        If Not (product Is Nothing) Then
            Me.lbModelImage.Text = "<img src='" + rootDir + product.IntroImagePath + "'  width='540' height='211'>"
            Me.landTitle.Text = product.Name
            Me.landContent.Text = product.Introduction
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + product.Name
        End If

        Session("cataid") = CategoryIds.PROBES  ' NOTE: [JC] Added for quotes.
        ucResources.ProductGroupId = product.GroupId
        ucResources.ShowRequestQuote = True
        If Not Session("CountryCode") Is Nothing Then
            If Len(Session("CountryCode").ToString) > 0 Then
                If Session("CountryCode").ToString = "208" Then
                    ucResources.ShowBuy = True
                End If
            End If
        End If

        Dim promotionHtml As String = String.Empty
        Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
        If (countryCode >= 0) Then
            promotionHtml = Functions.GetPromoImagesForSeries(_productSeriesId, rootDir, countryCode)
        End If
        If Not (String.IsNullOrEmpty(promotionHtml)) Then
            litPromotion.Text = promotionHtml
        Else
            litPromotion.Text = "<div class=""greyPadding""></div>"
        End If

        Dim additionalResources As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId, Int32.Parse(AppConstants.ADDITIONAL_RESOURCES_PRODUCT))
        If Not (additionalResources Is Nothing) Then
            lblAdditionalResources.Text = additionalResources.OverviewDetails
        End If
        getProductLine(_productSeriesId)
        getOverview(_productSeriesId)
        getProductDetail(_productId)
        getSpecs(_productId)
        getAccessories(_productId)
        getCompatability(_productId)
        lblTabsToShow.Text = tabsToShow.ToString()
        If hasDetail Then
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_DETAIL_TAB & "');</script>"
        ElseIf hasOverview Then
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
        Else
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.PRODUCT_LINE_TAB & "');</script>"
        End If
        Me.lb_leftmenu.Text = Functions.LeftMenu(captionId, rootDir, pn_leftmenu, menuId)
        Me.menulabel.Visible = False
    End Sub

    Protected Function getTitle(ByVal productSeriesId As Int32, ByVal partnum As String, ByVal introduction As String, ByVal pid As String) As String
        If (String.IsNullOrEmpty(introduction)) Then
            Return "<strong>" + partnum + "</strong>"
        Else
            Dim ps As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString"), productSeriesId)
            If Not (ps Is Nothing) Then
                Dim p As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString"), pid)
                If Not (p Is Nothing) Then
                    If Not (String.IsNullOrEmpty(ps.SeoValue)) And Not (String.IsNullOrEmpty(p.SeoValue)) Then
                        Return String.Format("<a href='/probes/{0}/{1}'><strong>{2}</strong></a>", ps.SeoValue, p.SeoValue, partnum)
                    Else
                        Return String.Format("<a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}{3}'><strong>{4}</strong></a>", pid, categoryId, productSeriesId, menuURL, partnum)
                    End If
                Else
                    Return String.Format("<a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}{3}'><strong>{4}</strong></a>", pid, categoryId, productSeriesId, menuURL, partnum)
                End If
            Else
                Return String.Format("<a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}{3}'><strong>{4}</strong></a>", pid, categoryId, productSeriesId, menuURL, partnum)
            End If
        End If
    End Function

    Private Sub getProductLine(ByVal productSeriesId As Int32)
        Dim productLine As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT distinct b.PRODUCTID, b.PARTNUMBER, B.PRODUCTDESCRIPTION, b.IMAGEFILENAME, b.INTRODUCTION, b.sort_id, b.SeoValue from product_series a INNER JOIN product_series_category psc on a.product_series_id = psc.product_series_id inner join product b ON psc.PRODUCT_ID = b.productid where b.disabled='n' and a.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND psc.CATEGORY_ID = 3 order by b.sort_id", sqlParameters.ToArray())
        If (products.Count > 0) Then
            productLine += "<p><span class='blue'>&nbsp;&nbsp;&nbsp;<strong> " + ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId).Name + "</strong></span></p><br /> "
            productLine += " <div class='searchResults'> "
            productLine += "   <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For Each product In products
                productLine = productLine + "<tr><td class='cell'>"
                If (Len(product.Introduction) > 0) Then
                    If Not (String.IsNullOrEmpty(product.SeoValue)) Then
                        Dim ps As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString"), productSeriesId)
                        If Not (ps Is Nothing) Then
                            If Not (String.IsNullOrEmpty(ps.SeoValue)) Then
                                productLine += String.Format("<strong><a href='/probes/{0}/{1}'>{2}</a></strong> - {3}", ps.SeoValue, product.SeoValue, product.PartNumber, Functions.GetProdDescriptionOnProdID(product.ProductId).ToString())
                            Else
                                productLine += String.Format("<strong><a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}'>{3}</a></strong> - {4}", product.ProductId.ToString(), categoryId.ToString(), productSeriesId.ToString(), product.PartNumber, Functions.GetProdDescriptionOnProdID(product.ProductId).ToString())
                            End If
                        Else
                            productLine += String.Format("<strong><a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}'>{3}</a></strong> - {4}", product.ProductId.ToString(), categoryId.ToString(), productSeriesId.ToString(), product.PartNumber, Functions.GetProdDescriptionOnProdID(product.ProductId).ToString())
                        End If
                    Else
                        productLine += String.Format("<strong><a href='/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}'>{3}</a></strong> - {4}", product.ProductId.ToString(), categoryId.ToString(), productSeriesId.ToString(), product.PartNumber, Functions.GetProdDescriptionOnProdID(product.ProductId).ToString())
                    End If
                Else
                    productLine += "<strong>" + product.PartNumber + "</strong>" + " - " + Functions.GetProdDescriptionOnProdID(product.ProductId).ToString
                End If
                productLine = productLine + "</td></tr>"
            Next
            productLine = productLine + "</table>"
            productLine = productLine + "</div>"
            lb_productLine.Text = productLine
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
        End If
    End Sub

    Private Sub getOverview(ByVal productSeriesId As Int32)
        If (productSeriesId <= 0) Then
            Return
        End If
        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId, AppConstants.SERIES_OVERVIEW_TYPE)
        If Not (cmsOverview Is Nothing) Then
            Me.lb_overview.Text = cmsOverview.OverviewDetails
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
        End If
    End Sub

    Private Sub getProductDetail(ByVal productId As Int32)
        Dim cmsOverview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productId, AppConstants.PRODUCT_OVERVIEW_TYPE)
        If Not (cmsOverview Is Nothing) Then
            If Not (String.IsNullOrEmpty(cmsOverview.OverviewDetails)) Then
                Me.lblProductDetail.Text = cmsOverview.OverviewDetails
                hasDetail = True
                tabsToShow.Append("<li><a href='#product_details'><img src='")
                tabsToShow.Append(rootDir)
                tabsToShow.Append("/images/tabs/tabs_product_details_off.gif' id='product_details' border='0'></a></li>")
            End If
        End If
    End Sub

    Private Sub getSpecs(ByVal productId As Int32)
        Dim propName As String = ""
        Dim propValue As String = ""
        Dim specString As String = ""
        Dim pNum As Integer
        Dim i As Integer
        Dim ii As Integer
        Dim strSQL As String = "SELECT distinct b.prop_cat_id, a.prop_cat_name,a.sort_id FROM " +
                " property_category a INNER JOIN  PROPERTY b ON  a.prop_cat_id = b.prop_cat_id " +
                " INNER JOIN PRODUCTPROPVALUE c  ON c.PROPERTYID = b.PROPERTYID " +
                " WHERE c.PRODUCTID=@PRODUCTID and c.VALUE>' ' ORDER by a.SORT_ID"
        Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
        pNum = ds.Tables(0).Rows.Count
        If pNum > 0 Then
            specString = " " + _
                " <table width='100%' border='0' cellspacing='0' cellpadding='0'> "
            For i = 0 To pNum - 1
                specString = specString + " <tr> "
                specString = specString + " <td colspan='2' class='cell'><h3>" + Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_name")) + "</h3></td>"
                specString = specString + " </tr> "
                strSQL = "SELECT b.PROPERTYNAME,b.PROPERTYID, c.VALUE " +
                        "FROM  PROPERTY as b INNER JOIN PRODUCTPROPVALUE as c On b.PROPERTYID=c.PROPERTYID " +
                        "WHERE b.prop_cat_id = @PROPCATID and c.PRODUCTID=@PRODUCTID and c.VALUE>' ' ORDER by b.SORTID"

                sqlParameters = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PROPCATID", Me.convertToString(ds.Tables(0).Rows(i)("prop_cat_id"))))
                sqlParameters.Add(New SqlParameter("@PRODUCTID", productId.ToString()))
                Dim ds2 As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), strSQL, sqlParameters.ToArray())
                pNum = ds2.Tables(0).Rows.Count
                If pNum > 0 Then
                    For ii = 0 To pNum - 1
                        If (String.Compare(ds2.Tables(0).Rows(ii)("PROPERTYID").ToString(), "456", True) = 0) Then  ' Exclude pricing if not US
                            If Not (String.Compare(countryCode.ToString(), "208", True) = 0) Then
                                Continue For
                            End If
                        End If
                        propName = ds2.Tables(0).Rows(ii)("PROPERTYNAME").ToString()
                        propValue = ds2.Tables(0).Rows(ii)("VALUE").ToString()
                        specString = specString + " <tr> " + _
                            " <td width='35%' class='cell'>" + propName + "</td>" + _
                            " <td width='65%' class='cell'>" + propValue + "</td> " + _
                           " </tr> "
                    Next
                    specString = specString + " <tr> " + _
                        " <td colspan='2' class='cell'>&nbsp;</td>" + _
                       " </tr> "
                End If
            Next
            specString = specString + " </table> "
            tabsToShow.Append("<li><a href='#spec'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_specs_off.gif' id='spec' border='0'></a></li>")
        End If
        Me.lblSpecs.Text = specString
    End Sub

    Private Sub getAccessories(ByVal productId As Int32)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@MODELID", productId.ToString()))
        Dim accessories As List(Of LeCroy.Library.Domain.Common.DTOs.Document) = DocumentRepository.FetchDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT a.DOCUMENT_ID, a.TITLE, a.DESCRIPTION, a.FILE_FULL_PATH, a.VERSION, a.DOC_SIZE, a.COMMENTS, a.IMAGE FROM DOCUMENT_CATEGORY AS b INNER JOIN DOCUMENT AS a ON a.DOCUMENT_ID = b.DOCUMENT_ID WHERE a.DOC_TYPE_ID = 19 AND a.POST = 'y' AND a.COUNTRY_ID = 208 and b.PRODUCT_ID =@MODELID ORDER BY a.SORT_ID", sqlParameters.ToArray())
        If (accessories.Count > 0) Then
            Me.dl_accessories.DataSource = accessories
            Me.dl_accessories.DataBind()
            tabsToShow.Append("<li><a href='#accessories'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/accessories_off.gif' id='accessories' border='0'></a></li>")
        End If
    End Sub

    Private Sub getCompatability(ByVal productId As Int32)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@MODELID", productId.ToString()))
        Dim compatabilities As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT d.PRODUCT_SERIES_ID, d.NAME, d.PRODUCTIMAGE, d.PRODUCTIMAGEDESC, d.SORTID FROM PRODUCT AS a INNER JOIN  CONFIG AS c ON a.PRODUCTID = c.PRODUCTID INNER JOIN PRODUCT_SERIES AS d INNER JOIN  PRODUCT_SERIES_CATEGORY AS e ON d.PRODUCT_SERIES_ID = e.PRODUCT_SERIES_ID ON a.PRODUCTID = e.PRODUCT_ID WHERE (e.CATEGORY_ID = 1) AND (a.DISABLED = 'n') AND (a.DISABLED = 'n') AND c.OPTIONID =@MODELID ORDER BY d.SORTID", sqlParameters.ToArray())
        If (compatabilities.Count > 0) Then
            Me.dl_series.DataSource = compatabilities
            Me.dl_series.DataBind()
            tabsToShow.Append("<li><a href='#compatability'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/compatability_off.gif' id='compatability' border='0'></a></li>")
        End If
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _productSeriesId = xParam.ProductSeriesId

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If Not (param2.Key Is Nothing) Then
                        Dim yParam As Product = ProductRepository.GetProductBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId, param2.Value)
                        If Not (yParam Is Nothing) Then
                            _productId = yParam.ProductId
                            CreateMeta(param1.Value, param2.Value)
                            Return
                        End If
                    End If
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue1 As String, ByVal seoValue2 As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/probes/{1}/{2}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue1, seoValue2)
                head.Controls.Add(link)
                Dim meta As New HtmlMeta()
                meta.Name = "series-id"
                meta.Content = _productSeriesId
                head.Controls.Add(meta)
                Dim meta2 As New HtmlMeta()
                meta2.Name = "model-id"
                meta2.Content = _productId
                head.Controls.Add(meta2)
            End If
        End If
    End Sub
#End Region
End Class