﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/threecolumn.master" CodeBehind="probeseries.aspx.vb" Inherits="LeCroy.Website.probes_probeseries" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server"><asp:Literal ID="menulabel" runat="server" /></asp:Panel>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <asp:Label ID="lbSeriesImage" runat="server" />
                            <div class="intro">
                                <h1><asp:Label ID="lbSeriesTileTop" runat="server" Text="" /></h1>
                                <p><asp:Label ID="lbSeriesDesc" runat="server" Text="" /></p>
                                <asp:Label ID="lbSeriesTileBelow" runat="server" Text="" CssClass="lecroyblue" />
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="tabs">
                    <div class="bg">
                        <ul class="tabNavigation"><asp:Label ID="lblTabsToShow" runat="server" /></ul>
                        <div id="product_line">
                            <p><span class="blue">&nbsp;&nbsp;&nbsp;<strong><asp:Literal ID="litProductHeader" runat="server" /></strong></span></p><br />
                            <asp:Repeater ID="rptProducts" runat="server">
                                <HeaderTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr><td class="cell"><strong><asp:HyperLink ID="hlkProduct" runat="server" /></strong> - <asp:Literal ID="litDescription" runat="server" /></td></tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div id="overview"><asp:Label ID="lblOverview" runat="server" /></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <asp:Literal ID="litPromotion" runat="server" />
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="3" ShowBuy="true" ShowRequestDemo="true" ShowRequestQuote="true" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>