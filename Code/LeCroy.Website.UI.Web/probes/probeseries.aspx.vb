﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL
Imports LeCroy.Website.Functions

Public Class probes_probeseries
    Inherits BasePage

#Region "Variables/Keys"
    Dim tabsToShow As StringBuilder = New StringBuilder()
    Private _productSeriesId As Int32 = 0
    Private Const captionId As Int32 = 102
    Private Const categoryId As Int32 = 3
    Private Const menuId As Int32 = 508
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Oscilloscope Probes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        HandleRoute()
        If Not (IsPostBack) Then
            ValidateQueryString()
            If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId, CategoryIds.PROBES)) Then
                Response.Redirect("~/")
            End If
            ucResources.CategoryId = categoryId
            ucResources.ProductSeriesId = _productSeriesId
            Session("seriesSelected") = _productSeriesId
            BindPage(_productSeriesId)
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub rptProducts_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptProducts.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Product = CType(e.Item.DataItem, Product)
                Dim hlkProduct As HyperLink = CType(e.Item.FindControl("hlkProduct"), HyperLink)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)

                hlkProduct.Text = row.PartNumber
                If Not (String.IsNullOrEmpty(row.Introduction)) Then
                    If Not (String.IsNullOrEmpty(row.SeoValue)) Then
                        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString"), _productSeriesId)
                        If (productSeries Is Nothing) Then
                            hlkProduct.NavigateUrl = String.Format("~/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}&capid={3}&mid={4}", row.ProductId, categoryId, _productSeriesId, captionId, menuId)
                        Else
                            If Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                                hlkProduct.NavigateUrl = String.Format("~/probes/{0}/{1}", productSeries.SeoValue, row.SeoValue)
                            Else
                                hlkProduct.NavigateUrl = String.Format("~/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}&capid={3}&mid={4}", row.ProductId, categoryId, _productSeriesId, captionId, menuId)
                            End If
                        End If
                    Else
                        hlkProduct.NavigateUrl = String.Format("~/probes/probemodel.aspx?modelid={0}&categoryid={1}&mseries={2}&capid={3}&mid={4}", row.ProductId, categoryId, _productSeriesId, captionId, menuId)
                    End If
                End If
                litDescription.Text = Functions.GetProdDescriptionOnProdID(row.ProductId).ToString()
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub ValidateQueryString()
        If (_productSeriesId <= 0) Then
            If Len(Request.QueryString("mseries")) = 0 Then
                Response.Redirect("default.aspx")
            Else
                If IsNumeric(Request.QueryString("mseries")) Then
                    If InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") Then
                        _productSeriesId = Mid(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), InStr(SQLStringWithOutSingleQuotes(Request.QueryString("mseries")), ",") + 1)
                    Else
                        _productSeriesId = SQLStringWithOutSingleQuotes(Request.QueryString("mseries"))
                    End If
                    If Not CheckSeriesExists(_productSeriesId, 3) Then
                        Response.Redirect("default.aspx")
                    End If

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId)
                    If Not (productSeries Is Nothing) Then
                        If Not (String.IsNullOrEmpty(productSeries.SeoValue)) Then
                            CreateMeta(productSeries.SeoValue)
                            Response.Redirect(String.Format("~/probes/{0}{1}", productSeries.SeoValue, IIf(productSeries.SeoValue.Contains("."), "/", String.Empty)))
                        End If
                    End If
                Else
                    Response.Redirect("default.aspx")
                End If
            End If
        End If
    End Sub

    Private Sub BindPage(ByVal productSeriesId As Int32)
        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId)
        getProductLine(productSeries)
        getOverview(productSeriesId)
        lblTabsToShow.Text = tabsToShow.ToString()
        Functions.CreateMetaTags(Me.Header, productSeriesId, AppConstants.SERIES_OVERVIEW_TYPE)
        Dim productSeriesMenu As List(Of ProductSeries) = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), "select distinct a.PRODUCT_SERIES_ID, a.name, a.sortid ,'1' as URLType, a.SeoValue from product_series a INNER JOIN product_series_Category psc on a.Product_series_id = psc.PRODUCT_SERIES_ID inner join product b ON psc.PRODUCT_ID = b.PRODUCTID where b.disabled='n' and a.DISABLED = 'N' and psc.category_id= 3 order by a.sortid", New List(Of SqlParameter)().ToArray())
        If (productSeriesMenu.Count > 0) Then
            Me.menulabel.Text += "<ul>"
            For Each psm In productSeriesMenu
                If (productSeriesId = psm.ProductSeriesId) Then
                    If Not (String.IsNullOrEmpty(psm.SeoValue)) Then
                        Me.menulabel.Text += String.Format("<li class='current'><a href='/probes/{0}'>{1}</a></li>", psm.SeoValue, productSeries.Name)
                    Else
                        Me.menulabel.Text += String.Format("<li class='current'><a href='/probes/probeseries.aspx?mseries={0}'>{1}</a></li>", productSeriesId, productSeries.Name)
                    End If
                Else
                    If Not (String.IsNullOrEmpty(psm.SeoValue)) Then
                        Me.menulabel.Text += String.Format("<li><a href='/probes/{0}'>{1}</a></li>", psm.SeoValue, psm.Name.ToString())
                    Else
                        Me.menulabel.Text += String.Format("<li><a href='/probes/probeseries.aspx?mseries={0}'>{1}</a></li>", psm.ProductSeriesId.ToString(), psm.Name.ToString())
                    End If
                End If
            Next
            Me.menulabel.Text += "</ul>"
        Else
            Response.Redirect("/default.aspx")
        End If
        lbSeriesTileTop.Text = productSeries.Name
        lbSeriesTileBelow.Text = "Explore " + productSeries.ShortName + "&nbsp;<img src='" + rootDir + "/images/icons/icon_small_arrow_down.gif' alt='Explore " + productSeries.ShortName + "' >"
        lbSeriesImage.Text = "<img src='" + rootDir + productSeries.TitleImage + "' width='260' align='right'>"
        lbSeriesDesc.Text = productSeries.Description
        Dim pageTitle As String = Functions.GetPageTitle(productSeriesId.ToString(), AppConstants.SERIES_OVERVIEW_TYPE)
        If Not (String.IsNullOrEmpty(pageTitle)) Then
            Me.Title = pageTitle
        Else
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - " + productSeries.Name
        End If

        Dim promotionHtml As String = String.Empty
        Dim countryCode As Int32 = Functions.ValidateIntegerFromSession(Session("CountryCode"))
        If (countryCode >= 0) Then
            promotionHtml = Functions.GetPromoImagesForSeries(productSeriesId, rootDir, countryCode)
        End If
        If Not (String.IsNullOrEmpty(promotionHtml)) Then
            litPromotion.Text = promotionHtml
        Else
            litPromotion.Text = "<div class=""greyPadding""></div>"
        End If

        Dim additionalResources As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId, Int32.Parse(AppConstants.ADDITIONAL_RESOURCES_SERIES.ToString()))
        If Not (additionalResources Is Nothing) Then
            lblAdditionalResources.Text = additionalResources.OverviewDetails
        End If
        lb_leftmenu.Text = Functions.LeftMenu(102, rootDir, pn_leftmenu, 508)
        pn_leftmenu.Visible = False
    End Sub

    Private Sub getOverview(ByVal productSeriesId As Int32)
        Dim overview As CmsOverview = CmsOverviewRepository.GetCmsOverview(ConfigurationManager.AppSettings("ConnectionString").ToString(), productSeriesId, Int32.Parse(AppConstants.SERIES_OVERVIEW_TYPE.ToString()))
        If Not (overview Is Nothing) Then
            Me.lblOverview.Text = overview.OverviewDetails
            tabsToShow.Append("<li><a href='#overview'><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_overview_off.gif' id='overview' border='0'></a></li>")
            lbOnLoad.Text = "<script type='text/javascript'>getHash('#" & AppConstants.OVERVIEW_TAB & "');</script>"
        End If
    End Sub

    Private Sub getProductLine(ByVal productSeries As ProductSeries)
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", productSeries.ProductSeriesId.ToString()))
        litProductHeader.Text = productSeries.Name
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT DISTINCT a.* FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_CATEGORY c WHERE  a.PRODUCTID = c.PRODUCT_ID  AND b.PRODUCT_SERIES_ID = c. PRODUCT_SERIES_ID  AND c.CATEGORY_ID= 3 AND c.PRODUCT_SERIES_ID=@PRODUCTSERIESID AND a.disabled='n' order by a.sort_id", sqlParameters.ToArray())
        If (products.Count > 0) Then
            tabsToShow.Append("<li><a href=""#product_line""><img src='")
            tabsToShow.Append(rootDir)
            tabsToShow.Append("/images/tabs/tabs_product_line_on.gif' id='product_line' border='0'></a></li>")
            _productSeriesId = productSeries.ProductSeriesId
            rptProducts.DataSource = products
            rptProducts.DataBind()
            Session("cataid") = CategoryIds.PROBES  ' NOTE: [JC] Added for quotes.
            ucResources.ProductGroupId = products.FirstOrDefault().GroupId
        End If
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() > 0) Then
            Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
            If Not (param1.Key Is Nothing) Then
                Dim xParam As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If Not (xParam Is Nothing) Then
                    _productSeriesId = xParam.ProductSeriesId
                    CreateMeta(param1.Value)
                End If
            End If
        Else

        End If
    End Sub

    Private Sub CreateMeta(ByVal seoValue As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                link.Href = String.Format("{0}/probes/{1}{2}", ConfigurationManager.AppSettings("DefaultDomain"), seoValue, IIf(seoValue.Contains("."), "/", String.Empty))
                head.Controls.Add(link)
                Dim meta As New HtmlMeta()
                meta.Name = "series-id"
                meta.Content = _productSeriesId
                head.Controls.Add(meta)
            End If
        End If
    End Sub
#End Region
End Class