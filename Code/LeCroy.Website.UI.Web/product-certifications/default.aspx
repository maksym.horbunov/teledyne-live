﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.product_certifications_default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; height: 100%;">
        <div style="padding-bottom: 12px; padding-left: 30px; padding-right: 30px; padding-top: 20px;">
            <h1>Teledyne LeCroy Product Certifications</h1>
            <p>See the product certification notice document for the applicable certifications for Teledyne LeCroy products</p>
            <%--<p><a href="#" target="_blank"><img src="/images/icons/icons_pdf.gif" border="0" /> Product Certification</a></p>--%>
        </div>
    </div>
</asp:Content>