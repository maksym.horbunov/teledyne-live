<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.quantum_data_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="400" valign="top">
                    <h1 style="padding-top:20px;font-size:22px;color:#0076c0;">Teledyne LeCroy to expand Protocol Solutions offerings into video protocols including HDMI and SDI with the acquisition of Quantum Data</h1>
                    <p style="font-size:16px;">Teledyne LeCroy Inc. announced that it has entered into an agreement to acquire Quantum Data, Inc., the market leader in HDMI and SDI signal generators and protocol analyzers as well as test tools for other digital video technologies.</p>
                </td>
                <td width="325" valign="top"><img src="/images/quantum_230x83.jpg" border="0" width="200px" hspace="50px" vspace="95px" /></td>
                <td width="250">
                    <div class="subNav">
                        <ul>
                            <li><a href="/pressreleases/document.aspx?news_id=1958">Press Release</a></li>
                            <li><a href="http://www.quantumdata.com/products.html">Products</a></li>
                            <li><a href="http://www.quantumdata.com/downloads.html">Downloads</a></li>
                            <li><a href="http://www.quantumdata.com/contact.html">Contact Quantum Data</a></li>
                        </ul>
			        </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <div style="background:url(/images/quantum-header.jpg) no-repeat;height:374px;">
<div style="padding:45px 0px 0px 300px;font-weight:500;font-size:18px;color:#ffffff;">
	<a href="http://www.quantumdata.com/" style="color:#ffffff;">Home</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/products.html" style="color:#ffffff;">Products</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/solutions.html" style="color:#ffffff;">Solutions</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/downloads.html" style="color:#ffffff;">Downloads</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/news.html" style="color:#ffffff;">News</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/support.html" style="color:#ffffff;">Support</a>&nbsp;&nbsp;&nbsp;
	<a href="http://www.quantumdata.com/contact.html" style="color:#ffffff;">Contact</a>&nbsp;&nbsp;&nbsp;
</div>



        </div>
        
        <hr width="75%" />
        <div style="padding-left:200px;padding-right:200px;padding-top:25px;">
            
        <h1 style="color: #0076c0;">Teledyne LeCroy to expand Protocol Solutions offerings into video protocols including HDMI and SDI with the acquisition of Quantum Data</h1>
<p>Teledyne LeCroy Inc. announced today that it has entered into an agreement to acquire Quantum Data, Inc., the market leader in HDMI and SDI signal generators and protocol analyzers as well as test tools for other digital video technologies.</p>
<p>Teledyne LeCroy is the market leader in protocol test tools for well known serial data standards like USB, PCI Express, SAS, and SATA.  The Company�s strategy is to continue to expand its protocol offerings into additional wired and wireless data communication standards with the goal to be the market leader in each protocol standard.</p>
<p>Teledyne LeCroy expects that acquiring Quantum Data will enable it to:
<ul><li>Broaden its offering of Serial Data Protocol Analysis and Test Tools.</li>
<li>Capitalize on growing customer demand for HDMI video generators and test tools.</li>
<li>Further penetrate the video broadcast market for emerging serial data test requirements.</li>
<li>Build on the recent acquisition of Frontline Test Equipment, a leader in wireless protocol test, bolstering recognition of Teledyne LeCroy as the leader in protocol test solutions.</li></ul></p>

<p>Teledyne LeCroy�s Chief Executive Officer Tom Reslewic said, �The Quantum Data acquisition will allow Teledyne LeCroy to expand its protocol test portfolio into important video technologies like HDMI and SDI.  These standards are key to emerging capabilities in consumer electronics, professional video and studio/broadcast applications.  We anticipate a growing need for protocol test tools among designers in these markets.  This expansion into adjacent video protocol standards reflects Teledyne�s commitment to build a market leading presence across a wide array of serial data communications standards through Teledyne LeCroy�s Protocol Solutions Group.�</p>
<p>Quantum Data CEO Allen Jorgensen said, "Joining Teledyne LeCroy�s Protocol Solutions Group will be a great opportunity for Quantum Data and its employees.  As the largest player in the serial data protocol test tools market, Teledyne LeCroy is well positioned to help Quantum Data unlock growth opportunities in new and existing markets.  The opportunity to leverage Teledyne LeCroy�s Ethernet protocol analyzers into emerging broadcast applications could provide Quantum Data a key advantage in the market.� </p>
<p>The acquisition of Quantum Data will support Teledyne LeCroy�s vision of becoming the leading brand for protocol test, with competencies that will enable the company to benefit from emerging industry trends requiring faster serial data connections in an expanding array of applications.</p>
<h1 style="color: #0076c0;">About Teledyne LeCroy</h1>
<p>Teledyne LeCroy is a leading manufacturer of advanced oscilloscopes, protocol analyzers, and other test instruments that verify performance, validate compliance, and debug complex electronic systems quickly and thoroughly. Since its founding in 1964, the Company has focused on incorporating powerful tools into innovative products that enhance "Time-to-Insight". Faster time to insight enables users to rapidly find and fix defects in complex electronic systems, dramatically improving time-to-market for a wide variety of applications and end markets. Teledyne LeCroy is based in Chestnut Ridge, N.Y. For more information, visit Teledyne LeCroy's website at teledynelecroy.com.</p>

<h1 style="color: #0076c0;">Forward-Looking Statements Cautionary Notice</h1>
<p>This press release contains forward-looking statements, as defined in the Private Securities Litigation Reform Act of 1995, relating to a potential acquisition of a company.  Actual results could differ materially from these forward-looking statements.  Many factors, including the ability of Teledyne and Quantum Data to achieve anticipated synergies, as well as market and economic conditions beyond either company�s control, could change anticipated results.</p>
            </div>
    </div>
</asp:Content>