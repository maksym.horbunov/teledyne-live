Partial Class quantum_data_default
    Inherits BasePage

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim css As HtmlLink = New HtmlLink()
        css.Href = ResolveUrl(ConfigurationManager.AppSettings("CssThickBox"))
        css.Attributes.Add("rel", "stylesheet")
        css.Attributes.Add("type", "text/css")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(css)
        Dim jQueryUIMin As HtmlGenericControl = New HtmlGenericControl("script")
        jQueryUIMin.Attributes.Add("type", "text/javascript")
        jQueryUIMin.Attributes.Add("language", "javascript")
        jQueryUIMin.Attributes.Add("src", ResolveUrl(ConfigurationManager.AppSettings("JsThickbox")))
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(jQueryUIMin)
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - High Definition Oscilloscopes"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
    End Sub
End Class