﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.redeem_default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryBottom">
        <div><h1><asp:Label ID="lblPromotion" runat="server" /></h1><br /><asp:Label ID="lblPromotionDescription" runat="server" /></div>
        <asp:Panel ID="pnlSubmitted" runat="server" Visible="false">
            <br />Thank you for participiating in our promotion! Your information has been sent to a Teledyne LeCroy Customer Service Specialist for processing of your free promotion request.<br /><br />
            <asp:HyperLink ID="hlkReturn" runat="server" NavigateUrl="~/" Text="Return to our home page" />
        </asp:Panel>
        <asp:Panel ID="pnlPromotionalCode" runat="server" DefaultButton="btnSubmitPromotionalCode">
            <div style="padding-left: 20px;">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right" width="250"><span style="color: #ff0000;">Please enter the promotional code you received:</span>&nbsp;</td>
                        <td align="left"><asp:TextBox ID="txtPromotionalCode" runat="server" MaxLength="20" Width="150" /></td>
                        <td align="left">&nbsp;<asp:CustomValidator ID="cvPromotionalCode" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgPromotionalCode" /></td>
                    </tr>
                    <tr><td>&nbsp;</td><td colspan="2"><asp:Button ID="btnSubmitPromotionalCode" runat="server" CausesValidation="false" Text="Submit" ValidationGroup="vgPromotionalCode" /></td></tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSerialNumber" runat="server" DefaultButton="btnSubmit" Visible="false">
            <div>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" id="tblBundled" runat="server">
                                <tr>
                                    <td align="right" width="250"><span style="color: #ff0000;">Please enter your scope ID:</span>&nbsp;</td>
                                    <td align="left" width="100"><asp:TextBox ID="txtScopeId" runat="server" MaxLength="10" Width="150" /></td>
                                    <td align="left">&nbsp;<asp:CustomValidator ID="cvScopeId" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSerialNumber" />&nbsp;<a href="/images/scopeid_serialnum_from_scope_menu.jpg" class="thickbox" title="Where is my scope ID?">Where is my scope ID?</a></td>
                                    <td width="30">&nbsp;</td>
                                </tr>
                                <tr valign="top">
                                    <td align="right" width="250"><span style="color: #ff0000;">Please enter your serial number:</span>&nbsp;</td>
                                    <td align="left" width="100"><asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="20" Width="150" /></td>
                                    <td align="left">&nbsp;<asp:CustomValidator ID="cvSerialNumber" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSerialNumber" />&nbsp;<a href="/images/scopeid_serialnum_from_scope_menu.jpg" class="thickbox" title="Where is my serial number?">Where is my serial number?</a></td>
                                    <td width="30">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" width="200">
                            <table border="0" cellpadding="0" cellspacing="0" width="200">
                                <tr>
                                    <td>
                                        <br />Please confirm your profile details:
                                        <br /><b><asp:Literal ID="litFirstName" runat="server" />&nbsp;<asp:Literal ID="litLastName" runat="server" /></b><br />
                                        <asp:Literal ID="litCompany" runat="server" /><br />
                                        <asp:Literal ID="litAddress" runat="server" /><br />
                                        <asp:Literal ID="litCity" runat="server" />,&nbsp;<asp:Literal ID="litState" runat="server" />&nbsp;&nbsp;<asp:Literal ID="litZipCode" runat="server" /><br />
                                        <asp:Literal ID="litCountry" runat="server" /><br />
                                        <asp:Literal ID="litPhone" runat="server" /><br />
                                        <asp:Literal ID="litEmail" runat="server" /><br />
                                        <asp:HyperLink ID="hlkUpdateRegistration" runat="server"><b><asp:Literal ID="litChangeProfile" runat="server" /></b></asp:HyperLink><br /><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trBundled" runat="server">
                        <td colspan="2">
                            <div><span style="color: #ff0000;">Select a FREE bundle below</span></div><asp:CustomValidator ID="cvSelectedPromotion" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="false" ValidationGroup="vgSerialNumber" />
                            <asp:Table ID="tblPromotion" runat="server" GridLines="Both" />
                            <asp:HiddenField ID="hdn" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" Text="Submit" ValidationGroup="vgSerialNumber" />&nbsp;
                            <asp:Button ID="btnStartOver" runat="server" CausesValidation="false" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <script type="text/javascript">
        function UpdateRadioButtonOption(selectedValue) {
            var hdn = document.getElementById('<%= hdn.ClientID %>')
            hdn.value = selectedValue;
        }
    </script>
</asp:Content>