﻿Imports System.Drawing
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class redeem_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "Special Offers"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
        Dim css As HtmlLink = New HtmlLink()
        css.Href = ResolveUrl(ConfigurationManager.AppSettings("CssThickbox"))
        css.Attributes.Add("rel", "stylesheet")
        css.Attributes.Add("type", "text/css")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(css)
        Dim js As HtmlGenericControl = New HtmlGenericControl("script")
        js.Attributes.Add("type", "text/javascript")
        js.Attributes.Add("language", "javascript")
        js.Attributes.Add("src", ResolveUrl(ConfigurationManager.AppSettings("JsThickbox")))
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(js)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub cvPromotionalCode_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvPromotionalCode.ServerValidate
        args.IsValid = IsValidPromotionalCode(txtPromotionalCode.Text, txtPromotionalCode)
    End Sub

    Private Sub btnSubmitPromotionalCode_Click(sender As Object, e As EventArgs) Handles btnSubmitPromotionalCode.Click
        ResetFields(New List(Of TextBox)({txtPromotionalCode}))
        cvPromotionalCode.Validate()
        If (cvPromotionalCode.IsValid) Then
            Session("PromotionalBundleAccessCode") = txtPromotionalCode.Text
            HandleLoginRedirectIfRequired()
            pnlPromotionalCode.Visible = False
            pnlSerialNumber.Visible = True
            BindProfileDetails()
            BindBundles()
        End If
    End Sub

    Private Sub cvScopeId_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvScopeId.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtScopeId.Text)) Then
            cvScopeId.ErrorMessage = "*required"
            SetFieldColorAndSetFocus(txtScopeId)
            Return
        End If
        Dim regex As Regex = New Regex("(\w{6}-\w{2})|[W,w][M,m][0][0][0][0][0][1]$")
        If Not (regex.IsMatch(txtScopeId.Text)) Then
            cvScopeId.ErrorMessage = "*invalid"
            SetFieldColorAndSetFocus(txtScopeId)
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub cvSerialNumber_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvSerialNumber.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(txtSerialNumber.Text)) Then
            cvSerialNumber.ErrorMessage = "*required"
            SetFieldColorAndSetFocus(txtSerialNumber)
            Return
        End If
        ' DB check?
        args.IsValid = True
    End Sub

    Private Sub cvSelectedPromotion_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvSelectedPromotion.ServerValidate
        args.IsValid = False
        If (String.IsNullOrEmpty(hdn.Value)) Then
            Return
        End If
        Dim testVal As Int32 = 0
        If Not (Int32.TryParse(hdn.Value, testVal)) Then
            Return
        End If
        args.IsValid = True
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        ResetFields(New List(Of TextBox)({txtScopeId, txtSerialNumber}))
        Page.Validate("vgSerialNumber")
        If (Page.IsValid) Then
            HandleLoginRedirectIfRequired()
            If (String.IsNullOrEmpty(Session("CampaignID"))) Then
                Session("CampaignID") = 0
            End If
            Dim now As DateTime = DateTime.Now
            Dim request As Request = New Request()
            request.CampaignId = Session("CampaignID")
            request.ContactId = Session("ContactId")
            request.DateEntered = now
            request.EnteredBy = "webuser"
            request.ObjectId = "0"
            request.PageId = 0
            request.PresentId = 0
            request.RequestTypeId = 15
            If (tblBundled.Visible) Then
                request.SerialNum = String.Format("S/N : {0}", txtSerialNumber.Text)
                request.Remarks = String.Format("Requested Bundle: {0} | ScopeID: {1}", PromotionalBundleRepository.GetPromotionalBundle(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(hdn.Value)).Name, txtScopeId.Text)
            Else
                request.Remarks = String.Format("Requested Bundle: {0}", PromotionalBundleRepository.GetPromotionalBundle(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(hdn.Value)).Name)
            End If
            Dim retVal As InsertReturnValue(Of Int32) = RequestRepository.InsertRequest(ConfigurationManager.AppSettings("ConnectionString").ToString(), request)
            If (retVal.Success) Then
                PromotionalBundleAccessCodeRepository.UpdatePromotionalBundleAccessCode(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId").ToString()), Session("PromotionalBundleAccessCode").ToString())
                Dim promotionalBundle As PromotionalBundle = PromotionalBundleRepository.GetPromotionalBundle(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(hdn.Value))
                Dim promoName As String = String.Empty
                If Not (promotionalBundle Is Nothing) Then promoName = promotionalBundle.Name
                Dim contact As Contact = ContactRepository.GetContact(ConfigurationManager.AppSettings("ConnectionString").ToString(), Int32.Parse(Session("ContactId").ToString()))
                SendEmailToCSRs(contact, retVal.PrimaryKeyId, txtScopeId.Text, txtSerialNumber.Text, Session("PromotionalBundleAccessCode"), promoName, now)
                SendConfirmationEmailToEndUser(contact, txtScopeId.Text, txtSerialNumber.Text, Session("PromotionalBundleAccessCode"), promoName, now, LocaleIds.ENGLISH_US)
                pnlSerialNumber.Visible = False
                pnlSubmitted.Visible = True
                ClearLocalSessionVariables()
                Return
            End If
        End If
        BindBundles()
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindPage()
        Dim promotion As Promotion = ValidateQueryString()
        lblPromotion.Text = promotion.PromoName
        lblPromotionDescription.Text = promotion.PromoDescription

        If (String.IsNullOrEmpty(Session("PromotionalBundleAccessCode"))) Then
            ResetPage()
            Return
        End If

        If Not (IsValidPromotionalCode(Session("PromotionalBundleAccessCode"), Nothing)) Then
            ClearLocalSessionVariables()
            ResetPage()
            Return
        End If

        HandleLoginRedirectIfRequired()
        pnlPromotionalCode.Visible = False
        pnlSerialNumber.Visible = True
        BindProfileDetails()
        BindBundles()
    End Sub

    Private Sub BindProfileDetails()
        litFirstName.Text = Session("FirstName").ToString()
        litLastName.Text = Session("LastName").ToString()
        litCompany.Text = Session("Company").ToString()
        litAddress.Text = Session("Address").ToString()
        If Not (String.IsNullOrEmpty(Session("Address2"))) Then
            litAddress.Text += String.Format("<br />{0}", Session("Address2"))
        End If
        litCity.Text = Session("City").ToString()
        litState.Text = Session("State").ToString()
        litZipCode.Text = Session("Zip").ToString()
        litCountry.Text = Session("Country").ToString()
        litPhone.Text = Session("Phone").ToString()
        litEmail.Text = Session("Email").ToString()
        hlkUpdateRegistration.NavigateUrl = String.Format("{0}/support/user/userprofile.aspx", rootDir)
        litChangeProfile.Text = Functions.LoadI18N("SLBCA0788", Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
    End Sub

    Private Function ValidateQueryString() As Promotion
        If (String.IsNullOrEmpty(Request.QueryString("promotionid"))) Then
            Response.Redirect("~/promotions")
        End If
        Dim testVal As Int32 = 0
        If Not (Int32.TryParse(Request.QueryString("promotionid"), testVal)) Then
            Response.Redirect("~/promotions")
        End If
        Dim promotion As Promotion = PromotionRepository.GetPromotion(ConfigurationManager.AppSettings("ConnectionString").ToString(), testVal)
        If (promotion Is Nothing) Then
            Response.Redirect("~/promotions")
        End If
        If Not (promotion.PromoId = testVal) Then
            Response.Redirect("~/promotions")
        End If
        If Not (String.Compare(promotion.CanBundle, "Y", True) = 0) Then
            Response.Redirect("~/promotions")
        End If
        Return promotion
    End Function

    Private Function IsValidPromotionalCode(ByVal promotionalCode As String, ByVal control As Control) As Boolean
        If (String.IsNullOrEmpty(promotionalCode)) Then
            cvPromotionalCode.ErrorMessage = "*required"
            If Not (control Is Nothing) Then SetFieldColorAndSetFocus(control)
            Return False
        End If
        promotionalCode = DashValue(promotionalCode.Replace("-", String.Empty).Replace(" ", String.Empty))
        Dim regex As Regex = New Regex("^([A-Za-z0-9]{4})-([A-Za-z0-9]{4})-([A-Za-z0-9]{4})-([A-Za-z0-9]{4})$") '|([A-Za-z0-9]{4}) ([A-Za-z0-9]{4}) ([A-Za-z0-9]{4}) ([A-Za-z0-9]{4})|([A-Za-z0-9]{16})$")
        If Not (regex.IsMatch(promotionalCode)) Then
            cvPromotionalCode.ErrorMessage = "*invalid"
            If Not (control Is Nothing) Then SetFieldColorAndSetFocus(control)
            Return False
        End If
        Dim promotion As Promotion = ValidateQueryString()
        If Not (PromotionalBundleAccessCodeRepository.IsValidPromotionalBundleAccessCode(ConfigurationManager.AppSettings("ConnectionString").ToString(), promotionalCode, promotion.PromoId)) Then
            cvPromotionalCode.ErrorMessage = "*invalid"
            If Not (control Is Nothing) Then SetFieldColorAndSetFocus(control)
            Return False
        End If
        Return True
    End Function

    Private Sub BindBundles()
        Dim bundles As List(Of PromotionalBundle) = PromotionalBundleRepository.GetPromotionalBundles(ConfigurationManager.AppSettings("ConnectionString").ToString(), Session("PromotionalBundleAccessCode").ToString())
        If (bundles.Count = 1) Then
            hdn.Value = bundles.FirstOrDefault().PromotionalBundlePkId.ToString()
            tblBundled.Visible = False
            trBundled.Visible = False
            Return
        End If

        trBundled.Visible = True
        tblBundled.Visible = True
        Dim tr As TableRow = New TableRow()
        Dim counter As Int32 = 0
        For Each bundle In bundles
            If (counter = 5) Then
                tblPromotion.Controls.Add(tr)
                tr = New TableRow()
            End If
            Dim td As TableCell = New TableCell()
            td.VerticalAlign = VerticalAlign.Top
            td.Controls.Add(GenerateBundleTable(bundle))
            tr.Controls.Add(td)
            counter += 1
        Next
        tblPromotion.Controls.Add(tr)
    End Sub

    Private Function GenerateBundleTable(ByVal promotionalBundle As PromotionalBundle) As Table
        Dim tbl As Table = New Table()
        tbl.Width = 175
        tbl.Controls.Add(GenerateBundleHeaderTableRow(promotionalBundle))
        tbl.Controls.Add(GenerateBundleContentTableRow(promotionalBundle.PromotionalBundlePkId))
        Return tbl
    End Function

    Private Function GenerateBundleHeaderTableRow(ByVal promotionalBundle As PromotionalBundle) As TableRow
        Dim tr As TableRow = New TableRow()
        Dim tdRadioButton As TableCell = New TableCell()
        tdRadioButton.Attributes.Add("style", "background-color: #008ecd;")
        tdRadioButton.HorizontalAlign = HorizontalAlign.Center
        tdRadioButton.Width = 20
        Dim rb As HtmlInputRadioButton = New HtmlInputRadioButton()
        rb.Attributes.Add("onclick", String.Format("UpdateRadioButtonOption('{0}')", promotionalBundle.PromotionalBundlePkId.ToString()))
        rb.ID = String.Format("rbSelection_{0}", promotionalBundle.PromotionalBundlePkId.ToString())
        rb.Name = "BundleGroup"
        rb.Value = promotionalBundle.PromotionalBundlePkId.ToString()
        If Not (String.IsNullOrEmpty(hdn.Value)) Then
            Dim testVal As Int32 = 0
            If (Int32.TryParse(hdn.Value, testVal)) Then
                If (String.Compare(hdn.Value, promotionalBundle.PromotionalBundlePkId.ToString(), True) = 0) Then rb.Checked = True
            End If
        End If
        tdRadioButton.Controls.Add(rb)
        tr.Controls.Add(tdRadioButton)
        Dim tdText As TableCell = New TableCell()
        tdText.Attributes.Add("style", "background-color: #cccccc; font-size: 14pt;")
        tdText.HorizontalAlign = HorizontalAlign.Center
        tdText.Text = promotionalBundle.Name
        tr.Controls.Add(tdText)
        Return tr
    End Function

    Private Function GenerateBundleContentTableRow(ByVal pkId As Int32) As TableRow
        Dim tr As TableRow = New TableRow()
        Dim promotionalBundleDetails As List(Of PromotionalBundleDetail) = PromotionalBundleDetailRepository.GetPromotionalBundleDetails(ConfigurationManager.AppSettings("ConnectionString").ToString(), pkId).Where(Function(x) x.IsActive).OrderBy(Function(x) x.SortOrder).ToList()
        Dim td As TableCell = New TableCell()
        td.Attributes.Add("style", "padding: 5px;")
        td.ColumnSpan = 2
        For Each pbd In promotionalBundleDetails
            Dim hlk As HyperLink = New HyperLink()
            hlk.NavigateUrl = String.Format("~/{0}", pbd.Url)
            hlk.Target = "_blank"
            hlk.Text = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), pbd.ProductFkId).Name
            td.Controls.Add(hlk)
            Dim lit As Literal = New Literal()
            lit.Text = "<br />"
            td.Controls.Add(lit)
        Next
        tr.Controls.Add(td)
        Return tr
    End Function

    Private Sub ResetFields(ByVal txtBoxes As List(Of TextBox))
        For Each txtBox As TextBox In txtBoxes
            txtBox.BackColor = Color.White
        Next
    End Sub

    Private Sub ResetPage()
        pnlSubmitted.Visible = False
        pnlSerialNumber.Visible = False
        pnlPromotionalCode.Visible = True
    End Sub

    Private Sub ClearLocalSessionVariables()
        Session("PromotionalBundleAccessCode") = Nothing
        Session.Remove("PromotionalBundleAccessCode")
        Session("RedirectTo") = Nothing
        Session.Remove("RedirectTo")
    End Sub

    Private Sub SetFieldColorAndSetFocus(ByVal control As Control)
        If (String.Compare(control.GetType().ToString(), "System.Web.UI.WebControls.TextBox", True) = 0) Then
            Dim ctl As TextBox = CType(control, TextBox)
            ctl.BackColor = ColorTranslator.FromHtml("#ff8888")
        Else
            Dim ctl As DropDownList = CType(control, DropDownList)
            ctl.BackColor = ColorTranslator.FromHtml("#ff8888")
        End If
    End Sub

    Private Sub HandleLoginRedirectIfRequired()
        Dim promotion As Promotion = ValidateQueryString()
        Session("RedirectTo") = String.Format("{0}/redeem/default.aspx?promotionid={1}", rootDir, promotion.PromoId)
        If Not (ValidateUser()) Then
            Response.Redirect(rootDir + "/support/user/")
        End If
        ValidateUserNoRedir()
    End Sub

    Private Function DashValue(ByVal inValue As String) As String
        Dim retVal As String = String.Empty
        Dim counter As Int32 = 0
        For Each c In inValue
            If (counter = 4) Then
                retVal += "-"
                counter = 0
            End If
            retVal += c
            counter += 1
        Next
        Return retVal
    End Function

    Private Sub SendEmailToCSRs(ByVal contact As Contact, ByVal requestId As Int32, ByVal scopeId As String, ByVal serialNumber As String, ByVal promotionalAccessCode As String, ByVal promotionalBundleName As String, ByVal enteredDate As DateTime)
        Dim sb As StringBuilder = New StringBuilder()
        sb.AppendLine("CUSTOMER DATA")
        sb.AppendLine("******************************************")
        sb.AppendLine(String.Format("Name: {0} {1}", contact.FirstName, contact.LastName))
        sb.AppendLine(String.Format("Email: {0}", contact.Email))
        sb.AppendLine(String.Format("Phone: {0}", contact.Phone))
        sb.AppendLine(String.Format("Address: {0}", contact.Address))
        If Not (String.IsNullOrEmpty(contact.Address2)) Then sb.AppendLine(String.Format("Address 2: {0}", contact.Address2))
        sb.AppendLine(String.Format("City: {0}", contact.City))
        sb.AppendLine(String.Format("State/Province: {0}", contact.StateProvince))
        sb.AppendLine(String.Format("Postal Code: {0}", contact.PostalCode))
        sb.AppendLine(String.Format("Company: {0}", contact.Company))
        sb.AppendLine("")
        sb.AppendLine("PROMOTIONAL BUNDLE DATA")
        sb.AppendLine("******************************************")
        sb.AppendLine(String.Format("Request ID: {0}", requestId.ToString()))
        sb.AppendLine(String.Format("Scope ID: {0}", scopeId))
        sb.AppendLine(String.Format("Serial Number: {0}", serialNumber))
        sb.AppendLine(String.Format("Promo Access Number: {0}", promotionalAccessCode))
        sb.AppendLine(String.Format("Requested Bundle: {0}", promotionalBundleName))
        sb.AppendLine(String.Format("Date Entered: {0}", enteredDate.ToString()))
        sb.AppendLine("")
        sb.AppendLine("******************************************")
        Utilities.SendEmail(New List(Of String)({ConfigurationManager.AppSettings("PromoRedeemCsrEmailRecipients")}), "webmaster@teledynelecroy.com", "Teledyne LeCroy Promotion Redeem Request", sb.ToString(), New List(Of String), New List(Of String))
    End Sub

    Private Sub SendConfirmationEmailToEndUser(ByVal contact As Contact, ByVal scopeId As String, ByVal serialNumber As String, ByVal promotionalAccessCode As String, ByVal promotionalBundleName As String, ByVal enteredDate As DateTime, ByVal localeId As Int32)
        Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
        replacements.Add("<#FirstName#>", contact.FirstName)
        replacements.Add("<#LastName#>", contact.LastName)
        replacements.Add("<#EmailAddress#>", contact.Email)
        replacements.Add("<#PhoneNumber#>", contact.Phone)
        replacements.Add("<#Address1#>", contact.Address)
        replacements.Add("<#City#>", contact.City)
        replacements.Add("<#State#>", contact.StateProvince)
        replacements.Add("<#PostalCode#>", contact.PostalCode)
        replacements.Add("<#CompanyName#>", contact.Company)
        replacements.Add("<#ScopeId#>", scopeId)
        replacements.Add("<#SerialNumber#>", serialNumber)
        replacements.Add("<#PromotionalCodeNumber#>", promotionalAccessCode)
        replacements.Add("<#DateEntered#>", enteredDate.ToString())
        replacements.Add("<#ContactOfficeUrl#>", String.Format("{0}/support/contact/", ConfigurationManager.AppSettings("CorporateBaseUrl")))
        replacements.Add("<#PromotionBundleName#>", promotionalBundleName)
        FeMailManager.Send_EmailWithReplacementParams(ConfigurationManager.AppSettings("ConnectionString").ToString(), 26, localeId, contact.Email, replacements)
    End Sub
#End Region
End Class