﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="compare-discontinued.aspx.vb" Inherits="LeCroy.Website.Resources_Compare_Discontinued" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; width: 951px;">
        <span class="pad10">Back to </span><a href="../../oscilloscope/default.aspx">Oscilloscopes</a><asp:Literal ID="litSpacer" runat="server" /><asp:HyperLink ID="hlkBreadCrumb" runat="server" /><asp:Literal ID="litSpacer2" runat="server" /><asp:HyperLink ID="hlkBreadCrumb2" runat="server" /><br />
        <table border="0" cellspacing="0" cellpadding="0" class="compare_options" width="100%">
            <tr>
                <td width="13%" valign="top">
                    <strong>Compare between:</strong><br />
                    <asp:RadioButtonList ID="rblCompareBy" runat="server" RepeatColumns="1" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Selected="true" Text="Series" Value="0" />
                        <asp:ListItem Text="Models" Value="1" />
                    </asp:RadioButtonList>
                    <div class="compare">
                        <p><asp:Button ID="btnCompare" runat="server" Text="Compare" /></p>
                    </div>
                    <asp:HiddenField ID="hdnCallingPage" runat="server" />
                    <asp:HiddenField ID="hdnModelsForProductSeries" runat="server" />
                    <asp:HiddenField ID="hdnImagesForProductSeries" runat="server" />
                    <asp:HiddenField ID="hdnProductId1" runat="server" />
                    <asp:HiddenField ID="hdnProductId2" runat="server" />
                    <asp:HiddenField ID="hdnProductId3" runat="server" />
                </td>
                <td width="29%" valign="top" align="center">
                    <asp:DropDownList ID="ddlProductSeries1" runat="server" CssClass="compare_selection" Width="200" />
                    <div id="divModel1" style="display: none;">
                        <br /><select id="ddlProduct1" name="ddlProduct1" runat="server" class="compare_selection" /><br /><br />
                    </div><br />
                    <asp:HyperLink ID="hlkImage1" runat="server"><asp:Image ID="imgProduct1" runat="server" BorderStyle="None" /></asp:HyperLink><br />
                    <strong><asp:HyperLink ID="hlkTitle1" runat="server" Text="" /></strong>
                </td>
                <td width="29%" valign="top" align="center">
                    <asp:DropDownList ID="ddlProductSeries2" runat="server" CssClass="compare_selection" Width="200" />
                    <div id="divModel2" style="display: none;">
                        <br /><select id="ddlProduct2" name="ddlProduct2" runat="server" class="compare_selection" /><br /><br />
                    </div><br />
                    <asp:HyperLink ID="hlkImage2" runat="server"><asp:Image ID="imgProduct2" runat="server" BorderStyle="None" /></asp:HyperLink><br />
                    <strong><asp:HyperLink ID="hlkTitle2" runat="server" Text="" /></strong>
                </td>
                <td width="29%" valign="top" align="center">
                    <asp:DropDownList ID="ddlProductSeries3" runat="server" CssClass="compare_selection" Width="200" />
                    <div id="divModel3" style="display: none;">
                        <br /><select id="ddlProduct3" name="ddlProduct3" runat="server" class="compare_selection" /><br /><br />
                    </div><br />
                    <asp:HyperLink ID="hlkImage3" runat="server"><asp:Image ID="imgProduct3" runat="server" BorderStyle="None" /></asp:HyperLink><br />
                    <strong><asp:HyperLink ID="hlkTitle3" runat="server" Text="" /></strong>
                </td>
            </tr>
        </table>
        <div class="compare_specs">
            <asp:Label ID="lbCompare" runat="server" Text="" /><br />
        </div>
    </div>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $('#<%= ddlProductSeries1.ClientID %>').change(function () {
                if (($(this).get(0).selectedIndex) == 0) {
                    Clear('<%= ddlProduct1.ClientID %>', '<%= hdnProductId1.ClientID %>', '<%= imgProduct1.ClientID %>', '<%= hlkTitle1.ClientID %>', '<%= hlkImage1.ClientID %>');
                }
                else {
                    GetModelsForSelectedSeries('<%= ddlProduct1.ClientID %>', $(this).val());
                    GetImagesForSelectedSeries('<%= imgProduct1.ClientID %>', $(this).val());
                    SetHyperlink($('#<%= hlkTitle1.ClientID %>'), $('#<%= hlkImage1.ClientID %>'), $('#<%= ddlProductSeries1.ClientID %> option:selected').text(), $(this).val(), false);
                }
            });
            $('#<%= ddlProductSeries2.ClientID %>').change(function () {
                if (($(this).get(0).selectedIndex) == 0) {
                    Clear('<%= ddlProduct2.ClientID %>', '<%= hdnProductId2.ClientID %>', '<%= imgProduct2.ClientID %>', '<%= hlkTitle2.ClientID %>', '<%= hlkImage2.ClientID %>');
                }
                else {
                    GetModelsForSelectedSeries('<%= ddlProduct2.ClientID %>', $(this).val());
                    GetImagesForSelectedSeries('<%= imgProduct2.ClientID %>', $(this).val());
                    SetHyperlink($('#<%= hlkTitle2.ClientID %>'), $('#<%= hlkImage2.ClientID %>'), $('#<%= ddlProductSeries2.ClientID %> option:selected').text(), $(this).val(), false);
                }
            });
            $('#<%= ddlProductSeries3.ClientID %>').change(function () {
                if (($(this).get(0).selectedIndex) == 0) {
                    Clear('<%= ddlProduct3.ClientID %>', '<%= hdnProductId3.ClientID %>', '<%= imgProduct3.ClientID %>', '<%= hlkTitle3.ClientID %>', '<%= hlkImage2.ClientID %>');
                }
                else {
                    GetModelsForSelectedSeries('<%= ddlProduct3.ClientID %>', $(this).val());
                    GetImagesForSelectedSeries('<%= imgProduct3.ClientID %>', $(this).val());
                    SetHyperlink($('#<%= hlkTitle3.ClientID %>'), $('#<%= hlkImage3.ClientID %>'), $('#<%= ddlProductSeries3.ClientID %> option:selected').text(), $(this).val(), false);
                }
            });
            $('#<%= ddlProduct1.ClientID %>').change(function () {
                SetHiddenProductIdField($(this), $('#<%= hdnProductId1.ClientID %>'));
            });
            $('#<%= ddlProduct2.ClientID %>').change(function () {
                SetHiddenProductIdField($(this), $('#<%= hdnProductId2.ClientID %>'));
            });
            $('#<%= ddlProduct3.ClientID %>').change(function () {
                SetHiddenProductIdField($(this), $('#<%= hdnProductId3.ClientID %>'));
            });
            $('#<%= rblCompareBy.ClientID %>').change(function () {
                var selectedValue = $('#<%= rblCompareBy.ClientID %> input:checked').val();
                if (selectedValue == "0") {
                    SetDivModelVisibility(false);
                }
                else {
                    SetDivModelVisibility(true);
                    GetModelsForSelectedSeries('<%= ddlProduct1.ClientID %>', $('#<%= ddlProductSeries1.ClientID %>').val());
                    GetModelsForSelectedSeries('<%= ddlProduct2.ClientID %>', $('#<%= ddlProductSeries2.ClientID %>').val());
                    GetModelsForSelectedSeries('<%= ddlProduct3.ClientID %>', $('#<%= ddlProductSeries3.ClientID %>').val());
                }
            });
            if ($('#<%= ddlProduct1.ClientID %> option').size() <= 0) {
                BindEmptyModelDropDowns('<%= ddlProduct1.ClientID %>');
            }
            if ($('#<%= ddlProduct2.ClientID %> option').size() <= 0) {
                BindEmptyModelDropDowns('<%= ddlProduct2.ClientID %>');
            }
            if ($('#<%= ddlProduct3.ClientID %> option').size() <= 0) {
                BindEmptyModelDropDowns('<%= ddlProduct3.ClientID %>');
            }
            var rbl = $('#<%= rblCompareBy.ClientID %> input:checked').val();
            if (rbl == "0") {
                SetDivModelVisibility(false);
            }
            else {
                SetDivModelVisibility(true);
                GetModelsForSelectedSeries('<%= ddlProduct1.ClientID %>', $('#<%= ddlProductSeries1.ClientID %>').val())
                if ($('#<%= hdnProductId1.ClientID %>').val() != '') {
                    $('#<%= ddlProduct1.ClientID %>').val($('#<%= hdnProductId1.ClientID %>').val());
                }
                GetModelsForSelectedSeries('<%= ddlProduct2.ClientID %>', $('#<%= ddlProductSeries2.ClientID %>').val())
                if ($('#<%= hdnProductId2.ClientID %>').val() != '') {
                    $('#<%= ddlProduct2.ClientID %>').val($('#<%= hdnProductId2.ClientID %>').val());
                }
                GetModelsForSelectedSeries('<%= ddlProduct3.ClientID %>', $('#<%= ddlProductSeries3.ClientID %>').val())
                if ($('#<%= hdnProductId3.ClientID %>').val() != '') {
                    $('#<%= ddlProduct3.ClientID %>').val($('#<%= hdnProductId3.ClientID %>').val());
                }
            }
        });

        function Clear(ddl, hdn, img, hlk, hlk2) {
            document.getElementById(ddl).options.length = 0;
            ClearProductIdField(jQuery(hdn));
            BindEmptyModelDropDowns(ddl);
            ClearImage(img);
            SetHyperlink(jQuery('#' + hlk), jQuery('#' + hlk2), '', '', true);
        }

        function SetDivModelVisibility(show) {
            if (!show) {
                jQuery('#divModel1').hide();
                jQuery('#divModel2').hide();
                jQuery('#divModel3').hide();
            }
            else {
                jQuery('#divModel1').show();
                jQuery('#divModel2').show();
                jQuery('#divModel3').show();
            }
        }

        function SetHiddenProductIdField(ddl, hdnField) {
            if ((jQuery(ddl).get(0).selectedIndex) == 0) {
                ClearProductIdField(hdnField);
            }
            else {
                jQuery(hdnField).val(jQuery(ddl).val());
            }
        }

        function SetHyperlink(hlk, hlk2, text, productSeriesId, isClear) {
            if (isClear) {
                jQuery(hlk).text('');
                jQuery(hlk).attr('href', '');
                jQuery(hlk2).attr('href', '');
            }
            else {
                jQuery(hlk).text(text);
                jQuery(hlk).attr('href', '../oscilloscope/oscilloscopeseries.aspx?mseries=' + productSeriesId);
                jQuery(hlk2).attr('href', '../oscilloscope/oscilloscopeseries.aspx?mseries=' + productSeriesId);
            }
        }

        function ClearProductIdField(hdnField) {
            jQuery(hdnField).val('');
        }

        function GetModelsForSelectedSeries(ddl, productSeriesId) {
            var modelString = jQuery('#<%= hdnModelsForProductSeries.ClientID %>').val();
            var seriesSplit = modelString.split('|');
            for (i = 0; i < seriesSplit.length; i++) {
                var modelSplit = seriesSplit[i].split('^');
                if (modelSplit[0] == productSeriesId) {
                    document.getElementById(ddl).options.length = 0;
                    var models = modelSplit[1].split(',');
                    var x = document.getElementById(ddl);
                    try {
                        document.getElementById(ddl).options[0] = new Option();
                        document.getElementById(ddl).options[0].value = '';
                        document.getElementById(ddl).options[0].text = 'Select Model';
                    }
                    catch (e) {
                    }
                    for (j = 0; j < models.length; j++) {
                        var products = models[j].split('*');
                        var productId = products[0];
                        var partNumber = products[1];
                        document.getElementById(ddl).options[j + 1] = new Option();
                        document.getElementById(ddl).options[j + 1].value = productId
                        document.getElementById(ddl).options[j + 1].text = partNumber;
                    }
                }
            }
        }

        function GetImagesForSelectedSeries(img, productSeriesId) {
            var seriesString = jQuery('#<%= hdnImagesForProductSeries.ClientID %>').val();
            var seriesSplit = seriesString.split('|');
            for (i = 0; i < seriesSplit.length; i++) {
                var series = seriesSplit[i].split('^');
                if (series[0] == productSeriesId) {
                    document.getElementById(img).src = 'https://teledynelecroy.com' + series[1];
                }
            }
        }

        function ClearImage(img) {
            document.getElementById(img).src = 'https://teledynelecroy.com/images/spacer.gif';
        }

        function BindEmptyModelDropDowns(ddl) {
            var x = document.getElementById(ddl);
            try {
                document.getElementById(ddl).options[0] = new Option();
                document.getElementById(ddl).options[0].value = '';
                document.getElementById(ddl).options[0].text = 'Select Model';
            }
            catch (e) {
            }
        }
</script>
</asp:Content>