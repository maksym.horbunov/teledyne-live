﻿Imports System.Data.SqlClient
Imports LeCroy.Library.VBUtilities

Public Class Resources_Compare_Discontinued
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Master.PageContentHeader.TopText = "Product Comparison"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            hdnCallingPage.Value = Request.Form("hdnCallingPage")
            BindProductSeriesInformation(Request.Form("mseries1"), Request.Form("mseries2"), Request.Form("mseries3"))
            If (BindModelInformation(Request.Form("modelid1"), Request.Form("modelid2"), Request.Form("modelid3"))) Then
                btnCompare_Click(Nothing, Nothing)
                BuildCrumber()
            Else
                BindSeriesSpecs()
                BuildCrumber()
            End If
        Else
            BindProductSeriesInformation(ddlProductSeries1.SelectedValue, ddlProductSeries2.SelectedValue, ddlProductSeries3.SelectedValue)
            If (BindModelInformation(hdnProductId1.Value, hdnProductId2.Value, hdnProductId3.Value)) Then
                btnCompare_Click(Nothing, Nothing)
                BuildCrumber()
            Else
                BindSeriesSpecs()
                BuildCrumber()
            End If
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub btnCompare_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCompare.Click
        If (String.Compare(rblCompareBy.SelectedValue, "1", True) = 0) Then
            Dim productSeriesAndProductSetCorrectly As Boolean = False
            If (ddlProductSeries1.SelectedIndex > 0) Then
                If Not (String.IsNullOrEmpty(hdnProductId1.Value)) Then
                    productSeriesAndProductSetCorrectly = True
                End If
            End If
            If (ddlProductSeries2.SelectedIndex > 0) Then
                If Not (String.IsNullOrEmpty(hdnProductId2.Value)) Then
                    productSeriesAndProductSetCorrectly = True
                End If
            End If
            If (ddlProductSeries3.SelectedIndex > 0) Then
                If Not (String.IsNullOrEmpty(hdnProductId3.Value)) Then
                    productSeriesAndProductSetCorrectly = True
                End If
            End If

            If (productSeriesAndProductSetCorrectly) Then
                BindProductSpecs()
                Return
            End If
        End If
        BindSeriesSpecs()
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindProductSeriesInformation(ByVal productSeriesId1 As String, ByVal productSeriesId2 As String, ByVal productSeriesId3 As String)
        Dim sqlString As String = String.Empty
        'Dim tableToUse As String = GetTableToUse()
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        'If (String.Compare(tableToUse, "PRODUCTPROPVALUE_COMPARE", True) = 0) Then
        '    sqlString = "SELECT DISTINCT ps.PRODUCT_SERIES_ID, ps.NAME, ps.SHORT_NAME, ISNULL(ps.PRODUCTIMAGE, '') AS [PRODUCTIMAGE], ps.SORTID FROM PRODUCT_SERIES_COMPARE psc, PRODUCT_SERIES ps WHERE(psc.ProductSeriesFkId1 = ps.PRODUCT_SERIES_ID OR psc.ProductSeriesFkId2 = ps.PRODUCT_SERIES_ID) ORDER BY ps.SORTID"
        'Else
        'Dim disabledProductContent As Tuple(Of String, SqlParameter) = Functions.GetDisabledProductSQL(Functions.ValidateIntegerAndGetDefaultLocaleIdIfNeededFromSession(Session("localeid")).ToString())
        'sqlString = String.Format("SELECT DISTINCT b.* FROM PRODUCT a, PRODUCT_SERIES b, product_series_category c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID=@CATEGORYID AND a.DISABLED = 'Y' AND a.PRODUCTID NOT IN ({0}) ORDER BY b.SORTID", disabledProductContent.Item1)
        sqlString = "SELECT DISTINCT b.* FROM PRODUCT a, PRODUCT_SERIES b, product_series_category c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID=@CATEGORYID AND a.DISABLED = 'Y' ORDER BY b.SORTID"
        sqlParameters.Add(New SqlParameter("@CATEGORYID", AppConstants.CAT_ID_SCOPE))
        'If Not (String.IsNullOrEmpty(disabledProductContent.Item2.ParameterName)) Then sqlParameters.Add(disabledProductContent.Item2)
        'End If
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0) Then
            ClearProductSeriesInformation()
            Dim uniqueIds As List(Of Int32) = New List(Of Int32)
            Dim sbProductData As StringBuilder = New StringBuilder()
            Dim sbImageData As StringBuilder = New StringBuilder()
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not (uniqueIds.Contains(Int32.Parse(dr("PRODUCT_SERIES_ID").ToString()))) Then
                    ddlProductSeries1.Items.Add(New ListItem(dr("SHORT_NAME").ToString() + "-" + dr("PRODUCT_SERIES_ID").ToString(), dr("PRODUCT_SERIES_ID").ToString()))
                    ddlProductSeries2.Items.Add(New ListItem(dr("SHORT_NAME").ToString() + "-" + dr("PRODUCT_SERIES_ID").ToString(), dr("PRODUCT_SERIES_ID").ToString()))
                    ddlProductSeries3.Items.Add(New ListItem(dr("SHORT_NAME").ToString() + "-" + dr("PRODUCT_SERIES_ID").ToString(), dr("PRODUCT_SERIES_ID").ToString()))
                    sbProductData.Append(String.Format("{0}^{1}|", dr("PRODUCT_SERIES_ID").ToString(), GetModelIdsAsParsableString(Int32.Parse(dr("PRODUCT_SERIES_ID").ToString()))))
                    sbImageData.Append(String.Format("{0}^{1}|", dr("PRODUCT_SERIES_ID").ToString(), dr("PRODUCTIMAGE").ToString()))
                End If
            Next

            ddlProductSeries1.Items.Insert(0, New ListItem("Select Series", String.Empty))
            ddlProductSeries2.Items.Insert(0, New ListItem("Select Series", String.Empty))
            ddlProductSeries3.Items.Insert(0, New ListItem("Select Series", String.Empty))

            If (sbProductData.ToString().Length > 0) Then
                Dim s As String = sbProductData.ToString()
                hdnModelsForProductSeries.Value = s.Substring(0, s.Length - 1)
            End If
            If (sbImageData.ToString().Length > 0) Then
                Dim s As String = sbImageData.ToString()
                hdnImagesForProductSeries.Value = s.Substring(0, s.Length - 1)
            End If
            If Not (String.IsNullOrEmpty(productSeriesId1)) Then
                ddlProductSeries1.SelectedValue = productSeriesId1
                Dim drs As DataRow() = ds.Tables(0).Select(String.Format("PRODUCT_SERIES_ID = {0}", productSeriesId1))
                If (drs.Length > 0) Then
                    imgProduct1.ImageUrl = String.Format("{0}{1}", rootDir, drs(0)("PRODUCTIMAGE").ToString())
                    hlkTitle1.Text = drs(0)("NAME").ToString()
                    hlkTitle1.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", drs(0)("PRODUCT_SERIES_ID").ToString())
                    hlkImage1.NavigateUrl = hlkTitle1.NavigateUrl
                End If
            End If
            If Not (String.IsNullOrEmpty(productSeriesId2)) Then
                ddlProductSeries2.SelectedValue = productSeriesId2
                Dim drs As DataRow() = ds.Tables(0).Select(String.Format("PRODUCT_SERIES_ID = {0}", productSeriesId2))
                If (drs.Length > 0) Then
                    imgProduct2.ImageUrl = String.Format("{0}{1}", rootDir, drs(0)("PRODUCTIMAGE").ToString())
                    hlkTitle2.Text = drs(0)("NAME").ToString()
                    hlkTitle2.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", drs(0)("PRODUCT_SERIES_ID").ToString())
                    hlkImage2.NavigateUrl = hlkTitle2.NavigateUrl
                End If
            End If
            If Not (String.IsNullOrEmpty(productSeriesId3)) Then
                ddlProductSeries3.SelectedValue = productSeriesId3
                Dim drs As DataRow() = ds.Tables(0).Select(String.Format("PRODUCT_SERIES_ID = {0}", productSeriesId3))
                If (drs.Length > 0) Then
                    imgProduct3.ImageUrl = String.Format("{0}{1}", rootDir, drs(0)("PRODUCTIMAGE").ToString())
                    hlkTitle3.Text = drs(0)("NAME").ToString()
                    hlkTitle3.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", drs(0)("PRODUCT_SERIES_ID").ToString())
                    hlkImage3.NavigateUrl = hlkTitle3.NavigateUrl
                End If
            End If
        End If
    End Sub

    Private Function GetModelIdsAsParsableString(ByVal productSeriesId As Int32) As String
        Dim retVal As String = String.Empty
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
        sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTSERIESID", productSeriesId.ToString()))
        Dim sqlString As String = String.Empty
        If (productSeriesId = 484) Or (productSeriesId = 332) Then     'Big hack
            sqlString = String.Format("SELECT DISTINCT d.* FROM PRODUCT a, CONFIG b, PRODUCT_SERIES_CATEGORY c, PRODUCT d WHERE a.PRODUCTID = c.PRODUCT_ID and b.OPTIONID = d.PRODUCTID AND a.PRODUCTID = b.PRODUCTID AND c.CATEGORY_ID={0} AND c.PRODUCT_SERIES_ID={1} AND d.GROUP_ID = 179 and d.disabled='Y' and a.disabled='Y' order by d.sort_id", AppConstants.CAT_ID_SCOPE, productSeriesId)
        Else
            sqlString = String.Format("SELECT DISTINCT a.* FROM PRODUCT a, PRODUCT_SERIES b, PRODUCT_SERIES_category c WHERE a.PRODUCTID = c.PRODUCT_ID AND b.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID AND c.CATEGORY_ID={0} AND c.PRODUCT_SERIES_ID = @PRODUCTSERIESID AND a.DISABLED = 'Y' ORDER BY a.SORT_ID", AppConstants.CAT_ID_SCOPE)  ' Removed AND a.PRODUCTID NOT IN ({1})  , GetDisabledProductSQL(localeid)
        End If
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlString, sqlParameters.ToArray())
        If (ds.Tables.Count > 0) Then
            If (ds.Tables(0).Rows.Count > 0) Then
                Dim sb As StringBuilder = New StringBuilder()
                For Each dr As DataRow In ds.Tables(0).Rows
                    sb.Append(String.Format("{0}*{1},", dr("PRODUCTID").ToString(), dr("NAME").ToString()))
                Next
                retVal = sb.ToString()
                If (retVal.Length > 0) Then retVal = retVal.Substring(0, retVal.Length - 1)
            End If
        End If
        Return retVal
    End Function

    Private Sub BindSeriesSpecs()
        Dim pnum As Integer
        Dim i As Integer

        Dim strCompare As String = String.Empty
        Dim subQuery1 As String = String.Empty
        Dim subQuery2 As String = String.Empty
        Dim subQuery3 As String = String.Empty
        Dim paramString As String = String.Empty
        Dim sqlParameters As List(Of System.Data.SqlClient.SqlParameter) = New List(Of System.Data.SqlClient.SqlParameter)
        If Not (String.IsNullOrEmpty(ddlProductSeries1.SelectedValue)) Then
            Dim productSeriesId As Int32 = -1
            If Not (Int32.TryParse(ddlProductSeries1.SelectedValue, productSeriesId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTSERIESID1", SQLStringWithOutSingleQuotes(ddlProductSeries1.SelectedValue)))
            subQuery1 = ", (SELECT DISTINCT cc.PROPERTY_VALUE FROM PRODUCT_SERIES aa, PROPERTY bb, SERIES_PROPERTY cc WHERE aa.PRODUCT_SERIES_ID = cc.PRODUCT_SERIES_ID AND bb.PROPERTYID = cc.PROPERTY_ID AND aa.PRODUCT_SERIES_ID = @PRODUCTSERIESID1 AND b.PROPERTYID=bb.PROPERTYID and cc.PROPERTY_VALUE>' ') AS series1"
            paramString += "@PRODUCTSERIESID1,"
        End If
        If Not (String.IsNullOrEmpty(ddlProductSeries2.SelectedValue)) Then
            Dim productSeriesId As Int32 = -1
            If Not (Int32.TryParse(ddlProductSeries2.SelectedValue, productSeriesId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTSERIESID2", SQLStringWithOutSingleQuotes(ddlProductSeries2.SelectedValue)))
            subQuery2 = ", (SELECT DISTINCT cc.PROPERTY_VALUE FROM PRODUCT_SERIES aa, PROPERTY bb, SERIES_PROPERTY cc WHERE aa.PRODUCT_SERIES_ID = cc.PRODUCT_SERIES_ID AND bb.PROPERTYID = cc.PROPERTY_ID AND aa.PRODUCT_SERIES_ID = @PRODUCTSERIESID2 AND b.PROPERTYID=bb.PROPERTYID and cc.PROPERTY_VALUE>' ') AS series2"
            paramString += "@PRODUCTSERIESID2,"
        End If
        If Not (String.IsNullOrEmpty(ddlProductSeries3.SelectedValue)) Then
            Dim productSeriesId As Int32 = -1
            If Not (Int32.TryParse(ddlProductSeries3.SelectedValue, productSeriesId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTSERIESID3", SQLStringWithOutSingleQuotes(ddlProductSeries3.SelectedValue)))
            subQuery3 = ", (SELECT DISTINCT cc.PROPERTY_VALUE FROM PRODUCT_SERIES aa, PROPERTY bb, SERIES_PROPERTY cc WHERE aa.PRODUCT_SERIES_ID = cc.PRODUCT_SERIES_ID AND bb.PROPERTYID = cc.PROPERTY_ID AND aa.PRODUCT_SERIES_ID = @PRODUCTSERIESID3 AND b.PROPERTYID=bb.PROPERTYID and cc.PROPERTY_VALUE>' ') AS series3"
            paramString += "@PRODUCTSERIESID3,"
        End If
        If (paramString.Length > 0) Then
            paramString = paramString.Substring(0, paramString.Length - 1)
        End If

        Dim sqlGetSeries As String = String.Format("SELECT DISTINCT b.PROPERTYID, b.PROPERTYNAME, b.SORTID{0}{1}{2} FROM PRODUCT_SERIES AS a INNER JOIN SERIES_PROPERTY AS c ON a.PRODUCT_SERIES_ID = c.PRODUCT_SERIES_ID INNER JOIN PROPERTY AS b ON c.PROPERTY_ID = b.PROPERTYID WHERE a.PRODUCT_SERIES_ID IN ({3}) and c.PROPERTY_VALUE>' ' ORDER BY b.SORTID", subQuery1, subQuery2, subQuery3, paramString)
        strCompare = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>"
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetSeries, sqlParameters.ToArray())
        If (ds.Tables.Count > 0) Then
            pnum = ds.Tables(0).Rows.Count
            If pnum > 0 Then
                For i = 0 To pnum - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    Dim rowType As String = ""
                    If ((i Mod 2) = 0) Then
                        rowType = "compare_odd_row"
                    Else
                        rowType = "compare_even_row"
                    End If

                    Dim pSeries1 As String = "&nbsp;"
                    If (dr.Table.Columns.Contains("series1")) Then
                        pSeries1 = dr("series1").ToString()
                    End If
                    Dim pSeries2 As String = "&nbsp;"
                    If (dr.Table.Columns.Contains("series2")) Then
                        pSeries2 = dr("series2").ToString()
                    End If
                    Dim pSeries3 As String = "&nbsp;"
                    If (dr.Table.Columns.Contains("series3")) Then
                        pSeries3 = dr("series3").ToString()
                    End If

                    strCompare = strCompare + "<tr class='" + rowType + "'>"
                    strCompare = strCompare + "<td class='compare_cellTitle' width='13%'>" + dr("PROPERTYNAME") + "</td>"
                    strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + pSeries1 + "</td>"
                    strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + pSeries2 + "</td>"
                    strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + pSeries3 + "</td>"
                    strCompare = strCompare + "</tr>"
                Next
            End If
        End If
        strCompare = strCompare + "</table>"
        lbCompare.Text = strCompare
    End Sub

    Private Sub BindProductSpecs()
        If (String.IsNullOrEmpty(hdnProductId1.Value) And String.IsNullOrEmpty(hdnProductId2.Value) And String.IsNullOrEmpty(hdnProductId3.Value)) Then
            BindSeriesSpecs()
        End If
        Dim mnum As Integer
        Dim i As Integer

        Dim strCompare As String = String.Empty
        Dim subQuery1 As String = String.Empty
        Dim subQuery2 As String = String.Empty
        Dim subQuery3 As String = String.Empty
        Dim paramString As String = String.Empty
        Dim modelName1 As String = String.Empty
        Dim modelName2 As String = String.Empty
        Dim modelName3 As String = String.Empty
        Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
        If Not (String.IsNullOrEmpty(hdnProductId1.Value)) Then
            Dim productId As Int32 = -1
            If Not (Int32.TryParse(hdnProductId1.Value, productId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTID1", SQLStringWithOutSingleQuotes(hdnProductId1.Value)))
            subQuery1 = String.Format(", (SELECT DISTINCT cc.VALUE FROM PRODUCT aa, PROPERTY bb, {0} cc WHERE aa.PRODUCTID = cc.PRODUCTID AND bb.PROPERTYID = cc.PROPERTYID AND aa.PRODUCTID = @PRODUCTID1 AND b.PROPERTYID=bb.PROPERTYID and cc.VALUE>' ' AND cc.QUICK_SPEC_IND = 'N') AS model1", GetTableToUse())
            paramString += "@PRODUCTID1,"
            Dim productDataSet As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT [NAME] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", SQLStringWithOutSingleQuotes(hdnProductId1.Value))}).ToArray())
            If (productDataSet.Tables.Count > 0) Then
                If (productDataSet.Tables(0).Rows.Count > 0) Then
                    modelName1 = productDataSet.Tables(0).Rows(0)("Name").ToString()
                End If
            End If
        End If
        If Not (String.IsNullOrEmpty(hdnProductId2.Value)) Then
            Dim productId As Int32 = -1
            If Not (Int32.TryParse(hdnProductId2.Value, productId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTID2", SQLStringWithOutSingleQuotes(hdnProductId2.Value)))
            subQuery2 = String.Format(", (SELECT DISTINCT cc.VALUE FROM PRODUCT aa, PROPERTY bb, {0} cc WHERE aa.PRODUCTID = cc.PRODUCTID AND bb.PROPERTYID = cc.PROPERTYID AND aa.PRODUCTID = @PRODUCTID2 AND b.PROPERTYID=bb.PROPERTYID and cc.VALUE>' ' AND cc.QUICK_SPEC_IND = 'N') AS model2", GetTableToUse())
            paramString += "@PRODUCTID2,"
            Dim productDataSet As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT [NAME] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", SQLStringWithOutSingleQuotes(hdnProductId2.Value))}).ToArray())
            If (productDataSet.Tables.Count > 0) Then
                If (productDataSet.Tables(0).Rows.Count > 0) Then
                    modelName2 = productDataSet.Tables(0).Rows(0)("Name").ToString()
                End If
            End If
        End If
        If Not (String.IsNullOrEmpty(hdnProductId3.Value)) Then
            Dim productId As Int32 = -1
            If Not (Int32.TryParse(hdnProductId3.Value, productId)) Then Response.Redirect("https://teledynelecroy.com/")
            sqlParameters.Add(New System.Data.SqlClient.SqlParameter("@PRODUCTID3", SQLStringWithOutSingleQuotes(hdnProductId3.Value)))
            subQuery3 = String.Format(", (SELECT DISTINCT cc.VALUE FROM PRODUCT aa, PROPERTY bb, {0} cc WHERE aa.PRODUCTID = cc.PRODUCTID AND bb.PROPERTYID = cc.PROPERTYID AND aa.PRODUCTID = @PRODUCTID3 AND b.PROPERTYID=bb.PROPERTYID and cc.VALUE>' ' AND cc.QUICK_SPEC_IND = 'N') AS model3", GetTableToUse())
            paramString += "@PRODUCTID3,"
            Dim productDataSet As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), "SELECT [NAME] FROM [PRODUCT] WHERE [PRODUCTID] = @PRODUCTID", New List(Of SqlParameter)({New SqlParameter("@PRODUCTID", SQLStringWithOutSingleQuotes(hdnProductId3.Value))}).ToArray())
            If (productDataSet.Tables.Count > 0) Then
                If (productDataSet.Tables(0).Rows.Count > 0) Then
                    modelName3 = productDataSet.Tables(0).Rows(0)("Name").ToString()
                End If
            End If
        End If
        If (paramString.Length > 0) Then
            paramString = paramString.Substring(0, paramString.Length - 1)
        End If

        Dim sqlGetModels As String = String.Format("SELECT DISTINCT b.PROPERTYID, b.PROPERTYNAME, d.PROP_CAT_NAME, d.SORT_ID, b.SORTID{0}{1}{2} FROM PRODUCT AS a INNER JOIN {4} AS c ON a.PRODUCTID = c.PRODUCTID INNER JOIN PROPERTY AS b ON c.PROPERTYID = b.PROPERTYID INNER JOIN PROPERTY_CATEGORY AS d ON b.PROP_CAT_ID = d.PROP_CAT_ID WHERE a.PRODUCTID IN ({3}) and c.VALUE>' ' and c.QUICK_SPEC_IND = 'N' ORDER BY d.SORT_ID, b.SORTID", subQuery1, subQuery2, subQuery3, paramString, GetTableToUse())

        strCompare = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>"
        Dim ds As DataSet = DbHelperSQL.Query(ConfigurationManager.AppSettings("ConnectionString").ToString(), sqlGetModels, sqlParameters.ToArray())
        mnum = ds.Tables(0).Rows.Count
        If mnum > 0 Then
            strCompare = strCompare + String.Format("<tr><td class='compare_header_row' width='13%'>&nbsp;</td><td align='center' class='compare_header_row' width='29%'>{0}</td><td align='center' class='compare_header_row' width='29%'>{1}</td><td align='center' class='compare_header_row' width='29%'>{2}</td></tr>", modelName1, modelName2, modelName3)
            Dim currentPropCatName As String = String.Empty
            For i = 0 To mnum - 1
                Dim dr As DataRow = ds.Tables(0).Rows(i)
                Dim propCatName As String = dr("PROP_CAT_NAME").ToString()
                If Not (String.Compare(currentPropCatName, propCatName, True) = 0) Then
                    currentPropCatName = ds.Tables(0).Rows(i)("PROP_CAT_NAME").ToString()
                    strCompare = strCompare + String.Format("<tr><td class='compare_header_row' colspan='4'>{0}</td></tr>", currentPropCatName)
                End If
                Dim rowType As String = ""
                If ((i Mod 2) = 0) Then
                    rowType = "compare_odd_row"
                Else
                    rowType = "compare_even_row"
                End If

                Dim pModel1 As String = "&nbsp;"
                If (dr.Table.Columns.Contains("model1")) Then
                    If Not (String.IsNullOrEmpty(dr("model1").ToString().Trim())) Then
                        pModel1 = dr("model1").ToString()
                    End If
                End If
                Dim pModel2 As String = "&nbsp;"
                If (dr.Table.Columns.Contains("model2")) Then
                    If Not (String.IsNullOrEmpty(dr("model2").ToString().Trim())) Then
                        pModel2 = dr("model2").ToString()
                    End If
                End If
                Dim pModel3 As String = "&nbsp;"
                If (dr.Table.Columns.Contains("model3")) Then
                    If Not (String.IsNullOrEmpty(dr("model3").ToString().Trim())) Then
                        pModel3 = dr("model3").ToString()
                    End If
                End If

                strCompare = strCompare + "<tr class='" + rowType + "'>"
                strCompare = strCompare + "<td class='compare_cellTitle' width='13%'>" + dr("PROPERTYNAME") + "</td>"
                strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + Server.HtmlDecode(pModel1) + "</td>"
                strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + Server.HtmlDecode(pModel2) + "</td>"
                strCompare = strCompare + "<td align='center' class='compare_cell' width='29%'>" + Server.HtmlDecode(pModel3) + "</td>"
                strCompare = strCompare + "</tr>"
            Next
        End If
        strCompare = strCompare + "</table>"
        lbCompare.Text = strCompare
    End Sub

    Private Sub ClearProductSeriesInformation()
        ddlProductSeries1.Items.Clear()
        ddlProductSeries2.Items.Clear()
        ddlProductSeries3.Items.Clear()
        hdnModelsForProductSeries.Value = String.Empty
        hdnImagesForProductSeries.Value = String.Empty
        imgProduct1.ImageUrl = "~/images/spacer.gif"
        hlkImage1.NavigateUrl = String.Empty
        hlkTitle1.Text = String.Empty
        hlkTitle1.NavigateUrl = String.Empty
        imgProduct2.ImageUrl = "~/images/spacer.gif"
        hlkImage2.NavigateUrl = String.Empty
        hlkTitle2.Text = String.Empty
        hlkTitle2.NavigateUrl = String.Empty
        imgProduct3.ImageUrl = "~/images/spacer.gif"
        hlkImage3.NavigateUrl = String.Empty
        hlkTitle3.Text = String.Empty
        hlkTitle3.NavigateUrl = String.Empty
    End Sub

    Private Function BindModelInformation(ByVal productId1 As String, ByVal productId2 As String, ByVal productId3 As String) As Boolean
        Dim retVal As Boolean = False
        If Not (String.IsNullOrEmpty(productId1)) Then
            hdnProductId1.Value = productId1
            rblCompareBy.SelectedValue = "1"
            retVal = True
        End If
        If Not (String.IsNullOrEmpty(productId2)) Then
            hdnProductId2.Value = productId2
            rblCompareBy.SelectedValue = "1"
            retVal = True
        End If
        If Not (String.IsNullOrEmpty(productId3)) Then
            hdnProductId3.Value = productId3
            rblCompareBy.SelectedValue = "1"
            retVal = True
        End If
        Return retVal
    End Function

    Private Function GetTableToUse() As String
        'If Not (String.IsNullOrEmpty(hdnCallingPage.Value)) Then
        '    If ((String.Compare(hdnCallingPage.Value, "ScopeSeriesPage", True) = 0) Or (String.Compare(hdnCallingPage.Value, "ScopeIndexPage", True) = 0) Or (String.Compare(hdnCallingPage.Value, "ScopeModelPage", True) = 0) Or (String.Compare(hdnCallingPage.Value, "LabMasterPage", True) = 0)) Then
        Return "PRODUCTPROPVALUE"
        '    End If
        'End If
        'Return "PRODUCTPROPVALUE_COMPARE"
    End Function

    Private Sub BuildCrumber()
        litSpacer.Text = String.Empty
        litSpacer2.Text = String.Empty
        If Not (String.IsNullOrEmpty(hdnCallingPage.Value)) Then
            If (String.Compare(hdnCallingPage.Value, "ScopeSeriesPage", True) = 0) Then
                hlkBreadCrumb.Text = String.Format("{0}", Functions.GetSeriesNameonID(Request.Form("mseries1")))
                hlkBreadCrumb.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}&capid=102&mid=1017", Request.Form("mseries1"))
                litSpacer.Text = "&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;"
            End If
            If (String.Compare(hdnCallingPage.Value, "ScopeModelPage", True) = 0) Then
                hlkBreadCrumb.Text = String.Format("{0}", Functions.GetSeriesNameonID(Request.Form("mseries1")))
                hlkBreadCrumb.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}&capid=102&mid=1017", Request.Form("mseries1"))
                hlkBreadCrumb2.Text = String.Format("{0}", Functions.GetProductNameonID(Request.Form("modelid1")))
                hlkBreadCrumb2.NavigateUrl = String.Format("~/oscilloscope/oscilloscopemodel.aspx?modelid={0}&capid=102&mid=1017", Request.Form("modelid1"))
                litSpacer.Text = "&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;"
                litSpacer2.Text = "&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;"
            End If
            If (String.Compare(hdnCallingPage.Value, "LabMasterPage", True) = 0) Then
                hlkBreadCrumb.Text = "LabMaster Zi"
                hlkBreadCrumb.NavigateUrl = "~/labmaster/"
                litSpacer.Text = "&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;"
            End If
        Else    ' Not from /Oscilloscope/Oscilloscope*.aspx
            hlkBreadCrumb.Text = String.Format("&nbsp;<span style=""color: #000000;"">&gt;</span>&nbsp;{0}", Functions.GetSeriesNameonID(Request.Form("mseries1")))
            hlkBreadCrumb.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", Request.Form("mseries1"))
        End If
    End Sub
#End Region
End Class