﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.Resources_ResourcesDefault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="document-type">
        <table border="0" cellpadding="2" cellspacing="0" style="background-color: #ffffff;" width="100%">
            <tr>
                <td colspan="2" style="padding-bottom: 5px;">
                    <span>Back to </span> <asp:HyperLink ID="hlkBreadCrumb" runat="server" /> &gt; <b>Resources</b>
                </td>
            </tr>
            <tr>
                <td valign="top" width="261">
                    <div class="pad10">
                        <asp:HyperLink ID="hlkProduct" runat="server"><asp:Image ID="imgProduct" runat="server" BorderStyle="None" /></asp:HyperLink>
                        <asp:DataList ID="dlAnchorLinks" runat="server" CellPadding="0" CellSpacing="0" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                            <HeaderTemplate>
                                <table border="0" cellpadding="0" cellspacing="2">
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="hlkAnchorType" runat="server" Target="_blank"><asp:Image ID="imgAnchorType" runat="server" BorderStyle="None" Height="25" Width="25" /></asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="hlkAnchorTypeName" runat="server" Target="_blank" />
                                        </td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </div>
                </td>
                <td valign="top">
                    <asp:DataList ID="dlDocumentTypes" runat="server" CellPadding="0" CellSpacing="0" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                        <HeaderTemplate>
                            <table border="0" cellpadding="2" cellspacing="0">
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="hlkDocumentType" runat="server"><asp:Image ID="imgDocumentType" runat="server" BorderStyle="None" Height="50" Width="50" /></asp:HyperLink>
                                    </td>
                                    <td style="font-size: 24px;">
                                        <asp:HyperLink ID="hlkDocumentTypeName" runat="server" CssClass="document-type" />
                                    </td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>