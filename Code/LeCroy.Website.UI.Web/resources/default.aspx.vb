﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class Resources_ResourcesDefault
    Inherits BasePage

#Region "Variables/Keys"
    Private _categoryId As Nullable(Of Int32) = Nothing
    Private _productSeriesId As Nullable(Of Int32) = Nothing
    Private _productGroupId As Nullable(Of Int32) = Nothing
    Private _protocolStandardId As Nullable(Of Int32) = Nothing
    Private _productId As Nullable(Of Int32) = Nothing

    Private _categorySeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _protocolSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _seriesSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _productSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _docTypeSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindLookups()
            If (Page.RouteData.Values.Count() > 0) Then
                HandleRoute()
            Else
                ValidateQueryString()
            End If

            Dim canShow As Boolean = False
            If (_productId.HasValue) Then
                canShow = FeProductManager.CanProductShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value, _categoryId, _productSeriesId)
            ElseIf (_productSeriesId.HasValue) Then
                canShow = FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value, _categoryId)
            ElseIf (_protocolStandardId.HasValue) Then
                canShow = FeProductManager.CanProtocolShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value, _productSeriesId)
            Else
                canShow = True
            End If

            If Not (canShow) Then
                Response.Redirect("~/")
            End If
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub dlAnchorLinks_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlAnchorLinks.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedDocumentFeatured = CType(e.Item.DataItem, ExtendedDocumentFeatured)
                Dim hlkAnchorType As HyperLink = CType(e.Item.FindControl("hlkAnchorType"), HyperLink)
                Dim imgAnchorType As Image = CType(e.Item.FindControl("imgAnchorType"), Image)
                Dim hlkAnchorTypeName As HyperLink = CType(e.Item.FindControl("hlkAnchorTypeName"), HyperLink)

                imgAnchorType.AlternateText = row.DocumentType.Name
                If Not (String.IsNullOrEmpty(row.DocumentType.ImagePathTemplate)) Then
                    imgAnchorType.ImageUrl = String.Format(row.DocumentType.ImagePathTemplate, imgAnchorType.Height.Value, imgAnchorType.Width.Value)
                End If
                hlkAnchorTypeName.NavigateUrl = hlkAnchorType.NavigateUrl
                hlkAnchorTypeName.Text = row.Document.Title

                If (String.Compare(row.Document.PasswordYN, "Y", True) = 0) Then
                    If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
                        If (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                            If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                                If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                Else
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                End If
                            End If
                        ElseIf (_categoryId.Value = CategoryIds.OSCILLOSCOPES Or _categoryId.Value = CategoryIds.PROBES) Then
                            If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                Else
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                End If
                            Else
                                hlkAnchorType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.Document.DocTypeId, GetQueryStringNameValuePair())
                                Return
                            End If
                        End If
                    End If

                    Dim queryStringKey As String = String.Empty
                    Dim queryStringValue As Int32 = 0
                    If (_productId.HasValue) Then
                        queryStringKey = "modelid"
                        queryStringValue = _productId.Value
                    ElseIf (_productSeriesId.HasValue) Then
                        queryStringKey = "mseries"
                        queryStringValue = _productSeriesId.Value
                    ElseIf (_productGroupId.HasValue) Then
                        queryStringKey = "groupid"
                        queryStringValue = _productGroupId.Value
                    ElseIf (_protocolStandardId.HasValue) Then
                        queryStringKey = "standardid"
                        queryStringValue = _protocolStandardId.Value
                    ElseIf (_categoryId.HasValue) Then
                        queryStringKey = "categoryid"
                        queryStringValue = _categoryId.Value
                    End If
                    If Not (String.IsNullOrEmpty(queryStringKey)) Then
                        hlkAnchorType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}={2}", row.Document.DocTypeId, queryStringKey, queryStringValue)
                        hlkAnchorType.Target = "_self"
                        hlkAnchorTypeName.Target = "_self"
                    End If
                Else
                    hlkAnchorType.NavigateUrl = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentFkId, row.Document.DocTypeId)
                End If
        End Select
    End Sub

    Private Sub dlDocumentTypes_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlDocumentTypes.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As DocumentType = CType(e.Item.DataItem, DocumentType)
                Dim hlkDocumentType As HyperLink = CType(e.Item.FindControl("hlkDocumentType"), HyperLink)
                Dim imgDocumentType As Image = CType(e.Item.FindControl("imgDocumentType"), Image)
                Dim hlkDocumentTypeName As HyperLink = CType(e.Item.FindControl("hlkDocumentTypeName"), HyperLink)

                If (Page.RouteData.Values.Count() > 0) Then
                    If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
                        If (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                            If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                                If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                    hlkDocumentType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.DocTypeId))
                                Else
                                    hlkDocumentType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _docTypeSeos(row.DocTypeId))
                                End If
                            Else
                                hlkDocumentType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.DocTypeId, GetQueryStringNameValuePair())
                            End If
                        ElseIf (_categoryId.Value = CategoryIds.OSCILLOSCOPES Or _categoryId.Value = CategoryIds.PROBES) Then
                            If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                                    hlkDocumentType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value), _docTypeSeos(row.DocTypeId))
                                Else
                                    hlkDocumentType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.DocTypeId))
                                End If
                            Else
                                hlkDocumentType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.DocTypeId, GetQueryStringNameValuePair())
                            End If
                        Else
                            hlkDocumentType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.DocTypeId, GetQueryStringNameValuePair())
                        End If
                    Else
                        hlkDocumentType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.DocTypeId, GetQueryStringNameValuePair())
                    End If
                Else
                    hlkDocumentType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.DocTypeId, GetQueryStringNameValuePair())
                End If
                imgDocumentType.AlternateText = row.Name
                Dim imageTypeTemplate As String = row.ImagePathTemplate
                If Not (String.IsNullOrEmpty(imageTypeTemplate)) Then
                    imgDocumentType.ImageUrl = String.Format(imageTypeTemplate, imgDocumentType.Height.Value, imgDocumentType.Width.Value)
                End If
                hlkDocumentTypeName.NavigateUrl = hlkDocumentType.NavigateUrl
                hlkDocumentTypeName.Text = row.Name
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindLookups()
        Dim categories As List(Of Category) = CategoryRepository.FetchCategories(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [CATEGORY_ID],[SeoValue] FROM [CATEGORY] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each c As Category In categories
            _categorySeos.Add(c.CategoryId, c.SeoValue)
        Next
        Dim protocols As List(Of ProtocolStandard) = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PROTOCOL_STANDARD_ID],[SeoValue] FROM [PROTOCOL_STANDARD] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As ProtocolStandard In protocols
            _protocolSeos.Add(p.ProtocolStandardId, p.SeoValue)
        Next
        Dim productSeries = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCT_SERIES_ID],[SeoValue] FROM [PRODUCT_SERIES] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each ps As ProductSeries In productSeries
            _seriesSeos.Add(ps.ProductSeriesId, ps.SeoValue)
        Next
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCTID],[SeoValue] FROM [PRODUCT] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As Product In products
            _productSeos.Add(p.ProductId, p.SeoValue)
        Next
        Dim docTypes As List(Of DocumentType) = DocumentTypeRepository.FetchDocumentTypes(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [DOC_TYPE_ID],[SeoValue] FROM [DOCUMENT_TYPE] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each dt As DocumentType In docTypes
            _docTypeSeos.Add(dt.DocTypeId, dt.SeoValue)
        Next
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() <= 0) Then Return

        Dim param0 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
        If (param0.Key Is Nothing) Then Return

        Dim category As Category = CategoryRepository.GetCategoryBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param0.Value)
        If (category Is Nothing) Then Return

        Select Case category.CategoryName.ToLower()
            Case "oscilloscopes", "probes", "waveform function generators", "tdr and s-parameters", "spectrum analyzers", "power supplies", "digital multimeters", "electronic loads"
                Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                If (param1.Key Is Nothing) Then Return

                Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                If (productSeries Is Nothing) Then Return

                _categoryId = category.CategoryId
                _productSeriesId = productSeries.ProductSeriesId
            Case "protocol analyzers"
                If (Page.RouteData.Values.Count() = 2) Then
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (protocolStandard Is Nothing) Then
                        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                        If (productSeries Is Nothing) Then
                            Return
                        Else
                            _productSeriesId = productSeries.ProductSeriesId
                        End If
                    Else
                        _protocolStandardId = protocolStandard.ProtocolStandardId
                    End If
                    _categoryId = category.CategoryId
                Else
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (protocolStandard Is Nothing) Then Return

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(2)
                    If (param2.Key Is Nothing) Then Return

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value)
                    If (productSeries Is Nothing) Then Return

                    _categoryId = category.CategoryId
                    _protocolStandardId = protocolStandard.ProtocolStandardId
                    _productSeriesId = productSeries.ProductSeriesId
                End If
            Case Else
                Return
        End Select
    End Sub

    Private Sub CreateMeta(ByVal category As String, ByVal series As String, ByVal standard As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                If (String.Compare(category, "protocolanalyzer", True) = 0) Then
                    Dim meta As New HtmlMeta()
                    If Not (String.IsNullOrEmpty(standard)) Then
                        meta = New HtmlMeta()
                        meta.Name = "protocol-standard-id"
                        meta.Content = _protocolStandardId
                        head.Controls.Add(meta)
                    End If
                    If Not (String.IsNullOrEmpty(series)) Then
                        meta = New HtmlMeta()
                        meta.Name = "product-series-id"
                        meta.Content = _productSeriesId
                        head.Controls.Add(meta)
                    End If
                    If Not (String.IsNullOrEmpty(standard)) Then
                        If Not (String.IsNullOrEmpty(series)) Then
                            link.Href = String.Format("{0}/{1}/{2}/{3}/resources", ConfigurationManager.AppSettings("DefaultDomain"), category, standard, series)
                        Else
                            link.Href = String.Format("{0}/{1}/{2}/resources", ConfigurationManager.AppSettings("DefaultDomain"), category, standard)
                        End If
                    ElseIf Not (String.IsNullOrEmpty(series))
                        link.Href = String.Format("{0}/{1}/{2}/resources", ConfigurationManager.AppSettings("DefaultDomain"), category, series)
                    End If
                Else
                    link.Href = String.Format("{0}/{1}/{2}/resources", ConfigurationManager.AppSettings("DefaultDomain"), category, series)
                    Dim meta As New HtmlMeta()
                    meta.Name = "product-series-id"
                    meta.Content = _productSeriesId
                    head.Controls.Add(meta)
                End If
                head.Controls.Add(link)
            End If
        End If
    End Sub

    Private Sub ValidateQueryString()
        If Not (String.IsNullOrEmpty(Request.QueryString("mseries"))) Then
            Dim productSeriesId As Int32 = -1
            Integer.TryParse(Request.QueryString("mseries"), productSeriesId)
            If (productSeriesId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productSeriesId = productSeriesId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("groupid"))) Then
            Dim groupId As Int32 = -1
            Integer.TryParse(Request.QueryString("groupid"), groupId)
            If (groupId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productGroupId = groupId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("standardid"))) Then
            Dim standardId As Int32 = -1
            Integer.TryParse(Request.QueryString("standardid"), standardId)
            If (standardId <= 0) Then
                HandleBouncedRedirect()
            End If
            '_productSeriesId = standardId
            _protocolStandardId = standardId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("categoryid"))) Then
            Dim categoryId As Int32 = -1
            Integer.TryParse(Request.QueryString("categoryid"), categoryId)
            If (categoryId <= 0) Then
                HandleBouncedRedirect()
            End If
            _categoryId = categoryId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("modelid"))) Then
            Dim productId As Int32 = -1
            Integer.TryParse(Request.QueryString("modelid"), productId)
            If (productId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productId = productId
        End If
        If Not (_categoryId.HasValue) And Not (_productSeriesId.HasValue) And Not (_productGroupId.HasValue) And Not (_protocolStandardId.HasValue) And Not (_productId.HasValue) Then
            HandleBouncedRedirect()
        End If
    End Sub

    Private Sub BindPage()
        If (Page.RouteData.Values.Count() <= 0) Then
            If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
                If (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                        If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            Response.Redirect(String.Format("~/{0}/{1}/{2}/resources", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value)))
                        Else
                            Response.Redirect(String.Format("~/{0}/{1}/resources", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value)))
                        End If
                    End If
                ElseIf (_categoryId.Value = CategoryIds.OSCILLOSCOPES Or _categoryId.Value = CategoryIds.PROBES) Then
                    If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                        If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                            Response.Redirect(String.Format("~/{0}/{1}/{2}/resources", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value)))
                        Else
                            Response.Redirect(String.Format("~/{0}/{1}/resources", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value)))
                        End If
                    End If
                End If
            ElseIf (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                    Response.Redirect(String.Format("~/{0}/{1}/{2}/resources", _categorySeos(CategoryIds.PROTOCOL_ANALYZERS), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value)))
                Else
                    Response.Redirect(String.Format("~/{0}/{1}/resources", _categorySeos(CategoryIds.PROTOCOL_ANALYZERS), _protocolSeos(_protocolStandardId.Value)))
                End If
            ElseIf (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.Value.ToString()))
                Dim catId As Int32 = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString"), "SELECT DISTINCT [CATEGORY_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [PRODUCT_SERIES_ID] = @PRODUCTSERIESID", sqlParameters.ToArray()).FirstOrDefault().CategoryId
                If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                    Response.Redirect(String.Format("~/{0}/{1}/{2}/resources", _categorySeos(catId), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value)))
                Else
                    Response.Redirect(String.Format("~/{0}/{1}/resources", _categorySeos(catId), _seriesSeos(_productSeriesId.Value)))
                End If
            End If
        End If
        Dim cat As String = String.Empty
        If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
            cat = _categorySeos(_categoryId.Value)
        End If
        Dim series As String = String.Empty
        If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
            series = _seriesSeos(_productSeriesId.Value)
        End If
        Dim standard As String = String.Empty
        If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
            standard = _protocolSeos(_protocolStandardId.Value)
        End If
        If Not (String.IsNullOrEmpty(cat)) Then 'cat is the only param absolutely required for meta building
            CreateMeta(cat, series, standard)
        End If

        Dim categoryId As Int32 = 0
        If (_productSeriesId.HasValue) Then
            If (_categoryId.HasValue) Then
                If (_categoryId.Value = 35 Or _categoryId = 36) Then
                    categoryId = _categoryId
                Else
                    categoryId = GetFetchedCategoryIdBySeriesId()
                End If
            Else
                categoryId = GetFetchedCategoryIdBySeriesId()
            End If

            If (categoryId = CategoryIds.PROTOCOL_ANALYZERS) Then
                If (_protocolStandardId.HasValue) Then
                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value)
                    If Not (protocolStandard Is Nothing) Then
                        If (_protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}", _protocolSeos(_protocolStandardId.Value))
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/protocolstandard.aspx?standardid={0}", protocolStandard.ProtocolStandardId)
                        End If
                        imgProduct.AlternateText = protocolStandard.Name
                        imgProduct.ImageUrl = String.Format("/images/category/{0}", protocolStandard.ProductImage)
                        hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                        hlkBreadCrumb.Text = protocolStandard.Name
                        Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), protocolStandard.Name)
                    End If
                Else
                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value)
                    If Not (productSeries Is Nothing) Then
                        If (_seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}/{1}", _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value))
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", _productSeriesId.Value)
                        End If
                        imgProduct.AlternateText = productSeries.Name
                        imgProduct.ImageUrl = productSeries.TitleImage
                        hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                        hlkBreadCrumb.Text = productSeries.Name
                        Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), productSeries.Name)
                    End If
                End If
            Else
                Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value)
                If Not (productSeries Is Nothing) Then
                    If (categoryId = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
                        If (_productSeriesId.Value = 276) Then
                            hlkProduct.NavigateUrl = "~/arbstudio/arbstudio.aspx"
                        ElseIf (_productSeriesId.Value = 399) Then
                            hlkProduct.NavigateUrl = "~/wavestation/"
                        Else
                            hlkProduct.NavigateUrl = "~/waveform-function-generators/"
                        End If
                    ElseIf (categoryId = 33) Then   'OMA
                        hlkProduct.NavigateUrl = "~/optical-modulation-analyzer/"
                    ElseIf (categoryId = CategoryIds.LOGIC_ANALYZERS) Then
                        hlkProduct.NavigateUrl = "~/logicstudio/?capid=131&mid=1051"
                    ElseIf (categoryId = CategoryIds.PERT3_SYSTEMS) Then
                        hlkProduct.NavigateUrl = "~/pert3/?capid=131&mid=1019"
                    ElseIf (categoryId = CategoryIds.SPARQ) Then
                        hlkProduct.NavigateUrl = "~/sparq/?capid=131&mid=1042"
                    ElseIf (categoryId = CategoryIds.SI_STUDIO) Then
                        hlkProduct.NavigateUrl = "~/sistudio/sistudio.aspx?mid=1084&modelid=6261"
                    ElseIf (categoryId = 35) Then   'TDR and S-Parameters
                        hlkProduct.NavigateUrl = "~/tdr-and-s-parameters/default.aspx"
                    ElseIf (categoryId = 36) Then   'Spectrum Analyzers
                        hlkProduct.NavigateUrl = "~/spectrum-analyzers/default.aspx"
                    ElseIf (categoryId = 37) Then   'Power Supplies
                        hlkProduct.NavigateUrl = "~/power-supplies/default.aspx"
                    ElseIf (categoryId = 38) Then   'Digital MM
                        hlkProduct.NavigateUrl = "~/digital-multimeters/default.aspx"
                    ElseIf (categoryId = 39) Then   'Electronic Loads
                        hlkProduct.NavigateUrl = "~/electronic-loads/default.aspx"
                    ElseIf (categoryId = CategoryIds.PROBES) Then
                        If (_seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            Dim seriesName As String = _seriesSeos(_productSeriesId.Value)
                            If (seriesName.Contains(".")) Then seriesName = String.Concat(seriesName, "/")
                            hlkProduct.NavigateUrl = String.Format("~/probes/{0}", seriesName)
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/probes/probeseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                    ElseIf (categoryId = CategoryIds.SOFTWARE_OPTIONS) Then
                        If (_productGroupId.HasValue) Then
                            hlkProduct.NavigateUrl = String.Format("/options/productseries.aspx?mseries={0}&groupid={1}", productSeries.ProductSeriesId, _productGroupId.Value)
                        Else
                            hlkProduct.NavigateUrl = String.Format("/options/productseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                    Else
                        If (_seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            hlkProduct.NavigateUrl = String.Format("~/oscilloscope/{0}", _seriesSeos(_productSeriesId.Value))
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                    End If
                    imgProduct.AlternateText = productSeries.Name
                    imgProduct.ImageUrl = productSeries.TitleImage
                    hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                    hlkBreadCrumb.Text = productSeries.Name
                    Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), productSeries.Name)
                End If
            End If
        ElseIf (_protocolStandardId.HasValue) Then
            Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value)
            If Not (protocolStandard Is Nothing) Then
                If (_protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                    hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}", _protocolSeos(_protocolStandardId.Value))
                Else
                    hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/protocolstandard.aspx?standardid={0}", protocolStandard.ProtocolStandardId)
                End If
                imgProduct.AlternateText = protocolStandard.Name
                imgProduct.ImageUrl = String.Format("/images/category/{0}", protocolStandard.ProductImage)
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = protocolStandard.Name
                Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), protocolStandard.Name)
            End If
        ElseIf (_categoryId.HasValue) Then
            Dim category As Category = CategoryRepository.GetCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), _categoryId.Value)
            If Not (category Is Nothing) Then
                If (categoryId = CategoryIds.HARDWARE_OPTIONS) Then   '11
                    hlkProduct.NavigateUrl = "~/options/default.aspx?categoryid=11&capid=102&mid=505"
                ElseIf (categoryId = CategoryIds.ACCESSORIES) Then   '23
                    hlkProduct.NavigateUrl = "~/options/default.aspx?categoryid=23&capid=102&mid=507"
                ElseIf (categoryId = 34) Then   'SAM
                    hlkProduct.NavigateUrl = "~/options/default.aspx?categoryid=34&capid=102&mid=1152"
                Else
                    hlkProduct.NavigateUrl = String.Format("~{0}", category.Url)
                End If
                imgProduct.AlternateText = category.CategoryName
                imgProduct.ImageUrl = category.Image
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = category.CategoryName
                Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), category.CategoryName)
            End If
        ElseIf (_productGroupId.HasValue) Then
            Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productGroupId.Value)
            If Not (productGroup Is Nothing) Then
                categoryId = GetCategoryIdForUnknowns(productGroup.GroupId)
                If (categoryId > 0) Then
                    If (categoryId = CategoryIds.PROBES) Then
                        hlkProduct.NavigateUrl = "~/probes/default.aspx"
                    Else
                        If (GetCategoryIdForUnknowns(productGroup.GroupId) = CategoryIds.PROBES) Then
                            hlkProduct.NavigateUrl = "~/probes/default.aspx"
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/options/default.aspx?categoryid={0}&groupid={1}", categoryId, productGroup.GroupId)
                        End If
                    End If
                Else
                    hlkProduct.NavigateUrl = "~/"
                End If
                imgProduct.AlternateText = productGroup.GroupName
                imgProduct.ImageUrl = productGroup.Image
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = productGroup.GroupName
                Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), productGroup.GroupName)
            End If
        ElseIf (_productId.HasValue) Then
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value)
            If Not (product Is Nothing) Then
                categoryId = GetCategoryIdForUnknowns(product.GroupId)
                If (categoryId > 0) Then
                    If (GetCategoryIdForUnknowns(product.GroupId) = CategoryIds.PROBES) Then
                        hlkProduct.NavigateUrl = "~/probes/default.aspx"
                    ElseIf (product.ProductId > 0) Then
                        hlkProduct.NavigateUrl = String.Format("~/options/productdetails.aspx?categoryid={0}&modelid={1}&groupid={2}", categoryId, product.ProductId, product.GroupId)
                    Else
                        hlkProduct.NavigateUrl = String.Format("~/options/default.aspx?categoryid={0}&modelid={1}&groupid={2}", categoryId, product.ProductId, product.GroupId)
                    End If
                Else
                    hlkProduct.NavigateUrl = "~/"
                End If
                imgProduct.AlternateText = product.Name
                'If Not (String.IsNullOrEmpty(product.IntroImagePath)) Then
                '    imgProduct.ImageUrl = product.IntroImagePath
                'Else
                imgProduct.ImageUrl = "~/images/spacer.gif"
                'End If
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = product.Name
                Page.Title = String.Format("{0} - {1} Resources", ConfigurationManager.AppSettings("DefaultPageTitle"), product.Name)
            End If
        End If
        If (categoryId > 0) Then _categoryId = categoryId
        BindAnchorLinks()
        BindDocumentTypes()
    End Sub

    Private Function GetFetchedCategoryIdBySeriesId() As Int32
        Dim categoryId As Int32 = 0
        Dim fetchedCategoryId As String = Functions.GetCategoryIDonSeriesID(_productSeriesId.Value)
        If (String.IsNullOrEmpty(fetchedCategoryId)) Then
            Response.Redirect("~/default.aspx")
        End If
        If Not (Int32.TryParse(fetchedCategoryId, categoryId)) Then
            Response.Redirect("~/default.aspx")
        End If
        Return categoryId
    End Function

    Private Sub BindAnchorLinks()
        Dim data As List(Of ExtendedDocumentFeatured) = New List(Of ExtendedDocumentFeatured)
        If (_productId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value).ToList()
        ElseIf (_productSeriesId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value).ToList()
        ElseIf (_productGroupId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productGroupId.Value).ToList()
        ElseIf (_protocolStandardId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value).ToList()
        ElseIf (_categoryId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), _categoryId.Value).ToList()
        End If
        'If (data.Count > 0) Then
        '    Dim distinctDocumentIds As List(Of Int32) = data.Select(Function(x) x.DocumentFkId).Distinct().ToList()
        '    Dim data2 As List(Of ExtendedDocumentFeatured) = New List(Of ExtendedDocumentFeatured)
        '    Dim addedTo As List(Of Int32) = New List(Of Int32)
        '    For Each d In data
        '        If (distinctDocumentIds.Contains(d.DocumentFkId) And Not addedTo.Contains(d.DocumentFkId)) Then
        '            data2.Add(d)
        '            addedTo.Add(d.DocumentFkId)
        '        End If
        '    Next    '(From d In data Where distinctDocumentIds.Contains(d.DocumentFkId) Select d Order By d.SortId, d.Document.Title).ToList()
        '    data2 = data2.OrderBy(Function(x) x.SortId).ThenBy(Function(x) x.Document.Title).ToList()
        '    If (data2.Count > 0) Then
        '        dlAnchorLinks.DataSource = data2
        '        dlAnchorLinks.DataBind()
        '    End If
        'End If
    End Sub

    Private Sub BindDocumentTypes()
        If Not (_categoryId.HasValue) And (_productGroupId.HasValue) Then
            _categoryId = GetCategoryIdForUnknowns(_productGroupId.Value)
        End If
        Dim data As List(Of DocumentType) = FeDocumentManager.GetDocumentTypesForDocumentCategoryProperties(ConfigurationManager.AppSettings("ConnectionString").ToString(), _categoryId, _productSeriesId, _productGroupId, _protocolStandardId, _productId).OrderBy(Function(x) x.Name).ToList()
        If (data.Count <= 0) Then
            HandleBouncedRedirect()
        End If
        dlDocumentTypes.DataSource = data
        dlDocumentTypes.DataBind()
    End Sub

    Private Sub HandleHomePageRedirect()
        Response.Redirect("~/")
    End Sub

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/support/techlib/")
    End Sub

    Private Function GetQueryStringNameValuePair() As String
        If (_categoryId.HasValue) Then
            If (_categoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                If (_productSeriesId.HasValue And _productGroupId.HasValue) Then
                    Return String.Format("mseries={0}&groupid={1}", _productSeriesId.Value, _productGroupId.Value)
                End If
            End If
        End If
        If (_productGroupId.HasValue) Then
            Return String.Format("groupid={0}", _productGroupId.Value)
        ElseIf (_productSeriesId.HasValue) Then
            If (_productId.HasValue) Then
                Dim productSeriesCategory As ProductSeriesCategory = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForSeries(ConfigurationManager.AppSettings("ConnectionString"), _productSeriesId.Value).ToList().Where(Function(x) x.ProductId = _productId.Value).FirstOrDefault()
                If Not (productSeriesCategory Is Nothing) Then
                    Return String.Format("categoryid={0}&mseries={1}&modelid={2}", productSeriesCategory.CategoryId, _productSeriesId.Value, _productId.Value)
                End If
                Return String.Format("mseries={0}", _productSeriesId.Value)
            End If
            Return String.Format("mseries={0}", _productSeriesId.Value)
        ElseIf (_protocolStandardId.HasValue) Then
            Return String.Format("standardid={0}", _protocolStandardId.Value)
        ElseIf (_categoryId.HasValue) Then
            If (_productId.HasValue) Then
                Dim productSeriesCategory As ProductSeriesCategory = ProductSeriesCategoryRepository.GetProductSeriesCategoriesForProductIdsAndCategory(ConfigurationManager.AppSettings("ConnectionString"), _categoryId.Value, New List(Of Int32)(New Int32() {_productId.Value})).FirstOrDefault()
                If Not (productSeriesCategory Is Nothing) Then
                    Return String.Format("categoryid={0}&mseries={1}&modelid={2}", _categoryId.Value, productSeriesCategory.ProductSeriesId, _productId.Value)
                End If
                Return String.Format("categoryid={0}", _categoryId.Value)
            End If
            Return String.Format("categoryid={0}", _categoryId.Value)
        ElseIf (_productId.HasValue) Then
            Return String.Format("modelid={0}", _productId.Value)
        Else
            Return String.Empty
        End If
    End Function

    Private Function GetCategoryIdForUnknowns(ByVal productGroupId As Int32) As Int32
        Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), productGroupId)
        If Not (productGroup Is Nothing) Then
            Return productGroup.CategoryId
        End If
        Return -1
    End Function
#End Region
End Class