﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="details.aspx.vb" Inherits="LeCroy.Website.Resources_ResourcesDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="document-type">
        <table border="0" cellpadding="2" cellspacing="0" style="background-color: #ffffff;" width="100%">
            <tr>
                <td colspan="2" style="padding-bottom: 5px;">
                    <span>Back to </span> <asp:HyperLink ID="hlkBreadCrumb" runat="server" /> &gt; <asp:HyperLink ID="hlkDocumentTypes" runat="server" Text="Resources" /> &gt; <b><asp:Literal ID="litDocumentTypeTop" runat="server" /></b>
                </td>
            </tr>
            <tr>
                <td valign="top" width="261">
                    <div class="pad10">
                        <asp:HyperLink ID="hlkProduct" runat="server"><asp:Image ID="imgProduct" runat="server" BorderStyle="None" /></asp:HyperLink>
                        <asp:DataList ID="dlAnchorLinks" runat="server" CellPadding="0" CellSpacing="0" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Table">
                            <HeaderTemplate>
                                <table border="0" cellpadding="0" cellspacing="2">
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="hlkAnchorType" runat="server" Target="_blank"><asp:Image ID="imgAnchorType" runat="server" BorderStyle="None" Height="25" Width="25" /></asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="hlkAnchorTypeName" runat="server" Target="_blank" />
                                        </td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </div>
                </td>
                <td valign="top">
                    <h1><asp:Literal ID="litDocumentType" runat="server" /></h1>
                    <asp:Repeater ID="rptDocuments" runat="server">
                        <HeaderTemplate>
                            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="sasr" id="divSasr" runat="server">
                                            <table border="0" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td rowspan="4" style="padding-right: 3px;" valign="top">
                                                        <asp:HyperLink ID="hlkImage" runat="server" Target="_blank"><asp:Image ID="imgDocument" runat="server" BorderStyle="None" Height="16" Width="16" /></asp:HyperLink>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:HyperLink ID="hlkDocument" runat="server" CssClass="blue" Target="_blank" />
                                                    </td>
                                                </tr>
                                                <tr id="trVersion" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litVersion" runat="server" /><asp:Literal ID="litReleaseDate" runat="server" />
                                                        <i><asp:Literal ID="litPasswordProtected" runat="server" Text="<br />*{0} requires a separate access password to download. Please contact your Sales Engineer to obtain access." Visible="false" /></i>
                                                    </td>
                                                </tr>
                                                <tr id="trDescription" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litDescription" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="trXrefs" runat="server" visible="false">
                                                    <td style="padding-left: 20px;" valign="top">
                                                        <asp:Repeater ID="rptXrefs" runat="server">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlkXRef" runat="server" Target="_blank" /><br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                                <tr id="trComments" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litComments" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                                <tr style="background-color: #deebf3;">
                                    <td>
                                        <div class="sasr" id="divSasr" runat="server">
                                            <table border="0" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td rowspan="4" style="padding-right: 3px;" valign="top">
                                                        <asp:HyperLink ID="hlkImage" runat="server" Target="_blank"><asp:Image ID="imgDocument" runat="server" BorderStyle="None" Height="16" Width="16" /></asp:HyperLink>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:HyperLink ID="hlkDocument" runat="server" CssClass="blue" Target="_blank" />
                                                    </td>
                                                </tr>
                                                <tr id="trVersion" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litVersion" runat="server" /><asp:Literal ID="litReleaseDate" runat="server" />
                                                        <i><asp:Literal ID="litPasswordProtected" runat="server" Text="<br />*{0} requires a separate access password to download. Please contact your Sales Engineer to obtain access." Visible="false" /></i>
                                                    </td>
                                                </tr>
                                                <tr id="trDescription" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litDescription" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="trXrefs" runat="server" visible="false">
                                                    <td style="padding-left: 20px;" valign="top">
                                                        <asp:Repeater ID="rptXrefs" runat="server">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlkXRef" runat="server" Target="_blank" /><br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                                <tr id="trComments" runat="server" visible="false">
                                                    <td valign="top">
                                                        <asp:Literal ID="litComments" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                        </AlternatingItemTemplate>
                        <SeparatorTemplate>
                                <tr><td colspan="2">&nbsp;</td></tr>
                        </SeparatorTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater><br /><br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>