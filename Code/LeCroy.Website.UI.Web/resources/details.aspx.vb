﻿Imports System.Data.SqlClient
Imports LeCroy.Library.DAL.Common
Imports LeCroy.Library.Domain.Common.Constants
Imports LeCroy.Library.Domain.Common.DTOs
Imports LeCroy.Website.BLL

Public Class Resources_ResourcesDetails
    Inherits BasePage

#Region "Variables/Keys"
    Private _docTypeId As Int32 = 0
    Private _categoryId As Nullable(Of Int32) = Nothing
    Private _productSeriesId As Nullable(Of Int32) = Nothing
    Private _productGroupId As Nullable(Of Int32) = Nothing
    Private _protocolStandardId As Nullable(Of Int32) = Nothing
    Private _productId As Nullable(Of Int32) = Nothing
    Private _documentVersion As String = String.Empty
    Private _documentXrefs As List(Of DocumentXRef) = New List(Of DocumentXRef)
    Private _childDocumentsViaXref As List(Of Document) = New List(Of Document)

    Private _categorySeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _protocolSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _seriesSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _productSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
    Private _docTypeSeos As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            BindLookups()
            If (Page.RouteData.Values.Count() > 0) Then
                HandleRoute()
            Else
                ValidateQueryString()
            End If

            Dim canShow As Boolean = False
            If (_productId.HasValue) Then
                canShow = FeProductManager.CanProductShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value, _categoryId, _productSeriesId)
            ElseIf (_productSeriesId.HasValue) Then
                canShow = FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value, _categoryId)
            ElseIf (_protocolStandardId.HasValue) Then
                canShow = FeProductManager.CanProtocolShow(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value, _productSeriesId)
            Else
                canShow = True
            End If

            If Not (canShow) Then
                Response.Redirect("~/")
            End If
            BindPage()
        End If
    End Sub
#End Region

#Region "Control Events"
    Private Sub dlAnchorLinks_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlAnchorLinks.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As ExtendedDocumentFeatured = CType(e.Item.DataItem, ExtendedDocumentFeatured)
                Dim hlkAnchorType As HyperLink = CType(e.Item.FindControl("hlkAnchorType"), HyperLink)
                Dim imgAnchorType As Image = CType(e.Item.FindControl("imgAnchorType"), Image)
                Dim hlkAnchorTypeName As HyperLink = CType(e.Item.FindControl("hlkAnchorTypeName"), HyperLink)

                imgAnchorType.AlternateText = row.DocumentType.Name
                If Not (String.IsNullOrEmpty(row.DocumentType.ImagePathTemplate)) Then
                    imgAnchorType.ImageUrl = String.Format(row.DocumentType.ImagePathTemplate, imgAnchorType.Height.Value, imgAnchorType.Width.Value)
                End If
                hlkAnchorTypeName.NavigateUrl = hlkAnchorType.NavigateUrl
                hlkAnchorTypeName.Text = row.Document.Title

                If (String.Compare(row.Document.PasswordYN, "Y", True) = 0) Then
                    If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
                        If (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                            If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                                If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                Else
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                End If
                            End If
                        ElseIf (_categoryId.Value = CategoryIds.OSCILLOSCOPES Or _categoryId.Value = CategoryIds.PROBES) Then
                            If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                                If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                Else
                                    hlkAnchorType.NavigateUrl = String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(row.Document.DocTypeId))
                                    Return
                                End If
                            Else
                                hlkAnchorType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}", row.Document.DocTypeId, GetQueryStringNameValuePair())
                                Return
                            End If
                        End If
                    End If

                    Dim queryStringKey As String = String.Empty
                    Dim queryStringValue As Int32 = 0
                    If (_productId.HasValue) Then
                        queryStringKey = "modelid"
                        queryStringValue = _productId.Value
                    ElseIf (_productSeriesId.HasValue) Then
                        queryStringKey = "mseries"
                        queryStringValue = _productSeriesId.Value
                    ElseIf (_productGroupId.HasValue) Then
                        queryStringKey = "groupid"
                        queryStringValue = _productGroupId.Value
                    ElseIf (_protocolStandardId.HasValue) Then
                        queryStringKey = "standardid"
                        queryStringValue = _protocolStandardId.Value
                    ElseIf (_categoryId.HasValue) Then
                        queryStringKey = "categoryid"
                        queryStringValue = _categoryId.Value
                    End If
                    If Not (String.IsNullOrEmpty(queryStringKey)) Then
                        hlkAnchorType.NavigateUrl = String.Format("~/resources/details.aspx?doctypeid={0}&{1}={2}", _docTypeId, queryStringKey, queryStringValue)
                        hlkAnchorType.Target = "_self"
                        hlkAnchorTypeName.Target = "_self"
                    End If
                Else
                    hlkAnchorType.NavigateUrl = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentFkId, row.Document.DocTypeId)
                End If
        End Select
    End Sub

    Private Sub rptDocuments_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptDocuments.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim divSasr As HtmlGenericControl = CType(e.Item.FindControl("divSasr"), HtmlGenericControl)
                Dim hlkImage As HyperLink = CType(e.Item.FindControl("hlkImage"), HyperLink)
                Dim imgDocument As Image = CType(e.Item.FindControl("imgDocument"), Image)
                Dim hlkDocument As HyperLink = CType(e.Item.FindControl("hlkDocument"), HyperLink)
                Dim litVersion As Literal = CType(e.Item.FindControl("litVersion"), Literal)
                Dim litReleaseDate As Literal = CType(e.Item.FindControl("litReleaseDate"), Literal)
                Dim litPasswordProtected As Literal = CType(e.Item.FindControl("litPasswordProtected"), Literal)
                Dim litDescription As Literal = CType(e.Item.FindControl("litDescription"), Literal)
                Dim litComments As Literal = CType(e.Item.FindControl("litComments"), Literal)
                Dim trVersion As HtmlTableRow = CType(e.Item.FindControl("trVersion"), HtmlTableRow)
                Dim trDescription As HtmlTableRow = CType(e.Item.FindControl("trDescription"), HtmlTableRow)
                Dim trXrefs As HtmlTableRow = CType(e.Item.FindControl("trXrefs"), HtmlTableRow)
                Dim trComments As HtmlTableRow = CType(e.Item.FindControl("trComments"), HtmlTableRow)
                Dim rptXrefs As Repeater = CType(e.Item.FindControl("rptXrefs"), Repeater)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkImage.NavigateUrl = redirectUrl
                    Dim imageTypeTemplate As String = DocumentTypeRepository.GetDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), row.DocTypeId).ImagePathTemplate
                    If Not (String.IsNullOrEmpty(imageTypeTemplate)) Then
                        imgDocument.ImageUrl = String.Format(imageTypeTemplate, imgDocument.Height.Value, imgDocument.Width.Value)
                    End If
                    hlkDocument.NavigateUrl = redirectUrl
                    hlkDocument.Text = row.Title
                    If Not (String.IsNullOrEmpty(row.Version)) Then
                        trVersion.Visible = True
                        litVersion.Text = String.Format("Version: {0}", row.Version.Replace("version", String.Empty))
                    End If
                    Dim docTypesForDate As List(Of Int32) = New List(Of Int32)({9, 10, 11, 12, 13, 14, 15, 16, 20, 24, 25})
                    If (docTypesForDate.Contains(row.DocTypeId)) Then
                        trVersion.Visible = True
                        litReleaseDate.Text = String.Format("{0}Released: {1}", IIf(String.IsNullOrEmpty(litVersion.Text), String.Empty, "&nbsp;&nbsp;"), row.DateEntered.ToString("dd-MMM-yyyy"))
                        If (String.Compare(row.PasswordYN, "Y", True) = 0) Then
                            litPasswordProtected.Visible = True
                            litPasswordProtected.Text = String.Format(litPasswordProtected.Text, row.Title)
                        End If
                        If Not (String.IsNullOrEmpty(row.Description)) Then
                            trDescription.Visible = True
                            litDescription.Text = row.Description
                        End If
                    End If

                    If (_documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Count > 0) Then
                        trXrefs.Visible = True
                        divSasr.Attributes.Remove("class")
                        AddHandler rptXrefs.ItemDataBound, AddressOf rptXrefs_ItemDataBound
                        Dim docFetch As List(Of Document) = (From c In _childDocumentsViaXref Where _documentXrefs.Where(Function(x) x.ParentDocumentFkId = row.DocumentId).ToList().Select(Function(x) x.ChildDocumentFkId).Contains(c.DocumentId) Select c).ToList()
                        If (docFetch.Count > 0) Then
                            rptXrefs.DataSource = docFetch
                            rptXrefs.DataBind()
                        End If
                    End If
                    If Not (String.IsNullOrEmpty(row.Comments)) Then
                        If (row.DocTypeId = 16) Then
                            trComments.Visible = True
                            divSasr.Attributes.Remove("class")
                            litComments.Text = row.Comments
                        End If
                    End If
                End If
        End Select
    End Sub

    Private Sub rptXrefs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim row As Document = CType(e.Item.DataItem, Document)
                Dim hlkXRef As HyperLink = CType(e.Item.FindControl("hlkXRef"), HyperLink)

                Dim redirectUrl As String = FeDocumentManager.GetDocumentRedirectUrl(row.DocumentId, row.DocTypeId)
                If Not (String.IsNullOrEmpty(redirectUrl)) Then
                    hlkXRef.NavigateUrl = redirectUrl
                    hlkXRef.Text = row.Title
                End If
        End Select
    End Sub
#End Region

#Region "Page Methods"
    Private Sub BindLookups()
        Dim categories As List(Of Category) = CategoryRepository.FetchCategories(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [CATEGORY_ID],[SeoValue] FROM [CATEGORY] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each c As Category In categories
            _categorySeos.Add(c.CategoryId, c.SeoValue)
        Next
        Dim protocols As List(Of ProtocolStandard) = ProtocolStandardRepository.FetchProtocolStandards(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PROTOCOL_STANDARD_ID],[SeoValue] FROM [PROTOCOL_STANDARD] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As ProtocolStandard In protocols
            _protocolSeos.Add(p.ProtocolStandardId, p.SeoValue)
        Next
        Dim productSeries = ProductSeriesRepository.FetchProductSeries(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCT_SERIES_ID],[SeoValue] FROM [PRODUCT_SERIES] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each ps As ProductSeries In productSeries
            _seriesSeos.Add(ps.ProductSeriesId, ps.SeoValue)
        Next
        Dim products As List(Of Product) = ProductRepository.FetchProducts(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [PRODUCTID],[SeoValue] FROM [PRODUCT] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each p As Product In products
            _productSeos.Add(p.ProductId, p.SeoValue)
        Next
        Dim docTypes As List(Of DocumentType) = DocumentTypeRepository.FetchDocumentTypes(ConfigurationManager.AppSettings("ConnectionString"), "SELECT [DOC_TYPE_ID],[SeoValue] FROM [DOCUMENT_TYPE] WHERE [SeoValue] IS NOT NULL", New List(Of SqlParameter)().ToArray()).ToList()
        For Each dt As DocumentType In docTypes
            _docTypeSeos.Add(dt.DocTypeId, dt.SeoValue)
        Next
    End Sub

    Private Sub HandleRoute()
        If (Page.RouteData.Values.Count() <= 0) Then Return

        Dim param0 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(0)
        If (param0.Key Is Nothing) Then Return

        Dim category As Category = CategoryRepository.GetCategoryBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param0.Value)
        If (category Is Nothing) Then Return

        Select Case category.CategoryName.ToLower()
            Case "oscilloscopes", "probes", "waveform function generators", "tdr and s-parameters", "spectrum analyzers", "power supplies", "digital multimeters", "electronic loads", "vector network analyzer"
                _categoryId = category.CategoryId
                If (Page.RouteData.Values.Count() = 3) Then
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (productSeries Is Nothing) Then Return

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(2)
                    If (param2.Key Is Nothing) Then Return

                    Dim docType As DocumentType = DocumentTypeRepository.GetDocumentTypeBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value)
                    If (docType Is Nothing) Then Return
                    _productSeriesId = productSeries.ProductSeriesId
                    _docTypeId = docType.DocTypeId
                Else
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (productSeries Is Nothing) Then Return

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(2)
                    If (param2.Key Is Nothing) Then Return

                    Dim passThroughProductSeriesId As Int32 = productSeries.ProductSeriesId
                    If (_productSeriesId = 591) Then    ' HACK: Special for TASK0162804
                        Dim productSeriesIds As List(Of Int32) = ProductSeriesCategoryRepository.GetProductSeriesIdsBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value.ToString())
                        If (productSeriesIds.Contains(511)) Then
                            passThroughProductSeriesId = 511
                        End If
                    End If

                    Dim product As Product = ProductRepository.GetProductBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), passThroughProductSeriesId, param2.Value)
                    If (product Is Nothing) Then Return

                    Dim param3 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(3)
                    If (param3.Key Is Nothing) Then Return

                    Dim docType As DocumentType = DocumentTypeRepository.GetDocumentTypeBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param3.Value)
                    If (docType Is Nothing) Then Return

                    _productSeriesId = productSeries.ProductSeriesId
                    _productId = product.ProductId
                    _docTypeId = docType.DocTypeId
                End If
            Case "protocol analyzers"
                If (Page.RouteData.Values.Count() = 3) Then
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (protocolStandard Is Nothing) Then
                        Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                        If (productSeries Is Nothing) Then
                            Return
                        Else
                            _productSeriesId = productSeries.ProductSeriesId
                        End If
                    Else
                        _protocolStandardId = protocolStandard.ProtocolStandardId
                    End If

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(2)
                    If (param2.Key Is Nothing) Then Return

                    Dim docType As DocumentType = DocumentTypeRepository.GetDocumentTypeBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value)
                    If (docType Is Nothing) Then Return

                    _categoryId = category.CategoryId
                    _docTypeId = docType.DocTypeId
                Else
                    Dim param1 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(1)
                    If (param1.Key Is Nothing) Then Return

                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandardBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param1.Value)
                    If (protocolStandard Is Nothing) Then Return

                    Dim param2 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(2)
                    If (param2.Key Is Nothing) Then Return

                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeriesBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param2.Value)
                    If (productSeries Is Nothing) Then Return

                    Dim param3 As KeyValuePair(Of String, Object) = Page.RouteData.Values.ElementAt(3)
                    If (param3.Key Is Nothing) Then Return

                    Dim docType As DocumentType = DocumentTypeRepository.GetDocumentTypeBySeoValue(ConfigurationManager.AppSettings("ConnectionString").ToString(), param3.Value)
                    If (docType Is Nothing) Then Return

                    _categoryId = category.CategoryId
                    _protocolStandardId = protocolStandard.ProtocolStandardId
                    _productSeriesId = productSeries.ProductSeriesId
                    _docTypeId = docType.DocTypeId
                End If
            Case Else
                Return
        End Select
    End Sub

    Private Sub CreateMeta(ByVal category As String, ByVal series As String, ByVal standard As String, ByVal docType As String)
        Dim masterpage As MasterPage_site = CType(Me.Master.Master, MasterPage_site)
        If Not (masterpage Is Nothing) Then
            Dim head As HtmlHead = CType(masterpage.FindControl("Head1"), HtmlHead)
            If Not (head Is Nothing) Then
                Dim link As New HtmlLink()
                link.Attributes.Add("rel", "canonical")
                If (String.Compare(category, "protocolanalyzer", True) = 0) Then
                    Dim meta As New HtmlMeta()
                    If Not (String.IsNullOrEmpty(standard)) Then
                        meta = New HtmlMeta()
                        meta.Name = "protocol-standard-id"
                        meta.Content = _protocolStandardId
                        head.Controls.Add(meta)
                    End If
                    If Not (String.IsNullOrEmpty(series)) Then
                        meta = New HtmlMeta()
                        meta.Name = "product-series-id"
                        meta.Content = _productSeriesId
                        head.Controls.Add(meta)
                    End If
                    If Not (String.IsNullOrEmpty(standard)) Then
                        If Not (String.IsNullOrEmpty(series)) Then
                            link.Href = String.Format("{0}/{1}/{2}/{3}/resources/{4}", ConfigurationManager.AppSettings("DefaultDomain"), category, standard, series, docType)
                        Else
                            link.Href = String.Format("{0}/{1}/{2}/resources/{3}", ConfigurationManager.AppSettings("DefaultDomain"), category, standard, docType)
                        End If
                    ElseIf Not (String.IsNullOrEmpty(series))
                        link.Href = String.Format("{0}/{1}/{2}/resources/{3}", ConfigurationManager.AppSettings("DefaultDomain"), category, series, docType)
                    End If
                Else
                    link.Href = String.Format("{0}/{1}/{2}/resources/{3}", ConfigurationManager.AppSettings("DefaultDomain"), category, series, docType)
                    Dim meta As New HtmlMeta()
                    meta.Name = "product-series-id"
                    meta.Content = _productSeriesId
                    head.Controls.Add(meta)
                End If
                head.Controls.Add(link)
            End If
        End If
    End Sub

    Private Function GetQueryStringNameValuePair() As String
        If (_categoryId.HasValue) Then
            If (_categoryId.Value = CategoryIds.SOFTWARE_OPTIONS) Then
                If (_productSeriesId.HasValue And _productGroupId.HasValue) Then
                    Return String.Format("mseries={0}&groupid={1}", _productSeriesId.Value, _productGroupId.Value)
                End If
            End If
        End If
        If (_productGroupId.HasValue) Then
            Return String.Format("groupid={0}", _productGroupId.Value)
        ElseIf (_productSeriesId.HasValue) Then
            Return String.Format("mseries={0}", _productSeriesId.Value)
        ElseIf (_protocolStandardId.HasValue) Then
            Return String.Format("standardid={0}", _protocolStandardId.Value)
        ElseIf (_categoryId.HasValue) Then
            Return String.Format("categoryid={0}", _categoryId.Value)
        ElseIf (_productId.HasValue) Then
            Return String.Format("modelid={0}", _productId.Value)
        Else
            Return String.Empty
        End If
    End Function

    Private Sub ValidateQueryString()
        If (String.IsNullOrEmpty(Request.QueryString("doctypeid"))) Then
            HandleBouncedRedirect()
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("doctypeid"))) Then
            Integer.TryParse(Request.QueryString("doctypeid"), _docTypeId)
            If (_docTypeId <= 0) Then
                HandleBouncedRedirect()
            End If
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("mseries"))) Then
            Dim productSeriesId As Int32 = -1
            Integer.TryParse(Request.QueryString("mseries"), productSeriesId)
            If (productSeriesId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productSeriesId = productSeriesId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("groupid"))) Then
            Dim groupId As Int32 = -1
            Integer.TryParse(Request.QueryString("groupid"), groupId)
            If (groupId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productGroupId = groupId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("standardid"))) Then
            Dim standardId As Int32 = -1
            Integer.TryParse(Request.QueryString("standardid"), standardId)
            If (standardId <= 0) Then
                HandleBouncedRedirect()
            End If
            '_productSeriesId = standardId
            _protocolStandardId = standardId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("categoryid"))) Then
            Dim categoryId As Int32 = -1
            Integer.TryParse(Request.QueryString("categoryid"), categoryId)
            If (categoryId <= 0) Then
                HandleBouncedRedirect()
            End If
            _categoryId = categoryId
        End If
        If Not (String.IsNullOrEmpty(Request.QueryString("modelid"))) Then
            Dim productId As Int32 = -1
            Integer.TryParse(Request.QueryString("modelid"), productId)
            If (productId <= 0) Then
                HandleBouncedRedirect()
            End If
            _productId = productId
        End If
    End Sub

    Private Sub BindPage()
        SetDocumentTypeLiteral()
        If (Page.RouteData.Values.Count() <= 0) Then
            If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
                If (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                        If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            Response.Redirect(String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(_docTypeId)))
                        Else
                            Response.Redirect(String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _protocolSeos(_protocolStandardId.Value), _docTypeSeos(_docTypeId)))
                        End If
                    End If
                ElseIf (_categoryId.Value = CategoryIds.OSCILLOSCOPES Or _categoryId.Value = CategoryIds.PROBES) Then
                    If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                        If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                            Response.Redirect(String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value), _docTypeSeos(_docTypeId)))
                        Else
                            Response.Redirect(String.Format("~/{0}/{1}/resources/{2}", _categorySeos(_categoryId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(_docTypeId)))
                        End If
                    End If
                End If
            ElseIf (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                    Response.Redirect(String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(CategoryIds.PROTOCOL_ANALYZERS), _protocolSeos(_protocolStandardId.Value), _seriesSeos(_productSeriesId.Value), _docTypeSeos(_docTypeId)))
                Else
                    Response.Redirect(String.Format("~/{0}/{1}/resources/{2}", _categorySeos(CategoryIds.PROTOCOL_ANALYZERS), _protocolSeos(_protocolStandardId.Value), _docTypeSeos(_docTypeId)))
                End If
            ElseIf (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                Dim sqlParameters As List(Of SqlParameter) = New List(Of SqlParameter)
                sqlParameters.Add(New SqlParameter("@PRODUCTSERIESID", _productSeriesId.Value.ToString()))
                Dim catId As Int32 = ProductSeriesCategoryRepository.FetchProductSeriesCategory(ConfigurationManager.AppSettings("ConnectionString"), "SELECT DISTINCT [CATEGORY_ID] FROM [PRODUCT_SERIES_CATEGORY] WHERE [PRODUCT_SERIES_ID] = @PRODUCTSERIESID", sqlParameters.ToArray()).FirstOrDefault().CategoryId
                If (_productId.HasValue AndAlso _productSeos.ContainsKey(_productId.Value)) Then
                    Response.Redirect(String.Format("~/{0}/{1}/{2}/resources/{3}", _categorySeos(catId), _seriesSeos(_productSeriesId.Value), _productSeos(_productId.Value), _docTypeSeos(_docTypeId)))
                Else
                    Response.Redirect(String.Format("~/{0}/{1}/resources/{2}", _categorySeos(catId), _seriesSeos(_productSeriesId.Value), _docTypeSeos(_docTypeId)))
                End If
            End If
        End If
        Dim cat As String = String.Empty
        If (_categoryId.HasValue AndAlso _categorySeos.ContainsKey(_categoryId.Value)) Then
            cat = _categorySeos(_categoryId.Value)
        End If
        Dim series As String = String.Empty
        If (_productSeriesId.HasValue AndAlso _seriesSeos.ContainsKey(_productSeriesId.Value)) Then
            series = _seriesSeos(_productSeriesId.Value)
        End If
        Dim standard As String = String.Empty
        If (_protocolStandardId.HasValue AndAlso _protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
            standard = _protocolSeos(_protocolStandardId.Value)
        End If
        If Not (String.IsNullOrEmpty(cat)) Then 'cat is the only param absolutely required for meta building
            CreateMeta(cat, series, standard, _docTypeSeos(_docTypeId))
        End If

        If (_productSeriesId.HasValue) Then
            Dim fetchedCategoryId As String = Functions.GetCategoryIDonSeriesID(_productSeriesId.Value)
            If (String.IsNullOrEmpty(fetchedCategoryId)) Then
                Response.Redirect("~/default.aspx")
            End If
            Dim categoryId As Int32
            If Not (Int32.TryParse(fetchedCategoryId, categoryId)) Then
                Response.Redirect("~/default.aspx")
            End If

            If (categoryId = CategoryIds.PROTOCOL_ANALYZERS) Then
                If (_protocolStandardId.HasValue) Then
                    Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value)
                    If Not (protocolStandard Is Nothing) Then
                        If (_protocolSeos.ContainsKey(_protocolStandardId.Value)) Then
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}", _protocolSeos(_protocolStandardId.Value))
                            hlkDocumentTypes.NavigateUrl = String.Format("~/protocolanalyzer/{0}/resources", _protocolSeos(protocolStandard.ProtocolStandardId))
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/protocolstandard.aspx?standardid={0}", protocolStandard.ProtocolStandardId)
                            hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?standardid={0}", protocolStandard.ProtocolStandardId)
                        End If
                        imgProduct.AlternateText = protocolStandard.Name
                        imgProduct.ImageUrl = String.Format("/images/category/{0}", protocolStandard.ProductImage)
                        hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                        hlkBreadCrumb.Text = protocolStandard.Name
                        Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), protocolStandard.Name, litDocumentType.Text)
                    End If
                Else
                    Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value)
                    If Not (productSeries Is Nothing) Then
                        If (_seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}", _productSeriesId.Value) ' Cannot randomly generate data
                            hlkDocumentTypes.NavigateUrl = String.Format("~/protocolanalyzer/{0}/resources", _seriesSeos(productSeries.ProductSeriesId))
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/protocoloverview.aspx?seriesid={0}", _productSeriesId.Value) ' Cannot randomly generate data
                            hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                        imgProduct.AlternateText = productSeries.Name
                        imgProduct.ImageUrl = productSeries.TitleImage
                        hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                        hlkBreadCrumb.Text = productSeries.Name
                        Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), productSeries.Name, litDocumentType.Text)
                    End If
                End If
            Else
                Dim productSeries As ProductSeries = ProductSeriesRepository.GetProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value)
                If Not (productSeries Is Nothing) Then
                    If (categoryId = CategoryIds.ARBITRARY_WAVEFORM_GENERATORS) Then
                        If (_productSeriesId.Value = 276) Then
                            hlkProduct.NavigateUrl = "~/arbstudio/"
                        ElseIf (_productSeriesId.Value = 399) Then
                            hlkProduct.NavigateUrl = "~/wavestation/"
                        Else
                            hlkProduct.NavigateUrl = "~/waveform-function-generators/"
                        End If
                    ElseIf (categoryId = CategoryIds.LOGIC_ANALYZERS) Then
                        hlkProduct.NavigateUrl = "~/logicstudio/?capid=131&mid=1051"
                    ElseIf (categoryId = CategoryIds.PERT3_SYSTEMS) Then
                        hlkProduct.NavigateUrl = "~/pert3/?capid=131&mid=1019"
                    ElseIf (categoryId = CategoryIds.SPARQ) Then
                        hlkProduct.NavigateUrl = "~/sparq/?capid=131&mid=1042"
                    ElseIf (categoryId = CategoryIds.SI_STUDIO) Then
                        hlkProduct.NavigateUrl = "~/sistudio/?capid=131&mid=1084"
                    ElseIf (categoryId = 35) Then   'TDR and S-Parameters
                        hlkProduct.NavigateUrl = "~/tdr-and-s-parameters/default.aspx"
                    ElseIf (categoryId = 36) Then   'Spectrum Analyzers
                        hlkProduct.NavigateUrl = "~/spectrum-analyzers/default.aspx"
                    ElseIf (categoryId = 37) Then   'Power Supplies
                        hlkProduct.NavigateUrl = "~/power-supplies/default.aspx"
                    ElseIf (categoryId = 38) Then   'Digital MM
                        hlkProduct.NavigateUrl = "~/digital-multimeters/default.aspx"
                    ElseIf (categoryId = 39) Then   'Electronic Loads
                        hlkProduct.NavigateUrl = "~/electronic-loads/default.aspx"
                    ElseIf (categoryId = CategoryIds.PROBES) Then
                        If (_seriesSeos.ContainsKey(_productSeriesId.Value)) Then
                            Dim seriesName As String = _seriesSeos(_productSeriesId.Value)
                            If (seriesName.Contains(".")) Then seriesName = String.Concat(seriesName, "/")
                            hlkProduct.NavigateUrl = String.Format("~/probes/{0}", seriesName)
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/probes/probeseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                    ElseIf (categoryId = CategoryIds.SOFTWARE_OPTIONS) Then
                        If (_productGroupId.HasValue) Then
                            hlkProduct.NavigateUrl = String.Format("/options/productseries.aspx?mseries={0}&groupid={1}", productSeries.ProductSeriesId, _productGroupId.Value)
                        Else
                            hlkProduct.NavigateUrl = String.Format("/options/productseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                        End If
                    Else
                        hlkProduct.NavigateUrl = String.Format("~/oscilloscope/oscilloscopeseries.aspx?mseries={0}", productSeries.ProductSeriesId)
                    End If
                    imgProduct.AlternateText = productSeries.Name
                    imgProduct.ImageUrl = productSeries.TitleImage
                    If (_categorySeos.ContainsKey(categoryId) AndAlso _seriesSeos.ContainsKey(productSeries.ProductSeriesId)) Then
                        hlkDocumentTypes.NavigateUrl = String.Format("~/{0}/{1}/resources", _categorySeos(categoryId), _seriesSeos(productSeries.ProductSeriesId))
                    Else
                        hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?mseries={0}", productSeries.ProductSeriesId)
                    End If
                    hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                    hlkBreadCrumb.Text = productSeries.Name
                    Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), productSeries.Name, litDocumentType.Text)
                End If
            End If
        ElseIf (_protocolStandardId.HasValue) Then
            Dim protocolStandard As ProtocolStandard = ProtocolStandardRepository.GetProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value)
            If Not (protocolStandard Is Nothing) Then
                If (_productSeriesId.HasValue) Then
                    hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}/{1}", _protocolSeos(protocolStandard.ProtocolStandardId), _seriesSeos(_productSeriesId.Value))
                Else
                    hlkProduct.NavigateUrl = String.Format("~/protocolanalyzer/{0}", _protocolSeos(protocolStandard.ProtocolStandardId))
                End If
                imgProduct.AlternateText = protocolStandard.Name
                imgProduct.ImageUrl = String.Format("/images/category/{0}", protocolStandard.ProductImage)
                hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?standardid={0}", _protocolStandardId.Value)
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = protocolStandard.Name
                Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), protocolStandard.Name, litDocumentType.Text)
            End If
        ElseIf (_categoryId.HasValue) Then
            Dim category As Category = CategoryRepository.GetCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), _categoryId.Value)
            If Not (category Is Nothing) Then
                hlkProduct.NavigateUrl = String.Format("~{0}", category.Url)
                imgProduct.AlternateText = category.CategoryName
                imgProduct.ImageUrl = category.Image
                If (_categoryId.Value = CategoryIds.OSCILLOSCOPES) Then
                    If (_productSeriesId.HasValue) Then
                        hlkDocumentTypes.NavigateUrl = String.Format("~/oscilloscope/{0}/resources", _seriesSeos(_productSeriesId.Value))
                    Else
                        hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?categoryid={0}", _categoryId.Value)
                    End If
                ElseIf (_categoryId.Value = CategoryIds.PROBES) Then
                    If (_productSeriesId.HasValue) Then
                        hlkDocumentTypes.NavigateUrl = String.Format("~/probes/{0}/resources", _seriesSeos(_productSeriesId.Value))
                    Else
                        hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?categoryid={0}", _categoryId.Value)
                    End If
                ElseIf (_categoryId.Value = CategoryIds.PROTOCOL_ANALYZERS) Then
                    If (_protocolStandardId.HasValue) Then
                        hlkDocumentTypes.NavigateUrl = String.Format("~/protocolanalyzer/{0}/resources", _protocolSeos(_protocolStandardId.Value))
                    Else
                        hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?categoryid={0}", _categoryId.Value)
                    End If
                Else
                    hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?categoryid={0}", _categoryId.Value)
                End If
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = category.CategoryName
                Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), category.CategoryName, litDocumentType.Text)
            End If
        ElseIf (_productGroupId.HasValue) Then
            Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productGroupId.Value)
            If Not (productGroup Is Nothing) Then
                _categoryId = GetCategoryIdForUnknowns(productGroup.GroupId)
                If (_categoryId > 0) Then
                    If (_categoryId = CategoryIds.PROBES) Then
                        hlkProduct.NavigateUrl = "~/probes/default.aspx"
                    Else
                        If (GetCategoryIdForUnknowns(productGroup.GroupId) = CategoryIds.PROBES) Then
                            hlkProduct.NavigateUrl = "~/probes/default.aspx"
                        Else
                            hlkProduct.NavigateUrl = String.Format("~/options/default.aspx?categoryid={0}&groupid={1}", _categoryId, productGroup.GroupId)
                        End If
                    End If
                Else
                    hlkProduct.NavigateUrl = "~/"
                End If
                imgProduct.AlternateText = productGroup.GroupName
                imgProduct.ImageUrl = productGroup.Image
                hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?groupid={0}", _productGroupId.Value)
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = productGroup.GroupName
                Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), productGroup.GroupName, litDocumentType.Text)
            End If
        ElseIf (_productId.HasValue) Then
            Dim product As Product = ProductRepository.GetProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value)
            If Not (product Is Nothing) Then
                _categoryId = GetCategoryIdForUnknowns(product.GroupId)
                If (_categoryId > 0) Then
                    If (GetCategoryIdForUnknowns(product.GroupId) = CategoryIds.PROBES) Then
                        hlkProduct.NavigateUrl = "~/probes/default.aspx"
                    ElseIf (product.ProductId > 0) Then
                        hlkProduct.NavigateUrl = String.Format("~/options/productdetails.aspx?categoryid={0}&modelid={1}&groupid={2}", _categoryId, product.ProductId, product.GroupId)
                    Else
                        hlkProduct.NavigateUrl = String.Format("~/options/default.aspx?categoryid={0}&modelid={1}&groupid={2}", _categoryId, product.ProductId, product.GroupId)
                    End If
                Else
                    hlkProduct.NavigateUrl = "~/"
                End If
                imgProduct.AlternateText = product.Name
                'If Not (String.IsNullOrEmpty(product.IntroImagePath)) Then
                '    imgProduct.ImageUrl = product.IntroImagePath
                'Else
                imgProduct.ImageUrl = "~/images/spacer.gif"
                'End If
                hlkDocumentTypes.NavigateUrl = String.Format("~/resources/default.aspx?modelid={0}", _productId.Value)
                hlkBreadCrumb.NavigateUrl = hlkProduct.NavigateUrl
                hlkBreadCrumb.Text = product.Name
                Page.Title = String.Format("{0} - {1} {2}", ConfigurationManager.AppSettings("DefaultPageTitle"), product.Name, litDocumentType.Text)
            End If
        End If
        BindAnchorLinks()
        BindRepeater()
    End Sub

    Private Sub SetDocumentTypeLiteral()
        Dim docType As DocumentType = DocumentTypeRepository.GetDocumentType(ConfigurationManager.AppSettings("ConnectionString").ToString(), _docTypeId)
        If (docType Is Nothing) Then
            HandleBouncedRedirect()
        End If
        litDocumentType.Text = docType.Name
        litDocumentTypeTop.Text = litDocumentType.Text
    End Sub

    Private Sub BindAnchorLinks()
        Dim data As List(Of ExtendedDocumentFeatured) = New List(Of ExtendedDocumentFeatured)
        If (_productId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProduct(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productId.Value).ToList()
        ElseIf (_productSeriesId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProductSeries(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productSeriesId.Value).ToList()
        ElseIf (_productGroupId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), _productGroupId.Value).ToList()
        ElseIf (_protocolStandardId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForProtocolStandard(ConfigurationManager.AppSettings("ConnectionString").ToString(), _protocolStandardId.Value).ToList()
        ElseIf (_categoryId.HasValue) Then
            data = FeDocumentManager.GetDocumentFeaturedForCategory(ConfigurationManager.AppSettings("ConnectionString").ToString(), _categoryId.Value).ToList()
        End If
        'If (data.Count > 0) Then
        '    Dim distinctDocumentIds As List(Of Int32) = data.Select(Function(x) x.DocumentFkId).Distinct().ToList()
        '    Dim data2 As List(Of ExtendedDocumentFeatured) = New List(Of ExtendedDocumentFeatured)
        '    Dim addedTo As List(Of Int32) = New List(Of Int32)
        '    For Each d In data
        '        If (distinctDocumentIds.Contains(d.DocumentFkId) And Not addedTo.Contains(d.DocumentFkId)) Then
        '            data2.Add(d)
        '            addedTo.Add(d.DocumentFkId)
        '        End If
        '    Next    '(From d In data Where distinctDocumentIds.Contains(d.DocumentFkId) Select d Order By d.SortId, d.Document.Title).ToList()
        '    data2 = data2.OrderBy(Function(x) x.SortId).ThenBy(Function(x) x.Document.Title).ToList()
        '    If (data2.Count > 0) Then
        '        dlAnchorLinks.DataSource = data2
        '        dlAnchorLinks.DataBind()
        '    End If
        'End If
    End Sub

    Private Sub BindRepeater()
        Dim documents As List(Of Document) = New List(Of Document)
        If (_docTypeId = DocumentTypeIds.VIDEOS) Then
            documents = FeDocumentManager.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _docTypeId, _categoryId, _productSeriesId, _productGroupId, _protocolStandardId, _productId).OrderBy(Function(x) x.SortId.Value).ToList()
        Else
            documents = FeDocumentManager.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _docTypeId, _categoryId, _productSeriesId, _productGroupId, _protocolStandardId, _productId).OrderBy(Function(x) x.Title).ToList()
            If (_protocolStandardId.HasValue) Then
                If (_protocolStandardId.Value = 3) Then ' PCI Express only as per HD JWIR-9PUUBM
                    documents = documents.OrderBy(Function(x) x.SortId.Value).ToList()
                End If
                If (_protocolStandardId.Value = 17) Then    ' Requested by MS TASK0153533
                    documents = documents.OrderBy(Function(x) x.SortId.Value).ThenBy(Function(x) x.Title).ToList()
                End If
            End If
        End If
        If (documents.Count <= 0) Then
            HandleBouncedRedirect()
        End If

        If Not (String.IsNullOrEmpty(_documentVersion)) Then
            documents = documents.Where(Function(x) x.Version.ToLower() = _documentVersion).ToList()
        End If

        _documentXrefs = DocumentXRefRepository.GetDocumentXRefs(ConfigurationManager.AppSettings("ConnectionString").ToString(), documents.Select(Function(x) x.DocumentId).Distinct().ToList())
        If (_documentXrefs.Count > 0) Then
            _childDocumentsViaXref = DocumentRepository.GetDocuments(ConfigurationManager.AppSettings("ConnectionString").ToString(), _documentXrefs.Select(Function(x) x.ChildDocumentFkId).Distinct().ToList()).Where(Function(x) x.Post.ToUpper() = "Y" Or x.Post.ToUpper() = "D").ToList()
        End If
        rptDocuments.DataSource = documents
        rptDocuments.DataBind()
    End Sub

    Private Sub HandleBouncedRedirect()
        Response.Redirect("~/support/techlib/")
    End Sub

    Private Function GetCategoryIdForUnknowns(ByVal productGroupId As Int32) As Int32
        Dim productGroup As ProductGroup = ProductGroupRepository.GetProductGroup(ConfigurationManager.AppSettings("ConnectionString").ToString(), productGroupId)
        If Not (productGroup Is Nothing) Then
            Return productGroup.CategoryId
        End If
        Return -1
    End Function
#End Region
End Class