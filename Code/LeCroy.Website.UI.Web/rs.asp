<%@ Language=VBScript %>
<% sAgent = Request.ServerVariables("HTTP_USER_AGENT")%>
<!--#include virtual="/cgi-bin/shared/browserdetect.asp" -->
<!--#include virtual="/include/standard.inc" -->
<!--#include virtual="/include/global.asp" -->

<%	'**************Every Page must include the header.asp file******************
	'**************DO NOT CHANGE THE FOLLOWING LISTING UNLESS OTHERWISE NOTED**************************
	PageTitle="Teledyne LeCroy Web Site" ' Title of Web Page
	PageDescription="LeCroy Corporation's Web Site" 'Description of Page
	PageKeywords="LeCroy, Digital Oscilloscopes,Analog Oscilloscopes" ' List of Keywords for Search Engines
	banner="testmeasurement" 'other available - see banners.txt for more details
	topic="products" ' This variable must appear on all pages - this sets which menu is highlights in the contents. This value also reference the previous banner topic.
	bgimage="" 'sets the background image for the document
	language="us" ' Sets the language of the current page
	division_id=1 'Sets the Division ID
	topic_id=1
	session("language") = "us"
	country_id=208
%>

<html>
<head>
<meta NAME="Title" Content="<% = PageTitle %>">
<meta NAME="Description" Content="<% = PageDescription %>">
<meta NAME="keywords" Content="<% = PageKeywords %>">
<%'Check for Browser Version and Display Appropriate StyleSheet %>
<% if bIsie then %>
	<!--#include virtual="/include/ie.asp" -->
<% elseif bIsNS then %>
	<!--#include virtual="/include/ns.asp" -->
<% elseif bIsMac then %>
	<!--#include virtual="/include/mac.asp" -->
<% end if %>


<title><% = PageTitle %></title>
<body <% if bgimage="" then %><% else %>background="nav/images/<% = bgimage %>" <% end if %>LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0" <%if bgcolor="" then%><%else%>bgcolor="<% = bgcolor %>" <% end if %>>
<!--#include virtual="/include/rollovers.asp" -->
<a NAME="TopofPage"></a>
<table border="0" width="<% if pagewidth="" then %>800<% else %><% = pagewidth %><%end if %>" cellpadding="0" cellspacing="0"> 
  <tr>
    <td><!--#include virtual="/include/header.asp"--></td>
  </tr>
 </table>
 <% if bIsie then %>
 <!--#include virtual="/archive/menus/default.asp"-->
 <% end if %>	
 <table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
    <td valign="top">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr height="10">
			  <td width="100" valign="top" rowspan="2" bgcolor="<% = tocbgcolor %>">
              <p align="center"><br>
              <br>
              <a href="default.asp"><img border="0" src="images/B-Arrow-green-bluebkg-PrevPg.gif" WIDTH="25" HEIGHT="36"></a>
			  </td>
			  <td valign="top"></td>
			  <td valign="top"></td>
			  <td width="10" valign="top" align="left">
              </td>
			</tr>
			<tr>
			  <td valign="top"></td>
			  <td width="90%" valign="top">
				<%'Whats New Table %>
				<table border="0" width="500" cellspacing="0" cellpadding="0">
					<tr>
						<td width="450">
						</td>
						<td>
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td rowspan="2" width="450">
                            <p align="center"><img border="0" src="images/Exhibition.gif" align="left" WIDTH="455" HEIGHT="37"><br>
                            <br>
                            <br>
                            <table border="0" cellpadding="10" cellspacing="0" width="100%">
                              <tr>
                                <td valign="top"><a href="http://ds.pennwellnet.com/Events/DS/ds_hm2001/ds_exhibitor-prospectus_2001.cfm" onclick="GaEventPush('OutboundLink', 'ds.pennwellnet.com/Events/DS/ds_hm2001/ds_exhibitor-prospectus_2001.cfm');"><img border="0" src="images/Logos/Exhibits/DataStorage.gif" WIDTH="218" HEIGHT="120"></a><font size="1"><font face="verdana,arial,helvetica"><br>
                                  <br>
                                  </font><font face="verdana,arial,helvetica" color="#000000">Place:
                                  Las Vegas, Nevada<br>
                                  <br>
                                  </font><font face="verdana,arial,helvetica">Date:
                                  November <font COLOR="#000000">10th
                                  - 11th</font></font></font></td>
                                <td width="80%"><font color="#1E90FF" face="verdana,arial,helvetica" size="2"><a href="http://ds.pennwellnet.com/Events/DS/ds_hm2001/ds_exhibitor-prospectus_2001.cfm" onclick="GaEventPush('OutboundLink', 'ds.pennwellnet.com/Events/DS/ds_hm2001/ds_exhibitor-prospectus_2001.cfm');"><b>LeCroy
                                  will be presenting at Head/Media -Las Vegas</b></a></font><font face="verdana,arial,helvetica"><br>
                                  <font COLOR="#000000"><br>
                                  Stardust Hotel<br>
                                  Las Vegas - Nevada</font></font>
                                  <font COLOR="#000000"><br>
                                  </font></td>
                              </tr>
                              <tr>
                                <td colspan="2" valign="top">
                                  <hr>
                                </td>
                              </tr>
                              <tr>
                                <td valign="top"></td>
                                <td width="80%"></td>
                              </tr>
                            </table>
                            <p>&nbsp;</p>
						</td>
						<td valign="top">
                            <img border="0" src="Nav/Images/FolderVertLine.gif" WIDTH="20" HEIGHT="200">
						</td>
						<td valign="top">
                            <br>
						</td>
					</tr>
					<tr>
						<td valign="bottom">
						</td>
						<td valign="bottom">
						</td>
					</tr>
				</table>
			  </td>
			  <td valign="top">
                <p>
            &nbsp;&nbsp;</p>
              </td>
			</tr>
		<tr>
			<td class="SideNav" bgcolor="<% = tocbgcolor %>"><img SRC="/Nav/Images/DottedLine100.gif" alt WIDTH="100" HEIGHT="6"><br>
				<nobr><font face="Verdana,Arial" size="1" class="SideNav"><a class="SideNav" style="color:<% =menufontcolor %>" href="default.asp#TopofPage"><font color="<% = menufontcolor %>">Top of Page</font></a><img SRC="/Nav/Images/UpArrowAnim.gif" alt="Bak to Top" WIDTH="10" HEIGHT="10"></font></nobr>
			</td>
			<td valign="bottom"><img SRC="/Nav/Images/BottomNavCurveUp.gif" alt WIDTH="40" HEIGHT="40"></td>
			<td><!--#include virtual="/include/InternationalSites.asp" --></td>
            <td></td>
		</tr>
		<tr>
			<td bgcolor="<% = tocbgcolor %>">&nbsp;</td>
			<td bgcolor="<% = BNavBGColor %>">&nbsp;</td>
			<td bgcolor="<% = BNavBGColor %>" colspan="2"><nobr><!--#include virtual="include/Footer.asp" --></nobr></td>
		</tr>
    </table>
    </td>
  </tr>
</table>
<a NAME="BottomofPage"></a>
</body>
</html>
