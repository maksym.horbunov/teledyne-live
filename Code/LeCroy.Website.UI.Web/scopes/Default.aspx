﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false" Inherits="LeCroy.Website.scopes_default" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <div id="SlideshowContainer">
        <div id="Slideshow">
            <!-- ws4000hd --><div id="card11" class="slideTop" onclick="window.location='/ws4000hd/'" style="cursor: pointer;"></div>
            <!-- wr8000hd --><div id="card12" class="slideTop" onclick="window.location='/wr8000hd/'" style="cursor: pointer;"></div>
            <!-- 12bits --><div id="card13" class="slideTop" onclick="window.location='/hdo/'" style="cursor: pointer;"></div>
            <!-- mda8000hd --><div id="card14" class="slideTop" onclick="window.location='/static-dynamic-complete/'" style="cursor: pointer;"></div>
            <!-- wavepulser --><div id="card15" class="slideTop" onclick="window.location='/wavepulser/'" style="cursor: pointer;"></div>
            <!-- waveprodhd --><div id="card16" class="slideTop" onclick="window.location='/waveprohd/'" style="cursor: pointer;"></div>
            <!-- wr9000 --><div id="card17" class="slideTop" onclick="window.location='/wr9000/'" style="cursor: pointer;"></div>
            <!-- HDA125 --><div id="card18" class="slideTop" onclick="window.location='/options/productdetails.aspx?modelid=9703&categoryid=18&groupid=54'" style="cursor: pointer;"></div>           
 </div>
        <div id="SlideshowPrev"></div>
        <div id="SlideshowNext"></div>
    </div>
    <div class="container">
        <div id="content">
            <div id="subfeature">
                <ul>
                    <li class="left"><a href="/probes/" style="text-decoration: none">
                        <img src="/images/home-probes-sm.png" alt="Oscilloscope Probes" class="align-left" width="146" height="150" border="0" /></a>
                        <div id="subfeatureheadleft"><a href="/probes/" style="text-decoration: none">Probes</a></div>
                        <div id="subfeaturesubheadleft"><a href="/probes/" style="text-decoration: none"><br />World class<br />probes and<br />amplifiers</a></div>
                    </li>
                    <li class="right2">
                        <a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none"><img src="https://assets.lcry.net/images/wavesurfer-3000z_home.png" alt="WaveSurfer 3000z Oscilloscope" width="158" height="150" class="align-right" border="0" /></a>
                        <div id="subfeatureheadright"><a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none">WaveSurfer<br />3000z</a></div>
                        <div id="subfeaturetextright"><a href="/oscilloscope/wavesurfer-3000z-oscilloscopes" style="text-decoration: none">100 MHz to 1 GHz<br /><br />with MAUI Advanced<br />User Interface</a></div>
                    </li>
                    <li class="right2"><a href="/maui/" style="text-decoration: none">
                        <img src="/images/maui-home.png" alt="MAUI Most Advanced User Interface" width="158" height="146" class="align-right" border="0" /></a>
                        <div id="subfeatureheadright"><a href="/maui/" style="text-decoration: none">MAUI</a></div>
                        <div id="subfeaturetextright"><a href="/maui/" style="text-decoration: none"><b>Most Advanced</b><br /><b>User Interface</b><br /><br />Designed for touch<br />Built for simplicity<br />Made to solve.</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>