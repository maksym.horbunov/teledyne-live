Partial Class scopes_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        InjectCssIntoHeadElement(ConfigurationManager.AppSettings("CssHomepage"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpCycle"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHoverIntent"))
        InjectJsIntoHeadElement(ConfigurationManager.AppSettings("JsHpCommon"))
        InjectIEAlternateCss()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Page.Header.DataBind()  ' NOTE: Needed for inline ResolveUrl's
        If (Session("localeid") Is Nothing Or Len(Session("localeid")) = 0) Then
            Session("localeid") = 1033
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
    Private Sub InjectCssIntoHeadElement(ByVal resourceUrl As String)
        Dim control As HtmlLink = New HtmlLink()
        control.Href = ResolveUrl(resourceUrl)
        control.Attributes.Add("rel", "stylesheet")
        control.Attributes.Add("type", "text/css")
        CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
    End Sub

    Private Sub InjectJsIntoHeadElement(ByVal resourceUrl As String)
        Dim control As HtmlGenericControl = New HtmlGenericControl("script")
        control.Attributes.Add("type", "text/javascript")
        control.Attributes.Add("language", "javascript")
        control.Attributes.Add("src", ResolveUrl(resourceUrl))
        CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(control)
    End Sub

    Private Sub InjectIEAlternateCss()
        Dim literal As Literal = New Literal()
        Dim sb As StringBuilder = New StringBuilder
        Dim alternateIE7Header As String = String.Format("<link type=""text/css"" rel=""stylesheet"" href=""{0}"" />", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateHeaderIE7")))
        sb.AppendFormat("<!--[if IE 7]><link type=""text/css"" rel=""stylesheet"" href=""{0}"" />{1}<![endif]-->", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateIE7")), alternateIE7Header)
        sb.AppendFormat("<!--[if IE 6]><link type=""text/css"" rel=""stylesheet"" href=""{0}"" /><![endif]-->", ResolveUrl(ConfigurationManager.AppSettings("CssAlternateIE6")))
        literal.Text = sb.ToString()
        CType(Me.Master.FindControl("Head1"), HtmlHead).Controls.Add(literal)
    End Sub
#End Region
End Class