﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.SDAIII_Default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="370" valign="top">
                    <h1 style="font-size:24px; padding-top:30px;">SDAIII-CompleteLinQ</h1>
                    <h3>Serial Data, Noise and Crosstalk Analysis Option Set</h3>
                    <p>Designers of multi-lane signal systems require multi-lane analysis. Unleash the power of multi-lane SDA analysis to perform eye and jitter measurements on up to four lanes.</p>
                    <!-- <a href="sdaiii_feature.html?KeepThis=true&TB_iframe=true&height=500&width=550" class="thickbox"><img src="/images/sdaiii_option_set_breakdown.png" border="0" /></a> -->
                </td>
                <td width="248" valign="top"><img src="<%=rootDir %>/images/sdaiii_main.png" width="242" height="278" border="0" /></td>
                <td width="203" valign="top">
                    <div class="subNav">
                        <ul>
                            <li><a href="<%=rootDir %>/options/productseries.aspx?mseries=405&groupid=103">Explore SDAIII-CompleteLinQ Features</a></li>
                            <!--<li><a href="sdaiii_feature.html?KeepThis=true&TB_iframe=true&height=500&width=550" class="thickbox">SDAIII Family Breakdown</a></li>-->
                            <li><a href="/support/techlib/videos.aspx?capid=106&mid=528&smid=662&docid=7563">SDAIII Introductory Video</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <h3>Simultaneous Analysis of Multiple Lanes</h3>
                    <p>Use multi-lane analysis to simultaneously measure, view and compare eye diagrams and results from up to 4 lanes and/or configurations.</p>
                    <h3>New Vertical Noise and Crosstalk Analysis Package</h3>
                    <p>Gain insight into sources of crosstalk and interference with capabilities that can only be found on Teledyne LeCroy real-time oscilloscopes.</p>
                    <h3>Extended SDA-CompleteLinQ Interface</h3>
                    <p>The SDAIII "framework flow dialog" provides access to all eye, jitter and noise analysis capabilities and signal integrity toolkits.</p>
                    <h3>Most Complete Set of Jitter, Eye and Analysis Tools</h3>
                    <p>Access the most complete set of serial data analysis tools to form eye diagrams and decompose jitter into Tj, Rj, Dj and beyond.</p>
                </td>
                <td width="75px">&nbsp;</td>
                <td valign="top"><img src="<%=rootDir %>/images/sdaiii_middle.png" height="250px" /></td>
            </tr>
        </table>
        <div class="rounded2">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left" valign="top">
                        <br /><br />
                        <h1>Compatibility</h1>
                        <h4>SDAIII-CompleteLinQ is compatible with: <ul><li><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=341">WavePro/SDA/DDA 7 Zi/Zi-A Oscilloscopes</a></li><li><a href="/oscilloscope/oscilloscopeseries.aspx?mseries=329">WaveMaster/SDA/DDA 8 Zi/Zi-A Oscilloscopes</a></li><li><a href="/labmaster/">LabMaster 9 Zi-A and 10 Zi Oscilloscopes</a></li></ul></h4><br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>