﻿Imports LeCroy.Library.VBUtilities

Public Class SDAIII_Default
    Inherits BasePage

#Region "Variables/Keys"
    Public Shared menuURL As String = ""
#End Region

#Region "Page Events"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim css As HtmlLink = New HtmlLink()
        css.Href = ResolveUrl(ConfigurationManager.AppSettings("CssThickBox"))
        css.Attributes.Add("rel", "stylesheet")
        css.Attributes.Add("type", "text/css")
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(css)
        Dim jQueryUIMin As HtmlGenericControl = New HtmlGenericControl("script")
        jQueryUIMin.Attributes.Add("type", "text/javascript")
        jQueryUIMin.Attributes.Add("language", "javascript")
        jQueryUIMin.Attributes.Add("src", ResolveUrl(ConfigurationManager.AppSettings("JsThickbox")))
        CType(Me.Master.Master.FindControl("Head1"), HtmlHead).Controls.Add(jQueryUIMin)
        Me.Master.PageContentHeader.TopText = "Products"
        Me.Master.PageContentHeader.BottomText = "SDAIII-CompleteLinQ"
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - SDAIII"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String = AppConstants.PRODUCT_CAPTION_ID
        Dim menuID As String

        '** MenuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        If Not Me.IsPostBack Then
        End If
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class