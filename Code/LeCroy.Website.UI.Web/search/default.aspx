﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.search_default" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
<link href="/css/cludo-search.css" type="text/css" rel="stylesheet">
<!--[if lte IE 9]>
<script src="https://api.cludo.com/scripts/xdomain.js" slave="https://api.cludo.com/proxy.html" type="text/javascript"></script>
<![endif]-->

         

    <div id="cludo-search-results">
    <div class="cludo-r">
        <div class="cludo-c-12">
            <div class="search-result-count"></div>
            <div class="search-did-you-mean"></div> 
            <div class="search-results"></div>
        </div>           
    </div>        
</div>       


<!-- www.Cludo.com Search body init script start -->
<script type="text/javascript" src="/js/cludo-search-script.min.js"></script>
<script>
var CludoSearch;
(function () {
  var cludoSettings = {
    customerId: 610,
    engineId: 8723,
    searchUrl: '/search/default.aspx',
    language: 'en',
    searchInputs: ['cludoquery'],
    type: 'inline',
    hideSearchFilters: true
    };
  CludoSearch= new Cludo(cludoSettings);
  CludoSearch.init();
})();</script>
<!-- www.Cludo.com Search body init script end -->

</asp:Content>