﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="austinlabs-training.aspx.vb" Inherits="LeCroy.Website.austinlabs_training" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-2.jpg"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12"><span class="page">Austin Labs</span>
									<h1>A full line of advanced Protocol Training classes</h1>
									<p>Instructor-led classes, guidings through the protocol specifications, hands on labs with trace analysis</p>
								</div>
							</div>
						</div>
					</div>
				</section>
		<section class="section-angle section-training-solutions">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12">
								<div class="section-heading text-center mx-auto">
									<h2 class="title px-0 mx-md-0">Enterprise Protocol Training Solutions</h2>
									<div class="row">
										<div class="col col-md-7 col-lg-7 col-xl-5 mx-auto">
											<p>In person and online instructor-lead protocol training courses</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<p>Austin Labs offers a full line of advanced protocol training classes. All classes are instructor led and guide students through the protocol specifications while using hands on labs with trace analysis to help students discover how the protocol is implemented.</p>
							</div>
							<div class="col-lg-6">
								<p>To create a reliable product, the user must first understand the technology and know how to use the tools needed to develop, test and debug the relevant host or peripheral specification. Here at Austin Labs we provide everything you need.</p>
							</div>
                            <div class="holder-btn-more col-lg-12 text-center "> 
                                <div class="link-more" style="padding-bottom: 100px;"><a class="link-arrow" href="austinlabs-faq.aspx"><span>Explore Protocol Training FAQs</span><i class="icon-btn-right-01"></i></a></div>
                            </div>
						</div>
					</div>
				</section>

		<section class="section-training-solutions section-gray section-angle-before">
					<div class="container">
						<div class="row justify-content-center"> 
							<div class="col-sm-12 col-lg-9">
								<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
									<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Upcoming Courses Offerings</h2>
								</div>
							</div>
						</div>
			<div class="holder-slider">
				<div class="slider-events">

				</div>
				<!--<div class="slider-controls">
					<div class="wrap-arrows"></div>
				</div>-->

			</div>
		</div>
	</section>
		<section class="section-izotope section-angle section-white">
					<div class="container">
						<div class="justify-content-center">                        
							<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
								<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Protocol Class Information</h2>
							</div>
						</div>
					</div>
					<div class="container">                                                                                                         
						<div class="filter-button-group izotope-filters" id="filters">
							<button class="button is-checked" data-filter="*">All</button>
							<button class="button" data-filter=".PCIe">PCIe</button>
							<button class="button" data-filter=".NVME">NVME</button>
							<button class="button" data-filter=".USB">USB </button>
							<button class="button" data-filter=".SAS">SAS</button>
                            <button class="button" data-filter=".iSCSI">iSCSI</button>
							<button class="button" data-filter=".Fibre">Fibre Channel</button>
						</div>
						<div class="grid">
							<div class="col-xl-4 element-item transition PCIe" data-category="PCIe">                          
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-1.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">PCIe Architecture and Functionality</h4>
											<div class="card-text">
												<p>One-Day Intensive. Topics include overview of PCI Express components, terminology, and architecture, link training, communication layers, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 1 day</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-pcie-intensive">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition PCIe" data-category="PCIe">     
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-2.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">PCIe Protocol</h4>
											<div class="card-text">
												<p>Topics include introduction to PCI Express components, terminology and architecture, configuration and enumeration, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-pcie-protocol">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition NVME" data-category="NVME">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-3.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">NVMe Protocol</h4>
											<div class="card-text">
												<p>Topics include NVMe components and terminology, device command sets, QoS and flow control implementations, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-nvme">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition NVME" data-category="NVME">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-4.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">NVMe over Fibre Channel</h4>
											<div class="card-text">
												<p>Topics include the fundamentals of Fibre Channel and NVMe, NVMe over Fabrics, binding specifications, how NVMe runs over FC, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-nvme-over-fibre-channel">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition NVME" data-category="NVME">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-5.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">NVMe over RoCE</h4>
											<div class="card-text">
												<p>Topics include NVMe architecture and commands, networking and the OSI model, how NVMe runs over Ethernet, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-nvme-over-roce">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition NVME" data-category="NVME">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-6.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">NVMe over TCP</h4>
											<div class="card-text">
												<p>Topics include Ethernet and TCP/IP review, NVMe architecture, NVMe mapping to the OSI model and encapsulation in Ethernet, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-nvme-over-tcp">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition USB" data-category="USB">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-7.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">Intro to USB – 1.0 through 3.2</h4>
											<div class="card-text">
												<p>Topics include the history of USB, topology, 2.x and 3.x architecture and terminology, configuration, transfer types, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 1 day</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-intro-usb">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition USB" data-category="USB">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-8.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">USB4 Protocol</h4>
											<div class="card-text">
												<p>Topics include architecture and terminology, link initialization, router configuration and enumeration, flow control, tunneling, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-usb4">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition iSCSI" data-category="iSCSI">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-9.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">iSCSI</h4>
											<div class="card-text">
												<p>Topics include fundamentals of iSCSI, architecture and network, communications model, connection setup and management, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-iscsi">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition SAS" data-category="SAS">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-10.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">SAS 4.0 Protocol</h4>
											<div class="card-text">
												<p>Topics include fundamental elements of SAS, expanders and SAS Management Protocol, tunneling, SCSi Protocol Transport Layer, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-sas">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition Fibre" data-category="Fibre Channel">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-11.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">Fibre Channel Protocol</h4>
											<div class="card-text">
												<p>Topics include NVMe components and terminology, device command sets, QoS and flow control implementations, and more.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> 4 days</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-fibre-channel">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 element-item transition Fibre" data-category="Fibre Channel">
								<div class="card-line p-0">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/protokol-12.jpg"></span></div>
									<div class="inner-content">
										<div class="inner-info">
											<h4 class="title">Fibre Channel over Ethernet</h4>
											<div class="card-text">
												<p>Free software to capture protocol traffic (hardware required), troubleshooth issues, and collaborate with peers.</p>
											</div>
										</div>
										<div class="footer-slide d-flex align-items-center justify-space">
											<div class="hold-date"><span class="date"><i class="icon-calendar-1"></i> Aug 11-14</span></div><a class="btn btn-default" href="https://teledynelecroy.com/doc/austin-labs-fibre-channel-over-ethernet">Learn More  </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
		<section class="section-explore section-gray">
					<div class="container small">                                            
						<div class="justify-content-center section-heading text-center">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Got a question? Ask us about testing or trainings</h2>
							<div class="sub-title">We’re here to help and answer any question you might have. We look forward to hearing from you</div>
						</div>
						<div class="explore-content">
							<iframe src="https://go.teledynelecroy.com/l/48392/2020-07-24/7zhn6w" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
						</div>
					</div>
				</section>
		<section class="section-explore">
					<div class="container">    
						<div class="card-line horizontale p-0 d-flex">
							<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/explore-2.jpg"></span></div>
							<div class="inner-content">
								<h4 class="title">Explore physical and protocol layer testing solutions</h4>
								<div class="card-text">
									<p>Teledyne LeCroy’s Austin Labs is the premier third-party test and validation center for servers, storage, and network devices. With decades of testing experience, the lab provides customized services to help our customers deliver fully tested products to market on time and within budget. Experience the best test equipment available including Oscilloscopes, Protocol Analyzers, Jammers, Exercisers, BERT’s, and protocol compliance test suites. </p>
								</div>
								<div class="footer-slide"><a class="btn btn-default" href="austinlabs-testing.aspx">Visit Testing Solutions Page</a></div>
							</div>
						</div>
					</div>
				</section>
			</main>
	<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
	<script>

        jQuery(document).ready(function () {
            // init Isotope
            var $grid = jQuery('.grid').isotope({
                itemSelector: '.element-item',
                layoutMode: 'fitRows'
			});

            // filter items on button click
            jQuery('.filter-button-group').on('click', '.button', function ( event ) {
				var filterValue = jQuery(this).attr('data-filter');
				console.log(filterValue);
				$grid.isotope({ filter: filterValue });
				event.preventDefault();
			});
            jQuery(".filter-button-group").each(function (i, buttonGroup) {
                var $buttonGroup = jQuery(buttonGroup);
                $buttonGroup.on("click", "button", function () {
                    $buttonGroup.find(".is-checked").removeClass("is-checked");
                    jQuery(this).addClass("is-checked");
                });
			});

			//json for upcoming trainings
            var jsonURL = "/events/output.aspx";
            console.log('setting up events json: %s', jsonURL);
            console.log('site.master relinquished control of the $ variable, must use jQuery instead');
            var now;
            try { now = $.now(); } catch { now = 'undefined' }
            console.log('Now based on $ is %s', now);
            now = jQuery.now();
            console.log('Now based on jQuery is %s', now);

            jQuery.getJSON(jsonURL, { eventtypeid: 5 }, doAll);
            //jQuery(".slider-events").html(outHtml);

		});
        //await json to append before adding js slider styles
        async function doAll(json) {
            await processRequest(json);
            SliderNew();
        }

        function processRequest(json) {
            var gethtml = "";
            for (i = 0; i < 6; i++) {

				var jsonStartDate = json[i].ExhibitionStartDate.replace("/Date(", "").replace("-0400)/", "").replace("-0500)/", "").replace("-0600)/", "");
                var date = new Date(jsonStartDate);
                const month = date.toLocaleString('default', { month: 'long' });
                var dateString = ("0" + date.getUTCDate()).slice(-2);
                dateString = month + "&nbsp;" + dateString;
                var jsonType = json[i].TypeId;
                var jsonTitle = json[i].Title;
                var jsonURL = json[i].Url;
                var jsonDescription = json[i].Description.replace(/(<([^>]+)>)/ig, "");
                var jsonAddress = json[i].Address.replace(/(<([^>]+)>)/ig, " ");
                var jsonCity = json[i].City;
                var jsonState = json[i].State;
                var jsonTimeZone = json[i].TimeZone;
				var location = "";
				var country = json[i].country;
                jsonTitle = '<h6 class="title-slide">' + jsonTitle + '</h6>';
                jsonURL = '<a class="btn btn-default" href="' + jsonURL + '">';
				jsonType = '<span class="type type-training">Training</span>';  

                gethtml += '<div class="slide-event">'+jsonTitle
                    + '<div class="footer-slide d-flex"><div class="hold-date d-flex align-items-center"><span class="date"><i class="icon-calendar-1"></i>' + dateString + '</span></div></div>'
                    + jsonURL + 'Learn More</a></div>';

            }
            jQuery('.slider-events').html(gethtml);


        }
        function SliderNew() {
            jQuery(".slider-events").slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                variableWidth: false,
                appendArrows: ".slider-controls .wrap-arrows",
                infinite: false,
                mobileFirst: true,
                // rew: (() => console.log($slider.slickGetOption('slidesToShow')))(),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                            variableWidth: false,
                        },
                    },
                    {
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 3,
                            variableWidth: false,
                        },
                    },
                ],
            });
        }
    </script>

</asp:Content>
