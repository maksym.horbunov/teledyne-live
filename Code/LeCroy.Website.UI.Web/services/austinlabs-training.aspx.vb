﻿Public Class austinlabs_training
    Inherits System.Web.UI.Page
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Austin Labs - Testing and Solutions"

        Master.MetaOgTitle = "Teledyne LeCroy - Austin Labs - Testing and Solutions"
        Master.MetaOgImage = "http://teledynelecroy.com/images/austinlabs_og.png"
        Master.MetaOgDescription = "Teledyne LeCroy’s Austin Labs is the premier third-party test and validation center for servers, storage, and network devices."
        Master.MetaOgUrl = "https://teledynelecroy.com/services/austinlabs-training.aspx"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class