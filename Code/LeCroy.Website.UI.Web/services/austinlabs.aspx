﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="austinlabs.aspx.vb" Inherits="LeCroy.Website.austinlabs" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<main id="main" role="main" class="has-header-transparented skew-style v1 reset">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs6.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Physical and Protocol Layer<br>Testing and Training Solutions</h1>
									<p>Building Technical Expertise, Workplace Efficiency and Product Quality</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12">
								<div class="section-heading text-center mx-auto">
									<h2 class="title px-0 mx-md-0">Testing and Training Solution</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<p>
									  Austin Labs is a leading provider of testing and training services. We focus on server, storage, networking interfaces and protocols.Our test engineers and trainers are experts in PCIe, NVMe, NVMe over Fabrics, Fibre Channel, RoCE, TCP, SAS, SATA, SCSI, iSCSI, and FCoE.</p>
								<p>
									  Austin Labs has helped develop some of the industry’s key technologies and continue to have a vigorous passion for improving products and sharing their knowledge. This experience and enthusiasm translates into the highest quality testing and training services possible.</p>
							    
                            			<div class="visual">
											<picture>
												<source srcset="https://assets.lcry.net/images/austinlabs_logo_small.png 2x"/><img src="https://assets.lcry.net/images/austinlabs_logo_small.png" alt="image description"/>
											</picture>
										</div>    
                            </div>

							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-6">
										<div class="card-line">
											<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/austin-general1.png"></span></div>
											<div class="inner-content">
												<div class="card-title">
													<h3>Testing Solutions</h3>
												</div>
												<div class="card-text">
													<p>
														Industry leading, 3rd-party testing facility focused on physical and protocol layer testing. We use state-of-the-art test equipment and top notch test engineers to provide comprehensive device and system testing services.</p>
												</div>
												<div class="footer-card">                                            <a class="btn btn-default" href="/services/austinlabs-testing.aspx">Explore Testing Solutions</a></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="card-line">
											<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/austin-general2.png"></span></div>
											<div class="inner-content">
												<div class="card-title">
													<h3>Training Solutions</h3>
												</div>
												<div class="card-text">
													<p>
														World class protocol training courses designed for intermediate and advanced students looking to build their expertise in technologies including PCI Express, NVMe, Ethernet, Fibre Channel, FCoE, iSCSI, SAS and more.</p>
												</div>
												<div class="footer-card">                                            <a class="btn btn-default" href="/services/austinlabs-training.aspx">Explore Training Solutions</a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-explore ">
					<div class="container small">                                            
						<div class="justify-content-center section-heading text-center">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Got a question?</h2>
							<div class="sub-title">Leave us your email. We're here to help and answer any question you might have.</div>
						</div>
						<div class="explore-content">
							<iframe src="https://go.teledynelecroy.com/l/48392/2020-07-24/7zhn6w" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
						</div>
					</div>
				</section>
			</main>
</asp:Content>

