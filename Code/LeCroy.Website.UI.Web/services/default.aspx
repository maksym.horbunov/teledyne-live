﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.services_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main class="has-header-transparented skew-style v1 reset" id="main" role="main">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-4.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner"> 
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Services</h1>
									<p>Testing and Training Solutions</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-sm-12">
								<div class="section-heading text-center mx-auto">
									<h2 class="title px-0 mx-md-0">Testing and Training Solutions</h2>
									<div class="row">
										<div class="col-md-12 mx-auto">
											<p>A wide range of testing and training services offerings tailored to help you reach your goals </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row mb-40">
							<div class="col-lg-12">
								<p>Teledyne LeCroy offers an broad array of testing services and educational training options.  From advanced multi-day protocol training courses to device interoperability testing to calibration and testing service,
								Teledyne LeCroy offers valuable expertise, in-depth knowledge, leading edge testing solution, and top-notch engineers able help move your projects forward.
								Whether you need your team to come up to speed on the latest technology or you just need more hands getting your product tested and ready for market, we can help.</p>
							</div>
						</div>
						<div class="row"> 
							<div class="col-md-6 mb-32 small">   
								<div class="card-line p-0"><a href="/services/austinlabs.aspx">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/solutions-services-1.jpg"></span></div>
									<div class="inner-content">
										<h4 class="title">Austin Labs Testing and Training Solutions                                   </h4>
										<div class="card-text">
											<p>Austin Labs is a leading provider of testing and training services. We focus on server, storage, networking interfaces and protocols.</p>
											<p>
											Our test engineers and trainers are experts in PCIe, NVMe, NVMe over Fabrics, Fibre Channel, RoCE, TCP, SAS, SATA, SCSI, iSCSI, and FCoE.</p>
										</div>
									</div>
									<div class="inner-visible p-24 pt-0">                                            <a class="link-arrow" href="/services/austinlabs.aspx"><span>Explore Austin Labs Testing and Training Solutions</span><i class="icon-btn-right-01">    </i></a></div></a>
								</div>
							</div>
							<div class="col-md-6 mb-32 small">   
								<div class="card-line p-0"><a href="/services/frontline.aspx">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/solutions-services-2.jpg"></span></div>
									<div class="inner-content">
										<h4 class="title">Frontline Test Services                                </h4>
										<div class="card-text">
											<p>Frontline Test Services offers consultancy and interoperability services to help in all stages of product development.</p>
											<p>Our test engineers and test solutions are focused on wireless technologies including Bluetooth, WiFi, Thread, and Zigbee.</p>
										</div>
									</div>
									<div class="inner-visible p-24 pt-0">                                           <a class="link-arrow" href="/services/frontline.aspx"><span>Explore Frontline Test Services</span><i class="icon-btn-right-01">    </i></a></div></a>
								</div>
							</div>
							<div class="col-md-6 mb-32 mb-md-0 small">   
								<div class="card-line p-0"> <a href="http://teledyne-ts.com/valvetest.html">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/solutions-services-3.jpg"></span></div>
									<div class="inner-content">
										<h4 class="title">Valve Testing Products & Services                                 </h4>
										<div class="card-text">
											<p>Teledyne Test Services offers industry-unique MOV and AOV calculation engines and databases to automate design margin analysis, actuator setup and many other trending and analysis tasks required of the Nuclear Valve Engineer.</p>
										</div>
									</div>
									<div class="inner-visible p-24 pt-0">                                         <a class="link-arrow" href="http://teledyne-ts.com/valvetest.html"><span>Explore Valve Testing Products & Services</span><i class="icon-btn-right-01">    </i></a></div></a>
								</div>
							</div>
							<div class="col-md-6 small">   
								<div class="card-line p-0"> <a href="https://bethesignal.com/bogatin/">
									<div class="inner-img jumbotron bg-retina"><span data-srcset="https://assets.lcry.net/images/solutions-services-4.png"></span></div>
									<div class="inner-content">
										<h4 class="title">Signal Integrity Academy                                </h4>
										<div class="card-text">
											<p>Learn signal integrity problem solving skills from the Signal Integrity Evangelist, Dr. Eric Bogatin. Whether you are a beginning novice or an experienced engineer, these lessons will accelerate you up the learning curve and help you get to the right answer faster.</p>
										</div>
									</div>
									<div class="inner-visible p-24 pt-0">                                            <a class="link-arrow" href="https://bethesignal.com/bogatin/"><span>Explore Signal Integrity</span><i class="icon-btn-right-01">                          </i></a></div></a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
