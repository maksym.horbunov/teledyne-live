﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="frontline-compliance.aspx.vb"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Bluetooth® Pre-Compliance Testing</h1>
									<p>Highlights problem areas that could prevent your product from passing compliance or qualification testing</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle">
					<div class="container">
						<div class="section-content">
							<div class="row d-flex justify-content-center">
								<div class="col-md-2"><svg width="206" height="206" viewBox="0 0 206 206" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<circle cx="103" cy="103" r="98" fill="white" stroke="white" stroke-width="10"/>
<circle cx="103" cy="103" r="88" fill="white" stroke="#0076C0" stroke-width="10"/>
<mask id="mask0" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="29" y="33" width="148" height="148">
<path d="M29.0771 33.0513H176.923V180.897H29.0771V33.0513Z" fill="#C4C4C4"/>
</mask>
<g mask="url(#mask0)">
<path d="M25.8984 40.2056H179.289V173.267H25.8984V40.2056Z" fill="url(#pattern0)"/>
</g>
<defs>
<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image0" transform="scale(0.00142857 0.00165017)"/>
</pattern>
<image id="image0" width="700" height="700" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAK8CAYAAAANumxDAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAIABJREFUeJzs3XeYJFW9h/F3NgJLziAoiAKiBDEh4YqySkZFBRQVs17FgOlivKjXhIiKWTFfEwaSARAEQZcgEiUpGUmSF1iWTXP/ON13anq7Z3pmTtWpOv1+nqeeha6q07+q7pn+zulTp0CSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmS1DxDqQuQgI2AZwNbAlsAmwNrAau0lhXSlSZJ6sNS4BHgPuBO4EbgGuAy4HzgpmSVSRh4lcYMYHdgP+C5wBPTliNJKtktwKnAicBpwKK05WjQGHhVpScDbwReDqybuBZJUhr3AT8Gvg5cmbgWDQgDr6rwDOBDhB5d33OSJIBh4BTg48B5iWtR5gwfKtPjgS8B+6QuRJJUaycB7wX+mboQ5Wl66gKUpdmEHt2fAlslrkWSVH9bAG8m5JJzCRfBSdHYw6vYNgeOA7ZNXYgkqZEuBl4BXJ26EOVjWuoClJWXAxdi2JUkTd5TCZ8lB6YuRPlwSINi+RhwDGE4gyRJUzELeCmhY+6stKUoBwZeTdU04GvAe1IXIknKznOAxwC/JczqIE2KgVdTMQT8AHh96kIkSdl6GrAJ4aYV0qQYeDUVXyBcVStJUpm2I9xy/vepC1EzGXg1We8HPpK6CEnSwHgmMB9vUqFJcFoyTcbzgD/gLB+SpGotBXYHzkhdiJrFwKuJWg+4BFg/dSGSpIH0b2Dr1r9SX+yh00R9D8OuJCmddYFjUxehZjHwaiIOAPZMXYQkaeDtizem0AQ4pEH9WoVwm8cNUxciSRJwK7AF8HDqQiTl438Ik36XuTwCnAC8A9gF2ABYsYqDkyRNyUzCNR47Am8HjgcWUP7nxn9XcXCSBsNqwP2U9wvrRuBtreeRJOVhVcLv9hsp7/PjfmCNio5HUuY+RDm/qB4GDif0DEiS8jSL8Lv+Ecr5LPlgdYciKVczgTuJ/wvqCmCrCo9DkpTWVsBVxP88uQ2YUeFxSMrQfsT/5XQ2Dl+QpEG0OnAO8T9XXlTlQUjKzy+J+0vpXGBOpUcgSaqTlYHzifvZ8otKj0BSVlYBFhLvF9K/gLUrPQJJUh2tR5hWLNbnywJgpUqPQFI29ibeL6NlwK6VVi9JqrO5xO3l3ava8tUk3mlNY3lexLZ+CJwVsT1JUrOdDvwkYntzI7YlaYBcRJy/uhcDj6u4dklS/W1K+IyI8VlzfsW1S8rADGARcX4JxfwLXpKUl+OI81mzEJhece2SGm5z4o2r2r3i2iVJzbEP8T5vtqy4dkkNty9xfvk8TLjLjiRJ3axAvLuw7V1x7WoIL1pTL5tEaud8wtAISZK6WQhcGKmtjSO1o8wYeNXLqpHauSpSO5KkfP09UjvrRGpHmTHwqpeVI7VzS6R2JEn5+lekdlaJ1I4yY+BVL7EC70OR2pEk5evBSO3MjtSOMmPgVS8zI7WzJFI7kqR8LYvUjtOSqSsDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrM1IXYDUcBsA/wFsBqwIDKUtR1JGhoGFwHXAn4Db05YjNZeBV5qc7YBPAnvgNyWSyrcM+D3wIeDSxLVIjeMHtTRx7wEuBPbCnyFJ1ZgG7E343XNY4lqkxvHDWpqYDwNHAdNTFyJpIM0AjgYOT12I1CQGXql/zwc+nroISQI+BeyaugipKQy8Un+mAcfgRWmS6mEIfydJfTPwSv15HrBl6iIkqWBrYJfURUhNYOCV+rNn6gIkqYu9UhcgNYGBV+rPk1MXIEld+LtJ6oOBV+rPKqkLkKQuVk1dgNQEBl6pP/NTFyBJXTyQugCpCQy8Un/+nroASerC301SHwy8Un9+l7oASerit6kLkJrAwCv15yzsSZFULxcDf0ldhNQEBl6pP8PAO4BlqQuRJMLvonekLkJqCgOv1L8z8f71kurhvcCfUxchNYWBV5qYzwFvAxalLkTSQHoUeAvwhdSFSE1i4JUm7mvAtsAvgMWJa5E0GBYDPwe2Ab6ZuBapcWakLkBqqKuBA4A1gJ2BzYA5wFDKoiRlZRhYAFwLnAPcn7YcqbkMvNLU3AecnLoISZLUm0MaJEmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWZqQuQGq4GcC2wGbAisBQ2nIkZWQYWAhcB1wCLElbjtRcBl5pctYHPgC8ClgjcS2S8ncf8EPg08CdiWuRGschDdLEvRi4GngHhl1J1VgDeCfhd8+LEtciNY6BV5qYVwG/BFZLXYikgbQ68CvgFakLkZrEwCv1b3vgWPy5kZTWNOC7wDapC5Gawg9uqX9fAWalLkKSgNnAMamLkJrCwCv151nAs1MXIUkFzwGemroIqQkMvFJ/9ktdgCR14e8mqQ8GXqk/26YuQJK62C51AVITGHil/jj9mKQ6WjN1AVITGHil/ixIXYAkdfFw6gKkJjDwSv25OnUBktSFv5ukPhh4pf6clroASeri1NQFSE1g4JX683vgptRFSFLBdcDpqYuQmsDAK/VnCfC+1EVIUsF7gKWpi5CawMAr9e8XwFdTFyFJwJeAE1MXITWFgVeamLcDR6cuQtJA+xxwWOoipCYx8EoTM0z4GvEFwEWJa5E0WP4GzAXeT/hdJKlPM1IXIDXUH1rLdsBzgc2AOcBQyqIkZWWYMAf4tcAZwGVpy5Gay8ArTc0lrUWSJNWUQxokSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtZmpC5AysAawKbAisBQ4lqUl0eBG4G7EtchSY1m4JUmZwXgjcDrgG0x6KpcVwM/AL4KPJi4FklqHIc0SBP3TOAK4BhgOwy7Kt+WwKeBa4DnJ65FkhrHwCtNzFzgLODxievQYNoA+D1wYOpCJKlJDLxS/x4P/IowVldKZTpheMNTUxciSU1h4JX6dwywauoiJGA28M3URUhSUxh4pf48Gdg7dRFSwTOAXVMXIUlNYOCV+rN/6gKkLl6SugBJagIDr9Sf7VMXIHXxtNQFSFITGHil/qyTugCpi3VTFyBJTWDglfqzKHUBUhePpi5AkprAwCv159rUBUhd+L6UpD4YeKX+nJm6AKmLM1IXIElNYOCV+nMicE/qIqSCR4Cfpi5CkprAwCv1ZwHwsdRFSAVHAXelLkKSmsDAK/XvK8BJqYuQgL8An0hdhCQ1hYFX6t8wcBBheIOUytnAPsDi1IVIUlMYeKWJeQR4MfBW4N+Ja9FgeQA4HNgNuD9xLZLUKDNSFyA10DDwdeB7hJ625wKbAXOAoYR1KS/DwELgBuBPhG8WHkpakSQ1lIFXmryFwC9biyRJqimHNEiSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsjYjdQHK3obAU1IX0QDDwBLgEeA+4MG05Ui1NB1YFZjdWmYBM4GlwKPAota/D7b+W5IAA6/K95HWoolZANwIXA1cCpwHnItBWPmaAWwOPAHYBNi09e8GwFrA2sBqwFCf7T0E3APcDdwJ3ATc0FquB64i/IEpaQAYeKV6WgnYqrXs33psMSH0ngAcB9yapjRpymYBTwd2ALYDtgGe1Ho8lpVby+N6rF8GXAtc1lrOJ/xhOT9iDZKkmvsG4Wt2l3ouS4HfA/vSf4+XlMosYDfgM8CfgYWk/xnq9XN1CfBVwh+aq5ZxMtTVocR5Db9SdeGSms3A25zlKuAVeBGq6mV94C3AiYShOKl/TiazLALOAv6LMNxC5THwSkrCwNu85RLgOd1eTKki6xBC7h8JvaWpfybK+Bn7APD4WCdM/8/AKykJA29zl+8Cqy//kkqlmAbsCfyaMM489fu/quUc4NXAilM/hcLAq5L5FaiUn9cSLsL5j9SFKGtrAx8mzHjwO+DFDNaF0DsDPwBuA44Bnpi2HEljMfBKedqY8LXye1IXouxsTvgG6GbgE/SeBWFQrA68nTCF4AmEICypZgy8Ur6mA0cBxzJYPW8qx9bArwjB7s34VX6nacALCUMdzgV2T1uOpCIDr3pZmLoARfN64JeEO1NJE7Ul8DPCDVD2x2nw+rEDcAoh/D4ncS2SMPCqt2tTF6CoXkjonZuZuhA1xjrAN4G/Awdi0J2MnQnTmp1CuLGGpEQMvOrlj6kLUHR7Ey6yMbhoLDMJY7//CbyJMDRGU7M74ULSY4A1EtciDSQDr3q5ErggdRGK7uXAx1IXodr6D+Bywtjv1RLXkpsZhIvb/gm8Jm0p0uAx8GosH0xdgErxYcIQB6ltNcLwhbOALdKWkr21gO8BfwA2TVyLNDAMvBrLGcAXUheh6IYIH7gbpy5EtbAn4RudN+FwlyrNJYyPfgeed6l0Bl6N5z2EcKS8rEG4I5sG1wrAlwk3jdgwcS2DaiXgS4TXYP3EtUhZM/BqPMPAGzD05mgu4daoGjxbAxcSbueq9PYgXNS2T+pCpFw5Gb36sYwQeiHctrZs1wOPVPA8dTOLML5vzQqf87PA8cCDFT6n0nol8C2ae+OIR4AHgEXAo8BiwmfZLMJc0ysDqySrbvLWAU4CPkMYZ78sbTlSXgy86leVofcWYC9gQcnPU1drAE8n9MC+jHIvbFkfeB/w0RKfQ/UwEzia+vfq3keYKeIKwh+/NwA3AncC99DfH8MzCX88rkO49fGmreVJwDbUdwjHEPAB4GnAKwjHK0lKYBph7OdwyctZhPFtg26IcFHRXyjvXM/HuUFztxZwNuX/3E50WUS4De9RwIup7kLKtYDnA0cQZkt4MNLxxFxuBJ5SzuHX0qHEOW9fqbpwSfky9KbxcuAuyjnXTkGXrycQ5n5NHeDayy3ANwhT461c4nFPxExgV8IQn8tJf47aywOEb3oGgYFXUi0ZetPYgHJ6e2/FO2rlaCfK+yNpIstthNkIdqQZU3BtSRjmcyXpz90i4HXlHm4tGHgl1dY0wuwNht5qzQZOIP553rfKg1Dp9iSMd00V1JYQLsLal2b/MfUs4DvAQ6QNvoeXfaCJGXgl1ZqhN41ZhLGHMc/xTys9ApXpJYQZDFIEs/uBI8nvxiarAocBN5Eu9H6y9KNMx8ArqfYMvWmsTriKPdb5fZAQpNVsBxN6V6sOY3cQblTTxCnBJmIGYTz9FaQJvV8s/xCTMPBKagRDbxo7E6aMi3V+d6u2fEV2ELCUagPY3cD7Gbyfy2mEPy7+QfWh9+gKjq9qBl5JjWHoTeP7xDu3OX9lmrsXEm7CUFXoWkQIXqtVcXA1NgN4J2H+4CpD78erOLgKGXglNYqht3qbEC/onFVp5Yrl+cBCqgtbpwJbVHJkzbEWYbq1mN+4jLe8v5Ijq4aBV1LjTCNur2Ov5UwMvW2/Is45va/qwjVl21HdjRPuJtyaWL3tDFxFdaH34GoOq3QGXkmNZOit1v7EO6ePqbh2Td5GhDmUqwhWvybcqlfjmw18mmrGUz8KPKeawyqVgVdSYxl6q7My8YY17FJx7ZqcVYBLKf/n6yHgDRUdU252JdxZruzX6F7CzTKazMArqdEMvdW5iDjn8qCqC9eEDVHOzUc6l78Dm1d0TLlaA/gN5b9W1xDmCm4qA6+kxjP0VuPHxDmPh1ZduCbsQ5T/8/QzYE5VB5S5IcKtisse4nACzbh1czcGXklZMPSW7yjinMP/qrpwTcjulBuclgEfqOxoBsu+lH+L4g9WdjRxGXglZcPQW66PEef8fbTqwtW3DQkzJZT1s7MQOLCyoxlM2wO3Ud5ruJRmXsRm4FWppqUuQANlGfA64IclP8+uwG8ZvNA7HKkdfy/U0xDwA8J8r2WYT5jP9+clta/gImAHwh3ayjAN+BFh7LCkFj/YVLVlwGsx9EoTdRgwt6S27yXcVvqcktrXaDcD/wFcXlL7GwPfLKltqZEMvErB0CtNzFOAT5XU9l2En5ULS2pf3d1JOO8XldT+y4BXl9S21DgGXqVSZej9DYZeNdc04DuEmxnEdh/wAsrradTY7iWc/ytKav8LwLoltS01ioFXKVUVep+LoVfN9U7gmSW0+yCwJ3BJCW2rf/cQhqpcW0LbawLHlNCu1DgGXqVm6JV62wT4RAntLgFeCpxfQtuauDsI083dVULbBwL7lNCu1CgGXtWBoVfq7ouUc/OHtwKnldCuJu96YD/C1HCxfRlYoYR2pcYw8KouDL3SaLsBLyyh3aOAb5fQrqbuPOCQEtrdBHh3Ce1KjWHgVZ20Q++PSn4eQ6/qbjqhdze2PwKHl9Cu4jkO+HwJ7X6AcOMSaSAZeFU3y4DXYOjVYHsjYSqymG4BDiLciUv19l+EO0bGtDLlTW0n1Z6BV3Vk6NUgWwH4SOQ2lwKvoJyLohTfUuBgwm2kY3oVsGXkNqVGMPCqrgy9GlT/Sfyvnj8D/DlymyrX7cDrI7c5DTgicptSIxh4VWeGXg2aOcQfY3shhpymOgn4VuQ2DwC2jtymVHsGXtVdlaH3ZAy9SuvNxL0z1mLgdYR5d9VM7wNujdjeEPGHzEi1Z+BVE1QVep+HoVfpzADeFbnNI/G2wU03nzDMJab9gcdHblOqNQOvmqIdev+35Ocx9CqVA4CNI7Z3HeXcpU3VOxn4dcT2pgOHRWxPqj0Dr5pkGWFSdkOvcvSeEtp7NHKbSue9xH09XwusGbE9qdYMvGoaQ69ytCOwfcT2TgdOjNie0rsBODpie3MIoVcaCAZeNZGhV7l5U8S2hgkXOik/nwbuidjeGyK2JdWagVdNZehVLlYDXhaxvV8Dl0RsT/XxIOFCxFi2BHaJ2J5UWwZeNZmhVzk4mHjvrWXARyO1pXr6CnBHxPbeGLEtqbYMvGo6Q6+a7pUR2zoBuDJie6qfBcAXI7a3P/5e0wAw8CoH7dD745Kfpx16Vyz5eTQ4HgvsELG9z0dsS/X1DcLwhhjmAHtHakuqLQOvcrEMeDXVhN7fYOhVHAcQ7nwVw3nAvEhtqd4eAI6N2N6BEduSasnAq5wYetU0B0Rs6ysR21L9fZUwI0cMewErR2pLqiUDr3Jj6FVTbAA8I1Jb9wK/jNSWmuE64MxIba0IzI3UllRLBl7lyNCrJtgjYls/wruqDaJvR2xrz4htSbVj4FWuDL2qu5gB4wcR21JznEC8i9di/gEm1Y6BVzmrcvYGQ68mYjrw/Eht/RO4OFJbapaFxLuF9GOBrSK1JdWOgVe5W4qhV/WzHbB6pLaOi9SOmunnEdvaNWJbUq0YeDUIDL2qm50itvXriG2peU4DHorU1s6R2pFqx8CrQWHoVZ3sGKmdO3E4w6BbBPwxUlsx/xCTasXAq0FSZej1jmwaS6xgcQrx5mJVc50SqZ3HAhtFakuqFQOvBk1VoXc3DL3qbn3ihYrTIrWjZjs1Ylux5oaWasXAq0HUDr0/Kfl5DL3qZpuIbf05YltqruuB2yO1tXWkdqRaMfBqUC0lzNNr6FXVYgXefwE3R2pLzfeXSO3E/INMqg0DrwaZoVcpxAoU8yK1ozwYeKUxGHg16Ay9qtqTIrVzSaR2lIdY74fNgFmR2pJqw8ArGXpVrU0jtXNZpHaUh1jvh2mE2RqkrBh4pcDQqyqsAqwVqS0Dr4ruJYzrjiHWH2VSbRh4pRGGXpVtk0jtLABuidSW8vGPSO0YeJUdA680Wjv0/rTk5zH0DqZYXxXfFKkd5eXGSO08LlI7Um0YeKXlLQVeRTWh9yQMvYNk3Ujt3BCpHeUl1vtinUjtSLVh4JW6qyr0zsXQO0hijd91OIO6iTUv89qR2pFqw8Ar9WboVWyxAu/dkdpRXu6J1E6s96lUGwZeaWyGXsUUK0jECjbKS6w/hAy8yo6BVxqfoVexzInUjoFX3cQKvLHep1JtGHil/hh6FUOsO1gtiNSO8vJIpHZmR2pHqg0Dr9Q/Q6+mKlaQWBSpHeXl0UjteGthZcfAK01MO/T+rOTnMfTmKVaQMPCqm1jvC3t4lR0DrzRxS4FXYujVxE2P1M6ySO0oL0sjtRPrfSrVhoFXmhxDryYjVlD1K2d1E6tnNlZwlmrDwCtNXpWh90QMvTlYEqkdA6+6iRV4F0dqR6oNA680NVWF3udj6M2BV9GrTLHeF7Hep1JtGHilqTP0ql8PRWpnjUjtKC+x3hcPRmpHqg0DrxSHoVf9mB+pHe+EpW7WjtTOA5HakWrDwCvF0w69Py/5eQy9zRXrDmmxgo3yEut9cV+kdqTaMPBKcS0FDsbQq+7uitTOBpHaUV5ivS9ivU8nIlYecYYJdWXgleIz9KqXOyK1s2mkdpSXWO+L2yO1MxGrRmon1t3mlBkDr1QOQ6+6uTVSO5tEakd52SRSO7HepxOxcaR2Yo2TV2YMvFJ5DL3qdHOkdtYGVovUlvKxWaR2borUzkRsHamdFMMxJEmE23T+DBgueVkYqZ0jSjkLAhgCHibO67RTxbWr3mYRbhgR4721bcW1r0QYihCj9j0rrl0NYQ+vVL6qenq9GUH9DQPXRmprm0jtKA9bATMitXVdpHb6tTvx7h5Yde1qCAOvVI126D0udSFK7ppI7Rh4VRTr/fAv4t0gpV+HRGrnEeL9QanMGHil6iwFXoGhd9BdEamdHSK1ozzEej/8PVI7/doC2DdSW5cAyyK1pcwYeKVqGXp1aaR2tgZWidSWmm/nSO1cFqmdfn2aeFnkz5HaUYYMvFL1DL2D7eJI7UzHXl4FqwFPjtTWJZHa6cd+wIsjtnd6xLaUGQOvlIahd3DdBNwdqa3nRWpHzfZc4n2e/zVSO+PZGDg2YnsPAWdHbE+ZMfBK6Rh6B9d5kdpxCiZBvPfBPVRz0dfqwG+AdSK2eTJhakapKwOvlJazNwymeZHa2RbYMFJbaq49IrXzl0jtjGVdwtCD2LOM/G/k9pQZA6+U3hIMvYPmnIhtxbrCXc20DfDYSG2VPSTgGcAFwNMit3sLcErkNiVJJZlBuDlF2Xdk805r6c0kjDmM8XqdUXHtqpdPEu9nP3YQbVsZ+Azx7gTXubynpLolSSWpQ+g9ouyDFAC/I87rtYTwNbEG07XEeR/dTfxvfdcHPgj8O1KNvep2ej6NK9ZtCCXF0R7eAHBAohpmEXpkVK6ziHOx0XTgIOCYCG2pWZ4JbBaprbOBlaaw/yxgbUI9TwPmEuYGnj710sZ0JPBgyc8hSSpJHXp6XZqzVH2zANXDt0n/3ku5XA+sMOWzKElKagbhQrbUHyouzVi8CcVgWZnQs5n6fZdy8YJN9c1ZGqT6WkKYp/cXqQtRI7wldQGq1MEM9tCj4whz70qSMmFPr0s/y6PABmgQDAHXkP49l2q5BVhjymdRA8UeXqn+7OlVP2YBb09dhCqxH7B56iISWUS4oPe+1IVIksphT6/LeMu9wKood/NI/15LtbwxwvmTJNWcoddlvOW/Uc72JP17LNXyqQjnT5LUEIZel7GW+3F8Y87+Svr3WIrlKzFOniSpWWYQxvSm/hByqedyJMrRS0n/3kqxfD7GyZMkNZOh16XX8ijwBJSTFYAbSP/eqnJZCrw7xsmTJDWbodel13IiysmHSP+eqnK5lzi33pYkZcLQ69Jr2QflYBPgIdK/n6pazgEeF+PESZLyYuh16bbcDKyCmu5U0r+Xqlj4j0+QAAAgAElEQVQeBN6J9wiQJI3B0OvSbfHq9mZ7NenfQ2UvS4HvAxvGOWWSpNwZel06l2XAC1ATPY4wzVzq91BZy2LgJ8BWsU6YJGlwzCB8iKT+MHOpz3IbsDZqkunA2aR/75Sx3AJ8AsfpSpIieDvwAOk/3FzqsZwMDKGm+Cjp3zOxlmXAZYT5oXfC96ES8E0n5W0N4GBgb2Cb1v97QUgzzCT+a/Vh4JOR21R8ewC/Jf7r/3Dk9tqGCcMTFhKmE7uTMGfwPwhB93zgvpKeW5IkNdhbid/TthTYvcqD0IRtSgiNsV/7H1Z5EJIkSf2YTugdix187gOeVOFxqH+rAZcT/zWfD2xQ4XFIkiT1bRfC+MfYAeh6YN0Kj0Pjmwn8gXLG0Hp7XkmSVGvfoZwQdAGwcoXHod6GCPPQlvE6X0KYuUWSJKm21gDuoJwwdBawYmVHol6OoZzXdwnw9AqPQ5IkadL2p5xANAz8Hphd3aGow6co77X9bIXHIUmSNGU/prxgdCqwUnWHopbPUd5regX+ISNJkhpmdeBmygtIZwOrVHY0g20I+DrlvZYLge0qOxpJkqSIdiGMyywrKF0MPKayoxlMKwC/oLzXcBh4V2VHI0mSVIKPUG5YuoVwVz7Ftw4wj3Jfv5MqOxpJkqSSTCNcaFZmaJpPuFBO8WxHmP+4zNftesKsHpIkSY23JuWHp2HCVf7TKzqmnL0aWEC5r9UCHLcrSZIyszXwIOWH3rOAjas5pOzMAY6l/NdoGHh5RcckSZJUqX2ApZQfpu4FXlbRMeXi6cA/qCbsfqyiY5IkSUriUKoJVcPAz4F1qzmsxlqBcDOJxVTzmvywmsOSJElK67NUF3rvAV5DmEtWo+0KXEN1r8WpwMwqDkySJKkOvkN1QWsYOBd4ZiVHVn+PA46j+vO/chUHJ0mSVBfTgJ9SbehaBvwI2KyC46ujNYFPA49Q7Xm/GKcfkyRJA2oG5d/Fq9uyGPg2oadzEKwGHAE8QPXn+jJg7dKPUJIkqcZmAD+j+iDWDr4/Bp5a+lGmsRFwFGmCbrtn17ArSZJEuFlE1WN6O5czCXPDzi75WMs2BOxG+CNiEenO5zxg9ZKPVZIkqXGqnL2h13I38CXg2SUfa2ybAx8BriP9OfwdsFK5hytJktRchwJLSB/ahoEbgc8Bz6V+02kNAdsDHyIMHUh9rtrLsYRhKpIkSRrDPsB80oe34jIfOB54F/AM0oS6LYE3AD8Abh+n3qqXZcDh5R26NFicOFySBsPHCV/R19XDhJ7VywkzEfwduAG4jRAAp2JtYFPgScA2reWp1PsCsHnATqmLkHLh1ySSNBiWpS5gHHOAnVtL0aPAzcCdhLu83U2YJeFRwkVkiwifZbOBWYSbMazdWtYhTJXWxBs0TDXkSyow8EqS6mw28MTWIkmTMi11AZIkSVKZDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRNxJ+Aq1MXUVN3AT9LXYSk5Rl4JUkTcRawFbAP8Ae8BS7AZcAbgMcCX01ci6QuDLySpIkaBn4LvADYEvg8oXdzkDwC/AjYBdgW+A6wMGlFknoy8EqSpuIfwHuBxwAvAn5FvsFvGWFIxxuB9YFXA39OWpGkvsxIXYAkKQuLgRNbyyrAvsD+wO7AygnrmqrFwNnA8a3ltrTlSJoMA68kKbYHgZ+0ltnAc4A9gLnAU4ChdKX15SbgdOBU4DTggbTlSJoqA68kqUyPEkLjaa3/X5sw7nVH4FnA9sCcNKUBsAi4HPgrMI/Qm3tTwnoklcDAK0mq0t2MDA+AcC3JE4BtCLM/bNH6/02BdSI+7wPADcB1wDXAVYSgeyVh2IKkjBl4JUkpLSNc+PaPLutWADYE1gPWAtYEViWMCV6B8Bk2vdXGEkJv8sPAfOA+Qrj+N2Hc7UNlHoSkejPwSpLqaiFwfWuRpElzWjJJkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSszUhdgCRJk7A+sDWwBvAwcBVwfdKKJNWWgVeS1BRDwEHAYcAzuqz/J/B14GvAoxXWJanmHNIgSWqCjYAzgJ/QPewCPBE4GrhsjG0kDSADrySp7l5CCLHP7XP7zYG/AIfj55wk/EUgSaqvOcCxwC8JY3UnYibwaUKv8EaR65LUMAZeSVIdbQ9cBLx+iu3sClwK7D/VgiQ1l4FXklQnQ8D7gHMJQxNiWBP4FfBtQq+xpAFj4JUk1cWGwGnAkcCsEtp/A/A3Qu+xpAFi4JUk1cF+hKEHc0t+ni0IvcfvI/QmSxoABl5JUkorEubOPRFYu6LnnEXoRT6N0KssKXMGXklSKtsCFwJvSfT8cwnTnb0w0fNLqoiBV5JUtSHgXcD5wFaJa1kLOAH4BrBS4loklcTAK0mq0nrA74AvALMT11L0ZkJv83apC5EUn4FXklSVvQhDCPZIXUgPTyL0Or8bL2iTsmLglSSVbQXgy8BvgXUT1zKeWcDngVOA9RPXIikSA68kqUxPBi4ADk1dyAS9gNAbvU/qQiRNnYFXklSWQwnjYrdOXcgkrQOcDHyVMH2apIYy8EqSYmsHxS8ThjM03VuBv9Lc4C4NPAOvJCmmXIcCPJkQet+JF7RJjWPglSTFMAs4mrwv9poNfJFw8d16iWuRNAEGXknSVG1JmM7rMKrv/fxXxc8HsCehF3uvBM8taRIMvJKkqXgz8Deqv2HDMuB/gE2BVwLzK37+dQk9vcdQrxtoSOrCwCtJmoy1gONJc0vem4FdgY8AS4AfEwL3uRXXAfB2wtjeJyd4bkl9MvBKkibqecClwIsSPPdxwLbAOR2P3wDsAnwcWFpxTVsTpl9r2lzDkiRJWTkCGI6w/JMQKGO0NZHlQeC1fR7rzsCNCWocBq6I1M6f+zxWSZIktRxBmgAYY7kAeMIEj3c14Kc1qN3AK9WAQxokSXW1DPgssBNw7QT3fQB4OXAIoXdY0gAz8EqS6uhWYC5wOLB4Cu38EHgqoZdY0oAy8EqS6uZ4YBvgzEjtXUfoJf4UoddY0oAx8EqS6mIBYV7f/YF7I7e9BPgQYYaJWyK3LanmDLySpDq4GHga8K2Sn+dPhGnNflHy80iqEQOvJCmlYeBoYAfg6oqe8z7gAOD1wMMVPaekhAy8kqRU7gD2AN4DLErw/N8lXNB2YYLnllQhA68kKYXfEC5MOy1xHf8EdiRMf+YFbVKmDLySpCotJNyCd1/grsS1tC0mTH82lzAdmqTMGHglSVW5HHg68NXUhfRwJqHX+fjUhUiKy8ArSarCl4FnAlekLmQc9xKmRXsTYZo0SRkw8EqSyvRvYG/gHYThDE3xbWB7wnRpkhrOwCtJKssphCECv0tdyCRdQ5gu7SjC9GmSGsrAK0mK7VHgMGAv4M7EtUzVIuB9wAuA2xPXImmSDLySpJiuBJ4FfJG8ekVPJ/RWn5S6EEkTZ+CVJMXyDcIsDJemLqQkdwMvBN4KPFLyc+X0x4IkSVIlPkwIUWUs7SA4SLYCLqG8c3pGdYciSZKUh0MoJ5idDmxY4XHUyWzgC4Q7tMU+r9+v7jAkSZLy8ATiBrL2xVxDVR5ETe0O3EHc8/uGSo9AkiQpE+cTJ4xdQ5ijViPWBX5DnPO7AFiz2vIlSZLysBdTD2PHAnOqLrxBDiVc0DaVc/zZyquWJEnKyHeYXAi7F3hJgnqb6CnA5UzuPF8CrFh9yZIkSfmYCRzPxELYWcBGCWptshWAY5jYeb6Swb0AUJIkKappwAcIY0XHCmAPAe/HOdunYk/gRsY+z8sIPe+rpilRkiQpX+sB/wX8CbiLcEvgu4A/EmZgWCddaVmZBbyS0LN+C7AQeAD4G3Ak8KR0pUmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJElSZI8DPgFcANwPLAHuBc4HjgS2SlfapB0KHNFaUhhuLVeXvE+ZhjuWT/a53/c79qvD8dSlln7qmA2cXNj2LmD7Ep97oo9LktQo04GPA4tYPtwUl6XAV4FZacqclKsZqT+FHAPvrYT3zFhWBh7CwDvZOlYETi1sdxvx/uA08EqSxjQjdQElmAb8HHhJ6/9vB74GnEcILI8B9gNe2dr2rcCmwD7AsqqLVS1sCOwB/HaMbV4GzKmmnAl5RuvfR5JWMbY5wEnA81r/fxOwG3Bdyc/bhHMjSdKk/DcjPTinA6v32O4FjO4BflMl1U2dPbxTV+zhb//3r8fZ5+wu+9TleOqg1zlZhZFzNwxcA2xc0XPH2l6SpFrZGFhI+DD7F+HDdiyfY+TD72+Fx4sfiEPAfwJXEsLOEYXtpgGHAOcA84HFwA2EHuWNxnjeLYAvAZe09lsC3AecC3wAWLXLPmMNzegWfidb26bANwi9cIsJ4yyPB57dUcdkA+804M2E872I0Ot+AfB2YGZhn9UJPXPDhPOzXo+2v1Bo/w0TrOca4NrWfy8C1u2x/WaE3v9h4DeMfw4m+vo+hZFjHQb26li/InBVYf1buxzL1T0em0YY931p6xjvJ7yeT2ltux7wXeAOwut9J3Ac8OQex9ZLtzpWA+YV1l1G79dxvHM61h96MYc0fLSw/lrCdQBF0wk/V2cQrgdY2vr3DOBVLD805luF9j7S9cjCt03tbY7psY0kSf/vY4x8cLytj+03Aj7cWg4vPF78QCx+YA0zEnhXA/7Ysa64zAf27vKchwCPjrHfMHA9y4fSiQTeyda2K/BAj32WAm/sODf9au/zD0LY6lXXX4G1Cvv9pLDu7V3aHSL8YTMMLKD7Hwpj1XM14XVv//97e2z/icI2L2TsczDZ1/ethfW3AmsU1n2tsO5XYxxLt8e+3aOG+cBcwljaXuu36HE+uumsYw3C69l+/AJgzQns36mKwPuBwrqrCENditYjDI0a67U9l9GhfpfCukt7HNuvCts8s8c2kiT9v+KH0fpTaKfdxsOtf+cBLwV2IPT2DQG/a61bBhwL7AnsCBxG6C0bJvQ2b1todwtCL9owodfvq639dgAOAi4sPPf3O2p6emu5sbDN0wtL22RrWxO4m9Ef3Ae29ns1cDmjP9gnE3jbyx8IY6x3JAwlKR7TKYX9nt9RT6dimPjJJOq5mhBO2kNbruqy7TRCb3c7iE6n9zmYyusLo/8Y+N/WY3sVHruJ0UG481g6H1vaqudTwM6EsH5pYf0y4N+EHvcdCa9JMVh+vUuNvRTrWBu4uPDYhYz/bUvqwPvewuOXsXxv/wxG/365iNAzu1Pr34sK685j5PqIIcI3K+11T+xodwVGLob8Z5djkyRpOfcTPjj+PcV2iuHsHJafxeFFhfXv6LL/psCDrfVnFh4/rLDfp7vstx6jP3S7GW8M72Rr+0hhvz+y/AWNKzM69E428J5MCJFF6wK3FLbZtfX4NODmwuObduz3lcK6PSZRT/sYij1sO3VsO7ew7n967N821dd3TUafhzcx8gfK4i619aqleL47v+l4Zsf63TvW71RYd36X5+ulvc+tLP/H0T3AY/vcP0XgfVfhsQvp3hN9SGGbeYSgWjSb0cM3Dims+5/C44d37LdfYd0RXZ5XkqTltC8ounaK7RQ/rLuFjBNa6+6l93RWxxbaaAe1NYAntJaVu+yzA1P74J9KbecXHuv1tepL+6ivm+L5fEqPbd5U2OYrhceLwwk+WHh8OiNh8DbGn1asWz3tY9ij8Nh3O7b939bjyxg5V73OQYzX9zmMvjCuvXyoz2MpPvYQo8dFQ/jjrb3+fkIPZNGcwvpuPd69dNY7TOjlLobEsWaFSRV4Dy38/6WE4UDdnFbY7lk9tnl2YZtTC49vUXj8wo59vldY19n7K0lSV7F7eOezfG8kjAStfpeDOvYfIsxB+hJCj8/XCTNKFGeNmGzgnWxt9xaOuTMEta3bR33dtPe5Z4xtNipsd07h8cczcsHY5YXHn1fY/sgJ1FKsp30MxWELDzHy9fuqhLHBw4TA02v/TlN5fSHMIV18jc6g+/uwVy3tx/4xzj7d1g/1WWOvNtvLSYSAXxzPPNbrlCLwdo61vpveF9XdxcgfCb0MMTIGvvN3UHE88yatx6YX2r1gjHYlSRplomN4VwE+01qKd9tqt9FrTN14N7ToXF7f2m+I0eNoi8t8+psBYLzAO9na2mNPxxpHOKOP+roZ73x2tn15x7qzCuu2bj32zcJjMWYUKF6d357toXiR3svG2R/ivL4QLigs7vu7CR7LeM8x1fVj7TMM/JSR3tziMI9lLD8DRb/PWUbgbS93Ff6786LAtvbPx3jfHhVn/Sh6R+E52hdH7lp47J3jtCtJ0v8r9oz1M0vDboXtby88Pt6H7z2t9bcx+sKxXkt7TOD7C21fSbgy/xmMvhBpqoF3srW1P/TH6oV9TB/1ddPe594xtin2Hs/rWHdIYd2nCGGqXe/fmLhux7ARI1/Bty+Q+wsjvXWzxtkf4ry+6xDei52hrNssFb3aSxl4b2R0b/QQ4ULE9vq76T4t3njP+c/CNv3u20/gPZ4wM8i/Co8d0OU5ptrDuy4jobn9/vpi6//HmnZPkqTlPJaRrylvofs4yqKfM/Ih9+PC4+N9+P6JkQ+qtXpsswnhK90dCBe0wOivzbvdEKOfHtTxAu9kaytOY7Zdj/1e10d93RQDRq+2Dyhs862OdXMIPaTDhCm9imNuu12Y1289ncdwcmHdiwv/3flVfK/9Y7y+xRq+wUiP/SN0vxVv3QJvt33WI8zv297mHJYfc93+uf1Xl/2nM/q2zv0+93iP38xIT/T+hcf/TZhpoqg4hneHLjVAmOmivc2pXdb/trVuGSH03zjGtpIkjal4kdMf6H0RSvEiqcXANoV1433gF7+e7DZR/CqEYNb+8Gx/qLY/1O+g+0VWB/Xx3MUbEHTOHjGV2ooX75zSpb41GT290mQD7+ksfyHVKoQe0fY2+3Rpo3ih3WWtfxexfDCZSD2dx1C8Yr44H/Hmfe4/1de3OBfv2YQew+LsGZcw8gfKWLXULfDC6OnVhgk99UXF91bnbBwHduzb73NP9PETC+t+1rHutYV181j+deicpeE1Xeosvv7fL/z3q7tsK0nSmKYTbhPb/jC5jXCF+26EnpkDWf7mB+/paGO8D++VGN3T+lPCuMsdCHNyFuc6fUthv+JcnacSepWeRQgDX2fkK89hwtf/O3Z57uLFLx8jzKhQ3G6ytc1hdOj4E2FWhh0J43yvZfQ5m8oFTecRPvx3JAxXKIbdv9L9Aq2durRzwgRq6FZP5zFMJ0yrVXyOsyaw/1Re3ycxcoHcAsJMDxD+ILmgsN/n+6iljoEXwt3n2tstY/SUaN8prLuY8MfHToSL/h5i5MLFMgPvxoxM2TdM6OVvm8non72LgIMJr+HBjH7tL6D7jBQrMvJNRXtZwPhzFEuS1NU0wnjehSwfkjo/bN7cZf9+PryfQLjKfaz2O+dincvYd+G6lJHZEnp9uH++x75TrQ3CjSi6jR9tL8V5byc7vrMYuDuXGxm5gr2bzmPafwI1dKun2zEU50wdJoSZfvef7Os7i9B7237ssI52t2Tk1sPLWs8zVi11DbyzGf36/5uRu5k9geXDYHv5OdXcaQ1GX2R3O6Pn5N2A0aG32/LX1na9fK9j+86eZEmSJuyxhGD3V8JX1EsIF2XNI0zy3msmh34/8FcE3t1q7z5CD96thA/obvP3AmwPHEcY07iYcDHMHwhDLGYTvt68lfB1fbdbka5EuNjlptY2S+g+DdtkaoPwAX8k4dgfJfR4nUvoDZ7qlFVXE3qzPg5cQfiDZAlhiMXn6T38pO2DhbbuofuQjonW02kTRnoT72X5GwyMt/9kXt/iHzHz6N7DXbw5wq2MjM9uUuCFMA653ZM9TOhBn15Y9zPCkJDFhD+APthaX1XgnU64ELK9zY861s8gjGX/A+G1XUz4+TuNMOxhrLmGYfR0esPAvuNsL0mSBswnGd3bLEmSJGVjBnAdI4F3m7E3lyRJkprhbYQL3I5jJOyekbQiSZIkKaLOi4IeYuRua5IkSVLj3Um4sO1hws0xnp62HEmSJEmSJEmSJEmSJEmSJEmSJEmDaDbwZcIdm5YSbsv6rqQVlac4g8EHxtl2rLtV5WKs274OE+7udi3wHcKtlFPVN5G7mJXlUMIdB49IW4YkSZqMT7B80DkiZUElKh7jAmCzMbY18I5elgLvTFRfHQLvILwfJEka977vTbV74b/fClxEuN997lYEvgnMTV1IDdwEvLTL46sCuwLvA1YAjgb+DPytorqe0fr3kYqeT5IkZeoqBqfnqlvP5Wt6bDsIPXr99qC+obDtN8suqqYG4f0gSVJ2xvr6unObq4Eh4D+BKwlfbx9R2G46cAjhNrb3ttbf2/r/V7XW93r+q4FphDGSlwKLgPuB44GntLZdD/guYZzxYsINFo4DnjzJY76HcBeyYeBuYJ0u244XcKZ6zN30es7O/Q4GLiGcq3uAk4Dte7Q5ln4D7+qFbS/osX/s98d49U1rtXkOMJ/wvrgB+Bqw0RjHMpPwTcY84IHWfncAJwMHtI6jWw3j/axIkqQammjg/VbHNke0tlkPOG+c9s5tbdft+a8Gvt1jv/mEIQe3jbF+i0kc89XAuwv//+Mu244VeGMcczf9BN6P93i+R5j4ndX6DbwrFba9uMf+sd8fY9W3GuFOcr3amw/s3aW9xxD+UBirlt8Shm901mDglSSpgZ7eWm5k5MO7/Vhb+/GHW//OI4z13IFwwdcMRoeZi4BXAju1/r2osO48Ro+Dbj++lNDL9ilgZ+CFhJ7e9vplhDHFbwZ2BF7C6GD49QkcczFATQcuLDy2e8e2vcJnjGOebOBdSDhXnwV2IYS6Yi2/6X3oXfUbeA8sbPuTLvuX8f7oVd8Q8DtG3hvHAnsS3huHEXpr2+eqOLPETEa/3n8EXtaq9RDgmsK6owv7jfVz4q2bJUlqiLF6Mos9WecAszrWH1JYP4/RPWMQpjybV9jmkB5tv61jv2d2rO8MozsV1p0/1sH1OJ52gHoqsKT12A2Ensy2XuclxjFPNvAOs/xMCY8h/NEwTBgmMBHj1bMu4ev/+YVt9+pRV+z3R6/6XlR4/B1dat4UeLC1/szC468p7Pcblh+6sA5wHyMBfnbHesfwSpLUYP0G3p26rD+tsP5ZPdp/dmGbU7u0/RCh961oVmH9/SwfTuYU1l/V43m76RagPld4/KjC473OS4xjnmzgXcDyQQzCLAuTCWPDE1y+O8b+sd8fxfaL5+sERsJ9r7G/xxb23bT12CmFx7bqsd9BwIdby1od6wy8kiQ1WD+Bdz7hIqFOdzESSnsZIlwcNMzo6c7abf+jx35jrR+iexgaT7d9VgKubz2+hJGLv3qdlxjHPNnA2+tcTTaM9Rt0L6X7bBZlvj+K7RfP1x30X/cwIcRCuNBxmHCR4mQYeCVJAyHXeXj7cSdhvGSn1Vv/jhUihgnBZ9XC9kXd2h1vfczQsYAwu8AphB7DbxOGVPQS45gna7xzNVnd5uEdBh4FbiEE0rGU+f7otGYf2xTN6dhvosM+JEkaKIMceJf2ePx+YO3W0ssQI9N+jdXTl9KphIuxXkHo4T1sjG3LPOZeX9GXbSHhgq7JqvL98SAhvN4O7NfH9tcX9lujtUiSpB66fWU76NpTVK1GuNq9m2cTeu+K29fRuwhz2gJ8jO5z88LUjnlR69+Vu+wzHdigr0qbo4z3x99b/65LuNDwwi7L3YQ/UP+vvfvVjSqI4gD8EyBoguMFSCp4AQhpn4C3KASBAoGBOhSKhODR/AkaAQ5RQRNQBFGBqiEhSAhNCuK06XR777a7F7pb+n3JZrN7d2bvzJrTzsw5Z1IH0JLk087zhSSXevp+l9rWspX+3x8AOIGOsoe3b8/p9eYzazl4oGr0FP7KBH0PvT5Nm5V07wNtDRnzl+b9iyPt2tRffXt4J937e5hp5nCS9kPmqq//2837Tzq+83z29mR/zd7KzJ2m3ascPAh5JbUt43e6M3+0FQlHs1EAAHNuSMB7Nsl687kPqSpgSzvPbZ7V95ksJ+0sAt6kqn+NC3iHjPlpc+1jakl+Ocm9VLaK7XR/50kNeIfMVV//C9k/3mepfMRXU7l92xzOt0batbl2X6fmfykVDH9rrnUVrWjH8SAVIC/1jBsAmDNDAt6kluHbYKDrsZ6Dy/XzGvAupqqW9QW8yfRjXsz+nLbt40WOXlp41LwGvMn0czWu/8VUxopxfT7s6G8xycaYNvrjUmMAAAErSURBVNtJVnvG8ainDQBwAgwNeJP6z9yNJG9TJ+63UsvJb1LL2l0H/uY14E2S+zk8qJlmzEnlgH2eSq+1largtZraw/s/BrzJ9HM1rv9zqfLQa6mCEVtJNlN/OHTlBN61kORuqpzx99S+6s0kL1OV/sa1e5zKaPErtdd3NI0aAABMZDfgnaS4CAAwgCwNcHzaMsQ/Z3YXAHDKnOY8vHCclpNcbl5vzOpGAADgXxg9GHZttrcDAAB/14/UIbTPSW7O+F4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADm2x9vBlQtTjoQigAAAABJRU5ErkJggg=="/>
</defs>
</svg>
								</div>
								<div class="col-md-8 d-flex align-item-center">
									<h4>Frontline Test Services Bluetooth pre-compliance and pre-qualification testing highlights problem areas that could prevent your product from passing compliance or qualification testing. This “preview” of qualification test results can provide valuable insight into the design of your product, resulting in a more cost-effective and time-efficient release.</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-gray">
					<div class="container">
						<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
							<h2>Why Perform Pre-Compliance Testing?</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6">
									<p>  Compliance testing is a risky, timeconsuming and costly way to find out that your finished product doesn’t meet the required standards of a validating body like the Bluetooth Special Interest Group.</p>
									<p>  Failing compliance tests at the end of your product development cycle means that an investigation of the failures must take place; corrections must be designed and implemented; and a new compliance test at a qualified lab must be scheduled.</p>
									<p>  Pre-compliance testing is a low-risk and cost-effective method to ensure that when you send your product off for compliance and qualification testing at the end of the development cycle, it will pass the first time.</p>
									<p>  Frontline Test Services offer precompliance, debugging, regression and robustness testing during development and post release. If you want to subject your product to more rigorous testing, you can also change specific test case parameters to test above and beyond the Bluetooth SIG specifications.</p>
								</div>
								<div class="col-md-6">
									<p>Frontline Test Services uses test equipment that is validated by the Bluetooth SIG, which ensures that the information gathered during precompliance testing is meaningful and will correspond to the information obtained during full compliance testing, including:</p>
									<p><b>PHY:</b> Bluetooth BR/EDR (RF); Bluetooth Low Energy (RFPHY); Transmitter/Receiver tests; AoA/AoD.</p>
									<p><b>Link Layer and Host Controller Interface (HCI):</b> All Bluetooth LE specifications, from 4.0 through 5.3; Iterative testing of individual test cases and full automated test runs; HCI and over-the-air capture files via X240 Protocol analyzer; Complete log of test case validations and verdicts.</p>
									<p><b>Protocol and Profile:</b> Functional requirements and specifications that reside above the HCI.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="section-heading text-center">
							<h2>Advantages of Pre-Compliance Testing</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-lg-6 mb-lg-0">
									<div class="card-line mb-4 h-auto">
										<div class="inner-content">
											<div class="card-title min-h-0">
												<h4>Lower test and design costs</h4>
											</div>
											<div class="card-text big">
												<p>Product issues left undiscovered until late in the development cycle can cost significantly more than the costs of pre-testing. Multiple test phases can ultimately result in a functional product, but for every non-compliant verdict, the process must be repeated. This adds delays to product development and increasing costs, including the costs of lost marketing efforts around a launch date that has to be postponed until the product makes its way successfully through the testing phases.</p>
											</div>
										</div>
									</div>
									<div class="card-line mb-4 h-auto">
										<div class="inner-content">
											<div class="card-title min-h-0">
												<h4>Reduced risk of failing certification by testing with validated test equipment</h4>
											</div>
											<div class="card-text big">
												<p>The “Pre-Compliance Testing” phase of development is the stage at which you subject your product to the testing that it will undergo in the actual compliance test. This phase can help you determine if a product is ready for submission to an accredited lab or not, but to emerge from these pre-tests with the highest confidence in advance of final testing, it is crucial to use test equipment that has been validated by the Bluetooth Special Interest Group. Frontline Test Services uses Teledyne LeCroy’s Frontline family of Bluetooth test tools that are recognized by the Bluetooth SIG as validated test systems.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 mb-lg-0">
									<div class="card-line mb-4 h-auto">
										<div class="inner-content">
											<div class="card-title min-h-0">
												<h4>Faster product life cycle with early problem detection</h4>
											</div>
											<div class="card-text big">
												<p>Early testing allows you to detect problems before sending a prototype out to accredited labs. By pre-testing devices, you are able to pinpoint and address potential problems before product submission. The earlier those product deficiencies are identified in the development process, the easier they are to rectify. Fixing problems after compliance testing is much more time consuming than fixing them during the design stage. Pre-compliance testing allows you to remedy and fix an issue right away, rather than compiling multiple costly test and redesign cycles. It is far better for a product to remain in the design and development stage until it meets the compliance requirements.</p>
											</div>
										</div>
									</div>
									<div class="card-line mb-4 h-auto">
										<div class="inner-content">
											<div class="card-title min-h-0">
												<h4>Improved end products</h4>
											</div>
											<div class="card-text big">
												<p>Pre-compliance testing can improve performance as well as reliability by detecting any potential problems before they happen on an industrial scale. Product performance is improved by eliminating malfunctions before production begins as well as by improving reliability during use. Pre-compliance testing ensures optimal product performance is achieved before a product is released into production, which gives you confidence that your customers will have the best possible user experience.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions front-line-list section-gray frontline-benefits">
					<div class="container">
						<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Key Features and Benefits</h2>
						</div>
						<div class="external-content">
							<div class="row justify-content">
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Minimize Testing and Certification Cost</h4>
											</div>
											<div class="content">
												<p>Identify problems in the development stage so that you can avoid using compliance testing as a way to shake out the bugs</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Shorten Development Time</h4>
											</div>
											<div class="content">
												<p>Pre-compliance testing allows you to remain in the right stage of development, avoiding costly and time-consuming backtracking</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Reduce Risks to Your Business</h4>
											</div>
											<div class="content">
												<p>Early identification of “show-stopper” failures means that you can set realistic launch goals for production and marketing</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Gain Meaningful and Accurate Results</h4>
											</div>
											<div class="content">
												<p>Performing pre-compliance testing with Bluetooth SIG-validated test equipment ensures that pre-compliance results are accurate and relevant</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Eliminate Eleventh Hour Surprises</h4>
											</div>
											<div class="content">
												<p>By performing the exact tests that are performed in qualification testing, Frontline Test Services pre-compliance testing gives you high confidence in your product’s ability to pass on the first submission</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
