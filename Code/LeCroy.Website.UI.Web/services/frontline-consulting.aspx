﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="frontline-consulting.aspx.vb" Inherits="LeCroy.Website.services_frontline_consulting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Consulting</h1>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions section-angle">
					<div class="container">          
						<div class="section-heading text-center">             
							<h2>Consulting Services</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-md-8 col-lg-8 col-xl-6">
									<p>Using our decades of knowledge and experience of Bluetooth, markets and products we’ll help you deliver products which are free of defects, minimize field issues, lower development costs and reduce time to market</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Root Cause Analysis (RCA)</h3>
											</div>
											<div class="card-text">
												<p>Using our knowledge from test tool development, interoperability device behavior and Bluetooth specifications, we determine and document faults so that you are only correcting bugs in your product</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Performance Analysis </h3>
											</div>
											<div class="card-text">
												<p>We provide deep analysis of device features and capabilities in order to provide efficiency gains, improve user experience, and avoid failure traps</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Test to Fail</h3>
											</div>
											<div class="card-text">
												<p>Using our experience of use cases and scenarios to drill down into your product’s performance, capturing as many issues as possible during the early stages of development where it is easiest to make software changes</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Competitor Benchmarking</h3>
											</div>
											<div class="card-text">
												<p>We perform comparison testing against competitor products to analyze and identify differences, improve the performance of your products and stay competitive in customer surveys; then we work with you to implement changes based on findings and help create robust and detailed supplier specifications and RFPs</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Device Characterization</h3>
											</div>
											<div class="card-text">
												<p>An in-depth analysis of a device’s functionality and performance to provide characterization helps improve next generation products, identify areas of improvement, and create an assessment of approach</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Field Issues</h3>
											</div>
											<div class="card-text">
												<p>Using our expertise and large inventory library, we reproduce issues in the field and find the fault in either your product or the interop device, then perform regression testing to make sure fixes work and don’t cause unforeseen problems</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 mb-32">
									<div class="card-line">
										<div class="inner-content">  
											<div class="card-title mb-3">                                         
												<h3>Training</h3>
											</div>
											<div class="card-text">
												<p>Customized training, from basic to advanced technology courses, is available to help internal departments improve technology knowledge, OTA log taking and identify issues more consistently and rapidly</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
