﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="frontline-interoperability.aspx.vb" Inherits="LeCroy.Website.services_frontline_interoperability" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Interoperability Testing</h1>
									<p>Unique insight and experience into Bluetooth classic and low energy specifications and profiles</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions section-angle">
					<div class="container">          
						<div class="section-heading text-center">             
							<h2>Interoperability Testing Services</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-md-8 col-lg-8 col-xl-6">
									<p>Using our decades of knowledge and experience of Bluetooth, markets and products we’ll help you deliver products which are free of defects, minimize field issues, lower development costs and reduce time to market</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">  
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Test Strategy Approach</h4>
											</div>
											<div class="card-text">
												<p>We help you create a test strategy that includes market assessments, the selection of device test lists of up to hundreds of devices, and an in-depth initial IOP designed to find as many issues as possible at the early development stage</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Test Plan Development </h4>
											</div>
											<div class="card-text">
												<p>Intelligent development of test plans and strategy based on years of experience of devices to ensure that the testing is cost effective and efficient for a given market</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Test Scenarios</h4>
											</div>
											<div class="card-text">
												<p>Test scenarios are either custom designed or selected from our library of thousands of test cases to provide assessments of device features, use case scenarios, and your test cases</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Root Cause Analysis</h4>
											</div>
											<div class="card-text">
												<p>RCA is a key area that sets Frontline Test Services apart from other test houses. We offer more than just pass/fail test results; we have Subject Matter Experts who will help you close bugs and improve your customer’s user experience</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Bug Discovery and Recording</h4>
											</div>
											<div class="card-text">
												<p>Using our experience with use case scenarios, we drill down into a product’s performance to discover issues earlier in development cycle when they are easier and more cost-effective to fix</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Pre-launch IOP Testing</h4>
											</div>
											<div class="card-text">
												<p>We perform in-depth feature and functionality testing to ensure basic interoperability, followed by prelaunch, Start-of-Production (SOP) testing, to give a high level of confidence before product launch in a particular region</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Regression Testing</h4>
											</div>
											<div class="card-text">
												<p>We test to verify that software updates and feature additions do not compromise IOP in the field</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Field Issues</h4>
											</div>
											<div class="card-text">
												<p>Using our large inventory library, we can reproduce issues in the field, find the fault and test that any applied product updates not only fix the issues, but do not cause other issues</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content small">    
											<div class="card-title mb-3">                                         
												<h4 class="height-auto">Maintenance/Website Results</h4>
											</div>
											<div class="card-text">
												<p>We perform Interoperability testing to increase market coverage of interoperability devices, test for the launch of new devices and device software or OS updates. These results can be used for a compatibility matrix on your website or that we can create for you</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
