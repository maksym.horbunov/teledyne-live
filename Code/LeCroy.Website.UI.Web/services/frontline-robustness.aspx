﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="frontline-robustness.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Robustness Testing</h1>
									<p>Ensures that your product is ready to deliver the best possible out-of-box experience to your customers</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle">
					<div class="container">
						<div class="section-content">
							<div class="row d-flex justify-content-center">
								<div class="col-md-2"><svg width="206" height="206" viewBox="0 0 206 206" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<circle cx="103" cy="103" r="98" fill="white" stroke="white" stroke-width="10"/>
<circle cx="103" cy="103" r="88" fill="white" stroke="#0076C0" stroke-width="10"/>
<mask id="mask0" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="29" y="33" width="148" height="148">
<path d="M29.0769 33.0513H176.923V180.897H29.0769V33.0513Z" fill="#C4C4C4"/>
</mask>
<g mask="url(#mask0)">
<path d="M31.4614 44.9741H175.333V169.769H31.4614V44.9741Z" fill="url(#pattern0)"/>
</g>
<defs>
<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image0" transform="scale(0.00142857 0.00165017)"/>
</pattern>
<image id="image0" width="700" height="700" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAK8CAYAAAANumxDAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAIABJREFUeJzs3XeYJFW9//H3bCLnLEFALkoQCSoIIkGUJEFQQAXFcI2IF+NV8Yr5KsbrNYAoPxMCKmLCgKgIIgqiIFnCkheBZYElbJzfH2fmTs9s90x316k6Vaffr+epB53uPv2tnqqtz5w+pw5IkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkhTBUOoCVJoh4MnA1sB6wLrAOiPb8sD0dKVJkpTMYmAhMB94ALgHmA3cBNwMLElWmUpj4M3HSsDewPOBHYDtgNWTViRJUrM8AVwF/An4A/BbYF7SihSFgbfZ1gVeBhwI7AHMSluOJElZWQr8Efj+yDYnbTnql4G3eaYBewGvB14MzExbjiRJA2EJ8HPgq8CvCGFYDWHgbY7pwFHAfwFbJq5FkqRBdj3w38B3CWOCVXMG3vqbBhwOnESYgCZJkurhJuB9wA+A4cS1aBIG3nrbCvgasFvqQiRJUkcXA28Crk5diNrz1lT1tBxwIuGrks0S1yJJkia3CWFuzSzCJDdvbVYz9vDWzxaEr0aekboQSZLUsysId1C6MXUhGjMtdQEa5yDgcgy7kiQ11Y6Ea/nhqQvRGIc01MMQ8CHCrU6WT1yLJEkqZjngCMKtyy5KXIsw8NbBdOArwNtTFyJJkqLaC9gYOA/v25uUY3jTmgl8kzDWR5Ik5emHhGv9otSFDCp7eNOZAZwNvDR1IZIkqVRbE241+iPs6U3CwJvGEPBl4OjUhUiSpEpsDWwI/DR1IYPIwJvGe4H3pC5CkiRVakfCPXr/kLqQQeMY3uodAZyVughJkpTMYYThDaqIgbdamwN/A1at6P0eBC4k3A/wRuBmYC4wH1iA635LkgbLEOHb7ZWAdQgrpG0NPAt4LrBuRXU8AuxAuC5LWZkJXEoImWVu9wCfJZy8LiwiSVJ3pgHPBD5JuJaWfb2+nJANpKx8lHJPnCuBl+PJI0lSUTOBI4G/U+61+4NV7ZBUha0I994r42S5kxB0HZ4iSVJc0wj3z72Dcq7hi4BtKtsbqURDwPmUc6J8GVi5ul2RJGkgrUK45pZxLb8QO62UgcOJf3LMAw6ucickSRIHAw8R/7p+WJU7IcU2gzADM+ZJMZswo1SSJFXvacCtxL2230DIDFIjHUX8E2KDSvdAkiRNtCFwPXGv8a6+qkYaIu7szluAjSrdA0mS1MmGxO3pvQbH8qqB9iPeSfAw8NRqy5ckSVPYirhjeveptnypuHOIdwI4QU2SpHo6lHjX+3Mqrl0qZE3Glu4tun254tolSVJvvkqca/5CYO2Ka5f69nriHPi3A6tWXLskSerNqsBdxLn2v6ni2qW+XUicg/5lVRcuSZL6cgxxrv2/rrpwqR+rAIspfsBfSVjSUJIk1d804B8Uv/4vImQJqdZi3Z3hyKoLlyRJhbyCOBngwKoLl3r1SYof6PcAs6ouXJIkFTILuJfiOeDTVReeO78yj2+PCG18kzBTU5IkNcdC4IwI7TwrQhtSaYaARyn+l90uVRcuSZKi2JXiOWA+dkqqxjak+EH+LzzIJUlqqunAXIrngU2qLjxnBqu4Yiz/ewmwNEI7kiSpekuAiyO0EyNTaISBN64tI7RxeYQ2JElSOjGu5RtHaEMjDLxxrRehjasjtCFJktK5LkIbG0ZoQyMMvHGtHKGNuyO0IUmS0pkdoY01IrShEQbeuGKsjHJPhDYkSVI690VoY7UIbWiEgTeuGD28j0RoQ5IkpfNohDaWi9CGRhh441o+QhtLIrQhSZLSWRyhjRkR2tAIA2/9DKcuQJIkFeK1vGYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1lzFo36+DixKXYQkSerbrNQFaDwDb/0ckboASZKknDikQZIkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtZmpC5Ay9gMmJ+6CEmS1LfVgJtSF6ExBt76uR8DryRJTbY4dQEazyENkiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKzNSF2ApMrNArYE1gQeAW4AHktakSRJJbKHVxoc2wNnAg8C/wAuBK4A5gE/Bw4DZiarTpIkNcIPgOGC28qVV63cTQM+Bixl6uNvDvAJ4ClJKpWkPKxO8TxwZuVVS10y8KpuhoBT6O9YvAA4Eliu8qolqdkMvMqagVd1cyzFj8n7gc8AT6u2dElqLAOvsmbgVZ2sANxL8WOydfsDcMxI25Kk9gy8NeOkNSlfhwDrRm5zd+BbwN3A/wBPj9y+JEnRGXilfO1VYturA28FrgIuBV6L305IkmrKwCvla/OK3mdn4DRCr+9XgZ0qel9Jkrpi4JXyVfX5vQrwBuBywv193wisWnENkiQtw8Ar5evOhO+9A/AV4B7gG8BzCLdIkySpcgZeKV8XpS4AWBF4NXAJYbzv8YQljSVJqoyBV8rXD4EnUhfRYlvgC4Sxvt8Gnoe9vpKkChh4pXw9CHw/dRFtLAccDVwIXAe8E1gnaUWSpKwZeKW8nZq6gCk8FTgZuAs4C9gH/12SJEXmhUXK2x8Jvah1NxM4Ajgf+CfwXmCDpBVJkrJh4JXyNkz9e3kn2hz4OHAH8CPgAGB60ookSY1m4JXy9y1gQeoi+jAdOBT4OXAr8EFg46QVSZIaycAr5W8u9Zy81ouNgZOA2cDPgEOAGQnrkSQ1iIFXGgxNG9bQyTTgQOBc4HbgY8BmSSuSJNWegVcaDBfTjMlrvdgAeB9wC/Br4KXArKQVSZJqycArDYYmTl7rxQuAswnLKX8K2DJtOZKkOjHwSoPj2zRz8lov1gHeBdwA/B54BbB8yoIkSekZeKXB8QDwg9RFVGgP4DuERS0+D2yTthxJUioGXmmwnBKpnQsYWx647tYE3gZcDVwCHAusmLIgSZKa7AeEsZJFtpUrr1qDZIgwea3ocboE2GSkzdHlge+L0G5V20PAl4HtC3yWktTJ6hT/d+rMyquWumTgVROcQJzQeNKEdmcR7pRwfqT2q9ouA14PrNL7RylJbRl4lTUDr5pgLeAJih+rd9J58YfNCffIvSfC+1S1zQe+Bjyb0BMuSf0y8CprBl41xXeIExIPmuJ9ZjK2PPCSSO9ZxXYlcBzhoiVJvTLwKmsGXjXF84gTDH/aw3tuDHyQsEJa6kDb7fYY8E3gudjrK6l7Bl5lzcCrpog5eW3jHt97OnAA8CNgcYQaqtquJYx/XqvH/ZU0eAy8ypqBV01S1uS1XrQuD5w60Ha7LQC+B+yFvb6S2jPwKmsGXjXJWoTwVvSYvYPOk9e6NQ3Yh7A88MIINVW1/RN4D7Bewf2XlBcDr7Jm4FXTfJc4we9FEWtaF3gnYXng1IG2220R4fzfFxf0kWTgVeYMvGqaFJPXujXE2PLAMW6jVtU2G/gAsGH0T0RSUxh4lTUDr5pmCLie4sdtP5PXerEmcDxheeDUgbaXz+QnhFu3FR3yIalZDLzKmoFXTfR24gS8D1ZQ6xDwHOAbwKOR6q5iuxP4MPDk+B+JpBoy8CprBl410drUZ/JaL1YD3gRcEaH2qralwC+BwwiLckjKk4FXWTPwqqnqOHmtFzsBpwCPdFFjXbY5wH8DW5TweUhKy8CrrBl41VR7ECfE/aTqwidYGXgtcCnpA20v2wXAUcBy8T8SSQkYeJU1A6+aKubktY0qrr2T7YAvAg+SPtB2u90PfBbYqoTPQ1J1DLzKmoFXTdakyWu9WAE4BriI9IG2l+0i4JUj9UtqFgOvsmbgVZPFnLw2veLau7UV8BlCT2rqQNvtNo/QU71dCZ+HpHIYeJU1A6+a7gzihLQDqy68R8sRxsxeQPpA28v2Z8IYZf+dkOrNwKusGXjVdHsSJ5ilnrzWiy0Id0uYQ/pA2+32COGuFDuV8HlIKs7Aq6wZeNV0Q8ANFD+O6zR5rVszCffH/QXhfrmpQ2232xWE+xGvFv8jkdQnA6+yZuBVDt5BnCD2X1UXHtGmhJXR7iR9oO12exQ4nbAS3VD0T0RSLwy8ypqBVzmINXntduo7ea1bM4CDCEM0lpA+1Ha7XQ28DVgz/kciqQsGXmXNwKtcDMrktV5sCJwIzCZ9oO12ewL4DmFhEXt9peoYeJU1A69ysSdxAtePK667CtOAfQnn+yLSh9putxuAdwLrxP9IJE1g4FXWDLzKxRBwI8WP5yZOXuvFesB7gH+SPtB2uy0EzgZeQAjvkuIz8CprBl7l5J3ECVhNnrzWrWnAXoShIDHGP1e13QK8D9gg/kciDTQDr7Jm4FVOnLzWn7WBE4BrSR9ou90WA+cCBzBYvyupLAZeZc3Aq9x8jziB6oCqC6+BIeC5wDeBx0kfarvdbgdOAjaJ/olIg8PAq6wZeJWbvYgTonKcvNaL1YG3AFeSPtB2uy0Ffg4cSliUQ1L3DLzKmoFXuYk5eW3DimuvoyHg2cDXgPmkD7XdbvcAHwM2j/+RSFky8CprBl7lKNbktQ9UXXjNrQK8HriM9IG2l+184AhgVvyPRMqGgVdZM/AqR+sQbmVV9NgetMlrvdge+BLwEOkDbbfbv4CTgaeW8HlITWfgVdYMvMrVmcQJSYM4ea0XKwHHAn8kfaDtZbsQeAWwfPRPRGomA6+yZuBVrmJNXju36sIbbBvg88Bc0gfabre5wBeAbUv4PKQmMfAqawZe5WqIOKuJLcbJa71aHng58DvSB9petkuAVxN6raVBY+CtGZeVlNSNYeDUCO1MB14ToZ1B8gRhBbe9CONlTwbuS1pRd54DfAO4G/gysEPaciRJsdjDq5zFmrx2G05eK2oW8FLg16Tvye1luxx4A7Bq/I9EqhV7eJU1A69y5+S1+tkM+CihJzV1oO12mw+cBuxMGC4j5cbAq6wZeJW7vYkTeJy8Ft8M4BDgZ4SFPlKH2m63q4DjgDXifyRSMgZeZc3Aq9xNw8lrTbAx8EHCvY9TB9put8eBbwG7Y6+vms/Aq6wZeDUI3kWcgHNi1YUPoOnA/sCPCH9kpA613W7XAW8H1o7/kUiVMPAqawZeDYJ1cfJaE20AvBe4mfSBttttAfA9wlAa7yqkJjHwKmsGXg2Ks4gTaPavunAxDdiH8DuM8YdLVdtNwH8C68f/SKToDLzKmoFXg+L5xAkxP6q6cI2zDvAO4AbSB9put0XAD4H98BsC1ZeBV1kz8GpQxJy89qSKa9eyhoDnAd8mLHSROtR2u90GfADYKP5HIhVi4FXWDLwaJO8mTmhx8lq9rAkcD/yD9IG2220J8FPgYMLt2aTUDLzKmoFXgyTW5LXZ+NV0HQ0BuxCWB36U9KG22+0uwljfFeJ/JFLXDLzKmoFXg8bJa4NhVeCNwF9JH2i73a4Dtizjw5C6YOBV1gy8GjROXhs8OwFfBR4mfaidapsDbFrKpyBNzsCrrBl4NWimEW4XVfS4d/Ja86wMvAa4lPTBdrLtj3gPX1XPwFsz/iMgqYilwNcitDMdeHWEdlSd+YTxvbsA2wFfBOYlrai9XYFDUhchSTmxh1eDyMlrGrUCcAzwB9L37LZuPylzp6U27OGtGXt4JRX1L+DcCO08GXhBhHaUzuOEe/k+D9ga+CzwQNKKgucR7johaUAZeCXFcGqkdl4fqR2ldx1hFbcNgaOACxLWshqwSsL3l6SsOKRBg8rJa+rGU4BPAPdS/bAGA6+q5JCGmrGHV1IMTl5TN+4nLF28fMXv+yBhkp0kKQJ7eDXI1gMWUfwcmI1/jOdmZcLqZ3Opvmd3GPhh+bsojWMPb814UZEUy73Em7z2wgjtKL0VgBOAWwhDGdZIVMdpid5XUk0YeCXFdEqkdpy81mzLAW8mjOv+LLBOwlrOB36Z8P0lKTsOadCgmwbcTPHzYDGwQcW1q7iZwGuB20gzdGHidguwfql7LLXnkIaasYdXUkwxJ6+9JkI7qsZ04GjCrchOAzZJWw4QljzeHZiTuhBJyo09vJKT1wbJNOClwLWk780d3e4A/h1X7VNa9vAqawZeKfg+ccLLvlUXrq4MAQcDfyd9wB3d7gHeSvW3PJPaMfAqawZeKXgBcUKMt5OqlyHCHyF/IX3AHd3uA94JrFjifku9MvAqawZeKXDyWn72BC4mfcAd3R4E3o8rqKmeDLw14/g4SWVw5bV87ApcAPwO2C1xLRBWTPsIsBnwMeCRtOVI0uCxh1casz5xJq/din+cp7ATcB7pe3JHt8eATwFrl7nTUiT28CprBl5pvBjnxDBOXqvS04FzSB9wR7cFwBfwfrpqFgOvsmbglcZz8lpzPI1wgV1K+pA7TPh24KvAxmXutFQSA6+yZuCVxptGWO0qRvhx8lo5Nge+CSwhfcgdHqnj/43UJTWVgbdmHBcnqUyxJq/NwMlrsW0MnALcALyS9NeD0Qv81sCxhD+UJEk1ZA+vtCwnr9XLBsAXCWNjU/fmjm7nEMYOS7mwh1dZM/BK7cWavPbCqgvPyDrAycDjpA+4o9vPCXeDkHJj4FXWDLxSey8kTkBy8lrvVgc+Srh/beqAO7r9BnhOmTstJWbgVdYMvFJ7Tl5LY29gDukD7uh2MbBXqXss1YOBt2YcDyepCjEnrx0boZ1BsB/wS2C91IUAlxPq2Z2wYpskqcHs4ZU6izV57Rb8Y30qTwLmkr5H90rgYGCo3N2Vasce3prxoiGpKnOAn0RoZzNgnwjt5OwkYI2E7389cCSwA+F3PpywFkky8Eqq1KmR2nl9pHZytBJwdKL3vplwT99tgbMJQ1kkKTkDr6QqnQ/MjtDOIYQhElrWzsAKFb/nHcC/A1sB3yasliZJtWHglVQlV14r3xYVvtc9wHHAvwGnEcZoS1LtGHglVe10YHGEdv4d/w1rZ3oF73E/8A7gKcCXCKu2SVJtebGQVLV7iDd57fkR2snNXSW2PQ94P+Gz/yxh1TZJqj0Dr6QUYk1ee0OkdnLyJ8qbLHYo8HHCqm2S1BgGXkkpnA/cFqEdJ68t6z7gvJLaPgfYvqS2Jak0Bl5JKbjyWrlOpJw7JawJXIChV1LDGHglpfIN4oQyJ68t60rCpLIyGHolNY4XCUmpxJq8tjlOXmvnC8B/ldS2oVdSoxh4JaXkymvl+giGXklSZD8grBlfZFu58qqldKYRVl4ret4sAtartvRGOZHin3Gn7QEMvdJEq1P83Dqz8qozZg+vpJRcea0aHwU+UFLboz29O5TUviSpZuzhlXr3JMLKa0XPnZvxj/iplN3Ta+iVAnt4a8aLg6TU7gZ+GqGdzYG9I7STs48SQm8Z1gR+g6FXUg0ZeCXVwSmR2nHltal9DEOvJKkAhzRI/ZmOk9eq9n4c3iCVxSENNWMPr6Q6WAKcFqEdV17r3scIobcM9vRKUsbs4ZX65+S1NN5HeT29czH0ajDZw1szXhQk1YWT19L4OOX19K6BtyyTVAMGXkl14spraXyc0NNbBkOvJGXGIQ1SMdOB2yh+Hjl5rT/vxeENUgwOaagZe3gl1ckS4q28dmyEdsq2FrAd8CzqEdA/gT29kqQp2MMrFbchcSav3UQ9/6hfATgBuJpla74OeBewUrLqgv/Enl6pCHt4lTUDrxTHucQJV8+vuvApbA/cyNR13wLslKjGUYZeqX8GXmXNwCvFsT9xgtXZVRc+iR2Ah+m+9vnA7kkqHVN26N2xul2RKmXgVdYMvFIcuU1eWxm4ld7rnw88L0G9rd6DoVfqlYG3Zuo4vk2SYq689qoI7RR1ArBpH69bCTiPtKH3k4Se3jKsQViRzdArSQ1iD68UTy6T11YH5rWpq5ftUWCPqguf4N3Y0yt1yx5eZc3AK8WVw+S1D09SV6+hd89qS1+GoVfqjoFXWTPwSnEdQJwwdVbVhY9Yi94mqhl6Db3Kg4FXWTPwSnHFmry2EFi34tohLNkbOxTWIfS+C0OvNBkDr7Jm4JXi+wBxgtS7K657HcJdFsoIhY8Be1W3K20ZeqXODLzKmoFXim9Dwl0bip5b/6TayWsnR6h5qtC7d2V7056hV2rPwKusGXilcvyYOCGqqoC4PiGQlhl4R0Nv6tXk3omhV5rIwKusGXilchxInABV1eS1z0Wqt5vtcQy9Ut0YeJU1A69UjunA7RQ/v6qYvLYh8ESEWg29hl41l4G3ZlxpTVITxFp5bSblr7z2XmC5kt9jouWBnwH7VPy+rT5NCL1lcEU2SaoRe3il8mxE/SevbQIsiFBjkZ7eF5S0b916B/b0SvbwKmsGXqlcPyFOcCpr8topkeorGnpfWNL+davM0PsgsFN1uyL1xcCrrBl4pXLFmrxWxoVkM2BRpPqKbk+QPvS+HUOvBpeBV1kz8Erlmg7cQfHzrIzJa1+PUFfs0Ltv5H3slaFXg8rAq6wZeKXyfZA4geldEWvaAlgcqS5Dr6FXzWfgVdYMvFL5Yk5eG4pU07ci1FNm6N0v0n726wQMvRosBl5lzcArVSPW5LW9ItTyNOIEcEOvoVf5MPAqawZeqRovIk5QinFBOSNSLY8R7mO7NFJ7E7cFwP4R9rcIQ68GhYFXWTPwStWYQbzJa+sUqGNb4gXUT420+aqIbbYLvQcU2N8Yyg69z6xuV6SODLzKmoFXqs4HiROSiqwO9v1INcxnfPB+JeWG3gML7HMM/4GhV3kz8CprBl6pOhsTZ+zsjfQ3ee0ZEd57dPt4m/aPwdBr6FVTGXiVNQOvVK2fEicg9TN57dxI7/0wsFaH9ziackPvi/rY75jehqFXeTLwKmsGXqlasSavfa/H990p0vsOAx+e4r2Opry7QCzE0CuVwcCrrBl4pWqlmrz2swjvOQzMI1wYp/IKyg29B/Ww72Uw9Co3Bl5lzcArVe8k4gSjbiev7Rzp/YaBD/Swny+n3NB7cA+1lOF4DL3Kh4FXWTPwStWrevLaryK81zAwF1i1x30tO/Qe0mM9sRl6lQsDr7Jm4JXSqGry2nMjvc8w8N4+9/VlGHr73eZh6FU1DLzKmoFXSuMg4gSiqSavXRDpfe6j2Lmee+h9K4ZeNZuBV1kz8EppzADupPj5t4DOk9f2jND+6FZksYtRR1Fe6F0EHBqhxiIMvWoyA6+yZuCV0jmJOGHoHW3aHgL+EKn9OcCKkfb5SGBxpLrahd4XR6qzX4ZeNZWBV1kz8ErpbEKcHs8bWHby2j4R2h3d/iPyfh9BuaH3sMj19uo4DL1qHgOvsmbgldKKdX/cPVvaHAL+FKndu4AV4u926aH38BJq7kXZofdZ1e2KBoSBV1kz8EppHUycEHRGS5v7R2pzGHhLGTs94qWUF3oXk3fofRDYqrpd0QAw8CprBl4prZiT19Ym9O5eFqG9YcKKcMuVt+tA/qH3LZQXev9OOH6kGAy8ypqBV0rvQ8QJQO8g3u3OhoE3lLnTLV5CuaH3JRXtRydlht6jK9wP5c3Aq6wZeKX0Yk5e+1uEdoaBW4FZZe70BIcTxt6WFXpfWt2utFVW6P11lTuhrBl4lTUDr1QPsSavxdpeU+7utlV26D2iul1pq4zQ+ygwrcqdULYMvMqagVeqh1iT12JsNwEzy93djg6j3NB7ZHW70tabib9fa1a6B8qVgbdm/EtWUo7OA+5OXcSIDxNCZwrnMHbLstimA98lbegt43Mt47OSpKzYwyvVx4dJ37t7PfWY+f9iyuvpXUKa0Hs0sLSPeifb7mPZRUekftjDq6wZeKX62IT4gajX7WWl72X3DqXc0HtUdbvCYZRzJwoDhmIx8CprBl6pXn5OurB7DeFr/zo5hHJDbxUB/wBgYUn7sGcF9WswGHiVNQOvVC+HkC7wpr51VyeHUF5gXAK8vMTa9waeKKn2H5ZYtwaPgVdZM/BK9TIDuIvqw+6V1HtS8MGUG3pfUULNuwLzS6r5GmCNEmrW4DLwKmsGXql+UkxeO7SSPSvmIMoNvTFXLdsJeKikWm8CNohYqwQGXmXOwCvVz5OpdvLaFTRnpn8TQu/TgQdKqvF2wvEhxWbgVdYMvFI9nUd1gfdFFe1TLC+ivqF3S2BOSbXdDWxRoDZpMgZeZc3AK9VTVZPX/kxzendbvQhYQDmfyVLgmD5q2gy4o6Sa7gO27qMmqVsGXmXNwCvV0wxCj17ZgXffqnaoBAdSbuh9ZQ+1bATcUlItDwI79FCL1A8Dr7Jm4JXq6yOUG3b/SDN7d1sdQLmh91Vd1LAeYYW6Mmp4BNil509F6p2BV1kz8Er19WTKnbz2/Op2pVRlh95jJ3nvNYGrSnrvx4A9+v1QpB4ZeJU1A69Ub2VNXvs9ze/dbbU/1Yfe1YDLSnrPBTR7uImax8CrrBl4pXo7lHIC1R5V7kRF9qe8Vc2WAq9uea+VgItLeq/FhEmLUpUMvMqagVcgHlRbAAAgAElEQVSqt5nEn7z2m0r3oFr7UW7ofQ2wPHB+ie9xVPRPRZqagVdZM/BK9Rd78tqu1ZZfuX0pN/T+raS2hxnfiyxVycCrrBl4pfrblHiT135RbenJlBl6y9reUsonIXXHwFsz01IXIEkVmw38KlJbH4zUTt39CjiYMPmrCd4NfCl1EZLqw8AraRCdEqGNnwF/idBOU/yaEHqfSF3IFD4EnJy6CEnKmUMapGaIMXltx8qrrocXAI+TfshCu+1T5HV7ODWXQxpqxh5eSYNoEXBqgdefC1wRqZamOR84iPr19H4JeA8hKEiSSmQPr9Qcq9FfL+8TwFMT1Fs3+1Cfnt7TsQNH9WIPb834D4SkQfUQcAS9T8R6M3BD/HIa5zfAi0jf03sW8DrCnTckSRWwh1dqnj2B+5j63FxAWChB4z0feIw0PbvnEsZjS3VjD6+yZuCVmmkd4HPAPJY9JxcD5wBbJ6uu/vam+tD7K2C5KnZO6oOBV1kz8ErNNgt4DvAKwipd+wNrJq2oOaoMvRcCK1azW1JfDLzKmoFX0iDbi/JD76XAKlXtkNQnA2/NOGlNkhTL74ADCHdvKMPfCb3uj5TUvqRMGXglSTH9nhB6H4vc7nXAC4EHI7craQAYeCVJsf2euKH3ZsLdIO6L1J6kAWPglSSV4ULihN47CGH3nsIVSRpYBl5JUlkuJIy57Tf03kO4+8Nt0SqSNJAMvJKkMv2BEHof7fF19xOWL74pekWSBo6BV5JUtj8A+9H9hLN7CGH32tIqkjRQDLySpCpcDGxPWLWuk2HgO8AOwJVVFCVpMMxIXYAkaWDcDhwObAEcBDwDWI3Q83sl8GNgdqriJOXLwCtJqtpNwOdSFyFpcDikQZIkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKwZeCVJkpQ1A68kSZKyZuCVJElS1gy8kiRJypqBV5IkSVkz8EqSJClrBl5JkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZM/BKkiQpawZeSZIkZc3AK0mSpKzNSF2AlvEAMJy6CEmS1Leh1AVoPANv/cxKXYAkSVJOHNIgSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNwCtJkqSsGXglSZKUNQOvJEmSsmbglSRJUtYMvJIkScqagVeSJElZm5G6AC3jYmBJ6iIkSVLfZgC7pS5CYwy89bM/MD91EZIkqW+rAw+mLkJjHNIgSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLySJEnKmoFXkiRJWTPwSpIkKWsGXkmSJGXNhSckqbiZwC7ANsDKkdpcBNxKWH1xbqQ2JVVjKHUBGs/AG9fSCG1Mj9CGpGqsALwLeCuwdknvsRj4PnAicEtJ7yEprhjX8sUR2pBK8W1guOC2QeVVS+rHZsA1FD/nu93mA4dXsmeSitqE4uf8NyqvOmOO4Y3rkQhtlNVLJCme9YHfA1tX+J4rAWcDB1X4npL6E+NaPj9CGxph4I1rXoQ2No7QhqTyDAFfJ/TgVG0a8C1gvQTvLal7T47QxgMR2tAIA29cd0do42kR2pBUnucCByR8/9WB/0z4/pKmFuNafm+ENjTCwBvXHRHa2ClCG5LKc2zqAoBjcIKrVGc7Rmjj1ghtSKXYmuKD1G/H25lIdXYD1U1Um2zbpuwdldSXIeAeip/j/1Z14VK3ZhFuI1L0IK9yIoyk3jxK+rA7DOxX9o5K6sszKH5+P4bf4kTlkIa4FgL/iNCOtx6S6ivG/bZjWJK6AEltxbiGX43neFQG3vj+EqGNV+KwBqmu6rL4Q13qkDRmGuEaXtQlEdpQCwNvfH+I0MYWwAsjtCMpvt+mLgC4DQOvVEcHEOeWZBdHaEMq1frEGZ9Xh4uqpGVtQxjWkHL87vtK30tJvRoiBNWi5/cSYI2Ka5f6chlxLmr7VF24pK58lXRh9xbCqmuS6uVA4pzjf6y6cKlf7yfOQX81MLPi2iVNbUXgUqoPuw8DO1Swf5J6sxzxbll4QsW1S33bnHgXuBMrrl1Sd1YFfkR1YfdmDLtSXX2EOOf5UmDjimuXCrmQOAf/IuDZFdcuqTtDwCGEyapljeu9hbCU8IoV7ZOk3uxOnHvwDwO/rLj2geGtr8pzFPC9SG3dQQi9cyK1Jym+NYCtCD2/MSwgLC16G+FCKKl+NiTM29kgUnuHA+dEakuqxEzgTuL18lwGrFLpHkiSpE5WA/5G3G9zXF1NjXQCcb/avBBYudI9kCRJE61CuJtCzGv8myvdAymilYB7iXtC/BlYt8qdkCRJ/2d94HLiXtvvINzpQWqsNxH3pBgGZgM7VbgPkiQpzKe5g/jX9WMr3AepFDOBa4h/ciwA3onjfSRJKtsMwt1SFhL/en4FXsuViT2If4KMbpcBu1a3K5IkDZTdiTs5rXVbCuxc3a5I5TuV8kLvMPBjPGkkSYphiNCZ9DPKvXb/b1U7JFVlVcI9Ncs8cYaBvxBmeq5XzW5JkpSNDYDjiD8prd12A2FyuyrgwhPV2hm4mDAWqGzDhHFBFxFO3GsJk93mjTwmSdKgGiIsFrMpYcGYZxKGH1a1fPcCQg/yFRW938Az8FbvrcD/JHz/xcBjjC2DKEnSoBgiTCZfkbQTxd5AGOqoihh4qzcEfA14bepCJElS5b5EGDahChl405gJ/BTYN3UhkiSpMj8HDiV8y6oKGXjTWRn4NfCc1IVIkqTSXUzo6HosdSGDaFrqAgbYfGA/4NLUhUiSpFJdBByAYTcZA29aDwMvAM5PXYgkSSrFecD+wCOpCxlkLmWX3kLgbGAjYPvEtUiSpHhOA44BnkhdyKAz8NbDEuAnhL/+no8975IkNdli4D+ADxCu8UrMSWv1sztwBqHHV5IkNcvtwJE4R6dW7Emsn4uA7YDvpi5EkiT15HTgGRh2a8ce3nrbj3CD6s1TFyJJkjr6J/AWnIReW47hrbebCEsPPgw8G1g+bTmSJKnFg4Rxuq8GbkxciyZhD29zrE4YAH88sEbiWiRJGmRzgc8DXwTmJa5FXTDwNs9KwCuBNxLG+kqSpGpcRRhq+F3g0cS1qAcG3uYaAnYEjgJeAmyatBpJkvJ0K3AO8B3gSmA4bTnqh4E3D0PAVoQ1up8L7Aasl7QiSZKa6V7gz8DvgN8A12DIbTwDb56GgHWBpwNbAJsB6wNrAasAs4CZyarTIFiNcOwVdQfwrwjtqHzrEef+4f8kTNSVyrIUWEQYe/sgcCfh35obgOuAuzHgSpK68D7CBaPo9uyqC1ffdiPO7/xdVRcuSZLUj6soHnxuwW+hmmQaYYWpor/3v1ZduCRJUq+2IU5P3yeqLlyFnUyc3/2WVRcuKW8uLSwptiMjtXNmpHZUnbMitRPrGJIkSYpuiDDxo2gP33U4nKGJhggrRBb9/V9ddeGS8mYPr6SYtifO19FnEYKPmmWYOD3z2wDbRmhHkgADr6S4jorUTqyvxlW9WENRYh1LkiRJ0QwBt1H86+y/V124orua4sfBTTisRVIk9vBKimUXYJMI7ThZrfli9NA/BdgpQjuSJEnRfJ44t6TavOrCFd2WxDkWTq66cEmSpE6mM7YcZ5Htz1UXrtL8leLHw+34TaSkCPyHRFIMuwMbRGjH4Qz5iPG73Bh4ToR2JEmSCvsqcb7C3qjqwlWaJxPnmPhi1YVLkiRNNBO4j+LB5g9VF67SXULx42IOYciMJPXNIQ2SitobWDtCOw5nyE+M3+l6wB4R2pEkSerb6RTvxVtCCDbKywbAUoofH6dWXbgkSdKo5YCHKB5ozq+6cFXmtxQ/Ph4AZlVduKR8OKRBUhH7AatGaMfhDPmKsQjFmsA+EdqRJEnq2RkU771bRAg0ytPawGKKHyffrLpwSZKklYBHKR5kflZ14arcLyh+nDwMLF914ZLy4JAGSf06EFgxQjsOZ8hfjN/xKsD+EdqRJEnq2jkU77V7nDhjgFVvqwELKH68xBgPLEmS1JVVgScoHmB+WHXhSuZcih8vjwErV124pOZzSIOkfhxCuCVZUQ5nGBwxftcrAC+K0I4kSdKUfk7x3rr5xBkDrGaINcnx3KoLlyRJg2ctwq3EigaXM6ouXMmdSfHjZgGwetWFS2o2hzRI6tVhwIwI7TicYfDEmHQ2Czg0QjuSJEkd/YbivXTziDMGWM2yPOF+ukWPn19UXbgkSRoc6wNLKB5YTq+6cNXGNyl+/CwmrOAmSV1xSIOkXryEOP9uOJxhcMX43U8HDo/QjiRJ0jIupnjv3H3AzKoLV23MBB6g+HH0u6oLlyRJ+duY4iFlGPhK1YWrdk6l+HG0FHhS1YVLaiaHNEjq1hGR2nF5WMUY1jBEGGIjSZIUzWUU75W7mzD+UoNtOjCH4sfTJVUXLkmS8rUFcYYzfL7qwlVb/0OcY2rTiuuW1EAOaZDUjSMjteNwBo2KdSzEGmojSZIG3FUU74mbTRh3KUHocLmd4sfVX6suXJIk5Wcb4nz1/MmqC1ftnUycY2vLqguX1CwOaZA0FYczqCyxFiCJdYxKkqQBNATcSPEeuBtxOIOWNQT8k+LH17V4fEmahD28kiazA/BvEdo5kxBMpFbDxOn53wrYNkI7kjJl4JU0GYczqGwOa5AkSckMAbdR/Ovmf1RduBrnaoofZzfhsAZJHdjDK6mTXYBNIrQTqwdP+YpxjDwF2ClCO5IkaYB8gTi3jNqi6sLVOP9GnGPt01UXLqkZZqQuQFJl1gC2Bp4MrAUsN8XzY4yJnAccGqEd5W8esHrBNl4OzJniOU8AcwkLoVwDPFTwPSU1gOOdpHxNA3YHDgdeADwtbTlSLV0H/Br4AfBHvJuIJEmNsDzwZuLc39TNbZC264E3MvW3H5IkKZEhwjCE20kfHNzcmrzdChyG34JK2fBklvKwFnAajpeVYjobeANhfLGkBjPwSs23HfBT4txCTNJ4twAHEZYvltRQBl6p2XYFfgGsmroQKWMPAvsBf0ldiKT+GHil5toR+D2wSuI6pEHwEPA84KrUhUjqnYFXaqYnAZcDG6QuRBogdwDPBP6VuhBJvXFpYal5pgNnYNiVqrYx8B28dkqNMz11AZJ6djzhXqGSqvcUwmpul6cuRFL3HNIgNct6hAUlHLcrpTMP2AJ4IHUhkrrj1zJSs7wHw66U2urAO1MXIal79vBKzbEacBewUupCJPEQsBEwP3UhkqZmD6/UHC/HsCvVxWrAEamLkNQdA6/UHEemLkDSOJ6TUkM4pEFqhlWBuXhnFalOFgJrAI+lLkTS5GakLkBSV3YmXtj9B3A6cDOwNFKbUhNMI9xd4TXANhHamwU8C7gwQluSJA28dwLDEbZPYy+xNAP4AnHOqeMrrl2SpGx9heIX5vNwGJM0agg4n+Ln1ReqLlxS75y0JjXDuhHa+BThAi0pnAufjNDOehHakFQyA6/UDCtGaOOKCG1IOYmxPHCMc1NSyQy8UjPEGIrgTHJpvCcitOF1VGoAT1RJkiRlzcArSZKkrBl4JUmSlDUDryRJkrJm4JUkSVLWDLxSM8S4f64rrEnjzYjQhve2lhrAwCs1w4IIbWwZoQ0pJ0+N0MbjEdqQVDIDr9QMD0Ro440R2pByEuOcuD9CG5JKZuCVmmF2hDbeBBwZoR0pB68AXhehndkR2pBUshjjlySV7+oIbQwBZwL7Al8HbgYWR2hXaorphKE9rwNeGanNf0RqR1KJYixXKql86wP3pC5C0jjDwNrA3NSFSJqcQxqkZpgDXJW6CEnj/AXDrtQIBl6pOX6QugBJ43hOSg3hkAapOTYBbsU/VKU6WEQ4J+ekLkTS1LxwSs1xO3B26iIkAfBdDLtSY9jDKzXLVoRZ4a6aJqWzCHgacEvqQiR1x4um1Cz3A2sCu6QuRBpgnwa+n7oISd2zh1dqnpWBvwNPSV2INICuB3bEJYWlRnEMr9Q88wkrpi1IXYg0YB4jnHuGXalhDLxSM/2VsFLUcOpCpAGxlLAcsffDlhrIMbxSc10D3AEcjMOTpDItBY7Fu6RIjWXglZrtb4TgezAwI3EtUo4eBY7ASWpSo9krJOVhe+AsYMvUhUgZuZYwZvfq1IVIKsYeXikPc4BvEHp5d8ZzWypiIfDfwNHA3YlrkSRJbWwOnEa4i8Owm5tb19sTwNeATZGUFYc0SPlaB3gZ8BJgV+z1ldpZAlwC/AA4g7C4i6TMGHilwbAKsBOwLaH3ai1gefw3QINlmPDNxwPAbMIy3ZcT7m0tSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkScrUk4GPAH8B5gGLgbnAn4FPAVunK61vxwEnjWwpDI9s15f8mjL1Wk/d6o/p1Yzt31LgOVM8f9+W5y8GnllqdZIkqaPpwIeBhYxdnNttS4AvAbPSlNmX6xmrPwUDb35+zdg+XkE4f9qZBdzQ8tyTK6lOkiQtYxrwA8YuyncDJwL7ALsAhwPfJITd0eecN/K6JjDwFmfgHW9TYD5j+3lch+e9p+U5NwErVFGcJEla1gcZuyj/Bli9w/NeyPge4NdXUl1xBt7iDLzLehtj+/kgsM6ExzcEHml5zl6VVidJkv7PxsAThAvyncAqUzz/ZMYu4H9t+XlrwBkC3gRcS+gVPqnledOAVwEXAQ8Di4BbgS8DG03yvk8FvgD8feR1iwkh40/Ae4FV27xmsqEZ7cJvv7VtBnwVuG3kNfcBP2JsbGfRwDsNeAPh815I6Fn8C/BWYGbLa1YHHmdsrOh6Hdr+XEv7r+ujniLPn7hfxwFXEvZrHuFz23bkuesB3wDmED7Xe4GzgW06vGevv79uj9lOpgGXtLRz+oTHv9fy2Nc6tDF9pOYLCGPll4z89wLgGNoPlZjqd9HpD7yJr3sF4XxaCDwA/ATYsUOb0N85COHfmM8Qfs/zCb+Xu4DvA8+b5P0kSYrmQ4xdCN/SxfM3Igx3OBH4z5aft15MT2V8sDxp5DmrAb+d8Fjr9jBwYJv3fBWwYJLXDQO3sGyo6SXw9lvbnsBDHV6zBPj3CZ9Nt0ZfcyMhBHaq6zJgrZbXndHy2FvbtDtE+MNmGHiMziGlUz0xA+/X6PxZ70MYWtPp8adOaLef3183x+xUtmLs2FwK7Dry8+e1tHU37b81WQ+4dJKahwlhcuIfLjEC74c7vN/jtJ9U1+85uAudz4/R7d0d9kOSpGhaL7jrF2hntI1HR/57CfASwgXvKYSgdR5jweA0YH9CQDiB0Is3TOhtfkZLu08l9AgNE3qUvjTyul2Ao4DLW977/02o6Zkj2+yW5zyzZRvVb21rAve3tP0n4MiR170S+AfjL+z9BN7R7XzCWOpdCUNJWvfply2ve8GEeibaveXxM/qoJ1bgXUL4vX4ceC5wCKEHcPTxpcC/CD3buxL2vTXIfaWlzX5/f1Mds906saWtvxEmql3V8rND27xmBuPPvSuAo4HdRv57Rctjl448f2Ld/QbeJwif/ScJx8OBE2r52YTXFTkHW3+nnwGeP/Ke72VsDPRSYLsO+yJJUhTzCBedfxVspzWcXcSyd3E4tOXx49u8fjPGxjv+ruXnJ7S87hNtXrdey+NXdahtqjG8/db2gZbX/ZbxoQRgZcaH3n4D709ZdoLgusAdLc/Zc+Tn04DbW36+2YTX/W/LY/v1UU+swDvMst8oPHvC4/tOeHy3lsf+3PLzfn9/Ux2z3ZrJ+GB3ccv//n6H17yq5TmXAMtPeHw5xg+XeFWbuvsNvMOE8cetNmRsUurcCY8VOQdHx/zf1eZ1bwWuHtmaMh9AktRQoxe5mwq203ox3a3N4+cydjHtdAun01raGA1qawBbjGwrt3nNLvQfAIrW9ueWnz27w+te0kV97bR+ntt2eM7rW57zvy0//0jLz9/X8vPpjPV23k3nfZ2snliBdz7jxx9DCJyjj88j9Ny2Wqnl8etaft7v72+qY7YXOxF6P1vbnEvncdSttzXbucNzntPynF+1/Lxo4H2MEKgnuq3D64qcg621fI7Jx8JLklSa2D28D9P+dmVzGB8GptqOmvD6IcKiF4cTxg5/hXBHida7RvQbePutbW7LPk8MZ6PW7aK+dkZf88Akz9mo5XkXtfx8c8LXxMOEHuZRe7c8/1M91NJaT6zAe+MUr2v3+FCHdvv9/U11zPbqUxPe59hJnnsfY8G+kyHGxr+2np9FA2+nz36q86Sfc/CljB2Lo9tNhMmHxxMWupEkqXS9juFdBfjvke1jLT8fbeOfHV431YIWE7fXjrxuiPHjMFu3hwnjDYsG3n5rGx3X2GmfIQxzKBJ4u237HxMe+33LY08f+dkpLT/rdKeDqerp5puA1rqum/DYVJ9FP4/3+/vr5jPuxQotbc6e4rmjx85Un+dNI89b2PKzooG319cVPQf3JIwzbzfpbSnwLWDFDjVJkhKZOE6z6X7N2FeqhxMmpEzm2YSb6UO4AL5/wuNLOrzuEcIkr3uAg7uo65aR/76LMLkGQnj6X8KdCW4i3BIJOgfZbvVb2zxg7ZHXdtLpK+1urTXJY63v+8iEx04H9hj53y8jfHaHjfz/K4BreqzjEcIfO5PVM2rtlv/9cI/v049+f3+jOh2zvXq85X8/McVzR4+dtSd5zhBj9/adrCd4ol6GqnSj6Dn4+5FtBcJk0V0IIfiFhH9PjyF8dm+IWrUkSS02Yazn5Q7aj9FrdRZjvTPfbfn5VL1HF448vpjOoWlTwsVwF8bGGI6OK5xP+1s7ddODOlUPb7+1td4Ga/sOr3tNF/W109oL1qntI1qec+qEx1YihM1hQsDbr+W57SZ2TaV1AtVUvcOHtzz36xMeK6OHt9/fXz+/l6l022brGN5dOjxn15bntI7hHT1f72zzmumMXwGul9o6nSf9noNPI4xhfwmwQ5vXbcfYPcB7CfSSJPWldZLT+YR7mrbTOklqEeNvJTTVxfT4luf8T5vHVyEEs2HCeMXRnvTRi/sc2vdcHdXFe1/X8px2M/H7re24ltf9sk19axIWPigaeH/DshO8ViEskjD6nBe1aaN1otbobbIWMnmvYidvYPwx0m7SE4TA2Rqc9pjweBmBt9/fX8rA++qW517Csp/nxLs0HNvyWOsxNfEuHEcy/tjppbZOgbffc7D1mLmMZce5TyMsJjIapiVJKtV04BzGLk53E4YqPJ/Q+3Qkyy5+8I4JbUx1MV2R8RfU7xHu/7kL4b6jrbd1emPL61rvR/orwtfyOwMHECbNLGp5fC5jN/5vdVnLcz5EGJbR+rx+a1uJ8eHjQkJv1q6EcaI3Mf4zK3If3ksJwWJXwi2qWsPuZbSfdLVbm3bO7aGGVtMZH8CuJQT+PQmf5z6Ee6ve0/Kc0yfZr5iBt9/fX8rAO5Pxx+UVhJXPdh35b+tx/xfGD6X6estjfyMM49iNMJFsPuMnifVSW6fA2+85uCHhjhCjj/0UeDHh9/Ji4Mctj53ZoSZJkqKaRlh9afQrxk7bY7Qfa9fNhX4LwgzxydqfeJ/PfZh8hacrGbtbQruLNYQb3rd7bdHaICxkcM8kr2m9720/gXc24wPbxG024ev6Tibu02GTPHcqawA/n6SW0W0pIQi1G+9eRuCF/n5/KQMvwAaMD73ttstGntdqC8aGq0zcziL+pLUi5+DLGR+I222X093YcEmSotmEEAwuI9wSaTHh1liXEJZb7XQnh24v9CsAbx9p70HCxfAuwoW6071QdyTcxujekeffR/ha/fWEr35fOdLGQsLFd6IVgc8TxiIuHNmndrdh66c2CEMXPkXY9wWESVR/IvQmdrqV1lRaX7MK4Y+Rawh/kCwmfEX/GToPPxn1vpa2HqD/xRVa7U3ovb2GELwWE8ZgXkEYUtBpzDFM/VkUebzX31/qwAvhj4LXEI7n+wg1/4swxvfVdJ4kuzWhV3TOyGtmE37X04kfeKHYObgV8GXCtwKPjrx+DmEY0GtYdriOJElSTz7G+N5mSZIkKRszgJsZC7zbTf50SZIkqRneQpjgdjZjYfeCpBVJkiRJEU2cFDSfsdXWJEmSpMa7lzCR7FHC4hjPTFuOJEmSJEmSJEmSJEmSJEmSJEmSpEG0HPBFwgpIS4DHgf9IWlF5Wu9g8N4pnjvZ6lO5mGqp4CeAm4CvE5ZSTlVfzBXR+nUcYcXBk9KWIUmS+vERlg06J6UsqESt+/gY8JRJnmvgHb8tAd6WqL46BN5BOB4kSeq4tn3T7dvyv98MXAH8K1EtVVoBOAXYJ3UhNXAb8JI2P18V2BN4F7A88FngYuCvFdX1rJH/Pl7R+0mSpExdx+D0XLXruTy2w3MHoUev2x7U17U895Syi6qpQTgeJEnKzmRfX098zvXAEPAm4FrC19sntTxvOvAqwjK2c0cenzvy/48ZebzT+18PTCOMkbwSWAjMA34EbDvy3PWAbxDGGS8iLLBwNrBNn/v8AGEVsmHgfmCdNs+dKuAU3ed2Or3nxNe9Avg74bP6/yy7Gm8AAAaESURBVO3dSYgcVRjA8f+YRHHiEjVGJF4CAcWb4pJk9CxRUEFcECFRBAUhGr1o8KAeFHFBPCiY6M0VD+KGCygojCsRQXGDRJBoXII60RgTTXv4quk3NfWqu6t7MjPO/wdFM/WWfq9SQ76pessu4EXg9EyddXoNeJckeT/MlB/2/dGtfYcUdb4LTBD3xXbgEeCkmr4sIt5kjAO/F+V2Ai8BlxX9qGpDt98VSZI0C/Ub8D5WynNHkecE4P0u9b1X5Kv6/i+BzZlyE8SQg+9r0k9u0OcvgZuTn5+syFsX8A6jz1V6CXjvynzfX/S/s1qvAe9okveTTPlh3x917Tua2EkuV98EcEFFfcuJPxTq2vIKMXyj3AYDXkmS5qAziuNbOv95t8+1tc//WXyOE2M9VxETvhYyOZjZClwFjBWfW5O095k8Drp9/l/iKdvdwDnARcST3nb6AWJM8XXAGuASJgeGj/bR5zSAWgB8nJw7r5Q3F3wOo89NA969xLW6FziXCOrStryc73qlXgPey5O8T1WUn477I9e+EeBVOvfGFmAtcW9sJJ7Wtq9VurLEIib/e78FXFq0dR3wVZL2YFKu7vfErZslSZoj6p5kpk+y3gUOLaWvS9LHmfxkDGLJs/Ekz7pM3TeUyp1VSi8Ho2NJ2gd1ncv0px1AnQb8U5zbTjzJbMtdl2H0uWnA22LqSgnLiT8aWsQwgX50a88y4vX/RJL3/Ey7hn1/5Np3cXJ+Q0WbVwC7i/S3k/Prk3IvM3XowvHAr3QC+MNK6Y7hlSRpDus14B2rSH8jST87U//qJM/rFXX/QTx9Sx2apP/G1OBkcZL+ReZ7q1QFUPcl5+9PzueuyzD63DTg3cPUQAxilYUmwVirz+OJmvLDvj/S+tPr9QKd4D439ndLUnZFce615NypmXJXALcXx3GlNANeSZLmsF4C3gliklDZz3SC0pwRYnJQi8nLnbXr/jpTri59hOpgqJuqMqPAtuL8P3Qmf+WuyzD63DTgzV2rpsFYr4Hup1SvZjGd90daf3q9dtJ7u1tEEAsx0bFFTFJswoBXkjQv/F/X4e3Fj8R4ybIlxWddENEiAp+jkvypqnq7pQ8z6NhDrC7wGvHEcDMxpCJnGH1uqtu1aqpqHd4W8DfwHRGQ1pnO+6Ps2B7ypBaXyvU77EOSpHllPge8/2bO/wYsLY6cETrLftU96ZtJrxOTsa4knvBurMk7nX3OvaKfbnuJCV1NHcz7YzcRvP4AXNhD/m1JuWOKQ5IkZVS9sp3v2ktUHU3Mdq+ymnh6l+afjW4i1rQFuJPqtXlhsD7vKz6PqCizADixp5bOHdNxf3xWfC4jJhp+XHH8QvyBupCYgAbwefG5FDglU/c7xLCW/eT//SVJ0hzUyxje3JjTq5M840ydUFWehb++j7oHTW9SZj3V40BTg/R5e3J+RalcuvRXbgxvv2N/u2lyDfspP8i1ytW/ITn/cMV3HklnTPZPdN7M3JiUe56pEyHPIoZltKhe+SPdkbC8GoUkSZrlBgl4FwEfJfm2EruArSk+03VWP6S/NWlnIuCF2P2rLuAdpM+PJ2mfEK/kx4BbidUqDlD9nXM14B3kWuXqH2Vyf58m1iNeRaztm67hfH2pXLrW7qvE9V9DBMO7krSqTSvSftxJBMhrMv2WJEmzzCABL8Rr+DQYqDo+Yurr+tka8K4kdi3LBbzQvM8rmbymbXo8S+9bC5fN1oAXml+ruvpXEitW1NV5T0V9K4FvasocADZl+vFApowkSZoDBg14IZ7MXQO8Scy430+8Tn6DeK1dNeFvtga8ALfRPahp0meINWCfIZbX2k/s4LWJGMP7fwx4ofm1qqv/cGJ76HFiw4j9wA7iD4eqNYHbRoFbiO2MfyXGVe8AniN2+qsr9xCxosU+YqxveRk1SZIkqS/tgLefzUUkSdIAXKVBOnjSbYj3zlgrJEmaZ+bzOrzSwTQGnJn8/M1MNUSSJEmaDuWJYWtntjmSJEnScP1FTEL7Arh2htsiSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZI0u/0HhwoBSJ0IEwYAAAAASUVORK5CYII="/>
</defs>
</svg>
								</div>
								<div class="col-md-8 d-flex align-item-center">
									<h4>Frontline Test Services robustness testing ensures that your product is ready to deliver the best possible out-of-box experience to your customers. Thorough robustness testing can include thousands of repeated test cases to check for overall product stability, interoperability in a crowded ecosystem, niche failures, accelerated life and performance limits.</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-gray">
					<div class="container">
						<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
							<h2>Why Perform Robustness Testing?</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6">
									<p>Your design went to plan, your communications implementation is compliant with the specification, your marketing requirements document is checked off, and you are confident that it’s time to release your product to the world. Once in the hands of the consumer, your product’s user experience will speak for itself, and your brand’s reputation hangs in the balance.</p>
									<p>Thorough robustness testing is the best way to protect your brand, inspire customer loyalty, and make sure that the buzz around your product is nothing but positive.</p>
									<p>Frontline Test Services offers hundreds of test blocks -- collections of actions that can be combined in multiple ways to create thousands of sequences to test:</p>
									<p><strong>Connection and disconnection, Bluetooth pairing and unpairing, Phone calls, Phonebook, Audio and more.</strong></p>
									<p>Your test results are logged in a secure database, where you will find essential data including pass/fail rates, a library of essential reports including a concise summary, and supporting data in the form of protocol trace captures and video captures.</p>
									<p><strong>Our robustness testing services are a key component to developing: Android and Apple phones, Car Kits, Audio equipment, Development kits, Phone apps.</strong></p>
								</div>
								<div class="col-md-6"><img src="https://assets.lcry.net/images/content-img.png" alt="img-description"></div>
							</div>
						</div>
					</div>
				</section>
				<!--section.section-gray.section-frontline-bg.bg-retina-->
				<!--    span(data-srcset="https://assets.lcry.net/images/frontline-bg-section.svg")-->
				<!--    .container-->
				<!--        .section-content-->
				<!--            .row-->
				<!--                .col-md-8-->
				<!--                    h3 Frontline Test Services robustness testing ensures that your product is ready to deliver the best possible out-of-box experience to your customers. Thorough robustness testing can include thousands of repeated test cases to check for overall product stability, interoperability in a crowded ecosystem, niche failures, accelerated life and performance limits.-->
				<section>
					<div class="container">
						<div class="section-heading text-center">
							<h2>Interoperability Testing with World-Class Device Library</h2>
						</div>
						<div class="section-content">
							<div class="card-line mb-4 h-auto">
								<div class="inner-content">
									<div class="card-text big">
										<p>In order to subject these devices to the thorough testing needed to ensure interoperability and consistent operation in a complex ecosystem, we introduce the very same kinds of devices into the test setup.</p>
										<p> Frontline Test Services houses one of the largest test device libraries in the industry, consisting of thousands of phones, Wi-Fi devices, wearables, car kits, speakers, soundbars, headsets, smart watches and various other Bluetooth® low energy devices.</p>
										<p> The latest devices are being added all the time and cover global markets (North and South America, Europe, Middle East, Asia and Australia). Because testing and support for new operating systems is critical to interoperability testing, new OS results for major iOS and Android versions are ready for you to publish within three business days of release.</p>
									</div>
								</div>
							</div>
							<div class="content-frontline-img">
								<div class="content-img"><img src="https://assets.lcry.net/images/content-phone-1.png" alt="img-description"></div>
								<div class="content-img"><img src="https://assets.lcry.net/images/content-phone-2.png" alt="img-description"></div>
								<div class="content-img"><img src="https://assets.lcry.net/images/content-phone-3.png" alt="img-description"></div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions front-line-list section-gray">
					<div class="container">
						<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Key Features and Benefits</h2>
						</div>
						<div class="external-content">
							<div class="row justify-content">
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Performance Measurement and Comparison Through Repeatable Tests</h4>
											</div>
											<div class="content">
												<p>By automating test cycles and compiling more data, Frontline Test Servoces can provide statistical analyses that are reliable and actionable</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">
													Find and Investigate Faults Even if
													They are Intermittent
												</h4>
											</div>
											<div class="content">
												<p>Thorough, multi-cycle robustness testing uncovers intermittent issues that seem inconsequential, but that become significant when transferred to a mass production environment</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-32">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">
													Determine Failure Rates Even if
													They are Low
												</h4>
											</div>
											<div class="content">
												<p>Low failure rates  can be difficult to identify when testing is performed in undemanding or limited test scenarios, but when identified by automated testing can be instrumental in producing consistent performance</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-24">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">Allow Variation of Parameters Through Multiple Values</h4>
											</div>
											<div class="content">
												<p>Variation in the numbers and types of actions between events, as well as timing between actions, allows for broad testing diversity</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 mb-24">
									<div class="card-line">
										<div class="inner-content h-auto">
											<div class="d-flex">
												<div class="icons mr-2"><svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 0C16.8466 0 12.7865 1.23163 9.33303 3.53914C5.8796 5.84665 3.18798 9.1264 1.59854 12.9636C0.00909895 16.8009 -0.406771 21.0233 0.403519 25.0969C1.21381 29.1705 3.21386 32.9123 6.15077 35.8492C9.08767 38.7861 12.8295 40.7862 16.9031 41.5965C20.9767 42.4068 25.1991 41.9909 29.0364 40.4015C32.8736 38.812 36.1534 36.1204 38.4609 32.667C40.7684 29.2135 42 25.1534 42 21C42 15.4305 39.7875 10.089 35.8493 6.15076C31.911 2.21249 26.5696 0 21 0ZM18 29.385L10.5 21.885L12.885 19.5L18 24.615L29.115 13.5L31.509 15.879L18 29.385Z" fill="url(#paint0_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="2.73809e-07" y1="21" x2="42" y2="21" gradientUnits="userSpaceOnUse">
<stop stop-color="#146194"/>
<stop offset="1" stop-color="#0076C0"/>
</linearGradient>
</defs>
</svg>
												</div>
												<h4 class="title">
													Test the Interaction and Behavior of
													Phone Apps
												</h4>
											</div>
											<div class="content">
												<p>Phone apps are at the heart of the way people interact with their personal communications devices, so knowing how reliably these programs function across multiple devices is key to a successful product</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
