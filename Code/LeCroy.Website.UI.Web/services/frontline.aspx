﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    <main id="main" role="main" class="has-header-transparented skew-style v1 reset no-container">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/banner-austinlabs-3.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Frontline Test Services</h1>
									<p>Unique insight and experience into Bluetooth classic and low energy specifications and profiles</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-training-solutions section-angle pt-40">
					<div class="container">
						<div class="section-content">
							<h4 class="mb-40"> Frontline Test Services offers consultancy and interoperability services to help in all stages of product development, from specification to market launch and beyond. Our global dedicated test labs for  Bluetooth, WI-FI, USB and Phone Pfojection technologies (Apple, CarPlay and Android Auto) help you minimize field issues, lower development costs, and speed your time to market with products that you can feel confident will deliver a superior user experience.</h4>
							<div class="slider-block">
								<div class="slider-block-content">
									<div class="slider-item">
										<div class="slider-item-img">
										    <a href="" target="_blank">
										        <img src="https://assets.lcry.net/images/fts_banner_bluetooth.png" alt="description"></a></div>
										<div class="slider-item-title">
										    <a class="link-arrow" href="" target="_blank">
										        <span>Bluetooth Pre-Compiance Testing Services</span><i class="icon-btn-right-01"> </i></a></div>
									</div>

								</div>
								<div class="slider-block-action">
									<div class="slider-block-info"><span class="current"></span> / <span class="length"> </span></div>
								</div>
							</div>
							<div class="section-content-video">
								<div class="row">
									<div class="col-lg-6 col-md-6 mb-32 mb-md-0">
										<div class="card-line">
											<div class="inner-content h-auto">
												<div class="card-title mb-3">
													<h3>Interoperability Testing</h3>
												</div>
												<div class="card-text">
													<p>With dedicated test labs for Bluetooth, Wi-Fi, USB and Phone projection technologies (CarPlay, Android Auto) across the globe, Frontline Test Services helps you meet all your interoperability (IOP) needs and requirements</p>
												</div>
											</div>
											<div class="inner-visible p-40 pt-0">  <a class="btn btn-default" href="frontline-interoperability.aspx">Explore                                                                            </a></div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 mb-32 mb-md-0">
										<div class="card-line">
											<div class="inner-content h-auto">
												<div class="card-title mb-3">
													<h3>Consultancy Services</h3>
												</div>
												<div class="card-text">
													<p>As the leading test and measurement company with unique expertise in both physical layer and protocol analysis, we help you through performance and root cause analysis, competitor benchmarking, and identifying areas of improvement so you can be confident that the product you bring to a crowded marketplace will be successful</p>
												</div>
											</div>
											<div class="inner-visible p-40 pt-0"> <a class="btn btn-default" href="frontline-consulting.aspx">Explore                            </a></div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>
		<section class="section-gray">
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-md-4">
									<div class="card-line">
										<div class="inner-visible text-center mt-0"><img src="https://assets.lcry.net/images/options-3.svg" alt="frontline services device library"></div>
										<div class="inner-content small h-auto">
											<div class="card-title">
												<h4>World-Class Device Library</h4>
											</div>
											<ul class="list-dots">
												<li>Phones: 2500+</li>
												<li>Car Kits: Covering 100’s of models</li>
												<li>Headsets/Ear buds: 100+</li>
												<li>Wi-Fi Access Points: 40+</li>
												<li>Fitness/Medical Devices: 50+</li>
												<li>HIDs: 50+</li>
												<li>100’s of laptops, watches, cameras</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="card-line">
										<div class="inner-visible text-center mt-0"><img src="https://assets.lcry.net/images/options-2.svg" alt="frontline services global testing"></div>
										<div class="inner-content small h-auto">
											<div class="card-title">
												<h4>Global Testing Centers of Excellence</h4>
											</div>
											<ul class="list-dots">
												<li>North America</li>
												<li>Europe</li>
												<li>Middle East</li>
												<li>Australia</li>
												<li>Japan</li>
												<li>South America</li>
												<li>Headquarters: Detroit, MI</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="card-line">
										<div class="inner-visible text-center mt-0"><img src="https://assets.lcry.net/images/options-1.svg" alt="frontline services facilities and systems"></div>
										<div class="inner-content small h-auto">
											<div class="card-title">
												<h4>Secure Facilities and Systems</h4>
											</div>
											<ul class="list-dots">
												<li>Access-restricted interactive project database</li>
												<li>High-Security Labs and Libraries</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="justify-content-center">
							<div class="section-heading text-center pb-lg-2 mb-3 mb-lg-4 mx-auto">
								<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Key Features and Benefits</h2>
							</div>
						</div>
						<div class="external-content">
							<div class="row mb-40">
								<div class="col-lg-4 col-md-6 col-12 mb-24"><a href="https://cdn.teledynelecroy.com/files/pdf/fts-mobile-datasheet.pdf" target="_blank">
										<div class="card-line h-240">
											<div class="inner-content">
												<h4 class="title d-flex">
													<div class="icons mr-3"><svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="2" fill="#0076C0"/>
<path d="M26.2703 8H13.7297C12.7757 8 12 8.76875 12 9.71429V30.2857C12 31.2313 12.7757 32 13.7297 32H26.2703C27.2243 32 28 31.2313 28 30.2857V9.71429C28 8.76875 27.2243 8 26.2703 8ZM20 28.4107C19.4027 28.4107 18.9189 27.9312 18.9189 27.3393C18.9189 26.7473 19.4027 26.2679 20 26.2679C20.5973 26.2679 21.0811 26.7473 21.0811 27.3393C21.0811 27.9312 20.5973 28.4107 20 28.4107Z" fill="white"/>
</svg>
													</div>Mobile
												</h4>
												<p>Test against current and beta hardware and operating systems across multiple vendors. obile vendors greatly improve UX by ensuring expected funcionality, cross platform interoperability and robustness of the products.</p>
											</div>
										</div></a></div>
								<div class="col-lg-4 col-md-6 col-12 mb-24"><a href="https://cdn.teledynelecroy.com/files/pdf/fts-auto-datasheet.pdf" target="_blank">
										<div class="card-line h-240">
											<div class="inner-content">
												<h4 class="title d-flex">
													<div class="icons mr-3"><svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="2" fill="#0076C0"/>
<path d="M29.2267 12.0137C28.96 11.227 28.2133 10.667 27.3333 10.667H12.6667C11.7867 10.667 11.0533 11.227 10.7733 12.0137L8.14667 19.5737C8.05333 19.8537 8 20.147 8 20.4537V30.0003C8 31.107 8.89333 32.0003 10 32.0003C11.1067 32.0003 12 31.107 12 30.0003V29.3337H28V30.0003C28 31.0937 28.8933 32.0003 30 32.0003C31.0933 32.0003 32 31.107 32 30.0003V20.4537C32 20.1603 31.9467 19.8537 31.8533 19.5737L29.2267 12.0137ZM12.6667 25.3337C11.56 25.3337 10.6667 24.4403 10.6667 23.3337C10.6667 22.227 11.56 21.3337 12.6667 21.3337C13.7733 21.3337 14.6667 22.227 14.6667 23.3337C14.6667 24.4403 13.7733 25.3337 12.6667 25.3337ZM27.3333 25.3337C26.2267 25.3337 25.3333 24.4403 25.3333 23.3337C25.3333 22.227 26.2267 21.3337 27.3333 21.3337C28.44 21.3337 29.3333 22.227 29.3333 23.3337C29.3333 24.4403 28.44 25.3337 27.3333 25.3337ZM10.6667 18.667L12.36 13.5737C12.5467 13.0403 13.0533 12.667 13.6267 12.667H26.3733C26.9467 12.667 27.4533 13.0403 27.64 13.5737L29.3333 18.667H10.6667Z" fill="white"/>
</svg>
													</div>Automotive
												</h4>
												<p>Car OEMs, Tier 1 and Tier 2 suppliers can test from simple scenarios with a mobile phone or more complecs eco-system style tests involving multiple connected devices and technologies.</p>
											</div>
										</div></a></div>
								<div class="col-lg-4 col-md-6 col-12 mb-24"><a href="https://cdn.teledynelecroy.com/files/pdf/fts-chipset-datasheet.pdf" target="_blank">
										<div class="card-line h-240">
											<div class="inner-content">
												<h4 class="title d-flex">
													<div class="icons mr-3"><svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="2" fill="#0076C0"/>
<path d="M25.5 14H14.5C14.2239 14 14 14.2239 14 14.5V25.5C14 25.7761 14.2239 26 14.5 26H25.5C25.7761 26 26 25.7761 26 25.5V14.5C26 14.2239 25.7761 14 25.5 14Z" fill="white"/>
<path d="M33 16C33.2652 16 33.5196 15.8946 33.7071 15.7071C33.8946 15.5196 34 15.2652 34 15C34 14.7348 33.8946 14.4804 33.7071 14.2929C33.5196 14.1054 33.2652 14 33 14H32V12C31.9988 10.9395 31.577 9.92275 30.8271 9.17285C30.0773 8.42296 29.0605 8.00116 28 8H26V7C26 6.73478 25.8946 6.48043 25.7071 6.29289C25.5196 6.10536 25.2652 6 25 6C24.7348 6 24.4804 6.10536 24.2929 6.29289C24.1054 6.48043 24 6.73478 24 7V8H21V7C21 6.73478 20.8946 6.48043 20.7071 6.29289C20.5196 6.10536 20.2652 6 20 6C19.7348 6 19.4804 6.10536 19.2929 6.29289C19.1054 6.48043 19 6.73478 19 7V8H16V7C16 6.73478 15.8946 6.48043 15.7071 6.29289C15.5196 6.10536 15.2652 6 15 6C14.7348 6 14.4804 6.10536 14.2929 6.29289C14.1054 6.48043 14 6.73478 14 7V8H12C10.9395 8.00116 9.92275 8.42296 9.17285 9.17285C8.42296 9.92275 8.00116 10.9395 8 12V14H7C6.73478 14 6.48043 14.1054 6.29289 14.2929C6.10536 14.4804 6 14.7348 6 15C6 15.2652 6.10536 15.5196 6.29289 15.7071C6.48043 15.8946 6.73478 16 7 16H8V19H7C6.73478 19 6.48043 19.1054 6.29289 19.2929C6.10536 19.4804 6 19.7348 6 20C6 20.2652 6.10536 20.5196 6.29289 20.7071C6.48043 20.8946 6.73478 21 7 21H8V24H7C6.73478 24 6.48043 24.1054 6.29289 24.2929C6.10536 24.4804 6 24.7348 6 25C6 25.2652 6.10536 25.5196 6.29289 25.7071C6.48043 25.8946 6.73478 26 7 26H8V28C8.00116 29.0605 8.42296 30.0773 9.17285 30.8271C9.92275 31.577 10.9395 31.9988 12 32H14V33C14 33.2652 14.1054 33.5196 14.2929 33.7071C14.4804 33.8946 14.7348 34 15 34C15.2652 34 15.5196 33.8946 15.7071 33.7071C15.8946 33.5196 16 33.2652 16 33V32H19V33C19 33.2652 19.1054 33.5196 19.2929 33.7071C19.4804 33.8946 19.7348 34 20 34C20.2652 34 20.5196 33.8946 20.7071 33.7071C20.8946 33.5196 21 33.2652 21 33V32H24V33C24 33.2652 24.1054 33.5196 24.2929 33.7071C24.4804 33.8946 24.7348 34 25 34C25.2652 34 25.5196 33.8946 25.7071 33.7071C25.8946 33.5196 26 33.2652 26 33V32H28C29.0605 31.9988 30.0773 31.577 30.8271 30.8271C31.577 30.0773 31.9988 29.0605 32 28V26H33C33.2652 26 33.5196 25.8946 33.7071 25.7071C33.8946 25.5196 34 25.2652 34 25C34 24.7348 33.8946 24.4804 33.7071 24.2929C33.5196 24.1054 33.2652 24 33 24H32V21H33C33.2652 21 33.5196 20.8946 33.7071 20.7071C33.8946 20.5196 34 20.2652 34 20C34 19.7348 33.8946 19.4804 33.7071 19.2929C33.5196 19.1054 33.2652 19 33 19H32V16H33ZM28 26C28 26.5304 27.7893 27.0391 27.4142 27.4142C27.0391 27.7893 26.5304 28 26 28H14C13.4696 28 12.9609 27.7893 12.5858 27.4142C12.2107 27.0391 12 26.5304 12 26V14C12 13.4696 12.2107 12.9609 12.5858 12.5858C12.9609 12.2107 13.4696 12 14 12H26C26.5304 12 27.0391 12.2107 27.4142 12.5858C27.7893 12.9609 28 13.4696 28 14V26Z" fill="white"/>
</svg>
													</div>Chipset
												</h4>
												<p>Test reference chipset prototypes or devopment boards for characterization, functionality and interoperability against a range of target market products.</p>
											</div>
										</div></a></div>
								<div class="col-lg-4 col-md-6 col-12 mb-24"><a href="https://cdn.teledynelecroy.com/files/pdf/fts-medical-datasheet.pdf" target="_blank">
										<div class="card-line h-240">
											<div class="inner-content">
												<h4 class="title d-flex">
													<div class="icons mr-3"><svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="2" fill="#0076C0"/>
<path d="M24 6.68359H16V15.9996H6.66663V23.9996H16V33.3156H24V23.9996H33.3333V15.9996H24V6.68359Z" fill="white"/>
</svg>
													</div>Medical
												</h4>
												<p>Provide consultancy and testing for medical devices ensuring they have a robustness paging, connection and reconnection strategy that operates within their battery life requirements.</p>
											</div>
										</div></a></div>
								<div class="col-lg-4 col-md-6 col-12 mb-24"><a href="https://cdn.teledynelecroy.com/files/pdf/fts-audio-datasheet.pdf" target="_blank">
										<div class="card-line h-240">
											<div class="inner-content">
												<h4 class="title d-flex">
													<div class="icons mr-3"><svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="40" height="40" rx="2" fill="#0076C0"/>
<path d="M24.9523 10.8914C26.8378 11.654 28.4523 12.9627 29.5886 14.6495C30.7249 16.3363 31.3312 18.3242 31.3295 20.3581C31.3311 22.3918 30.7248 24.3796 29.5885 26.0663C28.4522 27.753 26.8377 29.0615 24.9523 29.824V31.9535C27.3926 31.1499 29.5168 29.5961 31.0218 27.5139C32.5268 25.4316 33.3357 22.9273 33.3333 20.3581C33.3333 14.9493 29.8217 10.3581 24.9523 8.75579V10.8914V10.8914ZM24.9523 15.9192C25.594 16.4672 26.1089 17.148 26.4615 17.9145C26.814 18.6811 26.9958 19.5151 26.9943 20.3588C26.9959 21.2025 26.8141 22.0364 26.4616 22.8029C26.109 23.5693 25.594 24.25 24.9523 24.7977V26.9646C27.2304 25.6457 28.7619 23.1809 28.7619 20.3588C28.7624 19.0203 28.4106 17.7052 27.7419 16.5457C27.0732 15.3862 26.1112 14.4231 24.9523 13.7531V15.9192V15.9192ZM7.42854 26.4602H13.5238L20.3809 34.8526C21.1428 35.9497 22.9257 35.8666 23.4285 34.6225V6.15617C22.8106 4.91275 21.1893 4.81294 20.3809 5.86513L13.5238 14.2865H7.42854C5.4773 14.2865 5.14282 14.6377 5.14282 16.5447V24.1729C5.14282 26.0335 5.52377 26.4602 7.42854 26.4602Z" fill="white"/>
</svg>
													</div>Audio
												</h4>
												<p>Test headphones, headsets, wireless speackers and mobile apps for interoperability, functionality and audio quality with multiple phones and operating systems.</p>
											</div>
										</div></a></div>

							</div>
							<div class="row">
								<div class="col-md-12 m-auto">
									<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_7a7j4x0q20 videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/7a7j4x0q20/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>
								</div>
							</div>

						</div>
					</div>
				</section>
				<section class="section-training-solutions section-gray section-advantages">
					<div class="container">
						<div class="section-heading text-center">
							<h2>Interoperability and Consultancy Services</h2>
							<div class="sub-title row justify-content-center">
								<div class="col-md-8 col-lg-8 col-xl-8">
									<p>
										Teledyne LeCroy offers test and consultancy services to help in all stages of product development from Specification to Market Launch and beyond. Live results are available via a web interface. Teledyne LeCroy develops protocol analyzers and other test tools that are used by many manufacturers; this gives us a unique insight and experience into connectivity technologies such as Bluetooth® and Wi-Fi.</p>
								</div>
							</div>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 no-mobile">
									<h3>Interoperability Stages</h3>
								</div>
								<div class="col-md-6 text-right no-mobile">
									<h3>Consultancy Stages</h3>
								</div>
								<div class="col-md-12 mobile">
									<h3>Interoperability and Consultancy Stages</h3>
								</div>
							</div>
							<div class="advantages-center-top">
								<div class="card-line">
									<div class="inner-content">
										<div class="card-title mb-0">
											<h4 class="height-auto">Training</h4>
										</div>
										<div class="card-sub-title">Knowledge and capabilities</div>
										<div class="card-text">
											<p>
												Technology training for managers <br>In-depth training for engineers <br>
												Use of test tools to an advanced level
											</p>
										</div>
										<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img.png" alt="img-description"></div>
									</div>
								</div>
							</div>
							<div class="advantages">
								<div class="advantages-left">
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto">Interoperability Database</h4>
											</div>
											<div class="card-sub-title">
												                                     Live results via secure website</div>
											<div class="card-text">
												<p>
													Supporting information: observations, logs and capture files <br>
													In-built bug tracking or links to your system <br>
													Database of devices and their features
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-2.png" alt="img-description"></div>
										</div>
									</div>
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto"> <a href="frontline-compliance.aspx">  Pre-compliance Testing</a></h4>
											</div>
											<div class="card-sub-title">
												                                     Conformance with the specification</div>
											<div class="card-text">
												<p>
													Bluetooth Protocols – PTS <br>RF testing Bluetooth Classic and LE – TLF3000 <br>
													HCI BLE testing – Harmony
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-2.png" alt="img-description"></div>
										</div>
									</div>
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto">App Testing</h4>
											</div>
											<div class="card-sub-title">
												                                     Confidence across platforms</div>
											<div class="card-text">
												<p>
													Performance, functionality, UI and UX <br>Intermittent or transitory issues and unexpected behaviors <br>
													Security vulnerabilities and resilience to cyber-attacks
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-2.png" alt="img-description"></div>
										</div>
									</div>
								</div>
								<div class="advantages-center"></div>
								<div class="advantages-right">
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto">Test Strategy</h4>
											</div>
											<div class="card-sub-title">
												                                     Cost and time effectiveness</div>
											<div class="card-text">
												<p>
													Holistic approach to testing of device features <br>Confidence for a given market <br>
													Improved customer satisfaction
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-3.png" alt="img-description"></div>
										</div>
									</div>
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto">Supplier Specifications</h4>
											</div>
											<div class="card-sub-title">
												                                     Feature and behavior targets</div>
											<div class="card-text">
												<p>
													Help creating design specifications <br>RFPs to inprove suppliers’ products <br>
													Reduced unexpected behaviors that cause delays
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-5.png" alt="img-description"></div>
										</div>
									</div>
									<div class="card-line mb-40">
										<div class="inner-content">
											<div class="card-title mb-0">
												<h4 class="height-auto">Benchmarking</h4>
											</div>
											<div class="card-sub-title">
												                                     Perfomance and usability</div>
											<div class="card-text">
												<p>
													Product analysis of features, UI and UX <br>Co-existence assessment  (Wi-Fi, Bluetooth, Bluetooth Low Energy) <br>
													Reference Design Testing
												</p>
											</div>
											<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-7.png" alt="img-description"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="advantages-center-bottom">
								<div class="card-line mb-40">
									<div class="inner-content">
										<div class="card-title mb-0">
											<h4 class="height-auto">Pre-production</h4>
										</div>
										<div class="card-sub-title">
											                                     Confidence before launch</div>
										<div class="card-text">
											<p>
												Root-cause analysis (RCA) determines faults <br>Complex ecosystems (multi-device scenarios) <br>
												Extensive library of vehicles, car kits, and accessories
											</p>
										</div>
										<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-8.png" alt="img-description"></div>
									</div>
								</div>
								<div class="card-line mb-40">
									<div class="inner-content">
										<div class="card-title mb-0">
											<h4 class="height-auto">Product Launch</h4>
										</div>
										<div class="card-sub-title">
											                                     Market readiness</div>
										<div class="card-text">
											<p>
												In-depth assessment to ensure interoperability <br>Balanced approach to confidence versus time <br>
												Validation and correction of field issues
											</p>
										</div>
										<div class="advantages-icon"><img src="https://assets.lcry.net/images/frontline-img-9.png" alt="img-description"></div>
									</div>
								</div>
								<div class="card-line">
									<div class="inner-content">
										<div class="card-title mb-0">
											<h4 class="height-auto">Post-production</h4>
										</div>
										<div class="card-sub-title">
											                                     Issues and feature additions</div>
										<div class="card-text">
											<p>
												Ensured interoperability against new or updated devices <br>Increased market coverage   <br>
												Updates and new features do not compromise your product
											</p>
										</div>
										<div class="advantages-icon">       <img src="https://assets.lcry.net/images/frontline-img-10.png" alt="img-description"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

		<section class="section-explore ">
					<div class="container small">
						<div class="justify-content-center section-heading text-center">
							<h2 class="mb-0 px-0 mx-sm-5 mx-md-0">Got a question?</h2>
							<div class="sub-title">Leave us your email. We're here to help and answer any question you might have.</div>
						</div>
						<div class="explore-content">
							<iframe src="https://go.teledynelecroy.com/l/48392/2021-08-16/88xln1" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
						</div>
					</div>
				</section>
			</main>
</asp:Content>
