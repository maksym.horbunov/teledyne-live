﻿<%@ Page Title="Teledyne LeCroy - Motor Drive Analyzers" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.static_dynamic_complete_default" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
    			<main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner-slide section-white section-angle-before angle-small small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/oscilloscopes/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>Static. Dynamic. Complete.</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-content">
									<div class="page-text">
										<p>3-phase electrical and mechanical power and control analysis</p>
										<ul class="list-dots mb-3 green">
											<li>
												 Up to 16 Channels, 12 bits, 2 GHz High Definition Oscilloscope</li>
											<li>3-phase Power Analysis</li>
											<li>Motor Mechanical Analysis</li>
										</ul>
									</div>
									<div class="page-button"><a class="btn btn-default btn-green-outline" href="/oscilloscope/mda-8000hd-motor-drive-analyzers">Motor Drive Analyzer</a><a class="btn btn-default btn-green-outline" href="/options/productseries.aspx?mseries=589&amp;groupid=22">3-phase software</a><a class="btn btn-default btn-green-outline" href="/oscilloscope/configure/configure_step2.aspx?seriesid=606">Configure           </a><a class="btn btn-default btn-green-outline" href="https://go.teledynelecroy.com/l/48392/2018-02-13/6916lr">Technical Primer            </a><a class="btn btn-default btn-green-outline" href="#videos">Videos </a><a class="btn btn-default btn-green-outline" href="#webinars">Webinars </a></div>
									<div class="page-video"><iframe src="https://fast.wistia.net/embed/iframe/zls1eowbpw" title="MDA 8000HD Intro and Overview Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen msallowfullscreen width="325" height="185"></iframe>
									</div>
								</div>
							</div>
							<div class="col-md-6"><a href="/oscilloscope/mda-8000hd-motor-drive-analyzers/"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-1.png" alt="mda-8000hd-motor-drive-analyzer-images"></a></div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xl-4"> 
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery30" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-2.png"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-2.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>STATIC</h3>
										</div>
										<div class="card-content">        
											<p>The mean-value Numerics table displays voltage, current, power, speed, torque, etc. values for short (or long) time periods during constant load, speed and torque operating conditions - just like what a dedicated power analyzer instrument provides.</p>
										</div>
									</div>
									<div class="inner-visible text-center mb-40"><a href="http://cdn.teledynelecroy.com/files/pdf/mda8000hd-datasheet.pdf#page=8" target="_blank"><span class="btn btn-default btn-green">Explore</span></a></div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery31" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-3.png"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-3.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>DYNAMIC</h3>
										</div>
										<div class="card-content">        
											<p>Capture thousands of power cycles over long time periods. Per-cycle Waveform views help you understand dynamic behaviors. Use Zoom+Gate to isolate and correlate power behaviors to control system operation during dynamic load, speed, and torque operating conditions.            </p>
										</div>
									</div>
									<div class="inner-visible text-center mb-40"><a href="http://cdn.teledynelecroy.com/files/pdf/mda8000hd-datasheet.pdf#page=9" target="_blank"><span class="btn btn-default btn-green">Explore</span></a></div>
								</div>
							</div>
							<div class="col-md-6 col-xl-4">
								<div class="card-line">
									<div class="inner-img jumbotron bg-retina big pt-0"><a data-fancybox="gallery32" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-4.png"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-4.png"></a></div>
									<div class="inner-content">  
										<div class="card-title">                                      
											<h3>COMPLETE</h3>
										</div>
										<div class="card-content">        
											<p>Acquire and display analog, digital and serial data signals from both power and embedded control systems. Correlate power system behaviors to control system activity during time periods as short as a single device switching cycle.   </p>
										</div>
									</div>
									<div class="inner-visible text-center mb-40"> <a href="http://cdn.teledynelecroy.com/files/pdf/mda8000hd-datasheet.pdf#page=11" target="_blank"><span class="btn btn-default btn-green">Explore</span></a></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="poster">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xl-6">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery10" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-5.jpg"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-5.jpg" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-bottom-open"><span class="btn btn-default open-form btn-green">Request this poster</span>
											<iframe src="https://go.teledynelecroy.com/l/48392/2017-09-12/5n7mcs" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-6">
								<div class="card-line">
									<div class="inner-visible text-center"><a data-fancybox="gallery10" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-6.jpg"><img src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-6.jpg" alt="img-description"></a></div>
									<div class="inner-content">  
										<div class="card-bottom-open"><span class="btn btn-default open-form btn-green">Request this poster</span><iframe src="https://go.teledynelecroy.com/l/48392/2015-11-10/2mx9h8" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page" id="videos"> 
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_580ef2wpu5 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-580ef2wpu5-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_52.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_206.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_206.thumbnail_img" alt="mda8000hd-static" src="https://embedwistia-a.akamaihd.net/deliveries/6ef065da0db4d3fe1e0572881e5f5ef7.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_222.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_222.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Static Power Analysis</h3>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_bj4vy7syc0 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-bj4vy7syc0-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_72.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_257.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_257.thumbnail_img" alt="mda8000hd-complete" src="https://embed-fastly.wistia.com/deliveries/2560e5b44906917da04f384ba45ed2bf.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_272.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_272.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Complete Test Coverage</h3>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_qhv1pana48 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-qhv1pana48-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_95.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_360.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_360.thumbnail_img" alt="mda8000hd-vector-display" src="https://embedwistia-a.akamaihd.net/deliveries/9467cc25edf0d195b8aef866b655236f.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_375.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_375.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Vector Display Option</h3>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_y31fi8rhr2 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-y31fi8rhr2-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_62.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_232.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_232.thumbnail_img" alt="mda8000hd-dynamic" src="https://embedwistia-a.akamaihd.net/deliveries/de9094b1ce7ee29de87432ed7e4cabd6.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_247.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_247.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Dynamic Power Analysis</h3>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_rergkanull wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-rergkanull-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_82.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_282.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_282.thumbnail_img" alt="mda8000hd-mechanical" src="https://embedwistia-a.akamaihd.net/deliveries/728cc32e833e08fdfa2fbac97fb053f7.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_297.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_297.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Comprehensive Mechanical Interface</h3>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_45v5ligu2y wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-45v5ligu2y-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_108.thumb_container" style="position: relative; height: 191.344px; width: 340.188px;">
														<div id="wistia_385.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 191.344px; overflow: hidden; outline: none; position: relative; width: 340.188px;"><img id="wistia_385.thumbnail_img" alt="mda8000hd-harmonics" src="https://embedwistia-a.akamaihd.net/deliveries/16660e7dc3d19d2e29c830f343e9b335.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 191.344px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 340.188px; left: 0px; top: 0px;">
															<div id="wistia_400.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 140px; top: 77px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_400.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 77px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
									<h3>Harmonics Calculation Option</h3>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="Overview">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Overview/Compare</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<div class="col-md-6"><a data-fancybox="gallery9" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-7.png"> <img class="mb-4" src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-7.png" alt="img-description"></a></div>
								<div class="col-md-6"><a data-fancybox="gallery9" href="https://assets.lcry.net/images/oscilloscopes/static-dynamic-8.png"><img class="mb-4" src="https://assets.lcry.net/images/oscilloscopes/static-dynamic-8.png" alt="img-description"></a>
									<div class="text-center">
										<p>Use OscilloSYNC™ to quickly connect two MDAs or WaveRunner 8000HDs and create one 16-channel acquisition and analysis system.     </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-event-solutions pt-40" id="webinars">
					<div class="container">
						<div class="section-heading text-center">
							<h2>On-Demand Webinars</h2>
						</div>
						<div class="section-content">
							<div class="block-view view-list">
								<div class="tabs-content">
									<div class="row mb-24">
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2020-06-10/7yq94k">Assessing Power Conversion System Performance – Using Power Analyzers and Oscilloscopes ( Part 1 )</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2020-06-10/7yq94k">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2020-06-17/7yw4z4">Assessing Power Conversion System Performance – Using Power Analyzers and Oscilloscopes ( Part 2 )</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2020-06-17/7yw4z4">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2020-05-20/7ygr6v">Power Conversion Fundamentals – from Semiconductor Devices to Complex Drives</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2020-05-20/7ygr6v">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-10-24/7sxnsd">Probing in Power Electronics – What to Use and Why ( Part 1 )</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-10-24/7sxnsd">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-10-24/7sxnsd">Probing in Power Electronics – What to Use and Why ( Part 2 )</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-10-24/7sxnsd">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-03-06/7hccsm">Essential Principles of Power: Voltage, Current and Power – from AC Line to PWM</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-03-06/7hccsm">Watch Now </a></div>
											</div>
										</div>
										<div class="mb-3 col-lg-12">
											<div class="slide-event">
												<div class="top-card">
													<h6 class="title-slide"><a href="https://go.teledynelecroy.com/l/48392/2019-08-28/7rww3t">Measuring Motor Performance: Using Power Analyzers and Oscilloscopes</a></h6>
												</div>
												<div class="bottom-card"><a class="btn btn-default" href="https://go.teledynelecroy.com/l/48392/2019-08-28/7rww3t">Watch Now </a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray technical-docs" id="briefs">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Technical Docs</h2>
						</div>
						<div class="section-content">
							<div class="row"> 
								<div class="col-md-6"></div>
							</div>
							<ul class="collection technical-docs-ul">
								<div class="row">
									<div class="col-md-6">
										<li><a href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x">
												<h5>Comparing High Resolution Oscilloscope Design Approaches</h5>
												<div class="description" title="This white paper provides an overview of the various high resolution design approaches, with examples of their impact on oscilloscope performance.">This white paper provides an overview of the various high resolution design approaches, with examples of their impact on oscilloscope performance.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10254">
												<h5>Dynamic Rapid Motor Acceleration Loss Measurements Using the Teledyne LeCroy Motor Drive Analyzer</h5>
												<div class="description" title="The Teledyne LeCroy Motor Drive Analyzer is ideal for measuring dynamic (transient) losses to enable better understanding of motor and drive efficiencies during real-world operation. Additionally, the Motor Drive Analyzer derives the core and copper loss components using known motor constants and compares them to engineering models and the drive-control feedback circuit.">The Teledyne LeCroy Motor Drive Analyzer is ideal for measuring dynamic (transient) losses to enable better understanding of motor and drive efficiencies during real-world operation. Additionally, the Motor Drive Analyzer derives the core and copper loss components using known motor constants and compares them to engineering models and the drive-control feedback circuit.</div></a></li>
										<li><a href="/doc/static-and-dynamic-motor-loss-comparisons-using-the-teledyne-lecroy-motor-drive-analyzer">
												<h5>Static and Dynamic Motor Loss Comparisons Using the Teledyne LeCroy Motor Drive Analyzer</h5>
												<div class="description" title="The Teledyne LeCroy Motor Drive Analyzer is ideal for measuring dynamic (transient) losses to enable better understanding of motor and drive efficiencies during real-world operation. Additionally, the Motor Drive Analyzer derives the core and copper loss components using known motor constants and compares them to engineering models and the drive-control feedback circuit.">The Teledyne LeCroy Motor Drive Analyzer is ideal for measuring dynamic (transient) losses to enable better understanding of motor and drive efficiencies during real-world operation. Additionally, the Motor Drive Analyzer derives the core and copper loss components using known motor constants and compares them to engineering models and the drive-control feedback circuit.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10257">
												<h5>AC Input Dynamic Power and Harmonic Analysis using the Motor Drive Analyzer</h5>
												<div class="description" title="Variable frequency motor drives provide enormous operational and control benefits, but their ability to dynamically control a motor requires equivalent dynamic analysis capability for validation and debug during research and development.   The Teledyne LeCroy Motor Drive Analyzer (MDA) dynamically measures per-cycle power and harmonic distortion values, plots them as Waveforms, and permits easy correlation of these values to drive AC input waveforms.">Variable frequency motor drives provide enormous operational and control benefits, but their ability to dynamically control a motor requires equivalent dynamic analysis capability for validation and debug during research and development.   The Teledyne LeCroy Motor Drive Analyzer (MDA) dynamically measures per-cycle power and harmonic distortion values, plots them as Waveforms, and permits easy correlation of these values to drive AC input waveforms.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10123">
												<h5>Grinding Machine Per-phase Motor Analysis using the Motor Drive Analyzer</h5>
												<div class="description" title="Complex industrial machines use motor drives to add precision to industrial processes. Understanding the complete motor and drive electrical operation on a per-phase basis can aid in understanding how the motor and drive react as load conditions change, such as in this cylindrical grinding machine from Fritz Studer AG.">Complex industrial machines use motor drives to add precision to industrial processes. Understanding the complete motor and drive electrical operation on a per-phase basis can aid in understanding how the motor and drive react as load conditions change, such as in this cylindrical grinding machine from Fritz Studer AG.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10124">
												<h5>Using a Quadrature Encoder Interface (QEI) with the Teledyne LeCroy Motor Drive Analyzer</h5>
												<div class="description" title="Digital encoders, such as QEIs, provide motor shaft speed and angle feedback to motor drive control systems.  The MDA can easily interpret these signals and convert them to speed and angle values that can be used for calculation of mechanical power, for comparison to a reference speed feedback signal, or for comparison to other control and power signals.">Digital encoders, such as QEIs, provide motor shaft speed and angle feedback to motor drive control systems.  The MDA can easily interpret these signals and convert them to speed and angle values that can be used for calculation of mechanical power, for comparison to a reference speed feedback signal, or for comparison to other control and power signals.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10125">
												<h5>Six-phase Motor Analysis using the Motor Drive Analyzer</h5>
												<div class="description" title="Motors with more than three-phases suit applications calling for redundancy and high reliability. The MDA captures voltage and current signals from both winding sets, measures total static and dynamic power consumption, and calculates balance amongst the two winding sets.">Motors with more than three-phases suit applications calling for redundancy and high reliability. The MDA captures voltage and current signals from both winding sets, measures total static and dynamic power consumption, and calculates balance amongst the two winding sets.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10126">
												<h5>Small Motor Efficiency Measurements Using the Teledyne LeCroy Motor Drive Analyzer</h5>
												<div class="description" title="More than 90% of motor and motor-drive designs involve small, low-power motors and drives used in household, commercial, or light industrial applications. Such motors need not meet rigorous IEC or NEMA efficiency standards, but engineers still want to measure their designs’ efficiency. The Teledyne LeCroy Motor Drive Analyzer measures drive and motor efficiency at a bench without an expensive and/or scarce dynamometer.">More than 90% of motor and motor-drive designs involve small, low-power motors and drives used in household, commercial, or light industrial applications. Such motors need not meet rigorous IEC or NEMA efficiency standards, but engineers still want to measure their designs’ efficiency. The Teledyne LeCroy Motor Drive Analyzer measures drive and motor efficiency at a bench without an expensive and/or scarce dynamometer.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10127">
												<h5>Using CAN Digital Data for Speed Calculation in the Teledyne LeCroy Motor Drive Analyzer</h5>
												<div class="description" title="The Controlled Area Network (CAN) serial data protocol communicates information over a serial bus in automotive and industrial applications. For motor applications, CAN data may contain digitally encoded electric motor shaft speed. The MDA extracts this encoded data, converts it to an analog value, rescales it, and then uses it in the MDA as a source for motor shaft speed.">The Controlled Area Network (CAN) serial data protocol communicates information over a serial bus in automotive and industrial applications. For motor applications, CAN data may contain digitally encoded electric motor shaft speed. The MDA extracts this encoded data, converts it to an analog value, rescales it, and then uses it in the MDA as a source for motor shaft speed.</div></a></li>
										<li><a href="/doc/docview.aspx?id=10128">
												<h5>Two-wattmeter-method, Three-phase Power Calculations with Accurate Line-Line to Line-Neutral Conversion</h5>
												<div class="description" title="The two-wattmeter method reduces from six to four the number of input channels required to measure power on a three-phase system. With this method, the calculated power values from each line-line voltage and line-neutral current pair are not in phase or balanced, though the total three-phase power value is correct. However, the Motor Drive Analyzer (MDA) performs a line-to-line to line-neutral conversion and displays accurate and balanced per-phase power values that aid in understanding proper three-phase system operation.">The two-wattmeter method reduces from six to four the number of input channels required to measure power on a three-phase system. With this method, the calculated power values from each line-line voltage and line-neutral current pair are not in phase or balanced, though the total three-phase power value is correct. However, the Motor Drive Analyzer (MDA) performs a line-to-line to line-neutral conversion and displays accurate and balanced per-phase power values that aid in understanding proper three-phase system operation. </div></a></li>
									</div>
									<div class="col-md-6">
										<li><a href="/doc/docview.aspx?id=9968">
												<h5>Testing AC Induction Motor Drives with Motor Drive Analyzer</h5>
												<div class="description" title="Thorough characterization and analysis of an AC induction motor drive requires the ability to view waveforms during dynamic events and to visualize the measurements over time. Whereas traditional power analyzers provide only a static mean measurement capability and, perhaps, viewing of very short waveform acquisitions, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor drive’s performance and efficiency."> Thorough characterization and analysis of an AC induction motor drive requires the ability to view waveforms during dynamic events and to visualize the measurements over time. Whereas traditional power analyzers provide only a static mean measurement capability and, perhaps, viewing of very short waveform acquisitions, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor drive’s performance and efficiency.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9969">
												<h5>Testing a Battery-Powered Brushless DC Motor/Drive with Motor Drive Analyzer</h5>
												<div class="description" title="Thorough characterization and analysis of a brushless DC motor and its associated drive circuitry requires the ability to view waveforms during dynamic events and to visualize the measurements over time. Whereas traditional power analyzers provide only a static mean measurement capability and, perhaps, viewing of very short waveform acquisitions, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor/drive’s performance and efficiency.">Thorough characterization and analysis of a brushless DC motor and its associated drive circuitry requires the ability to view waveforms during dynamic events and to visualize the measurements over time. Whereas traditional power analyzers provide only a static mean measurement capability and, perhaps, viewing of very short waveform acquisitions, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor/drive’s performance and efficiency.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9970">
												<h5>Examining a Vehicular Propulsion Motor with Motor Drive Analyzer</h5>
												<div class="description" title="One road to thorough characterization and analysis of a vehicular propulsion motor and its associated drive circuitry involves use of analog torque load cells and analog tachometers to assess drive output to motor-shaft mechanical output efficiency. Whereas traditional power analyzers provide only a static mean measurement capability, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor/drive’s performance and efficiency."> One road to thorough characterization and analysis of a vehicular propulsion motor and its associated drive circuitry involves use of analog torque load cells and analog tachometers to assess drive output to motor-shaft mechanical output efficiency. Whereas traditional power analyzers provide only a static mean measurement capability, Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides static and dynamic power analysis coupled with long acquisition times for comprehensive analysis of the motor/drive’s performance and efficiency.     </div></a></li>
										<li><a href="/doc/docview.aspx?id=9971">
												<h5>Volt-second Sensing Control Analysis in a DB-DTFC with Motor Drive Analyzer</h5>
												<div class="description" title="Deadbeat-direct torque and flux control (DB-DTFC) is being explored as an alternative to field-oriented controls (FOC) or direct torque control (DTC) due to its ability to achieve fast torque dynamics, manipulate losses without compromising torque, and utilize full DC bus voltage at voltage-limited operation. Evaluating the Volt-second sensing accuracy requires a precise measurement of Volt-seconds as the reference. Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides such capabilities over each power-semiconductor switching period."> Deadbeat-direct torque and flux control (DB-DTFC) is being explored as an alternative to field-oriented controls (FOC) or direct torque control (DTC) due to its ability to achieve fast torque dynamics, manipulate losses without compromising torque, and utilize full DC bus voltage at voltage-limited operation. Evaluating the Volt-second sensing accuracy requires a precise measurement of Volt-seconds as the reference. Teledyne LeCroy’s MDA810 Motor Drive Analyzer provides such capabilities over each power-semiconductor switching period.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9973">
												<h5>Analyzing Variable-Flux Electric Machines with Motor Drive Analyzer</h5>
												<div class="description" title="Rotors with magnets configured for variable magnetization can minimize losses in a propulsion motor at different rotor speeds and loads. Varying the magnetization requires the motor drive control system to acquire data and make calculations during a very short magnetization period.   Teledyne LeCroy’s MDA810 Motor Drive Analyzer acquires and analyzes the controlled energy pulse trajectory to ensure proper operation of the motor drive and control system."> Rotors with magnets configured for variable magnetization can minimize losses in a propulsion motor at different rotor speeds and loads. Varying the magnetization requires the motor drive control system to acquire data and make calculations during a very short magnetization period.   Teledyne LeCroy’s MDA810 Motor Drive Analyzer acquires and analyzes the controlled energy pulse trajectory to ensure proper operation of the motor drive and control system.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9199">
												<h5>Comparing Three-Phase Power Measurement Instruments</h5>
												<div class="description" title="Teledyne LeCroy’s Motor Drive Analyzer (MDA) provides three-phase power measurements comparable to a power analyzer using its eight 12-bit input channels. This brief summarizes how to correctly compare Yokogawa’s power analyzer instrument to the MDA, and to explain any differences in measurement results between the two instruments.">Teledyne LeCroy’s Motor Drive Analyzer (MDA) provides three-phase power measurements comparable to a power analyzer using its eight 12-bit input channels. This brief summarizes how to correctly compare Yokogawa’s power analyzer instrument to the MDA, and to explain any differences in measurement results between the two instruments.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9279">
												<h5>Complex Drive and Control Interaction Testing with the Motor Drive Analyzer</h5>
												<div class="description" title="Motor drive control system debugging often requires the ability to view control and power waveforms concurrently during dynamic events to understand cause and effect relationships, and visualize the dynamic power behaviors over time, correlated to control activity. Traditional Power Analyzers only provide a static (mean value) measurement capability, limited waveform capture, and no support for measuring control system signals. The Teledyne LeCroy MDA810 Motor Drive Analyzer provides static and dynamic power analysis with complete oscilloscope embedded control test capability and correlation of control signals to power events."> Motor drive control system debugging often requires the ability to view control and power waveforms concurrently during dynamic events to understand cause and effect relationships, and visualize the dynamic power behaviors over time, correlated to control activity. Traditional Power Analyzers only provide a static (mean value) measurement capability, limited waveform capture, and no support for measuring control system signals. The Teledyne LeCroy MDA810 Motor Drive Analyzer provides static and dynamic power analysis with complete oscilloscope embedded control test capability and correlation of control signals to power events.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9255">
												<h5>The CA10 Current Sensor Adapter and Danisense Current Measurement Sensor</h5>
												<div class="description" title="The CA10 Current Sensor Adapter provides enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Danisense flux gate- based current measurement sensor. Each CA10 setting is described, making it easy to transfer this example to any other current sensor with a voltage output."> The CA10 Current Sensor Adapter provides enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Danisense flux gate- based current measurement sensor. Each CA10 setting is described, making it easy to transfer this example to any other current sensor with a voltage output.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9254">
												<h5>The CA10 Current Sensor Adapter and Pearson Current Sensor</h5>
												<div class="description" title="The CA10 Current Sensor Adapter enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Pearson current measurement sensor. Each CA10 setting will be described, making it easy to transfer this example to any other current sensor with a voltage output.">The CA10 Current Sensor Adapter enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Pearson current measurement sensor. Each CA10 setting will be described, making it easy to transfer this example to any other current sensor with a voltage output.                </div></a></li>
										<li><a href="/doc/docview.aspx?id=9256">
												<h5>The CA10 Current Sensor Adapter and Rogowski Coils</h5>
												<div class="description" title="The CA10 Current Sensor Adapter provides the ability for a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a PEM-UK Rogowski Coil. Each CA10 setting is described, making it easy to transfer this example to any other current sensor with a voltage output."> The CA10 Current Sensor Adapter provides the ability for a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a PEM-UK Rogowski Coil. Each CA10 setting is described, making it easy to transfer this example to any other current sensor with a voltage output.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9253">
												<h5>Using the CA10 Current Sensor Adapter With a Current Transformer</h5>
												<div class="description" title="The CA10 Current Sensor Adapter enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Murata Power Solutions current transformer. Each CA10 setting will be described, making it easy to transfer this example to any other current transformer.">The CA10 Current Sensor Adapter enables a third-party current measurement device to operate like a Teledyne LeCroy probe. This brief explains how to use the CA10 with a Murata Power Solutions current transformer. Each CA10 setting will be described, making it easy to transfer this example to any other current transformer.</div></a></li>
										<li><a href="/doc/docview.aspx?id=9321">
												<h5>Using a Resolver Interface with the Motor Drive Analyzer</h5>
												<div class="description" title="Motor drives utilizing complex control systems (e.g. Vector field-oriented control or FOC) commonly use a resolver interface to provide signals to the control system for calculation of motor shaft speed, angle, and absolute position. The control system requires this information for correct calculation of the required drive output signals. A resolver interface makes debugging and validating drives and control systems much easier when a power analyzer, such as the MDA810, calculates speed, angle, and position directly from the resolver interface and displays them as waveforms."> Motor drives utilizing complex control systems (e.g. Vector field-oriented control or FOC) commonly use a resolver interface to provide signals to the control system for calculation of motor shaft speed, angle, and absolute position. The control system requires this information for correct calculation of the required drive output signals. A resolver interface makes debugging and validating drives and control systems much easier when a power analyzer, such as the MDA810, calculates speed, angle, and position directly from the resolver interface and displays them as waveforms.</div></a></li>
									</div>
								</div>
							</ul>
						</div>
					</div>
				</section>
			</main>
</asp:Content>