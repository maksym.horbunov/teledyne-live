(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $("#tech-briefs").hide();

    $("#briefs").click(function () {
        $("#tech-briefs").toggle(function () {
            $(".header-brief").focus();
        });
        $(this).text(function (i, text) {
            return text === "SEE LESS" ? "SEE MORE" : "SEE LESS";
        });
    });     

$("#close-briefs").click(function(){
    $("#tech-briefs").addClass("hide");
});

  }); // end of document ready
})(jQuery); // end of jQuery name space