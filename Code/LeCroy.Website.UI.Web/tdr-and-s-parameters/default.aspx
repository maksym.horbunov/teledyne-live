<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.timedomainreflectometers_default" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content4" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop" style="display:table;">
                    <div style="display:table-cell;padding-left: 40px;"><h1 style="font-family: Roboto Condensed, sans-serif; font-size: 32px; font-weight: 500; color: #008ecd; margin-top: 25px;">Time Domain Reflectometers and<br />S-Parameter Analyzers</h1>
                    <img src="/images/testtools/wavepulser40ix-t3sp15d.png" style="margin-top: -15px;margin-left: 48px;" />
                        
                    </div><div class="subNav" style="display:table-cell;">
                        <ul>
                            <li><a href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=592">WavePulser 40iX High Speed Interconnect Analyzer</a></li>
                            <li><a href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=570">T3SP Time Domain Reflectometers</a></li>
                            <!--<li><a href="<%=rootDir %>/support/softwaredownload/documents.aspx?sc=23">Download Software</a></li>-->
                            <li><a href="<%=rootDir %>/support/techlib/productmanuals.aspx?type=2&cat=35">View Manuals</a></li>
                        </ul>
                    </div>
            </div>
    <div id="categoryBottom" style="padding-left: 60px;">
        <p style="font-weight: 700;font-size: 13px;">The Teledyne LeCroy WavePulser 40iX and the Teledyne Test Tools T3SP15D are a perfect combination<br />
                        of complimentary products to serve the requirements on testing, validating and troubleshooting cables,<br />
                        backplanes, connectors, transmission lines on boards and interconnect.</p>
        <style type="text/css">
table {
    border-collapse: collapse;
    table-layout:fixed;width: 700px;
  white-space: nowrap;
}

table td{
    border: 1px solid black; font-size: 12px; padding: 6px;white-space: nowrap;
  overflow: hidden;
}
            .rotate{
                   border:none;
                   
                   
                   font-weight: bold;
   background-color: lightgray;

  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  width: 1.5em;
}
.rotate div {transform: rotate(-90deg); text-transform: uppercase;float:left;transform-origin: bottom center 0;
             color: black;margin-top: 50px;
             margin-left: 10px; font-weight: bold;
        
}
            .col-one {
            width: 30px;}
            .col-two {
            width: 200px;}
            .col-three {
            width: 235px;}
            .col-four {
            width: 235px;}
            .example_a {
color: #fff !important;
text-transform: uppercase;
background: #008ecd;
padding: 20px;
border-radius: 5px;
display: inline-block;
border: none;
}.example_a:hover {
background: #434343;
letter-spacing: 1px;
-webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
-moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
transition: all 0.4s ease 0s;
}
</style>
        <table>
    <thead style="border: 1px solid #008ecd;background-color: #008ecd; color: whitesmoke;font-size: 14px;font-weight: 800; letter-spacing: 0.1em;">
        <tr><th class="col-one">&nbsp;</th><th class="col-two">&nbsp;</th><th class="col-three"><a style="color: whitesmoke;" href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=592">WavePulser 40iX</a></th><th class="col-four"><a style="color: whitesmoke;" href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=570">T3SP15D</a></th></tr>
    </thead>
    <tbody>
        <tr"><td class="rotate" rowspan="3"><div class="rotate">frequency</div></td><td style="border-top: none;">Frequency Range</td><td style="border-top: none;">DC to 40 GHz</td><td style="border-top: none;">DC to 15 GHz</td></tr>
        <tr><td>S-parameters</td><td>Single-ended, Differential, and Mixed-mode<br>Full S-parameters(S<sub>11</sub>, S<sub>12</sub>, S<sub>21</sub>, S<sub>22</sub>)</td><td>Single-ended S<sub>11</sub>, S<sub>12</sub>, S<sub>21</sub>, S<sub>22</sub><br>Differential S<sub>11</sub></td></tr>
        <tr><td>Calibration</td><td>Internal automatic & manual OSLT</td><td>Manual OSL and OSLT calibration</td></tr>
        <tr><td class="rotate" rowspan="4"><div class="rotate" style="margin-bottom: 33px;letter-spacing:1px;">time</div></td><td style="border-top: solid black 3px;">Impulse/Step Rise Time</td><td style="border-top: solid black 3px;">8.5 ps</td><td style="border-top: solid black 3px;">35 ps</td></tr>
        <tr><td>Impedance Profile</td><td>Differential and Common-mode</td><td>Differential</td></tr>
        <tr><td>TDR/TDT Solution</td><td>TDR/TDT</td><td>TDR</td></tr>
        <tr><td>Spatial Resolution</td><td>< 1 mm</td><td>< 3 mm</td></tr>
        <tr><td class="rotate" rowspan="4"><div class="rotate">deep toolbox</div></td><td style="border-top: solid black 3px;">Simulation and De-embedding</td><td style="border-top: solid black 3px;">Yes</td><td style="border-top: solid black 3px;">No</td></tr>
        <tr><td>Time-gating</td><td>Yes</td><td>No</td></tr>
        <tr><td>Emulation of Eye Diagrams</td><td>Yes</td><td>No</td></tr>
        <tr><td>Jitter Analysis</td><td>Yes</td><td>No</td></tr>
        <tr><td class="rotate" rowspan="4"><div class="rotate">platform</div></td><td style="border-top: solid black 3px;">Number of Ports</td><td style="border-top: solid black 3px;">4</td><td style="border-top: solid black 3px;">2</td></tr>
        <tr><td>USB-connected</td><td>Yes</td><td>Yes</td></tr>
        <tr><td>Size/Weight</td><td>105mm H x 305mm W x 230mm D, 3.3 kg</td><td>82.5mm H x 210mm W x 220mm D, 2.6 kg</td></tr>
        <tr><td>Battery-powered</td><td>No</td><td>Yes (optional)</td></tr>
        <tr><td style="border:none;" colspan="2">&nbsp;</td><td style="border:none;"><div class="button_cont" align="center"><a class="example_a" href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=592" rel="nofollow noopener">See more: WavePulser 40iX</a></div></td>
            <td style="border:none;"><div class="button_cont" align="center"><a class="example_a" href="<%=rootDir %>/tdr-and-s-parameters/series.aspx?mseries=570" rel="nofollow noopener">See more: T3SP15D</a></div></td></tr>
    </tbody>
</table>
    </div>
</asp:Content>