Imports LeCroy.Library.VBUtilities
Imports LeCroy.Website.BLL

Partial Class timedomainreflectometers_default
    Inherits BasePage
    Dim menuURL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim captionID As String
        Dim menuID As String
        ' if URL contains query string, validate all query string variables
        '** captionID
        If Len(Request.QueryString("capid")) = 0 Then
            captionID = AppConstants.SCOPE_CAPTION_ID
        Else
            If IsNumeric(Request.QueryString("capid")) Then
                captionID = Request.QueryString("capid")
            Else
                captionID = AppConstants.SCOPE_CAPTION_ID
            End If
        End If

        '  ** menuID
        If Len(Request.QueryString("mid")) = 0 Then
            menuID = getDefaultMenu(captionID)
        Else
            If IsNumeric(Request.QueryString("mid")) Then
                menuID = Request.QueryString("mid")
            Else
                menuID = getDefaultMenu(captionID)
            End If
        End If
        menuURL = "&capid=" + captionID
        menuURL += "&mid=" + menuID

        Session("menuSelected") = captionID

        If Not (FeProductManager.CanProductShowByProductSeriesId(ConfigurationManager.AppSettings("ConnectionString").ToString(), 570, 35)) Then
            Response.Redirect("~/")
        End If

        If Not Me.IsPostBack Then
            Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - TDR and S-Parameters"
        End If
    End Sub
End Class