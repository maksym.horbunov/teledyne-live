﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/singlecolumn.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.terms_default" %>
<%@ MasterType VirtualPath="~/MasterPage/singlecolumn.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CenterColumn" runat="server">
    <div style="background-color: #ffffff; height: 100%;">
        <div style="padding-bottom: 12px; padding-left: 30px; padding-right: 30px; padding-top: 20px;">

<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><h2>Terms and Conditions of Purchase (for Suppliers)</h2></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><h2>Terms and Conditions of Sale (for Customers)</h2></td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy, Inc.<br><a href="./teledyne_lecroy_us-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (US)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><a href="./teledyne_lecroy_terms_and_conditions_of_sale.pdf">Teledyne LeCroy General Terms and Conditions of Sale</a></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy AB<br><a href="./teledyne_lecroy_sweden-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (Sweden)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy GmbH<br><a href="./teledyne_lecroy_germany-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (Germany)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy S.A.R.L.<br><a href="./teledyne_lecroy_france-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (France)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy S.R.L.<br><a href="./teledyne_lecroy_italy-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (Italy)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy SA<br><a href="./teledyne_lecroy_switzerland-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (Switzerland)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>For orders placed by Teledyne LeCroy, a division of Teledyne Limited<br><a href="./teledyne_lecroy_uk-general_terms_and_conditions_of_purchase.pdf">Teledyne LeCroy General Terms and Conditions of Purchase (UK)</a></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td></td>
	</tr>
</table>

            
        </div>
    </div>
</asp:Content>