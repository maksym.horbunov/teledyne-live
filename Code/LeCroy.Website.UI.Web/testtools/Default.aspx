﻿	<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/site.master" CodeBehind="Default.aspx.vb" Inherits="LeCroy.Website.testtools_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
	<style>.big-text p,
.section-banner p {
    font-size: 20px !important;
    font-weight: 400 !important;
}</style>
			<main id="main" role="main" class="has-header-transparented skew-style reset v1">
				<section class="section-banner bg-retina small"><span data-srcset="https://assets.lcry.net/images/equipment-hero.png"></span>
					<div class="bg-overlay"></div>
					<div class="banner">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h1>Electronic Test Equipment</h1>
									<p>Wide selection of equipment covering all of your measurement needs</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-angle section-training-solutions big-text">
					<div class="container">
						<div class="row">
							<div class="col-lg-10 text-center m-auto">
								<p>Teledyne LeCroy offers a comprehensive range of electronic test equipment solutions to complement our well known family of oscilloscopes and analyzers. We offer these tools under both the Teledyne LeCroy and Teledyne Test Tools brand names. The Teledyne Test Tools family of products was created in collaboration with leading OEM technology partners and comes with the world class service and support you've come to expect from us.</p>
							</div>
						</div>
					</div>
				</section>
				<section class="section-gray section-visibility">
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line"> 
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-11.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Digital Multimeters</h3>
											</div>
											<div class="card-text">
												<p>Design and troubleshoot electrical systems by measuring voltage, current and resistance with a high degree of accuracy.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/digital-multimeters/default.aspx">Explore</a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-5.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Power Supplies</h3>
											</div>
											<div class="card-text">
												<p>Supply electric power to an electric load with a high degree of control, precision, reliability and visibility.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/power-supplies/default.aspx">Explore</a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-10.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Function and Arbitrary Waveform Generators</h3>
											</div>
											<div class="card-text">
												<p>Generate fixed or arbitrarily defined waveforms and signals for use in the development, testing and repair of electronic systems.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/waveform-function-generators/">Explore</a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-6.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Spectrum Analyzers</h3>
											</div>
											<div class="card-text">
												<p>Observe dominant frequency, power, distortion, harmonics, bandwidth, and other spectral components of a signal not easily detectable in time domain waveforms.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/spectrum-analyzers/default.aspx">Explore</a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-15.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>TDR and S-Parameters</h3>
											</div>
											<div class="card-text">
												<p>Test, validate, and troubleshoot performance characteristics of cables, backplanes, connectors, and transmission lines on boards.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/tdr-and-s-parameters/default.aspx">Explore                                                    </a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment07.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Electronic Loads</h3>
											</div>
											<div class="card-text">
												<p>Typically used to provide a fully programmable load to test and  ensure quality, reliability, and performance of a power source.</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/electronic-loads/default.aspx">Explore       </a></div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 mb-32 mb-md-0">
									<div class="card-line">
										<div class="card-images text-center">                                   <img src="https://assets.lcry.net/images/equipment-16.png" alt="img-description"></div>
										<div class="inner-content small h-auto pb-4">
											<div class="card-title">
												<h3>Vector Network Analyzer</h3>
											</div>
											<div class="card-text">
												<p>Generate signals to characterize the electrical network parameters of components, devices, circuits, and sub-assemblies.
												</p>
											</div>
										</div>
										<div class="inner-visible p-4 pb-40 pt-0"><a class="btn btn-default" href="/vector-network-analyzer/">Explore                                 </a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main></asp:Content>