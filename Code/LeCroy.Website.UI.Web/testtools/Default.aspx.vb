﻿Partial Class testtools_default
    Inherits BasePage

#Region "Variables/Keys"
#End Region

#Region "Page Events"
    Private Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.Title = ConfigurationManager.AppSettings("DefaultPageTitle") + " - Test Tools"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
    End Sub
#End Region

#Region "Control Events"
#End Region

#Region "Page Methods"
#End Region
End Class