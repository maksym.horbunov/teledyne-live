﻿<!DOCTYPE html>
<html lang="en">
<head>
  <meta content="IE=Edge"
        http-equiv="X-UA-Compatible">
  <meta content="text/html; charset=utf-8"
        http-equiv="Content-Type">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"
        name="viewport">
  <title>Teledyne Test Tools | Teledyne LeCroy</title>
  <meta content="Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analyzers have the right tools, right measurements."
        name="description">
  <meta content="Teledyne Test Tools"
        property="og:title">
  <meta content="https://teledynelecroy.com/images/testtools/testtools_highperformance.png"
        property="og:image">
  <meta content="https://teledynelecroy.com/testtools/"
        property="og:url">
  <meta content=
  "Teledyne LeCroy’s testing solutions for electrical embedded systems, torque measurements, and protocol analysis of wired and wireless systems have the right tools, right measurements, and fastest time to insight of any solution in the industry."
        property="og:description"><!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <link href="css/materialize.min.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="css/style.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700"
        rel="stylesheet"
        type="text/css">
</head>
<body>
  <div class="container">
    <nav>
      <div class="nav-wrapper white">
        <a class="brand-logo"
             href="/"><img src="/images/tl_weblogo_blkblue_189x30.png"></a>
        <ul class="right hide-on-med-and-down">
          <li><!--<div class="valign-wrapper">
            <a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true"><img src="/images/testtools/car-thumb.png" height="43px" class="responsive-img" alt="Automotive Solutions Group"></a>
          </div> --></li>
        </ul>
        <ul class="left hide-on-med-and-down"
            style="margin-left: 200px; margin-top:10px;">
          <li class="dropdown">
            <a class="dropdown-button"
                data-activates="dropdownp"
                data-beloworigin="true"
                href="#!">Products</a>
          </li>
          <li class="dropdown">
            <a class="dropdown-button"
                data-activates="dropdowns"
                data-beloworigin="true"
                href="#!">Serial Data</a>
          </li>
          <li class="dropdown">
            <a class="dropdown-button"
                data-activates="dropdownu"
                data-beloworigin="true"
                href="/support/">Support</a>
          </li>
          <li>
            <a href="/support/">Buy</a>
          </li>
        </ul>
      </div>
    </nav>
  </div><!-- Dropdown Structure -->
  <ul class="dropdown-content"
      id="dropdown1">
    <li>
      <a href="http://www.fte.com">Frontline</a>
    </li>
    <li>
      <a href="/protocolanalyzer/">Protocol Solutions Group</a>
    </li>
    <li>
      <a href="http://www.quantumdata.com/">quantumdata</a>
    </li>
    <li>
      <a href="/oscilloscope/">Oscilloscopes</a>
    </li>
    <li>
      <a href="https://www.teledyne-ts.com">Torque Test</a>
    </li>
  </ul>
  <ul class="dropdown-content"
      id="dropdownp">
    <li>
      <a href="/oscilloscope/">Oscilloscopes</a>
    </li>
    <li>
      <a href="/probes/">Oscilloscope Probes</a>
    </li>
    <li>
      <a href="/protocolanalyzer/">Protocol Analyzers</a>
    </li>
    <li>
      <a href="/pert3/pertphoenix.aspx">PeRT</a>
    </li>
    <li>
      <a href="/waveform-function-generators/">Waveform Function Generators</a>
    </li>
    <li>
      <a href="/logicanalyzers/">Logic Analyzers</a>
    </li>
    <li>
      <a href="/tdr-and-s-parameters/">TDR and S-Parameters</a>
    </li>
      <li><a href="/spectrum-analyzers/">Spectrum Analyzers</a></li>
<li><a href="/power-supplies/">Power Supplies</a></li>
 <li><a href="/digital-multimeters/">Digital Multimeters</a></li>
  </ul>
  <ul class="dropdown-content"
      id="dropdowns">
    <li>
      <a href="/serialdata/">Serial Data Standards</a>
    </li>
    <li>
      <a href="/sdaiii/">Serial Data Analysis</a>
    </li>
  </ul>
  <ul class="dropdown-content"
      id="dropdownu">
    <li>
      <a href="/support/techlib/">Tech Library</a>
    </li>
    <li>
      <a href="/support/service.aspx">Instrument Service</a>
    </li>
    <li>
      <a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a>
    </li>
    <li>
      <a href="/support/dsosecurity.aspx">Oscilloscope Security</a>
    </li>
    <li>
      <a href="/support/register/">Product Registration</a>
    </li>
    <li>
      <a href="/support/softwaredownload/">Software Downloads</a>
    </li>
    <li>
      <a href="/support/techhelp/">Technical Help</a>
    </li>
    <li>
      <a href="/support/requestinfo/">Request</a>
    </li>
    <li>
      <a href="/support/rohs.aspx">RoHS &amp; WEEE</a>
    </li>
    <li>
      <a href="/events/">Events &amp; Training</a>
    </li>
    <li>
      <a href="/support/training.aspx">Training</a>
    </li>
    <li>
      <a href="/support/user/userprofile.aspx">Update Profile</a>
    </li>
    <li>
      <a href="/support/contact/">Contact Us</a>
    </li>
  </ul><!--end of dropwdowns -->
 
    

<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m12 l12 xl12">
          <div class="slider">
            <ul class="slides">
                <li>
                  <img src="/images/testtools/control_banner.jpg" class="responsive-img">
                  <div class="caption right-align">
                    <h4 class="light grey-text text-lighten-3">Delivering control, precision, reliability and visibility.</h4>
                  </div>
                </li>
                <li>
                  <img src="/images/testtools/measurements_banner.jpg" class="responsive-img">
                  <div class="caption right-align">
                    <h4 class="light grey-text text-lighten-3">Bringing precision measurements to every lab.</h4>
                  </div>
                </li>
                <li>
                  <img src="/images/testtools/benches_banner.jpg" class="responsive-img">
                  <div class="caption left-align">
                    <h4 class="light grey-text text-lighten-3">Even small benches can hold the biggest name in test tools.</h4>
                  </div>
                </li>
                <li>
                  <img src="/images/testtools/performance_banner.jpg" class="responsive-img">
                  <div class="caption left-align">
                    <h4 class="light grey-text text-lighten-3">High performance test tools for high performance innovators.</h4>
                  </div>
                </li>
              </ul>
          </div>
      </div>
      <div class="col s12 m12 l12">
        <h1>Test tools and solutions that enable speed-to-market, product validation and design innovation.</h1>
      </div>
      <div class="col s12 m8 l10">
        <p class="flow-text">Teledyne Test Tools is a comprehensive range of test equipment solutions to complement Teledyne LeCroy&#39;s family of oscilloscopes and analyzers. These tools provide a one-stop-shop for test engineers, developers and teaching establishments looking to satisfy ongoing testing, education and electronics validation needs efficiently, reliably and within budget.<br />
            <br />The Teledyne Test Tools portfolio was created in collaboration with leading OEM technology partners to support new product design needs across a range of industries such as mobile, automotive, communications, defense and manufacturing.
            <br />
          </p>
      </div>
    </div>
  </div>
</div>


<!-- Subcategory Cards *****There are 3 sections for 3 media sizes Be sure to change all three!! -->

<!-- MED cards for MED screen sizes -->
<div class="clearfix"></div>
<div class="wrap hide-on-small-only hide-on-large-only">
  <div class="container">
    <div class="section">
    <!--Row 1 -->
      <div class="row">
        <div class="col s4">
          <div class="card">
            <div class="card-image ">
              <a href=/waveform-function-generators/"><img class="activator" src="/images/testtools/signalgenerators_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Signal and Waveform Generators<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Signal and Waveform Generators<i class="material-icons right">close</i></span>
              <p>Generate fixed or arbitrarily defined waveforms and signals for use in the development, testing and repair of electronic systems.</p>
            </div>
          </div>    
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/digital-multimeters/"><img class="activator" src="/images/testtools/multimeters_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Digital Multimeters<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Digital Multimeters<i class="material-icons right">close</i></span>
              <p>Design and troubleshoot electrical systems by measuring voltage, current and resistance with a high degree of accuracy.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/oscilloscope/"><img class="activator" src="/images/testtools/scopes_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Oscilloscopes and Probes<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Oscilloscopes and Probes<i class="material-icons right">close</i></span>
              <p>Analyze amplitude, frequency, rise time, time interval, distortion and other properties of the observed change of an electrical signal over time.</p>
            </div>
          </div>   
        </div>
      </div>
      <!-- Row 2 -->
      <div class="row">        
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/power-supplies/"><img class="activator" src="/images/testtools/powersupply_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Power Supplies<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Power Supplies<i class="material-icons right">close</i></span>
              <p>Supply electric power to an electric load with a high degree of control, precision, reliability and visibility.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/spectrum-analyzers/"><img class="activator" src="/images/testtools/spectrumanalyzer_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Spectrum Analyzers<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Spectrum Analyzers<i class="material-icons right">close</i></span>
              <p>Observe dominant frequency, power, distortion, harmonics, bandwidth, and other spectral components of a signal not easily detectable in time domain waveforms.</p>
            </div>
          </div>   
        </div>
        <div class="col s4">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/tdr-and-s-parameters/"><img class="activator" src="/images/testtools/tdr_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">True Differential Time Domain Reflectometers<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">True Differential Time Domain Reflectometers<i class="material-icons right">close</i></span>
              <p>Simplify the setup for signal integrity measurements in modern high speed designs implemented with differential transmission lines.</p>
            </div>
          </div>   
        </div>
      </div>
        <!--Row 3 -removed KO
      <div class="row">
        <div class="col s4">
          <div class="card">
            <div class="card-image ">
              <a href="/oscilloscope/"><img class="activator" src="/images/testtools/hsd_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Consumer-Class Tools and Accessories<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Comsumer-Class Tools and Accessories<i class="material-icons right">close</i></span>
              <p>Utilize best-in-class partner products and accessories for technically superior results at affordable prices.</p>
            </div>
          </div>    
        </div>
      </div>-->
    </div>
  </div>
</div>

<!-- Small Single row cards for tablet / phone screen sizes -->
<div class="clearfix"></div>
<div class="wrap hide-on-med-only hide-on-large-only">
  <div class="container">
    <div class="section">
      <!--Row 1 -->
      <div class="row">
        <div class="col s12">

          <div class="card">
            <div class="card-image ">
              <a href="/waveform-function-generators/"><img class="activator" src="/images/testtools/signalgenerators_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Signal and Waveform Generators<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Signal and Waveform Generators<i class="material-icons right">close</i></span>
              <p>Generate fixed or arbitrarily defined waveforms and signals for use in the development, testing and repair of electronic systems.</p>
            </div>
          </div>    

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/digital-multimeters/"><img class="activator" src="/images/testtools/multimeters_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Digital Multimeters<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Digital Multimeters<i class="material-icons right">close</i></span>
              <p>Design and troubleshoot electrical systems by measuring voltage, current and resistance with a high degree of accuracy.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/oscilloscope/"><img class="activator" src="/images/testtools/scopes_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Oscilloscopes and Probes<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Oscilloscopes and Probes<i class="material-icons right">close</i></span>
              <p>Analyze amplitude, frequency, rise time, time interval, distortion and other properties of the observed change of an electrical signal over time.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/power-supplies/"><img class="activator" src="/images/testtools/powersupply_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Power Supplies<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Power Supplies<i class="material-icons right">close</i></span>
              <p>Supply electric power to an electric load with a high degree of control, precision, reliability and visibility.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/spectrum-analyzers/"><img class="activator" src="/images/testtools/spectrumanalyzer_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Spectrum Analyzers<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Spectrum Analyzers<i class="material-icons right">close</i></span>
              <p>Observe dominant frequency, power, distortion, harmonics, bandwidth, and other spectral components of a signal not easily detectable in time domain waveforms.</p>
            </div>
          </div>   

          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/tdr-and-s-parameters/"><img class="activator" src="/images/testtools/tdr_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">True Differential Time Domain Reflectometers<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">True Differential Time Domain Reflectometers<i class="material-icons right">close</i></span>
              <p>Simplify the setup for signal integrity measurements in modern high speed designs implemented with differential transmission lines.</p>
            </div>
          </div>
            
            <!--<div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <a href="/oscilloscope/"><img class="activator" src="/images/testtools/hsd_menu.jpg"></a>
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">Consumer-Class Tools and Accessories<i class="material-icons right">more_vert</i></span>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">Consumer-Class Tools and Accessories<i class="material-icons right">close</i></span>
              <p>Utilize best-in-class partner products and accessories for technically superior results at affordable prices.</p>
            </div>
          </div>  -->
               
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Stripey Section for LARGE screen sizes-->
<div class="hide-on-med-and-down">

    <div class="clearfix section"></div>
    <div class="wrap2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/signalgenerators_menu.jpg">
          </div>
          <div class="col s7 top">
            <h1 class="noPad">Signal and Waveform Generators</h1>
            <p class="flow-text">Generate fixed or arbitrarily defined waveforms and signals for use in the development, testing and repair of electronic systems.</p>
            <a class="waves-effect waves-light btn #0288d1 light-blue darken-2" href="/waveform-function-generators/"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
   
    <div class="clearfix section"></div>
    <div class="none2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/multimeters_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">Digital Multimeters</h1>
            <p class="flow-text">Design and troubleshoot electrical systems by measuring voltage, current and resistance with a high degree of accuracy.</p>
            <a href="/digital-multimeters/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix section"></div>
    <div class="wrap2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/scopes_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">Oscilloscopes and Probes</h1>
            <p class="flow-text">Analyze amplitude, frequency, rise time, time interval, distortion and other properties of the observed change of an electrical signal over time.</p>
            <a href="/oscilloscope/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix section"></div>
    <div class="none2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/powersupply_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">Power Supplies</h1>
            <p class="flow-text">Supply electric power to an electric load with a high degree of control, precision, reliability and visibility.</p>
            <a href="/power-supplies/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix section"></div>
    <div class="wrap2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/spectrumanalyzer_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">Spectrum Analyzers</h1>
            <p class="flow-text">Observe dominant frequency, power, distortion, harmonics, bandwidth, and other spectral components of a signal not easily detectable in time domain waveforms.</p>
            <a href="/spectrum-analyzers/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix section"></div>
    <div class="none2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/tdr_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">True Differential Time Domain Reflectometers</h1>
            <p class="flow-text">Aimed at optimizing wire transmission quality and reducing signal loss across the wire. Ideal for the automotive industry using automotive ethernet, wiring harnesses and other applications.</p>
            <a href="/tdr-and-s-parameters/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>
  
    <!-- removed 01-05-2019 KO <div class="clearfix section"></div>
    <div class="wrap2">
      <div class="container">
        <div class="row">
          <div class="col s5">
            <img class="responsive-img" src="/images/testtools/hsd_menu.jpg">
          </div>
          <div class="col s7">
            <h1 class="noPad">Consumer-Class Tools and Accessories</h1>
            <p class="flow-text">Utilize best-in-class partner products and accessories for technically superior results at affordable prices.</p>
            <a href="/oscilloscope/" class="waves-effect waves-light btn #0288d1 light-blue darken-2"><i class="material-icons right">info_outline</i>More</a>
          </div>
        </div>
      </div>
    </div>-->
  
  </div>
  
  <!-- End Stripey -->

<!-- End Cards Section -->



  <!-- Address Block -->
  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s12 m12 l6">
            <div class="row">
              <div class="col s12 m8">
                <div class="card blue-grey darken-1">
                  <div class="card-content white-text">
                    <p>Teledyne Test Tools<br>
                    700 Chestnut Ridge Road<br>
                    Chestnut Ridge, NY 10977-6499<br>
                    800-553-2769</p>
                    <ul class="collapsible"
                        data-collapsible="accordion">
                      <li>
                        <div class="collapsible-header light-blue accent-4 white-text">
                          <i class="material-icons left">email</i>Contact Us
                        </div>
                        <div class="collapsible-body">
                          <iframe allowtransparency="true"
                               frameborder="0"
                               height="350"
                               src="https://go.teledynelecroy.com/l/48392/2018-09-27/74pjx2"
                               style="border: 0"
                               type="text/html"></iframe>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- Address -->
  <!-- Simple footer -->
  <footer class="page-footer white">
    <div class="container center social">
      <p><a href="https://facebook.com/teledynelecroy"
         onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/facebook.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://twitter.com/teledynelecroy"
         onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/twitter.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://youtube.com/lecroycorp"
         onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/youtube.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://blog.teledynelecroy.com/"
         onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/blogger.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://www.linkedin.com/company/10007"
         onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/linkedin.jpg"
           width="25px"></a></p>
      <p>&copy; 2019&nbsp;Teledyne LeCroy</p><a href="/support/securitypolicy.aspx"
           target="_blank">Privacy Policy</a> | <a href="/sitemap/"
           target="_blank">Site Map</a>
      <p><a href="http://teledyne.com/"
         onclick="GaEventPush('OutboundLink', 'http://teledyne.com');"
         target="_blank">Teledyne Technologies</a></p>
    </div>
  </footer><!--End Footer -->
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
  <script src="js/materialize.min.js"></script> 
  <script src="js/init.js"></script> 
  <script>

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-13095488-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
  </script> 
  <script src="https://teledynelecroy.com/js/pardot.js"></script>
</body>
</html>
