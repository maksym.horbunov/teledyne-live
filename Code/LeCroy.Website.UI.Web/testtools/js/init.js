$(document).ready(function () {
    $(".dropdown-button").dropdown({ hover: true });
    //$(".modal-trigger").leanModal();
    $('.collapsible').collapsible({
        accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $('.slider').slider({ height: 350 });
});