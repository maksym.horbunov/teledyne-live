﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.tools_default" %>

 
  <!DOCTYPE html>
  <html>
    <head>
    <title>Teledyne LeCroy | Periodic Table of Elements</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> <!--bootstrap custom StyleSheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a class="navbar-brand" href="https://teledynelecroy.com/" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
          
        <div class="navbar-collapse collapse">
              <ul>

<li class="dropdown"><a href="javascript:void(0);">Products</a>
<ul class="dropdown-menu">
<li><a href="/oscilloscope/">Oscilloscopes</a></li>
<li><a href="/probes/">Oscilloscope Probes</a></li>
<li><a href="/protocolanalyzer/">Protocol Analyzers</a></li>
<li><a href="/pert3/pertphoenix.aspx">PeRT</a></li>
<li><a href="/waveform-function-generators/">Waveform Generators</a></li>
<li><a href="/logicanalyzers/">Logic Analyzers</a></li>
<li><a href="/tdr-and-s-parameters/">TDR and S-Parameters</a></li>
<li><a href="/spectrum-analyzers/">Spectrum Analyzers</a></li>
<li><a href="/power-supplies/">Power Supplies</a></li>
<li><a href="/digital-multimeters/">Digital Multimeters</a></li>
<li><a href="/electronic-loads/">Electronic Loads</a></li>

</ul></li>
<li class="dropdown"><a href="/serialdata/">Serial Data</a><ul class="dropdown-menu">
<li><a href="/serialdata/">Serial Data Standards</a></li>
<li><a href="/sdaiii/">Serial Data Analysis</a></li></ul></li>
<li class="dropdown"><a href="/support/">Support</a><ul class="dropdown-menu">
<li><a href="/support/techlib/">Tech Library</a></li>
<li><a href="/support/service.aspx">Instrument Service</a></li>
<li><a href="/support/knowledgebase.aspx">FAQ/Knowledgebase</a></li>
<li><a href="/support/dsosecurity.aspx">Oscilloscope Security</a></li>
<li><a href="/support/register/">Product Registration</a></li>
<li><a href="/support/softwaredownload/">Software Downloads</a></li>
<li><a href="/support/techhelp/">Technical Help</a></li>
<li><a href="/support/requestinfo/">Request Info</a></li>
<li><a href="/support/rohs.aspx">RoHS &amp; WEEE</a></li>
<li><a href="/events/">Events &amp; Training</a></li>
<li><a href="/support/training.aspx">Training</a></li>
<li><a href="/support/user/userprofile.aspx">Update Profile</a></li>
<li><a href="/support/contact/">Contact Us</a></li></ul></li>
<li><a href="/support/">Buy</a></li>
              </ul>
            </div>
      </div>
    </div>
        <!-- Headlines -->
        <div class="container">
            <div class="row">
                <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                    <h1>Periodic Table of Oscilloscope Tools</h1>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-18">
                    <h1>Our Heritage</h1><p style="font-size:1.5em;">Teledyne LeCroy’s 50+ year heritage is in processing long records to extract meaningful insight. We invented the digital oscilloscope and many of the additional waveshape analysis tools.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-6 col-xs-18">
                    <h1>Our Obsession</h1><p style="font-size:1.5em;">Our tools and operating philosophy are standardized across much of our product line. This deep toolbox inspires insight; and your moment of insight is our reward.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-6 col-xs-18">
                    <h1>Our Tools</h1><p style="font-size:1.5em;">Our Periodic Table of Oscilloscope Tools explains the toolsets that Teledyne LeCroy has deployed in our oscilloscopes. Visit our interactive website to learn more about them.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                    <a class="btn btn-primary btn-lg" href="/doc/tools-poster" style="float:right;margin-top:15px;">Download PDF of Table</a><h1>Periodic Table of Oscilloscope Tools</h1>
                </div>
            </div>
        </div>

    <div id="table" class="container">
  <div class="row row-header">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 capture">
      <span>Capture</span>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 view">
      <span>View</span>
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 measure">
      <span>Measure</span>
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 math">
      <span>Math</span>
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 analyze">
      <span>Analyze</span>
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 document">
      <span>Document</span>
    </div>
  </div>
    <div class="row row-titles">
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Triggering</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Acquire</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Display Grids</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Display Views</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Zooming</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Parameters</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Parameter Analysis</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Function</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Advanced Functions</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Pass/Fail</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Anomaly Direction</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Serial Decode</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Serial Message Analysis</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Clock &amp; Timing Jitter</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Serial Data Jitter</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Serial Data Analysis</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Application Packages</div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Document</div>
    </div>
    <div class="row v-space" id="row1"><!-- 1 to 2 -->
    </div>
    <div class="row v-space" id="row2"><!-- 3 to 10 -->
    </div>
    <div class="row v-space" id="row3"><!-- 11 to 18 -->
    </div>
    <div class="row v-space" id="row4"><!-- 19 to 37 -->
    </div>
    <div class="row v-space" id="row5"><!-- 38 to 56 -->
    </div>
    <div class="row v-space" id="row6"><!-- 57 to 75 -->
    </div>
    <div class="row v-space" id="row7"><!-- 1 to 2 -->
    </div>
    <div class="row v-space"><!--empty row-->
      <div class="col-xs-18 col-sm-18 col-md-18 col-lg-18">
        &nbsp;
      </div>
    </div>
    <div class="row v-space" id="row8"><!-- 1 to 2 -->
    </div>
    <div class="row v-space" id="row9"><!-- 1 to 2 -->
    </div>
        <div class="row v-space"><!--empty row-->
      <div class="col-xs-18 col-sm-18 col-md-18 col-lg-18">
        &nbsp;
      </div>
    </div>
  </div>

  <!-- Modal Structure -->
  <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <h4 class="modal-title" id="myModalLabel">Title</h4>
        </div>
        <div class="modal-body" style="background-color: #595a5c;">
<div class="media">
  <div class="media-left media-middle">
      <img class="media-object" src="img/icon.png" alt="..." style="width: 250px;">
  </div>
  <div class="media-body" style="background-color: white;padding: 10px;">
    <h4 class="media-heading"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></h4>
    <p class="description">description</p>
    <p class="small-cat bg-info">category</p>
  </div>
</div>
          </div>
        </div>
      </div>
    </div>

<div class="text-center center-block footer">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p>&copy; 2019&nbsp;Teledyne LeCroy</p>

    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.12.0.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/init.js"></script>
  <script>
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }
      ,
      i[r].l = 1 * new Date();
      a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-13095488-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
  </script>
  <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>