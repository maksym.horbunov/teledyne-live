﻿{
    "elements": {
        "row1": [
            {
                "number": "1",
                "name": "Exclusion",
                "icon": "icon1.png",
                "category": "capture",
                "description": "Exclusion Trigger permits many of the different Teledyne LeCroy SMART triggers to be set for a condition that is inside or outside of a specified range.  Supported triggers includes Width, Glitch, Interval, Runt and Slew Rate.",
                "unique": "false",
                "invent": "true"
            },
            {
                "number": "0",
                "description": "col16"
            },
            {
                "number": "2",
                "name": "Hardcopy",
                "icon": "icon2.png",
                "category": "document",
                "description": "Utilize the Print button the oscilloscope to pre-define a documentation action and execute that action with the press of one button.  Select to create a file of a screen image in a variety of formats, send an email with an attached screen image, copy data to a clipboard, print a document, or save the waveform data, screen images, panel setups, and the end user's annotations as a LabNotebook <a onclick=&quot;updateModal(this)&quot; data-id=&quot;90&quot; href=&quot;javascript:void(0)&quot;>(90)</a>.  ",
                "unique": "false",
                "invent": "true"
            }
        ],
            "row2": [
                {
                    "number": "3",
                    "name": "Measurement ",
                    "icon": "icon3.png",
                    "category": "capture",
                    "description": "Measurement trigger provides the ability to utilize any Teledyne LeCroy measurement parameter to locate a specified measurement of interest in an acquisition, stop the acquisition and indicate the &quot;triggered&quot; position as if a normal hardware trigger condition was satisfied.  Nearly 40 measurement parameters are supported.  Measurement trigger leverages All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability to ensure that any measured event in an acquisition is found if it meets the defined condition.   Supported measurement trigger conditions include less than, greater than, in-range, out-of-range, and don't care.   A usage example would be to trigger on period jitter greater than a certain value, or on an out-of-range complex timing behavior.  ",
                    "unique": "false",
                    "invent": "false"
                },
                {
                    "number": "4",
                    "name": "5 MS/s Roll",
                    "icon": "icon4.png",
                    "category": "capture",
                    "description": "Roll Mode is an acquisition capability by which very low-speed and long-duration acquisitions are displayed simultaneous with acquisition.   This is highly desirable in acquisitions of several seconds or longer as immediate visual feedback of the acquisition is provided prior to the long-duration acquisition being completed.   Teledyne LeCroy provides Roll Mode capability at sample rates up to 5 MS/s - 10 times (or more) faster than competitors -  for acquisition durations of 1 second or longer.  Additionally, Multi-Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;28&quot; href=&quot;javascript:void(0)&quot;>(28)</a>, Vertical Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;51&quot; href=&quot;javascript:void(0)&quot;>(51)</a>, math functions such as Tracks/Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a> and many measurement parameters may be simultaneously applied to the Roll Mode acquisition, further enhancing understanding of low-speed events as they happen.  ",
                    "unique": "false",
                    "invent": "false"
                },
                {
                    "number": "0",
                    "description": "col10"
                },
                {
                    "number": "5",
                    "name": "Color Overlays",
                    "icon": "icon5.png",
                    "category": "analyze",
                    "description": "Intuitive, semi-transparent color-coded overlays are included as part of every serial decoder software option (more than 20 are available).  These color-coded overlays enhance understanding of the decoded physical layer serial data signal by clearly identifying the beginning and ending of the relevant portions of the protocol layer, such as ID, DATA, CRC, R/W, etc.  Additionally, certain protocol errors are highlighted in red to raise awareness of their presence.  The color-coded overlays gracefully compact on long acquisitions to avoid display clutter, but expand again in a Zoom trace to allow quick understanding of the protocol behaviors.  ",
                    "unique": "true",
                    "invent": "true"
                },
                {
                    "number": "6",
                    "name": "Measure Gate",
                    "icon": "icon6.png",
                    "category": "analyze",
                    "description": "Clock and timing jitter measurements for some JEDEC standards historically indicated the quantity of measurements to include in the measurement, e.g., 10,000 measurements for cycle-cycle, period, and half-period jitter.   Measure gate in the JITKIT software option provides a simple and intuitive method to define the measurement area and gate all of the measurement results to the specified area for validation to the standard or to understand worst-case performance areas for correlation to causal events.  Measure Gates may also be applied to any measurement parameter to limit the measurement area.",
                    "unique": "false",
                    "invent": "false"
                },
                {
                    "number": "7",
                    "name": "Eye Diagrams",
                    "icon": "icon7.png",
                    "category": "analyze",
                    "description": "Serial data jitter is most intuitively understood when displayed as logical 1s or 0s with corresponding transitions overlayed as a persisted waveform that resembles an open human eye.  Teledyne LeCroy invented the modern Eye Diagram as utilized in a real-time oscilloscope by combining long acquisition memory, software clock recovery (using a user-definable software PLL) and advanced techniques to mathematically &quot;slice&quot; and overlay the bits to resemble a traditional sampling (equivalent-time) oscilloscope and hardware clock-recovery persisted eye diagram, but without the additive effects of trigger jitter.   Teledyne LeCroy&apos;s Eye Diagram capability is available in numerous software options and works with very low-speed to very high-speed and continuous NRZ or PAM-4 signals see PAM-4 Analysis<a onclick=&quot;updateModal(this)&quot; data-id=&quot;16&quot; href=&quot;javascript:void(0)&quot;>(16)</a>), or bursted NRZ signals (e.g. using DDR Multi-Eye View <a onclick=&quot;updateModal(this)&quot; data-id=&quot;41&quot; href=&quot;javascript:void(0)&quot;>(41)</a> or the DDR Debug Toolkit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a>, or SPI, UART, CAN, FlexRay, ARINC, etc. using the various serial bus TDME or TDMP options for each low-speed serial protocol).   The Eye Diagram can be rendered in various analog or color-intensified views, and is highly-optimized for extremely fast display.  Some options permit concurrent numerical jitter calculations and Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a> for serial data or DDR Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;42&quot; href=&quot;javascript:void(0)&quot;>(42)</a> deconvolution, and further post-processing of the Eye Diagram data to provide subjective measurements or additional views of the quality of the Eye Diagram, such as Bathtub Curve <a onclick=&quot;updateModal(this)&quot; data-id=&quot;38&quot; href=&quot;javascript:void(0)&quot;>(38)</a>, IsoBER <a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a>, DDj and ISI calculations and views as described in Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>, vertical Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a>, views.   More advanced options (e.g. SDAIII) provide a variety of eye parameters for height, levels, width, extinction ratio, etc. with selectable slide width. ",
                    "unique": "false",
                    "invent": "true"
                }, {
                    "number": "8",
                    "name": "Multi-Lane",
                    "icon": "icon8.png",
                    "category": "analyze",
                    "description": "As serial data speeds have increased, parallel serial data has become more common, with multiple serial data &quot;lanes&quot; achieving higher speeds, with 4 to 16 lanes commonly used.   These lanes may electrically interfere with each other.  Therefore, it is highly desired  to analyze multiple serial data lanes simultaneously.  Teledyne LeCroy's SDAIII Serial Data Analysis toolset permits up to 4 active lanes and one reference lane to be analyzed simultaneously using the full toolsets, such as Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a>, Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a> jitter decomposition, vertical Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a> views, and various analytical jitter views, such as Bathtub Curve <a onclick=&quot;updateModal(this)&quot; data-id=&quot;38&quot; href=&quot;javascript:void(0)&quot;>(38)</a>, IsoBER <a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a>, Rj + BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a>, DDj, ISI and Pj calculations and views as described in Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>.  Multi-lane capability is further enhanced with the use of a LabMaster oscilloscope, which provides >4 high-bandwidth input channels at bandwidths suitable for commonly deployed 10-32 Gb/s serial data signals, or even faster data rates (e.g., 56 Gb/s or higher).  ",
                    "unique": "true",
                    "invent": "true"
                }, {
                    "number": "9",
                    "name": "EMC Pulse",
                    "icon": "icon9.png",
                    "category": "analyze",
                    "description": "EMC and ESD pulse measurements require specialized measurement algorithms to meet the IEC requirements - standard  IEEE-defined rise and fall time, width and other parameters will not meet the requirement.  Teledyne LeCroy provides enhanced capability for rise/fall time and width parameters to set the measurement algorithms for base-top (absolute or percent), peak-peak (percent), 0V-maximum (percent) or 0V-minimum (percent).  Additionally, specialized measurement parameters for EMC level-at-pulse and EMC time-to-halflife are also included.  Measurement filters may be used to limit the number of pulses reported in the measurement parameter, or gate the measurement to individual pulses in the acquisition.  HD4096 High Definition Technology <a onclick=&quot;updateModal(this)&quot; data-id=&quot;48&quot; href=&quot;javascript:void(0)&quot;>(48)</a> is especially helpful for resolving EMC pulses with high precision and confidence. Additionally, most Teledyne LeCroy oscilloscopes contain high sample rate (10x bandwidth), long acquisition memories, and <a onclick=&quot;updateModal(this)&quot; data-id=&quot;12&quot; href=&quot;javascript:void<a onclick=&quot;updateModal(this)&quot; data-id=&quot;0&quot; href=&quot;javascript:void(0)&quot;>(0)</a>&quot;><a onclick=&quot;updateModal(this)&quot; data-id=&quot;12&quot; href=&quot;javascript:void<a onclick=&quot;updateModal(this)&quot; data-id=&quot;0&quot; href=&quot;javascript:void(0)&quot;>(0)</a>&quot;>Sequence Mode <a onclick=&quot;updateModal(this)&quot; data-id=&quot;12&quot; href=&quot;javascript:void(0)&quot;>(12)</a></a></a> acquisition mode - ideal for capturing many bursted pulses at high speeds but separated by long time durations. ",
                    "unique": "true",
                    "invent": "true"
                },
                {
                    "number": "10",
                    "name": "Email on Action",
                    "icon": "icon10.png",
                    "category": "document",
                    "description": "Teledyne LeCroy oscilloscopes can be networked and configured to send an email to any user-defined email address.  The email contains an oscilloscope or desktop screen image (or both), and can be a Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a> from a Mask Test <a onclick=&quot;updateModal(this)&quot; data-id=&quot;33&quot; href=&quot;javascript:void(0)&quot;>(33)</a> or Boolean Compare <a onclick=&quot;updateModal(this)&quot; data-id=&quot;78&quot; href=&quot;javascript:void(0)&quot;>(78)</a>. This makes it simple and easy to remotely monitor your own or someone else's test and be alerted when an pass, fail or other abnormal result is obtained if the expectation is that testing will require a long period of time to uncover the result.  ",
                    "unique": "false",
                    "invent": "false"
                }
            ],
                "row3": [
                    {
                        "number": "11",
                        "name": "Multi-, Cascade",
                        "icon": "icon11.png",
                        "category": "capture",
                        "description": "A variety of Multi-state triggers are included in most Teledyne LeCroy oscilloscopes.  Qualified A+B allows creation of a trigger defined by the simultaneous occurrence of a pair of events.  Qualified First triggers on multiple events by arming the trigger on the &quot;A&quot; event and triggering repeatedly on a &quot;B&quot; event once the &quot;A&quot; condition has been satisfied once.  Cascade permits up to four stages to be defined with up to three stages as Cascaded arming events.  ",
                        "unique": "false",
                        "invent": "false"
                    },
                    {
                        "number": "12",
                        "name": "Sequence Mode",
                        "icon": "icon12.png",
                        "category": "capture",
                        "description": "Sequence acquisition mode captures only the signals of interest - defined by the trigger condition - and ignores the &quot;dead-time&quot; between triggered events.  Thousands of triggered acquisitions (segments) can be captured in one sequence mode acquisition with assurance that no events of interest (that satisfy the trigger condition) are missed for the duration of the sequence mode capture time.  In addition, for each segment, timestamps are stored along with intersegment time, so that the exact timing with nanosecond resolution is available to the user for each segment acquired.  Sequence mode capture time is a function of the capture time for each individual event and the overall capture time capability of the oscilloscope (which is a function of the installed acquisition memory and the timebase setting of the oscilloscope), and a single sequence mode acquisition can last for seconds or days.  Once complete, a variety of Segment <a onclick=&quot;updateModal(this)&quot; data-id=&quot;27&quot; href=&quot;javascript:void(0)&quot;>(27)</a> display modes enhances understanding of the acquired data.  The Segment math function makes it possible to quickly scroll through all of the available segments, and post-process them with math functions or measurement parameters, singly or in groups of N segments, so as to gain further understanding of each segments behavior.  ",
                        "unique": "false",
                        "invent": "true"
                    },
                    {
                        "number": "0",
                        "description": "col10"
                    },
                    {
                        "number": "13",
                        "name": "Protocol Table",
                        "icon": "icon13.png",
                        "category": "analyze",
                        "description": "Serial decoder options include a Protocol Table, similar to that found in a protocol analyzer, for convenient viewing of the decoded serial data (in addition to the Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a>).  The Protocol Table is user-configurable for columns and rows, and intuitively summarizes protocol error behaviors, and can be easily exported as an Excel document.  Search & Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;36&quot; href=&quot;javascript:void(0)&quot;>(36)</a> supports touching a table row to create a Zoom waveform showing the physical layer acquired signal and color-coded overlay so that understanding can be enhanced.   ProtoSync <a onclick=&quot;updateModal(this)&quot; data-id=&quot;102&quot; href=&quot;javascript:void(0)&quot;>(102)</a> (for supported protocols) augments the Protocol Table even further.",
                        "unique": "false",
                        "invent": "true"
                    },
                    {
                        "number": "14",
                        "name": "Jitter Overlay",
                        "icon": "icon14.png",
                        "category": "analyze",
                        "description": "With an analog oscilloscope, jitter was viewed as the (horizontal) width of a persisted trace at the trigger location, with the maximum width representing the peak-peak jitter for that number of acquisitions.  This provided a crude but intuitive way to understand the jitter.  Jitter Overlay improves on this technique by providing a software determination of edge placement (to eliminate the impact of oscilloscope hardware trigger jitter), a phosphor-like display of overlaid edges, and selection to view jitter activity as period, half period, cycle-cycle, time interval error (TIE), setup, hold, etc.  ",
                        "unique": "true",
                        "invent": "true"
                    },
                    {
                        "number": "15",
                        "name": "Tj, Rj, Dj",
                        "icon": "icon15.png",
                        "category": "analyze",
                        "description": "Serial data standards require that jitter be understood as Random (Rj) and Deterministic (Dj) components that add to a Total Jitter (Tj) value that would accurately represent expected performance in real-world systems.  Teledyne LeCroy's SDAIII Serial Data Analysis package performs this complex jitter decomposition and Tj calculation.  Clock recovery is accomplished in software to eliminate hardware trigger jitter, with user-selectable PLL including custom 2 pole PLL filter settings.  Rj method is user-definable to use one of three dual-dirac models - Spectral Rj Direct, Spectral Rj+Dj CDF Fit, or NQ-Scale - depending on user requirements or test specifications.  Bit error rate (BER) for Tj calculation is user-definable to 10e-21.  All decomposed jitter values may be displayed in time or unit intervals.   As measured time interval error (TIE) values may also be displayed along with associated TIE Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a>, TIE Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a>, and TIE Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a> views.  Eye Width can be calculated and displayed as one serial data unit interval minus Tj at user-selectable BER.  ",
                        "unique": "false",
                        "invent": "true"
                    }, {
                        "number": "16",
                        "name": "PAM-4 Analysis",
                        "icon": "icon16.png",
                        "category": "analyze",
                        "description": "PAM-4 represents a new step in the evolution of serial data signaling formats, overcoming some fundamental limitations of traditional NRZ signaling. But with new signal types come new measurement needs. Teledyne LeCroy’s PAM-4 analysis package meets these needs by leveraging industry-leading Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a> and the full set of  Jitter and Noise Measurements and Analysis and Jitter Simulation tools - see Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a>, Rj + BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a>, Bathtub Curve <a onclick=&quot;updateModal(this)&quot; data-id=&quot;38&quot; href=&quot;javascript:void(0)&quot;>(38)</a>, DDj+ISI Views and Pj Spectral Views (see Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>), IsoBER<a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a>, Jitter Simulation <a onclick=&quot;updateModal(this)&quot; data-id=&quot;83&quot; href=&quot;javascript:void(0)&quot;>(83)</a> and Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a> - to fully characterize PAM-4 signals.  Additionally, The PAM-4 Signal Analysis package performs a complete specialized analysis of PAM-4 waveforms, and it analyzes the signals, creates eye diagrams, measures eye closure in voltage and time, and predicts closure as a function of BER. It is fully integrated into Teledyne LeCroy’s sophisticated MAUI user interface, allowing for advanced capabilities like channel emulation and de-embedding using EyeDr/VP <a onclick=&quot;updateModal(this)&quot; data-id=&quot;105&quot; href=&quot;javascript:void(0)&quot;>(105)</a> capabilities.  PAM4 Eye diagrams can be displayed as full eye, or just upper, middle, or lower eye, and these can be viewed simultaneously using Multi-Lane <a onclick=&quot;updateModal(this)&quot; data-id=&quot;8&quot; href=&quot;javascript:void(0)&quot;>(8)</a> LaneScape views. ",
                        "unique": "false",
                        "invent": "true"
                    },
                    {
                        "number": "17-22",
                        "name": "Motor + Power",
                        "icon": "icon17-22.png",
                        "category": "motor",
                        "description": "A full-range of toolsets is provided for measuring 1-phase to 3-phase <a onclick=&quot;updateModal(this)&quot; data-id=&quot;20&quot; href=&quot;javascript:void(0)&quot;>(20)</a> AC line input to pulse-width modulated (PWM) power supply, inverter and drive outputs.  Electrical power quantities, such as real (P), apparent (S) and reactive (Q)  power and power factor/phase angle can be measured along with per-cycle RMS voltages and currents.  Both Static+Dynamic <a onclick=&quot;updateModal(this)&quot; data-id=&quot;21&quot; href=&quot;javascript:void(0)&quot;>(21)</a> power quantities can be measured over long durations of time, and Zoom+Gate <a onclick=&quot;updateModal(this)&quot; data-id=&quot;22&quot; href=&quot;javascript:void(0)&quot;>(22)</a> can be applied to limit the measurements to the specific area of interest.  Harmonics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;19&quot; href=&quot;javascript:void(0)&quot;>(19)</a> values by order can be measured and compared to a standard.  Power semiconductor Device Loss <a onclick=&quot;updateModal(this)&quot; data-id=&quot;17&quot; href=&quot;javascript:void(0)&quot;>(17)</a> during switching and conduction events, and on resistance for Si, SiC and GaN devices can be performed.  Mechanical (motor shaft) power can be calculated as well through the use of torque sensors and a wide-variety of speed sensors.  Switch-mode power supply Control Loop <a onclick=&quot;updateModal(this)&quot; data-id=&quot;18&quot; href=&quot;javascript:void(0)&quot;>(18)</a> analysis can be performed.  ",
                        "unique": "false",
                        "invent": "false"
                    },
                    {
                        "number": "23",
                        "name": "Compliance",
                        "icon": "icon23.png",
                        "category": "document",
                        "description": "Higher-speed serial data standards have comprehensive specifications that describe required performance characteristics.  Testing to these standards requires either a detailed knowledge of the test requirement or a standardized compliance test program, such as Teledyne LeCroy's QualiPHY&trade;.  QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a> supports a large number of standards using a scripted test framework to control the oscilloscope (and in some cases, other devices as well) and automate the test setup and test.  In the event of failures, QualiPHY can be exited and tests can be performed manually to debug root-cause of failures.  A test report can be automatically generated at the conclusion of test to indicate pass/fail in each relevant area, with the option of including supporting oscilloscope screen displays in the report.  ",
                        "unique": "false",
                        "invent": "false"
                    }
                ],
                    "row4": [
                        {
                            "number": "24",
                            "name": "Analog+Digital",
                            "icon": "icon24.png",
                            "category": "capture",
                            "description": "Combined analog+digital triggering permits complex logical patterns for various analog inputs - up to 8 analog inputs (using selected oscilloscopes with more than  4 channels <a onclick=&quot;updateModal(this)&quot; data-id=&quot;25&quot; href=&quot;javascript:void(0)&quot;>(25)</a>) - can be defined as a trigger condition, with logic conditions amongst all of the channels definable as AND, NAND, OR, or NOR.   Oscilloscopes equipped with digital inputs (MSOs) can combine analog patterns with up to 36-bit digital patterns at 500 MHz digital clock rate on some models.   The High-speed Digital Analyzers (HDA125) provides support for up to 18-bit digital patterns at 12.5 GS/s sample rate, and can be combined with four analog inputs on supported oscilloscopes for high-speed, complex analog+digital pattern triggering.  ",
                            "unique": "false",
                            "invent": "false"
                        }, {
                            "number": "25",
                            "name": "4 to 80 Channels",
                            "icon": "icon25.png",
                            "category": "capture",
                            "description": "At the world's highest real-time bandwidths, massive numbers of channels can be connected together using modular acquisition systems and ChannelSync&trade; architecture.  This is highly useful for high-speed Multi-Lane <a onclick=&quot;updateModal(this)&quot; data-id=&quot;8&quot; href=&quot;javascript:void(0)&quot;>(8)</a> electrical serial data validation, complex DP-QPSK or DP-16-QAM Optical (63-67) analysis, or optical coherent MIMO or space-division multiplexed signals.  At 350 MHz to 1 GHz bandwidths, the HDO8000 Series provides 8 channels in a single oscilloscope package for twice the number of channels - ideal for three-phase Motor + Power (17-22) power conversion analysis, deeply embedded systems, power integrity, digital power management, and physics applications. The SAM40 provides up to 24 additional channels at 40 kHz bandwidth - perfect for analyzing sensor data in conjunction with higher bandwidth control or power system events.",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "26",
                            "name": "Multi-Grid",
                            "icon": "icon26.png",
                            "category": "view",
                            "description": "Multi-Grid displays enable multiple full-resolution vertical grids in one oscilloscope display.   This maximizes the oscilloscope (vertical) dynamic range and optimizes the signal fidelity of the acquired waveform because the maximum resolution of the oscilloscope ADC is being used for the vertical digitization.   Whenever amplitude-related measurements are made on multiple waveforms and the accuracy of the measurement is critical to debug or validation, or Vertical Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;51&quot; href=&quot;javascript:void(0)&quot;>(51)</a> is required, multi-grid is the preferred operating mode. For instance, in &quot;Quad&quot; grid mode, there are four full-width grids each one fourth of the display in height.  Yet each one of these one fourth grids will display the full oscilloscope vertical resolution (8-bit or 12-bit Resolution <a onclick=&quot;updateModal(this)&quot; data-id=&quot;48&quot; href=&quot;javascript:void(0)&quot;>(48)</a>, depending on model).  Thus, it is easy to view all of the signal details without sacrificing vertical resolution by decreasing volts/div on multiple acquired waveforms as would be necessary on a conventional single-grid display.   Drag and Drop <a onclick=&quot;updateModal(this)&quot; data-id=&quot;49&quot; href=&quot;javascript:void(0)&quot;>(49)</a> is useful for locating the different acquired waveforms on the selected grids.  ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "27",
                            "name": "Segment",
                            "icon": "icon27.png",
                            "category": "view",
                            "description": "Sequence Mode <a onclick=&quot;updateModal(this)&quot; data-id=&quot;12&quot; href=&quot;javascript:void(0)&quot;>(12)</a> acquisitions capture multiple Segments with multiple triggers, but store all of the segments in one acquisition memory with no dead-time between Segments.  The segments may then be displayed in a variety of display views, with or without timestamps, to enable understanding.  Supported display views include Adjacent, Overlay, Waterfall, Perspective, and Mosaic.  Using Multi-Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;28&quot; href=&quot;javascript:void(0)&quot;>(28)</a> permits quick selection and zooming of a particular Segment, plus additional time-correlated waveforms, to quickly understand causal relationships.  Segment is also supported as a Math function, with selection of a particular Segment number for additional processing, and the Segment can be easily incremented through front panel Zoom or Math Segment control to apply additional math processing on the selected Segment and view the result. ",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "28",
                            "name": "Multi-Zoom",
                            "icon": "icon28.png",
                            "category": "view",
                            "description": "Push the front panel Zoom button and zoom traces are created of all channels, and all zoom traces are time-synchronized by default.  If desired, the time-synchronization may be disabled, or disabled, changed, and re-enabled to perform complex, repetitive analysis, e.g. failure analysis on rotating media.  Multi-Zoom links well to Sequence Mode <a onclick=&quot;updateModal(this)&quot; data-id=&quot;12&quot; href=&quot;javascript:void(0)&quot;>(12)</a> acquisitions for quickly zooming to one (or more) particular Segment <a onclick=&quot;updateModal(this)&quot; data-id=&quot;27&quot; href=&quot;javascript:void(0)&quot;>(27)</a> displays.  Up to 12 levels of zooms within zooms are supported in some oscilloscopes.  ",
                            "unique": "false",
                            "invent": "true"
                        }, {
                            "number": "29",
                            "name": "All Instance",
                            "icon": "icon29.png",
                            "category": "measure",
                            "description": "Most Teledyne LeCroy Measurement Parameters are All Instance in that every occurrence of the measurement parameter is calculated and returned as a result.  This permits simple and intuitive further analysis through a simple Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> table, a Histicon/Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a> function display of the measurement parameter values, and Tracks/Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a> of each value versus time and time-correlated to other acquired waveforms, or analysis of parameter values versus accumulated time.  There is no limit on the number of instances that can be calculated in a given analysis acquisition - unique to Teledyne LeCroy.",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "30",
                            "name": "Statistics",
                            "icon": "icon30.png",
                            "category": "measure",
                            "description": "Measurement Statistics permit accumulation of billions of events as a measurement set with calculations for minimum, maximum, mean, standard deviation,and total number accumulated returned in a Statistics table.  This adds tremendous flexibility to the oscilloscope for worst-case analysis or anomaly detection as the Statistics table can accumulate large numbers of values from one acquisition using All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> capability, and then multiple the numbers of values accumulated through additional acquisitions.  ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "31",
                            "name": "Full Memory FFT",
                            "icon": "icon31.png",
                            "category": "math",
                            "description": "FFTs may calculated on full acquisition or analysis memories (500 Mpts maximum analysis).  Up to 256 Mpts, calculation speed is enhanced through the use of Intel performance primitives, and beyond through proprietary techniques.  The benefits of using long acquisition memory for FFT calculation is that FFT frequency resolution increases inversely with acquisition time, and FFT span increases sample rate.  Long memory FFTs permit both long span and high resolution in the FFT calculation.  Seven different output types, five windows and two different calculation algorithms are supported.   FFTs are commonly used with the Track function (see Tracks /Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>)Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a> function to create Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a> using Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a>.   Full memory FFTs are also employed in the Spectrum Analyzer option, available on many Teledyne LeCroy oscilloscopes.  ",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "32",
                            "name": "Digital Filters",
                            "icon": "icon32.png",
                            "category": "math",
                            "description": "A variety of Infinite Impulse Response (IIR) and Finite Impulese Response (FIR) filters may be created with low-pass, high-pass, band-pass, band-stop, raised-cosine, raised-root cosine and gaussian characteristics, or a custom filter may be designed.  Boxcar filtering provides ability to create a local running average filter of specified length.  Simple noise-reduction and smoothing is accomplished with enhanced resolution (ERES) filters, and simple Sinx/x filters may be applied to provide increased sample points through interpolation.   Some of the filters are available as a post-processing step directly in the Channel setup dialog, making application simple and intuitive. ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "33",
                            "name": "Mask Test",
                            "icon": "icon33.png",
                            "category": "analyze",
                            "description": "Pass/Fail mask is particularly useful for comparing newly acquired signals to a previously acquired &quot;golden standard&quot; waveform.  A mask defines an area of the grid against which a source Channel, Zoom, or Math trace is compared. Test conditions are associated with the mask, defining how the waveform is to be compared to the masked area (e.g., some/all values fall within, some/all values fall outside), and a Pass or Fail result is returned indicating the condition was found to be true or false. Pass/Fail testing can be done using a pre-defined mask or a mask created from your actual waveform, with vertical and horizontal tolerances that you define. Some industry standard masks used for compliance testing are included with the oscilloscope software. The mask test can be confined to just a portion of the trace by the use of a Measure Gate <a onclick=&quot;updateModal(this)&quot; data-id=&quot;6&quot; href=&quot;javascript:void(0)&quot;>(6)</a>.  Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a> can be defined as a result of the mask test, and the mask test result can be combined with another mask or parameter pass/fail result in a Boolean Compare <a onclick=&quot;updateModal(this)&quot; data-id=&quot;78&quot; href=&quot;javascript:void(0)&quot;>(78)</a>.",
                            "unique": "false",
                            "invent": "false"
                        },
                        {
                            "number": "34",
                            "name": "TriggerScan",
                            "icon": "icon34.png",
                            "category": "analyze",
                            "description": "TriggerScan uses high-speed hardware triggering capability with persistence displays to capture only the signals of interest and provide answers up to 100X faster than other methods. Traditional fast display update modes work best on frequent events occurring on slow edge rates while TriggerScan excels in finding infrequent events on fast edge rates.  Since hardware triggering is used to capture only the elusive events, TriggerScan is more effective at finding anomalies quickly compared to simple display technologies. A built-in automated Trigger Trainer analyzes the waveforms, identifies normal behavior, and then sets up a large set of rare event smart trigger setups that target abnormal behavior. ",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "35",
                            "name": "Symbol",
                            "icon": "icon35.png",
                            "category": "analyze",
                            "description": "Encoding schemes organize serial data bits into symbols that are then assigned deeper meaning, e.g. 8b/10b encoding utilizes a defined sequence of 10 bits to define a K28.5 symbol.  Teledyne LeCroy offers serial data decoders that decode 8b/10b and 64b/66b into Symbols and provide transparent Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a> and Protocol Table <a onclick=&quot;updateModal(this)&quot; data-id=&quot;13&quot; href=&quot;javascript:void(0)&quot;>(13)</a> with the decoded Symbol information.  This can be combined with a Serial Data <a onclick=&quot;updateModal(this)&quot; data-id=&quot;47&quot; href=&quot;javascript:void(0)&quot;>(47)</a> Trigger to enhance debug and validation. ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "36",
                            "name": "Search & Zoom",
                            "icon": "icon36.png",
                            "category": "analyze",
                            "description": "Search & Zoom enhances the Symbol <a onclick=&quot;updateModal(this)&quot; data-id=&quot;35&quot; href=&quot;javascript:void(0)&quot;>(35)</a>, Protocol Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;58&quot; href=&quot;javascript:void(0)&quot;>(58)</a>, Application Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;80&quot; href=&quot;javascript:void(0)&quot;>(80)</a> and ProtoSync <a onclick=&quot;updateModal(this)&quot; data-id=&quot;102&quot; href=&quot;javascript:void(0)&quot;>(102)</a> serial data decode and Protocol Table <a onclick=&quot;updateModal(this)&quot; data-id=&quot;13&quot; href=&quot;javascript:void(0)&quot;>(13)</a> capabilities.  Simply touch a protocol table row and a zoom trace is created and appropriately scaled for easy viewing of the transparent Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a> decode information.  Use the Protocol Table to location information of interest, and then Search&Zoom to view the physical layer signal, or vice versa.  ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "37",
                            "name": "Jitter Track",
                            "icon": "icon37.png",
                            "category": "analyze",
                            "description": "Measurement Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> provide a partial story of jitter behavior.  A Jitter Track provides a fuller picture with a time-correlated waveform that displays jitter values on the vertical axis  vs. time on the horizontal axis, with the time-scale identical to that of other acquired or processed waveforms.  This is a unique and intuitive view of the jitter measurement behavior, and it makes it simple to correlate high measured jitter values with other events, such as power supply noise, crosstalk, or other disturbing events.  Jitter Tracks may be further processed by a Full Memory FFT <a onclick=&quot;updateModal(this)&quot; data-id=&quot;31&quot; href=&quot;javascript:void(0)&quot;>(31)</a> to create a resulting Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a>.  ",
                            "unique": "false",
                            "invent": "true"
                        },
                        {
                            "number": "38",
                            "name": "Bathtub Curve",
                            "icon": "icon38.png",
                            "category": "analyze",
                            "description": "The Bathtub Curve estimates bit error rate (BER) and provides an indication for timing margin in the system, and appears as a cross-section of a bathtub when the BER is very low.  It is calculated from the probability distribution function (PDF) for the serial data transitions at a user-defined crossing level (either in percent or absolute voltage).  A Cumulative Distribution Function (CDF) and a Q-Fit as described in Rj + BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a> can be displayed with the Bathtub Curve for better understanding of system behaviors.  An IsoBER <a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a> provides a different way of understanding the Bathtub Curve information by displaying Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a> extrapolated to a user-defined BER.  ",
                            "unique": "false",
                            "invent": "true"
                        }, {
                            "number": "39",
                            "name": "Rj + BUj Views",
                            "icon": "icon39.png",
                            "category": "analyze",
                            "description": "Random Jitter (Rj) plus Bounded Uncorrelated Jitter (BUj) is equal to the Total Jitter (Tj) &quot;stripped&quot; of all of the Data Dependent Jitter (DDj).  Teledyne LeCroy provides ability to calculate Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a> and then view the Rj+BUj with a variety of views to understand the non-data dependent nature of the jitter.  Specialized Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a> views are provided to understand the distribution of Rj+BUj values, a specialized Q-Fit Rj+BUj Histogram view in which guassian tails result in straight lines whose slope is equal to 1/Rj, Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a> views are provided to understand how the Rj+BUj values are changing over time and make it easy to correlate the changing behaviors to other acquired and time-correlated signals, and Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a> views aid in understanding of root-cause.  The Jitter Spectrum view identifies spectral peaks, and displays the peak threshold to aid in understanding.  Additionally, a Cumulative Distribution Function (CDF) for the extrapolated Rj+BUj histogram convolved with the DDj Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a> view described in Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>. Teledyne LeCroy is the only company to provide all of these Rj+BUj analysis views. ",
                            "unique": "true",
                            "invent": "true"
                        },
                        {
                            "number": "40-45",
                            "name": "DDR Analysis",
                            "icon": "icon40-45.png",
                            "category": "ddr",
                            "description": "Teledyne LeCroy provides a comprehensive set of tools for measurement and analysis of DDR clock and timing behaviors for nearly all varieties of DDR and LPDDR, ranging from QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a> compliance validation of DDR <a onclick=&quot;updateModal(this)&quot; data-id=&quot;108&quot; href=&quot;javascript:void(0)&quot;>(108)</a> standards to various toolsets used for DDR debug, including R/W Separation <a onclick=&quot;updateModal(this)&quot; data-id=&quot;40&quot; href=&quot;javascript:void(0)&quot;>(40)</a>, Multi-Eye View <a onclick=&quot;updateModal(this)&quot; data-id=&quot;41&quot; href=&quot;javascript:void(0)&quot;>(41)</a>, DDR Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;42&quot; href=&quot;javascript:void(0)&quot;>(42)</a> calculation and decomposition, Virtual Probe <a onclick=&quot;updateModal(this)&quot; data-id=&quot;44&quot; href=&quot;javascript:void(0)&quot;>(44)</a> and the complete DDR Debug Tookit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a> to intuitively understand and debug DDR behaviors.  ",
                            "unique": "false",
                            "invent": "false"
                        },
                        {
                            "number": "46",
                            "name": "WaveStudio",
                            "icon": "icon46.png",
                            "category": "document",
                            "description": "WaveStudio is a free tool that provides a fast and easy way to analyze acquired waveforms offline, or remotely control an oscilloscope from your desktop. WaveStudio works in one of two ways, either off-line or connected to the oscilloscope. When off-line, simply open a trace file from a USB stick or e-mail attachment from the oscilloscope's save waveform function. When connected to the oscilloscope, simply click on the display capture option to load a screen shot, or click a channel to automatically load in the trace information. WaveStudio provides x and y axis cursors for quick timing and amplitude measurements. For more precise values, use the built in 21 automatic measurements for accurate and quick results. WaveStudio makes it easy to document your work, whether the oscilloscope is next to you or is many miles away. LabNotebook <a onclick=&quot;updateModal(this)&quot; data-id=&quot;90&quot; href=&quot;javascript:void(0)&quot;>(90)</a> files and screenshots can be saved for future analysis.",
                            "unique": "false",
                            "invent": "false"
                        }
                    ],
                        "row5": [
                            {
                                "number": "47",
                                "name": "Serial Data",
                                "icon": "icon47.png",
                                "category": "capture",
                                "description": "A variety of serial data triggers are available, including more than 15 for low-speed serial protocols for embedded, automotive, military/avionic, and serial audio, such as I2C, SPI, UART, RS-232, CAN, LIN, FlexRay, ARINC, etc.  High-speed serial triggering is supported at the bit or symbol layer for a variety of standards, up to 14.1 Gb/s speeds.  In general, these serial triggers provide higher levels of trigger flexibility (e.g., for I2C, supported is provided for conditional data triggering and long EEPROM write/read lengths, in CAN support is provided for application layer triggering using customer .dbc files) than those available from competitors.  When combined with the various Serial Message Analysis Timing Parameters <a onclick=&quot;updateModal(this)&quot; data-id=&quot;81&quot; href=&quot;javascript:void(0)&quot;>(81)</a>, it is quick and easy to measure hundreds or thousands of cause-effect behaviors and latencies between two serial data messages or a combination of a serial data message and an analog signal.  ",
                                "unique": "true",
                                "invent": "false"
                            },
                            {
                                "number": "48",
                                "name": "High Definition Technology",
                                "icon": "icon48.png",
                                "category": "capture",
                                "description": "High Definition (HD) technology is especially helpful for wide dynamic range signals in which a fullscale signal must be acquired while at the same time very small amplitude signal details must be analyzed. High definition acquisitions combined with Vertical Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;51&quot; href=&quot;javascript:void(0)&quot;>(51)</a> and horizontal Multi-Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;28&quot; href=&quot;javascript:void(0)&quot;>(28)</a> help users obtain unparalleled insight to system behaviors and problems. High definition oscilloscopes provide better ability to assess corner cases and design margins, perform root cause analysis, and create the best possible solution for any discovered design issue.<br><strong>HD4096 12-bit Resolution</strong><br>HD4096 high definition technology provides 12 bits of vertical resolution (4096 vertical levels) compared to conventional 8-bit oscilloscopes (256 vertical levels) and low noise for uncompromised measurement performance – 16x closer to perfect, dramatically crisper and cleaner, and displayed more accurately. HD4096 is “12 bits all the time”, in contrast to oscilloscopes from other manufacturers that reduce some combination of bandwidth, sample rate, and channel count to achieve high resolution some of the time. Reference our white paper “Comparing High Resolution Oscilloscope Design Approaches” for more details.",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "49",
                                "name": "Drag and Drop",
                                "icon": "icon49.png",
                                "category": "view",
                                "description": "Teledyne LeCroy provides the best and most intuitive user-interface experience, with many actions just one touch, drag, drop, flick or pinch away, avoiding the need to dig through many layers of setup dialogs and pop-ups.  Drag-and-drop integrates with the touch screen interface to make it easy to set up your waveform display exactly the way you want so that it is most intuitive to you and communicates in the best possible way in screen images.  Drag-and-drop is especially helpful in organizing Multi-Grid <a onclick=&quot;updateModal(this)&quot; data-id=&quot;26&quot; href=&quot;javascript:void(0)&quot;>(26)</a> displays, but it is also employed to copy setups, channels, math functions, measurements, etc., change sources for a measurement parameter or math function, or assign cursors to a small zoom trace in a long acquisition.  Traces or measurements can be turned off with one flick, and zooming is optimized with drag to location and pinch to zoom.  ",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "50",
                                "name": "Waveform Histogram",
                                "icon": "icon50.png",
                                "category": "view",
                                "description": "Waveform (or Persistence) Histogram permits a mathematical Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a> to be created and displayed using a vertical or horizontal slice of an oscilloscope display persistence map.  This can simplify and enhance understanding of a simple persisted rising edge display of jitter (for instance) or the overshoot of an amplifier signal.  Unlike the simple display histograms often provided on other oscilloscopes, the Teledyne LeCroy Waveform (Persistence) Histogram can be further post-processed with Histogram parameters, and displayed in any desired location. ",
                                "unique": "false",
                                "invent": "true"
                            },
                            {
                                "number": "51",
                                "name": "Vertical Zoom",
                                "icon": "icon51.png",
                                "category": "view",
                                "description": "Vertical Zoom permits zooming in the vertical (amplitude) axis independent of zoom in the horizontal (time) axis.  Common applications for vertical zoom include Noise Analysis + Crosstalk <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a> and correlation to causal signals, amplifier analysis, signal or power integrity studies, and power semiconductor Device Loss <a onclick=&quot;updateModal(this)&quot; data-id=&quot;17&quot; href=&quot;javascript:void(0)&quot;>(17)</a> analysis.  When combined with a 12-bit Resolution <a onclick=&quot;updateModal(this)&quot; data-id=&quot;48&quot; href=&quot;javascript:void(0)&quot;>(48)</a> oscilloscope, Vertical Zoom provides an excellent alternative to offsetting a signal off-screen and overdriving the oscilloscope front-end to achieve wider dynamic range and high resolution on a signal.  Teledyne LeCroy offers Vertical Zoom standard on most oscilloscope models.  ",
                                "unique": "false",
                                "invent": "true"
                            },
                            {
                                "number": "52",
                                "name": "Parameter Math",
                                "icon": "icon52.png",
                                "category": "measure",
                                "description": "Parameter Math provides capability to add, subtract, multiple and divide two measurement parameters, and multiple operations can be cascaded to perform complex calculations.  Rescaling of values may also be performed, with automatic calculation of correct units or manual override to user-defined units.  Parameter math can utilize standard Teledyne LeCroy measurement parameters, user-defined Custom Measure <a onclick=&quot;updateModal(this)&quot; data-id=&quot;74&quot; href=&quot;javascript:void(0)&quot;>(74)</a>, or any combination.  ",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "53",
                                "name": "Parameter Acceptance",
                                "icon": "icon53.png",
                                "category": "measure",
                                "description": "Some Teledyne LeCroy software packages give you the ability to constrain measurement parameters to a vertically or horizontally limited range, or to occurrences gated by a second waveform. Furthermore, both constraints can operate together. This capability enables you to exclude unwanted characteristics from your All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurements. It is much more restrictive than Measure Gate <a onclick=&quot;updateModal(this)&quot; data-id=&quot;6&quot; href=&quot;javascript:void(0)&quot;>(6)</a> which is used only to narrow the span of analysis along the horizontal axis.  Measurements so constrained may then be further analyzed with a Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>, a Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>, a Trend <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a> or used in Parameter Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;52&quot; href=&quot;javascript:void(0)&quot;>(52)</a>.  Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> will contain only the constrained measurement parameter set.   Parameter Acceptance is also referred to as Qualified Parameters or Measure Accept.",
                                "unique": "false",
                                "invent": "false"
                            },
                            {
                                "number": "54",
                                "name": "Tracks / Trends",
                                "icon": "icon54.png",
                                "category": "math",
                                "description": "Tracks provide an intuitive display of the change of a measurement parameter value versus time, with the Track remaining time-correlated to the originally acquired waveforms from which the measurement parameters were derived.  Tracks may utilize standard Teledyne LeCroy measurement parameters, parameters created through Parameter Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;52&quot; href=&quot;javascript:void(0)&quot;>(52)</a> or Custom Measure <a onclick=&quot;updateModal(this)&quot; data-id=&quot;74&quot; href=&quot;javascript:void(0)&quot;>(74)</a>.  Tracks may be further processed by additional math functions to obtain other information, e.g. a Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a>.  Trends provide a means to display a series of measurement parameter values as acquired over a long period of time asynchronous to the acquisition window for real-time acquired waveforms, much like a conventional chart recorder or data logger would provide.  ",
                                "unique": "false",
                                "invent": "true"
                            },
                            {
                                "number": "55",
                                "name": "Processing Web",
                                "icon": "icon55.png",
                                "category": "math",
                                "description": "The Teledyne LeCroy Processing Web is a graphical programming interface for math and measurements in which math and measurements are wired together in a flow diagram.   It permits graphical processor-block view of the post-acquisition processing and setup of analytics performed on the acquired waveforms.   Math, Measure and other processing blocks may be Drag and Drop <a onclick=&quot;updateModal(this)&quot; data-id=&quot;49&quot; href=&quot;javascript:void(0)&quot;>(49)</a> is used to place and link together the math, measurement and other processing blocks in the graphical user-interface.  When using the Processing Web, GUI-related constraints on the number of math or measurement processing steps are removed, and complex processing setups can be deployed.   ",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "56",
                                "name": "Actions",
                                "icon": "icon56.png",
                                "category": "analyze",
                                "description": "Various Actions can be defined for the oscilloscope to perform, based upon a pass-fail query result.  Multiple pass-fail queries can be defined, and the complex result of all the queries can be used to define a set of actions if the complex result is a &quot;pass&quot; or a &quot;fail&quot;.  Actions include Save, Stop Acquisition, Alarm, output a Pulse, or Save a Hardcopy <a onclick=&quot;updateModal(this)&quot; data-id=&quot;2&quot; href=&quot;javascript:void(0)&quot;>(2)</a> or LabNotebook <a onclick=&quot;updateModal(this)&quot; data-id=&quot;90&quot; href=&quot;javascript:void(0)&quot;>(90)</a>.",
                                "unique": "false",
                                "invent": "false"
                            },
                            {
                                "number": "57",
                                "name": "WaveScan",
                                "icon": "icon57.png",
                                "category": "analyze",
                                "description": "WaveScan permits search and scan of an acquired waveform for a non-monotonic event, any measurement parameter value, a runt or various serial data/bus patterns.  The searchable event may be defined as greater than, less than, in a range, outside a range, etc.  or rarest N events.  The found events are identified in a table with select to zoom capability, and the found events can be displayed overlaid, similar to what is provided in Jitter Overlay <a onclick=&quot;updateModal(this)&quot; data-id=&quot;14&quot; href=&quot;javascript:void(0)&quot;>(14)</a>, as a histogram (similar to what is described in Histicon/Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>), or create Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a> when found.  ",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "58",
                                "name": "Protocol Layer",
                                "icon": "icon58.png",
                                "category": "analyze",
                                "description": "Serial data protocols organize serial data symbols and bits into messages that follow a standard structure.  Teledyne LeCroy offers more than 20 different serial data decoders for a variety of low-speed (I2C, I2S, SPI, UART, RS-232, CAN, LIN, FlexRay, SENT, ARINC429, MIL-1553, Manchester, etc.), medium-speed (USB2, USB2-HSIC, 10/100 Base-T Ethernet) and high-speed (SATA, SAS, FibreChannel, PCIe, MPHY, DPHY, UniPro, DigRF3G, DigRFv4, etc.).  Many of the decoders provide unparalleled setup flexibility, e.g. the SPI decoder permits decoding of simplified SPI without chip select signals, I2S decode supports left and right-justified, EIA and TDM, UART-RS232 can be configured in a Message Frame format to accomodate proprietary user protocols based on UART bytes, etc. Serial decoders are commonly combined with a Serial Data <a onclick=&quot;updateModal(this)&quot; data-id=&quot;47&quot; href=&quot;javascript:void(0)&quot;>(47)</a> Trigger to enhance debug and validation. ",
                                "unique": "false",
                                "invent": "false"
                            },
                            {
                                "number": "59",
                                "name": "Bus Parameters",
                                "icon": "icon59.png",
                                "category": "analyze",
                                "description": "Serial Bus Parameters are provided as part of the various serial bus TDME or TDMP and ProtoBus MAG options and can measure various generic bus activities, such as message bit-rate, bus load or number of serial messages in an acquisition.  When combined with the Track capability as described in Tracks/Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>, they provide a convenient method to measure bus activities and correlate unusual bus activities with causal events, e.g. correlate periods of high serial message density with receiving errors, or fluctuations in bit-rates with power supply disturbances.  ",
                                "unique": "false",
                                "invent": "true"
                            },
                            {
                                "number": "60",
                                "name": "Jitter Histogram",
                                "icon": "icon60.png",
                                "category": "analyze",
                                "description": "Measurement Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> provide a partial story of jitter behavior.  A Jitter Histogram provides a fuller picture with a Histogram display of all of the data values so that measurement modality can be observed and used to debug improper behaviors.  A variety of histogram measurements can be applied to objectively measure the histogram mean, peak-peak, mode, full-width half-maximum, etc.  This is a unique and intuitive view of the jitter measurement behavior, and it makes it simple to understand the statistical &quot;story&quot; behind the numerical measurement set.  ",
                                "unique": "false",
                                "invent": "true"
                            }, {
                                "number": "61",
                                "name": "IsoBER",
                                "icon": "icon61.png",
                                "category": "analyze",
                                "description": "IsoBER displays show lines of constant extrapolated bit error rate (BER) within the Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a>, similar to how an isobar shows lines of constant atmospheric pressure on a weather map.  BER settings for &quot;from&quot; and &quot;to&quot; are user-definable, as is BER step size.   If Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a> Crosstalk Eyes are utilized, the IsoBER lines inside the Eye Diagram will match the Crosstalk Eye lines on the inside of the Eye Diagram.  ",
                                "unique": "true",
                                "invent": "true"
                            }, {
                                "number": "62",
                                "name": "Dj Views",
                                "icon": "icon62.png",
                                "category": "analyze",
                                "description": "De-convoluting data-dependent pattern jitter (DDj) from Deterministic Jitter (Dj) is critical to enable understanding of jitter caused by specific digital pattern sequences, and then correlate them to other system activity.  Teledyne LeCroy provides this capability with automatic digital pattern recognition up to PRBS-23 with only a minimum of 10 patterns required for DDj analysis calculations and views, or capability to user-define the specific pattern.  Total DDj and the components of DDj (Duty Cycle Distortion, or DCD, and Intersymbol Interference, or ISI) are then calculated and displayed as measurements.  A variety of DDj views can be displayed, such as a Data Dependent Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a>, DDj Plot (or Data Dependent Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a>), which plots the DDj time-correlated to the extracted digital pattern, and a plot of the found Digital Pattern sequence.  An ISI Plot can also be displayed with user definition of the number of bits included in the ISI plot, and color-coding to specific bit transitions. Periodic Jitter (Pj) is the main component of Bounded Uncorrelated Jitter (BUj) and is the primary remaining Deterministic Jitter (Dj) component of interest once Data Dependent Jitter (DDj) is &quot;stripped&quot; from the Total Jitter (Tj).  To minimize sources of Dj that can lead to a high Tj, Pj must be isolated and understood, and this is accomplished through analysis of the spectral components of the Pj.  Once the spectral components of the Pj are known, they can be correlated to other in-circuit behaviors and eliminated.  Teledyne LeCroy is the only company to provide an ability to isolate and analyze the Pj spectrum using an Inverse FFT of a Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a> of the Rj + BUj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a> calculated waveform (which has already &quot;stripped&quot; out the data dependent jitter (DDj) components).  This inverse FFT also eliminates the baseline components of the spectrum (the threshold level for this elimination may be viewed overlaid on the inverse FFT).  The result - the Pj Inverse FFT - shows only the spectral composition of the Periodic Jitter so that root-cause can be more easily identified and eliminated.",
                                "unique": "true",
                                "invent": "true"
                            },
                            {
                                "number": "63-67",
                                "name": "",
                                "icon": "icon.png",
                                "category": "optical",
                                "description": "",
                                "unique": "false",
                                "invent": "false"
                            },
                            {
                                "number": "68",
                                "name": "LSIB",
                                "icon": "icon68.png",
                                "category": "document",
                                "description": "LeCroy Serial Interface Bus (LSIB) provides direct export of acquired data from the oscilloscope PCIe data transfer bus using a standard PCIe x4 connection to a user-supplied host computer.  Using LSIB, data transfer speeds up to 325 MB/s can be achieved.   Using the same LSIB link, remote commands can be sent back to the oscilloscope from the host PC.  Export mode allows data to be read continuously and stored in remote storage for later retrieval and analysis.  ",
                                "unique": "true",
                                "invent": "true"
                            }
                        ],
                            "row6": [
                                {
                                    "number": "69",
                                    "name": "",
                                    "icon": "icon.png",
                                    "category": "capture",
                                    "description": "",
                                    "unique": "false",
                                    "invent": "false"
                                },
                                {
                                    "number": "70",
                                    "name": "100 GHz / DBI",
                                    "icon": "icon70.png",
                                    "category": "capture",
                                    "description": "Digital Bandwidth Interleave (DBI) was invented by Teledyne LeCroy as a solution to the need to measure performance of next-generation high-bandwidth chipsets with real-time oscilloscopes.  DBI splits the oscilloscope high-bandwidth input signal path into two or more distinct paths and uses RF technology to downconvert the highest frequency path(s) into a lower frequency range within the bandwidth capability of the oscilloscope front-end and ADC.  Digitizing is then performed, and the high-bandwidth path is upconverted and re-combined with the lower-bandwidth content using digital signal processing (DSP).  Since 2009, Teledyne LeCroy DBI oscilloscopes have been the undisputed highest bandwidth real-time oscilloscopes and have been the driving force in next-generation electrical and optical research and development.  Current real-time bandwidth obtained with DBI technology is 100 GHz with 240 GS/s.  ",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "71",
                                    "name": "Q-Scape",
                                    "icon": "icon71.png",
                                    "category": "view",
                                    "description": "Q-Scape effectively quadruples the display area on the oscilloscope using a tabbed display with four tabs that may each be configured with a unique combination of single or Multi-Grid <a onclick=&quot;updateModal(this)&quot; data-id=&quot;26&quot; href=&quot;javascript:void(0)&quot;>(26)</a> display grids.  Tabs may be displayed one at a time (Q-Scape Single), two at a time side-by-side (Q-Scape Dual), or all at once (Q-Scape Mosaic).  A user can toggle back and forth between Q-Scape and &quot;Normal&quot; display mode and retain display settings in each mode.  Q-Scape is especially useful when performing extensive math analysis or displaying many waveforms at one time since the display &quot;work area&quot; can be separated into tabs for a more intuitive view of waveforms, zooms and analysis activity. ",
                                    "unique": "false",
                                    "invent": "true"
                                },
                                {
                                    "number": "72",
                                    "name": "3D Persistence",
                                    "icon": "icon72.png",
                                    "category": "view",
                                    "description": "Persistence is the process by which digitally acquired waveforms are processed and displayed in a way that emulates the phosphor decay appearance of an analog oscilloscope. Teledyne LeCroy is the only oscilloscope supplier to provide a 3-D persistence view of acquired waveforms, with capability to adjust the X and Y-axis of rotation, select color or monochrome view, and select a solid, shaded or wireframe display.  3D Persistence is ideal when monitoring recurring signal behaviors for error events or visually examing waveforms for patterns of behavior. ",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "73",
                                    "name": "Auto-Scroll",
                                    "icon": "icon73.png",
                                    "category": "view",
                                    "description": "Auto-Scroll provides ability to play back zoom waveforms so as to automate visual review of long acquisitions and reduce fatigue from excessive zoom position knob rotation.  Auto-scroll includes pause, play (both directions), and fast-forward/rewind. ",
                                    "unique": "false",
                                    "invent": "true"
                                },
                                {
                                    "number": "74",
                                    "name": "Custom Measure",
                                    "icon": "icon74.png",
                                    "category": "measure",
                                    "description": "Users can create custom measurements using MATLAB, MathCad, C++, VBScript, Jscript (JavaScript), or Excel.  Unlike other oscilloscope customization routines, the Custom Measure capability in Teledyne LeCroy scopes provides for complete integration of the calculation within the oscilloscope program using ActiveDSO to send acquired data to the 3rd-party program and accept a result back for display native within the oscilloscope and available for further processing, just like any other standard oscilloscope measurement.   The Custom Measure result operates like any other built-in measurement parameter in that All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a>, Parameter Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;52&quot; href=&quot;javascript:void(0)&quot;>(52)</a>, Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a>, Parameter Acceptance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;53&quot; href=&quot;javascript:void(0)&quot;>(53)</a> Histicon/Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>, and Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a> capabilities may be used on the Custom Measure result.  Furthermore, Automation <a onclick=&quot;updateModal(this)&quot; data-id=&quot;115&quot; href=&quot;javascript:void(0)&quot;>(115)</a> commands may be embedded within the Custom Math setup to further invoke other oscilloscope operations or to invoke a 3rd party program.  ",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "75",
                                    "name": "Histicon/Histogram",
                                    "icon": "icon75.png",
                                    "category": "measure",
                                    "description": "Histograms graphically represent the distribution of a numerical measurement parameter set and provide intuition to complex signal behaviors (bimodal, multimodal, symmetric, skewed, normal, etc.) that cannot be understand any other way.  The power of Histograms is made possible with All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability permits fast and complete accumulation of up to 2 billion events in a Histogram from one or more acquisitions.   Users may define the number of bins in the histogram, and apply specialized histogram parameters to quantify the graphical behavior.  Histicons are thumbnails that provide a convenient and summarized view of the measurement parameter distribution.   Histicons are &quot;histogram icons&quot; and are iconic display of a histogram distribution of a measurement parameter.   Click on a histicon and it expands into a full-sized histogram. This complete range of capabilities is unique to Teledyne LeCroy.",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "76",
                                    "name": "Demodulation",
                                    "icon": "icon76.png",
                                    "category": "math",
                                    "description": "Teledyne LeCroy provides a Demodulation math function to demodulate analog Frequency Modulated (FM), Phase Modulated (PM), Amplitude Modulated (AM), Wide-band AM, Time, and Real/Imaginary modulation of signals.  Carrier-frequency is user-definable.  Decimation is permitted so that the high-frequency modulated signal can be appropriately sampled, but the recovered signal (which does not need as high a sampling rate) can be decimated to allow for faster processing time.  A low-pass FIR filter may be applied for baseband signal recovery, with complete user control of rolloff via tap settings.  ",
                                    "unique": "false",
                                    "invent": "false"
                                },
                                {
                                    "number": "77",
                                    "name": "Custom Math",
                                    "icon": "icon77.png",
                                    "category": "math",
                                    "description": "Users can create custom math functions using MATLAB, MathCad, C++, VBScript, Jscript (JavaScript), or Excel.  Unlike other oscilloscope customization routines, the Custom Math capability in Teledyne LeCroy scopes provides for complete integration of the calculation within the oscilloscope program using ActiveDSO to send acquired data to the 3rd-party program and accept a result back for display native within the oscilloscope and available for further processing, just like any other standard oscilloscope math function.   Furthermore, Automation <a onclick=&quot;updateModal(this)&quot; data-id=&quot;115&quot; href=&quot;javascript:void(0)&quot;>(115)</a> commands may be embedded within the Custom Math setup to further invoke other oscilloscope operations or to invoke a 3rd party program. This complete range of capabilities is unique to Teledyne LeCroy.",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "78",
                                    "name": "Boolean Compare",
                                    "icon": "icon78.png",
                                    "category": "analyze",
                                    "description": "Teledyne LeCroy oscilloscopes permit powerful pass/fail conditional setup on measurement parameters.  Combined with All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a>, Parameter Acceptance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;53&quot; href=&quot;javascript:void(0)&quot;>(53)</a>, and Measure Gate <a onclick=&quot;updateModal(this)&quot; data-id=&quot;6&quot; href=&quot;javascript:void(0)&quot;>(6)</a>, it is likely that nearly any pass/fail condition you will need can be set up, using more than 200 different measurement parameters, or those created using Parameter Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;52&quot; href=&quot;javascript:void(0)&quot;>(52)</a> or Custom Measure <a onclick=&quot;updateModal(this)&quot; data-id=&quot;74&quot; href=&quot;javascript:void(0)&quot;>(74)</a> with automatic notification or action enabled using Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a>.  Parameter comparisons can be made using a variety of operators, such as >, ≥, =, <, ≤, ∆, or ∆%, with ∆ results compared to current mean value, mean value ± 1σ, or mean ± 3σ.  Powerful boolean conditions can be configured with up to 8 different pass/fail conditions (queries), using combinations of ALL TRUE, ALL FALSE, and various AND/OR conditions.   This capability is unique to Teledyne LeCroy.  ",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "79",
                                    "name": "History Mode",
                                    "icon": "icon79.png",
                                    "category": "analyze",
                                    "description": "History Mode allows you to review any acquisition saved in the oscilloscope's history buffer, which automatically stores all acquisition records until full. Not only can individual acquisitions be restored to the grid, you can &quot;scroll&quot; backward and forward through the history at varying speeds to capture individual details or changes in the waveforms over time. Each record is indexed and time-stamped, and you can choose to view the absolute time of acquisition or the time relative to when you entered History Mode. In the latter case, the last acquisition is time zero, and all others are stamped with a negative time. The maximum number of records stored depends on your acquisition settings and the size of the oscilloscope memory.",
                                    "unique": "false",
                                    "invent": "false"
                                },
                                {
                                    "number": "80",
                                    "name": "Application Layer",
                                    "icon": "icon80.png",
                                    "category": "analyze",
                                    "description": "The Application Layer serial decode provided by Teledyne LeCroy utilizes the previously obtained Protocol Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;58&quot; href=&quot;javascript:void(0)&quot;>(58)</a> decode combined with an application layer database to translate the protocol messages into commands and information more recognizable to a user.  Teledyne LeCroy supports this level of serial decode for Controlled Area Network (CAN) using a user-provided DBC (.dbc file extension) application layer datbase file.   This capability is also commonly known as Symbolic decode for CAN (not to be confused with various Symbol <a onclick=&quot;updateModal(this)&quot; data-id=&quot;35&quot; href=&quot;javascript:void(0)&quot;>(35)</a> encoding schemes used with high-speed serial data.    Serial decoders are commonly combined with a Serial Data <a onclick=&quot;updateModal(this)&quot; data-id=&quot;47&quot; href=&quot;javascript:void(0)&quot;>(47)</a> Trigger to enhance debug and validation, and the serial data standards that support Application Layer serial decode have Application Layer trigger setup.",
                                    "unique": "false",
                                    "invent": "true"
                                },
                                {
                                    "number": "81",
                                    "name": "Timing Parameters",
                                    "icon": "icon81.png",
                                    "category": "analyze",
                                    "description": "Serial Bus Timing Parameters are provided as part of the various serial bus TDME or TDMP and ProtoBus MAG options and can measure timing between different combinations of serial data messages and analog signals, e.g. Message to Analog, Analog to Message, Message to Message, (Trigger) Time to Message, or Delta Message (Time). These timing parameters greatly improve debug capability by allowing faster validation of timing conditions (e.g., when a certain I2C message is sent an analog power rail should increase from 0 to 3.3 V within 100 ms), and faster identification of boundary condition failures using Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a>, Histicons/Histograms <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>, and Pass/Fail  Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a> and Boolean Compare <a onclick=&quot;updateModal(this)&quot; data-id=&quot;78&quot; href=&quot;javascript:void(0)&quot;>(78)</a>.  With All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability, every instance in the acquisition is measured, ensuring faster validation, debug, and correlation to other events.  More than a dozen serial protocol decoders are supported (e.g., CAN, LIN, FlexRay, I2C, SPI, UART, RS-232, DigRF, USB2, etc.) with the Message being definable by ID, DATA, or other conditions (specific to the protocol) with conditional setup (>, ≥, =, <>, <, ≤, within range, outside range) and the analog message definable by level, slope, etc. Complex holdoff conditions may be assigned, as well as Measure Gate <a onclick=&quot;updateModal(this)&quot; data-id=&quot;6&quot; href=&quot;javascript:void(0)&quot;>(6)</a> and Parameter Acceptance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;53&quot; href=&quot;javascript:void(0)&quot;>(53)</a> conditions.",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "82",
                                    "name": "Jitter Spectrum",
                                    "icon": "icon82.png",
                                    "category": "analyze",
                                    "description": "Jitter Spectrums utilize Teledyne LeCroy X-Stream processing speed and Full-Memory FFT <a onclick=&quot;updateModal(this)&quot; data-id=&quot;31&quot; href=&quot;javascript:void(0)&quot;>(31)</a> capabilities to calculate jitter spectrums and identify spectral peaks at very low frequencies - lower than possible with any other instrument.  Any identifiable clock jitter quantity (period, half-period, cycle-cycle, duty cycle, etc.) can be processed into a spectral view.  Jitter spectrums are also extensively utilized in modified form in serial data analysis in Rj+BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a>, Pj Spectral Views as described in Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>, and Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a>.",
                                    "unique": "false",
                                    "invent": "true"
                                },
                                {
                                    "number": "83",
                                    "name": "Jitter Simulation",
                                    "icon": "icon83.png",
                                    "category": "analyze",
                                    "description": "The Jitter Simulator can simulate a Clock or Sinusoid, non-return to zero (NRZ), return to zero (RZ), and bipolar return to zero (bpRZ) serial data signals, phase amplitude modulated four-level (PAM-4), or pulse-width modulated (PWM) signals with user-definable clock frequency or bit rate, amplitude, offset, rise/fall time, and frequency cutoff.  Serial data signals can be defined with varying pattern sequences and lengths.  User-defined (timing) jitter and (vertical) noise and shape attributes may be added to the selected signal.  The Jitter Simulator is an ideal tool for learning how to use the Teledyne LeCroy serial data analysis toolsets, such as Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a>, Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a>, Rj + BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a>, Bathtub Curve <a onclick=&quot;updateModal(this)&quot; data-id=&quot;38&quot; href=&quot;javascript:void(0)&quot;>(38)</a>, IsoBER <a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a>, various DDj, ISI and Pj views as described in Dj views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>, and Noise + Crosstalk Analysis <a onclick=&quot;updateModal(this)&quot; data-id=&quot;84&quot; href=&quot;javascript:void(0)&quot;>(84)</a>.",
                                    "unique": "true",
                                    "invent": "true"
                                }, {
                                    "number": "84",
                                    "name": "Noise+Crosstalk",
                                    "icon": "icon84.png",
                                    "category": "analyze",
                                    "description": "Noise Analysis is the &quot;vertical&quot; equivalent of Jitter (&quot;horizontal/time&quot;) analysis and consists of Vertical Noise measurements with random noise (Rn) and deterministic noise (Dn) measurements and an extrapolated total noise (Tn) calculation.  Breakdown of Dn into InterSymbol Interference noise (ISIn) and Periodic noise (Pn) is provided.  A variety of analysis functions and view are provided, such as Noise-based eye height and width parameters, Random noise (Rn) + Bounded Uncorrelated noise (BUn) Noise Histogram, Q-fit for Noise Histogram, Rn+Bun Noise Spectrum and Peak threshold, Pn Inverse FFT Plot, and Rn+Bun Noise Track.  The Noise Analysis capabilities, in general, are vertical equivalents of the horizontal (jitter) calculations described for Rj + BUj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;39&quot; href=&quot;javascript:void(0)&quot;>(39)</a>, DDj + ISI Views  and Pj Spectral Views as described in Dj Views <a onclick=&quot;updateModal(this)&quot; data-id=&quot;62&quot; href=&quot;javascript:void(0)&quot;>(62)</a>.<br>Crosstalk can be measured numerically through vertical Noise Analysis and displayed visually as a Crosstalk eye diagram contour plot, which is like the IsoBER <a onclick=&quot;updateModal(this)&quot; data-id=&quot;61&quot; href=&quot;javascript:void(0)&quot;>(61)</a>, but which includes a plot of the probabilistic extent of noise, both inside and outside the eye. Use Multi-Lane <a onclick=&quot;updateModal(this)&quot; data-id=&quot;8&quot; href=&quot;javascript:void(0)&quot;>(8)</a> LaneScape Comparison mode to generate crosstalk eyes on multiple lanes, and use the reference Lane when performing multi-scenario testing, such as aggressor on/off analysis. Two crosstalk eyes from different signals can be compared to easily see the result of the analysis, and crosstalk eyes can be overlaid for simple comparisons.",
                                    "unique": "true",
                                    "invent": "true"
                                },
                                {
                                    "number": "85-89",
                                    "name": "",
                                    "icon": "icon.png",
                                    "category": "disk",
                                    "description": "",
                                    "unique": "false",
                                    "invent": "false"
                                },
                                {
                                    "number": "90",
                                    "name": "LabNotebook",
                                    "icon": "icon90.png",
                                    "category": "document",
                                    "description": "LabNotebook is a powerful documentation tool that can be simply thought of as &quot;Save Job&quot; or &quot;Save Everything&quot; from a single acquisition into a LabNotebook folder with multiple entries.  Each entry combines waveform data, screen images, panel setups, and the end user's annotations saved into a single file that is one entry in a LabNotebook folder, and can then be restored on any Teledyne LeCroy oscilloscope to allow further analysis and post-processing of the data as if it had just been acquired.   Multiple LabNotebooks may be created and maintained to organize work by project, task, user, etc.  Upon saving, screen images may be annotated using the &quot;Scribble&quot; capability, a text description could be made to the file set, and a report generated using the built-in PDF writer.  Multiple entries in a single LabNotebook can be previewed to make selection simple.  LabNotebooks are stored in a compact format that is portable from one oscilloscope to another.  ",
                                    "unique": "true",
                                    "invent": "true"
                                }
                            ],
                                "row7": [
                                    {
                                        "number": "91",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "capture",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "92",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "capture",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "93",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "view",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "94",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "view",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "95",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "view",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "96",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "measure",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "97",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "measure",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "98",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "math",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "99",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "math",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "100",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "analyze",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "101",
                                        "name": "",
                                        "icon": "icon.png",
                                        "category": "analyze",
                                        "description": "",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "102",
                                        "name": "ProtoSync",
                                        "icon": "icon102.png",
                                        "category": "analyze",
                                        "description": "ProtoSync permits a user to acquire the physical layer protocol signal, using a supported Teledyne LeCroy oscilloscope, and link the acquired waveform(s) with the (CATC) protocol decode and vice versa. This simplifies debug by combining the physical layer view a hardware engineer is used to seeing with the protocol layer view a software engineer is expecting.  If, for example, the user has a protocol error, they can link directly to the scope waveform and discover that the protocol error was caused by a non-monotonic edge, slow risetime, runt, glitch, etc.  By having the two linked, the protocol and physical layer issues can be co-identified on the same trace.  ProtoSync augments the Symbol <a onclick=&quot;updateModal(this)&quot; data-id=&quot;35&quot; href=&quot;javascript:void(0)&quot;>(35)</a> and Protocol Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;58&quot; href=&quot;javascript:void(0)&quot;>(58)</a> serial decodes performed within the oscilloscope software and the Protocol Table <a onclick=&quot;updateModal(this)&quot; data-id=&quot;13&quot; href=&quot;javascript:void(0)&quot;>(13)</a> display of these decodes.",
                                        "unique": "true",
                                        "invent": "true"
                                    },
                                    {
                                        "number": "103",
                                        "name": "Serial DAC Waveform",
                                        "icon": "icon103.png",
                                        "category": "analyze",
                                        "description": "Serial digital-to-analog converted (DAC) Waveforms may be created using various serial bus TDME or TDMP and ProtoBus MAG options.  This capability permits the extraction of digital (serial) data from a user-defined message and the conversion of the digital value to a properly scaled and unitized analog value, per the user-definition.  This analog value then exists as a measurement parameter value, and Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a>, Histicons/Histograms <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>, Pass/Fail  Actions <a onclick=&quot;updateModal(this)&quot; data-id=&quot;56&quot; href=&quot;javascript:void(0)&quot;>(56)</a> and Boolean Compare <a onclick=&quot;updateModal(this)&quot; data-id=&quot;78&quot; href=&quot;javascript:void(0)&quot;>(78)</a> all can be used for further analysis.  With All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability, every instance in the acquisition is measured, and all of these values can be displayed as a pseudo-analog &quot;synthesized&quot; waveform using a variation of Track (see Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>, which completes the conversion of digital data to an analog waveform, and provides an inuitive view of the underlying embedded serial data that cannot be achieved any other way.  An example is to view sensor data (e.g. steering angle information) embedded in CAN data, or temperature information embedded in I2C messages.  More than a dozen serial protocol decoders are supported (e.g., CAN, LIN, FlexRay, I2C, SPI, UART, RS-232, USB2, etc.) with the Message being definable by ID, DATA, and other conditions (specific to the protocol), all of which permit data location to the bit level.  The capability is further leveraged in CAN through the Application Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;80&quot; href=&quot;javascript:void(0)&quot;>(80)</a> or Symbolic setup capability, in which the exact message can be simply selected using a .dbc file, thereby avoiding the setup of the data location, re-scaling, and unit selection.  ",
                                        "unique": "true",
                                        "invent": "true"
                                    },
                                    {
                                        "number": "104",
                                        "name": "JitKit Views",
                                        "icon": "icon104.png",
                                        "category": "analyze",
                                        "description": "JitKit Quick View provides an instant setup of four views of jitter - statistical Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a>), Time variation (Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a>), persisted Jitter Overlay <a onclick=&quot;updateModal(this)&quot; data-id=&quot;14&quot; href=&quot;javascript:void(0)&quot;>(14)</a>, and spectral (Jitter Spectrum <a onclick=&quot;updateModal(this)&quot; data-id=&quot;82&quot; href=&quot;javascript:void(0)&quot;>(82)</a>) for multiple jitter parameters (period, frequency, cycle-cycle, TIE, etc.) all at one time. ",
                                        "unique": "true",
                                        "invent": "true"
                                    },
                                    {
                                        "number": "105",
                                        "name": "EyeDr / VP",
                                        "icon": "icon105.png",
                                        "category": "analyze",
                                        "description": "Eye Doctor II is a complete set of tools that allows the full-range of fixture, serial data channel and probe de-embedding, transmitter pre- and de-emphasis and serial data channel emulation, and receiver equalization (CTLE, FFE, or DFE) emulation on full record lengths with little impact on waveform processing time.   Eye Doctor II provides real-time results on the acquired or simulated (with the Jitter Simulation <a onclick=&quot;updateModal(this)&quot; data-id=&quot;83&quot; href=&quot;javascript:void(0)&quot;>(83)</a>) waveforms, with post-processing for Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a>, Jitter Analysis, and other Teledyne LeCroy tools.  Industry-standard S-Parameter measurements and Touchstone 1.1 files are used to describe all embedded or emulated objects.  Virtual probing (VP) may be enabled via Teledyne LeCroy's Processing Web <a onclick=&quot;updateModal(this)&quot; data-id=&quot;55&quot; href=&quot;javascript:void(0)&quot;>(55)</a>.  A subset of the EyeDr / VP tools is available in some products as a simple (serial data) Channel De-embedding using a Touchstone file or simple table definition of attenuation by frequency. ",
                                        "unique": "false",
                                        "invent": "true"
                                    }, {
                                        "number": "106",
                                        "name": "VectorLinQ VSA",
                                        "icon": "icon106.png",
                                        "category": "analyze",
                                        "description": "VectorLinQ transforms your Teledyne LeCroy oscilloscope into the most flexible vector signal analysis platform available. Acquire up to eight inputs using Teledyne LeCroy oscilloscopes that support 4 to 80 Channels <a onclick=&quot;updateModal(this)&quot; data-id=&quot;25&quot; href=&quot;javascript:void(0)&quot;>(25)</a> through the oscilloscope channels - Phase shift keyed (PSK), quadrature amplitude modulated (QAM) circular QAM, amplitude shift keyed (ASK), and frequency shift keyed (FSK) signal types are all supported, as well as user-defined symbol configurations. VectorLinQ operates through a unique, intuitive model, passing the signal through a chain of user-defined processing blocks, simplifying the most sophisticated analysis. Standard processing blocks include mixers, filters, phase estimators, and equalizers. Users may also insert MATLAB Custom Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;77&quot; href=&quot;javascript:void(0)&quot;>(77)</a> processing blocks at any point in the processing chain.",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "107-114",
                                        "name": "QualiPHY",
                                        "icon": "icon107-114.png",
                                        "category": "qualiphy",
                                        "description": "QualiPHY is highly automated compliance test software meant to help you develop and validate the PHY (physical-electrical) layer of a device, in accordance with the official documents published by the applicable standards organizations and special interest groups (SIGs). You can additionally set custom variables and limits to test compliance to internal standards.  QualiPHY is composed of a “framework” application that enables the configuration and control of separate tests for each standard through a common user interface. Features include Multiple Data Source Capability (Connect to your X-Stream oscilloscope via LAN or other interfaces), User-Defined Test Limits (Tighten limits to ensure devices are well within the passing region, even if subsequently measured with different equipment), and Flexible Test Results Reporting that includes XML Test Record Generation to better understand a device performance distribution, or obtain process related information from the devices under test.  QualiPHY supports a wide variety of Ethernet <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107&quot; href=&quot;javascript:void(0)&quot;>(107)</a>, Automotive <a onclick=&quot;updateModal(this)&quot; data-id=&quot;111&quot; href=&quot;javascript:void(0)&quot;>(111)</a>, Video <a onclick=&quot;updateModal(this)&quot; data-id=&quot;109&quot; href=&quot;javascript:void(0)&quot;>(109)</a>, MIPI <a onclick=&quot;updateModal(this)&quot; data-id=&quot;110&quot; href=&quot;javascript:void(0)&quot;>(110)</a>, PCIe <a onclick=&quot;updateModal(this)&quot; data-id=&quot;112&quot; href=&quot;javascript:void(0)&quot;>(112)</a>, USB <a onclick=&quot;updateModal(this)&quot; data-id=&quot;113&quot; href=&quot;javascript:void(0)&quot;>(113)</a> and Storage <a onclick=&quot;updateModal(this)&quot; data-id=&quot;114&quot; href=&quot;javascript:void(0)&quot;>(114)</a> serial data and DDR <a onclick=&quot;updateModal(this)&quot; data-id=&quot;108&quot; href=&quot;javascript:void(0)&quot;>(108)</a> memory standards to make compliance testing fast and easy.  ",
                                        "unique": "false",
                                        "invent": "false"
                                    },
                                    {
                                        "number": "115",
                                        "name": "Automation",
                                        "icon": "icon115.png",
                                        "category": "document",
                                        "description": "Teledyne LeCroy oscilloscopes use Microsoft Automation API for native control and interface to other applications.   The Automation command set provides unlimited control of the oscilloscope from a remote location or from within the local environment of the oscilloscope, both from within the oscilloscope application (e.g. a Teledyne LeCroy MAUI setup file) and from outside (e.g., another Automation-compatible program).   Every setting or button push available on the oscilloscope can be controlled with Automation commands, as described in the X-Stream Browser installed on the oscilloscope desktop.  Automation commands can be invoked from any text based language, including but not limited to Python, C#, a Visual Basic Script, Matlab, or Labview, from programs running on the oscilloscope platform, or off of it.  Remote connectivity to the oscilloscope is achieved through standards including Ethernet, USBTMC, GPIB, or the high speed interface <a onclick=&quot;updateModal(this)&quot; data-id=&quot;68&quot; href=&quot;javascript:void(0)&quot;>(68)</a> interface.  Locally on the oscilloscope, Automation commands can be used to create custom user interface objects using Teledyne LeCroy CustomDSO or plug-ins, or embedded within a Custom Math <a onclick=&quot;updateModal(this)&quot; data-id=&quot;77&quot; href=&quot;javascript:void(0)&quot;>(77)</a> function or Custom Measure <a onclick=&quot;updateModal(this)&quot; data-id=&quot;74&quot; href=&quot;javascript:void(0)&quot;>(74)</a> parameter, making it possible to customize the user-experience, create new capabilities within the oscilloscope user interface, or use a Custom Math/Measure routine to further invoke a third-party software calculation or action.  Automation commands can also invoke legacy (GPIB) IEEE 488.2 Remote Control commands. ",
                                        "unique": "true",
                                        "invent": "false"
                                    }
                                ],
                                    "row8": [
                                        {
                                            "number": "0",
                                            "description": "col2"
                                        },
                                        {
                                            "number": "17",
                                            "name": "Device Loss",
                                            "icon": "icon17.png",
                                            "category": "motor",
                                            "description": "Power semiconductor device conduction, switching loss and ON resistance testing is easily performed with Teledyne LeCroy's PWR software option, and runs with optimal results using Teledyne LeCroy's HDO 12-bit Resolution <a onclick=&quot;updateModal(this)&quot; data-id=&quot;48&quot; href=&quot;javascript:void(0)&quot;>(48)</a> oscilloscopes and DA1855A Differential Amplifier.  Loss values are accurately measured using the precision offset adjust in the DA1855A amplifier, and All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability combined with automatic gating of OFF and ON device events in the PWR software package allows fast analysis under a variety of operating conditions.  Histicons/Histograms <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a> and Tracks (see Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>) can be employed to better understand operating conditions.  ",
                                            "unique": "true",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "18",
                                            "name": "Control Loop",
                                            "icon": "icon18.png",
                                            "category": "motor",
                                            "description": "Power electronics control loop analysis can be performed using an All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> pulse-width modulated signal measurement of Width and a Track (see Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>) to monitor the change in width over time, time-correlated to the power supply voltage signals.  ",
                                            "unique": "true",
                                            "invent": "true"
                                        },
                                        {
                                            "number": "19",
                                            "name": "Harmonics",
                                            "icon": "icon19.png",
                                            "category": "motor",
                                            "description": "AC Line Input harmonics for a single-phase switch-mode power supply are easily calculated and displayed in &quot;binned&quot; spectral waveform and table format, with comparison to reference standard or user-defined levels.  In Motor Drive Analyzer models, AC Line Input or (Inverter) Drive Output harmonics can be calculated, displayed as total harmonic distortion (THD) or &quot;binned&quot; waveforms and tables for all three-phases, including simultaneous calculations for voltage, current and power.  Additionally, per-cycle numeric values can be calculated based on a full-spectrum acquisition, or just the fundamental, fundamental + N harmonics, or harmonic range.  All per-cycle values can be plotted as a waveform using a variation of a Track (see Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>).  ",
                                            "unique": "true",
                                            "invent": "true"
                                        },
                                        {
                                            "number": "20",
                                            "name": "3-phase",
                                            "icon": "icon20.png",
                                            "category": "motor",
                                            "description": "Motor Drive Analyzers provide three-phase inverter drive input/output voltage, current, power, and efficiency calculations.  Calculations may also be made on DC bus and Motor Shaft (mechanical) torque and speed quantities, including calculation of speed from complex sensors, such as Resolvers, Quadrature Encoder Interfaces (QEI) or Brushless DC (BLDC) Hall sensors.  Cycle period for calculation is determined using a user-defined &quot;Sync&quot; signal that may be filtered to achieve accurate zero-crossing determination, and also displayed for positive verification that correct period determination is being made.  The method is viable for fixed frequency or variable frequency signals.  Calculated per-cycle values may be displayed in a mean-value Numerics table, or, using All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> measurement capability, in a Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> table that permits Histogram (see Histicon/Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a>) displays.  Per-cycle quantities may be displayed using a modified version of Track (see Tracks / Trends <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>) to show the variation of the calculated quantity over time - a significant aid in understanding complex drive and power behaviors.  ",
                                            "unique": "true",
                                            "invent": "true"
                                        },
                                        {
                                            "number": "21",
                                            "name": "Static+Dynamic",
                                            "icon": "icon21.png",
                                            "category": "motor",
                                            "description": "Static and Dynamic power behaviors may be easily observed with the Motor Drive Analyzer.  The Numerics table displays a simple mean value for any of more than 30 voltage, current, power, mechanical, or efficiency parameter measurements using a table that can contain up to 10 rows (sources) and 12 columns (measurements).  Simple touch a single cell (mean value) of this Numerics table creates a per-cycle Waveform showing the variation of the parameter over time, and opens a Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> table display of the complete data set.  ",
                                            "unique": "true",
                                            "invent": "true"
                                        },
                                        {
                                            "number": "22",
                                            "name": "Zoom+Gate",
                                            "icon": "icon22.png",
                                            "category": "motor",
                                            "description": "Zoom+Gate is a powerful capability that, with a single button push, creates zooms of acquired Motor Drive Analyzer waveforms and simultaneously gates the Numerics mean value table to only include measurement values defined by the zoom position and size.  The Zoom+Gate position and size can be changed dynamically with instantaneous update of the Numerics table values along with any per-cycle Waveforms and Statistics values (see Static+Dynamic <a onclick=&quot;updateModal(this)&quot; data-id=&quot;21&quot; href=&quot;javascript:void(0)&quot;>(21)</a>).  This makes it fast to acquire extremely long events (seconds or minutes) of dynamic system behavior and quickly Zoom to and Gate the measurement quantities and per-cycle Waveforms to the exact (dynamic) area of interest.  ",
                                            "unique": "true",
                                            "invent": "true"
                                        }, {
                                            "number": "63",
                                            "name": "",
                                            "icon": "icon.png",
                                            "category": "optical",
                                            "description": "",
                                            "unique": "false",
                                            "invent": "false"
                                        }, {
                                            "number": "64",
                                            "name": "",
                                            "icon": "icon.png",
                                            "category": "optical",
                                            "description": "",
                                            "unique": "false",
                                            "invent": "false"
                                        }, {
                                            "number": "65",
                                            "name": "",
                                            "icon": "icon.png",
                                            "category": "optical",
                                            "description": "",
                                            "unique": "false",
                                            "invent": "false"
                                        }, {
                                            "number": "66",
                                            "name": "",
                                            "icon": "icon.png",
                                            "category": "optical",
                                            "description": "",
                                            "unique": "false",
                                            "invent": "false"
                                        }, {
                                            "number": "67",
                                            "name": "",
                                            "icon": "icon.png",
                                            "category": "optical",
                                            "description": "",
                                            "unique": "false",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "107",
                                            "name": "Ethernet",
                                            "icon": "icon107.png",
                                            "category": "qualiphy",
                                            "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, for Ethernet is an automated test package that performs electrical tests for the three common networking standards over unshielded twisted pair (UTP) cables normally referred to as CAT5 or CAT5E. The electrical tests are performed on 10Base-T, 100Base-TX, and 1000Base-T signals as specified in the IEEE 802.3-2005 and ANSI X3.263-1995 standards. The package gives the user the ability to both automate compliance testing and debug devices, hosts and hubs. Users interested in compliance testing should begin by using QualiPHY. Users interested in debugging circuits, should begin by using the oscilloscope’s embedded test tools.  Users that begin by using QualiPHY may detect failures on their device, and then have the option of switching to the oscilloscope’s embedded test tools. ",
                                            "unique": "false",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "108",
                                            "name": "DDR",
                                            "icon": "icon108.png",
                                            "category": "qualiphy",
                                            "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, DDR packages are automated test packages for performing all of the real time oscilloscope validation tests for various DDR and LPDDR standards in accordance with JEDEC Standards.  ",
                                            "unique": "false",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "109",
                                            "name": "Video",
                                            "icon": "icon109.png",
                                            "category": "qualiphy",
                                            "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, supports a automated compliance testing for a variety of video standards, such as DisplayPort, embedded DisplayPort (eDP), HDMI, and HDMI2.",
                                            "unique": "false",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "110",
                                            "name": "MIPI",
                                            "icon": "icon110.png",
                                            "category": "qualiphy",
                                            "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, supports a automated compliance testing for a variety of MIPI standards, such as DPHY and MPHY.",
                                            "unique": "false",
                                            "invent": "false"
                                        },
                                        {
                                            "number": "0",
                                            "description": "col1"
                                        }
                                    ],
                                        "row9": [
                                            {
                                                "number": "0",
                                                "description": "col2"
                                            },
                                            {
                                                "number": "40",
                                                "name": "R/W Separation",
                                                "icon": "icon40.png",
                                                "category": "ddr",
                                                "description": "Automatic separation of Read and Write bursts eliminates the time-consuming process of manual burst identification and simplifies the analysis of DDR systems. In a single, long acquisition, bursts can be separated algorithmically based on a DQ-DQS skew condition or on the command bus logic state when used in conjunction with the HDA125 High-speed Digital Analyzer, (see 4 to 80 Channels <a onclick=&quot;updateModal(this)&quot; data-id=&quot;25&quot; href=&quot;javascript:void(0)&quot;>(25)</a>). Or, a more traditional method of single-trigger/single-burst read/write separation can be performed using an Analog+Digital Trigger <a onclick=&quot;updateModal(this)&quot; data-id=&quot;24&quot; href=&quot;javascript:void(0)&quot;>(24)</a> pattern condition.  The HDA125 additionally enables a unique &quot;bus view&quot;, tabulating the Command Bus activity and placing Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a> and annotations on top of the physical layer waveform in an intuitive manner.  The HDA125 brings command bus acquisition to Teledyne LeCroy’s already comprehensive toolset, providing the ultimate in memory bus analysis capability.  Utilize QualiPHY for DDR <a onclick=&quot;updateModal(this)&quot; data-id=&quot;108&quot; href=&quot;javascript:void(0)&quot;>(108)</a> for automated compliance testing to JEDEC standards.  ",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "41",
                                                "name": "Multi-Eye View",
                                                "icon": "icon41.png",
                                                "category": "ddr",
                                                "description": "The DDR Debug Toolkit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a> can quickly create and display up to 10 Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a> simultaneously with a push of a button after Reads/Writes <a onclick=&quot;updateModal(this)&quot; data-id=&quot;40&quot; href=&quot;javascript:void(0)&quot;>(40)</a> have been automatically separated.  Visual inspection and analysis of side-by-side eye diagrams, using Multi-Measurement Views very similar to the Multi-Lane <a onclick=&quot;updateModal(this)&quot; data-id=&quot;8&quot; href=&quot;javascript:void(0)&quot;>(8)</a> serial data analysis capability, provides valuable skew and timing information. Choosing to have CK or DQS as the timing reference provides two different vantage points of system performance.  Enabling mask failure indicators will automatically identify and locate the specific UI where any mask violation occurred. Built-in measurements such as eye height, eye width, and eye opening are critical to gaining a quantitative understanding of the system performance. With simultaneous eye measurements it is easy to compare performance across multiple testing views.",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "42",
                                                "name": "DDR Tj, Rj, Dj",
                                                "icon": "icon42.png",
                                                "category": "ddr",
                                                "description": "The bursted nature of DDR signals makes it very different than most serial data communication standards. As a result, the traditional oscilloscope-based methods and algorithms for measuring NRZ Serial Data Tj, Rj, Dj <a onclick=&quot;updateModal(this)&quot; data-id=&quot;15&quot; href=&quot;javascript:void(0)&quot;>(15)</a> jitter and analyzing system performance are not capable of measuring bursted DDR signals. The DDR Debug Toolkit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a> uses jitter algorithms which have been tailored for bursted DDR signals. Jitter parameters including Tj, Rj, and Dj are calculated across all active DDR measurement views. To gain a deeper understanding of the jitter distribution, traditional displays such as Time Interval Error (TIE) Jitter Histogram <a onclick=&quot;updateModal(this)&quot; data-id=&quot;60&quot; href=&quot;javascript:void(0)&quot;>(60)</a>, TIE Jitter Track <a onclick=&quot;updateModal(this)&quot; data-id=&quot;37&quot; href=&quot;javascript:void(0)&quot;>(37)</a>, and Bathtub Curve <a onclick=&quot;updateModal(this)&quot; data-id=&quot;38&quot; href=&quot;javascript:void(0)&quot;>(38)</a> are available.",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "43",
                                                "name": "Debug Toolkit",
                                                "icon": "icon43.png",
                                                "category": "ddr",
                                                "description": "Most oscilloscope-based DDR physical layer test tools on the market are targeted exclusively at JEDEC compliance testing - Teledyne LeCroy has such tools as well, see ;>QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a> and DDR <a onclick=&quot;updateModal(this)&quot; data-id=&quot;108&quot; href=&quot;javascript:void(0)&quot;>(108)</a>.  However, the DDR Debug Toolkit provides test, debug and analysis tools for the entire DDR design cycle, making it the ultimate DDR analysis package. The DDR Debug Toolkit includes numerous time-saving industry firsts to simplify DDR testing. With a push of a button, Read/Write <a onclick=&quot;updateModal(this)&quot; data-id=&quot;40&quot; href=&quot;javascript:void(0)&quot;>(40)</a> bursts can be separated and Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a> for each can be displayed in real-time, quickly providing unique insight to system performance. This capability makes it effortless to identify the root cause of problems using jitter analysis specifically designed for the bursted DDR signals that conventional serial data tools cannot analyze. A wide variety of DDR-specific measurement parameters are built in to the toolkit enabling an easy quantitative analysis of system performance. These parameters utilize leverage All Instance <a onclick=&quot;updateModal(this)&quot; data-id=&quot;29&quot; href=&quot;javascript:void(0)&quot;>(29)</a> and Statistics <a onclick=&quot;updateModal(this)&quot; data-id=&quot;30&quot; href=&quot;javascript:void(0)&quot;>(30)</a> capabilities, and can be Histogrammed <a onclick=&quot;updateModal(this)&quot; data-id=&quot;75&quot; href=&quot;javascript:void(0)&quot;>(75)</a> or Tracked <a onclick=&quot;updateModal(this)&quot; data-id=&quot;54&quot; href=&quot;javascript:void(0)&quot;>(54)</a>. All this DDR analysis can be performed simultaneously over four different measurement scenarios allowing for a deep understanding of system performance, dramatically improving DDR testing efficiency, and providing faster results.",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "44",
                                                "name": "Virtual Probe",
                                                "icon": "icon44.png",
                                                "category": "ddr",
                                                "description": "Teledyne LeCroy's VirtualProbe Signal Integrity Tools package allows the user to virtually move the probing location to the DRAM ball grid array (BGA), which often cannot be physically probed. Additionally, VirtualProbe can be used to remove any effects of the probe or interposers through de-embedding to improve the signal quality. VirtualProbe is included in the EyeDrII-VP bundle (see Eye Dr / VP <a onclick=&quot;updateModal(this)&quot; data-id=&quot;106&quot; href=&quot;javascript:void(0)&quot;>(106)</a>, and if purchased, is the perfect complement to the toolsets in the DDR Debug Toolkit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a>. Additionally, as part of the VirtualProbe packages, Teledyne LeCroy offers a unique math function, Virtual Probe at Receiver (VP@Rcvr), that can be used to virtually move the probing location to the receiver. This is accomplished by describing the receiver and channel characteristics without requiring an S-parameter file. Accurately modeling the receiver can reduce reflections on the signals under test and allow for higher quality measurements to be made. VP@Rcvr can be used with both  ;>QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a> and DDR <a onclick=&quot;updateModal(this)&quot; data-id=&quot;108&quot; href=&quot;javascript:void(0)&quot;>(108)</a> packages and the DDR Debug Toolkit <a onclick=&quot;updateModal(this)&quot; data-id=&quot;43&quot; href=&quot;javascript:void(0)&quot;>(43)</a>.",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "45",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "ddr",
                                                "description": "",
                                                "unique": "",
                                                "invent": ""
                                            },
                                            {
                                                "number": "85",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "disk",
                                                "description": "",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "86",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "disk",
                                                "description": "",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "87",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "disk",
                                                "description": "",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "88",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "disk",
                                                "description": "",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "89",
                                                "name": "",
                                                "icon": "icon.png",
                                                "category": "disk",
                                                "description": "",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "111",
                                                "name": "Automotive",
                                                "icon": "icon111.png",
                                                "category": "qualiphy",
                                                "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, packages are available for a variety of automotive serial data standards, such as Broad-R-Reach and MOST (50/150, electrical and optical).  Additionally, physical layer Eye Diagrams <a onclick=&quot;updateModal(this)&quot; data-id=&quot;7&quot; href=&quot;javascript:void(0)&quot;>(7)</a> and test programs as well as Serial Data <a onclick=&quot;updateModal(this)&quot; data-id=&quot;47&quot; href=&quot;javascript:void(0)&quot;>(47)</a> Triggers and serial data Protocol Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;58&quot; href=&quot;javascript:void(0)&quot;>(58)</a> and Application <a onclick=&quot;updateModal(this)&quot; data-id=&quot;80&quot; href=&quot;javascript:void(0)&quot;>(80)</a> layer decoders with Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a> and Protocol Table <a onclick=&quot;updateModal(this)&quot; data-id=&quot;13&quot; href=&quot;javascript:void(0)&quot;>(13)</a> are available for other standards, such as FlexRay, CAN, LIN, SENT, Manchester, etc.  Serial message analysis for Search & Zoom <a onclick=&quot;updateModal(this)&quot; data-id=&quot;36&quot; href=&quot;javascript:void(0)&quot;>(36)</a>, Bus Parameters <a onclick=&quot;updateModal(this)&quot; data-id=&quot;59&quot; href=&quot;javascript:void(0)&quot;>(59)</a>, Timing Parameters <a onclick=&quot;updateModal(this)&quot; data-id=&quot;81&quot; href=&quot;javascript:void(0)&quot;>(81)</a>, and Serial DAC Waveform <a onclick=&quot;updateModal(this)&quot; data-id=&quot;103&quot; href=&quot;javascript:void(0)&quot;>(103)</a> are also available for many of the automotive protocols.",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "112",
                                                "name": "PCIe",
                                                "icon": "icon112.png",
                                                "category": "qualiphy",
                                                "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, packages are available for PCIe Gen1, Gen2, Gen3, and Gen4.  PCIe Gen3 and Gen4 are available with complete end-to-end Transmit (Tx) and Receiver (Rx) testing, including dynamic link equalization testing - something no other equipment supplier supports.  ProtoSync <a onclick=&quot;updateModal(this)&quot; data-id=&quot;102&quot; href=&quot;javascript:void(0)&quot;>(102)</a> is ideal for understanding and confirming link traffic and handshaking during dynamic link equalization activities.",
                                                "unique": "true",
                                                "invent": "true"
                                            },
                                            {
                                                "number": "113",
                                                "name": "USB",
                                                "icon": "icon113.png",
                                                "category": "qualiphy",
                                                "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, packages are available for USB1.1, 2.x, and 3.x.  USB3 support is available with complete end-to-end Transmit (Tx) and Receiver (Rx) testing.   Serial decoders and triggers are available many of the USB protocols or symbol formats.  ProtoSync <a onclick=&quot;updateModal(this)&quot; data-id=&quot;102&quot; href=&quot;javascript:void(0)&quot;>(102)</a> links CATC protocol software with the physical layer waveforms and serial decodes on the oscilloscope for complete understanding between hardware and software development teams.",
                                                "unique": "true",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "114",
                                                "name": "Storage",
                                                "icon": "icon114.png",
                                                "category": "qualiphy",
                                                "description": "QualiPHY <a onclick=&quot;updateModal(this)&quot; data-id=&quot;107-114&quot; href=&quot;javascript:void(0)&quot;>(107-114)</a>, also known as QPHY, packages are available for most versions of SAS and SATA.  Serial Data <a onclick=&quot;updateModal(this)&quot; data-id=&quot;47&quot; href=&quot;javascript:void(0)&quot;>(47)</a> Triggers and Protocol Layer <a onclick=&quot;updateModal(this)&quot; data-id=&quot;58&quot; href=&quot;javascript:void(0)&quot;>(58)</a> decoders with Color Overlays <a onclick=&quot;updateModal(this)&quot; data-id=&quot;5&quot; href=&quot;javascript:void(0)&quot;>(5)</a> and Protocol Tables <a onclick=&quot;updateModal(this)&quot; data-id=&quot;13&quot; href=&quot;javascript:void(0)&quot;>(13)</a> are available many of the USB protocols or symbol formats.  ProtoSync <a onclick=&quot;updateModal(this)&quot; data-id=&quot;102&quot; href=&quot;javascript:void(0)&quot;>(102)</a> links CATC protocol software with the physical layer waveforms and serial decodes on the oscilloscope for complete understanding between hardware and software development teams.",
                                                "unique": "false",
                                                "invent": "false"
                                            },
                                            {
                                                "number": "0",
                                                "description": "col1"
                                            }
                                        ]
    }
}