$(document).ready(function() {

      var json;
      var row;
      var col;
      var div;
      var image;
      var category;
      var title;
      $.getJSON("js/boxes.js", function(data) {
        json = data.elements;
        $.each(json, function(rowId, array) {
          if (rowId == "row1") {
            $.each(array, function(idx, obj) {
              //console.log(obj.number);
              if (obj.number == 0) {
                div = '<div class="col-xs-16 col-sm-16 col-md-16 col-lg-16">&nbsp;</div>';
                //console.log(div);
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title;
              }
              $('#row1').append(div);
            });
          } else if (rowId == "row2") {
            $.each(array, function(idx, obj) {
              //console.log(obj.number);
              if (obj.number == 0) {
                div = '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">&nbsp;</div>';
                //console.log(div);
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title;
              }
              $('#row2').append(div);
            });
          } else if (rowId == "row3") {
            $.each(array, function(idx, obj) {
              //console.log(obj.number);
              if (obj.number == 0) {
                div = '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">&nbsp;</div>';
                //console.log(div);
              } else if (obj.number == '17-22'){
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image motor"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title motor">' + obj.name + '</p></a></div>';
                div += category + image + title;
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title;
              }
              $('#row3').append(div);
            });
          } else if (rowId == "row4") {
            $.each(array, function(idx, obj) {
              if (obj.number == '40-45'){
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image ddr"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title ddr">' + obj.name + '</p></a></div>';
                div += category + image + title;
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title; }
              $('#row4').append(div);
            });
          } else if (rowId == "row5") {
            $.each(array, function(idx, obj) {
              if (obj.number == '63-67'){
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image optical"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title optical">' + obj.name + '</p></a></div>';
                div += category + image + title;
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title; }
              $('#row5').append(div);
            });
          } else if (rowId == "row6") {
            $.each(array, function(idx, obj) {
              if (obj.number == '85-89'){
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image disk"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title disk">' + obj.name + '</p></a></div>';
                div += category + image + title;
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title; }
              $('#row6').append(div);
            });
          } else if (rowId == "row7") {
            $.each(array, function(idx, obj) {
              if (obj.number == '107-114'){
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image qualiphy"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title qualiphy">' + obj.name + '</p></a></div>';
                div += category + image + title;
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title; }
              $('#row7').append(div);
            });
          } else if (rowId == "row8") {
            $.each(array, function(idx, obj) {
              //console.log(obj.number);
              if (obj.number == 0 && obj.description == "col2") {
                div = '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">&nbsp;</div>';
              } else if (obj.number == 0 && obj.description == "col1") {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">&nbsp;</div>';
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title;
              }
              $('#row8').append(div);
            });
          } else if (rowId == "row9") {
            $.each(array, function(idx, obj) {
              //console.log(obj.number);
              if (obj.number == 0 && obj.description == "col2") {
                div = '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">&nbsp;</div>';
              } else if (obj.number == 0 && obj.description == "col1") {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">&nbsp;</div>';
              } else {
                div = '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a class="open-modal" href="#largeModal" data-number="' + obj.number + '" data-name="' + obj.name + '" data-icon="' + obj.icon + '" data-category="' + obj.category + '" data-unique="' + obj.unique + '" data-invent="' + obj.invent + '" data-description="' + obj.description + '">';
                category = '<p class="category ' + obj.category + '">' + obj.number;
                if(obj.unique == 'true'){category += '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';}
                if(obj.invent == 'true'){category += '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';}
                image = '</p><p class="image dark-gray"><img class="icon img-responsive" src="thumb/'+ obj.icon+'"></p>';
                title = '<p class="title dark-gray">' + obj.name + '</p></a></div>';
                div += category + image + title;
              }
              $('#row9').append(div);
            });
          }
        });
      });


//key overlay
$("<div></div>").css({
    position: "absolute",
    width: "400px",
    height: "300px",    
    top: 90,
    left: 445,
    background: 'url("css/periodic-table-key.jpg") no-repeat'
}).appendTo($("#table").css("position", "relative"));




//MODAL trigger

  $('body').on('click','.open-modal', function () {
    var loc = $(this).data('icon');
    var uni = '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';
    var inv = '<span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>';
    var num = $(this).data('number');


    var modal = $('#largeModal');
    
    modal.find('img').attr("src","img/"+loc);
    modal.find('.modal-header').attr('class', 'modal-header');
    modal.find('.modal-header').addClass($(this).data('category'));
    if($(this).data('unique') == true){num += uni;}
    if($(this).data('invent') == true){num += inv;}
    modal.find('.modal-title').html(num);
    modal.find('.media-heading').text($(this).data('name'));
    modal.find('.description').html($(this).data('description'));
    modal.find('.small-cat').text('category: '+$(this).data('category'));

    $('#largeModal').modal('show');

  });



});



$( window ).load(function() {
  //remove empty links
$('a[data-name=""]').removeAttr('href class');
});


function updateModal(el){
 
  var identifier = $(el).data('id');
  $('a[data-number='+identifier+']').click();

}


  