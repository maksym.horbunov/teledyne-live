﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/MasterPage/site.master" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.trigger_decode_default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
							<style type="text/css">							    a.nav-link {cursor:pointer;
							    }
							</style>									
			<div id="main" role="main">
				<div class="container">
					<div class="row" style="margin-bottom:45px;">
						<div class="col-md-6">
							<h2 class="featurette-heading middle">Comprehensive Low-speed Serial Data Trigger, Decode, Measure/Graph, and Eye Diagram Capabilities</h2>
							<p class="lead">Teledyne LeCroy&rsquo;s Trigger<b>(T)</b>, Decode<b>(D)</b>, Measure/Graph<b>(M or G)</b> and Eye Diagram and Physical Layer<b>(E or P)</b> options are the best in the industry and nearly universally available across our entire oscilloscope product line.</p><a class="btn" href="/doc/tdme-datasheet" type="button"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> TDME Datasheet</a>
						</div>
						<div class="col-md-6"><img class="featurette-image img-responsive" alt="Trigger Decode Measure Graph Eye Diagram" style="max-height:500px;" src="/images/tdme-ds-cover-images.png"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="table-list text-center">
								<div class="row mb-3 mb-lg-5">
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Embedded Computing</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=516&amp;groupid=88" role="button">I<sup>2</sup>C</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=519&amp;groupid=88" role="button">SPI</a></li>
											<li><a class="text-black" href="/options/productseries.aspx?mseries=521&amp;groupid=88" role="button">UART-RS232</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Automotive + Industrial</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=496&amp;groupid=88" role="button">CAN</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=497&amp;groupid=88" role="button">CAN FD</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=517&amp;groupid=88" role="button">LIN</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=515&amp;groupid=88" role="button">FLEXRAY</a></li>
											<li><a class="text-black" href="/options/productseries.aspx?mseries=618&amp;groupid=88" role="button">SENT</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Avionics<br />&nbsp;</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=513&amp;groupid=88" role="button">ARINC-429</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=518&amp;groupid=88" role="button">MIL-STD-1553</a></li>
											<li><a class="text-black" href="/options/productseries.aspx?mseries=494&amp;groupid=88" role="button">SpaceWire</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Peripherals<br />&nbsp;</h6>
										<ul class="list-inline">
											<li><a class="text-black" href="/options/productseries.aspx?mseries=522&amp;groupid=88" role="button">USB 2.0</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Power Management</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=602&amp;groupid=88" role="button">SPMI</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=616&amp;groupid=88" role="button">SMBus</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=634&amp;groupid=88" role="button">PMBus</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">MIPI<br />&nbsp;</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=604&amp;groupid=88" role="button">I<sup>3</sup>C</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=374&amp;groupid=88" role="button">DPHY</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=322&amp;groupid=88" role="button">DigRF 3G</a></li>
											<li><a class="text-black" href="/options/productseries.aspx?mseries=347&amp;groupid=88" role="button">DigRF v4</a></li>
										</ul>
									</div>
									<div class="col-6 col-md-4 col-lg-3 col-xl">
										<h6 class="text-blue font-weight-medium">Other<br />&nbsp;</h6>
										<ul class="list-inline">
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=263&amp;groupid=88" role="button">AudioBus</a></li>
											<li class="mb-3"><a class="text-black" href="/options/productseries.aspx?mseries=437&amp;groupid=88" role="button">Manchester</a></li>
											<li><a class="text-black" href="/options/productseries.aspx?mseries=436&amp;groupid=88" role="button">NRZ</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!--.col-sm-12
						table.table
						  thead
						    tr
						      th(scope='col') Embedded Computing
						      th(scope='col') Automotive + Industrial
						      th(scope='col') Avionics
						      th(scope='col') Peripherals
						      th(scope='col') Power Mgmt
						      th(scope='col') MIPI
						      th(scope='col') Other
						  tbody
						    tr
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=516&groupid=88' role='button')
						          | I
						          sup 2
						          | C
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=496&groupid=88' role='button') CAN
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=513&groupid=88' role='button') ARINC-429
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=522&groupid=88' role='button') USB 2.0
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=602&groupid=88' role='button') SPMI
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=604&groupid=88' role='button')
						          | I
						          sup 3
						          | C
						      td
						        a.btn.btn-light(href='/options/productseries.aspx?mseries=263&groupid=88' role='button') AudioBus
						    tr
						      td
						        a.btn(href='/options/productseries.aspx?mseries=519&groupid=88' role='button') SPI
						      td
						        a.btn(href='/options/productseries.aspx?mseries=497&groupid=88' role='button') CAN FD
						      td
						        a.btn(href='/options/productseries.aspx?mseries=518&groupid=88' role='button') MIL-STD-1553
						      td &nbsp;
						      td
						        a.btn(href='/options/productseries.aspx?mseries=616&groupid=88' role='button') SMBus
						      td
						        a.btn(href='/options/productseries.aspx?mseries=374&groupid=88' role='button') DPHY
						      td
						        a.btn(href='/options/productseries.aspx?mseries=437&groupid=88' role='button') Manchester
						    tr
						      td
						        a.btn(href='/options/productseries.aspx?mseries=521&groupid=88' role='button') UART-RS232
						      td
						        a.btn(href='/options/productseries.aspx?mseries=517&groupid=88' role='button') LIN
						      td
						        a.btn(href='/options/productseries.aspx?mseries=494&groupid=88' role='button') FLEXRAYSpaceWire
						      td &nbsp;
						      td &nbsp;
						      td
						        a.btn(href='/options/productseries.aspx?mseries=322&groupid=88' role='button') DigRF 3G
						      td
						        a.btn(href='/options/productseries.aspx?mseries=436&groupid=88' role='button') NRZ
						    tr
						      td &nbsp;
						      td
						        a.btn(href='/options/productseries.aspx?mseries=515&groupid=88' role='button') FLEXRAY
						      td &nbsp;
						      td &nbsp;
						      td &nbsp;
						      td
						        a.btn(href='/options/productseries.aspx?mseries=347&groupid=88' role='button') DigRF v4
						      td &nbsp;
						    tr
						      td &nbsp;
						      td
						        a.btn(href='/options/productseries.aspx?mseries=618&groupid=88' role='button') SENT
						      td &nbsp;
						      td &nbsp;
						      td &nbsp;
						      td &nbsp;
						      td &nbsp;
						-->
					</div>
					<hr class="featurette-divider">
					<div class="row">
						<div class="row featurette">
							<div class="col-md-5"><a name="trigger"></a>
								<h2 class="featurette-heading top">Highest Performance<b> Triggers</b></h2>
								<p class="lead">Designed by people who know the standards, with the unique capabilities you want to isolate unusual events. Conditional data triggering permits maximum flexibility, and highly adaptable error frame triggering is available to isolate error conditions. Frame definition allows grouping of UART or SPI packets into message frames for customization.</p>
							</div>
							<div class="col-md-7">
								<!-- Nav tabs-->
								<ul class="nav nav-pills nav-justified" role="tablist">
									<li class="nav-item"><a class="nav-link active" data-target="#a1" role="tab" data-toggle="tab">Powerful, Flexible, Unique</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#a2" role="tab" data-toggle="tab">Conditional Data Setup</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#a3" role="tab" data-toggle="tab">Support for Many Proprietary Protocols</a></li>
								</ul>
								<!-- Tab panes-->
								<div class="tab-content">
									<div class="tab-pane active" id="a1"><a href="/images/tdme-trigger-buttons.png" rel="shadowbox"><img src="/images/tdme-trigger-buttons.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Every serial trigger we design exhibits deep knowledge of the standard.  Most serial triggers work with digital (MSO) inputs, or the EXT input for the Clock line so as to conserve analog channels.  Each serial trigger has some unique aspect for high performance, such as:
											<ul>
												<li>I2C trigger permits triggering on data in a specific location of an up to 2048 byte I2C EEPROM read or write.</li>
												<li>UART or SPI bytes can be combined into a single &ldquo;message frame&rdquo; - trigger on custom protocols based on UART or SPI byte blocks.</li>
												<li>UART supports 9-bit &ldquo;address&rdquo; or &ldquo;wakeup&rdquo; mode triggering.</li>
												<li>CAN, CAN FD, LIN, FlexRay and MIL-STD-1553 permit conditional ID/Address triggering.</li>
												<li>CAN and CAN FD permit triggering symbolically using a DBC file.</li>
												<li>USB 2.0 and MIL-STD-1553 triggers permits complex transaction definition and triggering.</li>
											</ul>
										</p>
									</div>
									<div class="tab-pane" id="a2"><a href="/images/tdme-conditional-data.png" rel="shadowbox"><img src="/images/tdme-conditional-data.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Every Teledyne LeCroy low-speed serial trigger that incorporates DATA trigger permits a conditional (&lt;. &lt;=, =, &gt;, &gt;=, &lt;&gt;, inside a range, outside a range) setup for the DATA condition. This is especially useful in situations where abnormal events should be monitored, such as when a temperature sensor transmitting via I2C exceeds a maximum temperature, or a CAN node broadcasts a low or high engine RPM or coolant pressure.  Furthermore, data for triggering can be specifically isolated in very long byte streams to specific bit locations, even those which span data bytes.</p>
									</div>
									<div class="tab-pane" id="a3"><a href="/images/tdme-byte-msg-modes.png" rel="shadowbox"><img src="/images/tdme-byte-msg-modes.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Many proprietary serial protocols make use of the common UART (single Data line) or USART (Clock and Data lines, e.g., SPI) byte structures, with multiple bytes grouped into proprietary protocol definitions. Our highly flexible UART byte and SPI format definitions accommodate nearly any customer need, and the UART or SPI bytes can be defined to be part of a single &ldquo;message frame&rdquo; through use of our Interframe Setup. Then, the trigger pattern setup can isolate any byte value, e.g., an ID, or a DATA string value, that is part of your proprietary protocol message definition.</p>
									</div>
								</div>
								<!-- End NAV TABS-->
							</div>
						</div>
					</div>
					<hr class="featurette-divider">
					<div class="row">
						<div class="row featurette">
							<div class="col-md-5"><a name="decode"></a>
								<h2 class="featurette-heading top">The Best Serial<b> Decoder</b></h2>
								<p class="lead">Decoded protocol information is color-coded to specific portions of the serial data waveform and transparently overlaid for an intuitive, easy-to-understand visual record. All decoded protocols are displayed in a single time-interleaved table.  Touch a row in the interactive table to quickly zoom to a packet of interest and easily search through long records for specific protocol events using the built-in search feature.</p>
							</div>
							<div class="col-md-7" style="text-align:left;">
								<!-- Nav tabs-->
								<ul class="nav nav-pills nav-justified" role="tablist">
									<li class="nav-item active"><a class="nav-link" data-target="#usb4" role="tab" data-toggle="tab">Intuitive, Color-Coded Overlays</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb5" role="tab" data-toggle="tab">Pattern Search</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb6" role="tab" data-toggle="tab">Interactive Table Summarizes Results</a></li>
								</ul>
								<!-- Tab panes-->
								<div class="tab-content">
									<div class="tab-pane active" id="usb4"><a href="/images/page-1-tdme-d-top.png" rel="shadowbox"><img src="/images/page-1-tdme-d-top.png" width="300px" border="0" align="left" hspace="10px" vspace="10px" style="margin-bottom: 35px;"></a>
										<p style="text-align:left;">A transparent overlay with color-coding for specific portions of each protocol and the entire message frame makes it easy to understand your serial data information. Unlike other solutions, with protocol decode information away from the signal, our solution correlates the waveform and the protocol decode directly on the display. As the acquisition length is expanded or shortened, the decode overlay will adjust to show you just the right amount of information.</p>
									</div>
									<div class="tab-pane" id="usb5"><a href="/images/page-3-tdme-d-search-middle.png" rel="shadowbox"><img src="/images/page-3-tdme-d-search-middle.png" width="300px" border="0" align="left" hspace="10px" vspace="10px" style="margin-bottom: 45px;"></a>
										<p style="text-align:left;">All decoders provide ability to search through a long record of decoded data by using a variety of search criteria, or values, or simply finding the next occurrence. Pattern Search automatically creates a zoom trace of the acquired waveform and displays the selected location complete with the transparent color-coded overlay.</p>
									</div>
									<div class="tab-pane" id="usb6"><a href="/images/tdme-interactive-table.png" rel="shadowbox"><img src="/images/tdme-interactive-table.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Turn the oscilloscope into a protocol analyzer with a tabular display of decoded information.  Customize the table to show only the data of interest and touch a message in the table to automatically zoom to it and display it on the screen.  Export the table for offline analysis.  Up to four different decoded signals of any type may be simultaneously displayed in the table.</p>
									</div>
								</div>
								<!-- End NAV TABS-->
							</div>
						</div>
					</div>
					<hr class="featurette-divider">
					<div class="row">
						<div class="row featurette">
							<div class="col-md-5"><a name="measure"></a>
								<h2 class="featurette-heading top"><b>Measure</b> and<b> Graph</b> Tools for Validation Efficiency</h2>
								<p class="lead">Quickly validate cause and effect with automated timing measurements to or from an analog signal or another serial message. Make multiple measurements in a single long acquisition to quickly acquire statistics during corner-case testing.  Serial (digital) data can be extracted to an analog value and graphed to monitor system performance over time, as if it was probed directly.  Complete validation faster and gain better insight.</p>
							</div>
							<div class="col-md-7" style="text-align:left;">
								<!-- Nav tabs-->
								<ul class="nav nav-pills nav-justified" role="tablist">
									<li class="active nav-item"><a class="nav-link" data-target="#usb8" role="tab" data-toggle="tab">Serial Data DAC and Graphing Tools</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb9" role="tab" data-toggle="tab">Automated Timing Measurements</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb10" role="tab" data-toggle="tab">Bus Status Measurements</a></li>
								</ul>
								<!-- Tab panes-->
								<div class="tab-content">
									<div class="tab-pane active" id="usb8"><a href="/images/page-1-tdme-m-can-steering-wheel--bottom.png" rel="shadowbox"><img src="/images/page-1-tdme-m-can-steering-wheel--bottom.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>Digital data can be extracted from specific locations in the serial data message using the Message to Value measurement parameter - a serial data DAC. This information can then be displayed as a measurement parameter value(s), or it can be viewed as a time-correlated waveform displaying the measurement value over time - as if you were able to probe and acquire it directly. Use the long acquisition time of the oscilloscope to understand how the data changes over long periods of time, in conjunction with other system behaviors.
										<p></p>
										<p>Some examples of the usefulness of this capability are:
											<ul>
												<li>Viewing I2C or SPI temperature sensor data</li>
												<li>Viewing DigRF 3G radio frequency I and Q modulated signals</li>
												<li>Viewing CAN wheel speed information used by an ABS</li>
												<li>Viewing reconstructed analog audio from serial I2S streams</li>
											</ul>
										</p>
									</div>
									<div class="tab-pane" id="usb9"><a href="/images/protobus-mag-hero-shot.png" rel="shadowbox"><img src="/images/protobus-mag-hero-shot.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Utilize a serial trigger to isolate a specific message and then measure a cause-effect timing relationship with a subsequent analog signal, or vice versa. But instead of manually measuring the timing with cursors, use these tools to automate the measurement and return thousands of values quickly as your system undergoes stress testing.  Automate the measurement and validation of gateway latency times from one serial message to another (e.g. CAN to LIN or low-speed CAN to high-speed CAN, or CAN to FlexRay) without having to manually use cursors or compare values and times in a protocol table. Quickly understand bus latency times or arbitration behaviors by measuring the difference between two messages on a single decoded waveform. Dramatically improve your validation efficiency and time to insight</p>
									</div>
									<div class="tab-pane" id="usb10"><a href="/images/tdme-bus-status.png" rel="shadowbox"><img src="/images/tdme-bus-status.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">The bus status measurements Bus Load, Message Bitrate, and Number of Messages, give an overall status of the decode protocol to quickly learn if the bus is over utilized and to verify the bit rate matches expectations.</p>
									</div>
									<!-- End NAV TABS-->
								</div>
							</div>
						</div>
					</div>
					<hr class="featurette-divider">
					<div class="row">
						<div class="row featurette">
							<div class="col-md-5"><a name="eye"></a>
								<h2 class="featurette-heading top"><b>Eye Diagrams</b> &amp;<b> Physical Layer Testing</b></h2>
								<p class="lead">Rapidly display an eye diagram of your packetized low-speed serial data signal without additional setup time. Use eye parameters to quantify system performance and apply a standard or custom mask to identify anomalies.  Mask failures can be indicated and can force the scope into Stop mode.</p>
							</div>
							<div class="col-md-7" style="text-align:left;">
								<!-- Nav tabs-->
								<ul class="nav nav-pills nav-justified" role="tablist">
									<li class="active nav-item"><a class="nav-link" data-target="#usb22" role="tab" data-toggle="tab">Up to 4 Simultaneous Eye Diagrams</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb23" role="tab" data-toggle="tab">Eye Diagram Measurement Parameters</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb24" role="tab" data-toggle="tab">Mask and Mask Failure Indication</a></li>
									<li class="nav-item"><a class="nav-link" data-target="#usb25" role="tab" data-toggle="tab">Physical Layer Testing (Eye Diagrams + Advanced Measurements)</a></li>
								</ul>
								<!-- Tab panes-->
								<div class="tab-content">
									<div class="tab-pane active" id="usb22"><a href="/images/page-8-tdme-e_four-eyes-middle.png" rel="shadowbox"><img src="/images/page-8-tdme-e_four-eyes-middle.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Up to four serial data signals can be decoded and displayed as eye diagrams at one time. These can be different protocols, or the same protocol measured at different points (e.g., transmit and receive, different nodes, or different standard-defined test points). Apply a user-defined filter to each eye diagram to only display specific signals in the eye</p>
									</div>
									<div class="tab-pane" id="usb23"><a href="/images/tdme-parameters-table.png" rel="shadowbox"><img src="/images/tdme-parameters-table.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Quantify physical layer signal quality in the eye by applying parameters for Eye Height, Eye Width, and Number of Mask Failures. Some packages (e.g. FlexRay TDMP) go a step further and include additional measurements defined in the standard.</p>
									</div>
									<div class="tab-pane" id="usb24"><a href="/images/page-9-tdme-e_mask-failure-top.png" rel="shadowbox"><img src="/images/page-9-tdme-e_mask-failure-top.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">A user-defined or pre-defined mask may be added to the eye diagram so as to objectively evaluate if the physical layer signal intrudes too far into the eye opening. Apply a filter to include or exclude specific messages from the Eye so as to determine failure source (e.g., messages from a specific node or with a specific ID).  Mask failures are indicated with a red circle and can be displayed in a table.  Touch the failure table to open a zoom of the failed area for further inspection</p>
									</div>
									<div class="tab-pane" id="usb25"><a href="/images/page-9-tdme-p-bottom.png" rel="shadowbox"><img src="/images/page-9-tdme-p-bottom.png" width="300px" border="0" align="left" hspace="10px" vspace="10px"></a>
										<p style="text-align:left;">Some standards, due to their speed or nodal complexity, provide specific guidance on what eye diagrams or measurements should be made and exactly how they should be performed.  FlexRay, and MIPI DPHY are examples.  In these cases, the Eye Diagram (&ldquo;E&rdquo;) capability is augmented with additional specialized &ldquo;P&rdquo; capability (for Physical Layer Measurements), per the standard  In these cases, the &ldquo;E&rdquo; capabilities previously described are also available.</p>
									</div>
									<!-- End NAV TABS-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

</asp:Content>