﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.usbc_default" %>
<!DOCTYPE html>
 <html lang="en">
<head>
  <title>USB-C Test Solutions | Teledyne LeCroy</title>
  <meta content="width=device-width, initial-scale=1.0"
        name="viewport">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
     <link href="/wr8000hd/css/style.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700"
        rel="stylesheet"
        type="text/css">
  <style type="text/css">
      h1 {font-size: 3.56rem;
      }
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }

      .card.larger {height: 720px;
      }
      .radial {color:black; 
          background: rgb(255,255,255);
          background: radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(0,118,192,1) 95%);
      }
      .dropdown-content li:hover,.dropdown-content li.active{background-color:#eee;font-weight:bold;}
  </style>
    <meta property="og:title" content="USB-C Test Solutions"/>
<meta property="og:description" content="USB-C Test Solutions"/> 
<meta property="og:url" content="https://teledynelecroy.com/usb-c/" /> 
<meta property="og:image" content="https://assets.lcry.net/images/usbc-01.png"/>
<meta property="og:type" content="website" />
</head>
<body>
  <div class="container">
    <a href="../"
         id="logo-container"><img alt="Teledyne LeCroy"
         class="brand-logo"
         src="/images/tl_weblogo_blkblue_189x30.png"></a>
  </div>
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h1 class="maui-blue">USB and USB Type-C<sup>&reg;</sup> Electrical Test Solutions</h1>
        <p class="black-text" style="font-size:1.0625rem;">Teledyne LeCroy's USB and USB-C<sup>&reg;</sup> electrical test solutions combine superior instruments with sophisticated software for: </p>
        <ul style="font-size:1.0625rem;">
          <li>&#9642; Fast and accurate transmitter (Tx) and receiver (Rx) compliance testing for USB4 and Thunderbolt 3</li>
          <li>&#9642; USB3.x and USB2 compliance and debug</li>
          <li>&#9642; USB-PD (Power Delivery) compliance and debug</li>
          <li>&#9642; DisplayPort&trade; over USB-C compliance testing</li>
        </ul>
           <p><span class="wistia_embed wistia_async_b0obbucpmn popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:183px;position:relative;width:325px">&nbsp;</span></p>
      </div>
        <!-- Dropdown for datasheets -->
                    <ul id="dropdown1" class="dropdown-content white blue-text text-accent-4" style="min-width: 300px;width: 300px;max-width: 300px;">
                        <li><a href="/doc/pcie5-test-solution-datasheet">QPHY-PCIE5-TX-RX Datasheet</a></li>
                        <li><a href="/doc/qphy-pcie4-tx-rx-ds">QPHY-PCIE4-TX-RX Datasheet</a></li>
                    </ul>
      <div class="col l6 s12"><img alt="USB-C Test Solutions Cover"
           class="responsive-img"
           src="https://assets.lcry.net/images/usbc-01.png"></div>
  </div></div>
  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="section">
        <!--   Icon Section   -->
        <div class="row">
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="https://assets.lcry.net/images/usbc-02.png" alt="USB-C Test Solutions Test Suite"></div>
              <div class="card-content"
                   style="max-height: 100%">
                <h5 class="center maui-blue">USB4<sup>&trade;</sup> and Thunderbolt<sup>&trade;</sup> 3 Electrical PHY Test Suite</h5>
                <ul>
                  <li>&#9642; Single Solution for Tx and Rx compliance testing</li>
                  <li>&#9642; Fastest, more efficient testing</li>
                  <li>&#9642; High accuracy and repeatability due to superior signal quality</li>
                </ul><a href="#one">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="https://assets.lcry.net/images/usbc-03.png" alt="USB-PD Test Solutions"></div>
              <div class="card-content">
                <h5 class="center maui-blue">USB3.x, USB 2.0, and USB-PD</h5>
                <ul>
                  <li>&#9642; Full range of instruments for all data rates</li>
                  <li>&#9642; USB-IF Approved compliance</li>
                  <li>&#9642; Protocol trigger, decode, and measurements for system debug</li>
                  <li>&#9642; USB-PD compliance and debug</li>
                </ul><a href="#two">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="center"
                   src="https://assets.lcry.net/images/usbc-04.png" alt="DisplayPort Test Solutions"></div>
              <div class="card-content">
                <h5 class="center maui-blue">DisplayPort over USB-C</h5>
                <ul><li>&#9642; VESA Approved compliance</li>
                  <li>&#9642; Supports 3rd party fixtures and test controllers</li>
                </ul><a href="/options/productseries.aspx?mseries=241&groupid=140">Learn more...</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="clearfix"></div>
  <div class="container">
    <div class="section"
         id="one">
      <div class="row">
        <div class="col m8 s12">
          <h3>USB4<sup>&trade;</sup> and Thunderbolt<sup>&trade;</sup> 3 Electrical PHY Test Suite</h3>
          <p>The Teledyne LeCroy LabMaster 10 Zi-A or SDA 8 Zi-B (25 GHz and above) with QPHY-USB4 software, used with an Anritsu SQA-R MP1900A BERT provides USB4 electrical validation and test engineers an integrated electrical test solution for automated compliance testing, ensuring faster time-to-market.</p>
        </div><div class="col m4 s12"><img class="materialboxed responsive-img center" alt="USB-C Visibility Test Solutions"
             src="https://teledynelecroy.com/images/qphy-usb4-04.png"
             width="300px"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Complete USB4 & Thunderbolt 3 Compliance Test Specification (CTS) Coverage</strong></h5>
              <p>Teledyne LeCroy’s high performance oscilloscopes and the SQA-R MP1900A with <a href="/options/productseries.aspx?mseries=612&groupid=140">QPHY-USB4-TX-RX</a> support the following 10-20Gb/s compliance test specifications:</p>
              <ul>
                <li>&#9642; USB4 Router Assembly Transmitter & Receiver</li>
<li>&#9642; USB4 Captive Device Transmitter & Receiver</li>
<li>&#9642; Thunderbolt 3 Device Transmitter</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Faster, more efficient testing</strong></h5>
              <ul>
                <li>&#9642; Test multiple USB4/TBT3 lanes simultaneously with the LabMaster 10 Zi-A; or use the SDA 8Zi-B for lane-lane testing</li>
                <li>&#9642; Debug faster with <a href="/sdaiii/">SDAIII-CompleteLinQ software</a></li>
              </ul>
</div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Superior Stressed Signal Generation</strong></h5>
              <p>QPHY-USB4-TX-RX fully automates receiver calibration and test with the Anritsu SQA-R MP1900A high-speed BERT solution:</p>
                <ul>
                <li>&#9642; Support for USB4/TBT3 and USB3.x</li>
                <li>&#9642; Jitter and noise generation</li>
                <li>&#9642; High quality waveform PPG with integrated 10-tap emphasis</li>
                <li>&#9642; High input sensitivity ED with clock recovery</li>
                <li>&#9642; Variable CTLE and link negotiation functions required for PCIe Rx tests</li></ul>
            </div>
          </div>
        </div>
      </div>
    </div></div>
  <div class="clearfix"></div>
    <div class="radial">
<div class="container">
    <div class="section" id="videos">
            <div class="row">
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_yal3z12gdg popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_52fh3uv56x popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_mimaj4xaln popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
            </div>
            </div></div></div><div class="container">
    <div class="section"
         id="two">
      <div class="row">
        <div class="col m8 s12">
          <h3>USB 3.x Electrical PHY Test Suite</h3>
          <p>USB 3.0, 3.1, 3.2?... the latest USB-IF USB3.x specification is USB3.2 with Gen1 (5Gb/s) and Gen2 (10Gb/s) data rates. Teledyne LeCroy LabMaster 10 Zi-A or SDA 8 Zi-B with QPHY-USB3.2 software, used with an Anritsu SQA-R MP1900A BERT provides the complete solution for USB3.x transmitter (Tx) and Receiver (Rx) electrical validation, compliance, and debug.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center" alt="USB-C Visibility Test Solutions"
             src="https://assets.lcry.net/images/usbc-07.png"
             width="300px"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Oscilloscopes for Transmitter Compliance and Validation</strong></h5>
              <p>Teledyne LeCroy’s high performance oscilloscopes with QPHY-USB3.2-TX-RX support transmitter (TX) and receiver (RX) compliance tests.</p>
                <ul><li>&#9642; <a href="/options/productseries.aspx?mseries=584&groupid=140">QPHY-USB3.2-TX-RX</a> is approved by the USB-IF for 'gold suite' compliance testing.</li>
                <li>&#9642; 16GHz (and above) models for testing at USB3.2 Gen1/2 (5Gb/s and 10 Gb/s); 8GHz (and above) models for testing Gen1 only (at 5Gb/s).
</li></ul>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Receiver Testing with Anritsu SQA-R MP1900A</strong></h5>
              <p>QPHY-USB3.2-TX-RX fully automates receiver test calibration and execution with the Anritsu SQA-R MP1900A.</p>
               <ul>
                <li>&#9642; USB-IF approved compliance test solution</li>
                <li>&#9642; Ping.LFPS generation for Tx testing</li>
                <li>&#9642; Loopback using LTSSM (Link Training Status State Machine)</li>
                <li>&#9642; USB3.2 ED (Error Detection)</li>
                <li>&#9642; Automated JTOL (Jitter Tolerance) -margin testing</li>
                <li>&#9642; Future proof performance to 32Gb/s for standards such as USB4 and Thunderbolt 3</li></ul>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Beyond Compliance with Signal Integrity and Protocol Tools</strong></h5>
              <p>Go beyond compliance with advanced signal integrity tools:</p>
                <ul>
                <li>&#9642; <a href="/sdaiii/">SDAIII-CompleteLinQ</a> software for serial data analysis and debug</li>
                <li>&#9642; The <a href="/tdr-and-s-parameters/series.aspx?mseries=592">WavePulser 40iX High-speed Interconnect Analyzer</a> provides cost effective S-parameter measurements for Cable and Interconnects.</li></ul>
                <p>USB3.x protocol decode and Protosync software for debug:</p>
                <ul>
                <li>&#9642; <a href="/options/productseries.aspx?mseries=334&groupid=88">USB3.x D - Decode software</a></li>
                <li>&#9642; <a href="/options/productseries.aspx?mseries=287&groupid=88">USB 3.x Protosync – Protocol analyzer software</a></li></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="three">
      <div class="row">
        <div class="col m8 s12">
          <h3>USB 2.0 Electrical PHY Test Suite</h3>
          <p>The USB2.0 standard consists of Low Speed (LS – 1.5Mb/s), Full Speed (FS – 12Mb/s), and High Speed (HS – 480Mb/s) Teledyne LeCroy Oscilloscope used with QPHY-USB software, active probes, and protocol layer tools provides a complete test solution for USB 2.0 electrical compliance testing and debug.</p>

        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center" alt="USB-C Test Solutions Devices"
             src="https://assets.lcry.net/images/usbc-protosync.png"
             width="300px"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Compliance Test</strong></h5>
              <p><a href="/options/productseries.aspx?mseries=255&groupid=140">QPHY-USB</a> test software implements a full set of electrical tests for USB 2.0, including High-, Full-, and Low-speed tests</p>
              <ul><li>&#9642; Compliant with all real-time oscilloscope tests specified by the USB-IF procedures</li>
<li>&#9642; Support for host, device, and hub testing</li>
<li>&#9642; Simple and easy-to-use automated testing</li>
<li>&#9642; Available TF-USB-B test fixture</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>TDME and Protocol Layer Tools for Debug</strong></h5>
              <p>USB2.0 decode and Protosync software for debug:</p>
<ul><li>&#9642; <a href="/options/productseries.aspx?mseries=522&groupid=88">USB2.0 TDME</a> - Trigger, Decode Measurements, Eye software.</li>
<li>&#9642; <a href="/options/productseries.aspx?mseries=522&groupid=88">USB2.0 TD</a> - Trigger, Decode software</li>
<li>&#9642; <a href="/options/productseries.aspx?mseries=385&groupid=88">HSIC D</a> – Decode software</li>
<li>&#9642; <a href="/options/productseries.aspx?mseries=287&groupid=88">USB 2.0 Protosync</a> – Protocol analyzer software</li></ul>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Wide Range of Oscilloscopes</strong></h5>
              <p>Recommended models for USB 2.0:</p>
              <p><a href="/options/productseries.aspx?mseries=584&groupid=140">SDA 8 Zi-B</a> (lower deck BNC Inputs)</p>
              <p><a href="/hdo/">WavePro HD</a> oscilloscopes</p>
                <img class="materialboxed responsive-img center" alt="USB-C Test Solutions Devices"
             src="https://assets.lcry.net/images/usbc-hd.png"
             width="300px">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="four">
      <div class="row">
        <div class="col m8 s12">
          <h3>USB Type-C<sup>&reg;</sup> Power Delivery</h3>
          <p>Only Teledyne LeCroy supplies the full range of solutions for USB Power Delivery</p>
            <ul>
              <li>&#9642; Oscilloscope-based TDME for Electrical and Digital Debug</li>
              <li>&#9642; Comprehensive USB Type-C<sup>&trade;</sup> & PD Compliance Test platforms</li>
              <li>&#9642; Low-cost solutions for USB 2.0, PD, and 'Alt Mode' protocol analysis</li></ul>
        </div>
          <div class="col m4 s12"><img class="materialboxed responsive-img center" alt="USB-C Test Solutions Devices"
             src="https://assets.lcry.net/images/usbc-09.png" width="300px"></div>
      </div>
    </div>
    <div class="row">
      <div class="col m4 s12">
        <div class="card medium grey lighten-5">
          <div class="card-content">
            <h5><strong>Oscilloscope TDME for USB-PD Debug</strong></h5> <ul>
              <li>&#9642; Trigger – Trigger on PD preamble, ordered set, messages, resets, and errors</li>
              <li>&#9642; Decode – Decode PD messages</li>
              <li>&#9642; Measurements – electrical voltage, current, and timing measurements</li>
                <li>&#9642; Eye Diagrams – BMC Eye Diagram mask testing and measurements</li>
            </ul><a href="/lighthousepartnersprogram/">Request USB-PD-TDME Beta Key</a>

          </div>
        </div>
      </div>
      <div class="col m4 s12">
        <div class="card medium grey lighten-5">
          <div class="card-content">
            <h5><strong>100% Coverage for PD & USB Type-C™ Compliance</strong></h5>
            <ul>
              <li>&#9642; USB-IF Approved for PD & Type-C Compliance Testing</li>
              <li>&#9642; The only USB 3.2 Gen2 solution approved for USB-IF Link Layer Compliance testing</li>
              <li>&#9642; Integrated USB 3.1 / PD exerciser option for device and host traffic generation</li>
            </ul><div class="valign-wrapper"><img class="responsive-img" alt="USB-C Test Solutions Devices" src="/images/voyager-m310p-cropped.png" /><a href="/protocolanalyzer/usb/voyager-m310p">Learn more about MP310P Voyager</a></div>
          </div>
        </div>
      </div>
      <div class="col m4 s12">
        <div class="card medium grey lighten-5">
          <div class="card-content">
            <h5><strong>Portable & Affordable Power Delivery analyzers</strong></h5>
            <ul>
              <li>&#9642; Analyze and debug PD / Type-C messages & state changes</li>
              <li>&#9642; Industry’s most comprehensive USB device class decoding</li>
              <li>&#9642; Power Tracker™ graphical display of VBUS power & current </li></ul>
                <div class="valign-wrapper"><img class="circle responsive-img" alt="USB-C Test Solutions Devices" src="/images/mercury_t2c_t2p_quote.png" />
            <a href="/protocolanalyzer/usb/mercury-t2c-t2p">Learn more about Mercury</a></div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end of container -->


  <footer class="page-footer white">
    <div class="container center social">
      <p><a href="https://facebook.com/teledynelecroy"
         onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/facebook.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://twitter.com/teledynelecroy"
         onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/twitter.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://youtube.com/lecroycorp"
         onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/youtube.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://blog.teledynelecroy.com/"
         onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/blogger.jpg"
           width="25px"></a> &nbsp;&nbsp; <a href="https://www.linkedin.com/company/10007"
         onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"
         target="_blank"><img border="0"
           height="25px"
           src="https://assets.lcry.net/images/linkedin.jpg"
           width="25px"></a></p>
      <p class="grey-text">&copy; 2020&nbsp;Teledyne LeCroy</p><a href="/support/securitypolicy.aspx"
           id="ucFooter_rptLinks_hlkLink_0"
           target="_blank">Privacy Policy</a> | <a href="/sitemap/"
           id="ucFooter_rptLinks_hlkLink_1"
           target="_blank">Site Map</a>
      <p><a href="http://teledyne.com/"
         onclick="GaEventPush('OutboundLink', 'http://teledyne.com');"
         target="_blank">Teledyne Technologies</a></p>
    </div>
  </footer>

    <script async
        charset="ISO-8859-1"
        src="//fast.wistia.com/assets/external/E-v1.js"></script> 
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        M.AutoInit();
        /*document.addEventListener('DOMContentLoaded', function () {
            //lightbox
    //var elems = document.querySelectorAll('.materialboxed');
            //var instances = M.Materialbox.init(elems);
            //dropdown trigger
            var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });*/</script>
    <script>
        (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
