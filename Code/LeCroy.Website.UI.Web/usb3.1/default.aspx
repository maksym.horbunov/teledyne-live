﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.usb31_default" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Teledyne LeCroy is the only vendor which offers a complete line of USB 3.2 test solutions covering transmitter, receiver, protocol, and cable/connector compliance, characterization, and debug. Teledyne LeCroy’s coordinated USB 3.2 test solutions ensure the right support for every stage of development.
">
    <meta name="author" content="Teledyne LeCroy">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>USB 3.1/3.2 Test Suite | Teledyne LeCroy</title>

    <!-- Bootstrap core CSS -->
    <link href="https://teledynelecroy.com/newwave/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.css">
    <link href="https://teledynelecroy.com/newwave/css/carousel.css" rel="stylesheet">
    <link href="https://teledynelecroy.com/newwave/css/newwave.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
 
      <style type="text/css">
      .tab-pane
      {
          margin-top:25px;
          vertical-align:middle;
      }
      </style>
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <a href="https://teledynelecroy.com/" alt=""><img src="https://teledynelecroy.com/images/tl_weblogo_blkblue_189x30.png" class="logo" alt="Teledyne LeCroy"/></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
              <ul>
                <li><a href="/">Products</a></li>
                    <li><a href="/serialdata/">Serial Data</a></li>
                    <li><a href="/support/">Support</a></li>
                    <li><a href="/support/">Buy</a></li>
              </ul>
            </div>
      </div>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading middle">Comprehensive USB 3.1/3.2 Test Suite</h2>
          <p class="lead">Teledyne LeCroy is the only vendor which offers a complete line of USB 3.1/3.2 test solutions covering transmitter, receiver, and protocol compliance, characterization, and debug. Teledyne LeCroy’s coordinated USB 3.2 test solutions ensure the right solution for every stage of development.</p>
          <a href="/doc/usb3x-testsuite-datasheet" type="button" class="btn btn-default"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> USB 3.2 Test Datasheet</a>

          
        </div>

        <div class="col-md-6">
          <img class="featurette-image img-responsive" alt="Comprehensive USB 3.2 Test Suite" style="max-height:500px;" src="/images/usb31-01.png">
        </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Transmitter Testing and Debug</h2>
          <p class="lead"><a href="/options/productseries.aspx?mseries=584&groupid=140">QPHY-USB3.2-Tx-Rx</a> offers a fully automated test package for USB 3.2 transmitter compliance testing, characterization, and debug. With <a href="/options/productseries.aspx?mseries=584&groupid=140">QPHY-USB3.2-Tx-Rx</a> USB 3.2 testing can be performed on both Gen1 (5 Gb/s) and Gen2 (10 Gb/s) devices under test according to the latest USB 3.2 specifications.
</p>
        </div>
        <div class="col-md-7">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#usb1" role="tab" data-toggle="tab">Automatic CP Control</a></li>
  <li><a href="#usb2" role="tab" data-toggle="tab">Complete SigTest Integration</a></li>
  <li><a href="#usb3" role="tab" data-toggle="tab">Advanced Debug Tools</a></li>
  </ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="usb1"><a href="/images/usb31-02.png" rel="shadowbox"><img src="/images/usb31-02.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;"><a href="/options/productseries.aspx?mseries=584&groupid=140">QPHY-USB3.2-Tx-Rx</a> can control the PeRT3 communication with the DUT on the protocol layer by sending a specific number of Ping.LFPS commands in order to stimulate it to output the required CPs for each test.  This enables fully automated transmitter compliance testing.</p></div>
  <div class="tab-pane" id="usb2"><a href="/images/usb31-03.png" rel="shadowbox"><img src="/images/usb31-03.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">SigTest is the official software written by the USB-IF for compliance testing. By integrating SigTest into <a href="/options/productseries.aspx?mseries=584&groupid=140">QPHY-USB3.2-Tx-Rx</a> pre-compliance testing can be confidently performed prior to attending a USB-IF workshop. QPHY will automatically pass waveforms to SigTest and incorporate the results in the complete QualiPHY report. </p></div>
  <div class="tab-pane" id="usb3"><a href="/images/usb31-04.png" rel="shadowbox"><img src="/images/usb31-04.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;"><a href="/sdaiii/">SDAIII-CompleteLinQ</a> is capable of simultaneously analyzing multiple lanes. Four “lanes” of analysis are available, allowing users to analyze data streams from different locations in their USB channel(s). For example, Lane 1 can show the Tx output, Lane 2 can show the signal at the far end of an emulated channel, Lane 3 can show the equalized far end signal, and Lane 4 the far end signal with a different CTLE/DFE equalization scheme.</p></div>
  </div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
        <div class="col-md-5">
          <h2 class="featurette-heading top">Receiver Tolerance Test</h2>
          <p class="lead"><a href="/pert3/pertphoenix.aspx">PeRT3 Phoenix System</a> is the first protocol aware receiver test solution for USB 3.2 Gen2 (10 Gb/s). At 10 Gb/s, USB3.1 uses a more efficient data encoding scheme and delivers more than twice the effective data rate of USB 3.0, presenting new challenges in receiver testing. The Teledyne LeCroy <a href="/pert3/pertphoenix.aspx">PeRT3 Phoenix System</a> simplifies these challenges.</p>
        </div>
        <div class="col-md-7" style="text-align:left;">
          <!-- Nav tabs -->
<ul class="nav nav-pills nav-justified" role="tablist">
  <li class="active"><a href="#usb4" role="tab" data-toggle="tab">Tolerance Test</a></li>
  <li><a href="#usb5" role="tab" data-toggle="tab">Protocol Aware</a></li>
  <li><a href="#usb6" role="tab" data-toggle="tab">Ease of Use</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="usb4"><a href="/images/usb31-06.png" rel="shadowbox"><img src="/images/usb31-06.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">All signal source and jitter parameters can be swept during BER testing. For example, by sweeping the de-emphasis and pre-shoot values, the user can identify the optimized equalization window. Each connect sequence provides the user with a list of options to customize the test criteria and debugging conditions. Pass/Fail information is plotted against the stress parameters under test. See what happened for every test case — in real-time. </p></div>
  <div class="tab-pane" id="usb5"><a href="/images/usb31-05.png" rel="shadowbox"><img src="/images/usb31-05.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;"><a href="/pert3/pertphoenix.aspx">PeRT3 Phoenix System</a> supports all the jitter and equalization generation required of USB 3.2, as well as the protocol level handshake that is critical to put the device under test into proper test modes and optimal equalization conditions. <a href="/pert3/pertphoenix.aspx">PeRT3 Phoenix System</a> also offers true SKP symbol injections and SKP filtering during BER testing, as well as 128b/132b pattern generation and detection.</p></div>
  <div class="tab-pane" id="usb6"><a href="/images/usb31-07.png" rel="shadowbox"><img src="/images/usb31-07.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">With a single integrated system, the <a href="/pert3/pertphoenix.aspx">PeRT3</a> replaces up to 6–8 other instruments. <a href="/pert3/pertphoenix.aspx">PeRT3</a> greatly simplifies experimental setup, increases the ease of designing and conducting fully automated testing. It provides specific test suites to ensure receiver compliance to USB 3.2 specifications.  The simple, intuitive interface for creating automated test scripts can, with a few clicks, set up automated tests that initialize the DUT and record error rates while sweeping through any user defined range of jitter parameters.</p></div>
</div>
<!-- End NAV TABS -->
        </div>
      </div>
      </div>

      <hr class="featurette-divider">
      
      <div class="row">
        <div class="row featurette">
          <div class="col-md-5">
            <h2 class="featurette-heading top">Protocol Compliance Testing and Verification </h2>
            <p class="lead">The <a href="/protocolanalyzer/protocoloverview.aspx?seriesid=477">USB Voyager systems</a> provide users with a multifunction platform with integrated exerciser. The <a href="/protocolanalyzer/protocoloverview.aspx?seriesid=477">Voyager</a> currently supports the Type-C, 3.1 and Power Delivery 2.0 specifications providing the industry’s comprehensive solution for testing next-generation USB devices and systems.</p>
          </div>
          <div class="col-md-7" style="text-align:left;">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified" role="tablist">
              <li class="active"><a href="#usb8" role="tab" data-toggle="tab">Power Delivery 2.0</a></li>
              <li><a href="#usb9" role="tab" data-toggle="tab">Compliance Testing</a></li>
              <li><a href="#usb10" role="tab" data-toggle="tab">CATC TraceTM - the industry's de facto standard</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="usb8"><a href="/images/usb31-08.png" rel="shadowbox"><img src="/images/usb31-08.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">Analyzer supports capture of Power Delivery protocol over the CC wire in USB Type-CTM cables. By utilizing the Exerciser users have the ability to emulate both Sink and Source behaviors. The Power Tracker option provides a time based graphical display of VBus measurements of voltage, current and power. </p></div>
              <div class="tab-pane" id="usb9"><a href="/images/usb31-10.png" rel="shadowbox"><img src="/images/usb31-10.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">Utilizing the Voyager with integrated Exerciser and the USB Compliance Suite, users gain comprehensive link layer test coverage to verify hosts and devices properly implement link training, link recovery and power management behaviors. The USB Voyager is a certified Link Verification System (LVS) used at official Compliance Workshops.</p></div>
              <div class="tab-pane" id="usb10"><a href="/images/usb31-09.png" rel="shadowbox"><img src="/images/usb31-09.png" width="300px" border="0" align="left" hspace="10px" /></a><p style="text-align:left;">The USB Software Suite boasts numerous views and reports including error and traffic summaries, bus utilization, LTSSM and detail views. Software includes comprehensive Device Decoding - SCSI Mass Storage, Hub, PTP, MTP and all popular USB device classes.</p></div>
            <!-- End NAV TABS -->
            </div>
        </div>
      </div>
          </div>
      <hr class="featurette-divider">
      <div class="row">
        <div class="copyright">
            <p class="social">
                <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
                    <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
                    <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
                    <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
                </a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
                    <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
                </a>
            </p>
            <p>&copy; 2019&nbsp;Teledyne LeCroy</p>

            <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

            <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

            <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
            </p>
        </div>
      </div><!-- /.row -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://teledynelecroy.com/newwave/js/bootstrap.min.js"></script>
    <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
    <script type="text/javascript" src="https://teledynelecroy.com/assets/plugins/shadowbox/shadowbox.js"></script> 
    <script type="text/javascript">
        Shadowbox.init();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13095488-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </body>
</html>
