﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/singlecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.waveformgenerators_default" Codebehind="default.aspx.vb" %>
<asp:Content ID="Content4" ContentPlaceHolderID="CenterColumn" runat="server">
    <div id="categoryTop" style="height:250px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="370" valign="top">
                    <h1 style="font-family: Roboto Condensed, sans-serif; font-size: 32px; font-weight: 500; color: #008ecd; margin-top: 25px;">Function / Arbitrary Waveform Generators</h1>
                    <p>Teledyne Test Tools generate fixed or arbitrarily defined waveforms and signals for use in the development, testing and repair of electronic systems.</p>                    </td>
                <td width="248" valign="top" style="padding-top:20px;"><img src="/images/testtools/t3awgseries.png" width="242"  border="0" /></td>
                <td width="203">
                    <div class="subNav">
                        <ul>
                            <li><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=582">Explore T3AWG3K Series Features</a></li>
                            <li><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=627">Explore T3AWG2K Series Features</a></li>
                            <li><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=573">Explore T3AFG Series Features</a></li>
                            <!--<li><a href="<%=rootDir %>/support/softwaredownload/documents.aspx?sc=23">Download Software</a></li>-->
                            <li><a href="<%=rootDir %>/support/techlib/productmanuals.aspx?type=2&cat=2">View Manuals</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="categoryBottom">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="padding:25px 15px;text-align: center;"><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=582"><img src="/images/testtools/t3awgseries.png"><br />T3AWG3K SERIES</a></td>
                
<td style="padding:25px 15px;">                    <h2 style="font-size: 24px; font-weight: 400; color: dimgray;">T3AWG3K Series - High Performance 2, 4 and 8 Channel Arbitrary Waveform Generators</h2>

                    <h3>16-bit Vertical Resolution, 12 Vpp Output Voltage, 250 MHz and 350 MHz models</h3>
                    <p>Exceptional detailed waveform generation with high-performance fidelity, unmatched wide output voltage amplitude (12Vpp) and HW baseline voltage offset (±12 V).</p>
                    <h3>Waveform memory up to 1 Gpoint @Ch </h3>
                    <p>Unmatched deep memory depth allows to store and reproduce complex pseudo-random waveforms for long play time testing.</p>
                    <h3>Mixed Signal Generation</h3>
                    <p>Combine 2, 4 and 8 analog channels with 8, 16 and 32 synchronized digital channels, ideal for debugging and validating digital design.</p>
                </td>
    </tr><tr><td colspan="2" style="background-color: lightgray;height: 1px;"></td></tr>
            <tr>
                <td style="padding:25px 15px;text-align: center;"><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=627"><img src="https://assets.lcry.net/images/t3awg2kseries_front.png"><br />T3AWG2K SERIES</a></td>
                
<td style="padding:25px 15px;">                    <h2 style="font-size: 24px; font-weight: 400; color: dimgray;">T3AWG2K Series - 16-bit Dual Channel Function / Arbitrary Waveform Generators</h2>

                    <h3>16 Bit Vertical Resolution, 6 Vpp Output Voltage, 150 MHz models</h3>
                    <p>Exceptional detailed waveform generation with high-performance fidelity, 6 Vpp at full frequency range and excellent Harmonic Distortion.</p>
                    <h3>Waveform memory 128 Mpoint @Ch</h3>
                    <p>Deep memory for downloading and generating complex pseudo-random both analog and digital waveforms.</p>
                    <h3>Mixed Signal Generation</h3>
                    <p>Combine two analog channels with 8 synchronized digital channels, ideal for debugging and validating digital design.</p>
                </td>
    </tr>
            <tr><td colspan="2" style="background-color: lightgray;height: 1px;"></td></tr>
            <tr><td style="padding:25px 15px;text-align: center;"><a href="<%=rootDir %>/waveform-function-generators/waveform.aspx?mseries=573"><img src="/images/testtools/t3afgseries.png"><br />T3AFG SERIES</a></td>
                <td style="padding:25px 15px;">                    <h2 style="font-size: 24px; font-weight: 400; color: dimgray;">T3AFG Series - Function / Arbitrary Waveform Generator</h2>

                    <h3>Deep Memory</h3>
                    <p>Generate complex arbitrary waveforms with up to 20 Mpts/Ch on 200 MHz to 500 MHz  models, 8 Mpts/Ch 40 MHz to 120 MHz models and 16 kpts on 5 MHz and 10 MHz models.</p>
                    <h3>High Resolution, Bandwidth Models up to 500 MHz</h3>
                    <p>Generate waveforms with low noise and spurious signal content with our wide choice of bandwidths.</p>
                    <h3>Smart Capabilities</h3>
                    <p>Create digital waveforms, patterns and busses of up to 36 channels and output analog and digital waveforms simultaneously.</p>
                    <p>IQ Modulation option available on 200 MHz, 350 MHz and 500 MHz models to support complex applications.</p>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>