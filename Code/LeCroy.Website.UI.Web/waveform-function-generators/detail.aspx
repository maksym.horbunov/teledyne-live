﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage/threecolumn.master" AutoEventWireup="false" Inherits="LeCroy.Website.waveformgenerators_detail" Codebehind="detail.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPage/threecolumn.master" %>
<%@ Register TagPrefix="LeCroy" TagName="Resources" Src="~/UserControls/Resources.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumn" runat="server">
    <ul>
        <asp:Literal ID="lb_leftmenu" runat="server" />
        <asp:Panel ID="pn_leftmenu" runat="server" />
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterColumn" runat="server">
    <asp:Label ID="lbModelImage" runat="server" />
    <div class="content2">
        <h1><asp:Literal ID="landTitle" runat="server" /></h1>
        <p><asp:Literal ID="landContent" runat="server" /></p>
    </div><br />
    <div class="tabs">
        <div class="bg">
            <ul class="tabNavigation"><asp:Literal ID="lblTabsToShow" runat="server" /></ul>
            <div id="product_line"><asp:Label ID="lb_productLine" runat="server" /></div>
            <div id="overview"><asp:Label ID="lb_overview" runat="server" /></div>
            <div id="product_details"><asp:Label ID="lblProductDetail" runat="server" /></div>
            <div id="spec"><asp:Literal ID="lblSpecs" runat="server" /></div>
            <div id="options"><asp:Literal ID="lblOptions" runat="server"/></div>
            <div id="listprice"><asp:Literal ID="lblListPrice" runat="server" /></div>
        </div>
    </div>
    <asp:Literal ID="lbOnLoad" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightColumn" runat="server">
    <div class="greyPadding"></div>
    <LeCroy:Resources ID="ucResources" runat="server" CategoryId="2" ShowBuy="false" ShowRequestDemo="true" ShowRequestQuote="false" ShowWhereToBuy="true" />
    <div class="divider"></div>
    <div class="resources"><asp:Literal ID="lblAdditionalResources" runat="server" /></div>
</asp:Content>