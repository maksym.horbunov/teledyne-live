﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.waveprohd_default" %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>WavePro HD 12 bit Oscilloscope | Teledyne LeCroy</title>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <meta property="og:title" content="Capture every detail with Teledyne LeCroy WavePro HD Oscilloscopes"/>
<meta property="og:image" content="https://assets.lcry.net/images/og-waveprohd-oscilloscope.png"/>
<meta property="og:description" content="Teledyne LeCroy WavePro HD oscilloscopes employ next-generation HD4096 technology, for 12-bit performance at all times. Sample rates of 20 GS/s and bandwidth up to 8 GHz give exceptionally low noise and pristine signal fidelity, and 5 Gpt of available acquisition memory enables the longest capture times." />
<meta property="og:url" content="https://teledynelecroy.com/waveprohd/" />
<meta property="og:type" content="website" />

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet" type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
      .btn-floating {font-size: 18px; font-weight: 500;margin-right:10px;
      }
      .card.larger {height: 600px;
      }
  </style>
</head>
<body>
    <div class="container">
            <a id="logo-container" href="../"><img src="img/tl_weblogo_blkblue_189x30.png" class="brand-logo" alt="Teledyne LeCroy"/></a>
    </div>


  <div id="index-banner" class="parallax-container">
    <div class="section">
      <div class="container">
      <div class="row">
        
        <div class="col l6 s12"><a href="/oscilloscope/wavepro-hd-oscilloscope"><img class="left responsive-img" src="/images/waveprohd-right-legsout-topleft.png" alt="WaveProd HD Oscilloscope"></a></div>
          <div class="col l6 s12"><p class="h1 maui-blue">WavePro HD</p>
            <h3 class="green-text">Capture Every Detail</h3>
              <p class="h3 black-text">8 GHz, 20 GS/s, 5 Gpts</p>
              <p class="h3 black-text">12 bits <strong>all the time</strong></p>
            <p><a class="waves-effect waves-light btn blue darken-2" href="/doc/waveprohd-datasheet" style="margin:5px;"><i class="material-icons left">info</i>Datasheet</a>&nbsp;
                <a class="waves-effect waves-light btn green" href="/oscilloscope/configure/configure_step2.aspx?seriesid=566" style="margin:5px;"><i class="material-icons left">settings</i>Configure</a>&nbsp;
                <a class="waves-effect waves-light btn orange darken-3" href="#videos"
               style="margin:5px"><i class="material-icons left">play_circle_filled</i>Videos</a>&nbsp;<a class="waves-effect waves-light btn blue darken-3"
               href="#docs"
               style="margin:5px;"><i class="material-icons left">reorder</i>Technical Docs</a></p>
              <p><span class="wistia_embed wistia_async_mus2tis6k0 popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:183px;position:relative;width:325px">&nbsp;</span></p>
        </div>
      </div>
      </div>
    </div>
  </div>

<div class="clearfix"></div>
  <div class="wrap">
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
            <div class="row">
        <div class="col s12 m4">
          <div class="card larger">
            <div class="card-image">
              <img class="responsive-img center" src="/images/hd4096-itransport.png" style="padding: 0 25px 0 25px;height: 290px;width: 390px;">
            </div>
            <div class="card-content" style="max-height: 100%">
            <h5 class="center green-text text-accent-4">HD4096 High Definition Technology</h5>

            <p style="padding-bottom: 0px;">HD4096 technology enables 12 bits of vertical resolution with 8 GHz bandwidth</p>
                <ul><li>&#9642; Clean, Crisp Waveforms</li>
<li>&#9642; More Signal Details</li>
<li>&#9642; Unmatched Measurement Precision</li>
</ul><a href="/hd4096/">Learn more...</a>
            </div>
          </div>
        </div>
                <div class="col s12 m4">
          <div class="card larger hoverable">
            <div class="card-image">
<img class="center" src="/images/long-memory.png">            </div>
            <div class="card-content"><h5 class="center green-text text-accent-4">Longest Memory</h5>

            <p>Up to 5 Gpt of acquisition memory means exceptionally long capture times at full sample rate and resolution.</p><p>Intuitive navigation tools make it easy to find events of interest and simplify analysis of long waveforms.
</p><a href="#one">Learn more...</a>
            </div>

          </div>
        </div>
                <div class="col s12 m4">
          <div class="card larger">
            <div class="card-image">
            <img class="responsive-img center" src="/images/obsessed-with-tools-elements.png">
            </div>
            <div class="card-content"><h5 class="center green-text text-accent-4">Deep Toolbox</h5>

            <p style="padding-bottom: 0px;">WavePro HD features the comprehensive toolset  common to all Teledyne LeCroy oscilloscopes:</p>
                <ul><li>&#9642; Measurements</li>
<li>&#9642; Math functions</li>
<li>&#9642; Analysis packages</li>
<li>&#9642; Documentation</li>
</ul><a href="/tools/">Learn more...</a>
            </div>

          </div>
        </div>
      </div>
      

    </div>
  </div>
</div>

<div class="clearfix"></div>

  <div class="container">
    <div class="section">
    	<div class="card-panel green lighten-5">
    <div class="row">
     <span class="grey-text text-darken-4" style="font-size: larger;">Today’s deeply-embedded electronic systems feature analog sensor inputs, highly sensitive Power Distribution Networks (PDNs), and High-speed CPUs and serial data interfaces. WavePro HD’s combination of High resolution and low noise, high analog bandwidth, very long capture times, and a complete software toolset makes it ideal for these key applications.
</span>
    </div><div class="row">
      <div class="col s12 m3">
        <div class="card hoverable">
          <div class="card-image">
            <a href="#two"><img src="/images/waveprodhd-lp07.png">
          <span class="card-title">Embedded Systems Debug</span></a>
          </div>
        </div>
      </div>

      <div class="col s12 m3">
        <div class="card hoverable">
          <div class="card-image">
            <a href="#three"><img src="/images/waveprodhd-lp08.png">
          <span class="card-title">Power Integrity</span></a>
          </div>
        </div>
      </div>

      <div class="col s12 m3">
        <div class="card hoverable">
          <div class="card-image">
            <a href="#four"><img src="/images/waveprodhd-lp09.png">
          <span class="card-title">High-speed serial test</span></a>
          </div>
        </div>
      </div>

      <div class="col s12 m3">
        <div class="card hoverable">
          <div class="card-image">
            <a href="#five"><img src="/images/waveprodhd-lp10.png">
          <span class="card-title">EMC/EMI</span></a>
          </div>
        </div>
      </div>                  
    </div>

        </div>
        </div>
        </div>

    <div class="clearfix"></div>

<div class="wrap">
<div class="container">
    <div class="row">

        <div class="col m7">
          <div class="card">
            <div class="card-image">

              <div class="material-placeholder"><img class="materialboxed initialized" src="/images/wavepro-hd-front-callouts.png"></div></div>
          </div>
        </div>

  <div class="col m5">
  <ul class="collection z-depth-1"> <li class="collection-item">
      <button class="btn-floating btn-small green left">1</button>
      <span>HD4096 technology provides 12-bit resolution up to 8 GHz and 20 GS/s</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">2</button>
      <span>Up to 5 Gpts of acquisition memory enables detailed viewing of long events</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">3</button>
      <span>15.6” 1900 x 1080 Full HD capacitive touchscreen</span><br />&nbsp;</li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">4</button>
      <span>ProBus2 input supports up to 8 GHz bandwidth while maintaining support for legacy ProBus probes</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">5</button>
      <span>MAUI with OneTouch user interface for intuitive and efficient operation</span></li>
    <li class="collection-item">
      <button class="btn-floating btn-small green left">6</button>
      <span>Waveform Control Knobs – Control channel, zoom, math and memory traces with the multiplexed vertical and horizontal knobs</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">7</button>
      <span>Color-coded panel indicators – Trigger, horizontal and vertical indicator colors correspond to the associated waveform on the display</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">8</button>
      <span>Cursor/Adjust Knobs – Enable and position cursors or adjust settings and parameters without opening a menu</span></li>
      <li class="collection-item">
      <button class="btn-floating btn-small green left">9</button>
      <span>Mixed Signal Capability – Debug complex embedded designs with integrated 16-channel mixed signal capability</span></li>
<li class="collection-item">
      <button class="btn-floating btn-small green left">10</button>
      <span>Easy connectivity with seven USB 3.1 ports (3 front, 4 side) and UHD (4k) HDMI and DisplayPort outputs</span></li>
  </ul>
        </div></div>

    </div>
  </div><div class="clearfix"></div>
    <div class="wrap">
  <div class="container">
    <div class="section" id="videos">
            <div class="row"><h3 class="center">Videos</h3>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_mus2tis6k0 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_egnoeri4ql popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_57uj9ezfbj popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                </div>
            </div>
        </div>
<div class="clearfix"></div>
<div class="container">
             <div class="section" id="one">
                 <div class="row">
  <div class="col m8 s12"><h3>Long Memory, No Compromise</h3>
  <p>With up to 5 Gpts of acquisition memory, WavePro HD 12-bit oscilloscopes capture events occurring over long periods of time, while still maintaining high sample rate for visibility into the smallest details.</p>
</div></div><div class="row">
<div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Longest memory</strong></h6><p> WavePro HD oscilloscopes contain a sophisticated acquisition and memory management architecture that makes 5 Gpt acquisitions fast and responsive. More memory means more visibility into system behavior.</p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp12.png" width="300px"></div></div></div>
<div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Simple navigation</strong></h6><p> Long memory and high sample rates capture both millisecond-scale trends and picosecond-scale glitches. WavePro HD oscilloscopes contain an advanced user interface that makes it easy to find features, navigate directly using timebase scale and position knobs, or set up zoom traces - whichever you prefer - and apply analysis.</p></div></div></div>
<div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>No compromise</strong></h6><p> WavePro HD can acquire 250 ms of data at full 20 GS/s sample rate - and always with 12 bits of resolution. Oscilloscopes with less memory require trading off sample rate for acquisition time.</p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp13.png" width="300px"></div></div></div>
  </div></div>

<div class="divider"></div>

<div class="section" id="two">
  <div class="row"><div class="col m10 s12"><h3>Deeply Embedded Computing Systems Testing</h3><p>WavePro HD has unsurpassed capabilities to acquire the longest records at the highest resolution for the most comprehensive deeply embedded computing system (analog, digital, serial data and sensor) testing.</p></div></div>
  <div class="row">
    <div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Powerful, deep toolbox</strong></h6><p> More standard math, measure, pass/ fail and other toolsets provide faster and more complete insight into circuit problems. Many additional application packages are optionally available to enhance understanding.
    </p><p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp14.png" width="220" class="center"></p></div></div></div>
<div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Superior serial data toolsets</strong></h6><p> Comprehensive low-speed serial data triggers and decoders, plus measure/ graph and eye diagram testing provide the best causal analysis. Powerful serial data jitter analysis toolsets and compliance packages simplify complex validation.
    </p></div></div></div>
<div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Comprehensive probe offering</strong></h6><p> A wide selection of low voltage, high voltage and current probes will accurately measure every signal in your circuit. New 8 GHz ProBus2 interface is backwards-compatible to the 20+ year legacy of ProBus-compatible probes.
    </p><p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp15.png" width="300px"></p></div></div></div></div>
  </div>
<div class="divider"></div>

<div class="section" id="three">
  <div class="row"><div class="col m8 s12"><h3>Power Integrity Debug and Validation</h3><p>WavePro HD's combination of high bandwidth and high resolution provides the capability to validate and debug all aspects of power supply, delivery and consumption - ensuring complete confidence.</p></div>
      <div class="col m4 s12"><a href="https://go.teledynelecroy.com/l/48392/2018-07-10/6yl9rg"><img src="/images/power-integrity203.png" class="responsive-img" /></a></div>
  </div>
  <div class="row">
    <div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>On-die ground bounce</strong></h6><p>WavePro HD's high bandwidth means accurate characterization of high-speed on-die effects such as ground bounce, while its exceptionally low noise enables identification and root-cause analysis of low-level noise sources.</p><p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp16.png" width="220"></p></div></div></div>
    <div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Find sources of PDN noise</strong></h6><p>Sensitive measurements such as rail collapse characterization can be made with complete confidence thanks to WavePro HD's high dynamic range and 0.5% gain accuracy. And its low noise floor enables extremely detailed spectral analysis of the PDN noise environment.
    </p></div></div></div>
    <div class="col m4 s12"><div class="card medium grey lighten-5"><div class="card-content"><h6><strong>Specialized power probes</strong></h6><p>The combination of WavePro HD and the RP4030 4 GHz Power Rail Probe gives unsurpassed insight into PDN behavior over the widest available bandwidth. A variety of probe tips ensure easy connectivity.
    </p><p><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp17.png" width="300"></p></div></div></div></div>
  </div>
<div class="divider"></div>

<div class="section" id="four">
  <div class="row"><div class="col m8 s12"><h3>Serial Data Jitter and Noise Analysis</h3><p>WavePro HD 12-bit oscilloscopes bring the high signal fidelity of HD4096 technology to high-speed serial
  data analysis, enabling precise measurements with exceptionally low noise and jitter.</p></div><div class="col m4 s12"><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp18.png" class="right" width="300"></div></div></div>
  <div class="row">
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>High precision, low jitter</strong></h6><p>WavePro HD's 12-bit resolution, exceptionally low noise and 60 fs timebase jitter mean a low jitter measurement floor, enabling the most accurate serial data jitter and noise measurements possible.</p></div></div></div>
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>Serial data insight</strong></h6><p>SDAIII CompleteLinQ provides the most complete set of serial data analysis tools available. Measure and decompose jitter and noise, compare eye diagrams, and leverage unique visualization tools to track down issues.</p></div></div></div>
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>Compliance made easy</strong></h6><p>User-friendly QualiPHY serial data compliance packages make validation easy for interfaces such as DDR memory, 10/100/1000BaseT Ethernet, USB, and many more.
    </p></div></div></div>
  </div>
             
            <div class="divider"></div>
             <div class="section" id="five">
  <div class="row"><div class="col m8 s12"><h3>Electromagnetic Compatibility (EMC/EMI)</h3><p>WavePro HD 12-bit oscilloscopes' high sample rate and long memory combine with Teledyne LeCroy's
dedicated EMC pulse parameter package to accurately characterize EMC test signals.</p></div>
<div class="col m4 s12"><img class="materialboxed responsive-img center" src="/images/waveprodhd-lp19.png" class="right" width="300"></div></div>
  <div class="row">
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>Pulse measurement fidelity</strong></h6><p>Fast pulse rise times may require 2.5 to 4 GHz bandwidth at very high sample rates to ensure measurement confidence. WavePro HD provides the most accurate characterization using 20 GS/s sample rate, 12-bit resolution, and 0.5% gain accuracy.</p></div></div></div>
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>Long capture time</strong></h6><p>WavePro HD combines high sample rate and exceptionally long memory to enable measurement of many fast transient packets in one acquisition, for fast and simple pulse train and transient testing.</p></div></div></div>
    <div class="col m4 s12"><div class="card small grey lighten-5"><div class="card-content"><h6><strong>EMC pulse parameter package </strong></h6><p>Customizable measurements provide values per specific EMC/ESD standards. Level selections can be made to ignore undershoot, overshoot or tail perturbations. Measurement filtering can limit measurement sets or ignore unwanted perturbations. (Optional)</p></div></div></div></div>
  </div>
  </div><!-- end of container --> 
<!--TECH DOCS-->
        <div class="container header-brief" id="docs"> 
 <ul class="collection with-header" style="-webkit-margin-after:0em;">
	<li class="collection-header blue darken-3 white-text"><h4>Technical Docs</h4></li>
	<li class="collection-item"><a href="/doc/wavepro-hd-v-keysight-s-series" target="_blank">
		<span class="title">WavePro HD vs Keysight S Series</span>
		<i class="material-icons secondary-content">description</i></a>
		<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WavePro HD oscilloscopes to the Keysight S Series oscilloscopes.</p></li>
<li class="collection-item"><a href="/doc/wavepro-hd-v-rto2000" target="_blank"><span class="title">WavePro HD vs Rohde & Schwarz RTO2000</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WavePro HD oscilloscopes to the Rohde & Schwarz RTO2000 oscilloscopes.</p></li>
     <li class="collection-item"><a href="/doc/wavepro-hd-v-tek6series" target="_blank"><span class="title">WavePro HD vs Tektronix 6 Series</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WavePro HD oscilloscopes to the Tektronix 6 Series oscilloscopes.</p></li>
     </ul></div>



<footer class="page-footer white">
      <div class="container center social">
    <p>
      <a href="https://facebook.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/facebook.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://twitter.com/teledynelecroy" target="_blank" onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');">
        <img src="https://assets.lcry.net/images/twitter.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://youtube.com/lecroycorp" target="_blank" onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');">
        <img src="https://assets.lcry.net/images/youtube.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://blog.teledynelecroy.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');">
        <img src="https://assets.lcry.net/images/blogger.jpg" border="0" width="25px" height="25px">
      </a>&nbsp;&nbsp;
      <a href="https://www.linkedin.com/company/10007" target="_blank" onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');">
        <img src="https://assets.lcry.net/images/linkedin.jpg" border="0" width="25px" height="25px">
      </a>
    </p>
    <p class="grey-text">&copy; 2020&nbsp;Teledyne LeCroy</p>

    <a id="ucFooter_rptLinks_hlkLink_0" target="_blank" href="/support/securitypolicy.aspx">Privacy Policy</a> |

    <a id="ucFooter_rptLinks_hlkLink_1" target="_blank" href="/sitemap/">Site Map</a>

    <p><a href="http://teledyne.com/" target="_blank" onclick="GaEventPush('OutboundLink', 'http://teledyne.com');">Teledyne Technologies</a>
    </p>
      </div>
  </footer>


  <!--  Scripts-->
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/init.js"></script>
  <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>
      <script>
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
    </script>
    <script src="https://teledynelecroy.com/js/pardot.js"></script>

  </body>
</html>
