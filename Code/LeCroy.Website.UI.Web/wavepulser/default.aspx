﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.wavepulser_default" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>WavePulser 40iX High-speed Interconnect Analyzer | Teledyne LeCroy</title>
  <meta content="IE=Edge"
        http-equiv="X-UA-Compatible">
  <meta content="text/html; charset=utf-8"
        http-equiv="Content-Type">
  <meta content="width=device-width, initial-scale=1"
        name="viewport"><!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <link href="css/materialize.min.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="css/style.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css"><!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700"
        rel="stylesheet"
        type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
      .btn-floating {font-size: 18px; font-weight: 500;margin-right:10px;
      }
      .card.larger {height: 675px;
      }
      .radial {color:black; 
          background: rgb(255,255,255);
          background: radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(0,118,192,1) 95%);
      }
  </style>
    <meta property="og:title" content="Teledyne LeCroy WavePulser 40iX High-speed Interconnect Analyzer"/>
<meta property="og:image" content="https://assets.lcry.net/images/wavepulser-og-image.png"/>
<meta property="og:description" content="WavePulser 40iX High-speed Interconnect Analyzer lets you test in both frequency and time domains for unmatched characterization insight." />
<meta property="og:url" content="https://teledynelecroy.com/wavepulser/" />
    <meta property="og:type" content="website" />


</head>
<body>
  <div class="container">
      <a href="../" id="logo-container"><img alt="Teledyne LeCroy" class="brand-logo" src="img/tl_weblogo_blkblue_189x30.png"></a>
  </div>
  <div class="container">

        <div class="row">
         <div class="col l6 s12">
            <h1 class="maui-blue" style="font-size:3.2rem">WavePulser 40iX High-speed Interconnect Analyzer</h1>
            <h3 class="green-text">Unmatched Characterization Insight</h3>
             <p class="black-text" style="font-size:1.5rem">The combination of S-parameters (frequency domain) and Impedance Profiles (time domain) in a single acquisition with a deep toolbox for simulation, emulation, de-embedding and time-gating provides unmatched characterization insight.</p>
             </div>
            <div class="col l6 s12">
            <a href="/tdr-and-s-parameters/series.aspx?mseries=592"><img alt="WavePulser 40iX Analyzer"
                 class="left responsive-img"
                 src="/images/wavepulser-ix-top.png"></a>
          </div></div>
      <div class="row"><div class="col l8 s12">
            
            <p><a class="waves-effect waves-light btn blue darken-2" href="/doc/wavepulser40ix-datasheet" style="margin: 5px; width: 148px;height: 42px; line-height: 42px;">
                <i class="material-icons left">info</i>Datasheet</a>&nbsp;
                <a class="waves-effect waves-light btn green" href="/tdr-and-s-parameters/series.aspx?mseries=592" style="margin: 5px; width: 148px;height: 42px; line-height: 42px;">
                <i class="material-icons left">settings</i>Configure</a>&nbsp;
                <a class="waves-effect waves-light btn orange darken-3" href="#videos" style="margin: 5px; width: 148px;height: 42px; line-height: 42px;">
                <i class="material-icons left">play_circle_filled</i>Videos</a><br />
                <a class="waves-effect waves-light btn blue darken-2" href="#webinar" style="margin: 5px; width: 148px;height: 42px; line-height: 42px;">
                <i class="material-icons left">ondemand_video</i>Webinars</a>&nbsp;
                <a class="waves-effect waves-light btn blue darken-2" href="#docs" style="margin: 5px; height: 42px; line-height: 42px;">
                <i class="material-icons left">reorder</i>Technical Docs</a>
            </p>
              <p><span class="wistia_embed wistia_async_04m8mhgdks popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:205px;position:relative;width:365px">&nbsp;</span></p>
          </div>
          </div>
        </div>

  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="section">
        <!--   Icon Section   -->
        <div class="row">
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="/images/40ix-01.png" alt="wavepulser s-parameters"></div>
              <div class="card-content"
                   style="max-height: 100%">
                <h5 class="center green-text text-accent-4">S-parameters</h5>
                <p style="padding-bottom: 0px;">Get a complete characterization of the signal path in one acquisition:</p>
                <ul>
                  <li>&#9642; Frequency range DC to 40 GHz</li>
                  <li>&#9642; Single-ended and mixed-mode</li>
                  <li>&#9642; Internal, automatic calibration</li>
                </ul><a href="#parameters">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center" src="/images/40ix-02.png" alt="wavepulser profiles"></div>
              <div class="card-content">
                <h5 class="center green-text text-accent-4">Impedance Profiles</h5>
                <p style="padding-bottom: 0px;">Precisely locate impairments in the circuit:</p>
                <ul>
                  <li>&#9642; Spatial resolution < 1 mm</li>
                  <li>&#9642; Differential and common-mode</li>
                  <li>&#9642; TDR and TDT capability</li>
                </ul><a href="#profile">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center" src="/images/40ix-03.png" alt="wavepulser toolbox"></div>
              <div class="card-content">
                <h5 class="center green-text text-accent-4">Deep Toolbox</h5>
                <p>WavePulser measurements are ready for simulation using built-in SI Studio and other tools:</p>
                <ul>
                  <li>&#9642; De-embedding and time-gating</li>
                  <li>&#9642; Eye diagram with equalized emulation</li>
                  <li>&#9642; Advanced jitter analysis</li>
                </ul><a href="#toolbox">Learn more...</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="clearfix"></div>
      <div class="radial">
  <div class="container">
    <div class="section" id="videos">
            <div class="row"><h3 class="center">Videos</h3></div>

                <div class="row"><div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_04m8mhgdks popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_8cvy81lh9d popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_zxqooi8p3m popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                  </div> <div class="row"> <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_o47mluiwb3 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m4 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_1thr024zg3 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
            </div>
            </div></div></div>
  <div class="clearfix"></div>
  <div class="container">
<div class="section"
         id="one">
      <div class="row">
        <div class="col m8 s12">
          <h3>Test in Both Frequency and Time Domains</h3>
          <p>WavePulser 40iX High-speed Interconnect Analyzer is the ideal, single measurement tool for high-speed hardware designers and test engineers, with all the features and capabilities that VNAs and TDRs lack. Make one acquisition, then measure in both frequency and time domains. </p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/40ix-04.png"
             width="300px" alt="WavePulser 40iX Analyzer Testing"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Designed for high-speed interconnect analysis</strong></h6>
              <p>WavePulser 40iX validates, debugs and troubleshoots interconnectivity issues in serial data cables, channels, connectors, vias, backplanes, printed-circuit boards, and chip and SoC packages. It is simple to set up and use.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Internal, automatic calibration</strong></h6>
              <p>WavePulser 40iX calibration standards are built-in, so calibration is always automated, simple and fast. There is no need to purchase additional, external calibration standards. The TDR/TDT-based approach is independent of setup, making calibration less likely.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Full range DC to 40 GHz</strong></h6>
              <p>WavePulser 40iX delivers TDR step response and time-gated and/or emulated physical-layer responses with no need for extrapolation to DC and low frequencies ideal for interconnection systems.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="wrap" id="webinar">
    <div class="container"><h3 class="center">On-Demand Webinars</h3>
        <p style="font-size: 1.5rem; line-height:120%">Dr. Eric Bogatin, "Signal Integrity Evangelist", explains S-parameter and TDR measurements.</p>
      <div class="row">
        <div class="col m10"> 
            <!--<p style="font-size: 1.5rem">Watch now</p>-->
          <div class="collection">
              
<a href="https://go.teledynelecroy.com/l/48392/2020-06-10/7yq8vy" class="collection-item"><i class="material-icons left">ondemand_video</i>S-Parameter Master Class Part 3 - Hands on Lab</a>
<a href="https://go.teledynelecroy.com/l/48392/2020-06-10/7yq8gk" class="collection-item"><i class="material-icons left">ondemand_video</i>S-Parameter Master Class Part 2 - Hands on Lab</a>
<a href="https://go.teledynelecroy.com/l/48392/2020-05-15/7yfrt2" class="collection-item"><i class="material-icons left">ondemand_video</i>S-Parameter Master Class Part 1 - Hands on Lab</a>
        <a href="https://lcry.us/2LEFi8t" class="collection-item"><i class="material-icons left">ondemand_video</i>Beyond the TDR</a>
        <a href="https://go.teledynelecroy.com/l/48392/2019-07-18/7rb412" class="collection-item"><i class="material-icons left">ondemand_video</i>How to Read S-parameters Like a Book</a>
        <a href="https://go.teledynelecroy.com/l/48392/2019-09-06/7s2snz" class="collection-item"><i class="material-icons left">ondemand_video</i>Lossy Interconnects and Insertion Loss</a>
        <a href="https://go.teledynelecroy.com/l/48392/2019-12-19/7vr4l4" class="collection-item"><i class="material-icons left">ondemand_video</i>Mixed mode S-parameters and TDR responses</a>
              <a href="https://go.teledynelecroy.com/l/48392/2019-12-20/7vr8h2" class="collection-item"><i class="material-icons left">ondemand_video</i>Principles of De-embedding</a>
              <a href="https://go.teledynelecroy.com/l/48392/2020-02-11/7wpz7z" class="collection-item"><i class="material-icons left">ondemand_video</i>Using S-parameters as a Behavioral Model in System Simulation</a>
      </div>
        </div>
       <!-- FOR FUTURE WEBINARS <div class="col m6">
            <p style="font-size: 1.5rem">Coming soon</p>
          <div class="collection">
                      
        <a href="https://go.teledynelecroy.com/l/48392/2020-02-11/7wpz7z" class="collection-item"><i class="material-icons left">launch</i>Using S-parameters as a Behavioral Model in System Simulation</a>

      </div>
        </div>-->
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="section"
         id="parameters">
      <div class="row">
        <div class="col m8 s12">
          <h3>S-parameters</h3>
          <p>WavePulser 40iX High-speed Interconnect Analyzer calculates both single-ended and mixed-mode S-parameters from one acquisition. Just change settings and recalculate results without having to reacquire.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/40ix-05.png"
             width="300px" alt="wavepulser s-parameters"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Mixed-mode S-parameters</strong></h6>
              <p>One acquisition displays all measurement results: mixed-mode return and insertion losses for all ports; differential-mode and common-mode measurements; DC frequency response. A tabular graphical user interface makes reading results straightforward.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Simple and Flexible Setup</strong></h6>
              <p>A simple setup requires you to enter only frequencies and number of ports for a single-ended acquisition. Choose a test time optimized for accuracy or speed or something in between. Reconfigure ports in software without reconnecting to the DUT. Reorder S-parameters in the Touchstone file.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Higher Accuracy</strong></h6>
              <p>Internal, electronic calibration permits measurements to begin sooner and be made with more confidence. Capabilities such as passivity, reciprocity and causality enforcement ensure higher measurement accuracy.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="profile">
      <div class="row">
        <div class="col m8 s12">
          <h3>Impedance Profiles</h3>
          <p>WavePulser 40iX High-speed Interconnect Analyzer calculates impedance profiles with < 1 mm spatial resolution.
</p></div>
 <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/40ix-06.png"
             width="300px" alt="wavepulser profiles"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Multiple Views</strong></h6>
              <p>WavePulser 40iX supports both differential-mode and mixed-mode measurements and can display multiple modes simultaneously. View step-response, pulse-response and reflection coefficient, as well.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Precisely Locate Impairments</strong></h6>
              <p>Use impedance profiles to detect and locate common problems in high-speed interconnects: improperly tightened connectors; damaged cables; incorrect cable-bend radiuses; defective vias on transmissions lines; other transmission line irregularities.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Optimize Efficiency</strong></h6>
              <p>Impedance profiles detect and locate impairments in the measurement setup, not just on the DUT, helping you work more efficiently. Understand when it is necessary to repeat calibration, and when it is not.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="toolbox">
      <div class="row">
        <div class="col m8 s12">
          <h3>Deep Toolbox</h3>
          <p>WavePulser 40iX High-speed Interconnect Analyzer can be purchased bundled with SI Studio™ (WavePulser-40iX-BUNDLE), a deep analysis toolbox tailored to understanding high-speed interconnect characteristics.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/40ix-07.png"
             width="300px" alt="wavepulser toolbox"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Time-gating and De-embedding</strong></h6>
              <p>Eliminate the effects of cables and connectors from results. Set gating through port extension or an impedance peeling algorithm, and save S-parameters with or without the gating region. De-embed serial data channels using modeled or measured S-parameters.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Fast Eye Diagram Views</strong></h6>
              <p>Import or simulate waveforms and use S-parameters to model impairments. Quickly view the impact of impairments with an intuitive serial data eye diagram. View the effects of de-embedding and equalization on the eye diagram. Supports PLL, pre-emphasis, de-emphasis, CTLE, FFE and DFE.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Advanced Jitter Analysis</strong></h6>
              <p>Measure total (Tj), random (Rj) and deterministic (Dj) jitter. De-convolve Dj into component parts. View jitter in spectral, histogram, jitter track, eye diagram and other plots.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div><!-- end of container -->
     <!--TECH DOCS-->
        <div class="container header-brief" id="docs"> 
 <ul class="collection with-header" style="-webkit-margin-after:0em;">
	<li class="collection-header blue darken-3 white-text"><h4>Technical Docs</h4></li>
<li class="collection-item"><a href="/doc/de-embedding-with-wavepulser" target="_blank"><span class="title">De-embedding with the WavePulser 40iX</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">The WavePulser 40iX offers many de-embedding methods. This technical brief explores these methods and help you obtain the best results.</p></li>
 <li class="collection-item"><a href="/doc/pulse-repetition-rate-and-frequency-resolution" target="_blank">
<span class="title">Pulse Repetition Rate and Frequency Resolution</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This paper covers important topics in s-parameter measurements made for signal integrity applications, where the time-domain implications are important.</p></li>
 <li class="collection-item"><a href="/doc/time-domain-de-embedding-and-peeling" target="_blank">
<span class="title">Time-Domain Techniques for De-embedding and Impedance Peeling</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This brief explains time-domain techniques for de-embedding that are available within the WavePulser 40iX called time gating and impedance peeling.</p></li>
 <li class="collection-item"><a href="/doc/second-tier-calibration" target="_blank">
<span class="title">WavePulser 40 iX Second Tier Calibration</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This paper discusses the three methods of calibration utilized in the WavePulser 40iX including the automatic internal calibration, manual calibration and second-tier calibration.</p></li>
<li class="collection-item"><a href="/doc/wavepulser-dynamic-range" target="_blank">
<span class="title">WavePulser 40iX Dynamic Range</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This paper explains the dynamic range of the Teledyne LeCroy WavePulser 40iX High Speed Interconnect Analyzer and compares it to our previous product, the SPARQ.</p></li>
<li class="collection-item"><a href="/doc/rise-time-and-spatial-resolution" target="_blank">
<span class="title">WavePulser 40iX Risetime and Spatial Resolution</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">The complex relationship between risetime, bandwidth, and temporal and spatial resolution is explored for various types of systems</p></li>
   </ul>
        </div>
    <footer class="page-footer white">
      <div class="container center social">
        <p><a href="https://facebook.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/facebook.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://twitter.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/twitter.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://youtube.com/lecroycorp"
           onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/youtube.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://blog.teledynelecroy.com/"
           onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/blogger.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://www.linkedin.com/company/10007"
           onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/linkedin.jpg"
             width="25px"></a></p>
        <p class="grey-text">&copy; 2020&nbsp;Teledyne LeCroy</p><a href="/support/securitypolicy.aspx"
             id="ucFooter_rptLinks_hlkLink_0"
             target="_blank">Privacy Policy</a> | <a href="/sitemap/"
             id="ucFooter_rptLinks_hlkLink_1"
             target="_blank">Site Map</a>
        <p><a href="http://teledyne.com/"
           onclick="GaEventPush('OutboundLink', 'http://teledyne.com');"
           target="_blank">Teledyne Technologies</a></p>
      </div>
    </footer>
  <script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async></script>
    <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
  <script src="js/materialize.min.js"></script> 
  <script src="js/init.js"></script> 
  <script async
        charset="ISO-8859-1"
        src="//fast.wistia.com/assets/external/E-v1.js"></script> 
  <script>



          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
  </script> 
  <script src="https://teledynelecroy.com/js/pardot.js"></script>

</body>
</html>
