﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.waveprohd_default" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>WaveRunner 8000HD 12 bit Oscilloscope | Teledyne LeCroy</title>
  <meta content="IE=Edge"
        http-equiv="X-UA-Compatible">
  <meta content="text/html; charset=utf-8"
        http-equiv="Content-Type">
  <meta content="width=device-width, initial-scale=1"
        name="viewport"><!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <link href="css/materialize.min.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="css/style.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css"><!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700"
        rel="stylesheet"
        type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
      .btn-floating {font-size: 18px; font-weight: 500;margin-right:10px;
      }
      .card.larger {height: 680px;
      }
      span.title {font-size:1.2em; font-weight: 500;color: #26a69a;}
.desc-tab {padding:0 15px;}
  </style>
    <meta property="og:title" content="Teledyne LeCroy WaveRunner 8000HD Oscilloscopes"/>
<meta property="og:image" content="https://teledynelecroy.com/images/og-wr8000hd-oscilloscope.jpg"/>
<meta property="og:description" content="Explore the Teledyne LeCroy WaveRunner 8000HD High Definition Oscilloscopes. The world’s only 2 GHz, 12 bit, 8 channel oscilloscope." />
<meta property="og:url" content="https://teledynelecroy.com/wr8000hd/" />
<meta property="og:type" content="website" />
<link href="https://teledynelecroy.com/wr8000hd/" rel="canonical"  />
</head>
<body>
  <div class="container">
    <a href="../"
         id="logo-container"><img alt="Teledyne LeCroy"
         class="brand-logo"
         src="img/tl_weblogo_blkblue_189x30.png"></a>
  </div>
  <div class="parallax-container"
       id="index-banner">
    <div class="section">
      <div class="container">
        <div class="row">
            
          <div class="col l6 s12">
            <h1 class="maui-blue">WaveRunner 8000HD High Definition Oscilloscopes</h1>
            <h3 class="green-text">Capture Every Detail</h3>
            <p class="h3 black-text">8 channels, 2 GHz, 5 Gpts</p>
            <p class="h3 black-text">12 bits <strong>all the time</strong></p>
            <p><a class="waves-effect waves-light btn blue darken-2"
               href="/doc/waverunner-8000hd-datasheet"
               style="margin:5px;"><i class="material-icons left">info</i>Datasheet</a>&nbsp;<a class="waves-effect waves-light btn green"
               href="/oscilloscope/configure/configure_step2.aspx?seriesid=607"
               style="margin:5px;"><i class="material-icons left">settings</i>Configure</a>&nbsp;<a class="waves-effect waves-light btn orange darken-3"
               href="#videos"
               style="margin:5px"><i class="material-icons left">play_circle_filled</i>Videos</a>&nbsp;<a class="waves-effect waves-light btn blue darken-3"
               href="#docs"
               style="margin:5px;"><i class="material-icons left">reorder</i>Technical Docs</a></p>
              <p><span class="wistia_embed wistia_async_ysin5rn3a0 popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:183px;position:relative;width:325px">&nbsp;</span></p>
          </div>
          <div class="col l6 s12">
            <a href="/oscilloscope/waverunner-8000hd-oscilloscopes"><img alt="WaveRunner 8000HD Oscilloscope"
                 class="left responsive-img"
                 src="/images/waverunner8000hd.png"></a>
          </div>
        </div>
      </div>
    </div>
      </div>
  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="section">
        <!--   Icon Section   -->
        <div class="row">
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="/images/hd4096-itransport-ws4k.png" alt="waverunner 8000hd oscilloscope hd4096"></div>
              <div class="card-content"
                   style="">
                <h5 class="center green-text text-accent-4">Highest Resolution</h5>
                <p style="padding-bottom: 0px;">HD4096 technology enables 12 bits of vertical resolution with 8 GHz bandwidth</p>
                <ul>
                  <li>&#9642; Clean, Crisp Waveforms</li>
                  <li>&#9642; More Signal Details</li>
                  <li>&#9642; Unmatched Measurement Precision</li>
                </ul><a href="/hd4096/">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="/images/wr8000hd-most-channels.png" alt="waverunner 8000hd oscilloscope channels"></div>
              <div class="card-content" style="">
                <h5 class="center green-text text-accent-4">Most Channels</h5>
                <p style="padding-bottom: 0px;">The most channels of any oscilloscope, with no analog-digital tradeoffs</p>
                <ul>
                  <li>&#9642; 8 analog channels</li>
                  <li>&#9642; 16 integrated digital channels</li>
                  <li>&#9642; Up to 16 analog channels with free OscilloSYNC&trade; technology</li>
                </ul><a href="#sync">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="/images/long-memory.png" alt="waverunner 8000hd oscilloscope long memory"></div>
              <div class="card-content" style="margin-top: 60px;">
                <h5 class="center green-text text-accent-4">Longest Memory</h5>
                <p>Exceptionally long capture times at full sample rate and resolution</p>
                <ul>
                  <li>&#9642; Up to 5 Gpts acquisition memory</li>
                  <li>&#9642; Always 12 bits of resolution, no compromises</li>
                  <li>&#9642; Intuitive navigation tools, find events of interest easily</li>
                </ul><a href="#one">Learn more...</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="section">
      <div class="card-panel green lighten-5">
        <div class="row">
          <span class="grey-text text-darken-4"
               style="font-size: larger;">Today’s highly complex power-conversion systems, automotive ECUs and embedded control systems are becoming smaller, denser and faster, with more signals, sensor inputs and lower-voltage power rails than
               ever.<br>
          WaveRunner 8000HD’s combination of 12-bit resolution at 2 GHz bandwidth, 8 (or more) channels, very long capture times, and a complete software toolset makes it ideal for these key applications.</span>
        </div>
        <div class="row">
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#two"><img src="/images/wr8000hd-3-phase-power-screen.png" alt="waverunner 8000hd oscilloscope three phase power screen"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#three"><img src="/images/wr8000hd-auto-electronics-screen.png" alt="waverunner 8000hd oscilloscope auto electronics screen"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#four"><img src="/images/wr8000hd-power-sequencing-screen.png" alt="waverunner 8000hd oscilloscope power sequencing screen"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#five"><img src="/images/wr8000hd-embedded-screen.png" alt="waverunner 8000hd oscilloscope embedded screen"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="wrap">
      <div class="container">
      <div class="row">
        <div class="col m7">
          <div class="card">
            <div class="card-image">
              <div class="material-placeholder"><img class="materialboxed initialized"
                   src="/images/wr8khd-callouts.png" alt="WaveRunner 8000HD Oscilloscope"></div>
            </div>
          </div>
        </div>
        <div class="col m5">
          <ul class="collection z-depth-1">
            <li class="collection-item"><button class="btn-floating btn-small green left">1</button> <span>HD4096 technology provides 12-bit resolution all the time</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">2</button> <span>8 analog channels standard, up to 16 with OscilloSYNC&trade; technology</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">3</button> <span>Up to 5 Gpts of acquisition memory enables detailed viewing of long events</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">4</button> <span>15.6” 1900 x 1080 Full HD capacitive touchscreen</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">5</button> <span>Mixed-signal capability – debug complex embedded designs with integrated 16-channel mixed signal option</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">6</button> <span>Easy connectivity with six USB 3.1 ports (2 front, 4 side), and UHD (4K) HDMI and DisplayPort outputs</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">7</button> <span>MAUI with OneTouch user interface for intuitive and efficient operation</span></li>
          </ul>
        </div>
      </div>
    </div></div>

  <div class="clearfix"></div>
    <div class="container"><div class="section"
         id="sync">
      <div class="row">
        <div class="col m6 s12">
          <h3>OscilloSYNC</h3>
          <p>Setup is incredibly easy with just four simple steps:</p>
          <ol>
            <li>Connect Ref. In/Out terminals.</li>
            <li>Connect Aux Out terminals.</li>
            <li>Connect Ethernet ports.</li>
            <li>Enter IP Address and press Connect.</li>
          </ol><span><i class="material-icons left">arrow_forward</i>Acquire 16 channels on one display.</span>
        </div>
        <div class="col m6 s12"><script src="https://fast.wistia.com/embed/medias/me32o6z8sw.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_me32o6z8sw popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
      </div>
    </div></div> <div class="clearfix"></div>
    <div class="wrap">
  <div class="container">
    <div class="section" id="videos">
            <div class="row"><h3 class="center">Videos</h3>
                <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ysin5rn3a0 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_4ja2dr8hyg popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_ssrlz8cpls popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                        <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_pdj1cnubp8 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                </div>
            </div>
        </div>
        </div><div class="clearfix"></div><div class="container">
    <div class="section"
         id="one">
      <div class="row">
        <div class="col m8 s12">
          <h3>Longest Memory, Simple Navigation</h3>
          <p>With up to 5 Gpts of acquisition memory, WaveRunner 8000HD 12-bit oscilloscopes capture long periods of time, yet maintain high sample rate for visibility into the smallest details.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/wr8000hd-long-memory2.png"
             width="300px"  alt="WaveRunner 8000HD Oscilloscope Long Memory"></div></div>
        <div class="row">
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Longest memory</strong></h5>
                <p>WaveRunner 8000HD oscilloscopes contain a sophisticated acquisition and memory management architecture that makes 5 Gpt acquisitions fast and responsive. More memory means more visibility into system behavior.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Simple navigation</strong></h5>
                <p>Long memory and high sample rates capture both millisecond-scale trends and picosecond-scale glitches. WaveRunner 8000HD oscilloscopes are equipped with an advanced user interface that makes it easy to find features, navigate
                directly using timebase scale and position knobs, or set up a zoom trace whichever you prefer. Apply analysis tools easily to any type of trace.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>No compromise</strong></h5>
                <p>WaveRunner 8000HD can acquire 500 ms of data at the full 10 GS/s sample rate and always with 12 bits of resolution. Oscilloscopes with less memory require trading sample rate for acquisition time.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="divider"></div>
      <div class="section"
           id="two">
        <div class="row">
          <div class="col m8 s12">
            <h3>3-phase Power Analysis</h3>
            <p>WaveRunner 8000HD 12-bit oscilloscopes deliver 8 analog channels (16 with OscilloSYNC), 3-phase power analysis software, and high performance probes for inverter subsection, power system and control testing.</p>
          </div>
          <div class="col m4 s12"><img class="materialboxed responsive-img center"
               src="/images/wr8000hd-3-phase-power2.png"
               width="300px" alt="WaveRunner 8000HD Oscilloscope Power Analysis"></div>
        </div>
        <div class="row">
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Static, dynamic, complete</strong></h5>
                <p>Analyze short or long acquisitions. The mean value Numerics table summarizes static performance, while per-cycle Waveforms help you understand dynamic behaviors. Use Zoom+Gate to isolate and correlate power system behaviors to
                control system activity during time periods as short as a single device switching cycle.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Comprehensive probing</strong></h5>
                <p>HVD series high voltage differential probes have 65 dB CMRR at 1 MHz with 1% gain accuracy, the widest voltage ranges, and up to 6 kV common-mode rating. Connect current probes or use your own transducers with the programmable
                CA10 current sensor adapter to create a customized “probe”. HVFO fiber-optic probes are ideal for gate drive probing.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Up to 16 analog channels</strong></h5>
                <p>8 analog inputs at up to 2 GHz let you monitor an H-bridge’s four pairs of device output and gate drive input signals. Cascaded H-bridges may be easily monitored using 12 channels, with three additional channels for output
                voltage. WaveRunner 8000HD has enough channels for full 3-phase power section input/output and control section analysis.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="divider"></div>
      <div class="section"
           id="three">
        <div class="row">
          <div class="col m8 s12">
            <h3>Automotive Electronics Testing</h3>
            <p>WaveRunner 8000HD 12-bit oscilloscopes combine a high channel count, long memory, and wide range of validation and debug software to best address the specific test needs of the automotive industry.</p>
          </div>
          <div class="col m4 s12"><img class="materialboxed responsive-img center"
               src="/images/wr8000hd-auto2.png"
               width="300px" alt="WaveRunner 8000HD Oscilloscope Automotive"></div>
        </div>
        <div class="row">
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Best vehicle bus debug tools</strong></h5>
                <p>Unique capabilities that build on our legacy serial data trigger and decode provide the most complete debug and validation of automotive buses. Cover all aspects of physical layer Automotive Ethernet testing with compliance test
                software and a dedicated Automotive Ethernet debug toolkit.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>More channels for ECU debug</strong></h5>
                <p>The flexibility of 8 12-bit analog channels and 16 digital channels makes WaveRunner 8000HD the best way to analyze the array of analog, digital and sensor signals in today’s complex ECUs. Easily capture system startup behavior
                and perform causal analysis with 5 Gpt of memory.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>EMI/EMC pre-compliance test</strong></h5>
                <p>12-bit resolution for spectral analysis provides more insight. Specialized EMC/EMI pulse parameters provide measurement flexibility. Support for all relevant electrical and magnetic field units of measure. Capability to measure
                sub-1 Hz magnetic field strengths.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="divider"></div>
      <div class="section"
           id="four">
        <div class="row">
          <div class="col m8 s12">
            <h3>Power Integrity and Power Sequencing Testing</h3>
            <p>WaveRunner 8000HD 12-bit oscilloscope's high resolution, long memory and high channel count let you validate and debug all aspects of power supply, delivery and consumption for complete confidence.</p>
          </div>
          <div class="col m4 s12">
              <img class="materialboxed responsive-img center" src="/images/wr8000hd-power-integrity2.png" width="300px" alt="WaveRunner 8000HD Oscilloscope Power Integrity"><br />
              <a href="https://go.teledynelecroy.com/l/48392/2018-07-10/6yl9rg"><img class="responsive-img" src="/images/btn-poster-pi.png" alt="power integrity poster form" /></a>
         </div>
        </div>

      <div class="row">
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Accurate PDN measurements</strong></h5>
              <p>Make sensitive measurements like rail collapse characterization with complete confidence thanks to WaveRunner 8000HD's high dynamic range and 0.5% gain accuracy. Its HD4096 architecture means an exceptionally low noise floor for
              easily pinpointing noise sources.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Specialized power probes</strong></h5>
              <p>Combine WaveRunner 8000HD with the RP4030 4 GHz Power Rail Probe for unsurpassed insight into PDN behavior. The variety of probe tips ensures easy connectivity, and its low loading characteristics minimize disruption to the device
              under test.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card large grey lighten-5">
            <div class="card-content">
              <h5><strong>Power sequencing</strong></h5>
              <p>8 analog channels with 12-bit resolution and high offset capability give full visibility into power sequencing behavior with 16 digital inputs available to decode and trigger on SPMI, SMbus, PMbus and other power management
              interfaces. Up to 5 Gpts of acquisition memory to capture every detail.</p>
            </div>
          </div>
        </div>
      </div></div>
      <div class="divider"></div>
      <div class="section"
           id="five">
        <div class="row">
          <div class="col m8 s12">
            <h3>Deeply Embedded Computing Systems Testing</h3>
            <p>WaveRunner 8000HD 12-bit oscilloscopes acquire the longest records at the highest resolution for the most comprehensive deeply embedded computing system analysis (analog, digital, serial data and sensor).</p>
          </div>
          <div class="col m4 s12"><img class="materialboxed responsive-img center"
               src="/images/wr8000hd-embedded2.png"
               width="300px" alt="WaveRunner 8000HD Oscilloscope Computing Systems Testing"></div>
        </div>
        <div class="row">
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Powerful, deep toolbox</strong></h5>
                <p>More standard math, measure, pass/ fail and other tools than other oscilloscopes provide faster and more complete insight into circuit problems. Many additional application packages are optionally available to enhance
                understanding.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>8 channels with long captures</strong></h5>
                <p>8 channels with 12-bit resolution make the WaveRunner 8000HD the best performing oscilloscope for embedded systems testing, specifically those with sensor signals. 5 Gpts of memory captures every detail when performing causal
                analysis.</p>
              </div>
            </div>
          </div>
          <div class="col m4 s12">
            <div class="card large grey lighten-5">
              <div class="card-content">
                <h5><strong>Comprehensive probe offering</strong></h5>
                <p>A wide selection of low voltage, high voltage and current probes accurately measures every signal in your circuit. Additional probe adapters easily integrate third-party probes.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
            </div><!-- end of container -->
                    <!--TECH DOCS-->
        <div class="container header-brief" id="docs"> 
 <ul class="collection with-header" style="-webkit-margin-after:0em;">
	<li class="collection-header blue darken-3 white-text"><h4>Technical Docs</h4></li>
	<li class="collection-item"><a href="https://go.teledynelecroy.com/l/48392/2019-11-10/7tw39x" target="_blank">
		<span class="title">Comparing High Resolution Oscilloscope Design Approaches</span>
		<i class="material-icons secondary-content">description</i></a>
		<p class="desc-tab">This white paper provides an overview of the various high resolution design approaches, with examples of their impact on oscilloscope performance.</p></li>
<li class="collection-item"><a href="/doc/power-rail-probing-options-appnote" target="_blank"><span class="title">Power-Rail Probing Options for Use with 12-Bit High Definition Oscilloscopes</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This application note explores considerations for probing power distribution networks, and compares options for probing power rails using Teledyne LeCroy’s 12-bit High Definition Oscilloscopes.</p></li>
 <li class="collection-item"><a href="/doc/10x-passive-probes-appnote" target="_blank">
<span class="title">Getting the Most Out of 10x Passive Probes</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">In this application note, we explain some best practices for getting good measurement results with standard, 10x passive probes.</p></li>
 <li class="collection-item"><a href="/doc/build-your-own-power-rail-probe-appnote" target="_blank">
<span class="title">Build Your Own Power Rail Probe</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This application note shows how to build a low-cost, simple power-rail probe for use in those applications where the 10x probe won’t do.</p></li>
 <li class="collection-item"><a href="/doc/power-rail-transient-response-appnote" target="_blank">
<span class="title">Analyzing Power-Rail Transient Response with 12-Bit High Definition Oscilloscopes</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">This application note describes how to use Teledyne LeCroy’s 12-bit High Definition Oscilloscopes to evaluate a single power rail’s response to the sudden application of a load, or the response of multiple rails within a power distribution network under similar conditions.</p></li>
<li class="collection-item"><a href="/doc/waverunner8000hd-vs-tek-3phase-power" target="_blank">
<span class="title">Competitive Comparison – 3-phase Power Conversion</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare 3-phase power conversion analysis capabilities for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Tektronix MSO58 oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/waverunner8000hd-vs-tek-automotive" target="_blank">
<span class="title">Competitive Comparison - Automotive Electronics</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare automotive electronics testing capabilities for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Tektronix MSO58 oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/waverunner8000hd-vs-tek-embedded" target="_blank">
<span class="title">Competitive Comparison - Deeply Embedded and Mechatronic Systems</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare deeply embedded/mechatronic systems testing capabilities for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Tektronix MSO58 oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/waverunner8000hd-vs-tek-pi-pseq" target="_blank">
<span class="title">Competitive Comparison - Power Integrity and Power Sequencing</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare power integrity and power sequencing analysis capabilities for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Tektronix MSO58 oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/waverunner8000hd-v-tek-5series" target="_blank">
<span class="title">WaveRunner 8000HD vs Tektronix 5 Series</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Tektronix 5 Series oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/waverunner8000hd-vs-yokogawa-power" target="_blank">
<span class="title">WaveRunner 8000HD vs Yokogawa DLM4000</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications and power analysis capabilities for the Teledyne LeCroy WaveRunner 8000HD 12-bit High Definition Oscilloscopes to the Yokogawa DLM4000 oscilloscopes.</p></li>
   </ul>
        </div>


    
    <footer class="page-footer white">
      <div class="container center social">
        <p><a href="https://facebook.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/facebook.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://twitter.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/twitter.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://youtube.com/lecroycorp"
           onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/youtube.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://blog.teledynelecroy.com/"
           onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/blogger.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://www.linkedin.com/company/10007"
           onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/linkedin.jpg"
             width="25px"></a></p>
        <p class="grey-text">&copy; 2019&nbsp;Teledyne LeCroy</p><a href="/support/securitypolicy.aspx"
             id="ucFooter_rptLinks_hlkLink_0"
             target="_blank">Privacy Policy</a> | <a href="/sitemap/"
             id="ucFooter_rptLinks_hlkLink_1"
             target="_blank">Site Map</a>
        <p><a href="http://teledyne.com/"
           onclick="GaEventPush('OutboundLink', 'http://teledyne.com');"
           target="_blank">Teledyne Technologies</a></p>
      </div>
    </footer>
  <script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async></script>
    <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
  <script src="js/materialize.min.js"></script> 
  <script src="js/init.js"></script> 
  <script async
        charset="ISO-8859-1"
        src="//fast.wistia.com/assets/external/E-v1.js"></script> 
  <script>



          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
  </script> 
  <script src="https://teledynelecroy.com/js/pardot.js"></script>
  </div>
</body>
</html>
