﻿<%@ Page Language="vb"  MasterPageFile="~/MasterPage/site.master" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.wr9000_default" %>
<%@ MasterType VirtualPath="~/MasterPage/site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteContent" runat="server">
<main id="main" role="main" class="has-header-transparented skew-style reset v1">
							<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
		<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
				<section class="section-banner-slide section-white section-angle-before angle-small small">
					<div class="banner-slider">
						<div class="banner-slide-item" style="background-image: url(https://assets.lcry.net/images/product-banner-default.png);"> 
							<div class="container">
								<div class="banner-slide-content">
									<div class="banner-slide-title"> 
										<h1>WaveRunner 9000 Oscilloscopes</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page"> 
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="page-title">
									<h2 class="blue">Unbelievably Powerful. Insanely Easy.</h2>
								</div>
								<div class="page-text">
									<p> <a href="https://teledynelecroy.com/oscilloscope/waverunner-9000-oscilloscopes">WaveRunner 9000 oscilloscopes </a>have the industry's widest and deepest collection of tools, making it unbelievably powerful and MAUI with 
										OneTouch makes measurement setup insanely easy, providing faster time to insight.
									</p>
								</div>
								<div class="page-button"><a class="btn btn-default btn-blue-outline" href="https://teledynelecroy.com/oscilloscope/waverunner-9000-oscilloscopes">WaveRunner 9000</a><a class="btn btn-default btn-blue-outline" href="http://cdn.teledynelecroy.com/files/pdf/waverunner9000-datasheet.pdf">Datasheet</a><a class="btn btn-default btn-blue-outline" href="https://teledynelecroy.com/oscilloscope/configure/configure_step2.aspx">Configure</a><a class="btn btn-default btn-blue-outline" href="#videos">Videos</a></div>
								<div class="row">
									<script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async=""></script>
									<script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script>
									<div class="col-md-8">
										<div class="visual text-center">
											<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
												<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_38sg6z1cj3 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-38sg6z1cj3-1" style="display:inline-block;height:100%;width:100%">
														<div class="wistia_click_to_play" id="wistia_45.thumb_container" style="position: relative; height: 190.875px; width: 339.359px;">
															<div id="wistia_105.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 190.875px; overflow: hidden; outline: none; position: relative; width: 339.344px;"></div>
															<div id="wistia_121.big_play_button_background" style="height: 190.875px; position: absolute; width: 339.344px; z-index: 1; background-color: rgba(0, 118, 192, 0.5); left: 0px; top: 0px; transition: all 80ms ease-out 0s; mix-blend-mode: multiply;">
																<div id="wistia_121.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 140px; top: 76px;"></div><img id="wistia_105.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/a657f6c2046cb37a108a89bf754f0055.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 190.875px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 339.344px; left: 0px; top: 0px;">
																<div id="wistia_121.big_play_button_bottom_text" style="color: rgb(255, 255, 255); font-family: &quot;Courier New&quot;, Courier, sans-serif; font-size: 16px; background: none; left: 0px; position: absolute; text-align: center; width: 100%; z-index: 1; top: 117px;">1:43       </div>
															</div>
														</div></span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6"><a href="/oscilloscope/waverunner-9000-oscilloscopes"><img src="https://assets.lcry.net/images/wr9000-hero.png" alt="img-description"></a></div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray">
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-visible text-center"><a data-fancybox="gallery1" href="https://assets.lcry.net/images/wr9000-1.png"><img src="https://assets.lcry.net/images/wr9000-1.png" alt="maui badge"></a></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>MAUI with OneTouch</h3>
											</div>
											<div class="card-text">
												<p>
													MAUI with OneTouch optimizes convenience and efficiency by enabling all common operations with a single touch of the display. Dramatically reduce setup time with revolutionary drag and drop actions to copy and setup channels, math functions, and measurement parameters 
													without lifting a finger.        
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-visible text-center"><img src="https://assets.lcry.net/images/wr9000-2.png" alt="obsessed with tools"></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Unbelievably Powerful                            </h3>
											</div>
											<div class="card-text">
												<p>
													The standard collection of math, measurement, debug, and documentation tools provides unsurpassed analysis capabilities. Application-specific packages enable streamlined debugging for common design/validation scenarios.  </p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="card-line">
										<div class="inner-visible text-center"><img src="https://assets.lcry.net/images/wr9000-3.png" alt="time-to-insight"></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Faster Time-to-Insight                                </h3>
											</div>
											<div class="card-text">
												<p>Insight alone is not enough. Markets and technologies change too rapidly. The timing of critical design decisions is significant. Time to insight is what matters.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="section-content">
							<div class="row">
								<div class="col-md-6">
									<div class="card-line">
										<div class="inner-content">  <a href="/tools/"><img class="mb-2" src="https://assets.lcry.net/images/wr9000-4.png" alt="periodic table of tools"></a>
											<div class="card-title">                                      
												<h3>Unbelievably Powerful                    </h3>
											</div>
											<div class="card-text">
												<p>WaveRunner 9000 oscilloscopes have the greatest breadth and depth of tools, ensuring quick resolution of the most complicated debug tasks. Our <a href="/tools/">Periodic Table of Oscilloscope Tools </a>provides a framework to understand the toolsets that Teledyne LeCroy has created and deployed in our oscilloscopes</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                            <a class="btn btn-default" href="/tools/">Explore</a></div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card-line">
										<div class="inner-content"> <a href="/maui/"><img class="mb-2" src="https://assets.lcry.net/images/wr9000-5.png" alt="maui screenshot"></a>
											<div class="card-title">                                      
												<h3>Insanely Easy                  </h3>
											</div>
											<div class="card-text">
												<p> <a href="/maui/">MAUI with OneTouch </a>delivers a unique set of touchscreen gestures that simplifies measurement setup and brings unsurpassed 
													efficiency and intuitiveness to oscilloscope operation. Common gestures such as drag, drop, pinch, and flick facilitate instinctive 
													interaction with the oscilloscope.
												</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                                 <a class="btn btn-default" href="/maui/">Explore</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page section-gray" id="videos">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Videos</h2>
						</div>
						<div class="section-content">
							<div class="row">
								<script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async=""></script>
								<script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_38sg6z1cj3 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-38sg6z1cj3-2" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_95.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_206.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;"><img id="wistia_206.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/a657f6c2046cb37a108a89bf754f0055.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
															<div id="wistia_221.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_221.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div>
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_uzb5ptzte1 wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-uzb5ptzte1-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_136.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_231.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;">
															<div id="wistia_246.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_246.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div><img id="wistia_231.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/66155c717b9ca992d294f4e8fe8af135.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-4">
									<div class="visual text-center">
										<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
											<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_qjngumviyf wistia_embed_initialized popover=true popoverAnimateThumbnail=true videoFoam=true" id="wistia-qjngumviyf-1" style="display:inline-block;height:100%;width:100%">
													<div class="wistia_click_to_play" id="wistia_197.thumb_container" style="position: relative; height: 199.312px; width: 354.359px;">
														<div id="wistia_307.thumbnail" tabindex="0" style="cursor: pointer; display: block; height: 199.297px; overflow: hidden; outline: none; position: relative; width: 354.344px;">
															<div id="wistia_322.big_play_button_background" style="height: 38px; position: absolute; width: 60px; z-index: 1; background-color: rgba(0, 118, 192, 0.76); left: 147px; top: 81px; transition: all 80ms ease-out 0s; mix-blend-mode: normal;"></div>
															<div id="wistia_322.big_play_button_graphic" tabindex="0" role="button" aria-label="Play" style="background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAmCAYAAACYsfiPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJNJREFUeNrs1ksNhEAURNGWgAQkjAQkIAUJ7QApIwEJSEACEh6PBatJhmZJ1T1JCahU+lMKAACAioj4Zjqnwqc9MzoVvszya8evLTM4FdZeO/7TWzva6Kwd7dbMx6nwpboV3l99rh+WXV5/lh+sOrlcWueqvcOzpLNqQ2GtVW8KT0WZ3MeisXAtLixWBQAAQDoEGACTzSwy3SX7YgAAAABJRU5ErkJggg==&quot;) 0px 0px / 60px 38px no-repeat transparent; cursor: pointer; display: block; height: 38px; outline: none; position: absolute; width: 60px; z-index: 1; left: 147px; top: 81px;"></div><img id="wistia_307.thumbnail_img" alt="Wistia video thumbnail" src="https://embedwistia-a.akamaihd.net/deliveries/500b16d1d33afddd270e00a898fdb352.webp?image_crop_resized=640x360" style="border: 0px; display: block; float: none; height: 199.297px; margin: 0px; max-height: none; max-width: none; padding: 0px; position: absolute; width: 354.344px; left: 0px; top: 0px;">
														</div>
													</div></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-default-page">
					<div class="container">
						<div class="section-heading text-center">             
							<h2>Serial Trigger, Decode, Measure/Graph, and Eye Diagrams</h2>
						</div>
						<div class="section-content">
							<p class="mb-4">
								Isolate events using the serial bus trigger and view color-coded protocol information on top of analog or digital waveforms. Timing and bus measurements allow quick and easy characterization of a serial data system. Serial (digital) data can be extracted and graphed to monitor
								system performance over time. Identify physical layer anomalies with eye diagram mask testing and mask failure locator.
							</p>
							<div class="row">
								<div class="col-md-6 col-xl-3">
									<div class="card-line">
										<div class="inner-visible text-center"><a data-fancybox="gallery10" href="https://assets.lcry.net/images/wr9000-6.png"><img src="https://assets.lcry.net/images/wr9000-6.png" alt="trigger-decode-icons"></a></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Trigger                                        </h3>
											</div>
											<div class="card-text">
												<p>
													Designed by people who know the standards, with the unique capabilities you want to isolate unusual events. Conditional data triggering permits maximum flexibility, and highly adaptable error frame triggering is available to isolate error conditions. Frame definition groups UART or SPI packets 
													into message frames for customization. Sequence Mode ignores idle time and acquires only data of interest.       
												</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                                 <a class="btn btn-default" href="https://teledynelecroy.com/trigger-decode/#trigger">Explore</a></div>
									</div>
								</div>
								<div class="col-md-6 col-xl-3">
									<div class="card-line">
										<div class="inner-visible text-center"><a data-fancybox="gallery11" href="https://assets.lcry.net/images/wr9000-7.png"><img src="https://assets.lcry.net/images/wr9000-7.png" alt="decode screenshot"></a></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Decode                                  </h3>
											</div>
											<div class="card-text">
												<p>
													Decoded protocol information is color-coded to specific portions of the serial data waveform and transparently overlaid for an intuitive, easy-to-understand visual record. All decoded protocols are displayed in a single time-interleaved table. Touch a row in the interactive table to quickly 
													zoom to a packet of interest and easily search through long records for specific protocol events using the built-in search feature.
												</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                            <a class="btn btn-default" href="/trigger-decode/#decode">Explore   </a></div>
									</div>
								</div>
								<div class="col-md-6 col-xl-3">
									<div class="card-line">
										<div class="inner-visible text-center"><a data-fancybox="gallery12" href="https://assets.lcry.net/images/wr9000-8.png"><img src="https://assets.lcry.net/images/wr9000-8.png" alt="measure screenshot"></a></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Measure/Graph                                </h3>
											</div>
											<div class="card-text">
												<p>
													Quickly validate cause and effect with automated timing measurements to or from an analog signal or another serial message. Make multiple measurements in a single long acquisition to quickly acquire statistics during corner-case testing. Serial (digital) 
													data can be extracted to an analog value and graphed to monitor system performance over time, as if it was probed directly. 
													Complete validation faster and gain better insight.
												</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                        <a class="btn btn-default" href="/trigger-decode/#measure">Explore</a></div>
									</div>
								</div>
								<div class="col-md-6 col-xl-3">
									<div class="card-line">
										<div class="inner-visible text-center"><a data-fancybox="gallery13" href="https://assets.lcry.net/images/wr9000-9.png"><img src="https://assets.lcry.net/images/wr9000-9.png" alt="eye diagram screeenshot"></a></div>
										<div class="inner-content">  
											<div class="card-title">                                      
												<h3>Eye Diagram                                </h3>
											</div>
											<div class="card-text">
												<p>
													Rapidly display an eye diagram of your packetized low-speed serial data signal without additional setup time. Use eye parameters to quantify system performance and apply a standard or custom mask to identify anomalies. Mask failures can be indicated and can 
													force the scope into Stop mode.
												</p>
												<p>SDAIII or DDR Debug (optional) create eye diagrams of streaming NRZ serial data or DDR signals, and measure and analyze jitter breakdown.</p>
											</div>
										</div>
										<div class="inner-visible p-40 pt-0">                                          <a class="btn btn-default" href="https://teledynelecroy.com/trigger-decode/#eye">Explore                                </a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
	</asp:Content>