﻿Public Class wr9000_default
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Teledyne LeCroy - WaveRunner 9000 Oscilloscopes"
        Me.MetaDescription = "Teledyne LeCroy WaveRunner 9000 oscilloscopes make measurement setup insanely easy, providing faster time to insight"

        Master.MetaOgTitle = "Teledyne LeCroy WaveRunner 9000 Oscilloscopes"
        Master.MetaOgImage = "https://assets.lcry.net/images/og-wr9000-oscilloscope.png"
        Master.MetaOgDescription = "Teledyne LeCroy WaveRunner 9000 oscilloscopes make measurement setup insanely easy, providing faster time to insight"
        Master.MetaOgUrl = "https://teledynelecroy.com/wr9000"
        Master.MetaOgType = "website"
        Master.BindMetaTags(True) 'if using site.master set to TRUE, if using other master set to FALSE

    End Sub
End Class