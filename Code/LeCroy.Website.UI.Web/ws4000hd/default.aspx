﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="LeCroy.Website.ws4000hd_default" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>WaveSurfer 4000HD High Definition Oscilloscopes | Teledyne LeCroy</title>
  <meta content="IE=Edge"
        http-equiv="X-UA-Compatible">
  <meta content="text/html; charset=utf-8"
        http-equiv="Content-Type">
  <meta content="width=device-width, initial-scale=1"
        name="viewport"><!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <link href="css/materialize.min.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css">
  <link href="css/style.css"
        media="screen,projection"
        rel="stylesheet"
        type="text/css"><!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/style-ie7.css"></link><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Roboto+Condensed:300,400,700"
        rel="stylesheet"
        type="text/css">
  <style type="text/css">
  .orange2 {
        background-color:#ff8e00;
  }
  .maui-blue{
    color:#0076c0;
  }
      .btn-floating {font-size: 18px; font-weight: 500;margin-right:10px;
      }
      .card.larger {height: 675px;
      }
  </style>
    <meta property="og:title" content="Teledyne LeCroy WaveSurfer 4000HD Oscilloscopes"/>
<meta property="og:image" content="https://teledynelecroy.com/images/og-ws4000hd-oscilloscope.jpg"/>
<meta property="og:description" content="The WaveSurfer 4000HD Oscilloscope answers the call as a highly affordable instrument that leverages Teledyne LeCroy’s HD4096 High Definition technology to deliver 12-bit resolution all the time." />
<meta property="og:url" content="https://teledynelecroy.com/ws4000hd/" />
    <meta property="og:type" content="website" />

<link href="https://teledynelecroy.com/ws4000hd/" rel="canonical"  />
</head>
<body>
  <div class="container">
    <a href="../"
         id="logo-container"><img alt="Teledyne LeCroy"
         class="brand-logo"
         src="img/tl_weblogo_blkblue_189x30.png"></a>
  </div>
  <div class="parallax-container"
       id="index-banner">
    <div class="section">
      <div class="container">
        <div class="row">
            <div class="col l6 s12">
            <a href="/oscilloscope/wavesurfer-4000hd-oscilloscopes"><img alt="WaveSurfer 4000HD Oscilloscope"
                 class="left responsive-img"
                 src="/images/wavesurfer4000hd-top.png"></a>
          </div>
          <div class="col l6 s12">
            <h1 class="h1 maui-blue">WaveSurfer 4000HD</h1>
            <h3 class="green-text">Unrivaled Performance. Unbeatable Value.</h3>
            <p class="h3 black-text">4 channels, 1 GHz, 25 Mpts</p>
            <p class="h3 black-text">12 bits <strong>all the time</strong></p>
            <p><a class="waves-effect waves-light btn blue darken-2" href="/doc/wavesurfer-4000hd-datasheet"><i class="material-icons left">info</i>Datasheet</a>&nbsp;
                <a class="waves-effect waves-light btn green" href="/oscilloscope/configure/configure_step2.aspx?seriesid=609"><i class="material-icons left">settings</i>Configure</a>
                &nbsp;<a class="waves-effect waves-light btn orange darken-3"
               href="#videos"><i class="material-icons left">play_circle_filled</i>Videos</a>&nbsp;<a class="waves-effect waves-light btn blue darken-3"
               href="#docs"
               style="margin:5px;"><i class="material-icons left">reorder</i>Technical Docs</a>
            </p>
              <p><span class="wistia_embed wistia_async_f4spblqnmg popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:205px;position:relative;width:365px">&nbsp;</span></p>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="section">
        <!--   Icon Section   -->
        <div class="row">
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center"
                   src="/images/hd4096-itransport-ws4k.png" alt="wavesurfer4000hd oscilloscope hd4096"></div>
              <div class="card-content"
                   style="max-height: 100%">
                <h5 class="center green-text text-accent-4">Highest Resolution</h5>
                <p style="padding-bottom: 0px;">HD4096 technology enables 12 bits of vertical resolution with 1 GHz bandwidth</p>
                <ul>
                  <li>&#9642; Clean, Crisp Waveforms</li>
                  <li>&#9642; More Signal Details</li>
                  <li>&#9642; Unmatched Measurement Precision</li>
                </ul><span class="wistia_embed wistia_async_2kced34urr popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">Learn more...</a></span>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center" src="/images/word-cloud-scopes.png" alt="wavesurfer4000hd oscilloscope suite"></div>
              <div class="card-content">
                <h5 class="center green-text text-accent-4">More Capability</h5>
                <p style="padding-bottom: 0px;">A suite of options for power, serial bus and embedded systems debug</p>
                <ul>
                  <li>&#9642; Multi-instrument options</li>
                  <li>&#9642; Serial trigger & decode software</li>
                  <li>&#9642; Spectrum & power analysis software</li>
                </ul><a href="#greenbar">Learn more...</a>
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card larger hoverable">
              <div class="card-image"><img class="responsive-img center" src="/images/probe-support.png" alt="wavesurfer4000hd oscilloscope probe support"></div>
              <div class="card-content">
                <h5 class="center green-text text-accent-4">Comprehensive Probe Support</h5>
                <p>Over 30 probes in 9 categories</p>
                <ul>
                  <li>&#9642; Active power rail probes</li>
                  <li>&#9642; Current probes</li>
                  <li>&#9642; High-voltage differential and fiber optic probes</li>
                  <li>&#9642; TekProbe adapters</li>
                </ul><span class="wistia_embed wistia_async_e0wb1tjt86 popover=true popoverContent=link" style="display:inline;position:relative"><a href="#">Learn more...</a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="clearfix"></div>
  <div class="container">
    <div class="section" id="videos">
            <div class="row">
                <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_f4spblqnmg popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_273w5j5jji popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_e0wb1tjt86 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                  <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_jlen8kljmq popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                    </div><div class="row"><div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_2kced34urr popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
                <div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_jqn1nlx3h3 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div><div class="col m3 s12"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_katv68awvc popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div></div></div>
            </div>
            </div></div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="section" id="greenbar">
      <div class="card-panel green lighten-5">
        <div class="row">
          <span class="grey-text text-darken-4"
               style="font-size: larger;">Today’s highly complex power-conversion systems and embedded control systems are becoming smaller and denser, with more sensor inputs and lower-voltage power rails than ever.
<br>
               WaveSurfer 4000HD’s combination of 12-bit resolution at 1 GHz bandwidth and a competitive software toolset makes it ideal for these key applications.
               </span>
        </div>
        <div class="row">
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#one"><img src="/images/wr4khd-clock-analysis.png" alt="wavesurfer4000hd oscilloscope clock analysis"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#two"><img src="/images/wr4khd-power-rail-analysis.png" alt="wavesurfer4000hd oscilloscope power rail analysis"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#three"><img src="/images/wr4khd-protocol-analysis.png" alt="wavesurfer4000hd oscilloscope protocol analysis"></a>
              </div>
            </div>
          </div>
          <div class="col s12 m3">
            <div class="card hoverable">
              <div class="card-image">
                <a href="#four"><img src="/images/wr4khd-power-analysis.png" alt="wavesurfer4000hd oscilloscope power analysis"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="wrap">
    <div class="container">
      <div class="row">
        <div class="col m7">
          <div class="card">
            <div class="card-image">
              <div class="material-placeholder"><img class="materialboxed initialized"
                   src="/images/ws4khd-callouts.png" alt="WaveSurfer 4000HD Oscilloscope"></div>
            </div>
          </div>
        </div>
        <div class="col m5">
          <ul class="collection z-depth-1">
            <li class="collection-item"><button class="btn-floating btn-small green left">1</button> <span>HD4096 technology provides 12-bit resolution all the time</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">2</button> 
              <span>ProBus input supports over 30 probes in 9 product categories</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">3</button> 
              <span>Long record capture of up to 25 Mpts</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">4</button> 
              <span>12.1” 1280 x 800 HD capacitive touchscreen</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">5</button> 
              <span>Mixed-signal capability – debug complex embedded designs with integrated 16-channel mixed signal option</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">6</button> 
              <span>Easy connectivity with 2 USB 3.1 ports</span></li>
            <li class="collection-item"><button class="btn-floating btn-small green left">7</button> 
              <span>MAUI with OneTouch user interface for intuitive and efficient operation</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="section"
         id="one">
      <div class="row">
        <div class="col m8 s12">
          <h3>Clock Analysis</h3>
          <p>Use the standard toolset of WaveSurfer 4000HD 12-bit oscilloscopes for good insight into the quality of a clock signal.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/sub-clock-analysis.png"
             width="300px" alt="wavesurfer4000hd oscilloscope clock analysis"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card small grey lighten-5">
            <div class="card-content">
              <h6><strong>Built-in Anomaly Detection</strong></h6>
              <p>Use built-in WaveScan or conditional Smart triggers to find anomalous clock edges.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card small grey lighten-5">
            <div class="card-content">
              <h6><strong>Statistical View of Data</strong></h6>
              <p>Capture long records to build statistics faster. All-instance measurements provide statistics for every clock edge for any acquisition length.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card small grey lighten-5">
            <div class="card-content">
              <h6><strong>Graphical View of Data</strong></h6>
              <p>Draw a Trend of measurement values over time. Use Histicons (miniature histograms) to show statistical distributions.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="two">
      <div class="row">
        <div class="col m8 s12">
          <h3>Power Rail Analysis</h3>
          <p>WaveSurfer 4000HD 12-bit oscilloscopes are ideally suited to power rail analysis.</p></div>
 <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/sub-power-rail-analysis.png"
             width="300px" alt="wavesurfer4000hd oscilloscope power rail analysis"><br />
              <a href="https://go.teledynelecroy.com/l/48392/2018-07-10/6yl9rg"><img src="/images/btn-poster-pi.png" alt="power integrity poster link" class="responsive-img" /></a></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Most Accurate Measurements</strong></h6>
              <p>HD4096 12-bit resolution clearly shows small signal details in power rails. Make sensitive measurements like rail collapse characterization with total confidence thanks to WaveSurfer 4000HD’s 0.5% gain accuracy.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Find Noise Sources</strong></h6>
              <p>View signals in the frequency domain using FFT (standard) or Spectrum Analyzer (optional) to determine the root cause of high noise events.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Native Probing</strong></h6>
              <p>Built-in high offset capability permits native probing of power rails and other wide dynamic range signals.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="three">
      <div class="row">
        <div class="col m8 s12">
          <h3>Protocol Analysis</h3>
          <p>WaveSurfer 4000HD 12-bit oscilloscopes support serial trigger and decode for widely used embedded system protocols: CAN, CAN FD, LIN, FlexRay, I2C, SPI and UART-RS232.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/sub-protocol-analysis.png"
             width="300px" alt="wavesurfer4000hd oscilloscope protocol analysis"></div>
      </div>
      <div class="row">
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Intuitive Decoding</strong></h6>
              <p>Color-coded overlays mark important protocol elements on the electrical signal. Digital decoding appears on overlay as well as in exportable result table.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Powerful Serial Triggering</strong></h6>
              <p>Trigger on protocol elements or specific DATA patterns using powerful conditional DATA triggering. Highly adaptable ERROR frame triggering isolates protocol errors. Combine UART/SPI bytes into single “message frame” to trigger on proprietary protocols.</p>
            </div>
          </div>
        </div>
        <div class="col m4 s12">
          <div class="card medium grey lighten-5">
            <div class="card-content">
              <h6><strong>Time Synchronized Views</strong></h6>
              <p>Interactive table shows simultaneous decoding of two protocols. Use Search and Zoom to correlate protocol events to other embedded signals.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="divider"></div>
    <div class="section"
         id="four">
      <div class="row">
        <div class="col m8 s12">
          <h3>Power Analysis</h3>
          <p>Add the Power Analysis software option to WaveSurfer 4000HD 12-bit oscilloscopes to measure and analyze the operating characteristics of power conversion circuits.</p>
        </div>
        <div class="col m4 s12"><img class="materialboxed responsive-img center"
             src="/images/sub-power-analysis.png"
             width="300px"  alt="wavesurfer4000hd oscilloscope power analysis"></div>
      </div>
    </div>
    <div class="row">
      <div class="col m4 s12">
        <div class="card small grey lighten-5">
          <div class="card-content">
            <h6><strong>Predefined Tests</strong></h6>
            <p>Analyze device, control loop or line power using predefine tests for losses, safe operating area, B-H curve, etc.</p>
          </div>
        </div>
      </div>
      <div class="col m4 s12">
        <div class="card small grey lighten-5">
          <div class="card-content">
            <h6><strong>Easy Test Setup</strong></h6>
            <p>Quickly setup voltage and current inputs. The software uses color-coded overlays to identify turn-on and turn-off transitions.</p>
          </div>
        </div>
      </div>
      <div class="col m4 s12">
        <div class="card small grey lighten-5">
          <div class="card-content">
            <h6><strong>Power Measurements</strong></h6>
            <p>Automatically calculate switching device measurements, input/output power and input harmonics.</p>
          </div>
        </div>
      </div>
    </div>
    
  </div><!-- end of container -->
     <!--TECH DOCS-->
        <div class="container header-brief" id="docs"> 
 <ul class="collection with-header" style="-webkit-margin-after:0em;">
	<li class="collection-header blue darken-3 white-text"><h4>Technical Docs</h4></li>

<li class="collection-item"><a href="/doc/wavesurfer-oscilloscopes-labnotebook" target="_blank"><span class="title">Documenting Lab Results with WaveSurfer 4000HD</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Different utilities built into WaveSurfer 4000HD allow you to capture and share waveform and measurement data to document your lab results.</p></li>
 <li class="collection-item"><a href="/doc/wavesurfer-oscilloscopes-decoding-appnote" target="_blank">
<span class="title">Decoding Serial Buses with WaveSurfer Oscilloscopes</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">WaveSurfer 3000z and WaveSurfer 4000HD oscilloscopes support serial trigger and decode for protocols commonly used in embedded systems design, helping you quickly find and fix problems on the bus.</p></li>
<li class="collection-item"><a href="/doc/wavesurfer4000hd-v-tek" target="_blank">
<span class="title">WaveSurfer 4000HD vs Tektronix 4 Series </span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WaveRunner 4000HD 12-bit High Definition Oscilloscopes to the Tektronix 4 Series oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/wavesurfer4000hd-v-rto" target="_blank">
<span class="title">WaveSurfer 4000HD vs Rohde & Schwarz RTA4000 Series</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WaveRunner 4000HD 12-bit High Definition Oscilloscopes to the Rohde & Schwarz RTA4000 Series oscilloscopes.</p></li>

<li class="collection-item"><a href="/doc/wavesurfer4000hd-v-keysight-s-series" target="_blank">
<span class="title">WaveSurfer 4000HD vs Keysight 4000X Series</span>
<i class="material-icons secondary-content">description</i></a>
<p class="desc-tab">Compare banner specifications for the Teledyne LeCroy WaveRunner 4000HD 12-bit High Definition Oscilloscopes to the Keysight 4000X Series oscilloscopes.</p></li>
   </ul>
        </div>
    <footer class="page-footer white">
      <div class="container center social">
        <p><a href="https://facebook.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://facebook.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/facebook.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://twitter.com/teledynelecroy"
           onclick="GaEventPush('OutboundLink', 'https://twitter.com/teledynelecroy');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/twitter.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://youtube.com/lecroycorp"
           onclick="GaEventPush('OutboundLink', 'https://youtube.com/lecroycorp');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/youtube.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://blog.teledynelecroy.com/"
           onclick="GaEventPush('OutboundLink', 'https://blog.teledynelecroy.com/');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/blogger.jpg"
             width="25px"></a> &nbsp;&nbsp; <a href="https://www.linkedin.com/company/10007"
           onclick="GaEventPush('OutboundLink', 'https://www.linkedin.com/company/10007');"
           target="_blank"><img border="0"
             height="25px"
             src="https://assets.lcry.net/images/linkedin.jpg"
             width="25px"></a></p>
        <p class="grey-text">&copy; 2020&nbsp;Teledyne LeCroy</p><a href="/support/securitypolicy.aspx"
             id="ucFooter_rptLinks_hlkLink_0"
             target="_blank">Privacy Policy</a> | <a href="/sitemap/"
             id="ucFooter_rptLinks_hlkLink_1"
             target="_blank">Site Map</a>
        <p><a href="http://teledyne.com/"
           onclick="GaEventPush('OutboundLink', 'http://teledyne.com');"
           target="_blank">Teledyne Technologies</a></p>
      </div>
    </footer>
  <script src="https://fast.wistia.com/embed/medias/ysin5rn3a0.jsonp" async></script>
    <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
  <script src="js/materialize.min.js"></script> 
  <script src="js/init.js"></script> 
  <script async
        charset="ISO-8859-1"
        src="//fast.wistia.com/assets/external/E-v1.js"></script> 
  <script>



          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-13095488-1', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
  </script> 
  <script src="https://teledynelecroy.com/js/pardot.js"></script>

</body>
</html>
