USE [TDY_LECROY_webcatalog]
GO

INSERT INTO [dbo].[EXHIBITIONS]
           ([title]
           ,[description]
           ,[type_id]
           ,[country_id]
           ,[state]
           ,[city]
           ,[address]
           ,[exhibition_startdate]
           ,[exhibition_enddate]
           ,[exhibition_time]
           ,[URL]
           ,[Logo]
           ,[topic_id]
           ,[disabled]
           ,[ARCHIVE_YN]
           ,[ARCHIVE_URL]
           ,[rowguid]
           ,[LocaleId]
           ,[ShowAddToCalendarYN]
           ,[TimeZone])
     VALUES
           (<title, nvarchar(400),>
           ,<description, nvarchar(500),>
           ,<type_id, int,>
           ,<country_id, int,>
           ,<state, nvarchar(50),>
           ,<city, nvarchar(100),>
           ,<address, nvarchar(500),>
           ,<exhibition_startdate, datetime,>
           ,<exhibition_enddate, datetime,>
           ,<exhibition_time, nvarchar(50),>
           ,<URL, nvarchar(500),>
           ,<Logo, nvarchar(250),>
           ,<topic_id, int,>
           ,<disabled, char(1),>
           ,<ARCHIVE_YN, char(1),>
           ,<ARCHIVE_URL, nvarchar(500),>
           ,<rowguid, uniqueidentifier,>
           ,<LocaleId, int,>
           ,<ShowAddToCalendarYN, char(1),>
           ,<TimeZone, varchar(50),>)
GO

USE [TDY_LECROY_webcatalog]
GO

INSERT INTO [dbo].[ExhibitionCategoryXref]
           ([ExhibitionFkId]
           ,[ExhibitionCategoryFkId]
           ,[RowGuid])
     VALUES
           (<ExhibitionFkId, int,>
           ,<ExhibitionCategoryFkId, int,>
           ,<RowGuid, uniqueidentifier,>)
GO

USE [TDY_LECROY_webcatalog]
GO

INSERT INTO [dbo].[ExhibitionCategory]
           ([Category]
           ,[SortOrder]
           ,[RowGuid])
     VALUES
           (<Category, varchar(50),>
           ,<SortOrder, int,>
           ,<RowGuid, uniqueidentifier,>)
GO


